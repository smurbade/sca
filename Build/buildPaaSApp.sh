# --------------------------------
# Author: Arjun Udagatti
# --------------------------------

ANS=""
DATA=0
COMPILE=0
BUNDLE=0
ADVT_BUNDLE=0
FORM_BUNDLE=0
SCA_FLASH_FILES=0
VER=""

# Defining the path for the application package data
cd appExecData
APP_PATH=`pwd`
cd -

# Defining the path for the forms 
cd ../FormDeck
FRM_PATH=`pwd`
cd -

# Defining the path where the application package will be created
PS_PKG_DIR=./package

echo ""
# Get Compilation option
if [ "$1" = "y" ] || [ "$1" = "Y" ]
then
	echo "Compilation Required"
	COMPILE=1
elif [ "$1" = "n" ] || [ "$1" = "N" ]
then
	echo "Compilation Not Required"
	COMPILE=0
else
	while [ $DATA = 0 ]
	do
		echo "Want to Compile Paas Application?[Y/N]"
		read ANS
		case $ANS in
			"Y" | "y" )
			COMPILE=1
			DATA=1
			;;

			"N" | "n" )
			COMPILE=0
			DATA=1
			;;
		esac
	done
fi

echo ""
# Get the bundling option
if [ "$2" = "y" ] || [ "$2" = "Y" ]
then
	echo "PaaS Bundling Enabled"
	BUNDLE=1
elif [ "$2" = "n" ] || [ "$2" = "N" ]
then
	echo "PaaS Bundling Not Required"
	BUNDLE=0
else
	DATA=0
	while [ $DATA = 0 ]
	do
		echo "Want to create PaaS App bundle?[Y/N]"
		read ANS
		case $ANS in
			"Y" | "y" )
			BUNDLE=1
			DATA=1
			;;

			"N" | "n" )
			BUNDLE=0
			DATA=1
			;;
		esac
	done
fi

echo ""
# if the bundling is YES, ask for version
if [ $BUNDLE = 1 ]
then
	if [ "$3" = "" ]
	then
		while [ "$VER" = "" ]
		do
			echo "Please enter the version no for applicatioon"
			read VER
		done
	else
		VER=$3
	fi
fi

#check if advertisement bundling is required
if [ "$4" = "y" ] || [ "$4" = "Y" ]
then
	echo "PaaS Advertisement Bundling Enabled"
	ADVT_BUNDLE=1
elif [ "$4" = "n" ] || [ "$4" = "N" ]
then
	echo "PaaS Advertisement Bundling Not Required"
	ADVT_BUNDLE=0
else
	DATA=0
	while [ $DATA = 0 ]
		do
		echo "Want to create PaaS Advertisement Media bundle?[Y/N]"
		read ANS
		case $ANS in
			"Y" | "y" )
			ADVT_BUNDLE=1
			DATA=1
			;;

			"N" | "n" )
			ADVT_BUNDLE=0
			DATA=1
			;;
		esac
	done
fi

#check if Form bundling is required
if [ "$5" = "y" ] || [ "$5" = "Y" ]
then
	echo "PaaS Form Bundling Enabled"
	FORM_BUNDLE=1
elif [ "$5" = "n" ] || [ "$5" = "N" ]
then
	echo "PaaS Form Bundling Not Required"
	FORM_BUNDLE=0
else
	DATA=0
	while [ $DATA = 0 ]
		do
		echo "Want to create PaaS Form bundle?[Y/N]"
		read ANS
		case $ANS in
			"Y" | "y" )
			FORM_BUNDLE=1
			DATA=1
			;;

			"N" | "n" )
			FORM_BUNDLE=0
			DATA=1
			;;
		esac
	done
fi

#check if SCA FILES bundling is required
if [ "$6" = "y" ] || [ "$6" = "Y" ]
then
	echo "Adding of SCA_FLASH_FILES to Forms Bundle Enabled"
	SCA_FLASH_FILES=1
elif [ "$6" = "n" ] || [ "$6" = "N" ]
then
	echo "Adding of SCA_FLASH_FILES to Forms Bundle Not Required"
	SCA_FLASH_FILES=0
else
	DATA=0
	while [ $DATA = 0 ]
		do
		echo "Want to add SCA_FLASH_FILES to forms bundle[Y/N]"
		read ANS
		case $ANS in
			"Y" | "y" )
			SCA_FLASH_FILES=1
			DATA=1
			;;

			"N" | "n" )
			SCA_FLASH_FILES=0
			DATA=1
			;;
		esac
	done
fi

# Start the compilation if needed
if [ $COMPILE = 1 ]
then
	echo ""
	echo " ------- Running Make ---------"
	export CYGPATH=cygpath
	make
	if [ $? = 0 ]
	then
		echo ""
		echo " ------- Make is SUCCESSFUL ------- "
	else
		echo ""
		echo " ------- Make FAILED --------- "
		return $?
	fi
fi

# Start the bundling if needed
if [ $BUNDLE = 1 ]
then
	echo "Bundling the PaaS Application"
	cd $PS_PKG_DIR
	./mkAppPkg.sh $VER $APP_PATH
	if [ $? = 0 ]
	then
		echo ""
		echo " ----------- Bundling SUCCESSFUL ----------"
	else
		echo ""
		echo " ----------- Bundling FAILED -------------- "
		return $?
	fi
	cd -
fi

# Start the bundling if needed
if [ $FORM_BUNDLE = 1 ]
then
	echo ""
	echo "Bundling Forms For PaaS Application"
	cd $PS_PKG_DIR
	./mkFormsPkg.sh $FRM_PATH $APP_PATH $SCA_FLASH_FILES
	if [ $? = 0 ]
	then
		echo ""
		echo " ----------- Bundling SUCCESSFUL ----------"
	else
		echo ""
		echo " ----------- Bundling FAILED -------------- "
	fi
	cd -
fi

# Start the bundling if needed
if [ $ADVT_BUNDLE = 1 ]
then
	echo ""
	echo "Bundling Advertisement media for PaaS Application"
	cd $PS_PKG_DIR
	./mkAdvtPkg.sh
	if [ $? = 0 ]
	then
		echo ""
		echo " ----------- Bundling SUCCESSFUL ----------"
	else
		echo ""
		echo " ----------- Bundling FAILED -------------- "
	fi
	cd -
fi