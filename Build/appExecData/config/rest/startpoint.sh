#!/bin/sh
#        
# This script is called by the *GO variable, i.e., we have in config.usr1 *GO=startpoint.sh
# This allows for an app to be started indirectly via the config.usr1 parameter *pointapp,
# so it allows a sysmode user to simply edit the *pointapp variable to be the app required to be run.
# e.g.                                                                                               
# - run release-mode version of app:
# *pointapp=point            
# - but the sysmode user can change it to be the following to run the debug version of the app:
# *pointapp=pointD                                                                      
#
sleep 15s     
appname=`/usr/local/bin/getenvfile *scaapp | tr "[:upper:]" "[:lower:]"`
if [ "$appname" = scad ]; then             
    ./PointMxSCAD.exe &
else                   
    ./PointMxSCA.exe &
fi                    
exit 0