#ifndef __CFG_DEF_H
#define __CFG_DEF_H

typedef enum
{
	T_ONE_TWO = 1,
	T_TWO_ONE
}
TRACK_ORDER_ENUM;

/* Card Swipe Settings */
/* Not loading any of these values in the structure
 * Refer to nrf demo for loading of the varaibles
 */
typedef struct
{
	short		maxBadSwipes;
	char		trk1StrtSentinel;
	char		trk1EndSentinel;
	char		trk1AcctSep;
	char		trk1NameSep;
	char		trk2StrtSentinel;
	char		trk2EndSentinel;
	char		trk2AcctSep;
}
CRDSLD_STRUCT_TYPE, * CRDSLD_PTR_TYPE;


#endif
