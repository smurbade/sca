#ifndef __UI_CFG_H
#define __UI_CFG_H

#include "uiGeneral.h"
#include "uiConstants.h"

/* Would be used in selecting the forms according to the platform */
#define MODEL_MX915	915
#define MODEL_MX925	925

#define MEDIA_DIR			"./flash/ADVT_MEDIA/"
#define MAX_VIDEO_COUNT		10
#define MAX_IMAGE_COUNT		50
#define MAX_DISP_PROMPT		52
#define MAX_DISP_MSG_SIZE	100
#define MAX_DISP_TITLE_SIZE	100
#define DFLT_IMG_UPD_TIME	5	/* 5 seconds */
#define MAX_IMG_UPD_TIME	120	/* 120 seconds */

#define DFLT_915_SIG_BEGX	3
#define DFLT_915_SIG_BEGY	3
#define DFLT_915_SIG_ENDX	205
#define DFLT_915_SIG_ENDY	205

#define DFLT_925_SIG_BEGX	10
#define DFLT_925_SIG_BEGY	10
#define DFLT_925_SIG_ENDX	410
#define DFLT_925_SIG_ENDY	410

#define DFLT_IP_SHOW_TIME	 	30	/* 30 seconds */
#define DFLT_SYSINFO_DISP_TIME	30	/* 30 seconds */
#define MAX_IP_SHOW_TIME		120	/* 120 seconds */
#define MAX_SYSINFO_DISP_TIME	120	/* 120 seconds */

#define SIGN_IMAGE_TYPE_TIFF	"TIFF"
#define SIGN_IMAGE_TYPE_BMP		"BMP"

/*
 * ------------------------------------------------------------------
 * Media Settings
 * ------------------------------------------------------------------
 */
/* Media Type */
typedef enum
{
	IMAGE,
	VIDEO,
	VOLUME
}
MEDIA_TYPE;

/* Particular Media config Settings */
typedef struct
{
	MEDIA_TYPE	advtMediaType;
	int			mediaUpdIntvl;
	char **		mediaList;
}
MEDIACFG_STYPE, * MEDIACFG_PTYPE;

/* UIAgent Media set config Settings */
typedef struct
{
	MEDIACFG_STYPE	stIdleScrImgSet;
	MEDIACFG_STYPE	stLIScrImgSet;
	MEDIACFG_STYPE	stVideoSet;
}
MEDIASET_STYPE, * MEDIASET_PTYPE;

/*
 * ------------------------------------------------------------------
 * Signature Capture Settings 
 * ------------------------------------------------------------------
 */
typedef struct
{
	int		startX;
	int		startY;
	int		endX;
	int		endY;
	char	szImageType[10];
}
SIG_CFG_STYPE, * SIG_CFG_PTYPE;

/*
 * ------------------------------------------------------------------
 * UIAgent Communication Settings 
 * ------------------------------------------------------------------
 */
typedef struct
{
	char	agentIP[16];
	uint	agentPort;
}
COM_CFG_STYPE, * COM_CFG_PTYPE;

/*
 * ------------------------------------------------------------------
 * UIAgent Display Prompts Settings
 * ------------------------------------------------------------------
 */

/* Labels */
typedef struct
{
	uchar	szLblOne[33];
	uchar	szLblTwo[33];
}
BTNLBL_STYPE, * BTNLBL_PTYPE;

/* UI Display prompts */
typedef struct
{
	char			szLang[25];
	BTNLBL_PTYPE	btnLbls[MAX_BTNLBL_IDX];
	uchar *			dispMsgs[MAX_DISP_MSGS];
	uchar *			scrTitles[MAX_TITLE_IDX];
}
DISPCFG_STYPE, * DISPCFG_PTYPE;

#endif
