#ifndef __EMV_MSGFMT_H
#define __EMV_MSGFMT_H

#include "emv.h"

/* APIs to build the EMV request message */
extern void getS_13Command( char *, int *);
extern void getS_14Command( char *, int *);
extern void getS_95Command( char *, int *);
extern void getS_20Command( char *, int *, char*, char*, char*);
extern void getS_66Command( char *, int *, char*);
extern void getM_40Command( char *, int *, char*);
extern void getM_41Command( char *, int *, char*, char*);
extern void getC_20Command( char *, int *, char *, int);
extern void getC_19Command( char *, int *, char *, int);
extern int  getC_30Command( char *, int *, char*, char*, char*, char*);
extern int  getU_00Command( char *, int *, char*);
extern int  getU_01Command( char *, int *, AID_LIST_INFO_PTYPE, PAAS_BOOL);
extern int  getC_32Command( char *, int *, char*);
extern int  getC_34Command( char *, int *, char*, int, CARDDTLS_PTYPE);
extern int getC_36Command( char *, int *, char*);
extern void getI_02Command( char *, int *);
extern void getI_05Command( char *, int *);	
extern void getD_32Command( char *, int *);
extern void getD_41Command( char* , int *, PAAS_BOOL, char*, char*);
extern void getE_02Command( char *, int *);
extern void getE_06Command( char *, int *);
extern void getE_10Command( char *, int *);

extern void getD_11Command( char *, int *, EMV_CAPK_INFO_PTYPE);
extern void getD_12Command( char *, int *);
extern void getD_13Command( char *, int *, EMV_CMD_INFO_PTYPE, EMV_EST_REC_PTYPE);
extern int  getD_14Command( char *, int *, EMV_MVT_REC_PTYPE);
extern void getD_15Command( char *, int *, EMV_EST_REC_PTYPE);
extern void getD_16Command( char *, int *, long, int);
extern void getD_17Command( char *, int *, EMV_EEST_REC_PTYPE);
extern void getD_18Command( char *, int *, EMV_OTHER_REC_PTYPE);
extern void getD_19Command( char *, int *, EMV_EMVT_REC_PTYPE);
extern void getD_20Command( char *, int *, EMV_ICCT_REC_PTYPE);
extern void getD_25Command( char *, int *, EMV_OTHER_REC_PTYPE); 

#endif
