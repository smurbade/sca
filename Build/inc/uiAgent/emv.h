#ifndef __EMV_H
#define __EMV_H

#include "emvConstants.h"

#if 0
typedef struct
{	
	int 					iEmvCardType;
    int 					CmdDispatched;
    int 					iTransType;
	unsigned char			ucConfirmAmount;
	int 					iLangSelected;  
	int 					iMerchantDecision; 
	int 					iXPIErrorNo; 
	PAAS_BOOL 				bCardRemoved;
	PAAS_BOOL       		bTransactionCompleted;
    PAAS_BOOL				EmvProcessingStarted;	
	unsigned short 			usInsertedCardType;
    PAAS_BOOL				bResponseReceived;	
	
}EMVINFO_STYPE, * EMVINFO_PTYPE;

#define SIZE_EMVDATA		sizeof(EMVDATA_STYPE)
#endif

//Data Structure List for creating CAPK Files(used for building D11 command)
typedef struct
{
	char 			szPKInd[2+1];
	int				iRIDLen;
	char			szRID[EMV_RID_SIZE+1]; /* 11 */ 
	int 			iPKIndexLen;
	//char *			pszPKIndex;		
	char 			szPKIndex[100];		
	int 			iModLen;
	//char *			pszModulus;	
	char 			szModulus[1024];	
	int 			iExpLen;
	//char *			pszExponent; 
	char 			szExponent[1024]; 
	int 			iCheckSumLen;
	//char *			pszCheckSum; 
	char 			szCheckSum[1024]; 
	char 			szExpDate[6+1];
}
EMV_CAPK_INFO_STYPE, * EMV_CAPK_INFO_PTYPE;
#define SIZE_EMV_CAPK		sizeof(EMV_CAPK_INFO_STYPE)

//Data Structure List for MVT (EMV Configuration Table) for D14 command
typedef struct TagMVT_REC
{
	short	inRecNo;			/* 2 */
	short 	inSchemeReference; /* 2 */
	short 	inIssuerReference; /* 2 */
	short 	inTRMDataPresent; /* 2 */
	short 	inTargetRSPercent; /* 2 */
	long 	lnFloorLimit; /* 4 */
	long 	lnRSThreshold; /* 4 */
	short 	inMaxTargetRSPercent; /* 2 */
	short 	inMerchantForcedOnlineFlag; /* 2 */
	short 	inBlackListedCardSupportFlag; /* 2 */
	short	inFallbackAllowed; /* 2*/
	char 	szTACDefault[EMV_TAC_SIZE+1]; /* 11+1 */
	char	szTACDenial[EMV_TAC_SIZE+1]; /* 11+1 */
	char 	szTACOnline[EMV_TAC_SIZE+1]; /* 11+1 */
	char 	szDefaultTDOL[EMV_MAX_TDOL_SIZE+1]; /* 65+1 */
	char 	szDefaultDDOL[EMV_MAX_DDOL_SIZE+1]; /* 65+1 */
	short	inNextRecord; /* 2 */
	short 	inAutoSelectAppln; /* 2*/
	unsigned long ulEMVCounter; /* 4 */
	char 	szTermCountryCode[EMV_COUNTRY_CODE_SIZE+1]; /* 5+1 */
	char 	szTermCurrencyCode[EMV_CURRENCY_CODE_SIZE+1]; /* 5+1 */
	char 	szTermCapabilities[EMV_TERM_CAPABILITIES_SIZE+1]; /* 7+1 */
	char 	szTermAddCapabilities[EMV_ADD_TERM_CAPABILITIES_SIZE+1]; /* 11+1 */
	char 	szTermType[EMV_TERM_TYPE_SIZE+1]; /* 3+1 */
	char 	szMerchantCategoryCode[EMV_MERCH_CAT_CODE_SIZE+1]; /* 5+1 */
	char 	szTerminalCategoryCode[EMV_TERM_CAT_CODE_SIZE+1]; /* 3+1 */
	char 	szTerminalID[EMV_TERM_ID_SIZE + 1]; /* 8+1 */
	char 	szMerchantID[EMV_MERCH_ID_SIZE + 1]; /* 15+1 */
	char 	szAcquirerID[EMV_ACQU_ID_SIZE + 1]; /* 13+1 */
	char 	szTermCAPKIndex[EMV_CAPK_IND_SIZE + 1]; /* 2+1 */
	char 	szPINBypassFlag[FLAG_SIZE + 1]; /* 1+1 */
	char 	szPINTimeout[TIMEOUT_SIZE + 1]; /* 3+1 */
	char 	szPINFormat[PIN_FORMAT_SIZE + 1]; /* 1+1 */
	char 	szPINScriptNum[SEC_SCRIPT_NUM_SIZE + 1]; /* 1+1 */
	char 	szPINMacroNum[PIN_MACRO_NUM_SIZE + 1]; /* 2+1 */
	char 	szPINDerivKeyFlag[FLAG_SIZE + 1]; /* 1+1 */
	char 	szPINDerivMacroNum[PIN_MACRO_NUM_SIZE + 1]; /* 2+1 */
	char 	szCardStatDispFlag[FLAG_SIZE + 1]; /* 1+1 */
	short 	inTermCurExp; /* 2 */
	short 	inIssAcqflag; /* 2 */
	short 	inNoDisplaySupportFlag; /* 2 */
	short 	inModifyCandListFlag; /* 2 */
	char 	szRFU1[EMV_STRING_SIZE+1]; /*25+1 */
	char 	szRFU2[EMV_STRING_SIZE+1]; /*25+1 */
	char 	szRFU3[EMV_STRING_SIZE+1]; /*25+1 */
	short 	shRFU1; /* 2 */
	short 	shRFU2; /* 2 */
	short 	shRFU3; /* 2 */
}
EMV_MVT_REC_STYPE, * EMV_MVT_REC_PTYPE;
#define SIZE_EMV_MVT_REC		sizeof(EMV_MVT_REC_STYPE)

typedef struct TagEMVT_REC
{
	short	inRecNo;			/* 2 */
	char 	szCTLSGroupName[CTLS_GROUP_NAME_SIZE+1]; /* 20 */
	char 	szCTLSFlrLimit[CTLS_AMT_LIMIT_SIZE+1]; /* 12 */
	char 	szCTLSCVMLimit[CTLS_AMT_LIMIT_SIZE+1]; /* 12 */
	char 	szCTLSTranLimit[CTLS_AMT_LIMIT_SIZE+1]; /* 12 */
	char 	szCTLSTACDefault[CTLS_TAC_SIZE+1]; /* 11 */
	char 	szCTLSTACDenial[CTLS_TAC_SIZE+1]; /* 11 */
	char 	szCTLSTACOnline[CTLS_TAC_SIZE+1]; /* 11 */
	char 	szCTLSVISATTQ[CTLS_VISA_TTQ_SIZE+1]; /* 8 */
	char 	szCTLSTermCapabilities[CTLS_TERM_CAPABILITIES_SIZE+1]; /* 6 */
	char	szCTLSTermAddCapabilities[CTLS_ADD_TERM_CAPABILITIES_SIZE+1]; /* 10 */
	char 	szCTLSCountryCode[CTLS_COUNTRY_CODE_SIZE+1]; /* 3 */
	char	szCTLSCurrencyCode[CTLS_COUNTRY_CODE_SIZE+1]; /* 3 */
	int 	inCTLSMVTIndex; /* 3 */
}
EMV_EMVT_REC_STYPE, * EMV_EMVT_REC_PTYPE;
#define SIZE_EMV_EMVT_REC		sizeof(EMV_EMVT_REC_STYPE)

//Data Structure List for EST (EMV Card Scheme Table) for D13
typedef struct TagEST_REC
{
	short	inRecNo;			/* 2 */
	long 	lnNbrOfTransactions; /* 4 */
	char 	szSchemeLabel[EMV_SCHEME_NAME_SIZE+1]; /* 35+1 */
	char 	szRID[EMV_RID_SIZE+1]; /* 11 */
	char 	szCSNList[EMV_CSN_FILE_NAME_SIZE+1]; /* 32+1 */
	char 	szTermAVN[EMV_AVN_SIZE+1]; /* 5+1 */
	char 	szSecondTermAVN[EMV_AVN_SIZE+1]; /* 5+1 */
	char 	szRcmdAppNameAID[MAX_APPNAME_LEN+1]; /* 17+1 */
	char 	szSupportedAID[15][EMV_MAX_AID_SIZE+1]; /* 32+1 */ // Assuming that max supported AID's is 15
	long 	inPartialNameAllowedFlag; /* 4+1 */
	long	inEMVTableRecord; /* 4+1 */
	char	szCSNListFile[EMV_CSN_FILE_NAME_SIZE+1]; /* 31+1 */
}
EMV_EST_REC_STYPE, * EMV_EST_REC_PTYPE;
#define SIZE_EMV_EST_REC		sizeof(EMV_EST_REC_STYPE)

//Data Structure List for EEST (Extended EMV Card Scheme Table)
typedef struct TagEEST_REC
{
	short	inRecNo;			/* 2 */
	char 	szSupportedAID[EMV_MAX_AID_SIZE+1]; /* 32 */
	short 	inEMVRec; 							/* 2 */
	short 	inMaxAIDLength;		 				/* 2 */
	short 	inApplicationFlow; 					/* 2 */
	short 	inPartialName; 						/* 1 */
	short 	inAcctSelFlag; 						/* 1 */
	short 	inCTLSEnableFlag; 					/* 1 */
	int 	iTransactionScheme; 				/* 3 */
}
EMV_EEST_REC_STYPE, *EMV_EEST_REC_PTYPE;
#define SIZE_EMV_EEST_REC		sizeof(EMV_EEST_REC_STYPE)

//Data Structure List for ICCT (Interac Contactless Configuration Table)
typedef struct TagICCT_REC
{
	short	inRecNo;			/* 2 */
	char 	szCTLSMerchantType [2+1];
	char 	szCTLSTermTranInfo [6+1];
	char 	szCTLSTermTranType [2+1];
	char 	szCTLSReqReceiptLimit [12+1];
	char 	szCTLSOptionStatus [4+1];
	char 	szCTLSReaderFloorLimit [12+1];
}
EMV_ICCT_REC_STYPE, *EMV_ICCT_REC_PTYPE;
#define SIZE_EMV_ICCT_REC		sizeof(EMV_ICCT_REC_STYPE)

//Data Structure List for Other EMV paramter values
typedef struct TagOther_REC
{
	char 	szBlwFlrLimitTermCap [6+1];
	char 	szAbvFlrLimitTermCap [6+1];
	char 	szCTLSCVMLimit[CTLS_AMT_LIMIT_SIZE+1]; /* 12 */	
	short	inEMVContactless;
	short	inVisaDebit;
	short 	inPDOLSupFlg;
	short	inValueLinkCrdSup;
	short	inISISNFCSup;
	short 	inAMEXCTLSSup;
	short	inMobileSupport;
	short	inUSDebitFlag;
}
EMV_OTHER_REC_STYPE, *EMV_OTHER_REC_PTYPE;

#define SIZE_EMV_OTHER_REC		sizeof(EMV_OTHER_REC_STYPE)

/* Typical nodes for key load command response list from host*/
typedef struct __emvKeyLoadNode
{
	EMVKEYLOAD_TYPE_ENUM			emvKeyLoadType;
	void *							emvData;

	struct __emvKeyLoadNode *		nextEmvNode;
}
EMVKEYLOAD_NODE_STYPE, * EMVKEYLOAD_NODE_PTYPE;

#if 0
typedef struct __estRec
{
	char 						szSupportedAID[EMV_MAX_AID_SIZE+1]; /* 32 */	
	EMV_EST_REC_STYPE   		stEstRecInfo;		//EST Table
	EMV_EEST_REC_STYPE  		stEestRecInfo;		//EEST Table
	
	struct __estRec * nxtSuppAIDNode;
}
EMV_SUPP_AID_INFO_STYPE, * EMV_SUPP_AID_INFO_PTYPE;
#endif
/* This structure is used to build 'D' commands */
typedef struct __emvKeyLoadLst
{
	char 						szAppID[10+1];		//Application ID
	int							iNumCAPKFiles;		// Max value is 15.
	int							iNumSuppAid;		//Max value is 15
	//Max 15 CAPK files stored for each APPID.
	EMV_CAPK_INFO_STYPE			stCAPKInfo[15];		//D11 cmd;
	EMV_MVT_REC_STYPE   		stMvtRecInfo;		//MVT Table
	EMV_EMVT_REC_STYPE  		stEmvtRecInfo;		//EMVT Table
	EMV_EST_REC_STYPE   		stEstRecInfo;		//EST Table
	EMV_EEST_REC_STYPE  		stEestRecInfo[15];	//EEST Table
	struct __emvKeyLoadLst *	nextEmvNode;
}
EMVKEYLOAD_LIST_STYPE, * EMVKEYLOAD_LIST_PTYPE;

/*This stucture is used to store emv paramters(Fallback value, offline floor limit, public keys)
  received from host for EMV download request*/
typedef struct
{
	char 						szKeyLoadStatus[1+1];
	char 						szReference[40+1]; //As per SSI guide , max len is 40
	char 						szTROUTD[10+1];	

	/* For the main emv key load list */
	EMVKEYLOAD_NODE_PTYPE		mainLstHead;
	EMVKEYLOAD_NODE_PTYPE		mainLstTail;
}
EMVKEYLOAD_INFO_STYPE, * EMVKEYLOAD_INFO_PTYPE;

typedef struct
{
	EMV_ICCT_REC_STYPE			stIcctRecInfo;		//ICCT Table
	EMV_OTHER_REC_STYPE			stOtherRecInfo;		//OTHER
	
	/* For the main emv key load list node */
	EMVKEYLOAD_LIST_PTYPE	mainKeyLoadLstHead;
	EMVKEYLOAD_LIST_PTYPE	mainKeyLoadLstTail;
}
EMVKEYLOAD_LST_INFO_STYPE, * EMVKEYLOAD_LST_INFO_PTYPE;

typedef struct
{
	int			iNumRec;
	long		lnTableNo;
	int 		iRecNo;
	short		inAIDNo;
	int			iCurCmd;
	int			iExecCount;
	int 		iNumSuppAid;
	int 		iNumCAPKFiles;
}
EMV_CMD_INFO_STYPE, * EMV_CMD_INFO_PTYPE;

typedef struct 
{
	char		szFallBackInd[2+1];			/* FALLBACK_IND (2 + 1) */
	char		szAppID[10+1];				/* APP_ID (10 + 1) */
	char		szFallBackValue[1+1];		/* FALLBACK_VALUE (1 + 1) */	
}
FALLBACK_STYPE, * FALLBACK_PTYPE;	

typedef struct 
{
	char		szOffFlrLmtInd[2+1];		/* OFFLINE_FLOOR_LIMIT_IND (2 + 1) */
	char		szAppID[10+1];				/* APP_ID (10 + 1) */
	char		szOffFlrLmt[9+1];			/* OFFLINE_FLOOR_LIMIT (9 + 1) */
	char		szOffFlrLmtThrshld[9+1];	/* FLOOR_LIMIT_THRESHOLD (9 + 1) */	
}
OFFLINE_FLR_LIMIT_STYPE, * OFFLINE_FLR_LIMIT_PTYPE;

typedef enum
{
	APPLN,
	KEYS,
	TERMINAL
}
EMV_INIT_ENUM;
#endif
