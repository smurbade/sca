
static KEYVAL_STYPE	visaDrlLst[] =
{
	{ "Index"							, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "Floorlimit"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "AppPrgIdLen"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "Application_PRG_ID"				, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "TXNlimit"						, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "FeatureSwitch"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	}
};

static VARLST_INFO_STYPE visaDrlLstInfo[] =
{
	{ 0			, NULL, sizeof(visaDrlLst) / KEYVAL_SIZE, visaDrlLst},
	{ -1        , NULL, 0 							 	, NULL }
};


static KEYVAL_STYPE	appList[] =
{
	{ "AID"								, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "VerNum"							, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "CL_Modes"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "AppName"							, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "ASI"								, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "CountryCodeTerm"					, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "BrKey"							, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "TermIdent"						, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "CL_CVM_Soft_Limit"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "CL_Ceiling_Limit"				, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "FloorLimit"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	}, 
	{ "SecurityLimit"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	},	
	{ "BelowLimitTerminalCapabilities"	, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "Threshold"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "TargetPercentage"				, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "MaxTargetPercentage"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	},	
	{ "Chip_VerNumber"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "MSR_VerNumber"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "AdditionalVersioNumbers"			, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "AID_Prio"						, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "SpecialTRX"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "AppFlowCap"						, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "AdditionalTagsTRM"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "AdditionalTagsCRD"				, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "AppTermCap"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	}, 
	{ "AppTermAddCap"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	},	
	{ "AppTerminalType"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "TAC_Denial"						, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "TAC_Online"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	}, 
	{ "TAC_Default"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	},	
	{ "EMV_Application"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "DefaultTDOL"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "DefaultDDOL"						, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "MerchIdent"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "CDA_Processing"					, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "AC_BeforeAfter"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "AIP_CVM_NotSupported"			, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "POS_EntryMode"					, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "AdditionalVersionNumbers"		, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "AppFlowCap"						, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "AdditionalTagsTRM"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	}, 
	{ "AdditionalTagsCRD"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	},	
	{ "MandatoryTaglistCRD"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "CountryCodeTerm"					, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "AppTermAddCap"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "AppTerminalType"					, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "AID_Prio"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	}, 
	{ "FallbackMIDs"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	},	
	{ "SpecialTRX"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "FallbackHandling"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "CustomerCVM"						, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "PhoneMessageTable"				, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "MagstripeCVM_aboveLimit"			, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "MagstripeCVM_belowLimit"			, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "ChipCVM_belowLimit"				, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "CL_Ceiling_LimitMobile"			, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "CL_Ceiling_LimitMobile"			, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "Torn_TXN_Liftime"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	}, 
	{ "Torn_TXN_Number"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	},	
	{ "Txn_Category_Code"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "MerchantName_Location"			, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "VisaTTQ"							, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "VisaDRLParams"					, NULL, PAAS_FALSE , VARLIST	, visaDrlLstInfo},
	{ "MTI_Merchant_Type_Ind"			, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "TTI_Term_Trans_Info"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	}, 
	{ "TTT_Term_Trans_Type"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	},	
	{ "TOS_Term_Option_Status"			, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "RDR_CTLS_FloorLimit"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "TERM_RCPT_REQLimit"				, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "AMEX_Term_Caps"					, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "ChksumParams"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "ChksumASCII_EMVCO"				, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "Chksum_EntryPoint"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "Chksum_Kernel"					, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "MasterAID"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	}
};

static VARLST_INFO_STYPE appLstInfo[] =
{
	{ 0			, "Application", sizeof(appList) / KEYVAL_SIZE, appList},
	{ -1        , NULL         , 0 , NULL }
};

/* ------- LINE ITEM -- GENERIC ADD ------- */
static KEYVAL_STYPE	appInitReqLst[] =
{
	{ "ApplicationData"		, NULL, PAAS_TRUE,  VARLIST, appLstInfo }
};


static KEYVAL_STYPE	pubkeyList[] =
{
	{ "Index"						, NULL, PAAS_FALSE, SINGLETON , NULL 	},
	{ "RID"							, NULL, PAAS_FALSE, SINGLETON , NULL 	},
	{ "Key"							, NULL, PAAS_FALSE, SINGLETON , NULL 	},
	{ "KeyLen"						, NULL, PAAS_FALSE, SINGLETON , NULL 	},
	{ "Exponent"					, NULL, PAAS_FALSE, SINGLETON , NULL	},
	{ "ExpiryDate"					, NULL, PAAS_FALSE, SINGLETON , NULL 	},
	{ "Hash"						, NULL, PAAS_FALSE, SINGLETON , NULL 	}
};

static VARLST_INFO_STYPE pubkeyLstInfo[] =
{
	{ 0			, "CapKey", sizeof(pubkeyList) / KEYVAL_SIZE, pubkeyList},
	{ -1        , NULL    , 0 , NULL }
};

/* ------- LINE ITEM -- GENERIC ADD ------- */
static KEYVAL_STYPE	pubkeyInitReqLst[] =
{
	{ "CapKeys"		, NULL, PAAS_TRUE,  VARLIST, pubkeyLstInfo }
};

static KEYVAL_STYPE	terminalList[] =
{
	{ "CL_Modes_Supported"			, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "TermTyp"						, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "CountryCodeTerm"				, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "CurrencyTrans"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "ExpTrans"					, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "SuppLang"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	}, 
	{ "IFD_SerialNumber"			, NULL, PAAS_FALSE , SINGLETON  , NULL 	},	
	{ "FlowOptions"					, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "MaxCTLSTranslimit"			, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "BeepVolume"					, NULL, PAAS_FALSE , SINGLETON  , NULL	},
	{ "KernelVersion"				, NULL, PAAS_FALSE , SINGLETON  , NULL 	},
	{ "FrameworkVersion"			, NULL, PAAS_FALSE , SINGLETON  , NULL 	}
};
