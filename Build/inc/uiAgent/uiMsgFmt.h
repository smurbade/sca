#ifndef __UI_MSGFMT_H
#define __UI_MSGFMT_H

#include "uiGeneral.h"

typedef enum
{
	INIT_REQ = 0,
	FAVER_REQ,
	RESET_REQ,
	BATCH_REQ,
	SETCFGREQ,
	GETCFGREQ,
	XLCS_REQ,
	XCLS_REQ,
	S002_REQ,
	INIT_EMV_REQ,
	INIT_EMV_CONFIG_REQ,
	XPI_RESET_REQ,
	XSWT_REQ,
	XMBX_REQ,
	XMBH_REQ,
	RSTART_REQ,
	XCLB_REQ,
	DEVICE_REG,
	ADVANCE_DDK
}
REQ_MSG_TYPE;

typedef struct
{
	int			iSize;
	int			iMsgLen;
	char		pszBuf[4096];
}
UIREQ_STYPE, * UIREQ_PTYPE;

/* APIs to build the UI request message */
extern void initUIReqMsg(UIREQ_PTYPE, REQ_MSG_TYPE);
extern void initFormWrapper(const char *, int , char *);
extern void showFormWrapper(int, char *);
extern int	setTimeWrapper(char *, char*);
extern void setStringValueWrapper(int, char, char *, char *);
extern void setBoolValueWrapper(int, char, int, char *);
extern void setShortValueWrapper(int, char, short, char *);
extern void getShortValueWrapper ( int, char, char*);
extern void getBoolValueWrapper(int , char , char* );
extern int 	getConfigVarWrapper(char**, char**, unsigned);
extern void setLongValueWrapper(int, char, char *, char *);
extern void setCurVolWrapper(int, int, int, char *);
extern void getCurVolWrapper(char *);
extern void xvclCmdWrapper(PAAS_BOOL, char *, REQ_MSG_TYPE);
extern void addTextboxTextWrapper(short, char *, char *);
extern void clearTextboxTextWrapper(short, char *);
extern void replaceTextboxTextWrapper(short , char * , char * );
extern void addListboxItemWrapper(int, int, char *, int, char *);
extern void removeListboxItemWrapper(int, int, char *);
extern void updateListboxItemWrapper(int , int , char * , char * );
extern void setListBoxRestoreState(int , char *);
extern void clearListBoxRestoreState(char *);
extern void clearListBoxfromCurrentForm(int, char *);
extern void setCfgVarWrapper(char *, char *, char *);
extern void setSigCapBoxArea(int, int, int, int, int, char *);
extern void setSigCapParams(int, int, int, int, int, int, char *);
extern void getSignature();
extern void formMkPinRequest(char *, char *);
extern void formDukptPinRequest(char *, char *, char *);
extern void getCardDataUtil(PAAS_BOOL, PAAS_BOOL, PAAS_BOOL, char *);
extern void getManualCardDataUtil(int, char *, char *, char *, char *);
extern void addQRCodeInfoWrapper(char * , int , int , int , char *);
#endif
