#ifndef __UI_CFGDATA
#define __UI_CFGDATA

#define SECTION_UI				"ui"	
#define SECTION_SIGN			"SCT"
#define SECTION_MEDIA			"MCT"
#define SECTION_DEVICE			"DCT"
#define SECTION_PAYMENT			"PDT"
#define SECTION_XPI_IAB			"IAB"

#define XPI_INTERNAL_TAP_FLAG					"internal_tap_flag"
#define EMV_ENABLED								"emvenabled"
#define CTLS_EMV_ENABLED						"contactlessEmvEnabled"
#define CTLS_ENABLED							"contactlessEnabled"
#define	EMV_KERNEL_SWITCHING_ALLOWED			"emvkernelswitchallowed"
#define	CARD_READ_SCREEN_FIRST					"cardreadscreenfirst"
#define	SUPPORTED_WALLETS						"supportedwallets"
#define SERVICE_CODE_CHECK_IN_FALLBACK			"servicecodechkforfallback"
#define	STORE_CARD_DTLS_POST_AUTH				"storecarddtlsforpostauth"
#define	STORE_PREV_TRANS_CARD_DTLS_GIFT_CLOSE	"storecarddtlsforgiftclose"
#define	EMPTY_CANDID_AS_FALLBACK				"emptycandidasfallback"
#define	XPI_LIBRARY								"xpilib"
#define VERBOSERECPT_ENABLED 					"verboseRecptEnabled"
#define DESCRIPTIVE_ENTRY_MODE					"descriptiveentrymode"
#define SIGCAP_ENABLED							"sigcapture"

/* Config variables under the UI section */
#define	DFLT_LANG			 	"defaultlang"
#define SIGN_BEG_X			 	"begx"
#define SIGN_BEG_Y			 	"begy"
#define SIGN_END_X			 	"endx"
#define SIGN_END_Y			 	"endy"
#define	SIGN_AMT_FLOOR		 	"sigamount"
#define SIGN_IMAGE_TYPE			"sigimagetype"
#define IDLE_IMG_LIST_FILE		"idlescrimglistfile"
#define LI_IMG_LIST_FILE		"liscrimglistfile"
#define VIDEO_LIST_FILE		 	"videolistfile"
#define IDLE_SCREEN_MODE_FILE	"idlescreenmodefile"
#define IP_DISP_INTRVL		 	"ipdispinterval"
#define	ADVANCE_VSP_IN_X_DAYS	"ADVANCEVSPKEYINXDAYS"
#define SYS_INFO_DISP_INTRVL 	"sysinfointerval"
#define DEVNAME_DISP_INTRVL 	"devnamedispinterval"
#define PINPAD_MODE			 	"mk"
#define STATUS_MSG_DISP_INTRVL	"statusmsgdispinterval"
#define	EMV_SUPP_LANGS		 	"emv_supported_lang"
#define LANE_CLOSED_FONT_COL 	"laneclosedfontcol"
#define LANE_CLOSED_FONT_SIZE 	"laneclosedfontsize"
#define PROMPTS_FILE_NAME	 	"./flash/displayPrompts.ini"
#define ANAIMATION_UPDATE_INTV	"animationupdIntv"
//#define CFG_INI_FILE_PATH	 	"./flash/emvConfig.ini"
#define QRCODE_IMG_FILE_NAME		"qrcode.png"
#define PREAMBLE_SELECT_WAIT_TIME	"preambleselectwaittime"

#define DFLT_FA_PORT		 9001
#define DFLT_ANIM_UPDT_INTV	 10

#endif
