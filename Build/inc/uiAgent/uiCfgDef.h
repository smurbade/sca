#ifndef __UI_CFG_DEF_H
#define __UI_CFG_DEF_H

#include "uiGeneral.h"
#include "uiConstants.h"
#include "emv.h"
#include "iniparser.h"

/* Would be used in selecting the forms according to the platform */
#define MODEL_MX915	915
#define MODEL_MX925	925

//#define CFG_INI_FILE_PATH	 	"./flash/emvConfig.ini"
#define AID_LIST_FILE_TXT		"./flash/AIDList.txt"
#define AID_LIST_FILE_INI		"./flash/AIDList.ini"
#define AID_LIST_FILE_INI_CAPS	"./flash/AIDList.INI"
#define EMV_TAGS_REQ_BY_HOST	"./flash/EmvTagsReqbyHost.txt"
//#define D25_COMMAND_FILE_NAME	"./flash/D25.txt"
#define CTLS_INI_FILE_PATH		"./flash/CTLSConfig.ini"
#define XPI_EMV_TABLES_PERM_PATH "/mnt/flash/userdata/share/EMVTables.ini"
#define XPI_EMV_TABLES_TEMP_PATH "./flash/XPIEMVTables.ini"
#define XPI_CTLS_CONFIG_PERM_PATH "/mnt/flash/userdata/share/CTLSConfig.ini"
#define XPI_CTLS_CONFIG_TEMP_PATH "./flash/XPICTLSConfig.ini"
#define EMV_TABLES_FILE_PATH	"./flash/EMVTables.ini"
#define CAPK_DATA_INI_FILE_PATH	"./flash/CAPKData.INI"
#define OPT_FLAG_FILE_PATH		"./flash/OptFlag.INI"
#define PAID_FILE_NAME			"/mnt/flash/userdata/share/PreferredAIDs.INI"
#define NFC_INI_FILE_NAME		"./flash/nfc.ini"

#define TEMP_CTLS_INI_FILE_PATH			"./flash/CTLSConfig_temp.ini"
#define TEMP_EMV_TABLES_FILE_PATH		"./flash/EMVTables_temp.ini"
#define TEMP_CAPK_DATA_INI_FILE_PATH	"./flash/CAPKData_temp.INI"
#define TEMP_OPT_FLAG_FILE_PATH			"./flash/OptFlag_temp.INI"


#define MEDIA_DIR				"./flash/ADVT_MEDIA/"
#define MAX_VIDEO_COUNT			10
#define MAX_IMAGE_COUNT			50
#define MAX_DISP_PROMPT			52
#define MAX_DISP_MSG_SIZE		100
#define MAX_DISP_TITLE_SIZE		100
#define DFLT_IMG_UPD_TIME		5	/* 5 seconds */
#define MAX_IMG_UPD_TIME		120	/* 120 seconds */

#define MAX_SUPPORT_LANG		5

#define DLFT_915_SIG_BEGX		3
#define DLFT_915_SIG_BEGY		3
#define DLFT_915_SIG_ENDX		205
#define DLFT_915_SIG_ENDY		205

#define DLFT_925_SIG_BEGX		10
#define DLFT_925_SIG_BEGY		10
#define DLFT_925_SIG_ENDX		410
#define DLFT_925_SIG_ENDY		410

#define DLFT_915_SIG_EX_BEGX	2
#define DLFT_915_SIG_EX_BEGY	127
#define DLFT_915_SIG_EX_ENDX	427
#define DLFT_915_SIG_EX_ENDY	270

#define DLFT_925_SIG_EX_BEGX	010
#define DLFT_925_SIG_EX_BEGY	278
#define DLFT_925_SIG_EX_ENDX	650
#define DLFT_925_SIG_EX_ENDY	480

#define DFLT_IP_SHOW_TIME	 	30	/* 30 seconds */
#define DFLT_SYSINFO_DISP_TIME	30	/* 30 seconds */
#define	DFLT_DEVNAME_DISP_TIME	30	/* 30 seconds */
#define MAX_IP_SHOW_TIME		120	/* 120 seconds */
#define MAX_SYSINFO_DISP_TIME	120	/* 120 seconds */
#define MAX_DEVNAME_DISP_TIME	120 /* 120 seconds */

#define DFLT_STATUS_DISP_TIME	1000 /*1000 MILLI SECONDS*/
#define MAX_STATUS_DISP_TIME	3000 /*3000 MILLI SECONDS*/
#define MIN_STATUS_DISP_TIME	500  /*500 MILLI SECONDS*/

#define DFLT_ADVANCE_VSP_DAYS	360
#define MAX_ADVANCE_VSP_DAYS	365
#define MIN_ADVANCE_VSP_DAYS	180

#define DFLT_MCNTSETTINGS_LEN	10
#define INV_MCNTSETTINGS_LEN	11

#define SIGN_IMAGE_TYPE_TIFF	"TIFF"
#define SIGN_IMAGE_TYPE_BMP		"BMP"
#define SIGN_IMAGE_TYPE_3BA		"3BA"

#define DISPLAY_MSG_CANCELLED				1
#define DISPLAY_MSG_TRAN_DECLINED			2
#define DISPLAY_MSG_ERROR					3
#define DISPLAY_MSG_TRAN_REVERSED			4
#define DISPLAY_MSG_TRAN_TIMEOUT			5
#define DISPLAY_MSG_TRAN_APPROVED			6
#define DISPLAY_MSG_CAPTURED				7
#define DISPLAY_MSG_CARD_NOTACCEPTED		8
#define DISPLAY_MSG_CARD_INVALID			9
#define DISPLAY_MSG_MANENTRY_NTALLWD		10
#define DISPLAY_MSG_CMD_NOT_SUP_PYMNT_TYPE	11
#define DISPLAY_MSG_VOIDED					12
#define DISPLAY_MSG_HOST_OFFLINE			13
#define DISPLAY_MSG_COMPLETED				14
#define DISPLAY_MSG_TRAN_APPROVED_BAL		15
#define DISPLAY_MSG_CAPTURED_BAL			16
#define DISPLAY_MSG_UNKNOWN					17
#define DISPLAY_MSG_DECLINED_INVALID_PIN	18
#define DISPLAY_MSG_DCC_APPROVAL			19
#define DISPLAY_MSG_TRAN_VERIFIED			20


#define DEFAULT_IDLE	0
#define FULL_IDLE_IMG	1
#define FULL_IDLE_VID	2
#define FULL_IDLE_ANIM	3

#define	DFLT_LANE_CLOSED_FONT_COL		"FF0000"	/* Red Color */
#define	DFLT_925_LANE_CLOSED_FONT_SIZE	32			/* Default Size for 925 */
#define	DFLT_915_LANE_CLOSED_FONT_SIZE	28			/* Default Size for 915 */


#define DISPLAY_ERROR_ICON '1'
#define DISPLAY_NO_BUTTONS '0'

typedef enum
{
	B_ENDIAN = 0,
	S_ENDIAN
}
ENDIAN_ENUM;

typedef struct
{
	uchar				szLblOne[33];
	uchar				szLblTwo[33];
}
BTNLBL_STYPE, * BTNLBL_PTYPE;

/* Signature Capture Settings */
typedef struct
{
	double				signAmt;
	int					startX;
	int					startY;
	int					endX;
	int					endY;
	char				szImageType[10];
}
SIG_STRUCT_TYPE, * SIG_PTR_TYPE;

/* Information needed for communication with UI agent */
typedef struct
{
	char				agentIP[16];
	uint				agentPort;
}
UI_COMPARAM_STYPE, * UI_COMPARAM_PTYPE;

typedef enum
{
	IMAGE,
	VIDEO,
	VOLUME
}
MEDIA_TYPE;

/* Image Update Settings */
typedef struct
{
	MEDIA_TYPE			advtMediaType;
	int					mediaUpdIntvl;
	char **				mediaList;
}
MEDIA_UPD_STYPE, * MEDIA_UPD_PTYPE;

/* Media Update Settings */
typedef struct
{
	MEDIA_UPD_STYPE		stIdleScrImgSet;
	MEDIA_UPD_STYPE		stLIScrImgSet;
	MEDIA_UPD_STYPE		stVideoSet;
	int					IdleScreenMode;
	int					IdleAnimUpdIntvl;
}
MEDIA_SET_STYPE, * MEDIA_SET_PTYPE;

typedef struct __prmpt
{
	/* amount format */
	uchar				amtChar;
	uchar				decChar;
	uchar				sepChar;
	int					iLangId;
	BTNLBL_PTYPE		btnLbls[MAX_BTNLBL_IDX];
	uchar *				dispMsgs[MAX_DISP_MSGS];
	uchar *				scrTitles[MAX_TITLE_IDX];
}
PROMPT_STRUCT_TYPE, * PROMPT_PTR_TYPE;

/*This stucture is used to store all display messages, prompts read from .ini file*/
typedef struct
{
	//Which holds the default application language id.
	int					iDefltLangId;	
	
	//Ptrs will hold the PROMPT_PTR_TYPE with IDs as index
	PROMPT_PTR_TYPE		index[MAX_SUPPORT_LANG];

}
DISPMSG_INFO_STYPE, * DISPMSG_INFO_PTYPE;

typedef struct
{
	int							FAPort;
	int							devModel;
	int							ipdispinterval;
	int 						sysinfointerval;
	int							statusMsgDispInterval;
	int							devnamedispinterval;
	int 						pinpadmode;
	int 						advancevspdays;
	int 						iAppIDCnt;			//Added for EMV application count.
	int							iPreambleSelectWaitTime;
	char						szLangType[25];
	char						szEmvVersion[50];
	char						szEMVKernelVer[11];
	char						szEMVCTLSVer[30];
	char						szEmvTagsReqbyHost[512];
	char						szLaneClosedFontColor[10];
	char						iLaneClosedFontSize;
//	char						szD25EMVCtls[30];
//	char						szD25MSDCtls[30];
	PAAS_BOOL					bEmvEnabled;
	PAAS_BOOL					bCTLSEmvEnabled;
	PAAS_BOOL					bCTLSEnabled;
	PAAS_BOOL					bEmvKernelSwitching;
	PAAS_BOOL					bApplePayWalletSupport;
	PAAS_BOOL					bGooglePayWalletSupport;
	PAAS_BOOL					bSamsungPayWalletSupport;
	PAAS_BOOL					bCardBasedTenderSel;
	PAAS_BOOL					bServiceCodeCheckInFallback;
	PAAS_BOOL					bEmptyCandidAsFallBack;
	PAAS_BOOL					bEmvUpdateReq;
	PAAS_BOOL					bEmvCustomTagReqd;
	PAAS_BOOL					bTagsReqdCardPresentVoid;
	PAAS_BOOL					bPartialEmvAllowed;
	PAAS_BOOL					bClearExpToEmvHost;
	PAAS_BOOL					sigCapEnabled;
	PAAS_BOOL					verboseRecptEnabled;
	PAAS_BOOL					bDescriptiveEntryMdoe;
	PAAS_BOOL					privLabelCvvReqd;
	PAAS_BOOL					privLabelExpReqd;
	PAAS_BOOL					giftcardExpReqd;
	PAAS_BOOL					giftcardCvvReqd;
	PAAS_BOOL					giftcardPINCodeReqd;	// MukeshS3: 10-Feb-16
	PAAS_BOOL					ebtcardExpReqd;
	PAAS_BOOL					ebtcardCvvReqd;
	PAAS_BOOL					sigOptOnPINEntryEnabled;
	PAAS_BOOL					bStorePrevTransCardDtls;
	PAAS_BOOL					bStorePrevTransCardDtlsForGiftClose;
	PAAS_BOOL					bXPILibEnabled;
	SIG_PTR_TYPE				signCfgPtr;
	MEDIA_SET_STYPE				stMediaSettings;
	DISPMSG_INFO_PTYPE			pstDispMsgsInfo;
	EMVKEYLOAD_LST_INFO_PTYPE	pstKeyLoadLstInfo;
}
UI_CFG_STYPE, * UI_CFG_PTYPE;


/* extern functions declarations */
extern int	 getIdleScrnMode();
extern int	 loadUIConfigParams();
extern int	 loadUIData(char *);
extern int	 getDevicePlatform();
extern int	 getNxtVideoFileName(char *);
extern int	 getNxtImageFileName(char *, UI_FORM_ENUM);
extern int	 getUIAgentComParam(UI_COMPARAM_PTYPE);
extern int	 getImageUpdateIntvl(UI_FORM_ENUM);
extern int   getGCManualEntryParam();
extern int   getMsgDispIntvl(UI_FORM_ENUM );
extern int   getDisplayMsg(char *, int);
extern int	 fetchScrTitle(char *, int);
extern int	 fetchBtnLabel(BTNLBL_PTYPE, int);
extern int	 getStatusMsgDispInterval();
extern int	 getAdvanceVSPDays();
extern char* getDefaultLang();
extern void  getSigCapCoordinates(int *, int *, int *, int *);
extern void  getSigImageType(char *);
extern int 	 updateEmvKeyLoadValues(EMVKEYLOAD_NODE_PTYPE);
extern int getPreambleSelectWaitTime();
extern EMVKEYLOAD_LST_INFO_PTYPE getKeyLoadListInfoNode(int *);
extern PAAS_BOOL getXPILibMode();
extern PAAS_BOOL isEmvEnabledInDevice();
extern PAAS_BOOL isContactlessEmvEnabledInDevice();
extern PAAS_BOOL isContactlessEnabledInDevice();
extern PAAS_BOOL isCardBasedTenderSelEnabled();
extern PAAS_BOOL isEmvKernelSwitchingAllowed();
extern PAAS_BOOL isServiceCodeCheckInFallbackEnabled();
extern PAAS_BOOL isEmptyCandidateCardAsFallBack();
extern PAAS_BOOL isStoreCardDtlsForPostAuthEnabled();
extern PAAS_BOOL isStoreCardDtlsForGiftCloseEnabled();
extern int getInternalTapFlagfromXPI();
extern PAAS_BOOL isEmvCustomTagsAllowed();
extern PAAS_BOOL isTagsReqInCardPresentVoid();
extern PAAS_BOOL isPartialEmvAllowed();
extern PAAS_BOOL isClearExpiryToEmvHost();
extern PAAS_BOOL isApplePayWalletEnabled();
extern PAAS_BOOL isGooglePayWalletEnabled();
extern PAAS_BOOL isSamsungPayWalletEnabled();
extern PAAS_BOOL isWalletEnabled();
extern PAAS_BOOL isVerboseRecptEnabled();
extern PAAS_BOOL isDescriptiveEntryModeEnabled();
extern PAAS_BOOL isCvvReqdForPrivLabel();
extern PAAS_BOOL isExpReqdForPrivLabel();
extern PAAS_BOOL isCvvReqdForGiftCard();
extern PAAS_BOOL isExpReqdForGiftCard();
extern PAAS_BOOL isPINCodeReqdForGiftCard();
extern PAAS_BOOL isSigOptionReqOnPINEntryScreen();
extern void setLangId(int);
extern int getCurLangId();
//extern int updateD25CmdFromINI();
//extern int updateTACFromINItoAIDList(char *);
extern int updateEMViniFiles(EMVKEYLOAD_INFO_PTYPE, char*);
extern int updateFallbackFromHost(FALLBACK_PTYPE, dictionary *);
extern int updateOfflineFloorLimit(OFFLINE_FLR_LIMIT_PTYPE, dictionary *);
extern int updateCAPKInfo(EMV_CAPK_INFO_PTYPE, dictionary *, char *);
extern void getD25CmdReq(char *, int *, EMV_OTHER_REC_PTYPE);
extern int checkAndCorrectFileFormat(char *);
extern int setSIGBOXArea();
extern char* getLaneClosedFontCol();
extern int 	getLaneClosedFontSize();
extern void setLaneClosedFontCol(char *);
extern void setLaneClosedFontSize(int);
//extern int storeD25Commands(char *, char *);
//extern char* getD25Commands(PAAS_BOOL);

extern void loadAppConfigDataFromXMLFile(char*);
extern void loadPubkeyDataFromXMLFile(char*);
extern void loadTermDataFromXMLFile(char*);
extern PAAS_BOOL isCvvReqdForEBTCard();
extern PAAS_BOOL isExpReqdForEBTCard();
#endif
