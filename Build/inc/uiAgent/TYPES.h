#ifndef TYPES_H
#define TYPES_H

typedef signed char         CHAR;
typedef unsigned char       UCHAR;
typedef unsigned char       UBYTE;
typedef signed short int    SHORT;
typedef unsigned short int  USHORT;
typedef unsigned short int  WORD;
typedef char                BYTE;
typedef unsigned char       BOOL;
typedef unsigned long int   DWORD;
typedef signed long int     LONG;
typedef unsigned long int   ULONG;

#ifndef PACKED
#define PACKED packed
#endif

#endif /* TYPES_H */

