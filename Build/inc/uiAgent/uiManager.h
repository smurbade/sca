
typedef struct system_information
{
	char szFAVersion[20];
	char szPaaSVersion[20];
	char szOSVersion[50];
	char szRFIDVersion[50];
	char szPackages[100];
}SYSTEM_INFO;
