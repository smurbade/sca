#ifndef __EMV_APIS_H
#define __EMV_APIS_H

#include "uiAgent/uiControl.h"
#include "uiAgent/emv.h"
#include "common/tranDef.h"
#include "uiAgent/emvConstants.h"
#include "db/cdtExtern.h"
#include "bLogic/bLogicCfgDef.h"

/* APIs to control draw user interface and get user input*/
extern int initEmvIFace();
extern int sendS20CmdToGetMSRCardData( char *, char*, char*);
extern int sendC30CmdToGetChipCardData(char*,  char *,char*, char*);
extern int sendD41cmdToEMVAgent(PAAS_BOOL, char*, char*);
extern int showAccntTypeOptScrn(char *, int *);
extern int getMACValue(char *, char *, char *);
//extern int applyScrptDataToEmvCard(char *);
extern int completeEmvOnlineTran(char *, CARDDTLS_PTYPE, int);
extern int showBlankScreen(char *);
extern int getEMVCardData(EMVDTLS_PTYPE, CARDDTLS_PTYPE, char **, char *, char*);
extern int setEncryptionModeForEmv(ENC_TYPE);
//extern int getEParamsData(char *);
//extern int getPKICipheredData(char **);
//extern int storeRsaEncDetails(char **, CARDDTLS_PTYPE);
extern int checkCrdPrsnceAndShwMsgToRemove();
extern int procEmvDwnldInit(EMVKEYLOAD_INFO_PTYPE, char*);
extern int processEmvTranReq(char *, CARDDTLS_PTYPE);
extern int processEmvD25Cmd(EMV_OTHER_REC_PTYPE);
extern int updateEMVtaginXPI(ENUM_EMV_CMDS, char*);
extern int getEMVDtlsfromAID(CARDDTLS_PTYPE, int *);
extern char* getEMVVersion();
extern int getHexTotal(char *, char *);
extern char* getEMVKernelVersion();
extern char* getEMVCTLSVersion();
extern void setEMVVersion(char*, char*, char*);
extern void setEmvInitUpdateFlag(PAAS_BOOL);
extern void setEMVTagsReqByHost(char* );
extern PAAS_BOOL getEmvInitUpdateFlag();
extern char* getEMVTagsReqByHost();
extern PAAS_BOOL isCashBackAllowedForEMVCard(CARDDTLS_PTYPE,char *);
extern int sendD01CmdToEMVAgent(int);
extern int getReqdEMVTagsFromXPI(CARDDTLS_PTYPE,char *,char *);
//extern PAAS_BOOL isCardInserted();
//extern int updateTranWithEmvSAFDetails(char *);
extern int getUIRespCodeOfEmvCmdResp(ENUM_EMV_CMDS, int );
extern void storeEmvCardData(EMVDTLS_PTYPE, CARDDTLS_PTYPE);
extern void fillEMVData(EMVDTLS_PTYPE , CARDDTLS_PTYPE );
extern int loadD25Commands();
extern int loadAIDListAndConfig(char * );
extern int loadEmvTagsReqForHost();
extern int loadPreferredDtlsFromPAID();
extern int createHashForEmvCTLSTags();
extern int getMerchantIdfromNfcFile(char*, int *, NFC_APPLE_SECT_DTLS_PTYPE);
extern void renametempEMVINIFiles();
extern void copyAndRenameEMVINIFiles();
extern int hex_decimal(char *);
extern int updateCTLSMode();
extern int updateTermDtlsFromINItoAIDList();
extern int checkWalletSupportinDevice();
extern int setXPIParameter(char *, char *);
extern int getSignLimitForAID(char *, double *);
extern PAAS_BOOL isSigReqAfterPINBypass(char *);
extern int sendC30ifReqd();
extern PAAS_BOOL isCVMAvailableInCard(char* , char* , double , double , PAAS_BOOL );
extern int Hex2Bin (unsigned char *, int , unsigned char *);
extern int parseVSDBlobDataResp(char *, int , CARD_TRK_PTYPE, EMVDTLS_PTYPE , MANUAL_PTYPE, int );
extern int getTagValue(char **, char *, char *, int*);
#endif
