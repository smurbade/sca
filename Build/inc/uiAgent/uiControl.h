#ifndef __UI_CTRL_H
#define __UI_CTRL_H

#include "uiGeneral.h"
#include "common/emvStructures.h"

typedef enum
{
	UI_IGNORE = 1,
	UI_XEVT_RESP,
	UI_PIN_RESP,
	UI_SIGN_RESP,
	UI_CARD_RESP,
	UI_VCL_RESP,
	UI_XGCD_RESP,
	UI_XVER_RESP,
	UI_VOL_RESP,
	UI_XGPV_RESP,
	UI_EMV_RESP,
	UI_EMV_U00_REQ,
	UI_EMV_U02_REQ,
	UI_XCLOCK_RESP,
	UI_XGETVAR_RESP,
	UI_XSETVAR_RESP,
	UI_LISTBOX_RESP,
	UI_XCONFIG_RESP,
	UI_XCLS_RESP,
	UI_GENRL_RESP,
	UI_XSWT_RESP,
	UI_XMBX_RESP,
	UI_XQRC_RESP,
	UI_R01_RESP
}
UIRESP_ENUM;

typedef enum
{
	UI_XEVT_DATA_TYPE = 1,
	UI_SWIPE_DATA_TYPE,
	UI_EMV_DATA_TYPE,
	UI_EMVADMIN_DATA_TYPE,
	UI_R01_DATA_TYPE
}
UIDATA_TYPE_ENUM;

typedef struct
{
	uint	uiCtrlType;
	uint	uiCtrlID;
	uint	uiCtrlData;
	uint	uiKeypadEvt;
	uint	uiLBEvt;
	uint	uiLBIdx;
	uint	uiImgXcord;
	uint	uiImgYcord;
	uint	uiVidEvt;
	char	szKpdVal[100];
	char	szFrmName[100];
	char	szVidName[100];
	char	*pszData;
}
XEVT_STYPE, * XEVT_PTYPE;	/* DONE */

typedef struct
{
	char	szFAVer[20];
	char	szSelfVer[20];
	char	szSelfBuildNum[20];
	char	szOSVer[50];
	char	szRFIDVer[50];
	char	szPkgInfo[100];
	char	szXPIVer[50];
	char	szClock[16];
	char	szDHIVer[20];
	char	szDHIProcessor[21];
	int		iDHIBuild;
}
SYSINFO_STYPE, * SYSINFO_PTYPE; /* DONE */

typedef struct
{
	char	szPAN[30];
	char	szClearPAN[30];	// filled by VSD response data
	char	szCVV[10];
	char	szExpDt[5];
	char	szExpClrDt[5];
	char	szEncBlob[512];
	char	szEparms[400];
	char	szVsdKSN[20+1];
	char	szVsdIV[20+1];
}
MANUAL_STYPE, * MANUAL_PTYPE;	/* DONE */

typedef struct
{
	char	szPIN[50];
	char	szKSN[21];
}
PINDATA_STYPE, * PINDATA_PTYPE; /* DONE */



typedef struct
{
	int		iCardSrc;
	char	szTrk1[100];
	char	szTrk2[100];
	char	szTrk3[100];
	char 	szRsaKeyId[32+1];
	char	szRsaTrk1[512];
	char	szRsaTrk2[512];
	char	szRsaTrk3[512];
	char	szClrTrk1[100];	// filled by VSD response data
	char	szClrTrk2[100];	// filled by VSD response data
	char	szEparms[400];
	char	szVsdEncBlob[1024];
	char	szVsdKSN[20+1];
	char	szVsdIV[20+1];
	char	szClrExpDt[6+1]; //Changed 5 to 7 for EMV exp date.
	char	szCrdHldrName[100];
	char	szServiceCodeByteOne[3];
	char	szPayPassType[3];
	char	szPAN[30];//Added store PAN from EMV tag.	
	VASDATA_DTLS_STYPE	stVasDataDtls;
}
CARD_TRK_STYPE, * CARD_TRK_PTYPE;	/* DONE */

typedef struct
{
	int							iCardSrc;
	int							iTrkNo;
	int							iEncType;
	int							iAccountType;
	int							iTranSeqNum;
	int							iKeyPointer;
	int							EMVlangSelctd;		//Lang selected on EMV C30 Resp//TODO: check whether it requird
	char						szEMVRespCode[4];		//Response code of EMV command
	char						szEMVRespCmd[4];		//Response command of EMV
	char						szEMVReqCmd[4];			//Sent/Requested EMV command
	char						szEMVlangPref[17];		//Pref Lang in order(At max 4, each in 2 chars)
	char						szXPIVer[50];			//XPI Version
	char						szPAN[30];
	char						szName[30];
	char 						szRsaKeyId[32+1];
	char						szEMVEncBlob[1024];
	char						szTrackData[512];
	char						szClrTrk1[100];	// filled by VSD response data
	char						szClrTrk2[100];	// filled by VSD response data
	char						szEparms[400];
	char						szVsdKSN[20+1];
	char						szVsdIV[20+1];
	char 						szServiceCode[5];
	char						szSpinBinData[1024];	 //Maximum for each card three records will be present
	char						szEMVTags[1024];
	char						szTagValue[30];
	PAAS_BOOL					bSignReqd;
	EMV_APPDTLS_STYPE			stEmvAppDtls;
	EMV_TRANDTLS_STYPE			stEmvtranDlts;
	EMV_ISSUERDTLS_STYPE		stEmvIssuerDtls;
	EMV_TERMINALDTLS_STYPE		stEmvTerDtls;
	EMV_CRYPTGRMDTLS_STYPE		stEmvCryptgrmDtls;
	EMV_CARD_STATUS_U02_STYPE	stEMVCardStatusU02;
	VASDATA_DTLS_STYPE			stVasDataDtls;
}
EMVDTLS_STYPE, * EMVDTLS_PTYPE;
#define SIZE_EMVDTLS		sizeof(EMVDTLS_STYPE)

typedef struct
{
	int		iSigLen;
	char *	szSigBuf;
}
SIGDATA_STYPE, * SIGDATA_PTYPE; /* DONE */

typedef struct
{
	int		iVol;
	int		iBass;
	int		iTrbl;
}
VOLDTLS_STYPE, * VOLDTLS_PTYPE;	/* DONE */

typedef struct
{
	unsigned short propValue;
}
PROPDTLS_STYPE, * PROPDTLS_PTYPE;


typedef struct
{
	char	szVarialbles[MAX_XGCD_PARAMS][20];
	char* 	szCfgValues[MAX_XGCD_PARAMS];
}
XGETVAR_STYPE, *XGETVAR_PTYPE;

typedef struct
{
	int lXbatchResp[MAX_FAILED_CMDS];
}
XBATCH_STYPE, *XBATCH_PTYPE;

typedef struct
{
	char	szRespStatus[100];
}
XCONFIG_STYPE, *XCONFIG_PTYPE;

typedef struct
{
	char	szRespText[100+1];
}
QRCODE_STYPE, *QRCODE_PTYPE;

typedef union
{
	XEVT_STYPE		stXEvtInfo;
	MANUAL_STYPE	stManualInfo;
	SYSINFO_STYPE	stSysInfo;
	SIGDATA_STYPE	stSigInfo;
	VOLDTLS_STYPE	stVolInfo;
	PINDATA_STYPE	stPINInfo;
	CARD_TRK_STYPE	stCrdTrkInfo;
	PROPDTLS_STYPE  stPropInfo;
	EMVDTLS_STYPE	stEmvDtls;
	XGETVAR_STYPE	stConfigInfo;
	XBATCH_STYPE	stXBatchInfo;
	XCONFIG_STYPE	stXConfigInfo;
	QRCODE_STYPE	stQRCodeInfo;
}
ALLRESP_STYPE, * ALLRESP_PTYPE;

typedef struct
{
	int				iStatus;
	pthread_mutex_t	uiRespMutex;
	UIRESP_ENUM		uiRespType;
	ALLRESP_STYPE	stRespDtls;
}
UIRESP_STYPE, * UIRESP_PTYPE;

typedef struct
{
	char szTitle1[100];
	char szTitle2[100];
	char szTitle3[100];
	char szTitle4[100];
	char szTitle5[100];
	char szTitle6[100];
}LABEL_STYPE_TITLE, * LABEL_PTYPE_TITLE;

typedef struct LI_NODE
{
	char *bufferMsg;
	struct LI_NODE *nxtNode;
}
LI_BUFFER_NODE_STYPE, *LI_BUFFER_NODE_PTYPE;

typedef struct
{
	int	bufferCnt;
	LI_BUFFER_NODE_PTYPE headNode;
	LI_BUFFER_NODE_PTYPE tailNode;
}
LI_BUFFER_QUEUE_STYPE, * LI_BUFFER_QUEUE_PTYPE;

extern int parseUIAgentResp(uchar *, int, UIRESP_PTYPE);
extern int waitForUIResp(char *szQId);

#endif
