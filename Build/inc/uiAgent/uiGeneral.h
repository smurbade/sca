#ifndef __UI_GEN_H
#define __UI_GEN_H

#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include "common/common.h"

#define MAX_FILE_NAME_LEN		50

/* STATUS IMAGE ICON CONSTANTS */
#define STANDBY_ICON			1
#define SUCCESS_ICON			2
#define FAILURE_ICON			3

#define GIFT_CARD				1
#define NON_GIFT_CARD			2
#define	PRIVATE_LABEL			3
#define PAYACCOUNT				4
#define EBT_CARD				5

/* return value constants (For user interface) */
#define UI_ENTER_PRESSED		-10
#define UI_CANCEL_PRESSED		-11
#define UI_BCKSPC_PRESSED		-12
#define UI_OTHER_SELECTED		-13
#define UI_NO_SELECTED			-14
#define UI_YES_SELECTED			-15
#define UI_SIGN_BUTTON_PRESSED	-16
#define UI_SKIP_BUTTON_PRESSED	-17
#define UI_STATIC_SELECTED		-18
#define UI_DHCP_SELECTED		-19
#define UI_DHCP_ALWAYS_SELECTED	-20
#define UI_CARD_INSERTED		-21

#define MAX_XGCD_PARAMS			 5
#define MAX_FAILED_CMDS			 100

#endif
