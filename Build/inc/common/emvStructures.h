/*
 * emvStructures.h
 *
 *  Created on: 02-Apr-2015
 *      Author: AjayS2
 */

#ifndef EMVSTRUCTURES_H_
#define EMVSTRUCTURES_H_

/*AID Name Node information*/
typedef struct __aid4f_name_node
{
	char 				szAIDName[33];
	struct __aid4f_name_node	*next;
}
AID_NAME_NODE_STYPE, * AID_NAME_NODE_PTYPE;

/*AID List information*/
typedef struct
{
	int					iTotalAIDs;
	char				szAIDSelected[33];
	char				szTermCap[10];
	AID_NAME_NODE_PTYPE	LstHead;
	AID_NAME_NODE_PTYPE	LstTail;
}
AID_LIST_INFO_STYPE, * AID_LIST_INFO_PTYPE;

typedef struct
{
	PAAS_BOOL	bCardInserted;
	PAAS_BOOL 	bCardSwiped;
	PAAS_BOOL 	bCardTapped;
	PAAS_BOOL 	bAppSelScreen;
	PAAS_BOOL 	bEMVPINEntry;
	PAAS_BOOL	bLangSelScreen;
	PAAS_BOOL 	bCardRemoved;
	PAAS_BOOL 	bEMVRetry;
	PAAS_BOOL	bSTBDataReceived;
}EMV_CARD_STATUS_U02_STYPE, *EMV_CARD_STATUS_U02_PTYPE;
typedef struct
{
	char 		 			szAID[33];						//4F
	char 		 			szAppSeqNum[3];					//5F34
	char					szAppLabel[33];					//50
	char					szAppPrefName[33];				//9F12
	char			 		szAppEffctiveDate[7];			//5F25
	char					szClearAppExpiryDate[7];		//5F24 Clear
	char					szEncAppExpiryDate[7];			//5F24 Encrypted
	char			 		szAppTranCounter[5];			//9F36
	char			 		szAppIntrChngProfile[5];		//82
	char					szAppVersNum[5];				//9F09
	char					szICCAppVersNum[5];				//9F08
	char					szAppUsageControl[5];			//9F07
	char					szIccCVMList[256];				//8E
	AID_LIST_INFO_STYPE		stAIDListInfo;
}
EMV_APPDTLS_STYPE, *EMV_APPDTLS_PTYPE;

typedef struct
{
	char					szAuthAmnt[13];					//9F02
	char 					szAuthRespCode[5];				//8A
	char					szCVMResult[7];					//9F34
	char					szDedicatedFileName[33];		//84
	char					szEMVCashBackAmnt[13];			//9F03
	char					szTranTime[7];					//9F21
	char					szTranCategoryCode[3];			//9F53
	char					szICCFormFactor[65];			//9F6E
	char					szTranStatusInfo[5];			//9B
	char					szTranStatusInfoPrev[5];		//9B for 1st Gen AC
	char					szTermTranCode[5];				//9F1A
	char					szTermTranDate[7];				//9A
	char					szTermTranQualfr[9];			//9F66
	char					szTermTranSeqCount[9];			//9F41
	char					szTranRefCurCode[10];			//9F3C
	char					szKeySerialNum[41];				//In C33 we got KSN(20 bytes)
	char					szPINBlock[33];					//In C33 we got PIN Block(16 bytes)
	char					szPOSEntryMode[10];				//9F39
	char					szPinTryCounter[3];
	char					szCurrencyCode[5];
	char					szAuthCodeForC34[3];
	int						iHostStatus;					//Host status for sending Issuer or Card Mode to POS
	int						PINEnteredStatus;				//0, 1, 2:PIN Success,No PIN entered, PIN Fail resp.
	int						iOfflineAllowed;
	int						CVMTpeChosen;
	int						iCashbackEnabled;
	PAAS_BOOL				bPINByPassed;
}
EMV_TRANDTLS_STYPE, *EMV_TRANSDTLS_PTYPE;

typedef struct
{
	char					szCryptgrmInfoData[3];			//9F27
	char					szCryptgrmInfoDataPrev[3];		//9F27 for 1st Gen AC
	char					szAppCryptgrm[17];				//9F26
	char					szCryptgrmTranType[3];			//9C
	char					szCryptgrmCurrCode[5];			//5F2A
	char			 		szUnprdctbleNum[9];				//9F37
}
EMV_CRYPTGRMDTLS_STYPE, *EMV_CRYPTGRMDTLS_PTYPE;

typedef struct
{
	char					szIssuerID[7];					//42
	char					szCustExclData[65];				//9F7C
	char					szIssuerAppData[50];			//9F10
	char					szIssuerAuthData[33];			//91
	char					szIssueScrptResults[200];		//9F5B
	char					szIssuerCntryCode[10];			//5F56
	char					szIACDefault[11];				//9F0D
	char					szIACDenial[11];				//9F0E
	char					szIACOnline[11];				//9F0F
	char					szIssuerCodeTableIndex[3];		//9F11
}
EMV_ISSUERDTLS_STYPE, *EMV_ISSUERDTLS_PTYPE;

typedef struct
{
	char					szTermAID[33];						//9F06
	char					szTermCapabilityProf[7];			//9F33
	char					szTermType[3];						//9F35
	char					szTermVerificationResults[11];		//95
	char					szTermVerificationResultsPrev[11];	//95 for 1st Gen AC
	char					szMACValue[9];						//Got this by S66
	char					szDevSerialNum[17];					//9F1E: Got this by S66
	char					szTACDefault[11];
	char					szTACDenial[11];
	char					szTACOnline[11];
	char					szEMVKernelVer[11];
	char					szEMVCTLSVer[21];
	char 					szTermCapDefault[7];
}
EMV_TERMINALDTLS_STYPE, *EMV_TERMINALDTLS_PTYPE;

typedef struct
{
	//We need to know on which form VAS data was captured because, after sending VAS to POS and on next Sale command, we will move to Card Swipe Screen or Loyalty
	//if VAS was captured there previously
	//1-> preswipe form
	//2-> Loyalty form
	//3-> CardSwipe form
	int			iVasCapturedForm;
	PAAS_BOOL 	bVASPresent;
	PAAS_BOOL	bVASsentToPOS;
	PAAS_BOOL 	bOnWelcomeScreenAfterVAS;
	PAAS_BOOL	bProvisionPassSent;
	PAAS_BOOL	bProvisionPassOnPayment;
	char    	szPubliserName[25];
	char   		szLoyaltyData[256];
}VASDATA_DTLS_STYPE, *VASDATA_DTLS_PTYPE;



#endif /* EMVSTRUCTURES_H_ */
