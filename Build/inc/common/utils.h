#ifndef __UTILS_H
#define __UTILS_H

#include "common.h"
#include "pthread.h"

/* APIs exposed */
extern int encdBase64(uchar *, int, uchar *, int *);
extern int dcdBase64(uchar *, int, uchar *, int *);
extern int doesFileExist(const char *);
extern int deleteFile(const char *);
extern int asciiToHex(uchar *, char *, int);
extern int hexToAscii(char *, uchar *, int);
extern int calcMAC(uchar *, int, uchar *, int, uchar *, int *);
extern int rsaEncryptData(uchar *,uchar *,uchar *,int *);
extern int stripSpaces(char *, int );
extern void strupper(char *);
extern int genTimeStamp(char *, int);
extern void formatAmount(char *, char *, AMT_SIGN);
extern uchar findLrc(char *, int);
extern int decryptDataWithAES(char *, char *, char *, int );
extern int encryptDataWithAES(char **, char **, char *, int *);
extern void getPKCS5PaddedData(char *);
extern int calcFingerPrint(char*, char*);
extern int local_svcSystem(const char *);

extern int getDate(char *, char *);
extern int getTime(char *, char *);
extern int getUUID(char*);
extern int getDevSrlNum(char*);

extern int validateBooleanField(char *);
//extern int validateAmountField(char *);
extern int validateNumericField(char *);
extern int validateBooleanField(char *);
extern void acquireMutexLock(pthread_mutex_t *, const char *);
extern void releaseMutexLock(pthread_mutex_t *, const char *);
extern void releaseAcquiredMutexLock(pthread_mutex_t * , const char * );

extern int checkForExpiry(char *, char *);
extern void getMaskedPAN(char *, char *);
extern void getMaskedTRACK2(char *, char *);
extern void getMaskedCVV(char*, char*);
extern void convStrToUTFEscStr(char *, char *);
extern void getMaskedPANForBAPI(char *, char *);
extern int  getTrackDataFromVCL(char* , char *, char *);

extern void alignString(char*, char*, int, int);
extern void alignStringToLeft (char * , int );
extern void alignStringToRight (char * , int );
extern void alignStringToCenter (char * , int );
extern void strToUpper(char *);
extern int restartDHI();
extern void getFGNAmntWithDecimals(char*, int, char*);
extern int strReplaceAll(char * input,  const char* search,  const char *format);
#endif
