
#ifndef VSP_H_
#define VSP_H_

#ifdef __cplusplus
extern "C" {
#endif

// Command IDs 
#define VSP_DEVICE_REG 					1
#define VSP_ADVANCE_DDK 				2
#define VSP_SWITCH_UKPD 				3
#define VSP_UPDATE_SETTINGS 			4
#define VSP_EPARM_REQUEST   			5
#define	VSP_DEVICE_REG_STOP 			6
#define	VSP_DEVICE_REG_SRED 			7	//added sandeepk1 when specifically we need to call SRED device registration
#define VSP_MSG_CLEAR_REQUEST   		8   //added RampraneshM1 we need this for getting clear track data
#define VSP_MSG_FLUSH_BUFFER_REQUEST   	9  // Added Abdul_R1, flush the VCL buffer
#define VSP_MSG_PAN_CLEAR_REQUEST		10 //Added by Mahendrakars1




// Error Codes
#define VSP_SUCCESS				     1
#define VSP_ERROR					-1
#define VSP_DAEMON_NOTAVAILABLE		-2
#define VSP_ENCRYPTION_OFF			-3
#define VSP_CREATINGMSGQ_FAILED		-4
#define VSP_HTDES_NOTFOUND			-5
#define VSP_INTERNAL_ERROR 			-6  //MSGQ related errors
#define VSP_INVALID_COMMAND_ERROR 	-7
#define VSP_COMMAND_EXECUTION_ERROR -8
#define VSP_INVALID_STATE_ERROR     -9
#define VSP_NOT_SUPPORTED		    -10
#define VSP_INVALID_MODE            -11
#define VSP_NO_VALID_UPDATE_FILE    -12



#define MAX_VSP_RESP_BUFFER_SIZE   1024+1
#define MAX_VSP_ERROR_RESPONSE_BUFFER 256+1
#define MAX_VSP_MSR_TRACK_DATA 107     // Max length of track data, track 3 is 107 -- AmitR1 , 03-DEC-13

#define DK_MODE_SHARED 1
#define DK_MODE_UNIQUE 2

typedef enum TagVSPSTATE
{
	VSP_LOCKED = 0,
	VSP_PROCESSING = 1,
	VSP_UNLOCKED = 2
} VSP_STATE;

typedef enum{
	VSP_NO_ERRORS,
	VSP_NO_TRACK_DATA,
	VSP_NO_START_SENTINEL,
	VSP_NO_END_SENTINEL
} VSP_TRACK_ERRORS;

/* Structure for one track data   -- AmitR1, 03-DEC-13*/
typedef struct {
    unsigned char ucStatus; // status of track
    unsigned char ucCount; // size in bytes of track Data payload
    char szTrackData[MAX_VSP_MSR_TRACK_DATA+1]; // pointer to track data
} VSP_MSR_TRACK_DATA;

/* Structure for clear and encrypted track data  of all tracks -- AmitR1, 03-DEC-13*/
typedef struct {
    unsigned long ulEncryptionStatus;
    VSP_MSR_TRACK_DATA stEncryptedTrack[3];  // Array of encrypted track data, will be filled by the function call
    VSP_MSR_TRACK_DATA stClearTrack[3];      // Array of clear track data, has to be filled by the callee
} VSP_MSR_DATA;



/****************************************************************
 *  This function can be used to initialize the message queue
 *  required parameters
 * **************************************************************/
extern int InitVSP();
// int IsHTDESModuleInstalled();
// int InitHTDESMsgQueues();
extern int ProcessVSPCommand(int CommandID, char *szOutputBuffer ,char *szErrorBuffer);
extern int VSP_EncryptData(char*, char*, char*);
extern int VSP_EncryptManualData(char* szPAN, char* szExpDate, char* szOutputBuf); //mahendrakars1 added.
extern int VSP_EncryptTrack2Data(char* szTrack2, char* szEncTrack, char* szEncryptedPAN, char *szEncryptedExpiry, char *szEncDesc);
/*To encrypt the whole MSR tracks*/
extern int VSP_EncryptMSR(MSG_MSR_DATA* stInputTracks, MSG_MSR_DATA* stOutputTracks);//t_roshana1 01 APR 2014
extern int vspLibVersion(char* szVersion);
extern int vspEncryptionStatus();
extern int GetVSPMDKLabel(char *szMdkLabel); //sandeepk1
extern int GetVSPState();	//sandeepk1
extern int GetKeyMode();
extern int GetEparmsStatus(void);    // (returns 0 for off, 1 for ON) AmitR1, 02-DEC-13, 
extern int VSP_EncryptCardData(VSP_MSR_DATA *pstMSRData); // AmitR1, 02-DEC-13
extern int VSP_GetLastEncryptionStatus();                     // AmitR1, 16-MAY-2014 

// return 1 if VSP state >= SEMTEKD_NORMAL_OPERATIONS, otherwise 0
extern int VSP_GetKeyStatus();                                // AmitR1, 26-MAY-2014

// returns "on" or "off"
extern int VSP_GetEncryptionState(char* pszEncState);         // AmitR1, 26-MAY-2014

extern int GetHTDESModuleVersion(char *version);	//mahendrakars1 date: 24-01-2014

extern int GetVCLBinTableID(char* szBINTableID, int len);		// AmitR1, 28-JAN-2015

#ifdef __cplusplus
}
#endif

#endif /*VSP_H_*/

