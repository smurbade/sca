#ifndef __CFG_DATA_H
#define __CFG_DATA_h

/* 
 * Sections/Tables in the configuration file
 * */
#define SECTION_PAYMENT			"PDT"
#define SECTION_PERM			"PERM"
#define SECTION_REG				"REG"
#define SECTION_TENDER			"TDT"
#define SECTION_SIGN			"SCT"
#define SECTION_CBACK			"CBT"
#define SECTION_CTRTIP			"CTT"
#define SECTION_DEVICE			"DCT"
#define SECTION_HOST			"HDT"
#define SECTION_MRCHNT			"MDT"
#define SECTION_MEDIA			"MCT"
#define SECTION_RECEIPT			"RCT"
#define SECTION_SAF				"SAF"
#define SECTION_CCY				"CCY"
#define SECTION_CONSUMEROPTS	"COT"
#define SECTION_DHI				"DHI"
#define SECTION_SPINTHEBIN		"STB"
#define SECTION_RCHI			"RCHI"
#define SECTION_CPHI			"CPHI"
#define SECTION_EHI				"EHI"
/*
 * Keys' definitions
 * */
/* --- Perm Section --- */
#define XPI_LIBRARY			"xpilib"

/* --- Reg Section -- */
#define PREAMBLE_SUCCESS	"preamble_success"
#define VSP_ACTIVATED		"vsp_activated"
#define PINPAD_MODE			"mk"		// mk=1(DUKPT), mk=0 (Master/Session)
#define XML_CFGFILE_PATH	"xmlcfgfilepath"
#define TIMEZONE			"timezone"


/* --- Payment Definition Table - PDT --- */
#define CASHBACK_ENABLED		"cashback"
#define EBTCASHBACK_ENABLED		"ebtcashback"
#define SPLT_TNDR_ENABLED		"splittender"
#define LYLTYCAP_ENABLED		"loyaltycapture"
#define EMAILCAP_ENABLED		"emailcapture"
#define SIGCAP_ENABLED			"sigcapture"
#define CNTCTLSS_ENABLED		"contactless"
#define TIP_SUPP_ENABLED		"PRTIP1"
#define CTRTIP_ENABLED			"countertip"
#define MANENTRY_ENABLED		"manualentry"
#define SAF_ENABLED				"safenabled"
#define PARTAUTH_ENABLED		"partialauth"
#define RECEIPT_ENABLED			"receiptenabled"
#define GIFTREFUND_ENABLED		"giftrefund"


/* --- Tender Types Definition Table - TDT --- */
#define CREDIT_TYPE			"credit"
#define DEBIT_TYPE			"debit"
#define GIFTCARD_TYPE		"gift"
#define GWALLET_TYPE		"gwallet"
#define IWALLET_TYPE		"iwallet"
#define CASH_TYPE			"cash"
#define PRIVATE_TYPE		"privlabel"
#define MERCHCREDIT_TYPE	"merchandisecredit"
#define PAYPAL_TYPE 		"paypal"
#define EBT_TYPE 			"EBT"
#define FSA_TYPE 			"FSA"

/* --- Signature Configuration Table - SCT --- */
#define SIGN_BEG_X			"begx"
#define SIGN_BEG_Y			"begy"
#define SIGN_END_X			"endx"
#define SIGN_END_Y			"endy"
#define	SIGN_AMT_FLOOR		"sigamount"
#define SIGN_IMAGE_TYPE		"sigimagetype"

/* --- Store and Forward Table - SAF --- */
#define SAF_TRAN_FLOOR_LIMIT						"transactionfloorlimit"
#define SAF_TOTAL_FLOOR_LIMIT						"totalfloorlimit"
#define SAF_PRIV_LBL_TRAN_FLOOR_LIMIT				"privlbltranfloorlimit"
#define SAF_DAYS_LIMIT								"dayslimit"
#define SAF_PING_INTERVAL							"safpinginterval"
#define SAF_PURGE_DAYS								"safpurgedays"
#define SAF_TRAN_POST_INTERVAL						"safpostinterval"
#define SAF_ALLOW_OFFLINE_TRAN_TOGOWITH_ONLINE		"allowOfflineTranToGoWithOnline"
#define SAF_THROTLING_ENABLED						"safthrottlingenable"
#define SAF_BUSINESSDATE							"businessdate"
#define SAF_FORCE_DUP_TRAN							"forcesafduptran"
#define SAF_CHK_CONN_REQD							"safchkconnreqd"
#define SAF_INDICATOR_REQUIRED						"safindicatorreqd"
#define SAF_ALLOW_REFUND_TRAN						"allowrefundtosaf"
#define SAF_ALLOW_VOID_TRAN							"allowvoidtosaf"
#define	SAF_ALLOW_GIFT_ACTIVATE						"allowgiftactivatetosaf"
#define SAF_CHK_FLOOR_LIMIT_TO_SAF_REFUND			"chkfloorlimittosafrefund"
#define SAF_THROT_INTERVAL							"safthrottlinginterval"
#define	SAF_ALLOW_PAYACCOUNT_TO_SAF					"allowpayaccounttosaf"

/* --- Cash back Table - CBT --- */
#define CB_AMT_LIMIT			"cashbackamtlimit"
#define CFG_CBAMT_PREFIX		"presetamt_"

/* --- Counter Tip Table - CTT --- */
#define CFG_CTRTIP_PREFIX		"presettip_"

/* --- Consumer Options Table - COT --- */
#define CONSUMER_OPTION_PREFIX	"consumer_option"
#define CONSUMER_OPTION1		"consumer_option1"
#define CONSUMER_OPTION2		"consumer_option2"
#define CONSUMER_OPTION3		"consumer_option3"
#define CONSUMER_OPTION4		"consumer_option4"
#define CONSUMER_OPTION5		"consumer_option5"
#define CONSUMER_OPTION6		"consumer_option6"

/* --- Device Configuration Table - DCT --- */
#define	VHI_APP									"vhiapp"
#define	DHI_APP									"dhiapp"
#define	DFLT_LANG								"defaultlang"
#define LISTEN_PORT								"deviceport"
#define FA_PORT									"faport"
#define BADCARDREAD_LIM							"badcardreadlim"
#define DEMO_MODE								"demomode"
#define VSP_ENABLED								"vspenabled"
#define EMV_ENABLED								"emvenabled"
#define CTLS_EMV_ENABLED						"contactlessEmvEnabled"
#define	SUPPORTED_WALLETS						"supportedwallets"
#define	STORE_CARD_DTLS_POST_AUTH				"storecarddtlsforpostauth"
#define	STORE_PREV_TRANS_CARD_DTLS_GIFT_CLOSE	"storecarddtlsforgiftclose"
#define CUSTOM_TAG_REQD							"customtagsreqd"
#define PARTIAL_EMV_ALLOWED						"partialemvallowed"
#define CLEAR_EXPIRY_TO_EMV_HOST				"clearexpiryin5F24tag"
#define TAGS_REQ_VOID_CARD_PRESENT				"tagsreqforcardpresentvoid"
#define CTLS_ENABLED							"contactlessEnabled"
#define	CARD_READ_SCREEN_FIRST					"cardreadscreenfirst"
#define EMV_KERNEL_SWITCHING_ALLOWED			"emvkernelswitchallowed"
#define	SAF_EMV_ONLINE_PIN_TRANS				"safemvonlinepintran"
#define SERVICE_CODE_CHECK_IN_FALLBACK			"servicecodechkforfallback"
#define	EMPTY_CANDID_AS_FALLBACK				"emptycandidasfallback"
#define VERBOSERECPT_ENABLED 					"verboseRecptEnabled"
#define DESCRIPTIVE_ENTRY_MODE					"descriptiveentrymode"
#define IP_DISP_INTRVL							"ipdispinterval"
#define SYS_INFO_DISP_INTRVL					"sysinfointerval"
#define DEVNAME_DISP_INTRVL 					"devnamedispinterval"
#define NETWORK_CFG_REQD 						"networkcfgreqd"
#define DEV_ADMIN_REQD 							"devadminreqd"
#define APPLICATION_TYPE						"ATYPE1"
#define PIN_ENCR_TYPE							"ENCRPT1"
#define DEVTYPE_SUFFIX							"devtypesuffix"
#define PRIV_LAB_CVV_REQD						"privlabelcvvreqd"
#define PRIV_LAB_EXP_REQD						"privlabelexpreqd"
#define GIFT_CVV_REQD							"giftcardcvvreqd"
#define GIFT_EXP_REQD							"giftcardexpreqd"
#define GIFT_PINCODE_REQD						"giftcardpincodereqd"
#define SIG_OPT_ON_PINENTRY_REQD				"sigoptonpinentryreqd"
#define DHI_ENABLED								"dhienabled"
#define STATUS_MSG_DISP_INTRVL					"statusmsgdispinterval"
#define	ADVANCE_VSP_IN_X_DAYS					"ADVANCEVSPKEYINXDAYS"
#define DEV_NAME_ENABLED						"devnameenabled"
#define DEVICE_NAME								"devicename"
#define SWIPE_AHEAD_ENABLED 					"swipeaheadenabled"
#define BAPI_ENABLED							"bapienabled"
#define CHECK_HOST_CONN_STATUS					"checkhostconnstatus"
#define HOST_PING_REQD							"hostpingreqd"
#define SCND_PORT_ENABLED						"scndportenabled"
#define DUP_DETECT_ENABLED 						"dupswipedetectenabled"
#define STB_LOGIC_ENABLED						"stblogicenabled"
#define STB_SIG_LIMIT							"signatureLimit"
#define STB_PIN_LIMIT							"pinLimit"
#define STB_INTERCHANGE_MGMT_LIMIT				"interchangeMgmtLimit"
#define DEBIT_VOID_CARD_DTLS_REQD 				"debitvoidcarddtlsreqd"
#define RETURN_EMBOSSED_NUM_FOR_GIFT			"returnembossednumforgift"
#define RETURN_EMBOSSED_NUM_FOR_PL				"returnembossednumforprivlbl"
#define CAPTURE_SIGN_FOR_MANUAL_TRAN			"capturesignformanualtran"
#define TOR_ENABLED								"torenabled"
#define	TOR_RETRY_COUNT							"torretries"
#define EMV_SETUP_REQD							"emvsetupreqd"
#define HOST_PROTOCOL_FORMAT					"hostprotocolformat"
#define PA_PYMT_TYPE_FORMAT						"papymttypeformat"
#define LANE_CLOSED_FONT_COL 					"laneclosedfontcol"
#define LANE_CLOSED_FONT_SIZE 					"laneclosedfontsize"
#define	EMV_SUPP_LANGS		 					"emv_supported_lang"
#define PREAMBLE_PING_REQD						"preamblepingreqd"
#define MCNT_SETTINGS_REQD						"merchantsettingsreqd"
#define LINEITEM_DISPLAY_ENABLED 				"lineitem_display"
#define FULL_LINEITEM_DISPLAY  				    "fulllidisplayenabled"
#define APP_LOG_ENABLED 						"applogenabled"
#define MAX_BCKUP_LOG_FILES 					"maxBckupLogFiles"
#define CUR_NUM_BCKUP_LOG_FILES 				"curNumBckupLogFiles"
#define MAX_LOG_FILE_SIZE 						"maxlogFileSize"
#define LOG_FILE_LOCATION 						"logFileLocation"
#define	MAX_POS_CONNECTIONS						"maxposconnections"
#define EMBOSSED_NUM_CALC_REQD					"embossednumcalcreqd"
#define	LISTEN_SCND_PORT						"devicescndport"
#define REDO_VSP_REG							"redovspreg"
#define CAPTURE_SIG_FOR_PRIV_LABEL				"capturesignatureforprivlbl"
#define SVS_NON_DENOMINATED_CARD				"svsnondenominatedcard"
#define WAIT_TO_OBTAIN_NETWORK_IP				"waittimetoobtainnetworkip"
#define EBT_CVV_REQD							"ebtcardcvvreqd"
#define EBT_EXP_REQD							"ebtcardexpreqd"
#define INCLUDE_TA_FLAG							"includeTAflag"
#define TERM_IDEN_DISPLAY_INTERVAL	 			"termidendispinterval"
#define TERM_IDEN_POS_ACK_INTERVAL	 			"termidenposackinterval"
#define BROADCAST_PORT				 			"broadcastport"
#define SEND_SIGN_DTLS_TO_SSI_HOST	 			"sendsigndtlstossihost"
#define DCC_ENABLED								"dccenabled"
#define SAF_ON_RESP_TIMEOUT						"safonresptimeout"
#define UNSOL_MSG_DURING_PYMT_TRAN	 			"sendunsolmsgduringpymttran"
#define DEBIT_OFFLINE_PIN_CVM		 			"debitOffPinCVM"
#define SWITCH_TO_PRIMURL_AFTER_X_TRAN			"switchtoprimurlafterxtran"
#define TOGGLE_URL_AFTER_X_RESP_TIMEOUT			"toggleurlafterxresptimeout"
#define PREAMBLE_SELECT_WAIT_TIME				"preambleselectwaittime"

/* --- Host Definition Table - HDT --- */
#define PRIM_HOST_URL			"primurl"
#define PRIM_HOST_PORT			"primport"
#define SCND_HOST_URL			"scndurl"
#define SCND_HOST_PORT			"scndport"
#define PRIM_CONN_TO			"primcontimeout"
#define SCND_CONN_TO			"scndcontimeout"
#define PRIM_RESP_TO			"primrestimeout"
#define SCND_RESP_TO			"scndresptimeout"
#define KEEP_ALIVE				"keepalive"
#define KEEP_ALIVE_INTERVAL		"keepaliveinterval"

/* --- Host Definition Table - HDT --- */
#define BAPI_PRIM_URL		"bapiprimurl"
#define BAPI_SCND_URL		"bapiscndurl"
#define BAPI_PRIM_PORT		"bapiprimport"
#define BAPI_SCND_PORT		"bapiscndport"
#define BAPI_PRIM_CONN_TO	"bapiprimcontimeout"
#define BAPI_SCND_CONN_TO	"bapiscndcontimeout"
#define BAPI_PRIM_RESP_TO	"bapiprimrestimeout"
#define BAPI_SCND_RESP_TO	"bapiscndresptimeout"
#define BAPI_DEST_ID		"bapidestinationid"

/* --- Direct Host Interface Section - DHI --- */
#define	PROCESSOR					"processor"
#define	CREDIT_PROCESSOR			"creditprocessor"
#define	DEBIT_PROCESSOR				"debitprocessor"
#define	GIFT_PROCESSOR				"giftprocessor"
#define BANK_ID						"bankid"
#define DIRECT						"direct"


/* ---Rapid Connect Host Interface - RCHI -- */
#define TPP_ID 					"tppid"
#define TERMINAL_ID				"tid"
#define MERCHANT_ID				"mid"
#define MERCHANT_CAT_CODE		"merchantcatcode"
#define TRANSACTION_CURRENCY	"trancrncy"
#define GROUP_ID				"groupid"
#define LANE_ID					"laneid"			/* POSSIBLY LANE ID IS SAME AS POS ID */
#define ADDTL_AMT_CURRENCY		"addamtcrncy"
#define ALTERNATE_MERCH_ID		"altmerchid"
#define DOMAIN					"domain"
#define BRAND					"brand"
#define FD_SERVICE_ID			"serviceid"
#define FD_APP_ID				"appid"
#define REG_HOST_URL			"reghosturl"
#define REG_HOST_PORT			"reghostport"
#define REG_CONN_TO				"regcontimeout"
#define REG_RESP_TO				"regresptimeout"
#define TOKEN_TYPE				"tokentype"

/* ---Chase payment Tech Host Interface - CPHI -- */
#define ROUTING_IND 			"routingind"
#define CLIENTNUM				"clientnum"
#define USERNAME				"username"
#define PASSWORD				"password"
#define SW_IDENTIFIER			"swidentifier"
#define HW_IDENTIFIER			"hwidentifier"
#define THIRDPARTYGIFT_ENABLED	"thirdpartygiftenabled"

/* ---Elavon Host Interface - EHI -- */
#define CHAIN_CODE 				"chaincode"
#define LOCATION				"location"
#define	TERMINAL_CURRENCY		"termcurncytrigraph"

/* --- Currency Definition Table - CCY --- */
#define CCY_CODE				"ccycode"
#define AMT_FMT					"amtformat"

/* --- Media Control Table - MCT --- */
#define IDLE_IMG_LIST_FILE		"idlescrimglistfile"
#define LI_IMG_LIST_FILE		"liscrimglistfile"
#define VIDEO_LIST_FILE			"videolistfile"
#define IDLE_SCREEN_MODE_FILE	"idlescreenmodefile"
#define ANAIMATION_UPDATE_INTV	"animationupdIntv"


/* --- Merchant Section -- */
#define PS_GC_MANUAL_ENTRY  	"PointGCManualEntry"
#define CLIENT_ID				"clientid"
#define	GIFT_CRD_PRCSR_ID		"giftprocessorid"
#define	CREDIT_PRCSR_ID			"creditprocessorid"
#define	DEBIT_PRCSR_ID			"debitprocessorid"
#define CHECK_PRCSR_ID			"checkprocessorid"

#define DFLT_BADCARD_READ_CNT	3
#define DFLT_LISTEN_PORT		5015
#define DFLT_FA_PORT			9001
//#define DFLT_HOSTCONN_TIMEOUT	10
//#define DFLT_HOSTRESP_TIMEOUT	10

#define PROMPTS_FILE_NAME		"./flash/displayPrompts.ini" //Praveen_P1: Changing the location of the file to flash
//#define CFG_INI_FILE_PATH	 	"./flash/emvConfig.ini"

/* Structure instances for loading and reading the parameters */

#endif
