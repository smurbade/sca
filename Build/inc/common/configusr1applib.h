/*
 * configusr1applib.h
 *
 *  Created on: Nov 9, 2016
 *      Author: KranthiK1
 */

#ifndef CONFIGUSR1APPLIB_H_
#define CONFIGUSR1APPLIB_H_

#ifdef __cplusplus
extern "C" {
#endif

extern int initConfigUsr1AppLib();

#ifdef __cplusplus
}
#endif

#endif /* CONFIGUSR1APPLIB_H_ */
