#ifndef __COMMON_H
#define __COMMON_H

#ifdef NEWKERNEL
#define SCA_APP_VERSION	"V_2.20.27"
#else
#define SCA_APP_VERSION	"V_2.19.27"
#endif

#define SCA_BUILD_NUMBER "3"

/* ASCII characters to be used */
/* VDR: Remove those which are not used at all */
#define SOH     0x01
#define STX     0x02
#define STX_e   0x82
#define ETX     0x03
#define EOT     0x04
#define EOT_e   0x84
#define ENQ     0x05
#define ACK     0x06
#define ACK0    0x06
#define ACK1    0x07
#define BS      0x08
#define HT      0x09
#define VT      0x0b
#define FF      0x0c
#define SO      0x0E
#define SI      0x0F
#define DLE     0x10
#define DLE_e   0x90
#define DC1     0x11
#define WACK    0x11
#define DC2     0x12
#define WAIT    0x12
#define NAK     0x15
#define NAK_e   0x95
#define SYN     0x16
#define ETB     0x17
#define CAN     0x18
#define SUB     0x1A
#define ESC     0x1B
#define FS      0x1C
#define GS      0x1D
#define RS      0x1E
#define US      0x1F
#define SPACE   0x20
#define FORMFEED 0x0C
#define CR      0x0D
#define LF      0x0A
#define PAD     0x0FF
#define PIPE    0x7C

#define SEMI_COLON 0x3B
#define COLON      0x3A

extern char chDebug;
extern char chMemDebug;

#ifdef DEBUG
	#include "tcpipdbg.h"
	//#define debug_sprintf(...) sprintf(__VA_ARGS__)
	#define debug_sprintf(...) \
	{ \
	    if (chDebug != NO_DEBUG) \
	    { \
	        sprintf(__VA_ARGS__) ; \
	    } \
	}

	#define debug_mem_sprintf(...) \
	{ \
		if (chMemDebug != NO_DEBUG) \
		{ \
			sprintf(__VA_ARGS__) ; \
		} \
	}

	extern void setupDebug(int *);
	extern void closeupDebug(void);
	extern void setupMemDebug(int *);
	extern void closeupMemDebug(void);
	extern void APP_TRACE(char *pszMessage);
	extern void APP_MEM_TRACE(char *pszMessage);
	extern void APP_TRACE_EX(char *pszMessage, int iLen);
	extern int setDebugParamsToEnv(int * );
#else
	#define debug_sprintf(...) ;
	#define debug_mem_sprintf(...) ;
	#define APP_TRACE(s) ;
	#define APP_MEM_TRACE(s) ;
	#define APP_TRACE_EX(s, i) ;
#endif

#define SUCCESS 		0
#define FAILURE			-1403
#define NO_REC_PRESENT	-10
#define SOCK_DISCON		-11
#define SOCK_CONN		-12
#define MAX_DEBUG_MSG_SIZE 15+4096+2+1

#define POS_DISCONNECTED	-1001
#define POS_CANCELLED		-1002

#define SAF_REQ_ENTIRE			1
#define SAF_REQ_RANGE			2
#define SAF_REQ_SINGLE			3
#define SAF_REQ_STATUS			4
#define SAF_REQ_STATUS_SINGLE	5
#define SAF_REQ_STATUS_RANGE	6
#define SAF_RECORD_FOUND		7
#define SAF_RECORD_NOT_FOUND	8
#define SAF_NOT_ENABLED			9
#define SAF_REQ_NOTHING			10
#define	SAF_RECORD_IN_PROGRESS	16
#define	SAF_NOT_ALLOWED			17
#define SAF_REC_PARTIALLY_FOUND 18

#define SAF_UN_SUPPORTIVE_PAYMENT_TYPE	10
#define SAF_TRAN_FLOOR_LIMIT_EXCEEDED	11
#define SAF_TOTAL_AMOUNT_EXCEEDED		12
#define SAF_OFFLINE_DAYS_EXCEEDED		13
#define SAF_INVALID_CARD				14
#define SAF_UN_SUPPORTIVE_TRAN			15

#define CDT_RECORD_FOUND     1
#define CDT_RECORD_NOT_FOUND 2

#define CARD_TOKEN_PRESENT     1
#define CARD_TOKEN_NOT_PRESENT 2

#define FIRST_TIME_CALL 1
#define NEXT_TIME_CALL  2

#define STRIP_LEADING_SPACES	0x01
#define STRIP_TRAILING_SPACES	0x02

#define SIG_CANCELLED				-50
#define PIN_CAPTURE_COMPULSORY 3

/* STATE ID CONSTANTS */
#define CAPTURE_LOYALTY      		1
#define CAPTURE_EMAIL		 		2
#define CAPTURE_COUNTERTIP	 		3
#define CAPTURE_TENDERSEL    		4
#define CAPTURE_CARDDETAILS	 		5
#define PROC_EMV_TRAN_REQ	 		6
#define CAPTURE_PINDETAILS   		7
#define CAPTURE_ACCOUNT_TYPE 		8
#define CAPTURE_SPLITTENDER	 		9
#define CAPTURE_CASHBACK     		10
#define CAPTURE_SIGNATURE    		11
#define CAPTURE_PAYPAL_PAYMENT_CODE 12
#define CAPTURE_PAYPAL_AMTOK		13
#define CAPTURE_EBT_TYPE			14
#define	KERNEL_VALIDATE				15
#define PROC_PYMT_FLOW_DECISION		16
#define	PYMNT_TYPE_DEPENDENT_FLOW	17
#define	PROC_EARLY_CARD_CAPTURE		18
#define CAPTURE_DCC					19
#define END							20

/* ERROR CONSTANTS */
#define ERR_INV_XML_GID		-99
#define ERR_INV_XML_MSG		-100
#define ERR_USR_CANCELED	-101
#define ERR_USR_TIMEOUT		-102
#define ERR_DEVICE_BUSY		-103
#define ERR_IN_SESSION		-104
#define ERR_NO_SESSION		-105
#define ERR_UNSUPP_OPR		-106
#define ERR_UNSUPP_CMD		-107
#define ERR_BAD_CARD		-108
#define ERR_SYSTEM			-109
#define ERR_VERISHIELD		-110
#define ERR_DEVICE_APP		-111
#define ERR_NO_CONN			-112
#define ERR_CONN_TO			-113
#define ERR_CONN_FAIL		-114
#define ERR_RESP_TO			-115
#define ERR_CFG_PARAMS		-116
#define ERR_FLD_MISMATCH	-117
#define ERR_INV_COMBI		-118
#define ERR_FLD_REQD		-119
#define ERR_INV_FLD			-120
#define ERR_DUP_FLD			-121
#define ERR_NO_FLD			-122
#define ERR_INV_FLD_VAL		-123
#define ERR_FLD_LTZ			-124
#define ERR_FLD_LTEZ		-125
#define ERR_POS_AUTH		-126
#define ERR_POS_REG			-127
#define ERR_POS_UNREG		-128

#define ERR_NO_KEYS_PRESENT   -129
#define ERR_NULL_PIN_ENTERED  -130
#define ERR_FILE_NOT_FOUND    -131

#define ERR_SAF_CARDDTLS_NOT_FOUND -132
#define ERR_SAF_SESSDTLS_NOT_FOUND -133
#define ERR_SAF_TRANDTLS_NOT_FOUND -134
#define ERR_SAF_PYMTDTLS_NOT_FOUND -135
#define ERR_SAF_FILE_OPEN          -136
#define ERR_SAF_FILE_WRITE         -137

#define ERR_OFFLINE_TRAN_AMT_EXCEEDED  		-138
#define ERR_OFFLINE_TOTAL_AMT_EXCEEDED 		-139
#define ERR_OFFLINE_DAYS_EXCEEDED      		-140
#define ERR_OFFLINE_INVALID_CARD_DATA  		-141
#define ERR_CARD_NOT_IN_INCLUSIONRANGE 		-142
#define ERR_INVALID_CARD_DATA		   		-143
#define ERR_MANUAL_NOTALLWD_FOR_CARD   		-144
#define ERR_OFFLINE_UN_SUPPORTIVE_PYMT_TYPE -145
#define	ERR_SAF_NOT_ALLOWED_CURRENTLY		-182

#define ERR_ENCODE							-146
#define ERR_AES_ENCRYPTION					-147
#define ERR_AES_DECRYPTION					-148
#define ERR_COUNTER							-149
#define ERR_MAC_MISMATCH					-150
#define ERR_MAC_MISSING						-151
#define ERR_MAC_NOTFOUND					-152
#define ERR_ENTRY_CODE_MISMATCH				-153

#define	ERR_DEV_REBOOTING					-154

#define	ERR_DUPSWIPE_DETECTED				-155
#define	ERR_INVOICE_MISMATCH				-156
#define	ERR_NODE_NOT_IN_LIST				-157
#define ERR_POS_REG_LIMIT_EXCEEDED			-158
#define ERR_PROXY_CONN_FAIL					-159
#define ERR_PROXY_RESP_TO					-160

#define ERR_INV_PYMT_TYPES_CNT				-161
#define ERR_INV_PYMT_TYPES					-162
#define ERR_INV_TENDER_CNT					-163
#define ERR_CHIP_ERROR						-164
#define	VAS_DATA_CAPTURED					-165
#define ERR_NO_MATCHING_PROV_PASS			-166
#define PROVISION_PASS_SENT					-167
#define	ERR_NO_LINE_ITEMS					-168
#define	EARLY_CARD_CAPTURED					-169
#define ERR_NOT_ACCEPTED_CONFIG				-170
#define	ERR_XBATCH_FAILED					-171
#define ERR_BAD_DATA_IN_CARD				-172
#define ERR_AMOUNT_EXCEEDS_LIMIT			-173
#define ERR_NO_UPDATES						-174
#define ERR_QRCODE_GENE_FAILED				-175
#define ERR_INV_DISP_QRCODE_CMD				-176
#define ERR_INV_CANCEL_QRCODE_CMD			-177
#define	ERR_INI_PARSING_FAIL				-178

/* VSP Reg Errors */
#define ERR_VCL_DATA_ERR					-179
#define ERR_VSP_HOST_ERR					-180
#define VSP_FULLY_SUCCESS					-181

#define ERR_PIN_REQD_EMV_CASHBACK			-183

/* VSP related error constants */
#define	SUCCESS_VSP_START_ENCRYPTN_4		   4
#define SUCCESS_VSP_ALL_TRACKS	  			 100
#define SUCCESS_VSP_T1_TRACKS			 	 102
#define SUCCESS_VSP_T2_TRACKS		 		 103
#define SUCCESS_VSP_MANUAL_PAN				 104
#define SUCCESS_VSP_T1_T2_TRACKS			 105
#define SUCCESS_VSP_T1_MANUAL_PAN 			 106
#define SUCCESS_VSP_T2_MANUAL_PAN			 107
#define SUCCESS_VSP_CARDNOT_ENCRYPTED		 200
#define SUCCESS_VSP_CARDNT_ENCRYPTED 		 201
#define SUCCESS_VSP_START_ENCRYPTN_905		 905
#define SUCCESS_VSP_DDK_ADVANCE_909			 909
#define SUCCESS_VSP_START_ENCRYPTN_935		 935

#define ERR_VSP_MISSING_DERIVATION_DATA		 326
#define ERR_VSP_KEY_DERIVATION				 327
#define ERR_VSP_MIV							 300
#define ERR_VSP_MIV_333						 333
#define ERR_VSP_MIV_334						 334
#define ERR_VSP_STRT_ENCRPTN_SRVR			 900
#define ERR_VSP_STRT_ENCRPTN_SRVR_FAILURE	 902
#define ERR_VSP_ADV_DDK_SERVER_FAILURE		 914
#define ERR_VSP_UPDATE_UNIQUE_915 			 915
#define ERR_VSP_UPDATE_UNIQUE_916 			 916
#define ERR_VSP_VCL_UPGRADE_917 			 917
#define ERR_VSP_VCL_UPGRADE_918 			 918
#define ERR_VSP_BIN_REJECT_920	 			 920
#define ERR_VSP_BIN_REJECT_921	 			 921
#define ERR_VSP_SETTING_UPDATE_923 			 923
#define ERR_VSP_SETTING_UPDATE_924 			 924
#define ERR_VSP_STRT_ENCRPTN_DEVICE_FAILURE	 936

/* PWC RESULT CODES */
#define ERR_PWC_RSLTCODE_XML_INCORRECT				-2
#define ERR_PWC_RSLTCODE_SUCCESS					-1
#define ERR_PWC_RSLTCODE_UNKNOWN					 0
#define SUCCESS_PWC_RSLTCODE_SETTLED				 2
#define SUCCESS_PWC_RSLTCODE_CAPTURED				 4
#define SUCCESS_PWC_RSLTCODE_APPROVED				 5
#define ERR_PWC_RSLTCODE_DECLINED					 6
#define SUCCESS_PWC_RSLTCODE_VOIDED					 7
#define SUCCESS_PWC_RSLTCODE_COMPLETED				10
#define SUCCESS_PWC_RSLTCODE_PARTCOMP				16
#define SUCCESS_PWC_RSLTCODE_TIPMODIFIED			17
#define SUCCESS_PWC_RSLTCODE_SETTLEMENT_SCHULD		21
#define ERR_HOST_TRAN_REVERSED						22
#define ERR_HOST_TRAN_TIMEOUT						23
#define ERR_HOST_NOT_AVAILABLE						24
#define ERR_HOST_TRANS_NOT_FOUND					26
#define ERR_HOST_TRANS_NOT_ALLOWED					27
#define SUCCESS_HOST_TRANS_VERIFIED					30
#define ERR_DEV_REG_DATA_INSUFF  					31
#define ERR_DECRYPTION_HOST_TIMEOUT					60
#define ERR_DECRYPTION_HOST_CONN_ERROR				69
#define ERR_PWC_RSLTCODE_DUP_SIG					82
#define ERR_PWC_RSLTCODE_INVLD_CARD_NUM				93
#define ERR_PWC_RSLTCODE_INVLD_EXP_DATE				97
#define ERR_PWC_RSLTCODE_INVLD_SIGNATURE			208
#define ERR_PWC_RSLTCODE_INVLD_TROUTD_FOR_SIG		3700
#define ERR_PWC_RSLTCODE_INVLD_SIG_REQUEST          9100
#define ERR_PWC_RSLTCODE_INVLD_AMT					1010
#define ERR_PWC_RSLTCODE_INVLD_USERID_USERPWD		3100
#define ERR_PWC_RSLTCODE_INVLD_TROUTD				3705
#define ERR_PWC_RSLTCODE_INVLD_REF_TROUTD			3745
#define ERR_PWC_RSLTCODE_INCORRECT_CLIENT_ID		2029999
#define ERR_PWC_RSLTCODE_INVALID_DATA_IN_REQ		2029666
#define ERR_PWC_RSLTCODE_PYMT_ENGINE_NOTACCESSIBLE	30000
#define SUCCESS_PWC_RSLTCODE_ACK					40
#define SUCCESS_SSI_RSLTCODE_LPT_FOUND              1
#define SUCCESS_SSI_RSLTCODE_LPT_NOT_FOUND          0

#define RCPT_LINE_LEN			40
#define MAX_RCPT_HEADERS		4
#define MAX_RCPT_FOOTERS		4
#define MAX_RCPT_DCLAIMERS		4
#define MAX_RCPT_DCC_DCLAIMERS	4

/* Max POS_TENDERx field that can be sent from POS */
#define MAX_POS_TENDER		3

#define SURVEY_OPTION_5     5
#define SURVEY_OPTION_10    10

#define DEVDEBUGMSG			"DEVDEBUG"

/* Application Logging #defines */

// App Entry for Transactional App Logging
#define	SCA "SCA"
#define	DHI "DHI"

#define	HOST_RC 	"RC"
#define	HOST_CP 	"CP"
#define	HOST_VANTIV "VANTIV"

// Entry Tyrp for Transactional App Logging
#define PAAS_INFO 		"INFO"
#define PAAS_ERROR 		"ERROR"
#define PAAS_FAILURE 	"FAILURE"
#define PAAS_WARNING 	"WARNING"
#define PAAS_SUCCESS 	"SUCCESS"

#define START_INDICATOR 			"******************** START ********************"
#define END_INDICATOR				"********************* END *********************"
#define SAF_START_INDICATOR 		"****************** SAF_START ******************"
#define SAF_END_INDICATOR			"******************* SAF_END *******************"

// App Entry Id for Transactional App Logging
#define START_UP					"START_UP"
#define	START						"START"
#define	COMMAND_END					"END"
#define RECEIVE						"RECEIVE"
#define SCI_REQUEST					"SCI_REQUEST"
#define PROCESSING					"PROCESSING"
#define PROCESSED					"PROCESSED"
#define SCI_RESPONSE				"SCI_RESPONSE"
#define SENT						"SENT"
#define PROCESSING					"PROCESSING"
#define SSI_REQUEST					"SSI_REQUEST"
#define SSI_RESPONSE				"SSI_RESPONSE"
#define DHI_REQUEST					"DHI_REQUEST"
#define DHI_RESPONSE				"DHI_RESPONSE"
#define RCHI_REQUEST				"RCHI_REQUEST"
#define RCHI_RESPONSE				"RCHI_RESPONSE"
#define UNSOLICITED_MESSAGE			"UNSOLICITED_MESSAGE"
#define RESPONDED					"RESPONDED"
#define DISPLAY_MESSAGE				"DISPLAY_MESSAGE"
#define DISPLAY_SCREEN				"DISPLAY_SCREEN"
#define CAPTURE_DETAILS				"CAPTURE_DETAILS"
#define DEVICE_BUSY					"DEVICE_BUSY"
#define TIMEOUT						"TIMEOUT"

/*
 * KranthiK1:
 * Defining our own signal
 * Adding SIGTIME
 */
#define	SIGUITIME		SIGRTMIN
#define	SIGSAFTIME 		SIGRTMIN+1
#define	SIG_KA_TIME 	SIGRTMIN+2	//To reset the keep alive interval timer as get the CURL Handle or whenever we receive SetTime command
#define	SIG_HOST_DOWN 	SIGRTMIN+3

/*
 * T_VinayS3:
 * Defining RCNASH Processor name
 */
#define RCNASH_PROCESSOR	"RCNASH"


/*
 *T_POLISETTYG1
 * defining the Modes for VAS DATA (APPLE PAY)
 */
#define VAS_OR_PAYMENT       "0000"
#define VAS_AND_PAYMENT      "0001"
#define VAS_ONLY             "0002"
#define PAYMENT_ONLY         "0003"



typedef enum
{
	PAAS_FALSE = 0,
	PAAS_TRUE
}
PAAS_BOOL;

typedef unsigned char 		uchar;
//typedef unsigned short 		ushort;
typedef unsigned int  		uint;
typedef unsigned long		ulong;
typedef unsigned long long	ullong;
typedef int	SOCK;

typedef enum
{
	SECURITY = 0, /* For POS Register/Unregister */
	SESSION,
	PAYMENT,
	LINE_ITEM,
	SAF,
	DEVICE,
	BATCH,
	REPORT,
	TMP_ADMIN,
	MAX_FUNC
}
FXN_TYPE_ENUM;

typedef enum
{
	PROCESSOR_CREDIT = 0,
	PROCESSOR_DEBIT,
	PROCESSOR_GIFT,
	PROCESSOR_CHECK
}
PROC_TYPES;

typedef enum
{
	PYMT_CREDIT = 0,
	PYMT_DEBIT,
	PYMT_GIFT,
	PYMT_GOOGLE,
	PYMT_ISIS,
	PYMT_CASH,
	PYMT_PRIVATE,
	PYMT_MERCHCREDIT,
	PYMT_PAYPAL,
	PYMT_EBT,
	PYMT_FSA,
	PYMT_POS_TENDER1,
	PYMT_POS_TENDER2,
	PYMT_POS_TENDER3,
	PYMT_PAYACCOUNT,
	PYMT_TOKEN,
	PYMT_CHKSALE,
	PYMT_CHKVERIFY,
	PYMT_CHECK,
	PYMT_OTHER,
	PYMT_ADMIN,
	MAX_PYMT
}
PYMT_TYPES;

typedef enum
{
	POS_CARD_DATA = 1,	/* POS Defined Tender Actions */
	POS_NOTIFICATION,
	MAX_POSTENDER_ACTIONS
}
POSTENDER_ACTIONS;

typedef enum
{
	EBT_FOODSNAP = 0,
	EBT_CASHBENEFITS,
	MAX_EBT_TYPES,
}
EBT_TYPES;

typedef enum
{
	RC_HOST = 1,
	VANTIV_HOST,
	CP_HOST,
	MAX_HOST_TYPES,
}
HOST_TYPES;

typedef enum
{
	PYMT_SUBTYPE_INTERNAL = 0,
	PYMT_SUBTYPE_EXTERNAL,
	PYMT_SUBTYPE_GIFTMALL,
	PYMT_SUBTYPE_INCOMM,
	PYMT_SUBTYPE_BLACKHAWK,
	MAX_SUBPYMT
}
PYMT_SUBTYPES;


/* @deprecated : do not use TODO remove this definition */
typedef enum
{
	TEND_CREDIT = 0,
	TEND_DEBIT,
	TEND_GIFT,
	TEND_GOOGLE,
	TEND_ISIS,
	TEND_BALANCE,
	TEND_PRIVATE,
	TEND_MERCHCREDIT,
	TEND_PAYPAL,
	TEND_EBT,
	TEND_FSA,
	TEND_POS_TENDER1,		/* POS Defined Tender */	/* Please change iPOSTndrNum, in case if you are adding any New Tender Type before "TEND_POS_TENDER1" */
	TEND_POS_TENDER2,		/* POS Defined Tender */
	TEND_POS_TENDER3,		/* POS Defined Tender */
	TEND_OTHER,
	MAX_TENDERS
}
TENDER_TYPES;

typedef enum
{
	CONSUMER_OPT1 = 0,
	CONSUMER_OPT2,
	CONSUMER_OPT3,
	CONSUMER_OPT4,
	CONSUMER_OPT5,
	CONSUMER_OPT6,
	MAX_CONSUMER_OPTS
}
CONSUMER_OPTIONS;

typedef enum
{
	GIFTRECEIPT_OPT = 0,
	EMAILRECEIPT_OPT,
	EMAILOFFER_OPT,
	PRIVCARD_OPT,
	CANCELPRESWIPE_OPT,
	MAX_OPTION_VALUES
}
CONSUMER_OPTION_VALUES;

typedef enum
{
	AMT_POSITIVE,
	AMT_NEGATIVE
}
AMT_SIGN;

typedef enum
{
	TRAN_START = 1,
	TRAN_BUSY,
	TRAN_COMPLETE,
	TRAN_ABORTED,
	TRAN_FINISH
}
TRAN_PROC_STATUS;


typedef enum
{
	CANCELLED = 0,					//0
	REBOOTING = 0,					//0
	NOT_ALLOWED = 1,				//1
	NO_TRAN,						//2
	NO_SESSION_IDLE = 10,			//10
	IN_SESSION_IDLE,				//11
	IN_LINEITEM,					//12
	CAPTURING_PAYMENT,				//13
	POSTING_PAYMENT,				//14
	POSTING_REPORT,					//15
	CAPTURING_VOID,					//16
	POSTING_VOID,					//17
	CAPTURING_REFUND,				//18
	POSTING_REFUND,					//19
	CAPTURING_GIFT,					//20
	POSTING_GIFT,					//21
	REGISTER,						//22
	SAF_QUERY,						//23
	SAF_REMOVAL,					//24
	CAPTURING_DEV_MAIL,				//25
	CAPTURING_DEV_CUST_QUESTION,	//26
	CAPTURING_DEV_CUST_SURVEY,		//27
	CAPTURING_DEV_LOYALTY,			//28
	CAPTURING_DEV_SIGNATURE,		//29
	CAPTURING_DEV_CHARITY,			//30
	CAPTURING_DEV_VERSION,			//31
	CAPTURING_DEV_CUST_BUTTON,		//32
	LANE_CLOSED,                    //33
	IN_SESSION_WELCOME,             //34
	CAPTURING_CREDIT_APP,			//35
	CAPTURING_PAYPAL_PAYMENT_CODE,	//36
	CAPTURING_EBT_BALANCE,			//37
	CAPTURING_PRIV_BALANCE,			//38
	POSTING_EBT_BALANCE,			//39
	POSTING_PRIV_BALANCE,			//40
	TRANSACTION_COMPLETED,			//41
	PROCESSING_SET_PARM,			//42
	PROCESSING_GET_PARM,			//43	/*KranthiK1: Please add any new state in the isCancelRebootAllowed Function too*/
	PROCESSING_VHQ_UPDATES,			//44
	PROCESSING_PROVISION_PASS,		//45
	PROCESSING_DISPLAY_MESSAGE,		//46
	PROCESSING_GET_CARD_DATA,		//47
	DISPLAYING_QRCODE_SCREEN,		//48
	IN_LINEITEM_QRCODE,				//49
	CAPTURING_DEV_CHECKBOX			//50
}
APPLN_STATE;

typedef enum
{
	NO_STATE = -1,
	DETAILED_TRANSACTION_COMPLETED = 0,
	PROCESSING_PLEASE_WAIT, //This is equivalent to posting payment POSTING_PAYMENT in secondary data status.
	CAPTURING_CARD_DETAILS,
	CAPTURING_PIN_DETAILS,
	CAPTURING_SPLIT_TENDER_AMOUNT,
	CAPTURING_TENDER_TYPE,
	CAPTURING_CASH_BACK,
	CAPTURING_ACCOUNT_NUM,
	CAPTURING_ZIP_CODE = 10,
	CAPTURING_CASHBACK_OTHER,
	IN_STATE_LINEITEM,
}
APPLN_DETAILED_STATE;

typedef enum
{
	DEMO_ON = 1
}
DEMO_ENUM;

/* STATE ID CONSTANTS FOR CAPTURE CARD DATA FLOW*/
typedef enum
{
	PRE_PROCESSING,
	CAPTURE_EMV_CARD_DATA,
	CAPTURE_NON_EMV_CARD_DATA,
	CAPTURE_MANUAL_CARD_DATA,
	CAPTURE_PAYPAL_CARD_DATA,
	VALIDATE_CARD_DATA,
	UPDATE_CARD_DATA,
	ENCRYPT_ACCT_NUM,
	CAPTURE_CARD_DATA_END
}
CARD_STATE;

typedef enum
{
	HOST_PROTOCOL_SSI,
	HOST_PROTOCOL_UGP
}
HOSTPROTOCOL_TYPE;

typedef enum
{
	DATA_EMPTY = 0,
	DATA_PRESENT,
	DATA_INQUEUE,
	DATA_SEND_ERROR_ON_PRESWIPE,
	DATA_SENT_ON_PRESWIPE,
	DATA_USED
}
EARLY_CARD_STATUS;

typedef enum
{
	CHECKBOX1 = 0,
	CHECKBOX2,
	CHECKBOX3,
	CHECKBOX4,
	CHECKBOX5,
	CHECKBOX6,
	MAX_CHECKBOX
}
CHECKBOX_LIST;

//#define    KEY_INDEX_0                      1<<0
#define    KEY_INDEX_1                      1<<0
#define    KEY_INDEX_2                      1<<1
#define    KEY_INDEX_3                      1<<2
#define    KEY_INDEX_4                      1<<3
#define    KEY_INDEX_5                      1<<4
#define    KEY_INDEX_6                      1<<5
#define    KEY_INDEX_7                      1<<6
#define    KEY_INDEX_8                      1<<7
//#define    KEY_INDEX_9                      1<<8

#define MEMDEBUG 1
extern void *scaMalloc(unsigned int, int, char* );
extern void *scaReAlloc(void *, unsigned int, int, char*);
extern void scaFree(void **, int, char*);

#endif
