#ifndef __TRANDEF_H
#define __TRANDEF_H

#include "common/common.h"
#include "common/tranCommon.h"
#include "common/metaData.h"
#include "db/cdtExtern.h"

#include "uiAgent/uiControl.h"	//ArjunU1: EMV Module testing
#include "uiAgent/emv.h"

#define CREDIT_APP_MAXPROMPTS 10

typedef enum
{
	PROMPTTYPE_QWERTY	= 0,
	PROMPTTYPE_NUMERICS,
	PROMPTTYPE_DOLLAR,
	PROMPTTYPE_QWERTY_SWIPE
}
PROMPT_TYPE;

/* T_RaghavendranR1 : Earlier the MaskType was Boolean TRUE/FALSE, We have now added the TRAILING Masking type where the test is shown for a second before it gets masked.
	Hence to provide backward compatibility, we now have a list of masktype, with TRUE/1/YES/T, FALSE/0/NO/F and TRAILING
	TRUE/1/YES/T is full masking of user entered data and FALSE/0/NO/F is No Masking and TRAILING is data masking with Delay*/
typedef enum
{
	PROMPTMASK_0	= 0,
	PROMPTMASK_FALSE,
	PROMPTMASK_NO,
	PROMPTMASK_F,
	PROMPTMASK_1,
	PROMPTMASK_TRUE,
	PROMPTMASK_YES,
	PROMPTMASK_T,
	PROMPTMASK_TRAILING
}
PROMPT_MASK;

typedef enum
{
	DCC_ELIGIBLE_USER_OPTED	= 1,
	DCC_NOT_ELIGIBLE,
	DCC_ELIGIBLE_USER_NOT_OPTED
}
DCC_IND_TYPE;

typedef enum
{
	RET_SCREEN_IDLE	= 1,
	RET_SCREEN_LI,
	RET_SCREEN_WELCOME,
	RET_SCREEN_STAY_CURRENT
}
RET_SCREEN;

typedef enum
{
	NOT_TAX	= 0,
	TAX_E_ZERO,
	TAX_NE_ZERO,
	TAX_EXEMPT,
}
TAX_IND;

typedef enum
{
	BUSINESS = 'B',
	CORPORATE = 'C',
	PURCHASE = 'P'
}
CMRCL_FLG;

typedef enum
{
	PERSONAL_CHK = 0,
	BUSINESS_CHK,
	PAYROLL_CHK,
	GOVT_CHK,
	MAX_CHKTYPE
}
CHECK_TYPES;

typedef enum
{
	CREDIT_APP_PROMPT_1 = 0,
	CREDIT_APP_PROMPT_2,
	CREDIT_APP_PROMPT_3,
	CREDIT_APP_PROMPT_4,
	CREDIT_APP_PROMPT_5,
	CREDIT_APP_PROMPT_6,
	CREDIT_APP_PROMPT_7,
	CREDIT_APP_PROMPT_8,
	CREDIT_APP_PROMPT_9,
	CREDIT_APP_PROMPT_10
}
CREDIT_APP_PROMPT_TYPES;

typedef enum
{
	MANUAL_PAN_PROMPT = 1,
	MANUAL_EXP_PROMPT,
	MANUAL_CVV_PROMPT,
	MANUAL_ZIP_PROMPT
}
MAN_PROMPT_OPTIONS;

/* General response details */
typedef struct
{
	char	szTermStat[40];
	char	szRsltCode[10];
	char	szRslt[80];
	char	szRespTxt[256]; 	//In some cases we do get large response text
	char	szHostRsltCode[50];
	char	szRespCode[50];
	char	szTranCntr[11];		//This is counter value which needs to be sent to POS.
}
RESPDTLS_STYPE, * RESPDTLS_PTYPE;

/* Other General response details */
typedef struct
{
	char	szPymntType[20];		/* Payment Type*/
	char 	szTranAmt[20];			/* TRANS_AMOUNT */
	char	szEBTType[17];			/* EBT Type (16 + 1) */
	char	szEmbossedPAN[30 + 1];	/* Embossed Acct Num (30 + 1) */
	char	szPaymentMedia[20 + 1];	/* Payment Media (20 + 1) */
	char	szCardHolder[30 + 1];	/* Card Holder Name (30 + 1) */
	char	szPAN[30 + 1];
	char	szClrMon[3];
	char	szClrYear[3];
	char	szCardEntryMode[25 + 1];
	char	szCardAbbrv[3];
}
OTHER_RESPDTLS_STYPE, * OTHER_RESPDTLS_PTYPE;

/* Tax related details */
typedef struct
{
	char		taxAmt[12+1];				/* TAX_AMOUNT */
	TAX_IND		taxInd;						/* TAX_IND */
	CMRCL_FLG	cmrclFlag;					/* CMRCL_FLAG */
	char		discAmt[12+1];				/* DISCOUNT_AMOUNT */
	char		freightAmt[12+1];			/* FREIGHT_AMOUNT */
	char		dutyAmt[12+1];				/* DUTY_AMOUNT */
	char		shipPostalcode[9+1];  		/* SHIP_FROM_ZIP_CODE */
	char 		prodDesc[40+1];				/* RETAIL_ITEM_DESC_1 */
	char		destPostalcode[9+1];		/* DEST_POSTAL_CODE */
	char		alternateTaxId[15+1];		/* ALT_TAX_ID */
	char		destCountryCode[3+1];		/* DEST_COUNTRY_CODE */
	char		customerCode[25+1];			/* CUSTOMER_CODE */
}
LVL2_STYPE, * LVL2_PTYPE;

/* Loyalty Details */
typedef struct
{
	char	szPAN[21];
	char	szPh[21];
	char	szEmail[41];
	char	szToken[20];
	char	szRewardTxt[257];
	char	szRewardId[17];
	char	szRewardAmt[20];
	char	szInputType[21];
}
LTYDTLS_STYPE, * LTYDTLS_PTYPE;

/* VSP related info - see vsp documentation */
typedef struct
{
	char	szTrxId[30];			/* VSP_TRXID */
	char	szCode[10];				/* VSP_CODE */
	char	szResultDesc[50];		/* VSP_RESULTDESC */
}
VSPDTLS_STYPE, * VSPDTLS_PTYPE;

/* Signature related details */
typedef struct
{
	char	szMime[16];
	char *	szSign;
	char 	szDispTxt[5000];
}
SIGDTLS_STYPE, * SIGDTLS_PTYPE;

/* Time related details */
typedef struct
{
	char 	szTimeTxt[16];
}
TIMEDTLS_STYPE, * TIMEDTLS_PTYPE;

/* Version related details */
typedef struct
{
	char 	szVersionInfo[200];
	int		iDisplayFlag;
}
VERDTLS_STYPE, * VERDTLS_PTYPE;

/* Survey Related Details */
typedef struct
{
	char 	szDispTxt1[51];
	char 	szDispTxt2[51];
	char 	szDispTxt3[51];
	char 	szDispTxt4[51];
	char 	szDispTxt5[51];
	char 	szDispTxt6[51];
	char 	szDispTxt7[51];
	char 	szDispTxt8[51];
	char 	szDispTxt9[51];
	char 	szDispTxt10[51];
	char 	szDispBulkTxt[4100];
	char 	szResultData[10];
	int 	iRetScreen;
}
SURVEYDTLS_STYPE, * SURVEYDTLS_PTYPE;

/* Charity Related Details */
typedef struct
{
	char 	szDispTxt1[51];
	char 	szDispTxt2[51];
	char 	szDispTxt3[51];
	char 	szDispTxt4[51];
	char 	szDispTxt5[51];
	char 	szAmount1[11];
	char 	szAmount2[11];
	char 	szAmount3[11];
	char 	szAmount4[11];
	char 	szAmount5[11];
	char 	szResultData[10];
}
CHARITYDTLS_STYPE, * CHARITYDTLS_PTYPE;

/* Customer Button Related Details */
typedef struct
{
	char 	szDispTxt1[46];
	char 	szDispTxt2[46];
	char 	szDispTxt3[46];
	char 	szDispTxt4[46];
	char 	szDispTxt5[46];
	char 	szButtonLabel1[16];
	char 	szButtonLabel2[16];
	char 	szButtonLabel3[16];
	char 	szButtonLabel4[16];
	char 	szButtonLabel5[16];
	char 	szButtonLabel6[16];
	char 	szResultData[10];
}
CUSTBUTTONDTLS_STYPE, *CUSTBUTTONDTLS_PTYPE;

/* CREDIT APPLICATION Related Details */
typedef struct
{
	int			iTitle;
	int 		iPromptMinChars;
	int 		iPromptMaxChars;
	int	 		iPromptMaskType;
	char 		szPromptText[300+1];
	char 		szPromptData[100+1];
	char 		szPromptDataWithLiterals[100+1];
	char 		iPromptType;
	char 		szPromptFormat[100+1];
}
PROMPTDTLS_STYPE, *PROMPTDTLS_PTYPE;

typedef struct
{
	int 				iProxyTimeout;
	int					iRetScreen;
	char 				szForwardProxy[100];
	char 				szProxyRef[15];
	char				szProxyResult[15];
	char				szCreditAppPayLoad[101];
	PROMPTDTLS_STYPE	stPromptDtls[CREDIT_APP_MAXPROMPTS];
}
CREDITAPPDTLS_STYPE, *CREDITAPPDTLS_PTYPE;

/* Counter Value related Details */
typedef struct
{
	char	szMacLabel[11];
//	char	szCounterVal[11];
}
COUNTERDTLS_STYPE, *COUNTERDTLS_PTYPE;

/* Device Name related Details */
typedef struct
{
	char	szDeviceName[21];
	char	szMacLabels[256];
	char	szSerialNum[15];
}
DEVICENAME_STYPE, *DEVICENAME_PTYPE;

/* Lane Closed related Details */
typedef struct
{
	char	szDisplayText[41];
	char	szFontColor[7];
	int		iFontSize;
}
LANECLOSED_STYPE, *LANECLOSED_PTYPE;

/* Employee ID related Details */
typedef struct
{
	char	szEmpID[21]; //currently upto 20 we are supporting
}
EMPID_STYPE, *EMPID_PTYPE;

/* Payment Types related Details */
typedef struct
{
	char	szPaymentTypes[100]; //currently upto 100 we are supporting
}
PAYMENTTYPES_STYPE, *PAYMENTTYPES_PTYPE;

/* Set Parameter Related Details */
typedef struct
{
	char	szMID[20+1];
	char	szTID[20+1];
	char	szLane[20+1];
	char	szAdminURL[80+1];
	char	szPrimURL[80+1];
	char	szScndURL[80+1];
	char	szAltMerchId[20+1];
	char	szTimeZone[20+1];
	int		iHostType;
}
SETPARM_STYPE, *SETPARM_PTYPE;

/* Display Message Box Details */
typedef struct
{
	char	cIcon;
	char	cButton;
	int		iTimeOut;
	char 	szDispTxt[150+1];
	char	szTimeOutResp[50+1];
}
DISPMSG_STYPE, * DISPMSG_PTYPE;

/* Apply Updates Details */
typedef struct
{
	PAAS_BOOL	bUpdateFlag;
}
APPLYUPDATES_STYPE, *APPLYUPDATES_PTYPE;

/* Set Parameter Related Details */
typedef struct
{
	char	szGetParmReq[100+1];
	char	szGetParmResp[512+1];
}
GETPARM_STYPE, *GETPARM_PTYPE;

/* Provision Pass Related Details */
typedef struct
{
	char	szMerchIndex[10+1];
	char	szCustData[20+1];
}
PROVISION_PASS_STYPE, *PROVISION_PASS_PTYPE;

/*NFC INI file related details*/
typedef struct __applepaysection
{
	char	szSectionName[50+1];
	char	szMerchIndex[10+1];
	char	szMerchID[100+1];
	char	szMerchURL[100+1];
	char	szMerchVASFilter[50+1];
	struct __applepaysection * next;
}
NFC_APPLE_SECT_DTLS_STYPE, *NFC_APPLE_SECT_DTLS_PTYPE;

/* Structure for NFC INI Contents*/
typedef struct
{
	char						szApplePayNumSections[10];
	NFC_APPLE_SECT_DTLS_PTYPE 	pstApplePaySectionList;
}
NFCINI_CONTENT_INFO_STYPE, *NFCINI_CONTENT_INFO_PTYPE;

/* Get Card Data Related Details */
typedef struct
{
	PAAS_BOOL	tranAmt;
	char 		szDispTxt1[51];
	char 		szDispTxt2[51];
	char 		szDispTxt3[51];
	char 		szDispTxt4[51];
	char 		szDispTxt5[51];
	char		szInputMethod[16];
	char    	szTrack1Data[512];
	char    	szTrack2Data[512];
	char    	szTrack3Data[512];
	char    	szClrTrk1[100];
	char    	szClrTrk2[100];
	char    	szPAN[512];
	char    	szClrMon[3];
	char    	szClrYear[3];
	char    	szName[30];
	int 		iCardEntryMode;

}
GET_CARDDATA_STYPE, *GET_CARDDATA_PTYPE;
/* PIN details */
typedef struct
{
	char	szPIN[100];			/* PIN BLOCK */
	char	szKSN[20+1];
}
PINDTLS_STYPE, * PINDTLS_PTYPE;

/* Amount Details */
typedef struct
{
	char	tranAmt[20];			/* TRANS_AMOUNT */
	char	origTranAmt[20];		/* Original TRANS_AMOUNT Received From POS, Since tranAmt may change for EBT and FSA*/
	char	origTranAmtFrmHost[20];	/* Original TRANS_AMOUNT Sent From Host, Need to Send Back to POS*/
	char	tipAmt[20];				/* TIP_AMOUNT */
	char	cashBackAmtFromPOS[20];	/* CASHBACK_AMNT From POS Honored in SCI Request*/
	char	cashBackAmt[20];		/* CASHBACK_AMNT */
	char	apprvdAmt[20];			/* APPROVED_AMOUNT */
	char	availBal[20];			/* AVAILABLE_BALANCE */
	char	dueamount[20];      	/* DIFFERENCE_AMOUNT_DUE */
	char 	diffAuthAmt[20];		/* Difference Auth Amount*/
	char	previousBal[20];		/* Previous Balance */
	char 	EBTCashBenefitsBal[20];	/* EBT Cash Benefits Balance Amount*/
	char 	EBTFoodSNAPBal[20];		/* EBT Food/SNAP Balance Amount*/
	char	tranAmtFirstC30[20];	/* Transaction Amount send in 1st C30, used for Kernel Switching*/
}
AMTDTLS_STYPE, * AMTDTLS_PTYPE;

/* Check Details */
typedef struct
{
	char szMICR[60];
	char szAcctNum[30];
	char szABANum[30];
	char szChkNum[30];
	char szDLState[30];
	char szDLNumber[30];
	int	 iChkType;
}
CHKDTLS_STYPE, * CHKDTLS_PTYPE;

/* Customer Info Details */
typedef struct
{
	char szCustStreetName[60];
	char szCustZipNum[30];
	char szPosCustZipNum[30]; //Praveen_P1: This is replicate of szCustZipNum to pre-populate the prompt on the terminal
	char szCustData[20+1]; 		//Apple Pay Provision Pass
	char szMerchantIndex[10+1];
	char szNFCVASMode[4+1];		// VAS Terminal Mode
}
CUSTINFODTLS_STYPE, * CUSTINFODTLS_PTYPE;

/* Follow on Transaction general details */
typedef struct
{
	char	szTroutd[11];
	char	szCTroutd[26];
	char	szAuthCode[17];
	char	szLPToken[22];
	char	szCardToken[41];
	char	szBankUserData[41];
	char	szReference[41];
	char	szTokenSrc[11];
	char 	szSAFApprovalCode[17];
}
FTRANDTLS_STYPE, * FTRANDTLS_PTYPE;

/* Current transaction general details */
typedef struct
{
	char	szTranSeq[10];			/* TRANS_SEQ_NUM */
	char	szTranId[16];			/* INTRN_SEQ_NUM */
	char	szAuthCode[17];			/* AUTH_CODE (16 + 1) */
	char	szTroutd[11];			/* TROUTD */
	char	szCTroutd[26];			/* CTROUTD (25 + 1) */
	char	szLPToken[22];			/* LPTOKEN */
	char	szPymntType[20];		/* Payment Type*/
	char	szSAFNo[20];			/* SAF Number */
	char	szSAFTORNo[20];			/* SAF TOR Number */
	char	szCardToken[41];    	/* Card Token */
//	char 	szTokenSource[20];  	/* Token Source */
	char	szTraceNum[30];     	/* Trace Number */
	char 	szRtnCheckFee[20];  	/* Return Check Fee */
	char	szRtnCheckNote[100]; 	/* Return Check Note */
	int		iAckRequired;			/* ACK Required flag */
	char	szCheckNum[20];     	/* Check Number */
	char	szBankUserData[51]; 	/* Bank User Data */
	char	szAuthRespCode[20]; 	/* Auth Response Code */
	char 	szTxnPosEntryMode[4];	/* Transaction POS Entry Mode */
	char 	szMerchId[30];			/* Merchant ID */
	char 	szTermId[16];			/* Terminal ID */
	char 	szLaneId[16];			/* LANE ID */
	char 	szStoreId[16];			/* LANE ID */
	char 	szApprvdAmt[20];		/* APPROVED_AMOUNT */
	char 	szPaypalCheckIDKey[50];	/* Paypal Checkin Key Id */
	char	szBatchTraceId[50];		/* Batch Trace Id for SSI*/
	char	szTransDate[15];
	char	szTransTime[15];
	char	szAvsCode[3];			/* AVS_CODE */
	char 	szCmrclFlag[5];
	char	szProcessor[20];
	char	szPPCV[20];
	char	szCreditPlanNbr[10];
	char	szReferenceNum[16];
	char	szValidtionCode[5];
	char	szVisaIdentifier[7];
	char	szDenialRecNum[10];
	char	szSignatureRef[12];
	char 	szAprType[2];				/* APR_TYPE */
	char	szPurchaseApr[13];			/* PURCHASE_APR */
	char	szSvcPhone[11];				/* SVC_PHONE */
	char 	szStatusFlag[2];			/* STATUS_FLAG */
	char	szReceiptText[4096];		/* RECEIPT_TEXT */
	char	szActionCode[3];			/* ACTION_CODE */
	char 	szReference[40+1];			/* REFERENCE - This could return on any transaction */
	char	szAthNtwID[4];
}
CTRANDTLS_STYPE, * CTRANDTLS_PTYPE;

/* Duplicate field details */
typedef struct
{
	char	szDupAuthCode[17];		/* AUTH_CODE (16 + 1) */
	char	szDupTroutd[11];		/* TROUTD */
	char	szDupCTroutd[11];		/* CTROUTD (10 + 1) */
	char	szDupAccNum[20];		/* ACCOUNT NUMBER */
	char	szDupPymtMedia[21];     /* -> card label also */
	char    szDupInvoice[11];        /* Invoice */
	char	szDupTranAmount[20];    /* TRANS_AMOUNT */
	char	szDupTransDate[11];     /* TRANSACTION DATE */
	char	szDupTransTime[11];     /* TRANSACTION TIME */
	char	szDupAVSCode[3];     	/* AVS Code*/
	char	szDupCVV2[5];    		/* CVV2 Value */
}
DUPFIELDDTLS_STYPE, *DUPFIELDDTLS_PTYPE;

/* EBT field details */
typedef struct
{
	char			szEBTType[17];			/* EBT Type (16 + 1) */
	char			szEBTCashElig[20];		/* Eligibility for EBT Cash */
	char			szEBTSNAPElig[20];		/* Eligibility for EBT SNAP */
	char			szEBTCashBack[20];		/* EBT Cash Back Amount  */
}
EBTDTLS_STYPE, *EBTDTLS_PTYPE;

/* FSA field details */
typedef struct
{
	char			szAmtHealthCare[20];	/* FSA Amount Health Care */
	char			szAmtPres[20];			/* FSA Amount Prescription */
	char			szAmtVision[20];		/* FSA Amount Vision */
	char			szAmtClinic[20];		/* FSA Amount Clinic  */
	char			szAmtDental[20];		/* FSA Amount Dental  */
}
FSADTLS_STYPE, *FSADTLS_PTYPE;

/* Line Item Information */
typedef struct
{
	int				iItemCnt;			/* Count of merchandise */
	int				iOfferCnt;			/* Count of offers */
	char			runTaxAmt[11];		/* RUNNING_TAX_AMOUNT (10 + 1) */
	char			runTranAmt[11];		/* RUNNING_TRANS_AMOUNT (10 + 1) */
	char			runSubTtlAmt[11];    /* RUNNING_SUB_TOTAL (10 + 1) */
	LI_NODE_PTYPE	mainLstHead;
	LI_NODE_PTYPE	mainLstTail;
}
LIINFO_STYPE, * LIINFO_PTYPE;

/*Akshaya :21-07-16 . Apple Pay :Ignoring the Implementation if having NFCVAS_MODE and MERCHANT_INDEX are sent in SHOW Line Item Command.*/
#if 0
/* Show Line Item Information*/
typedef struct
{
	char	szNFCVASMode[4+1];			// VAS Terminal Mode
	char 	szMerchantIndex[11];		// Merchant Index
}
SHWLIINFO_STYPE, *SHWLIINFO_PTYPE;
#endif
/* Consumer Option Data */
typedef struct
{
	int	iEmailReceipt;
	int	iGiftReceipt;
	int	iEmailOffer;
	int iPreSwiped;
	int iVasLoyalty;
	int iPayWithPaypal;
	int iCancelPressed;
	int iCardDtlCaptured;
	int iTransInFlight;
	char szEmail[41];
	char szCardToken[41];
	char szTokenSource[20];
	char szPubliserName[25];
	char szVASLoyaltyData[256];
	char szPOSIP[20];
	char szPOSPort[5];
	RAW_CARDDTLS_PTYPE	pstRawCrdDtls;	// MukeshS3: 9-Feb-16: Added to support FRD 3.75.1
}
UNSOLMSGINFO_STYPE, *UNSOLMSGINFO_PTYPE;

/*Basket payment node information*/
typedef struct
{
	int 				count;
	char				szShopToken[21];
	PYMNT_NODE_PTYPE	mainLstHead;
	PYMNT_NODE_PTYPE	mainLstTail;
}
PYMNTINFO_STYPE, * PYMNTINFO_PTYPE;

typedef struct
{
	char				 		szAuthRespCode[5];			//8A
	char						szEMVRespTags[2048];		// All emv tags
	int 						iAuthRespCodeLen;
	int							iEMVRespTagsLen;
	PAAS_BOOL					bEmvUpdateReq;
}
EMV_RESP_DTLS_STYPE, * EMV_RESP_DTLS_PTYPE;	/* DONE */

//Daivik:8/7/16- Structure added to store Backup Card Details.
typedef struct
{
char ** pszTrackDtlsBkup;
char ** pszRSATrackDtlsBkup;
char ** pszVSDBkup;
}BACKUP_CARDDTLS_STYPE, * BACKUP_CARDDTLS_PTYPE;

/* QR Code Details */
typedef struct
{
	char	szQRCodeData[200+1];
	char	szDispText[150+1];
	char	szBtnLabel[18+1];
	PAAS_BOOL	bLIScreen;
}
DISPQRCODE_DTLS_STYPE, *DISPQRCODE_DTLS_PTYPE;

/*CheckBox Details*/
typedef struct
{
	char	szTitletext[201];
	char	szCheckboxLabels[6][45];
	char	szRespCheckBox[6][2];
}
CUSTCHECKBOX_DTLS_STYPE, *CUSTCHECKBOX_DTLS_PTYPE;

/* Session Details */
typedef struct
{
	/* Daivik:25/1/2016 Coverity 67197, the gettime function, coverts the time to required format and places in given string.
	 * The maximum length that we have provided in strftime is 25 bytes and hence our string must be of a minimum length of 25 bytes.
	 */
	char				szStartTS[30];					/* HHMMSS format */
	char				szCloseTS[9];					/* HHMMSS format */
	char				szRcptTS[20];					/* YYYY-MM-DDThh:mm:ss */
	char				szPOSId[11];					/* Will contain the MAC label */
	char				szInvoice[7];					/* INVOICE (6 + 1)*/
	char				szStoreNo[10];					/* STORE_NUM */
	char				szLaneNo[21];					/* LANE (20 + 1) */
	char				szCashierId[11];				/* CASHIER_ID (10 + 1) */
	char				szServerId[4];					/* SERVER_ID (3 + 1) */
	char				szShiftId[2];					/* SHIFT_ID (1 + 1) */
	char				szTableNo[6];					/* TABLE_NUM (5 + 1) */
	char				szPurchaseId[26];				/* PURCHASE_ID (25 + 1) */
	char				sztranAmtFirstinC30[20];	/* Transaction Amount send in 1st C30, used for Kernel Switching, Storing here for Dup Swipe case*/
	char 				szBusinessDate[9];   			/* BUSINESSDATE(Format is CCYYMMDD) (8+1) */
	char 				szMerchantIndex[11];			/* MERCHANT_INDEX (10 +1)*/
	char				szNFCVASMode[4+1];				/* NFCVAS_MODE (4+1) */
	time_t				startTime;						/* Added time structure to calculate duration of the session*/
	int					iEncFlag;						/* Variable to hold whether the session is in Encrypted mode */
	int					iSwipeAheadFlag;    			/* Variable to enable/disable swipe-ahead/pre-swipe in run time */
	int					iDemoModeFlag;	  			  	/* Variable to enable/disable Training mode at run time */
	int					iPaymentType;					/* To store payment type for duplicate transaction */
	int					iEMVCrdPresence;				/* Variable to hold whether EMV Chip card presence on pre-swipe in this session*/
	int					iDefLangBeforeEMV;				/*Default Lang holder before getting EMV lang change */
	int					iPostAuthPymtType;		/*Stores the Payment Type of Post Auth Transaction, Can be GIFT/CREDIT*/
	int					iBalanceTranPymtType;			/*Stores the Payment Type of Previous Balance Transaction, Can be GIFT/MERCH_CREDIT/PRIV_LBL*/
	int					iBalanceTranPymtSubType;		/*Stores the Payment Sub Type of Previous Balance Transaction*/
	EARLY_CARD_STATUS	iEarlyCardDtlsStatus;			/* To determine current status of early captured card details present in session */
	PAAS_BOOL			isBAPIReq;						/* Will set this to true if atleast one payment is done or atleast one line item is added*/
	PAAS_BOOL			bSwipeAheadConfigVal; 			/* Variable to hold the current config value of preswipeenabled parameter */
	PAAS_BOOL			bDemoModeConfigVal;  		 	/* Variable to hold the current config value of demomode parameter */
	PAAS_BOOL			bPreSwipeDone; 					/* Variable to hold whether pre-swipe is done in this session or not */
	PAAS_BOOL			bInFallbackMode; 				/* Variable to hold whether Fallback Mode is there or not on Preswipe */
	PAAS_BOOL			bCardDataInBQueue;				/* Flag to hold whether card data is present in blogic queue & not yet processed from processCarddata thread */
	
	PAAS_BOOL			bScreenChangereqd;				/* Variable to indicate whether we need to move to line item/welcome screen when START SESSION command comes */
	LIINFO_PTYPE		pstLIInfo;
	PYMNTINFO_PTYPE 	pstPYMNTInfo;					/* Payment structure to track the details that can be useful for basket*/
	UNSOLMSGINFO_STYPE 	stUnsolMsgInfo;      			/* Consumer Option Details which will be captured during line
												   	   	   items and used later to send in the response */
	CARDDTLS_STYPE		stCardDtls;        				/* To store the pre-swipe/tap card details */
	CARDDTLS_PTYPE		pstDupSwipeCardDtls; 			/* To store the duplicate swipe card details */
	CARDDTLS_PTYPE		pstPostAuthTransCardDtls; 		/* To store the card details for next trans as we wont prompt for card dtls again, if next sale sent with Auth Code*/
	CARDDTLS_PTYPE		pstPrevGiftTranCardDtls;		 /* To store the gift card details for next trans as we wont prompt for card dtls again, if next CashOut command*/
	VASDATA_DTLS_PTYPE	pstVasDataDtls;					/* To Store Apple Pay Loyalty data in session*/
	BACKUP_CARDDTLS_STYPE stBkupDtls;
//	SHWLIINFO_PTYPE		pstShowLIInfo;
	DISPQRCODE_DTLS_PTYPE pstQRCodeDtls;				/* To store QR code details received in DISPLAY_QRCODE command */
}
SESSDTLS_STYPE, * SESSDTLS_PTYPE;

/* POS Defined Tender Details */
typedef struct
{
	int  iPOSTenderAction[3];	//Max 3 tenders can be sent from the POS
	char szPOSTender[3][31];
	char szPOSTndrData[3][101];
}
POSTENDERDTLS_STYPE, * POSTENDERDTLS_PTYPE;


/* Pass Through fields Details */
typedef struct
{
	int						iReqTagsCnt;
	int						iResTagsCnt;
	KEYVAL_PTYPE			pstReqTagList;
	KEYVAL_PTYPE			pstResTagList;
}
PASSTHRG_FIELDS_STYPE, *PASSTHRG_FIELDS_PTYPE;

/* Pass Through fields Details */
typedef struct
{
	int	 iDCCInd;
	int	 iMinorUnits;
	char szCardID[6 + 1];
	char szTranAmt[20 + 1];
	char szExchngRate[20 + 1];
	char szFgnCurCode[3 + 1];
	char szFgnAmount[20 + 1];
	char szMarginRatePercentage[10 + 1];
	char szExchangeRateSouceName[35 + 1];
	char szCommissionPercentage[10 + 1];
	char szAlphaCurrCode[10 + 1];
	char szCurrencySymbol[5];
	PAAS_BOOL isDccEligible;
}
DCCDTLS_STYPE, *DCCDTLS_PTYPE;

/* Payment Transaction Details */
typedef struct
{
	int							iPymtType;
	int							iPymtSubType;
	char						szCardType[10];

	char						szTranDt[9];			/* DDMMYYYY format */
	char						szTranTm[7];			/* HHMMSS format */
	
	char						szOrigTranDt[11];		/* YYYY.MM.SS format */
	char						szOrigTranTm[9];		/* HH:MM:SS format */
	char						szOrderDtTm[21];		/* MM/DD/YYYY HH:MM:SS */
	char						szAllowDupTran[7];		/* To store invoice value for allowing dup tran*/
	char						szPANHashValue[41];		/* Hash Value of the current transaction PAN */
	char						szManPrmptOptns[50];  	/* Manual Entry Prompts Options.E.g ZIP|PAN|CVV|EXP */
	char						szBatchTraceId[50+1]; 	/* Batch Trace Id For Transactions*/
	char						szCheckIdKey[50];   	/* CheckId key For Paypal Transactions*/
	char						szPaymentTypes[100];   	/* PaymentTypes to be Set for that Transaction Sent from POS*/
	char						szPromoCode[11];		/* Promo Code for Private Label, Gift Transactions */
	char						szCreditPlanNBR[6];		/* Credit Plan NBR for Private Label Transactions */
	char						szPurchaseApr[13];		/* PURCHASE_APR for Private Label Transactions */
	char						szAPRType[2];			/*APR_TYPE for Private Label Transaction*/
	PAAS_BOOL					bRegPymtFlow;
	PAAS_BOOL					bDupTranDetected;	/* This flag is set when dup tran is detected*/
	PAAS_BOOL					bPostAuthCardDtls;	/* This flag is set when we store the card details for next trans as we wont prompt for card dtls again, if next sale sent with Auth Code*/
	PAAS_BOOL					bForce;				/* Force flag for transaction */
	PAAS_BOOL					bManEntry;			/* Manual Entry required */
	PAAS_BOOL					bSplitTender;
	PAAS_BOOL					bSAFTran;			/* SAF Transaction indicator */
	PAAS_BOOL					bPrvCrdSAFAllwd;	/* To Indicate whether SAF is allowed for private card */
	PAAS_BOOL					bVSPTran;			/* VSP Transaction indicator */
	PAAS_BOOL					bBillPay;			/* BILL_PAY Indicator */
	PAAS_BOOL					bEarlyRtrnCardFlag;	/* Capture card early return indicator */
	PAAS_BOOL					bDCCNotAllowed;		/* DCC Not Allowed parameter from the POS*/
	POSTENDERDTLS_PTYPE			pstPOSTenderDtls;	/* POS Defined Tender Details */
	SESSDTLS_PTYPE				pstSess;			/* Session Details */
	AMTDTLS_PTYPE				pstAmtDtls;			/* Amount Details */
	CARDDTLS_PTYPE				pstCardDtls;		/* Card Details */
	EMVKEYLOAD_INFO_PTYPE		pstEmvKeyLoadInfo;	/* EMV Key Load Details */		
	EMV_RESP_DTLS_PTYPE			pstEmvRespDtls;		/* EMV Response Details */				
	PINDTLS_PTYPE				pstPINDtls;			/* PIN Details */
	SIGDTLS_PTYPE				pstSigDtls;			/* Signature Details */
	LVL2_PTYPE					pstLevel2Dtls;			/* Tax Details */
	LTYDTLS_PTYPE				pstLtyDtls;			/* Loyalty Details */
	VASDATA_DTLS_PTYPE			pstVasDataDtls;		/* VAS Data Details */
	FTRANDTLS_PTYPE				pstFTranDtls;		/* Follow on tran Details */
	CTRANDTLS_PTYPE				pstCTranDtls;		/* Current Transaction Details */
	CHKDTLS_PTYPE				pstChkDtls;			/* Check Processing Details */
	CUSTINFODTLS_PTYPE          pstCustInfoDtls;    /* Customer Info Details */
	VSPDTLS_PTYPE				pstVSPDtls;			/* VSP Details */
	DUPFIELDDTLS_PTYPE			pstDupDtls;         /* Duplicate Field Details */
	EBTDTLS_PTYPE				pstEBTDtls;         /* EBT Transactions Details*/
	FSADTLS_PTYPE				pstFSADtls;         /* FSA Transactions Details*/
	RAW_CARDDTLS_PTYPE			pstRawCardDtls;		/* Raw Card details for Early return */
	PASSTHRG_FIELDS_PTYPE		pstPassThrghDtls;	/* Pass through fields details*/
	DCCDTLS_PTYPE				pstDCCDtls;			/* DCC Dtls*/
}
PYMTTRAN_STYPE, * PYMTTRAN_PTYPE;

typedef struct __safRec
{
	int		iCmd;						/* Command Type */
	char	szPAN[21];
	char	szTranAmt[20];				/* TODO: Convert into amount type value */
	char	szAmtSpecialcase[20];		/*Incase the SAF record is Special Case (GIFT_ACTIVATE or Refund with No Amount Check), we update this AMOUNT*/
	char	szInvoice[11];
	char	szPymtType[21];				/* TODO: Convert into a constant list type */
	char	szPymtMedia[26];
	char	szSAFNo[10];
	char	szSAFStatus[30];			/* TODO: Convert into a constant list type */
	char	szTroutd[20];               /* Troutd of the Posted SAF record */
	char	szAuthCode[20];             /* Auth code of the Posted SAF record */
	char	szCTroutd[26];              /* CTroutd of the posted SAF record */
	char	szResultCode[20];           /* Result Code of the PWC */
	char 	szCardToken[41];			/* Card token for the given card returned by host*/
	char 	szBusinessDate[9];			/* Business Date for the SAF Record*/
	char	szBankUserData[51]; 		/* Bank User Data */
	char	szCVVCode[5];				/* CVV2_CODE Data */
	char 	szTxnPosEntryMode[4];		/* Transaction POS Entry Mode */
	char 	szMerchId[30];				/* Merchant ID */
	char 	szTermId[16];				/* Terminal ID */
	char 	szLaneId[16];				/* LANE ID */
	char 	szStoreId[16];				/* STORE_ID */
	char 	szApprvdAmt[20];			/* APPROVED_AMOUNT */
	char 	szLPToken[22];				/* LP_TOKEN */
	char 	szPPCV[22];					/* PPCV */
	char 	szAvsCode[3];				/* AVS Code */
	char 	szHostRespCode[50];			/* Host Response Code */
	char 	szSignatureRef[12];			/* SIgnature Reference */
	char 	szAprType[2];				/* APR_TYPE */
	char	szPurchaseApr[13];			/* PURCHASE_APR */
	char	szSvcPhone[11];				/* SVC_PHONE */
	char 	szStatusFlag[2];			/* STATUS_FLAG */
	char	szReceiptText[4096];		/* RECEIPT_TEXT */
	char	szActionCode[3];			/* ACTION_CODE */
	char	szCreditPlanNbr[6];			/* CREDIT_PLAN_NBR */
	char	szPymtSubType[21];			/* PAYMENT_SUBTYPE */
	char	szAthNtwID[4];				/* Auth Nw ID */
	char	szTransDate[15];			/* Transaction Time in format HH:MM:SS   */
	char	szTransTime[15];			/* Transaction Time in format YYYY.MM.DD */
	PASSTHRG_FIELDS_PTYPE	pstPassThrghDtls;	/* Pass Through Fields */
	
	void *	next;
}
SAFREC_STYPE, * SAFREC_PTYPE;

typedef struct
{
	char			szBegNo[10];
	char			szEndNo[10];
	char			szStatus[15];

	char			szTORBegNo[10];		/*SAF TOR begin Num*/
	char			szTOREndNo[10];		/*SAF TOR End Num*/
	char			szTORStatus[15];	/*SAF TOR Status*/

	char			szRecCnt[10];
	char			szTotAmt[20];
	char			szTotSpecialCaseAmt[20]; // Incase the SAF record is Special Case (GIFT_ACTIVATE or Refund with No Amount Check), we update this AMOUNT by adding all Special Case SAF Records so that while clearing the SAF record we take care in updating the Total SAF Amount
	SAFREC_PTYPE	recList;
}
SAFDTLS_STYPE, * SAFDTLS_PTYPE;

/* Device transaction details */
typedef union
{
	char				  szData[50];		/* Querty Data capture */
	SIGDTLS_STYPE		  stSigDtls;		/* Stand alone signature capture */
	LTYDTLS_STYPE		  stLtyDtls;		/* Stand alone email/loytaly capture */
	SURVEYDTLS_STYPE      stSurveyDtls;   	/* Survey details */
	TIMEDTLS_STYPE		  stTimeDtls;		/* Time Details */
	VERDTLS_STYPE		  stVerDtls;		/* Version Details */
	CHARITYDTLS_STYPE	  stCharityDtls;  	/* Charity Details */
    CUSTBUTTONDTLS_STYPE  stCustButtonDtls; /* Customer Button Details */
    CREDITAPPDTLS_STYPE   stCreditAppDtls;	/* Credit Application Details */
	COUNTERDTLS_STYPE     stCounterDtls;  	/* Counter details */
	DEVICENAME_STYPE	  stDevNameDtls;	/* Dev Name details*/
    LANECLOSED_STYPE      stLaneClosed;     /* Lane Closed details*/
	EMPID_STYPE			  stEmpIDDtls;    	/* Empoyee ID Details */
	PAYMENTTYPES_STYPE	  stPymtTypeDtls;   /* Payment Types Enabled for the Device */
	SETPARM_STYPE		  stSetParmDtls;   /* Set Parameter Details */
	DISPMSG_STYPE		  stDispMsgDtls;	/* Display Message Box Details */
	APPLYUPDATES_STYPE	  stApplyUpdatesDtls;/* Apply Updates Details */
	GETPARM_STYPE		  stGetParmDtls;	/* Get Parameter Details */
	PROVISION_PASS_STYPE  stProvPassDtls;	/* Provision Pass Details*/
	VASDATA_DTLS_STYPE	  stVasDataDtls;	/*VAS Data Dtls for Wallets*/
	GET_CARDDATA_STYPE	  stGetCardDataDtls; /*Get Card Data Details*/
	NFCINI_CONTENT_INFO_STYPE stNfcIniContentInfoDtls;  /*Query NFC INI Details*/
	DISPQRCODE_DTLS_STYPE stDispQRCodeDtls;	/*Disp QR Code Dtls*/
	CUSTCHECKBOX_DTLS_STYPE	  stCustCheckBoxDtls;	/*Customer Checkbox Details*/
}
DEVTRAN_STYPE, * DEVTRAN_PTYPE;

/* Pass through transaction details */
typedef struct
{
	void *			pstMetaData;
	char *			szRespMsg;
	/*
	 * Following two fields are added for the
	 * LPTOKEN QUERY
	 */
	PAAS_BOOL		bManEntry;			/* Manual Entry required */
	int				iPymtType;          /* Payment Type */
	CARDDTLS_PTYPE	pstCardDtls;		/* Card Details */
}
PASSTHRU_STYPE, * PASSTHRU_PTYPE;

/* POS Authentication related Data */
typedef struct
{
	char	szPIN[11];
	char	szMACLbl[11];
	char 	szMACKey[600];
	char 	szRsaKey[600];
	char	szRegCmdVer[10];
	char	szEncEntryCode[600];
}
POSSEC_STYPE, * POSSEC_PTYPE;

typedef struct
{
	int						iStatus;
	int						iFxn;
	int						iCmd;
	int						iEncrypted;
	void *					data;
	char					szTranCntr[11];
	RESPDTLS_STYPE			stRespDtls;
	OTHER_RESPDTLS_STYPE	stOtherRespDtls;
}
TRAN_STYPE, * TRAN_PTYPE;

typedef struct
{
	char	szSessMacLbl[15];
	char	szSessDuration[15];
	char	szInvoice[15];
	char	szDeviceName[21];
	char	szSerialNum[15];
}
STATUSDTLS_STYPE, *STATUSDTLS_PTYPE;

#define TRAN_SIZE	sizeof(TRAN_STYPE)

#endif
