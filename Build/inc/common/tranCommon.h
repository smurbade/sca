#ifndef __TRAN_COMMON_H
#define __TRAN_COMMON_H

/* Types of Line Items */
typedef enum
{
	MERCHANDISE = 1,
	OFFER
}
LI_TYPE_ENUM;

typedef enum
{
	PYMNT = 0,
	REFUND
}PYMNT_TYPE_ENUM;

typedef enum
{
	MRCNT_CPN = 0,	/* MERCHANT_COUPON */
	LYLTY_CRD,		/* LOYALTY_CARD */
	MANU_CPN,		/* MANUFACTURER_COUPON */
	OTH_CPN			/* OTHER_COUPON */
}
OFFER_ENUM;

/* Unique details for Merchandise type line item */
typedef struct
{
	char			szUPC[51];			/* UPC (50 + 1) */
	char			szQty[11];			/* QUANTITY (10 + 1) */
	char			szUnitPrice[11];		/* UNIT_PRICE (10 + 1) */
	char			szExtPrice[11];		/* EXTENDED_PRICE (10 + 1) */
}
MCNT_LI_STYPE, * MCNT_LI_PTYPE;

typedef struct
{
	OFFER_ENUM		eOfferType;		
	char			szOfferAmt[11];		/* OFFER_AMOUNT (10 + 1) */
	char 			szOfferLI[11];		/* OFFER_LINE_ITEM (10 + 1) */
}
OFF_LI_STYPE, * OFF_LI_PTYPE;

/* Typical Line Item node in a line items list */
typedef struct __liNode
{
	int					iIndex;
	char				szLiNo[11];		/* LINE_ITEM_ID (10 + 1) */
	char				szDesc[41];		/* DESCRIPTION (41 + 1) */
	char				szSKU[51];		/* SKU (50 + 1) */
	char				szLiFontCol[7];
	LI_TYPE_ENUM		liType;
	void *				liData;

	struct __liNode *	nextLI;
}
LI_NODE_STYPE, * LI_NODE_PTYPE;

/*Payment structure node useful for basket*/
typedef struct __pymntNode
{
	char				szPymtMedia[21];
	char				szPAN[21];
	char				apprvdAmt[20];		/* APPROVED_AMOUNT */
	char				szAuthCode[17];		/* AUTH_CODE (16 + 1) */
	char				szName[30];
	char				szCTroutd[11];		/* CTROUTD (10 + 1) */
	char				szLPToken[256];		/* LPTOKEN */

	PYMNT_TYPE_ENUM 	type;

	struct __pymntNode* next;
}
PYMNT_NODE_STYPE, * PYMNT_NODE_PTYPE;

/* Typical line item change data needed for OVERRIDE/REMOVE commands */
typedef struct
{
	char 			szLiNo[11];			/* LINE_ITEM_ID */
	char			szQty[11];			/* QUANTITY (10 + 1) */
	char			szUnitPrice[11];	/* UNIT_PRICE (10 + 1) */
	char			extPrice[11];		/* EXTENDED_PRICE */
}
LI_CHNG_STYPE, * LI_CHNG_PTYPE;

#endif
