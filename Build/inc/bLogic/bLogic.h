#ifndef __BLOGIC_H
#define __BLOGIC_H

#include "uiAgent/uiControl.h"

typedef struct
{
	int				iDataType;
	int	 			iCtrlId;
	CARD_TRK_STYPE	stCrdTrkInfo;
	EMVDTLS_STYPE	stEmvDtls;
	char 			szData[100];
}
BLUIDATA_STYPE, *BLUIDATA_PTYPE;

typedef struct
{
	int				iEvtType;
	char			szMacLbl[10];
	char			szTranKey[10];
	char			szConnInfo[25];
	BLUIDATA_STYPE	stUIData;
}
BLDATA_STYPE, * BLDATA_PTYPE;

typedef struct
{
	char	szHostSection[15+1];
	char	szMid[41];
	char	szTid[41];
	char	szLaneId[41];
	char	szAltMerchId[41];
}
MCNT_SETTINGS_STYPE, *MCNT_SETTINGS_PTYPE;

extern int initBLogicModule(char *, char *);
extern int closeBLogicModule();

#endif
