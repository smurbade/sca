#ifndef __BLOGIC_CFG_DEF_H
#define __BLOGIC_CFG_DEF_H

#include "common/common.h"

#define	PAN_HASHVALUE_FILE			"./flash/PANhashValue.dat"

//#define SALE							1
#define MAX_CBACK_AMTS					5
#define MAX_CTRTIPS						4
#define	DFLT_WAIT_TO_OBTAIN_NETWORK_IP	10
#define DFLT_TERM_IDEN_DISPLAY_INETRVAL	10
#define DFLT_TERM_IDEN_POS_ACK_INTERVAL	120
#define DFLT_BROADCAST_PORT				9990

#define TERM_IDEN_BUFLEN		512
#define BROADCAST_SERVER_IP 	"255.255.255.255"
#define POS_ACK_PORT			18945
#define MAX_VSD_SECTION			12

/* Cash back settings */
typedef struct
{
	int			iTotCnt;
	int			maxCBAmt;
	int			cbAmt[MAX_CBACK_AMTS];
}
CBACK_STYPE, * CBACK_PTYPE;

/* STB/ASR settings */
typedef struct
{
	PAAS_BOOL		stbLogicEnabled;
	double			fSigLimit;
	double			fPinLimit;
	double			fInterchangeMgmtLimit;
}
STB_CFG_STYPE, * STB_CFG_PTYPE;

/*Encryption type */
typedef enum
{
	NO_ENC =0,
	VSD_ENC=1,
	RSA_ENC=3,
	VSP_ENC=5
}
ENC_TYPE;

/* SAF Settings */
typedef struct
{
	int				iOfflineDaysLimit;
	double			fPrvLblTranFlrLmt;
	double			fTotalFloorLimit;
	double			fTranFloorLimit;
	int				iSAFPingInterval;
	int				iSAFPurgeDays;
	int				iSAFPostInterval;
	int				iSAFThrotInterval;
	PAAS_BOOL		bSAFThrottlingEnable;
	PAAS_BOOL		bProcSAFTranWithOnlineTran;
	PAAS_BOOL		bForceSAFDupTran;
	PAAS_BOOL		bSAFChkConnReqd;
	PAAS_BOOL		bSAFIndicatorReqd;
	PAAS_BOOL		bAllowSAFRefund;
	PAAS_BOOL		bAllowSAFVoid;
	PAAS_BOOL		bAllowSAFGiftAtvt;
	PAAS_BOOL		bChkFloorLimitForRefund;
	PAAS_BOOL		bAllowSAFPayAccount;
	PAAS_BOOL		bSafEMVOnlinePINTrans;
	char			szBusinessDate[9];

}
SAF_STRUCT_TYPE, *SAF_PTR_TYPE;

/* Counter tip settings */
/* Values in percent */
typedef struct
{
	int			iTotCnt;
	int			ctrTipPcnt[MAX_CTRTIPS];
}
CTRTIP_STYPE, * CTRTIP_PTYPE;

typedef enum
{
	TAG_CARD_TYPE = 0,
	TAG_PAY_TYPE,
	TAG_PAN,
	TAG_EXP_DATE,
	TAG_CXX,
	TAG_PAN_6FIRST,
	TAG_PAN_4LAST,
	TAG_SERVICE_CODE,
	TAG_CARDHOLDER_NAME,
	TAG_ENC_MODULE,
	TAG_TRACK1_REG,
	TAG_TRACK2_REG,
	TAG_TRACK3_REG,
	TAG_TRACK1_RAND,
	TAG_TRACK2_RAND,
	TAG_TRACK1_SHA,
	TAG_TRACK2_SHA,
	TAG_TRACK1_MASK,
	TAG_TRACK2_MASK,
	TAG_ENCBLOB0,
	TAG_KSN0,
	TAG_IV0,
	TAG_ENCBLOB1,
	TAG_KSN1,
	TAG_IV1,
	TAG_EMVTAG_5A,
	TAG_EMVTAG_5F24,
	TAG_EMVTAG_57,
	TAG_EMVTAG_9F1F,
	TAG_EMVTAG_9F20,
	TAG_EMVTAG_5F30,
	MAX_VSD_TAGS
}
VSD_TAGS;


typedef enum
{
	PYMT_CARDS=0,
	GIFT_CARDS,
	LYLT_CARDS,
	NONISO_CARDS,
	MAX_CARD_TYPES
}
VSD_CARD_TYPE;

typedef struct _node_
{
	VSD_TAGS		tagID;
	char			tagName[20+1];
	struct _node_ 	*next;
}
VSD_TAGNODE_STYPE, * VSD_TAGNODE_PTYPE;

typedef struct
{
	int					iTotTags;
	VSD_TAGNODE_PTYPE	start;
	VSD_TAGNODE_PTYPE	end;
}
VSD_TAGLIST_STYPE, * VSD_TAGLIST_PTYPE;

typedef struct
{
	int 				payType;
	VSD_CARD_TYPE		cardType;
	VSD_TAGLIST_PTYPE	pstTagList;
}
VSD_BLOB_STYPE, * VSD_BLOB_PTYPE;

typedef struct
{
	int 			iTotSection;
	char			szCardTypeTagName[20+1];
	char			szPayTypeTagName[20+1];
	VSD_BLOB_PTYPE	pstVSDBlob[MAX_VSD_SECTION];

}
VSD_DTLS_STYPE, * VSD_DTLS_PTYPE;


/*Blogic settings*/
typedef struct
{
	int			iWaitToObtainNetworkIP;
	int			iTermIdenDispInterval;
	int			iTermIdenPOSAckInterval;
	int			iBroadcastPort;
	int			iPOSACKPort;
	int			iTORretryCount;
	PAAS_BOOL	vspEnabled;
	PAAS_BOOL	vsdB64EncodingEnabled;
	PAAS_BOOL	bVHQSupEnabled;
	PAAS_BOOL	networkcfgreqd;
	PAAS_BOOL	redoVSPReg;
	PAAS_BOOL	bapiEnabled;
	PAAS_BOOL	mcntSettingsReqd;
	ENC_TYPE	encryptionType;
	PAAS_BOOL	voidCardDtlsreqd;
	PAAS_BOOL   debitVoidCardDtlsReqd;
	PAAS_BOOL	swipeaheadEnabled;
	PAAS_BOOL	dupSwipeDetectEnabled;
	PAAS_BOOL	lineItemDisplayEnabled;
	PAAS_BOOL   fullLineItemDisplayEnabled;
	PAAS_BOOL	devNameEnabled;
	PAAS_BOOL	returnEmbossedNumForGift;
	PAAS_BOOL	bEmbossedNumCalReqd;
	PAAS_BOOL	returnEmbossedNumForPL;
	PAAS_BOOL	captureSignForManualTran;
	PAAS_BOOL	torEnabled;
	PAAS_BOOL	captureSignForPrivTran;
	PAAS_BOOL	sendSignDtlsToSSIHost;
	PAAS_BOOL	svsNonDenominated;
	PAAS_BOOL	includeTAFlag;
	PAAS_BOOL	dccEnabled;
	PAAS_BOOL	sendUnsolMsgDuringPymtTran;
	PAAS_BOOL	eparamsON;
	PAAS_BOOL	debitOffPinCVM;
	PAAS_BOOL	redoVSPAfterDDKAdvanced;
	char		szRSAFingerPrint[40];
	char		szDevName[21];
	char		szPANHashValue[41];
	VSD_DTLS_PTYPE	pstVSDDtls;
}
BLOGIC_CFG_STYPE, *BLOGIC_CFG_PTYPE;

/* Payment Settings */
typedef struct
{
	PAAS_BOOL			cashBackEnabled;
	PAAS_BOOL			ebtCashBackEnabled;
	PAAS_BOOL			tipSuppEnabled;
	PAAS_BOOL			ctrTipEnabled;
	PAAS_BOOL			splitTenderEnabled;
	PAAS_BOOL			loyaltyCapEnabled;
	PAAS_BOOL			emailCapEnabled;
	PAAS_BOOL			manEntryEnabled;
	PAAS_BOOL			cntctLessEnabled;
	PAAS_BOOL			safEnabled;
	//PAAS_BOOL			bSAFThrottlingEnable;
	PAAS_BOOL			partAuthEnabled;
	PAAS_BOOL			giftRefundEnabled;
	CBACK_PTYPE			cbCfgPtr;
	CTRTIP_PTYPE		ctrTipCfgPtr;
	SAF_PTR_TYPE		safCfgPtr;
	PAAS_BOOL			tenderList[MAX_TENDERS];
	int					consumerOptList[MAX_CONSUMER_OPTS][2];
}
PYMNT_CFG_STYPE, * PYMNT_CFG_PTYPE;

/* EMV SETUP  */
#define EMV_SETUP_NOT_REQ 			0
#define EMV_SETUP_REQ_WITHOUT_HOST 	1
#define EMV_SETUP_REQ_WITH_HOST 	2
#define	EMV_SETUP_READ_AGAIN		3

/* extern functions */
extern PAAS_BOOL 	isPreambleSuccessful();
extern PAAS_BOOL 	isDemoModeEnabled();
extern PAAS_BOOL 	isLoyaltyCapEnabled();
extern PAAS_BOOL 	isEmailCapEnabled();
extern PAAS_BOOL 	isPartAuthEnabled();
extern PAAS_BOOL 	isSigCapEnabled();
extern PAAS_BOOL 	isSplitTenderEnabled();
extern PAAS_BOOL 	isCtrTipEnabled();
extern PAAS_BOOL 	isCashBackEnabled();
extern PAAS_BOOL	isEBTCashBackEnabled();
extern PAAS_BOOL 	isSAFEnabled();
extern PAAS_BOOL	isUnsolMsgDuringPymtTranEnabled();
extern PAAS_BOOL	isVSPEparamsON();
extern PAAS_BOOL 	isVSPActivated();
extern PAAS_BOOL	isRedoVSPAfterAdvanceDDKActivated(PAAS_BOOL);
extern PAAS_BOOL 	isVSPReRegReqd();
extern PAAS_BOOL 	isNwCfgReqd();
extern PAAS_BOOL 	isVSPEnabledInDevice();
extern PAAS_BOOL	isVSDb64EncodingEnabled();
extern PAAS_BOOL 	isCardDtlsRequiedforVoid();
extern PAAS_BOOL 	isCardDtlsRequiedforDebitnEBTVoid();
extern PAAS_BOOL 	isSwipeAheadEnabled();
extern PAAS_BOOL 	isDupSwipeDetectEnabled();
extern PAAS_BOOL 	isLineItemDisplayEnabled();
extern PAAS_BOOL    isFullLineItemDisplayEnabled();
extern PAAS_BOOL 	isDevNameEnabled();
extern PAAS_BOOL	isDebitOffPinCVMEnabled();
extern PAAS_BOOL 	isMerchantSettingsReqd();
extern PAAS_BOOL 	isSTBLogicEnabled();
extern PAAS_BOOL 	isGiftRefundEnabled();
extern PAAS_BOOL 	returnEmbossedNumForGiftType();
extern PAAS_BOOL	isEmbossedCardNumCalcRequired();
extern PAAS_BOOL 	returnEmbossedNumForPrivLblType();
extern PAAS_BOOL 	isSigCaptureReqdForManualTran();
extern PAAS_BOOL 	isTOREnabled();
extern int		 	getTORretryCount();
extern void		 	setSwipeAheadParamValue(PAAS_BOOL );
extern PAAS_BOOL	isSigCaptureReqdForPrivTran();
extern PAAS_BOOL	isSVSNonDenominatedEnabled();
extern int			getWaitTimeToObtainNetworkIP();
extern PAAS_BOOL 	isIncludeTAFlagEnabled();
extern int			getTermIdenDisplayInterval();
extern int			getTermIdenPOSACKInterval();
extern int			getTermIdenBroadcastPort();
extern int			setPOSACKPort(int);
extern int			getPOSACKPort();
extern PAAS_BOOL 	isSendSignDtlsToSSIHostEnabled();

extern double 		 getSTBPINLimit();
extern double		 getSTBSignatureLimit();
extern double 		 getSTBInterchangeMgmtLimit();

extern int		 setDevAdminRqd(PAAS_BOOL);
extern int		 getPinPadMode();
extern int		 setVSPActivatedStatus(PAAS_BOOL);
extern int		setRedoVSPAfterAdvanceDDK(PAAS_BOOL);
extern int 		 setVSPReRegReqd(PAAS_BOOL);

extern int 		 setNwCfgReqd(PAAS_BOOL);
extern int 		 setDevAdminRqd(PAAS_BOOL);
extern int 		 setDevEmvSetupRqd(int);

extern char* 		getDemoModeText();
extern char* 	 	getRSAFingerPrint();
extern ENC_TYPE		getEncryptionType();
extern VSD_DTLS_PTYPE getVSDEncryptionSettings();
extern int 			getTendersListFromSettings(int * );
extern PAAS_BOOL 	isPrivLabelTenderEnabled();
extern int 			getConsOptions(int arr[][2] );
extern int 			setConsOptSelected(int , int);
extern int 			resetConsOptSelection();
extern int 			getPresetCtrTipsInfo(CTRTIP_PTYPE);
extern int 			getPresetCbackAmtsInfo(CBACK_PTYPE);
extern double 		getSigFloorAmount();
extern int 			setPreambleStatus(PAAS_BOOL status);
extern double 		getSAFTranFloorAmount();
extern double 		getSAFTotalFloorAmount();
extern double 		getSAFTranFloorAmountForPrivLabel();
extern int   		getSAFOfflineDaysLimit();
extern int   		getSAFPingInterval();
extern int 		 	getSAFPostInterval();
extern int		 	getSAFThrottlingInterval();
extern PAAS_BOOL 	isSAFThrottlingEnabled();
extern PAAS_BOOL 	bProcSAFTranWithOnlineTran();
extern int   		getSAFPurgeDays();
extern void 		getSigImageType(char *);
extern int			getDeviceKey(char *);
extern char * 		getURLFrmCnfgFile();
extern int	  		loadBLogicConfigParams(char *);
extern PAAS_BOOL 	isConsumerOptsEnabled();
extern PAAS_BOOL 	isForceFlgSetForSAFTran();
extern PAAS_BOOL 	isCheckConnReqdForSAFTran();
extern PAAS_BOOL 	isSAFIndReqdForSSIReq();
extern PAAS_BOOL    isSAFAllowedForRefundTran();
extern PAAS_BOOL    isSafAllowedForEmvOnlinePIN();
extern PAAS_BOOL    isSAFAllowedForVoidTran();
extern PAAS_BOOL    isSAFAllowedForGiftActivate();
extern PAAS_BOOL	isFloorLimitChkAllowedForSAFRefund();
extern PAAS_BOOL	isSAFAllowedForPayAccount();
extern void 		getDeviceName(char*);
extern void 		updateDeviceName(char*);
extern void 		getBusinessDate(char *);
extern void 		setBusinessDate(char *);
extern int 			getHashValueOfPreviousPAN(char * );
extern int 			storeHashValueOfCurPAN(const char * );
extern PAAS_BOOL	isPaypalTenderEnabled();
extern PAAS_BOOL	isFSATenderEnabled();
extern PAAS_BOOL	isEBTTenderEnabled();
extern PAAS_BOOL 	isPayBalanceTenderEnabled();
extern int 			isDevEmvSetupRqd();
extern PAAS_BOOL 	isDCCEnabled();
#endif
