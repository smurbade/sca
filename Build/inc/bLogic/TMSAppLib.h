#ifndef _TMS_APPLIB_H_
#define _TMS_APPLIB_H_
/**********
*
*   Last updated Date -: 01-MAR-16
*
***********/
#ifdef __cplusplus
extern "C" {
#endif

/***********TMS************/

#define LTMS_MSGQ_PATH "/var/tmp/tms.msg"
#define LTMS_MSGQ_ID 'T'
#define LTMS_MSGQ_TYPE 1122


#define LTMS_MAX_APP_NAME_LEN 20
#define LTMS_MAX_PARAM_FILENAME_LEN 20 + LTMS_MAX_APP_NAME_LEN // 20 is for /home/usr1/flash/
#define LTMS_MAX_MSG_DESCRP 256
#define LTMS_MAX_PARAM_NAME_LEN 63
#define LTMS_MAX_PARAM_NAME_VAL 254

/***Return values ****/
#define LTMS_INIT_SUCCESS	         1 //TMS_Init() return value for success
#define LTMS_INIT_FAIL               0 //TMS_Init() return value for failure
#define LTMS_GENERIC_ERROR          -1
#define LTMS_CALL_SERVER_FAIL	    -8
#define LTMS_UNREG_SUCCESS          500
#define LTMS_CALLSERVER_SUCCESS     501
#define LTMS_SETSTATE_SUCCESS       502


/******
* LIB to APP communication exchange MSG TYPES -
* Usage : In the TMS callback TMS_LIBTOAPP_DATA structure variable iReqType will be filled with bellow MSG TYPE
* NOTE  : For the below MSG TYPES - TMS library will be waiting for the MSGQ response at a configured interval time
*******/
#define LTMS_MSGTYPE_GET_REBOOT_APPROVE 100
#define LTMS_MSGTYPE_GET_APPRESTART_APPROVE 101
#define LTMS_MSGTYPE_GET_PARAM_LIST 102 // TODO: still under discussion
#define LTMS_MSGTYPE_GET_INSTALL_APPROVE 103
#define LTMS_MSGTYPE_GET_APP_INFO 104
#define LTMS_MSGTYPE_GET_APP_STATE 105
#define LTMS_MSGTYPE_INSTALLFILE_FULL 106
#define LTMS_MSGTYPE_GET_CLOCKUPDATE_APPROVE 107

/******
*
* All the below MSG TYPES are just notification to the  App
*
******/
/*
* This notification will be sent from LIB to APP
* All notification starts with 2XX series
*/
#define LTMS_MSGTYPE_CONTENTINST_SUCCESS_NOTIFY 200
#define LTMS_MSGTYPE_CONTENTINST_ERROR_NOTIFY 201
#define LTMS_INSTALLSUCCESS_REBOOT_NOTIFY 202
#define LTMS_INSTALLSUCCESS_APPRESTART_NOTIFY 203
#define LTMS_INSTALLSUCCESS_NOTIFY 204
#define LTMS_INSTALLFAILED_NOTIFY 205


/*****
*
* Usage: Below message response will be sent from application to library in  struct tms_AppStatus - iAppStatus
*
*****/
#define LTMS_MSGRESP_APPROVE 1
#define LTMS_MSGRESP_REJECT  2
#define LTMS_MSGRESP_POSTPONED 3
#define LTMS_MSGRESP_FREE 1
#define LTMS_MSGRESP_BUSY 2
#define LTMS_MSGRESP_FILEINSTALL_SUCCESS 202
#define LTMS_MSGRESP_FILEINSTALL_FAIL 203
#define LTMS_MSGRESP_FILEINSTALL_BUSY 204
#define LTMS_MSGRESP_FILEINSTALL_POSTPONED 205

#define LTMS_MAX_FILE_NAME_LEN 256

/****************Details that user application that sends to library (A)*************/

#define LTMS_PARAMETER_TYPE_IDENTIFIER 1
#define LTMS_PARAMETER_TYPE_DEVICE_PROFILE 2
#define LTMS_PARAMETER_TYPE_DIAGNOSTIC 3

/**
* Application Parameter Structure used by the application to pass an application parameter to VHQ/TMS.
* The application will setup one of these structures for each custom parameter it would like to sent
* to the VHQ/TMS server.  An array of these structures is then pointed to in the tms_AppInfo structure.
*/
struct tms_AppParameter
{
	char szparameterName[LTMS_MAX_PARAM_NAME_LEN + 1];  /**< NULL terminated parameter name - i.e. "STOREID" */
	char szparameterValue[LTMS_MAX_PARAM_NAME_VAL + 1];  /**< NULL terminated parameter Value - i.e. 1234 */
	int iparameterType; /**< Parameter type (i.e. LTMS_PARAMETER_TYPE_IDENTIFIER, LTMS_PARAMETER_TYPE_DEVICE_PROFILE, or LTMS_PARAMETER_TYPE_DIAGNOSTIC) */
};


/*SVC_STRUCT*/
/**
* Application Information Structure used by the application to pass application parameters to VHQ/TMS.  This structure is filled out by the application and passed to VHQ/TMS in response to a TMS_EVT_SET_APP_INFO event.
*/
struct tms_AppInfo
{
	struct tms_AppParameter* stparameterList; /**< Pointer to an array of tms_AppParameter[] values to be sent to the server */
	int iparameterCount; /**< number of tms_AppParameter values in the parameterList array */
};


struct tms_ParamList
{
    /*filename that contains application parameter information.
     * this will be used for LTMS_MSGTYPE_GET_PARAM_LIST message type
     */
    char **pszFileinfo; //Pointer to an array of char* - where it contain the file name with absolute path - Max file name length should be LTMS_MAX_FILE_NAME_LEN

    /*
    Hold number of file name in the above array
    */
    int iTotalFileCount;
};

/*
 * Used to get the application permission for following message type
 * LTMS_MSGTYPE_REBOOT_APPROVE/LTMS_MSGTYPE_APPRESTART_APPROVE/LTMS_MSGTYPE_CONTENT_INSTALL_APPROVE
 */
struct tms_AppStatus
{
     /**
     * Will hold the app status:
     * LTMS_MSGRESP_APPROVE/LTMS_MSGRESP_REJECT/LTMS_MSGRESP_POSTPONED/LTMS_MSGRESP_FILEINSTALL_SUCCESS/LTMS_MSGRESP_FILEINSTALL_FAIL/LTMS_MSGRESP_FREE/LTMS_MSGRESP_BUSY
     **/
    int iAppStatus;
     /**
     * Will hold the application description if any for the MSG TYPE LTMS_MSGTYPE_INSTALLFILE request.
     */
    char szStatusDescription[LTMS_MAX_MSG_DESCRP + 1];
};

union TMS_App_Data
{

    /* User application to fill for following request type and description
     *  LTMS_MSGTYPE_GET_REBOOT_APPROVE/LTMS_MSGTYPE_GET_APPRESTART_APPROVE/LTMS_MSGTYPE_GET_INSTALL_APPROVE/LTMS_MSGTYPE_INSTALLFILE
     */
    struct tms_AppStatus stTMSAppStatus;
    /*
     * User application to fill for following request type
     *  LTMS_MSGTYPE_GET_PARAM_LIST
     */
    struct tms_ParamList stTMSParamList;
    /* User application to fill for following request type
     *  LTMS_MSGTYPE_GET_APP_INFO
     */
    struct tms_AppInfo stTMSAppInfo;
};

typedef struct TMS_MSG
{
    long MsgType; // should be filled with LTMS_MSGQ_TYPE
    int iReqType;//User application should fill this variable with the message type that received in the request.
    union TMS_App_Data uAppData;
    void *NotInUse;
}TMS_APPTOLIB_MSG;

/*********End (A)****************/

/******Details that library will send to Application (B)********/

/***
* TMS lib will send the parameter file details to application in the below structure for MSG TYPE LTMS_MSGTYPE_INSTALLFILE.
* User application should install the file form szSrcFile location to the szDestFile location and send back the response LTMS_MSGRESP_FILEINSTALL_SUCCESS/LTMS_MSGRESP_FILEINSTALL_FAIL
*
*/
struct tmsFileDetails
{
    char szSrcFile[LTMS_MAX_FILE_NAME_LEN + 1];
    char szDestFile[LTMS_MAX_FILE_NAME_LEN + 1];//For now this parameter is not used
};
typedef struct tms_Lib_Data
{
    int iReqType;//
    struct tmsFileDetails *ptrsttmsFileData;//This structure will be filled only for LTMS_MSGTYPE_INSTALLFILE
    void *NotInUse;
}TMS_LIBTOAPP_DATA;




/*****************End (B)***********/

/***********Initialization structure (C)************/
typedef void (*p_tmsfnLibCallBack)(TMS_LIBTOAPP_DATA stLibData);//Callback signature
typedef struct tms_init
{
    /***
    * Name that is used by the VHQ agent to help identify the different applications that
    * are registered.  The app name is used for identification purposes, as well as
    * parameter management.The registered app name MUST match the application name
    * specified in Secure Installer bundle for V/OS devices. This name is case sensitive so it must
    * match exactly.
    ***/
    char szAppName[LTMS_MAX_APP_NAME_LEN + 1];
    /***
    * A callback function provided by the application to handle events sent from the TMS app lib
    * to the application.  The prototype for the callback function is:
    * <CODE>void (*p_tmsfnLibCallBack)(TMS_LIBTOAPP_DATA stLibData);</CODE>
    * All the above MSG TYPE can be sent from the TMS user lib to the application and need to be
    * handled by the application in the callback
    ***/
    p_tmsfnLibCallBack pLibCallBack;

    /*For the events that the TMS App lib expects a response from, the TMS App lib will timeout
     * after below configured seconds of waiting for the response (Min 4sec and Max 540sec).
     * Note: If the iLibWaitTimeoutSec is less than MIN value then default it to 4sec,
     * and if it is greater than MAX value then default it 540sec.
     */
    int iLibWaitTimeoutSec;

    void *NotInUse;

}TMS_APP_INIT;

/*****************End (C)***********/

/*
 * ============================================================================
 * Function Name: TMS_Init
 *
 * Description	: This function initializes the TMS library.
 *
 * In Params: TMS_APP_INIT - Structure that filled by user application and send to library.
 *
 * Return Value : int -  LTMS_INIT_FAIL
 *                       LTMS_INIT_SUCCESS
 * ============================================================================
 */


int TMS_Init(TMS_APP_INIT stAppInit);

/*
 * ============================================================================
 * Function Name: TMS_GetVersion_SVC_Agent
 *
 * Description	: This function is to get the TMS/VHQ agent version details.
 *
 * Params: NIL
 *
 * Return Value : char * -  <SVC Version> / <Agent Version> (Note: SVC and Agent version are '/' separated.
 * ============================================================================
 */
char* TMS_GetVersion_SVC_Agent(void);


/*
 * ============================================================================
 * Function Name: TMS_GetVersion_Lib
 *
 * Description	: This function is to get the TMS user interface library version details.
 *
 * In/Out Params: char * - TMS lib version number.
 *
 * Return Value : void
 * ============================================================================
 */
char* TMS_GetVersion_Lib(void);


/*
 * ============================================================================
 * Function Name: TMS_ForceSetAppState
 *
 * Description	: This function is to set the application state on the go.
 *
 * In Params: int : Flag that application need set its state LTMS_MSGRESP_FREE/LTMS_MSGRESP_BUSY
 *
 * Return Value : int - LTMS_SETSTATE_SUCCESS for success
                      - LTMS_GENERIC_ERROR for failure
 * ============================================================================
 */
 int TMS_ForceSetAppState(int iFlag);

/*
 * ============================================================================
 * Function Name: TMS_Unregister
 *
 * Description	: This function will unregister the application that was registered with TMS Agent.
 *
 * In Params: char * : Application name that registered with TMS Agent.
 *
 * Return Value : int - LTMS_UNREG_SUCCESS for success
                      - LTMS_GENERIC_ERROR for failure
 * ============================================================================
 */
 int TMS_Unregister(char *pszAppName);


/*
 * ============================================================================
 * Function Name: TMS_CallServer
 *
 * Description	: This function will perform force call serve.
 *
 * In Params: int : The number of seconds from now to contact the server.
 *					value 0 indicates to contact the server right now.
 *
 * Return Value : int - LTMS_CALLSERVER_SUCCESS
 *
 * ============================================================================
 */
 int TMS_CallServer(int iWaittime);
//TODO:
/*
Things that are in discussion
1) Check the diff.. content - software - Parameter.
2) SCA need to update the parameter only for specific parameter.
3) Send install status to app

*/

#ifdef __cplusplus
}
#endif

#endif // _TMS_APPLIB_H_

