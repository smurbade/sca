#ifndef __BL_STEP_H
#define __BL_STEP_H

typedef enum
{
	NETWORK_CFG,
	DEVICE_ADMIN_REG,
	VSP_REG,
	PAYMENT_ENGINE_INIT,
	DUMMY_SALE
}
PREAMBLE_STEP_ENUM;

/* PREAMBLE */
extern int configureNetwork();
extern int devAdminSetup(char *);
extern int preambleVSP(char *);
extern int doDummyTransaction(char *);
extern int configureDHIMcntSettings();
extern void preProcessPreambleSteps();

//ArjunU1: EMV Testing
extern int procEmvInitialization();

/* LINE ITEMS */
extern int addLI(char *, char *, int , char *);
extern int removeLI(char *, char *, int , char *);
extern int removeAllLI(char *,char *,int,char *);
extern int overrideLI(char *, char *, int , char *);
extern int showLI(char *, char *, char *);

extern int processVoidTran(char * , char *, char *, int*);
extern int processRefundTran(char * , char *, char *, int*);
extern int processCompletionTran(char * , char * , char *, int*);
extern int processGiftCardTran(char * , char * , int , char * , PAAS_BOOL);
extern int processEBTBalanceTran(char * , char * , char * , PAAS_BOOL);
extern int processPrivBalanceTran(char * , char * , char * , PAAS_BOOL);
extern int processBalanceTran(char * , char * , char * );
extern int processRestaurantTran(char * , char * , int , char * );
extern int processAuthTran(char * , char * , int ,char *, int*);
extern int processSaleTran(char * , char * , char *, int*);
extern int processLPTokenQuery(char * , char * );
extern int processTokenQuery(char *, char *, char *, PAAS_BOOL);
extern int processPayAccount(char * , char *, char * , int* );
extern int getPrivCardToken(char * , int , char * );
extern int procSSIRequest(char *, char *);
//extern int completeEmvOnlineTran(char * , char *, int*);

/* REPORT */
extern int getDuplicateTrans(char * , char * );
extern int getLastTransaction(char * , char * );
extern int getReportSummary(char * , char *, int );
/* BATCH */
extern int processBatchSettle(char * , char * );

/* MISC */
extern PAAS_BOOL isVSPRegReqdAgain();
extern int doVSPReg(char *, char *, PAAS_BOOL, char*, int );
extern int doDeviceResyncKey();

#endif
