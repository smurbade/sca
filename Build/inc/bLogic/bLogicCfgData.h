/*
 * bLogicCfgData.h
 *
 *  Created on: Dec 17, 2013
 * Author: kranthik1
 */

#ifndef BLOGICCFGDATA_H_
#define BLOGICCFGDATA_H_

#define SECTION_DEVICE			"DCT"
#define SECTION_REG				"REG"
#define SECTION_TENDER			"TDT"
#define SECTION_PAYMENT			"PDT"
#define SECTION_CBACK			"CBT"
#define SECTION_CTRTIP			"CTT"
#define SECTION_SAF				"SAF"
#define SECTION_DHI				"DHI"
#define SECTION_CONSUMEROPTS	"COT"
#define SECTION_SPINTHEBIN		"STB"
#define SECTION_RCHI			"RCHI"
#define SECTION_VHI				"VHI"
#define SECTION_CPHI			"CPHI"

#define VSP_ENABLED					"vspenabled"
#define NETWORK_CFG_REQD 			"networkcfgreqd"
#define REDO_VSP_REG				"redovspreg"
#define VSP_ACTIVATED				"vsp_activated"
#define REDO_VSP_AFTER_ADVANCE_DDK	"redovspafteradvanceddk"
#define VOID_CARD_DTLS_REQD 		"voidcarddtlsreqd"
#define SWIPE_AHEAD_ENABLED 		"swipeaheadenabled"
#define DUP_DETECT_ENABLED 			"dupswipedetectenabled"
#define LINEITEM_DISPLAY_ENABLED 	"lineitem_display"
#define FULL_LINEITEM_DISPLAY		"fulllidisplayenabled"
#define	DEVICE_NAME					"devicename"
#define DEV_NAME_ENABLED			"devnameenabled"
#define MCNT_SETTINGS_REQD			"merchantsettingsreqd"
#define STB_LOGIC_ENABLED			"stblogicenabled"
#define STB_SIG_LIMIT				"signatureLimit"
#define STB_PIN_LIMIT				"pinLimit"
#define STB_INTERCHANGE_MGMT_LIMIT	"interchangeMgmtLimit"
#define DEBIT_VOID_CARD_DTLS_REQD 	"debitvoidcarddtlsreqd"
#define RETURN_EMBOSSED_NUM_FOR_GIFT "returnembossednumforgift"
#define RETURN_EMBOSSED_NUM_FOR_PL	 "returnembossednumforprivlbl"
#define CAPTURE_SIGN_FOR_MANUAL_TRAN "capturesignformanualtran"
#define TOR_ENABLED					 "torenabled"
#define TOR_RETRY_COUNT				 "torretries"
#define EMV_SETUP_REQD				 "emvsetupreqd"
#define EMBOSSED_NUM_CALC_REQD		 "embossednumcalcreqd"
#define CAPTURE_SIG_FOR_PRIV_LABEL	 "capturesignatureforprivlbl"
#define SVS_NON_DENOMINATED_CARD	 "svsnondenominatedcard"
#define WAIT_TO_OBTAIN_NETWORK_IP	 "waittimetoobtainnetworkip"
#define INCLUDE_TA_FLAG				 "includeTAflag"
#define TERM_IDEN_DISPLAY_INTERVAL	 "termidendispinterval"
#define TERM_IDEN_POS_ACK_INTERVAL	 "termidenposackinterval"
#define BROADCAST_PORT				 "broadcastport"
#define SEND_SIGN_DTLS_TO_SSI_HOST	 "sendsigndtlstossihost"
#define DCC_ENABLED					 "dccenabled"
#define UNSOL_MSG_DURING_PYMT_TRAN	 "sendunsolmsgduringpymttran"
#define DEBIT_OFFLINE_PIN_CVM		 "debitOffPinCVM"


/* Under Reg Section*/
#define	TIME_ZONE				"timezone"

/* --- Consumer Options Table - COT --- */
#define CONSUMER_OPTION1	"consumer_option1"
#define CONSUMER_OPTION2	"consumer_option2"
#define CONSUMER_OPTION3	"consumer_option3"
#define CONSUMER_OPTION4	"consumer_option4"
#define CONSUMER_OPTION5	"consumer_option5"
#define CONSUMER_OPTION6	"consumer_option6"


/* --- Tender Types Definition Table - TDT --- */
#define CREDIT_TYPE			"credit"
#define DEBIT_TYPE			"debit"
#define GIFTCARD_TYPE		"gift"
#define GWALLET_TYPE		"gwallet"
#define IWALLET_TYPE		"iwallet"
#define CASH_TYPE			"cash"
#define PRIVATE_TYPE		"privlabel"
#define MERCHCREDIT_TYPE	"merchandisecredit"
#define PAYPAL_TYPE 		"paypal"
#define EBT_TYPE 			"EBT"
#define FSA_TYPE 			"FSA"

/* --- Payment Definition Table - PDT --- */
#define CASHBACK_ENABLED	"cashback"
#define EBTCASHBACK_ENABLED	"ebtcashback"
#define SPLT_TNDR_ENABLED	"splittender"
#define LYLTYCAP_ENABLED	"loyaltycapture"
#define EMAILCAP_ENABLED	"emailcapture"
#define SIGCAP_ENABLED		"sigcapture"
#define CNTCTLSS_ENABLED	"contactless"
#define TIP_SUPP_ENABLED	"PRTIP1"
#define CTRTIP_ENABLED		"countertip"
#define MANENTRY_ENABLED	"manualentry"
#define SAF_ENABLED			"safenabled"
#define PARTAUTH_ENABLED	"partialauth"
#define RECEIPT_ENABLED		"receiptenabled"
#define	PRIV_TOK_REQD		"privtokrequired"
#define GIFTREFUND_ENABLED	"giftrefund"

/* --- Store and Forward Table - SAF --- */
#define SAF_TRAN_FLOOR_LIMIT					"transactionfloorlimit"
#define	SAF_EMV_ONLINE_PIN_TRANS				"safemvonlinepintran"
#define SAF_TOTAL_FLOOR_LIMIT					"totalfloorlimit"
#define SAF_PRIV_LBL_TRAN_FLOOR_LIMIT			"privlbltranfloorlimit"
#define SAF_DAYS_LIMIT							"dayslimit"
#define SAF_PING_INTERVAL						"safpinginterval"
#define SAF_PURGE_DAYS							"safpurgedays"
#define SAF_TRAN_POST_INTERVAL					"safpostinterval"
#define SAF_THROTLING_ENABLED					"safthrottlingenable"
#define SAF_THROT_INTERVAL						"safthrottlinginterval"
#define SAF_ALLOW_OFFLINE_TRAN_TOGOWITH_ONLINE	"allowOfflineTranToGoWithOnline"
#define SAF_BUSINESSDATE						"businessdate"
#define SAF_FORCE_DUP_TRAN						"forcesafduptran"
#define SAF_CHK_CONN_REQD						"safchkconnreqd"
#define SAF_INDICATOR_REQUIRED					"safindicatorreqd"
#define SAF_ALLOW_REFUND_TRAN					"allowrefundtosaf"
#define SAF_ALLOW_VOID_TRAN						"allowvoidtosaf"
#define	SAF_ALLOW_GIFT_ACTIVATE					"allowgiftactivatetosaf"
#define	SAF_CHK_FLOOR_LIMIT_TO_SAF_REFUND		"chkfloorlimittosafrefund"
#define	SAF_ALLOW_PAYACCOUNT_TO_SAF				"allowpayaccounttosaf"

/* --- Cash back Table - CBT --- */
#define CB_AMT_LIMIT		"cashbackamtlimit"
#define CFG_CBAMT_PREFIX	"presetamt_"

/* --- Counter Tip Table - CTT --- */
#define CFG_CTRTIP_PREFIX	"presettip_"

#define PREAMBLE_SUCCESS	"preamble_success"

#define VSD_INI_FILE_NAME	"/mnt/flash/userdata/share/VSD.INI"


/* DHI Merchant Settings DATA */
#define	PROCESSOR				"processor"
#define MID						"mid"
#define TID 					"tid"
#define LANEID 					"laneid"
#define ALTMERCHID				"altmerchid"


#endif /* BLOGICCFGDATA_H_ */
