#ifndef __BL_APIS_H
#define __BL_APIS_H

#include "common/common.h"
#include "common/tranDef.h"

/* PREAMBLE related */
extern int processSCAPreamble();

/* SECURITY related */
extern int registerPOS(char *, char *);
extern int unregisterPOS(char *, char *, char *);
extern int unregisterAllPOS(char *, char *, char *, char *);
extern int registerEncryptedPOS(char * , char * );
extern int authenticatePOS(char *, char *, char *, int , char *);

/* DEVICE related */
extern int getSignForDevTran(char *, char *, int);
extern int doOfflineLtyCapture(char *, char *);
extern int processProvisionPass(char * , char * );
extern int doOfflineEmailCapture(char *, char *);
extern int doSurvey(char * , int , char * );
extern int setTimeForDevice(char *, char *);
extern int doCustQuestion(char *, char *, int *, int *);
extern int getApplicationVersionInfo(char * , int *, char * );
extern int doCharity(char * , char * );
extern int doCustomerButtonOption(char * , char * );
extern int doCreditApplication(char * , char * , int *, int *);
extern int doRebootDevice(char * , char * , char *, char * );
extern int getEnabledPaymentTypes(char * , char * );
extern int getCurrentCounterValue(char * , char * );
extern int doLaneClose(char * , char * , char *);
extern int getDeviceNameForDevTran(char *, char *);
extern int setDeviceNameForDevTran(char *, char *);
extern int doEmpIDCapture(char * , char * );
extern int processSetParm(char * , char * );
extern int processDispMsg(char * , char *);
extern int processGetParm(char * , char *);
extern int doGetCardData(char *,char *);
extern int processQueryNfcIniFile(char *);
extern int processDispQRCode(char *, char *);
extern int processCancelQRCode(char *);
extern int processCustCheckbox(char *, char *);

/* SAF related */
extern int doSAFQuery(char *, char *);
extern int doSAFRemoval(char *, char *);

/* SSI/SESSION related */
extern int startNewSession(char *, char *, char *);
extern int finishSession(char *, char *, PAAS_BOOL);
extern PAAS_BOOL isSessionInProgress();
extern int execLIFlow(char *, char *, char *, int , int );
extern int execPymtFlow(char *, char *, char *, int);
extern int execBatchTranFlow(char *, char *, int);
extern int execReportTranFlow(char *, char *, int);
extern int getLPTokenDetails(char * , char * , char * , int );
extern int setWaterMarkText(PAAS_BOOL);

extern PAAS_BOOL isPreSwipeDone();
extern int flushPreSwipeCardDetails(PAAS_BOOL);
extern int flushDupSwipeCardDetails(int*);
extern PAAS_BOOL isCustomerWaitingForCashier();
extern PAAS_BOOL isPreSwipeDtlsPresentInSession();
extern PAAS_BOOL isDupCardDtlsPresentInSession();
extern void 	 setPreSwipeDoneFlag(PAAS_BOOL);
extern PAAS_BOOL isCardDataInBQueueFlag();
extern void 	 setCardDataInBQueueFlag(PAAS_BOOL);

extern int getDHIVersionFromHost(SYSINFO_PTYPE);
/* EMV Key Load related */
extern int doEmvInitialization();

extern int procDeviceResyncKey();
//Daivik:8/7/16 - These Functions are used to flush/store/get the backup card details, which are stored when we fail track validation
//but we would not know the payment type yet.
extern int resetBkupDtlsInSession();
extern int storeBkupDtlsinSession(BACKUP_CARDDTLS_PTYPE);
extern int getBkupDtlsFromSession(BACKUP_CARDDTLS_PTYPE);
extern void getLIRunningAmounts(char *, char *, char *);

extern int  getScndData();
extern int	getScndDetailedStatus();
extern void setScndData(int);
extern void setScndDetailedStatus(int);

extern int  getAppState();
extern int	getDetailedAppState();
extern void	setAppState(int);
extern void setDetailedAppState(int );
extern void	setLastForm(int);
extern int  getLastForm();

extern int checkForScreenChange(EMVDTLS_PTYPE);

extern int	getCardReadEnabled();
extern int  getD41Active();
extern int  getAllowLITran();
extern int 	getRetryLITran();
extern void setD41Active(int);
extern void setRetryLITran(int);
extern void setAllowLITran(int);
extern void setCardReadEnabled(int);

extern void setOnlineTranStatus(int);
extern int	getOnlineTranStatus();
extern void setPOSInitiatedState(int);
extern int	getPOSInitiatedState();
extern int retValForPosIniState();

extern PAAS_BOOL isCancelRebootAllowed();
extern int storePaypalPymtCodeDtlsInSession(char *);
extern int getPaypalPymtCodeDtlsInSession(char *);
extern int getEnabledTenders(int *, char *,int *,int *, PAAS_BOOL *);
extern int applyVHQUpdates(char *, char * );
extern int validateEnabledTenders(char *pszTranKey);

extern int doDDKAdvance(char * , char *);
#define VSP_LAST_UPDATE_ORIGINAL		"/mnt/flash/config/vsp_last_keyupdate_date.txt"
#define VSP_LAST_UPDATE_SHADOW			"/mnt/flash/logs/usr1/vsp_last_keyupdate_date.txt"
//#define SAF_DATA_FILE_NAME_PERMANENT	"./flash/SAFData.db"
#define SAF_ELIGIBLE_RECORDS_FILE_NAME	"./flash/SAFRecords.db"

#define CHECK_POS_INITIATED_STATE if(getPOSInitiatedState()) {\
			cancelRequest();\
			setCardReadEnabled(PAAS_FALSE);\
			debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, retValForPosIniState());\
			APP_TRACE(szDbgMsg);\
			return retValForPosIniState();\
		}\

#endif
