/*
 * blVHQSupDef.h
 *
 *  Created on: Aug 20, 2015
 *      Author: T_MukeshS3
 */

#ifndef __BL_VHQ_SUP_H
#define __BL_VHQ_SUP_H

#include "common/common.h"
#include "bLogic/TMSAppLib.h"

#define SCA_APP_NAME				"pointMx_SCA-App"

#ifdef DEBUG
#define TMS_LIB_NAME 				"libTMS9xxD.so"
#else
#define TMS_LIB_NAME 				"libTMS9xx.so"
#endif

#define TMS_INIT					"TMS_Init"
#define TMS_GET_SVC_VERSION			"TMS_GetVersion_SVC_Agent"
#define TMS_GET_LIB_VERSION 		"TMS_GetVersion_Lib"
#define TMS_SET_APP_STATE			"TMS_ForceSetAppState"
#define TMS_UNREGISTER_APP			"TMS_Unregister"

#define VHQ_CONFIG_FILE_NAME		"/mnt/flash/system/vhq/VHQ.ini"

#define SCA_VHQ_MSGQ_PATH			"/var/tmp/scavhq.msg"		// This used to create message Q -  using ftok()
#define SCA_VHQ_MSGQ_ID 			'S'							// Message ID that used to create message Q -  using ftok()
#define SCA_VHQ_MSG_TYPE 			5678 						// message type that used to send any message in message Q

#define POS_VHQ_MSGQ_PATH			"/var/tmp/posvhq.msg"		// This used to create message Q -  using ftok()
#define POS_VHQ_MSGQ_ID 			'S'							// Message ID that used to create message Q -  using ftok()
#define POS_VHQ_MSG_TYPE 			5679 						// message type that used to send any message in message Q

#define VHQ_SUP_ENABLED				"vhqsupenabled"
#define POS_APRV_REQD_FOR_UPDATE	"posapprovalreqdforupdate"
#define POS_UPDATE_NOTIFY_TIMEOUT	"posnotifytimeout"
#define SCA_PACKAGE_NAME			"scapackagename"

#define DFLT_VHQ_EVENT_RESP_TIMEOUT	30
#define MIN_VHQ_EVENT_RESP_TIMEOUT	5		//10
#define MAX_VHQ_EVENT_RESP_TIMEOUT	600		//600
// timeout values in minutes
#define DFLT_POS_UPDATE_NOTIFY_TIMEOUT	5
#define MIN_POS_UPDATE_NOTIFY_TIMEOUT	1
#define MAX_POS_UPDATE_NOTIFY_TIMEOUT	120
//#define	VHQ_EVENT_TIMEOUT			250
#define SCA_APP_FREE 				1
#define SCA_APP_BUSY 				0

#define BLOCKED_CALL				0

#define SCA_POS_APPROVED			10
#define SCA_POS_REJECT				11
#define SCA_POS_RESP_TIMEDOUT		12
#define SCA_POS_NOTIFY_SUCCESS		13
#define SCA_POS_NOTIFY_FAIL			-1

#if 0
#define POS_SECONDARY_PORT			"secodaryport"
#define POS_UNSOLICITED				"unsolicited"
#endif

extern int		 initVHQSupport(char *);

extern PAAS_BOOL isVHQSupEnabled(void);
extern PAAS_BOOL isPOSApprReqdForUpdate(void);
extern int getVHQEventRespTimeout(void);
extern PAAS_BOOL isVHQUpdateAvailable(void);
extern void 	 setVHQUpdtAvailableFlag(PAAS_BOOL);
extern void		 setVHQUpdateStatus(int);
extern void 	 setVHQUpdateInProgressFlag(PAAS_BOOL);
extern int 		 getVHQUpdateStatus(void);
extern int 		 getVHQRcvFrmPOSMsgQId(void);
extern PAAS_BOOL getVHQUpdateInProgressFlag(void);
extern void setCriticalStepInProgressFlag(PAAS_BOOL);
extern time_t getPOSNotificationTimoutValue();
extern char* getSCAPkgName();
// function pointer to unregister the SCA-APP


/* constants for application information */
typedef enum
{
	SCA_LANE_ID = 0,
	SCA_STORE_ID,
	MAX_APP_INFO
}
SCA_APP_INFO;

/* VHQ Update Status values */
typedef enum
{
	VHQ_NO_UPDATES = 0,
	VHQ_UPDATE_AVAILABLE,				//1
	VHQ_UPDATE_PROCESSING,				//2
	VHQ_UPDATE_SUCCESS,					//3
	VHQ_UPDATE_FAILED,					//4
	VHQ_UPDATE_SUCCESS_REBOOTING,		//5
	VHQ_UPDATE_SUCCESS_RESTARTING_APP,	//6
	MAX_UPDATE_STATUS
}
VHQ_UPDATE_STATUS;

/* VHQ Settings */
typedef struct
{
	PAAS_BOOL	bVHQSupEnabled;
	PAAS_BOOL	bPOSAprvReqdForUpdate;
	int			iEventRespTimeOut;
	int			iPOSUpdateNotifyTimeOut;
	char		szSCAPkgName[LTMS_MAX_APP_NAME_LEN + 1];
}
VHQ_CFG_STYPE, *VHQ_CFG_PTYPE;

typedef struct
{
	int 	iRespType;
	char	szText[30+1];
}
POSRESPDATA_VHQ_STYPE, *POSRESPDATA_VHQ_PTYPE;


#endif /* blVHQSupDef.h_ */
