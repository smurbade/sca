
#ifndef __CDT_EXTERN_H
#define __CDT_EXTERN_H

#include "common/common.h"
#include "common/emvStructures.h"

#define CREDIT_CARD_TYPE		0
#define DEBIT_CARD_TYPE			1
#define LOYALTY_CARD_TYPE		7
#define PURCHASE_CARD_TYPE		9
#define COMMERCIAL_CARD_TYPE	10
#define GIFT_CARD_TYPE			12
#define BUSINESS_CARD_TYPE		13
#define PL_CARD_TYPE			7
#define MERCH_CREDIT_CARD_TYPE  11
#define FSA_CARD_TYPE  			14
#define EBT_CARD_TYPE			15
#define PAYACCOUNT_CARD_TYPE	16
#define POS_TENDER_TYPE			17

#define BET_FILENAME 			"./flash/kit.txt"

/* Defining some error return values for Cards */
#define	ERR_NOT_IN_BINRANGE		-501
#define	ERR_CARD_INVALID		-502
#define ERR_MANUAL_NOTALLWD		-503

typedef enum
{
	CRD_MSR					= 'M',
	CRD_NFC					= 'N',
	CRD_RFID				= 'R',
	CRD_EMV_CT				=  5,
	CRD_EMV_CTLS			=  6,
	CRD_EMV_FALLBACK_MSR	=  7,
	CRD_EMV_MSD_CTLS		= 91,	
	CRD_UNKNWN				= 'U',
	CRD_VAS_ONLY			= 'V',
	CRD_PASS_THROUGH		= 'P'	//Do not add this to the list of SSI request or SCI response. this is being used just to differentiate the pass through tranx
}
E_CARD_SRC;

typedef enum
{
	NOTRACK_INDICATOR ,
	TRACK1_INDICATOR ,
	TRACK2_INDICATOR,
	TRACK3_INDICATOR,
}
E_TRACK_INDICATOR;

typedef enum
{
	B_TYPE_REC_INDEX  = 0,
	C_TYPE_REC_INDEX  = 1,
	D_TYPE_REC_INDEX
}
STB_REC_INDEX;

typedef struct
{
	/* 
	//Praveen_P1: These variables not required to be part of this structure, thats why commenting
	int			iCardType;
	int			iCardSrc;
	int			iTrkNo;
	int			iEncType;
	char		szPymtMedia[21];
	char		szPAN[30];
	char		szName[30];
	char		szCVV[5];
	char		szCVVCode[5];
	char		szExpMon[3];
	char		szClrMon[3];
	char		szExpYear[3];
	char		szClrYear[3];
	char		szTrackData[512];
	char		szEncBlob[512];
	char		szEncPayLoad[400];
	char 		szServiceCode[5];
	char		szEmbossedPAN[30];
	char		szPINlessDebit[2];
	char 		szCardToken[41];
	double		fSigFlrLimit;
	PAAS_BOOL	bManEntry;
	PAAS_BOOL	bPINReqd;
	PAAS_BOOL	bSignReqd;
	PAAS_BOOL	bSAFAllwd;*/
	int 		iNetworkIndicator;
	char		cPinlessInd;
}NETWIDDTLS_STYPE, * NETWIDDTLS_PTYPE;

typedef struct
{
	char				cType;
	int					iBinLen;
	long 	long		lBinLow;
	long 	long		lBinHigh;
	char				cProd;
	int					iPanLen;
	int					iMaxPanLen;
	char				cCardInd;
	char				cFSAInd;
	char				cPrePaidInd;
	int					iNumEntries;
	NETWIDDTLS_STYPE	stNewtDtls[20]; //Maximum 20 will be present
	int					iTypePresent;
}
BINDTLS_STYPE, * BINDTLS_PTYPE;

typedef struct
{
	int							iCardType;
	int							iCardSrc;
	int							iTrkNo;
	int							iEncType;
	int							iAccountType;
	int							iTranSeqNum;
	int							iKeyPointer;
	int							iPINReqd;
	char						szEMVRespCode[4];		//Response code of EMV command
	char						szEMVRespCmd[4];		//Response command of EMV
	char						szEMVReqCmd[4];			//Sent/Requested EMV command
	int							EMVlangSelctd;		//Lang selected on EMV C30 Resp//TODO: check whether it requird
	char						szEMVlangPref[17];		//Pref Lang in order(At max 4, each in 2 chars)
	char						szXPIVer[50];			//XPI Version
	char						szPymtMedia[26]; 		/* -> card label also */
	char						szPAN[30];
	char						szClrPAN[30];			// Added for VSD support
	char						szName[30];
	char						szCVV[11];
	char						szCVVCode[5];
	char						szExpMon[3];
	char						szClrMon[3];
	char						szExpYear[3];
	char						szClrYear[3];
	char						szTrackData[512];
	char						szClrTrkData[512];	// added for VSD encryption; can be used for other purpose also when VSD is enabled in terminal
	char						szEncBlob[512];
	char						szEMVEncBlob[1024];		//EMV Enc Blob will be saved in here in case of RSA encrypt
	char						szEncPayLoad[400];
	char						szVSDInitVector[32+1];
	char 						szServiceCode[5];
	char						szEmbossedPAN[30];
	char						szPINlessDebit[2];
	char						szBarCode[101];
	char						szPINCode[15];
	char						szPayPalPymtCode[15];
	char						szEMVTags[1024];
	char						szEmvReversalType[5];	// For EMV Void Transactions
	char						szOrigPAN[30];			// For Refund Transactions, Masked A/c Num from SCI request
	char						szPayPassType[3];
	char						szServiceCodeByteOne[3];
	char						szPrevTranAmt[20];		//Previous Transaction amount for Apple Wallet
	char						szCardAbbrv[3];		//Card Abbv From CDT File
	double						fSigFlrLimit;
	double						fSafFlrLimit;
	PAAS_BOOL					bManEntry;
	PAAS_BOOL					bSignReqd;
	PAAS_BOOL					bSAFAllwd;
	PAAS_BOOL					bSAFForcefully;
	PAAS_BOOL					bEncReqd;
	PAAS_BOOL					bEmvData;
	PAAS_BOOL					bEmvFallback;
	PAAS_BOOL					bPartialEMV;
	PAAS_BOOL					bKohlsChargeCard;
	PAAS_BOOL					bBtnOnPinScrnPressed;
	PAAS_BOOL					bTogglePymtType;
	BINDTLS_STYPE				stBinDtls[3];	 //Maximum for each card three records will be present
	EMV_APPDTLS_STYPE			stEmvAppDtls;
	EMV_TRANDTLS_STYPE			stEmvtranDlts;
	EMV_ISSUERDTLS_STYPE		stEmvIssuerDtls;
	EMV_TERMINALDTLS_STYPE		stEmvTerDtls;
	EMV_CRYPTGRMDTLS_STYPE		stEmvCryptgrmDtls;
	EMV_CARD_STATUS_U02_STYPE	stEMVCardStatusU02;
	VASDATA_DTLS_PTYPE			pstVasDataDtls;
}
CARDDTLS_STYPE, * CARDDTLS_PTYPE;

/* structure to hold raw card data */
typedef struct
{
	char		szTrack1Data[512];
	char		szTrack2Data[512];
	char		szTrack3Data[512];
	char		szPAN[30];
	char		szClrMon[3];
	char		szClrYear[3];
	char		szName[30];
	char		szPymtType[20];
	char		szPymtMedia[21];
	char		szCardEntryMode[20];
	char		szRsltCode[10];
}
RAW_CARDDTLS_STYPE, *RAW_CARDDTLS_PTYPE;

typedef struct
{
	char	szProdSet[10];
	char	szDetailIndSet[20];
	char	szPinLessIndSet[10];
	char	szDecisionSet[7];
}
STBFILERECDTLS_STYPE, * STBFILERECDTLS_PTYPE;

typedef struct BETRange
{
	long 	int		 lower;
	long 	int 	 upper;
	struct	BETRange *next;
}
BETRECORD_STYPE, *BETRECORD_PTYPE;

extern int commitCDTChanges();
extern int modifyCDT(int, char *, char *);
extern int updateCardRangeField(char *, char *);
extern int getCardDtls(CARDDTLS_PTYPE, char **, char** , char**, char *, PAAS_BOOL, char *);
extern int getPANFromTrack(char ** ,char *,int);
extern int validateAndUpdateCardDtls(CARDDTLS_PTYPE,char *);
extern int isKohlsChargeCard(CARDDTLS_PTYPE);
extern int getCrdTypeByCDT(int *,CARDDTLS_PTYPE, PAAS_BOOL);
extern int getPymtMediafromCDT(CARDDTLS_PTYPE, int);
extern int getPymtMediafromCDTandEnabledTndr(CARDDTLS_PTYPE pstCardDtls);
extern PAAS_BOOL checkBinExclusion(char *);
extern int getSTBDtls(CARDDTLS_PTYPE , char *);

#endif
