#ifndef __POS_DSTORE_H
#define __POS_DSTORE_H

#include "common/common.h"

#define POS_AUTH_FILE	"./flash/posAuth.dat"

typedef struct
{
	ulong			tranCtr;
	char			MACLabel[15];
	uchar			MACKey[16];
	char			szTStamp[15]; /* yyyymmddhhmmss format */
}
POSID_STRUCT_TYPE, *POSID_PTR_TYPE;

/* APIs exposed to handle the POS authentication records */
extern int			loadPosInfoRecords();
extern int			deletePosInfoRecord(char *);
extern int			deleteAllPosInfoRecord();
extern int			addPosInfoRecord(uchar *, char *);
extern int 			savePosInfoRecords();
extern int			getAESKey(char *, uchar *, char *);
extern int			getTranCounter(char *, ulong *);
extern int			updateTranCounter(char *, ulong);
extern void 		getMACLabel(char *);
extern int  		getDevMacLabels(char*);
extern PAAS_BOOL	isPOSRegistered(char*);

#endif
