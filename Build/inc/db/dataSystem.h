#ifndef __DATA_SYSTEM_H
#define __DATA_SYSTEM_H

#include "common/tranDef.h"

#define SCA_PASSTHRGH_FIELDS_FILE_NAME "/home/usr1/flash/scaPassThrgFields.INI"

/* Common utilities etc */
extern int initDataSystem();
extern int closeDataSystem();
extern int createQueue(char *);
extern int createStack(char *);
extern int deleteQueue(char *);
extern int deleteStack(char *);
extern int flushQueue(char *);
extern int flushStack(char *);
extern int addDataToQ(void *, int, char *);
extern int addDataToStack(void *, int, char *);
extern void * getDataFrmQ(char *, int *);
extern void * popDataFrmStack(char *, int *);
extern void * peekDataFrmQ(char *, int *);
extern void * getDataFrmStack(char *, int *);
extern void   releaseListQueueMutexLocks();
extern PASSTHRG_FIELDS_PTYPE getPassThrgFieldsDtls(void);

/* SCI TRAN (MAIN TRAN BODY) */
extern int createTran(char *);
extern int getTran(char *, TRAN_PTYPE *);
extern int deleteTran(char *);

/* SSI TRAN */
extern int pushNewSSITran(char *);
extern int getTopSSITran(char *, TRAN_PTYPE *);
extern int popSSITran(char *);

/* For Session */
extern int openNewSession(char *);
extern int getSession(char *, SESSDTLS_PTYPE *);
extern int getSessionDtls(SESSDTLS_PTYPE *);
extern int closeSession(char *);
/*Akshaya :21-07-16 . Apple Pay :Ignoring the Implementation if having NFCVAS_MODE and MERCHANT_INDEX are sent in SHOW Line Item Command.*/
#if 0
extern int getLIShowReqDtls(char *, SHWLIINFO_PTYPE);
extern int updateLIShowReqDtlsInSession(char * , SHWLIINFO_PTYPE);
#endif
extern int flushPreSwipeVASDtls(char *);
#endif
