#ifndef __SAFDATASTORE_H
#define __SAFDATASTORE_H

#define SAF_NUM_RECORD_SEPARATOR	50

#define SAF_STATUS_QUEUED       	1
#define SAF_STATUS_PROCESSED    	2
#define SAF_STATUS_INPROCESS    	3
#define SAF_STATUS_DECLINED     	4
#define SAF_STATUS_NOTPROCESSED 	5
#define	SAF_TOR_STATUS_PENDING		6
#define SAF_TOR_STATUS_REVERSED		7
#define SAF_TOR_STATUS_NOTALLOWED	8
#define SAF_TOR_STATUS_NOTFOUND		9
#define	SAF_STATUS_ALL				10
#define	SAF_TOR_STATUS_ALL			11

typedef enum
{
	SAF_IN_IDLE = 0,
	SAF_IN_FLIGHT,
}
SAF_INPROGRESS_STATUS;

extern double getSAFTotalAmount();
extern int   getSAFTotalRecords();
//extern int   getCurrentSAFRecordNumber();
extern time_t  getDeviceOfflineTimeInSeconds();
extern int   readSAFRecordNumber();
//extern int   updateSAFRecordNumber();
extern int   readSAFRecords();
extern int 	 clearAllSAFRecords();
extern int   clearOneSAFRecord();
extern int 	 clearRangeofSAFRecords();
extern int	 getToRemoveSAFRecordCount();
//extern int   getSAFRemovedNodeDetails(SAF_RESP_RECORD_PTYPE , int );
extern int 	 getSAFRecordSize();
extern int 	 addSAFRecordToFile(char *, int);
extern int   findNumEligibleRecords();
extern int   readSAFData();
extern int 	 readQueuedSAFRecord();
extern int 	 updateSAFDetails(char *, double , char *);
extern int   updateSAFDataToFile();
extern int   getSAFRecordCount();
extern int	 getSAFDtlsAll(SAFDTLS_PTYPE, PAAS_BOOL );
extern int 	 getSAFDtlsByRecStatus(SAFDTLS_PTYPE, PAAS_BOOL );
extern int 	 getSAFDtlsByRecStatusCumRecNum(SAFDTLS_PTYPE, PAAS_BOOL);
extern int 	 getSAFDtlsByRecNum(SAFDTLS_PTYPE , PAAS_BOOL);
extern int	 getSAFDtlsByRecRange(SAFDTLS_PTYPE , PAAS_BOOL);
extern int   getSAFDtlsByRecStatusCumRange(SAFDTLS_PTYPE , PAAS_BOOL);
extern int   resetSAFTranDetails();
extern int   postSAFRecordToPWC( );
extern int	 purgeSAFRecords();
extern int   saveSAFResp();
extern int   initSAFFileMutex();
extern int 	 writeTimeStamptoSAFFile(int );
extern int   resetSAFLastOfflineEpochTime();
extern PAAS_BOOL isSAFRecordPresent();
extern int   processTransactionOffline(char *, char * );
extern int 	 safPatchCode();
extern int 	 initSAF();
extern int   waitForHostConnection(PAAS_BOOL *, int );
extern PAAS_BOOL checkResultCodeinSAFErrCodeList(int );
extern int 	isSAFRecordsPendingInDevice();

//EMV testing
extern int updateTranWithEmvSAFDetails(char *);

//Host Response Details
typedef struct
{
	char 		szPWCSAFResponseCode[10];
	char 		szPWCSAFTroutd[15];
	char 		szPWCSAFCTroutd[15];
	char 		szPWCSAFAuthCode[15];
	char 		szPWCSAFCardToken[41];
	char 		szPWCSAFBankUserData[51];
	char 		szPWCSAFCVVCode[5];
	char 		szTxnPOSEntryMode[4];
	char 		szMerchId[26];
	char 		szTermId[16];
	char 		szLaneId[16];
	char 		szStoreId[16];
	char		szApprvdAmt[20];		/* APPROVED_AMOUNT */
	char		szLPToken[20];			/* LP TOKEN */
	char		szPPCV[20];				/* PPCV */
	char		szPaymentMedia[21];
	char		szAVSCode[3];
	char		szHostRespCode[50];
	char		szSignatureRef[12];
	char 		szAprType[2];			/* APR_TYPE */
	char		szPurchaseApr[13];		/* PURCHASE_APR */
	char		szSvcPhone[11];			/* SVC_PHONE */
	char 		szStatusFlag[2];		/* STATUS_FLAG */
	char		szReceiptText[4096];	/* RECEIPT_TEXT */
	char		szActionCode[3];		/* ACTION_CODE */
	char		szCreditPlanNbr[6];		/* CREDIT_PLAN_NBR */
	char		szAcctNum[30];
	char		szAthNtwID[4];
	char		szTransDate[15];
	char		szTransTime[15];
}
HOSTRESPDTLS_STYPE, * HOSTRESPDTLS_PTYPE;

#endif
