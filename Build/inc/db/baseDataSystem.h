#ifndef __BASE_DS_H
#define __BASE_DS_H

#include "common/common.h"

typedef void (* DATA_FXN)(void *);

typedef struct __nodeMetaInfo
{
	int				iDataSize;
	DATA_FXN		initData;	
	DATA_FXN		cleanupData;
}
DATAINFO_STYPE, * DATAINFO_PTYPE;

extern int	initDataSystem();
extern int	flushDataSystem();

extern int	flushDataForKey(char *, int);
extern int	addData(DATAINFO_PTYPE, char *);
extern void * getData(char *);

#endif
