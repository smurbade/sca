#ifndef __TRAN_DS_H
#define __TRAN_DS_H

#include "common/tranDef.h"

extern int getFxnNCmdForTran(char *, int *, int *);
extern int getFxnNCmdForSSITran(char *, int *, int *);
extern int getGenRespDtlsForSSITran(char *, RESPDTLS_PTYPE);
extern int getOtherGenRespDtlsForSSITran(char *, OTHER_RESPDTLS_PTYPE);
extern int resetGenRespDtlsForSSITran(char *);
extern int getGenRespDtlsForTran(char *, RESPDTLS_PTYPE);
extern int getPOSSecDtlsForTran(char *, POSSEC_PTYPE);
extern int getSigDtlsForDevTran(char *, SIGDTLS_PTYPE);
extern int getLtyDtlsForDevTran(char *, LTYDTLS_PTYPE);
extern int getProvisionPassDtlsForDevTran(char * , PROVISION_PASS_PTYPE );
extern int getVerDtlsForDevTran(char * , VERDTLS_PTYPE );
extern int getSetParmDtlsForDevTran(char * , SETPARM_PTYPE );
extern int getDispMsgDtlsForDevTran(char * , DISPMSG_PTYPE );
extern int getGetParmDtlsForDevTran(char * , GETPARM_PTYPE );
extern int getTimeDtlsForDevTran(char *, TIMEDTLS_PTYPE);
extern int getCharityDtlsForDevTran(char *, CHARITYDTLS_PTYPE);
extern int getCardDataDtlsForDevTran(char * , GET_CARDDATA_PTYPE);
extern int getCreditAppDtlsForDevTran(char * , CREDITAPPDTLS_PTYPE );
extern int getCustButtonDtlsForDevTran(char * , CUSTBUTTONDTLS_PTYPE);
extern int getLaneClosedDtlsForDevTran(char *, LANECLOSED_PTYPE);
extern int getCounterDtlsForDevTran(char *, COUNTERDTLS_PTYPE);
extern int getDevNameDtlsForDevTran(char *, DEVICENAME_PTYPE);
extern int getSurveyDtlsForDevTran(char *, SURVEYDTLS_PTYPE);
extern int getSAFDtlsForDevTran(char *, SAFDTLS_PTYPE);
extern int getSessDtlsFrmPOSReq(char *, SESSDTLS_PTYPE);
extern int getLIReqDtlsForTran(char *, LIINFO_PTYPE);
extern int getLIDtlsFromSession(char *, LIINFO_PTYPE);
extern int getPymntTypeDtlsFromSession(char *, int *);
extern int getPymtTypeForPymtTran(char *, int *);
//extern int getVASmodeForPymtTran(char *, PAAS_BOOL *);
extern int getPymtSubTypeForPymtTran(char *, int *);
extern int getCustInfoDtlsForPymtTran(char *, CUSTINFODTLS_PTYPE);
extern int getHashVaueOfCurPANForPymtTran(char *, char *);
extern int getPaypalPaymentCodeForPymtTran(char * , char *);
extern int getPrivCrdSAFTranIndForPymtTran(char *, PAAS_BOOL *);
extern int getPymtFlowForPymtTran(char *, PAAS_BOOL *);
extern int getCardTypeForPymtTran(char *, char *);
extern int getTranDateForPymtTran(char *, char *);
extern int getTranTimeForPymtTran(char *, char *);
extern int getForceFlagForPymtTran(char *, PAAS_BOOL *);
extern int getSAFTranIndicatorForPymtTran(char * , PAAS_BOOL * );
extern int getManualFlagForPymtTran(char *, PAAS_BOOL *);
extern int getDCCNotAllowedFlagForPymtTran(char*, PAAS_BOOL*);
extern int getManPromptOptionsForPymtTran(char *, int *);
extern int getDupTranDetectionDtlsForPymtTran(char *, char *, PAAS_BOOL *);
extern int getBatchTraceIdDtlsForPymtTran(char * , char *);
extern int getSplitTndrFlagForPymtTran(char *, PAAS_BOOL *);
extern int getSessDtlsForPymtTran(char *, SESSDTLS_PTYPE);
extern int getAmtDtlsForPymtTran(char *, AMTDTLS_PTYPE);
extern int getCardDtlsForPymtTran(char *, CARDDTLS_PTYPE);
extern int getEBTDtlsForPymtTran(char * , EBTDTLS_PTYPE );
extern int getFSADtlsForPymtTran(char * , FSADTLS_PTYPE );
extern int getCardDtlsForRptTran(char * , CARDDTLS_PTYPE );
extern int getDupFieldDtlsForPymtTran(char * , DUPFIELDDTLS_PTYPE );
extern int getChkDtlsForPymtTran(char * , CHKDTLS_PTYPE );
extern int getEmvDtlsForPymtTran(char *, EMVDTLS_PTYPE);
extern int getEmvRespDtlsForPymtTran(char *, EMV_RESP_DTLS_PTYPE);
extern int getEmvKeyLoadDtlsForPymtTran(char *, EMVKEYLOAD_INFO_PTYPE);
extern int getPINDtlsForPymtTran(char *, PINDTLS_PTYPE);
extern int getSigDtlsForPymtTran(char *, SIGDTLS_PTYPE);
extern int getLevel2DtlsForPymtTran(char *, LVL2_PTYPE);
extern int getCmdTypeForPymtTran(char *, int *);
extern int getLtyDtlsForPymtTran(char *, LTYDTLS_PTYPE);
extern int getFollowOnDtlsForPymt(char *, FTRANDTLS_PTYPE);
extern int getCurTranDtlsForPymt(char *, CTRANDTLS_PTYPE);
extern int getVSPDtlsForPymtTran(char *, VSPDTLS_PTYPE);
extern int getVASDtlsForPymtTran(char * , VASDATA_DTLS_PTYPE );
extern int getDupTranIndicatorForPymtTran(char * , PAAS_BOOL *);
extern int getPostAuthCardDtlsIndicatorForPymtTran(char * , PAAS_BOOL *);
extern int getDevTranDtls(char *, DEVTRAN_PTYPE*);
extern int getPaymentTypesDtlsForPymtTran(char * , char *);
extern int getPOSTendersDtlsForPymtTran(char * , POSTENDERDTLS_PTYPE);
extern int getBillPayDtlsForPymtTran(char * , PAAS_BOOL *);
extern int getVHQAplyUpdtDtlsForDevTran(char * , APPLYUPDATES_PTYPE );
extern int getEarlyRtrnCrdFlagForPymtTran(char *, PAAS_BOOL *);
extern int getEarlyCrdPrsntFlagFromSession(char *, int *);
extern int getRawCardDtls(RAW_CARDDTLS_PTYPE *, CARDDTLS_PTYPE, PAAS_BOOL);
extern int getPassThrghDtlsForPymtTran(char *, PASSTHRG_FIELDS_PTYPE);
extern int getDccDtlsForPymtTran(char *, DCCDTLS_PTYPE);
extern int getDispQRCodeDtlsForDevTran(char *, DISPQRCODE_DTLS_PTYPE);
extern int getCustCheckBoxDtlsForDevTran(char *, CUSTCHECKBOX_DTLS_PTYPE);

extern int setFxnNCmdForTran(char *, int, int);
extern int setFxnNCmdForSSITran(char *, int, int);
extern int setFxnForSSITran(char * , int );
extern int setStatusInTran(char *, int);
extern int setGenRespDtlsinTran(char *, char *);
extern int setDupTranIndicatorForPymtTran(char *, PAAS_BOOL);
extern int setPostAuthCardDtlsIndicatorForPymtTran(char * , PAAS_BOOL );
extern int setHashVaueOfCurPANForPymtTran(char *, char *);
extern int updateGenSessDtls(char *, SESSDTLS_PTYPE);
extern int updateLIDtlsInSession(char *, LIINFO_PTYPE);
extern int updatePOSSecDtlsForTran(char *, POSSEC_PTYPE);
extern int updateSigDtlsForDevTran(char *, SIGDTLS_PTYPE);
extern int updateLtyDtlsForDevTran(char *, LTYDTLS_PTYPE);
extern int updateVASDtlsForDevTran(char * , VASDATA_DTLS_PTYPE);
extern int updateSurveyDtlsForDevTran(char * , SURVEYDTLS_PTYPE );
extern int updateCharityDtlsForDevTran(char * , CHARITYDTLS_PTYPE );
int updateGetCardDataDtlsForDevTran(char * , GET_CARDDATA_PTYPE);
extern int updateCreditAppPromptDtlsForDevTran(char * , CREDITAPPDTLS_PTYPE );
extern int updateCustButtonDtlsForDevTran(char * szTranKey, CUSTBUTTONDTLS_PTYPE pstCustButton);
extern int updateSAFDtlsForDevTran(char *, SAFDTLS_PTYPE);
extern int updatePymtTypeForPymtTran(char *, int);
extern int updatePymtSubTypeForPymtTran(char * , int );
extern int updateSAFTranIndicatorForPymtTran(char *, PAAS_BOOL);
extern int updatePrivCrdSAFTranIndForPymtTran(char *, PAAS_BOOL);
extern int updateVSPTranIndicatorForPymtTran(char *, PAAS_BOOL);
extern int updatePaymntFlowIndicForPymtTran(char *, PAAS_BOOL);
extern int updatePaypalPymtCodeForPymtTran(char * , char * );
extern int updatePaypalCheckIdKeyForPymtTran(char * , char * );
extern int updateCardTypeForPymtTran(char *, char *);
extern int updateTranDateForPymtTran(char *, char *);
extern int updateTranTimeForPymtTran(char *, char *);
extern int updateForceFlagForPymtTran(char *, PAAS_BOOL);
extern int updateFSADtlsForPymtTran(char *, FSADTLS_PTYPE);
extern int updateEBTDtlsForPymtTran(char *, EBTDTLS_PTYPE);
extern int updateManualFlagForPymtTran(char *, PAAS_BOOL);
extern int updateBatchTraceIdForPymtTran(char * , char * );
extern int updateSplitTndrFlagForPymtTran(char *, PAAS_BOOL);
extern int addSessionToPymtTran(char *, char *);
extern int updateAmtDtlsForPymtTran(char *, AMTDTLS_PTYPE);
extern int updateCardDtlsForPymtTran(char *, CARDDTLS_PTYPE);
extern int updatePOSTenderDtlsForPymtTran(char *, POSTENDERDTLS_PTYPE);
extern int updateCustInfoDtlsForPymtTran(char *, CUSTINFODTLS_PTYPE);
extern int updateVASDtlsForPymtTran(char * , VASDATA_DTLS_PTYPE );
extern int updateEmvKeyLoadDtlsForPymtTran(char *, EMVKEYLOAD_INFO_PTYPE);
extern int updateEmvDtlsinCardDtlsForPymtTran(char *, EMVDTLS_PTYPE);
extern int updatePINDtlsForPymtTran(char *, PINDTLS_PTYPE);
extern int updateSigDtlsForPymtTran(char *, SIGDTLS_PTYPE);
extern int updateTaxDtlsForPymtTran(char *, LVL2_PTYPE);
extern int updateLtyDtlsForPymtTran(char *, LTYDTLS_PTYPE);
extern int updateFollowOnDtlsForPymt(char *, FTRANDTLS_PTYPE);
extern int updateCurTranDtlsForPymt(char *, CTRANDTLS_PTYPE);
extern int updateVSPDtlsForPymtTran(char *, VSPDTLS_PTYPE);
extern int updateGenRespDtlsForPymtTran(char * , RESPDTLS_PTYPE );
extern int updatePymntDtlsForBAPI(char* , char* , int );
extern int updateSessDtlsForPymtTran(char * , SESSDTLS_PTYPE );
extern int updateTimeDtlsForDevTran(char *, TIMEDTLS_PTYPE);
extern int updateVerDtlsForDevTran(char * , VERDTLS_PTYPE);
extern int updateCounterDtlsForDevTran(char * , COUNTERDTLS_PTYPE);
extern int updateDevNameDtlsForDevTran(char * , DEVICENAME_PTYPE);
extern int updateGetPymtTypesDtlsForDevTran(char * , PAYMENTTYPES_PTYPE);
extern int updateEmpIDDtlsForDevTran(char * , EMPID_PTYPE );
extern int updateDevTranDtls(char *, DEVTRAN_PTYPE);
extern int clearCashbackDtlsForUserCancel(char *);
extern int updateDispMsgDtlsForDevTran(char*, DISPMSG_PTYPE);
extern int updateBillPayForPymtTran(char *, PAAS_BOOL);
extern int updateGetParmDtlsForDevTran(char *, GETPARM_PTYPE);
extern int updateRawCardDtlsForPymtTrans(char * , CARDDTLS_PTYPE , char **);
extern int updatePassThrghDtlsForPymtTran(char * , PASSTHRG_FIELDS_PTYPE );
extern int copyAndUpdatePassThrghDtlsForPymtTran(char * pszTranStkKey, PASSTHRG_FIELDS_PTYPE pstPassThrghDtls);
extern int updateQueryNfcIniDtlsForDevTran(char * , NFCINI_CONTENT_INFO_PTYPE);
//extern int updateVASmodeForPymtTran(char * , PAAS_BOOL);
extern int updateDccDtlsForPymtTran(char *, DCCDTLS_PTYPE);
extern int updateQRCodeDtlsInSession(char*, DISPQRCODE_DTLS_PTYPE);
extern int updateCustCheckBoxDtlsForDevTran(char *, CUSTCHECKBOX_DTLS_PTYPE);
#endif
