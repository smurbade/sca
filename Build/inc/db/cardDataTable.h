#ifndef __CARD_DATA_TBL
#define __CARD_DATA_TBL

typedef struct
{
	int		iCardType;				/* Type of card used for the transaction */
	char	szPANLow[21];			/* Account number low end of the range */
	char	szPANHigh[21];			/* Account number High end of the range */
	int		iPANMinLength;			/* Minimum Number of PAN digits */
	int		iPANMaxLength;			/* Maximum Number of PAN digits */
	int		iAVSCode;				/* Address Verification System  */
	int		iTipDiscount;			/* Card Issuer discount rate to be applied to tip amount */
	int		iCVV2Code;				/* Card verification value type 2 */
	char	szCardAbbrevation[3];	/* 2-letter card abbreviation */
	char	szCardLabel[21];		/* 20-letter card label */
	char	szIPCLabel[21];			/* Connect label */
	char	szIPCProcessor[21];		/* Connect processor; updated via Admin */
	char	szTrackRequired[4];		/* Tracks Required */
	char	szSAFFloorLimit[11];		/* Store/Fwd Floor Limit */
	char	szSigFloorLimit[11];	/* Signature Limit Amount */
	char	szGiftMinAmount[11];		/* Identifies the minimum amount that a gift card can be activated for */
	char	szGiftMaxAmount[11];		/* Identifies the maximum amount that a gift card can be activated for */
	int		iCardDisabled;			/* Disables the current card range */
	int		iLUHNCheckRequired;		/* Enables/ disables the check for the LUHN on the card */
	int		iExpiryDateRequired;	/* Enables/ disables the requirement of an expiration date */
	int		iManualEntryAllowed;	/* Enables/ disables the allowance of manual entry of the account number for a transaction */
	int		iSigLineRequired;		/* Enables/ disables the signature line on receipts */
	int		iPINRequired;			/* Determines whether the PINPad is required */
}
OLD_CDTREC_STYPE, * OLD_CDTREC_PTYPE;

#define OLD_CDTREC_SIZE		sizeof(OLD_CDTREC_STYPE)

/* Card Record Details */
typedef struct
{
	int			iCardType;			/* Type of card */
	int			iMinPANLen;			/* Minimum Number of PAN digits */
	int			iMaxPANLen;			/* Maximum Number of PAN digits */
	int			iAVSCode;			/* Address Verification System  */
	int			iTipDscnt;			/* Discount rate to be applied to tip */
	int			iCVV2;				/* Card verification value type 2 */
	char		szPANLo[21];		/* Account number low end of the range */
	char		szPANHi[21];		/* Account number High end of the range */
	char		szCardAbbr[3];		/* 2-letter card abbreviation */
	char		szCardLbl[21];		/* 20-letter card label */
	char		szIPCLbl[21];		/* Connect label */
	char		szIPCProcId[21];	/* Connect processor; updated via Admin */
	char		szTrkRqd[4];		/* Tracks Required */
	char		szSAFFlrLim[11];		/* Store/Fwd Floor Limit */
	char		szSigFlrLim[11];	/* Signature Limit Amount */
	char		szGiftMinAmt[11];	/* Min amt gift card can be activated for */
	char		szGiftMaxAmt[11];	/* Max amt gift card can be activated for */
	PAAS_BOOL	bLUHNChkRqd;		/* for LUHN check for the card */
	PAAS_BOOL	bExpDateChkRqd;		/* for expiration date check for the card */
	PAAS_BOOL	bManEntryAllwd;		/* manual entry of card details for tran */
	PAAS_BOOL	bSigLineRqd;		/* Signature line on receipts */
	PAAS_BOOL	bPINRqd;			/* PINPad required or not */
	PAAS_BOOL	bDisableCR;			/* Disables the current card range */
}
CDTREC_DTLS_STYPE, * CDTREC_DTLS_PTYPE;

#define	CDTREC_DTLS_SIZE	sizeof(CDTREC_DTLS_STYPE)

/* Card record of the CDT table */
typedef struct _stCDTRec
{
	CDTREC_DTLS_STYPE	recDtls;
	struct _stCDTRec *	nextRec;
}
CDTREC_STYPE, * CDTREC_PTYPE;

#define CDTREC_SIZE		sizeof(CDTREC_STYPE)

/* CDT table itself */
typedef struct __stCDT
{
	int				iRecCnt;
	CDTREC_PTYPE	cdtRecsHead;
	CDTREC_PTYPE	cdtRecsTail;
}
CDT_STYPE, * CDT_PTYPE;

typedef enum
{
	CARD_TYPE = 0, /* For POS Register/Unregister */
	PAN_LOW,
	PAN_HIGH,
	PAN_MIN_LEN,
	PAN_MAX_LEN,
	AVS,
	TIP_DISCOUNT,
	CVV_CODE,
	CARD_ABB,
	CARD_LABEL,
	IPC_LABEL,
	IPC_PROCESSOR,
	TRACK_REQ,
	SAF_LIMIT,
	SIG_LIMIT,
	GIFT_MIN_AMOUNT,
	GIFT_MAX_AMOUNT,
	CARD_DISABLED,
	LUHN_REQ,
	EXPIRY_REQ,
	MANUAL_ALLOWED,
	SIG_LINE_REQ,
	PIN_REQ,
	NOT_IN_USE
}
CDT_LABEL_ENUM;

#endif
