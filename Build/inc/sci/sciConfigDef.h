/*
 * sciConfigDef.h
 *
 *  Created on: Dec 16, 2013
 *  Author: kranthik1
 */

#ifndef SCICONFIGDEF_H_
#define SCICONFIGDEF_H_

#define SECTION_DEVICE				"DCT"
#define SECTION_PAYMENT				"PDT"
#define SECTION_RECEIPT				"RCT"
#define SECTION_DHI					"DHI"
#define RECEIPT_ENABLED				"receiptenabled"
#define LISTEN_PORT					"deviceport"
#define	LISTEN_SCND_PORT			"devicescndport"
#define	SCND_PORT_ENABLED			"scndportenabled"
#define	MAX_POS_CONNECTIONS			"maxposconnections"
#define POS_AUTHENTICATION_REQD	 	"posauthenticationreqd"
#define	PROCESSOR					"processor"

/* --- RECEIPT SECTION - RCT --- */
#define RCT_HEADER_PREFIX		"header_"
#define RCT_FOOTER_PREFIX		"footer_"
#define RCT_DCLAIM_PREFIX		"disclaimer_"
#define RCT_DCC_DCLAIM_PREFIX	"dccdisclaimer_"

#define DFLT_LISTEN_PORT			5015
#define DFLT_LISTEN_SCND_PORT		5016
#define DFLT_MAX_POS_CONNECTIONS	10

typedef struct
{
	char*				headers[4];
	char*				footers[4];
	char*				disclaim[4];
	char* 				dccDisclaim[4];
}
RCPT_DATA_STYPE, * RCPT_DATA_PTYPE;

typedef struct
{
	int					listenPort;
	int					listenScndPort;
	int					maxReceiptLineLen;
	int					maxPOSConnections;
	PAAS_BOOL			receiptEnabled;
	PAAS_BOOL			scndPortEnabled;
	PAAS_BOOL			bPOSAuthReqd;
	RCPT_DATA_STYPE		stRcptCfg;
}
SCI_CFG_STYPE, * SCI_CFG_PTYPE;


extern int 		 setReceiptHeader(int, char *);
extern int 		 setReceiptFooter(int, char *);
extern int 		 getRcptDisclaimers(char **);
extern int 		 setReceiptDisclaimer(int, char *);
extern int  	 getMaxReceiptLineLen();
extern int 		 getListeningPortNum();
extern int 		 getListeningScndPortNum();
extern int 		 getMaxPOSConnections();
extern PAAS_BOOL isReceiptTemplatePresent();
extern PAAS_BOOL isReceiptEnabled();
extern PAAS_BOOL isPOSAuthReqdFromConfig();
extern PAAS_BOOL isSecondaryPortEnabled();
extern PAAS_BOOL isDHIEnabled();


#endif /* SCICONFIGDEF_H_ */
