#ifndef __SCI_MAIN_H
#define __SCI_MAIN_H

typedef enum
{
	DATA,
	DISCONN,
	CANCEL,
	UIDATA,
	REBOOT,
	STATUS,
	UPDATE_QUERY,
	UPDATE_STATUS
}
SCI_REQTYPE;

typedef enum
{
	PRIMARY = 0,
	SECONDARY
}
PORT_TYPE_ENUM;

typedef enum
{
	NOSTATE = 0,
	POSCANCELLED,
	POSDISCONNECTED,
	TRANCOMPLETED
}
SECONDARY_PORT_STATE;

typedef struct
{
	int		iPortType;
	int		iEvtType;
	int		iSize;
	char	szConnInfo[25];	/* ip:port */
	uchar *	szMsg;			/* xml message */
}
SCIDATA_STYPE, * SCIDATA_PTYPE;

extern int initSCIModule(char *);
extern int closeSCIModule();
extern int processSCIReq(char **, int, char *, char *, char *, int);
extern int sendSCIResp(char *, char *, char *);

/* Temp... for debuggging */
extern void printQIds();

#endif
