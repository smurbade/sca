#ifndef __SCI_IFACE_H
#define __SCI_IFACE_H

#include "common/common.h"

typedef struct
{
	int		iLen;
	char	szConnInfo[25];	/* ip:port */
	uchar *	szMsg;			/* xml message */
}
MSG_STYPE, * MSG_PTYPE;

typedef void (* DATA_CALLBK)(uchar *, int, char *, int);
typedef void (* DISCON_CALLBK)(char *, void *);
typedef void (* CONN_CALLBK)(char *);

typedef struct
{
	int				iListenPrimPort;
	int				iListenScndPort;
	int				iMaxConn;
	char			szDevIP[16];
	char			szSndQ[10];
	CONN_CALLBK		connFunc;
	DATA_CALLBK		dataFunc;
	DISCON_CALLBK	disconFunc;
}
SCIINIT_STYPE, * SCIINIT_PTYPE;

typedef struct __stConn
{
	int					fd;
	char				szConnInfo[25];
	PAAS_BOOL			bDataSent;
	struct __stConn *	next;
}
CONN_STYPE, * CONN_PTYPE;

typedef struct
{
	int				iListenPrimFd;
	int				iListenScndFd;
	int				iCurPrimConnCnt;
	int				iCurScndConnCnt;
	CONN_PTYPE		pstPrimPortConnList;
	CONN_PTYPE		pstScndPortConnList;
	SCIINIT_STYPE	stSCIParam;
	pthread_mutex_t	listLock;
}
SCI_STYPE, * SCI_PTYPE;

/* Extern functions */
extern int initSCIIface(SCIINIT_PTYPE);
extern int closeSCIIface();
extern int sendDatatoSCIServer(uchar *, int , char *, int );
extern int getFDFrmConnList(char * , int * pFd, SCI_PTYPE , CONN_PTYPE *);
#endif
