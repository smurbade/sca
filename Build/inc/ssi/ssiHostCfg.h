#ifndef __PWC_CFG_DEF_H
#define __PWC_CFG_DEF_H

#include "common/common.h"

/* PWC host url types */
typedef enum
{
	PRIMARY_URL = 0,
	SECONDARY_URL
}
HOST_URL_ENUM;

#define AVOID_HOST_CONN_CHECK 3
#define USE_DEFAULT_URL -1

/* Host definitions */
typedef struct
{
	char *		url;
	int			portNum;
	long		conTimeOut;
	long		respTimeOut;
}
HOSTDEF_STRUCT_TYPE, * HOSTDEF_PTR_TYPE;

typedef struct
{
	char userID[100];			/* USER ID */
	char userPWD[50];			/* USER PASSWORD */
	char clientID[50];			/* CLIENT ID */
	char serialNo[100];			/* DEVICE SERIAL NUMBER */
	char deviceType[50];		/* DEVICE TYPE. e.g Mx915, Mx925 */
	char deviceKey[200];		/* DEVICE KEY */
	char deviceName[100];		/* Device Name (DEVICE TYPE + CLIENT ID) */
}
PWC_AUTH_DTLS_S_TYPE, *PWC_AUTH_DTLS_P_TYPE;

#define DFLT_KEEP_ALIVE_INTERVAL	10
#define KEEP_ALIVE_DISABLED			0
#define KEEP_ALIVE_PERMANENT		1
#define KEEP_ALIVE_DEFINED_TIME		2

/* extern functions' declarations */

extern int 		 getSSIHostDetails(HOST_URL_ENUM, HOSTDEF_PTR_TYPE);
extern PAAS_BOOL isHostPingReqd();
extern int 		 getNumUniqueHosts();
extern PAAS_BOOL isPreamblePingReqd();
extern int 		 getBAPIHostDetails(HOST_URL_ENUM, HOSTDEF_PTR_TYPE);

#endif
