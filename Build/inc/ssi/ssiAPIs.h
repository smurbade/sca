#ifndef __SSI_APIS_H
#define __SSI_APIS_H

#include "common/common.h"

/* Data structure to be used to communicating with payment hosts using the SSI
 * module */
#if 0 //Praveen_P1: These data structures are not being used any where, commenting it for now
/* @deprecated to be removed : TODO */
typedef struct
{
	int			iReqSize;	/* To be populated by caller */
	int			iRespSize;	/* Will be populated by the library */
	char *		szReq;		/* To be populated by caller */
	char *		szResp;		/* Will be populated by the library */
	char *		szCardType;	/* To be populated by caller */
	PYMNT_TYPES	ePymtType;	/* To be populated by caller */
}
SSICOMM_STYPE, * SSICOMM_PTYPE;

typedef struct
{
	int			iFxn;
	int			iCmd;
	char *		szDataKey;
	char *		szCardType;
	PYMT_TYPES	ePymtType;
}
TRANCOM_STYPE, * TRANCOM_PTYPE;
#endif
#endif
