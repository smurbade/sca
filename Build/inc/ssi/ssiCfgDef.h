#ifndef __SSI_CFG_H
#define __SSI_CFG_H

#include <pthread.h>
#include "common/common.h"
#include "ssi/ssiHostCfg.h"

typedef enum
{
	CREDIT_HOST,
	DEBIT_HOST,
	GIFT_HOST,
	CHECK_HOST,
	AMEX_HOST,
	ALT_HOST,
	MAX_HOST
}
HOST_ENUM;

typedef enum
{
	PRIM_HOST,
	SCND_HOST
}
HOSTTYPE_ENUM;

typedef struct
{
	int				iConnTo;
	int				iRespTo;
	char			szURL[100];
	char			szCert[50];
	PAAS_BOOL		bPingRqd;
}
HOSTDEF_STYPE, * HOSTDEF_PTYPE;

typedef struct __stMap
{
	HOSTDEF_PTYPE			pstHost[2];
	HOSTTYPE_ENUM			curHost;
	pthread_mutex_t			recMutex;
	PAAS_BOOL				bDup;
}
HOSTMAP_STYPE, * HOSTMAP_PTYPE;

/* Merchant Settings */
typedef struct
{
	char	szClientId[17];			/* CLIENT_ID (16 + 1) */
	char	szDevKey[65];			/* DEVICEKEY (64 + 1) */
	char	szPymntPrcsrId[4][25];
	char 	szGiftCrdPrcsrId[25]; 	/* GIFT CARD PROCESSOR ID */
	char 	szStoreId[11];			/* STORE_ID */
	char 	szLaneId[11];			/* LANE */
	int		iPointGCManualEntry;
}
MCNT_DTLS_STYPE, * MCNT_DTLS_PTYPE;

typedef struct
{
	int						devModel;
	int						bapiDestId;
	int						iDHIBuild;
	int						iKeepAlive;			// Implemented to keep the connection alive if needed
	int						iKeepAliveInt;		// Duration/Interval to keep the connection alive
	int						ihostProtocol;		// Type of Host Format we need to Follow(UGP, SSI, etc.)
	int						iPAPymtProtocol;	// Type of Payment type we need to Follow(UGP, SSI, etc.)
	char					szDevSrlNo[12];		/* 11 + 1 (abc-pqr-xyz)*/
	char					szDevType[10];
	char					szDemoModeText[50];
	char					szDHIVersion[20];
	char					szMID[20];
	char					szTID[20];
	char					szDHIProc[21];
	PAAS_BOOL				hostPingReqd;
	PAAS_BOOL				preamblePingReqd;
	PAAS_BOOL				demoMode;
	PAAS_BOOL				bapiEnabled;
	PAAS_BOOL				dhiEnabled;
	PAAS_BOOL				chkHostConnStatus;
	HOSTDEF_STRUCT_TYPE		hostDef[2];
	HOSTDEF_STRUCT_TYPE		bapiHostDef[2];
	MCNT_DTLS_PTYPE			stMcntCfgPtr;
	int						iSAFOnRespTimeout;
	int						iNumUniqueHosts;
	int						iSwitchOnPrimUrlAfterXTran;
	int						iSwitchUrlAfterXRespTimeout;
}
SSI_CFG_STYPE, * SSI_CFG_PTYPE;

#define DEVTYPE_SUFFIX		"devtypesuffix"
#define DEMO_MODE			"demomode"
#define DEMO_MODE_TEXT		"demomodemsg"
#define TEMP_FILE			"/var/tmp/tmpDevKey.dat"
#define	DEVKEY_FILE			"./flash/devKey.dat"

/* Section in the config file for the SSI parameters */
#define SECTION_SSI			"ssi"
#define SECTION_DEVICE		"DCT"
#define SECTION_HOST		"HDT"
#define SECTION_MRCHNT		"MDT"
#define SECTION_DHI			"DHI"

/* --- Host Definition Table - HDT --- */
#define BAPI_PRIM_URL		"bapiprimurl"
#define BAPI_SCND_URL		"bapiscndurl"
#define BAPI_PRIM_PORT		"bapiprimport"
#define BAPI_SCND_PORT		"bapiscndport"
#define BAPI_PRIM_CONN_TO	"bapiprimcontimeout"
#define BAPI_SCND_CONN_TO	"bapiscndcontimeout"
#define BAPI_PRIM_RESP_TO	"bapiprimrestimeout"
#define BAPI_SCND_RESP_TO	"bapiscndresptimeout"


#define PRIM_HOST_URL			"primurl"
#define PRIM_HOST_PORT			"primport"
#define SCND_HOST_URL			"scndurl"
#define SCND_HOST_PORT			"scndport"
#define PRIM_CONN_TO			"primcontimeout"
#define SCND_CONN_TO			"scndcontimeout"
#define PRIM_RESP_TO			"primrestimeout"
#define SCND_RESP_TO			"scndresptimeout"
#define KEEP_ALIVE				"keepalive"
#define KEEP_ALIVE_INTERVAL		"keepaliveinterval"

#define BAPI_DEST_ID			"bapidestinationid"
#define	BAPI_DEST_ID_DEF		"1"

/* --- Merchant Definition Table - MDT --- */
#define CLIENT_ID				"clientid"
#define DEVICE_KEY				"devicekey"
#define	GIFT_CRD_PRCSR_ID		"giftprocessorid"
#define	CREDIT_PRCSR_ID			"creditprocessorid"
#define	DEBIT_PRCSR_ID			"debitprocessorid"
#define CHECK_PRCSR_ID			"checkprocessorid"
#define PS_GC_MANUAL_ENTRY  	"PointGCManualEntry"
#define MCNT_STORE_ID			"storeid"
#define MCNT_LANE_ID			"laneid"

/* SSI params in DCT section*/
#define BAPI_ENABLED			"bapienabled"
#define DHI_ENABLED				"dhienabled"
#define CHECK_HOST_CONN_STATUS	"checkhostconnstatus"
#define HOST_PROTOCOL_FORMAT	"hostprotocolformat"
#define PA_PYMT_TYPE_FORMAT		"papymttypeformat"
#define SAF_ON_RESP_TIMEOUT		"safonresptimeout"
#define SWITCH_TO_PRIMURL_AFTER_X_TRAN	"switchtoprimurlafterxtran"
#define TOGGLE_URL_AFTER_X_RESP_TIMEOUT	"toggleurlafterxresptimeout"

/* SSI params in the SSI section */
#define HOST_URL			"_url"
#define HOST_CONNTO			"_connto"
#define HOST_RESPTO			"_respto"
#define HOST_PINGRQD		"_pingrqd"
#define HOST_CERTIF			"_certificate"

#define DFLT_HOSTCONN_TIMEOUT	30
#define DFLT_HOSTRESP_TIMEOUT	45

#define UUID_SIZE				17

/* Extern functions declarations */
extern PAAS_BOOL 	 isDemoModeEnabled();
extern int 			 loadSSIConfigParams();
extern int 			 loadSSIParams();
extern int 			 getDeviceKey(char * );
extern int 			 setDevAdminRqd(PAAS_BOOL);
extern int 			 storeDeviceKey(char *);
extern int 			 getClientId(char *);
extern int			 storeClientId(char *);
extern int			 saveMcntStoreId(char *);
extern int			 saveMcntLaneId(char *);
extern int			 storeDHIVersionInfo(char*);
extern int 			 getGCManualEntryParam();
extern int 			 getGenDevPymtDtls(char **, int *);
extern int 			 storeProcessorId(char *, char *);
extern int 			 setAdminPacketRequestStatus(PAAS_BOOL);
extern int 			 getBAPIDestId();
extern int 			 getDHIBuildNum();
extern int		     getKeepAliveParameter();
extern int		     getKeepAliveInterval();
extern int			 getHostProtocolFormat();
extern int 			 getPAPymtTypeFormat();
extern char* 		 getDHIVersionInfo();
extern char* 		 getDHIMID();
extern char* 		 getDHITID();
extern char* 		 getDHIProcessor();
extern char* 		 getMerchantProcessor(int);
extern char*		 getMerchantStoreId();
extern char* 		 getMerchantLaneId();
extern char* 		 getURLFrmCnfgFile();
extern PAAS_BOOL	 isBapiEnabled();
extern PAAS_BOOL	 isDHIEnabled();
extern PAAS_BOOL 	 isCheckHostConnStatusEnable();
extern HOSTDEF_PTYPE getHostDtlsForComm(HOST_ENUM, int);
extern int			 getNumOfTimedOutTranToSAF();
extern int 			 getNumOfMaxTranOnScndURL();
extern int 			 getNumOfMaxTimeoutTranToSwitchURL();

#endif
