#ifndef __SSI_CFG_H
#define __SSI_CFG_H

#include <pthread.h>
#include "common.h"

typedef enum
{
	CREDIT_HOST,
	DEBIT_HOST,
	GIFT_HOST,
	CHECK_HOST,
	AMEX_HOST,
	ALT_HOST,
	MAX_HOST
}
HOST_ENUM;

typedef enum
{
	PRIM_HOST,
	SCND_HOST
}
HOSTTYPE_ENUM;

typedef struct
{
	int			iConnTo;
	int			iRespTo;
	char		szURL[100];
	char		szCert[50];
	PAAS_BOOL	bPingRqd;
}
HOSTDEF_STYPE, * HOSTDEF_PTYPE;

typedef struct __stMap
{
	HOSTDEF_PTYPE		pstHost[2];
	HOSTTYPE_ENUM		curHost;
	pthread_mutex_t		recMutex;
	PAAS_BOOL			bDup;
}
HOSTMAP_STYPE, * HOSTMAP_PTYPE;

/* Section in the config file for the SSI parameters */
#define SECTION_SSI		"ssi"

/* SSI params in the SSI section */
#define HOST_URL		"_url"
#define HOST_CONNTO		"_connto"
#define HOST_RESPTO		"_respto"
#define HOST_PINGRQD	"_pingrqd"
#define HOST_CERTIF		"_certificate"

/* Extern functions declarations */
extern int loadSSIParams();
extern HOSTDEF_PTYPE getHostDtlsForComm(HOST_ENUM, int);

#endif
