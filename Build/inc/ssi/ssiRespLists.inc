/* 
 * --------------------------------------------------------
 * -------------- SSI RESPONSE DATA -----------------------
 * --------------------------------------------------------
 */

static KEYVAL_STYPE	resyncKeyList[] =
{
	{"RESYNC_KEY", NULL, PAAS_FALSE, SINGLETON, NULL}
};

static KEYVAL_STYPE	genRespLst[] =
{
	{ "TERMINATION_STATUS", NULL, PAAS_FALSE, SINGLETON },
	{ "RESULT"            , NULL, PAAS_FALSE, SINGLETON },
	{ "RESULT_CODE"       , NULL, PAAS_FALSE, SINGLETON },
	{ "RESPONSE_TEXT"     , NULL, PAAS_FALSE, SINGLETON },
	{ "HOST_RESPCODE"     , NULL, PAAS_FALSE, SINGLETON },
	{ "RESPONSE_CODE"     , NULL, PAAS_FALSE, SINGLETON }
};

static KEYVAL_STYPE	vspRespLst[] =
{
	{ "VSP_CODE"      , NULL, PAAS_FALSE, SINGLETON },
	{ "VSP_TRXID"     , NULL, PAAS_FALSE, SINGLETON },
	{ "VSP_RESULTDESC", NULL, PAAS_FALSE, SINGLETON }
};

static KEYVAL_STYPE amtLst[] =
{
	{ "TRANS_AMOUNT"     , NULL, PAAS_FALSE, SINGLETON },
	{ "APPROVED_AMOUNT"  , NULL, PAAS_FALSE, SINGLETON },
	{ "AUTH_AMOUNT"  	 , NULL, PAAS_FALSE, SINGLETON },
	{ "AVAIL_BALANCE"    , NULL, PAAS_FALSE, SINGLETON },
	{ "AMOUNT_BALANCE"   , NULL, PAAS_FALSE, SINGLETON },
	{ "ORIG_TRANS_AMOUNT", NULL, PAAS_FALSE, SINGLETON },
	{ "DIFF_AMOUNT_DUE"  , NULL, PAAS_FALSE, SINGLETON },
	{ "DIFF_DUE_AMOUNT"  , NULL, PAAS_FALSE, SINGLETON },
	{ "DIFF_AUTH_AMOUNT" , NULL, PAAS_FALSE, SINGLETON },
	{ "PREVIOUS_BALANCE" , NULL, PAAS_FALSE, SINGLETON },
	{ "FS_AVAIL_BALANCE" , NULL, PAAS_FALSE, SINGLETON },
	{ "CB_AVAIL_BALANCE" , NULL, PAAS_FALSE, SINGLETON }
};


static KEYVAL_STYPE	cardRespLst[] =
{
	{ "PAYMENT_MEDIA"		, NULL, PAAS_FALSE, SINGLETON },
	{ "CARDHOLDER"   		, NULL, PAAS_FALSE, SINGLETON },
	{ "CVV2_CODE"    		, NULL, PAAS_FALSE, SINGLETON },
	{ "ACCT_NUM"     	 	, NULL, PAAS_FALSE, SINGLETON },
	{ "EMBOSSED_ACCT_NUM"   , NULL, PAAS_FALSE, SINGLETON },
	{ "PINLESSDEBIT"   		, NULL, PAAS_FALSE, SINGLETON },
	{ "TKN_CNAME" 			, NULL, PAAS_FALSE, SINGLETON },
	{ "TKN_EXPDATE"  		, NULL, PAAS_FALSE, SINGLETON },
	{ "CARD_ABBRV"	  		, NULL, PAAS_FALSE, SINGLETON }
};

static KEYVAL_STYPE	pymtRespLst[] =
{
	{ "TRANS_SEQ_NUM"    	, NULL, PAAS_FALSE, SINGLETON },
	{ "INTRN_SEQ_NUM"     	, NULL, PAAS_FALSE, SINGLETON },
	{ "TROUTD"            	, NULL, PAAS_FALSE, SINGLETON },
	{ "CTROUTD"           	, NULL, PAAS_FALSE, SINGLETON },
	{ "AUTH_CODE"         	, NULL, PAAS_FALSE, SINGLETON },
	{ "LPTOKEN"           	, NULL, PAAS_FALSE, SINGLETON },
	{ "PAYMENT_TYPE" 	  	, NULL, PAAS_FALSE, SINGLETON },
	{ "CARD_TOKEN"        	, NULL, PAAS_FALSE, SINGLETON },
	{ "TA_TOKEN"        	, NULL, PAAS_FALSE, SINGLETON },
	{ "TRACE_NUM"        	, NULL, PAAS_FALSE, SINGLETON },
	{ "RETURN_CHECK_FEE"  	, NULL, PAAS_FALSE, SINGLETON },
	{ "RETURN_CHECK_NOTE" 	, NULL, PAAS_FALSE, SINGLETON },
	{ "ACK_REQUIRED"      	, NULL, PAAS_FALSE, SINGLETON },
	{ "CHECK_NUM"        	, NULL, PAAS_FALSE, SINGLETON },
	{ "BANK_USERDATA"     	, NULL, PAAS_FALSE, SINGLETON },
	{ "AUTH_RESP_CODE"    	, NULL, PAAS_FALSE, SINGLETON },
	{ "TXN_POSENTRYMODE"  	, NULL, PAAS_FALSE, SINGLETON },
	{ "MERCHID"			  	, NULL, PAAS_FALSE, SINGLETON },
	{ "TERMID"			  	, NULL, PAAS_FALSE, SINGLETON },			
	{ "LANE"  			  	, NULL, PAAS_FALSE, SINGLETON },
	{ "STORE_NUM"		  	, NULL, PAAS_FALSE, SINGLETON },
	{ "PAYPAL_CHECKID_KEY"	, NULL, PAAS_FALSE, SINGLETON },
	{ "BATCH_TRACE_ID"	 	, NULL, PAAS_FALSE, SINGLETON },
	{ "AVS_CODE"		  	, NULL, PAAS_FALSE, SINGLETON },
	{ "TRANS_DATE"		  	, NULL, PAAS_FALSE, SINGLETON },
	{ "TRANS_TIME"		  	, NULL, PAAS_FALSE, SINGLETON },
	{ "TRANS_DATETIME"	  	, NULL, PAAS_FALSE, SINGLETON },
	{ "CMRCL_FLAG"		  	, NULL, PAAS_FALSE, SINGLETON },
	{ "PROCESSOR"		  	, NULL, PAAS_FALSE, SINGLETON },
	{ "TKN_PAYMENT"    		, NULL, PAAS_FALSE, SINGLETON },
	{ "TKN_MATCHING" 		, NULL, PAAS_FALSE, SINGLETON },
	{ "PPCV"    			, NULL, PAAS_FALSE, SINGLETON },
	{ "CREDIT_PLAN_NBR"    	, NULL, PAAS_FALSE, SINGLETON },
	{ "REF_NUMBER"    		, NULL, PAAS_FALSE, SINGLETON },
	{ "VALIDATION_CODE"    	, NULL, PAAS_FALSE, SINGLETON },
	{ "VISA_IDENTIFIER"    	, NULL, PAAS_FALSE, SINGLETON },
	{ "DENIAL_REC_NUM"    	, NULL, PAAS_FALSE, SINGLETON }, 
	{ "SIGNATUREREF"    	, NULL, PAAS_FALSE, SINGLETON },
	{ "APR_TYPE"			, NULL, PAAS_FALSE, SINGLETON },
	{ "PURCHASE_APR"		, NULL, PAAS_FALSE, SINGLETON },
	{ "SVC_PHONE"			, NULL, PAAS_FALSE, SINGLETON },
	{ "STATUS_FLAG"			, NULL, PAAS_FALSE, SINGLETON },
	{ "RECEIPT_TEXT"		, NULL, PAAS_FALSE, SINGLETON },
	{ "ACTION_CODE"			, NULL, PAAS_FALSE, SINGLETON },
	{ "AUTHNWID"            , NULL, PAAS_FALSE, SINGLETON },
	{ "REFERENCE"       	, NULL, PAAS_FALSE, SINGLETON }
};

static KEYVAL_STYPE dupRespLst[] = 
{
	{ "DUP_CTROUTD"          , NULL, PAAS_FALSE, SINGLETON },
	{ "DUP_AUTH_CODE"        , NULL, PAAS_FALSE, SINGLETON },
	{ "DUP_ACCT_NUM"     	 , NULL, PAAS_FALSE, SINGLETON },
	{ "DUP_PAYMENT_MEDIA"	 , NULL, PAAS_FALSE, SINGLETON },
	{ "DUP_TROUTD"           , NULL, PAAS_FALSE, SINGLETON },
	{ "DUP_INVOICE"          , NULL, PAAS_FALSE, SINGLETON },
	{ "DUP_TRANS_AMOUNT"     , NULL, PAAS_FALSE, SINGLETON },
	{ "DUP_TRANS_DATE"       , NULL, PAAS_FALSE, SINGLETON },
	{ "DUP_TRANS_TIME"       , NULL, PAAS_FALSE, SINGLETON },
	{ "DUP_TRANS_DATETIME"   , NULL, PAAS_FALSE, SINGLETON },
	{ "DUP_AVS_CODE"		 , NULL, PAAS_FALSE, SINGLETON },
	{ "DUP_CVV2_CODE"        , NULL, PAAS_FALSE, SINGLETON }
};

static KEYVAL_STYPE dccRespLst[] = 
{
	{ "EXCHANGE_RATE"          	, NULL, PAAS_FALSE, SINGLETON },
	{ "FGN_CURR_CODE"			, NULL, PAAS_FALSE, SINGLETON },
	{ "FGN_AMOUNT"     	 		, NULL, PAAS_FALSE, SINGLETON },
	{ "DCC_OFFERED"	 			, NULL, PAAS_FALSE, BOOLEAN   },
	{ "MARGINRATE_PERCENTAGE"   , NULL, PAAS_FALSE, SINGLETON },
	{ "EXCHANGE_RATE_SOURCENAME", NULL, PAAS_FALSE, SINGLETON },
	{ "COMMISSION_PERCENTAGE"   , NULL, PAAS_FALSE, SINGLETON },
	{ "MINOR_UNITS"       		, NULL, PAAS_FALSE, SINGLETON },
	{ "ALPHA_CURRENCY_CODE"     , NULL, PAAS_FALSE, SINGLETON },
	{ "CURRENCY_SYMBOL"     	, NULL, PAAS_FALSE, SINGLETON }
	
};

/* -------------- PAYMENT RESPONSE-------------- */
static KEYVAL_STYPE	enabledLst[] =
{
	{ "ENABLED", NULL, PAAS_TRUE, SINGLETON, NULL }
};

static KEYVAL_STYPE	numList[] =
{
	{ "NO", NULL, PAAS_TRUE, SINGLETON, NULL }
};

static KEYVAL_STYPE	fxnAttribList[] =
{
	{ "PROCESSOR_ID", NULL, PAAS_TRUE, SINGLETON, NULL },
	{ "ID"          , NULL, PAAS_TRUE, SINGLETON, NULL }
};

static VARLST_INFO_STYPE headerList[] =
{
	{ 0 , "HEADER", sizeof(numList) / KEYVAL_SIZE, numList},
	{ -1, NULL    , 0 , NULL }
};

static VARLST_INFO_STYPE footerList[] =
{
	{ 0 , "FOOTER", sizeof(numList) / KEYVAL_SIZE, numList},
	{ -1, NULL    , 0 , NULL }
};

static VARLST_INFO_STYPE disclaimList[] =
{
	{ 0 , "DISCLAIMER", sizeof(numList) / KEYVAL_SIZE, numList},
	{ -1, NULL        , 0 , NULL }
};

static VARLST_INFO_STYPE crdList[] =
{
	{ 0 , "CARD_TYPE", sizeof(enabledLst) / KEYVAL_SIZE, enabledLst},
	{ -1, NULL       , 0 , NULL }
};

static VARLST_INFO_STYPE fxnList[] =
{
	{ 0 , "SUPPORTED_FUNCTION", sizeof(fxnAttribList) / KEYVAL_SIZE,
																fxnAttribList},
	{ -1, NULL                , 0 , NULL }
};

/* ------ PWC ADMIN RESPONSE RELATED ------------ */
static KEYVAL_STYPE	adminRespLst[] =
{
	{ "DEVICEKEY"          , NULL, PAAS_FALSE, SINGLETON  , NULL },
	{ "CLIENT_ID"          , NULL, PAAS_FALSE, SINGLETON  , NULL },
	{ "HEADERS"            , NULL, PAAS_FALSE, ATTR_LKDLST, headerList },
	{ "FOOTERS"            , NULL, PAAS_FALSE, ATTR_LKDLST, footerList },
	{ "DISCLAIMERS"        , NULL, PAAS_FALSE, ATTR_LKDLST, disclaimList },
	{ "CARD_TYPES"         , NULL, PAAS_FALSE, ATTR_LKDLST, crdList },
	{ "SUPPORTED_FUNCTIONS", NULL, PAAS_FALSE, ATTR_LKDLST, fxnList }
};

//ArjunU1: EMV Testing
/* ------- FALLBACK INDICATOR ------- */
static KEYVAL_STYPE	falllBckIndicatorList[] =
{
	{ "FALLBACK_IND"        , NULL, PAAS_FALSE, SINGLETON  , NULL },
	{ "APP_ID"           	, NULL, PAAS_FALSE, SINGLETON  , NULL },
	{ "FALLBACK_VALUE"  	, NULL, PAAS_FALSE, SINGLETON  , NULL }	
};

static VARLST_INFO_STYPE fallBackListInfo[] =
{
	{ FALLBACK_INDICATOR_0, "FALLBACK_INDICATOR_0", sizeof(falllBckIndicatorList) / KEYVAL_SIZE,
																falllBckIndicatorList},
	{ FALLBACK_INDICATOR_1, "FALLBACK_INDICATOR_1", sizeof(falllBckIndicatorList) / KEYVAL_SIZE,
																falllBckIndicatorList},
	{ FALLBACK_INDICATOR_2, "FALLBACK_INDICATOR_2", sizeof(falllBckIndicatorList) / KEYVAL_SIZE,
																falllBckIndicatorList},
	{ FALLBACK_INDICATOR_3, "FALLBACK_INDICATOR_3", sizeof(falllBckIndicatorList) / KEYVAL_SIZE,
																falllBckIndicatorList},
	{ FALLBACK_INDICATOR_4, "FALLBACK_INDICATOR_4", sizeof(falllBckIndicatorList) / KEYVAL_SIZE,
																falllBckIndicatorList},	
	{ FALLBACK_INDICATOR_5, "FALLBACK_INDICATOR_5", sizeof(falllBckIndicatorList) / KEYVAL_SIZE,
																falllBckIndicatorList},	
																
	{ -1         , NULL         , 0 , NULL }
};

/* ------- FALLBACK INDICATOR ------- */
static KEYVAL_STYPE	offlineFlrLmtList[] =
{
	{ "OFFLINE_FLOOR_LIMIT_IND"         , NULL, PAAS_FALSE, SINGLETON, NULL },
	{ "APP_ID"           				, NULL, PAAS_FALSE, SINGLETON, NULL },
	{ "OFFLINE_FLOOR_LIMIT_VALUE" 		, NULL, PAAS_FALSE, SINGLETON, NULL },
	{ "FLOOR_LIMIT_THRESHOLD"  			, NULL, PAAS_FALSE, SINGLETON, NULL }
};

static VARLST_INFO_STYPE offlineFlrLmtListInfo[] =
{
	{ OFFLINE_FLOOR_LIMIT_0, "OFFLINE_FLOOR_LIMIT_0", sizeof(offlineFlrLmtList) / KEYVAL_SIZE,
																offlineFlrLmtList},
	{ OFFLINE_FLOOR_LIMIT_1, "OFFLINE_FLOOR_LIMIT_1", sizeof(offlineFlrLmtList) / KEYVAL_SIZE,
																offlineFlrLmtList},
	{ OFFLINE_FLOOR_LIMIT_2, "OFFLINE_FLOOR_LIMIT_2", sizeof(offlineFlrLmtList) / KEYVAL_SIZE,
																offlineFlrLmtList},
	{ OFFLINE_FLOOR_LIMIT_3, "OFFLINE_FLOOR_LIMIT_3", sizeof(offlineFlrLmtList) / KEYVAL_SIZE,
																offlineFlrLmtList},
	{ -1         , NULL         , 0 , NULL }
};

/* ------- FALLBACK INDICATOR ------- */
static KEYVAL_STYPE	capkList[] =
{
	{ "PUBLIC_KEY_CHECKSUM"         	, NULL, PAAS_FALSE, SINGLETON, NULL },
	{ "PUBLIC_KEY_EXP_LEN"           	, NULL, PAAS_FALSE, SINGLETON, NULL },
	{ "APP_ID"  						, NULL, PAAS_FALSE, SINGLETON, NULL },
	{ "PUBLIC_KEY_MOD"  				, NULL, PAAS_FALSE, SINGLETON, NULL },
	{ "PUBLIC_KEY_CHECKSUM_LEN"        	, NULL, PAAS_FALSE, SINGLETON, NULL },
	{ "PUBLIC_KEY_IND"           		, NULL, PAAS_FALSE, SINGLETON, NULL },
	{ "PUBLIC_KEY_MOD_LEN"  			, NULL, PAAS_FALSE, SINGLETON, NULL },
	{ "PUBLIC_KEY_EXP"           		, NULL, PAAS_FALSE, SINGLETON, NULL },
	{ "PUBLIC_KEY_INDEX"  				, NULL, PAAS_FALSE, SINGLETON, NULL },
	{ "PUBLIC_KEY_EXP_DATE"				, NULL, PAAS_FALSE, SINGLETON, NULL }		
};

static VARLST_INFO_STYPE capkListInfo[] =
{
	{ PUBLIC_KEY_0, 	"PUBLIC_KEY_0",		sizeof(capkList) / KEYVAL_SIZE,
																capkList},
	{ PUBLIC_KEY_1, 	"PUBLIC_KEY_1", 	sizeof(capkList) / KEYVAL_SIZE,
																capkList},
	{ PUBLIC_KEY_2, 	"PUBLIC_KEY_2",		sizeof(capkList) / KEYVAL_SIZE,
																capkList},
	{ PUBLIC_KEY_3, 	"PUBLIC_KEY_3", 	sizeof(capkList) / KEYVAL_SIZE,
																capkList},
	{ PUBLIC_KEY_4, 	"PUBLIC_KEY_4",		sizeof(capkList) / KEYVAL_SIZE,
																capkList},
	{ PUBLIC_KEY_5, 	"PUBLIC_KEY_5", 	sizeof(capkList) / KEYVAL_SIZE,
																capkList},				
	{ -1         , NULL         , 0 , NULL }
};

/*ArjunU1: Temporary List,More tags to be added later */
/* ------- EMV KEY LOAD Response List ------- */
static KEYVAL_STYPE	emvKeyLoadGenRespList[] =
{
	{ "DOWNLOAD_STATUS"       	, NULL, PAAS_FALSE, SINGLETON  , NULL }
};
/* ------- EMV CAPK File Response List ------- */
static KEYVAL_STYPE	emvCAPKRespList[] =
{
	{ "PUBLIC_KEY"  	, NULL, PAAS_FALSE, VARLIST, capkListInfo },		
};

/* ------- EMV EMV Configuration Table(MVT) Response List ------- */
static KEYVAL_STYPE	emvMVTRespList[] =
{
	{ "FALLBACK_INDICATOR"  	, NULL, PAAS_FALSE, VARLIST, fallBackListInfo },		
	{ "OFFLINE_FLOOR_LIMIT" 	, NULL, PAAS_FALSE, VARLIST, offlineFlrLmtListInfo }
};

/* ------- EMV Response List ------- */
static KEYVAL_STYPE	emvRespList[] =
{
	{ "EMV_TAGS_RESPONSE"	 		 	, NULL, PAAS_FALSE, SINGLETON  , NULL },
	{ "EMV_UPDATE"	 				 	, NULL, PAAS_FALSE, SINGLETON  , NULL }
};

/* Version Info List */

static KEYVAL_STYPE	versionRespList[] =
{
	{"VERSION_INFO", NULL, PAAS_TRUE, SINGLETON, NULL}
};

/* Credit Application List */

static KEYVAL_STYPE	creditAppRespList[] =
{
	{"PROXY_REF"		, 	NULL, PAAS_FALSE, SINGLETON, NULL},
	{"PROXY_RESULT"		, 	NULL, PAAS_TRUE , SINGLETON, NULL},
	{"CREDITAPP_PAYLOAD", 	NULL, PAAS_FALSE, SINGLETON, NULL}
};

/*
 * ============================================================================
 * End of file ssiRespLists.inc
 * ============================================================================
 */
