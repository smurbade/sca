
#ifndef __PWC_COMM_H
#define __PWC_COMM_H

typedef struct
{
	char	url[100];
	int		conTimeOut;
	int		resTimeOut;
}
PWCURL_STRUCT_TYPE, * PWCURL_PTR_TYPE;

typedef struct
{
	int		iTotSize;
	int		iWritten;
	char *	szMsg;
}
RESP_DATA_STYPE, * RESP_DATA_PTYPE;

#endif
