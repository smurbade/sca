#ifndef __SSI_MAIN_H
#define __SSI_MAIN_H

#include "common/common.h"

extern int 	 initSSIModule();
extern int 	 closeSSIModule();
extern int	 doProxyCommunication(char *, char *);
extern int 	 doSSICommunication(char *, char *);
extern int 	 doBAPICommunication(char*, int);
extern void* sendBasketData(void*);
extern PAAS_BOOL isHostConnected();
extern void getEmbossedPANFromTrackData(char *, char *, PAAS_BOOL );
extern int getDataFromVCL(char*, char *, char*, char*, PAAS_BOOL);

#endif
