#ifndef __PSAMT_DEF_H
#define __PSAMT_DEF_H

typedef enum
{
	NEG_AMT = -1,
	POS_AMT = 1
}
PSAMT_SIGN;

typedef enum
{
	LESSER_THAN = -1,
	EQUAL_TO,
	GREATER_THAN
}
CMP_VAL;

/* Currency definition */
typedef struct
{
	int		iPrec;
	char	szSym[5];
	char	cDecSep;
	char	cThSep;
}
CRNCY_STYPE, * CRNCY_PTYPE;

#define CRNCY_SIZE	sizeof(CRNCY_STYPE)

/* Amount definition */
typedef struct
{
	int			iChar;
	int			iMan;
	PSAMT_SIGN	eSign;
	CRNCY_PTYPE	pstCrncy;
}
AMT_STYPE, * AMT_PTYPE;

#define AMT_SIZE	sizeof(AMT_STYPE)

#endif
