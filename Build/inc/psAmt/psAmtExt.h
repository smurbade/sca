#ifndef __PSAMT_EXT_H
#define __PSAMT_EXT_H

#include "amt/psAmtDef.h"

/* Amount formatting (String to amount and vice versa) */
extern int	getFormattedAmtStr(AMT_PTYPE, char *, int size);
extern int	getSimpleAmtStr(AMT_PTYPE, char *, int size);
extern int	getAmtFrmStr(AMT_PTYPE, CRNCY_PTYPE, char *);

/* Mathematical operations on amounts */
extern int	addAmts(AMT_PTYPE, AMT_PTYPE, AMT_PTYPE);
extern int	subtractAmts(AMT_PTYPE, AMT_PTYPE, AMT_PTYPE);
extern int	mulScalarWithAmt(int, AMT_PTYPE, AMT_PTYPE);
extern int	divideAmtByScalar(int, AMT_PTYPE, AMT_PTYPE);

/* Comparison operations on amounts */
extern int	isAmtgt(AMT_PTYPE, AMT_PTYPE);
extern int	isAmtlt(AMT_PTYPE, AMT_PTYPE);
extern int	isAmtgte(AMT_PTYPE, AMT_PTYPE);
extern int	isAmtlte(AMT_PTYPE, AMT_PTYPE);

/* Some operations on currencies */
extern int	stripAmtStrForCrncy(char *, CRNCY_PTYPE);
extern int	compareCrncys(CRNCY_PTYPE, CRNCY_PTYPE);
extern int	getCcyFrmTbl(char *, CRNCY_PTYPE);
extern int	runAmtCcyCfg();
extern void	getDfltCcy(CRNCY_PTYPE);

#endif
