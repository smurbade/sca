#ifndef __APPLOG_CFGDEF_H
#define __APPLOG_CFGDEF_H

#include "common/common.h"

#define SECTION_DEVICE				"DCT"

#define DFLT_MAX_BCKUP_APP_LOG_FILES	4				//1(main app log file)+ 4(backup files)
#define DFLT_MAX_APP_LOG_FILE_SIZE		4				// in MB
#define DFLT_APP_LOG_FILE_LOC			"/mnt/flash/logs/usr1"

#define APP_LOG_ENABLED 				"applogenabled"
#define MAX_BCKUP_LOG_FILES 			"maxBckupLogFiles"
#define CUR_NUM_BCKUP_LOG_FILES 		"curNumBckupLogFiles"
#define MAX_LOG_FILE_SIZE 				"maxlogFileSize"
#define LOG_FILE_LOCATION 				"logFileLocation"
#define XML_MASK_LIST_FILELOC 			"/home/usr1/flash/xmlMaskList.lst"
#define MAX_XML_MASK_NODE				50
#define MAX_XML_NODE_SIZE				50
#define MAX_MASK_ENTRYID				20
#define MAX_ENTRYID_SIZE				50

#define MAX_DATA_SIZE				5100 //Please don't change it to higher value since msg queue allows max 8192 bytes in single msg.

/*ArjunU1: Note: Its very Important that whole sizeof structure should not be greater than MSGMAX.
 * 		   MSGMAX is maximum size for a message text: 8192 bytes (on Linux, this limit can be read and modified via /proc/sys/kernel/msgmax).
 */
typedef struct
{
	long			lMsgType;
	char 			szTSPrefix[30];
	char 			szAppName[20];
	char 			szEntryType[20];
	char 			szEntryId[30];
	//char 			szData[MAX_DATA_SIZE+1];
	char *			pszData;
	char 			szErrDiagSteps[300];
}
APPLOGDATA_STYPE, * APPLOGDATA_PTYPE;	//Now total sizeof(APPLOGDATA_STYPE) = 8190 bytes < 8192(MSGMAX).

/*Application Logging Settings*/
typedef struct
{
	PAAS_BOOL			bAppLogEnabled;
	long				lAppMaxBckupLogFiles;
	int					iAppCurNumBckupLogFiles;
	long				lAppLogFileSize;
	char				szAppLogFileLocation[100];
}
APP_LOG_STYPE, *APP_LOG_PTYPE;

extern int	loadAppLogParams();
extern int 	getAppCurNumBckupLogFiles();
extern void updateNumOfLogFiles(int);
extern void getTranAppLogSettings( APP_LOG_PTYPE);

#endif
