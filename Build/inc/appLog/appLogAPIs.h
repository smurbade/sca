#ifndef __APPLOG_APIS_H
#define __APPLOG_APIS_H

#include "common/common.h"

extern int 		 initAppLog();
extern void		 addAppEventLog( char *, char *, char *, char *, char *);
extern int		 closeAppLog();
extern int		 isAppLogEnabled();

#endif
