/******************************************************************
*                     emvAPIs.c                                   *
* ================================================================*
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "svc.h"

#include "common/common.h"
#include "common/metaData.h"
#include "common/tranDef.h"
#include "db/tranDS.h"
#include "common/xmlUtils.h"
#include "common/utils.h"

//#include "common/ConfigINI.h"

#include "uiAgent/guimgr.h"
#include "uiAgent/ctrlids.h"


#include "uiAgent/uiGeneral.h"
#include "uiAgent/emv.h"
#include "uiAgent/emvMsgFmt.h"
#include "uiAgent/emvConstants.h"
#include "uiAgent/emvAPIs.h"
#include "uiAgent/uiControl.h"
#include "uiAgent/uiCfgDef.h"
#include "uiAgent/uiMsgFmt.h"
#include "uiAgent/uiAPIs.h"
#include "bLogic/bLogicCfgDef.h"
#include "bLogic/blAPIs.h"
#include "appLog/appLogAPIs.h"
//#include "uiAgent/emvConfigList.inc"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

extern int sendUIReqMsg(char *);
extern int sendUIReqMsg_EX(char *, int);
extern PAAS_BOOL isEmvEnabledInDevice();
extern int hex_decimal(char*);
extern void Char2Hex(char *, char *, int);

#define HASHSIZE 101
typedef struct __AIDNode
{
	char	szAID[33];
	char	szRID[11];
	char	szTermCapPreferred[7];
	char	szTermCapDefault[7];
	char	szPymtMedia[21];
	char	szTACDefault[11];
	char	szTACDenial[11];
	char	szTACOnline[11];
	char	szTACDefaultCTLS[11];
	char	szTACDenialCTLS[11];
	char	szTACOnlineCTLS[11];
	char	szCardAbbrv[3];
	int		iPymtType;
	int		iCashbackEnabled;
	int		iCapSignOnPINBypass;
	double	fThreshold;
	double	fSigLimit;
	struct	__AIDNode *next;
}
AID_NODE_STYPE, * AID_NODE_PTYPE;

static AID_NODE_PTYPE hashtab[HASHSIZE];  // pointer table

typedef struct __TagNode
{
	char	szTag[7];
	int		index;
	struct	__TagNode *next;
}
CTLS_TAG_NODE_STYPE, * CTLS_TAG_NODE_PTYPE;

static CTLS_TAG_NODE_PTYPE hashtab_ctls_tags[HASHSIZE];  // pointer table

/* Extern variables */
extern int 				whichForm;
extern char *			frmName[];
extern PAAS_BOOL		bBLDataRcvd;
extern UIRESP_STYPE		stBLData;

/* Static variables */
static UIREQ_STYPE		stEMVReq;
static int				eReqCmd;	//TODO: Need to use Mutex if required.

/* Static functions declarations */
#if 0
static int updateKeyLoadListValue(EMVKEYLOAD_INFO_PTYPE); 
#endif
static int getSupportedRIDSchemes(dictionary *, char *);
static void addParametersinEMVUIReq(char *, char *, char *);
//static int initEmvTransaction();
static int isEmvAgentRunning();
static int dispatchEmvRequest(char *, int);
//static int setSwipeCardMsgTitle();
//static void initEmvTranData();
static int buildEmvCmd(ENUM_EMV_CMDS,UIREQ_PTYPE, char*);
//static int buildEmvDcmd(EMV_CMD_INFO_PTYPE, EMVKEYLOAD_LIST_PTYPE, UIREQ_PTYPE);
//static int processEmvTran(EMVDTLS_PTYPE, char*);
//static int processEmvCmd(ENUM_EMV_CMDS, EMVDTLS_PTYPE, char*);
//static int processEmvC30Cmd(EMVDTLS_PTYPE, char*);
static int processEmvC32Cmd(CARDDTLS_PTYPE, char*);
static int processEmvC34Cmd(CARDDTLS_PTYPE , char*, int );
//static int processEmvC36Cmd(EMVDTLS_PTYPE, char*);
static int processEmvI02Cmd();
#if 0
static int processEmvDcmd(EMV_CMD_INFO_PTYPE, EMVKEYLOAD_LIST_PTYPE);
static int processEmvCTLSDcmd(EMV_CMD_INFO_PTYPE, EMVKEYLOAD_LST_INFO_PTYPE);
static int processEmvD11cmd(EMV_CMD_INFO_PTYPE, EMVKEYLOAD_LIST_PTYPE);
static int getNxtEmvCmd(EMV_CMD_INFO_PTYPE);
static int getApplnInitMetaData(METADATA_PTYPE);
static int getPubkeyInitMetaData(METADATA_PTYPE);
static int getTermInitMetaData(METADATA_PTYPE);
static int getMetaDataForEMVInitReq(METADATA_PTYPE, EMV_INIT_ENUM);
static int storePubKeyData(METADATA_PTYPE);
#endif

extern void setEmvReqCmd(ENUM_EMV_CMDS);
#if 0
static int updateAppDataListINI(VAL_NODE_PTYPE);
static int updateAppDataInfoInINI(VAL_LST_PTYPE);
static int updateAppLstinINI(KEYVAL_PTYPE);
#endif

#ifdef DEVDEBUG
static void printCardEMVResp(CARDDTLS_PTYPE );
#endif
static int 					addFieldstoHashTable(AID_NODE_PTYPE);
static int 					addCTLSTagtoHashTable(CTLS_TAG_NODE_STYPE );
static unsigned int 		generateHashVal(char *);
static AID_NODE_PTYPE 		lookupHashTable(char *, char *);
static CTLS_TAG_NODE_PTYPE	lookupHashTableforCTLSTag(char *);
static AID_NODE_PTYPE 		lookupHashTableForPartialMatching(char *);
static AID_NODE_PTYPE 		addEntryToHashTable(char *, AID_NODE_PTYPE);
static CTLS_TAG_NODE_PTYPE	addCTLSTagEntryToHashTable(char *, CTLS_TAG_NODE_STYPE );
//static int updateEmvCTLSTermDtlsFromCTLSConfig();
static int setTermDtlsInHashTable(PAAS_BOOL, char *, char *, char *, char *, char *);
static int createAIDListIniFile(char [100][33], char *);
static int loadAIDListTxtFile(char [100][33], char *);
static int loadAIDListINIFile(char * );
#ifdef DEVDEBUG
/*
 * ============================================================================
 * Function Name: printCardEMVResp
 *
 * Description	: This function would display EMV response.
 *
 * Input Params	: @szMsg	   -> Input message to be displayed.
 *
 * Output Params: None
 * ============================================================================
 */
static void printCardEMVResp(CARDDTLS_PTYPE pstEmvDtls)
{
#ifdef DEBUG
	char	szDbgMsg[2048]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__,
					"*******************CARD EMVResp Data*********************");
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"AID_4F",pstEmvDtls->stEmvAppDtls.szAID);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_PAN_SEQ_NUM_5F34", pstEmvDtls->stEmvAppDtls.szAppSeqNum);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_LABEL_50",pstEmvDtls->stEmvAppDtls.szAppLabel);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_PREF_NAME_9F12", pstEmvDtls->stEmvAppDtls.szAppPrefName);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_EFFECTIVE_DATE_5F25",pstEmvDtls->stEmvAppDtls.szAppEffctiveDate);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"Clear APPLICATION_EXPIRY_DATE_5F24", pstEmvDtls->stEmvAppDtls.szClearAppExpiryDate);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"Enc APPLICATION_EXPIRY_DATE_5F24", pstEmvDtls->stEmvAppDtls.szEncAppExpiryDate);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_TRAN_COUNTER_9F36", pstEmvDtls->stEmvAppDtls.szAppTranCounter);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"AIP_82",pstEmvDtls->stEmvAppDtls.szAppIntrChngProfile);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_VERSION_NUM_9F09", pstEmvDtls->stEmvAppDtls.szAppVersNum);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_ICC_VERSION_NUM_9F08", pstEmvDtls->stEmvAppDtls.szICCAppVersNum);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"AMOUNT_AUTHORISED_9F02", pstEmvDtls->stEmvtranDlts.szAuthAmnt);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"AUTH_RESP_CODE_8A", pstEmvDtls->stEmvtranDlts.szAuthRespCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"CVM_RESULTS_9F34", pstEmvDtls->stEmvtranDlts.szCVMResult);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"DEDICATED_FILE_NAME_84", pstEmvDtls->stEmvtranDlts.szDedicatedFileName);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"AMOUNT_OTHER_9F03", pstEmvDtls->stEmvtranDlts.szEMVCashBackAmnt);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"EMV_TRANS_TIME_9F21", pstEmvDtls->stEmvtranDlts.szTranTime);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TRANSACTION_CATEGORY_CODE_9F53", pstEmvDtls->stEmvtranDlts.szTranCategoryCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"ICC_FORM_FACTOR_9F6E", pstEmvDtls->stEmvtranDlts.szICCFormFactor);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TRANS_STATUS_INFO_9B", pstEmvDtls->stEmvtranDlts.szTranStatusInfo);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TERM_TRANS_CODE_9F1A", pstEmvDtls->stEmvtranDlts.szTermTranCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TRANS_DATE_9A", pstEmvDtls->stEmvtranDlts.szTermTranDate);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TERMINAL_TRAN_QUALIFIER_9F66", pstEmvDtls->stEmvtranDlts.szTermTranQualfr);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TRANS_SEQ_COUNTER_9F41", pstEmvDtls->stEmvtranDlts.szTermTranSeqCount);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TRANS_REF_CURR_CODE_9F3C", pstEmvDtls->stEmvtranDlts.szTranRefCurCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"KeySerialNum", pstEmvDtls->stEmvtranDlts.szKeySerialNum);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"PINBlock", pstEmvDtls->stEmvtranDlts.szPINBlock);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"POS_ENTRY_MODE_9F39", pstEmvDtls->stEmvtranDlts.szPOSEntryMode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"PinTryCounter", pstEmvDtls->stEmvtranDlts.szPinTryCounter);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"CurrencyCode", pstEmvDtls->stEmvtranDlts.szCurrencyCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bPINByPassed", pstEmvDtls->stEmvtranDlts.bPINByPassed);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"CRYPTGRM_INFO_DATA_9F27", pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmInfoData);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_CRYPTGRM_9F26", pstEmvDtls->stEmvCryptgrmDtls.szAppCryptgrm);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TRANS_TYPE_9C", pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmTranType);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"CRYPTOGRM_CURR_CODE_5F2A", pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmCurrCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"UNPREDICTBLE_NUM_9F37", pstEmvDtls->stEmvCryptgrmDtls.szUnprdctbleNum);APP_TRACE(szDbgMsg);


	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"ISSUER_ID_42", pstEmvDtls->stEmvIssuerDtls.szIssuerID);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"CUSTOMER_EXCLUSIVE_DATA_9F7C", pstEmvDtls->stEmvIssuerDtls.szCustExclData);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"ISSUER_APP_DATA_9F10", pstEmvDtls->stEmvIssuerDtls.szIssuerAppData);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"ISSUER_AUTH_DATA_91", pstEmvDtls->stEmvIssuerDtls.szIssuerAuthData);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"ISSUER_SCRIPT_RESULTS_9F5B", pstEmvDtls->stEmvIssuerDtls.szIssueScrptResults);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"ISSUER_COUNTRY_CODE_9F56", pstEmvDtls->stEmvIssuerDtls.szIssuerCntryCode);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TERMINAL_AID_9F06", pstEmvDtls->stEmvTerDtls.szTermAID);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TERM_CAPABILITIES_9F33", pstEmvDtls->stEmvTerDtls.szTermCapabilityProf);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TERM_TYPE_9F35", pstEmvDtls->stEmvTerDtls.szTermType);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TERMINAL_VERIF_RESULTS_95", pstEmvDtls->stEmvTerDtls.szTermVerificationResults);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"MACValue", pstEmvDtls->stEmvTerDtls.szMACValue);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"DEVICE_SERIAL_NUM_9F1E", pstEmvDtls->stEmvTerDtls.szDevSerialNum);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"PINEnteredStatus", pstEmvDtls->stEmvtranDlts.PINEnteredStatus);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"CVMTpeChosen", pstEmvDtls->stEmvtranDlts.CVMTpeChosen);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bCardInserted", pstEmvDtls->stEMVCardStatusU02.bCardInserted);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bCardSwiped", pstEmvDtls->stEMVCardStatusU02.bCardSwiped);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bCardTapped", pstEmvDtls->stEMVCardStatusU02.bCardTapped);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bAppSelScreen", pstEmvDtls->stEMVCardStatusU02.bAppSelScreen);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bLangSelScreen", pstEmvDtls->stEMVCardStatusU02.bLangSelScreen);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bEMVPINEntry", pstEmvDtls->stEMVCardStatusU02.bEMVPINEntry);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bCardRemoved", pstEmvDtls->stEMVCardStatusU02.bCardRemoved);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bEMVRetry", pstEmvDtls->stEMVCardStatusU02.bEMVRetry);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"iCardSrc", pstEmvDtls->iCardSrc);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"iTrkNo", pstEmvDtls->iTrkNo);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"iEncType", pstEmvDtls->iEncType);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"iAccountType", pstEmvDtls->iAccountType);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"iTranSeqNum", pstEmvDtls->iTranSeqNum);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"iKeyPointer", pstEmvDtls->iKeyPointer);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"EMVlangSelctd", pstEmvDtls->EMVlangSelctd);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bSignReqd", pstEmvDtls->bSignReqd);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szEMVRespCode", pstEmvDtls->szEMVRespCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szEMVRespCmd", pstEmvDtls->szEMVRespCmd);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szEMVReqCmd", pstEmvDtls->szEMVReqCmd);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szEMVlangPref", pstEmvDtls->szEMVlangPref);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szXPIVer", pstEmvDtls->szXPIVer);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szPAN", pstEmvDtls->szPAN);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szName", pstEmvDtls->szName);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szTrackData", pstEmvDtls->szTrackData);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szServiceCode", pstEmvDtls->szServiceCode);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szEmvTags", pstEmvDtls->szEMVTags);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szEMVEncBlob", pstEmvDtls->szEMVEncBlob);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: ---Returning---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

}
#endif

/*
 * ============================================================================
 * Function Name: initEmvIFace
 *
 * Description	: This API would check whether emv feature is enabled on device. If yes, 
 *			 	  It would return SUCCESS if EMV(XPI) application is running else returns FAILURE.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int initEmvIFace()
{
	int			rv						= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	
	debug_sprintf(szDbgMsg, "%s: ---Enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
	while(1)
	{
		//check whether EMV App is running or not by sending S95 EMV command.
		rv = isEmvAgentRunning();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: EMV agent is not running", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		if(PAAS_FALSE == isEmvEnabledInDevice())
		{
			debug_sprintf(szDbgMsg, "%s: EMV is Disabled", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS;
			break;
		}
		debug_sprintf(szDbgMsg, "%s: EMV is Enabled", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: fillEMVData
 *
 * Description	: This API would store the EMV details from CARDDtls structure to EMVDTLS Structure
 *
 *
 * Input Params	: EMVDtls and CardDtls Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void fillEMVData(EMVDTLS_PTYPE pstEmvDtls, CARDDTLS_PTYPE pstCardDtls)
{
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(pstCardDtls->stEmvAppDtls.szAID) > 0)
		strcpy(pstEmvDtls->stEmvAppDtls.szAID, pstCardDtls->stEmvAppDtls.szAID);

	if(strlen(pstCardDtls->stEmvAppDtls.szAppSeqNum) > 0)
		strcpy(pstEmvDtls->stEmvAppDtls.szAppSeqNum, pstCardDtls->stEmvAppDtls.szAppSeqNum);

	if(strlen(pstCardDtls->stEmvAppDtls.szAppLabel) > 0)
		strcpy(pstEmvDtls->stEmvAppDtls.szAppLabel, pstCardDtls->stEmvAppDtls.szAppLabel);

	if(strlen(pstCardDtls->stEmvAppDtls.szAppPrefName) > 0)
		strcpy(pstEmvDtls->stEmvAppDtls.szAppPrefName, pstCardDtls->stEmvAppDtls.szAppPrefName);

	if(strlen(pstCardDtls->stEmvAppDtls.szAppEffctiveDate) > 0)
		strcpy(pstEmvDtls->stEmvAppDtls.szAppEffctiveDate, pstCardDtls->stEmvAppDtls.szAppEffctiveDate);

	if(strlen(pstCardDtls->stEmvAppDtls.szClearAppExpiryDate) > 0)
		strcpy(pstEmvDtls->stEmvAppDtls.szClearAppExpiryDate, pstCardDtls->stEmvAppDtls.szClearAppExpiryDate);

	if(strlen(pstCardDtls->stEmvAppDtls.szEncAppExpiryDate) > 0)
		strcpy(pstEmvDtls->stEmvAppDtls.szEncAppExpiryDate, pstCardDtls->stEmvAppDtls.szEncAppExpiryDate);

	if(strlen(pstCardDtls->stEmvAppDtls.szAppTranCounter) > 0)
		strcpy(pstEmvDtls->stEmvAppDtls.szAppTranCounter, pstCardDtls->stEmvAppDtls.szAppTranCounter);

	if(strlen(pstCardDtls->stEmvAppDtls.szAppIntrChngProfile) > 0)
		strcpy(pstEmvDtls->stEmvAppDtls.szAppIntrChngProfile, pstCardDtls->stEmvAppDtls.szAppIntrChngProfile);

	if(strlen(pstCardDtls->stEmvAppDtls.szAppVersNum) > 0)
		strcpy(pstEmvDtls->stEmvAppDtls.szAppVersNum, pstCardDtls->stEmvAppDtls.szAppVersNum);


	if(strlen(pstCardDtls->stEmvtranDlts.szAuthAmnt) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szAuthAmnt, pstCardDtls->stEmvtranDlts.szAuthAmnt);

	if(strlen(pstCardDtls->stEmvtranDlts.szAuthRespCode) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szAuthRespCode, pstCardDtls->stEmvtranDlts.szAuthRespCode);

	if(strlen(pstCardDtls->stEmvtranDlts.szCVMResult) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szCVMResult, pstCardDtls->stEmvtranDlts.szCVMResult);

	if(strlen(pstCardDtls->stEmvtranDlts.szDedicatedFileName) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szDedicatedFileName, pstCardDtls->stEmvtranDlts.szDedicatedFileName);

	if(strlen(pstCardDtls->stEmvtranDlts.szEMVCashBackAmnt) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szEMVCashBackAmnt, pstCardDtls->stEmvtranDlts.szEMVCashBackAmnt);

	if(strlen(pstCardDtls->stEmvtranDlts.szTranTime) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szTranTime, pstCardDtls->stEmvtranDlts.szTranTime);

	if(strlen(pstCardDtls->stEmvtranDlts.szTranCategoryCode) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szTranCategoryCode, pstCardDtls->stEmvtranDlts.szTranCategoryCode);

	if(strlen(pstCardDtls->stEmvtranDlts.szICCFormFactor) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szICCFormFactor, pstCardDtls->stEmvtranDlts.szICCFormFactor);

	if(strlen(pstCardDtls->stEmvtranDlts.szTranStatusInfo) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szTranStatusInfo, pstCardDtls->stEmvtranDlts.szTranStatusInfo);

	if(strlen(pstCardDtls->stEmvtranDlts.szTermTranCode) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szTermTranCode, pstCardDtls->stEmvtranDlts.szTermTranCode);

	if(strlen(pstCardDtls->stEmvtranDlts.szTermTranDate) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szTermTranDate, pstCardDtls->stEmvtranDlts.szTermTranDate);

	if(strlen(pstCardDtls->stEmvtranDlts.szTermTranQualfr) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szTermTranQualfr, pstCardDtls->stEmvtranDlts.szTermTranQualfr);

	if(strlen(pstCardDtls->stEmvtranDlts.szTermTranSeqCount) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szTermTranSeqCount, pstCardDtls->stEmvtranDlts.szTermTranSeqCount);

	if(strlen(pstCardDtls->stEmvtranDlts.szTranRefCurCode) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szTranRefCurCode, pstCardDtls->stEmvtranDlts.szTranRefCurCode);

	if(strlen(pstCardDtls->stEmvtranDlts.szKeySerialNum) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szKeySerialNum, pstCardDtls->stEmvtranDlts.szKeySerialNum);

	if(strlen(pstCardDtls->stEmvtranDlts.szPINBlock) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szPINBlock, pstCardDtls->stEmvtranDlts.szPINBlock);

	if(strlen(pstCardDtls->stEmvtranDlts.szPOSEntryMode) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szPOSEntryMode, pstCardDtls->stEmvtranDlts.szPOSEntryMode);

	if(strlen(pstCardDtls->stEmvtranDlts.szPinTryCounter) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szPinTryCounter, pstCardDtls->stEmvtranDlts.szPinTryCounter);

	if(strlen(pstCardDtls->stEmvtranDlts.szCurrencyCode) > 0)
		strcpy(pstEmvDtls->stEmvtranDlts.szCurrencyCode, pstCardDtls->stEmvtranDlts.szCurrencyCode);


	if(strlen(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData) > 0)
		strcpy(pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmInfoData, pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData);

	if(strlen(pstCardDtls->stEmvCryptgrmDtls.szAppCryptgrm) > 0)
		strcpy(pstEmvDtls->stEmvCryptgrmDtls.szAppCryptgrm, pstCardDtls->stEmvCryptgrmDtls.szAppCryptgrm);

	if(strlen(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmTranType) > 0)
		strcpy(pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmTranType, pstCardDtls->stEmvCryptgrmDtls.szCryptgrmTranType);

	if(strlen(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmCurrCode) > 0)
		strcpy(pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmCurrCode, pstCardDtls->stEmvCryptgrmDtls.szCryptgrmCurrCode);

	if(strlen(pstCardDtls->stEmvCryptgrmDtls.szUnprdctbleNum) > 0)
		strcpy(pstEmvDtls->stEmvCryptgrmDtls.szUnprdctbleNum, pstCardDtls->stEmvCryptgrmDtls.szUnprdctbleNum);


	if(strlen(pstCardDtls->stEmvIssuerDtls.szIssuerID) > 0)
		strcpy(pstEmvDtls->stEmvIssuerDtls.szIssuerID, pstCardDtls->stEmvIssuerDtls.szIssuerID);

	if(strlen(pstCardDtls->stEmvIssuerDtls.szCustExclData) > 0)
		strcpy(pstEmvDtls->stEmvIssuerDtls.szCustExclData, pstCardDtls->stEmvIssuerDtls.szCustExclData);

	if(strlen(pstCardDtls->stEmvIssuerDtls.szIssuerAppData) > 0)
		strcpy(pstEmvDtls->stEmvIssuerDtls.szIssuerAppData, pstCardDtls->stEmvIssuerDtls.szIssuerAppData);

	if(strlen(pstCardDtls->stEmvIssuerDtls.szIssuerAuthData) > 0)
		strcpy(pstEmvDtls->stEmvIssuerDtls.szIssuerAuthData, pstCardDtls->stEmvIssuerDtls.szIssuerAuthData);

	if(strlen(pstCardDtls->stEmvIssuerDtls.szIssueScrptResults) > 0)
		strcpy(pstEmvDtls->stEmvIssuerDtls.szIssueScrptResults, pstCardDtls->stEmvIssuerDtls.szIssueScrptResults);

	if(strlen(pstCardDtls->stEmvIssuerDtls.szIssuerCntryCode) > 0)
		strcpy(pstEmvDtls->stEmvIssuerDtls.szIssuerCntryCode, pstCardDtls->stEmvIssuerDtls.szIssuerCntryCode);


	if(strlen(pstCardDtls->stEmvTerDtls.szTermAID) > 0)
		strcpy(pstEmvDtls->stEmvTerDtls.szTermAID, pstCardDtls->stEmvTerDtls.szTermAID);

	if(strlen(pstCardDtls->stEmvTerDtls.szTermCapabilityProf) > 0)
		strcpy(pstEmvDtls->stEmvTerDtls.szTermCapabilityProf, pstCardDtls->stEmvTerDtls.szTermCapabilityProf);

	if(strlen(pstCardDtls->stEmvTerDtls.szTermType) > 0)
		strcpy(pstEmvDtls->stEmvTerDtls.szTermType, pstCardDtls->stEmvTerDtls.szTermType);

	if(strlen(pstCardDtls->stEmvTerDtls.szTermVerificationResults) > 0)
		strcpy(pstEmvDtls->stEmvTerDtls.szTermVerificationResults, pstCardDtls->stEmvTerDtls.szTermVerificationResults);

	if(strlen(pstCardDtls->stEmvTerDtls.szMACValue) > 0)
		strcpy(pstEmvDtls->stEmvTerDtls.szMACValue, pstCardDtls->stEmvTerDtls.szMACValue);

	if(strlen(pstCardDtls->stEmvTerDtls.szDevSerialNum) > 0)
		strcpy(pstEmvDtls->stEmvTerDtls.szDevSerialNum, pstCardDtls->stEmvTerDtls.szDevSerialNum);

	/*CID67436:T_POLISETTYG1: self assignment which has no affect */
	pstEmvDtls->stEmvtranDlts.PINEnteredStatus = pstCardDtls->stEmvtranDlts.PINEnteredStatus;
	pstEmvDtls->stEmvtranDlts.CVMTpeChosen = pstCardDtls->stEmvtranDlts.CVMTpeChosen;

	if(pstCardDtls->iCardSrc != 0)
		pstEmvDtls->iCardSrc	=	pstCardDtls->iCardSrc;
	if(pstCardDtls->iTrkNo != 0)
		pstEmvDtls->iTrkNo 	=	pstCardDtls->iTrkNo;
	if(pstCardDtls->iEncType != 0)
		pstEmvDtls->iEncType 	=	pstCardDtls->iEncType;
	if(pstCardDtls->EMVlangSelctd != 0)
		pstEmvDtls->EMVlangSelctd = pstCardDtls->EMVlangSelctd;
	if(pstCardDtls->iAccountType != 0)
		pstEmvDtls->iAccountType = pstCardDtls->iAccountType;
	if(pstCardDtls->iKeyPointer != 0)
		pstEmvDtls->iKeyPointer = pstCardDtls->iKeyPointer;
	if(pstCardDtls->iTranSeqNum != 0)
		pstEmvDtls->iTranSeqNum = pstCardDtls->iTranSeqNum;
	if(pstCardDtls->bSignReqd == PAAS_TRUE)
		pstEmvDtls->bSignReqd = PAAS_TRUE;

	if(strlen(pstCardDtls->szEMVReqCmd) > 0)
		strcpy(pstEmvDtls->szEMVReqCmd, pstCardDtls->szEMVReqCmd);
	if(strlen(pstCardDtls->szEMVRespCmd) > 0)
		strcpy(pstEmvDtls->szEMVRespCmd, pstCardDtls->szEMVRespCmd);
	if(strlen(pstCardDtls->szEMVRespCode) > 0)
		strcpy(pstEmvDtls->szEMVRespCode, pstCardDtls->szEMVRespCode);
	if(strlen(pstCardDtls->szEMVlangPref) > 0)
		strcpy(pstEmvDtls->szEMVlangPref, pstCardDtls->szEMVlangPref);
	if(strlen(pstCardDtls->szXPIVer) > 0)
		strcpy(pstEmvDtls->szXPIVer, pstCardDtls->szXPIVer);
	if(strlen(pstCardDtls->szPAN) > 0)
		strcpy(pstEmvDtls->szPAN, pstCardDtls->szPAN);
	if(strlen(pstCardDtls->szName) > 0)
		strcpy(pstEmvDtls->szName, pstCardDtls->szName);
	if(strlen(pstCardDtls->szTrackData) > 0)
		strcpy(pstEmvDtls->szTrackData, pstCardDtls->szTrackData);
	if(strlen(pstCardDtls->szServiceCode) > 0)
		strcpy(pstEmvDtls->szServiceCode, pstCardDtls->szServiceCode);

	if(strlen(pstCardDtls->szEMVTags) > 0)
		strcat(pstEmvDtls->szEMVTags, pstCardDtls->szEMVTags);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: storeEmvCardData
 *
 * Description	: This API would store the EMV details from EMVDTLS structure to CARDDtls Structure
 *
 *
 * Input Params	: EMVDtls and CardDtls Structure
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void storeEmvCardData(EMVDTLS_PTYPE pstEmvDtls, CARDDTLS_PTYPE pstCardDtls)
{
	char*		pszRsaFingerPrint 	= NULL;
	char*		cTmp				= NULL;
	char		szTmp[100]			= "";
	char		szTmp2[50]			= "";
	char		szPayPassType[3]	= "";
	char		szPayPassTypeHex[5]	= "";
	PAAS_BOOL	bIsDF79Reqd			= PAAS_FALSE;
#ifdef DEBUG
	char		szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstEmvDtls == NULL || pstCardDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed ", __FUNCTION__);
					APP_TRACE(szDbgMsg);
		return;
	}

	cTmp = getEMVTagsReqByHost();
    if(cTmp != NULL && strstr(cTmp, "DF79") != NULL)
    {
    	bIsDF79Reqd = PAAS_TRUE;
    }

	pstCardDtls->bEmvData = PAAS_TRUE;
	//pstCardDtls->bSAFAllwd = PAAS_TRUE;

	pstCardDtls->iEncType = getEncryptionType();

	if(strlen(pstEmvDtls->stEmvAppDtls.szAID) > 0)
		strcpy(pstCardDtls->stEmvAppDtls.szAID, pstEmvDtls->stEmvAppDtls.szAID);
	if(strlen(pstEmvDtls->stEmvAppDtls.szAppSeqNum) > 0)
		strcpy(pstCardDtls->stEmvAppDtls.szAppSeqNum, pstEmvDtls->stEmvAppDtls.szAppSeqNum);
	if(strlen(pstEmvDtls->stEmvAppDtls.szAppLabel) > 0)
		strcpy(pstCardDtls->stEmvAppDtls.szAppLabel, pstEmvDtls->stEmvAppDtls.szAppLabel);
	if(strlen(pstEmvDtls->stEmvAppDtls.szAppPrefName) > 0)
		strcpy(pstCardDtls->stEmvAppDtls.szAppPrefName, pstEmvDtls->stEmvAppDtls.szAppPrefName);
	if(strlen(pstEmvDtls->stEmvAppDtls.szAppEffctiveDate) > 0)
		strcpy(pstCardDtls->stEmvAppDtls.szAppEffctiveDate, pstEmvDtls->stEmvAppDtls.szAppEffctiveDate);
	if(strlen(pstEmvDtls->stEmvAppDtls.szAppTranCounter) > 0)
		strcpy(pstCardDtls->stEmvAppDtls.szAppTranCounter, pstEmvDtls->stEmvAppDtls.szAppTranCounter);
	if(strlen(pstEmvDtls->stEmvAppDtls.szAppIntrChngProfile) > 0)
		strcpy(pstCardDtls->stEmvAppDtls.szAppIntrChngProfile, pstEmvDtls->stEmvAppDtls.szAppIntrChngProfile);
	if(strlen(pstEmvDtls->stEmvAppDtls.szAppVersNum) > 0)
		strcpy(pstCardDtls->stEmvAppDtls.szAppVersNum, pstEmvDtls->stEmvAppDtls.szAppVersNum);
	if(strlen(pstEmvDtls->stEmvAppDtls.szICCAppVersNum) > 0)
		strcpy(pstCardDtls->stEmvAppDtls.szICCAppVersNum, pstEmvDtls->stEmvAppDtls.szICCAppVersNum);
	if(strlen(pstEmvDtls->stEmvAppDtls.szAppUsageControl) > 0)
		strcpy(pstCardDtls->stEmvAppDtls.szAppUsageControl, pstEmvDtls->stEmvAppDtls.szAppUsageControl);
	if(strlen(pstEmvDtls->stEmvAppDtls.szEncAppExpiryDate) > 0)
		strcpy(pstCardDtls->stEmvAppDtls.szEncAppExpiryDate, pstEmvDtls->stEmvAppDtls.szEncAppExpiryDate);
	if(strlen(pstEmvDtls->stEmvAppDtls.szClearAppExpiryDate) > 0)
	{
		strcpy(pstCardDtls->stEmvAppDtls.szClearAppExpiryDate, pstEmvDtls->stEmvAppDtls.szClearAppExpiryDate);
		if(strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP) == SUCCESS)
		{
			strncpy(pstCardDtls->szClrYear, pstEmvDtls->stEmvAppDtls.szClearAppExpiryDate, 2);
			strncpy(pstCardDtls->szClrMon, pstEmvDtls->stEmvAppDtls.szClearAppExpiryDate + 2, 2);
		}
	}

	if(strlen(pstEmvDtls->stEmvtranDlts.szAuthAmnt) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szAuthAmnt, pstEmvDtls->stEmvtranDlts.szAuthAmnt);
	if(strlen(pstEmvDtls->stEmvtranDlts.szAuthRespCode) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szAuthRespCode, pstEmvDtls->stEmvtranDlts.szAuthRespCode);
	if(strlen(pstEmvDtls->stEmvtranDlts.szCVMResult) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szCVMResult, pstEmvDtls->stEmvtranDlts.szCVMResult);
	if(strlen(pstEmvDtls->stEmvtranDlts.szDedicatedFileName) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szDedicatedFileName, pstEmvDtls->stEmvtranDlts.szDedicatedFileName);
	if(strlen(pstEmvDtls->stEmvtranDlts.szEMVCashBackAmnt) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szEMVCashBackAmnt, pstEmvDtls->stEmvtranDlts.szEMVCashBackAmnt);
	if(strlen(pstEmvDtls->stEmvtranDlts.szTranTime) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szTranTime, pstEmvDtls->stEmvtranDlts.szTranTime);
	if(strlen(pstEmvDtls->stEmvtranDlts.szTranCategoryCode) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szTranCategoryCode, pstEmvDtls->stEmvtranDlts.szTranCategoryCode);
	if(strlen(pstEmvDtls->stEmvtranDlts.szICCFormFactor) > 0)
	{
		strcpy(pstCardDtls->stEmvtranDlts.szICCFormFactor, pstEmvDtls->stEmvtranDlts.szICCFormFactor);
		strncpy(szPayPassTypeHex, pstCardDtls->stEmvtranDlts.szICCFormFactor + 8, 4);
		asciiToHex((unsigned char*)szPayPassType, szPayPassTypeHex, 2);
		sprintf(pstCardDtls->szPayPassType, "%d", atoi(szPayPassType));
	}
	if(strlen(pstEmvDtls->stEmvtranDlts.szTranStatusInfo) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szTranStatusInfo, pstEmvDtls->stEmvtranDlts.szTranStatusInfo);
	if(strlen(pstEmvDtls->stEmvtranDlts.szTermTranCode) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szTermTranCode, pstEmvDtls->stEmvtranDlts.szTermTranCode);
	if(strlen(pstEmvDtls->stEmvtranDlts.szTermTranDate) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szTermTranDate, pstEmvDtls->stEmvtranDlts.szTermTranDate);
	if(strlen(pstEmvDtls->stEmvtranDlts.szTermTranQualfr) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szTermTranQualfr, pstEmvDtls->stEmvtranDlts.szTermTranQualfr);
	if(strlen(pstEmvDtls->stEmvtranDlts.szTermTranSeqCount) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szTermTranSeqCount, pstEmvDtls->stEmvtranDlts.szTermTranSeqCount);
	if(strlen(pstEmvDtls->stEmvtranDlts.szTranRefCurCode) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szTranRefCurCode, pstEmvDtls->stEmvtranDlts.szTranRefCurCode);
	if(strlen(pstEmvDtls->stEmvtranDlts.szKeySerialNum) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szKeySerialNum, pstEmvDtls->stEmvtranDlts.szKeySerialNum);
	if(strlen(pstEmvDtls->stEmvtranDlts.szPINBlock) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szPINBlock, pstEmvDtls->stEmvtranDlts.szPINBlock);
	if(strlen(pstEmvDtls->stEmvtranDlts.szPOSEntryMode) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szPOSEntryMode, pstEmvDtls->stEmvtranDlts.szPOSEntryMode);
	if(strlen(pstEmvDtls->stEmvtranDlts.szPinTryCounter) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szPinTryCounter, pstEmvDtls->stEmvtranDlts.szPinTryCounter);
	if(strlen(pstEmvDtls->stEmvtranDlts.szCurrencyCode) > 0)
		strcpy(pstCardDtls->stEmvtranDlts.szCurrencyCode, pstEmvDtls->stEmvtranDlts.szCurrencyCode);

	if(strlen(pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmInfoData) > 0)
		strcpy(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData, pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmInfoData);
	if(strlen(pstEmvDtls->stEmvCryptgrmDtls.szAppCryptgrm) > 0)
		strcpy(pstCardDtls->stEmvCryptgrmDtls.szAppCryptgrm, pstEmvDtls->stEmvCryptgrmDtls.szAppCryptgrm);
	if(strlen(pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmTranType) > 0)
		strcpy(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmTranType, pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmTranType);
	if(strlen(pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmCurrCode) > 0)
		strcpy(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmCurrCode, pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmCurrCode);
	if(strlen(pstEmvDtls->stEmvCryptgrmDtls.szUnprdctbleNum) > 0)
		strcpy(pstCardDtls->stEmvCryptgrmDtls.szUnprdctbleNum, pstEmvDtls->stEmvCryptgrmDtls.szUnprdctbleNum);

	if(strlen(pstEmvDtls->stEmvIssuerDtls.szIssuerID) > 0)
		strcpy(pstCardDtls->stEmvIssuerDtls.szIssuerID, pstEmvDtls->stEmvIssuerDtls.szIssuerID);
	if(strlen(pstEmvDtls->stEmvIssuerDtls.szCustExclData) > 0)
		strcpy(pstCardDtls->stEmvIssuerDtls.szCustExclData, pstEmvDtls->stEmvIssuerDtls.szCustExclData);
	if(strlen(pstEmvDtls->stEmvIssuerDtls.szIssuerAppData) > 0)
		strcpy(pstCardDtls->stEmvIssuerDtls.szIssuerAppData, pstEmvDtls->stEmvIssuerDtls.szIssuerAppData);
	if(strlen(pstEmvDtls->stEmvIssuerDtls.szIssuerAuthData) > 0)
		strcpy(pstCardDtls->stEmvIssuerDtls.szIssuerAuthData, pstEmvDtls->stEmvIssuerDtls.szIssuerAuthData);
	if(strlen(pstEmvDtls->stEmvIssuerDtls.szIssueScrptResults) > 0)
		strcpy(pstCardDtls->stEmvIssuerDtls.szIssueScrptResults, pstEmvDtls->stEmvIssuerDtls.szIssueScrptResults);
	if(strlen(pstEmvDtls->stEmvIssuerDtls.szIssuerCntryCode) > 0)
		strcpy(pstCardDtls->stEmvIssuerDtls.szIssuerCntryCode, pstEmvDtls->stEmvIssuerDtls.szIssuerCntryCode);
	if(strlen(pstEmvDtls->stEmvIssuerDtls.szIACDefault) > 0)
			strcpy(pstCardDtls->stEmvIssuerDtls.szIACDefault, pstEmvDtls->stEmvIssuerDtls.szIACDefault);
	if(strlen(pstEmvDtls->stEmvIssuerDtls.szIACDenial) > 0)
			strcpy(pstCardDtls->stEmvIssuerDtls.szIACDenial, pstEmvDtls->stEmvIssuerDtls.szIACDenial);
	if(strlen(pstEmvDtls->stEmvIssuerDtls.szIACOnline) > 0)
			strcpy(pstCardDtls->stEmvIssuerDtls.szIACOnline, pstEmvDtls->stEmvIssuerDtls.szIACOnline);
	if(strlen(pstEmvDtls->stEmvIssuerDtls.szIssuerCodeTableIndex) > 0)
			strcpy(pstCardDtls->stEmvIssuerDtls.szIssuerCodeTableIndex, pstEmvDtls->stEmvIssuerDtls.szIssuerCodeTableIndex);

	if(strlen(pstEmvDtls->stEmvTerDtls.szTermAID) > 0)
		strcpy(pstCardDtls->stEmvTerDtls.szTermAID, pstEmvDtls->stEmvTerDtls.szTermAID);
	if(strlen(pstEmvDtls->stEmvTerDtls.szTermCapabilityProf) > 0)
		strcpy(pstCardDtls->stEmvTerDtls.szTermCapabilityProf, pstEmvDtls->stEmvTerDtls.szTermCapabilityProf);
	if(strlen(pstEmvDtls->stEmvTerDtls.szTermType) > 0)
		strcpy(pstCardDtls->stEmvTerDtls.szTermType, pstEmvDtls->stEmvTerDtls.szTermType);
	if(strlen(pstEmvDtls->stEmvTerDtls.szTermVerificationResults) > 0)
		strcpy(pstCardDtls->stEmvTerDtls.szTermVerificationResults, pstEmvDtls->stEmvTerDtls.szTermVerificationResults);
	if(strlen(pstEmvDtls->stEmvTerDtls.szMACValue) > 0)
		strcpy(pstCardDtls->stEmvTerDtls.szMACValue, pstEmvDtls->stEmvTerDtls.szMACValue);
	if(strlen(pstEmvDtls->stEmvTerDtls.szDevSerialNum) > 0)
		strcpy(pstCardDtls->stEmvTerDtls.szDevSerialNum, pstEmvDtls->stEmvTerDtls.szDevSerialNum);

	pstCardDtls->stEmvtranDlts.PINEnteredStatus = pstEmvDtls->stEmvtranDlts.PINEnteredStatus;
	pstCardDtls->stEmvtranDlts.CVMTpeChosen = pstEmvDtls->stEmvtranDlts.CVMTpeChosen;

	if(pstEmvDtls->iCardSrc != 0)
		pstCardDtls->iCardSrc	=	pstEmvDtls->iCardSrc;
	if(pstEmvDtls->iTrkNo != 0)
		pstCardDtls->iTrkNo 	=	pstEmvDtls->iTrkNo;
	if(pstEmvDtls->iEncType != 0)
		pstCardDtls->iEncType 	=	pstEmvDtls->iEncType;
	if(pstEmvDtls->EMVlangSelctd != 0)
		pstCardDtls->EMVlangSelctd = pstEmvDtls->EMVlangSelctd;
	if(pstEmvDtls->iAccountType != 0)
		pstCardDtls->iAccountType = pstEmvDtls->iAccountType;
	if(pstEmvDtls->iKeyPointer != 0)
		pstCardDtls->iKeyPointer = pstEmvDtls->iKeyPointer;
	if(pstEmvDtls->iTranSeqNum != 0)
		pstCardDtls->iTranSeqNum = pstEmvDtls->iTranSeqNum;
	if(pstEmvDtls->bSignReqd == PAAS_TRUE)
		pstCardDtls->bSignReqd = PAAS_TRUE;

	if(strlen(pstEmvDtls->szEMVReqCmd) > 0)
		strcpy(pstCardDtls->szEMVReqCmd, pstEmvDtls->szEMVReqCmd);

	if(strlen(pstEmvDtls->szEMVEncBlob) > 0)
	{
		strcpy(pstCardDtls->szEMVEncBlob, pstEmvDtls->szEMVEncBlob);
		if(pstEmvDtls->iEncType == VSD_ENC)
		{
			strcpy(pstCardDtls->szEncPayLoad, pstEmvDtls->szVsdKSN);
			strcpy(pstCardDtls->szVSDInitVector, pstEmvDtls->szVsdIV);
			if(strlen(pstEmvDtls->szClrTrk2) > 0)
			{
				strcpy(pstCardDtls->szClrTrkData, pstEmvDtls->szClrTrk2);
			}
			else if(strlen(pstEmvDtls->szClrTrk1) > 0)
			{
				strcpy(pstCardDtls->szClrTrkData, pstEmvDtls->szClrTrk1);
			}
			pstCardDtls->iEncType = VSD_ENC;

		}
		else if(pstEmvDtls->iEncType == RSA_ENC)
		{
			pszRsaFingerPrint = getRSAFingerPrint();
			strcpy(pstCardDtls->szEncPayLoad, pszRsaFingerPrint);
			pstCardDtls->iEncType = RSA_ENC;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Sould not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	if(strlen(pstEmvDtls->szEparms) > 0)
	{
		strcpy(pstCardDtls->szEncPayLoad, pstEmvDtls->szEparms);
		pstCardDtls->iEncType = VSP_ENC;
	}

	if(strlen(pstEmvDtls->szEMVRespCmd) > 0)
		strcpy(pstCardDtls->szEMVRespCmd, pstEmvDtls->szEMVRespCmd);
	if(strlen(pstEmvDtls->szEMVRespCode) > 0)
		strcpy(pstCardDtls->szEMVRespCode, pstEmvDtls->szEMVRespCode);
	if(strlen(pstEmvDtls->szEMVlangPref) > 0)
		strcpy(pstCardDtls->szEMVlangPref, pstEmvDtls->szEMVlangPref);
	if(strlen(pstEmvDtls->szXPIVer) > 0)
		strcpy(pstCardDtls->szXPIVer, pstEmvDtls->szXPIVer);
	if(strlen(pstEmvDtls->szPAN) > 0)
		strcpy(pstCardDtls->szPAN, pstEmvDtls->szPAN);
	if(strlen(pstEmvDtls->szName) > 0)
		strcpy(pstCardDtls->szName, pstEmvDtls->szName);
	if(strlen(pstEmvDtls->szTrackData) > 0)
		strcpy(pstCardDtls->szTrackData, pstEmvDtls->szTrackData);
	if(strlen(pstEmvDtls->szServiceCode) > 0)
		strcpy(pstCardDtls->szServiceCode, pstEmvDtls->szServiceCode);
	if( (pstCardDtls->bPartialEMV == PAAS_FALSE)	&&
		(strlen(pstEmvDtls->szEMVTags) > 0 )		&&
		(!strcmp(pstEmvDtls->szEMVRespCmd, EMV_C33_RESP) || pstEmvDtls->iCardSrc == CRD_EMV_CTLS)
		)
	{
		strcpy(pstCardDtls->szEMVTags, pstEmvDtls->szEMVTags);

#if 0
		//AjayS2: 20/Jan/2016: No need as we are sending 9F06 during parsing C31 for EMV CTLS
		//Copying Tag 4f to 9F06 in case of EMV Ctls card used
		if(pstEmvDtls->iCardSrc == CRD_EMV_CTLS)
		{
			memset(szTmp, 0x00, sizeof(szTmp));
			sprintf(szTmp, "%s%.2d%s", "9F06", strlen(pstEmvDtls->stEmvAppDtls.szAID)/2, pstEmvDtls->stEmvAppDtls.szAID);
			strcat(pstCardDtls->szEMVTags, szTmp);
			debug_sprintf(szDbgMsg, "%s: Copying in 9F06 from 4F tag for EMV_CTLS cards szTmp [%s]", __FUNCTION__, szTmp);
			APP_TRACE(szDbgMsg);
		}
#endif

		//Copying Cutom Tags to szEmvTags/EMV_TAGS field in SSI_Req if custom tags are required
		if(isEmvCustomTagsAllowed())
		{
			memset(szTmp, 0x00, sizeof(szTmp));
			memset(szTmp2, 0x00, sizeof(szTmp2));
			cTmp = getEMVKernelVersion();
			hexToAscii(szTmp2, (unsigned char *)cTmp, 2*strlen(cTmp));
			debug_sprintf(szDbgMsg, "%s: EMV Kernel Version as ASCII [%s]", __FUNCTION__, szTmp2);
			APP_TRACE(szDbgMsg);
			sprintf(szTmp, "%s%.2X%s", "FF21", strlen(szTmp2)/2, szTmp2);
			strcat(pstCardDtls->szEMVTags, szTmp);
			debug_sprintf(szDbgMsg, "%s: szTmp [%s]", __FUNCTION__, szTmp);
			APP_TRACE(szDbgMsg);

			memset(szTmp, 0x00, sizeof(szTmp));
			sprintf(szTmp, "%s%.2X%s", "FF20", 1, "35");
			strcat(pstCardDtls->szEMVTags, szTmp);
			debug_sprintf(szDbgMsg, "%s: szTmp [%s]", __FUNCTION__, szTmp);
			APP_TRACE(szDbgMsg);

			memset(szTmp, 0x00, sizeof(szTmp));
			if(isContactlessEmvEnabledInDevice() == PAAS_TRUE)
			{
				sprintf(szTmp, "%s%.2X%s", "FF22", 1, "32");
			}
			else
			{
				sprintf(szTmp, "%s%.2X%s", "FF22", 1, "30");
			}
			strcat(pstCardDtls->szEMVTags, szTmp);
			debug_sprintf(szDbgMsg, "%s: szTmp [%s]", __FUNCTION__, szTmp);
			APP_TRACE(szDbgMsg);
		}

		//AjayS2: Adding DF79 Tag
		if(bIsDF79Reqd)
		{
			cTmp = getEMVKernelVersion();
			memset(szTmp2, 0x00, sizeof(szTmp2));
			strcpy(szTmp2, cTmp);
			memset(szTmp, 0x00, sizeof(szTmp));
			sprintf(szTmp, "%c.%c%c", szTmp2[1], szTmp2[2], szTmp2[3]);
			strcat(szTmp, "l");

			memset(szTmp2, 0x00, sizeof(szTmp2));
			hexToAscii(szTmp2, (unsigned char *)szTmp, 2*strlen(szTmp));
			strupper(szTmp2);
			debug_sprintf(szDbgMsg, "%s: EMV Kernel Version as ASCII [%s]", __FUNCTION__, szTmp2);
			APP_TRACE(szDbgMsg);

			memset(szTmp, 0x00, sizeof(szTmp));
			sprintf(szTmp, "%s%.2X%s", "DF79", strlen(szTmp2)/2, szTmp2);
			strcat(pstCardDtls->szEMVTags, szTmp);
			debug_sprintf(szDbgMsg, "%s: szTmp [%s]", __FUNCTION__, szTmp);
			APP_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: addParametersinEMVUIReq
 *
 * Description	: This API would copy the devID and config type in XCONFIG
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void addParametersinEMVUIReq(char* szMsg, char* devID, char* configType)
{
	char		szTmp[100] = "";

	memset(szTmp, 0x00, 100);
	sprintf(szTmp, "%s%c%s%c", devID, FS, configType, FS);
	strcat(szMsg, szTmp);
}
#if 0
/*
 * ============================================================================
 * Function Name: updateKeyLoadListValue
 *
 * Description	:	Updates the values from key load info structure(values received from host)
 *					to key load linked list which holds default key load values from the ini file.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int updateKeyLoadListValue(EMVKEYLOAD_INFO_PTYPE pstEmvKeyLoadInfo)
{
	int							rv						= SUCCESS;
	EMVKEYLOAD_NODE_PTYPE		pstEmvKeyLoadNode		= NULL;	
#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstEmvKeyLoadNode = pstEmvKeyLoadInfo->mainLstHead;	
	while(pstEmvKeyLoadNode != pstEmvKeyLoadInfo->mainLstTail)
	{
		/* update key load values to the existing key load linked list */	
		updateEmvKeyLoadValues(pstEmvKeyLoadNode);		
		/*check next node */
		pstEmvKeyLoadNode = pstEmvKeyLoadNode->nextEmvNode;
	}
	debug_sprintf(szDbgMsg, "%s: ---Return----[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);	
	
	return rv;
}

/*
 * ============================================================================
 * Function Name: procEmvDwnldInit
 *
 * Description	: This API would get the key load values to build 'D' commands and sends to 
 *				  EMV (XPI) agent app through FormAgent app.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int procEmvDwnldInit(EMVKEYLOAD_INFO_PTYPE pstEmvKeyLoadInfo)
{
	int 						rv 						= SUCCESS;
	int							iNumRec					= 0;
	long 						lnTableNo				= 0;
	int 						iNxtCmd					= EMV_D16;
	PAAS_BOOL					bLoop					= PAAS_TRUE;
	EMVKEYLOAD_LST_INFO_PTYPE	pstTmpKeyLoadListInfo	= NULL;
	EMVKEYLOAD_LIST_PTYPE 		pstTempKeyLoadLst		= NULL;
	EMVKEYLOAD_LIST_PTYPE 		pstHeadKeyLoadLst		= NULL;	
	EMV_CMD_INFO_STYPE			stEmvCmdInfo;
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif	

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Update the values from key load info structure(values recieved from host) 
		to key load linked list which holds default key load values*/
	if( SUCCESS != updateKeyLoadListValue(pstEmvKeyLoadInfo))
	{
		debug_sprintf(szDbgMsg, "%s: Failed get key load list value", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	/* Get the key load linked list info node */
	if( (pstTmpKeyLoadListInfo = getKeyLoadListInfoNode(&iNumRec)) == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Failed get key load list info node", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;		
	}	

	//Assign main list head node.
	pstTempKeyLoadLst = pstHeadKeyLoadLst = pstTmpKeyLoadListInfo->mainKeyLoadLstHead;
	if(pstTempKeyLoadLst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Key load linked list is empty", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;		
	}

	//TODO: Testing purpose added. Remove it Later.
#ifdef DEVDEBUG
	while(pstTempKeyLoadLst != pstTmpKeyLoadListInfo->mainKeyLoadLstTail)
	{
		//TODO:Remove these debug logs later.
		int iNumCAPKFiles			= -1;

		debug_sprintf(szDbgMsg, "%s: szAppID[%s]", __FUNCTION__, pstTempKeyLoadLst->szAppID);
		APP_TRACE(szDbgMsg);	
		debug_sprintf(szDbgMsg, "%s: inFallbackAllowed[%d]", __FUNCTION__, 
									pstTempKeyLoadLst->stMvtRecInfo.inFallbackAllowed);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: lnFloorLimit[%ld]", __FUNCTION__, 
									pstTempKeyLoadLst->stMvtRecInfo.lnFloorLimit);
		APP_TRACE(szDbgMsg);
		
		iNumCAPKFiles = pstTempKeyLoadLst->iNumCAPKFiles - 1;
		while(iNumCAPKFiles > 0)
		{
			debug_sprintf(szDbgMsg, "%s: szModulus[%s]", __FUNCTION__, 
						pstTempKeyLoadLst->stCAPKInfo[iNumCAPKFiles].szModulus);
			APP_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: szRID[%s]", __FUNCTION__, 
						pstTempKeyLoadLst->stCAPKInfo[iNumCAPKFiles].szRID);
			APP_TRACE(szDbgMsg);		
			
			iNumCAPKFiles--;
		}

		pstTempKeyLoadLst = pstTempKeyLoadLst->nextEmvNode;
	}
#endif
	
	debug_sprintf(szDbgMsg, "%s: ---Total records=%d---", __FUNCTION__, iNumRec);
	APP_TRACE(szDbgMsg);

   /*Reset the command info structure*/
	memset(&stEmvCmdInfo, 0x00, sizeof(EMV_CMD_INFO_STYPE));	

	/* By this time key load list structure might have filled with proper values. 
	   Use these values to build 'D' commands*/	
	debug_sprintf(szDbgMsg, "%s: ---EMV Key Load Started---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
	while(1)
	{
		while(bLoop)
		{
			switch(iNxtCmd)
			{
				case EMV_D11:
					stEmvCmdInfo.iCurCmd	= EMV_D11;
					rv = processEmvD11cmd(&stEmvCmdInfo, pstHeadKeyLoadLst);
					if(rv ==SUCCESS)
					{
						iNxtCmd = getNxtEmvCmd(&stEmvCmdInfo);			
					}
					else
					{	
						iNxtCmd = rv;
					}
					debug_sprintf(szDbgMsg, "%s: EMV command[D11] returned[%d]", 
																__FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
					break;

				case EMV_D12:
					stEmvCmdInfo.iCurCmd	= EMV_D12;
					stEmvCmdInfo.iNumRec	= iNumRec;
					stEmvCmdInfo.lnTableNo	= lnTableNo;
					//Send D12 command only one time to clear CAPK files.
					stEmvCmdInfo.iExecCount	= 1; 
					rv = processEmvDcmd(&stEmvCmdInfo, pstHeadKeyLoadLst);
					if(rv ==SUCCESS)
					{
						iNxtCmd = getNxtEmvCmd(&stEmvCmdInfo);						
					}
					else
					{
						//Set EMV Resp/Error code.
						iNxtCmd = rv;
					}
					debug_sprintf(szDbgMsg, "%s: EMV command[D12] returned[%d]", 
					                                           __FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
					break;

				case EMV_D13:
					stEmvCmdInfo.iCurCmd	= EMV_D13;
					stEmvCmdInfo.iNumRec	= iNumRec;
					stEmvCmdInfo.lnTableNo	= lnTableNo;
					stEmvCmdInfo.iExecCount	= iNumRec;
					stEmvCmdInfo.iRecNo		= 0;
					stEmvCmdInfo.inAIDNo	= 1;
					rv = processEmvDcmd(&stEmvCmdInfo, pstHeadKeyLoadLst);
					if(rv ==SUCCESS)
					{
						iNxtCmd = getNxtEmvCmd(&stEmvCmdInfo);						
					}
					else
					{
						//Set EMV Resp/Error code.
						iNxtCmd = rv;
					}					
					debug_sprintf(szDbgMsg, "%s: EMV command[D13] returned[%d]", 
																__FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
					break;

				case EMV_D14:
					stEmvCmdInfo.iCurCmd	= EMV_D14;
					stEmvCmdInfo.iNumRec	= iNumRec;
					stEmvCmdInfo.lnTableNo	= lnTableNo;
					stEmvCmdInfo.iExecCount	= iNumRec;
					stEmvCmdInfo.iRecNo		= 0;	
					rv = processEmvDcmd(&stEmvCmdInfo, pstHeadKeyLoadLst);
					if(rv ==SUCCESS)
					{
						iNxtCmd = getNxtEmvCmd(&stEmvCmdInfo);					
					}
					else
					{
						//Set EMV Resp/Error code.
						iNxtCmd = rv;
					}				
					debug_sprintf(szDbgMsg, "%s: EMV command[D14] returned[%d]", 
															__FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
					break;

				case EMV_D15:
					stEmvCmdInfo.iCurCmd	= EMV_D15;
					stEmvCmdInfo.iNumRec	= iNumRec;
					stEmvCmdInfo.iRecNo		= 0;
					stEmvCmdInfo.lnTableNo	= lnTableNo;
					stEmvCmdInfo.iExecCount	= iNumRec;
					rv = processEmvDcmd(&stEmvCmdInfo, pstHeadKeyLoadLst);
					if(rv ==SUCCESS)
					{
						iNxtCmd = getNxtEmvCmd(&stEmvCmdInfo);								
					}
					else
					{
						//Set EMV Resp/Error code.
						iNxtCmd = rv;
					}				
					debug_sprintf(szDbgMsg, "%s: EMV command[D15] returned[%d]",
																__FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
					break;
					
				case EMV_D16:
					/* Increment Table Number here to create MVT, EST, EMVT, EEST tables*/
					lnTableNo++;
					stEmvCmdInfo.iCurCmd	= EMV_D16;
					stEmvCmdInfo.iNumRec	= iNumRec;
					stEmvCmdInfo.lnTableNo	= lnTableNo;
					stEmvCmdInfo.iExecCount	= 1;	//Send D16 command only one time.
					rv = processEmvDcmd(&stEmvCmdInfo, pstHeadKeyLoadLst);
					if(rv ==SUCCESS)
					{
						iNxtCmd = getNxtEmvCmd(&stEmvCmdInfo);
					}
					else
					{
						//Set EMV Resp/Error code.
						iNxtCmd = rv;
					}					
					debug_sprintf(szDbgMsg, "%s: EMV command[D16] returned[%d]", 
																__FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
					break;		

				case EMV_D17:
					stEmvCmdInfo.iCurCmd		= EMV_D17;
					stEmvCmdInfo.iNumRec		= iNumRec;
					stEmvCmdInfo.lnTableNo		= lnTableNo;
					stEmvCmdInfo.iRecNo			= 0;
					stEmvCmdInfo.iExecCount		= iNumRec;	//TODO: Need to check this.	
					rv = processEmvDcmd(&stEmvCmdInfo, pstHeadKeyLoadLst);
					if(rv ==SUCCESS)
					{
						iNxtCmd = getNxtEmvCmd(&stEmvCmdInfo);						
					}
					else
					{
						//Set EMV Resp/Error code.
						iNxtCmd = rv;
					}					
					debug_sprintf(szDbgMsg, "%s: EMV command[D17] returned[%d]", 
																__FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
					break;

				case EMV_D18:
					stEmvCmdInfo.iCurCmd	= EMV_D18;
					// stEmvCmdInfo.iNumRec	= iNumRec;
					// stEmvCmdInfo.lnTableNo	= lnTableNo;
					// stEmvCmdInfo.iExecCount	= 1;	
					rv = processEmvCTLSDcmd(&stEmvCmdInfo, pstTmpKeyLoadListInfo);
					if(rv ==SUCCESS)
					{
						iNxtCmd = getNxtEmvCmd(&stEmvCmdInfo);					
					}
					else
					{
						//Set EMV Resp/Error code.
						iNxtCmd = rv;
					}				
					debug_sprintf(szDbgMsg, "%s: EMV command[D18] returned[%d]", 
																__FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
					break;

				case EMV_D19:
					stEmvCmdInfo.iCurCmd	= EMV_D19;
					stEmvCmdInfo.iNumRec	= iNumRec;
					stEmvCmdInfo.lnTableNo	= lnTableNo;
					stEmvCmdInfo.iExecCount	= iNumRec;
					stEmvCmdInfo.iRecNo		= 0;
					rv = processEmvDcmd(&stEmvCmdInfo, pstHeadKeyLoadLst);
					if(rv ==SUCCESS)
					{
						iNxtCmd = getNxtEmvCmd(&stEmvCmdInfo);					
					}
					else
					{
						//Set EMV Resp/Error code.
						iNxtCmd = rv;
					}				
					debug_sprintf(szDbgMsg, "%s: EMV command[D19] returned[%d]", 
																__FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
					break;

				case EMV_D20:
					stEmvCmdInfo.iCurCmd	= EMV_D20;
					// stEmvCmdInfo.iNumRec	= iNumRec;
					// stEmvCmdInfo.lnTableNo	= lnTableNo;
					// stEmvCmdInfo.iExecCount	= 1;	
					rv = processEmvCTLSDcmd(&stEmvCmdInfo, pstTmpKeyLoadListInfo);
					if(rv ==SUCCESS)
					{
						iNxtCmd = getNxtEmvCmd(&stEmvCmdInfo);					
					}
					else
					{
						//Set EMV Resp/Error code.
						iNxtCmd = rv;
					}				
					debug_sprintf(szDbgMsg, "%s: EMV command[D20] returned[%d]",
															__FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
					break;

				case EMV_D25:
					stEmvCmdInfo.iCurCmd	= EMV_D25;
					// stEmvCmdInfo.iNumRec	= iNumRec;
					// stEmvCmdInfo.lnTableNo	= lnTableNo;
					// stEmvCmdInfo.iExecCount	= 1;
					rv = processEmvCTLSDcmd(&stEmvCmdInfo, pstTmpKeyLoadListInfo);
					if(rv ==SUCCESS)
					{
						iNxtCmd = getNxtEmvCmd(&stEmvCmdInfo);					
					}
					else
					{
						//Set EMV Resp/Error code.
						iNxtCmd = rv;
					}				
					debug_sprintf(szDbgMsg, "%s: EMV command[D25] returned[%d]", 
																__FUNCTION__, rv);
					APP_TRACE(szDbgMsg);
					break;
					
				case EMV_COMPLETE:
					debug_sprintf(szDbgMsg, "%s: Download Initialization flow completed",
																			__FUNCTION__);
					APP_TRACE(szDbgMsg);

					/* Exit from the loop*/
					bLoop = PAAS_FALSE;					
					break;
					
				default:
					debug_sprintf(szDbgMsg, "%s: Download Initialization flow interrupted",
																			__FUNCTION__);
					APP_TRACE(szDbgMsg);			
					
					/* Exit from the loop*/
					bLoop = PAAS_FALSE;
					break;					
			}					
		}
		if(bLoop == PAAS_FALSE)
		{
			break;
		}
		svcWait(100);
	}

	//Free allocated memory for storing EMV download parameters*/
	EMVKEYLOAD_NODE_PTYPE	pstTmpNode			= NULL;
	EMVKEYLOAD_NODE_PTYPE	pstEmvKeyLoadNode	= NULL;

	pstEmvKeyLoadNode = pstEmvKeyLoadInfo->mainLstHead; 	
	while(pstEmvKeyLoadNode != NULL)
	{		
		#if 0
		if(pstEmvKeyLoadNode->nextEmvNode == NULL)
		{			
			debug_sprintf(szDbgMsg, "%s: inside if, break;", __FUNCTION__);
			APP_TRACE(szDbgMsg);		
			svcWait(5000);
			break;
		}	
		if( (pstEmvKeyLoadNode->emvData != NULL) && 
			(pstEmvKeyLoadNode->emvKeyLoadType == PUBLIC_KEY) )
		{
			debug_sprintf(szDbgMsg, "%s: --- Freeing CAPKFiles data ---", __FUNCTION__);
			APP_TRACE(szDbgMsg);	
			EMV_CAPK_INFO_PTYPE		pstCAPKDtls  = NULL;	
			pstCAPKDtls = (EMV_CAPK_INFO_PTYPE)pstEmvKeyLoadNode->emvData;				
			if(pstCAPKDtls->pszCheckSum != NULL)
			{		
				free(pstCAPKDtls->pszCheckSum);
				debug_sprintf(szDbgMsg, "%s: --- Freed checksum data ---", __FUNCTION__);
				APP_TRACE(szDbgMsg);					
			}
			if(pstCAPKDtls->pszPKIndex != NULL)
			{
				free(pstCAPKDtls->pszPKIndex);
				debug_sprintf(szDbgMsg, "%s: --- Freed PKIndex data ---", __FUNCTION__);
				APP_TRACE(szDbgMsg);					
			}
			if(pstCAPKDtls->pszModulus != NULL)
			{
				free(pstCAPKDtls->pszModulus);
				debug_sprintf(szDbgMsg, "%s: --- Freed Modulus data ---", __FUNCTION__);
				APP_TRACE(szDbgMsg);					
			}
			if(pstCAPKDtls->pszExponent != NULL)
			{
				free(pstCAPKDtls->pszExponent);
				debug_sprintf(szDbgMsg, "%s: --- Freed Exponent data ---", __FUNCTION__);
				APP_TRACE(szDbgMsg);					
			}		
			free(pstEmvKeyLoadNode->emvData);		
			debug_sprintf(szDbgMsg, "%s: --- Freed CAPKFiles data ---", __FUNCTION__);
			APP_TRACE(szDbgMsg);					
		}
		#endif
		if(pstEmvKeyLoadNode->emvData != NULL)
		{	
			free(pstEmvKeyLoadNode->emvData);
		}
	
		pstTmpNode = pstEmvKeyLoadNode;
		pstEmvKeyLoadNode  = pstEmvKeyLoadNode->nextEmvNode;
		free(pstTmpNode);
		pstTmpNode = NULL;
	}	

	debug_sprintf(szDbgMsg, "%s: ---Returning---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);	
	
	return rv;
}

/*
 * ============================================================================
 * Function Name: processEmvD11cmd
 *
 * Description	: This function would process EMV 'D' command request.
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static int processEmvD11cmd(EMV_CMD_INFO_PTYPE pstEmvCmdInfo, 
									EMVKEYLOAD_LIST_PTYPE pstKeyLoadList)
{
	int 							rv					= SUCCESS;
	int								iNumCAPKFiles		= -1;
	EMVKEYLOAD_LIST_PTYPE 			pstTmpKeyLoadList	= NULL;
	PAAS_BOOL		bWait								= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif	

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Build and dispatch 'D' commands to EMV App(XPI App)*/
	pstTmpKeyLoadList = pstKeyLoadList;

	/* Get number of CAPK Files present for APPID.*/
	iNumCAPKFiles = pstTmpKeyLoadList->iNumCAPKFiles;			

	while(pstTmpKeyLoadList != NULL)
	{	
		if( (pstEmvCmdInfo->iCurCmd == EMV_D11) && 
			( (iNumCAPKFiles) <= 0 ))
		{
			debug_sprintf(szDbgMsg, "%s: Skip D11 command, PK NOT is present for APPID[%s]",
												__FUNCTION__, pstTmpKeyLoadList->szAppID);
			APP_TRACE(szDbgMsg);

			pstTmpKeyLoadList = pstTmpKeyLoadList->nextEmvNode;

			/* Get number of CAPK Files present for next APPID.*/
			if(pstTmpKeyLoadList != NULL)
			{
				iNumCAPKFiles = pstTmpKeyLoadList->iNumCAPKFiles;			
			}
			
			continue;
		}		
		/* Reset the req command structure for each command*/
		initUIReqMsg(&stEMVReq, INIT_REQ);
		
		pstEmvCmdInfo->iNumCAPKFiles = iNumCAPKFiles-1;
		rv = buildEmvDcmd(pstEmvCmdInfo, pstTmpKeyLoadList, &stEMVReq);

		//APP_TRACE("D Command is");
		//APP_TRACE_EX(stEMVReq.pszBuf, stEMVReq.iSize);

		if(rv == SUCCESS)
		{
			/*Its very important to wait until businesslogic thread 
			receives data from UI Agent*/
			bBLDataRcvd = PAAS_FALSE;
			
			rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																		__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_DEVICE_APP;
			}
			else
			{
				while(bWait == PAAS_TRUE)
				{	
					if(bBLDataRcvd == PAAS_TRUE)
					{
						acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
						if(stBLData.uiRespType == UI_EMV_RESP)
						{
							rv = getUIRespCodeOfEmvCmdResp(pstEmvCmdInfo->iCurCmd, stBLData.iStatus);
							switch(rv)
							{
								case SUCCESS:
										debug_sprintf(szDbgMsg, "%s: D command returned [SUCCESS]", __FUNCTION__);
										APP_TRACE(szDbgMsg);				

										break;						

								default:
									/* 'D' command failed */
									debug_sprintf(szDbgMsg, "%s: D command returned Error[%d]", __FUNCTION__,
																							stBLData.iStatus);
									APP_TRACE(szDbgMsg);			
									
									break;		
							}						
							bWait = PAAS_FALSE;
							bBLDataRcvd = PAAS_FALSE;
						}
						releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);				
					}
					svcWait(100);			
				}
			}		
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed to build command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}
		if(rv == SUCCESS)
		{
			//Decrement the counter
			iNumCAPKFiles--;		
			
			if( (pstEmvCmdInfo->iCurCmd == EMV_D11) &&
			  (iNumCAPKFiles > 0 ))
			{
				svcWait(1000);	//Its very important wait before sending next commnad
				
				debug_sprintf(szDbgMsg, "%s: Sending D11 command for next CAPK File",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);
						
				continue;
			}
		
			svcWait(1000);	//Its very important wait before sending next commnad

			debug_sprintf(szDbgMsg, "%s: Sending next 'D' command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			
			if(pstTmpKeyLoadList != NULL)
			{
				pstTmpKeyLoadList = pstTmpKeyLoadList->nextEmvNode;
			}
			/* Get number of CAPK Files present for next APPID.*/
			if(pstTmpKeyLoadList != NULL)
			{
				iNumCAPKFiles = pstTmpKeyLoadList->iNumCAPKFiles;			
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed send 'D' command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			
			break;
		}		
	}//End of while

	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processEmvDcmd
 *
 * Description	: This function would process EMV 'D' command request.
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static int processEmvDcmd(EMV_CMD_INFO_PTYPE pstEmvCmdInfo, EMVKEYLOAD_LIST_PTYPE pstKeyLoadList)
{
	int 							rv					= SUCCESS;
	int								iNumSuppAID 		= 0;
	int								iExecCount			= 0;
	EMVKEYLOAD_LIST_PTYPE 			pstTmpKeyLoadList	= NULL;
	PAAS_BOOL		bWait								= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif	

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Build and dispatch 'D' commands to EMV App(XPI App)*/
	pstTmpKeyLoadList = pstKeyLoadList;
	
	//Copy supported AID counter. This counter is used to send D13 and D17 commands that many times for each AID.
	iNumSuppAID	= pstTmpKeyLoadList->iNumSuppAid;

	/* Copy value to know how many times to send particular 'D' command*/
	iExecCount = pstEmvCmdInfo->iExecCount;
	while((iExecCount > 0) && (pstTmpKeyLoadList != NULL))
	{
		/* Reset the req command structure for each command*/
		initUIReqMsg(&stEMVReq, INIT_REQ);

		pstEmvCmdInfo->iNumSuppAid 	= iNumSuppAID;		
		
		rv = buildEmvDcmd(pstEmvCmdInfo, pstTmpKeyLoadList, &stEMVReq);

		//APP_TRACE("D Command is");
		//APP_TRACE_EX(stEMVReq.pszBuf, stEMVReq.iSize);

		if(rv == SUCCESS)
		{
			/*Its very important to wait until businesslogic thread 
			receives data from UI Agent*/
			bBLDataRcvd = PAAS_FALSE;
			
			rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																		__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_DEVICE_APP;
			}
			else
			{
				while(bWait == PAAS_TRUE)
				{	
					if(bBLDataRcvd == PAAS_TRUE)
					{
						acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
						if(stBLData.uiRespType == UI_EMV_RESP)
						{
							rv = getUIRespCodeOfEmvCmdResp(pstEmvCmdInfo->iCurCmd, stBLData.iStatus);
							switch(rv)
							{
								case SUCCESS:
										debug_sprintf(szDbgMsg, "%s: D command returned [SUCCESS]", __FUNCTION__);
										APP_TRACE(szDbgMsg);				

										break;						

								default:
									/* 'D' command failed */
									debug_sprintf(szDbgMsg, "%s: D command returned Error[%d]",__FUNCTION__,  stBLData.iStatus);
									APP_TRACE(szDbgMsg);			

									break;		
							}
							bWait = PAAS_FALSE;
							bBLDataRcvd = PAAS_FALSE;
						}
						releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);				
					}
					svcWait(100);			
				}
			}		
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed to build command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}

		if(rv == SUCCESS)
		{	
			svcWait(1000);	//Its very important wait before sending next commnad

			if( (pstEmvCmdInfo->iCurCmd == EMV_D13 || pstEmvCmdInfo->iCurCmd == EMV_D17) && 
			   ( iNumSuppAID > 0 ))
			{
				pstEmvCmdInfo->inAIDNo++;
				
				iNumSuppAID--;
				
				debug_sprintf(szDbgMsg, "%s: Sending next 'D' command", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				
				continue;
			}
			//Decrement the execution counter
			iExecCount--;
			
			debug_sprintf(szDbgMsg, "%s: Curr execution count=%d!", __FUNCTION__, iExecCount);
			APP_TRACE(szDbgMsg);			
			
			if(iExecCount <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: Completed!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			
			pstEmvCmdInfo->iRecNo++;
			//Set the AID number to 1 for new record
			pstEmvCmdInfo->inAIDNo = 1;		
			if(pstTmpKeyLoadList == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: KeyLoad List is empty", __FUNCTION__);
				APP_TRACE(szDbgMsg);			
				break;
			}		
			pstTmpKeyLoadList = pstTmpKeyLoadList->nextEmvNode;
			
			iNumSuppAID					= pstTmpKeyLoadList->iNumSuppAid;
			pstEmvCmdInfo->iNumSuppAid 	= iNumSuppAID;
			
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed send 'D' command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			
			break;
		}		
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processEmvCTLSDcmd
 *
 * Description	: This function would process EMV 'D'[D18, D20 and D25] command request.
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static int processEmvCTLSDcmd(EMV_CMD_INFO_PTYPE pstEmvCmdInfo, EMVKEYLOAD_LST_INFO_PTYPE pstKeyLoadListInfo)
{
	int 							rv						= SUCCESS;
	int								iCurCmd					= 0;	
	EMVKEYLOAD_LST_INFO_PTYPE 		pstTmpKeyLoadListInfo	= NULL;
	PAAS_BOOL		bWait									= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif	

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Build and dispatch 'D' commands to EMV App(XPI App)*/
	pstTmpKeyLoadListInfo = pstKeyLoadListInfo;
	
	//Get the current command
	iCurCmd	= pstEmvCmdInfo->iCurCmd;

	while(1)
	{
		/* Reset the req command structure for each command*/
		initUIReqMsg(&stEMVReq, INIT_REQ);

		switch (iCurCmd)
		{		
			case EMV_D18:
				debug_sprintf(szDbgMsg, "%s: Building EMV [D18] command", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				getD_18Command(stEMVReq.pszBuf,&stEMVReq.iSize, 
										&(pstTmpKeyLoadListInfo->stOtherRecInfo));			
				break;				
				
			case EMV_D20:
				debug_sprintf(szDbgMsg, "%s: Building EMV [D20] command", __FUNCTION__);
				APP_TRACE(szDbgMsg);	

				getD_20Command(stEMVReq.pszBuf,&stEMVReq.iSize, 
												 &pstTmpKeyLoadListInfo->stIcctRecInfo);			
				break;	
				
			case EMV_D25:
				debug_sprintf(szDbgMsg, "%s: Building EMV [D25] command", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				
				getD_25Command(stEMVReq.pszBuf, &stEMVReq.iSize, 
												&pstTmpKeyLoadListInfo->stOtherRecInfo);						
				break;	
							
			default:
				debug_sprintf(szDbgMsg, "%s: SHOULD NOT COME HERE [Unknow command]", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
		}			
		//APP_TRACE("D Command is");
		//APP_TRACE_EX(stEMVReq.pszBuf, stEMVReq.iSize);

		if(rv == SUCCESS)
		{
			/*Its very important to wait until businesslogic thread 
			receives data from UI Agent*/
			bBLDataRcvd = PAAS_FALSE;
			
			rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																		__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_DEVICE_APP;
			}
			else
			{
				while(bWait == PAAS_TRUE)
				{	
					if(bBLDataRcvd == PAAS_TRUE)
					{
						acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
						if(stBLData.uiRespType == UI_EMV_RESP)
						{
							rv = getUIRespCodeOfEmvCmdResp(iCurCmd, stBLData.iStatus);
							switch(rv)
							{
								case SUCCESS:
										debug_sprintf(szDbgMsg, "%s: D command returned [SUCCESS]", __FUNCTION__);
										APP_TRACE(szDbgMsg);				

										break;						

								default:
									/* 'D' command failed */
									debug_sprintf(szDbgMsg, "%s: D command returned Error[%d]",__FUNCTION__,  stBLData.iStatus);
									APP_TRACE(szDbgMsg);			

									break;		
							}
							bWait = PAAS_FALSE;
							bBLDataRcvd = PAAS_FALSE;
						}
						releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);				
					}
					svcWait(100);			
				}
			}
			break;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed to build command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}
		break;		
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: procEmvDwnldInit
 *
 * Description	: This API would send XCONFIG
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int procEmvDwnldInit(EMVKEYLOAD_INFO_PTYPE pstEmvKeyLoadInfo, char *pszErrMsg)
{
	int 						rv 						= SUCCESS;
	int							iAppLogEnabled			= isAppLogEnabled();
	char						szAppLogData[300]		= "";
	PAAS_BOOL					bWait					= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif	

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	/* Update the values from key load info structure(values recieved from host) 
		to key load linked list which holds default key load values*/
/*	if( SUCCESS != updateKeyLoadListValue(pstEmvKeyLoadInfo))
	{
		debug_sprintf(szDbgMsg, "%s: Failed get key load list value", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
*/
	initUIReqMsg(&stEMVReq, INIT_EMV_CONFIG_REQ);

	addParametersinEMVUIReq(stEMVReq.pszBuf, "IN", "EMV");
	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "Sending Command to Use EMV Config Files for EMV Initialization");
		addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
	}
	rv = sendUIReqMsg(stEMVReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error in sending request to UI Agent", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	while(bWait == PAAS_TRUE)
	{
		CHECK_POS_INITIATED_STATE;
		if(bBLDataRcvd == PAAS_TRUE)
		{
			acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

			if(stBLData.uiRespType == UI_XCONFIG_RESP)
			{
				if(stBLData.iStatus == 1)
				{
					rv = SUCCESS;
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "EMV Initialization Command Return Success");
						addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
					}
				}
				else
				{
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "EMV Initialization Command Return Failure");
						addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
					}
					rv = FAILURE;
					strcpy(pszErrMsg, stBLData.stRespDtls.stXConfigInfo.szRespStatus);
				}
				bBLDataRcvd = PAAS_FALSE;
				bWait 		= PAAS_FALSE;
			}

			releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			if(bWait == PAAS_FALSE)
			{
				break;
			}
		}
		svcWait(15);
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);	

	return rv;
}

/*
 * ============================================================================
 * Function Name: getVersionFromEMVAgent
 *
 * Description	: This API would get running EMV(XPI App) version information
 *                to be displayed on the screen.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getVersionFromEMVAgent(char* pszEMVVersion, char* pszEmvKernelVer, char* pszEmvCTLSVer)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	PAAS_BOOL		bWait				= PAAS_TRUE;
	char			szAppLogData[300]	= "";
#ifdef DEBUG
	char			szDbgMsg[128]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);
	
	rv = buildEmvCmd(EMV_S95, &stEMVReq, NULL);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;

				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						rv = getUIRespCodeOfEmvCmdResp(EMV_S95, stBLData.iStatus);
						switch(rv)
						{
						case SUCCESS:
							//Copying the XPI version from the response data
							memset(pszEmvKernelVer, 0x00, 10);

							strcpy(pszEMVVersion, stBLData.stRespDtls.stEmvDtls.szXPIVer);

							debug_sprintf(szDbgMsg, "%s: emvkerver [%s] %d", __FUNCTION__, stBLData.stRespDtls.stEmvDtls.stEmvTerDtls.szEMVKernelVer, strlen(stBLData.stRespDtls.stEmvDtls.stEmvTerDtls.szEMVKernelVer));
							APP_TRACE(szDbgMsg);

							memcpy(pszEmvKernelVer, stBLData.stRespDtls.stEmvDtls.stEmvTerDtls.szEMVKernelVer, strlen(stBLData.stRespDtls.stEmvDtls.stEmvTerDtls.szEMVKernelVer));

							strcpy(pszEmvCTLSVer, stBLData.stRespDtls.stEmvDtls.stEmvTerDtls.szEMVCTLSVer);

							debug_sprintf(szDbgMsg, "%s: XPIVersion [%s] CTLSVersion [%s] KernelVersion [%s]", __FUNCTION__, pszEMVVersion, pszEmvCTLSVer, pszEmvKernelVer);
							APP_TRACE(szDbgMsg);

							memset(szAppLogData, 0x00, sizeof(szAppLogData));
							if(iAppLogEnabled == 1)
							{
								sprintf(szAppLogData, "XPI Application Version Details [%s]", pszEMVVersion);
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}

							debug_sprintf(szDbgMsg, "%s: S95 command returned [SUCCESS]", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							break;

						default:
							/*  command failed */
							debug_sprintf(szDbgMsg, "%s: S95 command returned Error[%d]",__FUNCTION__,
									stBLData.iStatus);
							APP_TRACE(szDbgMsg);

							break;
						}					
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;						
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
					if(bWait == PAAS_FALSE)
					{
						break;
					}
				}

				svcWait(100);
			}		
		}		
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[S95]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sendS20CmdToGetMSRCardData
 *
 * Description	: This API would send S20 command to enable MSR.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendS20CmdToGetMSRCardData(char* szVASMode, char* szMerchantIndex, char* szCustData)
{
	int				rv				= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_EMV_REQ);
	
	//Store the request command type in the EMV dtls structure.
	setEmvReqCmd(EMV_S20);

//	rv = buildEmvCmd(EMV_S20, &stEMVReq, NULL);

	getS_20Command(stEMVReq.pszBuf, &stEMVReq.iSize, szVASMode, szMerchantIndex, szCustData);

	if(rv == SUCCESS)
	{
		rv = sendUIReqMsg(stEMVReq.pszBuf);
//		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}	

		else
		{
			setCardReadEnabled(PAAS_TRUE);
		}
	}
	else
	{
		rv = FAILURE;
	}
	
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sendC30CmdToGetChipCardData
 *
 * Description	: This API would send C30 command to smart card reader
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendC30CmdToGetChipCardData(char* szTranKey, char* szVASMode, char* szMerchantIndex, char* szCustData)
{
	int				rv				= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	//Store the request command type in the EMV dtls structure.
	setEmvReqCmd(EMV_C30);

	rv = getC_30Command(stEMVReq.pszBuf, &stEMVReq.iSize, szTranKey, szVASMode, szMerchantIndex, szCustData);

	rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		setCardReadEnabled(PAAS_TRUE);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processM41Command
 *
 * Description	: This API would set XPI[iab] param using M41
 *
 * Input Params	: Param name and Value
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int processM41Command(char* szParam, char* szValue)
{
	int				rv				= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	//Store the request command type in the EMV dtls structure.
	setEmvReqCmd(EMV_M41);

	getM_41Command(stEMVReq.pszBuf, &stEMVReq.iSize, szParam, szValue);

	rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sendD41cmdToEMVAgent
 *
 * Description	: This API would send D_41 command to XPI
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendD41cmdToEMVAgent(PAAS_BOOL bProvisionPass, char* szMerchIndex, char *pszCustData)
{
	int				rv				= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	//Store the request command type in the EMV dtls structure.
	setEmvReqCmd(EMV_D41);

	getD_41Command(stEMVReq.pszBuf, &stEMVReq.iSize, bProvisionPass, szMerchIndex, pszCustData);

	//rv = sendUIReqMsg_EX(stEMVReq.pszBuf, stEMVReq.iSize);
	rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		setCardReadEnabled(PAAS_TRUE);
		setD41Active(PAAS_TRUE);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: sendC19CmdToEMVAgent
 *
 * Description	: This API would send C19 command to smart card reader to update a
 * 					single EMV tag TLV
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendC19CmdToEMVAgent(char* szEmvTLV, int iEMVLen)
{
	int				rv				= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	//Store the request command type in the EMV dtls structure.
	setEmvReqCmd(EMV_C19);

	debug_sprintf(szDbgMsg, "%s: Building EMV [C19] command", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	getC_19Command(stEMVReq.pszBuf, &stEMVReq.iSize, szEmvTLV, iEMVLen);

	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[C19]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sendU01ResponseToEMVAgent
 *
 * Description	: This API would send U01 response based on flag
 *
 * Input Params	: AID_LIST_INFO_PTYPE and Bool to indicate whether to send all AID
 * 				  back or single selected to XPI
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendU01ResponseToEMVAgent(AID_LIST_INFO_PTYPE pstAIDListInfo, PAAS_BOOL bIsAIDChosen)
{
	int				rv						= SUCCESS;
	//AID_NAME_NODE_PTYPE pstAIDNameNode		= NULL;
	//AID_NAME_NODE_PTYPE pstTmpAIDNameNode	= NULL;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstAIDListInfo == NULL)
    {
    	debug_sprintf(szDbgMsg, "%s:NULL params Passed", __FUNCTION__);
    	APP_TRACE(szDbgMsg);
    	return FAILURE;
    }

	initUIReqMsg(&stEMVReq, INIT_REQ);

	debug_sprintf(szDbgMsg, "%s: Building EMV [U01] command", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = getU_01Command(stEMVReq.pszBuf, &stEMVReq.iSize, pstAIDListInfo, bIsAIDChosen);

	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[U01]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

#if 0
	//Freeing Memory of AID List
	pstAIDNameNode = pstTmpAIDNameNode = pstAIDListInfo->LstHead;
	while(pstAIDNameNode != NULL)
	{
		pstAIDNameNode = pstAIDNameNode->next;
		free(pstTmpAIDNameNode);
		pstTmpAIDNameNode = pstAIDNameNode;
	}
#endif
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: sendC36CmdToEMVAgent
 *
 * Description	: This API would send C36 command to XPI
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendC36CmdToEMVAgent(char *emvTags)
{
	int				rv				= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	//Store the request command type in the EMV dtls structure.
	setEmvReqCmd(EMV_C36);

	debug_sprintf(szDbgMsg, "%s: Building EMV [C36] command", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = getC_36Command(stEMVReq.pszBuf, &stEMVReq.iSize,emvTags);

	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[C36]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getReqdEMVTagsFromXPI
 *
 * Description	: This API is used to query for the required tags using C36 and store it in the
 * 				  card details structure. Currently we are storing only tags 9F07,8E and 9F33.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getReqdEMVTagsFromXPI(CARDDTLS_PTYPE pstCardDtls,char *emvTags,char *pszTranSSIKey)
{
	EMVDTLS_STYPE	stEmvDtls;
	PAAS_BOOL		bWait			= PAAS_TRUE;
	int				rv				= SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = sendC36CmdToEMVAgent(emvTags);
	if(rv == SUCCESS)
	{
		/* Wait for the response*/
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;

			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_EMV_RESP)
				{
					debug_sprintf(szDbgMsg, "%s: Received C36 EMV data", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					memset(&stEmvDtls, 0x00, sizeof(EMVDTLS_STYPE));
					memcpy(&stEmvDtls, &(stBLData.stRespDtls.stEmvDtls), sizeof(EMVDTLS_STYPE));

					debug_sprintf(szDbgMsg, "%s: EMV resp str= %s int= %d",__FUNCTION__, stEmvDtls.szEMVRespCode, atoi(stEmvDtls.szEMVRespCode));
					APP_TRACE(szDbgMsg);

					rv = getUIRespCodeOfEmvCmdResp(EMV_C36,(atoi(stEmvDtls.szEMVRespCode)));
					if(rv == SUCCESS)
					{
						strcpy(pstCardDtls->stEmvTerDtls.szTermCapabilityProf, stEmvDtls.stEmvTerDtls.szTermCapabilityProf);
						strcpy(pstCardDtls->stEmvAppDtls.szAppUsageControl,stEmvDtls.stEmvAppDtls.szAppUsageControl);
						strcpy(pstCardDtls->stEmvAppDtls.szIccCVMList,stEmvDtls.stEmvAppDtls.szIccCVMList);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error Response Code on C36 :%d",__FUNCTION__,rv);

						APP_TRACE(szDbgMsg);
						//rv = FAILURE; Avoid returning the failure , instead return the actual response code so that we can check if card is removed and act accordingly.
						//break;
					}

					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				if(bWait == PAAS_FALSE)
				{
					break;
				}
			}
			svcWait(15);
		}

		// CID 67949 (#1 of 1): Unused value (UNUSED_VALUE). T_RaghavendranR1. Do the Operation only when previous operation (rv) is success.
		if(rv == SUCCESS)
		{
			/* Should we take a seperate local card detail structure and update this, or is it ok if we update what was passed from execute work flow */
			rv = updateCardDtlsForPymtTran(pszTranSSIKey, pstCardDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure while updating card details to Payment Transaction",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

	}

	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to send command[C36]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: sendC32CmdToEMVAgent
 *
 * Description	: This API would send C32 command to smart card reader
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendC32CmdToEMVAgent(char* szTranKey)
{
	int				rv				= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	//Store the request command type in the EMV dtls structure.
	setEmvReqCmd(EMV_C32);

	debug_sprintf(szDbgMsg, "%s: Building EMV [C32] command", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = getC_32Command(stEMVReq.pszBuf, &stEMVReq.iSize, szTranKey);

	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[C32]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sendC34CmdToEMVAgent
 *
 * Description	: This API would send C34 command to smart card reader XPI
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendC34CmdToEMVAgent(CARDDTLS_PTYPE pstCardDtls, char* szTranKey, int iResultCode)
{
	int				rv				= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	//Store the request command type in the EMV dtls structure.
	setEmvReqCmd(EMV_C34);

	debug_sprintf(szDbgMsg, "%s: Building EMV [C34] command", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = getC_34Command(stEMVReq.pszBuf, &stEMVReq.iSize, szTranKey, iResultCode, pstCardDtls);

	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[C34]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: sendC20CmdToEMVAgent
 *
 * Description	: This API would send C20 command to smart card reader
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendC20CmdToEMVAgent(char* pszEmvTag, int iLen)
{
	int				rv				= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	//Store the request command type in the EMV dtls structure.
	setEmvReqCmd(EMV_C20);

	debug_sprintf(szDbgMsg, "%s: Building EMV [C20] command", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	getC_20Command(stEMVReq.pszBuf, &stEMVReq.iSize, pszEmvTag, iLen);

	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[C20]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sendD25CmdToEMVAgent
 *
 * Description	: This function will send the D25 command to EMV agent inorder
 * 				  to enable or disable
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendD25CmdToEMVAgent(EMV_OTHER_REC_PTYPE pstEmvOtherRec)
{
	int				rv				= SUCCESS;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	getD_25Command(stEMVReq.pszBuf, &stEMVReq.iSize, pstEmvOtherRec);

	//Store the request command type in the EMV dtls structure.
	setEmvReqCmd(EMV_D25);

	debug_sprintf(szDbgMsg, "%s: Building EMV [D25] command", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[D25]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showAccntTypeOptScrn
 *
 * Description	: This API would show account type option(CHQ/SAV) screen.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showAccntTypeOptScrn(char * szTranStkKey, int *piAccountType)
{
	int				rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;	
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);
	
	rv = buildEmvCmd(EMV_S13, &stEMVReq, szTranStkKey);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;

				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						rv = getUIRespCodeOfEmvCmdResp(EMV_S13, stBLData.iStatus);
						switch(rv)
						{
							case SUCCESS:
									//Storing Account type from the response data
									*piAccountType = stBLData.stRespDtls.stEmvDtls.iAccountType;
									
									debug_sprintf(szDbgMsg, "%s: S13 command returned [SUCCESS]", __FUNCTION__);
									APP_TRACE(szDbgMsg);				

									break;						

							default:
								/* 'D' command failed */
								debug_sprintf(szDbgMsg, "%s: S13 command returned Error[%d]",__FUNCTION__,  
																						stBLData.iStatus);
								APP_TRACE(szDbgMsg);			

								break;		
						}					
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;						
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				svcWait(100);
			}		
		}		
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[S13]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showBlankScreen
 *
 * Description	: This API would show blank screen. Mainly this API is used to clear off the XPI screen.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showBlankScreen(char * szTranStkKey)
{
	int				rv				= SUCCESS;
	//PAAS_BOOL		bWait			= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);
	
	rv = buildEmvCmd(EMV_S14, &stEMVReq, szTranStkKey);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			//For S14 there wont be any response.
			//svcWait(100); //Wait to complete the S14 command.
		}		
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[S14]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMACValue
 *
 * Description	: This API would get the MAC value for the MAC string sent in the request.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getMACValue(char * szTranStkKey, char *pszMACValue, char *pszDevSrlNum)
{
	int				rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;	
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);
	
	rv = buildEmvCmd(EMV_S66, &stEMVReq, szTranStkKey);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;

				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						rv = getUIRespCodeOfEmvCmdResp(EMV_S66, stBLData.iStatus);
						switch(rv)
						{
							case SUCCESS:
								debug_sprintf(szDbgMsg, "%s: Received the MAC Value",
																	__FUNCTION__);
								APP_TRACE(szDbgMsg);
								//Copying the EMV dtls from the response data
								memcpy(pszMACValue, &stBLData.stRespDtls.stEmvDtls.stEmvTerDtls.szMACValue,
										strlen(stBLData.stRespDtls.stEmvDtls.stEmvTerDtls.szMACValue));

								memcpy(pszDevSrlNum, &stBLData.stRespDtls.stEmvDtls.stEmvTerDtls.szDevSerialNum,
										strlen(stBLData.stRespDtls.stEmvDtls.stEmvTerDtls.szDevSerialNum));
								
								debug_sprintf(szDbgMsg, "%s: S66 command returned [SUCCESS]", __FUNCTION__);
								APP_TRACE(szDbgMsg);				

								break;						

							default:
								/* 'D' command failed */
								debug_sprintf(szDbgMsg, "%s: S66 command returned Error[%d]",__FUNCTION__, 
																						stBLData.iStatus);
								APP_TRACE(szDbgMsg);			

								break;		
						}					
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;						
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(100);
			}		
		}		
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[S66]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

#if 0
/*
 * ============================================================================
 * Function Name: getNxtEmvCmd
 *
 * Description	: This API would get next command to be executed based on current command
 *				  and table number.
 *
 * Input Params	: COmmand Info structure
 *
 * Output Params: Next EMV command
 * ============================================================================
 */
static int getNxtEmvCmd(EMV_CMD_INFO_PTYPE pstEmvCmdInfo)
{
	int					iNxtCmd			= SUCCESS;
	long				lnTableNo		= 0;
	int 				iCurCmd			= 0;

	/* Copy to local variables*/	
	lnTableNo = pstEmvCmdInfo->lnTableNo;
	iCurCmd	  = pstEmvCmdInfo->iCurCmd;

	if( (lnTableNo == 1) && (iCurCmd == EMV_D16) )
	{
		iNxtCmd = EMV_D12;
	}
	else if( (lnTableNo == 1) && (iCurCmd == EMV_D12) )
	{
		iNxtCmd = EMV_D15;
	}
	else if( (lnTableNo == 1) && (iCurCmd == EMV_D15) )
	{
		iNxtCmd = EMV_D13;
	}
	else if( (lnTableNo == 1) && (iCurCmd == EMV_D13) )
	{
		iNxtCmd = EMV_D11;
	}
	else if( (lnTableNo == 1) && (iCurCmd == EMV_D11) )
	{
		iNxtCmd = EMV_D16;
	}
	else if( (lnTableNo == 2) && (iCurCmd == EMV_D16) )
	{
		iNxtCmd = EMV_D14;
	}
	else if( (lnTableNo == 2) && (iCurCmd == EMV_D14) )
	{
		iNxtCmd = EMV_D16;
	}
	else if( (lnTableNo == 3) && (iCurCmd == EMV_D16) )
	{
		iNxtCmd = EMV_D17;
	}
	else if( (lnTableNo == 3) && (iCurCmd == EMV_D17) )
	{
		iNxtCmd = EMV_D16;
	}
	else if( (lnTableNo == 4) && (iCurCmd == EMV_D16) )
	{
		iNxtCmd = EMV_D19;
	}
	else if( (lnTableNo == 4) && (iCurCmd == EMV_D19) )
	{
		iNxtCmd = EMV_D20;
	}
	else if( (lnTableNo == 4) && (iCurCmd == EMV_D20) )
	{
		iNxtCmd = EMV_D18;
	}
	else if( (lnTableNo == 4) && (iCurCmd == EMV_D18) )
	{
		iNxtCmd = EMV_D25;
	}
	else
	{
		iNxtCmd = EMV_COMPLETE;
	}

	return iNxtCmd;
}
/*
 * ============================================================================
 * Function Name: loadD25Commands
 *
 * Description	: This API will load the D25 Command
 					from file to memory at startup
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int loadD25Commands()
{
	int				rv			= SUCCESS;
	int				iLen1		= 0;
	int				iLen2		= 0;
	char *			curr		= NULL;
	char *			next		= NULL;
	char			szLine[256]	= "";
	char			szD25EMV[256] = "";
	char			szD25MSD[256] = "";
	FILE*			fp			= NULL;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	while(1)
	{
		if(PAAS_FALSE == isEmvEnabledInDevice())
		{
			debug_sprintf(szDbgMsg, "%s: EMV is Disabled Returning SUCCESS", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS;
			break;
		}
		if(doesFileExist(D25_COMMAND_FILE_NAME) == SUCCESS)
		{
			fp = fopen(D25_COMMAND_FILE_NAME, "r+");

			if(fp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while opening the D25 File %s", __FUNCTION__, D25_COMMAND_FILE_NAME);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			while(!feof(fp))
			{
				memset(szLine, 0x00, sizeof(szLine));
				if(fgets(szLine, sizeof(szLine), fp) != NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Read Line [%s]", __FUNCTION__, szLine);
					APP_TRACE(szDbgMsg);

					if(strncmp(szLine, "//", 2) == 0 || strncmp(szLine, "/*", 2) == 0)
					{
						debug_sprintf(szDbgMsg, "%s: Its a comment line, ignoring it", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						continue; //goes to read next line
					}

					memset(szD25EMV, 0x00, strlen(szD25EMV));
					curr = szLine;
					next = strchr(curr, '|');
					if(next == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						return FAILURE; //Invalid format
					}
					iLen1 = next - curr;
					memcpy(szD25EMV, curr, iLen1);

					memset(szD25MSD, 0x00, strlen(szD25MSD));
					curr = next + 1;
					next = strchr(curr, '#');
					if(next == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Does not contain # in the last of line, invalid format!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						return FAILURE; //Invalid format
					}
					iLen2 = next - curr;
					memcpy(szD25MSD, curr, iLen2);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}

			if(fp != NULL)
			{
				fclose(fp);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: D25.txt NOT present, We will not send any D25 in b/w Trans", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS;
			break;
		}

		if(rv == SUCCESS)
		{
			rv = storeD25Commands(szD25EMV, szD25MSD);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: loadEmvTagsReqForHost
 *
 * Description	: This API will load the Required EMV Tags which will be send
 * 				  to Host in EMV_TAGS field
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int loadEmvTagsReqForHost()
{
	int				rv			= SUCCESS;
	char			szLine[512]	= "";
	FILE*			fp			= NULL;

#ifdef DEBUG
	char		szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	while(1)
	{
		/* Check if the EmvTags Req Host file (txt file) is present. */
		if(doesFileExist(EMV_TAGS_REQ_BY_HOST) == SUCCESS)
		{
			fp = fopen(EMV_TAGS_REQ_BY_HOST, "r");

			if(fp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while opening the EmvTags Host Req File %s", __FUNCTION__, EMV_TAGS_REQ_BY_HOST);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			while(!feof(fp))
			{
				memset(szLine, 0x00, sizeof(szLine));
				if(fgets(szLine, sizeof(szLine), fp) != NULL)
				{
					if(strlen(szLine) < 500)
					{
						debug_sprintf(szDbgMsg, "%s: Read Line [%s]", __FUNCTION__, szLine);
						APP_TRACE(szDbgMsg);
					}

					if(strncmp(szLine, "//", 2) == 0 || strncmp(szLine, "/*", 2) == 0)
					{
						debug_sprintf(szDbgMsg, "%s: Its a comment line, ignoring it", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						continue; //goes to read next line
					}

					setEMVTagsReqByHost(szLine);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			if(fp != NULL)
			{
				fclose(fp);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No information exists regarding EMV Tags Req By Host", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: createHashForEmvCTLSTags
 *
 * Description	: This API will create a Hash of EMV tags for CTLS, so that all
 * 				  tags will be sent to Host in the order specified in EmvtagsReqd
 * 				  file
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
extern int createHashForEmvCTLSTags()
{
	int     				rv 			  		= SUCCESS;
	int						iCount				= 0;
	int 					iLengthParsed 		= 0;
	int						iTotalTagsLeninHex	= 0;
	char 					szTag[7]     		= "";
	char					szEmvTags[512]		= "";
	char					tagsDefault[]		= "82959A9C5F245F2A5F349F029F039F099F1A9F1E9F269F279F339F349F359F369F379F399F419F53849F109F6E9F219F069F079F0D9F0E9F0F9F08";
	char					szEmvTagsinHex[256]	= "";
	char					*cTmp				= NULL;
	unsigned short 			tag;
	CTLS_TAG_NODE_STYPE 	stCTLSTagNode;

#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szEmvTags, 0x00, sizeof(szEmvTags));
	cTmp = getEMVTagsReqByHost();
	if(cTmp != NULL && strlen(cTmp) > 0)
	{
		strcpy(szEmvTags, cTmp);
		debug_sprintf(szDbgMsg, "%s: File Present with Tags[%s]", __FUNCTION__, szEmvTags);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		strcpy(szEmvTags, tagsDefault);
		debug_sprintf(szDbgMsg, "%s: File NOT Present Using Default[%s]", __FUNCTION__, szEmvTags);
		APP_TRACE(szDbgMsg);
	}

	memset(szEmvTagsinHex, 0x00, sizeof(szEmvTagsinHex));
	iTotalTagsLeninHex = Hex2Bin((unsigned char *)szEmvTags, strlen(szEmvTags)/2, (unsigned char *)szEmvTagsinHex);

	if(iTotalTagsLeninHex <= 0)
	{
		debug_sprintf(szDbgMsg, "%s: ERROR: File[EmvTagsReqbyHost.txt] Presents but contains No or Invalid Data", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	cTmp = szEmvTagsinHex;

	while(iLengthParsed < iTotalTagsLeninHex)
	{
		//Getting TAG
		tag = 0;
		tag = *cTmp++;
		iLengthParsed++;

		if( (tag & 0x1F) == 0x1F )// tag value is atleast 2 bytes
		{
			do {
				tag = tag << 8;
				tag = tag | *cTmp++;
				iLengthParsed++;
			} while((tag & 0x80) == 0x80);
		}

		//Convert tag(Hex value) to ASCII string[for e.g., "0" is stores as "30" on szTag]
		memset(szTag, 0x00, sizeof(szTag));
		sprintf(szTag, "%X", tag);

		memset(&stCTLSTagNode, 0x00, sizeof(CTLS_TAG_NODE_STYPE));
		strcpy(stCTLSTagNode.szTag, szTag);
		stCTLSTagNode.index = iCount++;
		addCTLSTagtoHashTable(stCTLSTagNode);

		debug_sprintf(szDbgMsg, "%s:Added Tag [%s] with index[%d]", __FUNCTION__, szTag, iCount - 1);
		APP_TRACE(szDbgMsg);
	}

#ifdef DEVDEBUG
	for(tag=0; tag<HASHSIZE; tag++)
	{
		if(hashtab_ctls_tags[tag] != NULL)
		{
			debug_sprintf(szDbgMsg, "%s:Tag [%s] with index[%d] at %d", __FUNCTION__, hashtab_ctls_tags[tag]->szTag,hashtab_ctls_tags[tag]->index, tag);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s:No Value at %d", __FUNCTION__, tag);
			APP_TRACE(szDbgMsg);
		}
	}
#endif
	//debug_sprintf(szDbgMsg,"%s: Returning rv =%d", __FUNCTION__, rv);
	//APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeTagsForEMVCTLS
 *
 * Description	: This API will store EMV tags for EMVCTLS in string of array in a
 * 					sequential pattern
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
extern int storeTagsForEMVCTLS(char *pszTagValue, char *pszTag, char szEmvTagsForCTLS[64][128])
{
	int     				rv 			  		= SUCCESS;
	CTLS_TAG_NODE_PTYPE 	pstCTLSTagNode;

#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	if(pszTag == NULL || pszTagValue == NULL || szEmvTagsForCTLS == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	pstCTLSTagNode = lookupHashTableforCTLSTag(pszTag);
#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg,"%s: Got [%s] with [%s]", __FUNCTION__, pszTag, pszTagValue);
	APP_TRACE(szDbgMsg);
#endif
	if(pstCTLSTagNode != NULL)
	{
		strcpy(szEmvTagsForCTLS[pstCTLSTagNode->index], pszTagValue);
#if DEVDEBUG
		debug_sprintf(szDbgMsg,"%s: Added [%s] at [%d]", __FUNCTION__, szEmvTagsForCTLS[pstCTLSTagNode->index], pstCTLSTagNode->index);
		APP_TRACE(szDbgMsg);
#endif
	}

	//debug_sprintf(szDbgMsg,"%s: Returning rv =%d", __FUNCTION__, rv);
	//APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadAIDListAndConfig
 *
 * Description	: This API will load the all AID's payment type, pymtmedia,
 * 				  pymntt type etc from AIDList.txt or AIDList.ini, whichever
 * 				  is present. AIDList.txt id preferred and converted to
 * 				  AIDList.ini also
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int loadAIDListAndConfig(char * pszErrMsg)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char 			szCmd[100] 			= "";
	char			szAppLogData[300]	= "";
	char			szAllAIDs[100][33];

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	/*AjayS2: 04 Nov 2016: FRD 4.05 and AIDList Format Conversion:
	 * SCA will use AIDList.ini file from now onwards. For backward compatibility, SCA will check the presence of
	 * AIDList.txt. If AIDList.txt is present, txt file will we loaded and converted to new ini format.
	 * If none of the file(ini or txt) is present, Error will be thrown.
	 * If both ini and txt files are present, Preference will be given to txt file.
	 */
	while(1)
	{
		if(doesFileExist(AID_LIST_FILE_TXT) == SUCCESS)
		{
			//AIDLis.txt present, Will be converted to AIDList.ini and txt file will be deleted
			debug_sprintf(szDbgMsg, "%s: [%s] Present; Loading and converting to [%s]", __FUNCTION__, AID_LIST_FILE_TXT, AID_LIST_FILE_INI);
			APP_TRACE(szDbgMsg);
			memset(szAllAIDs, 0x00, sizeof(szAllAIDs));

			//Load AIDList.txt file in Hashtable
			rv = loadAIDListTxtFile(szAllAIDs, pszErrMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while Loading AID Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			//Create AIDList.ini from hashtable created in last step
			rv = createAIDListIniFile(szAllAIDs, pszErrMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while Creating file [%s] ", __FUNCTION__, AID_LIST_FILE_INI);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			//remove AIDList.txt file after creating ini file
			if(remove(AID_LIST_FILE_TXT) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Removal of File [%s] Failed ", __FUNCTION__, AID_LIST_FILE_TXT);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Error While Removing file [%s], After conversion to INI format", AID_LIST_FILE_TXT);
					addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
				}
			}
		}
		else if(doesFileExist(AID_LIST_FILE_INI) == SUCCESS)
		{
			//AIDList.txt NOT present, AIDList.ini is Present, Loading AIDList.ini to hashtable
			debug_sprintf(szDbgMsg, "%s: [%s] Present; Loading AIDs", __FUNCTION__, AID_LIST_FILE_INI);
			APP_TRACE(szDbgMsg);
			rv = loadAIDListINIFile(pszErrMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while Parsing [%s] ", __FUNCTION__, AID_LIST_FILE_INI);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Error in Parsing [%s]: Please Correct the file ", AID_LIST_FILE_INI);
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
				}
				break;
			}
		}
		else if(doesFileExist(AID_LIST_FILE_INI_CAPS) == SUCCESS)
		{
			//AIDList.ini NOT present, AIDList.INI is Present, Renaming to AIDList.ini and Reading
			debug_sprintf(szDbgMsg, "%s: [%s] Present; Renaming to [%s] and Loading AIDs", __FUNCTION__, AID_LIST_FILE_INI_CAPS, AID_LIST_FILE_INI);
			APP_TRACE(szDbgMsg);

			if(rename(AID_LIST_FILE_INI_CAPS, AID_LIST_FILE_INI) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Renaming of File [%s] Failed ", __FUNCTION__, AID_LIST_FILE_INI_CAPS);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Error While Renaming file [%s] to [%s]", AID_LIST_FILE_INI_CAPS, AID_LIST_FILE_INI);
					addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
				}
			}
			rv = loadAIDListINIFile(pszErrMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while Parsing [%s] ", __FUNCTION__, AID_LIST_FILE_INI);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Error in Parsing [%s]: Please Correct the file ", AID_LIST_FILE_INI);
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
				}
				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: ERROR: No file(s)/information exists regarding AIDs List", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "AIDList File(s) Missing: Please Contact Admin");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrMsg, MSG_FILE_NOT_PRESENT);
			sprintf(pszErrMsg, "%s: %s or %s", pszErrMsg, AID_LIST_FILE_TXT, AID_LIST_FILE_INI);
		}
		break;
	}

	/* Making a Temp copy of EMVTables and CTLSConfig file in order to read Term Dtls and reading TermDtls from Config to AIDList
	 */
	if(rv == SUCCESS)
	{
		if(doesFileExist(XPI_EMV_TABLES_PERM_PATH) == SUCCESS)
		{
			memset(szCmd, 0x00, sizeof(szCmd));
			sprintf(szCmd, "%s %s %s", "cp", XPI_EMV_TABLES_PERM_PATH, XPI_EMV_TABLES_TEMP_PATH);
			if(local_svcSystem(szCmd) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to execute command [%s]",__FUNCTION__, szCmd);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Copy the XPI EMVTable Config File");
					addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
				}
			}
			else
			{
				if(updateTermDtlsFromINItoAIDList(PAAS_FALSE) != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to Update TermDetails from EMVTables to AIDList", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Failed to Update TermDetails From EMVTables to AID List");
						addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
					}
				}
				memset(szCmd, 0x00, sizeof(szCmd));
				sprintf(szCmd, "%s %s", "rm", XPI_EMV_TABLES_TEMP_PATH);
				if(local_svcSystem(szCmd) != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to execute command [%s]",__FUNCTION__, szCmd);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Failed to Remove the Temporary EMV Config File");
						addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
					}
				}
			}
		}
		else
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "EMVTable Config File Not found in XPI Location");
				addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
			}
		}

		if(isContactlessEmvEnabledInDevice())
		{
			if(doesFileExist(XPI_CTLS_CONFIG_PERM_PATH) == SUCCESS)
			{
				memset(szCmd, 0x00, sizeof(szCmd));
				sprintf(szCmd, "%s %s %s", "cp", XPI_CTLS_CONFIG_PERM_PATH, XPI_CTLS_CONFIG_TEMP_PATH);
				if(local_svcSystem(szCmd) != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to execute command [%s]",__FUNCTION__, szCmd);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Failed to Copy the XPI CTLSConfig File");
						addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
					}
				}
				else
				{
					if(updateTermDtlsFromINItoAIDList(PAAS_TRUE) != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to Update TermDetails from CTLSConfig to AIDList", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "Failed to Update TermDetails From CTLSConfig to AID List");
							addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
						}
					}
					memset(szCmd, 0x00, sizeof(szCmd));
					sprintf(szCmd, "%s %s", "rm", XPI_CTLS_CONFIG_TEMP_PATH);
					if(local_svcSystem(szCmd) != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to execute command [%s]",__FUNCTION__, szCmd);
						APP_TRACE(szDbgMsg);
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "Failed to Remove the Temporary CTLS Config File");
							addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
						}
					}
				}
			}
			else
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "CTLSConfig File Not found in XPI Location");
					addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
				}
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: createAIDListIniFile
 *
 * Description	: This function will create AIDList.ini file from
 * 					already created AIDList Nodes
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int createAIDListIniFile(char szAllAIDs[100][33], char *pszErrMsg)
{
	int					rv					= SUCCESS;
	int 				iCnt				= 0;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[300]	= "";
	char				szKey[100]			= "";
	char				szTmp[256]			= "";
	dictionary *		dict				= NULL;
	FILE	*			fp					= NULL;
	AID_NODE_PTYPE		pstAIDNode;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		//Creating a new AIDList.ini File
		fp = fopen(AID_LIST_FILE_INI, "w");
		if(fp == NULL)
		{
			/* Unable to Create new file */
			debug_sprintf(szDbgMsg, "%s: Unable to Create New AIDList INI File", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
			sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_INI);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Unable to Create AIDList INI File");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
			break;
		}
		fclose(fp);
		fp = NULL;
		dict = iniparser_load(AID_LIST_FILE_INI);
		if(dict == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to load ini file [%s]",	__FUNCTION__, AID_LIST_FILE_INI);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Unable to Load AIDList INI File");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
			sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_INI);
			rv = FAILURE;
			break;
		}

		iCnt = 0;
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s", "SupportedAIDs");
		iniparser_setstr(dict, szKey, NULL);

		while(strlen(szAllAIDs[iCnt]) > 0)
		{
			memset(szTmp, 0x00, sizeof(szTmp));
			strncpy(szTmp, szAllAIDs[iCnt], 10);
			pstAIDNode = lookupHashTable(szTmp, szAllAIDs[iCnt]);
			if(pstAIDNode != NULL)
			{
				iCnt++;
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s%d", "SupportedAIDs", "AID", iCnt);
				memset(szTmp, 0x00, sizeof(szTmp));
				sprintf(szTmp, "%s", pstAIDNode->szAID);
				iniparser_setstr(dict, szKey, szTmp);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s%s", pstAIDNode->szAID, ".Config");
				iniparser_setstr(dict, szKey, szTmp);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s%s:%s", pstAIDNode->szAID, ".Config", "PaymentType");
				memset(szTmp, 0x00, sizeof(szTmp));
				sprintf(szTmp, "%d", pstAIDNode->iPymtType);
				iniparser_setstr(dict, szKey, szTmp);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s%s:%s", pstAIDNode->szAID, ".Config", "PaymentMedia");
				memset(szTmp, 0x00, sizeof(szTmp));
				sprintf(szTmp, "%s", pstAIDNode->szPymtMedia);
				iniparser_setstr(dict, szKey, szTmp);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s%s:%s", pstAIDNode->szAID, ".Config", "CashbackEnabled");
				memset(szTmp, 0x00, sizeof(szTmp));
				sprintf(szTmp, "%d", pstAIDNode->iCashbackEnabled);
				iniparser_setstr(dict, szKey, szTmp);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s%s:%s", pstAIDNode->szAID, ".Config", "CardAbbrv");
				memset(szTmp, 0x00, sizeof(szTmp));
				sprintf(szTmp, "%s", pstAIDNode->szCardAbbrv);
				iniparser_setstr(dict, szKey, szTmp);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s%s:%s", pstAIDNode->szAID, ".Config", "signaturelimit");
				memset(szTmp, 0x00, sizeof(szTmp));
				sprintf(szTmp, "%.2lf", pstAIDNode->fSigLimit);
				iniparser_setstr(dict, szKey, szTmp);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s%s:%s", pstAIDNode->szAID, ".Config", "capsignonpinbypass");
				memset(szTmp, 0x00, sizeof(szTmp));
				sprintf(szTmp, "%d", pstAIDNode->iCapSignOnPINBypass);
				iniparser_setstr(dict, szKey, szTmp);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Could not found AID [%s], Checking next AID",	__FUNCTION__, szTmp);
				APP_TRACE(szDbgMsg);
				iCnt++;
			}
		}
		break;
	}
	if(dict != NULL)
	{
		fp = fopen(AID_LIST_FILE_INI, "w");
		if(fp == NULL)
		{
			rv = FAILURE;
		}
		else
		{
			iniparser_dump_ini(dict, fp);
			fclose(fp);
		}
		iniparser_freedict(dict);
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: loadAIDListTxtFile
 *
 * Description	: This API will load the all AID's payment type and pymtmedia
 					from AID List Text file to hash at startup
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int loadAIDListTxtFile(char szAllAids[100][33], char * pszErrMsg)
{
	int				rv					= SUCCESS;
	int				iCnt				= 0;
	char			szLine[256]			= "";
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	char			szTmp[4]			= "";
	char*			curr				= NULL;
	char*			next				= NULL;
	FILE*			fp					= NULL;
	AID_NODE_STYPE	stAIDNode;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	while(1)
	{
		//Ajays2: 10 My 2016: Making Code AID List file fault tolerant, So any empty lines also will be ignored now.
		rv = checkAndCorrectFileFormat(AID_LIST_FILE_TXT); // T_RaghavendranR1 CID 83367 (#1 of 1): Unchecked return value (CHECKED_RETURN)
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to improve Format %s ", __FUNCTION__, AID_LIST_FILE_TXT);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Loading AIDList.txt File");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
			sprintf(pszErrMsg, "%s: %s",pszErrMsg, AID_LIST_FILE_TXT);
			rv = FAILURE;
			break;
		}

		fp = fopen(AID_LIST_FILE_TXT, "r+");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while opening the AIDList File %s", __FUNCTION__, AID_LIST_FILE_TXT);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Opening AIDList.txt File");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
			sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_TXT);
			break;
		}

		while(!feof(fp))
		{
			memset(szLine, 0x00, sizeof(szLine));
			//Read line in format
			//AID Payment_type PaymentMedia#
			if(fgets(szLine, sizeof(szLine), fp) != NULL)
			{
				/*					debug_sprintf(szDbgMsg, "%s: Read Line [%s]", __FUNCTION__, szLine);
					APP_TRACE(szDbgMsg);*/

				if(strncmp(szLine, "//", 2) == 0 || strncmp(szLine, "/*", 2) == 0 || strncmp(szLine, "*/", 2) == 0)
				{
					debug_sprintf(szDbgMsg, "%s: Its a comment line, ignoring it", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue; //goes to read next line
				}

				memset(&stAIDNode, 0x00, sizeof(AID_NODE_STYPE));
				curr = szLine;
				next = strchr(curr, '|');
				if(next == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					// CID-67414: 25-Jan-16: MukeshS3: Need to close this AID file, before returning.
					if(fp != NULL)
					{
						fclose(fp);
					}
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Error while Reading AIDList.txt File");
						addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
					}
					getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
					sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_TXT);
					return FAILURE; //Invalid format
				}
				memcpy(stAIDNode.szAID, curr, next - curr);
				memcpy(szAllAids[iCnt++], curr, next - curr);

				curr = next + 1;
				next = strchr(curr, '|');
				if(next == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Does not contain 2nd | in the line, invalid format!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					// CID-67414: 25-Jan-16: MukeshS3: Need to close this AID file, before returning.
					if(fp != NULL)
					{
						fclose(fp);
					}
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Error while Reading AIDList.txt File");
						addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
					}
					getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
					sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_TXT);
					return FAILURE; //Invalid format
				}
				memset(szTmp, 0x00, 4);
				memcpy(szTmp, curr, next - curr);
				if(strlen(szTmp) > 0)
				{
					stAIDNode.iPymtType = atoi(szTmp);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Does not contain Payment Type Field, will be taken as 0(Credit)", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

				curr = next + 1;
				next = strchr(curr, '|');
				if(next == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					// CID-67414: 25-Jan-16: MukeshS3: Need to close this AID file, before returning.
					if(fp != NULL)
					{
						fclose(fp);
					}
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Error while Reading AIDList.txt File");
						addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
					}
					getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
					sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_TXT);
					return FAILURE; //Invalid format
				}
				memcpy(stAIDNode.szPymtMedia, curr, next - curr);

				/*AjayS2: 07 Nov 2016: From now onwards, we will not be using AIDList.txt file and also there will be No TACs in newly formatted AIDList.INI file also,
				 * Below code is just for backward compatibilty. If old AIDList.txt is present and TACs are present in that file, we will not be using
				 * any TACs from AIDList.txt. We are going to read EMVTables.ini and CTLSConfig.ini for emv contact and contact resp.
				 */
				curr = next + 1;
				next = strchr(curr, '|');
				if(next == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					// CID-67414: 25-Jan-16: MukeshS3: Need to close this AID file, before returning.
					if(fp != NULL)
					{
						fclose(fp);
					}
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Error while Reading AIDList.txt File");
						addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
					}
					getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
					sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_TXT);
					return FAILURE; //Invalid format
				}
				memcpy(stAIDNode.szTACDefault, curr, next - curr);

				curr = next + 1;
				next = strchr(curr, '|');
				if(next == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					// CID-67414: 25-Jan-16: MukeshS3: Need to close this AID file, before returning.
					if(fp != NULL)
					{
						fclose(fp);
					}
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Error while Reading AIDList.txt File");
						addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
					}
					getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
					sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_TXT);
					return FAILURE; //Invalid format
				}
				memcpy(stAIDNode.szTACDenial, curr, next - curr);

				curr = next + 1;
				next = strchr(curr, '|');
				if(next == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, Must be older AIDList!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					next = strchr(curr, '#');
					if(next == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Does not contain # in the last of line, invalid format!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						if(fp != NULL) // T_RaghavendranR1 CID 67414 (#3 of 3): Resource leak (RESOURCE_LEAK)
						{
							fclose(fp);
						}
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "Error while Reading AIDList.txt File");
							addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
						}
						getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
						sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_TXT);
						return FAILURE; //Invalid format
					}
					memcpy(stAIDNode.szTACOnline, curr, next - curr);
					stAIDNode.iCashbackEnabled = 1;
					/* Daivik:31-5-2016-Setting the Cashback enabled as true by default. Based on the cashback capability on the terminal
					 * and the capability of the card the final decision would be taken to prompt cashback.
					 */
					debug_sprintf(szDbgMsg, "%s: Enabling the Cashback by default for this AID because of missing value", __FUNCTION__);
					APP_TRACE(szDbgMsg);

				}
				else
				{
					memcpy(stAIDNode.szTACOnline, curr, next - curr);
					curr = next + 1;
					if(curr != NULL)
					{
						next = strchr(curr, '|');
						if(next == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, Must be older AIDList, where Cashback is present and Card Abbrv is not Present!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//by default we will consider the value to be 1
							stAIDNode.iCashbackEnabled = 1;
							next = strchr(curr, '#');
							if(next == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain # in the last of line, invalid format!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								if(fp != NULL) // T_RaghavendranR1 CID 67414 (#3 of 3): Resource leak (RESOURCE_LEAK)
								{
									fclose(fp);
								}
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "Error while Reading AIDList.txt File");
									addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
								}
								getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
								sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_TXT);
								return FAILURE; //Invalid format
							}
							memset(szTmp, 0x00, 4);
							if((next - curr) == 1)
							{
								//If they are setting a value like 0abcd , then we should consider this as a invalid value and set default value of 1
								//We expect the cashback indicator in the AIDList file to be a 1 charachter value
								szTmp[0] = curr[0];
							}
							if(szTmp[0] == '0')
							{
								stAIDNode.iCashbackEnabled = 0;
							}
							else if(szTmp[0] == '1')
							{
								stAIDNode.iCashbackEnabled = 1;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Will use default value of cashback for this AID", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}
						}
						else
						{
							//by default we will consider the value to be 1
							stAIDNode.iCashbackEnabled = 1;
							memset(szTmp, 0x00, 4);
							if((next - curr) == 1)
							{
								//If they are setting a value like 0abcd , then we should consider this as a invalid value and set default value of 1
								//We expect the cashback indicator in the AIDList file to be a 1 charachter value
								szTmp[0] = curr[0];
							}
							if(szTmp[0] == '0')
							{
								stAIDNode.iCashbackEnabled = 0;
							}
							else if(szTmp[0] == '1')
							{
								stAIDNode.iCashbackEnabled = 1;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Will use default value of cashback for this AID", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}

							curr = next + 1;
							if(curr != NULL)
							{
								next = strchr(curr, '#');
								if(next == NULL)
								{
									debug_sprintf(szDbgMsg, "%s: Does not contain # in the last of line, invalid format!!!", __FUNCTION__);
									APP_TRACE(szDbgMsg);
									if(fp != NULL) // T_RaghavendranR1 CID 67414 (#3 of 3): Resource leak (RESOURCE_LEAK)
									{
										fclose(fp);
									}
									if(iAppLogEnabled == 1)
									{
										strcpy(szAppLogData, "Error while Reading AIDList.txt File");
										addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
									}
									getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
									sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_TXT);
									return FAILURE; //Invalid format
								}
								if((next - curr) <= 2) //Card Abbrv must be 2 chars, and not more than 2 chars
								{
									memcpy(stAIDNode.szCardAbbrv, curr, next - curr);
								}
							}
						}
					}
				}
				//AjayS2: Adding Default Values for Signature limit and cappinonsigbypass to AID Nodes, so that it will be loaded in AIDList.ini file also
				stAIDNode.fSigLimit = 0.00;
				stAIDNode.iCapSignOnPINBypass = 0;

				strncpy(stAIDNode.szRID, stAIDNode.szAID, 10);
				/* Daivik:16/9/2016 - Default value of TermCap set to include PIN CVM( Maintains backward compatibility)
				 * so that we will prompt for cashback if Default TermpCap could not be set due to some reason.
				 */
				strncpy(stAIDNode.szTermCapDefault,"E0F8C8",sizeof(stAIDNode.szTermCapDefault)-1);

				debug_sprintf(szDbgMsg, "%s: Read AID is %s pymttype: %d pymtmedia %s szRID %s cashback:%d CardAbbrv:%s",
						__FUNCTION__, stAIDNode.szAID, stAIDNode.iPymtType, stAIDNode.szPymtMedia, stAIDNode.szRID,stAIDNode.iCashbackEnabled, stAIDNode.szCardAbbrv);
				APP_TRACE(szDbgMsg);

				/* Daivik : 4/2/2016 - Coverity 67950 - Large value passed, instead passing by reference */
				rv = addFieldstoHashTable(&stAIDNode);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to add Fields to Hash Table", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Error While Adding Nodes to AIDList HashTable");
						addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
					}
					getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
					sprintf(pszErrMsg, "%s: %s", pszErrMsg, "Error While Creating AIDList Nodes");
					break;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error while Reading AIDList.txt File");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
				}
				getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
				sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_TXT);
				break;
			}
		}
		break;
	}

	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadAIDListINIFile
 *
 * Description	: This API will load the all AID's payment type and pymtmedia
 					from AID List INI file to hash at startup
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int loadAIDListINIFile(char * pszErrMsg)
{
	int				rv					= SUCCESS;
	int				iCnt				= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	char			szKey[100]			= "";
	char *			value				= NULL;
	dictionary *	dictAID				= NULL;
	AID_NODE_STYPE	stAIDNode;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	while(1)
	{
		rv = checkAndCorrectFileFormat(AID_LIST_FILE_INI);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to improve Format %s ", __FUNCTION__, AID_LIST_FILE_INI);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Loading AIDList.ini file");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
			sprintf(pszErrMsg, "%s: %s",pszErrMsg, AID_LIST_FILE_INI);
			rv = FAILURE;
			break;
		}

		dictAID = iniparser_load(AID_LIST_FILE_INI);
		if(dictAID == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to load ini file [%s]",	__FUNCTION__, AID_LIST_FILE_INI);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Loading AIDList.ini file");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
			sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_INI);
			break;
		}

		iCnt = 0;
		while(1)
		{
			iCnt++;
			memset(&stAIDNode, 0x00, sizeof(AID_NODE_STYPE));
			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s:%s%d", "SupportedAIDs", "AID", iCnt);
			value = iniparser_getstring(dictAID, szKey, NULL);

			if(value == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: AID%d onwards Not Present, Finished Parsing All AIDs present in AIDList File",	__FUNCTION__, iCnt);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Parsed All AIDs present in AIDList File");
					addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
				}
				break;
			}
			strcpy(stAIDNode.szAID, value);

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s.%s:%s", stAIDNode.szAID, "Config", "PaymentType");
			value = iniparser_getstring(dictAID, szKey, NULL);
			if(value == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: AID [%s] Present, But Payment type Missing, Will be Taken as 0(Credit)",	__FUNCTION__, stAIDNode.szAID);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Error while Loading EMV Config Files:AID [%s] Present, But Payment type Missing", stAIDNode.szAID);
					addAppEventLog(SCA, PAAS_ERROR, PROCESSING, szAppLogData, NULL);
				}
			}
			else
			{
				stAIDNode.iPymtType = atoi(value);
			}

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s.%s:%s", stAIDNode.szAID, "Config", "PaymentMedia");
			value = iniparser_getstring(dictAID, szKey, NULL);
			if(value == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: AID [%s] Present, But Payment Media Missing",	__FUNCTION__, stAIDNode.szAID);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Error while Loading EMV Config Files:AID [%s] Present, But Payment Media Missing", stAIDNode.szAID);
					addAppEventLog(SCA, PAAS_ERROR, PROCESSING, szAppLogData, NULL);
				}
			}
			else
			{
				strcpy(stAIDNode.szPymtMedia, value);
			}

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s.%s:%s", stAIDNode.szAID, "Config", "CashbackEnabled");
			value = iniparser_getstring(dictAID, szKey, NULL);
			stAIDNode.iCashbackEnabled = 1;
			if(value == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: AID [%s] Present, But CashbackEnabled Missing; Taking as 1(Enabled)", __FUNCTION__, stAIDNode.szAID);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "AID [%s] Present, But CashbackEnabled Missing; Taking as 1(Enabled)", stAIDNode.szAID);
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
			}
			else if(strlen(value) == 1 && *value == '0')
			{
				stAIDNode.iCashbackEnabled = 0;
			}

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s.%s:%s", stAIDNode.szAID, "Config", "CardAbbrv");
			value = iniparser_getstring(dictAID, szKey, NULL);
			if(value == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: AID [%s] Present, But CardAbbrv Missing",	__FUNCTION__, stAIDNode.szAID);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Error while Loading EMV Config Files:AID [%s] Present, But CardAbbrv Missing", stAIDNode.szAID);
					addAppEventLog(SCA, PAAS_ERROR, PROCESSING, szAppLogData, NULL);
				}
			}
			else
			{
				strcpy(stAIDNode.szCardAbbrv, value);
			}

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s.%s:%s", stAIDNode.szAID, "Config", "Signaturelimit");
			value = iniparser_getstring(dictAID, szKey, NULL);
			if(value == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: AID [%s] Present, But Signaturelimit Missing", __FUNCTION__, stAIDNode.szAID);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "AID [%s] Present, But Signaturelimit Missing", stAIDNode.szAID);
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
			}
			else
			{
				stAIDNode.fSigLimit = atof(value);
			}

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s.%s:%s", stAIDNode.szAID, "Config", "Capsignonpinbypass");
			value = iniparser_getstring(dictAID, szKey, NULL);
			stAIDNode.iCapSignOnPINBypass = 0;
			if(value == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: AID [%s] Present, But Capsignonpinbypass Missing", __FUNCTION__, stAIDNode.szAID);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "AID [%s] Present, But Capsignonpinbypass Missing", stAIDNode.szAID);
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
			}
			else if(strlen(value) == 1 && *value == '1')
			{
				stAIDNode.iCapSignOnPINBypass = 1;
			}

			strncpy(stAIDNode.szRID, stAIDNode.szAID, 10);
			/* Daivik:16/9/2016 - Default value of TermCap set to include PIN CVM( Maintains backward compatibility)
			 * so that we will prompt for cashback if Default TermpCap could not be set due to some reason.
			 */
			strncpy(stAIDNode.szTermCapDefault,"E0F8C8",sizeof(stAIDNode.szTermCapDefault)-1);

			debug_sprintf(szDbgMsg, "%s: Read AID is %s pymttype: %d pymtmedia %s szRID %s cashback:%d CardAbbrv:%s siglimit:%lf capsigonpinbypass:%d",
					__FUNCTION__, stAIDNode.szAID, stAIDNode.iPymtType, stAIDNode.szPymtMedia, stAIDNode.szRID,stAIDNode.iCashbackEnabled, stAIDNode.szCardAbbrv, stAIDNode.fSigLimit, stAIDNode.iCapSignOnPINBypass);
			APP_TRACE(szDbgMsg);

			/* Daivik : 4/2/2016 - Coverity 67950 - Large value passed, instead passing by reference */
			rv = addFieldstoHashTable(&stAIDNode);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to add Fields to Hash Table", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error While Adding Nodes to AIDList HashTable");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
				}
				getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
				sprintf(pszErrMsg, "%s: %s", pszErrMsg, "Error While Creating AIDList Nodes");
				break;
			}
		}
		break;
	}

	if(dictAID != NULL)
	{
		iniparser_freedict(dictAID);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: updateEmvCTLSTermDtlsFromCTLSConfig
 *
 * Description	: This function will update EMV CTLS TermDtls from CTLS Config file
 * 					to AID List Node
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static int updateEmvCTLSTermDtlsFromCTLSConfig(AID_NODE_PTYPE pstAIDNode, dictionary *dict)
{
	int					rv					= SUCCESS;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[300]	= "";
	char *				value				= NULL;
	char				szTACDefault[11]	= "";
	char				szTACDenial[11]		= "";
	char				szTACOnline[11]		= "";
	char				szKey[100]			= "";

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstAIDNode == NULL || dict == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	while(1)
	{
		memset(szTACDefault, 0x00, sizeof(szTACDefault));
		memset(szTACDenial, 0x00, sizeof(szTACDenial));
		memset(szTACOnline, 0x00, sizeof(szTACOnline));

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s.%s:%s", pstAIDNode->szPymtMedia, "Config", "TACDefault");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			memcpy(szTACDefault, value, 10);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No value found for key %s", __FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Error While Reading TACDefault from CTLS Config for %s", pstAIDNode->szPymtMedia);
				addAppEventLog(SCA, PAAS_ERROR, PROCESSING, szAppLogData, NULL);
			}
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s.%s:%s", pstAIDNode->szPymtMedia, "Config", "TACDenial");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			memcpy(szTACDenial, value, 10);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No value found for key %s", __FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Error While Reading TACDenial from CTLS Config for %s", pstAIDNode->szPymtMedia);
				addAppEventLog(SCA, PAAS_ERROR, PROCESSING, szAppLogData, NULL);
			}
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s.%s:%s", pstAIDNode->szPymtMedia, "Config", "TACOnline");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			memcpy(szTACOnline, value, 10);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No value found for key %s", __FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Error While Reading TACOnline from CTLS Config for %s", pstAIDNode->szPymtMedia);
				addAppEventLog(SCA, PAAS_ERROR, PROCESSING, szAppLogData, NULL);
			}
		}
		debug_sprintf(szDbgMsg, "%s: szAID: %s Updating For CTLS TACDef: %s TACDen: %s TACOnl: %s",
				__FUNCTION__, pstAIDNode->szPymtMedia, szTACDefault, szTACDenial,szTACOnline);
		APP_TRACE(szDbgMsg);

		strcpy(pstAIDNode->szTACDefaultCTLS, szTACDefault);
		strcpy(pstAIDNode->szTACDenialCTLS, szTACDenial);
		strcpy(pstAIDNode->szTACOnlineCTLS, szTACOnline);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: updateTermDtlsFromINItoAIDList
 *
 * Description	: This function will update TACs  and Default TermCap from
 *				  EMVTables.ini/CTLSConfig to the AID nodes.
 *
 * Input Params	: If bForEmvCtls is TRUE, read from CTLSConfig file
   				  If bForEmvCtls is FALSE, read from EMVTables file
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateTermDtlsFromINItoAIDList(PAAS_BOOL bForEmvCtls)
{
	extern	int			errno;
	int					rv					= SUCCESS;
	int					iScheme				= 0;
	int					iAids				= 0;
	int					iUpdatedAid			= 0;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[300]	= "";
	char *				sceheme				= NULL;
	char *				suppAid				= NULL;
	char *				termCap				= NULL;
	char *				tacdefault			= NULL;
	char *				tacdenial			= NULL;
	char *				taconline			= NULL;
	char				szKey[100]			= "";
	dictionary *		dict				= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	if(bForEmvCtls)
	{
		if(SUCCESS != doesFileExist(XPI_CTLS_CONFIG_TEMP_PATH))
		{
			debug_sprintf(szDbgMsg, "%s: %s File not present, ERROR", __FUNCTION__, XPI_CTLS_CONFIG_TEMP_PATH);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while trying to parse CTLS Config");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
			return rv;
		}
		rv = checkAndCorrectFileFormat(XPI_CTLS_CONFIG_TEMP_PATH);
		if(rv !=SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to improve Format %s ", __FUNCTION__, XPI_CTLS_CONFIG_TEMP_PATH);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Correcting Format of CTLS Config");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
			return rv;
		}
		dict = iniparser_load(XPI_CTLS_CONFIG_TEMP_PATH);
		if(dict == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: iniparser_load error; file [%s] could not be opened!, errno=%d [%s]", __FUNCTION__, XPI_CTLS_CONFIG_TEMP_PATH, errno, strerror(errno));
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error in Parsing CTLS Config");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
			return rv;
		}
	}
	else
	{
		if(SUCCESS != doesFileExist(XPI_EMV_TABLES_TEMP_PATH))
		{
			debug_sprintf(szDbgMsg, "%s: %s File not present, ERROR", __FUNCTION__, XPI_EMV_TABLES_TEMP_PATH);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while trying to parse EMVTable Config");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
			return rv;
		}
		rv = checkAndCorrectFileFormat(XPI_EMV_TABLES_TEMP_PATH);
		if(rv !=SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to improve Format %s ", __FUNCTION__, XPI_EMV_TABLES_TEMP_PATH);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Correcting Format of EMVTable Config");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
			return rv;
		}
		dict = iniparser_load(XPI_EMV_TABLES_TEMP_PATH);
		if(dict == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: iniparser_load error; file [%s] could not be opened!, errno=%d [%s]", __FUNCTION__, XPI_EMV_TABLES_TEMP_PATH, errno, strerror(errno));
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error in Parsing EMVTable Config");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
			return rv;
		}
	}

	while(1)
	{
		// First Get the different supported schemes
		iAids=0;
		iScheme++;
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:Scheme%d","SupportedSchemes",iScheme);
		sceheme = iniparser_getstring(dict, szKey, NULL);
		if(sceheme == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No More Schemes, Final Num of schemes:%d", __FUNCTION__,iScheme-1);
			APP_TRACE(szDbgMsg);
			break;
		}
		if(bForEmvCtls == PAAS_FALSE)
		{
			//Get TermCap for this Scheme Only for EMV Contact
			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s.Config:TermCapabilities",sceheme);
			termCap = iniparser_getstring(dict, szKey, NULL);
			if(termCap == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: TermCap Missing for this scheme :%s, check the next scheme", __FUNCTION__,sceheme);
				APP_TRACE(szDbgMsg);
				continue;
			}
			//TermCap must be 6 chars. If it is not , then we have an incorrect value
			if(strlen(termCap) != 6)
			{
				debug_sprintf(szDbgMsg, "%s: TermCap has incorrect value :%s for scheme:%s", __FUNCTION__,termCap,sceheme);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}

		//Get TACDef for this Scheme
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s.Config:TACDefault",sceheme);
		tacdefault = iniparser_getstring(dict, szKey, NULL);
		if(tacdefault == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: TACDefault Missing for this scheme :%s, check the next scheme", __FUNCTION__,sceheme);
			APP_TRACE(szDbgMsg);
			continue;
		}
		//TACDef must be 10 chars. If it is not , then we have an incorrect value
		if(strlen(tacdefault) != 10)
		{
			debug_sprintf(szDbgMsg, "%s: TACDefault has incorrect value :%s for scheme:%s", __FUNCTION__,tacdefault,sceheme);
			APP_TRACE(szDbgMsg);
			continue;
		}

		//Get TACDen for this Scheme
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s.Config:TACDenial",sceheme);
		tacdenial = iniparser_getstring(dict, szKey, NULL);
		if(tacdenial == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: TACDenial Missing for this scheme :%s, check the next scheme", __FUNCTION__,sceheme);
			APP_TRACE(szDbgMsg);
			continue;
		}
		//TACDen must be 10 chars. If it is not , then we have an incorrect value
		if(strlen(tacdenial) != 10)
		{
			debug_sprintf(szDbgMsg, "%s: TACDenial has incorrect value :%s for scheme:%s", __FUNCTION__,tacdenial,sceheme);
			APP_TRACE(szDbgMsg);
			continue;
		}

		//Get TACOnline for this Scheme
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s.Config:TACOnline",sceheme);
		taconline = iniparser_getstring(dict, szKey, NULL);
		if(taconline == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: TACOnline Missing for this scheme :%s, check the next scheme", __FUNCTION__,sceheme);
			APP_TRACE(szDbgMsg);
			continue;
		}
		//TACOnline must be 10 chars. If it is not , then we have an incorrect value
		if(strlen(taconline) != 10)
		{
			debug_sprintf(szDbgMsg, "%s: TACOnline has incorrect value :%s for scheme:%s", __FUNCTION__,taconline,sceheme);
			APP_TRACE(szDbgMsg);
			continue;
		}

		//Get the Different Partially Matching AIDs for the sceheme , search for it in AIDs Hash table and then set the termcap for each of the AIDs
		// For Example:If EMVTables.ini has A000041010 , it would match with A000041010A1/A000041010AB1 etc.
		while(1)
		{
			iAids++;
			memset(szKey, 0x00, sizeof(szKey));
			//Format for EMVTable and CTLS Config is little different, SupportedAIDx for EMVTables and AIDx for CTLSConfig
			if(bForEmvCtls)
			{
				sprintf(szKey, "%s.AID:AID%d",sceheme,iAids);
			}
			else
			{
				sprintf(szKey, "%s.AID:SupportedAID%d",sceheme,iAids);
			}
			suppAid = iniparser_getstring(dict, szKey, NULL);
			if(suppAid ==  NULL)
			{
				debug_sprintf(szDbgMsg, "%s: No More AIDs, Final Num of AIDs in Scheme:%d", __FUNCTION__, iAids-1);
				APP_TRACE(szDbgMsg);
				break;
			}
			if((iUpdatedAid = setTermDtlsInHashTable(bForEmvCtls, suppAid, termCap, tacdefault, tacdenial, taconline)) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Updated %d AIDs", __FUNCTION__, iUpdatedAid);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: No AIDs :%s found to update", __FUNCTION__,suppAid);
				APP_TRACE(szDbgMsg);
			}
			iUpdatedAid = 0;
		}
	}

	if(dict != NULL)
	{
		iniparser_freedict(dict);
		dict = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: updateCTLSMode
 *
 * Description	: This function will update CTLS Mode based on config
 * 					ctlsemvenabled and contactlessEnabled
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int updateCTLSMode(char *pszErrMsg)
{
	int					rv					= SUCCESS;
	int 				iTmp				= 0;
	int					iTapFlagXPI			= getInternalTapFlagfromXPI();
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[300]	= "";
	char *				value				= NULL;
	char				szKey[100]			= "";
	char				szTmp[256]			= "";
//	char				szErrMsg[256]		= "";
	PAAS_BOOL			bEmvCTLSEnabled		= isContactlessEmvEnabledInDevice();
	PAAS_BOOL			bEmvEnabled			= isEmvEnabledInDevice();
	PAAS_BOOL			bCTLSEnabled		= isContactlessEnabledInDevice();
	EMV_OTHER_REC_STYPE	stOtherRecInfo;
	dictionary *		dict				= NULL;
	FILE	*			finp				= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		memset(&stOtherRecInfo, 0x00, sizeof(EMV_OTHER_REC_STYPE));

		if(SUCCESS != doesFileExist(OPT_FLAG_FILE_PATH))
		{
			debug_sprintf(szDbgMsg, "%s: %s File not present, ERROR", __FUNCTION__, OPT_FLAG_FILE_PATH);
			APP_TRACE(szDbgMsg);
			getDisplayMsg(pszErrMsg, MSG_FILE_NOT_PRESENT);
			sprintf(pszErrMsg, "%s: %s",pszErrMsg, OPT_FLAG_FILE_PATH);
			rv = FAILURE;
			break;
		}
		rv = checkAndCorrectFileFormat(OPT_FLAG_FILE_PATH);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to improve Format %s ", __FUNCTION__, OPT_FLAG_FILE_PATH);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Loading EMV Config Files");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
			sprintf(pszErrMsg, "%s: %s",pszErrMsg, OPT_FLAG_FILE_PATH);
			rv = FAILURE;
			break;
		}

		dict = iniparser_load(OPT_FLAG_FILE_PATH);
		if(dict == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to load ini file [%s]",
					__FUNCTION__, OPT_FLAG_FILE_PATH);
			APP_TRACE(szDbgMsg);
			getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
			sprintf(pszErrMsg, "%s: %s", pszErrMsg, OPT_FLAG_FILE_PATH);
			rv = FAILURE;
			break;
		}

		if(bEmvEnabled)
		{
			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s:%s", "Rec_00", "VisaDebit");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				stOtherRecInfo.inVisaDebit = *value - '0';
			}

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s:%s", "Rec_00", "CTLSEnable");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				if(bCTLSEnabled)
				{
					if(bEmvCTLSEnabled)
					{
						stOtherRecInfo.inEMVContactless = 0;
					}
					else
					{
						stOtherRecInfo.inEMVContactless = 1;
					}
				}
				else
				{
					stOtherRecInfo.inEMVContactless = 2;
				}
				memset(szTmp, 0x00, sizeof(szTmp));
				sprintf(szTmp, "%d", stOtherRecInfo.inEMVContactless);
				iTmp = iniparser_setstr(dict, szKey, szTmp);

				debug_sprintf(szDbgMsg, "%s: internal_tap_flag should %d", __FUNCTION__, stOtherRecInfo.inEMVContactless);
				APP_TRACE(szDbgMsg);

				if(iTapFlagXPI == stOtherRecInfo.inEMVContactless)
				{
					debug_sprintf(szDbgMsg, "%s: internal_tap_flag is already %d No need of D25 command", __FUNCTION__, stOtherRecInfo.inEMVContactless);
					APP_TRACE(szDbgMsg);
					break;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: D25 command is needed", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s:%s", "Rec_00", "PDOLSupport");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				stOtherRecInfo.inPDOLSupFlg = *value - '0';
			}

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s:%s", "Rec_00", "ValueLinkSupport");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				stOtherRecInfo.inValueLinkCrdSup = *value - '0';
			}

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s:%s", "Rec_00", "softcardsupport");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				stOtherRecInfo.inISISNFCSup = *value - '0';
			}

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s:%s", "Rec_00", "AMEXCTLS");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				stOtherRecInfo.inAMEXCTLSSup = *value - '0';
			}

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s:%s", "Rec_00", "MobileSupport");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				stOtherRecInfo.inMobileSupport = *value - '0';
			}

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s:%s", "Rec_00", "usdebitflag");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				stOtherRecInfo.inUSDebitFlag = *value - '0';
			}
		}
		else	//EMV disable(Need to check only CTLS enable Flag)
		{
			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s:%s", "Rec_00", "CTLSEnable");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				if(bCTLSEnabled)
				{
					stOtherRecInfo.inEMVContactless = 1;
				}
				else
				{
					stOtherRecInfo.inEMVContactless = 2;
				}
				memset(szTmp, 0x00, sizeof(szTmp));
				sprintf(szTmp, "%d", stOtherRecInfo.inEMVContactless);
				iTmp = iniparser_setstr(dict, szKey, szTmp);

				debug_sprintf(szDbgMsg, "%s: internal_tap_flag should %d", __FUNCTION__, stOtherRecInfo.inEMVContactless);
				APP_TRACE(szDbgMsg);

				if(iTapFlagXPI == stOtherRecInfo.inEMVContactless)
				{
					debug_sprintf(szDbgMsg, "%s: internal_tap_flag is already %d No need of D25 command", __FUNCTION__, stOtherRecInfo.inEMVContactless);
					APP_TRACE(szDbgMsg);
					break;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: D25 command is needed", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		rv = processEmvD25Cmd(&stOtherRecInfo);
		break;
	}
	if(dict != NULL)
	{
		finp = fopen(OPT_FLAG_FILE_PATH, "w");
		if(finp == NULL)
		{
			rv = FAILURE;
		}
		else
		{
			iniparser_dump_ini(dict, finp);
			fclose(finp);
		}
		iniparser_freedict(dict);
	}

	if(rv != SUCCESS)
	{
		getDisplayMsg(szTmp, MSG_EMV_INIT_ERR);
		sprintf(pszErrMsg, "%s. %s.",pszErrMsg, szTmp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: isEmvAgentRunning
 *
 * Description	: This function helps us to know if EMV agent is running by sending
 * 					Get Version command to the EMV agent.
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static int isEmvAgentRunning()
{
	int				rv						= SUCCESS;
	char			szEmvVersion[50] 		= "";
	char			szEmvKernelVer[10+1]	= "";
	char			szEmvCTLSVer[20+1]		= "";

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
	//check whether EMV App is running or not by sending S95 EMV command.	
	rv = getVersionFromEMVAgent(szEmvVersion, szEmvKernelVer, szEmvCTLSVer);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get EMV version", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		setEMVVersion(szEmvVersion, szEmvKernelVer, szEmvCTLSVer);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);	

	return rv;
}

/*
 * ============================================================================
 * Function Name: checkWalletSupportinDevice
 *
 * Description	: This function helps us to know if Wallet Support is enabled in
 * 					SAC and XPI both or not
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
extern int checkWalletSupportinDevice()
{
	int				rv						= SUCCESS;
	int				iVal					= 0;
	int				iLen					= 0;
	int				iAppLogEnabled			= isAppLogEnabled();
	char			szAppLogData[300]		= "";
	char			szAppLogDiag[300]		= "";
	char			szTemp[1+1]				= "";

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
	iLen = sizeof(szTemp);

	while(1)
	{
		if(isWalletEnabled() == PAAS_FALSE)
		{
			debug_sprintf(szDbgMsg, "%s: Wallet Not Enabled in Device", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS;
			break;
		}

		memset(szTemp, 0x00, iLen);
		rv = getEnvFile("iab", "wallet_support", szTemp, iLen);
		if(rv > 0)
		{
			iVal = atoi(szTemp);
			debug_sprintf(szDbgMsg,"%s: Value found for wallet_support = %d", __FUNCTION__, iVal);
			APP_TRACE(szDbgMsg);
			if( iVal != 1)
			{
				debug_sprintf(szDbgMsg,"%s: Wallet Enabled in SCA but Not in XPI", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			rv = SUCCESS;
		}
		else
		{
			/* No value found for wallet_support*/
			debug_sprintf(szDbgMsg,"%s: No value found for wallet_support", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		memset(szTemp, 0x00, iLen);
		rv = getEnvFile("iab", "append_walletdata", szTemp, iLen);
		if(rv > 0)
		{
			iVal = atoi(szTemp);
			debug_sprintf(szDbgMsg,"%s: Value found for append_walletdata = %d", __FUNCTION__, iVal);
			APP_TRACE(szDbgMsg);
			if( iVal != 1)
			{
				debug_sprintf(szDbgMsg,"%s: Wallet Enabled in SCA but append_walletdata is incorrect", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			rv = SUCCESS;
		}
		else
		{
			/* No value found for append_walletdata*/
			debug_sprintf(szDbgMsg,"%s: No value found for append_walletdata, but XPI takes default as 1 which we needs, Proceeding Successfully", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS;
		}

		memset(szTemp, 0x00, iLen);
		rv = getEnvFile("iab", "nfc_cancel_on_reset", szTemp, iLen);
		if(rv > 0)
		{
			iVal = atoi(szTemp);
			debug_sprintf(szDbgMsg,"%s: Value found for nfc_cancel_on_reset = %d", __FUNCTION__, iVal);
			APP_TRACE(szDbgMsg);
			if( iVal != 1)
			{
				debug_sprintf(szDbgMsg,"%s: Wallet Enabled in SCA but nfc_cancel_on_reset is incorrect", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			rv = SUCCESS;
		}
		else
		{
			/* No value found for nfc_cancel_on_reset*/
			debug_sprintf(szDbgMsg,"%s: No value found for nfc_cancel_on_reset", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		break;
	}

	if(rv != SUCCESS)
	{
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Wallet Support Configuration Mismatched in Device,");
			sprintf(szAppLogDiag, "Please Check All the Wallet Configuration Settings in Device");
			addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, szAppLogDiag);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: processEmvTranReq
 *
 * Description	: This function would execute EMV transaction flow.
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int processEmvTranReq(char* szTranStkKey, CARDDTLS_PTYPE pstCardDtls)
{
	int 		rv 					= SUCCESS;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";

#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif	

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "Processing EMV Transaction First Gen AC Request");
		addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
	}
	rv = processEmvC32Cmd(pstCardDtls, szTranStkKey);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Got error during C32 command", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "EMV Transaction First Gen AC Request Failed [%d]", rv);
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);	
	
	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: applyScrptDataToEmvCard
 *
 * Description	: This API would allows the creation of EMVS71.DAT and EMVS72.DAT files a
 * 				  dding the scripts received from the host to the inserted card.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int applyScrptDataToEmvCard(char * szTranStkKey)
{
	int				rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	rv = buildEmvCmd(EMV_C25, &stEMVReq, szTranStkKey);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			while(bWait == PAAS_TRUE)
			{
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						rv = getUIRespCodeOfEmvCmdResp(EMV_C25, stBLData.iStatus);
						switch(rv)
						{
							case SUCCESS:
								debug_sprintf(szDbgMsg, "%s: C25 command returned [SUCCESS]", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								break;

							default:
								/* 'D' command failed */
								debug_sprintf(szDbgMsg, "%s: C25 command returned Error[%d]",__FUNCTION__,
																						stBLData.iStatus);
								APP_TRACE(szDbgMsg);

								break;
						}
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				svcWait(100);
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[C25]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: completeEmvOnlineTran
 *
 * Description	: This API would complete an online EMV transaction.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int completeEmvOnlineTran(char * szTranStkKey, CARDDTLS_PTYPE pstCardDtls, int iResultCode)
{
	int				rv				= SUCCESS;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = processEmvC34Cmd(pstCardDtls, szTranStkKey, iResultCode);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Got error during C34 command", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: checkCrdPrsnceAndShwMsgToRemove
 *
 * Description	: This function would check whether card is present and 
 *				  shows message to remove a card if present.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int checkCrdPrsnceAndShwMsgToRemove()
{
	int 					rv 					= SUCCESS;
	int						iAppLogEnabled		= isAppLogEnabled();
	char					szAppLogData[300]	= "";
#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif	

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = processEmvI02Cmd();
	if(rv == SUCCESS && iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "EMV Card Removed");
		addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
	}
	else if (rv == UI_AGENT_BUSY)
	{
		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "UI Agent is Busy");
			addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
		}
		rv = SUCCESS;
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);	
	
	return rv;
}

/*
 * ============================================================================
 * Function Name: processEmvC32Cmd
 *
 * Description	: This function would process EMV command request.
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static int processEmvC32Cmd(CARDDTLS_PTYPE pstCardDtls, char* szTranStkKey)
{
	int 			rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	EMVDTLS_STYPE	stEmvDtls;

#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = sendC32CmdToEMVAgent(szTranStkKey);
	if(rv == SUCCESS)
	{
		/* Wait for the response*/
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;

			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_EMV_RESP)
				{
					debug_sprintf(szDbgMsg, "%s: Received the EMV Data while waiting for C33",
							__FUNCTION__);
					APP_TRACE(szDbgMsg);
					if( strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd,EMV_U00_REQ) == 0 || strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_U02_REQ) ==0 )
					{

						debug_sprintf(szDbgMsg, "%s: Received U02",__FUNCTION__);
						APP_TRACE(szDbgMsg);
//						memset(&stEmvDtls, 0x00, sizeof(EMVDTLS_STYPE));
//						memcpy(&stEmvDtls, &(stBLData.stRespDtls.stEmvDtls), sizeof(EMVDTLS_STYPE));
						bBLDataRcvd = PAAS_FALSE;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Received C33 EMV data", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						memset(&stEmvDtls, 0x00, sizeof(EMVDTLS_STYPE));
						memcpy(&stEmvDtls, &(stBLData.stRespDtls.stEmvDtls), sizeof(EMVDTLS_STYPE));

						debug_sprintf(szDbgMsg, "%s: EMV resp str= %s int= %d",__FUNCTION__, stEmvDtls.szEMVRespCode, atoi(stEmvDtls.szEMVRespCode));
						APP_TRACE(szDbgMsg);

						rv = getUIRespCodeOfEmvCmdResp(EMV_C32, atoi(stEmvDtls.szEMVRespCode));
						if(rv == SUCCESS)
						{
							storeEmvCardData(&stEmvDtls, pstCardDtls);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "EMV Transaction First Gen AC Success");
								addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
							}
							if(strlen(pstCardDtls->stEmvtranDlts.szAuthRespCode) > 0)
							{
								debug_sprintf(szDbgMsg, "%s: Got Auth Resp Code[%s] after C32 cmd",
										__FUNCTION__, pstCardDtls->stEmvtranDlts.szAuthRespCode);
								APP_TRACE(szDbgMsg);
							}
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}
				else if(stBLData.uiRespType == UI_CARD_RESP)
				{
					debug_sprintf(szDbgMsg, "%s: Got C3321 Response, 1st Gen AC failure Case, FallBack to MSR", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = EMV_FALLBACK_TO_MSR;

					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}
				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				if(bWait == PAAS_FALSE)
				{
					break;
				}
			}
			svcWait(15);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to send command[C32]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processEmvC34Cmd
 *
 * Description	: This function would process EMV command request.
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static int processEmvC34Cmd(CARDDTLS_PTYPE pstCardDtls, char* szTranStkKey, int iResultCode)
{
	int 			rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;
	EMVDTLS_STYPE	stEmvDtls;

#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = sendC34CmdToEMVAgent(pstCardDtls, szTranStkKey, iResultCode);
	if(rv == SUCCESS)
	{
		/* Wait for the response*/
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;

			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_EMV_RESP)
				{
					rv = getUIRespCodeOfEmvCmdResp(EMV_C34, atoi(stBLData.stRespDtls.stEmvDtls.szEMVRespCode));
					switch(rv)
					{
					case SUCCESS:
						debug_sprintf(szDbgMsg, "%s: C34 command returned [SUCCESS]", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						memset(&stEmvDtls, 0x00, sizeof(EMVDTLS_STYPE));
						memcpy(&stEmvDtls, &(stBLData.stRespDtls.stEmvDtls), sizeof(EMVDTLS_STYPE));

						storeEmvCardData(&stEmvDtls, pstCardDtls);
						if(strlen(pstCardDtls->stEmvtranDlts.szAuthRespCode) > 0)
						{
							debug_sprintf(szDbgMsg, "%s: Got Auth Resp Code[%s] after C34 cmd",	__FUNCTION__, pstCardDtls->stEmvtranDlts.szAuthRespCode);
							APP_TRACE(szDbgMsg);
						}
						break;

					default:
						/* 'C34' command failed */
						debug_sprintf(szDbgMsg, "%s: C34 command returned Error[%d]",__FUNCTION__,
								stBLData.iStatus);
						APP_TRACE(szDbgMsg);

						break;
					}
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				if(bWait == PAAS_FALSE)
				{
					break;
				}
			}
			svcWait(15);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to send command[C34]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
#if DEVDEBUG
	printCardEMVResp(pstCardDtls);
#endif
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTagFromEmvAgent
 *
 * Description	: This function would process EMV command request C20 and request card for all tags in the
 * 					szEmvTags
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static int getTagFromEmvAgent(char* pszEmvTag, char* pszEMVTagValue, int iLen)
{
	int 			rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;
	EMVDTLS_STYPE	stEmvDtls;

#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = sendC20CmdToEMVAgent(pszEmvTag, iLen);
	if(rv == SUCCESS)
	{
		/* Wait for the response*/
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;

			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_EMV_RESP)
				{
					debug_sprintf(szDbgMsg, "%s: Received C20 EMV data", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					memset(&stEmvDtls, 0x00, sizeof(EMVDTLS_STYPE));
					memcpy(&stEmvDtls, &(stBLData.stRespDtls.stEmvDtls), sizeof(EMVDTLS_STYPE));

					debug_sprintf(szDbgMsg, "%s: EMV resp str= %s int= %d",__FUNCTION__, stEmvDtls.szEMVRespCode, atoi(stEmvDtls.szEMVRespCode));
					APP_TRACE(szDbgMsg);

					rv = getUIRespCodeOfEmvCmdResp(EMV_C20,(atoi(stEmvDtls.szEMVRespCode)));
					if(rv == SUCCESS)
					{
						strcpy(pszEMVTagValue, stEmvDtls.szTagValue);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid Command Response Code on C20",__FUNCTION__);

						APP_TRACE(szDbgMsg);
						//rv = FAILURE;
						//break;
					}

					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				if(bWait == PAAS_FALSE)
				{
					break;
				}
			}
			svcWait(15);
		}
	}

	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to send command[C20]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processEmvC19Cmd
 *
 * Description	: This function would process EMV command and request card
 * 					set an EMV tag value
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static int processEmvC19Cmd(char *szEmvTags, int iLen)
{
	int 			rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;
	EMVDTLS_STYPE	stEmvDtls;

#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = sendC19CmdToEMVAgent(szEmvTags, iLen);
	if(rv == SUCCESS)
	{
		/* Wait for the response*/
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;

			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_EMV_RESP)
				{
					debug_sprintf(szDbgMsg, "%s: Received C19 EMV data", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					memset(&stEmvDtls, 0x00, sizeof(EMVDTLS_STYPE));
					memcpy(&stEmvDtls, &(stBLData.stRespDtls.stEmvDtls), sizeof(EMVDTLS_STYPE));

					debug_sprintf(szDbgMsg, "%s: EMV resp str= %s int= %d",__FUNCTION__, stEmvDtls.szEMVRespCode, atoi(stEmvDtls.szEMVRespCode));
					APP_TRACE(szDbgMsg);

					rv = getUIRespCodeOfEmvCmdResp(EMV_C20,(atoi(stEmvDtls.szEMVRespCode)));
					if(rv == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Got Success Command Response Code on C19",__FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid Command Response Code on C19",__FUNCTION__);

						APP_TRACE(szDbgMsg);
						rv = FAILURE;
						//break;
					}
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				if(bWait == PAAS_FALSE)
				{
					break;
				}
			}
			svcWait(15);
		}
	}

	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to send command[C19]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: processEmvD25Cmd
 *
 * Description	:
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int processEmvD25Cmd(EMV_OTHER_REC_PTYPE pstEmvOtherRec)
{
	int 			rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;

#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);


	rv = sendD25CmdToEMVAgent(pstEmvOtherRec);
	if(rv == SUCCESS)
	{
		/* Wait for the response*/
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;

			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_EMV_RESP)
				{
					debug_sprintf(szDbgMsg, "%s: Received D25 EMV data", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = stBLData.iStatus;

					if(rv == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Got Success Command Response Code on D25",__FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid Command Response Code on D25",__FUNCTION__);

						APP_TRACE(szDbgMsg);
						rv = FAILURE;
						//break;
					}
					bBLDataRcvd = PAAS_FALSE;
					bWait 		= PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				if(bWait == PAAS_FALSE)
				{
					break;
				}
			}
			svcWait(15);
		}
	}

	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to send command[D25]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: setXPIParameter
 *
 * Description	: This function would set XPI[iab] parameter
 *
 * Input Params	: parameter name and Value
 *
 * Output Params: None
 * ============================================================================
 */
extern int setXPIParameter(char *szParam, char *szValue)
{
	int 			rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;

#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = processM41Command(szParam, szValue);
	if(rv == SUCCESS)
	{
		/* Wait for the response*/
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;

			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_EMV_RESP)
				{
					debug_sprintf(szDbgMsg, "%s: Received M41 Response", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = stBLData.iStatus;
					if(rv == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Got Success Command Response Code on M41",__FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error Response Code on M41",__FUNCTION__);

						APP_TRACE(szDbgMsg);
						rv = FAILURE;
						//break;
					}
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				if(bWait == PAAS_FALSE)
				{
					break;
				}
			}
			svcWait(15);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to send command[M41]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: processEmvD25Cmd
 *
 * Description	: This function would process EMV command and request card
 * 					set an EMV tag value
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int processEmvD25Cmd(int bCtlsEMV)
{
	int 			rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;
	EMVDTLS_STYPE	stEmvDtls;

#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(doesFileExist(D25_COMMAND_FILE_NAME) != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: D25.txt NOT present, We will not send any D25 in b/w Trans", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SUCCESS;
	}
	rv = sendD25CmdToEMVAgent(bCtlsEMV);
	if(rv == SUCCESS)
	{
		/* Wait for the response*/
		while(bWait == PAAS_TRUE)
		{
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_EMV_RESP)
				{
					debug_sprintf(szDbgMsg, "%s: Received D25 EMV data", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					memset(&stEmvDtls, 0x00, sizeof(EMVDTLS_STYPE));
					memcpy(&stEmvDtls, &(stBLData.stRespDtls.stEmvDtls), sizeof(EMVDTLS_STYPE));

					debug_sprintf(szDbgMsg, "%s: EMV resp str= %s int= %d",__FUNCTION__, stEmvDtls.szEMVRespCode, atoi(stEmvDtls.szEMVRespCode));
					APP_TRACE(szDbgMsg);

					rv = getUIRespCodeOfEmvCmdResp(EMV_D25,(atoi(stEmvDtls.szEMVRespCode)));
					if(rv == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Got Success Command Response Code on D25",__FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid Command Response Code on D25",__FUNCTION__);

						APP_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
					bBLDataRcvd = PAAS_FALSE;
					bWait 		= PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			svcWait(10);
		}
	}

	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to send command[D25]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: updateEMVtaginXPI
 *
 * Description	: This API would be used to update a Tag in XPI
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateEMVtaginXPI(ENUM_EMV_CMDS eCmd, char* sztransCmd)
{
	int				rv				= SUCCESS;
	int				iTLVLen			= 0;
	char 			szEmvTLV[50]	= "";
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szEmvTLV, 0x00, sizeof(szEmvTLV));
	strcpy(szEmvTLV, sztransCmd); iTLVLen = strlen(sztransCmd);

	rv = processEmvC19Cmd(szEmvTLV, iTLVLen);
//remove
	debug_sprintf(szDbgMsg, "%s: Returning rv = %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	setEmvReqCmd(eCmd);

	return rv;
}
/*
 * ============================================================================
 * Function Name: isCashBackAllowedForEMVCard
 *
 * Description	: This API would be used to check whether cashback is allowed on
 * 					EMV card
 *
 * Input Params	: Application Usage Control ( 9F07 ) - obtained by using C36 using getReqdEMVTagsFromXPI
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
PAAS_BOOL isCashBackAllowedForEMVCard(CARDDTLS_PTYPE pstCardDtls,char *pszTranKey)
{
	int				iTmp			= 0;
	int				rvTemp			= 0;
	char			szEMVTagVal[20] = "";
	PAAS_BOOL 		bCashbackEMV	= PAAS_FALSE;
	PAAS_BOOL		bPredictCVM		= PAAS_TRUE;
	AMTDTLS_STYPE	stAmtDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Earlier we used to query for 9F07 specifically by sending a C20. But to improve performance, we will be sending C36.
	 * This is required since C20 can query for only 1 tag. In case kernel switching is enabled, we will have to query for other tags as well
	 * So we are sending a single C36 and querying for all these tags. We pass the 9F07 tag value as parameter to this function.
	 */
	//Sending 9F07
	//getTagFromEmvAgent("9F07", szEMVTagVal, 4);

	strncpy(szEMVTagVal,pstCardDtls->stEmvAppDtls.szAppUsageControl,sizeof(szEMVTagVal)-1);
	//if(strlen(stCardDtls.szEMVTags) > 0)
	if(strlen(szEMVTagVal) > 0)
	{
		if(szEMVTagVal[2] <= '9')
		{
			iTmp = szEMVTagVal[2] - '0';
		}
		else
		{
			iTmp = 10 + (szEMVTagVal[2] - 'A');
		}

		debug_sprintf(szDbgMsg, "%s: App Usage Control: %s Itmp:%d", __FUNCTION__, szEMVTagVal, iTmp);
		APP_TRACE(szDbgMsg);

		if((iTmp & 0x4) || (iTmp & 0x8))
		{
			bCashbackEMV = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: EMV CASH back enabled on card", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: App Usage Control Not found Setting as CashBack NOT ALLOWED", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	if(!bCashbackEMV)
	{
		return bCashbackEMV;
	}
	memset(&stAmtDtls, 0x00, sizeof(AMTDTLS_STYPE));
	rvTemp = getAmtDtlsForPymtTran(pszTranKey, &stAmtDtls);
	if(rvTemp != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get Amnt dtls for EMV ",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		return bCashbackEMV;
	}
	/* Daivik:16/9/2016 -  We send Cashback Amount as 1.00 because we want to predict the CVM assuming there is
	 * cashback amount in the transaction. We will first Predict the CVM with the Tag 9F33.
	 * We will then predict the CVM based on the default Terminal Cap obtained from EMVTables.ini
	 */
	if((bCashbackEMV = isCVMAvailableInCard(pstCardDtls->stEmvTerDtls.szTermCapabilityProf,pstCardDtls->stEmvAppDtls.szIccCVMList,atof(stAmtDtls.tranAmt),1.00,bPredictCVM)) == TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: Cashback Supported - Current Kernel Setting", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Cashback Not Supported - Current Kernel Setting", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		//Daivik:29/9/2016 - Adding the EMV Kernel Switching  Condition because , if current term cap does not support Cashback and if EMV Kernel Switch is not supported, we will anyway not be switching to a different termcap
		//We will be using the current term cap only
		if((strncmp(pstCardDtls->stEmvTerDtls.szTermCapabilityProf,pstCardDtls->stEmvTerDtls.szTermCapDefault,6)) && isEmvKernelSwitchingAllowed())//This would happen only in case of EMV Kernel Switching
		{
			if((bCashbackEMV = isCVMAvailableInCard(pstCardDtls->stEmvTerDtls.szTermCapDefault,pstCardDtls->stEmvAppDtls.szIccCVMList,atof(stAmtDtls.tranAmt),1.00,bPredictCVM)) == TRUE)
			{
				debug_sprintf(szDbgMsg, "%s: Cashback  Supported - Default Kernel Setting", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Cashback Not Supported - Default Kernel Setting", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s:Current TermCap same as Default TermCap/KernelSwitch Disabled, need not check Cashback Support", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}


	debug_sprintf(szDbgMsg, "%s: Returning CashBack Enabled %d", __FUNCTION__, bCashbackEMV);
	APP_TRACE(szDbgMsg);

	return bCashbackEMV;
}

#if 0
/*
 * ============================================================================
 * Function Name: processEmvC36Cmd
 *
 * Description	: This function would process EMV command request.
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static int processEmvC36Cmd(EMVDTLS_PTYPE pstEmvDtls, char* szTranStkKey)
{
	int 			rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;

#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//FIXME: For now "SWIPE_FRM" screen is assigned.Later assign EMV form or remove it.
	whichForm = SWIPE_FRM;

	initUIReqMsg(&stEMVReq, INIT_REQ);

	rv = buildEmvCmd(EMV_C36, &stEMVReq, szTranStkKey);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			/* Wait for the response*/
			while(bWait == PAAS_TRUE)
			{
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						rv = getUIRespCodeOfEmvCmdResp(EMV_C36, stBLData.iStatus);
						switch(rv)
						{
							case SUCCESS:
								debug_sprintf(szDbgMsg, "%s: Received EMV data", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								//Copy data
								//AJAYS2:TODO:Check whether whole cpying is req/ or only some members
								memcpy(pstEmvDtls, &stBLData.stRespDtls.stEmvDtls, SIZE_EMVDTLS);
								break;

							default:
								/* Do nothing */
								debug_sprintf(szDbgMsg, "%s: EMV command returned Error[%d]", __FUNCTION__,
																							stBLData.iStatus);
								APP_TRACE(szDbgMsg);

								break;
						}
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(200);
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[C36]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: isEmvChipCardPresennt
 *
 * Description	: This function would check card presence in the card slot.
 *
 * Input Params	: Transaction instance stack key
 *
 * Output Params: CARD_PRESENT if card present else CARD_NOT_PRESENT
 * ============================================================================
 */
static int isEmvChipCardPresennt()
{
	int 			rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;

#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	setEmvReqCmd(EMV_I05);

	getI_05Command(stEMVReq.pszBuf, &stEMVReq.iSize);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			/* Wait for the response*/
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;

				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						switch(stBLData.iStatus)
						{
							case CARD_NOT_PRESENT :
								debug_sprintf(szDbgMsg, "%s: Card Not Present", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = CARD_NOT_PRESENT;
								break;

							case CARD_PRESENT :
								debug_sprintf(szDbgMsg, "%s: Card Present ", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = CARD_PRESENT;
								break;

							default:
								/* Do nothing */
								debug_sprintf(szDbgMsg, "%s: EMV command returned Error[%d]", __FUNCTION__,
																							stBLData.iStatus);
								APP_TRACE(szDbgMsg);

								break;
						}
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[I05]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processEmvI02Cmd
 *
 * Description	: This function would show message to remove card if present.
 *
 * Input Params	: Transaction instance stack key
 *
 * Output Params: CARD_REMOVED if card not present else CARD_NOT_REMOVED
 * ============================================================================
 */
static int processEmvI02Cmd()
{
	int 			rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;

#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	setEmvReqCmd(EMV_I02);

	getI_02Command(stEMVReq.pszBuf, &stEMVReq.iSize);
	if(rv == SUCCESS)
	{
		//Daivik:29/6/2016-Observed an Issue where we did not wait for I02 response and immediately sent the transaction cancelled message
		//But we later received the I02 response and hence remained on Please remove card screen.
		bBLDataRcvd = PAAS_FALSE;
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			/* Showing Stand by Status in the GEN_FORM */
			while(bWait == PAAS_TRUE)
			{
				//CHECK_POS_INITIATED_STATE; Praveen_P1: 25 Oct 2016: Putting this check causing returning of this function without waiting for I02 response

				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						switch(stBLData.iStatus)
						{
							case CARD_REMOVED:
								debug_sprintf(szDbgMsg, "%s: Card Removed", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = CARD_REMOVED;
								break;

							case CARD_NOT_REMOVED:
								debug_sprintf(szDbgMsg, "%s: Card Remove Unsuccessful", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = CARD_NOT_REMOVED;
								break;

							case UI_AGENT_BUSY:
								debug_sprintf(szDbgMsg, "%s: UI Agent is Busy. So not honoring this command", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = UI_AGENT_BUSY;
								break;

							default:
								/* Do nothing */
								debug_sprintf(szDbgMsg, "%s: EMV command returned Error[%d]", __FUNCTION__,
																							stBLData.iStatus);
								APP_TRACE(szDbgMsg);

								break;
						}
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
					if(bWait == PAAS_FALSE)
					{
						break;
					}
				}
				/* Daivik :4/3/2016 :Changing this to svcWait(15) from svcWait(200) since we have seen an issue where we had sent I02, and we recieved I02 response from FA.
				 * But we also recieved another response '01' . This was overwriting the earlier recieved response.
				 * Though such cases should not occur , reducing this wait time to accomodate such scenario.
				 */
				svcWait(15);
			}
			CHECK_POS_INITIATED_STATE; //Praveen_P1 25 Oct 2016: Bringing this check from top so that we will check for disconnection/cancel notification after getting I02 response
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[I02]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0 //Remove it later.
/*
 * ============================================================================
 * Function Name: processEmvCmd
 *
 * Description	: This function would process EMV command request
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static int processEmvCmd(ENUM_EMV_CMDS eCmd, EMVDTLS_PTYPE pstEmvDtls, char* szTranStkKey)
{
	int 			rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;

#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif	

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//FIXME: For now "SWIPE_FRM" screen is assigned.Later assign EMV form or remove it.
	whichForm = SWIPE_FRM;

	initUIReqMsg(&stEMVReq, INIT_REQ);

	rv = buildEmvCmd(eCmd, &stEMVReq, szTranStkKey);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			/* Wait for the response*/
			while(bWait == PAAS_TRUE)
			{
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						rv = getUIRespCodeOfEmvCmdResp(eCmd, stBLData.iStatus);
						switch(rv)
						{
							case SUCCESS:
								debug_sprintf(szDbgMsg, "%s: Received EMV data", __FUNCTION__);
								APP_TRACE(szDbgMsg);				
						
								//Copy data 
								memcpy(pstEmvDtls, &stBLData.stRespDtls.stEmvDtls,
																sizeof(EMVDTLS_STYPE));	
								break;							

							default:
								/* Do nothing */
								debug_sprintf(szDbgMsg, "%s: EMV command returned Error[%d]", __FUNCTION__,
																							stBLData.iStatus);
								APP_TRACE(szDbgMsg);			

								break;		
						}
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(200);
			}		
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command['C' cmd no=%d]", 
															__FUNCTION__, eCmd);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: buildEmvCmd
 *
 * Description	: This function would build EMV C commands.
 *
 * Input Params	: @eCmd 	-> EMV command name
 *				  
 * Output Params: SUCCESS -> @pstEMVReq contains command and length of the command.
 * 				  FAILURE -> @pstEMVReq=NULL
 * ============================================================================
 */
static int buildEmvCmd(ENUM_EMV_CMDS eCmd, UIREQ_PTYPE pstEMVReq, char* szTranStkKey)
{
	int				 rv					= SUCCESS; 					 		

#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif	

	debug_sprintf(szDbgMsg, "%s: ---enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);	

	//Store the request command type in the EMV dtls structure.
	setEmvReqCmd(eCmd);

	switch (eCmd)
	{
		case EMV_S13:	
			debug_sprintf(szDbgMsg, "%s: Building EMV [S13] command", 
														__FUNCTION__);
			APP_TRACE(szDbgMsg);				
			getS_13Command(pstEMVReq->pszBuf,&pstEMVReq->iSize);
			break;
		case EMV_S14:	
			debug_sprintf(szDbgMsg, "%s: Building EMV [S14] command", 
														__FUNCTION__);
			APP_TRACE(szDbgMsg);				
			getS_14Command(pstEMVReq->pszBuf,&pstEMVReq->iSize);
			break;

		case EMV_S20:	
			debug_sprintf(szDbgMsg, "%s: Building EMV [S20] command", 
														__FUNCTION__);
			APP_TRACE(szDbgMsg);				
			getS_20Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, NULL, NULL, NULL);
			break;

		case EMV_D32:	
			debug_sprintf(szDbgMsg, "%s: Building EMV [D32] command", 
														__FUNCTION__);
			APP_TRACE(szDbgMsg);				
			getD_32Command(pstEMVReq->pszBuf,&pstEMVReq->iSize);
			break;
			
		case EMV_S95:	
			debug_sprintf(szDbgMsg, "%s: Building EMV [S95] command", 
														__FUNCTION__);
			APP_TRACE(szDbgMsg);				
			getS_95Command(pstEMVReq->pszBuf,&pstEMVReq->iSize);
			break;
		
		case EMV_S66:	
			debug_sprintf(szDbgMsg, "%s: Building EMV [S66] command", 
														__FUNCTION__);
			APP_TRACE(szDbgMsg);				
			getS_66Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, szTranStkKey);
			break;			

		case EMV_C20:
			debug_sprintf(szDbgMsg, "%s: Building EMV [C20] command", 
														__FUNCTION__);
			APP_TRACE(szDbgMsg);				
			getC_20Command(pstEMVReq->pszBuf, &pstEMVReq->iSize, NULL, 0);
			break;
			
		case EMV_C30:
			debug_sprintf(szDbgMsg, "%s: Building EMV [C30] command", 
														__FUNCTION__);
			APP_TRACE(szDbgMsg);				
			getC_30Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, szTranStkKey, NULL, NULL, NULL);
			break;

		case EMV_C32:
			debug_sprintf(szDbgMsg, "%s: Building EMV [C32] command", 
														__FUNCTION__);
			APP_TRACE(szDbgMsg);	
			getC_32Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, szTranStkKey);
			break;	
			
		case EMV_C34:
			debug_sprintf(szDbgMsg, "%s: Building EMV [C34] command", 
														__FUNCTION__);
			APP_TRACE(szDbgMsg);				
			getC_34Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, szTranStkKey, 0, NULL);
			break;
			
		case EMV_I02:
			debug_sprintf(szDbgMsg, "%s: Building EMV [I02] command", 
														__FUNCTION__);
			APP_TRACE(szDbgMsg);				
			getI_02Command(pstEMVReq->pszBuf,&pstEMVReq->iSize);
			break;
			
		case EMV_I05:
			debug_sprintf(szDbgMsg, "%s: Building EMV [I05] command", 
														__FUNCTION__);
			APP_TRACE(szDbgMsg);				
			getI_05Command(pstEMVReq->pszBuf, &pstEMVReq->iSize);
			break;
			
		case EMV_S11:
			//getS_11Command(pstEMVReq->pszBuf,&pstEMVReq->iSize);
			break;

		case EMV_M40:
			debug_sprintf(szDbgMsg, "%s: Building EMV [M40] command",
														__FUNCTION__);
			APP_TRACE(szDbgMsg);
			getM_40Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, szTranStkKey);
			break;

		case EMV_M41:
			debug_sprintf(szDbgMsg, "%s: Building EMV [M41] command",
														__FUNCTION__);
			APP_TRACE(szDbgMsg);
			getM_40Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, szTranStkKey);
			break;

		case EMV_U00:
			rv = getU_00Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, szTranStkKey);
			break;
		case EMV_E02:
			getE_02Command(pstEMVReq->pszBuf, &pstEMVReq->iSize);
			break;
		case EMV_E10:
			getE_10Command(pstEMVReq->pszBuf, &pstEMVReq->iSize);
			break;
		case EMV_E06:
			getE_06Command(pstEMVReq->pszBuf, &pstEMVReq->iSize);
			break;
		default:
			debug_sprintf(szDbgMsg, "%s: SHOULD NOT COME HERE [Unknow command]", 
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);		
	
	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: buildEmvDcmd
 *
 * Description	: This function would build EMV D commands for EMV download initiazation.
 *
 * Input Params	: @pstEmvCmdInfo 			-> EMV command info
 *				  @iNumRec					-> Total number of record(No of APPID's present)		
 *				  @pstKeyLoadList			-> EMV key load structure used for building 'D' commands.
 * Output Params: SUCCESS -> @pstEMVReq contains command and length of the command.
 * 				  FAILURE -> @pstEMVReq=NULL
 * ============================================================================
 */
static int buildEmvDcmd(EMV_CMD_INFO_PTYPE pstEmvCmdInfo, EMVKEYLOAD_LIST_PTYPE pstKeyLoadList,
																			UIREQ_PTYPE pstEMVReq)
{
	int				 	rv					= SUCCESS; 					 		
	int 				iNumRec				= 0;
	int					iCurCmd				= 0;
	int 				iCurCAPKIndex		= 0;
	int 				iCurSuppAidIndex	= 0;
	long				lnTableNo			= 0;
	//short 				iRecNo				= 0;
	short				inAIDNo				= 0;
	
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif	

	debug_sprintf(szDbgMsg, "%s: ---enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);	

	/* copy to local variables */
	iNumRec				= pstEmvCmdInfo->iNumRec;	
	lnTableNo 			= pstEmvCmdInfo->lnTableNo;
	iCurCmd 			= pstEmvCmdInfo->iCurCmd;
	//iRecNo 				= pstEmvCmdInfo->iRecNo;
	inAIDNo				= pstEmvCmdInfo->inAIDNo;
	iCurCAPKIndex 		= pstEmvCmdInfo->iNumCAPKFiles;
	iCurSuppAidIndex	= pstEmvCmdInfo->iNumSuppAid;	

	//Store the request command type in the EMV dtls structure.
	setEmvReqCmd(iCurCmd);
	
	switch (iCurCmd)
	{
		case EMV_D11:	
			debug_sprintf(szDbgMsg, "%s: Building EMV [D11] command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iCurCAPKIndex > 15)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to build [D11]cmd;(CAPK Index > 15)",
																		__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			getD_11Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, 
										&pstKeyLoadList->stCAPKInfo[iCurCAPKIndex]);
			break;
			
		case EMV_D12:
			debug_sprintf(szDbgMsg, "%s: Building EMV [D12] command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			
			getD_12Command(pstEMVReq->pszBuf,&pstEMVReq->iSize);
			break;

		case EMV_D13:
			debug_sprintf(szDbgMsg, "%s: Building EMV [D13] command", __FUNCTION__);
			APP_TRACE(szDbgMsg);	

			getD_13Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, pstEmvCmdInfo,
												&pstKeyLoadList->stEstRecInfo);	
			break;	
			
		case EMV_D14:
			debug_sprintf(szDbgMsg, "%s: Building EMV [D14] command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			getD_14Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, &pstKeyLoadList->stMvtRecInfo);
			break;
			
		case EMV_D15:
			debug_sprintf(szDbgMsg, "%s: Building EMV [D15] command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			getD_15Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, &pstKeyLoadList->stEstRecInfo);
			break;
			
		case EMV_D16:
			debug_sprintf(szDbgMsg, "%s: Building EMV [D16] command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			
			getD_16Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, lnTableNo, iNumRec);
			break;
			
		case EMV_D17:
			debug_sprintf(szDbgMsg, "%s: Building EMV [D17] command", __FUNCTION__);
			APP_TRACE(szDbgMsg);	
			
			getD_17Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, &pstKeyLoadList->stEestRecInfo[iCurSuppAidIndex]);
			break;
				
		case EMV_D19:
			debug_sprintf(szDbgMsg, "%s: Building EMV [D19] command", __FUNCTION__);
			APP_TRACE(szDbgMsg);	
			
			getD_19Command(pstEMVReq->pszBuf,&pstEMVReq->iSize, &pstKeyLoadList->stEmvtRecInfo);
			break;	

		default:
			debug_sprintf(szDbgMsg, "%s: SHOULD NOT COME HERE [Unknow command]", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
	}	
	debug_sprintf(szDbgMsg, "%s: ---Returning---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);		
	
	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: dispatchEmvRequest
 *
 * Description	: This function would dispatch EMV Request messsage to XPI through FormAgent.
 *
 * Input Params	: msgBuf: Request command message.
 *				  IBuffLen: Length of command message
 *
 * Output Params: SUCCESS: It is dispatched
 *				  FAILURE : It is not dispatched
 * ============================================================================
 */
static int dispatchEmvRequest(char *msgBuf, int iBuffLen)
{
    int				 		rv		 		= SUCCESS;
    int 					iLen			= 0;
    char 					szCmd[10+1]		="";
    char 					szSend[4096] 	="";
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
    // Note msgBuf[0] contains <STX> envelope character
    memset(szCmd, 0, sizeof(szCmd));

    iLen = sizeof(szCmd)-1; // Max number of bytes to copy into szCmd[]
    if (msgBuf[iBuffLen-1] == ETX && msgBuf[0] == STX)
    {
    	debug_sprintf(szDbgMsg,"%s: ETX at Last", __FUNCTION__);
    		APP_TRACE(szDbgMsg);
        if ((iBuffLen-2) < iLen)
        {
            iLen = (iBuffLen-2);
        }
        memcpy(szCmd, (const char *)&msgBuf[1], iLen);
    }
    else
    {
        strncpy(szCmd, (const char *)&msgBuf[0], iLen);
        memset(szSend, 0, sizeof(szSend));
        szSend[0]=STX;
        memcpy(&szSend[1], (const char *)&msgBuf[0], iBuffLen);
        szSend[iBuffLen+1]= ETX;iBuffLen += 2;
    }
	
	rv = sendUIReqMsg_EX(szSend, iBuffLen);

 	debug_sprintf(szDbgMsg,"%s: ---Returning---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

    return rv;
}

/*
 * ============================================================================
 * Function Name: setEncryptionModeForEmv
 *
 * Description	: This command is used to select the encryption mode to be used during runtime. If PKI is set, VSP
 *				  will be disabled to prevent double encryption during card swipe. If VSP is set, VSP will be
 *				  enabled, and PKI encryption will not be used.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int setEncryptionModeForEmv(ENC_TYPE eEncType)
{
	int				rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	rv = buildEmvCmd(EMV_E10, &stEMVReq, NULL);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;

				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						rv = getUIRespCodeOfEmvCmdResp(EMV_E10, stBLData.iStatus);
						switch(rv)
						{
							case SUCCESS:
									debug_sprintf(szDbgMsg, "%s: E10 command returned [SUCCESS]", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									break;

							default:
								/* 'D' command failed */
								debug_sprintf(szDbgMsg, "%s: E10 command returned Error[%d]",__FUNCTION__,
																						stBLData.iStatus);
								APP_TRACE(szDbgMsg);

								break;
						}
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
					if(bWait == PAAS_FALSE)
					{
						break;
					}
				}

				svcWait(100);
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[E10]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sendD01CmdToEMVAgent
 *
 * Description	:This will help to enable/disable pinbypass for current C30 session
 *
 * Input Params	: PIN_BYPASS_ENABLE/PIN_BYPASS_DISABLE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendD01CmdToEMVAgent(int iPinBypass)
{
	int				rv					= SUCCESS;
#ifdef DEBUG
	char			szDbgMsg[128]		= "";
#endif
	PAAS_BOOL		bWait				= PAAS_TRUE;
	initUIReqMsg(&stEMVReq, INIT_REQ);

	setEmvReqCmd(EMV_D01);

	if(iPinBypass)
	{
		strcpy(stEMVReq.pszBuf,"D011");
	}
	else
	{
		strcpy(stEMVReq.pszBuf,"D010");
	}

	rv = dispatchEmvRequest(stEMVReq.pszBuf, 4);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;

			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_EMV_RESP)
				{
					rv = getUIRespCodeOfEmvCmdResp(EMV_D01, stBLData.iStatus);
					if(rv == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Got Success Command Response Code on D01",__FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid Command Response Code on D01 :%d",__FUNCTION__,rv);
						APP_TRACE(szDbgMsg);
					}
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}
				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				if(bWait == PAAS_FALSE)
				{
					break;
				}
			}

			svcWait(50);
		}
	}
	debug_sprintf(szDbgMsg, "%s: ---Return---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: getEMVCardData
 *
 * Description	: This API would be used to get the card data.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getEMVCardData(EMVDTLS_PTYPE pstEmvDtls, CARDDTLS_PTYPE pstCardDtls,
							char ** szTracks, char *szClrDt, char* szTranStkKey)
{
	int					rv				= SUCCESS;
	int					iLen			= 0;	
	int					iCardSrc		= 0;
	static PAAS_BOOL	bFirstTime		= PAAS_TRUE;	
	PAAS_BOOL			bWait			= PAAS_TRUE;
	CARD_TRK_PTYPE		pstCard			= NULL;	
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif


	debug_sprintf(szDbgMsg, "%s: ---enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		//Initialize XPI Request and Response structures.
		//initEmvTransaction();

		initUIReqMsg(&stEMVReq, INIT_REQ);

		if(bFirstTime == PAAS_TRUE)
		{
			//Send D32 command to set title for swipe card screen.
			rv = setSwipeCardMsgTitle();
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Titles for swipe card screen set",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				bFirstTime = PAAS_FALSE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to set titles for swipe card screen",
																			__FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}
		rv = buildEmvCmd(EMV_C30, &stEMVReq, szTranStkKey);
		if(rv == SUCCESS)
		{
			rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																		__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_DEVICE_APP;
			}
			else
			{
				/* Wait for the user input to get the selected cashback amt */
				while(bWait == PAAS_TRUE)
				{
					if(bBLDataRcvd == PAAS_TRUE)
					{
						acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

						if(stBLData.uiRespType == UI_EMV_RESP)
						{
							rv = getUIRespCodeOfEmvCmdResp(EMV_C30, stBLData.iStatus);							
							switch(rv)
							{
								case SUCCESS:
									debug_sprintf(szDbgMsg, "%s: Received EMV data", __FUNCTION__);
									APP_TRACE(szDbgMsg);				
							
									//Copy data 
									memcpy(pstEmvDtls, &stBLData.stRespDtls.stEmvDtls,
																	sizeof(EMVDTLS_STYPE));	
									break;							

								default:
									/* Do nothing */
									debug_sprintf(szDbgMsg, "%s: C30 command returned Error[%d]", __FUNCTION__,
																								stBLData.iStatus);
									APP_TRACE(szDbgMsg);			
									
									break;		
							}
							bBLDataRcvd = PAAS_FALSE;
							bWait = PAAS_FALSE;
						}
						else if(stBLData.uiRespType == UI_EMV_U00_REQ)
						{
							initUIReqMsg(&stEMVReq, INIT_REQ);
							//Build and dispatch U00 Response command to XPI.
							rv = buildEmvCmd(EMV_U00, &stEMVReq, szTranStkKey);
							if(rv == SUCCESS)
							{
								rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
								if(rv != SUCCESS)
								{
									debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																							__FUNCTION__);
									APP_TRACE(szDbgMsg);

									rv = ERR_DEVICE_APP;
									break;
								}
								bBLDataRcvd = PAAS_FALSE;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: FAILED to build U00 response command",
																					__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
								break;
							}
						}
						else if(stBLData.uiRespType == UI_CARD_RESP)
						{
							if(stBLData.iStatus == SUCCESS)
							{
								pstCard = &(stBLData.stRespDtls.stCrdTrkInfo);

								debug_sprintf(szDbgMsg, "%s: Received the Card Data",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);
								
								/* Track 1 --- */
								iLen = strlen(pstCard->szTrk1);
								if(iLen > 0)
								{
									szTracks[0] = (char *)malloc(iLen + 1);
									memset(szTracks[0], 0x00, iLen + 1);
									memcpy(szTracks[0], pstCard->szTrk1, iLen);												
								}

								/* Track 2 --- */
								iLen = strlen(pstCard->szTrk2);
								if(iLen > 0)
								{
									szTracks[1] = (char *)malloc(iLen + 1);
									memset(szTracks[1], 0x00, iLen + 1);
									memcpy(szTracks[1], pstCard->szTrk2, iLen);
								}

								/* Track 3 --- */
								iLen = strlen(pstCard->szTrk3);
								if(iLen > 0)
								{
									szTracks[2] = (char *)malloc(iLen + 1);
									memset(szTracks[2], 0x00, iLen + 1);
									memcpy(szTracks[2], pstCard->szTrk3, iLen);
								}

								/* Copy clear expiry date*/
								iLen = strlen(pstCard->szClrExpDt);
								if(iLen > 0)
								{
									memset(szClrDt, 0x00, iLen + 1);
									memcpy(szClrDt, pstCard->szClrExpDt, iLen);

									debug_sprintf(szDbgMsg, "%s: Copying Clear Expiry date",
																			__FUNCTION__);
									APP_TRACE(szDbgMsg);

								}
								else
								{
									debug_sprintf(szDbgMsg, "%s: No Clear exp date to copy", 
																			__FUNCTION__);
									APP_TRACE(szDbgMsg);
								}

								/* Card source */
								pstCardDtls->iCardSrc = pstCard->iCardSrc;

								rv = SUCCESS;
							}
							else if(stBLData.iStatus == ERR_BAD_CARD)
							{
								debug_sprintf(szDbgMsg, "%s: Max bad card reads done",
																		__FUNCTION__);
								APP_TRACE(szDbgMsg);
								
								rv = ERR_BAD_CARD;							
							}
							else;
							bBLDataRcvd = PAAS_FALSE;
							bWait = PAAS_FALSE;
						}					
						releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
					}
					svcWait(200);
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed to build command[C30]",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		
		if( (rv == SUCCESS) && ( (pstCardDtls->iCardSrc == CRD_MSR) ||  //Ascii value for R is 77
								 (pstCardDtls->iCardSrc == CRD_RFID) || //Ascii value for R is 82
								 (pstCardDtls->iCardSrc == CRD_NFC) ) ) //Ascii value for  is 78
		{
			debug_sprintf(szDbgMsg, "%s: Card Source [MSR/RFID/NFC]", __FUNCTION__);
			APP_TRACE(szDbgMsg);		
			
			break;
		}	
		else if( rv == SUCCESS)
		{
			/*Note: POS Entry Mode(Card Source)
					01 - Manual entry; 02 - Magnetic stripe read
					05 - Integrated circuit card read; 07 - EMV Contactless
					91 - MSD Contactless
			*/
			iCardSrc = atoi(pstEmvDtls->stEmvtranDlts.szPOSEntryMode);
			/* TODO: Currently EMV contactless is not supported in PS. 
					XPI will give POS entry mode(9F39 tag)value only if EMV contactless is enabled in XPI. 
					For now don't depend on POS entry mode, instead check response code*/
		
			if( iCardSrc == 21) //Chip card is swiped
			{										
				pstCardDtls->iCardSrc = CRD_MSR;				
			}
			else
			{
				//TODO: Uncomment it when EMV contactless is supported in PS.
				#if 0
				if(iCardSrc == 0) //Card souce is not present, Get the card source
				{
					//Get the card source
					rv = processEmvCmd(EMV_C20, pstEmvDtls, szTranStkKey);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to get POS entry Mode(Card Source)", 
																				  __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					iCardSrc = atoi(pstEmvDtls->stEmvtranDlts.szPOSEntryMode);
				}
				#endif
				//Store emv chip card data.
				storeEmvCardData(pstEmvDtls, pstCardDtls);
								
				if(iCardSrc == 7)
				{
					//06 - EMV Contactless				
					pstCardDtls->iCardSrc = CRD_EMV_CTLS;				
				}
				else
				{
					//05 - Integrated circuit card read
					pstCardDtls->iCardSrc = CRD_EMV_CT;
				}
			}			
			break;
		}
		break;			
	}//End of while	
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getEParamsData
 *
 * Description	: This API would retrieve EParams data. EParms is a string of data returned by the VCL module
 * 				  that is used by the host to determine information about the encryption characteristics used by the VCL.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getEParamsData(char* szEparms)
{
	int				rv				= SUCCESS;
	int				iLen			= 0;
	PAAS_BOOL		bWait			= PAAS_TRUE;
	CARD_TRK_PTYPE	pstCard			= NULL;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	rv = buildEmvCmd(EMV_E02, &stEMVReq, NULL);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			while(bWait == PAAS_TRUE)
			{
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						rv = getUIRespCodeOfEmvCmdResp(EMV_E02, stBLData.iStatus);
						switch(rv)
						{
							case SUCCESS:
									debug_sprintf(szDbgMsg, "%s: E02 command returned [SUCCESS]", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									pstCard = &(stBLData.stRespDtls.stCrdTrkInfo);

									/*Eparms data ----*/
									iLen = strlen(pstCard->szEparms);
									if(iLen > 0)
									{
										memcpy(szEparms, pstCard->szEparms, iLen);
									}
									else
									{
										debug_sprintf(szDbgMsg, "%s: No EParams data", __FUNCTION__);
										APP_TRACE(szDbgMsg);
									}
									break;

							default:
								/* 'D' command failed */
								debug_sprintf(szDbgMsg, "%s: E02 command returned Error[%d]",__FUNCTION__,
																						stBLData.iStatus);
								APP_TRACE(szDbgMsg);

								break;
						}
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				svcWait(100);
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[E02]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPKICipheredData
 *
 * Description	: This API would retrieve PKI Ciphered data.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getPKICipheredData(char **szRsaTracks)
{
	int				rv				= SUCCESS;
	int				iLen			= 0;
	PAAS_BOOL		bWait			= PAAS_TRUE;
	CARD_TRK_PTYPE	pstCard			= NULL;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	rv = buildEmvCmd(EMV_E06, &stEMVReq, NULL);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			while(bWait == PAAS_TRUE)
			{
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						rv = getUIRespCodeOfEmvCmdResp(EMV_E06, stBLData.iStatus);
						switch(rv)
						{
							case SUCCESS:
									debug_sprintf(szDbgMsg, "%s: E06 command returned [SUCCESS]", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									pstCard = &(stBLData.stRespDtls.stCrdTrkInfo);

									/*Copy the RSA Track 1/Track2 */
									iLen = strlen(pstCard->szRsaTrk1);
									if(iLen > 0)
									{
										szRsaTracks[0] = (char *)malloc(iLen + 1);
										memset(szRsaTracks[0], 0x00, iLen + 1);
										memcpy(szRsaTracks[0], pstCard->szRsaTrk1, iLen);
									}

									/*Copy the RSA Track 3 if present */
									iLen = strlen(pstCard->szRsaTrk2);
									if(iLen > 0)
									{
										szRsaTracks[1] = (char *)malloc(iLen + 1);
										memset(szRsaTracks[1], 0x00, iLen + 1);
										memcpy(szRsaTracks[1], pstCard->szRsaTrk2, iLen);
									}
									break;

							default:
								/* 'D' command failed */
								debug_sprintf(szDbgMsg, "%s: E06 command returned Error[%d]",__FUNCTION__,
																						stBLData.iStatus);
								APP_TRACE(szDbgMsg);

								break;
						}
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				svcWait(100);
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[E06]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setSwipeCardMsgTitle
 *
 * Description	: This API would get running EMV(XPI App) version information
 *                to be displayed on the screen.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int setSwipeCardMsgTitle()
{
	int				rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;	
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);
	
	rv = buildEmvCmd(EMV_D32, &stEMVReq, NULL);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			while(bWait == PAAS_TRUE)
			{
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						rv = getUIRespCodeOfEmvCmdResp(EMV_D32, stBLData.iStatus);
						switch(rv)
						{
							case SUCCESS:								
									debug_sprintf(szDbgMsg, "%s: D32 command returned [SUCCESS]", __FUNCTION__);
									APP_TRACE(szDbgMsg);				

									break;						

							default:
								/* 'D' command failed */
								debug_sprintf(szDbgMsg, "%s: D32 command returned Error[%d]",__FUNCTION__,
																						stBLData.iStatus);
								APP_TRACE(szDbgMsg);			

								break;		
						}					
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;						
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				svcWait(100);
			}		
		}		
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[D32]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCfgVariableFromEmvApp
 *
 * Description	: This command can be used for reading the value of parameters from iab section.
 *
 * Input Params	: Parameter to read
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getCfgVariableFromEmvApp(char *szParameter)
{
	int				rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	rv = buildEmvCmd(EMV_M40, &stEMVReq, szParameter);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			while(bWait == PAAS_TRUE)
			{
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						rv = getUIRespCodeOfEmvCmdResp(EMV_M40, stBLData.iStatus);
						switch(rv)
						{
							case SUCCESS:
									debug_sprintf(szDbgMsg, "%s: M40 command returned [SUCCESS]", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									break;

							default:
								/* 'D' command failed */
								debug_sprintf(szDbgMsg, "%s: M40 command returned Error[%d]",__FUNCTION__,
																						stBLData.iStatus);
								APP_TRACE(szDbgMsg);

								break;
						}
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				svcWait(100);
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[M40]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setCfgVariableInEmvApp
 *
 * Description	: This command can be used for setting  the value of parameters from iab section.
 *
 * Input Params	: Parameter to set
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int setCfgVariableInEmvApp(char *szParameter)
{
	int				rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stEMVReq, INIT_REQ);

	rv = buildEmvCmd(EMV_M41, &stEMVReq, szParameter);
	if(rv == SUCCESS)
	{
		rv = dispatchEmvRequest(stEMVReq.pszBuf, stEMVReq.iSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			while(bWait == PAAS_TRUE)
			{
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_EMV_RESP)
					{
						rv = getUIRespCodeOfEmvCmdResp(EMV_M41, stBLData.iStatus);
						switch(rv)
						{
							case SUCCESS:
									debug_sprintf(szDbgMsg, "%s: M41 command returned [SUCCESS]", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									break;

							default:
								/* 'D' command failed */
								debug_sprintf(szDbgMsg, "%s: M41 command returned Error[%d]",__FUNCTION__,
																						stBLData.iStatus);
								APP_TRACE(szDbgMsg);

								break;
						}
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				svcWait(100);
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build command[M41]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeRsaEncDetails
 *
 * Description	: This API would be used to store the RSA encryption details into
 *				  card dtls structure.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

int storeRsaEncDetails(char **szRsaTracks, CARDDTLS_PTYPE pstCardDtls)
{
	int				rv				= SUCCESS;
	char 			*pszRSAFingerPrint		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Track Information */
	if(strlen(pstCardDtls->szTrackData) == 0)
	{
		if( (szRsaTracks != NULL) && (szRsaTracks[0] != NULL) )
		{
			strcpy(pstCardDtls->szTrackData, szRsaTracks[0]);

			pszRSAFingerPrint = getRSAFingerPrint();
			strcpy(pstCardDtls->szEncPayLoad, pszRSAFingerPrint);
		}
		else if( (szRsaTracks != NULL) && (szRsaTracks[1] != NULL) )
		{
			strcpy(pstCardDtls->szTrackData, szRsaTracks[1]);

			pszRSAFingerPrint = getRSAFingerPrint();
			strcpy(pstCardDtls->szEncPayLoad, pszRSAFingerPrint);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: RSA track data is not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: TrackData field is already populated with value", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: isCardInserted
 *
 * Description	: This API would be used to check whether the card inserted in the clard slot.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
PAAS_BOOL isCardInserted()
{
	int				rv				= PAAS_FALSE;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif


	debug_sprintf(szDbgMsg, "%s: ---enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = processEmvCmd(EMV_I05, NULL, NULL);

	if(rv == CARD_PRESENT)
	{
		debug_sprintf(szDbgMsg, "%s: Card is present", __FUNCTION__);
		APP_TRACE(szDbgMsg);		
		rv = PAAS_TRUE;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Card is not present", __FUNCTION__);
		APP_TRACE(szDbgMsg);					
	}
		
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: getUIRespCodeOfEmvCmdResp
 *
 * Description	: This API would be used to get mapped UI response code of EMV cmd response.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getUIRespCodeOfEmvCmdResp(ENUM_EMV_CMDS eCmd, int iRespCode)
{
	int				rv						= SUCCESS;
	int				iAppLogEnabled			= isAppLogEnabled();
	char			szAppLogData[300]		= "";
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	debug_sprintf(szDbgMsg, "%s: ---enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*TODO: Return proper response code for particular XPI command using input param eCmd. 
	Since error codes differ for each XPI command.
	*/
	switch(iRespCode)
	{
		case EMV_CMD_SUCCESS:
			debug_sprintf(szDbgMsg, "%s: EMV returned [SUCCESS]", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
			break;

		case EMV_CARD_WAS_SWIPED:
			debug_sprintf(szDbgMsg, "%s: EMV returned [CARD SWIPED]",
														__FUNCTION__);
			APP_TRACE(szDbgMsg);
	
			rv = SUCCESS;	
			break;

		case EMV_RESPONSE_HAS_MORE_PACKETS:
	
			if(eCmd == EMV_S13 || eCmd == EMV_S20)
			{
				debug_sprintf(szDbgMsg, "%s: EMV Process [CANCELLED]",
														__FUNCTION__);
				APP_TRACE(szDbgMsg);
				
				rv = UI_CANCEL_PRESSED;	
				sprintf(szAppLogData, "EMV Processing Cancelled [%d]", iRespCode);
				break;
			}
			//TODO: Check here for other commands.
			debug_sprintf(szDbgMsg, "%s: EMV returned [HAS MORE PACKETS]",
														__FUNCTION__);
			APP_TRACE(szDbgMsg);
			sprintf(szAppLogData, "EMV Has More Packets [%d]", iRespCode);
			rv = SUCCESS;	
			break;
			
		case EMV_ERROR_INVALID_COMMAND_CODE:
		case EMV_ERROR_INVALID_DATA_FORMAT:
		case EMV_ERROR_INVALID_CONFIG:
		case EMV_ERROR_TIMER:
		case EMV_ERROR_COMMUNICATION:
		case EMV_ERROR_READER_FAILED:
		case EMV_ERROR_BAD_CARD:
			if(eCmd == EMV_E02)
			{
				debug_sprintf(szDbgMsg, "%s: EMV returned [EParams Error]",
														__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_DEVICE_APP;
				sprintf(szAppLogData, "Payment Engine Returned EParams Error [%d]", iRespCode);
				break;
			}
			else if( (eCmd == EMV_E06) && (iRespCode == 1))
			{
				debug_sprintf(szDbgMsg, "%s: EMV returned [No encrypted data]",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				sprintf(szAppLogData, "Payment Engine Returned No Encrypted Data [%d]", iRespCode);
				rv = SUCCESS;
				break;
			}
			else if( eCmd == EMV_E06 )
			{
				debug_sprintf(szDbgMsg, "%s: EMV returned [PKI Ciphered Error]",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				sprintf(szAppLogData, "Payment Engine returned PKI Ciphered Error [%d]", iRespCode);
				rv = ERR_DEVICE_APP;
				break;
			}
			debug_sprintf(szDbgMsg, "%s: EMV Process [ERR_BAD_CARD]",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);		
			
			sprintf(szAppLogData, "Payment Engine returned Bad Card Read Error [%d]", iRespCode);
			rv = ERR_BAD_CARD;	
			break;
		
		case EMV_ERROR_TXN_CANCELLED:
			debug_sprintf(szDbgMsg, "%s: EMV Process [CANCELLED]",
													__FUNCTION__);
			APP_TRACE(szDbgMsg);
			sprintf(szAppLogData, "EMV Processing Cancelled [%d]", iRespCode);
			rv = UI_CANCEL_PRESSED;
			break;		

		case EMV_ERROR_CANDIDATE_LISTEMPTY:
			debug_sprintf(szDbgMsg, "%s: Candidate List is Empty", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			sprintf(szAppLogData, "EMV Candidate List is Empty [%d]", iRespCode);
			rv = EMV_ERROR_CANDIDATE_LISTEMPTY;	//MSR Fallback case but we should treat card as Normal Swipe
			break;
			
		case EMV_FALLBACK_TO_MSR:
			debug_sprintf(szDbgMsg, "%s: EMV Fallback to MSR",
													 __FUNCTION__);
			APP_TRACE(szDbgMsg);						
			sprintf(szAppLogData, "EMV Fallback to MSR [%d]", iRespCode);
			rv = EMV_FALLBACK_TO_MSR;	//MSR Fallback case.
			break;
		
		case EMV_ERROR_CARD_NOT_SUPPORTED:
			debug_sprintf(szDbgMsg, "%s: EMV returned [CARD NOT SUPPORTED]",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);	
			sprintf(szAppLogData, "Payment Engine Returned CARD NOT SUPPORTED [%d]", iRespCode);
			rv = ERR_CARD_NOT_IN_INCLUSIONRANGE;
			break;
		
		case EMV_ERROR_CARD_REMOVED:
			debug_sprintf(szDbgMsg, "%s: EMV returned [CARD REMOVED]",
														__FUNCTION__);
			APP_TRACE(szDbgMsg);	
			sprintf(szAppLogData, "Payment Engine Returned CARD REMOVED [%d]", iRespCode);
			rv = EMV_ERROR_CARD_REMOVED;
			break;

		case EMV_ERROR_BLOCKED_CARD:
			debug_sprintf(szDbgMsg, "%s: EMV returned [CARD OR APP BLOCKED]",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
			sprintf(szAppLogData, "Payment Engine Returned CARD OR APP BLOCKED [%d]", iRespCode);
			rv = ERR_CARD_NOT_IN_INCLUSIONRANGE;
			break;
		
		case EMV_ERROR_TIMEOUT:
			debug_sprintf(szDbgMsg, "%s: EMV returned [TIME OUT]",
														__FUNCTION__);
			APP_TRACE(szDbgMsg);	
			sprintf(szAppLogData, "Payment Engine Returned TIME OUT [%d]", iRespCode);
			rv = EMV_ERROR_TIMEOUT;
			break;
			
		case EMV_ERROR_NO_DEBIT_KEYS:
			debug_sprintf(szDbgMsg, "%s: EMV returned [NO DEBIT KEYS]",
														__FUNCTION__);
			APP_TRACE(szDbgMsg);	
			sprintf(szAppLogData, "Payment Engine Returned NO DEBIT KEYS [%d]", iRespCode);
			rv = ERR_NO_KEYS_PRESENT;
			break;

		case EMV_ERROR_CTLS:
			debug_sprintf(szDbgMsg, "%s: EMV CTLS Error ",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			sprintf(szAppLogData, "EMV CTLS Error [%d]", iRespCode);
			rv = ERR_BAD_CARD;
			break;

		case EMV_CTLS_FALLBACK_TO_CT:
			debug_sprintf(szDbgMsg, "%s: EMV CTLS Fallback to CT",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			sprintf(szAppLogData, "EMV CTLS Fallback to CT [%d]", iRespCode);
			rv = EMV_CTLS_FALLBACK_TO_CT;
			break;
			
		case EMV_UNKNOWN_ERROR:
			if(eCmd == EMV_C30)
			{
				debug_sprintf(szDbgMsg, "%s: EMV returned [EMV CONFIGURATION ERROR]",
																		__FUNCTION__);
				APP_TRACE(szDbgMsg);
				sprintf(szAppLogData, "Payment Engine Returned EMV CONFIGURATION ERROR [%d]", iRespCode);
				rv = ERR_CFG_PARAMS;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: EMV returned [FATAL ERROR]",
															__FUNCTION__);
				APP_TRACE(szDbgMsg);			
				sprintf(szAppLogData, "Payment Engine Returned FATAL ERROR [%d]", iRespCode);
				rv = ERR_DEVICE_APP;				
			}
			break;

		default:
			/* Do nothing */
			debug_sprintf(szDbgMsg, "%s: EMV Resp Code[%d] stBLData.istatus[%d]", __FUNCTION__, iRespCode,
														stBLData.iStatus);
			APP_TRACE(szDbgMsg);			
			sprintf(szAppLogData, "EMV ERROR [%d]", iRespCode);
			rv = iRespCode;
			break;		
	}
		
	if(iAppLogEnabled)
	{
		addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
	}
	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	
	return rv;
}
int getEmvReqCmd(char *pszReqCmd)
{
	char 		szReqCmd[3+1]		= "";
	int 		EMVreqCmd			= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

/*	debug_sprintf(szDbgMsg, "%s: ---enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);*/

	memset(szReqCmd, 0x00, sizeof(szReqCmd));

	EMVreqCmd = eReqCmd;

	switch(eReqCmd)	//eReqCmd is global static variable.
	{
	case EMV_C20:
		sprintf(szReqCmd, "%s", "C20");
		break;
	case EMV_C19:
		sprintf(szReqCmd, "%s", "C19");
		break;
	case EMV_C30:
		sprintf(szReqCmd, "%s", "C30");
		break;
	case EMV_C32:
		sprintf(szReqCmd, "%s", "C32");
		break;
	case EMV_C36:
		sprintf(szReqCmd, "%s", "C36");
		break;
	case EMV_C25:
		sprintf(szReqCmd, "%s", "C25");
		break;
	case EMV_C34:
		sprintf(szReqCmd, "%s", "C34");
		break;
	case EMV_I02:
		sprintf(szReqCmd, "%s", "I02");
		break;
	case EMV_S13:
		sprintf(szReqCmd, "%s", "S13");
		break;
	case EMV_S14:
		sprintf(szReqCmd, "%s", "S14");
		break;
	case EMV_S20:
		sprintf(szReqCmd, "%s", "S20");
		break;
	case EMV_S95:
		sprintf(szReqCmd, "%s", "S95");
		break;
	case EMV_S66:
		sprintf(szReqCmd, "%s", "S66");
		break;
	case EMV_I05:
		sprintf(szReqCmd, "%s", "I05");
		break;
	case EMV_U00:
		sprintf(szReqCmd, "%s", "U00");
		break;
	case EMV_E02:
		sprintf(szReqCmd, "%s", "E02");
		break;
	case EMV_E06:
		sprintf(szReqCmd, "%s", "E06");
		break;
	case EMV_E10:
		sprintf(szReqCmd, "%s", "E10");
		break;
	case EMV_S11:
		sprintf(szReqCmd, "%s", "S11");
		break;
	case EMV_D11:
		sprintf(szReqCmd, "%s", "D11");
		break;
	case EMV_D12:
		sprintf(szReqCmd, "%s", "D12");
		break;
	case EMV_D13:
		sprintf(szReqCmd, "%s", "D13");
		break;
	case EMV_D14:
		sprintf(szReqCmd, "%s", "D14");
		break;
	case EMV_D15:
		sprintf(szReqCmd, "%s", "D15");
		break;
	case EMV_D16:
		sprintf(szReqCmd, "%s", "D16");
		break;
	case EMV_D17:
		sprintf(szReqCmd, "%s", "D17");
		break;
	case EMV_D18:
		sprintf(szReqCmd, "%s", "D18");
		break;
	case EMV_D19:
		sprintf(szReqCmd, "%s", "D19");
		break;
	case EMV_D20:
		sprintf(szReqCmd, "%s", "D20");
		break;
	case EMV_D25:
		sprintf(szReqCmd, "%s", "D25");
		break;
	case EMV_D32:
		sprintf(szReqCmd, "%s", "D32");
		break;
	case EMV_D41:
		sprintf(szReqCmd, "%s", "D41");
		break;
	case EMV_Z60:
		sprintf(szReqCmd, "%s", "Z60");
		break;
	case EMV_Z62:
		sprintf(szReqCmd, "%s", "Z62");
		break;
	case EMV_M41:
		sprintf(szReqCmd, "%s", "M41");
		break;
	case EMV_D01:
		sprintf(szReqCmd, "%s", "D01");
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Failed to set request cmd[Unkown command]",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;
	}

	if(strlen(szReqCmd) > 0)
	{
		memset(pszReqCmd, 0x00, sizeof(szReqCmd));
		sprintf(pszReqCmd, "%s", szReqCmd);
	}

/*	debug_sprintf(szDbgMsg, "%s: ---Returning [%s]---", __FUNCTION__, pszReqCmd);
	APP_TRACE(szDbgMsg);*/

	return EMVreqCmd;
}

void setEmvReqCmd(ENUM_EMV_CMDS cmd)
{
	eReqCmd = cmd;
}
/*
 * ============================================================================
 * Function Name: addFieldstoHashTable
 *
 * Description	: This API frames the combination of szKey and AID Node
 * 					to add to hash table
 *
 * Input Params	: AID Node Details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int addFieldstoHashTable(AID_NODE_PTYPE pstAIDNode)
{
	int				rv					= SUCCESS;
	AID_NODE_PTYPE 	pstTmpAIDNode			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

/*	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Adding AID to hash using RID(1st 10 chars) as key which is [%s]", __FUNCTION__, stAIDNode.szRID);
	APP_TRACE(szDbgMsg);*/
	/* Daivik : 4/2/2016 - Coverity 67953 - Large value passed, instead passing by reference */
	pstTmpAIDNode = addEntryToHashTable(pstAIDNode->szRID, pstAIDNode);
	if(pstTmpAIDNode != NULL)
	{
/*		debug_sprintf(szDbgMsg,"%s: Added AID Node to Hash",__FUNCTION__);
		APP_TRACE(szDbgMsg);*/
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: Error while adding to the Hash Table!!!",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
/*	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);*/

	return rv;
}
/*
 * ============================================================================
 * Function Name: addCTLSTagtoHashTable
 *
 * Description	: This API frames the combination of szKey and CTLS Tag Node
 * 					to add to hash table
 *
 * Input Params	: AID Node Details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int addCTLSTagtoHashTable(CTLS_TAG_NODE_STYPE stCTLSTagNode)
{
	int						rv						= SUCCESS;
	CTLS_TAG_NODE_PTYPE 	pstCTLSTagNode			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

/*	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

*/
	pstCTLSTagNode = addCTLSTagEntryToHashTable(stCTLSTagNode.szTag, stCTLSTagNode);
	if(pstCTLSTagNode != NULL)
	{
/*		debug_sprintf(szDbgMsg,"%s: Added CTLS Node to Hash",__FUNCTION__);
		APP_TRACE(szDbgMsg);*/
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: Error while adding to the Hash Table!!!",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
/*	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);*/

	return rv;
}

/*
 * ============================================================================
 * Function Name: generateHashVal
 *
 * Description	: This function is used to calculate the hash value for the given
 * 				  input string
 * 				  This adds each character value in the string to a scrambled
 * 				  combination of the previous ones and returns the remainder
 * 				  modulo the array size. This is not the best possible hash function,
 * 				  but it is short and effective
 *
 * Input Params	: Input String to Calculate Hash
 *
 * Output Params: Hash Value
 * ============================================================================
 */
static unsigned int generateHashVal(char *pszInput)
{
    unsigned int iHashVal = -1;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	if(pszInput == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Input to calculate hash!!! ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iHashVal;
	}

    for (iHashVal = 0; *pszInput != '\0'; pszInput++)
    {
    	iHashVal = *pszInput + (31 * iHashVal);
    }

    //debug_sprintf(szDbgMsg, "%s: iHashVal [%d] ", __FUNCTION__, iHashVal % HASHSIZE);
    //APP_TRACE(szDbgMsg);

    return iHashVal % HASHSIZE;
}

/*
 * ============================================================================
 * Function Name: lookupHashTable
 *
 * Description	: This function is used to lookup hash table for the given
 * 				  input string
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The search is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 * 				  If exact AID is found we will return that otherwise NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
static AID_NODE_PTYPE lookupHashTable(char *pszInPut, char *pszAID)
{
	AID_NODE_PTYPE pstAIDNode = NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

    for (pstAIDNode = hashtab[generateHashVal(pszInPut)]; pstAIDNode != NULL; pstAIDNode = pstAIDNode->next)
    {
        if (strcmp(pszAID, pstAIDNode->szAID) == 0)
        {
        	debug_sprintf(szDbgMsg, "%s: Found Mapping for %s in hash table", __FUNCTION__, pszAID);
        	APP_TRACE(szDbgMsg);

            return pstAIDNode;     // Found
        }
    }

    //debug_sprintf(szDbgMsg, "%s: Did not find Mapping for %s in hash table", __FUNCTION__, pszInPut);
    //APP_TRACE(szDbgMsg);

    return NULL;           // NOT found
}

/*
 * ============================================================================
 * Function Name: lookupHashTableforCTLSTag
 *
 * Description	: This function is used to lookup hash table for the given
 * 				  input string
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The search is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 * 				  If exact CTLS Tag is found we will return that otherwise NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
static CTLS_TAG_NODE_PTYPE lookupHashTableforCTLSTag(char *pszInPut)
{
	CTLS_TAG_NODE_PTYPE pstCTLSTagNode = NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

    for (pstCTLSTagNode = hashtab_ctls_tags[generateHashVal(pszInPut)]; pstCTLSTagNode != NULL; pstCTLSTagNode = pstCTLSTagNode->next)
    {
        if (strcmp(pszInPut, pstCTLSTagNode->szTag) == 0)
        {
        	debug_sprintf(szDbgMsg, "%s: Found Mapping for %s in hash table", __FUNCTION__, pszInPut);
        	APP_TRACE(szDbgMsg);

            return pstCTLSTagNode;     // Found
        }
    }

    //debug_sprintf(szDbgMsg, "%s: Did not find Mapping for %s in hash table", __FUNCTION__, pszInPut);
    //APP_TRACE(szDbgMsg);

    return NULL;           // NOT found
}
/*
 * ============================================================================
 * Function Name: setTermDtlsInHashTable
 *
 * Description	: If any Partial Matching or Full Matching AID is found in AIDList.txt
 * then we need to update the Terminal Details based on the value recieved from EMVTables.ini/CTLSConfig(based on bforEmvCtls)
 *
 * Input Params	: Input String is the AID to search in AID Hash table,TermCap is the Default TermCap
 * from the EMVTables.ini/CTLSConfig.ini, and TACs
 *
 * Output Params: Number of AIDs updated in Hashtable with new Terminal Dtls
 * ============================================================================
 */
static int setTermDtlsInHashTable(PAAS_BOOL bForEmvCtls, char *pszInPut, char *pszTermCap, char *pszTACDef, char *pszTACDen, char *pszTACOnl)
{
	char			szTmp[11]				= "";
	int				iLen					= 0;
	int				iCnt					= 0;
	AID_NODE_PTYPE	pstAIDNode	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszInPut == NULL || strlen(pszInPut) < 10 || (!bForEmvCtls && pszTermCap == NULL) || pszTACDef == NULL || pszTACDen == NULL || pszTACOnl == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Returning NULL as AID is incorrect OR NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iCnt;
	}

	memset(szTmp, 0x00, 11);
	strncpy(szTmp, pszInPut, sizeof(szTmp)-1);

	iLen = strlen(pszInPut);

	for (pstAIDNode = hashtab[generateHashVal(szTmp)]; pstAIDNode != NULL; pstAIDNode = pstAIDNode->next)
    {
    	if (strstr(pstAIDNode->szAID,pszInPut) != NULL)
        {
        	iCnt++;
        	if(bForEmvCtls)
        	{
        		strncpy(pstAIDNode->szTACDefaultCTLS,pszTACDef,sizeof(pstAIDNode->szTACDefaultCTLS)-1);	//Found
        		strncpy(pstAIDNode->szTACDenialCTLS,pszTACDen,sizeof(pstAIDNode->szTACDenialCTLS)-1);
        		strncpy(pstAIDNode->szTACOnlineCTLS,pszTACOnl,sizeof(pstAIDNode->szTACOnlineCTLS)-1);
        		debug_sprintf(szDbgMsg, "%s: Found Mapping for %s in hash table for CTLS as %s, Set TACDef[%s] TACDen[%s] TACOn[%s]", __FUNCTION__,pszInPut, pstAIDNode->szAID,pszTACDef,pszTACDen,pszTACOnl);
        		APP_TRACE(szDbgMsg);
        	}
        	else
        	{
        		strncpy(pstAIDNode->szTermCapDefault,pszTermCap,sizeof(pstAIDNode->szTermCapDefault)-1);     // Found
        		strncpy(pstAIDNode->szTACDefault,pszTACDef,sizeof(pstAIDNode->szTACDefault)-1);
        		strncpy(pstAIDNode->szTACDenial,pszTACDen,sizeof(pstAIDNode->szTACDenial)-1);
        		strncpy(pstAIDNode->szTACOnline,pszTACOnl,sizeof(pstAIDNode->szTACOnline)-1);
        		debug_sprintf(szDbgMsg, "%s: Found Mapping for %s in hash table as %s, Set TermCap[%s] TACDef[%s] TACDen[%s] TACOn[%s]", __FUNCTION__,pszInPut, pstAIDNode->szAID,pszTermCap,pszTACDef,pszTACDen,pszTACOnl);
        		APP_TRACE(szDbgMsg);
        	}
        }
    }

    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    return iCnt;
}
/*
 * ============================================================================
 * Function Name: lookupHashTableForPartialMatching
 *
 * Description	: If an AID is found with patial matchingwe will
 * 					return that otherwise NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
static AID_NODE_PTYPE lookupHashTableForPartialMatching(char *pszInPut)
{
	char			szTmp[11]				= "";
	int				iLen					= 0;
	AID_NODE_PTYPE	pstAIDNode	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	if(pszInPut == NULL || strlen(pszInPut) < 10)
	{
		debug_sprintf(szDbgMsg, "%s: Returning NULL as AID is incorrect", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return NULL;
	}

	memset(szTmp, 0x00, 11);
	strncpy(szTmp, pszInPut, 10);

	iLen = strlen(pszInPut);

	for (pstAIDNode = hashtab[generateHashVal(szTmp)]; pstAIDNode != NULL; pstAIDNode = pstAIDNode->next)
    {
    	if (	((strlen(pstAIDNode->szAID)) <= iLen) &&
        		((strstr(pszInPut, pstAIDNode->szAID)) != NULL)
        		)
        {
        	debug_sprintf(szDbgMsg, "%s: Found Mapping for %s in hash table as %s", __FUNCTION__, pszInPut, pstAIDNode->szAID);
        	APP_TRACE(szDbgMsg);

            return pstAIDNode;     // Found
        }
    }

    //debug_sprintf(szDbgMsg, "%s: Did not find Mapping for %s in hash table", __FUNCTION__, pszInPut);
    //APP_TRACE(szDbgMsg);

    return NULL;           // NOT found
}
/*
 * ============================================================================
 * Function Name: addEntryToHashTable
 *
 * Description	: This function is used to add entry to the hash table
 *
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The search is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
static AID_NODE_PTYPE addEntryToHashTable(char *pszSetName, AID_NODE_PTYPE pstNewAIDNode)
{
	AID_NODE_PTYPE pstAIDNode;
    unsigned int hashval;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif


    if ((pstAIDNode = lookupHashTable(pszSetName, pstNewAIDNode->szAID)) == NULL)        // NOT found
    {
    	pstAIDNode = (AID_NODE_PTYPE)malloc(sizeof(AID_NODE_STYPE));

    	if (pstAIDNode == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while creating memory for AID Hash list ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return NULL;
		}

    	memset(pstAIDNode, 0x00, sizeof(AID_NODE_STYPE));

        hashval = generateHashVal(pszSetName); //Generating the Hash Value

        memcpy(pstAIDNode, pstNewAIDNode, sizeof(AID_NODE_STYPE));
        pstAIDNode->next = hashtab[hashval];

        hashtab[hashval] = pstAIDNode;

    }
    else // Already there
    {
    	debug_sprintf(szDbgMsg, "%s: Already present in Hash List Not Adding Again", __FUNCTION__);
    	APP_TRACE(szDbgMsg);
	   	return NULL;
    }
    return pstAIDNode;
}

/*
 * ============================================================================
 * Function Name: addCTLSTagEntryToHashTable
 *
 * Description	: This function is used to add entry to the hash table
 *
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The search is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
static CTLS_TAG_NODE_PTYPE addCTLSTagEntryToHashTable(char *pszSetName, CTLS_TAG_NODE_STYPE stCTLSTagNode)
{
	CTLS_TAG_NODE_PTYPE pstCTLSTagNode;
    unsigned int hashval;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif


    if ( (pstCTLSTagNode = lookupHashTableforCTLSTag(pszSetName)) == NULL)        // NOT found
    {
    	pstCTLSTagNode = (CTLS_TAG_NODE_PTYPE)malloc(sizeof(CTLS_TAG_NODE_STYPE));

    	if (pstCTLSTagNode == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while creating memory for CTLS Tag Hash list ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return NULL;
		}

    	memset(pstCTLSTagNode, 0x00, sizeof(CTLS_TAG_NODE_STYPE));

        hashval = generateHashVal(pszSetName); //Generating the Hash Value

        memcpy(pstCTLSTagNode, &stCTLSTagNode, sizeof(CTLS_TAG_NODE_STYPE));
        pstCTLSTagNode->next = hashtab_ctls_tags[hashval];

        hashtab_ctls_tags[hashval] = pstCTLSTagNode;

    }
    else // Already there
    {
    	debug_sprintf(szDbgMsg, "%s: Already present in Hash List Not Adding Again", __FUNCTION__);
    	APP_TRACE(szDbgMsg);
	   	return NULL;
    }
    return pstCTLSTagNode;
}
/*
 * ============================================================================
 * Function Name: getEMVDtlsfromAID
 *
 * Description	: This function is used to update payment media and type from hash
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
int getEMVDtlsfromAID(CARDDTLS_PTYPE pstCardDtls, int *iPymtType)
{
	int				rv					= SUCCESS;
	AID_NODE_PTYPE 	pstAIDNode			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstAIDNode = lookupHashTableForPartialMatching(pstCardDtls->stEmvAppDtls.szAID);
	if(pstAIDNode != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Read AID is %s pymttype: %d pymtmedia %s TACDef %s TACDen %s TACOnline %s  Cashback:%d CardAbbrv %s DefaultTermCap:%s",
		__FUNCTION__, pstAIDNode->szAID, pstAIDNode->iPymtType, pstAIDNode->szPymtMedia, pstAIDNode->szTACDefault, pstAIDNode->szTACDenial, pstAIDNode->szTACOnline,pstAIDNode->iCashbackEnabled, pstAIDNode->szCardAbbrv,pstAIDNode->szTermCapDefault);
		APP_TRACE(szDbgMsg);

		if(pstAIDNode->iPymtType != -1)
		{
			if(pstAIDNode->iPymtType == 2)
			{
				/* Daivik:6/6/2016 - Return Paymen type as Debit when we have configured the AID to toggle.
				 * Only when all toggle validations pass, we will be setting the transaction as Credit. Untill then
				 * this transaction will be treated as DEBIT.
				 */
				*iPymtType = PYMT_DEBIT;
				pstCardDtls->bTogglePymtType = PAAS_TRUE;
			}
			else
			{
				*iPymtType = pstAIDNode->iPymtType;
			}
		}

		if( strlen(pstAIDNode->szPymtMedia) > 0)
		{
			strcpy(pstCardDtls->szPymtMedia, pstAIDNode->szPymtMedia);
		}

		if( strlen(pstAIDNode->szCardAbbrv) > 0)
		{
			strcpy(pstCardDtls->szCardAbbrv, pstAIDNode->szCardAbbrv);
		}

		pstCardDtls->stEmvtranDlts.iCashbackEnabled = pstAIDNode->iCashbackEnabled;

		if(pstCardDtls->iCardSrc == CRD_EMV_CTLS)
		{
			if( strlen(pstAIDNode->szTACDefault) > 0)
			{
				strcpy(pstCardDtls->stEmvTerDtls.szTACDefault, pstAIDNode->szTACDefaultCTLS);
			}

			if( strlen(pstAIDNode->szTACDenial) > 0)
			{
				strcpy(pstCardDtls->stEmvTerDtls.szTACDenial, pstAIDNode->szTACDenialCTLS);
			}

			if( strlen(pstAIDNode->szTACOnline) > 0)
			{
				strcpy(pstCardDtls->stEmvTerDtls.szTACOnline, pstAIDNode->szTACOnlineCTLS);
			}
		}
		else
		{
			if( strlen(pstAIDNode->szTACDefault) > 0)
			{
				strcpy(pstCardDtls->stEmvTerDtls.szTACDefault, pstAIDNode->szTACDefault);
			}

			if( strlen(pstAIDNode->szTACDenial) > 0)
			{
				strcpy(pstCardDtls->stEmvTerDtls.szTACDenial, pstAIDNode->szTACDenial);
			}

			if( strlen(pstAIDNode->szTACOnline) > 0)
			{
				strcpy(pstCardDtls->stEmvTerDtls.szTACOnline, pstAIDNode->szTACOnline);
			}

			if(strlen(pstAIDNode->szTermCapDefault) > 0)
			{
				strncpy(pstCardDtls->stEmvTerDtls.szTermCapDefault, pstAIDNode->szTermCapDefault,sizeof(pstCardDtls->stEmvTerDtls.szTermCapDefault)-1);
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: No such AID[%s] in Hash table",__FUNCTION__, pstCardDtls->stEmvAppDtls.szAID);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setPreferredDtlsForAID
 *
 * Description	: This function is used to update Term Cap and Threshold
 *
 * Input Params	: Input Strings
 *
 * Output Params: result code
 * ============================================================================
 */
int setPreferredDtlsForAID(char *pszAID, char *pszPrefTermCap, double fThreshold)
{
	int				rv					= SUCCESS;
	char			szKey[11]			= "";
	AID_NODE_PTYPE 	pstAIDNode			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszAID == NULL || pszPrefTermCap == NULL)
	{
		debug_sprintf(szDbgMsg,"%s: NULL params Passed",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	strncpy(szKey, pszAID, 10);

	pstAIDNode = lookupHashTable(szKey, pszAID);
	if(pstAIDNode != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Updating Threshold[%lf]  PrefTermCap[%s]  for AID[%s]", __FUNCTION__, fThreshold, pszPrefTermCap, pszAID);
		APP_TRACE(szDbgMsg);

		strcpy(pstAIDNode->szTermCapPreferred, pszPrefTermCap);
		pstAIDNode->fThreshold = fThreshold;
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: No such AID[%s] in Hash table",__FUNCTION__, pszAID);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPreferredDtlsForAID
 *
 * Description	: This function is used to get Term Cap and Threshold
 *
 * Input Params	: Input Strings
 *
 * Output Params: result code
 * ============================================================================
 */
int getPreferredDtlsForAID(char *pszAID, char *pszPrefTermCap, double *fThreshold)
{
	int				rv					= SUCCESS;
	AID_NODE_PTYPE 	pstAIDNode			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszAID == NULL || pszPrefTermCap == NULL)
	{
		debug_sprintf(szDbgMsg,"%s: NULL params Passed",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	pstAIDNode = lookupHashTableForPartialMatching(pszAID);
	if(pstAIDNode != NULL)
	{
		strcpy(pszPrefTermCap, pstAIDNode->szTermCapPreferred);
		*fThreshold = pstAIDNode->fThreshold;
		debug_sprintf(szDbgMsg, "%s: Got Threshold[%lf]  PrefTermCap[%s]  for AID[%s]", __FUNCTION__, *fThreshold, pszPrefTermCap, pszAID);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: No such AID[%s] in Hash table",__FUNCTION__, pszAID);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSignLimitForAID
 *
 * Description	: This function is used to get Sig Limit For passed AID
 *
 * Input Params	: Input Strings
 *
 * Output Params: result code
 * ============================================================================
 */
int getSignLimitForAID(char *pszAID, double *fSigLimit)
{
	int				rv					= SUCCESS;
	AID_NODE_PTYPE 	pstAIDNode			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszAID == NULL || fSigLimit == NULL)
	{
		debug_sprintf(szDbgMsg,"%s: NULL params Passed",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	pstAIDNode = lookupHashTableForPartialMatching(pszAID);
	if(pstAIDNode != NULL)
	{
		*fSigLimit = pstAIDNode->fSigLimit;
		debug_sprintf(szDbgMsg, "%s: Got SigLimit[%lf]  for AID[%s]", __FUNCTION__, *fSigLimit, pszAID);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: No such AID[%s] in Hash table",__FUNCTION__, pszAID);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isSigReqAfterPINBypass
 *
 * Description	: This function is used to check whether signature is needed
 * 					if PIN is bypassed for the given AID
 *
 * Input Params	: Input Strings
 *
 * Output Params: result code
 * ============================================================================
 */
PAAS_BOOL isSigReqAfterPINBypass(char *pszAID)
{
	PAAS_BOOL		bSigReq				= PAAS_FALSE;
	AID_NODE_PTYPE 	pstAIDNode			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszAID == NULL)
	{
		debug_sprintf(szDbgMsg,"%s: NULL params Passed; Returning False",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		bSigReq = PAAS_FALSE;
	}
	else
	{
		pstAIDNode = lookupHashTableForPartialMatching(pszAID);
		if(pstAIDNode != NULL)
		{
			if( pstAIDNode->iCapSignOnPINBypass == 1)
			{
				bSigReq = PAAS_TRUE;
			}
			else
			{
				bSigReq = PAAS_FALSE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: No such AID[%s] in Hash table;Returning False",__FUNCTION__, pszAID);
			APP_TRACE(szDbgMsg);
			bSigReq = PAAS_FALSE;
		}
	}
	return bSigReq;
}
#if 0
/*
 * ============================================================================
 * Function Name: getD25CmdReq
 *
 * Description	:
 *
 * Output Params:
 * ============================================================================
 */
void getD25CmdReq(char *szCmd, int *len, EMV_OTHER_REC_PTYPE pstOtherRec)
{
	getD_25Command(szCmd, len, pstOtherRec);
}
#endif
/*
 * ============================================================================
 * Function Name: updateINIFromNode
 *
 * Description	: This API would update emv INI files from a single host node
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateINIFromNode(EMVKEYLOAD_NODE_PTYPE pstEmvKeyLoadNode, dictionary * dictEMVTables, dictionary * dictCAPKInfo, PAAS_BOOL *bPubKeysPresent, char * pszSupportedSchemes)
{
	int									rv						= SUCCESS;
	FALLBACK_PTYPE 						pstFallBckIndicator		= NULL;
	OFFLINE_FLR_LIMIT_PTYPE				pstOfflineFlrLmt		= NULL;
	EMV_CAPK_INFO_PTYPE 				pstCAPKInfo 			= NULL;
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{

		if(pstEmvKeyLoadNode->emvKeyLoadType == FALLBACK_INDICATOR && dictEMVTables != NULL)
		{
			pstFallBckIndicator	= (FALLBACK_PTYPE) pstEmvKeyLoadNode->emvData;

			rv = updateFallbackFromHost(pstFallBckIndicator, dictEMVTables);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to update FallbackValues in INI",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}
		else if(pstEmvKeyLoadNode->emvKeyLoadType == OFFLINE_FLOOR_LIMIT && dictEMVTables != NULL)
		{
			pstOfflineFlrLmt	= (OFFLINE_FLR_LIMIT_PTYPE) pstEmvKeyLoadNode-> emvData;

			rv = updateOfflineFloorLimit(pstOfflineFlrLmt, dictEMVTables);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to updateOfflineFloorLimit in INI",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
#if 0
			/* check whether already node present with given APPID*/
			tmpKeyLoadLst = findNodeForAppId(pstOfflineFlrLmt->szAppID, headKeyLoadLst);
			if(tmpKeyLoadLst != NULL)
			{
				memset(szTmp, 0x00, strlen(szTmp));
				memcpy(szTmp, (char*)pstOfflineFlrLmt->szOffFlrLmt + 2, 4);
				memcpy(szTmp + 4, (char*)pstOfflineFlrLmt->szOffFlrLmt + 7, 2);

				memcpy(tmpKeyLoadLst->stMvtRecInfo.szFloorLimit, szTmp, 6);

				memset(szTmp, 0x00, strlen(szTmp));
				memcpy(szTmp, (char*)pstOfflineFlrLmt->szOffFlrLmtThrshld + 2, 4);
				memcpy(szTmp + 4, (char*)pstOfflineFlrLmt->szOffFlrLmtThrshld + 7, 2);

				memcpy(tmpKeyLoadLst->stMvtRecInfo.szRSThreshold, szTmp, 6);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Value not stored,Matching APPID doesn't exists",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
#endif
		}
		else if(pstEmvKeyLoadNode->emvKeyLoadType == PUBLIC_KEY && dictCAPKInfo != NULL)
		{
			pstCAPKInfo	= (EMV_CAPK_INFO_PTYPE) pstEmvKeyLoadNode-> emvData;

			rv = updateCAPKInfo(pstCAPKInfo, dictCAPKInfo, pszSupportedSchemes);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to updateCAPKInfo in INI",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			*bPubKeysPresent = PAAS_TRUE;
		}
		break; //exit from the while loop
	}
	debug_sprintf(szDbgMsg, "%s: ---Returning---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);


	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: updateEMViniFiles
 *
 * Description	: This API would update emv INI files
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateEMViniFiles(EMVKEYLOAD_INFO_PTYPE pstEmvKeyLoadInfo)
{
	extern	int					errno;
	int 						rv 						= SUCCESS;
	int							iAppLogEnabled			= isAppLogEnabled();
	char						szAppLogData[300]		= "";
	PAAS_BOOL					bEmvTablePresent		= PAAS_TRUE;
	PAAS_BOOL					bCAPKPresent			= PAAS_TRUE;
	EMVKEYLOAD_NODE_PTYPE		pstEmvKeyLoadNode		= NULL;
	dictionary	*				dictEMVTables			= NULL;
	dictionary	*				dictCAPKInfo			= NULL;
	FILE	*					fCapK					= NULL;
	FILE	*					fEmv					= NULL;
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	if(doesFileExist(EMV_TABLES_FILE_PATH) != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: File %s Not Present skipping the updatation of File", __FUNCTION__, EMV_TABLES_FILE_PATH);
		APP_TRACE(szDbgMsg);
		bEmvTablePresent = PAAS_FALSE;
	}
	else
	{
		rv = checkAndCorrectFileFormat(EMV_TABLES_FILE_PATH);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to improve Format %s ", __FUNCTION__, EMV_TABLES_FILE_PATH);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Loading EMV Config Files");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
			return rv;
		}
	}
	if(doesFileExist(CAPK_DATA_INI_FILE_PATH) != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: File %s Not Present skipping the updatation of File", __FUNCTION__, CAPK_DATA_INI_FILE_PATH);
		APP_TRACE(szDbgMsg);
		bCAPKPresent = PAAS_FALSE;
	}
	else
	{
		rv = checkAndCorrectFileFormat(CAPK_DATA_INI_FILE_PATH);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to improve Format %s ", __FUNCTION__, CAPK_DATA_INI_FILE_PATH);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Loading EMV Config Files");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
			return rv;
		}
	}

	if(bCAPKPresent == PAAS_FALSE && bEmvTablePresent == PAAS_FALSE)
	{
		debug_sprintf(szDbgMsg, "%s: No EMV ini File Present to update => skippng updation", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = SUCCESS;
		return rv;
	}
	if(bEmvTablePresent)
	{
		dictEMVTables = iniparser_load(EMV_TABLES_FILE_PATH);
		if(dictEMVTables == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: iniparser_load error; file [%s] could not be opened!, errno=%d [%s]", __FUNCTION__, EMV_TABLES_FILE_PATH, errno, strerror(errno));
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Loading EMV Config Files");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
			return rv;
		}
	}

	if(bCAPKPresent)
	{
		dictCAPKInfo = iniparser_load(CAPK_DATA_INI_FILE_PATH);
		if(dictCAPKInfo == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: iniparser_load error; file [%s] could not be opened!, errno=%d [%s]", __FUNCTION__, CAPK_DATA_INI_FILE_PATH, errno, strerror(errno));
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Loading EMV Config Files");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
			return rv;
		}
	}
	pstEmvKeyLoadNode = pstEmvKeyLoadInfo->mainLstHead;
	while(pstEmvKeyLoadNode != NULL)
	{
		/* update Ini files node by node */
		rv = updateINIFromNode(pstEmvKeyLoadNode, dictEMVTables, dictCAPKInfo);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed update node", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		if(pstEmvKeyLoadNode == pstEmvKeyLoadInfo->mainLstTail)
		{
			//Reached and processed last Node
			break;
		}
		/*check next node */
		pstEmvKeyLoadNode = pstEmvKeyLoadNode->nextEmvNode;
	}

	if(dictCAPKInfo != NULL)
	{
		fCapK = fopen(CAPK_DATA_INI_FILE_PATH, "w");
		if(fCapK == NULL)
		{
			rv = FAILURE;
		}
		else
		{
			iniparser_dump_ini(dictCAPKInfo, fCapK);
			fclose(fCapK);
		}
		iniparser_freedict(dictCAPKInfo);
	}
	if(dictEMVTables != NULL)
	{
		fEmv = fopen(EMV_TABLES_FILE_PATH, "w");
		if(fEmv == NULL)
		{
			rv = FAILURE;
		}
		else
		{
			iniparser_dump_ini(dictEMVTables, fEmv);
			fclose(fEmv);
		}
		iniparser_freedict(dictEMVTables);
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: updateEMViniFiles
 *
 * Description	: This API would update emv INI files
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateEMViniFiles(EMVKEYLOAD_INFO_PTYPE pstEmvKeyLoadInfo, char *pszErrMsg)
{
	extern	int					errno;
	int 						rv 						= SUCCESS;
	int							iAppLogEnabled			= isAppLogEnabled();
	char						szAppLogData[300]		= "";
	char						szSupportedSchemes[512]	= "";
	PAAS_BOOL					bEmvTablePresent		= PAAS_TRUE;
	PAAS_BOOL					bPubKeysPresent			= PAAS_FALSE;
	EMVKEYLOAD_NODE_PTYPE		pstEmvKeyLoadNode		= NULL;
	dictionary	*				dictEMVTables			= NULL;
	dictionary	*				dictCAPKInfo			= NULL;
	FILE	*					fCapK					= NULL;
	FILE	*					fEmv					= NULL;
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	if(doesFileExist(EMV_TABLES_FILE_PATH) != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: File %s Not Present skipping the updatation of this File", __FUNCTION__, EMV_TABLES_FILE_PATH);
		APP_TRACE(szDbgMsg);
		bEmvTablePresent = PAAS_FALSE;
	}
	else
	{
		rv = checkAndCorrectFileFormat(EMV_TABLES_FILE_PATH);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to improve Format %s ", __FUNCTION__, EMV_TABLES_FILE_PATH);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Loading EMV Config Files");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
			sprintf(pszErrMsg, "%s: %s",pszErrMsg, EMV_TABLES_FILE_PATH);
			rv = FAILURE;
			return rv;
		}
	}
	if(bEmvTablePresent)
	{
		dictEMVTables = iniparser_load(EMV_TABLES_FILE_PATH);
		if(dictEMVTables == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: iniparser_load error; file [%s] could not be opened!, errno=%d [%s]", __FUNCTION__, EMV_TABLES_FILE_PATH, errno, strerror(errno));
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Loading EMV Config Files");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
			sprintf(pszErrMsg, "%s: %s", pszErrMsg, EMV_TABLES_FILE_PATH);
			rv = FAILURE;
			return rv;
		}
	}

	//Creating a new CAPKData File
	fCapK = fopen(TEMP_CAPK_DATA_INI_FILE_PATH, "w");
	if(fCapK == NULL)
	{
		/* Unable to Create new file */
		debug_sprintf(szDbgMsg, "%s: Unable to Create New CAPKData File", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Unable to Create a New EMV File");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
		}
		rv = FAILURE;
		return rv;
	}
	fclose(fCapK);

	dictCAPKInfo = iniparser_load(TEMP_CAPK_DATA_INI_FILE_PATH);
	if(dictCAPKInfo == NULL)
	{
		/* Unable to load the ini file */
		debug_sprintf(szDbgMsg, "%s: iniparser_load error; file [%s] could not be opened!, errno=%d [%s]", __FUNCTION__, CAPK_DATA_INI_FILE_PATH, errno, strerror(errno));
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error while Loading EMV Config Files");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
		}
		getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
		sprintf(pszErrMsg, "%s: %s",pszErrMsg, TEMP_CAPK_DATA_INI_FILE_PATH);
		rv = FAILURE;
		return rv;
	}
	iniparser_set(dictCAPKInfo,"CAPKFiles", NULL);

	memset(szSupportedSchemes, 0x00, sizeof(szSupportedSchemes));
	rv = getSupportedRIDSchemes(dictEMVTables, szSupportedSchemes);
	if(rv != SUCCESS)
	{
		/* Unable to load the ini file */
		debug_sprintf(szDbgMsg, "%s: Error while Reading Supported RID Schemes", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error while Loading/Reading EMV Config Files");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
		}
		rv = FAILURE;
		return rv;
	}

	pstEmvKeyLoadNode = pstEmvKeyLoadInfo->mainLstHead;
	while(pstEmvKeyLoadNode != NULL)
	{
		/* update Ini files node by node */
		rv = updateINIFromNode(pstEmvKeyLoadNode, dictEMVTables, dictCAPKInfo, &bPubKeysPresent, szSupportedSchemes);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed  to update node", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		if(pstEmvKeyLoadNode == pstEmvKeyLoadInfo->mainLstTail)
		{
			//Reached and processed last Node
			break;
		}
		/*check next node */
		pstEmvKeyLoadNode = pstEmvKeyLoadNode->nextEmvNode;
	}

	if(dictCAPKInfo != NULL)
	{
		fCapK = fopen(TEMP_CAPK_DATA_INI_FILE_PATH, "w");
		if(fCapK == NULL)
		{
			//CID 67401 (#1 of 1): Unused value (UNUSED_VALUE). T_RaghavendranR1. Commented the below stmt and added log in case of failed to open the file.
			//rv = FAILURE;
			debug_sprintf(szDbgMsg, "%s: Failed to open [%s[ ", __FUNCTION__, TEMP_CAPK_DATA_INI_FILE_PATH);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			iniparser_dump_ini(dictCAPKInfo, fCapK);
			fclose(fCapK);
		}
		iniparser_freedict(dictCAPKInfo);

		rv = checkAndCorrectFileFormat(TEMP_CAPK_DATA_INI_FILE_PATH);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to improve Format %s ", __FUNCTION__, TEMP_CAPK_DATA_INI_FILE_PATH);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error while Loading EMV Config Files");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
			sprintf(pszErrMsg, "%s: %s",pszErrMsg, TEMP_CAPK_DATA_INI_FILE_PATH);
			rv = FAILURE;
			return rv;
		}
	}
	if(dictEMVTables != NULL)
	{
		fEmv = fopen(EMV_TABLES_FILE_PATH, "w");
		if(fEmv == NULL)
		{
			rv = FAILURE;
		}
		else
		{
			iniparser_dump_ini(dictEMVTables, fEmv);
			fclose(fEmv);
		}
		iniparser_freedict(dictEMVTables);

	}
	//If Host did not send any public keys, we should delete the temp CAPKData file which we are creating it in the starting
	if(bPubKeysPresent == PAAS_FALSE)
	{
		if(remove(TEMP_CAPK_DATA_INI_FILE_PATH) != SUCCESS) // CID 67299 (#1 of 1): Unchecked return value from library (CHECKED_RETURN) T_RaghavendranR1
		{
			debug_sprintf(szDbgMsg, "%s: Remove File [%s] Failed ", __FUNCTION__, TEMP_CAPK_DATA_INI_FILE_PATH);
			APP_TRACE(szDbgMsg);
		}

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "No Public Keys Send by Host During EMV Initialization");
			addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
		}
	}
	else
	{
		if(remove(CAPK_DATA_INI_FILE_PATH) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Remove File [%s] Failed ", __FUNCTION__, CAPK_DATA_INI_FILE_PATH);
			APP_TRACE(szDbgMsg);
		}

		if(rename(TEMP_CAPK_DATA_INI_FILE_PATH, CAPK_DATA_INI_FILE_PATH) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Rename File [%s] to [%s] Failed ", __FUNCTION__, TEMP_CAPK_DATA_INI_FILE_PATH, CAPK_DATA_INI_FILE_PATH);
			APP_TRACE(szDbgMsg);
		}
		debug_sprintf(szDbgMsg, "%s: Created %s successfully ", __FUNCTION__, CAPK_DATA_INI_FILE_PATH);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSupportedRIDSchemes
 *
 * Description	: This API will get all supported EMV schemes in a string
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getSupportedRIDSchemes(dictionary *dict, char *pszSupportedSchemes)
{
	int					rv					= 0;
	int					iCount				= 0;
	int					MAXAID				= 6;
	char				szKey[100]			= "";
	char				szVal[100]			= "";
	char				*value				= NULL;
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif


	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCount = 1; iCount <= MAXAID; iCount++)
	{
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s%d", "supportedschemes", "scheme", iCount);

		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			memset(szVal, 0x00, sizeof(szVal));
			strcpy(szVal, value);
			debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
			APP_TRACE(szDbgMsg);

			if(strcasecmp(szVal, "VISA") == 0)
			{
				strcat(pszSupportedSchemes, "A000000003");
				strcat(pszSupportedSchemes, "A000000098");
			}
			else if(strcasecmp(szVal, "MASTERCARD") == 0)
			{
				strcat(pszSupportedSchemes, "A000000004");
			}
			else if(strcasecmp(szVal, "INTERAC") == 0)
			{
				strcat(pszSupportedSchemes, "A000000277");
			}
			else if(strcasecmp(szVal, "AMEX") == 0)
			{
				strcat(pszSupportedSchemes, "A000000025");
			}
			else if(strcasecmp(szVal, "JCB") == 0)
			{
				strcat(pszSupportedSchemes, "A000000065");
			}
			else if(strcasecmp(szVal, "DISCOVER") == 0)
			{
				strcat(pszSupportedSchemes, "A000000152");
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: ERROR: Got Unknown Scheme [%s] Increasing MAXAID variable by one",__FUNCTION__, szVal);
				APP_TRACE(szDbgMsg);
				MAXAID++;
			}
		}
		else
		{
			break;
		}
	}
	debug_sprintf(szDbgMsg, "%s: Read [%d] Supported Schemes: [%s]",__FUNCTION__, iCount - 1, pszSupportedSchemes);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: ---Returning [%d]----", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: copyAndRenameEMVINIFiles
 *
 * Description	: This function will copy and rename four INI files
 *
 * Output Params:
 * ============================================================================
 */
void copyAndRenameEMVINIFiles()
{
	char szCmd[100] = "";

#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szCmd, 0x00, sizeof(szCmd));
	sprintf(szCmd, "%s %s %s", "cp", CTLS_INI_FILE_PATH, TEMP_CTLS_INI_FILE_PATH);
	local_svcSystem(szCmd);

	memset(szCmd, 0x00, sizeof(szCmd));
	sprintf(szCmd, "%s %s %s", "cp", EMV_TABLES_FILE_PATH, TEMP_EMV_TABLES_FILE_PATH);
	local_svcSystem(szCmd);

	memset(szCmd, 0x00, sizeof(szCmd));
	sprintf(szCmd, "%s %s %s", "cp", CAPK_DATA_INI_FILE_PATH, TEMP_CAPK_DATA_INI_FILE_PATH);
	local_svcSystem(szCmd);

	memset(szCmd, 0x00, sizeof(szCmd));
	sprintf(szCmd, "%s %s %s", "cp", OPT_FLAG_FILE_PATH, TEMP_OPT_FLAG_FILE_PATH);
	local_svcSystem(szCmd);

	debug_sprintf(szDbgMsg, "%s: ---Returning---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: RenametempEMVINIFiles
 *
 * Description	: This function will rename temp four emv ini files
 *
 * Output Params:
 * ============================================================================
 */
void renametempEMVINIFiles()
{
	char szCmd[100] = "";

#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szCmd, 0x00, sizeof(szCmd));
	sprintf(szCmd, "%s %s %s", "mv", TEMP_CTLS_INI_FILE_PATH, CTLS_INI_FILE_PATH);
	local_svcSystem(szCmd);

	memset(szCmd, 0x00, sizeof(szCmd));
	sprintf(szCmd, "%s %s %s", "mv", TEMP_EMV_TABLES_FILE_PATH, EMV_TABLES_FILE_PATH);
	local_svcSystem(szCmd);

	memset(szCmd, 0x00, sizeof(szCmd));
	sprintf(szCmd, "%s %s %s", "mv", TEMP_CAPK_DATA_INI_FILE_PATH, CAPK_DATA_INI_FILE_PATH);
	local_svcSystem(szCmd);

	memset(szCmd, 0x00, sizeof(szCmd));
	sprintf(szCmd, "%s %s %s", "mv", TEMP_OPT_FLAG_FILE_PATH, OPT_FLAG_FILE_PATH);
	local_svcSystem(szCmd);

	debug_sprintf(szDbgMsg, "%s: ---Returning---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: isCVMAvailableInCard
 *
 * Description	: This function tells us if there is one match between terminal
 * 					capabilities applied are part of the CVM list
 *
 * Input Params	: CHAR* terminal capabilities(9F33) and CARD CVM LIST(8E)
 *
 * Output Params: BOOL
 * ============================================================================
 */
PAAS_BOOL isCVMAvailableInCard(char* pszTermCapab, char* pszCardCVMLst, double ftransAmnt, double fCashbackAmnt, PAAS_BOOL bPredictCVM)
{
	int			iCVMs					= 0;
	int			iCount					= 0;
	double		fAmountX				= 0;
	double		fAmountY				= 0;
	int			iCVMCondCode			= -1;
	int			iCVMByte0				= 0x00;
	char		iTermCVMs				= 0;
	char		iCardCVMs				= 0;
	char		szCVM[4 + 1]			= "";
	char		szTmp[32]				= "";
	PAAS_BOOL 	bCVMAvailable			= PAAS_TRUE;
	PAAS_BOOL	bCVMMatch				= PAAS_FALSE;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	Hex2Bin((unsigned char *)pszTermCapab + 2, 1, (unsigned char *)&iTermCVMs);

	if(strlen(pszCardCVMLst) < 16)
	{
		debug_sprintf(szDbgMsg, "%s: Tag 8E is Less than 8 bytes ERROR", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return PAAS_FALSE;
	}

	//Copying X Amount
	memset(szTmp, 0x00, sizeof(szTmp));
	strncpy(szTmp, pszCardCVMLst, 8);
	fAmountX = (double)(atoi(szTmp))/100;
	pszCardCVMLst = pszCardCVMLst + 8;

	//Copying Y Amount
	memset(szTmp, 0x00, sizeof(szTmp));
	strncpy(szTmp, pszCardCVMLst, 8);
	fAmountY = (double)(atoi(szTmp))/100;
	pszCardCVMLst = pszCardCVMLst + 8;

	iCVMs = strlen(pszCardCVMLst)/4;
#if DEVDEBUG
	debug_sprintf(szDbgMsg, "%s: AmountX %lf", __FUNCTION__, fAmountX);
	APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: AmountY %lf", __FUNCTION__, fAmountY);
	APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: TransAmnt %lf", __FUNCTION__, ftransAmnt);
	APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: CashBack %lf", __FUNCTION__, fCashbackAmnt);
	APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: Card CVMs List %s", __FUNCTION__, pszCardCVMLst);
	APP_TRACE(szDbgMsg);
#endif

	for(iCount = 0; iCount < iCVMs; iCount++)
	{
		strncpy(szCVM, pszCardCVMLst, 4);
		pszCardCVMLst += 4;

		iCVMCondCode 	= atoi(szCVM + 2);
		debug_sprintf(szDbgMsg, "%s: iCVMCondCode %d szCVM[1]:%c", __FUNCTION__, iCVMCondCode,szCVM[1]);
		APP_TRACE(szDbgMsg);
		if( (iCVMCondCode == 0 || iCVMCondCode == 3)		||
			(iCVMCondCode == 2 && fCashbackAmnt <= 0)		||
			(iCVMCondCode == 5 && fCashbackAmnt > 0)		||
			(iCVMCondCode == 6 && ftransAmnt <= fAmountX)	||
			(iCVMCondCode == 7 && ftransAmnt > fAmountX)	||
			(iCVMCondCode == 8 && ftransAmnt <= fAmountY)	||
			(iCVMCondCode == 9 && ftransAmnt > fAmountY)
			)
		{
			switch(szCVM[1])
			{
			case '1':
				iCardCVMs = iCardCVMs | KEY_INDEX_8;
				break;

			case '2':
				iCardCVMs = iCardCVMs | KEY_INDEX_7;
				break;

			case '3':
				iCardCVMs = iCardCVMs | KEY_INDEX_6;
				iCardCVMs = iCardCVMs | KEY_INDEX_8;
				break;

			case '4':
				iCardCVMs = iCardCVMs | KEY_INDEX_5;
				break;

			case '5':
				iCardCVMs = iCardCVMs | KEY_INDEX_6;
				iCardCVMs = iCardCVMs | KEY_INDEX_5;
				break;

			case 'E':
				iCardCVMs = iCardCVMs | KEY_INDEX_6;
				break;

			case 'F':
				iCardCVMs = iCardCVMs | KEY_INDEX_4;
				break;
			}

			/* Daivik:16/9/2016 If CardCVM is one of the valid ones for this transaction,then predict the CVM ,
			 * based on the Terminal Capability.
			 */
			if( bPredictCVM && ((iCardCVMs != 0) || szCVM[1] == '0'))
			{
				/* Daivik:16/7/2016 - If Capability in Card does not match with Terminal Capability, 
				 * then check next CVM based on the first byte of CVM;
				 * If First Byte of CVM indicates 4 or 5 , that means next CVM can be performed , so we can check what the next CVM is;
				 * We are equating against CardCVM because , there are few CVMs which need two bits to be set and we need to ensure all the bits are set correctly.
				 */

				if((iTermCVMs & iCardCVMs) == iCardCVMs)
				{
					bCVMMatch = PAAS_TRUE;
				}
				/* DParam:23/9/2016 - Converting the first nibble of the CVM rule to hex , so that
				 * we can check whether the 2nd bit is set or not.
				 */
				iCVMByte0 = ((szCVM[0] <= '9') && (szCVM[0] >= '0'))? (szCVM[0] - '0') :
						    (((szCVM[0] >= 'a') && (szCVM[0] <= 'f'))? (szCVM[0] - 'a' + 10) :
						    (szCVM[0] - 'A' + 10) );

				if(!bCVMMatch && ((iCVMByte0 & 0x4) || (iCVMCondCode == 3)))
				{
					debug_sprintf(szDbgMsg, "%s: Card CVMs %x", __FUNCTION__, iCardCVMs);
					APP_TRACE(szDbgMsg);
					iCardCVMs = 0;
					bCVMMatch = PAAS_FALSE;
					continue;
				}
				if(!(iCardCVMs == 0x40 || iCardCVMs == 0x80 || iCardCVMs == 0x10 || iCardCVMs == 0x30 || iCardCVMs == 0xA0) || !bCVMMatch)
				{
					debug_sprintf(szDbgMsg, "%s: Cashback Not allowed : Card CVM:%x", __FUNCTION__,iCardCVMs);
					APP_TRACE(szDbgMsg);
					iCardCVMs = 0;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Require to predict CVM so can break after getting first CVM,szCVM[1]:%c", __FUNCTION__,szCVM[1]);
					APP_TRACE(szDbgMsg);
				}
				break;
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: Terminal CVMS %x", __FUNCTION__, iTermCVMs);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Card CVMs %x", __FUNCTION__, iCardCVMs);
	APP_TRACE(szDbgMsg);

	bCVMAvailable = iTermCVMs & iCardCVMs;

	if(bCVMAvailable > 0)
	{
		debug_sprintf(szDbgMsg, "%s: CVM Matched: %x", __FUNCTION__, bCVMAvailable);
		APP_TRACE(szDbgMsg);
		bCVMAvailable = PAAS_TRUE;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: CVM Mis Matched", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		bCVMAvailable = PAAS_FALSE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, bCVMAvailable);
	APP_TRACE(szDbgMsg);

	return bCVMAvailable;
}

/*
 * ============================================================================
 * Function Name: sendC30ifReqd
 *
 * Description	: This function sends c30 if the cvm applied is not available
 *					in the card
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int sendC30ifReqd(char* pszTranKey)
{
	int				rv						= SUCCESS;
	int				iAppLogEnabled			= isAppLogEnabled();
	int				iTmp					 = -1;
	char			szAppLogData[256]		= "";
	char			szCardCVMLst[256+1]		= "";
	char			szTermCapabilities[10]	= "";
	char			szPrefTermCap[7]		= "";
	double			fThreshold				= 0.00;
	PAAS_BOOL		bWait					= PAAS_TRUE;
	PAAS_BOOL		bCVMAvailable			= PAAS_FALSE;
	PAAS_BOOL		bSTBDataReceived		= PAAS_FALSE;
	PAAS_BOOL		bCardDataReceived		= PAAS_FALSE;
	PAAS_BOOL		bAmountChanged			= PAAS_FALSE;
	PAAS_BOOL		bDefaultCVM				= PAAS_FALSE;
	PAAS_BOOL		bPreferredCVM			= PAAS_FALSE;
	PAAS_BOOL		bSwitchReqd				= PAAS_TRUE;
	PAAS_BOOL		bCashbackPreferred		= PAAS_FALSE;
	PAAS_BOOL		bCashbackDefault		= PAAS_FALSE;
	CARDDTLS_STYPE	stCardDtls;
	AMTDTLS_STYPE	stAmtDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&stAmtDtls, 0x00, sizeof(AMTDTLS_STYPE));
	rv = getAmtDtlsForPymtTran(pszTranKey, &stAmtDtls);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get Amnt dtls for EMV ",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	//get Card Dtls
	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
	rv = getCardDtlsForPymtTran(pszTranKey, &stCardDtls);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get card dtls for EMV ",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	memset(szPrefTermCap, 0x00, sizeof(szPrefTermCap));
	rv = getPreferredDtlsForAID(stCardDtls.stEmvAppDtls.szAID, szPrefTermCap, &fThreshold);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get Preferred Dtls PAID ",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Failed to Read Preferred Details");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}
		rv = ERR_DEVICE_APP;
		return rv;
	}

	if(atof(stAmtDtls.cashBackAmt) > 0)
	{
		rv = getEMVDtlsfromAID(&stCardDtls, &iTmp);
		if(rv == SUCCESS)
		{
			if((strlen(stCardDtls.stEmvTerDtls.szTermCapabilityProf) > 0) && (strlen(stCardDtls.stEmvAppDtls.szIccCVMList) > 0))
			{
				// DParam:22/9 -We can use the available CVM list if already present. If it is not , then we need to send C36 and then query
				// We would normally have the required tags by this point , but if Cashback was obtained from POS, we would not have these tags
			}
			else
			{
				rv = getReqdEMVTagsFromXPI(&stCardDtls,"8E9F33",pszTranKey);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get Required EMV Tags :%d",__FUNCTION__,rv);
					APP_TRACE(szDbgMsg);
					return rv;
				}
				debug_sprintf(szDbgMsg, "%s: Obtained CVM List :%s Term Cap:%s", __FUNCTION__,stCardDtls.stEmvAppDtls.szIccCVMList,stCardDtls.stEmvTerDtls.szTermCapabilityProf);
				APP_TRACE(szDbgMsg);
			}
			 strncpy(szCardCVMLst,stCardDtls.stEmvAppDtls.szIccCVMList,sizeof(szCardCVMLst)-1); // We would have obtained the CVM list during the cashback state
			 bCashbackPreferred = isCVMAvailableInCard(szPrefTermCap, szCardCVMLst, atof(stAmtDtls.tranAmt), atof(stAmtDtls.cashBackAmt),PAAS_TRUE);
			 bCashbackDefault = isCVMAvailableInCard(stCardDtls.stEmvTerDtls.szTermCapDefault, szCardCVMLst, atof(stAmtDtls.tranAmt), atof(stAmtDtls.cashBackAmt),PAAS_TRUE);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get EMV Details;Use Default Behavior ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			bCashbackPreferred = PAAS_FALSE;
			bCashbackDefault = PAAS_FALSE;
		}
	}

	if(isPreSwipeDone())
	{
		debug_sprintf(szDbgMsg, "%s: Trans Amt: %lf Threshold: %lf",__FUNCTION__, atof(stAmtDtls.tranAmt), fThreshold);
		APP_TRACE(szDbgMsg);
		if(atof(stAmtDtls.tranAmt) >= fThreshold)
		{
			bAmountChanged = PAAS_TRUE;
			bDefaultCVM = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Card PreInserted, Final Amount More than Threshold, Need to re initiate the transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			bPreferredCVM = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Card PreInserted, Final Amount Less than Threshold, Need to Check CVM List", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		if( (atof(stAmtDtls.tranAmtFirstC30) >= fThreshold) && (atof(stAmtDtls.tranAmt) < fThreshold) )
		{

			if(strlen(stCardDtls.stEmvAppDtls.szIccCVMList) > 0)
			{
				strcpy(szCardCVMLst,stCardDtls.stEmvAppDtls.szIccCVMList);
			}
			else
			{
				rv = getTagFromEmvAgent("008E", szCardCVMLst, 4);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get required EMV Tags for kernel switching :%d ",__FUNCTION__,rv);
					APP_TRACE(szDbgMsg);
					if(rv == EMV_ERROR_CARD_REMOVED)
					{
						return rv;
					}
				}

			}

			//getTagFromEmvAgent("008E", szCardCVMLst, 4);
			if(isCVMAvailableInCard(szPrefTermCap, szCardCVMLst, atof(stAmtDtls.tranAmt), atof(stAmtDtls.cashBackAmt),PAAS_FALSE))
			{
				strncpy(stCardDtls.stEmvAppDtls.stAIDListInfo.szTermCap, szPrefTermCap,sizeof(stCardDtls.stEmvAppDtls.stAIDListInfo.szTermCap)-1);
				debug_sprintf(szDbgMsg, "%s: Prev Amount More but Final Amount Less than Threshold, And card support PAID CVM  Reinitiating", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				bAmountChanged = PAAS_TRUE;
				bPreferredCVM = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Prev Amount More but Final Amount Less than Threshold, but card does not support PAID CVM  No need to Reinitiate", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				bSwitchReqd = PAAS_FALSE;
				bDefaultCVM = PAAS_TRUE;
			}
		}
		else if( (atof(stAmtDtls.tranAmtFirstC30) < fThreshold) && (atof(stAmtDtls.tranAmt) >= fThreshold) )
		{
			bAmountChanged = PAAS_TRUE;
			bDefaultCVM = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Prev Amount Less but Final Amount greater than Threshold, Reinitiating", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else if( (atof(stAmtDtls.tranAmtFirstC30) >= fThreshold) && (atof(stAmtDtls.tranAmt) >= fThreshold) )
		{
			debug_sprintf(szDbgMsg, "%s: Prev and Final Amount greater than Threshold No need to Reinitiate", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			bDefaultCVM = PAAS_TRUE;
			bSwitchReqd = PAAS_FALSE;
		}
		else if( (atof(stAmtDtls.tranAmtFirstC30) < fThreshold) && (atof(stAmtDtls.tranAmt) < fThreshold) )
		{
			bPreferredCVM = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Prev and Final Amount Less than Threshold, Need to Check CVM List", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	debug_sprintf(szDbgMsg, "%s: Cashback :%s bSwitchReqd :%d bPreferredCVM:%d bCashbackPreferred:%d bDefaultCVM:%d bCashbackDefault:%d", __FUNCTION__,stAmtDtls.cashBackAmt,
			bSwitchReqd,bPreferredCVM,bCashbackPreferred,bDefaultCVM,bCashbackDefault);
	APP_TRACE(szDbgMsg);

	 if(!bSwitchReqd)
	{
		 //If Cashback is not enabled, no need to check CVM capability , we can return from here
		 if(!(atof(stAmtDtls.cashBackAmt) > 0))
		 {
			debug_sprintf(szDbgMsg, "%s: No Switch Required and Cashback not present, Return",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			 return SUCCESS;
		 }
		 /* DParam:23/9/16	If Cashback is present and if the TermCap is preffered   , we expect Cashback to be allowed by Preferred TermCap + 8E combination
		  * If Cashback is present and if TermCap is Default , we expect Cashback to be allowed by Default TermCap + 8E Combination
		  * If above two conditions are not met , no switch is required and we can return. We can also return if Cashback is not supported by either Default/Preferred TermCap
		  */
		 if((bPreferredCVM && bCashbackPreferred) || (bDefaultCVM && bCashbackDefault) || (!bCashbackDefault && !bCashbackPreferred))
		 {
			debug_sprintf(szDbgMsg, "%s:Cashback Present and supported as per TermCap settings;Return ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			 return SUCCESS;
		 }

		 if(bCashbackPreferred)
		 {
			 strncpy(stCardDtls.stEmvAppDtls.stAIDListInfo.szTermCap, szPrefTermCap,sizeof(stCardDtls.stEmvAppDtls.stAIDListInfo.szTermCap)-1);
			 bPreferredCVM = PAAS_TRUE;
			 debug_sprintf(szDbgMsg, "%s:Cashback Present and supported in Preferred Settings ",__FUNCTION__);
			 APP_TRACE(szDbgMsg);
		 }
		 else
		 {
			 //strcpy(stCardDtls.stEmvAppDtls.stAIDListInfo.szTermCap, stCardDtls.stEmvTerDtls.szTermCapDefault);
			 //If we do not fill SzTermCap , we will not being sending it in U01, and hence XPI will pick up defaults from EMVtables.ini
			 bDefaultCVM = PAAS_TRUE;
			 debug_sprintf(szDbgMsg, "%s:Cashback Present and supported in Default Settings ",__FUNCTION__);
			 APP_TRACE(szDbgMsg);
		 }
		 bAmountChanged = PAAS_TRUE;
	}
	//If amount has been exceeded threshold then we have to send C30 again, so if we know that C30 need to be sent
	// then No need of getting Tags
	if(bAmountChanged == PAAS_FALSE)
	{
		if((strlen(stCardDtls.stEmvTerDtls.szTermCapabilityProf) > 0) && (strlen(stCardDtls.stEmvAppDtls.szIccCVMList) > 0))
		{
			debug_sprintf(szDbgMsg, "%s: Already Present CVM List :%s Term Cap:%s", __FUNCTION__,stCardDtls.stEmvAppDtls.szIccCVMList,stCardDtls.stEmvTerDtls.szTermCapabilityProf);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			/* In case cashback is not enabled, then we would have not queried for these two tags earlier, hence we are querying for them now.
			 */
			//Daivik:4/4/2016 - We need to handle the error accordingly in the caller.
			rv = getReqdEMVTagsFromXPI(&stCardDtls,"8E9F33",pszTranKey);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get Required EMV Tags :%d",__FUNCTION__,rv);
				APP_TRACE(szDbgMsg);
				return rv;
			}
			debug_sprintf(szDbgMsg, "%s: Obtained CVM List :%s Term Cap:%s", __FUNCTION__,stCardDtls.stEmvAppDtls.szIccCVMList,stCardDtls.stEmvTerDtls.szTermCapabilityProf);
			APP_TRACE(szDbgMsg);
		}
		strcpy(szTermCapabilities,stCardDtls.stEmvTerDtls.szTermCapabilityProf);
		strcpy(szCardCVMLst,stCardDtls.stEmvAppDtls.szIccCVMList);

		//Sending 9F33
		//getTagFromEmvAgent("9F33", szTermCapabilities, 4);

		debug_sprintf(szDbgMsg, "%s: CVM Applied %s", __FUNCTION__, szTermCapabilities);
		APP_TRACE(szDbgMsg);

		//Sending 8E
		//getTagFromEmvAgent("008E", szCardCVMLst, 4);

		debug_sprintf(szDbgMsg, "%s: Card CVM List %s", __FUNCTION__, szCardCVMLst);
		APP_TRACE(szDbgMsg);

		bCVMAvailable = isCVMAvailableInCard(szTermCapabilities, szCardCVMLst, atof(stAmtDtls.tranAmt), atof(stAmtDtls.cashBackAmt),PAAS_FALSE);
	}
	debug_sprintf(szDbgMsg, "%s:bAmountChange:%d bPreferredCVM:%d bDefaultCVM:%d bCVMAvailable:%d", __FUNCTION__,bAmountChanged,bPreferredCVM,bDefaultCVM,bCVMAvailable);
	APP_TRACE(szDbgMsg);
	if((atof(stAmtDtls.cashBackAmt) > 0) && bSwitchReqd)
	{
		if(bAmountChanged && (bCashbackPreferred || bCashbackDefault))
		{
			if(!((bPreferredCVM && bCashbackPreferred) || (bDefaultCVM && bCashbackDefault)))
			{
				//Since we were supposed to switch CVM,but Cashback is not compatible with CVM we are moving to ,we will stay in the current CVM
				debug_sprintf(szDbgMsg, "%s:Cashback Supported in Current Settings Only;Return ",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				return SUCCESS;
			}
		}
		if(!bAmountChanged && (bCashbackPreferred || bCashbackDefault)) // Condition true only when we are before and after in the Preferred Kernel State
		{
			if(bCVMAvailable) // if CVM is available , we would normally be staying in the Same Kernel.Check if Cashback is also allowed, If not go to Default Kernel
			{
				if(bCashbackPreferred) // If Cashback Is not allowed for Preferred, it must be available for Default, else we would not enter this path
				{
					debug_sprintf(szDbgMsg, "%s:Cashback Supported in Preferred Settings;Return ",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					return SUCCESS;
				}
				bCVMAvailable = PAAS_FALSE; // If we need to move to default kernel when CVM available is true, we need to make this variable false.
				debug_sprintf(szDbgMsg, "%s:Cashback unsupported in Preferred Settings;Move to Default State Treat CVMAvail as False ",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else //CVM is not available in Preffered Kernel; Check the Cashback ability in Default Kernel, if it is allowed, go ahead, If not then stay in the Preferred Kernel ( It must have cashback ability)
			{
				//The condition when we enter this path , ensures that Cashback is supported either via Default or Preferred Kernel.So if it is not supported in default, it must be supported via preferred kernel.
				if(!bCashbackDefault)
				{
					debug_sprintf(szDbgMsg, "%s:Cashback Supported in Preferred Settings Only;Return ",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					return SUCCESS;
				}
			}
		}
		//If PIN CVM is available based on Default TermCap. Then we can switch
	}

	/* KranthiK1: Need to switch the kernel configuration as there is a mismatch
	 * in the CVM supported by card and the CVM supported by the card.
	 * Since GPO is already done re initiating the transaction to change the
	 * kernel configuration.
	 */

	if(bCVMAvailable == PAAS_FALSE || bAmountChanged)
	{
		debug_sprintf(szDbgMsg, "%s: Re initiating the Transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Shifting to the U01 so that preferred aid method is not selected
		 * and there is no user interaction and everything is done at the
		 * background.
		 * */
//		putEnvFile("IAB", "send_u01", "1");
		setXPIParameter("send_u01", "1");

		rv = isEmvChipCardPresennt();
		if(rv == CARD_NOT_PRESENT)
		{
			debug_sprintf(szDbgMsg, "%s: EMV Card Removed in Between Transaction ",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Shifting back to the preferred AID method*/
			setXPIParameter("send_u01", "0");

			return EMV_ERROR_CARD_REMOVED;
		}
		else if(rv != CARD_PRESENT)
		{
			debug_sprintf(szDbgMsg, "%s: Error in Processing I05 Command ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* Shifting back to the preferred AID method*/
			setXPIParameter("send_u01", "0");

			return FAILURE;
		}

		//AjayS2: 13-Jan-2016
		//If wallet is enabled we have to send TERM_CAP as Payment only, because from next release
		//default TERM_CAP is taken as VAS and PAYMENT
		if(isWalletEnabled())
		{
			sendC30CmdToGetChipCardData(pszTranKey, PAYMENT_ONLY, NULL, NULL);
		}
		else
		{
			sendC30CmdToGetChipCardData(pszTranKey, NULL, NULL, NULL);
		}

		/* Wait for the card data to be received from the UI agent */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;

			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_EMV_RESP)
				{
					if( strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_U00_REQ) == 0 || strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_U02_REQ) == 0)
					{
						if(stBLData.iStatus == SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: Received U02 EMV Response",__FUNCTION__);
							APP_TRACE(szDbgMsg);

							if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bEMVRetry == PAAS_TRUE)
							{
								/* Initialize the screen */
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "Error While Inserting/Swiping/Tapping card");
									addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, NULL);
								}
							}
							else if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bCardInserted == PAAS_TRUE)
							{
								/* KranthiK1: Added this only to show the processing form
								 * as C30 is taking a long time to be respond.
								 * Remove later if not required
								 */
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "EMV Card Inserted");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}
								bBLDataRcvd = PAAS_FALSE;

								releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

								//showPSGenDispForm(MSG_PROCESSING, STANDBY_ICON, 0);
							}
							else if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bSTBDataReceived == PAAS_TRUE)
							{
								debug_sprintf(szDbgMsg, "%s: Received STB data",__FUNCTION__);
								APP_TRACE(szDbgMsg);

								bSTBDataReceived = PAAS_TRUE;

								if(bCardDataReceived == PAAS_TRUE) //If we have received the card data then we need not wait
								{
									bWait = PAAS_FALSE;
								}
							}
							bBLDataRcvd = PAAS_FALSE;
						}
					}
					else if(isEmvKernelSwitchingAllowed() && (strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_U01_REQ) == 0))
					{
						debug_sprintf(szDbgMsg, "%s: Got U01 while waiting for Card Data", __FUNCTION__);
						APP_TRACE(szDbgMsg);
#if 0
						memset(&stEmvDtls, 0x00, sizeof(EMVDTLS_STYPE));
						memcpy(&stEmvDtls, &(stBLData.stRespDtls.stEmvDtls), sizeof(EMVDTLS_STYPE));

						debug_sprintf(szDbgMsg, "%s: Total AIDs %d", __FUNCTION__, stEmvDtls.stEmvAppDtls.stAIDListInfo.iTotalAIDs);
						APP_TRACE(szDbgMsg);

						pstAIDName = stEmvDtls.stEmvAppDtls.stAIDListInfo.LstHead;

						/*AjayS2: In sendU01ResponseToEMVAgent
						 * 	If need to send all AID(s) back to XPI, send PAAS_FALSE in 2nd parameter
						 * 	If need to send an already selected AID, paste the AID, TerCap into
						 * 		stEmvDtls.stEmvAppDtls.stAIDListInfo.szAIDSelected and/or szTermCap resp, and send
						 * 		PAAS_TRUE in 2nd parameter. Ex: Sending AID A0000000043060 to be selected
						 *
						 * 		strcpy(stEmvDtls.stEmvAppDtls.stAIDListInfo.szAIDSelected, "A0000000043060");
						 * 		rv = sendU01ResponseToEMVAgent(&(stEmvDtls.stEmvAppDtls.stAIDListInfo), PAAS_TRUE);
						 *
						 * 		Note: After sending U01, all AID(s) will be cleared from memory
						*/
#endif
						strcpy(stCardDtls.stEmvAppDtls.stAIDListInfo.szAIDSelected, stCardDtls.stEmvAppDtls.szAID);
						//strcpy(stCardDtls.stEmvAppDtls.stAIDListInfo.szTermCap, "E008C8");
						rv = sendU01ResponseToEMVAgent(&(stCardDtls.stEmvAppDtls.stAIDListInfo), PAAS_TRUE);
						//Sending ALL AIDs back to XPI
						//rv = sendU01ResponseToEMVAgent(&(stCardDtls.stEmvAppDtls.stAIDListInfo), PAAS_FALSE);
						if(rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: U01 Response sending Fails", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							bWait = PAAS_FALSE;
						}
						bBLDataRcvd = PAAS_FALSE;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Got EMV Command Response = %s",__FUNCTION__, stBLData.stRespDtls.stEmvDtls.szEMVRespCode);
						APP_TRACE(szDbgMsg);
						rv = atoi(stBLData.stRespDtls.stEmvDtls.szEMVRespCode);
						if(rv != EMV_CMD_SUCCESS)
						{
							bWait = PAAS_FALSE;
						}

						bBLDataRcvd 	  = PAAS_FALSE;
						bCardDataReceived = PAAS_TRUE;

						/*
						 * If STB is enabled, then we need to wait for the XEVT data
						 * thats why not setting bwait to false here
						 */
						if(!isSTBLogicEnabled())
						{
							bWait = PAAS_FALSE;
						}
						else
						{
							/*
							 * If STB is enabled..we have to wait only if we have received the card data, thats why checking rv value
							 * If we have received the STB data already then we need not wait
							 */
							if(bSTBDataReceived == PAAS_TRUE)
							{
								bWait = PAAS_FALSE;
							}
						}
					}
				}
				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			if(bWait == PAAS_FALSE)
			{
				break;
			}
			/* Wait for message from the UI agent */
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}

		/* Shifting back to the preferred AID method*/
		//putEnvFile("IAB", "send_u01", "0");
		setXPIParameter("send_u01", "0");
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadPreferredDtlsFromPAID
 *
 * Description	: This function will load the threshold amount from PAID file
 *
 * Output Params:
 * ============================================================================
 */
int loadPreferredDtlsFromPAID()
{
	int				rv				= SUCCESS;
	int				iAmount			= 0;
	int				iCount			= 0;
	int				iLen			= 0;
	int				MAX_PAID_RANGE	= 32;
	char			szKey[30]		= "";
	char			szAID[33]		= "";
	char			szPrefTerCap[7] = "";
	char			length			= 0;
	char			szAmnt[20]		= "";
	char*			pszValue		= "";
	char*			curPtr			= NULL;
	char*			nxtPtr			= NULL;
	double			fAmount			= 0.0;
	dictionary *	dict			= NULL;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	dict = iniparser_load(PAID_FILE_NAME);
	if(dict == NULL)
	{
		/* Unable to load the ini file */
		debug_sprintf(szDbgMsg, "%s: Unable to load ini file [%s]", __FUNCTION__, PAID_FILE_NAME);
		APP_TRACE(szDbgMsg);
		rv = SUCCESS;
		return rv;
	}

	while(iCount < MAX_PAID_RANGE)
	{
		iCount++;
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s%d", "PreferredAIDs", "PAID", iCount);
		pszValue = iniparser_getstring(dict, szKey, NULL);
		if(pszValue != NULL)
		{
			nxtPtr = strchr(pszValue, ',');
			if(nxtPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while Parsing [%s]", __FUNCTION__, PAID_FILE_NAME);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				return rv;
			}

			curPtr = pszValue;
			iLen = nxtPtr - curPtr;
			memset(szAID, 0x00, sizeof(szAID));
			strncpy(szAID, curPtr, iLen);

			curPtr = strstr(pszValue, "9F33");
			if(curPtr != NULL)
			{
				curPtr += 6;

				memset(szPrefTerCap, 0x00, sizeof(szPrefTerCap));
				strncpy(szPrefTerCap, curPtr, 6);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while Parsing [%s]", __FUNCTION__, PAID_FILE_NAME);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				return rv;
			}

			curPtr = strstr(pszValue, "9F02");
			if(curPtr != NULL)
			{
				curPtr += 4;

				Hex2Bin((unsigned char*)curPtr, 1, (unsigned char*)&length);

				curPtr += 2;
				//nxtPtr = curPtr + length*2;
				//Daivik: 8/2/2016 - Coverity 67247 - Anyway the nxtPtr is overwritten in the next iteration , so we are effectively not using this assignment.
				//Check if the above change is ok

				memset(szAmnt, 0x00, sizeof(szAmnt));
				strncpy(szAmnt, curPtr, length*2);

				iAmount = atoi(szAmnt);
				fAmount = (double)iAmount/100;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while Parsing [%s]", __FUNCTION__, PAID_FILE_NAME);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				return rv;
			}

			rv = setPreferredDtlsForAID(szAID, szPrefTerCap, fAmount);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while Storing PrefAID Data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				return rv;
			}
		}
		else
		{
			break;
		}
	}
	if(dict != NULL)
	{
		iniparser_freedict(dict);
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: parseVSDBlobDataResp
 *
 * Description	: This function would parse the XPI E07 VSD Ciphered data response.
 *
 * Input Params	: source = 1 called from C31 command parsing
 * 				  source = 2 called from C3121 MSR Chip command parsing
 * 				  source = 3 called from S20 command parsing
 * 				  source = 4 called from XGCD command parsing
 *
 * Output Params:
 * ============================================================================
 */
int parseVSDBlobDataResp(char *szMsg, int iBuffSize, CARD_TRK_PTYPE	pstTrk, EMVDTLS_PTYPE pstEmvDtls, MANUAL_PTYPE pstManual, int iSource)
{
	int				rv						= SUCCESS;
	int				iLen					= 0;
	int				iCnt					= 0;
	int				iDataType				= -1;
	int				iCardType				= -1;
	int				iPayType				= -1;
	int				tagLength				= 0;
	int				iEncLen					= 0;
	int				iDecLen					= 0;
	int				iResCode				= 0;
	char			szEncType[2+1]			= "";
	char 			szTmp[50]				= "";
	char			szResCode[5]			= "";
	char			tagValue[2048]			= "";
	char			szTempB64EncData[2048]	= "";
	char			szTempB64DecData[2048]	= "";
	char			szVSDEncBlob0[512]		= "";	// mapped to track2 data for MSR
	char			szVSDEncBlob1[512]		= "";	// mapped to track1 data for MSR
	char			szKSN0[20+1]			= "";	// Associated with encBlob0
	char			szKSN1[20+1]			= "";	// Associated with encBlob1
	char			szIV0[20+1]				= "";	// Associated with encBlob0
	char			szIV1[20+1]				= "";	// Associated with encBlob1
	char			szClrTrk1[100]			= "";
	char			szClrTrk2[100]			= "";
	char			szClrPAN[30]			= "";
	char *			curPtr					= NULL;
	char *			nxtPtr					= NULL;
	VSD_TAGS		tagID  					= 0;
	VSD_DTLS_PTYPE	pstVSDDtls				= NULL;
	VSD_TAGLIST_PTYPE	pstLocTagList		= NULL;
	VSD_TAGNODE_PTYPE	pstTmpTagNode 		= NULL;
#ifdef DEVDEBUG
	char 		szDbgMsg[4096]	= "";
#elif DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( (iSource == 1 && pstEmvDtls != NULL) || ( (iSource == 2 || iSource ==3) && (pstTrk != NULL) ) || (iSource == 4 && pstManual != NULL))
	{
		debug_sprintf(szDbgMsg, "%s: Source called: %d", __FUNCTION__, iSource);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: NULL params or wrong source passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	while(1)
	{
		if(iBuffSize == 0)
		{
			debug_sprintf(szDbgMsg, "%s: VSD Encryption Blob data is not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = ERR_INVALID_CARD_DATA;// FIXME: is this valid ?
			break;
		}

		if(iSource != 4)	// for other than manual entry
		{
			/* MukeshS3: Format for E07 response in VSD encryption
			 * <STX> E07<Response code-2N  00 - SUCCESS><Encryption Type-2N  01 - VSP, 02 - PKI/RSA, 05 - VSD><FS><Data Type-2N 	01 - Track Data
																																	02 - EMV Data
																																	03 - Manual Entry Data>
				<FS>VSD Blob Data - TLV list of elements according to configuration in VSD.INI <ETX>
			 */

			// copy & check the response code
			memcpy(szResCode, szMsg, 2);
			szMsg += 2;
			iBuffSize -= 2;
			iResCode = atoi(szResCode);
			if(iResCode != 0)
			{
				debug_sprintf(szDbgMsg, "%s: Encrypted data NOT present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			curPtr = szMsg;
			/* Copy & Check the encryption type */
			memset(szEncType, 0x00, sizeof(szEncType));
			memcpy(szEncType, curPtr, 2);

			if(strcmp(szEncType, "05"))	//VSD encryption
			{
				debug_sprintf(szDbgMsg, "%s: Encryption other than VSD", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			curPtr +=3; // 2N for encryption type & 1N for FS
			iBuffSize -= 3;

			// Get the Data type returned.
			nxtPtr = strchr(curPtr, FS);
			iLen = nxtPtr - curPtr;
			if(iLen > 0)
			{
				memset(szTmp, 0x00, sizeof(szTmp));
				memcpy(szTmp, curPtr, iLen);
				iDataType = atoi(szTmp);
			}

			curPtr += iLen + 1;
			iBuffSize -= (iLen + 1);		// +1 for <FS>;
		}
		else
		{
			curPtr = szMsg;
			iDataType = 3;	// for Manual entry data
		}

		/* From here curPtr points to VSD blob data
		 * Parse the VSD blob data according to VSD tag list configured for each section in VSD.INI
		 * Here First two tags would be always cardtype & paytype in the list (Mandatory)
		 */

		if(isVSDb64EncodingEnabled())
		{
			debug_sprintf(szDbgMsg, "%s: VSD Blob data is B64 Encoded. Decoding the data of length[%d]", __FUNCTION__, iBuffSize);
			APP_TRACE(szDbgMsg);
#ifdef DEVDEBUG
			APP_TRACE_EX(curPtr, iBuffSize);
#endif
			memset(szTempB64DecData, 0x00, sizeof(szTempB64DecData));
			dcdBase64((uchar *)curPtr, iBuffSize, (uchar *)szTempB64DecData, &iDecLen);
			curPtr = szTempB64DecData;
			debug_sprintf(szDbgMsg, "%s: Data is decoded[%d]", __FUNCTION__, iDecLen);
			APP_TRACE(szDbgMsg);
#ifdef DEVDEBUG
			APP_TRACE_EX(curPtr, iDecLen);
#endif
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is Not B64 encoded", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		//get the VSD settings
		pstVSDDtls = getVSDEncryptionSettings();

		// get Card type tag
		memset(tagValue, 0x00, sizeof(tagValue));
		rv = getTagValue(&curPtr, pstVSDDtls->szCardTypeTagName, tagValue, &tagLength);
		if((rv != SUCCESS) || (tagLength == 0))
		{
			debug_sprintf(szDbgMsg, "%s: Coudn't found the tag value, Error !", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		iCardType = atoi(tagValue);

		//FIXME: need to discuss & remove later after getting FIX in VSD library
		// For NON iso cards, its coming as 99, so mapping this to corresponding section in VSD.INI
		if(iCardType == 99)
		{
			debug_sprintf(szDbgMsg, "%s: A Non ISO card is being used", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iCardType = 3;
		}

		// get Pay type tag
		memset(tagValue, 0x00, sizeof(tagValue));
		getTagValue(&curPtr, pstVSDDtls->szPayTypeTagName, tagValue, &tagLength);
		if(rv != SUCCESS || tagLength == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Coudn't found the tag value, Error !", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		// paytype tag is always a two byte value
		iPayType = tagValue[0]*256 + tagValue[1];

		debug_sprintf(szDbgMsg, "%s: CardType[%d] PayType[%d] DataType[%d]", __FUNCTION__,iCardType, iPayType, iDataType);
		APP_TRACE(szDbgMsg);

		/* search for the corresponding tag list for the given card type & paytype tags*/
		for(iCnt = 0; iCnt < MAX_VSD_SECTION; iCnt++)
		{
			if(pstVSDDtls->pstVSDBlob[iCnt] == NULL)
			{
				continue;
			}

			if((pstVSDDtls->pstVSDBlob[iCnt]->cardType == iCardType) && (pstVSDDtls->pstVSDBlob[iCnt]->payType == iPayType))
			{
				pstLocTagList = pstVSDDtls->pstVSDBlob[iCnt]->pstTagList;
				break;
			}
		}
		if(iCnt == MAX_VSD_SECTION)
		{
			/*
			 * It means that it's a valid card type(not 0x99) which satisfies the BIN.DAT range
			 * but tags are not being configured in VSD.INI file for this card type. So, we need to through an error for this Invalid card/Invalid configuration
			 */
			debug_sprintf(szDbgMsg, "%s: Tag list not found, Error !", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = ERR_INVALID_CARD_DATA;
			break;
		}

		pstTmpTagNode = pstLocTagList->start;
		// First two tags will always comes as card type & payment type, which we have already parsed above. So, just skipping these from the list
		// NULL check is being added just for safety purpose(in case wrong data from VSD library)
		if(pstTmpTagNode != NULL)
		{
			pstTmpTagNode = pstTmpTagNode->next;		//card type
			if(pstTmpTagNode != NULL)
			{
				pstTmpTagNode = pstTmpTagNode->next; 	//pay type
			}
		}

		// go through the entire tag list & parse each tag & save the required details
		// If, any new tag needs to be parsed in future, it has to be added to the below switch case block
		while(pstTmpTagNode != NULL)
		{
			getTagValue(&curPtr, pstTmpTagNode->tagName, tagValue, &tagLength);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Coudn't found the tag value, Error !", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			if(tagLength > 0)
			{
#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s: Tag Name[%s] Tag Length[%d] Tag Value: ", __FUNCTION__, pstTmpTagNode->tagName, tagLength);
				APP_TRACE(szDbgMsg);
				APP_TRACE_EX(tagValue, tagLength);
#endif
				//Added because b64 encoded data needs to be sent to the host
				tagID = pstTmpTagNode->tagID;
				if(tagID == TAG_ENCBLOB0 || tagID == TAG_ENCBLOB1 || tagID == TAG_KSN0 || tagID == TAG_KSN1 || tagID == TAG_IV0 || tagID == TAG_IV1)
				{
					memset(szTempB64EncData, 0x00, sizeof(szTempB64EncData));
					encdBase64((uchar *)tagValue, tagLength, (uchar *)szTempB64EncData, &iEncLen);
					debug_sprintf(szDbgMsg, "%s: Data is Encoded[%d]", __FUNCTION__, iEncLen);
					APP_TRACE(szDbgMsg);
#ifdef DEVDEBUG
					APP_TRACE_EX(szTempB64EncData, iEncLen);
#endif
					memset(tagValue, 0x00, sizeof(tagValue));
					memcpy(tagValue, szTempB64EncData, iEncLen);
					tagLength = iEncLen;
				}

				switch(pstTmpTagNode->tagID)
				{
					case TAG_TRACK1_REG:
						memset(szClrTrk1, 0x00, sizeof(szClrTrk1));
						memcpy(szClrTrk1, tagValue, tagLength);
						break;

					case TAG_TRACK2_REG:
					case TAG_EMVTAG_57:	// TRACK2 REGULAR
						memset(szClrTrk2, 0x00, sizeof(szClrTrk2));
						memcpy(szClrTrk2, tagValue, tagLength);
						break;

					case TAG_PAN:
					case TAG_EMVTAG_5A:	// PAN
						memset(szClrPAN, 0x00, sizeof(szClrPAN));
						memcpy(szClrPAN, tagValue, tagLength);
						break;

					case TAG_ENCBLOB0:
						memset(szVSDEncBlob0, 0x00, sizeof(szVSDEncBlob0));
						memcpy(szVSDEncBlob0, tagValue, tagLength);
						break;

					case TAG_ENCBLOB1:
						memset(szVSDEncBlob1, 0x00, sizeof(szVSDEncBlob1));
						memcpy(szVSDEncBlob1, tagValue, tagLength);
						break;

					case TAG_KSN0:
						memset(szKSN0, 0x00, sizeof(szKSN0));
						memcpy(szKSN0, tagValue, tagLength);
						break;

					case TAG_KSN1:
						memset(szKSN1, 0x00, sizeof(szKSN1));
						memcpy(szKSN1, tagValue, tagLength);
						break;

					case TAG_IV0:
						memset(szIV0, 0x00, sizeof(szIV0));
						memcpy(szIV0, tagValue, tagLength);
						break;

					case TAG_IV1:
						memset(szIV1, 0x00, sizeof(szIV1));
						memcpy(szIV1, tagValue, tagLength);
						break;

					default:
						break;
				}
			}
			else
			{
				//just logging here, because the configured data may not be present in card or may not come because of some reason
				debug_sprintf(szDbgMsg, "%s: Couldn't found the tag value", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			pstTmpTagNode = pstTmpTagNode->next;
		}

		/* In VSD.INI file, Track2 data should be always configured for encBlob0 & Track1 data should be always configured for encBlob1
		 * So, we will pick up first ennBlob0(Track2) if present, otherwise will go for the encBlob1(Track1)
		 */
		if(iDataType == 1 && (iSource == 2 || iSource == 3))	// MSR track data
		{
			if(strlen(szVSDEncBlob0) > 0)
			{
				memset(pstTrk->szVsdEncBlob, 0x00, sizeof(pstTrk->szVsdEncBlob));
				memset(pstTrk->szVsdKSN, 0x00, sizeof(pstTrk->szVsdKSN));
				memset(pstTrk->szVsdIV, 0x00, sizeof(pstTrk->szVsdIV));
				memcpy(pstTrk->szVsdEncBlob, szVSDEncBlob0, sizeof(szVSDEncBlob0));
				memcpy(pstTrk->szVsdKSN, szKSN0, sizeof(szKSN0));
				memcpy(pstTrk->szVsdIV, szIV0, sizeof(szIV0));
			}
			else if(strlen(szVSDEncBlob1) > 0)
			{
				memset(pstTrk->szVsdEncBlob, 0x00, sizeof(pstTrk->szVsdEncBlob));
				memset(pstTrk->szVsdKSN, 0x00, sizeof(pstTrk->szVsdKSN));
				memset(pstTrk->szVsdIV, 0x00, sizeof(pstTrk->szVsdIV));
				memcpy(pstTrk->szVsdEncBlob, szVSDEncBlob1, sizeof(szVSDEncBlob1));
				memcpy(pstTrk->szVsdKSN, szKSN1, sizeof(szKSN1));
				memcpy(pstTrk->szVsdIV, szIV1, sizeof(szIV1));
			}
			if(strlen(szClrTrk1))
			{
				memset(pstTrk->szClrTrk1, 0x00, sizeof(pstTrk->szClrTrk1));
				memcpy(pstTrk->szClrTrk1, szClrTrk1, sizeof(pstTrk->szClrTrk1));
			}
			if(strlen(szClrTrk2))
			{
				memset(pstTrk->szClrTrk2, 0x00, sizeof(szClrTrk2));
				memcpy(pstTrk->szClrTrk2, szClrTrk2, sizeof(szClrTrk2));
			}
		}
		else if(iDataType == 2 && (iSource == 1))	// EMV data
		{
			if(strlen(szVSDEncBlob0) > 0)
			{
				memset(pstEmvDtls->szEMVEncBlob, 0x00, sizeof(pstEmvDtls->szEMVEncBlob));
				memset(pstEmvDtls->szVsdKSN, 0x00, sizeof(pstEmvDtls->szVsdKSN));
				memset(pstEmvDtls->szVsdIV, 0x00, sizeof(pstEmvDtls->szVsdIV));
				memcpy(pstEmvDtls->szEMVEncBlob, szVSDEncBlob0, sizeof(szVSDEncBlob0));
				memcpy(pstEmvDtls->szVsdKSN, szKSN0, sizeof(szKSN0));
				memcpy(pstEmvDtls->szVsdIV, szIV0, sizeof(szIV0));
			}
			else if(strlen(szVSDEncBlob1) > 0)
			{
				memset(pstEmvDtls->szEMVEncBlob, 0x00, sizeof(pstEmvDtls->szEMVEncBlob));
				memset(pstEmvDtls->szVsdKSN, 0x00, sizeof(pstEmvDtls->szVsdKSN));
				memset(pstEmvDtls->szVsdIV, 0x00, sizeof(pstEmvDtls->szVsdIV));
				memcpy(pstEmvDtls->szEMVEncBlob, szVSDEncBlob1, sizeof(szVSDEncBlob1));
				memcpy(pstEmvDtls->szVsdKSN, szKSN1, sizeof(szKSN1));
				memcpy(pstEmvDtls->szVsdIV, szIV1, sizeof(szIV1));
			}
			if(strlen(szClrTrk1))
			{
				memset(pstEmvDtls->szClrTrk1, 0x00, sizeof(pstEmvDtls->szClrTrk1));
				memcpy(pstEmvDtls->szClrTrk1, szClrTrk1, sizeof(szClrTrk1));
			}
			if(strlen(szClrTrk2))
			{
				memset(pstEmvDtls->szClrTrk2, 0x00, sizeof(pstEmvDtls->szClrTrk2));
				memcpy(pstEmvDtls->szClrTrk2, szClrTrk2, sizeof(szClrTrk2));
			}
			pstEmvDtls->iEncType = VSD_ENC;
		}
		else if(iDataType == 3 && (iSource == 4))	// Manual entry data
		{
			if(strlen(szVSDEncBlob0) > 0)
			{
				memset(pstManual->szEncBlob, 0x00, sizeof(pstManual->szEncBlob));
				memset(pstManual->szVsdKSN, 0x00, sizeof(pstManual->szVsdKSN));
				memset(pstManual->szVsdIV, 0x00, sizeof(pstManual->szVsdIV));
				memcpy(pstManual->szEncBlob, szVSDEncBlob0, sizeof(szVSDEncBlob0));
				memcpy(pstManual->szVsdKSN, szKSN0, sizeof(szKSN0));
				memcpy(pstManual->szVsdIV, szIV0, sizeof(szIV0));
			}
			else if(strlen(szVSDEncBlob1) > 0)
			{
				memset(pstManual->szEncBlob, 0x00, sizeof(pstManual->szEncBlob));
				memset(pstManual->szVsdKSN, 0x00, sizeof(pstManual->szVsdKSN));
				memset(pstManual->szVsdIV, 0x00, sizeof(pstManual->szVsdIV));
				memcpy(pstManual->szEncBlob, szVSDEncBlob1, sizeof(szVSDEncBlob1));
				memcpy(pstManual->szVsdKSN, szKSN1, sizeof(szKSN1));
				memcpy(pstManual->szVsdIV, szIV1, sizeof(szIV1));
			}
			if(strlen(szClrPAN))
			{
				memset(pstManual->szClearPAN, 0x00, sizeof(pstManual->szClearPAN));
				memcpy(pstManual->szClearPAN, szClrPAN, sizeof(pstManual->szClearPAN));
			}
		}

		break;
	}

	if(rv == SUCCESS)
	{
		if(iSource == 2 || iSource == 3)
		{
			debug_sprintf(szDbgMsg, "%s: szEncBlob[%s] KSN[%s] IV[%s]", __FUNCTION__,
																		pstTrk->szVsdEncBlob,
																		pstTrk->szVsdKSN,
																		pstTrk->szVsdIV);
			APP_TRACE(szDbgMsg);
		}
		else if(iSource == 1)
		{
			debug_sprintf(szDbgMsg, "%s: szEncBlob[%s] KSN[%s] IV[%s]", __FUNCTION__,
																		pstEmvDtls->szEMVEncBlob,
																		pstEmvDtls->szVsdKSN,
																		pstEmvDtls->szVsdIV);
			APP_TRACE(szDbgMsg);
		}
		else if(iSource == 4)
		{
			debug_sprintf(szDbgMsg, "%s: szEncBlob[%s] KSN[%s] IV[%s]", __FUNCTION__,
																		pstManual->szEncBlob,
																		pstManual->szVsdKSN,
																		pstManual->szVsdIV);
			APP_TRACE(szDbgMsg);
		}
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: getMerchantIdfromNfcFile
 *
 * Description	: This function will load the Mer ID from nfc.ini file
 *
 * Input Params	:  Merchant Index, structure to fill provision Pass details
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getMerchantIdfromNfcFile(char * szTranKey, int *iMerchIndex, NFC_APPLE_SECT_DTLS_PTYPE pstNfcIniContent)
{
	int						rv						= SUCCESS;
	int						iTotMerchID				= 0;
	int						iCount					= 0;
	int						iSectionCount			= 0;
	int						iLen					= 0;
	int						iAppLogEnabled			= isAppLogEnabled();
	char					szAppLogData[300]		= "";
	char					szKey[30]				= "";
	char					szTotMerchID[10+1]		= "";
	char					szCurMerIndex[10+1]		= "";
	char					szMerchID[100+1]		= "";
	char					szMerchURL[100+1]		= "";
	char					szMerchVASFilter[100+1]	= "";
	char*					pszValue				= "";
	char* 					pszSectionName			= NULL;
	char*					szSectionValue			= "";
	dictionary *			dict					= NULL;
	NFCINI_CONTENT_INFO_PTYPE	pszTemp					= NULL;
	NFCINI_CONTENT_INFO_PTYPE	pszSections				= NULL;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstNfcIniContent = (NFC_APPLE_SECT_DTLS_PTYPE)malloc(sizeof(NFC_APPLE_SECT_DTLS_STYPE));
	if(pstNfcIniContent == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Could not allocate memory !!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(pstNfcIniContent, 0x00, sizeof(NFC_APPLE_SECT_DTLS_STYPE));

	dict = iniparser_load(NFC_INI_FILE_NAME);
	if(dict == NULL)
	{
		/* Unable to load the ini file */
		debug_sprintf(szDbgMsg, "%s: Unable to load ini file [%s]", __FUNCTION__, NFC_INI_FILE_NAME);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		*iMerchIndex = 0;
		return rv;
	}

	/*Getting Total Number of Merchant ID*/
	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s:%s", "applepay", "NUM_MERCHANT_IDS");
	pszValue = iniparser_getstring(dict, szKey, NULL);
	if(pszValue == NULL)
	{
		/* Unable to load the ini file */
		debug_sprintf(szDbgMsg, "%s: Unable to Get Total Merch IDs from [%s]", __FUNCTION__, NFC_INI_FILE_NAME);
		APP_TRACE(szDbgMsg);
		rv = ERR_INI_PARSING_FAIL;
		*iMerchIndex = 0;
		return rv;
	}

	iTotMerchID = atoi(pszValue);
	strcpy(szTotMerchID, pszValue);
	strcpy(pstNfcIniContent->szTotMerchIds, pszValue);

	debug_sprintf(szDbgMsg, "%s: Total Merch ID[%d]", __FUNCTION__, iTotMerchID);
	APP_TRACE(szDbgMsg);

	iCount = 1;

	while(iCount <= iTotMerchID)
	{
		/* Getting Section Name from NFC.ini file*/
		pszSectionName = iniparser_getsecname(dict, iCount);
		if(pszSectionName == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to Section Name [%s]", __FUNCTION__, NFC_INI_FILE_NAME);
			APP_TRACE(szDbgMsg);
			rv = ERR_INI_PARSING_FAIL;
			*iMerchIndex = 0;
			return rv;
		}

		iLen = strlen(pszSectionName);
		strcpy(pstNfcIniContent->szSectionName, pszSectionName);
		szSectionValue = pszSectionName + iLen - 1;

		iSectionCount = atoi(szSectionValue);

		/*Getting the MERCHANT_INDEX*/
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "applepay/merchant%d:%s", iSectionCount, "MERCHANT_INDEX");
		pszValue = iniparser_getstring(dict, szKey, NULL);
		if(pszValue == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to Get Merch%d Index from [%s]", __FUNCTION__, iCount, NFC_INI_FILE_NAME);
			APP_TRACE(szDbgMsg);
			rv = ERR_INI_PARSING_FAIL;
			*iMerchIndex = 0;
			break;
		}
		memset(szCurMerIndex, 0x00, sizeof(szCurMerIndex));
		strcpy(szCurMerIndex, pszValue);
		strcpy(pstNfcIniContent->szMerchIndex, szCurMerIndex);


		/*Getting the MERCHANT_ID*/
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "applepay/merchant%d:%s", iSectionCount, "MERCHANT_ID");
		pszValue = iniparser_getstring(dict, szKey, NULL);
		if(pszValue == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to Get Merch%d ID from [%s]", __FUNCTION__, iCount, NFC_INI_FILE_NAME);
			APP_TRACE(szDbgMsg);
			rv = ERR_INI_PARSING_FAIL;
			*iMerchIndex = 0;
			break;
		}
		memset(szMerchID, 0x00, sizeof(szMerchID));
		strcpy(szMerchID, pszValue);
		strcpy(pstNfcIniContent->szMerchID, szMerchID);



		/*Getting Merchant_url*/
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "applepay/merchant%d:%s", iSectionCount, "MERCHANT_URL");
		pszValue = iniparser_getstring(dict, szKey, NULL);
		if(pszValue == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to Get Merch%d Url from [%s]", __FUNCTION__, iCount, NFC_INI_FILE_NAME);
			APP_TRACE(szDbgMsg);
			rv = ERR_INI_PARSING_FAIL;
			*iMerchIndex = 0;
			break;
		}
		memset(szMerchURL, 0x00, sizeof(szMerchURL));
		strcpy(szMerchURL, pszValue);
		strcpy(pstNfcIniContent->szMerchURL, szMerchURL);


		/*Getting Merchant_VAS_Fliter*/
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "applepay/merchant%d:%s", iSectionCount, "MERCHANT_VAS_FILTER");
		pszValue = iniparser_getstring(dict, szKey, NULL);
		if(pszValue == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to Get Merch%d Filter from [%s]", __FUNCTION__, iCount, NFC_INI_FILE_NAME);
			APP_TRACE(szDbgMsg);
			rv = ERR_INI_PARSING_FAIL;
			*iMerchIndex = 0;
			break;
		}
		memset(szMerchVASFilter, 0x00, sizeof(szMerchVASFilter));
		strcpy(szMerchVASFilter, pszValue);
		strcpy(pstNfcIniContent->szMerchVASFilter, szMerchVASFilter);


		if(iCount == 1)
		{
			pszSections =(NFCINI_CONTENT_INFO_PTYPE)malloc (sizeof(NFCINI_CONTENT_INFO_PTYPE));
			if(pszSections == NULL)
			{
				debug_sprintf(szDbgMsg, "%s:Error in Memory Allocation", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return FAILURE;
				break;
			}

			memset(pszSections, 0x00, sizeof(pszSections));
			pszSections->recList = pstNfcIniContent;
			pszSections->next = NULL;
		}
		else
		{
			pszTemp = pszSections;
			while(pszTemp->next != NULL)
			{
				pszTemp=pszTemp->next;
			}
			pszTemp = (NFCINI_CONTENT_INFO_PTYPE)malloc (sizeof(NFCINI_CONTENT_INFO_STYPE));
			if(pszTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s:Error in Memory Allocation", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return FAILURE;
				break;
			}
			memset(pszTemp, 0x00, sizeof(pszTemp));
			pszTemp->recList=pstNfcIniContent;
			pszTemp->next = NULL;
			if(pszSections->next != NULL)
			{
				pszSections->next->next=pszTemp;
			}
			else
			{
				pszSections->next=pszTemp;
			}
		}
		iCount++;
	}

	if(pszSections != NULL)
	{
		rv = updateQueryNfcIniDtlsForDevTran(szTranKey, pszSections);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to update NFC dtls for Device Tran",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Update NFC Details for Device Transaction");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
		}
	}
	if(dict != NULL)
	{
		iniparser_freedict(dict);
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning- [%d]--", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
#if 0
/*
 * ============================================================================
 * Function Name: updateEmvCfgINIFile
 *
 * Description	: This function will update emvConfig.ini from the metadata
 *
 * Output Params:
 * ============================================================================
 */
int updateEmvCfgINIFile(METADATA_PTYPE pstMetaData)
{
	int				rv					= SUCCESS;
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	listPtr = pstMetaData->keyValList;
	while(1)
	{
		rv = updateAppLstinINI(listPtr);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to update AppLst in ini",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		//Add capkdata here
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;


}
/*
 * ============================================================================
 * Function Name: updateAppLstinINI
 *
 * Description	: This function will update AppLst emvConfig.ini from the metadata
 *
 * Output Params:
 * ============================================================================
 */
int updateAppLstinINI(KEYVAL_PTYPE listPtr)
{
	int						rv					= SUCCESS;
	int						iCnt				= 0;
	int						iTotCnt				= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt = sizeof(appInitReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Value is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "ApplicationData") == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Storing Application Data in emvConfig.ini", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = updateAppDataInfoInINI(listPtr[iCnt].value);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Storing Application Data in emvConfig.ini", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;

}
/*
 * ============================================================================
 * Function Name: updateAppDataInfoinINI
 *
 * Description	: This function will update AppLst emvConfig.ini from the metadata
 *
 * Output Params:
 * ============================================================================
 */
int updateAppDataInfoInINI(VAL_LST_PTYPE pstValLst)
{
	int						rv					= SUCCESS;
	VAL_NODE_PTYPE			nodePtr				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	nodePtr = pstValLst->start;

	while(nodePtr != NULL)
	{
		if(nodePtr->type == 0)
		{
			rv = updateAppDataListINI(nodePtr);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to store FALLBACK_INDICATOR", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid type %d",	__FUNCTION__, nodePtr->type);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		nodePtr = nodePtr->next;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: updateAppDataListINI
 *
 * Description	: This function will update AppLst emvConfig.ini from the metadata
 *
 * Output Params:
 * ============================================================================
 */
int updateAppDataListINI(VAL_NODE_PTYPE pstValNode)
{
	int						rv				= SUCCESS;
	int						iCnt			= 0;
	int						iTotCnt			= 0;
	int						iTemp			= 0;
	char					szTmp[100]		= "";
	char					szKey[100]		= "";
	char					szAID[30]		= "";
	char					szVal[200]		= "";
	char	*				value			= NULL;
	dictionary				*dict			= NULL;
	KEYVAL_PTYPE			listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(SUCCESS != doesFileExist(CFG_INI_FILE_PATH))
		{
			debug_sprintf(szDbgMsg, "%s: %s File not present, ERROR", __FUNCTION__, CFG_INI_FILE_PATH);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		dict = iniparser_load(CFG_INI_FILE_PATH);
		if(dict == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to load ini file [%s]",
					__FUNCTION__, CFG_INI_FILE_PATH);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		listPtr = pstValNode->elemList;
		iTotCnt = pstValNode->elemCnt;
		debug_sprintf(szDbgMsg, "%s: iTotCnt[%d]", __FUNCTION__, iTotCnt);
		APP_TRACE(szDbgMsg);
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			debug_sprintf(szDbgMsg, "%s: --- Inside for[%d], [%s] [%s]", __FUNCTION__, iCnt, (char *)listPtr[iCnt].key, (char*)listPtr[iCnt].value);
			APP_TRACE(szDbgMsg);
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "AID") == SUCCESS)
			{
				memset(szTmp, 0x00, sizeof(szTmp));
				memset(szAID, 0x00, sizeof(szAID));

				strncpy(szAID,  listPtr[iCnt].value, 10);
				strcpy(szTmp,  (char*)(listPtr[iCnt].value));

				if(iniparser_find_entry(dict, szAID) == 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to find [%s] in INI file", __FUNCTION__, szAID);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				else
				{
					memset(szKey, 0x00, sizeof(szKey));
					sprintf(szKey, "%s:%s_%s", szAID, szAID, "SUPPORTED");
					value = iniparser_getstring(dict, szKey, NULL);
					if(value != NULL)
					{
						if(strstr(value, szTmp) == NULL)
						{
							memset(szVal, 0x00, sizeof(szVal));
							strcpy(szVal, value);
							strcat(szVal, ",");
							strcat(szVal, szTmp);
							if(iniparser_set(dict, szKey, szVal) != 0)
							{
								debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
								APP_TRACE(szDbgMsg);
								rv = FAILURE;
								break;
							}
						}
					}
				}
			}
			else if(strcmp(listPtr[iCnt].key, "VerNum") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "TERM_AVN");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "AppName") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "RECMD_AID_NAME");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "TermIdent") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "TERM_ID");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "FloorLimit") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				iTemp = hex_decimal(szVal);
				memset(szVal, 0x00, sizeof(szVal));
				sprintf(szVal, "%d", iTemp);
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "FLR_LIMIT");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "Threshold") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				iTemp = hex_decimal(szVal);
				memset(szVal, 0x00, sizeof(szVal));
				sprintf(szVal, "%d", iTemp);
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "RSTHRESHOLD");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "BelowLimitTerminalCapabilities") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s", "D18", "BELOW_FLR_LIMIT_TERM_CAPABILITIES");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "TargetPercentage") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "TARGET_RS_PERCENT");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "MaxTargetPercentage") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "MAX_TARGET_RS_PERCENT");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "TAC_Denial") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				iTemp = hex_decimal(szVal);
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "TAC_DENIAL");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "TAC_Online") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "TAC_ONLINE");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "TAC_Default") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szVal, "TAC_DEFAULT");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "DefaultTDOL") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				iTemp = hex_decimal(szVal);
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "DEFAULT_TDOL");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "DefaultDDOL") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				iTemp = hex_decimal(szVal);
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "DEFAULT_DDOL");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "MerchIdent") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				iTemp = hex_decimal(szVal);
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "MERCH_ID");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "AdditionalTagsTRM") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				iTemp = hex_decimal(szVal);
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "TRM_DATA_PRESENT");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "AppTermCap") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				iTemp = hex_decimal(szVal);
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "TERM_CAPACITY");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "CountryCodeTerm") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				iTemp = hex_decimal(szVal);
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "COUNTRY_CODE");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "AppTermAddCap") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				iTemp = hex_decimal(szVal);
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "ADD_CAPACITY");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "AppTerminalType") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				iTemp = hex_decimal(szVal);
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "TERM_TYPE");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "FallbackMIDs") == SUCCESS)
			{
				memset(szVal, 0x00, sizeof(szVal));
				strcpy(szVal,  listPtr[iCnt].value);

				iTemp = hex_decimal(szVal);
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s_%s", szAID, szAID, "FALLBACK_ALLOWED_FLG");

				if(iniparser_set(dict, szKey, szVal) != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set [%s] in INI file", __FUNCTION__, szKey);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
		}
		break;
	}
	if(dict != NULL)
	{
		iniparser_freedict(dict);
		dict = NULL;
	}
	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
/*
 * ============================================================================
 * Function Name: loadAppConfigDataFromXMLFile
 *
 * Description	:
 *
 * Output Params:
 * ============================================================================
 */
void loadAppConfigDataFromXMLFile(char* pszFileName)
{
	int				rv 			= SUCCESS;
	METADATA_STYPE	stMetaData;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = getMetaDataForEMVInitReq(&stMetaData, APPLN);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to get metada for appln initialization", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	rv = parseXMLFile(pszFileName, "ApplicationData", "ApplicationData", &stMetaData);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while parsing xml file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	rv = updateEmvCfgINIFile(&stMetaData);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while updating emvConfig.ini file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	freeMetaDataEx(&stMetaData);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: loadKeyDataFromXMLFile
 *
 * Description	:
 *
 * Output Params:
 * ============================================================================
 */
void loadPubkeyDataFromXMLFile(char* pszFileName)
{
	int				rv 			= SUCCESS;
	METADATA_STYPE	stMetaData;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = getMetaDataForEMVInitReq(&stMetaData, KEYS);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to get metada for appln initialization", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	rv = parseXMLFile(pszFileName, "CapKeys", "CapKeys", &stMetaData);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while parsing xml file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	rv = storePubKeyData(&stMetaData);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while storing the pubKey data", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}


	freeMetaDataEx(&stMetaData);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: loadKeyDataFromXMLFile
 *
 * Description	:
 *
 * Output Params:
 * ============================================================================
 */
void loadTermDataFromXMLFile(char* pszFileName)
{
	int				rv 			= SUCCESS;
	METADATA_STYPE	stMetaData;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = getMetaDataForEMVInitReq(&stMetaData, TERMINAL);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to get metada for appln initialization", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	rv = parseXMLFile(pszFileName, "TerminalData", NULL, &stMetaData);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while parsing xml file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	freeMetaDataEx(&stMetaData);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getMetaDataForSSIReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getMetaDataForEMVInitReq(METADATA_PTYPE pstMeta, EMV_INIT_ENUM eType)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((pstMeta == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize the meta data */
		memset(pstMeta, 0x00, METADATA_SIZE);

		switch(eType)
		{
		case APPLN:
			rv = getApplnInitMetaData(pstMeta);
			break;

		case KEYS:
			rv = getPubkeyInitMetaData(pstMeta);
			break;

		case TERMINAL:
			rv = getTermInitMetaData(pstMeta);
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getApplnInitMetaData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getApplnInitMetaData(METADATA_PTYPE pstMeta)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	KEYVAL_PTYPE	srcListPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt 	= sizeof(appInitReqLst) / KEYVAL_SIZE;
		srcListPtr  = appInitReqLst;

		/* Allocate memory for the key list */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Populate the values */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		memcpy(listPtr, srcListPtr, iTotCnt * KEYVAL_SIZE);
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPubkeyInitMetaData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getPubkeyInitMetaData(METADATA_PTYPE pstMeta)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	KEYVAL_PTYPE	srcListPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt 	= sizeof(pubkeyInitReqLst) / KEYVAL_SIZE;
		srcListPtr  = pubkeyInitReqLst;

		/* Allocate memory for the key list */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Populate the values */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		memcpy(listPtr, srcListPtr, iTotCnt * KEYVAL_SIZE);
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTermInitMetaData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getTermInitMetaData(METADATA_PTYPE pstMeta)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	KEYVAL_PTYPE	srcListPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt 	= sizeof(terminalList) / KEYVAL_SIZE;
		srcListPtr  = terminalList;

		/* Allocate memory for the key list */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Populate the values */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		memcpy(listPtr, srcListPtr, iTotCnt * KEYVAL_SIZE);
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

#if 0
/*
 * ============================================================================
 * Function Name: updateTermDtlsinINI
 *
 * Description	: This function will update Term dtls emvConfig.ini from the metadata
 *
 * Output Params:
 * ============================================================================
 */
int updateTermDtlsinINI(KEYVAL_PTYPE listPtr)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iSize			= 0;
	int				iTotCnt			= 0;
	int				iinID			= 0;
	char			szSec[50]		= "";
	char			szVar[100]		= "";
	char			szVal[100]		= "";
	char			szD18[4]		= "D18";
	char			szD19[4]		= "D19";
	char			szD25[4]		= "D25";
	INI_STATUS		iniStatus;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iniStatus = iniLib_loadINIFile(CFG_INI_FILE_PATH, 0, 0);
		if(iniStatus.iRetVal != INI_FILE_LOADED)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Load INI file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		iinID = iniStatus.iINIID;

		if(iniLib_setVar(iinID, szD18, NULL, NULL, 0) == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to create section[%s] INI file", __FUNCTION__, szD18);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		if(iniLib_setVar(iinID, szD20, NULL, NULL, 0) == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to create section[%s] INI file", __FUNCTION__, szD20);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		if(iniLib_setVar(iinID, szD25, NULL, NULL, 0) == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to create section[%s] INI file", __FUNCTION__, szD25);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		iTotCnt = sizeof(terminalList) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "TermCap") == SUCCESS)
			{
				memset(szVar, 0x00, sizeof(szVar));
				sprintf(szVar, "%s","");

				memset(szVal, 0x00, sizeof(szVal));
				strncpy(szVal, listPtr[iCnt].value, 4);

				if(iniLib_setVar(iinID, szD19, (char*)szVar + 1, NULL, 0) == 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to create/update %s", __FUNCTION__, szVar);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
			else if(strcmp(listPtr[iCnt].key, "CurrencyTrans") == SUCCESS)
			{
				memset(szVar, 0x00, sizeof(szVar));
				sprintf(szVar, "%s","");

				memset(szVal, 0x00, sizeof(szVal));
				strncpy(szVal, listPtr[iCnt].value, 4);

				if(iniLib_setVar(iinID, szD19, (char*)szVar + 1, NULL, 0) == 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to create/update %s", __FUNCTION__, szVar);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}

		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif

/*
 * ============================================================================

 * Function Name: storePubKeyData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storePubKeyData(METADATA_PTYPE pstMetaData)
{
	int						rv 					= SUCCESS;
	int						iCnt				= 0;
	int						iTotCnt				= 0;
	KEYVAL_PTYPE			listPtr				= NULL;
	VAL_LST_PTYPE			pstValLst			= NULL;
	VAL_NODE_PTYPE			nodePtr				= NULL;
	EMVKEYLOAD_NODE_STYPE 	stEmvKeyloadNode;
	EMV_CAPK_INFO_STYPE		stCapkInfo;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	listPtr = pstMetaData->keyValList;

	memset(&stCapkInfo, 0x00, sizeof(EMV_CAPK_INFO_STYPE));
	memset(&stEmvKeyloadNode, 0x00, sizeof(EMVKEYLOAD_NODE_STYPE));

	stEmvKeyloadNode.emvKeyLoadType = PUBLIC_KEY;
	stEmvKeyloadNode.emvData		= (void*)&stCapkInfo;

	if(strcmp(listPtr->key, "CapKeys") == SUCCESS)
	{
		pstValLst = listPtr->value;
	}
	else
	{
		return FAILURE;
	}

	nodePtr = pstValLst->start;

	while(nodePtr != NULL)
	{
		listPtr = nodePtr->elemList;
		iTotCnt = nodePtr->elemCnt;
		memset(&stCapkInfo, 0x00, sizeof(MCNT_LI_STYPE));

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "Index") == SUCCESS)
			{
				strcpy(stCapkInfo.szPKIndex, listPtr[iCnt].value);
				stCapkInfo.iPKIndexLen = strlen(stCapkInfo.szPKIndex);
			}
			else if(strcmp(listPtr[iCnt].key, "RID") == SUCCESS)
			{
				strcpy(stCapkInfo.szRID, listPtr[iCnt].value);
				stCapkInfo.iRIDLen	= strlen(stCapkInfo.szRID);
			}
			else if(strcmp(listPtr[iCnt].key, "Key") == SUCCESS)
			{
				strcpy(stCapkInfo.szModulus, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "KeyLen") == SUCCESS)
			{
				stCapkInfo.iModLen = hex_decimal(listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "Exponent") == SUCCESS)
			{
				strcpy(stCapkInfo.szExponent, listPtr[iCnt].value);
				stCapkInfo.iExpLen = strlen(stCapkInfo.szExponent);
			}
			else if(strcmp(listPtr[iCnt].key, "ExpiryDate") == SUCCESS)
			{
				strcpy(stCapkInfo.szExpDate, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "Hash") == SUCCESS)
			{
				strcpy(stCapkInfo.szCheckSum, listPtr[iCnt].value);
				stCapkInfo.iCheckSumLen = strlen(stCapkInfo.szCheckSum);
			}
		}
		updateEmvKeyLoadValues(&stEmvKeyloadNode);
		nodePtr = nodePtr->next;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * End of file emvAPIs.c
 * ============================================================================
 */
