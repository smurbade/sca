/******************************************************************
*                       uiCommUtils.c                             *
*******************************************************************
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>

#include "svc.h"
#include "uiAgent/TCPIP.h"
#include "common/common.h"
#include "common/utils.h"
#include "uiAgent/uiCfgDef.h"
#include "uiAgent/uiControl.h"
#include "appLog/appLogAPIs.h"

/* Extern variables */
extern PAAS_BOOL	gbUIAgentConn;
extern int			giFACommInProgress;
extern PAAS_BOOL		bBLDataRcvd;
extern UIRESP_STYPE		stBLData;

/* Extern functions */
extern int addDataToQ(void *, int, char *);
extern void acquireGlobUIReqMutex(int ,char * );
extern void releaseGlobUIReqMutex(int ,char * );

/* Global static variables */
static char		gszIPAddr[25]	= "";
static char		gszQueueID[10]	= "";

/* Static functions decalarations */
static int	tryCreateClient();
static int	doInitTCPIP();
static int	testUICommunication(int);
static void onSocketDisconnect();
static void onConnectCallbackTCPIP(char *);
static void onTCPIPDataAvailable(uchar *, int, int);

/*
 * ============================================================================
 * Function Name: createUIClient
 *
 * Description	: 
 *
 * Input Params	: UI Q ID, iSource to determine who was the caller function
 * 					if iSource = 1 ; Than, it was called from main thread & we are suppose to wait for response in UI Thread - as conventional
 * 					if iSource = 2 ; Than, it was called from uimanager thread because of some disconnectivity in between & we are trying to reconnect.
 * 									 we are suppose to wait for response in this same UI Thread So, bringing the same piece of code here to parse the response
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int createUIClient(char * szQid, int iSource)
{
	int			rv						= SUCCESS;
	int			iAppLogEnabled			= isAppLogEnabled();
	char		szAppLogData[300]		= "";
	char		szAppLogDiag[300]		= "";
	static 		PAAS_BOOL bFirstTime	= PAAS_TRUE;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* TODO: Implement the queue id logic */
	strcpy(gszQueueID, szQid);

	debug_sprintf(szDbgMsg, "%s: Queue ID = [%s]", __FUNCTION__, gszQueueID);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(bFirstTime == PAAS_TRUE)
		{
			/* Do the initialization of the tcpip library */
			rv = doInitTCPIP();
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to complete first step",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				syslog(LOG_INFO|LOG_USER, "Fail to initialize TCPIP");

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Fail to Initialize TCPIP Library.");
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, szAppLogDiag);
				}


				break;
			}

			syslog(LOG_INFO|LOG_USER, "TCPIP initialized Successfully");

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "TCPIP initialized Successfully");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

			bFirstTime = PAAS_FALSE;
		}

		/* TCPIP library is initiated properly, create a client to connect
		 * to the UI agent */
		rv = tryCreateClient();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create client for UI agent",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Connect to UI Agent.");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, szAppLogDiag);
			}

			break;
		}

		/* Test the connection */
		rv = testUICommunication(iSource);
		if(rv != SUCCESS)
		{
			removeClient(gszIPAddr);

			/*Waiting for 3 seconds and then trying to connect to FA again*/
			svcWait(3000);
			continue;
		}

		break;
	}

	if(rv == SUCCESS)
	{
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "UI Agent Communication  Successful");
			addAppEventLog(SCA, PAAS_INFO, RESPONDED, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sendUIReqMsg
 *
 * Description	:  NEW function
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int sendUIReqMsg(char * szMsgBuf)
{
	int		rv				= 0;
	int		iLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[5100]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	iLen = strlen(szMsgBuf);

	if( (szMsgBuf[iLen - 1] == RS) || (szMsgBuf[iLen - 1] == FS) )
	{
		szMsgBuf[iLen - 1] = ETX;
	}

	/* Add the LRC to the batch message -- Do not include STX or ETX in CRC
	 * calculation */
	szMsgBuf[iLen] = findLrc(szMsgBuf + 1, iLen - 1);

#ifdef DEVDEBUG
	if(strlen(szMsgBuf) < 5000)
	{
		debug_sprintf(szDbgMsg,"%s:%s: Framed XBATCH command [%s]", DEVDEBUGMSG,
						__FUNCTION__, szMsgBuf);
		APP_TRACE(szDbgMsg);
	}
#endif

	//giFACommInProgress = 1;

	//acquireGlobUIReqMutex(__LINE__,(char *)__FUNCTION__);
	/* Send the data using the tcpip library API */
	rv = sendMessage(gszIPAddr, (uchar *)szMsgBuf, iLen + 1, 5, 5);
	//releaseGlobUIReqMutex(__LINE__,(char *)__FUNCTION__);

	//giFACommInProgress = 0;

	if(rv != 0) //Logging only when there is a error case
	{
		debug_sprintf(szDbgMsg,"%s: Returning [%d]", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);
	}

	return rv;
}

//ArjunU1: EMV Testing

/*
 * ============================================================================
 * Function Name: sendUIReqMsg_EX
 *
 * Description	:  NEW function
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int sendUIReqMsg_EX(char * szMsgBuf, int iLen)
{
	int		rv				= 0;
#ifdef DEBUG
	char	szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( (szMsgBuf[iLen - 1] == RS) || (szMsgBuf[iLen - 1] == FS) )
	{
		szMsgBuf[iLen - 1] = ETX;
	}

	/* Add the LRC to the batch message -- Do not include STX or ETX in CRC
	 * calculation */
	szMsgBuf[iLen] = findLrc(szMsgBuf + 1, iLen - 1);

#ifdef DEVDEBUG
	APP_TRACE("Final Framed Command is");
	APP_TRACE_EX(szMsgBuf, iLen+1);
#endif

	//giFACommInProgress = 1;
	//acquireGlobUIReqMutex(__LINE__,(char *)__FUNCTION__);
	/* Send the data using the tcpip library API */
	rv = sendMessage(gszIPAddr, (uchar *)szMsgBuf, iLen + 1, 5, 5);
	//releaseGlobUIReqMutex(__LINE__,(char *)__FUNCTION__);

	//giFACommInProgress = 0;

	debug_sprintf(szDbgMsg,"%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: sendAck
 *
 * Description	: This function sends ACK to FA
 *
 * Input Params	: nothing
 *
 * Output Params: void
 * ============================================================================
 */
void sendACK()
{
	char chACK = ACK;
	postMessage((char *)gszIPAddr, (uchar *)&chACK, 1);
}

/*
 * ============================================================================
 * Function Name: sendNAK
 *
 * Description	: This function sends ACK to FA
 *
 * Input Params	: nothing
 *
 * Output Params: void
 * ============================================================================
 */
void sendNAK()
{
	char chNAK = NAK;
	postMessage((char *)gszIPAddr, (uchar *)&chNAK, 1);
}

/*
 * ============================================================================
 * Function Name: testUICommunication
 *
 * Description	: This function is used for testing the communication with UI
 * 					agent.
 *
 * Input Params	: UI Q ID, iSource to determine who was the caller function
 * 					if iSource = 1 ; Than, it was called from main thread & we are suppose to wait for response in UI Thread - as conventional
 * 					if iSource = 2 ; Than, it was called from uimanager thread because of some disconnectivity in between & we are trying to reconnect.
 * 									 we are suppose to wait for response in this same UI Thread So, bringing the same piece of code here to parse the response
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int testUICommunication(int iSource)
{
	int			rv						= SUCCESS;
	int			iLen					= 0;
	int			iAppLogEnabled			= isAppLogEnabled();
	char		szAppLogData[300]		= "";
	char		szAppLogDiag[300]		= "";
	char		szTmp[25]				= "";
	PAAS_BOOL	bWait					= PAAS_TRUE;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//sprintf(szTmp, "%c72%c", STX, ETX);
	sprintf(szTmp, "%cE00%c", STX, ETX);
	iLen = strlen(szTmp);
	szTmp[iLen] = findLrc(szTmp + 1, iLen - 1);

	rv = sendMessage(gszIPAddr, (uchar *)szTmp, iLen + 1, 5, 3);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Test comm with UI Agent FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Communication Test With UI Agent Failed.");
			strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
			addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
		}

		rv = FAILURE;
	}
	else
	{
		//NOTE: Please refer to the description of this function for this logic
		if(iSource == 2)
		{
			rv = waitForUIResp(gszQueueID);
		}
		else if(iSource == 1)
		{
			while(bWait == PAAS_TRUE)
			{
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait 		= PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
					if(bWait == PAAS_FALSE)
					{
						break;
					}
				}
				svcWait(15);
			}
		}

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: tryCreateClient
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
static int tryCreateClient()
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < 10; iCnt++)
	{
		rv = createClient(gszIPAddr, onConnectCallbackTCPIP);
		if(rv == 1)
		{
			break;
		}
		else if(rv == -1)
		{
			debug_sprintf(szDbgMsg,
						"%s: client creation failed - invalid params passed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			removeClient(gszIPAddr);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Waiting for UI Agent to come up!",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			removeClient(gszIPAddr);
		}

		svcWait(10000);
	}

	if(iCnt >= 10)
	{
		debug_sprintf(szDbgMsg, "%s: Could not create client", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		syslog(LOG_CRIT|LOG_USER, "Could not create client to UI agent");

		rv = FAILURE;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Client for UI agent created Successfully",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		syslog(LOG_INFO|LOG_USER, "Client for UI agent created Successfully");

		rv = SUCCESS;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doInitTCPIP
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
static int doInitTCPIP()
{
	int					rv				= SUCCESS;
	UI_COMPARAM_STYPE	uiComParams;
	TCPIP_INIT_PARAMS	tcpipParams;
#ifdef DEBUG
	char				szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Initialize data structures */
	memset(&uiComParams, 0x00, sizeof(uiComParams));

	while(1)
	{
		/* Get the UI agent communication parameters */
		rv = getUIAgentComParam(&uiComParams);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: Error in getting UI agent com params",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: UI Agent IP=[%s], port=[%d]",
					__FUNCTION__, uiComParams.agentIP, uiComParams.agentPort);
		APP_TRACE(szDbgMsg);

		/* Setting the tcpip library initialization parameters */
		memset(&tcpipParams, 0x00, sizeof(tcpipParams));

		tcpipParams.OnDataAvailable			= onTCPIPDataAvailable;
		tcpipParams.OnSocketDisconnect		= onSocketDisconnect;
		tcpipParams.nMaxSeverConnections	= 9;
		tcpipParams.nMaxClientConnections	= 5;
		tcpipParams.rxProtocolOFF			= PAAS_FALSE;
		/*
		 * Setting the rxProtocolOFF to FALSE to leave for the tcpip library
		 * to take care about ACK NAK protocol.
		 */

		/* Store it globally */
		sprintf(gszIPAddr, "%s:%d", uiComParams.agentIP, uiComParams.agentPort);

		rv = initTCPIP(&tcpipParams, 0);
		if(rv != 1)
		{
			if (rv == 100)
			{
				debug_sprintf(szDbgMsg, "%s: libtcpip init FAILED, nwk open",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: libtcpip init FAILED",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			closeTCPIP();
			rv = FAILURE;
		}
		else
		{
			rv = SUCCESS;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: onTCPIPDataAvailable
 *
 * Description	: This function the vftcpip library to send/receive data
 *
 * Input Params	:
 *
 * Output Params: void
 * ============================================================================
 */
static void onTCPIPDataAvailable(uchar *pchData, int iDataSize, int flags)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[5100]	= "";
#endif

	/*Leaving for the tcpip library to take care of ACK NAK protocol*/
	/*So not sending the ACK*/
//	sendACK();

	debug_sprintf(szDbgMsg, "%s: ---------- TCPIP Data Available ------------",
																__FUNCTION__);
	APP_TRACE(szDbgMsg);

	#ifdef DEVDEBUG
	if(iDataSize < 5000)
	{
		debug_sprintf(szDbgMsg, "%s: Data[%s], Data size[%d]",__FUNCTION__, (char *)pchData, iDataSize);
		APP_TRACE(szDbgMsg);	
	}
	#endif
	
	debug_sprintf(szDbgMsg, "%s: First part: %.6s and its length: %d",
											__FUNCTION__, pchData, strlen((char*)pchData));
	APP_TRACE(szDbgMsg);



	/* Accomodate for the '\0' character for the string */
	rv = addDataToQ(pchData, iDataSize + 1, gszQueueID);
	if(rv == SUCCESS)
	{
		//debug_sprintf(szDbgMsg, "%s: data added to the queue", __FUNCTION__);
		//APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failure in adding data to the queue",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: onSocketDisconnect
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void onSocketDisconnect()
{
	int		iAppLogEnabled			= isAppLogEnabled();
	char	szAppLogData[300]		= "";
//	char	szAppLogDiag[300]		= "";

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: UI Agent Disconnected", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	syslog(LOG_ALERT|LOG_USER, "UI Agent Disconnected");

	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "UI Agent Disconnected");
		addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
	}

	gbUIAgentConn = PAAS_FALSE;

	return;
}

/*
 * ============================================================================
 * Function Name: onConnectCallbackTCPIP
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void onConnectCallbackTCPIP(char * pszIP)
{
	int		iAppLogEnabled			= isAppLogEnabled();
	char	szAppLogData[300]		= "";
//	char	szAppLogDiag[300]		= "";

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Connected to %s", __FUNCTION__, pszIP);
	APP_TRACE(szDbgMsg);

	syslog(LOG_INFO|LOG_USER, "UI Agent is Connected");

	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "Connected to UI Agent");
		addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
	}

	gbUIAgentConn = PAAS_TRUE;

	return;
}

/*
 * ============================================================================
 * End of file uiCommUtils.c
 * ============================================================================
 */
