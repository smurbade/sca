/******************************************************************
*                     uiManager.c                                 *
*******************************************************************
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <svc.h>
#include <setjmp.h>

#include "common/common.h"
#include "common/utils.h"
#include "uiAgent/guimgr.h"
#include "uiAgent/uiCfgDef.h"
#include "uiAgent/uiMsgFmt.h"
#include "uiAgent/uiControl.h"
#include "uiAgent/uiAPIs.h"
#include "uiAgent/ctrlids.h"
#include "bLogic/bLogicCfgDef.h"
#include "bLogic/bLogic.h"
#include "bLogic/blAPIs.h"
#include "sci/sciMain.h"
#include "db/dataSystem.h"
#include "appLog/appLogCfgDef.h"
#include "appLog/appLogAPIs.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

/* Extern function's declarations */
extern int	createUIClient(char *, int);
extern int	sendUIReqMsg(char *);
extern void sendNAK();
extern int	createQueue(char *);
extern void *getDataFrmQ(char *, int *);
extern void initUIAPIs();
extern int 	initEmvIFace();
//extern int initglobUIReqMutex();

//ArjunU1: EMV Testing
extern int	sendUIReqMsg_EX(char *, int);

/* Static data declarations and definitions */
struct advtUpdate
{
	PAAS_BOOL	image;
	PAAS_BOOL	video;
	PAAS_BOOL	volume;
};

PAAS_BOOL		gbUIAgentConn;

int				giLabelLen			= 0;
int				giVolIncBtn			= 0;
int				giVolDcrBtn			= 0;
int				giImgCtrl			= 0;
int				giVidCtrl			= 0;
int				giImgUpdIntvl		= DFLT_IMG_UPD_TIME;
int 			whichForm			= -1;
PAAS_BOOL		bBLDataRcvd			= PAAS_FALSE;
int				giFACommInProgress	= 0;
UIRESP_STYPE	stBLData;

extern int 		bCardDataReceived;

char *			frmName[MAX_FORMS] =
{
	"",
	"PAAS_AMT_ENTRY",
	"PAAS_EMAILSCREEN",
	"PAAS_GEN_CRFM",
	"PAAS_GEN_DISPLAY",
	"PAAS_IDLESCREEN",
	"PAAS_LINEITEMSCREEN",
	"PAAS_LOYALTYSCREEN",
	"PAAS_POS_PIN",
	"PAAS_SELECT_TIP",
	"PAAS_CB_AMOUNT_SCREEN",
	"PAAS_SIG_CAP",
	"PAAS_SWIPE_STI",
	"PAAS_TAP",
	"PAAS_TEND_SEL_SCREEN",
	"FA_XENCDATA",
	"FA_PINE",
	"PAAS_SYSTEM_INFO",
	"PS_NET_OPT",
	"PS_NET_DTLS",
	"PS_IP_ADDRESS_DISPLAY",
	"PS_NUM_ENTRY",
	"PAAS_CUST_QUES_SCREEN1",
	"PAAS_CUST_QUES_SCREEN2",
	"PAAS_SURVEY_SCREEN",
	"PAAS_LINEITEMSCREEN1",
	"PAAS_LINEITEMSCREEN2",
	"PAAS_CHARITY_SCREEN",
	"PAAS_CUSTBTN_SCREEN",
	"PAAS_LANECLOSED_SCREEN",
	"PS_WELCOME_SCREEN",
	"PAAS_PAYPAL_DATA",
	"PAAS_PAYPAL_AMTOK",
	"PAAS_CREDITAPP_QWERTY",
	"PAAS_CREDITAPP_NUMERIC",
	"PAAS_IDLESCREEN_IMG",
	"PS_WELCOME_DUP_SCREEN",
	"PAAS_IDLESCREEN_VID",
	"PAAS_IDLESCREEN_ANI",
	"PAAS_SIG_CAP_EX",
	"PAAS_HOST_SELECTION",
	"PAAS_SWIPE_ST",
	"PAAS_SWIPE_SI",
	"PAAS_SWIPE_S",
	"PAAS_SWIPE_T",
	"PAAS_DCC",
	"PAAS_QRCODE_SCREEN",
	"PAAS_PREAMBLE_STEPS",
	"PAAS_CUSTCHECKBOX"
};

static UIREQ_STYPE			stUIReq;
static UIRESP_STYPE			stUIResp;
static VOLDTLS_STYPE		stVolDtls;
static struct advtUpdate	advtFormStatus[MAX_FORMS] =
{
	/* IMAGE,     VIDEO       VOLUME */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* DUMMY */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* AMT_ENTRY_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* EMAIL_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* GEN_CFRM_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* GEN_DISP_FRM */
	{ PAAS_TRUE , PAAS_TRUE , PAAS_TRUE },	/* IDLE_DISP_FRM */
//  { PAAS_TRUE , PAAS_FALSE, PAAS_FALSE},	/* LI_DISP_FRM */
	{ PAAS_FALSE , PAAS_FALSE, PAAS_FALSE},	/* LI_DISP_FRM */ //Praveen_P1: Currently not updating image on the line item screen
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* LYLTY_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* POS_PIN_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* SEL_TIP_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* SEL_CBACK_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* SIG_CAP_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* SWIPE_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* TAP_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* SEL_TENDER_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* MANUAL_ENTRY_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* FA_PIN_ENTRY_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* SYSINFO_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* NET_CFG_OPT_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* NET_CFG_SET_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* IP_DISP_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* NUM_ENTRY_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* PAAS_CUST_QUES_SCREEN1 */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* PAAS_CUST_QUES_SCREEN2 */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},	/* PAAS_SURVEY_SCREEN */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},  /* LI_DISP_OPT_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},  /* CHARITY_FRM */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},  /* CUST_BUTTON_FRM */
	{ PAAS_FALSE, PAAS_TRUE,  PAAS_TRUE },	/* LANE_CLOSED_SCREEN */
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},  /* WELCOME_SCREEN */ //TODO: Later we need to update image on welcome screen, Set image status to PAAS_TRUE.
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},  /* PAYPAL PAYMENT CODE CAPTURE FORM*/
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},  /* PAYPAL AMOUNT CONFIRMATION OK FORM*/
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},  /* IDLE SCREEN IMG FORM*/
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},  /* WELCOME_DUP_SCREEN */ //TODO: Later we need to update image on welcome screen, Set image status to PAAS_TRUE.
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},  /* IDLE SCREEN VID FORM*/
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE},   /* IDLE SCREEN ANI FORM*/
	{ PAAS_FALSE, PAAS_FALSE, PAAS_FALSE}   /* PROVISION PASS TAP SCREEN ANI FORM*/
};

/*Static global Variables*/
static sigjmp_buf	uiJmpBuf;
static pthread_t	uiMgrId	= 0;

/* extern variables to store status of the FA command response. */

/* static functions' declarations */
static int 		 updateAdvtMedia(short, short, char *);
static int 		 parseNValidateUIResp(uchar *, int, UIRESP_PTYPE);
static void * 	 uiManagerThread(void *);
static PAAS_BOOL processedLocally(UIRESP_PTYPE, char *);
static void 	 uiTimeSigHandler(int);

#if 0
static int doConsumerOption(int );
#endif
typedef struct
{
	char *pszBLQId;
	char *pszUIQId;
}
QueueID ;

/*
 * ============================================================================
 * Function Name: initUIModule
 *
 * Description	: This API must be called before using Any UI module API's.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int initUIModule(char *pszBLQId, char*pszErrDispMsg)
{
	int			rv						= SUCCESS;
//	int			iplatform				= 0;
	char		*pszQueueID				= NULL;
	char		szErrMsg[256]			= "";
//	char		szErrDispMsg[512]		= "";	// this buffer will store the final error message to be displayed on screen.
	char		szUIQueueId[10]			= "";	// use this local copy of UI Q Id for this function
//	char		szComPinFormName[40]	= "";
//	char		szCmd[100]				= "";
//	char		szFormPath[10]			= "";
//	char		szFAPinFormName[40]		= "";
//	char		szOptPinFormName[40]	= "";
	QueueID     *pstQId;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Load the configuration parameters into the application memory */
		rv	= loadUIConfigParams();
		if(rv != SUCCESS)
		{
			/* Replacing debug_sprintf with sprintf so that syslogs get printed
			 * even with the execution of release version of the application. */
			sprintf(szErrMsg,"%s: FAILED to load UI config params",__FUNCTION__);
			APP_TRACE(szErrMsg);
			syslog(LOG_ERR|LOG_USER, szErrMsg);

			break;
		}

		/*
		 * Allocating memory here since we are passing this reference to
		 * the UIManager thread, so this reference should exist
		 * after this function call(initUIModule) also for
		 * UIManger to take the arguments..
		 * If it is static declaration, UIManager can get NULL arguments
		 * if it starts after execution of initUIModule function
		 */
		pszQueueID = (char *)malloc(sizeof(char) * 15);
		if(pszQueueID == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc failed for Queue ID", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Creating Queue between UI Module and UI Agent", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Create the queue to be used for receiving UI responses */
		rv = createQueue(pszQueueID);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create queue for UI agent",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			// CID-67289, 67289: 25-Jan-16: MukeshS3: freeing the memory allocated to pszQueueID in case of FAILURE
			if(pszQueueID != NULL)
			{
				free(pszQueueID);
				pszQueueID = NULL;
			}
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Queue ID between UI Module and UI Agent [%s]", __FUNCTION__, pszQueueID);
		APP_TRACE(szDbgMsg);

		pstQId = (QueueID *)malloc(sizeof(QueueID));
		if(pstQId == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc failed for Queue ID struct", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			// CID 67289 (#1 of 1): Resource leak (RESOURCE_LEAK). T_RaghavendranR1. Freed the Allocated memory before leaving the function
			if(pszQueueID != NULL)
			{
				free(pszQueueID);
				pszQueueID = NULL;
			}
			rv = FAILURE;
			break;
		}

		memset(pstQId, 0x00, sizeof(QueueID));
		pstQId->pszBLQId = pszBLQId;
		pstQId->pszUIQId = pszQueueID;
		strcpy(szUIQueueId, pszQueueID);	// use this local copy of UI Q Id for this function
		


		/* Initializing the UI request data
		 *
		 * WARNING: Please do not memset the whole UI request data anywhere in
		 * the code. Doing that will lose the buffer present inside, which
		 * will adversely affect the module, thus creating bugs */
		memset(&stUIReq, 0x00, sizeof(UIREQ_STYPE));

		/* Initialize the message for the UI request message */
		initUIReqMsg(&stUIReq, INIT_REQ);

		/* Initializing the UI response data
		 *
		 * WARNING: Please do not memset the whole UI response data anywhere in
		 * the code. Doing that will overwrite the mutex present inside, which
		 * will adversely affect the module, thus creating bugs */
		memset(&stUIResp, 0x00, sizeof(UIRESP_STYPE));

		/* Initializing the mutex for UI response. */
		pthread_mutex_init(&(stUIResp.uiRespMutex), NULL);

#if 0
		rv = initglobUIReqMutex();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to initializa the Global UI req mutex",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
#endif
		/* Initializing the BL data (UI response to be stored here and provided
		 * to business Logic)
		 *
		 * WARNING: Please do not memset the whole UI response data anywhere in
		 * the code. Doing that will overwrite the mutex present inside, which
		 * will adversely affect the module, thus creating bugs */
		memset(&stBLData, 0x00, sizeof(UIRESP_STYPE));

		/* Initializing the mutex for UI response. */
		pthread_mutex_init(&(stBLData.uiRespMutex), NULL);

		debug_sprintf(szDbgMsg, "%s: Queue Id = [%s]", __FUNCTION__, pszQueueID);
		APP_TRACE(szDbgMsg);

		gbUIAgentConn = PAAS_TRUE;	// making it true to initialize UI client by main thread for first time
		/* Create the ui manager thread */

		//rv = pthread_create(&uiMgrId, NULL, uiManagerThread, (void *)pszQueueID);
		rv = pthread_create(&uiMgrId, NULL, uiManagerThread, (void *)pstQId);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create ui manager thread",
						   										__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		giLabelLen = 35;
		initUIAPIs();
		/* Create a client for the ui agent */
		rv = createUIClient(szUIQueueId, 1);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create client for UI agent",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			// CID-67390: 25-Jan-16: MukeshS3: freeing the memory allocated to pstQId & pszQueueID in case of FAILURE
			if(pszQueueID != NULL)
			{
				free(pszQueueID);
			}
			if(pstQId != NULL)
			{
				free(pstQId);
			}
			//pstQId->pszUIQId = NULL;
			break;
		}

		/* Load the UI Data required into the application memory */
		rv	= loadUIData(pszErrDispMsg);
		if(rv != SUCCESS)
		{
			/* Replacing debug_sprintf with sprintf so that syslogs get printed
			 * even with the execution of release version of the application. */
			sprintf(szErrMsg,"%s: FAILED to load Required UI Data",__FUNCTION__);
			APP_TRACE(szErrMsg);
			syslog(LOG_ERR|LOG_USER, szErrMsg);
			break;
		}

		/*
		 * Initializing EMV Agent as this agent would be needed for reading chip card
		 * and other card data and also displaying screens on the device and thus
		 * interacting with the user.
		 */
		rv = initEmvIFace();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to initialize EMV module",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			//MukeshS3: FRD 3.79
			showMessageBox("Critical Error", "Failed To Initialize EMV Module");
			break;
		}

		/*
		 * Following function sets all the required config variables in the
		 * UI agent application that are required by SCA application
		 */
		rv = initUIAgent();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to init UI agent application",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

#if 0
		/*
		 * Praveen_P1: We are not using FA_PINE form now, so commenting
		 * the below part of the code
		 * we are using XPI pin entry form since for payment processing
		 * we are using XPI now
		 */
		memset(szFAPinFormName, 0x00, sizeof(szFAPinFormName));
		memset(szComPinFormName, 0x00, sizeof(szComPinFormName));
		memset(szOptPinFormName, 0x00, sizeof(szOptPinFormName));
		memset(szFormPath, 0x00, sizeof(szFormPath));

		iplatform = getDevicePlatform();

		strcpy(szFormPath, "./flash");

		sprintf(szFAPinFormName, "%s/%d_%s", szFormPath, iplatform, "FA_PINE.FRM");
		sprintf(szComPinFormName, "%s/%d_%s", szFormPath, iplatform, "FA_PINE_2.FRM");
		sprintf(szOptPinFormName, "%s/%d_%s", szFormPath, iplatform, "FA_PINE_1.FRM");

		if(isSigOptionReqOnPINEntryScreen() == PAAS_FALSE)
		{
			debug_sprintf(szDbgMsg, "%s: Signature Option is NOT Required on the PIN Entry Screen",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Original FA Pine Form[%s], Compulsory FA Pine Form[%s[",__FUNCTION__, szFAPinFormName, szComPinFormName);
			APP_TRACE(szDbgMsg);

			memset(szCmd, 0x00, sizeof(szCmd));
			sprintf(szCmd, "cp %s %s", szComPinFormName, szFAPinFormName);

			debug_sprintf(szDbgMsg, "%s: Command to Execute [%s]", __FUNCTION__, szCmd);
			APP_TRACE(szDbgMsg);

			rv = local_svcSystem(szCmd);
			if(rv != SUCCESS)
			{
#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s: FAILED to execute [%s] rv = [%d]",
											DEVDEBUGMSG, __FUNCTION__, szCmd, rv);
#else
				debug_sprintf(szDbgMsg,"%s: FAILED to Copy %s file",__FUNCTION__, szComPinFormName);
#endif
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			/*
			 * Changing the form for this case may not required since default form(FA_PINE.FRM) will have button on that
			 * but when they are flipping from one to another on the same terminal will be problem
			 * thats why copying explicitly for this case also
			 */
			debug_sprintf(szDbgMsg, "%s: Signature Option is Required on the PIN Entry Screen",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Original FA Pine Form[%s], Optional FA Pine Form [%s]",__FUNCTION__, szFAPinFormName, szOptPinFormName);
			APP_TRACE(szDbgMsg);

			memset(szCmd, 0x00, sizeof(szCmd));
			sprintf(szCmd, "cp %s %s", szOptPinFormName, szFAPinFormName);

			debug_sprintf(szDbgMsg, "%s: Command to Execute [%s]", __FUNCTION__, szCmd);
			APP_TRACE(szDbgMsg);

			rv = local_svcSystem(szCmd);
			if(rv != SUCCESS)
			{
#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s: FAILED to execute [%s] rv = [%d]",
											DEVDEBUGMSG, __FUNCTION__, szCmd, rv);
#else
				debug_sprintf(szDbgMsg,"%s: FAILED to Copy %s file",__FUNCTION__, szOptPinFormName);
#endif
				APP_TRACE(szDbgMsg);
			}

		}
#endif
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: isUIDataEmvResp
 *
 * Description	: This function will compare the response type and tell if it is
 * 				  UI Data response field which needs to be honoured on  Line Item
 * 				  Screen.
 *
 * Input Params	: none
 *
 * Output Params: NULL
 * ============================================================================
 */
static PAAS_BOOL isUIDataEmvResp(char* szEMVResp)
{
	if(!strcmp(szEMVResp, EMV_C31_RESP) ||
	   !strcmp(szEMVResp, EMV_U01_REQ)	||
	   !strcmp(szEMVResp, EMV_U02_REQ)	||
	   !strcmp(szEMVResp, EMV_U00_REQ))
	{
		return PAAS_TRUE;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/*
 * ============================================================================
 * Function Name: uiManagerThread
 *
 * Description	: This thread will help manage the User Interface of the
 * 					application. The responsibilities of this thread would be
 * 					to update the media on the screen, to receive responses
 * 					from the UI agent and act upon it, either passing the
 * 					parsed details to business Logic or process the response
 * 					itself.
 *
 * Input Params	: none
 *
 * Output Params: NULL
 * ============================================================================
 */
static void * uiManagerThread(void * arg)
{
	int				rv				= SUCCESS;
	int				locFrmNo		= 0;
	int				iSize			= 0;
	int				iVal			= 0;
	int				iCount			= 4;
	char			szUIQueueId[10]	= "";
	char			szBLQueueId[10]	= "";
	uchar *			pszRespMsg		= NULL;
	ullong			curTime			= 0L;
	ullong			endTime			= 0L;
	QueueID     	*pstQId;
	BLDATA_STYPE	stUIBLData;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[256]	= "";


#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Thread Running", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Add signal handler for SIGUITIME SETTIME */
	if(SIG_ERR == signal(SIGUITIME, uiTimeSigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGUITIME",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	pstQId = (QueueID *)arg;

	if(pstQId != NULL)
	{
		/* Get the queue Id for the thread */
		strcpy(szUIQueueId, pstQId->pszUIQId);

		debug_sprintf(szDbgMsg, "%s: UI Queue Id = [%s]", __FUNCTION__, szUIQueueId);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Error while accessing the queue id", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if(pstQId != NULL)
	{
		strcpy(szBLQueueId, pstQId->pszBLQId);

		debug_sprintf(szDbgMsg, "%s: BL Queue Id = [%s]", __FUNCTION__, szBLQueueId);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Error while accessing the queue id", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/*
	 * Memory is allocated for this buffer before passing arg to this thread
	 * copied to the local buffer, freeing the memory
	 */
	if(pstQId != NULL)
	{
		if((char *)pstQId->pszUIQId != NULL)
		{
			//free((char *)pstQId->pszUIQId);
			free(pstQId->pszUIQId);
		}
		free(pstQId);
	}

	iVal = sigsetjmp(uiJmpBuf, 1);
	if(iVal == 1)
	{
		debug_sprintf(szDbgMsg, "%s: Change in Time. Resetting the timer of image updation interval",
								__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* Initialize the timers */
	curTime = svcGetSysMillisec();
	endTime = curTime + 5000L;

	locFrmNo = whichForm;

	/* The thread should execute infinitely in this loop */
	while(1)
	{
		curTime = svcGetSysMillisec();

		/* Check for any response from the UI agent */
		pszRespMsg = getDataFrmQ(szUIQueueId, &iSize);
		if(pszRespMsg != NULL)
		{
			/* Get the Mutex Lock on stUIResp */
			acquireMutexLock(&(stUIResp.uiRespMutex), __FUNCTION__);

			/* Parse and validate the incoming response from UI agent */
			rv = parseNValidateUIResp(pszRespMsg, iSize, &stUIResp);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Message", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(stUIResp.uiRespType == UI_IGNORE)
			{
				debug_sprintf(szDbgMsg,"%s: Ignoring UI resp", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if((((stUIResp.uiRespType == UI_XEVT_RESP) && (stUIResp.stRespDtls.stXEvtInfo.uiCtrlType == 2 /*Button type control*/))
					   &&
					((!strcmp(stUIResp.stRespDtls.stXEvtInfo.szFrmName, "PAAS_LINEITEMSCREEN")) || (!strcmp(stUIResp.stRespDtls.stXEvtInfo.szFrmName, "PAAS_LINEITEMSCREEN1"))
					|| (!strcmp(stUIResp.stRespDtls.stXEvtInfo.szFrmName, "PAAS_LINEITEMSCREEN2")) || (!strcmp(stUIResp.stRespDtls.stXEvtInfo.szFrmName, "PS_WELCOME_SCREEN"))
					|| (!strcmp(stUIResp.stRespDtls.stXEvtInfo.szFrmName, "PS_WELCOME_DUP_SCREEN"))
					|| (!strcmp(stUIResp.stRespDtls.stXEvtInfo.szFrmName, "PAAS_QRCODE_SCREEN"))))
					||
					( (stUIResp.uiRespType == UI_R01_RESP || stUIResp.uiRespType == UI_CARD_RESP || (stUIResp.uiRespType == UI_EMV_RESP && isUIDataEmvResp(stUIResp.stRespDtls.stEmvDtls.szEMVRespCmd)))
							&& (whichForm == LI_DISP_OPT_FRM || whichForm == LI_FULL_DISP_FRM || whichForm == LI_DISP_FRM || whichForm == WELCOME_DISP_FRM || whichForm == WELCOME_DUP_DISP_FRM || whichForm == QRCODE_DISP_FRM)
							&& (getOnlineTranStatus() == PAAS_FALSE)) )
			{
				if(stUIResp.uiRespType == UI_XEVT_RESP)
				{
					debug_sprintf(szDbgMsg,"%s: Consumer Option/Cancel button on Line item/Weclome, need to send data to BL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					memset(&stUIBLData, 0x00, sizeof(stUIBLData));

					stUIBLData.iEvtType = UIDATA;

					stUIBLData.stUIData.iDataType = UI_XEVT_DATA_TYPE;

					stUIBLData.stUIData.iCtrlId = stUIResp.stRespDtls.stXEvtInfo.uiCtrlID;
				}
				else if(stUIResp.uiRespType == UI_CARD_RESP)
				{
					memset(&stUIBLData, 0x00, sizeof(stUIBLData));
					debug_sprintf(szDbgMsg,"%s: Received card data from the line item screen", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					stUIBLData.iEvtType = UIDATA;

					stUIBLData.stUIData.iDataType = UI_SWIPE_DATA_TYPE;

					setCardDataInBQueueFlag(PAAS_TRUE);

					memset(&stUIBLData.stUIData.stCrdTrkInfo, 0x00, sizeof(CARD_TRK_STYPE));
					memcpy(&stUIBLData.stUIData.stCrdTrkInfo, &stUIResp.stRespDtls.stCrdTrkInfo, sizeof(CARD_TRK_STYPE));
				}
				else if(stUIResp.uiRespType == UI_EMV_RESP)
				{
					memset(&stUIBLData, 0x00, sizeof(stUIBLData));
					debug_sprintf(szDbgMsg,"%s: Received EMV data from the line item screen", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					stUIBLData.iEvtType = UIDATA;

					stUIBLData.stUIData.iDataType = UI_EMV_DATA_TYPE;

					setCardDataInBQueueFlag(PAAS_TRUE);

					iCount = 4;
					while(stUIResp.stRespDtls.stEmvDtls.stEMVCardStatusU02.bSTBDataReceived  &&
							bCardDataReceived == PAAS_FALSE && iCount)
					{
						svcWait(5);
						iCount--;
					}

					memset(&stUIBLData.stUIData.stEmvDtls, 0x00, sizeof(EMVDTLS_STYPE));
					memcpy(&stUIBLData.stUIData.stEmvDtls, &stUIResp.stRespDtls.stEmvDtls, sizeof(EMVDTLS_STYPE));
				}
				else if(stUIResp.uiRespType == UI_R01_RESP)
				{
					memset(&stUIBLData, 0x00, sizeof(stUIBLData));
					debug_sprintf(szDbgMsg,"%s: Received R01 Unsolicited Response", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					stUIBLData.iEvtType = UIDATA;

					/* AjayS2:
					 * Setting the ctrlId as if user has press cancel button, since the flushing of card dtls
					 * now will be done and taken care by the doConsumerOptCature thread.
					 */
					stUIBLData.stUIData.iDataType = UI_R01_DATA_TYPE;
					stUIBLData.stUIData.iCtrlId = 23;
					strcpy(stUIBLData.stUIData.stEmvDtls.szEMVRespCmd, "R01");
				}

				if(stUIBLData.iEvtType == UIDATA)
				{
					/* Queue the Business Logic data node for the business logic process
					 * to look into and subsequently process */
					rv = addDataToQ(&stUIBLData, sizeof(stUIBLData), szBLQueueId);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to add BL tran to queue",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						//deleteTran(stBLData.szTranKey);
					}
				}
			}
			else if(PAAS_TRUE != processedLocally(&stUIResp, szUIQueueId))
			{
				/* Notify globally for the data receipt from UI agent. There
				 * might be some business Logic thread waiting for the data */

				/*KranthiK1: Added this so uithread waits till Card data is being parsed*/
				iCount = 4;
				while(bBLDataRcvd && stUIResp.stRespDtls.stEmvDtls.stEMVCardStatusU02.bSTBDataReceived
						&& iCount)
				{
					if(iAppLogEnabled)
					{
						strcpy(szAppLogData, "Received STB data and waiting at uimanager thread");
						addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
					}
					svcWait(5);
					iCount--;
				}

				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				
				/* Copy the UI response in the BL data */
				stBLData.iStatus = stUIResp.iStatus;
				stBLData.uiRespType = stUIResp.uiRespType;

				memset(&(stBLData.stRespDtls), 0x00, sizeof(ALLRESP_STYPE));

				memcpy(&(stBLData.stRespDtls), &(stUIResp.stRespDtls),
														sizeof(ALLRESP_STYPE));

				/* Set the flag for the business logic thread to notice the
				 * presence of data */
				bBLDataRcvd = PAAS_TRUE;

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
/*			else
			{
				debug_sprintf(szDbgMsg, "%s: Processed locally returned True", __FUNCTION__, bBLDataRcvd);
				APP_TRACE(szDbgMsg);

				debug_sprintf(szDbgMsg, "%s: bBLDataRcvd %d Setting it now to 0", __FUNCTION__, bBLDataRcvd);
				APP_TRACE(szDbgMsg);

				 Set the flag for the business logic thread to notice the
				 * presence of data
				bBLDataRcvd = PAAS_FALSE;
			}*/

			/* Release the Mutex Lock on stUIResp */
			releaseMutexLock(&(stUIResp.uiRespMutex), __FUNCTION__);

			free(pszRespMsg);
			pszRespMsg = NULL;
		}

		/* Check if uiAgent is still connected */
		if(PAAS_FALSE == gbUIAgentConn)
		{
			while(gbUIAgentConn != PAAS_TRUE)
			{
				debug_sprintf(szDbgMsg, "%s: UI manager thread creating client again...",
										__FUNCTION__);
				APP_TRACE(szDbgMsg);
				createUIClient(szUIQueueId, 2);
			}
		}

		/* Check if the image update interval needs to be reset. This can
		 * happen if some other form is displayed which has image update
		 * enabled and has a different update interval */
		if( (whichForm != -1) && (locFrmNo != whichForm) &&
						(advtFormStatus[whichForm].image == PAAS_TRUE) )
		{
				locFrmNo = whichForm;
				curTime = svcGetSysMillisec();
				endTime = curTime + (giImgUpdIntvl * 1000L);
		}


		/* Check if image needs to be updated */
		if(curTime >= endTime)
		{
			if(whichForm != -1)
			{
				if(advtFormStatus[whichForm].image == PAAS_TRUE)
				{
					updateAdvtMedia(IMAGE, giImgCtrl, szUIQueueId);
				}

				/* Start the timer again */
				locFrmNo = whichForm;
				curTime = svcGetSysMillisec();
				endTime = curTime + (giImgUpdIntvl * 1000L);
			}
			else
			{
				curTime = svcGetSysMillisec();
				endTime = curTime + 5000L;
			}
		}

		svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
	}

	return NULL;
}

/*
 * ============================================================================
 * Function Name: processedLocally
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: NULL
 * ============================================================================
 */
static PAAS_BOOL processedLocally(UIRESP_PTYPE pstUIResp, char *szUIQueueId)
{
	PAAS_BOOL	bRv				= PAAS_TRUE;
	XEVT_PTYPE	pstXevtDtls		= NULL;
	//int			iRv				= 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Daivik: Coverity 67374 - pstUIResp which is passed to this function is address of a static structure that cannot be NULL */

	if(pstUIResp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s:Should Not come here Param NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return bRv;
	}

	while(1)
	{
		switch(pstUIResp->uiRespType)
		{
		case UI_XEVT_RESP:

			/* Get the XEVT details */
			pstXevtDtls = &(pstUIResp->stRespDtls.stXEvtInfo);

			if(pstXevtDtls->uiCtrlType == 17)
			{
				if(pstXevtDtls->uiVidEvt == 2)
				{
					/* Video needs to be updated */
					debug_sprintf(szDbgMsg, "%s: Video update needed",__FUNCTION__);
					APP_TRACE(szDbgMsg);

					updateAdvtMedia(VIDEO, pstXevtDtls->uiCtrlID, szUIQueueId);
				}
				else if(pstXevtDtls->uiVidEvt == 1)
				{
					/* Video needs to be updated */
					debug_sprintf(szDbgMsg, "%s: Touch Event on Video Screen",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else if(!strcmp(pstXevtDtls->szFrmName, frmName[whichForm]) &&
							(advtFormStatus[whichForm].volume == PAAS_TRUE) )
			{
				if(pstXevtDtls->uiCtrlID == giVolIncBtn)
				{
					debug_sprintf(szDbgMsg, "%s: Vol increase needed",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					/* Increase the volume */
					updateAdvtMedia(VOLUME, 1, szUIQueueId);
				}
				else if(pstXevtDtls->uiCtrlID == giVolDcrBtn)
				{
					debug_sprintf(szDbgMsg, "%s: Vol decrease needed",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					/* Decrease the volume */
					updateAdvtMedia(VOLUME, 2, szUIQueueId);
				}
				else
				{
					/* Business Logic thread should have a look at the data */
					bRv = PAAS_FALSE;
				}
			}
#if 0
			else if(!strcmp(pstXevtDtls->szFrmName, "PAAS_LINEITEMSCREEN1") &&
					pstXevtDtls->uiCtrlType == 2)//Button type control
			{
				debug_sprintf(szDbgMsg, "%s: Button pressed on the Line Item screen", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRv = doConsumerOption(pstXevtDtls->uiCtrlID);
				if(iRv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failure while doing consumer option", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

				/* Showing line item screen after capturing the Consumer option */
				if(isLineItemDisplayEnabled())
				{
					showLineItemScreen();
				}
				else
				{
					showWelcomeScreen(PAAS_FALSE);
				}

			}
#endif
			else
			{
				/* Business Logic thread should have a look at the data */
				bRv = PAAS_FALSE;
			}

			break;

		case UI_VOL_RESP:
			debug_sprintf(szDbgMsg, "%s: Storing volume details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			memcpy(&stVolDtls, &(pstUIResp->stRespDtls.stVolInfo),
														sizeof(VOLDTLS_STYPE));
			break;

		default:
			/* Business Logic thread should have a look at the data */
			bRv = PAAS_FALSE;
			break;
		}
#if 0
		if(whichForm != -1 && strcmp(frmName[whichForm], "PAAS_IDLESCREEN") == SUCCESS)
		{
			bRv = PAAS_TRUE;
		}
#endif

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__,
										(bRv == PAAS_TRUE) ? "TRUE": "FALSE");
	APP_TRACE(szDbgMsg);

	return bRv;
}

/*
 * ============================================================================
 * Function Name: parseNValidateUIResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: NULL
 * ============================================================================
 */
static int parseNValidateUIResp(uchar * pszMsg, int len, UIRESP_PTYPE pstUIResp)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[4096]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	if(strlen((char *)pszMsg) < 4000)
	{
		debug_sprintf(szDbgMsg, "%s:%s: UI resp = [%s]", DEVDEBUGMSG, __FUNCTION__, (char *)pszMsg);
		APP_TRACE(szDbgMsg);
	}
#endif

	while(1)
	{
		/* If received NAK please ignore */
		if( (pszMsg[0] == NAK) || (pszMsg[0] == ACK) )
		{
			rv = FAILURE;
			break;
		}

		/*
		 * Praveen_P1: ACK/NAK protocol is handled by the TCPIP library
		 * so not doing the LRC check here
		 */
#if 0
		/* Check for any possible LRC error in the message */
		msgLRCChar = pszMsg[len - 1];
		locLRCChar = findLrc((char *)(pszMsg + 1), len - 2);

		if(msgLRCChar != locLRCChar)
		{
			/* LRC Error: Ignore the message and send NAK: VDR */
			debug_sprintf(szDbgMsg,
							"%s: LRC Mismatch (%d vs %d), ignoring message",
							__FUNCTION__, msgLRCChar, locLRCChar);
			APP_TRACE(szDbgMsg);

			sendNAK();

			rv = FAILURE;
			break;
		}
#endif
		/* Parse the UI agent response for any meaningfull data */
		rv = parseUIAgentResp(pszMsg + 1, len - 2, pstUIResp);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to meaningfully parse response",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	//debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	//APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateAdvtMedia
 *
 * Description	: This API would be called to update media content on device
 * 					screen.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int updateAdvtMedia(short iOpt, short iCtrl, char *szUIQueueId)
{
	int				rv				= SUCCESS;
	char			advtFile[128]	= "";
	int				iSize			= 0;
	uchar *			pszRespMsg		= NULL;
	UIRESP_STYPE	stLocUIResp;

#ifdef DEBUG
	char szDbgMsg[1024]		= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	initUIReqMsg(&stUIReq, BATCH_REQ);

	switch(iOpt)
	{
	case VIDEO:
		/* Video File update */

		if(SUCCESS == getNxtVideoFileName(advtFile))
		{
			debug_sprintf(szDbgMsg, "%s: Nxt vid = [%s]",__FUNCTION__,advtFile);
			APP_TRACE(szDbgMsg);

			setStringValueWrapper(iCtrl, PROP_STR_VIDEO_FILE_NAME, advtFile,
															stUIReq.pszBuf);
			showFormWrapper(PM_NORMAL, stUIReq.pszBuf);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Didn't get  next video to play ..!!",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
		break;

	case IMAGE:
		/* Image File update */

		if(SUCCESS == getNxtImageFileName(advtFile, whichForm))
		{
			debug_sprintf(szDbgMsg, "%s: Nxt img = [%s]",__FUNCTION__,advtFile);
			APP_TRACE(szDbgMsg);

			setStringValueWrapper(iCtrl, PROP_STR_IMAGE_FILE_NAME, advtFile,
															stUIReq.pszBuf);
			showFormWrapper(PM_NORMAL, stUIReq.pszBuf);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Didn't get  next image to show ..!!",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}

		break;

	case VOLUME:
		/* Volume Update */

		switch(iCtrl)
		{
		case 1:
			/* Increase Volume */
			if(stVolDtls.iVol + 5 > 100)
			{
				stVolDtls.iVol += 100 - stVolDtls.iVol;
			}
			else
			{
				stVolDtls.iVol += 5;
			}
			break;

		case 2:
			/* Decrease Volume */
			if(stVolDtls.iVol - 5 < 0)
			{
				stVolDtls.iVol = 0;
			}
			else
			{
				stVolDtls.iVol -= 5;
			}
			break;
		}

		setCurVolWrapper(stVolDtls.iVol, stVolDtls.iBass, stVolDtls.iTrbl,
															stUIReq.pszBuf);
		break;
	}

	if(rv == SUCCESS)
	{
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		while(1)
		{
			/* Check for any response from the UI agent */
			pszRespMsg = getDataFrmQ(szUIQueueId, &iSize);
			if(pszRespMsg != NULL)
			{
				memset(&stLocUIResp, 0x00, sizeof(UIRESP_STYPE));

				/* Parse and validate the incoming response from UI agent */
				rv = parseNValidateUIResp(pszRespMsg, iSize, &stLocUIResp);

				free(pszRespMsg); //Praveen_P1: We are not freeing this memory which leads to memory leak in the application, so freeing it
				pszRespMsg = NULL;

				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Invalid Message", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else if(stLocUIResp.uiRespType == UI_GENRL_RESP)
				{
					debug_sprintf(szDbgMsg,"%s: Ignoring UI resp", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}

			}
			svcWait(5);
		}
	}

	//debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	//APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: waitForUIResp
 *
 * Description	: This API will wait for FA response. Once FA sends the response it will read it from UI queue & parse the response for
 * 					SUCCESS or FAILURE.
 *
 * Input Params	: UI Q ID - Do Not Change this
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int waitForUIResp(char *szQId)
{
	int				rv					= SUCCESS;
	int				iSize 				= 0;
	uchar *			pszRespMsg			= NULL;
	PAAS_BOOL		bWait				= PAAS_TRUE;
	//UIRESP_STYPE	stUIResp;
#ifdef DEBUG
	char szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ----- Enter ------", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(bWait == PAAS_TRUE)
	{
		/* Check for any response from the UI agent */
		pszRespMsg = getDataFrmQ(szQId, &iSize);
		if(pszRespMsg != NULL)
		{
			acquireMutexLock(&(stUIResp.uiRespMutex), __FUNCTION__);

			memset(&stUIResp, 0x00, sizeof(UIRESP_STYPE));
			/* Parse and validate the incoming response from UI agent */
			rv = parseNValidateUIResp(pszRespMsg, iSize, &stUIResp);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Message", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(stUIResp.uiRespType == UI_GENRL_RESP)
			{
				debug_sprintf(szDbgMsg,"%s: Got UI General resp [%d]", __FUNCTION__, stUIResp.iStatus);
				APP_TRACE(szDbgMsg);
				if(stUIResp.iStatus == SUCCESS)
				{
					rv = SUCCESS;
				}
				else
				{
					rv = FAILURE;
				}
				bWait 		= PAAS_FALSE;
			}
			else
			{
				rv = FAILURE;
			}

			releaseMutexLock(&(stUIResp.uiRespMutex), __FUNCTION__);

			free(pszRespMsg);
			pszRespMsg = NULL;
			if(bWait == PAAS_FALSE)
			{
				break;
			}
		}
		svcWait(15);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
/*
 * ============================================================================
 * Function Name: uiSigHandler
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static void uiTimeSigHandler(int iSigNo)
{
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(iSigNo == SIGUITIME)
	{
		debug_sprintf(szDbgMsg, "%s: Signal SIGUITIME delivered", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		siglongjmp(uiJmpBuf, 1);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Unknown signal", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	return;
}

/*
 * ============================================================================
 * Function Name: resetUIImageUpdationTimer
 *
 * Description	: Sends the signal to UI thread to update image on idle screen
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
int resetUIImageUpdationTimer()
{
	int		iRetVal;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: UiManager thread id: %d", __FUNCTION__, (int)uiMgrId);
	APP_TRACE(szDbgMsg);

	iRetVal = pthread_kill(uiMgrId, SIGUITIME);

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);
	return iRetVal;
}


/*
 * ============================================================================
 * End of file uiManager.c
 * ============================================================================
 */
