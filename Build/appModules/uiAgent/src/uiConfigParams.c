/******************************************************************
*                       uiConfigParams.c                          *
*******************************************************************
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis which loads and reads the 		  *
* 			   config parameters required for the UI Module       *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                kranthik1                                        *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>
#include <svc.h>
#include<ctype.h>


#include "common/cfgData.h"
#include "uiAgent/uiCfgData.h"
#include "uiAgent/uiCfgDef.h"
#include "common/utils.h"
#include "appLog/appLogAPIs.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

static UI_CFG_STYPE uiSettings;


static int 	getSignatureSettings();
static int	loadMediaSettings(char*);
static void initializeUISettings();
static int 	loadAdvtMedia(char *, MEDIA_UPD_PTYPE);
static int  loadIdleScrnModeValue(char *);
static int  loadDispMsgsAndPrompts(char*);
//static int  storeDispMsgsAndPromptsLst(char *, char *, PROMPT_PTR_TYPE);
static int  storeDispMsgsAndPromptsLst_1(char *, char *, DISPMSG_INFO_PTYPE *, char*);
static int  getScrnTitles(dictionary *, char *, uchar **, char*);
static int  getBtnLbls(dictionary *, char *, BTNLBL_PTYPE *, char*);
static int  getDispMsgs(dictionary *, char *, uchar **, char*);
static int  getAmtFormat(dictionary *, char *, PROMPT_PTR_TYPE);
static PROMPT_PTR_TYPE getPromptsNodeForCurLang();
static int getLangIdFromDispPrompFile(dictionary *, char *, int *);
static int getLangIdFromDispPrompFileifinEMVSupLang(dictionary *, char *, int *);

extern int 	isDevEmvSetupRqd();
//ArjunU1: EMV Testing
#if 0
static EMVKEYLOAD_LIST_PTYPE findNodeForAppId(char *, EMVKEYLOAD_LIST_PTYPE); 
static int loadEmvDefaultValues();
static int storeEmvDfltValues(char * , EMVKEYLOAD_LST_INFO_PTYPE *);
static int storeAppIDDfltValues(dictionary *, EMVKEYLOAD_LIST_PTYPE); 
static int saveDfltValue(int , char *, EMVKEYLOAD_LIST_PTYPE );
static int storeD18DfltValues(dictionary *, char *, EMVKEYLOAD_LST_INFO_PTYPE); 
static int storeD20DfltValues(dictionary *, char *, EMVKEYLOAD_LST_INFO_PTYPE); 
static int storeD25DfltValues(dictionary *, char *, EMVKEYLOAD_LST_INFO_PTYPE); 
static int saveEstTableDlftValues(int , char *, EMV_EST_REC_PTYPE);
static int saveEestTableDlftValues(int , char *, EMV_EEST_REC_PTYPE);
static int saveMvtTableDlftValues(int , char *, EMV_MVT_REC_PTYPE);
static int saveIcctTableDlftValues(int , char *, EMV_ICCT_REC_PTYPE);
static int saveEmvtTableDlftValues(int , char *, EMV_EMVT_REC_PTYPE);
static int saveD18DlftValues(int, char *, EMV_OTHER_REC_PTYPE);
static int saveD25DlftValues(int, char *, EMV_OTHER_REC_PTYPE);

//Below strings are required to read default values from ini file to build XPI 'D' commands.
static char *dfltAppIdValList[]	= { "RECNO", "RID", "PARTIAL_NAME_FLG", "TERM_AVN", "SCND_TERM_AVN", "RECMD_AID_NAME", "LNNUMOFTRANS", "SCHMREF", "ISSUER_REF",
									"TRM_DATA_PRESENT","TARGET_RS_PERCENT", "FLR_LIMIT", "RSTHRESHOLD", "MAX_TARGET_RS_PERCENT",
									"MERCH_FORCE_ONLINE_FLG", "BLACK_LIST_CARD_SUPPORT_FLG", "FALLBACK_ALLOWED_FLG",
									"TAC_DEFAULT","TAC_DENIAL","TAC_ONLINE", "DEFAULT_TDOL", "DEFAULT_DDOL", "NEXT_RECORD",
									"AUTO_SELECT_APPL", "EMV_COUNTER", "COUNTRY_CODE","CURRENCY_CODE", "TERM_CAPACITY", "ADD_CAPACITY",
									"TERM_TYPE", "MERCH_CAT_CODE", "TERMINAL_CAT_CODE", "TERM_ID", "MERCH_ID", "ACQUIRER_ID",
									"CAPK_INDEX","PIN_BYPASS_FLG", "EMV_PIN_TIMEOUT", "PIN_FORMAT", "PIN_SCRIPT_NUM", "PIN_MACRO_NUMBER",
									"DEVRI_KEY_FLG","DEVRI_MACRO_NUM", "CARD_STAT_DISPLAY_FLG", "TERM_CUR_EXP", "ISS_ACQ_FLG",
									"NO_DISPLAY_SUPPORT_FLG", "MODIFY_CAND_LIST_FLG", "STRING_RFU1","STRING_RFU2", "STRING_RFU3", "SHORT_RFU1",
									"SHORT_RFU2","SHORT_RFU3","SCHM_LABEL", "CSN_LIST", "CSN_LIST_FILE", "EMV_TABLE_RECORD",
									"GROUP_NAME","CTLS_FLR_LIMIT", "CTLS_CVM_LIMIT", "CTLS_TRN_LIMIT", "CTLS_TAC_DEFAULT",
									"CTLS_TAC_DENIAL", "CTLS_TAC_ONLINE", "VISATTQ", "TERMINAL_CAPABILITIES",
									"ADDITIONAL_CAPABILITIES","CTLS_COUNTRY_CODE","CTLS_CURRECY_CODE", "CTLS_MVT_INDEX", "SUPPORTED",
									"EMV_RECORD","MAX_AID_LEN", "APP_FLOW", "PARTIAL_NAME", "ACCOUNT_SEL_FLG",
									"CTLS_ENABLE_FLG", "TRANS_SCHEME", NULL
								  };
								  
static char *dfltD18ValList[]	= { "BELLOW_FLR_LIMIT_TERM_CAPABILITIES", "ABOVE_FLR_LIMIT_TERM_CAPABILITIES",
									"CTLSCVM_FLR_LIMIT", NULL };
static char *dfltD20ValList[]	= { "REC_NUMBER", "CTLS_MERCHANT_TYPE", "CTLS_TERM_TRAN_INFO", 
									"CTLS_TERM_TRAN_TYPE", "CTLS_REQ_RECIEPT_LIMIT", "CTLS_OPTION_STATUS",
									"CTLS_READER_FLR_LIMIT", NULL };
static char *dfltD25ValList[]	= { "VISA_DEBIT","CONTACTLESS_MODE", "PDOL_SUPPORT", 
									"VALUE_LINK_CARD_SUPPORT", "IS_NFC_SUPPROT", NULL };
#endif
/*
 * ============================================================================
 * Function Name: loadUIConfigParams
 *
 * Description	: This function reads all the config parameters from the
 * 				  config.ur1 file and store them in the static structure.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int	loadUIConfigParams()
{
	int		rv						= 0;
	int		iLen					= 0;
	int		iVal					= 0;
	int		iSize					= 0;
	int		iTimeIntvl				= 0;
	int		iAppLogEnabled			= isAppLogEnabled();
	char	szAppLogData[300]		= "";
	char	szTemp[20]				= "";
//	char*	szConfigXMLFiles[7]		= {"./flash/EMV_APPLICATIONS_01.XML", "./flash/EMV_CTLS_APPLICATIONS_01.XML", "./flash/EMV_KEYS_01.XML",
//										"./flash/EMV_CTLS_KEYS_01.XML", "./flash/EMV_TERMINAL_01.XML", "./flash/EMV_CTLS_TERMINAL_01.XML",
//										"./flash/emvConfig.ini"};

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initializeUISettings();

	iLen = sizeof(szTemp);

	/* ------------ Set device model ------------------ */
	uiSettings.devModel = svcInfoPlatform(7);

	debug_sprintf(szDbgMsg, "%s: Device Platform = Mx%d", __FUNCTION__,
													uiSettings.devModel);
	APP_TRACE(szDbgMsg);

	/* ------------ Check for default language --------------- */
	memset(uiSettings.szLangType, 0x00, sizeof(uiSettings.szLangType));
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, DFLT_LANG, szTemp, iLen);
	if(rv > 0)
	{
		sprintf(uiSettings.szLangType, "%s", szTemp);
		debug_sprintf(szDbgMsg, "%s: Default lang [%s]", __FUNCTION__, szTemp);
		APP_TRACE(szDbgMsg);		
		rv = SUCCESS;
	}
	else
	{
		/* Set the defualt language as English */
		sprintf(uiSettings.szLangType, "%s", "ENGLISH");
		/* Update the config.usr1 */
		putEnvFile(SECTION_DEVICE, DFLT_LANG, "ENGLISH");
		debug_sprintf(szDbgMsg,
				"%s: Default lang not present, setting English as defualt",
														__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = SUCCESS;
	}

/* ------------- Check for UI Agent Port -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_REG, "TERMPRT", szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		if( (iVal > 0) && (iVal <= 65535))
		{
			debug_sprintf(szDbgMsg, "%s: UI Agent Port = [%d]", __FUNCTION__,
																		iVal);
			APP_TRACE(szDbgMsg);

			uiSettings.FAPort = iVal;
		}
		else
		{
			/* Incorrect value for UI agent port */
			debug_sprintf(szDbgMsg,
					"%s: Incorrect val for UI agent port, using default %d",
											__FUNCTION__, DFLT_FA_PORT);
			APP_TRACE(szDbgMsg);

			uiSettings.FAPort = DFLT_FA_PORT;
		}
	}
	else
	{
		/* No value found for UI agent port */
		debug_sprintf(szDbgMsg,
					"%s: No val found for UI agent port, using default %d",
											__FUNCTION__, DFLT_FA_PORT);
		APP_TRACE(szDbgMsg);

		uiSettings.FAPort = DFLT_FA_PORT;
	}

	/* ------------- Check for Private Label Exp CVV Required -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, PRIV_LAB_CVV_REQD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Priv Label Cvv Reqd as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.privLabelCvvReqd = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Priv Label  Cvv Reqd as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.privLabelCvvReqd = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for priv lbl exp cvv parameter */
		debug_sprintf(szDbgMsg,"%s: No val found for PRIV_LAB_CVV_REQD;Set FALSE"
															, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.privLabelCvvReqd = PAAS_FALSE;
	}

	/* ------------- Check for Private Label Exp CVV Required -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, PRIV_LAB_EXP_REQD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Priv Label Exp Date Reqd as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.privLabelExpReqd = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Priv Label Exp Date Reqd as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.privLabelExpReqd = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for priv lbl exp cvv parameter */
		debug_sprintf(szDbgMsg,"%s: No val found for PRIV_LAB_EXP_REQD;Set FALSE"
															, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.privLabelExpReqd = PAAS_FALSE;
	}

	/* ------------- Check for Gift Card Exp CVV Required -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, GIFT_EXP_REQD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Gift card Exp Date Reqd as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.giftcardExpReqd = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Gift Card Exp Date Reqd as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.giftcardExpReqd = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for gift card exp parameter */
		debug_sprintf(szDbgMsg,"%s: No val found for GIFT_EXP_REQD;Set TRUE"
															, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.giftcardExpReqd = PAAS_TRUE;
	}

	/* ------------- Check for Gift Card CVV Required -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, GIFT_CVV_REQD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Gift card Cvv Reqd as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.giftcardCvvReqd = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Gift Card Cvv Reqd as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.giftcardCvvReqd = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for gift cvv parameter */
		debug_sprintf(szDbgMsg,"%s: No val found for GIFT_CVV_REQD;Set FALSE"
															, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.giftcardCvvReqd = PAAS_FALSE;
	}

	/* ------------- Check for Gift Card PIN Code Required -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, GIFT_PINCODE_REQD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Gift card PIN Code Reqd as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.giftcardPINCodeReqd = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Gift Card PIN Code Reqd as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.giftcardPINCodeReqd = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for gift pin code reqd parameter */
		debug_sprintf(szDbgMsg,"%s: No val found for GIFT_PINCODE_REQD; Set FALSE"
															, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.giftcardPINCodeReqd = PAAS_FALSE;
	}

	/* ------------- Check for EBT Card Expiry Entry Required -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, EBT_EXP_REQD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting EBT card Exp Date Reqd as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.ebtcardExpReqd = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Gift Card EBT Date Reqd as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.ebtcardExpReqd = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for gift card exp parameter */
		debug_sprintf(szDbgMsg,"%s: No val found for EBT_EXP_REQD;Set TRUE"
															, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.ebtcardExpReqd = PAAS_FALSE;
	}

	/* ------------- Check for EBT Card CVV Required -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, EBT_CVV_REQD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting EBT card Cvv Reqd as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.ebtcardCvvReqd = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting EBT Card Cvv Reqd as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.ebtcardCvvReqd = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for gift cvv parameter */
		debug_sprintf(szDbgMsg,"%s: No val found for EBT_CVV_REQD;Set FALSE"
															, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.ebtcardCvvReqd = PAAS_FALSE;
	}

	/* ------------- Check for Signature Option On Pin Entry Screen -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, SIG_OPT_ON_PINENTRY_REQD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Signature Option on Pin Entry Screen as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.sigOptOnPINEntryEnabled = PAAS_TRUE;

			putEnvFile("iab", "z62_ccnv_enable", "1"); //set corresponding XPI variable
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Signature Option on Pin Entry Screen as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.sigOptOnPINEntryEnabled = PAAS_FALSE;

			putEnvFile("iab", "z62_ccnv_enable", "0");//set corresponding XPI variable
		}
	}
	else
	{
		/* No value found for sign opt on pin entry parameter */
		debug_sprintf(szDbgMsg,"%s: No val found for SIG_OPT_ON_PINENTRY_REQD;Set TRUE(Default)"
															, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.sigOptOnPINEntryEnabled = PAAS_TRUE;

		putEnvFile("iab", "z62_ccnv_enable", "1");//set corresponding XPI variable
	}

	/* ------------- Check for EMV settings ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, EMV_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting EMV enabled as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bEmvEnabled = PAAS_TRUE;
		}
		else 
		{
			debug_sprintf(szDbgMsg, "%s: Setting EMV enabled as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bEmvEnabled = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for Contact EMV Enabled parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for EMV contact enabled;Set FALSE"
				, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bEmvEnabled = PAAS_FALSE;
	}

	/* ------------- Check for Contactless Enabled ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, CTLS_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		if( ((*szTemp == 'Y') || (*szTemp == 'y')) )
		{
			debug_sprintf(szDbgMsg, "%s: Setting CTLS enabled as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bCTLSEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting CTLS enabled as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bCTLSEnabled = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for Contactless Enabled parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for CTLS enabled;Set TRUE"
				, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bCTLSEnabled = PAAS_TRUE;
	}

	/* ------------- Check for EMV Contactless Enabled ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, CTLS_EMV_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		if( ((*szTemp == 'Y') || (*szTemp == 'y')) && uiSettings.bEmvEnabled == PAAS_TRUE && uiSettings.bCTLSEnabled == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Setting CTLS EMV enabled as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bCTLSEmvEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting CTLS EMV enabled as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			putEnvFile(SECTION_DEVICE, CTLS_EMV_ENABLED, "N");

			uiSettings.bCTLSEmvEnabled = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for Contact EMV Enabled parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for EMV CTLS enabled;Set FALSE"
				, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bCTLSEmvEnabled = PAAS_FALSE;
	}

	/* ------------ Check for Supported Wallets --------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, SUPPORTED_WALLETS, szTemp, iLen);
	if(uiSettings.bCTLSEnabled == PAAS_FALSE)
	{
		uiSettings.bApplePayWalletSupport = PAAS_FALSE;
		uiSettings.bGooglePayWalletSupport = PAAS_FALSE;
		uiSettings.bSamsungPayWalletSupport = PAAS_FALSE;
		debug_sprintf(szDbgMsg, "%s: ContactLess is Disabled on the Device, Not enabling Wallet Support",	__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else if( (rv > 0) && (strlen(szTemp) > 0) && (strlen(szTemp) <= 3))
	{
		debug_sprintf(szDbgMsg, "%s: Wallets Supported [%s]", __FUNCTION__, szTemp);
		APP_TRACE(szDbgMsg);
		if(strchr(szTemp, 'A') != NULL || strchr(szTemp, 'a') != NULL )
		{
			uiSettings.bApplePayWalletSupport = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Apple Pay Wallet Supporting",	__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			uiSettings.bApplePayWalletSupport = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: Apple Pay Wallet NOT Supporting",	__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(strchr(szTemp, 'G') != NULL || strchr(szTemp, 'g') != NULL )
		{
			uiSettings.bGooglePayWalletSupport = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Google/Android Pay Wallet Supporting",	__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			uiSettings.bGooglePayWalletSupport = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: Google/Android Pay Wallet NOT Supporting",	__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(strchr(szTemp, 'S') != NULL || strchr(szTemp, 's') != NULL )
		{
			uiSettings.bSamsungPayWalletSupport = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Samsung Pay Wallet Supporting", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			uiSettings.bSamsungPayWalletSupport = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: Samsung Pay Wallet NOT Supporting", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		rv = SUCCESS;
	}
	else
	{
		uiSettings.bApplePayWalletSupport = PAAS_FALSE;
		uiSettings.bGooglePayWalletSupport = PAAS_FALSE;
		uiSettings.bSamsungPayWalletSupport = PAAS_FALSE;
		debug_sprintf(szDbgMsg, "%s: No or Unknown value for supportedwallets NOT Enabling Wallets", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* ------------- Check for cardreadscreenfirst Enabled ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, CARD_READ_SCREEN_FIRST, szTemp, iLen);
	if(rv > 0)
	{
		if( ((*szTemp == 'Y') || (*szTemp == 'y')) )
		{
			debug_sprintf(szDbgMsg, "%s: Setting CARD_READ_SCREEN_FIRST enabled as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bCardBasedTenderSel = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting CARD_READ_SCREEN_FIRST enabled as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bCardBasedTenderSel = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for CARD_READ_SCREEN_FIRST Enabled parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for CARD_READ_SCREEN_FIRST enabled;Setting as TRUE"
				, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bCardBasedTenderSel = PAAS_TRUE;
	}

	/* ------------- Check for emvkernelswitchallowed Enabled ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, EMV_KERNEL_SWITCHING_ALLOWED, szTemp, iLen);
	if(rv > 0)
	{
		if( ((*szTemp == 'Y') || (*szTemp == 'y')) )
		{
			debug_sprintf(szDbgMsg, "%s: Setting EMV_KERNEL_SWITCHING_ALLOWED enabled as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bEmvKernelSwitching = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting EMV_KERNEL_SWITCHING_ALLOWED enabled as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bEmvKernelSwitching = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for EMV_KERNEL_SWITCHING_ALLOWED Enabled parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for EMV_KERNEL_SWITCHING_ALLOWED enabled;Set False"
				, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bEmvKernelSwitching = PAAS_FALSE;
	}

	/* ------------- Check for Service Code based Card Entry------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, SERVICE_CODE_CHECK_IN_FALLBACK, szTemp, iLen);
	if(rv > 0)
	{
		if( ((*szTemp == 'Y') || (*szTemp == 'y')) )
		{
			debug_sprintf(szDbgMsg, "%s: Setting Service Code based Card Entry as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bServiceCodeCheckInFallback = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Service Code based Card Entry as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bServiceCodeCheckInFallback = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for Service Code based Card Entry parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for Service Code based Card Entry;Set False"
				, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bServiceCodeCheckInFallback = PAAS_FALSE;
	}

	/* ------------- Check for Empty Candid as Fallback Flag------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, EMPTY_CANDID_AS_FALLBACK, szTemp, iLen);
	if(rv > 0)
	{
		if( ((*szTemp == 'Y') || (*szTemp == 'y')) )
		{
			debug_sprintf(szDbgMsg, "%s: Setting Empty Candid as Fallback Flag as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bEmptyCandidAsFallBack = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Empty Candid as Fallback Flag as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bEmptyCandidAsFallBack = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for Empty Candid as Fallback Flag */
		debug_sprintf(szDbgMsg,"%s: No value found for Empty Candid as Fallback Flag;Set False"
				, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bEmptyCandidAsFallBack = PAAS_FALSE;
	}

	/* ------------- Check for XPI as a library value------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_PERM, XPI_LIBRARY, szTemp, iLen);
	if(rv > 0)
	{
		if(*szTemp == '1')
		{
			debug_sprintf(szDbgMsg, "%s: Setting XPI library Flag as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bXPILibEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting XPI library Flag as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bXPILibEnabled = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for XPI as a library Flag */
		debug_sprintf(szDbgMsg,"%s: No value found for Setting XPI library;Set False"
				, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bXPILibEnabled = PAAS_FALSE;
	}

	/* ------------- Check for STORE_PREV_TRANS_CARD_DTLS ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, STORE_CARD_DTLS_POST_AUTH, szTemp, iLen);
	if(rv > 0)
	{
		if( ((*szTemp == 'Y') || (*szTemp == 'y')) )
		{
			debug_sprintf(szDbgMsg, "%s: Setting STORE_PREV_TRANS_CARD_DTLS as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bStorePrevTransCardDtls = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting STORE_PREV_TRANS_CARD_DTLS as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bStorePrevTransCardDtls = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for STORE_PREV_TRANS_CARD_DTLS Enabled parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for STORE_PREV_TRANS_CARD_DTLS enabled;Set FALSE"
				, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bStorePrevTransCardDtls = PAAS_FALSE;
	}

	/* ------------- Check for STORE_PREV_TRANS_CARD_DTLS_GIFT_CLOSE for GIFT ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, STORE_PREV_TRANS_CARD_DTLS_GIFT_CLOSE, szTemp, iLen);
	if(rv > 0)
	{
		if( ((*szTemp == 'Y') || (*szTemp == 'y')) )
		{
			debug_sprintf(szDbgMsg, "%s: Setting STORE_PREV_TRANS_CARD_DTLS_GIFT_CLOSE as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bStorePrevTransCardDtlsForGiftClose = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting STORE_PREV_TRANS_CARD_DTLS_GIFT_CLOSE as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bStorePrevTransCardDtlsForGiftClose = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for STORE_PREV_TRANS_CARD_DTLS_GIFT_CLOSE Enabled parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for STORE_PREV_TRANS_CARD_DTLS_GIFT_CLOSE enabled;Set FALSE"
				, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bStorePrevTransCardDtlsForGiftClose = PAAS_FALSE;
	}

	/* ------------- Check for EMV Verbose Receipt Enabled ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, VERBOSERECPT_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Verbose Receipt Enabled as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.verboseRecptEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Verbose Receipt Enabled as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.verboseRecptEnabled = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for EMV verbose Receipt parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for Verbose Receipt Enabled;Setting FALSE", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.verboseRecptEnabled = PAAS_FALSE;
	}

	/* ------------- Check for Descriptive Entry Mode ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, DESCRIPTIVE_ENTRY_MODE, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Descriptive Entry Mode as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bDescriptiveEntryMdoe = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Descriptive Entry Mode as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bDescriptiveEntryMdoe = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for Descriptive Entry Mode parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for Descriptive Entry Mode;Setting FALSE", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bDescriptiveEntryMdoe = PAAS_FALSE;
	}
	
	/* ------------- Check for EMV Custom Tags Reqd ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, CUSTOM_TAG_REQD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Custom Tags Required as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bEmvCustomTagReqd = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Custom Tags Required as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bEmvCustomTagReqd = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for EMV Custom Tag Required */
		debug_sprintf(szDbgMsg,"%s: No value found for Custom Tags Required;Setting FALSE"
																			, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bEmvCustomTagReqd = PAAS_FALSE;
	}

	/* ------------- Check for cardpresentvoidtagsreq ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, TAGS_REQ_VOID_CARD_PRESENT, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting TAGS_REQ_VOID_CARD_PRESENT as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bTagsReqdCardPresentVoid = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting TAGS_REQ_VOID_CARD_PRESENT as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bTagsReqdCardPresentVoid = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for TAGS_REQ_VOID_CARD_PRESENT */
		debug_sprintf(szDbgMsg,"%s: No value found for TAGS_REQ_VOID_CARD_PRESENT;Setting FALSE"
																			, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bTagsReqdCardPresentVoid = PAAS_FALSE;
	}

	/* ------------- Check for partialemvallowed ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, PARTIAL_EMV_ALLOWED, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting PARTIAL_EMV_ALLOWED as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bPartialEmvAllowed = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting PARTIAL_EMV_ALLOWED as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bPartialEmvAllowed = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for PARTIAL_EMV_ALLOWED */
		debug_sprintf(szDbgMsg,"%s: No value found for PARTIAL_EMV_ALLOWED;Setting FALSE"
																			, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bPartialEmvAllowed = PAAS_FALSE;
	}

	/* ------------- Check for clearexpiryin5F24tag ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, CLEAR_EXPIRY_TO_EMV_HOST, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting CLEAR_EXPIRY_TO_EMV_HOST as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bClearExpToEmvHost = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting CLEAR_EXPIRY_TO_EMV_HOST as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.bClearExpToEmvHost = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for CLEAR_EXPIRY_TO_EMV_HOST */
		debug_sprintf(szDbgMsg,"%s: No value found for CLEAR_EXPIRY_TO_EMV_HOST;Setting FALSE"
																			, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.bClearExpToEmvHost = PAAS_FALSE;
	}

	/* ------------- Check for Advance VSP Days ------------------- */
	memset(szTemp, 0x00, iLen);

	rv = getEnvFile(SECTION_DEVICE, ADVANCE_VSP_IN_X_DAYS, szTemp, iLen);
	if(rv > 0)
	{
		iSize = stripSpaces(szTemp,STRIP_TRAILING_SPACES|STRIP_LEADING_SPACES);
		if(iSize == strspn(szTemp, "1234567890"))
		{
			/* Value is numeric */
			uiSettings.advancevspdays = atoi(szTemp);
			if(uiSettings.advancevspdays > MAX_ADVANCE_VSP_DAYS)
			{
				uiSettings.advancevspdays = DFLT_ADVANCE_VSP_DAYS;
			}
			else if(uiSettings.advancevspdays < MIN_ADVANCE_VSP_DAYS)
			{
				uiSettings.advancevspdays = DFLT_ADVANCE_VSP_DAYS;
			}
		}
		else
		{
			uiSettings.advancevspdays = DFLT_ADVANCE_VSP_DAYS;
		}
		debug_sprintf(szDbgMsg, "%s: Setting ADVANCEVSPKEYINXDAYS to %d", __FUNCTION__, uiSettings.advancevspdays);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		/* No value found for IP Address display */
		debug_sprintf(szDbgMsg,"%s: No val found for  Advance VSP Days;setting dflt %d", __FUNCTION__, DFLT_ADVANCE_VSP_DAYS);
		APP_TRACE(szDbgMsg);

		uiSettings.advancevspdays = DFLT_ADVANCE_VSP_DAYS;
	}

	/* ------------- Check for ip disp interval ------------------- */
	memset(szTemp, 0x00, iLen);

	rv = getEnvFile(SECTION_DEVICE, IP_DISP_INTRVL, szTemp, iLen);
	if(rv > 0)
	{
		iSize = stripSpaces(szTemp,STRIP_TRAILING_SPACES|STRIP_LEADING_SPACES);
		if(iSize == strspn(szTemp, "1234567890"))
		{
			/* Value is numeric */
			iTimeIntvl = atoi(szTemp);
			if(iTimeIntvl > MAX_IP_SHOW_TIME)
			{
				iTimeIntvl = MAX_IP_SHOW_TIME;
			}
			else if(iTimeIntvl <= 0)
			{
				iTimeIntvl = DFLT_IP_SHOW_TIME;
			}
		}
		else
		{
			iTimeIntvl = DFLT_IP_SHOW_TIME;
		}
		uiSettings.ipdispinterval = iTimeIntvl;
	}
	else
	{
		/* No value found for IP Address display */
		debug_sprintf(szDbgMsg,"%s: No val found for IP display;setting dflt %d"
											, __FUNCTION__, DFLT_IP_SHOW_TIME);
		APP_TRACE(szDbgMsg);

		uiSettings.ipdispinterval = DFLT_IP_SHOW_TIME;
	}

	/* ------------- Check for status message disp interval ------------------- */
	memset(szTemp, 0x00, iLen);

	rv = getEnvFile(SECTION_DEVICE, STATUS_MSG_DISP_INTRVL, szTemp, iLen);
	if(rv > 0)
	{
		iSize = stripSpaces(szTemp, STRIP_TRAILING_SPACES|STRIP_LEADING_SPACES);
		if(iSize == strspn(szTemp, "1234567890"))
		{
			/* Value is numeric */
			iTimeIntvl = atoi(szTemp);
			if(iTimeIntvl > MAX_STATUS_DISP_TIME || iTimeIntvl < MIN_STATUS_DISP_TIME)
			{
				iTimeIntvl = DFLT_STATUS_DISP_TIME;

				debug_sprintf(szDbgMsg,"%s: Status disp interval is out of range. Defaulting it to %dms"
															, __FUNCTION__, DFLT_STATUS_DISP_TIME);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			iTimeIntvl = DFLT_STATUS_DISP_TIME;
			debug_sprintf(szDbgMsg,"%s: Invalid time given Defaulting it to %dms"
																		, __FUNCTION__, DFLT_STATUS_DISP_TIME);
		}
		uiSettings.statusMsgDispInterval = iTimeIntvl;
	}
	else
	{
		/* No value found for Status Message display interval */
		debug_sprintf(szDbgMsg,"%s: No val found for Status Msg disp time ;setting dflt %dms"
											, __FUNCTION__, DFLT_STATUS_DISP_TIME);
		APP_TRACE(szDbgMsg);

		uiSettings.statusMsgDispInterval = DFLT_STATUS_DISP_TIME;
	}
	memset(szTemp, 0x00, iLen);
	sprintf(szTemp, "%d", uiSettings.statusMsgDispInterval);
	putEnvFile(SECTION_DEVICE, STATUS_MSG_DISP_INTRVL, szTemp);

	/* ------ Check for system info screen display interval ------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, SYS_INFO_DISP_INTRVL, szTemp, iLen);
	if(rv > 0)
	{
		iSize = stripSpaces(szTemp,STRIP_TRAILING_SPACES|STRIP_LEADING_SPACES);
		if(iSize == strspn(szTemp, "1234567890"))
		{
			/* Value is numeric */
			iTimeIntvl = atoi(szTemp);
			if(iTimeIntvl > MAX_SYSINFO_DISP_TIME)
			{
				iTimeIntvl = MAX_SYSINFO_DISP_TIME;
			}
			else if(iTimeIntvl <= 0)
			{
				iTimeIntvl = DFLT_SYSINFO_DISP_TIME;
			}
		}
		else
		{
			iTimeIntvl = DFLT_SYSINFO_DISP_TIME;
		}

		uiSettings.sysinfointerval = iTimeIntvl;
	}
	else
	{
		/* No value found for IP Address display */
		debug_sprintf(szDbgMsg,"%s: No val found for sysinfo display;setting %d"
										, __FUNCTION__, DFLT_SYSINFO_DISP_TIME);
		APP_TRACE(szDbgMsg);

		uiSettings.sysinfointerval = DFLT_SYSINFO_DISP_TIME;
	}

	/* ------ Check for device name screen display interval ------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, DEVNAME_DISP_INTRVL, szTemp, iLen);
	if(rv > 0)
	{
		iSize = stripSpaces(szTemp,STRIP_TRAILING_SPACES|STRIP_LEADING_SPACES);
		if(iSize == strspn(szTemp, "1234567890"))
		{
			/* Value is numeric */
			iTimeIntvl = atoi(szTemp);
			if(iTimeIntvl > MAX_SYSINFO_DISP_TIME)
			{
				iTimeIntvl = MAX_SYSINFO_DISP_TIME;
			}
			else if(iTimeIntvl <= 0)
			{
				iTimeIntvl = DFLT_DEVNAME_DISP_TIME;
			}
		}
		else
		{
			iTimeIntvl = DFLT_DEVNAME_DISP_TIME;
		}

		uiSettings.devnamedispinterval = iTimeIntvl;
	}
	else
	{
		/* No value found for IP Address display */
		debug_sprintf(szDbgMsg,"%s: No val found for sysinfo display;setting %d"
										, __FUNCTION__, DFLT_DEVNAME_DISP_TIME);
		APP_TRACE(szDbgMsg);

		uiSettings.devnamedispinterval = DFLT_DEVNAME_DISP_TIME;
	}
	sprintf(szTemp, "%d", uiSettings.devnamedispinterval);
	putEnvFile(SECTION_DEVICE, DEVNAME_DISP_INTRVL, szTemp);

	/* ------ Check for device name screen display interval ------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, LANE_CLOSED_FONT_COL, szTemp, iLen);
	if(rv > 0)
	{
		iSize = stripSpaces(szTemp,STRIP_TRAILING_SPACES|STRIP_LEADING_SPACES);
		if(iSize == strspn(szTemp, "1234567890ABCDEF"))
		{
			sprintf(uiSettings.szLaneClosedFontColor, "%s", szTemp);
			debug_sprintf(szDbgMsg, "%s: uiSettings.szLaneClosedFontColor [%s]", __FUNCTION__, uiSettings.szLaneClosedFontColor);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			sprintf(uiSettings.szLaneClosedFontColor, "%s", DFLT_LANE_CLOSED_FONT_COL);
		}
	}
	else
	{
		/* No value found for Font Color */
		sprintf(uiSettings.szLaneClosedFontColor, "%s", DFLT_LANE_CLOSED_FONT_COL);
	}
	putEnvFile(SECTION_DEVICE, LANE_CLOSED_FONT_COL, uiSettings.szLaneClosedFontColor);


	/* ------ Check for device name screen display interval ------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, LANE_CLOSED_FONT_SIZE, szTemp, iLen);
	if(rv > 0)
	{
		iSize = stripSpaces(szTemp,STRIP_TRAILING_SPACES|STRIP_LEADING_SPACES);
		if(iSize == strspn(szTemp, "1234567890"))
		{
			uiSettings.iLaneClosedFontSize = atoi(szTemp);
			debug_sprintf(szDbgMsg, "%s: uiSettings.iLaneClosedFontSize [%d]", __FUNCTION__, uiSettings.iLaneClosedFontSize);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			if(uiSettings.devModel == 915)
				uiSettings.iLaneClosedFontSize = DFLT_915_LANE_CLOSED_FONT_SIZE;
			else
				uiSettings.iLaneClosedFontSize = DFLT_925_LANE_CLOSED_FONT_SIZE;
		}
	}
	else
	{
		/* No value found for Font Size*/
		if(uiSettings.devModel == 915)
			uiSettings.iLaneClosedFontSize = DFLT_915_LANE_CLOSED_FONT_SIZE;
		else
			uiSettings.iLaneClosedFontSize = DFLT_925_LANE_CLOSED_FONT_SIZE;
	}

	sprintf(szTemp, "%d", uiSettings.iLaneClosedFontSize);
	putEnvFile(SECTION_DEVICE, LANE_CLOSED_FONT_SIZE, szTemp);

	/* ------------- Check for PIN pad mode -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_REG, PINPAD_MODE, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		if( iVal == 0 || iVal == 1 )
		{
			debug_sprintf(szDbgMsg, "%s: PIN Pad Mode is  = [%s]", __FUNCTION__,
											(iVal == 1)? "DUKPT" : "Master/Session" );
			APP_TRACE(szDbgMsg);

			uiSettings.pinpadmode = iVal;
		}
		else
		{
			/* Incorrect value for PIN pad mode */
			debug_sprintf(szDbgMsg, "%s: Incorrect val for PIN pad mode, using default %d",
																		   __FUNCTION__, 1);
			APP_TRACE(szDbgMsg);

			uiSettings.pinpadmode = 1;	// DUKPT mode
		}
	}
	else
	{
		/* No value found for pin pad mode */
		debug_sprintf(szDbgMsg,"%s: No val found for pin pad mode, using default %d",
																	__FUNCTION__, 1);
		APP_TRACE(szDbgMsg);

		uiSettings.pinpadmode = 1; // DUKPT mode
	}

	/* ------------- Check for PIN pad mode -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, PREAMBLE_SELECT_WAIT_TIME, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		if( iVal > 0 && iVal < 121 )
		{
			debug_sprintf(szDbgMsg, "%s: Preamble Select Wait Time  = [%d]", __FUNCTION__, iVal);
			APP_TRACE(szDbgMsg);

			uiSettings.iPreambleSelectWaitTime = iVal;
		}
		else
		{
			/* Incorrect value for PIN pad mode */
			debug_sprintf(szDbgMsg, "%s: Incorrect val for Preamble Select Wait Time, using default 5",
																		   __FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.iPreambleSelectWaitTime = 5;	// DUKPT mode
		}
	}
	else
	{
		/* No value found for pin pad mode */
		debug_sprintf(szDbgMsg,"%s: No val found for Preamble Select Wait Time, using default 5 ",
																	__FUNCTION__);
		APP_TRACE(szDbgMsg);

		uiSettings.iPreambleSelectWaitTime = 5; // DUKPT mode
	}

#if 0
	if(uiSettings.bEmvEnabled)
	{
		if(isDevEmvSetupRqd() == PAAS_TRUE)
		{
			if(doesFileExist(szConfigXMLFiles[0]) == SUCCESS)
			{
				loadAppConfigDataFromXMLFile(szConfigXMLFiles[0]);
				deleteFile(szConfigXMLFiles[0]);
			}

			if(doesFileExist(szConfigXMLFiles[1]) == SUCCESS)
			{
				loadAppConfigDataFromXMLFile(szConfigXMLFiles[1]);
				deleteFile(szConfigXMLFiles[1]);
			}

			if(doesFileExist(szConfigXMLFiles[2]) == SUCCESS)
			{
				loadPubkeyDataFromXMLFile(szConfigXMLFiles[2]);
				deleteFile(szConfigXMLFiles[2]);
			}

			if(doesFileExist(szConfigXMLFiles[3]) == SUCCESS)
			{
				loadPubkeyDataFromXMLFile(szConfigXMLFiles[3]);
				deleteFile(szConfigXMLFiles[3]);
			}

			if(doesFileExist(szConfigXMLFiles[4]) == SUCCESS)
			{
				loadTermDataFromXMLFile(szConfigXMLFiles[4]);
				deleteFile(szConfigXMLFiles[4]);
			}

			if(doesFileExist(szConfigXMLFiles[5]) == SUCCESS)
			{
				loadTermDataFromXMLFile(szConfigXMLFiles[5]);
				deleteFile(szConfigXMLFiles[5]);
			}
		}

		/* Load the EMV default values in the memory  for download initialization*/
		rv = loadEmvDefaultValues();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in loading EMV default values", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
		debug_sprintf(szDbgMsg, "%s: Loaded EMV download initialization default values", __FUNCTION__);
		APP_TRACE(szDbgMsg);			
	}
#endif

	/* MukeshS3: 27-April-16: Moving the below two functions loadDispMsgsAndPrompts & loadMediaSettings
	 * inside loadUIData function, after creating the client for UI agent.
	 * So that, any error while loading the data can be logged to the UI screen.
	 */
#if 0
	/* Load the display prompts in the memory */
	rv = loadDispMsgsAndPrompts();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error in loading display prompts",
															__FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error While Loading Display Prompts and Messages.");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
		}

		return FAILURE;
	}
#endif
	/* Check for signature capture */
	rv = getSignatureSettings();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg,"%s: Error while loading signature settings",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error While Loading Signature Settings.");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
		}

		return FAILURE;
	}
#if 0
	/* Load the advertisement media settings */
	rv = loadMediaSettings();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error in loading advt media settings",
															__FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error While Loading the Media Settings, Please Check the Media Settings and Files.");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
		}

		return FAILURE;
	}
#endif
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: loadUIData
 *
 * Description	: This function loads display prompts message & media settings into memory.
 *
 * Input Params	: None:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int	loadUIData(char *pszErrDispMsg)
{
	int		rv						= 0;
	int		iAppLogEnabled			= isAppLogEnabled();
	char	szAppLogData[300]		= "";
	char	szErrMsg[500]			= "";	// this buffer will store the main error message filled by the culprit function for failure.
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Load the display prompts in the memory */
		rv = loadDispMsgsAndPrompts(szErrMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in loading display prompts",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error While Loading Display Prompts and Messages.");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrDispMsg, MSG_APP_LOAD_ERR);
			if(strlen(pszErrDispMsg) > 0)
			{
				sprintf(pszErrDispMsg, "%s. %s",pszErrDispMsg, szErrMsg);
			}
			else
			{
				// Add a default error message for display prompts failure
				sprintf(pszErrDispMsg, "Failed To Load ./flash/displayPrompts.ini %s", szErrMsg);
			}
			rv = FAILURE;
			break;
		}

		/* Load the advertisement media settings */
		rv = loadMediaSettings(szErrMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in loading advt media settings",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error While Loading the Media Settings, Please Check the Media Settings and Files.");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrDispMsg, MSG_APP_LOAD_ERR);
			if(strlen(szErrMsg) == 0)// Add a default error message for display prompts failure
			{
				strcat(pszErrDispMsg, ". Unable To Load Media For Device UI.");
			}
			else
			{
				sprintf(pszErrDispMsg, "%s. %s",pszErrDispMsg, szErrMsg);
			}
			rv = FAILURE;
			break;;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
/*
 * ============================================================================
 * Function Name: getUIAgentComParam
 *
 * Description	: This function would get the communication parameters (IP and
 * 					port number) needed to communicate with the UI Agent
 * 					(Formagent), API to be called by User Interface module.
 *
 * Input Params	: commPtr -> pointer to structure having IP and port number
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getUIAgentComParam(UI_COMPARAM_PTYPE commPtr)
{
	int		rv		= SUCCESS;

	if(commPtr != NULL)
	{
		memset(commPtr, 0x00, sizeof(UI_COMPARAM_STYPE));
		sprintf(commPtr->agentIP, "127.0.0.1");
		commPtr->agentPort = uiSettings.FAPort;
	}
	else
	{
		rv = FAILURE;
	}

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: addEmvKeyLoadNodeToList
 *
 * Description	: 				  
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateEmvKeyLoadValues(EMVKEYLOAD_NODE_PTYPE pstEmvKeyLoadNode)
{
	int									rv 						= SUCCESS;
	EMVKEYLOAD_LIST_PTYPE 				headKeyLoadLst			= NULL;	
	EMVKEYLOAD_LIST_PTYPE 				tmpKeyLoadLst			= NULL;	
	FALLBACK_PTYPE						pstFallBckIndicator		= NULL;
	OFFLINE_FLR_LIMIT_PTYPE				pstOfflineFlrLmt		= NULL;
	EMV_CAPK_INFO_PTYPE					pstCAPKInfo				= NULL;	
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
	if(uiSettings.pstKeyLoadLstInfo != NULL)
	{
		if(uiSettings.pstKeyLoadLstInfo->mainKeyLoadLstHead != NULL)
		{
			headKeyLoadLst = uiSettings.pstKeyLoadLstInfo->mainKeyLoadLstHead;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: ---Default values for D command not found----", __FUNCTION__);
			APP_TRACE(szDbgMsg);	
			
			return FAILURE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: ---Default values for D command not found----", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Updating the host EMV keyload values", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
	while(1)
	{
		if(pstEmvKeyLoadNode->emvKeyLoadType == FALLBACK_INDICATOR)
		{
			pstFallBckIndicator	= (FALLBACK_PTYPE) pstEmvKeyLoadNode->emvData; 

			/* check whether already node present with given APPID*/
			tmpKeyLoadLst = findNodeForAppId(pstFallBckIndicator->szAppID, headKeyLoadLst);
			if(tmpKeyLoadLst != NULL)
			{
				if(!(strcmp(pstFallBckIndicator->szFallBackValue, "Y")) || 
				   !(strcmp(pstFallBckIndicator->szFallBackValue, "y")))
				{   
					tmpKeyLoadLst->stMvtRecInfo.inFallbackAllowed = 1;
				}
				else
				{
					tmpKeyLoadLst->stMvtRecInfo.inFallbackAllowed = 0;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Value not stored,Matching APPID doesn't exists",
																			__FUNCTION__); 
				APP_TRACE(szDbgMsg);
			}			
		}
		else if(pstEmvKeyLoadNode->emvKeyLoadType == OFFLINE_FLOOR_LIMIT)
		{
			pstOfflineFlrLmt	= (OFFLINE_FLR_LIMIT_PTYPE) pstEmvKeyLoadNode-> emvData;
			
			/* check whether already node present with given APPID*/
			tmpKeyLoadLst = findNodeForAppId(pstOfflineFlrLmt->szAppID, headKeyLoadLst);
			if(tmpKeyLoadLst != NULL)
			{
				tmpKeyLoadLst->stMvtRecInfo.lnFloorLimit = 
										atol(pstOfflineFlrLmt->szOffFlrLmt);
				tmpKeyLoadLst->stMvtRecInfo.lnRSThreshold = 
										atol(pstOfflineFlrLmt->szOffFlrLmtThrshld);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Value not stored,Matching APPID doesn't exists",
																			__FUNCTION__); 
				APP_TRACE(szDbgMsg);
			}
		}
		else if(pstEmvKeyLoadNode->emvKeyLoadType == PUBLIC_KEY)
		{
			pstCAPKInfo	= (EMV_CAPK_INFO_PTYPE) pstEmvKeyLoadNode-> emvData; 
		
			/* check whether already node present with given APPID*/
			tmpKeyLoadLst = findNodeForAppId(pstCAPKInfo->szRID, headKeyLoadLst);
			if(tmpKeyLoadLst != NULL)
			{
				memcpy(&(tmpKeyLoadLst->stCAPKInfo[tmpKeyLoadLst->iNumCAPKFiles]), 
										pstCAPKInfo, sizeof(EMV_CAPK_INFO_STYPE));
				/*Increment the counter for CAPK index 
					stored for each APPID */
				tmpKeyLoadLst->iNumCAPKFiles++;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Value not stored,Matching APPID doesn't exists",
																			__FUNCTION__); 
				APP_TRACE(szDbgMsg);
			}			
		}
		break; //exit from the while loop
	}		
	debug_sprintf(szDbgMsg, "%s: ---Return----[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);	
		
	return rv;
}

/*
 * ============================================================================
 * Function Name: getKeyLoadListInfoNode
 *
 * Description	: This function gives the head node of the key load list structure.
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
EMVKEYLOAD_LST_INFO_PTYPE getKeyLoadListInfoNode(int *piAppIDCnt)
{
	EMVKEYLOAD_LST_INFO_PTYPE 		locpstKeyLoadLstInfo 	= NULL;
#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
	if(uiSettings.pstKeyLoadLstInfo != NULL)
	{
		locpstKeyLoadLstInfo = uiSettings.pstKeyLoadLstInfo;
		*piAppIDCnt = uiSettings.iAppIDCnt;
	}
	else
	{
		return NULL;
	}	
	debug_sprintf(szDbgMsg, "%s: ---Return---", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
	
	return locpstKeyLoadLstInfo;	
}
#endif
/*
 * ============================================================================
 * Function Name: getSigCapCoordinates
 *
 * Description	: This function gives the coordinates of the signature capture
 *                box(Config variables)
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
void getSigCapCoordinates(int *startX, int *startY, int *endX, int *endY)
{
#ifdef DEBUG
	char	szDbgMsg[256] = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	*startX = uiSettings.signCfgPtr->startX;
	*startY = uiSettings.signCfgPtr->startY;
	*endX   = uiSettings.signCfgPtr->endX;
	*endY   = uiSettings.signCfgPtr->endY;

	debug_sprintf(szDbgMsg, "%s: %d-%d-%d-%d", __FUNCTION__,
											uiSettings.signCfgPtr->startX,
											uiSettings.signCfgPtr->startY,
											uiSettings.signCfgPtr->endX,
											uiSettings.signCfgPtr->endY);
	APP_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: getIdleScrnMode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int	getIdleScrnMode()
{
	return uiSettings.stMediaSettings.IdleScreenMode;
}

/*
 * ============================================================================
 * Function Name: getXPILibMode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL getXPILibMode()
{
	return uiSettings.bXPILibEnabled;
}
/*
 * ============================================================================
 * Function Name: getSignatureSettings
 *
 * Description	: This function is responsible for checking if the signature is
 * 					enabled and if yes, then for getting the settings for the
 * 					signature capture.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getSignatureSettings()
{
	int				rv				= SUCCESS;
	int				iLen			= 0;
	int				iVal			= 0;
	char			szParam[32]		= "";
	char			szTemp[10]		= "";
	SIG_PTR_TYPE	signCfgPtr		= NULL;
	int				devModel;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	devModel = svcInfoPlatform(7);

	iLen 	 = sizeof(szTemp);

	/* Check if signature capture is enabled in the configuration file */
	rv = getEnvFile(SECTION_PAYMENT, SIGCAP_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Bool value accordingly */
		if( (*szTemp == 'y') || (*szTemp == 'Y') )
		{
			uiSettings.sigCapEnabled = PAAS_TRUE;

			debug_sprintf(szDbgMsg, "%s: Signature Capture ENABLED",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			uiSettings.sigCapEnabled = PAAS_FALSE;

			debug_sprintf(szDbgMsg, "%s: Signature Capture DISABLED",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		uiSettings.sigCapEnabled = PAAS_FALSE;

		debug_sprintf(szDbgMsg,"%s: Signature Capture DISABLED (default action)"
																, __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* Get other configuration parameters for signature capture */
	signCfgPtr = (SIG_PTR_TYPE) malloc(sizeof(SIG_STRUCT_TYPE));
	if(signCfgPtr != NULL)
	{
		memset(signCfgPtr, 0x00, sizeof(SIG_STRUCT_TYPE));

		iLen = sizeof(szTemp);
		memset(szTemp, 0x00, sizeof(szTemp));

		/* ---------- Get Image type for signature capture-------- */

		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SIGN, SIGN_IMAGE_TYPE, szTemp, iLen);
		if(rv > 0)
		{
			if(strcmp(szTemp, SIGN_IMAGE_TYPE_TIFF) != 0 && strcmp(szTemp, SIGN_IMAGE_TYPE_BMP) != 0 &&
					strcmp(szTemp, SIGN_IMAGE_TYPE_3BA) != 0)
			{
				debug_sprintf(szDbgMsg, "%s: SIGN_IMAGE_TYPE is set to invalid value", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//Setting it to default value
				strcpy(signCfgPtr->szImageType, SIGN_IMAGE_TYPE_TIFF);
			}
			else
			{
				strcpy(signCfgPtr->szImageType, szTemp);
			}
		}
		else
		{
			/* Value Not Found */
			debug_sprintf(szDbgMsg, "%s: Signature image type not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//Setting it to default value
			strcpy(signCfgPtr->szImageType, SIGN_IMAGE_TYPE_TIFF);
		}

		debug_sprintf(szDbgMsg, "%s: Signature image type is %s", __FUNCTION__, signCfgPtr->szImageType);
		APP_TRACE(szDbgMsg);

		rv = SUCCESS;

		/* ---------- Get floor amount for Signature capture -------- */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SIGN, SIGN_AMT_FLOOR, szTemp, iLen);
		if(rv  > 0)
		{
			iVal = atof(szTemp);
			if(iVal > 0)
			{
				signCfgPtr->signAmt = iVal;

				debug_sprintf(szDbgMsg,
								"%s: Signature capture floor amount = [%d]",
								__FUNCTION__, iVal);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				/* Incorrect Value */
				debug_sprintf(szDbgMsg,
							"%s: Incorrect Signature capture floor amt [%d]"
							, __FUNCTION__, iVal);
				APP_TRACE(szDbgMsg);

				free(signCfgPtr);
				rv = FAILURE;
			}
		}
		else
		{
			/* Value Not Found */
			debug_sprintf(szDbgMsg,
							"%s: Signature capture floor amt not present",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);

			free(signCfgPtr);
			rv = FAILURE;
		}

		if(rv == SUCCESS)
		{
			/* ----------- Get Start X ------------- */
			sprintf(szParam, "%d_%s", devModel, SIGN_BEG_X);
			rv = getEnvFile(SECTION_SIGN, szParam, szTemp, iLen);
			if(rv  > 0)
			{
				iVal = atoi(szTemp);
				if(iVal > 0)
				{
					signCfgPtr->startX = iVal;
				}
				else
				{
					/* Incorrect Value - Set Default */
					if(devModel == MODEL_MX915)
					{
						signCfgPtr->startX = DLFT_915_SIG_BEGX;
					}
					else
					{
						signCfgPtr->startX = DLFT_925_SIG_BEGX;
					}
				}
			}
			else
			{
				/* Value Not Found  - Set Default */
				if(devModel == MODEL_MX915)
				{
					signCfgPtr->startX = DLFT_915_SIG_BEGX;
				}
				else
				{
					signCfgPtr->startX = DLFT_925_SIG_BEGX;
				}
			}

			/* ----------- Get Start Y ----------- */
			memset(szTemp, 0x00, sizeof(szTemp));
			sprintf(szParam, "%d_%s", devModel, SIGN_BEG_Y);
			rv = getEnvFile(SECTION_SIGN, szParam, szTemp, iLen);
			if(rv  > 0)
			{
				iVal = atoi(szTemp);
				if(iVal > 0)
				{
					signCfgPtr->startY = iVal;
				}
				else
				{
					/* Incorrect Value - Set Default */
					if(devModel == MODEL_MX915)
					{
						signCfgPtr->startY = DLFT_915_SIG_BEGY;
					}
					else
					{
						signCfgPtr->startY = DLFT_925_SIG_BEGY;
					}
				}
			}
			else
			{
				/* Value Not Found  - Set Default */
				if(devModel == MODEL_MX915)
				{
					signCfgPtr->startY = DLFT_915_SIG_BEGY;
				}
				else
				{
					signCfgPtr->startY = DLFT_925_SIG_BEGY;
				}
			}

			/* ----------- Get End X ------------ */
			memset(szTemp, 0x00, sizeof(szTemp));
			sprintf(szParam, "%d_%s", devModel, SIGN_END_X);
			rv = getEnvFile(SECTION_SIGN, szParam, szTemp, iLen);
			if(rv  > 0)
			{
				iVal = atoi(szTemp);
				if(iVal > 0)
				{
					signCfgPtr->endX = iVal;
				}
				else
				{
					/* Incorrect Value - Set Default */
					if(devModel == MODEL_MX915)
					{
						signCfgPtr->endX = DLFT_915_SIG_ENDX;
					}
					else
					{
						signCfgPtr->endX = DLFT_925_SIG_ENDX;
					}
				}
			}
			else
			{
				/* Value Not Found  - Set Default */
				if(devModel == MODEL_MX915)
				{
					signCfgPtr->endX = DLFT_915_SIG_ENDX;
				}
				else
				{
					signCfgPtr->endX = DLFT_925_SIG_ENDX;
				}
			}

			/* ------------ Get End Y ----------- */
			memset(szTemp, 0x00, sizeof(szTemp));
			sprintf(szParam, "%d_%s", devModel, SIGN_END_Y);
			rv = getEnvFile(SECTION_SIGN, szParam, szTemp, iLen);
			if(rv  > 0)
			{
				iVal = atoi(szTemp);
				if(iVal > 0)
				{
					signCfgPtr->endY = iVal;
				}
				else
				{
					/* Incorrect Value - Set Default */
					if(devModel == MODEL_MX915)
					{
						signCfgPtr->endY = DLFT_915_SIG_ENDY;
					}
					else
					{
						signCfgPtr->endY = DLFT_925_SIG_ENDY;
					}
				}
			}
			else
			{
				/* Value Not Found  - Set Default */
				if(devModel == MODEL_MX915)
				{
					signCfgPtr->endY = DLFT_915_SIG_ENDY;
				}
				else
				{
					signCfgPtr->endY = DLFT_925_SIG_ENDY;
				}
			}

			debug_sprintf(szDbgMsg, "%s: Signature Capture Beg X = [%d]",
										__FUNCTION__, signCfgPtr->startX);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Signature Capture Beg Y = [%d]",
										__FUNCTION__, signCfgPtr->startY);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Signature Capture End X = [%d]",
											__FUNCTION__, signCfgPtr->endX);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Signature Capture End Y = [%d]",
											__FUNCTION__, signCfgPtr->endY);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;

			/* All successfull */
			uiSettings.signCfgPtr = signCfgPtr;
		}
	}
	else
	{
		/* Memory Allocation failure for signature capture settings */
		debug_sprintf(szDbgMsg, "%s: Memory allocation failure",
															__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: getSigFloorAmount
 *
 * Description	: This function gives the config variable value
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
double getSigFloorAmount()
{
	return uiSettings.signCfgPtr->signAmt;
}

/*
 * ============================================================================
 * Function Name: getSigImageType
 *
 * Description	: This function gives the config variable value
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
void getSigImageType(char *pszImageType)
{
	strcpy(pszImageType, uiSettings.signCfgPtr->szImageType);
}

/*
 * ============================================================================
 * Function Name: isSigCapEnabled
 *
 * Description	: This function informs the caller if the signature capture is
 * 					enabled for PaaS transactions or not.
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isSigCapEnabled()
{
	return uiSettings.sigCapEnabled;
}


/*
 * ============================================================================
 * Function Name: loadMediaSettings
 *
 * Description	:
 *
 * Input Params	: buffer for any error message
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================ */
static int loadMediaSettings(char *pszErrMsg)
{
	int		rv					= SUCCESS;
	int		iLen				= 0;
	int		iCnt				= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szTemp[50]			= "";
	char **	tmpListPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iLen = sizeof(szTemp);

		/* Initialize the memory */
		memset(&(uiSettings.stMediaSettings), 0x00, sizeof(MEDIA_SET_STYPE));

		/* MukeshS3: PTMX-810, Amdocs Case # 160128-18771: SCA is not initializing when the imagelist1.dat file is not present in the media bundle and is stuck in the blue screen
		 * First, we need to load the idle screen mode of SCA. If, it is set to Full screen Image, Video or Animation than we dont need to have
		 * imageList & videoList present in device. If it is set to default idle screen mode, than imageList & videoList file must be present in device.
		 */
		/* Get the Control file name for Idle Screen Mode Selection */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_MEDIA, IDLE_SCREEN_MODE_FILE, szTemp, iLen);
		if(rv <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No value for [%s] exists in MCT table",
											__FUNCTION__, IDLE_SCREEN_MODE_FILE);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "No Value Found For [%s] in MCT Table", IDLE_SCREEN_MODE_FILE);
				addAppEventLog(SCA, PAAS_WARNING, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrMsg, MSG_MISSING_PARM_ERR);
			sprintf(pszErrMsg, "%s: %s",pszErrMsg, IDLE_SCREEN_MODE_FILE);
			rv = FAILURE;
			break;
		}
		/* VDR: Please remove this later */
		debug_sprintf(szDbgMsg, "%s: Idle Screen Control File = [%s]", __FUNCTION__, szTemp);
		APP_TRACE(szDbgMsg);

		/* Load the Idle Screen Control file */
		rv = loadIdleScrnModeValue(szTemp);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to load Idle Screen Mode parameter; setting it to default",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			uiSettings.stMediaSettings.IdleScreenMode = DEFAULT_IDLE;

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Unable to Load Idle Screen Mode Parameter, Setting it to Default Idle Screen.");
				addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
			}
			rv = SUCCESS;
		}

		// image list & video list are mandatory for only default mode idle screen(video with banner image)
		if(uiSettings.stMediaSettings.IdleScreenMode == DEFAULT_IDLE)
		{
			/* Get the image list file name for the idle screen */
			rv = getEnvFile(SECTION_MEDIA, IDLE_IMG_LIST_FILE, szTemp, iLen);
			if(rv <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: No value for [%s] exists in MCT table",
												__FUNCTION__, IDLE_IMG_LIST_FILE);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "No Value Found For [%s] in MCT Table", IDLE_IMG_LIST_FILE);
					addAppEventLog(SCA, PAAS_WARNING, PROCESSED, szAppLogData, NULL);
				}
				getDisplayMsg(pszErrMsg, MSG_MISSING_PARM_ERR);
				sprintf(pszErrMsg, "%s: %s",pszErrMsg, IDLE_IMG_LIST_FILE);
				rv = FAILURE;
				break;
			}

			/* VDR: Please remove this later */
			debug_sprintf(szDbgMsg, "%s: Image File = [%s]", __FUNCTION__, szTemp);
			APP_TRACE(szDbgMsg);

			/* Load the image list */
			uiSettings.stMediaSettings.stIdleScrImgSet.advtMediaType = IMAGE;
			rv = loadAdvtMedia(szTemp, &(uiSettings.stMediaSettings.stIdleScrImgSet));
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Unable to load Image List",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Unable to Load The Advertisement Image Media List");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
				}
				getDisplayMsg(pszErrMsg, MSG_MISSING_MEDIA_ERR);
				break;
			}

			/* Get the video list file name for the idle screen */
			memset(szTemp, 0x00, sizeof(szTemp));
			rv = getEnvFile(SECTION_MEDIA, VIDEO_LIST_FILE, szTemp, iLen);
			if(rv <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: No value for [%s] exists in MCT table",
												__FUNCTION__, VIDEO_LIST_FILE);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "No Value Found For [%s] in MCT Table", VIDEO_LIST_FILE);
					addAppEventLog(SCA, PAAS_WARNING, PROCESSED, szAppLogData, NULL);
				}
				getDisplayMsg(pszErrMsg, MSG_MISSING_PARM_ERR);
				sprintf(pszErrMsg, "%s: %s",pszErrMsg, VIDEO_LIST_FILE);
				rv = FAILURE;
				break;
			}

			/* VDR: Please remove this later */
			debug_sprintf(szDbgMsg, "%s: Video File = [%s]", __FUNCTION__, szTemp);
			APP_TRACE(szDbgMsg);

			/* Load the video list */
			uiSettings.stMediaSettings.stVideoSet.advtMediaType = VIDEO;
			rv = loadAdvtMedia(szTemp, &(uiSettings.stMediaSettings.stVideoSet));
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Unable to load Video List",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Unable to Load The Advertisement Video Media List");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
				}
				getDisplayMsg(pszErrMsg, MSG_MISSING_MEDIA_ERR);
				break;
			}
		}

		/* Get the image list file name for the line item screen */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_MEDIA, LI_IMG_LIST_FILE, szTemp, iLen);
		if(rv <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No value for [%s] exists in MCT table",
											__FUNCTION__, LI_IMG_LIST_FILE);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "No Value Found For [%s] in MCT Table", LI_IMG_LIST_FILE);
				addAppEventLog(SCA, PAAS_WARNING, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrMsg, MSG_MISSING_PARM_ERR);
			sprintf(pszErrMsg, "%s: %s",pszErrMsg, LI_IMG_LIST_FILE);
			rv = FAILURE;
			break;
		}

		/* VDR: Please remove this later */
		debug_sprintf(szDbgMsg, "%s: LIImage File = [%s]", __FUNCTION__, szTemp);
		APP_TRACE(szDbgMsg);

		/* Load the image list */
		uiSettings.stMediaSettings.stLIScrImgSet.advtMediaType = IMAGE;
		rv = loadAdvtMedia(szTemp, &(uiSettings.stMediaSettings.stLIScrImgSet));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to load Image List",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Unable to Load The Line Item Image Media List");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			getDisplayMsg(pszErrMsg, MSG_MISSING_MEDIA_ERR);
			break;
		}

		break;
	}

	if(rv != SUCCESS)
	{
		/* Flush out all the media lists from the structure */
		tmpListPtr = uiSettings.stMediaSettings.stIdleScrImgSet.mediaList;
		if(tmpListPtr != NULL)
		{
			for(iCnt = 0; iCnt < MAX_IMAGE_COUNT; iCnt++)
			{
				if(tmpListPtr[iCnt] == NULL)
				{
					break;
				}

				free(tmpListPtr[iCnt]);
			}
		}

		tmpListPtr = uiSettings.stMediaSettings.stLIScrImgSet.mediaList;
		if(tmpListPtr != NULL)
		{
			for(iCnt = 0; iCnt < MAX_IMAGE_COUNT; iCnt++)
			{
				if(tmpListPtr[iCnt] == NULL)
				{
					break;
				}

				free(tmpListPtr[iCnt]);
			}
		}

		tmpListPtr = uiSettings.stMediaSettings.stVideoSet.mediaList;
		if(tmpListPtr != NULL)
		{
			for(iCnt = 0; iCnt < MAX_VIDEO_COUNT; iCnt++)
			{
				if(tmpListPtr[iCnt] == NULL)
				{
					break;
				}

				free(tmpListPtr[iCnt]);
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadAdvtMedia
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int loadAdvtMedia(char * szFileName, MEDIA_UPD_PTYPE mediaSetPtr)
{
	int			rv			= SUCCESS;
	int			iCnt		= 0;
	int			jCnt		= 0;
	int			iSize		= 0;
	int			iMaxElem	= 0;
	int			iTimeIntvl	= 0;
	char *		tmpPtr		= NULL;
	char *		cPtr		= NULL;
	FILE *		fp			= NULL;
	char		tmpStr[50]	= "";
	char		szFile[60]	= "";
	char *		tmpList[50]	= {NULL};
	PAAS_BOOL	bFirstTime	= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: File Name = [%s]", __FUNCTION__, szFileName);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check if the file exists */
		rv = doesFileExist(szFileName);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Media list file [%s] doesnt exist",
												__FUNCTION__, szFileName);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* open the file for reading the media file names */
		fp = fopen(szFileName, "r");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Couldn't open file [%s]", __FUNCTION__,
																	szFileName);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;

			break;
		}

		switch(mediaSetPtr->advtMediaType)
		{
		case IMAGE:
			iMaxElem = MAX_IMAGE_COUNT;
			break;

		case VIDEO:
			iMaxElem = MAX_VIDEO_COUNT;
			break;

		default:

			debug_sprintf(szDbgMsg, "%s: Should never come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			/* Stop further processing if some procedure above failed */
			break;
		}

		/* read the file and set the list */
		while( (!feof(fp)) && (iCnt < iMaxElem) )
		{
			if(fgets(tmpStr, 50, fp) != NULL)
			{
				/* Check and Remove LF */
				if((cPtr = strchr(tmpStr, LF)) != NULL)
				{
					*cPtr = 0x00;
				}

				/* Check and Remove CR */
				if((cPtr = strchr(tmpStr, CR)) != NULL)
				{
					*cPtr = 0x00;
				}

				iSize = stripSpaces(tmpStr, STRIP_TRAILING_SPACES |
														STRIP_LEADING_SPACES);

				/* The first line in the image list file should contain the
				 * time interval of image update mentioned. So while reading
				 * the file, if it is an image list, get the time interval from
				 * the first line and the name of the images from the rest of
				 * the lines */
				if(bFirstTime == PAAS_TRUE)
				{
					bFirstTime = PAAS_FALSE;

					if(mediaSetPtr->advtMediaType == IMAGE)
					{
						/* Read the time out interval for the image list */
						if(iSize > 0)
						{
							if(iSize == strspn(tmpStr, "1234567890"))
							{
								/* The first line is a numeric value, it means
								 * the timeout is specified in the first line */
								iTimeIntvl = atoi(tmpStr);
								if(iTimeIntvl > MAX_IMG_UPD_TIME)
								{
									iTimeIntvl = MAX_IMG_UPD_TIME;
								}
								else if(iTimeIntvl <= 0)
								{
									iTimeIntvl = DFLT_IMG_UPD_TIME;
								}
							}
							else
							{
								/* Check if its an image file name and the file
								 * exists in the flash directory */
								sprintf(szFile, "./flash/%s", tmpStr);

								rv = doesFileExist(szFile);
								if(rv == SUCCESS)
								{
									iSize += 1;
									tmpPtr = (char *) malloc(iSize);
									memset(tmpPtr, 0x00, iSize);
									memcpy(tmpPtr, tmpStr, iSize - 1);

									tmpList[iCnt++] = tmpPtr;
								}

								/* Set default time interval for the list */
								iTimeIntvl = DFLT_IMG_UPD_TIME;
							}
						}
						else
						{
							iTimeIntvl = DFLT_IMG_UPD_TIME;
						}

						mediaSetPtr->mediaUpdIntvl = iTimeIntvl;

						continue;
					}
				}

				/* Read the media names */
				if(iSize > 0)
				{
					/* Check if the file exists */
					sprintf(szFile, "./flash/%s", tmpStr);

					rv = doesFileExist(szFile);
					if(rv == SUCCESS)
					{
						iSize += 1;
						tmpPtr = (char *) malloc(iSize);
						memset(tmpPtr, 0x00, iSize);
						memcpy(tmpPtr, tmpStr, iSize - 1);

						tmpList[iCnt++] = tmpPtr;
					}
				}
			}
		}

		if(iCnt > 0)
		{
			mediaSetPtr->mediaList = (char **) malloc((iCnt+1)* sizeof(char *));
			if(mediaSetPtr->mediaList != NULL)
			{
				memset(mediaSetPtr->mediaList,0x00,(iCnt + 1) * sizeof(char *));

				for(jCnt = 0; jCnt < iCnt; jCnt++)
				{
					mediaSetPtr->mediaList[jCnt] = tmpList[jCnt];
				}
			}
			else
			{
				for(jCnt = 0; jCnt < iCnt; jCnt++)
				{
					free(tmpList[jCnt]);
					tmpList[jCnt] = NULL;
				}
			}
		}
		else
		{
			/* If not even a single element was added, return error */
			debug_sprintf(szDbgMsg, "%s: No media element added in the list",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		break;
	}

	/* Daivik : 27/1/2016 - Coverity Issue - 67282 Close file here to ensure we close it in all cases */
	if(fp != NULL)
	{
		/* Close the media list file after reading */
		fclose(fp);
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadIdleScrnMode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int loadIdleScrnModeValue(char * szFileName)
{
	int			rv			= SUCCESS;
	int			iSize		= 0;
	int			iLen		= 0;
	int			iVal		= 0;
	FILE *		fp			= NULL;
	char		szTemp[20]	= "";
	char *		cPtr		= NULL;
	char		tmpStr[50]	= "";
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: File Name = [%s]", __FUNCTION__, szFileName);
	APP_TRACE(szDbgMsg);

	uiSettings.stMediaSettings.IdleScreenMode = DEFAULT_IDLE;		// setting it to default value initially.

	while(1)
	{
		/* Check if the file exists */
		rv = doesFileExist(szFileName);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: [%s] file doesn't exist; setting Idle Screen mode to default",
												__FUNCTION__, szFileName);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;

			break;
		}

		/* open the file for reading the FULL Screen Modes */
		fp = fopen(szFileName, "r");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Couldn't open file [%s]", __FUNCTION__,
																	szFileName);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;

			break;
		}

		/* read the file and set the IdleScreenMode value */
		while( (!feof(fp)) )
		{
			if(fgets(tmpStr, 50, fp) != NULL)
			{
				/* Check and Remove LF */
				if((cPtr = strchr(tmpStr, LF)) != NULL)
				{
					*cPtr = 0x00;
				}

				/* Check and Remove CR */
				if((cPtr = strchr(tmpStr, CR)) != NULL)
				{
					*cPtr = 0x00;
				}

				iSize = stripSpaces(tmpStr, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

				if(iSize > 0)
				{
					//strupper(tmpStr);		//converting to Upper Case letters.

					if(strcasecmp(tmpStr, "IMAGE") == SUCCESS)
					{
						uiSettings.stMediaSettings.IdleScreenMode = FULL_IDLE_IMG;
					}
					else if(strcasecmp(tmpStr, "VIDEO") == SUCCESS)
					{
						uiSettings.stMediaSettings.IdleScreenMode = FULL_IDLE_VID;
					}
					else if(strcasecmp(tmpStr, "ANIMATION") == SUCCESS)
					{
						uiSettings.stMediaSettings.IdleScreenMode = FULL_IDLE_ANIM;

						iLen = sizeof(szTemp);
						memset(szTemp, 0x00, iLen);
						rv = getEnvFile(SECTION_MEDIA, ANAIMATION_UPDATE_INTV, szTemp, iLen);

						if(rv > 0)
						{
							iVal = atoi(szTemp);

							if( (iVal > 0) && (iVal <= 30))
							{
								debug_sprintf(szDbgMsg,"%s: Value found for Animation update Interval = %d", __FUNCTION__, iVal);
								APP_TRACE(szDbgMsg);
								uiSettings.stMediaSettings.IdleAnimUpdIntvl = iVal;
							}
							else
							{
								/* Incorrect value for Animation Update Interval */
								debug_sprintf(szDbgMsg, "%s: Incorrect val for Animation Update Interval, Setting default %d", __FUNCTION__, DFLT_FA_PORT);
								APP_TRACE(szDbgMsg);

								uiSettings.stMediaSettings.IdleAnimUpdIntvl = DFLT_ANIM_UPDT_INTV;
							}
						}
						else
						{
							/* No value found for internal_tap_flag*/
							debug_sprintf(szDbgMsg,"%s: No value found for Animation update Interval, Setting to default", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							uiSettings.stMediaSettings.IdleAnimUpdIntvl = DFLT_ANIM_UPDT_INTV;
						}
					}
					else if(strcasecmp(tmpStr, "VIDEO_BANNER") == SUCCESS)
					{
						uiSettings.stMediaSettings.IdleScreenMode = DEFAULT_IDLE;
					}
					else
					{
						uiSettings.stMediaSettings.IdleScreenMode = DEFAULT_IDLE;		// if anything else is defined, other than specified values; set to default
					}
					rv = SUCCESS;
					break;
				}
			}
		}

	/* Close the idlescrnmode.dat file after reading */
	fclose(fp);

	break;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getNxtVideoFileName
 *
 * Description	: This function would get the next video in the list to be
 * 					played, API to be called by UI Interface media manager
 *
 * Input Params	: fileName -> name of next file would be saved here
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getNxtVideoFileName(char *fileName)
{
	int			rv			= SUCCESS;
	char **		vidFileList	= NULL;
	static int	curCount	= 0;

	vidFileList = uiSettings.stMediaSettings.stVideoSet.mediaList;

	if(fileName != NULL)
	{
		if((curCount >= MAX_VIDEO_COUNT)|| (vidFileList[curCount] == NULL))
		{
			curCount = 0;
		}

		sprintf(fileName, "%s", vidFileList[curCount]);
		curCount++;
	}
	else
	{
		rv = FAILURE;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getNxtImageFileName
 *
 * Description	: This function would get the next image file in the list to be
 * 					displayed, API to be called by UI Interface media manager
 *
 * Input Params	: fileName -> name of next file would be saved here
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getNxtImageFileName(char *fileName, UI_FORM_ENUM eForm)
{
	int			rv				= SUCCESS;
	char **		imgFileList		= NULL;
	static int	liCurCnt		= 0;
	static int	idleCurCnt		= 0;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	while(1)
	{
		if(fileName == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		switch(eForm)
		{
		case IDLE_DISP_FRM:

			imgFileList = uiSettings.stMediaSettings.stIdleScrImgSet.mediaList;

			if( (idleCurCnt >= MAX_IMAGE_COUNT) ||
										(imgFileList[idleCurCnt] == NULL))
			{
				idleCurCnt = 0;
			}

			sprintf(fileName, "%s", imgFileList[idleCurCnt]);
			idleCurCnt++;

			break;

		case LI_DISP_FRM:

			imgFileList = uiSettings.stMediaSettings.stLIScrImgSet.mediaList;

			if((liCurCnt >= MAX_IMAGE_COUNT) || (imgFileList[liCurCnt] == NULL))
			{
				liCurCnt = 0;
			}

			sprintf(fileName, "%s", imgFileList[liCurCnt]);
			liCurCnt++;

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		break;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getImageUpdateIntvl
 *
 * Description	: This function will return back the image update interval set
 *					by the users
 *
 * Input Params	: none
 *
 * Output Params: update interval currently set
 * ============================================================================
 */
int getImageUpdateIntvl(UI_FORM_ENUM eForm)
{
	int		iTimeIntvl	= 0;

	switch(eForm)
	{
	case IDLE_DISP_FRM:
		iTimeIntvl = uiSettings.stMediaSettings.stIdleScrImgSet.mediaUpdIntvl;
		break;

	case LI_DISP_FRM:
		iTimeIntvl = uiSettings.stMediaSettings.stLIScrImgSet.mediaUpdIntvl;
		break;

	case IDLE_DISP_ANIM_FRM:
		iTimeIntvl = uiSettings.stMediaSettings.IdleAnimUpdIntvl;
		break;
	default:
		break;
	}

	return iTimeIntvl;
}

/*
 * ============================================================================
 * Function Name: initializeUISettings
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
static void initializeUISettings()
{
	int	iCnt			= 0;
	int	iNode			= 0;

	if(uiSettings.pstKeyLoadLstInfo != NULL)
	{
		free(uiSettings.pstKeyLoadLstInfo);
	}
	
	if(uiSettings.signCfgPtr != NULL)
	{
		free(uiSettings.signCfgPtr);
	}

	if(uiSettings.pstDispMsgsInfo != NULL)
	{
		PROMPT_PTR_TYPE		pstTmpNode		= NULL;

		for(iNode = 0; iNode < MAX_SUPPORT_LANG; iNode++)
		{
			pstTmpNode = uiSettings.pstDispMsgsInfo->index[iNode];
			
			/* Flush the structure of any residual details */
			for(iCnt = 0; iCnt < MAX_TITLE_IDX; iCnt++)
			{
				if(pstTmpNode->scrTitles[iCnt] != NULL)
				{
					free(pstTmpNode->scrTitles[iCnt]);
					pstTmpNode->scrTitles[iCnt] = NULL;
				}
			}

			for(iCnt = 0; iCnt < MAX_DISP_MSGS; iCnt++)
			{
				if(pstTmpNode->dispMsgs[iCnt] != NULL)
				{
					free(pstTmpNode->dispMsgs[iCnt]);
					pstTmpNode->dispMsgs[iCnt] = NULL;
				}
			}

			for(iCnt = 0; iCnt < MAX_BTNLBL_IDX; iCnt++)
			{
				if(pstTmpNode->btnLbls[iCnt] != NULL)
				{
					free(pstTmpNode->btnLbls[iCnt]);
					pstTmpNode->btnLbls[iCnt] = NULL;
				}
			}
					
			free(pstTmpNode);
		}
	}	

	memset(&uiSettings, 0x00, sizeof(UI_CFG_STYPE));

	return;
}

/*
 * ============================================================================
 * Function Name: getDefaultLang
 *
 * Description	: This function will return the default language
 *
 * Input Params	: Form enum value
 *
 * Output Params: time interval
 * ============================================================================
 */
char* getDefaultLang()
{
	return uiSettings.szLangType;
}

/*
 * ============================================================================
 * Function Name: getLaneClosedFontCol
 *
 * Description	: This function will return the default Lane Closed Font Color From Config File
 *
 * Input Params	:
 *
 * Output Params: Font Color From Config File
 * ============================================================================
 */
char* getLaneClosedFontCol()
{
	return uiSettings.szLaneClosedFontColor;
}

/*
 * ============================================================================
 * Function Name: setLaneClosedFontCol
 *
 * Description	: This function will Set the Font Color value to the UI settings memory.
 *
 * Input Params	: Font Color
 *
 * Output Params:
 * ============================================================================
 */
void setLaneClosedFontCol(char *pszFontCol)
{
	if(pszFontCol != NULL && strlen(pszFontCol))
	{
		strcpy(uiSettings.szLaneClosedFontColor, pszFontCol);
		putEnvFile(SECTION_DEVICE, LANE_CLOSED_FONT_COL, pszFontCol);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: getLaneClosedFontSize
 *
 * Description	: This function will return the default Lane Closed Font Size From Config File
 *
 * Input Params	:
 *
 * Output Params: Font Size From Config File
 * ============================================================================
 */
int getLaneClosedFontSize()
{
	return uiSettings.iLaneClosedFontSize;
}

/*
 * ============================================================================
 * Function Name: setLaneClosedFontSize
 *
 * Description	: This function will Set the Lane Closed Font Size to uUIi settings memory
 *
 * Input Params	: Font Size From Config File
 *
 * Output Params:
 * ============================================================================
 */
void setLaneClosedFontSize(int iFontSize)
{
	char 	szFontSize[5] = "";

	uiSettings.iLaneClosedFontSize = iFontSize;
	sprintf(szFontSize, "%d", iFontSize);
	putEnvFile(SECTION_DEVICE, LANE_CLOSED_FONT_SIZE, szFontSize);

	return;
}

/*
 * ============================================================================
 * Function Name: getAdvanceVSPDays
 *
 * Description	: This function will return the days for advancing VSP
 *
 * Input Params	: Form enum value
 *
 * Output Params: time interval in days
 * ============================================================================
 */
int getAdvanceVSPDays()
{
	return uiSettings.advancevspdays;
}

/*
 * ============================================================================
 * Function Name: getStatusMsgDispInterval
 *
 * Description	: This function will return the status msg disp interval
 *
 * Input Params	: Form enum value
 *
 * Output Params: time interval
 * ============================================================================
 */
int getStatusMsgDispInterval()
{
	return uiSettings.statusMsgDispInterval;
}

/*
 * ============================================================================
 * Function Name: getMsgDispIntvl
 *
 * Description	: This function will return back the time interval for sysinfo and ip address display.
 *
 * Input Params	: Form enum value
 *
 * Output Params: time interval
 * ============================================================================
 */
int getMsgDispIntvl(UI_FORM_ENUM eForm)
{
	int		iTimeIntvl	= 0;

	switch(eForm)
	{
	case IP_DISP_FRM:
		iTimeIntvl = uiSettings.ipdispinterval;
		break;

	case SYSINFO_FRM:
		iTimeIntvl = uiSettings.sysinfointerval;
		break;

	case EMAIL_FRM:
		iTimeIntvl = uiSettings.devnamedispinterval;
		break;

	default:
		break;
	}

	return iTimeIntvl;
}

/*
 * ============================================================================
 * Function Name: getPreambleSelectWaitTime
 *
 * Description	: This function will return Wait Time for the Preamble Select Screen.
 *
 * Input Params	: Form enum value
 *
 * Output Params: time interval
 * ============================================================================
 */
int getPreambleSelectWaitTime()
{
	return uiSettings.iPreambleSelectWaitTime;
}

/*
 * ============================================================================
 * Function Name: getPinPadMode
 *
 * Description	: This API would be called to get pin pad mode (DUKPT mode or Master/Session mode).
 *
 * Input Params	: none
 *
 * Output Params: 1(DUKPT mode)/ 0( M/S mode)
 * ============================================================================
 */
int getPinPadMode()
{
	return uiSettings.pinpadmode;
}
#if 0
/*
 * ============================================================================
 * Function Name: loadEmvDefaultValues
 *
 * Description	: This function is responsible for loading the EMV download initialization 
 *				  default values which are need to build XPI "D" commands.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int loadEmvDefaultValues()
{
	int		rv									= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Check if the EMV default values file exits and has some data */
	rv = doesFileExist(CFG_INI_FILE_PATH);
	if(rv == SUCCESS)
	{
		/* Read and store the default values */
		rv = storeEmvDfltValues(CFG_INI_FILE_PATH, &(uiSettings.pstKeyLoadLstInfo));
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: File [%s] does not exist", __FUNCTION__,
				CFG_INI_FILE_PATH);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeEmvDfltValues
 *
 * Description	: This function is responsible for parsing the input ini file
 * 					and storing the emv download initialization default values 
 * 					in the memory Application ID(AppID) wise.
 *
 * Input Params	: fileName	  		 -> name of the file containing prompts
 * 				  dpstKeyLoadLstInfo -> list where the parameters would be stored
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeEmvDfltValues(char * fileName, EMVKEYLOAD_LST_INFO_PTYPE  *dpstKeyLoadLstInfo)
{
	int								rv						= SUCCESS;
	int								iCnt					= 0;
	int								iSectionCnt				= 0;
	char *							pszSecName				= NULL;
	dictionary *					dict					= NULL;
	EMVKEYLOAD_LST_INFO_PTYPE  		pstKeyLoadLstInfo 		= NULL;
	EMVKEYLOAD_LIST_PTYPE 			pstKeyLoadList			= NULL;
	EMVKEYLOAD_LIST_STYPE 			stKeyLoadList;	
		
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Load the ini file in the parser library */
	dict = iniparser_load(fileName);
	if(dict == NULL)
	{
		/* Unable to load the ini file */
		debug_sprintf(szDbgMsg, "%s: Unable to parse ini file [%s]",
												__FUNCTION__, fileName);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	/* Get the number of RID(Registered Application provider identifier) 
		stored in the ini file */
	iSectionCnt = iniparser_getnsec(dict);
	if(iSectionCnt <= 0)
	{
		debug_sprintf(szDbgMsg, "%s: No sections present in ini file [%s]",
							__FUNCTION__, fileName);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	debug_sprintf(szDbgMsg, "%s: Number of sections present = [%d]",
					__FUNCTION__, iSectionCnt);
	APP_TRACE(szDbgMsg);
	/* Copy EMV default application count(No.of RID's present in 
		ini file) value*/
	uiSettings.iAppIDCnt = iSectionCnt -3 ; //Don't count D18, D20, D25 sections as APPID count. so decrement by 3
	
	/* Allocate memory */
	if(pstKeyLoadLstInfo == NULL)
	{
		pstKeyLoadLstInfo = (EMVKEYLOAD_LST_INFO_PTYPE) malloc(sizeof(EMVKEYLOAD_LST_INFO_STYPE));
		if(pstKeyLoadLstInfo == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
	}
	memset(pstKeyLoadLstInfo, 0x00, sizeof(EMVKEYLOAD_LST_INFO_STYPE));	
	
	while(1)
	{
		/* Get the language definitions */
		for(iCnt = 0; iCnt < iSectionCnt; iCnt++)
		{
			pszSecName = iniparser_getsecname(dict, iCnt);

			debug_sprintf(szDbgMsg, "%s: iCnt[%d], Section Name[%s]", __FUNCTION__,
																iCnt, pszSecName);
			APP_TRACE(szDbgMsg);
		
			memset(&stKeyLoadList, 0x00, sizeof(EMVKEYLOAD_LIST_STYPE));
			
			/* Convert string to upper case */
			strToUpper(pszSecName);
			
			if( !strcmp(pszSecName, "D18") )
			{
				/* store the D18 default values in keyload list node*/
				rv = storeD18DfltValues(dict, pszSecName, pstKeyLoadLstInfo);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to store EMV D18, D20 & D25 default values",
																						__FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}				
			}
			else if( !strcmp(pszSecName, "D20") )
			{
				/* store the D18 default values in keyload list node*/
				rv = storeD20DfltValues(dict, pszSecName, pstKeyLoadLstInfo);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to store EMV D18, D20 & D25 default values",
																						__FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}				
			}
			else if( !strcmp(pszSecName, "D25") )
			{
				/* store the D25 default values in keyload list node*/
				rv = storeD25DfltValues(dict, pszSecName, pstKeyLoadLstInfo);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to store EMV D18, D20 & D25 default values",
																						__FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}				
			}			
			else
			{
				/* Copy Application ID*/
				strcpy(stKeyLoadList.szAppID, pszSecName);		
				
				/* store the all default values corresponds to unique AppID 
					in keyload list node*/
				rv = storeAppIDDfltValues(dict, &stKeyLoadList);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to store EMV download initialization default values",
																									__FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}
				pstKeyLoadList = (EMVKEYLOAD_LIST_PTYPE) malloc(sizeof(EMVKEYLOAD_LIST_STYPE));
				if(pstKeyLoadList == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(pstKeyLoadList, 0x00, sizeof(EMVKEYLOAD_LIST_STYPE));
				memcpy(pstKeyLoadList, &stKeyLoadList, sizeof(EMVKEYLOAD_LIST_STYPE));
				
				if(pstKeyLoadLstInfo->mainKeyLoadLstHead == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Adding First Node of keyload list",__FUNCTION__);
					APP_TRACE(szDbgMsg);		
					pstKeyLoadLstInfo->mainKeyLoadLstHead = pstKeyLoadList;
					pstKeyLoadLstInfo->mainKeyLoadLstTail = pstKeyLoadList;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Adding Node to existing keyload linked list",__FUNCTION__);
					APP_TRACE(szDbgMsg);		
					pstKeyLoadLstInfo->mainKeyLoadLstTail->nextEmvNode = pstKeyLoadList;
					pstKeyLoadLstInfo->mainKeyLoadLstTail = pstKeyLoadList;
				}
				pstKeyLoadList = NULL;	
			}	
		}
			
		/* Allocate memory */
		if(*dpstKeyLoadLstInfo == NULL)
		{
			*dpstKeyLoadLstInfo = (EMVKEYLOAD_LST_INFO_PTYPE) malloc(sizeof(EMVKEYLOAD_LST_INFO_STYPE));
			if(*dpstKeyLoadLstInfo == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return FAILURE;
			}
		}
		memset(*dpstKeyLoadLstInfo, 0x00, sizeof(EMVKEYLOAD_LST_INFO_STYPE));		
		/* copy data to output buffer*/
		memcpy(*dpstKeyLoadLstInfo, pstKeyLoadLstInfo, sizeof(EMVKEYLOAD_LST_INFO_STYPE));
		break;
	}

	if(rv != SUCCESS)
	{
		EMVKEYLOAD_LIST_PTYPE	pstTmpNode	= NULL;

		pstKeyLoadList = pstKeyLoadLstInfo->mainKeyLoadLstHead;
		while(pstKeyLoadList != pstKeyLoadLstInfo->mainKeyLoadLstTail)
		{
			pstTmpNode = pstKeyLoadList;
			pstKeyLoadList  = pstKeyLoadList->nextEmvNode;
			free(pstTmpNode);
		}
	}
	
	/* Free the dictionary */
	if(dict != NULL)
	{
		iniparser_freedict(dict);
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name:
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeAppIDDfltValues(dictionary * dict, EMVKEYLOAD_LIST_PTYPE pstKeyLoadLst)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		jCnt			= 0;
	int 	iNumRecord		= 0;
	int		iLen			= 0;
	char	szKey[50]		= "";
	char 	szSuppAID[33]	= "";
    char *	curPtr			= NULL;
	char *	nxtPtr			= NULL;	
	char *	value			= NULL;
	uchar*	cTmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < MAX_DFLT_VAL; iCnt++)
	{	
	
		sprintf(szKey, "%s:%s_%s", pstKeyLoadLst->szAppID, pstKeyLoadLst->szAppID, 
														dfltAppIdValList[iCnt]);
		value = iniparser_getstring(dict, szKey, NULL);
		if(value == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing default value at %s",
														__FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);

			continue;
			//TODO: currently continuing even though some values are missing. Check later is it correct behavior??.
			//rv = FAILURE;
			//break;
		}

		iLen = strlen(value);
		cTmpPtr = (uchar *) malloc(iLen + 1);
		if(cTmpPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(cTmpPtr, 0x00, iLen + 1);
		memcpy(cTmpPtr, value, iLen);

		stripSpaces((char *)cTmpPtr,STRIP_LEADING_SPACES|STRIP_TRAILING_SPACES);

		debug_sprintf(szDbgMsg, "%s: Key[%s] Value [%s]",
											__FUNCTION__, szKey, (char *)cTmpPtr);
		APP_TRACE(szDbgMsg);		
		
		if( !strcmp(dfltAppIdValList[iCnt], "SUPPORTED") )
		{
			debug_sprintf(szDbgMsg, "%s: Need to parse[%s] for Supported AID",
												__FUNCTION__, (char *)cTmpPtr);		
			curPtr = (char *)cTmpPtr;
			nxtPtr = strtok(curPtr, ",");
			while(nxtPtr != NULL && (rv == SUCCESS))
			{
				//Get the supported AID.
				memset(szSuppAID, 0x00, sizeof(szSuppAID));				
				memcpy(szSuppAID, nxtPtr, strlen(nxtPtr));
				debug_sprintf(szDbgMsg, "%s: Got Supported AID[%s]",
													__FUNCTION__, szSuppAID);
				APP_TRACE(szDbgMsg);				

				//Store Supported AID into EST and EEST tables.
				saveDfltValue(iCnt, (char *)szSuppAID, pstKeyLoadLst);				
						
				//Get default values for given supported Aid.
				jCnt 		= iCnt + 1; //Go to next record in the ini file.
				iNumRecord 	= 0;
				while( iNumRecord < 7)	//Checking for only 7 record for D17 command
				{					
					memset(szKey, 0x00, sizeof(szKey));
					sprintf(szKey, "%s:%s_%s", pstKeyLoadLst->szAppID, szSuppAID, 
																	dfltAppIdValList[jCnt]);
					value = iniparser_getstring(dict, szKey, NULL);
					if(value == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Missing default value at %s",
																	__FUNCTION__, szKey);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}

					iLen = strlen(value);
					cTmpPtr = (uchar *) malloc(iLen + 1);
					if(cTmpPtr == NULL)
					{
						debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}

					memset(cTmpPtr, 0x00, iLen + 1);
					memcpy(cTmpPtr, value, iLen);

					stripSpaces((char *)cTmpPtr,STRIP_LEADING_SPACES|STRIP_TRAILING_SPACES);

					debug_sprintf(szDbgMsg, "%s: Key[%s] Value [%s]",
														__FUNCTION__, szKey, (char *)cTmpPtr);
					APP_TRACE(szDbgMsg);		
					
					saveDfltValue(jCnt, (char *)cTmpPtr, pstKeyLoadLst);				
		
					//Increment the default value read counter.
					jCnt++;
					
					//Increment the D17 record count.
					iNumRecord++;
					
					cTmpPtr = NULL;
					value = NULL;					
				}
				//Increment the supported Aid counter.
				pstKeyLoadLst->iNumSuppAid++;
				
				//curPtr = nxtPtr + 1;				
				nxtPtr = strtok(NULL, ",");	//Get next supported AID
			}
			//Decrement last counter to make supported AID count correct.
			pstKeyLoadLst->iNumSuppAid--;
			
			curPtr = NULL;
			nxtPtr = NULL;
			if(jCnt > 0)
			{
				iCnt += 7;
			}	
		}
		else
		{
			/* Save value into key load list node */
			saveDfltValue(iCnt, (char *)cTmpPtr, pstKeyLoadLst);			
		}	
		cTmpPtr = NULL;
		value = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name:
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeD18DfltValues(dictionary * dict, char *pszSecName, EMVKEYLOAD_LST_INFO_PTYPE pstKeyLoadLstInfo)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		iLen			= 0;
	char	szKey[50]		= "";
	char *	value			= NULL;
	uchar *	cTmpPtr			= NULL;
				  
#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < MAX_D18_DFLT_VAL; iCnt++)
	{		
		sprintf(szKey, "%s:%s", pszSecName, dfltD18ValList[iCnt]);
		value = iniparser_getstring(dict, szKey, NULL);
		if(value == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing default value at %s",
														__FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		iLen = strlen(value);
		cTmpPtr = (uchar *) malloc(iLen + 1);
		if(cTmpPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(cTmpPtr, 0x00, iLen + 1);
		memcpy(cTmpPtr, value, iLen);

		stripSpaces((char *)cTmpPtr,STRIP_LEADING_SPACES|STRIP_TRAILING_SPACES);

		switch(iCnt)
		{
			case BELLOW_FLR_LIMIT_TERM_CAPABILITIES:
			case ABOVE_FLR_LIMIT_TERM_CAPABILITIES:
			case CTLSCVM_FLR_LIMIT:

			/* store Other default values */
			saveD18DlftValues(iCnt, (char *)cTmpPtr, &(pstKeyLoadLstInfo->stOtherRecInfo));
			break;	
			
		default:
			debug_sprintf(szDbgMsg, "%s: key[%d], Value[%s] is unknown default value, Not saving", 
																__FUNCTION__, iCnt, (char *)cTmpPtr);
			APP_TRACE(szDbgMsg);
			break;
		}				
		cTmpPtr = NULL;
		value = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name:
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeD20DfltValues(dictionary * dict, char *pszSecName, EMVKEYLOAD_LST_INFO_PTYPE pstKeyLoadLstInfo)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		iLen			= 0;
	char	szKey[50]		= "";
	char *	value			= NULL;
	uchar *	cTmpPtr			= NULL;
				  
#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < MAX_D20_DFLT_VAL; iCnt++)
	{		
		sprintf(szKey, "%s:%s", pszSecName, dfltD20ValList[iCnt]);
		value = iniparser_getstring(dict, szKey, NULL);
		if(value == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing default value at %s",
														__FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		iLen = strlen(value);
		cTmpPtr = (uchar *) malloc(iLen + 1);
		if(cTmpPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(cTmpPtr, 0x00, iLen + 1);
		memcpy(cTmpPtr, value, iLen);

		stripSpaces((char *)cTmpPtr,STRIP_LEADING_SPACES|STRIP_TRAILING_SPACES);

		switch(iCnt)
		{
		case REC_NUMBER:
		case CTLS_MERCHANT_TYPE:
		case CTLS_TERM_TRAN_INFO:
		case CTLS_TERM_TRAN_TYPE:
		case CTLS_REQ_RECIEPT_LIMIT:
		case CTLS_OPTION_STATUS:
		case CTLS_READER_FLR_LIMIT:
		
			/* store ICCT table default values */
			saveIcctTableDlftValues(iCnt, (char *)cTmpPtr, &(pstKeyLoadLstInfo->stIcctRecInfo));
			break;	
			
		default:
			debug_sprintf(szDbgMsg, "%s: key[%d], Value[%s] is unknown default value, Not saving", 
																__FUNCTION__, iCnt, (char *)cTmpPtr);
			APP_TRACE(szDbgMsg);
			break;
		}				
		cTmpPtr = NULL;
		value = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name:
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeD25DfltValues(dictionary * dict, char *pszSecName, EMVKEYLOAD_LST_INFO_PTYPE pstKeyLoadLstInfo)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		iLen			= 0;
	char	szKey[50]		= "";
	char *	value			= NULL;
	uchar *	cTmpPtr			= NULL;
				  
#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < MAX_D25_DFLT_VAL; iCnt++)
	{		
		sprintf(szKey, "%s:%s", pszSecName, dfltD25ValList[iCnt]);
		value = iniparser_getstring(dict, szKey, NULL);
		if(value == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing default value at %s",
														__FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		iLen = strlen(value);
		cTmpPtr = (uchar *) malloc(iLen + 1);
		if(cTmpPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(cTmpPtr, 0x00, iLen + 1);
		memcpy(cTmpPtr, value, iLen);

		stripSpaces((char *)cTmpPtr,STRIP_LEADING_SPACES|STRIP_TRAILING_SPACES);

		switch(iCnt)
		{
		case VISA_DEBIT:
		case CONTACTLESS_MODE:
		case PDOL_SUPPORT:
		case VALUE_LINK_CARD_SUPPORT:
		case IS_NFC_SUPPROT:	

			/* store Other default values */
			saveD25DlftValues(iCnt, (char *)cTmpPtr, &(pstKeyLoadLstInfo->stOtherRecInfo));
			break;	
			
		default:
			debug_sprintf(szDbgMsg, "%s: key[%d], Value[%s] is unknown default value, Not saving", 
																	__FUNCTION__, iCnt, (char *)cTmpPtr);
			APP_TRACE(szDbgMsg);
			break;
		}			
		cTmpPtr = NULL;
		value = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name:
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int saveDfltValue(int iKey, char *pValue, EMVKEYLOAD_LIST_PTYPE pstKeyLoadLst)
{
	int		rv					= SUCCESS;
	int 	iSuppAidIndex		= 0;

#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(iKey)
	{

	case RECNO:
		/* store RecNo in the MVT, EST, EMVT and EEST tables*/

		//Store in EST and EEST tables
		iSuppAidIndex = pstKeyLoadLst->iNumSuppAid;
		saveEstTableDlftValues(iKey, pValue, &(pstKeyLoadLst->stEstRecInfo));
		saveEestTableDlftValues(iKey, pValue, &(pstKeyLoadLst->stEestRecInfo[iSuppAidIndex]));

		//Store in MVT and EMVT tables
		saveMvtTableDlftValues(iKey, pValue, &(pstKeyLoadLst->stMvtRecInfo));
		saveEmvtTableDlftValues(iKey, pValue, &(pstKeyLoadLst->stEmvtRecInfo));

		break;

	case RID:
	case SCHM_LABEL:
	case PARTIAL_NAME_FLG:
	case TERM_AVN:
	case SCND_TERM_AVN:
	case RECMD_AID_NAME:
	case CSN_LIST:
	case CSN_LIST_FILE:
	case EMV_TABLE_RECORD:
#if 0	//TODO: Remove it later
		/* store EST table default values */
		memset(pstKeyLoadLst->stEstRecInfo.szRID, 0x00,
							sizeof(pstKeyLoadLst->stEstRecInfo.szRID));
		sprintf(pstKeyLoadLst->stEstRecInfo.szRID, "%s", pstKeyLoadLst->szAppID);
#endif
		saveEstTableDlftValues(iKey, pValue, &(pstKeyLoadLst->stEstRecInfo));		
		break;	
		
	case SUPPORTED:
		debug_sprintf(szDbgMsg, "%s: Storing Supported AID[%s] ", __FUNCTION__, pValue);
		APP_TRACE(szDbgMsg);	
		
		iSuppAidIndex = pstKeyLoadLst->iNumSuppAid;
		/* save SUPPORTED AID in both EST and EEST Table */
		sprintf(pstKeyLoadLst->stEstRecInfo.szSupportedAID[iSuppAidIndex], "%s", pValue);
		sprintf(pstKeyLoadLst->stEestRecInfo[iSuppAidIndex].szSupportedAID, "%s", pValue);
		
		break;		

	case SCHMREF:
	case ISSUER_REF:
	case TRM_DATA_PRESENT:
	case TARGET_RS_PERCENT:
	case FLR_LIMIT:
	case RSTHRESHOLD:
	case MAX_TARGET_RS_PERCENT:
	case MERCH_FORCE_ONLINE_FLG:
	case BLACK_LIST_CARD_SUPPORT_FLG:
	case FALLBACK_ALLOWED_FLG:
	case TAC_DEFAULT:
	case TAC_DENIAL:
	case TAC_ONLINE:
	case DEFAULT_TDOL:
	case DEFAULT_DDOL:
	case NEXT_RECORD:
	case AUTO_SELECT_APPL:
	case EMV_COUNTER:
	case COUNTRY_CODE:
	case CURRENCY_CODE:
	case TERM_CAPACITY:
	case ADD_CAPACITY:
	case TERM_TYPE:
	case MERCH_CAT_CODE:
	case TERMINAL_CAT_CODE:
	case TERM_ID:
	case MERCH_ID:
	case ACQUIRER_ID:
	case CAPK_INDEX:
	case PIN_BYPASS_FLG:
	case EMV_PIN_TIMEOUT:
	case PIN_FORMAT:
	case PIN_SCRIPT_NUM:
	case PIN_MACRO_NUMBER:
	case DEVRI_KEY_FLG:
	case DEVRI_MACRO_NUM:
	case CARD_STAT_DISPLAY_FLG:
	case TERM_CUR_EXP:
	case ISS_ACQ_FLG:
	case NO_DISPLAY_SUPPORT_FLG:
	case MODIFY_CAND_LIST_FLG:
	case STRING_RFU1:
	case STRING_RFU2:
	case STRING_RFU3:
	case SHORT_RFU1:
	case SHORT_RFU2:
	case SHORT_RFU3:	

		/* store MVT table default values */
		saveMvtTableDlftValues(iKey, pValue, &(pstKeyLoadLst->stMvtRecInfo));		
		break;	
	case EMV_RECORD:
	case MAX_AID_LEN:
	case APP_FLOW:
	case PARTIAL_NAME:
	case ACCOUNT_SEL_FLG:
	case CTLS_ENABLE_FLG:
	case TRANS_SCHEME:

		iSuppAidIndex = pstKeyLoadLst->iNumSuppAid;
		/* store EEST table default values */
		saveEestTableDlftValues(iKey, pValue, &(pstKeyLoadLst->stEestRecInfo[iSuppAidIndex]));
		break;

	case GROUP_NAME:
	case CTLS_FLR_LIMIT:
	case CTLS_CVM_LIMIT:
	case CTLS_TRN_LIMIT:
	case CTLS_TAC_DEFAULT:
	case CTLS_TAC_DENIAL:
	case CTLS_TAC_ONLINE:
	case VISATTQ:
	case TERMINAL_CAPABILITIES:
	case ADDITIONAL_CAPABILITIES:
	case CTLS_COUNTRY_CODE:
	case CTLS_CURRECY_CODE:
	case CTLS_MVT_INDEX:

		/* store EMVT table default values */
		saveEmvtTableDlftValues(iKey, pValue, &(pstKeyLoadLst->stEmvtRecInfo));
		break;
		
	default:
		debug_sprintf(szDbgMsg, "%s: key[%d], Value[%s] is unknown default value, Not saving", 
																__FUNCTION__, iKey, pValue);
		APP_TRACE(szDbgMsg);
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveEstTableDlftValues
 *
 * Description	: 				  
 *
 * Input Params	: None
 *
 * Output Params: EST structure
 * ============================================================================
 */
static int saveEstTableDlftValues(int iKey, char *pValue, EMV_EST_REC_PTYPE pstEstRecInfo)
{
	int 					rv 				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#if 0 //TODO: Remove it Later.
	/* Create CSN list file using RID */
	if(strlen(pstEstRecInfo->szRID) > 0 && strlen(pstEstRecInfo->szCSNList) <= 0)
	{
		memset(pstEstRecInfo->szCSNList, 0x00, sizeof(pstEstRecInfo->szCSNList));
		sprintf(pstEstRecInfo->szCSNList, "%s.CSN", pstEstRecInfo->szRID);		
	}
#endif
	if( iKey == RECNO)
	{
		pstEstRecInfo->inRecNo = atoi(pValue);
	}
	else if( iKey == RID)
	{
		sprintf(pstEstRecInfo->szRID, "%s", pValue);
	}
	else if( iKey == CSN_LIST)
	{
		sprintf(pstEstRecInfo->szCSNList, "%s", pValue);
	}
	else if( iKey == SCHM_LABEL)
	{
		sprintf(pstEstRecInfo->szSchemeLabel, "%s", pValue);
	}
	else if( iKey == PARTIAL_NAME_FLG)
	{
		pstEstRecInfo->inPartialNameAllowedFlag = atol(pValue);
	}
	else if( iKey == TERM_AVN)
	{
		sprintf(pstEstRecInfo->szTermAVN, "%s", pValue);
	}
	else if( iKey == SCND_TERM_AVN)
	{
		sprintf(pstEstRecInfo->szSecondTermAVN, "%s", pValue);
	}
	#if 0
	else if( iKey == SUPPORTED)
	{
		sprintf(pstEstRecInfo->szSupportedAID, "%s", pValue);
	}
	#endif
	else if( iKey == CSN_LIST_FILE)
	{
		sprintf(pstEstRecInfo->szCSNListFile, "%s", pValue);	
	}
	else if( iKey == RECMD_AID_NAME)
	{
		sprintf(pstEstRecInfo->szRcmdAppNameAID, "%s", pValue);	
	}	
	else if( iKey == EMV_TABLE_RECORD)
	{
		pstEstRecInfo->inEMVTableRecord = atol(pValue);	
	}
	else if( iKey == LNNUMOFTRANSACTION)
	{
		pstEstRecInfo->lnNbrOfTransactions = atol(pValue);
	}	
	else;
	
	//pstEstRecInfo->lnNbrOfTransactions	= 0;	//default value
	//pstEstRecInfo->inEMVTableRecord		= 0;	//default value

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	
	return rv;	
}

/*
 * ============================================================================
 * Function Name: saveEestTableDlftValues
 *
 * Description	: 				  
 *
 * Input Params	: None
 *
 * Output Params: EST structure
 * ============================================================================
 */
static int saveEestTableDlftValues(int iKey, char *pValue, EMV_EEST_REC_PTYPE pstEestRecInfo)
{
	int 					rv 				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( iKey == RECNO)
	{
		pstEestRecInfo->inRecNo = atoi(pValue);
	}
	else if( iKey == SUPPORTED)
	{
		sprintf(pstEestRecInfo->szSupportedAID, "%s", pValue);
	}
	else if( iKey == EMV_RECORD)
	{
		pstEestRecInfo->inEMVRec = atoi(pValue);
	}	
	else if( iKey == MAX_AID_LEN)
	{
		pstEestRecInfo->inMaxAIDLength = atoi(pValue);
	}
	else if( iKey == APP_FLOW)
	{
		pstEestRecInfo->inApplicationFlow = atoi(pValue);
	}
	else if( iKey == PARTIAL_NAME)
	{
		pstEestRecInfo->inPartialName = atoi(pValue);
	}
	else if( iKey == ACCOUNT_SEL_FLG)
	{
		pstEestRecInfo->inAcctSelFlag = atoi(pValue);
	}
	else if( iKey == CTLS_ENABLE_FLG)
	{
		pstEestRecInfo->inCTLSEnableFlag = atoi(pValue);
	}
	else if( iKey == TRANS_SCHEME)
	{
		pstEestRecInfo->iTransactionScheme = atoi(pValue);	
	}
	else;
	
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	
	return rv;	
}

/*
 * ============================================================================
 * Function Name: saveIcctTableDlftValues
 *
 * Description	: 				  
 *
 * Input Params	: None
 *
 * Output Params: EST structure
 * ============================================================================
 */
static int saveIcctTableDlftValues(int iKey, char *pValue, EMV_ICCT_REC_PTYPE pstIcctRecInfo)
{
	int 					rv 				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( iKey == REC_NUMBER )
	{
		pstIcctRecInfo->inRecNo = atoi(pValue);
	}	
	else if( iKey == CTLS_MERCHANT_TYPE)
	{
		sprintf(pstIcctRecInfo->szCTLSMerchantType, "%s", pValue);
	}
	else if( iKey == CTLS_TERM_TRAN_INFO)
	{
		sprintf(pstIcctRecInfo->szCTLSTermTranInfo, "%s", pValue);
	}
	else if( iKey == CTLS_TERM_TRAN_TYPE)
	{
		sprintf(pstIcctRecInfo->szCTLSTermTranType, "%s", pValue);
	}
	else if( iKey == CTLS_REQ_RECIEPT_LIMIT)
	{
		sprintf(pstIcctRecInfo->szCTLSReqReceiptLimit, "%s", pValue);
	}
	else if( iKey == CTLS_OPTION_STATUS)
	{
		sprintf(pstIcctRecInfo->szCTLSOptionStatus, "%s", pValue);
	}
	else if( iKey == CTLS_READER_FLR_LIMIT)
	{
		sprintf(pstIcctRecInfo->szCTLSReaderFloorLimit, "%s", pValue);	
	}
	else;
	
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	
	return rv;	
}

/*
 * ============================================================================
 * Function Name: saveMvtTableDlftValues
 *
 * Description	: 				  
 *
 * Input Params	: None
 *
 * Output Params: EST structure
 * ============================================================================
 */
static int saveMvtTableDlftValues(int iKey, char *pValue, EMV_MVT_REC_PTYPE pstMvtRecInfo)
{
	int 					rv 				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( iKey == RECNO)
	{
		pstMvtRecInfo->inRecNo = atoi(pValue);
	}
	else if( iKey == ISSUER_REF)
	{
		pstMvtRecInfo->inSchemeReference = atoi(pValue);
	}
	else if( iKey == ISSUER_REF)
	{
		pstMvtRecInfo->inIssuerReference = atoi(pValue);
	}
	else if( iKey == TRM_DATA_PRESENT)
	{
		pstMvtRecInfo->inTRMDataPresent = atoi(pValue);
	}
	else if( iKey == TARGET_RS_PERCENT)
	{
		pstMvtRecInfo->inTargetRSPercent = atoi(pValue);
	}
	else if( iKey == FLR_LIMIT)
	{
		pstMvtRecInfo->lnFloorLimit = atol(pValue);
	}
	else if( iKey == RSTHRESHOLD)
	{
		pstMvtRecInfo->lnRSThreshold = atol(pValue);
	}
	else if( iKey == MAX_TARGET_RS_PERCENT)
	{
		pstMvtRecInfo->inMaxTargetRSPercent = atoi(pValue);
	}
	else if( iKey == MERCH_FORCE_ONLINE_FLG)
	{
		pstMvtRecInfo->inMerchantForcedOnlineFlag = atoi(pValue);
	}
	else if( iKey == BLACK_LIST_CARD_SUPPORT_FLG)
	{
		pstMvtRecInfo->inBlackListedCardSupportFlag = atoi(pValue);
	}
	else if( iKey == FALLBACK_ALLOWED_FLG)
	{
		pstMvtRecInfo->inFallbackAllowed = atoi(pValue);
	}
	else if( iKey == TAC_DEFAULT)
	{
		sprintf(pstMvtRecInfo->szTACDefault, "%s", pValue);
	}
	else if( iKey == TAC_DENIAL)
	{
		sprintf(pstMvtRecInfo->szTACDenial, "%s", pValue);
	}
	else if( iKey == TAC_ONLINE)
	{
		sprintf(pstMvtRecInfo->szTACOnline, "%s", pValue);
	}	
	else if( iKey == DEFAULT_TDOL)
	{
		sprintf(pstMvtRecInfo->szDefaultTDOL, "%s", pValue);
	}
	else if( iKey == DEFAULT_DDOL)
	{
		sprintf(pstMvtRecInfo->szDefaultDDOL, "%s", pValue);
	}
	else if( iKey == NEXT_RECORD)
	{
		pstMvtRecInfo->inNextRecord = atoi(pValue);
	}
	else if( iKey == AUTO_SELECT_APPL)
	{
		pstMvtRecInfo->inAutoSelectAppln = atoi(pValue);
	}
	else if( iKey == EMV_COUNTER)
	{
		pstMvtRecInfo->ulEMVCounter = atol(pValue);
	}
	else if( iKey == COUNTRY_CODE)
	{
		sprintf(pstMvtRecInfo->szTermCountryCode, "%s", pValue);
	}
	else if( iKey == CURRENCY_CODE)
	{
		sprintf(pstMvtRecInfo->szTermCurrencyCode, "%s", pValue);
	}
	else if( iKey == TERM_CAPACITY)
	{
		sprintf(pstMvtRecInfo->szTermCapabilities, "%s", pValue);
	}
	else if( iKey == ADD_CAPACITY)
	{
		sprintf(pstMvtRecInfo->szTermAddCapabilities, "%s", pValue);	
	}
	else if( iKey == TERM_TYPE)
	{
		sprintf(pstMvtRecInfo->szTermType, "%s", pValue);
	}
	else if( iKey == MERCH_CAT_CODE)
	{
		sprintf(pstMvtRecInfo->szMerchantCategoryCode, "%s", pValue);
	}
	else if( iKey == TERMINAL_CAT_CODE)
	{
		sprintf(pstMvtRecInfo->szTerminalCategoryCode, "%s", pValue);	
	}
	else if( iKey == TERM_ID)
	{
		sprintf(pstMvtRecInfo->szTerminalID, "%s", pValue);
	}
	else if( iKey == MERCH_ID)
	{
		sprintf(pstMvtRecInfo->szMerchantID, "%s", pValue);
	}
	else if( iKey == ACQUIRER_ID)
	{
		sprintf(pstMvtRecInfo->szAcquirerID, "%s", pValue);
	}
	else if( iKey == CAPK_INDEX)
	{
		sprintf(pstMvtRecInfo->szTermCAPKIndex, "%s", pValue);
	}
	else if( iKey == PIN_BYPASS_FLG)
	{
		sprintf(pstMvtRecInfo->szPINBypassFlag, "%s", pValue);	
	}
	else if( iKey == EMV_PIN_TIMEOUT)
	{
		sprintf(pstMvtRecInfo->szPINTimeout, "%s", pValue);
	}
	else if( iKey == PIN_FORMAT)
	{
		sprintf(pstMvtRecInfo->szPINFormat, "%s", pValue);
	}
	else if( iKey == PIN_SCRIPT_NUM)
	{
		sprintf(pstMvtRecInfo->szPINScriptNum, "%s", pValue);
	}
	else if( iKey == PIN_MACRO_NUMBER)
	{
		sprintf(pstMvtRecInfo->szPINMacroNum, "%s", pValue);
	}
	else if( iKey == DEVRI_KEY_FLG)
	{
		sprintf(pstMvtRecInfo->szPINDerivKeyFlag, "%s", pValue);	
	}
	else if( iKey == DEVRI_MACRO_NUM)
	{
		sprintf(pstMvtRecInfo->szPINDerivMacroNum, "%s", pValue);	
	}	
	else if( iKey == CARD_STAT_DISPLAY_FLG)
	{
		sprintf(pstMvtRecInfo->szCardStatDispFlag, "%s", pValue);
	}
	else if( iKey == TERM_CUR_EXP)
	{
		pstMvtRecInfo->inTermCurExp = atoi(pValue);
	}
	else if( iKey == ISS_ACQ_FLG)
	{
		pstMvtRecInfo->inIssAcqflag = atoi(pValue);
	}
	else if( iKey == NO_DISPLAY_SUPPORT_FLG)
	{
		pstMvtRecInfo->inNoDisplaySupportFlag = atoi(pValue);
	}
	else if( iKey == MODIFY_CAND_LIST_FLG)
	{
		pstMvtRecInfo->inModifyCandListFlag = atoi(pValue);
	}
	else if( iKey == STRING_RFU1)
	{
		sprintf(pstMvtRecInfo->szRFU1, "%s", pValue);
	}
	else if( iKey == STRING_RFU2)
	{
		sprintf(pstMvtRecInfo->szRFU2, "%s", pValue);
	}
	else if( iKey == STRING_RFU3)
	{
		sprintf(pstMvtRecInfo->szRFU3, "%s", pValue);	
	}
	else if( iKey == SHORT_RFU1)
	{
		pstMvtRecInfo->shRFU1 = atoi(pValue);
	}
	else if( iKey == SHORT_RFU2)
	{
		pstMvtRecInfo->shRFU2 = atoi(pValue);
	}
	else if( iKey == SHORT_RFU3)
	{
		pstMvtRecInfo->shRFU3 = atoi(pValue);	
	}
	else;	
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	
	return rv;	
}

/*
 * ============================================================================
 * Function Name: saveEmvtTableDlftValues
 *
 * Description	: 				  
 *
 * Input Params	: None
 *
 * Output Params: EST structure
 * ============================================================================
 */
static int saveEmvtTableDlftValues(int iKey, char *pValue, EMV_EMVT_REC_PTYPE pstEmvtRecInfo)
{
	int 					rv 				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( iKey == RECNO)
	{
		pstEmvtRecInfo->inRecNo = atoi(pValue);
	}
	else if( iKey == GROUP_NAME)
	{
		sprintf(pstEmvtRecInfo->szCTLSGroupName, "%s", pValue);
	}
	else if( iKey == CTLS_FLR_LIMIT)
	{
		sprintf(pstEmvtRecInfo->szCTLSFlrLimit, "%s", pValue);
	}
	else if( iKey == CTLS_CVM_LIMIT)
	{
		sprintf(pstEmvtRecInfo->szCTLSCVMLimit, "%s", pValue);
	}
	else if( iKey == CTLS_TRN_LIMIT)
	{
		sprintf(pstEmvtRecInfo->szCTLSTranLimit, "%s", pValue);
	}
	else if( iKey == CTLS_TAC_DEFAULT)
	{
		sprintf(pstEmvtRecInfo->szCTLSTACDefault, "%s", pValue);
	}
	else if( iKey == CTLS_TAC_DENIAL)
	{
		sprintf(pstEmvtRecInfo->szCTLSTACDenial, "%s", pValue);	
	}
	else if( iKey == CTLS_TAC_ONLINE)
	{
		sprintf(pstEmvtRecInfo->szCTLSTACOnline, "%s", pValue);
	}
	else if( iKey == VISATTQ)
	{
		sprintf(pstEmvtRecInfo->szCTLSVISATTQ, "%s", pValue);
	}
	else if( iKey == TERMINAL_CAPABILITIES)
	{
		sprintf(pstEmvtRecInfo->szCTLSTermCapabilities, "%s", pValue);
	}
	else if( iKey == ADDITIONAL_CAPABILITIES)
	{
		sprintf(pstEmvtRecInfo->szCTLSTermAddCapabilities, "%s", pValue);
	}
	else if( iKey == CTLS_COUNTRY_CODE)
	{
		sprintf(pstEmvtRecInfo->szCTLSCountryCode, "%s", pValue);	
	}	
	else if( iKey == CTLS_CURRECY_CODE)
	{
		sprintf(pstEmvtRecInfo->szCTLSCurrencyCode, "%s", pValue);
	}
	else if( iKey == CTLS_MVT_INDEX)
	{
		pstEmvtRecInfo->inCTLSMVTIndex = atoi(pValue);
	}
	else;
	
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	
	return rv;	
}


/*
 * ============================================================================
 * Function Name: saveD18DlftValues
 *
 * Description	: 				  
 *
 * Input Params	: None
 *
 * Output Params: EST structure
 * ============================================================================
 */
static int saveD18DlftValues(int iKey, char *pValue, EMV_OTHER_REC_PTYPE pstOtherRecInfo)
{
	int 					rv 				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( iKey == BELLOW_FLR_LIMIT_TERM_CAPABILITIES)
	{
		sprintf(pstOtherRecInfo->szBlwFlrLimitTermCap, "%s", pValue);
	}
	else if( iKey == ABOVE_FLR_LIMIT_TERM_CAPABILITIES)
	{
		sprintf(pstOtherRecInfo->szAbvFlrLimitTermCap, "%s", pValue);
	}
	else if( iKey == CTLSCVM_FLR_LIMIT)
	{
		sprintf(pstOtherRecInfo->szCTLSCVMLimit, "%s", pValue);
	}	
	else;
	
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	
	return rv;	
}

/*
 * ============================================================================
 * Function Name: saveD25DlftValues
 *
 * Description	: 				  
 *
 * Input Params	: None
 *
 * Output Params: EST structure
 * ============================================================================
 */
static int saveD25DlftValues(int iKey, char *pValue, EMV_OTHER_REC_PTYPE pstOtherRecInfo)
{
	int 					rv 				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( iKey == VISA_DEBIT)
	{
		pstOtherRecInfo->inVisaDebit = atoi(pValue);
	}
	else if( iKey == CONTACTLESS_MODE)
	{
		pstOtherRecInfo->inEMVContactless = atoi(pValue);
	}
	else if( iKey == PDOL_SUPPORT)
	{
		pstOtherRecInfo->inPDOLSupFlg = atoi(pValue);
	}
	else if( iKey == VALUE_LINK_CARD_SUPPORT)
	{
		pstOtherRecInfo->inValueLinkCrdSup = atoi(pValue);
	}
	else if( iKey == IS_NFC_SUPPROT)
	{
		pstOtherRecInfo->inISISNFCSup = atoi(pValue);
	}	
	else;
	
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	
	return rv;	
}

/*
 * ============================================================================
 * Function Name: findNodeForAppId
 *
 * Description	: 				  
 *
 * Input Params	: 
 *
 * Output Params: pointer to passed APPID matching node of key load list
 * ============================================================================
 */
static EMVKEYLOAD_LIST_PTYPE findNodeForAppId(char *pszAppId, EMVKEYLOAD_LIST_PTYPE pstHeadKeyLoadLst)
{
	PAAS_BOOL				bFound			= PAAS_FALSE;	
    EMVKEYLOAD_LIST_PTYPE 	tmpKeyLoadLst 	= pstHeadKeyLoadLst;
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
    while(tmpKeyLoadLst != NULL)
    {
		debug_sprintf(szDbgMsg, "%s:Comparing APPID[%s] with Host APPID[%s]", __FUNCTION__, 
											tmpKeyLoadLst->szAppID, pszAppId);
		APP_TRACE(szDbgMsg);	
        if(!strcasecmp(tmpKeyLoadLst->szAppID, pszAppId))
        {
			bFound = PAAS_TRUE;
            break;
        }
        else
        {
            tmpKeyLoadLst = tmpKeyLoadLst->nextEmvNode;
        }
    }	

    if(bFound == PAAS_TRUE)
    {
		debug_sprintf(szDbgMsg, "%s: APPID matching node exist!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);	
		return tmpKeyLoadLst;
    }
    else
    {
		debug_sprintf(szDbgMsg, "%s: APPID matching node do not exist!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);	
        return NULL;
    }
}
#endif
/*
 * ============================================================================
 * Function Name: loadDispMsgsAndPrompts
 *
 * Description	: This function is responsible for loading the display prompts
 * 					needed for the displays on the screens
 *
 * Input Params	: buffer for any error message
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int loadDispMsgsAndPrompts(char *pszErrMsg)
{
	int		rv				= SUCCESS;
	char 	szCfgLang[25]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Get default language type to local variable*/
	sprintf(szCfgLang, "%s", getDefaultLang());

	/* Check if the prompts file exits and has some data */
	rv = doesFileExist(PROMPTS_FILE_NAME);
	if(rv == SUCCESS)
	{
		/* Read and store the prompts */
		rv = storeDispMsgsAndPromptsLst_1(PROMPTS_FILE_NAME, szCfgLang,
											&(uiSettings.pstDispMsgsInfo), pszErrMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: File [%s] does not exist", __FUNCTION__,
															PROMPTS_FILE_NAME);
		APP_TRACE(szDbgMsg);
		sprintf(pszErrMsg, "%s: %s",pszErrMsg, PROMPTS_FILE_NAME);
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fetchBtnLabel
 *
 * Description	: This function would get the button labels demanded by the
 * 					caller, in the current language of the transaction.
 *
 * Input Params	: btnLblPtr -> placeholder for required button labels
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int fetchBtnLabel(BTNLBL_PTYPE btnLblPtr, int btnLblNo)
{
	int						rv					= SUCCESS;
	char					tempMsg[256]		= "";	
	PROMPT_PTR_TYPE			pstPromptList		= NULL;
	BTNLBL_PTYPE			locBtnLblPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	pstPromptList = getPromptsNodeForCurLang();
	
	if(pstPromptList != NULL)
	{
		if(btnLblPtr != NULL)
		{
				if( (btnLblNo >= 0) && (btnLblNo < MAX_BTNLBL_IDX) )
				{
					memset(btnLblPtr, 0x00, sizeof(BTNLBL_STYPE));

					/* Fetch the button label pointer */
					if((pstPromptList->btnLbls[btnLblNo]) != NULL)
					{
						locBtnLblPtr = pstPromptList->btnLbls[btnLblNo];
						if(locBtnLblPtr != NULL)
						{
							memset(tempMsg, 0x00, sizeof(tempMsg));					
							/* If the screen title is not empty return it back to the caller */
							memcpy(tempMsg, (char *)locBtnLblPtr->szLblOne, strlen((char *)locBtnLblPtr->szLblOne));
							
							/* Convert string to UTF string */
							convStrToUTFEscStr(tempMsg, (char *)btnLblPtr->szLblOne);

							memset(tempMsg, 0x00, sizeof(tempMsg));					
							/* If the screen title is not empty return it back to the caller */
							memcpy(tempMsg, (char *)locBtnLblPtr->szLblTwo, strlen((char *)locBtnLblPtr->szLblTwo));
							
							/* Convert string to UTF string */
							convStrToUTFEscStr(tempMsg, (char *)btnLblPtr->szLblTwo);
						}
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Button labels are not present", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = FAILURE;
					}					
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Input param button Label Num is invalid", 
																			__FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
				}				
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Input param button Label is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Required Prompts msg node is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}	
	return rv;
}

/*
 * ============================================================================
 * Function Name: fetchScrTitle
 *
 * Description	: This function would get the screen title demanded by the
 * 					caller, in the current language of the transaction.
 *
 * Input Params	: scrTitlePtr -> placeholder for required screen title.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int fetchScrTitle(char *scrTitlePtr, int scrTitleNo)
{	
	int		rv								= SUCCESS;
	char	tempMsg[1024]					= "";
	PROMPT_PTR_TYPE			pstPromptList	= NULL;	
	uchar *	locScrTitlePtr					= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	pstPromptList = getPromptsNodeForCurLang();
	
	if(pstPromptList != NULL)
	{
		if(scrTitlePtr != NULL)
		{

			if( (scrTitleNo >= 0) && (scrTitleNo < MAX_TITLE_IDX) )
			{

				memset(scrTitlePtr, 0x00, sizeof(char)*MAX_DISP_TITLE_SIZE);
				/* Get the current language prompt list pointer */
				if(( pstPromptList->scrTitles[scrTitleNo] != NULL))
				{
					locScrTitlePtr = pstPromptList->scrTitles[scrTitleNo];
					if(locScrTitlePtr != NULL)
					{
						memset(tempMsg, 0x00, sizeof(tempMsg));					
						/* If the screen title is not empty return it back to the caller */
						memcpy(tempMsg, locScrTitlePtr, strlen((char *)locScrTitlePtr));
						
						/* Convert string to UTF string */
						convStrToUTFEscStr(tempMsg, scrTitlePtr);
						
						debug_sprintf(szDbgMsg, "%s: Got Screen Title=%s", __FUNCTION__, scrTitlePtr);
						APP_TRACE(szDbgMsg);
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Screen titles are not present", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
				}

			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Screen title is not present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
			}
		}
		else
		{
			APP_TRACE("fetchScrTitle: Screen Title Pointer is NULL");
			rv = FAILURE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Required Prompts msg node is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}		
	return rv;
}

/*
 * ============================================================================
 * Function Name: getDisplayMsg
 *
 * Description	: This function would get the display Message demanded by the
 * 					caller, in the current language of the transaction.
 *
 * Input Params	: msgPtr -> placeholder for required screen message
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getDisplayMsg(char *msgPtr, int msgNo)
{
	int		rv								= SUCCESS;
	char	tempMsg[1024]					= "";	
	uchar *	locMsgPtr						= NULL;
	PROMPT_PTR_TYPE			pstPromptList	= NULL;		
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
	
	pstPromptList = getPromptsNodeForCurLang();
	
	if(pstPromptList != NULL)
	{
		if(msgPtr != NULL)
		{

			if( (msgNo >= 0) && (msgNo < MAX_DISP_MSGS) )
			{

				memset(msgPtr, 0x00, sizeof(char)*MAX_DISP_MSG_SIZE);
				/* Get the current language prompt list pointer */
				if((pstPromptList->dispMsgs[msgNo] != NULL))
				{
					locMsgPtr = pstPromptList->dispMsgs[msgNo];
					if(locMsgPtr != NULL)
					{

						memset(tempMsg, 0x00, sizeof(tempMsg));					
						/* If the prompt is not empty return it back to the caller */
						memcpy(tempMsg, (char *)locMsgPtr, strlen((char *)locMsgPtr));
						
						/* Convert string to UTF string */
						convStrToUTFEscStr(tempMsg, msgPtr);
						
						debug_sprintf(szDbgMsg, "%s: Display msg=%s", __FUNCTION__, msgPtr);
						APP_TRACE(szDbgMsg);
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Display message are not present", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
				}

			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Display message is not present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
			}
		}
		else
		{
			APP_TRACE("getDisplayMsg: Msg Pointer is NULL");
			rv = FAILURE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Required Prompts msg node is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	
	return rv;
}

/*
 * ============================================================================
 * Function Name: storeDispMsgsAndPromptsLst_1
 *
 * Description	: This function is responsible for parsing the input ini file
 * 					and storing the prompts in the memory language wise.
 *
 * Input Params	: fileName	-> name of the file containing prompts
 * 				  pstDispDtls	-> list where the parameters would be stored
 * 				  pszErrMsg		-> Buffer for any error message
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeDispMsgsAndPromptsLst_1(char * fileName,  char * szCfgLang, 
												DISPMSG_INFO_PTYPE  *dpstDispMsgsInfo, char *pszErrMsg)
{
	int								rv						= SUCCESS;
	int								iCnt					= 0;
	int								iCurLangId				= -1;
	int								iLangCnt				= 0;
	int 							iNode					= 0;
	int								iAppLogEnabled			= isAppLogEnabled();
	char							szAppLogData[300]		= "";
	char							szLangVal[30]			= "";
	PAAS_BOOL						bGotDefltLang			= PAAS_FALSE;	
	PAAS_BOOL						bEmvEnabled				= PAAS_FALSE;
	char *							pszLangName				= NULL;
	dictionary *					dict					= NULL;
	DISPMSG_INFO_PTYPE  			pstDispMsgsInfo 		= NULL;
	PROMPT_PTR_TYPE 				pstPromptList			= NULL;
	PROMPT_STRUCT_TYPE 				stPromptList;			
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	sprintf(szLangVal, "%s", szCfgLang); //Copying to local buffer so that we can null set this buffer before modifying it

	//Get EMV status.
	bEmvEnabled = isEmvEnabledInDevice();
	
	/* Load the ini file in the parser library */
	dict = iniparser_load(fileName);
	if(dict == NULL)
	{
		/* Unable to load the ini file */
		debug_sprintf(szDbgMsg, "%s: Unable to parse ini file [%s], errno [%d]",
												__FUNCTION__, fileName, errno);
		APP_TRACE(szDbgMsg);
		sprintf(pszErrMsg, "%s: %s",pszErrMsg, PROMPTS_FILE_NAME);
		rv = FAILURE;
		return rv;
	}

	/* Get the number of languages stored in the ini file */
	iLangCnt = iniparser_getnsec(dict);
	if(iLangCnt <= 0)
	{
		debug_sprintf(szDbgMsg, "%s: No sections present in ini file [%s]", __FUNCTION__, fileName);
		APP_TRACE(szDbgMsg);
		sprintf(pszErrMsg, "%s: %s",pszErrMsg, PROMPTS_FILE_NAME);
		rv = FAILURE;
		return rv;
	}

	debug_sprintf(szDbgMsg, "%s: Number of Language(s) present in Display Prompts File = [%d]",	__FUNCTION__, iLangCnt);
	APP_TRACE(szDbgMsg);
	
	if( SUCCESS != getLangIdFromDispPrompFile(dict, szLangVal, &iCurLangId) )
	{
		debug_sprintf(szDbgMsg, "%s: Default Language[%s] Not Present in Display Prompts File, Setting ENGLISH as default",	__FUNCTION__, szLangVal);
		APP_TRACE(szDbgMsg);
		memset(szLangVal, 0x00, sizeof(szLangVal));
		sprintf(szLangVal, "%s", "ENGLISH");
		putEnvFile(SECTION_DEVICE, DFLT_LANG, szLangVal);
	}
	iCurLangId = -1;

	/* Allocate memory */
	if(pstDispMsgsInfo == NULL)
	{
		pstDispMsgsInfo = (DISPMSG_INFO_PTYPE) malloc(sizeof(DISPMSG_INFO_STYPE));
		if(pstDispMsgsInfo == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
	}
	memset(pstDispMsgsInfo, 0x00, sizeof(DISPMSG_INFO_STYPE));	
	
	while(1)
	{
		/* Get the language definitions */
		for(iCnt = 0; iCnt < iLangCnt; iCnt++)
		{
			pszLangName = iniparser_getsecname(dict, iCnt);

			debug_sprintf(szDbgMsg, "%s: iCnt[%d], Language Name[%s]", __FUNCTION__,
					iCnt, pszLangName);
			APP_TRACE(szDbgMsg);

			memset(&stPromptList, 0x00, sizeof(PROMPT_STRUCT_TYPE));

			/* Convert string to upper case */
			strToUpper(pszLangName);

			/* App Launcher Section, Skip it */
			if( !strcasecmp("DEBUG.LABEL", pszLangName) )
			{
				debug_sprintf(szDbgMsg, "%s: App Launcher Section, Skipping it ", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				continue;
			}

			if( bEmvEnabled == PAAS_TRUE )
			{
				/* Get the language id for the given langauge name */
				if( (strcasecmp(szLangVal, pszLangName) == SUCCESS) && getLangIdFromDispPrompFile(dict, pszLangName, &iCurLangId) == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Current Lang is  Default lang, and Present in Display Prompts File; Need to load", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else if( SUCCESS != getLangIdFromDispPrompFileifinEMVSupLang(dict, pszLangName, &iCurLangId) )
				{
					debug_sprintf(szDbgMsg, "%s: Current Lang is not Not Supported; checking next Lang to Load", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					continue;
				}
			}
			else
			{
				if(strcasecmp(szLangVal, pszLangName) != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Current Lang is not Default lang and EMV is disabled; Need to load only Default Lang, checking next Lang", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					continue;
				}
				/* Get the language id for the given langauge name */
				if( SUCCESS != getLangIdFromDispPrompFile(dict, pszLangName, &iCurLangId) )
				{
					debug_sprintf(szDbgMsg, "%s: Couldn't find the id for given language", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			/* Get the amount format stored for the language */
			getAmtFormat(dict, pszLangName, &stPromptList);

			/* Get the titles for the screen */
			rv = getScrnTitles(dict, pszLangName, stPromptList.scrTitles, pszErrMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get screen titles",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error While Getting the Screen Titles From displayPrompts.ini File");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
				}
				break;
			}

			/* Get the status messages for the application */
			rv = getDispMsgs(dict, pszLangName, stPromptList.dispMsgs, pszErrMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get display messages"
						, __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error While Getting the Display Messages From displayPrompts.ini File");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
				}
				break;
			}

			/* Get the button labels for the application screens */
			rv = getBtnLbls(dict, pszLangName, stPromptList.btnLbls, pszErrMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get button labels",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error While Getting the Button Labels From displayPrompts.ini File");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
				}
				break;
			}
			//store the Language Id.
			stPromptList.iLangId = iCurLangId;

			pstPromptList = (PROMPT_PTR_TYPE) malloc(sizeof(PROMPT_STRUCT_TYPE));
			if(pstPromptList == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPromptList, 0x00, sizeof(PROMPT_STRUCT_TYPE));
			memcpy(pstPromptList, &stPromptList, sizeof(PROMPT_STRUCT_TYPE));

			pstDispMsgsInfo->index[iCurLangId] = pstPromptList;
			pstPromptList = NULL;			

			//Check current language whether matches with default language.
			if( (strcasecmp(szLangVal, pszLangName) == SUCCESS) )
			{
				debug_sprintf(szDbgMsg, "%s: Setting current lang id of the default lang id", 
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				//Set the defalut language id in the dispMsgs node.
				pstDispMsgsInfo->iDefltLangId = iCurLangId;

				bGotDefltLang = PAAS_TRUE;	
			}

			if(bEmvEnabled == PAAS_FALSE)
			{
				/*Note: If EMV is disabled, Just load only defaultlang messages and prompts. 
						No need of loading all other language mesgs and prompts present in .ini file. 
						So break from the loop*/
				debug_sprintf(szDbgMsg, "%s: EMV is disabled; Loaded only one language msgs/prompts", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;				
			}
			pstPromptList = NULL;
			iCurLangId = -1;
		}

		if( bGotDefltLang == PAAS_FALSE )
		{
			/* There is no section defined for the language being specified in
			 * the config.usr1 file */
			debug_sprintf(szDbgMsg, "%s: No display prompts found for [%s]", __FUNCTION__, szCfgLang);
			APP_TRACE(szDbgMsg);
			//Praveen_P1: If this particular language is not there, defaulting it to English
			debug_sprintf(szDbgMsg, "%s:Defaulting to ENGLISH" , __FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szLangVal, 0x00, sizeof(szLangVal));
			sprintf(szLangVal, "%s", "ENGLISH");
			putEnvFile(SECTION_DEVICE, DFLT_LANG, szLangVal);

			/* Get the language id for the given langauge name */
			/* AjayS2: 14 Oct 2016:
			 * If default language is not set properly in config usr1, we will take ENGLISH as Default;
			 * In such a case, if ENGLISH is not a part of emv_supported language, ERROR will be thrown
			 *
			 * AjayS2: 17 Oct 2016:
			 * Loading the default language always, doesn't matter it present in supported lang or Not
			 */
			//Check whether ENGLISH is present in Display_Prompts File
			if( SUCCESS != getLangIdFromDispPrompFile(dict, szLangVal, &iCurLangId) )
			{
				debug_sprintf(szDbgMsg, "%s: Couldn't find the id for given language", __FUNCTION__);
				APP_TRACE(szDbgMsg);					
				
				rv = FAILURE;
				break;
			}
			
			//Set the defalut language id in the dispMsgs node.
			pstDispMsgsInfo->iDefltLangId = iCurLangId;

			/* Note: When EMV is enabled, We would have actually loaded all prompts/msgs earlier. 
					 So no need to load it again.*/
			if(bEmvEnabled != PAAS_TRUE && rv == SUCCESS)
			{
				continue; //load the English prompts
			}	
		}
		
		/* Allocate memory */
		if(*dpstDispMsgsInfo == NULL)
		{
			*dpstDispMsgsInfo = (DISPMSG_INFO_PTYPE) malloc(sizeof(DISPMSG_INFO_STYPE));
			if(*dpstDispMsgsInfo == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				//return FAILURE;
				//CID 67383 (#1 of 2): Resource leak (RESOURCE_LEAK). T_RaghavendranR1
				rv = FAILURE;
				break;
			}
		}
		memset(*dpstDispMsgsInfo, 0x00, sizeof(DISPMSG_INFO_STYPE));		
		/* copy data to output buffer*/
		memcpy(*dpstDispMsgsInfo, pstDispMsgsInfo, sizeof(DISPMSG_INFO_STYPE));
		break;
	}

	if(rv != SUCCESS)
	{
		PROMPT_PTR_TYPE		pstTmpNode	= NULL;
		for(iNode=0; iNode < MAX_SUPPORT_LANG; iNode++)
		{
			pstTmpNode = pstDispMsgsInfo->index[iNode];

			if(pstTmpNode != NULL)
			{
				/* Flush the structure of any residual details */
				for(iCnt = 0; iCnt < MAX_TITLE_IDX; iCnt++)
				{
					if(pstTmpNode->scrTitles[iCnt] != NULL)
					{
						free(pstTmpNode->scrTitles[iCnt]);
						pstTmpNode->scrTitles[iCnt] = NULL;
					}
				}

				for(iCnt = 0; iCnt < MAX_DISP_MSGS; iCnt++)
				{
					if(pstTmpNode->dispMsgs[iCnt] != NULL)
					{
						free(pstTmpNode->dispMsgs[iCnt]);
						pstTmpNode->dispMsgs[iCnt] = NULL;
					}
				}

				for(iCnt = 0; iCnt < MAX_BTNLBL_IDX; iCnt++)
				{
					if(pstTmpNode->btnLbls[iCnt] != NULL)
					{
						free(pstTmpNode->btnLbls[iCnt]);
						pstTmpNode->btnLbls[iCnt] = NULL;
					}
				}
				free(pstTmpNode);
			}
		}
	}
	
	// CID-67383: 25-Jan-16: MukeshS3: Free the allocated memory for disp msg info structure
	if(pstDispMsgsInfo != NULL)
	{
		free(pstDispMsgsInfo);
	}

	/* Free the dictionary */
	if(dict != NULL)
	{
		iniparser_freedict(dict);
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPromptsNodeForCurLang
 *
 * Description	: This function gives the Prompt msgs node of the given language type.
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
PROMPT_PTR_TYPE getPromptsNodeForCurLang()
{
	int						rv					= SUCCESS;
	int						iCurLangId			= -1;
	DISPMSG_INFO_PTYPE		pstDispMsgsInfo		= NULL;
	PROMPT_PTR_TYPE			pstPromptList		= NULL;
	
#ifdef DEBUG
char			szDbgMsg[256]	= "";
#endif

	pstDispMsgsInfo = uiSettings.pstDispMsgsInfo;

	if(pstDispMsgsInfo != NULL)
	{
		//Get the current language id set in the application
		iCurLangId = getCurLangId();
		pstPromptList = pstDispMsgsInfo->index[iCurLangId];

		if(pstPromptList == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Failed; No Disp msg node list for curr Def Lang",
																			__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s:  No disp msgs/prompts present", __FUNCTION__);
		APP_TRACE(szDbgMsg);	
		rv = FAILURE;
	}
	
	debug_sprintf(szDbgMsg, "%s: ---Return---", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
	
	if(rv != SUCCESS)
	{
		return NULL;
	}
	else
	{
		return pstPromptList;
	}
}

/*
 * ============================================================================
 * Function Name: setLangId
 *
 * Description	: This function is responsible for setting language Id. This API is called whenever
 *				  application need to show prompts/msgs in different language other then defalut application
 *				  language. This secenario may happen when EMV card langauge and default 
 *				  application  language both differ.
 *
 * Input Params	: Language id.
 *
 * Output Params: None
 * ============================================================================
 */
 
void setLangId(int iLangId)
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
	if( uiSettings.pstDispMsgsInfo != NULL && iLangId < MAX_SUPPORT_LANG && uiSettings.pstDispMsgsInfo->index[iLangId] != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Setting Default Lang Id as [%d]", __FUNCTION__, iLangId);
		APP_TRACE(szDbgMsg);	
		uiSettings.pstDispMsgsInfo->iDefltLangId = iLangId;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: ERROR:: Cannot set the Language", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getCurLangId
 *
 * Description	: This function is responsible for getting default language Id. This API is called whenever
 *				  application need to show prompts/msgs in different language other then defalut application
 *				  language. This secenario may happen when EMV card langauge and default 
 *				  application  language both differ.
 *
 * Input Params	: None.
 *
 * Output Params: None
 * ============================================================================
 */
 
int getCurLangId()
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: Returning current LangId[%d]",
					__FUNCTION__, uiSettings.pstDispMsgsInfo->iDefltLangId);
	APP_TRACE(szDbgMsg);
	
	return uiSettings.pstDispMsgsInfo->iDefltLangId;
}

#if 0
/*
 * ============================================================================
 * Function Name: storeDispMsgsAndPromptsLst
 *
 * Description	: This function is responsible for parsing the input ini file
 * 					and storing the prompts in the memory language wise.
 *
 * Input Params	: fileName	-> name of the file containing prompts
 * 				  pstDispDtls	-> list where the parameters would be stored
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeDispMsgsAndPromptsLst(char * fileName, char * szCfgLang,
													PROMPT_PTR_TYPE pstDispDtls)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				langCnt			= 0;
	char *			langName		= NULL;
	dictionary *	dict			= NULL;
	char			szLanVal[30]	= "";
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	sprintf(szLanVal, "%s", szCfgLang); //Copying to local buffer so that we can null set this buffer before modifying it

	/* Load the ini file in the parser library */
	dict = iniparser_load(fileName);
	if(dict == NULL)
	{
		/* Unable to load the ini file */
		debug_sprintf(szDbgMsg, "%s: Unable to parse ini file [%s]",
												__FUNCTION__, fileName);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	/* Get the number of language definitons stored in the ini file */
	langCnt = iniparser_getnsec(dict);
	if(langCnt <= 0)
	{
		debug_sprintf(szDbgMsg, "%s: No sections present in ini file [%s]",
							__FUNCTION__, fileName);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	debug_sprintf(szDbgMsg, "%s: Number of sections present = [%d]",
					__FUNCTION__, langCnt);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Get the language definitions */
		for(iCnt = 0; iCnt < langCnt; iCnt++)
		{
			langName = iniparser_getsecname(dict, iCnt);

			/* Store display prompt and messages for default language type set
			 * in config.usr1*/
			debug_sprintf(szDbgMsg, "%s: iCnt[%d], szCfgLang[%s], langName[%s]",
							__FUNCTION__, iCnt, szLanVal, langName);
			APP_TRACE(szDbgMsg);

			if(strcasecmp(szLanVal, langName) == SUCCESS)
			{
				/* A section found with the language name same as that provided
				 * in the config.usr1 file */
				memset(pstDispDtls, 0x00, sizeof(PROMPT_STRUCT_TYPE));

				/* Get the amount format stored for the language */
				getAmtFormat(dict, langName, pstDispDtls);

				/* Get the titles for the screen */
				rv = getScrnTitles(dict, langName, pstDispDtls->scrTitles);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get screen titles",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}

				/* Get the status messages for the application */
				rv = getDispMsgs(dict, langName, pstDispDtls->dispMsgs);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get display messages"
																, __FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}

				/* Get the button labels for the application screens */
				rv = getBtnLbls(dict, langName, pstDispDtls->btnLbls);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get button labels",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}

				break;
			}
		}

		if(iCnt >= langCnt)
		{
			/* There is no section defined for the language being specified in
			 * the config.usr1 file */
			debug_sprintf(szDbgMsg, "%s: No display prompts found for [%s]",
													__FUNCTION__, szCfgLang);
			APP_TRACE(szDbgMsg);
			//Praveen_P1: If this particular language is not there, defaulting it to English
			debug_sprintf(szDbgMsg, "%s:Defaulting to ENGLISH" , __FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szLanVal, 0x00, sizeof(szLanVal));
			sprintf(szLanVal, "%s", "ENGLISH");
			putEnvFile(SECTION_DEVICE, DFLT_LANG, szLanVal);
			continue; //load the English prompts
		}

		break;
	}

	if(rv != SUCCESS)
	{
		/* Flush the structure of any residual details */
		for(iCnt = 0; iCnt < MAX_TITLE_IDX; iCnt++)
		{
			if(pstDispDtls->scrTitles[iCnt] != NULL)
			{
				free(pstDispDtls->scrTitles[iCnt]);
				pstDispDtls->scrTitles[iCnt] = NULL;
			}
		}

		for(iCnt = 0; iCnt < MAX_DISP_MSGS; iCnt++)
		{
			if(pstDispDtls->dispMsgs[iCnt] != NULL)
			{
				free(pstDispDtls->dispMsgs[iCnt]);
				pstDispDtls->dispMsgs[iCnt] = NULL;
			}
		}

		for(iCnt = 0; iCnt < MAX_BTNLBL_IDX; iCnt++)
		{
			if(pstDispDtls->btnLbls[iCnt] != NULL)
			{
				free(pstDispDtls->btnLbls[iCnt]);
				pstDispDtls->btnLbls[iCnt] = NULL;
			}
		}
	}

	/* Free the dictionary */
	if(dict != NULL)
	{
		iniparser_freedict(dict);
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name:
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getScrnTitles(dictionary * dict, char * szLang, uchar ** scrTitles, char*pszErrMsg)
{
	int		rv					= SUCCESS;
	int		iCnt				= 0;
	int		iLen				= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szKey[30]			= "";
	char *	value				= NULL;
	uchar *	cTmpPtr				= NULL;
#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < MAX_TITLE_IDX; iCnt++)
	{
		sprintf(szKey, "%s:T%02d", szLang, iCnt + 1);
		value = iniparser_getstring(dict, szKey, NULL);
		if(value == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing screen title at %s",
														__FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Missing Screen Title [%s] From Display Prompts File, Please Check and Add the Title in DisplayPrompts.ini File.", szKey);
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			sprintf(pszErrMsg, "%s: Missing %s",pszErrMsg, szKey);
			rv = FAILURE;
			break;
		}

		iLen = strlen(value);
		cTmpPtr = (uchar *) malloc(iLen + 1);
		if(cTmpPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(cTmpPtr, 0x00, iLen + 1);
		memcpy(cTmpPtr, value, iLen);

		stripSpaces((char *)cTmpPtr,STRIP_LEADING_SPACES|STRIP_TRAILING_SPACES);

		scrTitles[iCnt] = cTmpPtr;
		cTmpPtr = NULL;
		value = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name:
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getDispMsgs(dictionary * dict, char * szLang, uchar ** dispMsgs, char*pszErrMsg)
{
	int		rv					= SUCCESS;
	int		iCnt				= 0;
	int		iLen				= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szKey[30]			= "";
	char *	value				= NULL;
	uchar *	cTmpPtr				= NULL;
#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < MAX_DISP_MSGS; iCnt++)
	{
		sprintf(szKey, "%s:M%02d", szLang, iCnt + 1);
		value = iniparser_getstring(dict, szKey, NULL);
		if(value == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing message prompt at %s",
														__FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Missing Display Message [%s] From Display Prompts File, Please Check and Add the Message in DisplayPrompts.ini File.", szKey);
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			sprintf(pszErrMsg, "%s: Missing %s",pszErrMsg, szKey);
			rv = FAILURE;
			break;
		}

		iLen = strlen(value);
		cTmpPtr = (uchar *) malloc(iLen + 1);
		if(cTmpPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(cTmpPtr, 0x00, iLen + 1);
		memcpy(cTmpPtr, value, iLen);

		stripSpaces((char *)cTmpPtr,STRIP_LEADING_SPACES|STRIP_TRAILING_SPACES);

		dispMsgs[iCnt] = cTmpPtr;
		cTmpPtr = NULL;
		value = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name:
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getAmtFormat(dictionary * dict, char * szLang,
												PROMPT_PTR_TYPE pstDispDtls)
{
	int		rv				= SUCCESS;
	char	szKey[16]		= "";
	char *	pszValue		= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	sprintf(szKey, "%s:amtformat", szLang);

	pszValue = iniparser_getstring(dict, szKey, NULL);
	if(pszValue == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Amt format not present; taking default",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		pstDispDtls->amtChar = '$';
		pstDispDtls->decChar = '.';
		pstDispDtls->sepChar = ',';
	}
	else
	{
		pstDispDtls->amtChar = pszValue[0];
		pstDispDtls->decChar = pszValue[1];
		pstDispDtls->sepChar = pszValue[2];
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name:
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getLangIdFromDispPrompFile(dictionary * dict, char * szLang,
												int *piLangId)
{
	int		rv				= SUCCESS;
	char	szKey[16]		= "";
	char *	pszValue		= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	sprintf(szKey, "%s:id", szLang);

	pszValue = iniparser_getstring(dict, szKey, NULL);
	if(pszValue == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Lang Id doesn't exist for Lang name[%s]",
														__FUNCTION__, szLang);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		*piLangId = atoi(pszValue);
		if(*piLangId >= MAX_SUPPORT_LANG || *piLangId < 0)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR:: Lang Id for [%s] is More than MAX Allowed[%d] or less than 0", __FUNCTION__, szLang, MAX_SUPPORT_LANG-1);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}
	//debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	//APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name:
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getLangIdFromDispPrompFileifinEMVSupLang(dictionary * dict, char * szLang,
												int *piLangId)
{
	int					rv						= SUCCESS;
	int 				iLen					= 0;
	static char			szEmvSuppLang[100]	= "";
	char				szTempEmvSuppLang[100]	= "";
	static PAAS_BOOL	bFirstTime				= PAAS_TRUE;
	char *				pszEmvSuppLang			= NULL;
	char				szKey[16]				= "";
	char *				pszValue				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//APP_TRACE(szDbgMsg);
	iLen = sizeof(szEmvSuppLang);
	while(1)
	{
		if( bFirstTime == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Getting the emv supported languages from config.usr1 file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szEmvSuppLang, 0x00, iLen);
			rv = getEnvFile(SECTION_DEVICE, EMV_SUPP_LANGS, szEmvSuppLang, iLen);
			if(rv > 0)
			{
				debug_sprintf(szDbgMsg, "%s: emv supported languages [%s]", __FUNCTION__, szEmvSuppLang);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
				bFirstTime = PAAS_FALSE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: emv supported languages not present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;

				break;
			}
		}

		debug_sprintf(szDbgMsg, "%s: Looking Language id for [%s]", __FUNCTION__, szLang);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: EMV supp Lang list is [%s]", __FUNCTION__, szEmvSuppLang);
		APP_TRACE(szDbgMsg);

		memset(szTempEmvSuppLang, 0x00, sizeof(szTempEmvSuppLang));
		strcpy(szTempEmvSuppLang, szEmvSuppLang);

		/* ArjunU1: In case of emv enabled, We need to return the language id based on
		 * 			emv_supported_lang config(config.usr1) parameter.To get language id for given
		 * 			language, emv supported language list is parsed and corresponding language
		 * 			id is returned. In case of Non Emv mode we return language id fetched from the
		 * 			ini file.
		 *
		 * 			Note: Order of emv supported language on PS side should be same as order of emv supported language on XPI side.
		 * 				  For instance, emv_supported_lang=en,es,pr,fr (config.usr1)
		 * 				  				emv_supported_lang=en,es,pr,fr (config.usr2)
		 *
		 */
		// grab the first token (making sure there is a first token!)

		/*AjayS2: We are getting all languages one by one and matching with current req Lang, if matches we are sending ID for that
		 * 	from dict
		 */
		if ((pszEmvSuppLang = (char *)strtok(szTempEmvSuppLang, ",")) != NULL)
		{
			do
			{
				if(!strcasecmp(pszEmvSuppLang, szLang))
				{
					sprintf(szKey, "%s:id", szLang);

					pszValue = iniparser_getstring(dict, szKey, NULL);
					if(pszValue == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Lang Id doesn't exist for Lang name[%s] Error in DisplayPrompts File", __FUNCTION__, szLang);
						APP_TRACE(szDbgMsg);
						rv = FAILURE;
					}
					else
					{
						*piLangId = atoi(pszValue);
						if(*piLangId >= MAX_SUPPORT_LANG || *piLangId < 0)
						{
							debug_sprintf(szDbgMsg, "%s: ERROR:: Lang Id for [%s] is More than MAX Allowed[%d] or less than 0", __FUNCTION__, szLang, MAX_SUPPORT_LANG-1);
							APP_TRACE(szDbgMsg);
							rv = FAILURE;
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Found Langid[%d] for %s language", __FUNCTION__, *piLangId, szLang);
							APP_TRACE(szDbgMsg);
						}
					}
					//Found Language id..so break from the loop.
					break;
				}
				// now, the while continuation condition grabs the
				// next token (by passing NULL as the first param)
				// and continues if the token's not NULL:
			} while ((pszEmvSuppLang = (char *)strtok(NULL, ",")) != NULL);

			if( pszEmvSuppLang == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fetch Language Id for %s", __FUNCTION__, szLang);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed to fetch Language Id for %s", __FUNCTION__, szLang);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		break;
	}
	//debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	//APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name:
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getEmvParamKeyForIndex(int index, char * szKey, int iCmd)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	if( szKey == NULL )
	{
		debug_sprintf(szDbgMsg, "%s: NULL param passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	while(1)
	{
		if( iCmd == 11 || iCmd == 12 || iCmd == 13 || iCmd == 14 ||
			iCmd == 15 || iCmd == 16 || iCmd == 17 || iCmd == 19 )
		{
			if(dfltAppIdValList[index] != NULL)
			{
				sprintf(szKey, "%s", dfltAppIdValList[index]);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Couldn't find the Emv param", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
		}
		if(iCmd == 18)
		{
			if(dfltD18ValList[index] != NULL)
			{
				sprintf(szKey, "%s", dfltD18ValList[index]);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Couldn't find the Emv param", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
		}
		if(iCmd == 20)
		{
			if(dfltD20ValList[index] != NULL)
			{
				sprintf(szKey, "%s", dfltD20ValList[index]);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Couldn't find the Emv param", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
		}
		if(iCmd == 25)
		{
			if(dfltD25ValList[index] != NULL)
			{
				sprintf(szKey, "%s", dfltD25ValList[index]);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Couldn't find the Emv param", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Unkown EMV cmd; Couldn't find the Emv param", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: updateTACFromINItoAIDList
 *
 * Description	: This API helps us in updating TACs for each AID
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateTACFromINItoAIDList(char * pszErrMsg)
{
	extern	int					errno;
	int					rv					= SUCCESS;
	int					iPos				= 0;
	int					iPosTmp				= 0;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[300]	= "";
	char *				value				= NULL;
	char*				curr				= NULL;
	char*				next				= NULL;
	char*				start				= NULL;
	char				szKey[100]			= "";
	char				szAppName[15]		= "";
	char				szLine[256]			= "";
	char				szTACDefault[11]	= "";
	char				szTACDenial[11]		= "";
	char				szTACOnline[11]		= "";
	FILE *		 		fpRead				= NULL;
	FILE *		 		fpWrite				= NULL;
	dictionary *		dict				= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	if(isEmvEnabledInDevice() == PAAS_FALSE)
	{
		debug_sprintf(szDbgMsg, "%s: EMV is Dissabled, Not Updating TACs return[SUCCESS]", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SUCCESS;
	}
	if(SUCCESS != doesFileExist(EMV_TABLES_FILE_PATH))
	{
		debug_sprintf(szDbgMsg, "%s: %s File not present, ERROR", __FUNCTION__, EMV_TABLES_FILE_PATH);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error while Loading EMV Config Files");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
		}
		getDisplayMsg(pszErrMsg, MSG_FILE_NOT_PRESENT);
		sprintf(pszErrMsg, "%s: %s", pszErrMsg, EMV_TABLES_FILE_PATH);
		rv = FAILURE;
		return rv;
	}

	rv = checkAndCorrectFileFormat(EMV_TABLES_FILE_PATH);
	if(rv !=SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to improve Format %s ", __FUNCTION__, EMV_TABLES_FILE_PATH);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error while Loading EMV Config Files");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
		}
		getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
		sprintf(pszErrMsg, "%s: %s", pszErrMsg, EMV_TABLES_FILE_PATH);
		rv = FAILURE;
		return rv;
	}

	dict = iniparser_load(EMV_TABLES_FILE_PATH);
	if(dict == NULL)
	{
		/* Unable to load the ini file */
		debug_sprintf(szDbgMsg, "%s: iniparser_load error; file [%s] could not be opened!, errno=%d [%s]", __FUNCTION__, EMV_TABLES_FILE_PATH, errno, strerror(errno));
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error in Parsing EMV Config Files");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
		}
		getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
		sprintf(pszErrMsg, "%s: %s", pszErrMsg, EMV_TABLES_FILE_PATH);
		rv = FAILURE;
		return rv;
	}

	fpRead = fopen(AID_LIST_FILE_NAME, "r");
	if (fpRead == NULL)   // File open failed!
	{
		debug_sprintf(szDbgMsg, "%s: Cannot open file [%s] for writing file information!",
				__FUNCTION__, AID_LIST_FILE_NAME);
		APP_TRACE(szDbgMsg);
		iniparser_freedict(dict);
		dict = NULL;
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error in Loading AID List File");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
		}
		getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
		sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_NAME);
		rv = FAILURE;
		return rv;
	}
	fpWrite = fopen(AID_LIST_FILE_NAME, "r+");
	if (fpWrite == NULL)   // File open failed!
	{
		debug_sprintf(szDbgMsg, "%s: Cannot open file [%s] for writing file information!",
				__FUNCTION__, AID_LIST_FILE_NAME);
		APP_TRACE(szDbgMsg);
		iniparser_freedict(dict);
		dict = NULL;
		if(fpRead!= NULL)
		{
			fclose(fpRead);
		}
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error in Loading AID List File");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
		}
		getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
		sprintf(pszErrMsg, "%s: %s", pszErrMsg, AID_LIST_FILE_NAME);
		rv = FAILURE;
		return rv;
	}
	APP_TRACE("opened file for writing");

	while(!feof(fpRead))
	{
		memset(szLine, 0x00, sizeof(szLine));
		if(fgets(szLine, sizeof(szLine), fpRead) != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Read Line [%s]", __FUNCTION__, szLine);
			APP_TRACE(szDbgMsg);

			if(strncmp(szLine, "//", 2) == 0 || strncmp(szLine, "/*", 2) == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Its a comment line, ignoring it", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(fgets(szLine, sizeof(szLine), fpWrite) == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				continue; //goes to read next line
			}

			memset(szAppName, 0x00, sizeof(szAppName));
			memset(szTACDefault, 0x00, sizeof(szTACDefault));
			memset(szTACDenial, 0x00, sizeof(szTACDenial));
			memset(szTACOnline, 0x00, sizeof(szTACOnline));

			curr = start = szLine;
			next = strchr(curr, '|');
			if(next == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break; //Invalid format
			}

			curr = next + 1;
			next = strchr(curr, '|');
			if(next == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain 2nd | in the line, invalid format!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break; //Invalid format
			}

			curr = next + 1;
			next = strchr(curr, '|');
			if(next == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break; //Invalid format
			}
			memcpy(szAppName, curr, next - curr);

			debug_sprintf(szDbgMsg, "%s: Read AID[%s] from file AIDList", __FUNCTION__, szAppName);
			APP_TRACE(szDbgMsg);


			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s.%s:%s", szAppName, "Config", "TACDefault");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				memcpy(szTACDefault, value, 10);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: No value found for key %s", __FUNCTION__, szKey);
				APP_TRACE(szDbgMsg);
				if(fgets(szLine, sizeof(szLine), fpWrite) == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				continue; //goes to read next line
			}
			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s.%s:%s", szAppName, "Config", "TACDenial");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				memcpy(szTACDenial, value, 10);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: No value found for key %s", __FUNCTION__, szKey);
				APP_TRACE(szDbgMsg);
				if(fgets(szLine, sizeof(szLine), fpWrite) == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				continue; //goes to read next line
			}
			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s.%s:%s", szAppName, "Config", "TACOnline");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				memcpy(szTACOnline, value, 10);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: No value found for key %s", __FUNCTION__, szKey);
				APP_TRACE(szDbgMsg);
				if(fgets(szLine, sizeof(szLine), fpWrite) == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				continue; //goes to read next line
			}
			debug_sprintf(szDbgMsg, "%s: szAID: %s Updating TACDef: %s TACDen: %s TACOnl: %s", __FUNCTION__, szAppName, szTACDefault, szTACDenial,szTACOnline);
			APP_TRACE(szDbgMsg);


			curr = next + 1;
			next = strchr(curr, '|');
			if(next == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break; //Invalid format
			}

			iPos = iPosTmp = ftell(fpWrite);
			if(iPos != -1)
			{
				debug_sprintf(szDbgMsg, "%s: iPos= %d\n", __FUNCTION__, iPos);
				APP_TRACE(szDbgMsg);

				iPos = iPos + curr - start;
				debug_sprintf(szDbgMsg, "%s: iPos= %d\n", __FUNCTION__, iPos);
				APP_TRACE(szDbgMsg);

				if(SUCCESS == fseek(fpWrite, iPos, SEEK_SET))
				{
					/* Daivik : Coverity 67324 - fwrite cannot return a negative interger and hence changed the
					 * condition below.
					 */
					if(fwrite(szTACDefault, 1, 10, fpWrite) == 0)
					{
						debug_sprintf(szDbgMsg, "%s: Error while fwrite!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
				}
				rv = fseek(fpWrite, iPosTmp, SEEK_SET);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while ftell!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			curr = next + 1;
			next = strchr(curr, '|');
			if(next == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break; //Invalid format
			}

			iPos = iPosTmp;
			if(iPos != -1)
			{
				debug_sprintf(szDbgMsg, "%s: iPos= %d\n", __FUNCTION__, iPos);
				APP_TRACE(szDbgMsg);

				iPos = iPos + curr - start;
				debug_sprintf(szDbgMsg, "%s: iPos= %d\n", __FUNCTION__, iPos);
				APP_TRACE(szDbgMsg);

				if(SUCCESS == fseek(fpWrite, iPos, SEEK_SET))
				{
					/* Daivik : Coverity 67324 - fwrite cannot return a negative interger and hence changed the
					 * condition below.
					 */
					if(fwrite(szTACDenial, 1, 10, fpWrite) == 0)
					{
						debug_sprintf(szDbgMsg, "%s: Error while fwrite!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
				}
				rv = fseek(fpWrite, iPosTmp, SEEK_SET);
			}


			curr = next + 1;
			next = strchr(curr, '#');
			if(next == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE; //Invalid format
				break;
			}

			iPos = iPosTmp;
			if(iPos != -1)
			{
				debug_sprintf(szDbgMsg, "%s: iPos= %d\n", __FUNCTION__, iPos);
				APP_TRACE(szDbgMsg);

				iPos = iPos + curr - start;
				debug_sprintf(szDbgMsg, "%s: iPos= %d\n", __FUNCTION__, iPos);
				APP_TRACE(szDbgMsg);

				if(SUCCESS == fseek(fpWrite, iPos, SEEK_SET))
				{
					/* Daivik : Coverity 67324 - fwrite cannot return a negative interger and hence changed the
					 * condition below.
					 */
					if(fwrite(szTACOnline, 1, 10, fpWrite) == 0)
					{
						debug_sprintf(szDbgMsg, "%s: Error while fwrite!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
				}
				rv = fseek(fpWrite, iPosTmp, SEEK_SET);
				if(fgets(szLine, sizeof(szLine), fpWrite) == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
	}

	if(fpRead!= NULL)
	{
		fclose(fpRead);
	}
	if(fpWrite!= NULL)
	{
		fclose(fpWrite);
	}
	if(dict != NULL)
	{
		iniparser_freedict(dict);
		dict = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: updateTACFromINItoAIDList
 *
 * Description	: This API helps us in updating TACs for each AID
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateTACFromINItoAIDList()
{
	int					rv					= SUCCESS;
	int					iPos				= 0;
	int					iPosTmp				= 0;
	char *				value				= NULL;
	char*				curr				= NULL;
	char*				next				= NULL;
	char*				start				= NULL;
	char				szKey[100]			= "";
	char				szAIDinList[11]		= "";
	char				szLine[256]			= "";
	char				szTACDefault[11]	= "";
	char				szTACDenial[11]		= "";
	char				szTACOnline[11]		= "";
	FILE *		 		fpRead				= NULL;
	FILE *		 		fpWrite				= NULL;
	dictionary *		dict				= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);


	if(SUCCESS != doesFileExist(CFG_INI_FILE_PATH))
	{
		debug_sprintf(szDbgMsg, "%s: %s File not present, ERROR", __FUNCTION__, CFG_INI_FILE_PATH);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	dict = iniparser_load(CFG_INI_FILE_PATH);
	if(dict == NULL)
	{
		/* Unable to load the ini file */
		debug_sprintf(szDbgMsg, "%s: Unable to load ini file [%s]",
				__FUNCTION__, CFG_INI_FILE_PATH);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	fpRead = fopen(AID_LIST_FILE_NAME, "r");
	if (fpRead == NULL)   // File open failed!
	{
		debug_sprintf(szDbgMsg, "%s: Cannot open file [%s] for writing file information!",
				__FUNCTION__, AID_LIST_FILE_NAME);
		APP_TRACE(szDbgMsg);
		iniparser_freedict(dict);
		dict = NULL;
		rv = FAILURE;
		return rv;
	}
	fpWrite = fopen(AID_LIST_FILE_NAME, "r+");
	if (fpWrite == NULL)   // File open failed!
	{
		debug_sprintf(szDbgMsg, "%s: Cannot open file [%s] for writing file information!",
				__FUNCTION__, AID_LIST_FILE_NAME);
		APP_TRACE(szDbgMsg);
		iniparser_freedict(dict);
		dict = NULL;
		if(fpRead!= NULL)
		{
			fclose(fpRead);
		}
		rv = FAILURE;
		return rv;
	}
	APP_TRACE("opened file for writing");

	while(!feof(fpRead))
	{
		memset(szLine, 0x00, sizeof(szLine));
		if(fgets(szLine, sizeof(szLine), fpRead) != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Read Line [%s]", __FUNCTION__, szLine);
			APP_TRACE(szDbgMsg);

			if(strncmp(szLine, "//", 2) == 0 || strncmp(szLine, "/*", 2) == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Its a comment line, ignoring it", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(fgets(szLine, sizeof(szLine), fpWrite) == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				continue; //goes to read next line
			}

			memset(szAIDinList, 0x00, sizeof(szAIDinList));
			memset(szTACDefault, 0x00, sizeof(szTACDefault));
			memset(szTACDenial, 0x00, sizeof(szTACDenial));
			memset(szTACOnline, 0x00, sizeof(szTACOnline));

			curr = start = szLine;
			next = strchr(curr, '|');
			if(next == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break; //Invalid format
			}
			memcpy(szAIDinList, curr, 10);

			debug_sprintf(szDbgMsg, "%s: Read AID[%s] from file AIDList", __FUNCTION__, szAIDinList);
			APP_TRACE(szDbgMsg);

			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s:%s_%s", szAIDinList, szAIDinList, "TAC_DEFAULT");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				memcpy(szTACDefault, value, 10);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: No value found for key %s", __FUNCTION__, szKey);
				APP_TRACE(szDbgMsg);
				if(fgets(szLine, sizeof(szLine), fpWrite) == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				continue; //goes to read next line
			}
			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s:%s_%s", szAIDinList, szAIDinList, "TAC_DENIAL");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				memcpy(szTACDenial, value, 10);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: No value found for key %s", __FUNCTION__, szKey);
				APP_TRACE(szDbgMsg);
				if(fgets(szLine, sizeof(szLine), fpWrite) == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				continue; //goes to read next line
			}
			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "%s:%s_%s", szAIDinList, szAIDinList, "TAC_ONLINE");
			value = iniparser_getstring(dict, szKey, NULL);
			if(value != NULL)
			{
				memcpy(szTACOnline, value, 10);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: No value found for key %s", __FUNCTION__, szKey);
				APP_TRACE(szDbgMsg);
				if(fgets(szLine, sizeof(szLine), fpWrite) == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				continue; //goes to read next line
			}
			debug_sprintf(szDbgMsg, "%s: szAID: %s Updating TACDef: %s TACDen: %s TACOnl: %s", __FUNCTION__, szAIDinList, szTACDefault, szTACDenial,szTACOnline);
			APP_TRACE(szDbgMsg);


			curr = next + 1;
			next = strchr(curr, '|');
			if(next == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain 2nd | in the line, invalid format!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break; //Invalid format
			}

			curr = next + 1;
			next = strchr(curr, '|');
			if(next == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break; //Invalid format
			}

			curr = next + 1;
			next = strchr(curr, '|');
			if(next == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break; //Invalid format
			}

			iPos = iPosTmp = ftell(fpWrite);
			if(iPos != -1)
			{
				debug_sprintf(szDbgMsg, "%s: iPos= %d\n", __FUNCTION__, iPos);
				APP_TRACE(szDbgMsg);

				iPos = iPos + curr - start;
				debug_sprintf(szDbgMsg, "%s: iPos= %d\n", __FUNCTION__, iPos);
				APP_TRACE(szDbgMsg);

				if(SUCCESS == fseek(fpWrite, iPos, SEEK_SET))
				{
					if(fwrite(szTACDefault, 1, 10, fpWrite) < 0)
					{
						debug_sprintf(szDbgMsg, "%s: Error while fwrite!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
				}
				rv = fseek(fpWrite, iPosTmp, SEEK_SET);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while ftell!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			curr = next + 1;
			next = strchr(curr, '|');
			if(next == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break; //Invalid format
			}

			iPos = iPosTmp;
			if(iPos != -1)
			{
				debug_sprintf(szDbgMsg, "%s: iPos= %d\n", __FUNCTION__, iPos);
				APP_TRACE(szDbgMsg);

				iPos = iPos + curr - start;
				debug_sprintf(szDbgMsg, "%s: iPos= %d\n", __FUNCTION__, iPos);
				APP_TRACE(szDbgMsg);

				if(SUCCESS == fseek(fpWrite, iPos, SEEK_SET))
				{
					if(fwrite(szTACDenial, 1, 10, fpWrite) < 0)
					{
						debug_sprintf(szDbgMsg, "%s: Error while fwrite!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
				}
				rv = fseek(fpWrite, iPosTmp, SEEK_SET);
			}


			curr = next + 1;
			next = strchr(curr, '#');
			if(next == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain | in the line, invalid format!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE; //Invalid format
				break;
			}

			iPos = iPosTmp;
			if(iPos != -1)
			{
				debug_sprintf(szDbgMsg, "%s: iPos= %d\n", __FUNCTION__, iPos);
				APP_TRACE(szDbgMsg);

				iPos = iPos + curr - start;
				debug_sprintf(szDbgMsg, "%s: iPos= %d\n", __FUNCTION__, iPos);
				APP_TRACE(szDbgMsg);

				if(SUCCESS == fseek(fpWrite, iPos, SEEK_SET))
				{
					if(fwrite(szTACOnline, 1, 10, fpWrite) < 0)
					{
						debug_sprintf(szDbgMsg, "%s: Error while fwrite!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
				}
				rv = fseek(fpWrite, iPosTmp, SEEK_SET);
				if(fgets(szLine, sizeof(szLine), fpWrite) == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while reading the line!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
	}

	if(fpRead!= NULL)
	{
		fclose(fpRead);
	}
	if(fpWrite!= NULL)
	{
		fclose(fpWrite);
	}
	if(dict != NULL)
	{
		iniparser_freedict(dict);
		dict = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: updateD25CmdFromINI
 *
 * Description	: This API helps us in storing D25 command value
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateD25CmdFromINI()
{
	int					rv					= SUCCESS;
	int					len					= 0;
	char *				value				= NULL;
	char				szKey[100]			= "";
	char				szD25EMVCTLS[256]	= "";
	char				szD25MSDCTLS[256]	= "";
	EMV_OTHER_REC_STYPE	stOtherRecInfo;
	FILE *		 		fp					= NULL;
	dictionary *		dict				= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

//TODO:AJAYS2: Change as in emvConfig.ini intead of OptFlag.ini

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(SUCCESS != doesFileExist(CFG_INI_FILE_PATH))
		{
			debug_sprintf(szDbgMsg, "%s: %s File not present, ERROR", __FUNCTION__, CFG_INI_FILE_PATH);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		dict = iniparser_load(CFG_INI_FILE_PATH);
		if(dict == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to load ini file [%s]",
					__FUNCTION__, CFG_INI_FILE_PATH);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		fp = fopen(D25_COMMAND_FILE_NAME, "w");
		if (fp == NULL)   // File open failed!
		{
			debug_sprintf(szDbgMsg, "%s: Cannot open file [%s] for writing file information!",
					__FUNCTION__, D25_COMMAND_FILE_NAME);
			APP_TRACE(szDbgMsg);
			iniparser_freedict(dict);
			dict = NULL;
			rv = FAILURE;
			break;
		}
		APP_TRACE("opened file for writing");

		memset(&stOtherRecInfo, 0x00, sizeof(EMV_OTHER_REC_STYPE));

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "D25", "VISA_DEBIT");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inVisaDebit = *value - '0';
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "D25", "CONTACTLESS_MODE");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inEMVContactless = *value - '0';
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "D25", "PDOL_SUPPORT");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inPDOLSupFlg = *value - '0';
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "D25", "VALUE_LINK_CARD_SUPPORT");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inValueLinkCrdSup = *value - '0';
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "D25", "IS_NFC_SUPPROT");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inISISNFCSup = *value - '0';
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "D25", "AMEX_CTLS");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inAMEXCTLSSup = *value - '0';
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "D25", "MOBILE_SUPPORT");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inMobileSupport = *value - '0';
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "D25", "US_DEBIT_FLAG");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inUSDebitFlag = *value - '0';
		}

		memset(szD25EMVCTLS, 0x00, sizeof(szD25EMVCTLS));
		getD25CmdReq(szD25EMVCTLS, &len, &stOtherRecInfo);

		memset(szD25MSDCTLS, 0x00, sizeof(szD25MSDCTLS));
		stOtherRecInfo.inEMVContactless = 0;
		getD25CmdReq(szD25MSDCTLS, &len, &stOtherRecInfo);

		debug_sprintf(szDbgMsg, "%s: Builded szD25EMVCTLS[%s]", __FUNCTION__, szD25EMVCTLS);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Builded szD25MSDCTLS[%s]", __FUNCTION__, szD25MSDCTLS);
		APP_TRACE(szDbgMsg);

		if(dict != NULL)
		{
			iniparser_freedict(dict);
			dict = NULL;
		}
		memset(szKey, 0x00, sizeof(szKey));
		if(fputs(szD25EMVCTLS, fp) <= 0)
		{
			rv = FAILURE;
			debug_sprintf(szDbgMsg, "%s: Failed to write in file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey,"|");
		if(fputs(szKey, fp) <= 0)
		{
			rv = FAILURE;
			debug_sprintf(szDbgMsg, "%s: Failed to write in file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		if(fputs(szD25MSDCTLS, fp) <= 0)
		{
			rv = FAILURE;
			debug_sprintf(szDbgMsg, "%s: Failed to write in file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey,"#");
		if(fputs(szKey, fp) <= 0)
		{
			rv = FAILURE;
			debug_sprintf(szDbgMsg, "%s: Failed to write in file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		break;
	}
	if(fp!= NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: updateD25CmdFromINI
 *
 * Description	: This API helps us in storing D25 command value
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateD25CmdFromINI()
{
	int					rv					= SUCCESS;
	int					len					= 0;
	char *				value				= NULL;
	char				szKey[100]			= "";
	char				szD25EMVCTLS[256]	= "";
	char				szD25MSDCTLS[256]	= "";
	EMV_OTHER_REC_STYPE	stOtherRecInfo;
	FILE *		 		fp					= NULL;
	dictionary *		dict				= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(SUCCESS != doesFileExist(OPT_FLAG_FILE_PATH))
		{
			debug_sprintf(szDbgMsg, "%s: %s File not present, ERROR", __FUNCTION__, OPT_FLAG_FILE_PATH);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		dict = iniparser_load(OPT_FLAG_FILE_PATH);
		if(dict == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to load ini file [%s]",
					__FUNCTION__, OPT_FLAG_FILE_PATH);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		fp = fopen(D25_COMMAND_FILE_NAME, "w");
		if (fp == NULL)   // File open failed!
		{
			debug_sprintf(szDbgMsg, "%s: Cannot open file [%s] for writing file information!",
					__FUNCTION__, D25_COMMAND_FILE_NAME);
			APP_TRACE(szDbgMsg);
			iniparser_freedict(dict);
			dict = NULL;
			rv = FAILURE;
			break;
		}
		APP_TRACE("opened file for writing");

		memset(&stOtherRecInfo, 0x00, sizeof(EMV_OTHER_REC_STYPE));

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "Rec_00", "VisaDebit");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inVisaDebit = *value - '0';
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "Rec_00", "CTLSEnable");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inEMVContactless = *value - '0';
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "Rec_00", "PDOLSupport");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inPDOLSupFlg = *value - '0';
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "Rec_00", "ValueLinkSupport");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inValueLinkCrdSup = *value - '0';
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "Rec_00", "AMEXCTLS");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inAMEXCTLSSup = *value - '0';
		}

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:%s", "Rec_00", "MobileSupport");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			stOtherRecInfo.inMobileSupport = *value - '0';
		}

		memset(szD25EMVCTLS, 0x00, sizeof(szD25EMVCTLS));
		getD25CmdReq(szD25EMVCTLS, &len, &stOtherRecInfo);

		memset(szD25MSDCTLS, 0x00, sizeof(szD25MSDCTLS));
		stOtherRecInfo.inEMVContactless = 0;
		getD25CmdReq(szD25MSDCTLS, &len, &stOtherRecInfo);

		debug_sprintf(szDbgMsg, "%s: Builded szD25EMVCTLS[%s]", __FUNCTION__, szD25EMVCTLS);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Builded szD25MSDCTLS[%s]", __FUNCTION__, szD25MSDCTLS);
		APP_TRACE(szDbgMsg);

		memset(szKey, 0x00, sizeof(szKey));
		if(fputs(szD25EMVCTLS, fp) <= 0)
		{
			rv = FAILURE;
			debug_sprintf(szDbgMsg, "%s: Failed to write in file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey,"|");
		if(fputs(szKey, fp) <= 0)
		{
			rv = FAILURE;
			debug_sprintf(szDbgMsg, "%s: Failed to write in file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		if(fputs(szD25MSDCTLS, fp) <= 0)
		{
			rv = FAILURE;
			debug_sprintf(szDbgMsg, "%s: Failed to write in file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey,"#");
		if(fputs(szKey, fp) <= 0)
		{
			rv = FAILURE;
			debug_sprintf(szDbgMsg, "%s: Failed to write in file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		break;
	}
	if(fp!= NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name:
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getBtnLbls(dictionary * dict, char * szLang, BTNLBL_PTYPE *pstBtnLbl, char *pszErrMsg)
{
	int				rv					= SUCCESS;
	int				iLen				= 0;
	int				iCnt				= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	char			szKey[16]			= "";
	char *			value				= NULL;
	BTNLBL_PTYPE	pstTmpLbl			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < MAX_BTNLBL_IDX; iCnt++)
	{
		sprintf(szKey, "%s:L%02d", szLang, iCnt + 1);
		value = iniparser_getstring(dict, szKey, NULL);
		if(value == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Button labels at %s",
														__FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Missing Button Label [%s] From Display Prompts File, Please Check and Add the Button Label in DisplayPrompts.ini File.", szKey);
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}
			sprintf(pszErrMsg, "%s: Missing %s",pszErrMsg, szKey);
			rv = FAILURE;
			break;
		}

		/* Value is present, parse and store it */
		pstTmpLbl = (BTNLBL_PTYPE) malloc(sizeof(BTNLBL_STYPE));
		if(pstTmpLbl == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pstTmpLbl, 0x00, sizeof(BTNLBL_STYPE));
		iLen = strlen(value);
		if(iLen > 64)
		{
			iLen = 64;
		}

		if(iLen > 32)
		{
			memcpy(pstTmpLbl->szLblTwo, &value[32], iLen - 32);
			iLen -= (iLen - 32);
		}

		memcpy(pstTmpLbl->szLblOne, &value[0], iLen);

		/* Strip the leading/trailing white space characters */
		stripSpaces((char *) pstTmpLbl->szLblOne, STRIP_LEADING_SPACES| STRIP_TRAILING_SPACES);
		stripSpaces((char *) pstTmpLbl->szLblTwo, STRIP_LEADING_SPACES| STRIP_TRAILING_SPACES);

		pstBtnLbl[iCnt] = pstTmpLbl;

		value = NULL;
		pstTmpLbl = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isContactlessEmvEnabledInDevice
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isContactlessEmvEnabledInDevice()
{
	return uiSettings.bCTLSEmvEnabled;
}

/*
 * ============================================================================
 * Function Name: isContactlessEnabledInDevice
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isContactlessEnabledInDevice()
{
	return uiSettings.bCTLSEnabled;
}

/*
 * ============================================================================
 * Function Name: isCardBasedTenderSelEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isCardBasedTenderSelEnabled()
{
	return uiSettings.bCardBasedTenderSel;
}

/*
 * ============================================================================
 * Function Name: isEmvKernelSwitchingAllowed
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isEmvKernelSwitchingAllowed()
{
	return uiSettings.bEmvKernelSwitching;
}

/*
 * ============================================================================
 * Function Name: isServiceCodeCheckInFallbackEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isServiceCodeCheckInFallbackEnabled()
{
	return uiSettings.bServiceCodeCheckInFallback;
}

/*
 * ============================================================================
 * Function Name: isEmptyCandidateCardAsFallBack
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isEmptyCandidateCardAsFallBack()
{
	return uiSettings.bEmptyCandidAsFallBack;
}

/*
 * ============================================================================
 * Function Name: isStoreCardDtlsForPostAuthEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isStoreCardDtlsForPostAuthEnabled()
{
	return uiSettings.bStorePrevTransCardDtls;
}

/*
 * ============================================================================
 * Function Name: isStoreCardDtlsForGiftCloseEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isStoreCardDtlsForGiftCloseEnabled()
{
	return uiSettings.bStorePrevTransCardDtlsForGiftClose;
}

/*
 * ============================================================================
 * Function Name: getInternalTapFlagfromXPI
 *
 * Description	: Return internal_tap_flag from iab by reading
 * 					directly from file
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getInternalTapFlagfromXPI()
{
	int		rv						= 0;
	int		iLen					= 0;
	int		iVal					= 0;
	char	szTemp[20]				= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	iLen = sizeof(szTemp);
	memset(szTemp, 0x00, iLen);

	rv = getEnvFile(SECTION_XPI_IAB, XPI_INTERNAL_TAP_FLAG, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		debug_sprintf(szDbgMsg,"%s: Value found for internal_tap_flag = %d", __FUNCTION__, iVal);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		/* No value found for internal_tap_flag*/
		debug_sprintf(szDbgMsg,"%s: No value found for internal_tap_flag returning -1", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iVal = -1;
	}
	return iVal;
}

/*
 * ============================================================================
 * Function Name: isEmvEnabledInDevice
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isEmvEnabledInDevice()
{
	return uiSettings.bEmvEnabled;
}

/*
 * ============================================================================
 * Function Name: isVerboseRecptEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isVerboseRecptEnabled()
{
	return uiSettings.verboseRecptEnabled;
}

/*
 * ============================================================================
 * Function Name: isDescriptiveEntryModeEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isDescriptiveEntryModeEnabled()
{
	return uiSettings.bDescriptiveEntryMdoe;
}

/*
 * ============================================================================
 * Function Name: isEmvCustomTagsAllowed
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isEmvCustomTagsAllowed()
{
	return uiSettings.bEmvCustomTagReqd;
}

/*
 * ============================================================================
 * Function Name: isTagsReqInCardPresentVoid
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isTagsReqInCardPresentVoid()
{
	return uiSettings.bTagsReqdCardPresentVoid;
}

/*
 * ============================================================================
 * Function Name: isPartialEmvAllowed
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isPartialEmvAllowed()
{
	return uiSettings.bPartialEmvAllowed;
}

/*
 * ============================================================================
 * Function Name: isClearExpiryToEmvHost
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isClearExpiryToEmvHost()
{
	return uiSettings.bClearExpToEmvHost;
}

/*
 * ============================================================================
 * Function Name: isApplePayWalletEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isApplePayWalletEnabled()
{
	return uiSettings.bApplePayWalletSupport;
}

/*
 * ============================================================================
 * Function Name: isGooglePayWalletEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isGooglePayWalletEnabled()
{
	return uiSettings.bGooglePayWalletSupport;
}

/*
 * ============================================================================
 * Function Name: isSamsungPayWalletEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isSamsungPayWalletEnabled()
{
	return uiSettings.bSamsungPayWalletSupport;
}

/*
 * ============================================================================
 * Function Name: isWalletEnabled
 *
 * Description	: Used to check whether VAS Details Captured or not
 *
 * Input Params	:
 *
 * Output Params: bool
 * ============================================================================
 */
PAAS_BOOL isWalletEnabled()
{
	//Currently Only Wallet Supporting is Apple Pay Wallet, so setting only Apple Wallet here. Need to add more wallets later.
	return (uiSettings.bApplePayWalletSupport);//
}

/*
 * ============================================================================
 * Function Name: isExpCvvReqdForPrivLabel
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isCvvReqdForPrivLabel()
{
	return uiSettings.privLabelCvvReqd;
}

/*
 * ============================================================================
 * Function Name: isExpCvvReqdForPrivLabel
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isExpReqdForPrivLabel()
{
	return uiSettings.privLabelExpReqd;
}

/*
 * ============================================================================
 * Function Name: isExpCvvReqdForGiftCard
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isCvvReqdForGiftCard()
{
	return uiSettings.giftcardCvvReqd;
}

/*
 * ============================================================================
 * Function Name: isExpCvvReqdForGiftCard
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isExpReqdForGiftCard()
{
	return uiSettings.giftcardExpReqd;
}

/*
 * ============================================================================
 * Function Name: isPINCodeReqdForGiftCard
 *
 * Description	:
 *
 * Input Params	: NONE
 *
 * Output Params: TRUE/FALSE
 * ============================================================================
 */
PAAS_BOOL isPINCodeReqdForGiftCard()
{
	return uiSettings.giftcardPINCodeReqd;
}

/*
 * ============================================================================
 * Function Name: isCvvReqdForEBTCard
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isCvvReqdForEBTCard()
{
	return uiSettings.ebtcardCvvReqd;
}

/*
 * ============================================================================
 * Function Name: isExpReqdForEBTCard
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isExpReqdForEBTCard()
{
	return uiSettings.ebtcardExpReqd;
}

/*
 * ============================================================================
 * Function Name: isSigOptionReqOnPINEntryScreen
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isSigOptionReqOnPINEntryScreen()
{
	return uiSettings.sigOptOnPINEntryEnabled;
}

/*
 * ============================================================================
 * Function Name: getDeviceType
 *
 * Description	: This function returns the model of the device on which the
 * 					application is currently running.
 *
 * Input Params	: none
 *
 * Output Params: MODEL_MX915 / MODEL_MX925
 * ============================================================================
 */
int getDevicePlatform()
{
	if(uiSettings.devModel == MODEL_MX925)
	{
		return MODEL_MX925;
	}
	else
	{
		return MODEL_MX915;
	}
}

/*
 * ============================================================================
 * Function Name: setSmartCardEvents
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int setSmartCardEvents(PAAS_BOOL status)
{
	int		rv			= SUCCESS;
	char	strVal[2]	= "";

	switch(status)
	{
	case PAAS_TRUE:
		*strVal = '1';
		break;

	case PAAS_FALSE:
		*strVal = '0';
		break;
	}
	rv = putEnvFile(SECTION_REG, "smartcard_events", strVal);
	if(rv < 0)
	{
		rv = FAILURE;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: setEMVVersion
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
void setEMVVersion(char* pszEMVVersion, char* pszEmvKernelVer, char* pszEmvCTLSVer)
{
	strcpy(uiSettings.szEmvVersion, pszEMVVersion);
	strcpy(uiSettings.szEMVKernelVer, pszEmvKernelVer);
	strcpy(uiSettings.szEMVCTLSVer, pszEmvCTLSVer);
}

/*
 * ============================================================================
 * Function Name: setEMVTagsReqByHost
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
void setEMVTagsReqByHost(char* pszEMVTagsList)
{
	strcpy(uiSettings.szEmvTagsReqbyHost, pszEMVTagsList);
}

/*
 * ============================================================================
 * Function Name: bEmvUpdateReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
void setEmvInitUpdateFlag(PAAS_BOOL bEmbUpdateReqd)
{
	uiSettings.bEmvUpdateReq = bEmbUpdateReqd;
}
/*
 * ============================================================================
 * Function Name: getEMVVersion
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
char* getEMVVersion()
{
	return uiSettings.szEmvVersion;
}
/*
 * ============================================================================
 * Function Name: getEMVTagsReqByHost
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
char* getEMVTagsReqByHost()
{
	return uiSettings.szEmvTagsReqbyHost;
}
/*
 * ============================================================================
 * Function Name: getEMVKernelVersion
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
char* getEMVKernelVersion()
{
	return uiSettings.szEMVKernelVer;
}
/*
 * ============================================================================
 * Function Name: getEMVCTLSVersion
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
char* getEMVCTLSVersion()
{
	return uiSettings.szEMVCTLSVer;
}
/*
 * ============================================================================
 * Function Name: getEmvInitUpdateFlag
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
PAAS_BOOL getEmvInitUpdateFlag()
{
	return uiSettings.bEmvUpdateReq;
}

#if 0
/*
 * ============================================================================
 * Function Name: storeD25Commands
 *
 * Description	: This API stores D25 Commands in memory
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int storeD25Commands(char * szD25EMVCTLS, char * szD25MSDCTLS)
{
	int			rv					= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: D25EMV Ctls command: %s", __FUNCTION__, szD25EMVCTLS);
	APP_TRACE(szDbgMsg);

	strcpy(uiSettings.szD25EMVCtls, szD25EMVCTLS);

	debug_sprintf(szDbgMsg, "%s: D25MSD Ctls command: %s", __FUNCTION__, szD25EMVCTLS);
	APP_TRACE(szDbgMsg);

	strcpy(uiSettings.szD25MSDCtls, szD25MSDCTLS);

	debug_sprintf(szDbgMsg, "%s: --- Returning [%d] ---", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: getD25Commands
 *
 * Description	: This API returns D25 Commands from memory
 * Input Params: bcmd = TRUE return EMVCTLS cmd
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
char* getD25Commands(PAAS_BOOL bEMV)
{
	if(bEMV == PAAS_TRUE)
	{
		return uiSettings.szD25EMVCtls;
	}
	else
	{
		return uiSettings.szD25MSDCtls;
	}
}
#endif
/*
 * ============================================================================
 * Function Name: updateFallbackFromHost
 *
 * Description	: This API will update ini file
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateFallbackFromHost(FALLBACK_PTYPE pstFallBckIndicator, dictionary *dict)
{
	int					rv					= 0;
	int					iCount				= 0;
	int					MAXAID				= 6;
	char				szKey[100]			= "";
	char				szVal[100]			= "";
	char				*szAID[]			= { "A000000025", "A000000003", "A000000004", "A000000065", "A000000277", "A000000152"};
	char				*szApp[]			= { "AMEX", "VISA", "MASTERCARD", "JCB", "INTERAC", "DISCOVER"};
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif


	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Got AID[%s]", __FUNCTION__, pstFallBckIndicator->szAppID);
	APP_TRACE(szDbgMsg);
	for(iCount = 0; iCount < MAXAID; iCount++)
	{
		if( strcasecmp(pstFallBckIndicator->szAppID, szAID[iCount]) != 0)
		{
			debug_sprintf(szDbgMsg, "%s: AID [%s] continuing to next AID",__FUNCTION__, szAID[iCount]);
			APP_TRACE(szDbgMsg);
			continue;
		}


		memset(szVal, 0x00, sizeof(szVal));
		memcpy(szVal, pstFallBckIndicator->szFallBackValue, 1);

		strcpy(szVal, ((*szVal == 'Y' || *szVal == 'y')?"1":"0") );
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s.%s:%s", szApp[iCount], "Config", "FallbackAllowed");

		debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
		APP_TRACE(szDbgMsg);

		rv = iniparser_setstr(dict, szKey, szVal);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to Update INI File",
					__FUNCTION__);
			break;
		}
		//AID Found break now
		break;
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning [%d]----", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: updateOfflineFloorLimit
 *
 * Description	: This API will update ini file
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateOfflineFloorLimit(OFFLINE_FLR_LIMIT_PTYPE pstOfflineFlrLmt, dictionary *dict)
{
	int					rv					= 0;
	int					iCount				= 0;
	int					MAXAID				= 6;
	char				szKey[100]			= "";
	char				szVal[100]			= "";
	char				*szAID[]			= { "A000000025", "A000000003", "A000000004", "A000000065", "A000000277", "A000000152"};
	char				*szApp[]			= { "Amex", "Visa", "MasterCard", "JCB", "Interac", "Discover"};
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif


	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Got AID[%s]", __FUNCTION__, pstOfflineFlrLmt->szAppID);
	APP_TRACE(szDbgMsg);
	for(iCount = 0; iCount < MAXAID; iCount++)
	{
		if( strcasecmp(pstOfflineFlrLmt->szAppID, szAID[iCount]) != 0)
		{
			debug_sprintf(szDbgMsg, "%s: AID [%s] continuing to next AID",__FUNCTION__, szAID[iCount]);
			APP_TRACE(szDbgMsg);
			continue;
		}

		memset(szVal, 0x00, sizeof(szVal));
		memcpy(szVal, (char*)pstOfflineFlrLmt->szOffFlrLmt + 2, 4);
		memcpy(szVal + 4, (char*)pstOfflineFlrLmt->szOffFlrLmt + 7, 2);

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s.%s:%s", szApp[iCount], "Config", "FloorLimit");

		debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
		APP_TRACE(szDbgMsg);

		rv = iniparser_setstr(dict, szKey, szVal);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to Update INI File", __FUNCTION__);
			break;
		}

		memset(szVal, 0x00, sizeof(szVal));
		memcpy(szVal, (char*)pstOfflineFlrLmt->szOffFlrLmtThrshld + 2, 4);
		memcpy(szVal + 4, (char*)pstOfflineFlrLmt->szOffFlrLmtThrshld + 7, 2);

		memset(szVal, 0x00, sizeof(szVal));
		memcpy(szVal, pstOfflineFlrLmt->szOffFlrLmtThrshld, 1);

		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s.%s:%s", szApp[iCount], "Config", "RSThreshold");

		debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
		APP_TRACE(szDbgMsg);

		rv = iniparser_setstr(dict, szKey, szVal);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to Update INI File",
					__FUNCTION__);
			break;
		}
		//AID Found break now
		break;
	}
	debug_sprintf(szDbgMsg, "%s: ---Returning [%d]----", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: updateCAPKInfo
 *
 * Description	: This API will update ini file
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateCAPKInfo(EMV_CAPK_INFO_PTYPE pstCAPKInfo, dictionary *dict)
{
	int					rv					= 0;
	int					idivMod				= 0;
	char				szRIDKey[15]		= "";
	char				szTmp[100]			= "";
	char				szKey[100]			= "";
	char				szVal[1024]			= "";
	char				szTmpMod[1024]		= "";
#ifdef DEBUG
	char		szDbgMsg[2048]		= "";
#endif


	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Got RID[%s]", __FUNCTION__, pstCAPKInfo->szRID);
	APP_TRACE(szDbgMsg);


	memset(szRIDKey, 0x00, sizeof(szRIDKey));
	memcpy(szRIDKey, pstCAPKInfo->szRID , pstCAPKInfo->iRIDLen);

	memset(szTmp, 0x00, sizeof(szTmp));
	memcpy(szTmp, pstCAPKInfo->szPKIndex , pstCAPKInfo->iPKIndexLen);

	strcat(szRIDKey, szTmp);


	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "DATA", "MODULUSLEN");
	memset(szVal, 0x00, sizeof(szVal));
	sprintf(szVal, "%d", (pstCAPKInfo->iModLen)/2);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_setstr(dict, szKey, szVal);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File", __FUNCTION__);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "DATA", "EXPONENTLEN");
	memset(szVal, 0x00, sizeof(szVal));
	sprintf(szVal, "%d", (pstCAPKInfo->iExpLen)/2);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_setstr(dict, szKey, szVal);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File", __FUNCTION__);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "DATA", "EXPONENT");
	memset(szVal, 0x00, sizeof(szVal));
	memcpy(szVal, pstCAPKInfo->szExponent, pstCAPKInfo->iExpLen);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_setstr(dict, szKey, szVal);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File", __FUNCTION__);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "DATA", "HASHVALUE");
	memset(szVal, 0x00, sizeof(szVal));
	memcpy(szVal, pstCAPKInfo->szCheckSum, pstCAPKInfo->iCheckSumLen);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_setstr(dict, szKey, szVal);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File", __FUNCTION__);
		return rv;
	}

	idivMod = pstCAPKInfo->iModLen / 8;
	memset(szTmpMod, 0x00, sizeof(szTmpMod));
	memcpy(szTmpMod, pstCAPKInfo->szModulus, pstCAPKInfo->iModLen);

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "MODULUS", "MOD1");
	memset(szVal, 0x00, sizeof(szVal));
	memcpy(szVal, szTmpMod, idivMod);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_setstr(dict, szKey, szVal);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File", __FUNCTION__);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "MODULUS", "MOD2");
	memset(szVal, 0x00, sizeof(szVal));
	memcpy(szVal, (char *)szTmpMod + idivMod, idivMod);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_setstr(dict, szKey, szVal);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File", __FUNCTION__);
		return rv;
	}
	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "MODULUS", "MOD3");
	memset(szVal, 0x00, sizeof(szVal));
	memcpy(szVal, (char *)szTmpMod + idivMod*2, idivMod);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_setstr(dict, szKey, szVal);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File", __FUNCTION__);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "MODULUS", "MOD4");
	memset(szVal, 0x00, sizeof(szVal));
	memcpy(szVal, (char *)szTmpMod + idivMod*3, idivMod);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_setstr(dict, szKey, szVal);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File", __FUNCTION__);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "MODULUS", "MOD5");
	memset(szVal, 0x00, sizeof(szVal));
	memcpy(szVal, (char *)szTmpMod + idivMod*4, idivMod);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_setstr(dict, szKey, szVal);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File", __FUNCTION__);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "MODULUS", "MOD6");
	memset(szVal, 0x00, sizeof(szVal));
	memcpy(szVal, (char *)szTmpMod + idivMod*5, idivMod);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_setstr(dict, szKey, szVal);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File", __FUNCTION__);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "MODULUS", "MOD7");
	memset(szVal, 0x00, sizeof(szVal));
	memcpy(szVal, (char *)szTmpMod + idivMod*6, idivMod);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_setstr(dict, szKey, szVal);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File", __FUNCTION__);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "MODULUS", "MOD8");
	memset(szVal, 0x00, sizeof(szVal));
	memcpy(szVal, (char *)szTmpMod + idivMod*7, idivMod);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_setstr(dict, szKey, szVal);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File", __FUNCTION__);
		return rv;
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning [%d]----", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: updateCAPKInfo
 *
 * Description	: This API will update ini file
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateCAPKInfo(EMV_CAPK_INFO_PTYPE pstCAPKInfo, dictionary *dict,  char *pszSupportedSchemes)
{
	int					rv					= 0;
	int					idivMod				= 0;
	int					idivRemainder		= 0;
	int					iCnt				= 0;
	static	int			iCAPKCnt			= 0;
	char				szRIDKey[15]		= "";
	char				szKey[100]			= "";
	char				szVal[1024]			= "";
	char				szTmpMod[1024]		= "";
#ifdef DEBUG
	char		szDbgMsg[2048]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----\n", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Got RID[%s]\n", __FUNCTION__, pstCAPKInfo->szRID);
	APP_TRACE(szDbgMsg);

	//Checking whether this Public Key already exists or not
	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s%s.%s:%s",pstCAPKInfo->szRID, pstCAPKInfo->szPKIndex, "DATA", "MODULUSLEN");
	if(iniparser_getstring(dict, szKey, NULL) != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Public Key with RID[%s] & Index[%s] Already exists", __FUNCTION__, pstCAPKInfo->szRID, pstCAPKInfo->szPKIndex);
		APP_TRACE(szDbgMsg);
		return SUCCESS;
	}

	//Check whether current Scheme supported or not
	if(strstr(pszSupportedSchemes, pstCAPKInfo->szRID) == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Current RID not supported Not adding in CAPKData File", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SUCCESS;
	}

	//Get current Count for the Public keys
	//iCnt = iniparser_getsecnkeys(dict,"capkfiles");
	iCnt = iCAPKCnt++;
	memset(szVal, 0x00, sizeof(szVal));
	sprintf(szVal, "%s%d", "FILENAME", iCnt + 1);

	debug_sprintf(szDbgMsg, "%s: adding %s  icnt= %d", __FUNCTION__, szVal, iCnt);
	APP_TRACE(szDbgMsg);
	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s:%s", "capkfiles", szVal);

	memset(szVal, 0x00, sizeof(szVal));
	sprintf(szVal, "%s.%s", pstCAPKInfo->szRID, pstCAPKInfo->szPKIndex);

	iniparser_set(dict, szKey, szVal);
	if( rv != 0)
	{
		debug_sprintf(szDbgMsg, "%s: Error while adding key to CAPKData file\n", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return rv;
	}
	debug_sprintf(szDbgMsg, "%s: added key %s val %s\n", __FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	memset(szRIDKey, 0x00, sizeof(szRIDKey));
	sprintf(szRIDKey, "%s%s", pstCAPKInfo->szRID, pstCAPKInfo->szPKIndex);

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s", szRIDKey, "DATA");
	rv = iniparser_set(dict, szKey, NULL);
	if( rv != 0)
	{
		debug_sprintf(szDbgMsg, "%s: Error while adding key to CAPKData file\n", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "DATA", "MODULUSLEN");
	memset(szVal, 0x00, sizeof(szVal));
	sprintf(szVal, "%d", (pstCAPKInfo->iModLen)/2);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s\n",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_set(dict, szKey, szVal);
	if(rv != 0)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File\n", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "DATA", "EXPONENTLEN");
	memset(szVal, 0x00, sizeof(szVal));
	sprintf(szVal, "%d", (pstCAPKInfo->iExpLen)/2);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s\n",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_set(dict, szKey, szVal);
	if(rv != 0)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File\n", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "DATA", "EXPONENT");
	memset(szVal, 0x00, sizeof(szVal));
	memcpy(szVal, pstCAPKInfo->szExponent, pstCAPKInfo->iExpLen);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s\n",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_set(dict, szKey, szVal);
	if(rv != 0)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File\n", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "DATA", "HASHVALUE");
	memset(szVal, 0x00, sizeof(szVal));
	memcpy(szVal, pstCAPKInfo->szCheckSum, pstCAPKInfo->iCheckSumLen);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s\n",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	rv = iniparser_set(dict, szKey, szVal);
	if(rv != 0)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to Update INI File\n", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return rv;
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s:%s", szRIDKey, "DATA", "CAPKEXPDATE");
	memset(szVal, 0x00, sizeof(szVal));
	strcpy(szVal, pstCAPKInfo->szExpDate);
	debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s\n",__FUNCTION__, szKey, szVal);
	APP_TRACE(szDbgMsg);
	if(strlen(szVal) > 0)
	{
		rv = iniparser_set(dict, szKey, szVal);
		if(rv != 0)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to Update INI File\n", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return rv;
		}
	}
	else
	{
		rv = iniparser_set(dict, szKey, NULL);
		if(rv != 0)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to Update INI File\n", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return rv;
		}
	}

	memset(szKey, 0x00, sizeof(szKey));
	sprintf(szKey, "%s.%s", szRIDKey, "MODULUS");
	rv = iniparser_set(dict, szKey, NULL);
	if(rv != 0)
	{
		debug_sprintf(szDbgMsg, "%s: Error while adding key to CAPKData file\n", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return rv;
	}

	idivMod = pstCAPKInfo->iModLen / 8;
	//If Pub Key is not multiple of 8, We will add the remainder to last MOD8
	idivRemainder = pstCAPKInfo->iModLen % 8;

	memset(szTmpMod, 0x00, sizeof(szTmpMod));
	memcpy(szTmpMod, pstCAPKInfo->szModulus, pstCAPKInfo->iModLen);

	for(iCnt = 1; iCnt <= 8; iCnt++)
	{
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s.%s:%s%d", szRIDKey, "MODULUS", "MOD", iCnt);
		memset(szVal, 0x00, sizeof(szVal));
		if(iCnt == 8)
		{
			memcpy(szVal, (char *)szTmpMod + idivMod*(iCnt -1), idivMod + idivRemainder);
		}
		else
		{
			memcpy(szVal, (char *)szTmpMod + idivMod*(iCnt -1), idivMod);
		}
		debug_sprintf(szDbgMsg, "%s: szKey: %s szVal: %s\n",__FUNCTION__, szKey, szVal);
		APP_TRACE(szDbgMsg);
		rv = iniparser_set(dict, szKey, szVal);
		if(rv != 0)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to Update INI File\n", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return rv;
		}
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning [%d]----\n", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: checkAndCorrectFileFormat
 *
 * Description	: This API will check ini file for any semicolons or blank lines
 *
 * Input Params: Name of the file need to check
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int checkAndCorrectFileFormat(char* szFileName)
{
	int		rv 				= SUCCESS;
	char	szLine[256]		= "";
	char	szTmpFileName[]	= "./flash/temp@#$%1312273";
	FILE *	fpWrite			= NULL;
	FILE *	fpRead			= NULL;
#ifdef DEBUG
	char		szDbgMsg[2048]		= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Entering ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	fpRead = fopen(szFileName, "r");
	fpWrite = fopen(szTmpFileName, "w");
	if(fpRead == NULL || fpWrite == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to open files for Read or Write", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		if(fpWrite != NULL)
		{
			fclose(fpWrite);
		}
		if(fpRead != NULL)
		{
			fclose(fpRead);
		}
		return rv;
	}
	/* copy the contents of file 1 to file 2 except all blank lines and semicolons;*/
	while (!feof(fpRead))
	{
		memset(szLine, 0x00, sizeof(szLine));
		fgets(szLine, sizeof(szLine) - 1, fpRead);
		//Since empty line in Linux will have CR and LF so taking len as 2
		if( (strlen(szLine) <= 2) || (szLine[0] == ';') )
		{
			continue;
		}
		fputs(szLine, fpWrite);
	}

	if(remove(szFileName) != SUCCESS) // CID 67307 (#1 of 1): Unchecked return value from library (CHECKED_RETURN) T_RaghavendranR1
	{
		debug_sprintf(szDbgMsg, "%s: Remove File [%s] Failed ", __FUNCTION__, szFileName);
		APP_TRACE(szDbgMsg);
	}

	/* Daivik: Coverity 67362 - Check the return value of rename */
	if(rename(szTmpFileName, szFileName) < 0)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to rename the Files", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	if(fpWrite != NULL)
	{
		fclose(fpWrite);
	}
	if(fpRead != NULL)
	{
		fclose(fpRead);
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning [%d]----", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
