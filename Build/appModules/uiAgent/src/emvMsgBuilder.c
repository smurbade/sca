/******************************************************************
*                       emvMsgBuilder.c                           *
*******************************************************************
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include "common/common.h"
#include "uiAgent/uiCfgDef.h"
#include "uiAgent/emv.h"
#include "uiAgent/emvAPIs.h"
#include "uiAgent/emvConstants.h"
#include "common/tranDef.h"
#include "db/tranDS.h"
#include "db/safDataStore.h"
#include "ssi/ssiDef.h"
#include "bLogic/bLogicCfgDef.h"
#include "appLog/appLogAPIs.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

//static functions
//static void  Char2Hex(char *, char *, int);
//static char *rightPad(char *, int, char);
//static char *leftPad(char *, int, char);

/* Extern variables */
extern int				giLabelLen;
extern int splitStringOfReqLength(char*, int, LABEL_PTYPE_TITLE);

//Function declarations
int HexToDec(int);
char *strrev(char *);
extern int Hex2Bin (unsigned char *, int, unsigned char *);
int Dec2Hex(unsigned int, char *, int);
int DecBin(char *, int);
void AsciiStringToHex(unsigned char* , unsigned char* , int );
//Extern Function Declaration

extern int isOfflineAllowed(char *);

/*
 * AjayS2: This API will convert any string to Int multiplied by 100.
 * 08 April 2016: JIRA- PTMX-1268, JIRA- PTMX-766, Amdocs Case 160304-23176
 * 				Elavon-EMV-9F02 does not have accurate value
 * 				SCA sending incorrect amount in 9F02 tag in SSI request
 * This issue was due to the precision lost while converting from String to Float.
 * e.g.,	float f = ((float) 1938) will be 1938.00
 * 		But	float f = ((float) 1938)/100 will not be 19.38, it will be 19.379999
 *
 * Modified the code to use integers always while sending amount tag to XPI.
 */
extern long int strToIntintoHundred(char *pszStr)
{
	long int	iAmtintoHundred = 0;
	int			index			= 0;
	int			iAfterDec		= 0;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	//Return 0 if No amount is Present
	if(strlen(pszStr) <= 0)
	{
		return iAmtintoHundred;
	}

	//Skip all starting 0s
	while(pszStr[index] == '0')
	{
		index++;
	}

	while(pszStr[index] != '.')
	{
		iAmtintoHundred = iAmtintoHundred*10 + (pszStr[index] - '0');
		index++;
	}
	index++;

	iAfterDec = ( ( (pszStr[index] - '0')* 10 ) + (pszStr[index + 1] - '0') );

	//fTmp = fTmp + ((float)iAfterDec)/100;
	iAmtintoHundred = 100*iAmtintoHundred + iAfterDec;

	debug_sprintf(szDbgMsg, "%s: Returning AmountIntoHundred [%ld] ", __FUNCTION__,  iAmtintoHundred);
	APP_TRACE(szDbgMsg);

	//return fTmp;
	return iAmtintoHundred;
}
#if 0
/*
 * AjayS2: This API will convert string to float
 * 			with only two decimals precision
 */
extern float strToFloat(char *pszStr)
{
	float		fTmp		= 0;
	int			index		= 0;
	int			iAfterDec	= 0;

	//Return 0 if No amount is Present
	if(strlen(pszStr) <= 0)
	{
		return fTmp;
	}

	//Skip all starting 0s
	while(pszStr[index] == '0')
	{
		index++;
	}

	while(pszStr[index] != '.')
	{
		fTmp = fTmp*10 + (pszStr[index] - '0');
		index++;
	}
	index++;
	iAfterDec = ( ( (pszStr[index] - '0')* 10 ) + (pszStr[index + 1] - '0') );
	fTmp = fTmp + ((float)iAfterDec)/100;

	return fTmp;
}
#endif
//extern int getTagValue(TAG_ENUM, char *, int *);
/*
 * ============================================================================
 * Function Name: getS_13Command
 *
 * Description	: This command is used to display a menu on the screen.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getS_13Command( char *pszCmd, int *len)
{
	char 				szTemp[256]			= "";
	int					iBytes				= 0;
    int 				length 				= 0;
	LABEL_STYPE_TITLE 	stLblTitle;	
	BTNLBL_STYPE		stBtnLbl;
	
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "S13 "); length +=4;
    memcpy(pszCmd, szTemp, length);

	/* Get the Select account type Prompt */
	fetchScrTitle(szTemp, SELECT_ACCOUNT_TITLE);
	
	memset(&stLblTitle, 0x00, sizeof(stLblTitle));
	//Split title string of required length(i.e Based on maxLabelLength defined for Mx915 or Mx925).
	splitStringOfReqLength(szTemp, giLabelLen-5, &stLblTitle);
	debug_sprintf(szDbgMsg, "%s: szTitle1 =%s, szTitle2 =%s, szTitle3 =%s",__FUNCTION__,
			stLblTitle.szTitle1, stLblTitle.szTitle2, stLblTitle.szTitle3);
	APP_TRACE(szDbgMsg);

	//Msg Line1 (upto 16 characters)
    memset(szTemp, 0x00, sizeof(szTemp));
    iBytes = sprintf(szTemp, "%s", stLblTitle.szTitle1);
    memcpy(pszCmd + length, szTemp, iBytes); length += iBytes;

	pszCmd[length] = FS; length += 1;	

	//Msg Line2 (Upto 16 characters)
    memset(szTemp, 0x00, sizeof(szTemp));
    iBytes = sprintf(szTemp, "%s", stLblTitle.szTitle2);
    memcpy(pszCmd + length, szTemp, iBytes); length += iBytes;

	pszCmd[length] = FS; length += 1;	

	//XPI supports only two msg lines in S13 command. So Ignoring third title if present also.
	
	/* Get the labels for cheque and saving accout type */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_23);
	
	//Choice1 (upto 16 characters)
    memset(szTemp, 0x00, sizeof(szTemp));
    iBytes = sprintf(szTemp, "%s", (char *)stBtnLbl.szLblOne);
    memcpy(pszCmd + length, szTemp, iBytes); length += iBytes;

	pszCmd[length] = FS; length += 1;	

	//Choice2 (Upto 16 characters)
    memset(szTemp, 0x00, sizeof(szTemp));
    iBytes = sprintf(szTemp, "%s", (char *)stBtnLbl.szLblTwo);
    memcpy(pszCmd + length, szTemp, iBytes); length += iBytes;

	pszCmd[length] = FS; length += 1;
	pszCmd[length] = FS; length += 1;
	
    *len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
}

/*
 * ============================================================================
 * Function Name: getS_14Command
 *
 * Description	: This command is used to display a message. For now we are using it show blank screen or to just
 *				  clear the previous XPI screen.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getS_14Command( char *pszCmd, int *len )
{
	char 				szTemp[256]			= "";
    int 				length 				= 0;
	//LABEL_STYPE_TITLE 	stLblTitle;	
	//BTNLBL_STYPE		stBtnLbl;
	
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "S14"); length +=3;
    memcpy(pszCmd, szTemp, length);
	
	//Note: Later If required use this function to show 4 line messages on screen.
	//For now we use this function as to clear off the previous XPI form and show blank screen.
	pszCmd[length] = FS; length += 1;
	
    *len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
}
/*
 * ============================================================================
 * Function Name: getD_41CmdforPayment
 *
 * Description	: This command is used to get the VAS DATA from the wallet present on phone
 *
 * Input Params	: pointer to a string cotaining request
 * 				  pointer to int conatins the length
 *				  ponter to a string containing VAS Mode
 *
 * Output Params: None
 * ============================================================================
 */
void getD_41CmdforPayment(char *pszCmd, int *length, char *szVASMode, char* szMerchIndex, char *szCustData)
{
	int  iLen 			= 0;
	int  iTempLen		= 0;
	char szTemp[64] 	= "";

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szTemp, 0x00, sizeof(szTemp));

	/*format of D41  :: <9F><A0><0C><len>TERM_CAP = 000X<FS>CUST_DATA=xxxxxxxxxxxxxxxxxxxx<FS>MERCHANT_INDEX=xxxxxxxxxx<FS>CMD_OPTS=000X*/

	sprintf(szTemp, "%c%c%c", 0x9F, 0xA0, 0x0C);
	strcpy(pszCmd, szTemp);
	iLen = iLen+4;

	memcpy(pszCmd + iLen, "TERM_CAP=", 9);
	iLen = iLen+9;

	memcpy(pszCmd + iLen, szVASMode, 4);
	iLen = iLen+4;

	//CUST_DATA, MERCH_INDEX are independent Fields
	if(szCustData != NULL && (strlen(szCustData) > 0))
	{
		memset(szTemp, 0x00, sizeof(szTemp));
		sprintf(szTemp, "%c", FS);
		memcpy(pszCmd + iLen, szTemp, 1);
		iLen = iLen + 1;

		memcpy(pszCmd + iLen, "CUST_DATA=", 0x0A);				//Customer Data
		iLen = iLen+10;

		iTempLen = strlen(szCustData);
		memcpy(pszCmd +iLen, szCustData, iTempLen);
		iLen = iLen + iTempLen;
	}
	if(szMerchIndex != NULL && (strlen(szMerchIndex) > 0))
	{
		memset(szTemp, 0x00, sizeof(szTemp));
		sprintf(szTemp, "%c", FS);
		memcpy(pszCmd + iLen, szTemp, 1);
		iLen = iLen + 1;

		memcpy(pszCmd + iLen, "MERCHANT_INDEX=", 0x0F);			//Merchant index
		iLen = iLen + 15;

		iTempLen = strlen(szMerchIndex);
		memcpy(pszCmd +iLen, szMerchIndex, iTempLen);
		iLen = iLen + iTempLen;
	}
	/*
	 * AjayS2: 20 July 2016: sending CMD_OPTS now as default 1 will be used for Provision Pass also from now onwards
	 */
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%c", FS);
	memcpy(pszCmd + iLen, szTemp, 1);
	iLen = iLen + 1;

	memcpy(pszCmd + iLen , "CMD_OPTS=", 9);					//Command Options
	iLen= iLen + 9;
	memcpy(pszCmd +iLen, "0001" , 4);
	iLen = iLen + 4;

	pszCmd[3] = iLen - 4;

	*length= iLen;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: getS_20Command
 *
 * Description	: This command would be used to get MSR card data.
 *
 * Input Params	: pointer to a string cotaining request
 * 				  pointer to int conatins the length
 *				  ponter to a string containing VAS Mode
 * Output Params: None
 * ============================================================================
 */
void getS_20Command( char *pszCmd, int *len, char* szVASMode, char* szMerchantIndex, char* szCustData)
{
	char 				szTemp[256]			= "";
	char  				szD41Cmd[128]       = "";          //size needs to be changed
	int					iTimeOut			= 0;
	int					iLen				= 0;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*Format of S20 command <STX>S20000<ETX>*/
	//000 = no timeout, infinite wait.

	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s%03d%c", "S20", iTimeOut, FS);
	strcat(pszCmd, szTemp);

	if (szVASMode != NULL && strlen(szVASMode) > 0)
	{
		memset(szTemp, 0x00, sizeof(szTemp));
		sprintf(szTemp, "%c", FS);
		strcat(pszCmd, szTemp);

		memset(szD41Cmd, 0x00, sizeof(szD41Cmd));
		getD_41CmdforPayment(szD41Cmd, &iLen, szVASMode, szMerchantIndex, szCustData);
		strcat(pszCmd, szD41Cmd);
	}

	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%c", FS);
	strcat(pszCmd, szTemp);

	*len = strlen(pszCmd);

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
}

/*
 * ============================================================================
 * Function Name: getS_95Command
 *
 * Description	: This function will generate S95 command to get XPI Version info.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getS_95Command(char *pszCmd, int *len)
{
	char szTemp[8 + 1];
    int length = 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "S95"); length +=3;
    memcpy(pszCmd, szTemp, length);

    *len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
}

/*
 * ============================================================================
 * Function Name: getS_66Command
 *
 * Description	: This command sends a string of data which will be MACed using the ANSI x9.19 
 *					standard and the MAC Working Key.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getS_66Command( char *pszCmd, int *len, char* szTranStkKey)
{
	char 		szTemp[256]			= "";
	int			iBytes				= 0;
    int 		length 				= 0;
	EMVDTLS_STYPE	stEmvDtls;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Get the current emv details from the transaction instance */
	memset(&stEmvDtls, 0x00, sizeof(EMVDTLS_STYPE));
	getEmvDtlsForPymtTran(szTranStkKey, &stEmvDtls);
		
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "S66"); length +=3;
    memcpy(pszCmd, szTemp, length);
	
	/*TODO: Once host string format for MAC data is known. Create same mac data and put it here later.
			For now using some hardcoded value.*/
    memset(szTemp, 0x00, sizeof(szTemp));
    iBytes = sprintf(szTemp, "%s", "4567");
    memcpy(pszCmd + length, szTemp, iBytes); length += iBytes;
	
    *len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
}

/*
 * ============================================================================
 * Function Name: getM_40Command
 *
 * Description	: This function will generate M40 command.
 * 				  This command can be used for reading the value of parameters from iab section.
 *
 * Input Params	: parameter to read
 *
 * Output Params: Command
 * ============================================================================
 */
void getM_40Command( char *pszCmd, int *len, char *szParameter)
{
	char 		szTemp[256]			= "";
	int			iBytes				= 0;
    int 		length 				= 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "M40"); length +=3;
    memcpy(pszCmd, szTemp, length);

    memset(szTemp, 0x00, sizeof(szTemp));
    iBytes = sprintf(szTemp, "%c%s%c", FS, szParameter, FS);
    memcpy(pszCmd + length, szTemp, iBytes); length += iBytes;

    *len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getM_41Command
 *
 * Description	: This function will generate M41 command.
 * 				  This command can be used for setting  the value of parameters from iab section.
 *
 * Input Params	: parameter to set the value
 *
 * Output Params: Command
 * ============================================================================
 */
void getM_41Command( char *pszCmd, int *len, char *szParameter, char *szValue)
{
	char 		szTemp[256]			= "";
	int			iBytes				= 0;
    int 		length 				= 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "M41"); length +=3;
    memcpy(pszCmd, szTemp, length);

    memset(szTemp, 0x00, sizeof(szTemp));
    iBytes = sprintf(szTemp, "%c[iab]%s=%s", FS, szParameter, szValue);
    memcpy(pszCmd + length, szTemp, iBytes); length += iBytes;

    *len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getC_20Command
 *
 * Description	: This function will generate C20 command. 
 *				  This command is used to get tag information from the inserted card.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getC_20Command(char *pszCmd, int *len, char *szEmvTags, int iLen)
{
	char szTemp[256]		= "";
    int length			 	= 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
   	
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "C20"); length +=3;
    memcpy(pszCmd, szTemp, length);

	//Number of tags
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%02d", 1);	// No.of tags=1
	memcpy(pszCmd+length, szTemp, 2); length +=2; 

	//Tag
	memcpy(pszCmd+length, szEmvTags, iLen); length +=iLen;
	
    *len = length;
	
	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
}

/*
 * ============================================================================
 * Function Name: getC_30Command
 *
 * Description	: This function will generate C30 command.
 *
 * Input Params	:  pointer to a string cotaining request
 * 				  pointer to int conatins the length
 *				  pointer to a string cotaining transation key
 *				  ponter to a string containing VAS Mode
 *
 * Output Params: Command
 * ============================================================================
 */
int getC_30Command(char *pszCmd, int *len, char* szTranStkKey, char* szVASMode, char* szMerchantIndex, char* szCustData)
{
	char 				szTemp[256]				= "";
	char 				szTotal[16]				= "";
	char  				szD41Cmd[128]           = "";          //size needs to be changed
	char 				szHexTotal[16]			= "";
	int 				length 					= 0;
	int 				iTotal					= 0;
	long int			iAmt					= 0;
	int					iFxn					= 0;
	int					iCmd					= 0;
	int					iLen					= 0;
	char				iTxntype				= 0;
	char				iCTLSTxnType			= 0;
	char				cTimeOut				= 0;
    struct tm 			*loctime				= NULL;
	time_t 				curtime;
	AMTDTLS_STYPE		stAmtDtls;
	GET_CARDDATA_STYPE	stGetCardData;
	PAAS_BOOL			bSendZeroAmt = PAAS_FALSE;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif		

	debug_sprintf(szDbgMsg, "%s: ---Enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//Get function and command
	getFxnNCmdForSSITran(szTranStkKey, &iFxn, &iCmd);

/*
     *  Preswipe Case:
     *  Not sending the amount when the transaction amount is 0
     *  so contactless will be disabled.
     *  */
    memset(&stAmtDtls, 0x00, sizeof(AMTDTLS_STYPE));
	memset(&stGetCardData, 0x00, sizeof(GET_CARDDATA_STYPE));

	if((getAmtDtlsForPymtTran(szTranStkKey, &stAmtDtls)) != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while fetching the Amount details!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		//return FAILURE;
	}
    if(strlen(stAmtDtls.tranAmt) > 0)
    {
    	cTimeOut = 0xFF;
    }

    memset(szTemp, 0x00, sizeof(szTemp));
    // Get the current time.
    curtime = time (NULL);	

    strcat(szTemp, "C30\0"); length +=5;

    memcpy(pszCmd, szTemp, length);
    memset(szTemp, 0x00, sizeof(szTemp));
    loctime = localtime (&curtime);
    sprintf(szTemp, "%c%c%c%c%c%c", 0xC1, 1, cTimeOut, 0xC1, 1, 1);// Timeout and terminal record
    memcpy(pszCmd + length, szTemp, 6);
    length += 6;

    // Date and time
    memset(szTemp, 0x00, sizeof(szTemp));	
    sprintf(szTemp, "%c%c%c%c%c%c%c%c%c%c", 0xC1, 3, HexToDec(loctime->tm_year % 100), 
			HexToDec(loctime->tm_mon + 1), HexToDec(loctime->tm_mday),
			0xC1, 3, HexToDec(loctime->tm_hour), HexToDec(loctime->tm_min), HexToDec(loctime->tm_sec));

    memcpy(pszCmd + length, szTemp, 10);
    length +=10;

	/* Note: Possible values for EMV transaction type are
		- '00' for a purchase transaction
		- '01' for a cash advance transaction
		- '09' for a purchase with cashback
		- '20' for a refund transaction
	*/
	if((iFxn == SSI_PYMT) && (iCmd == SSI_CREDIT))
	{
		//Refund transaction
		iTxntype 	 = 0x20;
		iCTLSTxnType = 0x20;
	}

    memset(szTemp, 0x00, sizeof(szTemp));	
    sprintf(szTemp, "%c%c%c%c%c%c%c%c", 0xC1, 1, iTxntype, 0xC1, 1, 0x31, 0xC1, 0);
    memcpy(pszCmd + length, szTemp, 8);
    length +=8;

	if (szVASMode != NULL && strlen(szVASMode) > 0)
	{
		memset(szD41Cmd, 0x00, sizeof(szD41Cmd));
		getD_41CmdforPayment(szD41Cmd, &iLen, szVASMode, szMerchantIndex, szCustData);
		//AjayS2: 16 Feb 2016: We have to send amount tag as 0 only if emvcontactless is enabled.
		if(strlen(stAmtDtls.tranAmt) == 0 && isContactlessEmvEnabledInDevice())
		{
			/*for preswipe case to enable contactless we are sending amount as 0(NULL)
			 *(it will work only for wallet for emv contactless it will give bad card reader error)
			 */
			bSendZeroAmt = PAAS_TRUE;
		}
	}
    if((iCmd == SSI_TOKENQUERY) && isContactlessEmvEnabledInDevice())
    {

    	/* Daivik:25/5/16 - For Token Query if EMV is enabled we are now sending C30 command. And if any of the contactless interfaces ( EMV/MSD) needs to be enabled,
    	 * then we need to send zero amount in the C30 request.
    	 */
    	bSendZeroAmt = PAAS_TRUE;
    }
	if(bSendZeroAmt == PAAS_TRUE)
	{
		 memset(szTemp, 0x00, sizeof(szTemp));
		 sprintf(szTemp, "%c%c%c", 0xC1, 1, 0X00);
		 memcpy(pszCmd + length, szTemp, 3);
		 length +=3;
	}
	if(getCardDataDtlsForDevTran(szTranStkKey, &stGetCardData) == SUCCESS)
	{
		if(stGetCardData.tranAmt == PAAS_TRUE)
		{
			memset(szTemp, 0x00, sizeof(szTemp));
			sprintf(szTemp, "%c%c%c", 0xC1, 1, 0X00);
			memcpy(pszCmd + length, szTemp, 3);
			length +=3;
		}
	}
    if(strlen(stAmtDtls.tranAmt) > 0)
    {
    	/*Multiplying by 100 to get it to the format req by EMV
    	 * Example 10.25 is written as 1025
    	 */
    	//iAmt 	= 	atof(stAmtDtls.tranAmt)*100;
    	//iAmt = strToIntintoHundred(stAmtDtls.tranAmt)*100;
    	iAmt = strToIntintoHundred(stAmtDtls.tranAmt);
    	sprintf(szTotal, "%ld", iAmt);

    	//Copying amtdtls for using on 2nd C30
    	strcpy(stAmtDtls.tranAmtFirstC30, stAmtDtls.tranAmt);
    	/* 26/8/16 Fix Coverity : 83360 */
    	if(updateAmtDtlsForPymtTran(szTranStkKey, &stAmtDtls) != SUCCESS)
    	{
    		debug_sprintf(szDbgMsg, "%s: Error while updating the Amount details!!!", __FUNCTION__);
    		APP_TRACE(szDbgMsg);
    	}

    	debug_sprintf(szDbgMsg, "%s: Tran amt: %s", __FUNCTION__, szTotal);
    	APP_TRACE(szDbgMsg);

    	memset(szTemp, 0x00, sizeof(szTemp));
    	memset(szHexTotal, 0x00, sizeof(szHexTotal));
		// CID-67353, 67207: 25-Jan-16: MukeshS3: Need to handle the error value returned from getHexTotal,
		// otherwise it may result pszCmd buffer overflow while using memcpy for iRet=-1 size.
    	//iTotal = getHexTotal(szTotal, szHexTotal);
    	if((iTotal = getHexTotal(szTotal, szHexTotal)) == -1)
    	{
    		return FAILURE;
    	}

    	sprintf(szTemp, "%c%c", 0xC1, iTotal);
    	memcpy(szTemp+2, szHexTotal, iTotal);
    	memcpy(pszCmd + length, szTemp, iTotal + 2); length = iTotal + length + 2;

    	memset(szTemp, 0x00, sizeof(szTemp));
    	memset(szHexTotal, 0x00, sizeof(szHexTotal));
    	memset(szTotal, 0x00, sizeof(szTotal));

    	/* Multiplying by 100 to get it to the format req by EMV
    	 * Example 10.25 is written as 1025
    	 */
    	//iAmt 	= atof(stAmtDtls.cashBackAmt)*100;
    	//iAmt 	= strToIntintoHundred(stAmtDtls.cashBackAmt)*100;
    	iAmt 	= strToIntintoHundred(stAmtDtls.cashBackAmt);
    	sprintf(szTotal, "%ld", iAmt);

    	debug_sprintf(szDbgMsg, "%s: Cash back:%s", __FUNCTION__, szTotal);
    	APP_TRACE(szDbgMsg);

		// CID-67353: 25-Jan-16: MukeshS3: Need to handle the error value returned from getHexTotal,
		// otherwise it may result pszCmd buffer overflow while using memcpy for iRet=-1 size.
    	//iTotal = getHexTotal(szTotal, szHexTotal);
    	if((iTotal = getHexTotal(szTotal, szHexTotal)) == -1)
    	{
    		return FAILURE;
    	}

    	sprintf(szTemp, "%c%c", 0xC1, iTotal);
    	memcpy(szTemp+2, szHexTotal, iTotal);
    	memcpy(pszCmd + length, szTemp, iTotal + 2); length = iTotal + length + 2;

    	//Contactless Transaction Type - 20 = Refund, 00 (or empty) = Sale, FF = disable CTLS
    	memset(szTemp, 0x00, sizeof(szTemp));
    	sprintf(szTemp, "%c%c%c", 0xC1, 1, iCTLSTxnType);
    	memcpy(pszCmd + length, szTemp, 3);
    	length +=3;
    }

	if (szVASMode != NULL && strlen(szVASMode) > 0)
	{
		memcpy(pszCmd + length, szD41Cmd, iLen);
		length += iLen;
	}

    pszCmd[4] 	   = length - 5;

    *len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return---", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: getD_41Command
 *
 * Description	: This function will generate D41 command.
 * 				  This command can be used for apple pay (getting VAS data)
 *
 * Input Params	: parameter to set the value
 *
 * Output Params: NONE
 * ============================================================================
 */
void getD_41Command( char *pszCmd, int *len, PAAS_BOOL bProvisionPass, char* pszMerchIndex, char *pszCustData)
{
	char szTmp[128]	= "";

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*format of D41 is <STX>D41<ETX>*/

	strcat(pszCmd, "D41");

	*len = 3;

	//AjayS2: 13-Jan-2016
	//FRD5.0 Phase 2 - Offers & Loyalty (Supporting Multiple Merchant Index)
	//We can have merchant index only in start session command, so that only that particular VAS should be acceppted
	if(bProvisionPass)
	{
		if(pszCustData != NULL && (strlen(pszCustData) > 0))
		{
			memset(szTmp, 0x00, sizeof(szTmp));
			sprintf(szTmp, "%c%s%s", FS, "CUST_DATA=", pszCustData);
			strcat(pszCmd, szTmp);
			*len += strlen(szTmp);
		}
		if(pszMerchIndex != NULL && (strlen(pszMerchIndex) > 0))
		{
			memset(szTmp, 0x00, sizeof(szTmp));
			sprintf(szTmp, "%c%s%s", FS, "MERCHANT_INDEX=", pszMerchIndex);
			strcat(pszCmd, szTmp);
			*len += strlen(szTmp);
		}

		/*
		 * AjayS2: 20 July 2016: sending CMD_OPTS now as default 1 will be used for Provision Pass also from now onwards
		 */
		memset(szTmp, 0x00, sizeof(szTmp));
		sprintf(szTmp, "%c%s%s", FS, "CMD_OPTS=", "0001");					//Command Options  Hard Coded to one 0001 - Get from Phone
		strcat(pszCmd, szTmp);
		*len += strlen(szTmp);
	}
	else
	{
		if(pszMerchIndex != NULL && (strlen(pszMerchIndex) > 0) )
		{
			memset(szTmp, 0x00, sizeof(szTmp));
			sprintf(szTmp, "%c%s%s", FS, "MERCHANT_INDEX=", pszMerchIndex);
			strcat(pszCmd, szTmp);
			*len += strlen(szTmp);
		}
	}
	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getU_00Command
 *
 * Description	: This function will generate U00 command.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
int getU_00Command( char *pszCmd, int *len, char* szTranStkKey)
{
	char 				szTemp[256]				= "";
	char 				szTotal[16]				= "";
	char 				szHexTotal[16]			= "";
    int 				length 					= 0;
	int					iTxntype				= 0;
	int					iAmt					= 0;
	int					iFxn					= 0;
	int					iCmd					= 0;
	int					iTotal					= 0;
	AMTDTLS_STYPE		stAmtDtls;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//Get function and command
	getFxnNCmdForSSITran(szTranStkKey, &iFxn, &iCmd);

    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "U00\0"); length +=5;
    memcpy(pszCmd, szTemp, length);

    /* Note: Possible values for EMV transaction type are
		- '00' for a purchase transaction
		- '01' for a cash advance transaction
		- '09' for a purchase with cashback
		- '20' for a refund transaction
	*/
	if((iFxn == SSI_PYMT) && (iCmd == SSI_CREDIT))
	{
		//Refund transaction
		iTxntype = 20;
	}

    memset(szTemp, 0x00, sizeof(szTemp));
    sprintf(szTemp, "%c%c%c", 0xC1, 1, iTxntype);
    memcpy(pszCmd + length, szTemp, 3);
    length +=3;

    memset(&stAmtDtls, 0x00, sizeof(AMTDTLS_STYPE));
    if(getAmtDtlsForPymtTran(szTranStkKey, &stAmtDtls) != SUCCESS) //CID 67357 (#1 of 1): Unchecked return value (CHECKED_RETURN) T_RaghavendranR1.
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get Amount details", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

    /*Multiplying by 100 to get it to the format req by EMV
     * Example 10.25 is written as 1025
     */
    iAmt 	= 	atof(stAmtDtls.tranAmt)*100;
	sprintf(szTotal, "%d", iAmt);

    memset(szTemp, 0x00, sizeof(szTemp));
    memset(szHexTotal, 0x00, sizeof(szHexTotal));
	// CID-67238, 67195,67194: 25-Jan-16: MukeshS3: Need to handle the error value returned from Hex2Bin,
	// otherwise it may result pszCmd buffer overflow while using memcpy for iRet=-1 size.
    //iTotal = getHexTotal(szTotal, szHexTotal);
    if((iTotal = getHexTotal(szTotal, szHexTotal)) == -1)
    {
    	return FAILURE;
    }
    sprintf(szTemp, "%c%c", 0xC1, iTotal);
    memcpy(szTemp+2, szHexTotal, iTotal);
    memcpy(pszCmd + length, szTemp, iTotal + 2); length = iTotal + length + 2;

    memset(szTemp, 0x00, sizeof(szTemp));
    memset(szHexTotal, 0x00, sizeof(szHexTotal));
    memset(szTotal, 0x00, sizeof(szTotal));

    /*Multiplying by 100 to get it to the format req by EMV
     * Example 10.25 is written as 1025
     */
    iAmt 	= (atof(stAmtDtls.cashBackAmt) + atof(stAmtDtls.tipAmt))*100;
	sprintf(szTotal, "%d", iAmt);

	debug_sprintf(szDbgMsg, "%s: Cash back:%s TipAmount: %s Total: %d", __FUNCTION__, stAmtDtls.cashBackAmt, stAmtDtls.tipAmt, iAmt);
	APP_TRACE(szDbgMsg);
	// CID-67238, 67195,67194, 67431: 25-Jan-16: MukeshS3: Need to handle the error value returned from Hex2Bin,
		// otherwise it may result pszCmd buffer overflow while using memcpy for iRet=-1 size.
    if((iTotal = getHexTotal(szTotal, szHexTotal)) == -1)
    {
    	return FAILURE;
    }
    sprintf(szTemp, "%c%c", 0xC1, iTotal);
    memcpy(szTemp+2, szHexTotal, iTotal);
    memcpy(pszCmd + length, szTemp, iTotal + 2); length = iTotal + length + 2;

    pszCmd[4] = length - 5;

    *len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: getU_01Command
 *
 * Description	: This function will generate U01 command based on Flag
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
int getU_01Command( char *pszCmd, int *len, AID_LIST_INFO_PTYPE pstAIDListInfo, PAAS_BOOL bIsAIDChosen)
{
	char 				szTemp[256]				= "";
	char 				szAIDData[128]			= "";
	char 				szTermCapData[128]		= "";
	int					iAIDLen					= 0;
	int					iTermCapLen				= 0;
	int					iLen					= 0;
    int 				length 					= 0;
	//AID_NAME_NODE_PTYPE	pstAIDNameNode			= NULL;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter--- Is AID Already Selected: %s", __FUNCTION__, (bIsAIDChosen == PAAS_TRUE)?"YES":"NO");
	APP_TRACE(szDbgMsg);

	memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "U010"); length +=4;
    memcpy(pszCmd, szTemp, length);

    if(bIsAIDChosen == PAAS_FALSE)
    {
#if 0
    	pstAIDNameNode = pstAIDListInfo->LstHead;

    	while(pstAIDNameNode != NULL)
    	{
    		memset(szAIDData, 0x00, sizeof(szAIDData));
    		memset(szTemp, 0x00, sizeof(szTemp));
    		iAIDLen = Hex2Bin((unsigned char *)pstAIDNameNode->szAIDName, strlen(pstAIDNameNode->szAIDName)/2, (unsigned char *)szTemp);
    		sprintf(szAIDData, "%c%c", 0x4F, iAIDLen);
    		memcpy((char *)szAIDData + 2, szTemp, iAIDLen);

    		iLen = iAIDLen + 2;
    		//Add here if need to add more tags(9F33 etc.) in the U01 Response other than 4F

    		memset(szTemp, 0x00, sizeof(szTemp));
    		sprintf(szTemp, "%c%c", 0xC1, iLen);
    		memcpy((char *)szTemp + 2, szAIDData, iLen);
    		memcpy(pszCmd + length, szTemp, iLen + 2);    	length = length + iLen + 2;

    		pstAIDNameNode = pstAIDNameNode->next;
    	}
#endif
    }
    else
    {
    	if(strlen(pstAIDListInfo->szAIDSelected) <= 0)
    	{
    		debug_sprintf(szDbgMsg, "%s: Selected AID and/Or Terminal Capabilities NOT Present", __FUNCTION__);
    		APP_TRACE(szDbgMsg);
    		return FAILURE;
    	}

		memset(szAIDData, 0x00, sizeof(szAIDData));
		memset(szTemp, 0x00, sizeof(szTemp));
	    // CID-67382, 67304: 25-Jan-16: MukeshS3: Need to handle the error case also, otherwise it may result szAIDData buffer overflow while using memcpy for iRet=-1 size
		//iAIDLen = Hex2Bin((unsigned char *)pstAIDListInfo->szAIDSelected, strlen(pstAIDListInfo->szAIDSelected)/2, (unsigned char *)szTemp);
		if((iAIDLen = Hex2Bin((unsigned char *)pstAIDListInfo->szAIDSelected, strlen(pstAIDListInfo->szAIDSelected)/2, (unsigned char *)szTemp)) == -1)
		{
			return FAILURE;
		}
		sprintf(szAIDData, "%c%c", 0x4F, iAIDLen);
		memcpy((char *)szAIDData + 2, szTemp, iAIDLen);

		iLen = iAIDLen + 2;
		if(strlen(pstAIDListInfo->szTermCap) > 0)
		{
			memset(szTermCapData, 0x00, sizeof(szTermCapData));
			memset(szTemp, 0x00, sizeof(szTemp));
			// CID-67313: 25-Jan-16: MukeshS3: Need to handle the error value returned from Hex2Bin,
			// otherwise it may result pszCmd buffer overflow while using memcpy for iRet=-1 size.
			//iTermCapLen = Hex2Bin((unsigned char *)pstAIDListInfo->szTermCap, strlen(pstAIDListInfo->szTermCap)/2, (unsigned char *)szTemp);
			if((iTermCapLen = Hex2Bin((unsigned char *)pstAIDListInfo->szTermCap, strlen(pstAIDListInfo->szTermCap)/2, (unsigned char *)szTemp)) == -1)
			{
				return FAILURE;
			}
			sprintf(szTermCapData, "%c%c%c", 0x9F, 0x33, iTermCapLen);
			memcpy((char *)szTermCapData + 3, szTemp, iTermCapLen);

			memcpy((char *)szAIDData + iLen, szTermCapData, iTermCapLen + 3);
			iLen = iLen + iTermCapLen + 3;
		}

		memset(szTemp, 0x00, sizeof(szTemp));
		sprintf(szTemp, "%c%c", 0xC1, iLen);
		memcpy((char *)szTemp + 2, szAIDData, iLen);
		memcpy(pszCmd + length, szTemp, iLen + 2);    	length = length + iLen + 2;
    }
    pszCmd[3] = length - 4;

    *len = length;

    debug_sprintf(szDbgMsg, "%s: ---Return---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: getE_02Command
 *
 * Description	: This function would build command to retrieve EParams data.
 *
 * Input Params	:
 *
 * Output Params: Command
 * ============================================================================
 */
void getE_02Command( char *pszCmd,  int *len)
{
	char 		szTemp[256]			= "";
    int			length 				= 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "E02"); length +=3;
    memcpy(pszCmd, szTemp, length);

	//Copy length to output length placeholder
	*len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: getE_06Command
 *
 * Description	: This function would build message to get PKI ciphered data.
 *
 * Input Params	:
 *
 * Output Params: Command
 * ============================================================================
 */
void getE_06Command( char *pszCmd,  int *len)
{
	char 		szTemp[256]			= "";
    int			length 				= 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "E06"); length +=3;
    memcpy(pszCmd, szTemp, length);

	//Copy length to output length placeholder
	*len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: getE_10Command
 *
 * Description	: This function would build command to set encryption mode in XPI application.
 *
 * Input Params	:
 *
 * Output Params: Command
 * ============================================================================
 */
void getE_10Command( char *pszCmd,  int *len)
{
	char 		szTemp[256]			= "";
    int			length 				= 0;
    ENC_TYPE 	eEncType;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	eEncType = getEncryptionType();
    memset(szTemp, 0x00, sizeof(szTemp));

    if( eEncType == VSP_ENC )
    {
    	strcat(szTemp, "E10VSP");
    }
    else if( eEncType == RSA_ENC )
    {
    	strcat(szTemp, "E10PKI");
    }
    else if( eEncType == VSD_ENC )
    {
    	strcat(szTemp, "E10VSD");
    }
    else
    {
    	strcat(szTemp, "E10NOE");
    }
    length +=6;
    memcpy(pszCmd, szTemp, length);

	//Copy length to output length placeholder
	*len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getC_19Command
 *
 * Description	: This function will generate C20 command.
 *				  This command is used to set a tag information in the XPI
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getC_19Command(char *pszCmd,  int *len, char *szEmvTLV, int iEMVLen)
{
	char szTemp[256]		= "";
    int length			 	= 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "C19"); length +=3;
    memcpy(pszCmd, szTemp, length);

	//Value
    memcpy(pszCmd+length, szEmvTLV, iEMVLen); length += iEMVLen;

    *len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getC_32Command
 *
 * Description	: This function will generate C32 command.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
int getC_32Command( char *pszCmd, int *len, char* szTranStkKey)
{
	char 				szTemp[256]				= "";
	char 				szTotal[16]				= "";
	char 				szHexTotal[16]			= "";
    int 				length 					= 0;
    int					rv						= SUCCESS;
	int					iRet					= 0;
    int 				iTotal					= 0;	
    long int			iAmt					= 0;
	int 				iMerchantDecision 		= 0; 
	int					iFxn					= 0;
	int					iCmd					= 0;
	char *				szTmp					= NULL;
	char				tag[256]				= "";
	char				tagsDefault[256]		= "82959A9C5F245F2A5F349F029F039F099F1A9F1E9F269F279F339F349F359F369F379F399F419F53849F109F6E9F219F069F079F0D9F0E9F0F9F08";
    CARDDTLS_STYPE		stCardDtls;
	AMTDTLS_STYPE		stAmtDtls;
	DCCDTLS_STYPE		stDCCDtls;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif	
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
	memset(&stAmtDtls, 0x00, sizeof(AMTDTLS_STYPE));

	if((getAmtDtlsForPymtTran(szTranStkKey, &stAmtDtls)) != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while fetching the Amount details!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		//return FAILURE;
	}

	if((getDccDtlsForPymtTran(szTranStkKey, &stDCCDtls)) != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while fetching the DCC details!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		//return FAILURE;
	}
	//Get function and command
	getFxnNCmdForSSITran(szTranStkKey, &iFxn, &iCmd);
	if(getCardDtlsForPymtTran(szTranStkKey, &stCardDtls) != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get card details", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	
	/* Note: Possible values for EMV transaction type are
			- '00' for a purchase transaction
			- '01' for a cash advance transaction
			- '09' for a purchase with cashback
			- '20' for a refund transaction
	 */
	if(atof(stAmtDtls.cashBackAmt) > 0)
	{
		/* Need to send change the transaction type now to purchase card transaction sale
		 * So need to send C19 command
		 */
		debug_sprintf(szDbgMsg, "%s: Changing the transaction type to Purchase Sale Cash Back", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = updateEMVtaginXPI(EMV_C32, "009C00109");
		if(rv != SUCCESS)
		{
			/*TODO: Check what to do if it fails*/
			debug_sprintf(szDbgMsg, "%s: ERROR: Unable to Update EMV cash back transaction type 9C", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else if((iFxn == SSI_PYMT) && (iCmd == SSI_CREDIT))
	{
		/* Need to send change the transaction type now to refund
				 * So need to send C19 command
		*/
		rv = updateEMVtaginXPI(EMV_C32, "009C00120");
	}


    /*Multiplying by 100 to get it to the format req by EMV
     * Example 10.25 is written as 1025
     */
    //iAmt 	= 	atof(stAmtDtls.tranAmt)*100;
	//iAmt 	= 	strToIntintoHundred(stAmtDtls.tranAmt)*100;
	if(stDCCDtls.iDCCInd == DCC_ELIGIBLE_USER_OPTED)
	{
		iAmt = atol(stDCCDtls.szFgnAmount);
	}
	else
	{
		iAmt 	= 	strToIntintoHundred(stAmtDtls.tranAmt);
	}
	sprintf(szTotal, "%ld", iAmt);
	
	debug_sprintf(szDbgMsg, "%s: Total = [%s]", __FUNCTION__, szTotal);
	APP_TRACE(szDbgMsg);
	
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "C32\0"); length +=5;
    memcpy(pszCmd, szTemp, length);

    memset(szTemp, 0x00, sizeof(szTemp));
    memset(szHexTotal, 0x00, sizeof(szHexTotal));
    // CID-67319: 25-Jan-16: MukeshS3: Need to handle the error case also, otherwise it may result pszCmd buffer overflow while using memcpy for iRet=-1 size
    //iTotal = getHexTotal(szTotal, szHexTotal);
    if((iTotal = getHexTotal(szTotal, szHexTotal)) == -1)
    {
    	return FAILURE;
    }
    sprintf(szTemp, "%c%c", 0xC1, iTotal);
    memcpy(szTemp+2, szHexTotal, iTotal);
    memcpy(pszCmd + length, szTemp, iTotal + 2); length = iTotal + length + 2;

    memset(szTemp, 0x00, sizeof(szTemp));
    memset(szHexTotal, 0x00, sizeof(szHexTotal));
    memset(szTotal, 0x00, sizeof(szTotal));
    
    /*Multiplying by 100 to get it to the format req by EMV
     * Example 10.25 is written as 1025
     */
    //iAmt 	= atof(stAmtDtls.cashBackAmt)*100;
    //iAmt 	= strToIntintoHundred(stAmtDtls.cashBackAmt)*100;
    iAmt 	= strToIntintoHundred(stAmtDtls.cashBackAmt);
	sprintf(szTotal, "%ld", iAmt);
	
	debug_sprintf(szDbgMsg, "%s: Cash back:%s ", __FUNCTION__, szTotal);
	APP_TRACE(szDbgMsg);

	// CID-67285, 67435, 67319: (#1 of 1): Argument cannot be negative (NEGATIVE_RETURNS), T_RaghavendranR1: Need to handle the error value returned from getHexTotal,
	// otherwise it may result pszCmd buffer overflow while using memcpy for iRet=-1 size.
	if((iTotal = getHexTotal(szTotal, szHexTotal)) == -1)
    {
    	return FAILURE;
    }
    sprintf(szTemp, "%c%c", 0xC1, iTotal);
    memcpy(szTemp+2, szHexTotal, iTotal);
    memcpy(pszCmd + length, szTemp, iTotal + 2); length = iTotal + length + 2;

    //Note: As per the requirement, We are not setting force decline flag for refund transactions.
#if 0
    //Set merchant decision force decline for refund and void transactions.
	if( (iFxn == SSI_PYMT) &&  ( iCmd == SSI_CREDIT || iCmd == SSI_VOID) )
	{
		//Force decline(AAC)
		iMerchantDecision = 2;
	}
#endif

	if( (iFxn == SSI_PYMT) &&  ( iCmd == SSI_CREDIT || iCmd == SSI_PREAUTH) )
	{
		//Force online; All PRE_AUTH transactions should go online.
		iMerchantDecision = 1;
	}

	/*Daivik:26/5/2016 - For TOKEN QUERY when we are sending the C32 Request (1st gen AC)
	 *we need to request for AAC because we are treating this similar to Partial EMV
     */
	if((isPartialEmvAllowed() && stCardDtls.bPartialEMV) || (iCmd == SSI_TOKENQUERY))
	{
		iMerchantDecision = 2;
		debug_sprintf(szDbgMsg, "%s: Partial EMV Forcing Decline Offline", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/*Acquirer Record and Merchant Decision*/
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%c%c%c%c%c%c%c%c%c", 0xC1, 1, 1, 0xC1, 1, 0,0xC1, 1, iMerchantDecision);
	memcpy(pszCmd + length, szTemp, 9); length += 9;

    memset(tag, 0x00, 256);
    szTmp = getEMVTagsReqByHost();
    if(strlen(szTmp) > 0)
    {
    	strcpy(tag, szTmp);
    }
    else
    {
    	strcpy(tag, tagsDefault);
    }
    debug_sprintf(szDbgMsg, "%s: EmvTgas req By Host [%s]", __FUNCTION__, tag);
    APP_TRACE(szDbgMsg);

   	memset(szTemp, 0x00, sizeof(szTemp));
   	sprintf(szTemp, "%c%c", 0xE1, strlen(tag)/2);
    memcpy(pszCmd + length, szTemp, 2); length += 2;

    memset(szTemp, 0x00, 256);

    // CID-67435: 25-Jan-16: MukeshS3: Need to handle the error case also, otherwise it may result pszCmd buffer overflow while using memcpy for iRet=-1 size
    //iRet = Hex2Bin((unsigned char *)tag, strlen(tag)/2, (unsigned char *)szTemp);
    if((iRet = Hex2Bin((unsigned char *)tag, strlen(tag)/2, (unsigned char *)szTemp)) == -1)
    {
    	return FAILURE;
    }
    memcpy(pszCmd + length, szTemp, iRet); length += iRet;


    pszCmd[4] = length - 5;
    *len = length;
	
	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
	return SUCCESS;
}
/*
 * ============================================================================
 * Function Name: getC_34Command
 *
 * Description	: This function will generate C34 command.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
int getC_34Command( char *pszCmd, int *len, char *szTranStkKey, int iResultCode, CARDDTLS_PTYPE pstCardDtls)
{
	char 						szTemp[2048]		= "";
	int							iAppLogEnabled		= isAppLogEnabled();
	char						szAppLogData[300]	= "";
	int 						length 				= 0;
	int							iE1Len				= 2;//Should be two by default
	int							rv					= SUCCESS;
	int							iTmp				= 0;
	int							iAuthStatus 		= 0;
	int							iOfflineAllowed		= -1;
	EMV_RESP_DTLS_STYPE			stEmvRespDtls;	

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif	
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "C34\0"); length +=5;
    memcpy(pszCmd, szTemp, length);

	memset(&stEmvRespDtls, 0x00, sizeof(EMV_RESP_DTLS_STYPE));
    rv = getEmvRespDtlsForPymtTran(szTranStkKey, &stEmvRespDtls);
    if(rv != SUCCESS)
    {
    	debug_sprintf(szDbgMsg, "%s: Error while getting stEmvRespDtls", __FUNCTION__);
    	APP_TRACE(szDbgMsg);
    	return FAILURE;
    }

    if(pstCardDtls == NULL)
    {
    	debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
    	APP_TRACE(szDbgMsg);
    	return FAILURE;
    }

	debug_sprintf(szDbgMsg, "%s: HOST result %d", __FUNCTION__, iResultCode);
	APP_TRACE(szDbgMsg);

	if(iResultCode == SUCCESS_PWC_RSLTCODE_CAPTURED || iResultCode == SUCCESS_PWC_RSLTCODE_APPROVED)
	{
		//Host Approved
		iAuthStatus = 0;
	}
	else if(iResultCode == ERR_CONN_TO || iResultCode == ERR_CONN_FAIL || iResultCode == ERR_HOST_NOT_AVAILABLE ||
			iResultCode == ERR_DECRYPTION_HOST_TIMEOUT || iResultCode == ERR_DECRYPTION_HOST_CONN_ERROR ||
			checkResultCodeinSAFErrCodeList(iResultCode)/*If Result Code is part of configured SAF Error Codes then treat it as Host offline*/)
	{
		//Host Offline
		iAuthStatus = 2;
		iOfflineAllowed = isOfflineAllowed(szTranStkKey);

		pstCardDtls->stEmvtranDlts.iOfflineAllowed = iOfflineAllowed;
		if(iOfflineAllowed == SUCCESS)
		{
			strcpy(pstCardDtls->stEmvtranDlts.szAuthCodeForC34, "Y3");
		}
		else
		{
			strcpy(pstCardDtls->stEmvtranDlts.szAuthCodeForC34, "Z3");
		}

		//AjayS2: taking Backup of the 1st Gen AC parameters, so that if req we can send them
		strcpy(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoDataPrev, pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData);
		//strcpy(pstCardDtls->stEmvtranDlts.szTranStatusInfoPrev, pstCardDtls->stEmvtranDlts.szTranStatusInfo);
		strcpy(pstCardDtls->stEmvTerDtls.szTermVerificationResultsPrev, pstCardDtls->stEmvTerDtls.szTermVerificationResults);
	}
	else if(iResultCode == ERR_PWC_RSLTCODE_DECLINED)
	{
		//Host Declined
		iAuthStatus = 1;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Unknown ERROR:: iResultCode [%d]: Sending Decline to Card", __FUNCTION__, iResultCode);
		APP_TRACE(szDbgMsg);
		iAuthStatus = 1;
	}

	debug_sprintf(szDbgMsg,"%s: stEmvRespDtls.szAuthRespCode [%s]", __FUNCTION__, stEmvRespDtls.szAuthRespCode);
	APP_TRACE(szDbgMsg);

	/* Auth Status */
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%c%c%c", 0xC1, 1, iAuthStatus);
	memcpy(pszCmd + length, szTemp, 3); length += 3;

	/*Display message on screen*/
    memset(szTemp, 0x00, sizeof(szTemp));
    sprintf(szTemp, "%c%c%c", 0xC1, 1, 0); // 0 = don't display result.
    memcpy(pszCmd + length, szTemp, 3); length += 3;

	if(strlen(stEmvRespDtls.szAuthRespCode) > 0)
	{
		strcpy(pstCardDtls->stEmvtranDlts.szAuthRespCode, stEmvRespDtls.szAuthRespCode);
		memset(szTemp, 0x00, sizeof(szTemp));
		if(stEmvRespDtls.iEMVRespTagsLen < 128)
		{
			sprintf(szTemp, "%c%c", 0xE1, stEmvRespDtls.iEMVRespTagsLen);
			memcpy((char *)szTemp + 2, stEmvRespDtls.szEMVRespTags, stEmvRespDtls.iEMVRespTagsLen);
			iTmp = stEmvRespDtls.iEMVRespTagsLen + 2;
		}
		else if(stEmvRespDtls.iEMVRespTagsLen >= 128 && stEmvRespDtls.iEMVRespTagsLen < 256)
		{
			sprintf(szTemp, "%c%c%c", 0xE1, 0x81, stEmvRespDtls.iEMVRespTagsLen);
			memcpy((char *)szTemp + 3, stEmvRespDtls.szEMVRespTags, stEmvRespDtls.iEMVRespTagsLen);
			iTmp = stEmvRespDtls.iEMVRespTagsLen + 3;
			iE1Len = iE1Len + 1;
		}
		else
		{
			sprintf(szTemp, "%c%c%c%c", 0xE1, 0x82, stEmvRespDtls.iEMVRespTagsLen >> 8, stEmvRespDtls.iEMVRespTagsLen & 0xFF);
			memcpy((char *)szTemp + 4, stEmvRespDtls.szEMVRespTags, stEmvRespDtls.iEMVRespTagsLen);
			iTmp = stEmvRespDtls.iEMVRespTagsLen + 4;
			iE1Len = iE1Len + 2;
		}
	}
	else
	{
		if(strlen(stEmvRespDtls.szEMVRespTags) > 0)
		{
			if(iAuthStatus == 0)
			{
				//Host Online and Approved the trans but did not send 8A Tag, so adding 8A0200
				debug_sprintf(szDbgMsg, "%s: Emv Tags Response Found without 8A, Appending and sending for Approval to card[8A0200]", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				strcpy(pstCardDtls->stEmvtranDlts.szAuthRespCode, "00");

				memset(szTemp, 0x00, sizeof(szTemp));
				sprintf(szTemp, "%c%c%c%c", 0x8A, STX, '0', '0');

				memcpy((char *)stEmvRespDtls.szEMVRespTags + stEmvRespDtls.iEMVRespTagsLen, szTemp, 4);
				stEmvRespDtls.iEMVRespTagsLen = stEmvRespDtls.iEMVRespTagsLen + 4;

				memset(szTemp, 0x00, sizeof(szTemp));
				if(stEmvRespDtls.iEMVRespTagsLen < 128)
				{
					sprintf(szTemp, "%c%c", 0xE1, stEmvRespDtls.iEMVRespTagsLen);
					memcpy((char *)szTemp + 2, stEmvRespDtls.szEMVRespTags, stEmvRespDtls.iEMVRespTagsLen);
					iTmp = stEmvRespDtls.iEMVRespTagsLen + 2;
				}
				else if(stEmvRespDtls.iEMVRespTagsLen >= 128 && stEmvRespDtls.iEMVRespTagsLen < 256)
				{
					sprintf(szTemp, "%c%c%c", 0xE1, 0x81, stEmvRespDtls.iEMVRespTagsLen);
					memcpy((char *)szTemp + 3, stEmvRespDtls.szEMVRespTags, stEmvRespDtls.iEMVRespTagsLen);
					iTmp = stEmvRespDtls.iEMVRespTagsLen + 3;
					iE1Len = iE1Len + 1;
				}
				else
				{
					sprintf(szTemp, "%c%c%c%c", 0xE1, 0x82, stEmvRespDtls.iEMVRespTagsLen >> 8, stEmvRespDtls.iEMVRespTagsLen & 0xFF);
					memcpy((char *)szTemp + 4, stEmvRespDtls.szEMVRespTags, stEmvRespDtls.iEMVRespTagsLen);
					iTmp = stEmvRespDtls.iEMVRespTagsLen + 4;
					iE1Len = iE1Len + 2;
				}

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Host Did Not Send Required 8A EMV Tag in EMV Tags SSI Response, Appending the Required Tag and Sending to Card for Approval");
					addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
				}
			}
			else if(iAuthStatus == 1)
			{
				//Host Online and Declined the trans but did not send 8A Tag, so adding 8A0205
				debug_sprintf(szDbgMsg, "%s: Emv Tags Response Found without 8A, Appending and sending for Decline to card[8A0205]", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				memset(szTemp, 0x00, sizeof(szTemp));
				sprintf(szTemp, "%c%c%c%c", 0x8A, STX, '0', '5');

				strcpy(pstCardDtls->stEmvtranDlts.szAuthRespCode, "05");

				memcpy((char *)stEmvRespDtls.szEMVRespTags + stEmvRespDtls.iEMVRespTagsLen, szTemp, 4);
				stEmvRespDtls.iEMVRespTagsLen = stEmvRespDtls.iEMVRespTagsLen + 4;

				memset(szTemp, 0x00, sizeof(szTemp));
				if(stEmvRespDtls.iEMVRespTagsLen < 128)
				{
					sprintf(szTemp, "%c%c", 0xE1, stEmvRespDtls.iEMVRespTagsLen);
					memcpy((char *)szTemp + 2, stEmvRespDtls.szEMVRespTags, stEmvRespDtls.iEMVRespTagsLen);
					iTmp = stEmvRespDtls.iEMVRespTagsLen + 2;
				}
				else if(stEmvRespDtls.iEMVRespTagsLen >= 128 && stEmvRespDtls.iEMVRespTagsLen < 256)
				{
					sprintf(szTemp, "%c%c%c", 0xE1, 0x81, stEmvRespDtls.iEMVRespTagsLen);
					memcpy((char *)szTemp + 3, stEmvRespDtls.szEMVRespTags, stEmvRespDtls.iEMVRespTagsLen);
					iTmp = stEmvRespDtls.iEMVRespTagsLen + 3;
				}
				else
				{
					sprintf(szTemp, "%c%c%c%c", 0xE1, 0x82, stEmvRespDtls.iEMVRespTagsLen >> 8, stEmvRespDtls.iEMVRespTagsLen & 0xFF);
					memcpy((char *)szTemp + 4, stEmvRespDtls.szEMVRespTags, stEmvRespDtls.iEMVRespTagsLen);
					iTmp = stEmvRespDtls.iEMVRespTagsLen + 4;
				}

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Host Did Not Send Required 8A EMV Tag in EMV Tags SSI Response, Appending the Required Tag and Sending to Card for Declining");
					addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
				}
			}
			else if(iAuthStatus == 2)
			{
				//Host Offline But we received EMV tags Response, so sending 8A02Y3 to card for Approving Online
				stEmvRespDtls.iEMVRespTagsLen = 4;
				memset(szTemp, 0x00, sizeof(szTemp));

				if(iOfflineAllowed == SUCCESS)
				{
					strcpy(pstCardDtls->stEmvtranDlts.szAuthRespCode, "Y3");

					sprintf(szTemp, "%c%c%c%c%c%c", 0xE1, EOT, 0x8A, STX, 'Y', '3');
					debug_sprintf(szDbgMsg, "%s: Emv Tags Response Found with Host Offline Sending 8A as Offline Approval to card[8A02Y3]", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					strcpy(pstCardDtls->stEmvtranDlts.szAuthRespCode, "Z3");

					sprintf(szTemp, "%c%c%c%c%c%c", 0xE1, EOT, 0x8A, STX, 'Z', '3');
					debug_sprintf(szDbgMsg, "%s: Emv Tags Response Found with Host Offline Sending 8A as Offline Decline to card[8A02Z3]", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

				iTmp = 6;

				if(iAppLogEnabled == 1)
				{
					if(iOfflineAllowed == SUCCESS)
					{
						strcpy(szAppLogData, "Host Offline and EMV Tags Response Present, Adding Required Tags and Sending to Card for Approval");
					}
					else
					{
						strcpy(szAppLogData, "Host Offline and EMV Tags Response Present, Adding Required Tags and Sending to Card for Decline");
					}
					addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Should Not come here iAuthStatus[%d]", __FUNCTION__, iAuthStatus);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
			}
		}
		else
		{
			if(iAuthStatus == 0)
			{
				//Host Online and Approved the trans but did not send us EMV_TAGS, so adding 8A0200
				debug_sprintf(szDbgMsg, "%s: No Emv Tags Response Found Sending 8A as Online Approval to card[8A0200]", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				stEmvRespDtls.iEMVRespTagsLen = 4;

				strcpy(pstCardDtls->stEmvtranDlts.szAuthRespCode, "00");

				memset(szTemp, 0x00, sizeof(szTemp));
				sprintf(szTemp, "%c%c%c%c%c%c", 0xE1, EOT, 0x8A, STX, '0', '0');
				iTmp = 6;
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Host Did Not Send EMV Tags in SSI Response, Adding Required Tags and Sending to Card for Approval");
					addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
				}
			}
			else if(iAuthStatus == 1)
			{
				//Host Online and Declined the trans but did not send us EMV_TAGS, so adding 8A0205
				debug_sprintf(szDbgMsg, "%s: No Emv Tags Response Found Sending 8A as Online Decline to card[8A0205]", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				stEmvRespDtls.iEMVRespTagsLen = 4;

				strcpy(pstCardDtls->stEmvtranDlts.szAuthRespCode, "05");

				memset(szTemp, 0x00, sizeof(szTemp));
				sprintf(szTemp, "%c%c%c%c%c%c", 0xE1, EOT, 0x8A, STX, '0', '5');
				iTmp = 6;
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Host Did Not Send EMV Tags in SSI Response, Adding Required Tags and Sending to Card for Declining");
					addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
				}
			}
			else if(iAuthStatus == 2)
			{
				stEmvRespDtls.iEMVRespTagsLen = 4;
				memset(szTemp, 0x00, sizeof(szTemp));
				//sprintf(szTemp, "%c%c%c%c%c%c", 0xE1, EOT, 0x8A, STX, 'Y', '3');

				if(iOfflineAllowed == SUCCESS)
				{
					strcpy(pstCardDtls->stEmvtranDlts.szAuthRespCode, "Y3");

					sprintf(szTemp, "%c%c%c%c%c%c", 0xE1, EOT, 0x8A, STX, 'Y', '3');
					debug_sprintf(szDbgMsg, "%s: No Emv Tags Response Found Sending 8A as Offline Approval to card[8A02Y3]", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					strcpy(pstCardDtls->stEmvtranDlts.szAuthRespCode, "Z3");

					sprintf(szTemp, "%c%c%c%c%c%c", 0xE1, EOT, 0x8A, STX, 'Z', '3');
					debug_sprintf(szDbgMsg, "%s: No Emv Tags Response Found Sending 8A as Offline Decline to card[8A02Z3]", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

				iTmp = 6;

				if(iAppLogEnabled == 1)
				{
					//strcpy(szAppLogData, "Host Offline or Not Present, Adding Required Tags and Sending to Card for Approval");
					if(iOfflineAllowed == SUCCESS)
					{
						strcpy(szAppLogData, "Host Offline or Not Present, Adding Required Tags and Sending to Card for Approval");
					}
					else
					{
						strcpy(szAppLogData, "Host Offline or Not Present, Adding Required Tags and Sending to Card for Decline");
					}
					addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Should Not come here iAuthStatus[%d]", __FUNCTION__, iAuthStatus);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
			}
		}
	}

	memcpy(pszCmd + length, szTemp, iTmp); length += stEmvRespDtls.iEMVRespTagsLen + iE1Len;

    pszCmd[4] = length - 5;
    *len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return---- [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);	
	return rv;
}
/*
 * ============================================================================
 * Function Name: getC_36Command
 *
 * Description	: This function will generate C36 command.
 *				  This command is used to obtain values for a set of tags from XPI
 *
 * Input Params	: EMV Tags that needs to be queried from XPI
 *
 * Output Params: Command
 * ============================================================================
 */
int getC_36Command(char *pszCmd,int *len,char *emvTags)
{
	char szTemp[256]		= "";
	int length			 	= 0;
	int	iRet				= 0;
	//char				tag[256]				= "";

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(emvTags == NULL)
	{
		return FAILURE;
	}
	memset(szTemp, 0x00, sizeof(szTemp));
	strcat(szTemp, "C36\0");length +=5;
	memcpy(pszCmd, szTemp, length);

	//sprintf(szTemp, "%c",(strlen(emvTags)/2)+2);

	/*Acquirer Record and Merchant Decision*/
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%c%c", 0xE1,strlen(emvTags)/2);
	memcpy(pszCmd + length, szTemp, 2);length += 2;

	memset(szTemp, 0x00, 256);
	// CID-67951: 25-Jan-16: MukeshS3: Need to handle the error value returned from Hex2Bin,
	// otherwise it may result pszCmd buffer overflow while using memcpy for iRet=-1 size.
	//iRet = Hex2Bin((unsigned char *)emvTags, strlen(emvTags)/2, (unsigned char *)szTemp);
	if((iRet = Hex2Bin((unsigned char *)emvTags, strlen(emvTags)/2, (unsigned char *)szTemp)) == -1)
	{
		return FAILURE;
	}

	if(iRet == -1)
	{
		debug_sprintf(szDbgMsg, "%s: Hex2Bin returned -1", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	memcpy(pszCmd + length, szTemp, iRet);

	length += iRet;

	pszCmd[4] = length - 5;
	*len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return SUCCESS;
}
/*
 * ============================================================================
 * Function Name: getI_02Command
 *
 * Description	: This function will generate I02 command.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getI_02Command( char *pszCmd, int *len)
{
	char szTemp[8 + 1];
    int length = 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "I02"); length +=3;
    memcpy(pszCmd, szTemp, length);

    *len = length;
	
	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
}

/*
 * ============================================================================
 * Function Name: getI_05Command
 *
 * Description	: This function will generate I05 command.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getI_05Command( char *pszCmd, int *len)
{
	char szTemp[8 + 1];
    int length = 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "I050"); length +=4;
    memcpy(pszCmd, szTemp, length);

    *len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
}

#if 0
/*
 * ============================================================================
 * Function Name: getD_11Command
 *
 * Description	: This function would create the CAPK files.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getD_11Command( char *pszCmd, int *len, EMV_CAPK_INFO_PTYPE  pstCAPKInfo)
{
	char 		szTemp[4096]		= "";	
	int 		iBytes				= 0;
    int			length 				= 0;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
		
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "D11"); length +=5;	//5=(cmd + 2 byte cmd len).
    memcpy(pszCmd, szTemp, length);
	
	// #RID	
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%03d", pstCAPKInfo->iRIDLen);
	memcpy(pszCmd+length, szTemp, 3); length +=3; 
    memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = sprintf(szTemp, "%s", pstCAPKInfo->szRID);
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes; 
	
	// #Public Key(PK)
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%03d", pstCAPKInfo->iPKIndexLen);
	memcpy(pszCmd+length, szTemp, 3); length +=3;
    memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = sprintf(szTemp, "%s", pstCAPKInfo->szPKIndex);	
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes; 
	
	// #Modulus
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%03d", pstCAPKInfo->iModLen/2);
	memcpy(pszCmd+length, szTemp, 3); length +=3; 
    memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = Hex2Bin((unsigned char *)&pstCAPKInfo->szModulus, 
			strlen(pstCAPKInfo->szModulus)/2, (unsigned char *)szTemp);	
	/* TODO: KranthiK1 App crashes here because of return value -1
	 * Handle the error condition well
	 * */
    /* Daivik : 25/1/2016 - Coverity Issue 67220 - Should not pass negative value to memcpy, this causes out of bound access */
    if(iBytes < 0)
    {
		debug_sprintf(szDbgMsg, "%s: iBytes cannot be negative", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
    }
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes; 
	
	// #Exponent
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%03d", pstCAPKInfo->iExpLen/2);
	memcpy(pszCmd+length, szTemp, 3); length +=3; 
    memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = Hex2Bin((unsigned char *)&pstCAPKInfo->szExponent, 
			 strlen(pstCAPKInfo->szExponent)/2,(unsigned char *)szTemp);	
    /* Daivik : 25/1/2016 - Coverity Issue 67220 - Should not pass negative value to memcpy, this causes out of bound access */
    if(iBytes < 0)
    {
		debug_sprintf(szDbgMsg, "%s: iBytes cannot be negative", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
    }
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes; 
	
	// #CheckSum
    memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = Hex2Bin((unsigned char *)&pstCAPKInfo->szCheckSum, 
			 strlen(pstCAPKInfo->szCheckSum)/2,(unsigned char *)szTemp);	
    /* Daivik : 25/1/2016 - Coverity Issue 67220 - Should not pass negative value to memcpy, this causes out of bound access */
    if(iBytes < 0)
    {
		debug_sprintf(szDbgMsg, "%s: iBytes cannot be negative", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
    }
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes;
	
	// #Expiry date
	/*TODO: check whether Exp date need to be in the format <33><31><31><32><31><36> in D11*/
	if(strlen(pstCAPKInfo->szExpDate) > 0)
	{	
		pszCmd[length] = FS; length += 1;
		memset(szTemp, 0x00, sizeof(szTemp));
		iBytes = Hex2Bin((unsigned char *)pstCAPKInfo->szExpDate, 
				 strlen(pstCAPKInfo->szExpDate)/2,(unsigned char *)szTemp);	
	    /* Daivik : 25/1/2016 - Coverity Issue 67220 - Should not pass negative value to memcpy, this causes out of bound access */
	    if(iBytes < 0)
	    {
			debug_sprintf(szDbgMsg, "%s: iBytes cannot be negative", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return;
	    }
		memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes;
	}
	//Copy length to output length placeholder
	*len = length;
	
	//Remove length of cmd and msg length	
	length = length - 5;
	
	//Message length
	pszCmd[3] = length / 256;
 	pszCmd[4] = length % 256;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
}

/*
 * ============================================================================
 * Function Name: getD_12Command
 *
 * Description	: This function would clear the CAPK files.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getD_12Command( char *pszCmd, int *len)
{
	char 		szTemp[256]			= "";
    int			length 				= 0;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
		
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "D1200"); length +=5;	//5=(cmd + 2 byte cmd len).
    memcpy(pszCmd, szTemp, length);
	
	*len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
}
/*
 * ============================================================================
 * Function Name: getD_13Command
 *
 * Description	: This function would Update AID in EST file.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getD_13Command( char *pszCmd, int *len,  EMV_CMD_INFO_PTYPE pstEmvCmdInfo, 
													EMV_EST_REC_PTYPE  pstEstRecInfo)
{
	char 		szTemp[256]			= "";
    int			length 				= 0;
	int 		iLen				= 0;
	int 		iCurSuppAidIndex	= 0;
	//short 		iRecNo				= 0;
	short 		inAIDNo				= 0;
		
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//iRecNo	 			= pstEmvCmdInfo->iRecNo;
	inAIDNo				= pstEmvCmdInfo->inAIDNo;
	iCurSuppAidIndex	= pstEmvCmdInfo->iNumSuppAid;

	debug_sprintf(szDbgMsg, "%s: CurrSuppAid at %d =%s", __FUNCTION__, iCurSuppAidIndex, 
										pstEstRecInfo->szSupportedAID[iCurSuppAidIndex]);
	APP_TRACE(szDbgMsg);
	
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "D13"); length +=3;	
    memcpy(pszCmd, szTemp, length);
	
	// Rec#	
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%02d", pstEstRecInfo->inRecNo);
	memcpy(pszCmd+length, szTemp, 2); length +=2; 
	
	// AID#	
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%02d", inAIDNo);
	memcpy(pszCmd+length, szTemp, 2); length +=2; 
	
	// AID Data 
	//SupportedAID (32 bytes)
	memset(szTemp, 0x00, sizeof(szTemp));
	iLen = strlen(pstEstRecInfo->szSupportedAID[iCurSuppAidIndex]);
	memcpy(szTemp, pstEstRecInfo->szSupportedAID[iCurSuppAidIndex], iLen);	
	memcpy(pszCmd+length, szTemp, 32); length +=32;

	//PartialNameFlag (4byte binary hex)
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%c", (int)pstEstRecInfo->inPartialNameAllowedFlag); //iParFlag = 01 or 02
	memcpy(pszCmd+length, szTemp, 4); length +=4; 

	//TermAVN (6byte binary hex)
	memset(szTemp, 0x00, sizeof(szTemp));
	iLen = strlen(pstEstRecInfo->szTermAVN);
	memcpy(szTemp, pstEstRecInfo->szTermAVN, iLen);	
	memcpy(pszCmd+length, szTemp, 6); length +=6;
	
	//2nd TermAVN (6byte binary hex)
	memset(szTemp, 0x00, sizeof(szTemp));
	iLen = strlen(pstEstRecInfo->szSecondTermAVN);
	memcpy(szTemp, pstEstRecInfo->szSecondTermAVN, iLen);	
	memcpy(pszCmd+length, szTemp, 6); length +=6;
	
	//RecommendAIDName (16bytes)
	memset(szTemp, 0x00, sizeof(szTemp));
	iLen = strlen(pstEstRecInfo->szRcmdAppNameAID);
	memcpy(szTemp, pstEstRecInfo->szRcmdAppNameAID, iLen);	
	memcpy(pszCmd+length, szTemp, 16); length +=16;
	
	*len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getD_14Command
 *
 * Description	: This function would update MVT (EMV Configuration Table).
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
int getD_14Command( char *pszCmd, int *len, EMV_MVT_REC_PTYPE  pstMvtRecInfo)
{
	char 		szTemp[256]			= "";
	char 		szHexTotal[16]		= "";
	int			iTotal				= 0;
    int			length 				= 0;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
		
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "D14"); length +=5;	
    memcpy(pszCmd, szTemp, length);
	
	// Rec#	
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%02d", pstMvtRecInfo->inRecNo);
	memcpy(pszCmd+length, szTemp, 2); length +=2; 

	// Scheme ref
	//Big Endian
	pszCmd[length] 	 = (pstMvtRecInfo->inSchemeReference) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->inSchemeReference) / 256;	
	length +=2;

	//Issuer ref
	pszCmd[length] 	 = (pstMvtRecInfo->inIssuerReference) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->inIssuerReference) / 256;	
	length +=2;

	//TRM data present
	pszCmd[length] 	 = (pstMvtRecInfo->inTRMDataPresent) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->inTRMDataPresent) / 256;	
	length +=2;
	
	// Target RS present
	pszCmd[length] 	 = (pstMvtRecInfo->inTargetRSPercent) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->inTargetRSPercent) / 256;	
	length +=2;	
	
	//Terminal Floor Limit (4byte)
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%ld", pstMvtRecInfo->lnFloorLimit);	
    memset(szHexTotal, 0x00, sizeof(szHexTotal));
	// CID-67269: 25-Jan-16: MukeshS3: Need to handle the error value returned from getHexTotal,
	// otherwise it may result pszCmd buffer overflow while using memcpy for iRet=-1 size.
    //iTotal = getHexTotal(szTemp, szHexTotal);
    if((iTotal = getHexTotal(szTemp, szHexTotal)) == -1)
    {
    	return FAILURE;
    }
    if(iTotal == -1)
    {
    	debug_sprintf(szDbgMsg, "%s: getHexTotal returned -1", __FUNCTION__);
    	APP_TRACE(szDbgMsg);
    	return;
    }
    memset(szTemp, 0x00, sizeof(szTemp));	
    memcpy(szTemp, szHexTotal, iTotal);
    memcpy(pszCmd + length, szTemp, 4); length += 4;
	
	//RS Threshold 	
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%ld", pstMvtRecInfo->lnRSThreshold);	
    memset(szHexTotal, 0x00, sizeof(szHexTotal));
	// CID-67269: 25-Jan-16: MukeshS3: Need to handle the error value returned from getHexTotal,
	// otherwise it may result pszCmd buffer overflow while using memcpy for iRet=-1 size.
    //iTotal = getHexTotal(szTemp, szHexTotal);
    if((iTotal = getHexTotal(szTemp, szHexTotal)) == -1)
    {
    	return FAILURE;
    }
    if(iTotal == -1)
	{
		debug_sprintf(szDbgMsg, "%s: getHexTotal returned -1", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}
    memset(szTemp, 0x00, sizeof(szTemp));	
    memcpy(szTemp, szHexTotal, iTotal);
    memcpy(pszCmd + length, szTemp, 4); length += 4;
	
	// Max Target RS present
	pszCmd[length] 	 = (pstMvtRecInfo->inMaxTargetRSPercent) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->inMaxTargetRSPercent) / 256;	
	length +=2;
	
	//MerchForceOnlineFlg (0; 2byte short)
	pszCmd[length] 	 = (pstMvtRecInfo->inMerchantForcedOnlineFlag) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->inMerchantForcedOnlineFlag) / 256;	
	length +=2;
	
	//BlackListCardSupportFlg (0; 2byte short)
	pszCmd[length] 	 = (pstMvtRecInfo->inBlackListedCardSupportFlag) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->inBlackListedCardSupportFlag) / 256;	
	length +=2;
	
	//FallBackAllowedFlg (1; 2byte short)
	pszCmd[length] 	 = (pstMvtRecInfo->inFallbackAllowed) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->inFallbackAllowed) / 256;	
	length +=2;	

	//TACDefault (D84000A800; ASCII String)
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstMvtRecInfo->szTACDefault);	
	memcpy(pszCmd+length, szTemp, 12); length +=12;

	//TACDenial (0010000000; ASCII String)
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstMvtRecInfo->szTACDenial);	
	memcpy(pszCmd+length, szTemp, 12); length +=12;

	//TACOnline (D84004F800; ASCII String)
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstMvtRecInfo->szTACOnline);		
	memcpy(pszCmd+length, szTemp, 12); length +=12;

	//DefaultTDOL (9F02065F2A029A039C0195059F3704; ASCII)
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstMvtRecInfo->szDefaultTDOL);	
	memcpy(pszCmd+length, szTemp, 66); length +=66;

	//DefaultDDOL (9f3704; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstMvtRecInfo->szDefaultDDOL);		
	memcpy(pszCmd+length, szTemp, 66); length +=66;

	//NextRecord (-1; 2byte short (xFFxFF))
	pszCmd[length] 	 = (pstMvtRecInfo->inNextRecord) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->inNextRecord) / 256;	
	length +=2;	
	
	//AutoSelectAppl (1; 2byte short (x01x00))
	pszCmd[length] 	 = (pstMvtRecInfo->inAutoSelectAppln) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->inAutoSelectAppln) / 256;	
	length +=2;	

	/*EMVCounter (0; 4byte long (x00x00x00x00)- This value is used 
				 internally by the kernel, and should always be set to zero.)*/
	//pszCmd[length] = pstMvtRecInfo->ulEMVCounter;
	length +=4;

	//CountryCode (0124; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));	
	memcpy(szTemp, pstMvtRecInfo->szTermCountryCode, 6);	
	memcpy(pszCmd+length, szTemp, 6); length +=6;

	//CurrencyCode (0124; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szTermCurrencyCode, 6);	
	memcpy(pszCmd+length, szTemp, 6); length +=6;

	//szTermCapabilities (E0B8C8; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));	
	memcpy(szTemp, pstMvtRecInfo->szTermCapabilities, 8);	
	memcpy(pszCmd+length, szTemp, 8); length +=8;

	//TermAddCapabilities (F000F0F001; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szTermAddCapabilities, 12);	
	memcpy(pszCmd+length, szTemp, 12); length +=12;

	//TermType (22; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstMvtRecInfo->szTermType);	
	memcpy(pszCmd+length, szTemp, 4); length += 4;

	//MerchCatCode (; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstMvtRecInfo->szMerchantCategoryCode);	
	memcpy(pszCmd+length, szTemp, 6); length += 6;

	//TerminalCatCode (; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstMvtRecInfo->szTerminalCategoryCode);	
	memcpy(pszCmd+length, szTemp, 4); length += 4;

	//TermID (12345678; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szTerminalID, 9);	
	memcpy(pszCmd+length, szTemp, 9); length +=9;

	//MerchID (9999999999; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szMerchantID, 16);	
	memcpy(pszCmd+length, szTemp, 16); length +=16;

	//AcquireID (112233; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szAcquirerID, 14);	
	memcpy(pszCmd+length, szTemp, 14); length +=14;

	//CAPKIndex (FF; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szTermCAPKIndex, 3);	
	memcpy(pszCmd+length, szTemp, 3); length +=3;

	//PINBypassFlg (1; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szPINBypassFlag, 2);	
	memcpy(pszCmd+length, szTemp, 2); length +=2;

	//PINTimout (060; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szPINTimeout, 4);	
	memcpy(pszCmd+length, szTemp, 4); length +=4;

	//PINFormat (0; 0-1 MS, 2-9 DUK, A EMV, B secure script)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szPINFormat, 2);	
	memcpy(pszCmd+length, szTemp, 2); length +=2;

	//PINScriptNumber (0; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szPINScriptNum, 2);	
	memcpy(pszCmd+length, szTemp, 2); length +=2;

	//PINMacroNumber (0; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szPINMacroNum, 3);	
	memcpy(pszCmd+length, szTemp, 3); length +=3;

	//PINDevriKeyFlg (0; 0 - Traditional, 1 - Script, 2 - None)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szPINDerivKeyFlag, 2);	
	memcpy(pszCmd+length, szTemp, 2); length +=2;

	//PINDevriMacroNum (0; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szPINDerivMacroNum, 3);	
	memcpy(pszCmd+length, szTemp, 3); length +=3;

	//CardStatDisplayFlg (1; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szCardStatDispFlag, 2);	
	memcpy(pszCmd+length, szTemp, 2); length +=2;

	//TermCurExp (0; 2byte short)
	pszCmd[length] 	 = (pstMvtRecInfo->inTermCurExp) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->inTermCurExp) / 256;	
	length +=2;	
	
	//IssAcqflag (0; 2byte short)
	pszCmd[length] 	 = (pstMvtRecInfo->inIssAcqflag) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->inIssAcqflag) / 256;	
	length +=2;	
	
	//NoDisplaySupportFlag (0; 2byte short)
	pszCmd[length] = pstMvtRecInfo->inNoDisplaySupportFlag;
	length +=2;

	//ModifyCandListFlag (0; 2byte short)
	pszCmd[length] 	 = (pstMvtRecInfo->inModifyCandListFlag) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->inModifyCandListFlag) / 256;	
	length +=2;		
	
	//StringRFU1 (; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szRFU1, 26);	
	memcpy(pszCmd+length, szTemp, 26); length +=26;

	//StringRFU2 (; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szRFU2,  26);	
	memcpy(pszCmd+length, szTemp,  26); length += 26;

	//StringRFU3 (; ASCII string)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstMvtRecInfo->szRFU3, 26);	
	memcpy(pszCmd+length, szTemp,  26); length += 26;

	//ShortRFU1 (0; 2byte short)
	pszCmd[length] 	 = (pstMvtRecInfo->shRFU1) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->shRFU1) / 256;	
	length +=2;	

	//ShortRFU2 (0; 2byte short)
	pszCmd[length] 	 = (pstMvtRecInfo->shRFU2) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->shRFU2) / 256;	
	length +=2;	
	
	//ShortRFU3 (0; 2byte short)
	pszCmd[length] 	 = (pstMvtRecInfo->shRFU3) % 256;
	pszCmd[length+1] = (pstMvtRecInfo->shRFU3) / 256;	
	length +=2;		

	//Copy length to output length placeholder
	*len = length;
	
	//Remove length of cmd and msg length	
	length = length - 5;
	
	//Little endian format.
	pszCmd[3] = length / 256;
	pszCmd[4] = length % 256;
	
	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: getD_15Command
 *
 * Description	: This function would Update AID in EST file.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getD_15Command( char *pszCmd, int *len,EMV_EST_REC_PTYPE  pstEstRecInfo)
{
	char 		szTemp[256]			= "";
    int			length 				= 0;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
		
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "D15"); length +=5;	
    memcpy(pszCmd, szTemp, length);
	
	// Rec#	
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%02d", pstEstRecInfo->inRecNo);
	memcpy(pszCmd+length, szTemp, 2); length +=2; 
	
	// lnNbrOfTransactions (0x0000; 4byte long)	
    memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(pszCmd+length, szTemp, 4); length +=4; 
	
	//SchemeLabel (Visa; 36 bytes padded with 0x00)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstEstRecInfo->szSchemeLabel, strlen(pstEstRecInfo->szSchemeLabel));	
	memcpy(pszCmd+length, szTemp, 36); length +=36;

	//RID (A000000003; 11 bytes padded with 0x00)
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstEstRecInfo->szRID); //iParFlag = 01 or 02
	memcpy(pszCmd+length, szTemp, 11); length +=11; 

	//CSNList (A000000003.CSN; 32 bytes padded with 0x00)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstEstRecInfo->szCSNList, strlen(pstEstRecInfo->szCSNList));	
	memcpy(pszCmd+length, szTemp,32); length +=32;
	
	//EMV Table Record (0x0000; 4byte long)
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%ld", pstEstRecInfo->inEMVTableRecord);
	memcpy(pszCmd+length, szTemp, 4); length +=4;
	
	//CSNListFile (; 31 bytes padded with 0x00)
	memset(szTemp, 0x00, sizeof(szTemp));
	memcpy(szTemp, pstEstRecInfo->szCSNListFile, strlen(pstEstRecInfo->szCSNListFile));	
	memcpy(pszCmd+length, szTemp, 31); length +=31;

	//Copy length to output length placeholder
	*len = length;
	
	//Remove length of cmd and msg length	
	length = length - 5;
	
	pszCmd[3] = length / 256;
	pszCmd[4] = length % 256;
	
	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getD_16Command
 *
 * Description	: This function would create EMV Tables.
 *
 * Input Params	: table# and Rec#
 *
 * Output Params: Command
 * ============================================================================
 */
void getD_16Command( char *pszCmd, int *len, long lnTableNo, int inNumRec)
{
	char 		szTemp[256]			= "";
    int			length 				= 0;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
		
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "D16"); length +=3;
    memcpy(pszCmd, szTemp, length);

	// Table#
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%02ld", lnTableNo);
	memcpy(pszCmd+length, szTemp, 2); length +=2;

	// Rec#
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%03d", inNumRec);
	memcpy(pszCmd+length, szTemp, 3); length +=3;

	*len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
}

/*
 * ============================================================================
 * Function Name: getD_17Command
 *
 * Description	: This function would update EEST Table.
 *
 * Input Params	: Rec#
 *
 * Output Params: Command
 * ============================================================================
 */
void getD_17Command( char *pszCmd, int *len, EMV_EEST_REC_PTYPE  pstEestRecInfo)
{
	char 		szTemp[256]			= "";
    int			length 				= 0;
	int 		iLen				= 0; 
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
		
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "D17"); length +=5;	
    memcpy(pszCmd, szTemp, length);
	
	// Rec#	
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%03d", pstEestRecInfo->inRecNo);
	memcpy(pszCmd+length, szTemp, 3); length +=3; 
	
	//SupportedAID (32 bytes)
	memset(szTemp, 0x00, sizeof(szTemp));
	iLen = strlen(pstEestRecInfo->szSupportedAID);
	memcpy(szTemp, pstEestRecInfo->szSupportedAID, iLen);	
	memcpy(pszCmd+length, szTemp, 32); length +=32;

	// EMVRec (Links supported AID to corresponding EMVT record number)	
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%02d", pstEestRecInfo->inEMVRec);
	memcpy(pszCmd+length, szTemp, 2); length +=2; 

	// Max AID length	
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%02d", pstEestRecInfo->inMaxAIDLength);
	memcpy(pszCmd+length, szTemp, 2); length +=2;
	
	/*ApplicationFlow (	1 - MASTERCARD,2 - AMEX, 3 - MC MSTRIPE,
						6 - VISA, 13 - DISCOVER, 14 - JCB, 21 - INTERAC )*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%02d", pstEestRecInfo->inApplicationFlow); 
	memcpy(pszCmd+length, szTemp, 2); length +=2; 

	/*Partial Name: Flag indicating whether the full AID is required*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%1d", pstEestRecInfo->inPartialName); 
	memcpy(pszCmd+length, szTemp, 1); length +=1; 
	
	/*AcctSelFlag: Flag indicating whether the account selection prompt will 
				   appear for card types with multiple applications*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%1d", pstEestRecInfo->inAcctSelFlag); 
	memcpy(pszCmd+length, szTemp, 1); length +=1; 

	/*CTLSEnableFlag: Flag indicating whether contactless processing is allowed 
				      for the specific card scheme*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%1d", pstEestRecInfo->inCTLSEnableFlag); 
	memcpy(pszCmd+length, szTemp, 1); length +=1;
	
	/*TransactionScheme: Decimal representation of Transaction Scheme bit field*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%03d", pstEestRecInfo->iTransactionScheme); 
	memcpy(pszCmd+length, szTemp, 3); length +=3; 

	//Copy length to output length placeholder
	*len = length;
	
	//Remove length of cmd and msg length	
	length = length - 5;
	
	pszCmd[3] = length / 256;
	pszCmd[4] = length % 256;
	
	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getD_18Command
 *
 * Description	: This function would create message to initialize CTLS Reader.
 *
 * Input Params	: BelowFloorLimitTermCapabilities and AboveFloorLimitTermCapabilities
 *
 * Output Params: Command
 * ============================================================================
 */
void getD_18Command( char *pszCmd, int *len, EMV_OTHER_REC_PTYPE pstOtherRecInfo )
{
	char 		szTemp[256]					= "";
	char *		pszBlwFlrLimitTermCap		= NULL;
	char *		pszAbvFlrLimitTermCap		= NULL;
	char *		pszCTLSCVMLimit				= NULL;		
	char *		pStr						= NULL;
    int			length 						= 0;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/* Copy values */
	pszBlwFlrLimitTermCap =	pstOtherRecInfo->szBlwFlrLimitTermCap; 
	pszAbvFlrLimitTermCap = pstOtherRecInfo->szAbvFlrLimitTermCap;  
	pszCTLSCVMLimit		  = pstOtherRecInfo->szCTLSCVMLimit;  	
	
	memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "D18"); length +=3;
    memcpy(pszCmd, szTemp, length);

	// BelowFloorLimitTermCapabilities
    memset(szTemp, 0x00, sizeof(szTemp));
	pStr = leftPad(pszBlwFlrLimitTermCap, 6, '0');
	sprintf(szTemp, "%s", pStr);
	memcpy(pszCmd+length, szTemp, 6); length +=6; 

	// AboveFloorLimitTermCapabilities
    memset(szTemp, 0x00, sizeof(szTemp));
	pStr = leftPad(pszAbvFlrLimitTermCap, 6, '0');
	sprintf(szTemp, "%s", pStr);
	memcpy(pszCmd+length, szTemp, 6); length +=6; 

	// FloorLimit(Optional, 12 bytes long padded with zeros)
    memset(szTemp, 0x00, sizeof(szTemp));
	pStr = leftPad(pszCTLSCVMLimit, 12, '0');
	sprintf(szTemp, "%s", pStr);
	memcpy(pszCmd+length, szTemp, 12); length +=12; 
	
	*len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
}

/*
 * ============================================================================
 * Function Name: getD_19Command
 *
 * Description	: This function would update MVT (EMV Configuration Table).
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getD_19Command( char *pszCmd, int *len, EMV_EMVT_REC_PTYPE  pstEmvtRecInfo)
{
	char 		szTemp[256]			= "";
	char 		*pTmpStr			= NULL;
	int			iLen				= 0;
    int			length 				= 0;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
		
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "D19"); length +=5;	
    memcpy(pszCmd, szTemp, length);
	
	// Rec#	
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%03d", pstEmvtRecInfo->inRecNo);
	memcpy(pszCmd+length, szTemp, 3); length +=3; 

	//GroupName (VISA, "MASTERCARD",INTERAC;20 bytes padded with 0x00.)
	memset(szTemp, 0x00, sizeof(szTemp));
	iLen = strlen(pstEmvtRecInfo->szCTLSGroupName);
	memcpy(szTemp, pstEmvtRecInfo->szCTLSGroupName, iLen);	
	memcpy(pszCmd+length, szTemp, 20); length +=20;
	
	/*Amount used to determine if transaction should be performed online or offline. 
	 If offline is not supported, the value should be zero.12 bytes, 
	 Right padded with '0', include 2-digit decimal.*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstEmvtRecInfo->szCTLSFlrLimit);
	memcpy(pszCmd+length, szTemp, 12); length +=12; 

	/*The Cardholder Verification Method Limit - determines whether a signature
	  line should be printed on the receipt. Values above this limit require a signature.*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstEmvtRecInfo->szCTLSCVMLimit);
	memcpy(pszCmd+length, szTemp, 12); length +=12; 
	
	/*Contactless Transaction Limit - maximum transaction amount allowed 
	  for contactless transactions.*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstEmvtRecInfo->szCTLSTranLimit);
	memcpy(pszCmd+length, szTemp, 12); length +=12; 

	/*Contactless Terminal Action Code - Default*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstEmvtRecInfo->szCTLSTACDefault);
	pTmpStr = rightPad(szTemp, 10, '0');	
	memcpy(pszCmd+length, pTmpStr, 10); length +=10;

	/*Contactless Terminal Action Code - Denial*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstEmvtRecInfo->szCTLSTACDenial);
	pTmpStr = rightPad(szTemp, 10, '0');	
	memcpy(pszCmd+length, pTmpStr, 10); length +=10;

	/*Contactless Terminal Action Code - Online*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstEmvtRecInfo->szCTLSTACOnline);
	pTmpStr = rightPad(szTemp, 10, '0');		
	memcpy(pszCmd+length, pTmpStr, 10); length +=10;

	/*Visa Terminal Transaction Qualifier - indicates reader data element 
	  capabilities and transaction specific requirements (e.g., online CVM) of the reader.*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstEmvtRecInfo->szCTLSVISATTQ);
	pTmpStr = rightPad(szTemp, 8, '0');			
	memcpy(pszCmd+length, pTmpStr, 8); length +=8;

	/*Index into MVT table - 00 - 99. Links the card scheme record to 
	  the associated MVT record number*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%02d", pstEmvtRecInfo->inCTLSMVTIndex);
	memcpy(pszCmd+length, szTemp, 2); length +=2;

	/*Represents the card data input, CVM, and security capabilities of the device.*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstEmvtRecInfo->szCTLSTermCapabilities);
	pTmpStr = rightPad(szTemp, 6, '0');		
	memcpy(pszCmd+length, pTmpStr, 6); length +=6;

	/*Represents the data input and output capabilities of the device*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstEmvtRecInfo->szCTLSTermAddCapabilities);
	pTmpStr = rightPad(szTemp, 10, '0');			
	memcpy(pszCmd+length, pTmpStr, 10); length +=10;
	
	/*TODO: Commented for now.Because eventhough we send country code and currency 
			in this request,XPI will not consider these values.*/
	#if 0 
	/*Country Code used for contactless transactions.
       840 = US
	   124 = CAN */
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstEmvtRecInfo->szCTLSCountryCode);
	memcpy(pszCmd+length, szTemp, 3); length +=3;

	/*Currency Code used for contactless transactions.
	  840 = US
	  124 = CAN */
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstEmvtRecInfo->szCTLSCurrencyCode);
	memcpy(pszCmd+length, szTemp, 3); length +=3;
	#endif
	
	//Copy length to output length placeholder
	*len = length;
	
	//Remove length of cmd and msg length	
	length = length - 5;
	
	pszCmd[3] = length / 256;
	pszCmd[4] = length % 256;
	
	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getD_20Command
 *
 * Description	: This function would create message to update ICCT. This command will 
 *				  update the Interac Contactless Configuration (ICCT) Table with the 
 *				  parameters required in order to configure the CTLS reader to accept 
 *				  Interac cards. 
 *
 * Input Params	: Rec#
 *
 * Output Params: Command
 * ============================================================================
 */
void getD_20Command( char *pszCmd, int *len, EMV_ICCT_REC_PTYPE pstIcctRecInfo)
{
	char 		szTemp[256]			= "";
	char 		*pStr				= NULL;
    int			length 				= 0;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
		
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "D20"); length +=5;
    memcpy(pszCmd, szTemp, length);

	// Rec#	
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%03d", pstIcctRecInfo->inRecNo);
	memcpy(pszCmd+length, szTemp, 3); length +=3; 
	
	/*Merchant Type Indicator used by the card to choose limits for 
	card risk management. Valid values 01 - 05*/
	memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%02d", atoi(pstIcctRecInfo->szCTLSMerchantType));	
	memcpy(pszCmd+length, szTemp, 2); length +=2;
	
	/*Provides Terminal Transaction Information such as; display capability, 
	Interac contact interface application support, online capabilities, and 
	card read capabilities (mag stripe, contact).*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstIcctRecInfo->szCTLSTermTranInfo);
	memcpy(pszCmd+length, szTemp, 6); length +=6; 

	/*A dynamic value that must to updated to provide the reader with the current
	transaction type.*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%02d", atoi(pstIcctRecInfo->szCTLSTermTranType));
	memcpy(pszCmd+length, szTemp, 2); length +=2; 
	
	/*Terminal Contactless Receipt Required Limit - Limit amount used to compare 
	against Transaction amount to automatically print a transaction record.*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstIcctRecInfo->szCTLSReqReceiptLimit);
	pStr = rightPad(szTemp, 12, '0');	
	memcpy(pszCmd+length, pStr, 12); length +=12; 
	
	/*Options supported by the terminal defining how to process transactions using
	a different currency or country code.*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstIcctRecInfo->szCTLSOptionStatus);
	memcpy(pszCmd+length, szTemp, 4); length +=4; 

	/*Floor limit amount used to compare against Transaction amount.*/
    memset(szTemp, 0x00, sizeof(szTemp));
	sprintf(szTemp, "%s", pstIcctRecInfo->szCTLSReaderFloorLimit);
	pStr = rightPad(szTemp, 12, '0');	
	memcpy(pszCmd+length, pStr, 12); length +=12; 
	
	//Copy length to output length placeholder
	*len = length;
	
	//Remove length of cmd and msg length	
	length = length - 5;
	
	pszCmd[3] = length / 256;
	pszCmd[4] = length % 256;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
}
#endif
/*
 * ============================================================================
 * Function Name: getD_25Command
 *
 * Description	: This function would create message to get additional EMV Info.
 *
 * Input Params	: table# and Rec#
 *
 * Output Params: Command
 * ============================================================================
 */
void getD_25Command( char *pszCmd, int *len, EMV_OTHER_REC_PTYPE pstOtherRecInfo)
{
	char 		szTemp[256]			= "";
    int			length 				= 0;
	int 		iBytes				= 0;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "D25"); length +=3;
    memcpy(pszCmd, szTemp, length);

	// Enable VISA Debit	
    memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = sprintf(szTemp, "%1d", pstOtherRecInfo->inVisaDebit);
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes; 

	// Contactless
    memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = sprintf(szTemp, "%1d", pstOtherRecInfo->inEMVContactless);
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes; 

	// PDOL support
    memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = sprintf(szTemp, "%1d", pstOtherRecInfo->inPDOLSupFlg);
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes; 

	// ValueLink Card support
    memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = sprintf(szTemp, "%1d", pstOtherRecInfo->inValueLinkCrdSup);
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes; 

	// ISIS NFC support Setting as 0 Remove following line later if required
	memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = sprintf(szTemp, "%1d", pstOtherRecInfo->inISISNFCSup);
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes;

	// AMEX CTLS support; 
    memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = sprintf(szTemp, "%1d", pstOtherRecInfo->inAMEXCTLSSup);
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes;

	// Mobile support;
	memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = sprintf(szTemp, "%1d", pstOtherRecInfo->inMobileSupport);
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes;

	// US Debit Flag;
	memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = sprintf(szTemp, "%1d", pstOtherRecInfo->inUSDebitFlag);
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes;

	//9th - 16th Bytes are RFU
    memset(szTemp, 0x00, sizeof(szTemp));
	iBytes = sprintf(szTemp, "%08d", 0);
	memcpy(pszCmd+length, szTemp, iBytes); length +=iBytes;
	
	*len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
}

/*
 * ============================================================================
 * Function Name: getS_13Command
 *
 * Description	: This command is used to control the messages that are displayed during the C30 command.
 *
 * Input Params	: None
 *
 * Output Params: Command
 * ============================================================================
 */
void getD_32Command( char *pszCmd, int *len)
{
	char				szTitle1[100]		= "";
	char				szTitle2[100]		= "";
	char				szTitle3[100]		= "";
	char 				szTemp[256]			= "";
	int					idisplayAmnt		= 0;
	int					iBytes				= 0;
    int 				length 				= 0;
	LABEL_STYPE_TITLE 	stLblTitle;	
	
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Get the Title 1*/
	fetchScrTitle(szTitle1, SWIPE_TAP1_TITLE);
	/* Get the Title 2*/
	fetchScrTitle(szTitle2, INSERT_CARD_TITLE);
	/* Get the Title 3*/
	fetchScrTitle(szTitle3, SWIPE_TAP2_TITLE);	
	
	memset(szTemp, 0x00, sizeof(szTemp));	
	sprintf(szTemp, "%s %s %s", szTitle1, szTitle2, szTitle3);
	
	memset(&stLblTitle, 0x00, sizeof(stLblTitle));
	//Split title string of required length(i.e Based on maxLabelLength defined for Mx915 or Mx925).
	splitStringOfReqLength(szTemp, giLabelLen-10, &stLblTitle);
	debug_sprintf(szDbgMsg, "%s: szTitle1 =%s, szTitle2 =%s, szTitle3 =%s",__FUNCTION__,
			stLblTitle.szTitle1, stLblTitle.szTitle2, stLblTitle.szTitle3);
	APP_TRACE(szDbgMsg);
	
    memset(szTemp, 0x00, sizeof(szTemp));
    strcat(szTemp, "D32"); length +=3;
    memcpy(pszCmd, szTemp, length);

	pszCmd[length] = FS; length += 1;

	//Display Command; 0-Clear previous settings, 1- use new settings.
	pszCmd[length] = '1'; length += 1;

	pszCmd[length] = FS; length += 1;
	
	//Msg Line1 (upto 32 characters)
    memset(szTemp, 0x00, sizeof(szTemp));
    iBytes = sprintf(szTemp, "%d%s", idisplayAmnt, stLblTitle.szTitle1);
    memcpy(pszCmd + length, szTemp, iBytes); length += iBytes;

	pszCmd[length] = FS; length += 1;	

	//Msg Line2 (Upto 32 characters)
    memset(szTemp, 0x00, sizeof(szTemp));
    iBytes = sprintf(szTemp, "%d%s", idisplayAmnt, stLblTitle.szTitle2);
    memcpy(pszCmd + length, szTemp, iBytes); length += iBytes;

	pszCmd[length] = FS; length += 1;	


	//Msg Line3 (upto 32 characters)
    memset(szTemp, 0x00, sizeof(szTemp));
    iBytes = sprintf(szTemp, "%d%s", idisplayAmnt, stLblTitle.szTitle3);
    memcpy(pszCmd + length, szTemp, iBytes); length += iBytes;

	pszCmd[length] = FS; length += 1;	

	//Msg Line4 (Upto 32 characters)
    memset(szTemp, 0x00, sizeof(szTemp));
    iBytes = sprintf(szTemp, "%d%s", idisplayAmnt, stLblTitle.szTitle4);
    memcpy(pszCmd + length, szTemp, iBytes); length += iBytes;

	pszCmd[length] = FS; length += 1;
	
	
    *len = length;

	debug_sprintf(szDbgMsg, "%s: ---Return----", __FUNCTION__);
	APP_TRACE(szDbgMsg);	
}
#if 0
/*=============================================================================
 rightPad - This function will right pad string with passed padChar.
 @return - string.
================================================================================*/

static char *rightPad(char *str, int size, char padChar) 
{
	int 	iLen		= 0;
	int 	i			= 0;	  
	int 	iPadSize	= 0;

	if (str == NULL) 
	{
	  return NULL;
	}
	iLen = strlen(str);
	iPadSize = size - iLen;	  

	if (iPadSize <= 0) 
	{
	  return str; // returns original string
	}
	for(i = 0; i < iPadSize; i++)
	{
	 sprintf(str+iLen, "%c", padChar);
	 iLen++;
	}
	return str;
}

/*=============================================================================
 leftPad - This function will left pad string with passed padChar.
 @return - string.
================================================================================*/

static char *leftPad(char *str, int size, char padChar) 
{
	
	int 	iLen		= 0;
	int 	i			= 0;	  
	int 	iPadSize	= 0;
	//char 	*szTmpStr	= NULL;
	char 	szTmpStr[50] = "";
	
	if (str == NULL) 
	{
	  return NULL;
	}
	iLen = strlen(str);
	iPadSize = size - iLen;	  
	
	if (iPadSize <= 0) 
	{
	  return str; // returns original string
	}
	iLen = iPadSize+iLen;
	//szTmpStr = malloc(iLen);
	memset(szTmpStr, 0x00, 50); 
	
	iLen = 0;
	for(i = 0; i < iPadSize; i++)
	{
		sprintf(szTmpStr+iLen, "%c", padChar);	
		iLen++;
	}
	if(iLen > 0)
	{
		strcat(szTmpStr, str);
		//return szTmpStr;
		memset(str, 0x00, 7);
		sprintf(str, "%s", szTmpStr);
		return str;
	}
	else
	{
		return str;
	}
}
#endif
/*=============================================================================
 HexBuffToi - This function will convert input Hex in Integer
 @iHex - input Hex.
================================================================================*/
int HexToDec(int iHex)
{
	int iRet = 0, i = 0;

	while(iHex)
	{
		iRet = iRet + (iHex % 10) * (int)pow(16, i++);
		iHex = iHex / 10;
	}
	return iRet;
}

int getHexTotal(char *szTotal, char *hexTotal)
{
	int  rv 				= SUCCESS;	
	char buff[50] 			="";
	char total[50]			="";

	memset(buff, 0x00, sizeof(buff));
	memset(total, 0x00, sizeof(total));

	Dec2Hex(atoi(szTotal), total, strlen(szTotal));
	if(strlen(total) % 2 > 0)
	{
		buff[0] = '0';
	}
	strcat(buff, total);

	rv = Hex2Bin((unsigned char *)buff, strlen(buff)/2, (unsigned char *	)hexTotal);

	return rv;
}
#if 0
/*This function will convert a character buffer into two byte Hex buff*/
static void  Char2Hex(char *inBuff, char *outBuff, int iLen)
{
	char temp[2 + 1];
	char chHex[] = "0123456789ABCDEF\0";
	int i = 0, iDec;
	while(i < iLen)
	{
		iDec = inBuff[i++];
		memset(temp, 0x00, sizeof(temp));

		temp[1] = chHex[iDec%16];
		iDec = iDec/16;
		temp[0] = chHex[iDec%16];

		strcat(outBuff, temp);
	}
}
#endif

/*=============================================================================
 DecBin - This function will convert input Decimal to Binary and returns the length
 @inoutBuff - input decimal.
 @iLength - Length of the buffer.
================================================================================*/
int DecBin(char *inoutBuff, int iLength)
{
	int i = 0;
	while(i<iLength)
	{
		inoutBuff[i] -= '0';
		i++;
	}
	return iLength;
}

/*=============================================================================
 Dec2Hex - This function will convert input Decimal to Hex and returns the length
 @iDec - input decimal.
 @iBuff- Return hex buffer.
 @iLen - Length of the buffer.
================================================================================*/
int Dec2Hex(unsigned int iDec, char *Buff, int iLen)
{
	int i=0;
	char chHex[] = "0123456789ABCDEF\0";

	do
	{
		Buff[i++] = chHex[iDec%16];
		iDec = iDec/16;

		if(i>iLen)
		{
			memset(Buff, 0x00, iLen);
			return -1;
		}

	}while(iDec > 0);

	strrev(Buff);
	return i;

}
/*============================================================================
Hex2Bin - This is utility function to perform Hex to Binary conversion
on input data
@param - data. Pointer to input hex data
@param - length. number of bytes to convert
@param - dest. Buffer to hold converted data buffer
@return - length.
=============================================================================*/
int Hex2Bin (unsigned char *data, int length, unsigned char *dest)
{
    int c;
    unsigned char ch;
    unsigned char byte;
    for (c = 0; c < length; c++)
    {
        byte = 0;
        ch = *data++;

        if (ch >= '0' && ch <= '9')
        {
            ch -= '0';
        }
        else
        {
            if (ch >= 'A' && ch <= 'F')
            {
                ch -= 'A';
                ch += 10;
            }
            else if (ch >= 'a' && ch <= 'f')
            {
            	ch -= 'a';
            	ch += 10;
            }
            else
                return (-1);
        }

        byte = ch << 4;
        ch = *data++;

        if (ch >= '0' && ch <= '9')
        {
            ch -= '0';
        }
        else
        {
            if (ch >= 'A' && ch <= 'F')
            {
                ch -= 'A';
                ch += 10;
            }
            else if (ch >= 'a' && ch <= 'f')
            {
            	ch -= 'a';
            	ch += 10;
            }
            else
                return (-1);
        }

        byte |= ch;
        *dest = byte;
        dest++;
    }
    return (length);
}
// Reverse a string in place using XOR swapping
char *strrev(char *str)
{
    char *p1, *p2;

    if (! str || ! *str)
        return str;
    for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
    {
        *p1 ^= *p2;
        *p2 ^= *p1;
        *p1 ^= *p2;
    }

    return str;
}

void AsciiStringToHex(unsigned char* destination, unsigned char* source, int sourceSize)
{
	int i, j;
	unsigned char temp[2];
	unsigned char* src = source;
	for( i = 0; i < sourceSize; i += 2 )
	{
		for ( j = 0; j < 2; ++j, ++src)
		{
		temp[j] = *src <= '9' ? *src - '0' : *src - '7';
		}
		*destination++ = (temp[0] << 4) | (temp[1] & 0x0F);
	}
}
#if 0
void HexToAsciiString(unsigned char* destination, unsigned char* source, int sourceSize)
{
	int				i;
	unsigned char 	byte = 0;

	for ( i = 0; i < sourceSize; i++)

	{

	*destination++ = (( byte = (*source ) >> 4 ) <= 9) ? byte + '0' : byte + '7';

	*destination++ = (( byte = (*source++) & 0x0f) <= 9) ? byte + '0' : byte + '7';

	}

}
#endif
 /* Function to convert hexadecimal to decimal */
int hex_decimal(char hex[])
{
    int i, length, sum=0;
    for(length=0; hex[length]!='\0'; ++length);
    for(i=0; hex[i]!='\0'; ++i, --length)
    {
        if(hex[i]>='0' && hex[i]<='9')
            sum+=(hex[i]-'0')*pow(16,length-1);
        if(hex[i]>='A' && hex[i]<='F')
            sum+=(hex[i]-55)*pow(16,length-1);
        if(hex[i]>='a' && hex[i]<='f')
            sum+=(hex[i]-87)*pow(16,length-1);
    }
    return sum;
}

/*
 * ============================================================================
 * End of file emvMsgBuilder.c
 * ============================================================================
 */
