/******************************************************************
*                     uiAPIs.c                                 *
* ================================================================*
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <libsecins.h>
#include <arpa/inet.h>

#include "svc.h"
#include "common/common.h"
#include "common/utils.h"
#include "common/tranDef.h"
#include "uiAgent/guimgr.h"
#include "uiAgent/ctrlids.h"
#include "uiAgent/uiAPIs.h"
#include "uiAgent/emvAPIs.h"
#include "uiAgent/uiMsgFmt.h"
#include "uiAgent/uiControl.h"
#include "uiAgent/uiCfgDef.h"
#include "uiAgent/uiCfgData.h"
#include "bLogic/bLogic.h"
#include "bLogic/bLogicCfgDef.h"
#include "bLogic/blAPIs.h"
#include "ssi/ssiDef.h"
#include "ssi/ssiHostCfg.h"
#include "ssi/ssiCfgDef.h"
#include "ssi/ssiMain.h"
#include "sci/sciConfigDef.h"
#include "appLog/appLogAPIs.h"
#include "db/tranDS.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

/* Extern functions */
extern int sendUIReqMsg(char *);
extern int sendUIReqMsg_EX(char *, int);
extern int getPinPadMode();

int 					giEnvCommInProgress;
extern PAAS_BOOL checkBinInclusion(char *);

/* Extern variables */
extern int				giLabelLen;
extern int				giVolIncBtn;
extern int				giVolDcrBtn;
extern int				giImgCtrl;
extern int				giVidCtrl;
extern int				giImgUpdIntvl;
extern int 				whichForm;
extern int				giFACommInProgress;
extern char *			frmName[];
extern PAAS_BOOL		bBLDataRcvd;
extern UIRESP_STYPE		stBLData;

#define MAX_LI_BUFFER	45 //Praveen_P1: 28 March 2016 - Increased the buffer count to 45 from 30

/* Static variables */
//static UIREQ_STYPE		stUIReq;

//static char *			pszLIMsg[MAX_LI_BUFFER];
LI_BUFFER_QUEUE_STYPE liBufferHead;

static pthread_mutex_t  gptLIBufferMsgMutex; //Praveen_P1: Adding this mutex for the LINE item buffer, adding is done by online transaction, reading/clearing is done by BLOGIC
static pthread_mutex_t  gptLITailBufferMutex;
static pthread_mutex_t  gptUIReqMsgBuffer;
static pthread_mutex_t 	gptUpdateLIScreen = PTHREAD_MUTEX_INITIALIZER;


extern int getDevAdminRqdStatus();
extern int storePreSwipeCardDtls(CARDDTLS_PTYPE, PAAS_BOOL );
extern int getQRCodeDtlsFromSession(DISPQRCODE_DTLS_PTYPE);
/* Static functions declarations */
static void setLanClosedText(char *, LANECLOSED_PTYPE);
static int getPAN(char *);
static int getCVV(char *);
static int getUISysInfo(SYSINFO_PTYPE);
static int getExpiryDate(char *, char *);
static int getGCManData(char *, char *, char *, char*);
static int getFormattedLabels(char [8][50], PAAS_BOOL);
static int displaySysInfo(SYSINFO_PTYPE, PAAS_BOOL, char *, int);
static int getManualCardData(int, char *, char *, char *, char *, char*, char*, char **, char*, char*, char*);
static void appendSpacesToString(char *, int);
static void getFileNamewithPlatformPrefix(char *);
static int getOptionsForCtrlid(int, char*);
static int captureCardDtlsForPreswipe();
//static int getSelectedRadioOptForCtrlid (int , char * , int *);
//static int getSelectedSurveyOption(int *, int );
static int setScreenTitle(char* , char*, int*, int, int, char*);
static int setCreditAppScreenTitle(char* , int*, int, int, char*);
static int createDisplayString(char *frmtStr, char * dispStr);
static int createFormatString(char* promptFrmtStr, char* finalFrmtStr);
static int createInputStrWithLiterals(PROMPTDTLS_PTYPE pstPromptDtls);
static int broadcastTerminalIdentification(char *, char *,  char *, char *);
static int broadcastTermIdenAndWaitForPOSACK(char *, char *,  char *, char *);
static int updatePOSACKIntvSecsInScreen(char *);
static int getCardDtlsForDevCmd(GET_CARDDATA_PTYPE);
static int bufferFailedLICmds(char *,XBATCH_PTYPE,LI_BUFFER_NODE_PTYPE);

int splitStringOfReqLength(char*, int, LABEL_PTYPE_TITLE);



/*
 * ============================================================================
 * Function Name: initglobUIReqMutex
 *
 * Description	: Initializes the Global UI req mutex, which is used while sending any message to UI agent
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
int initglobUIReqMutex()
{
	int				rv			 = SUCCESS;

#ifdef DEBUG
	char szDbgMsg[512]		= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pthread_mutex_init(&gptUIReqMsgBuffer, NULL))//Initialize the AllowLITran Mutex
	{
		debug_sprintf(szDbgMsg, "%s - Failed to create Line Item Buffer mutex!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: acquireGlobUIReqMutex
 *
 * Description	: Acquired while sending any message to UI agent or while doing sig jmp
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
void acquireGlobUIReqMutex(int iLineNum,char * pszCallerFuncName)
{
	#ifdef DEBUG
		char szDbgMsg[256]		= "";
	#endif

		debug_sprintf(szDbgMsg, "%s: Acquiring Global UI Mutex Lock called by [%s] from Line[%d]",__FUNCTION__,pszCallerFuncName, iLineNum);
		APP_TRACE(szDbgMsg);

		acquireMutexLock(&gptUIReqMsgBuffer, "Global UI Request");

		debug_sprintf(szDbgMsg, "%s: Global UI Mutex Acquired by [%s] from Line[%d]",__FUNCTION__,pszCallerFuncName, iLineNum);
		APP_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: releaseGlobUIReqMutex
 *
 * Description	: released after sending any message to UI agent or after doing sig jmp
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
void releaseGlobUIReqMutex(int iLineNum,char * pszCallerFuncName)
{
	#ifdef DEBUG
		char szDbgMsg[256]		= "";
	#endif

		debug_sprintf(szDbgMsg, "%s: Releasing Global UI Mutex Lock called by [%s] from Line[%d]",__FUNCTION__,pszCallerFuncName, iLineNum);
		APP_TRACE(szDbgMsg);

		releaseMutexLock(&gptUIReqMsgBuffer, "Global UI Request");

}

/*
 * ============================================================================
 * Function Name: initUIAPIs
 *
 * Description	:
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
void initUIAPIs()
{
//	memset(&stUIReq, 0x00, sizeof(UIREQ_STYPE));
}

/*
 * ============================================================================
 * Function Name: initUIAgent
 *
 * Description	: This API sets all the required parameters in the UI Agent
 * 					application.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int initUIAgent()
{
	int				rv									 = SUCCESS;
	int				startX 								 = 0;
	int				startY 								 = 0;
	int				endX   								 = 0;
	int				endY   								 = 0;
	int				iLen								 = 0;
	char			szTemp[10]							 = "";
	char			szStartX[4]							 = "";
	char			szStartY[4]							 = "";
	char			szEndX[4]							 = "";
	char			szEndY[4]							 = "";
	char			szParamName[30] 					 = "";
	char			szParamValue[MAX_DISP_TITLE_SIZE]    = "";
	char			szCommand[256]						 = "";
	PAAS_BOOL		bWait								 = PAAS_TRUE;
	PAAS_BOOL 		bSTBEnaled							 = PAAS_FALSE;
	BTNLBL_STYPE	stBtnLbl;
	UIREQ_STYPE		stUIReq;

	#ifdef DEBUG
		char szDbgMsg[512]		= "";
	#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/*
		 * This is Mutex is created for static global variable pszLIMsg
		 * This variable is shared across threads(Online and BLogic threads)
		 * so to prevent the variable, making it mutable
		 * Onlinetransaction fills up this buffer, BLogic thread clears this buffer
		 */
		if(pthread_mutex_init(&gptLIBufferMsgMutex, NULL))//Initialize the AllowLITran Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create Line Item Buffer mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		if(pthread_mutex_init(&gptLITailBufferMutex, NULL))//Initialize the AllowLITran Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create Line Item Buffer mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/*
		 * Initializing LI Message Buffer to NULL
		 */
		/*
		for(iIndex = 0; iIndex < MAX_LI_BUFFER; iIndex++)
		{
			pszLIMsg[iIndex] = NULL;
		}
		 */
		liBufferHead.bufferCnt = 0;
		liBufferHead.headNode = NULL;
		liBufferHead.tailNode = NULL;

		iLen = sizeof(szTemp);
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile("STB", "stblogicenabled", szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'Y') || (*szTemp == 'y'))
			{
				bSTBEnaled = PAAS_TRUE;
			}
			else
			{
				bSTBEnaled = PAAS_FALSE;
			}
		}

		initUIReqMsg(&stUIReq, SETCFGREQ);

		/*
		 * Need to set the SIGBOX dimensions
		 */

		/* Getting the signature capture box coordinates */
		getSigCapCoordinates(&startX, &startY, &endX, &endY);

		sprintf(szStartX, "%03d", startX);
		sprintf(szStartY, "%03d", startY);
		sprintf(szEndX, "%03d", endX);
		sprintf(szEndY, "%03d", endY);

		memset(szParamValue, 0x00, sizeof(szParamValue));
		sprintf(szParamValue, "%s%s%s%s", szStartX, szStartY, szEndX, szEndY);

		memset(szParamName, 0x00, sizeof(szParamName));
		strcpy(szParamName, "SIGBOX");

		debug_sprintf(szDbgMsg, "%s: Parameter Name [%s]  = Parameter Value [%s]", __FUNCTION__,
																szParamName, szParamValue);
		APP_TRACE(szDbgMsg);

		setCfgVarWrapper(szParamName, szParamValue, stUIReq.pszBuf);

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_XSETVAR_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			}
		}

		debug_sprintf(szDbgMsg, "%s: Successfully set the SIGBOX parameter", __FUNCTION__);
		APP_TRACE(szDbgMsg);

#if 0
		/*
		 * Setting the PIN screen Title
		 */

		/* Get the Title from .ini file */
		memset(szParamValue, 0x00, sizeof(szParamValue));
		fetchScrTitle(szParamValue, ENTER_PIN_TITLE);

		memset(szParamName, 0x00, sizeof(szParamName));
		strcpy(szParamName, "*pinscrtitle");

		debug_sprintf(szDbgMsg, "%s: Parameter Name [%s]  = Parameter Value [%s]", __FUNCTION__, szParamName, szParamValue);
		APP_TRACE(szDbgMsg);

		memset(szCommand, 0x00, sizeof(szCommand));
		sprintf(szCommand, "M45%c%d=%s%c", FS, 6, szParamValue, RS);

		debug_sprintf(szDbgMsg, "%s: Command to set PIN Title [%s]", __FUNCTION__, szCommand);
		APP_TRACE(szDbgMsg);

		//setCfgVarWrapper(szParamName, szParamValue, stUIReq.pszBuf);

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		else
		{
#if 0
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_XSETVAR_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(10);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			}
#endif
		}
#endif
		/*
		 * Setting the PIN screen Button Label
		 */


		memset(szParamValue, 0x00, sizeof(szParamValue));
		memset(szParamName, 0x00, sizeof(szParamName));

		strcpy(szParamName, "*pinbtnlbl");

		/*
		 * If STB logic is enabled, then we have to change the text
		 * for the button label on the pin entry screen.
		 * in non-stb case we show Verify with Signature
		 * if STB is enabled then we have to show press to continue
		 */
		if(bSTBEnaled)
		{
			/* Get the Label from .ini file */
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_26); //Need to make it 26 later
			strcpy(szParamValue, (char *)stBtnLbl.szLblOne);

			debug_sprintf(szDbgMsg, "%s: STB Logic is enabled, setting the button label on the PIN entry screen with %s", __FUNCTION__, szParamValue);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			/* Get the Label from .ini file */
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_11);
			strcpy(szParamValue, (char *)stBtnLbl.szLblTwo);

			debug_sprintf(szDbgMsg, "%s: STB Logic is disabled, setting the button label on the PIN entry screen with %s", __FUNCTION__, szParamValue);
			APP_TRACE(szDbgMsg);
		}

		debug_sprintf(szDbgMsg, "%s: Parameter Name [%s]  = Parameter Value [%s]", __FUNCTION__, szParamName, szParamValue);
		APP_TRACE(szDbgMsg);

		memset(szCommand, 0x00, sizeof(szCommand));
		sprintf(szCommand, "%cM45%c%s=%s%c", STX, FS, "162EN", szParamValue, RS);

		debug_sprintf(szDbgMsg, "%s: Command to set Button Label on Pin Entry screen [%s]", __FUNCTION__, szCommand);
		APP_TRACE(szDbgMsg);

		//setCfgVarWrapper(szParamName, szParamValue, stUIReq.pszBuf);

		bWait = PAAS_TRUE;
		rv = sendUIReqMsg(szCommand);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			}
		}

		debug_sprintf(szDbgMsg, "%s: Successfully set the PIN Button Label", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showWelcomeScreen
 *
 * Description	: This API would be called to  show welcome screen
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showWelcomeScreen(PAAS_BOOL bDupTranDetectedflg)
{
	int     			rv								= SUCCESS;
	int					locForm							= 0;
	int					iDisableFormFlag				= 0;
	int					ctrlIds[2];
	int					iAppLogEnabled					= isAppLogEnabled();
	int					iEmvCrdPresence					= 0;
	char				szTitle[100]					= "";
	char				szAppLogData[256]				= "";
	char				szMerchIndex[10+1]				= "";
//	char				szFileName[MAX_FILE_NAME_LEN]	= "";
//	char				advtFile[128]					= "";
	char				szTmp[100]		 				= "";
	PAAS_BOOL			bWait							= PAAS_TRUE;
	PAAS_BOOL			bCaptureCardDtls				= PAAS_FALSE;
	PAAS_BOOL			bEmvEnabled						= PAAS_FALSE;
	PAAS_BOOL			bCardReadEnabled				= PAAS_FALSE;
    LABEL_STYPE_TITLE   stLblTitle;
	UIREQ_STYPE			stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(IN_SESSION_WELCOME);


	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	if(getXPILibMode())
	{
		// 10-Dec-15: MukeshS3: Using this 'bCardReadEnabled' variable to check whether the card reader is already
		// enabled or not.
		bCardReadEnabled = getCardReadEnabled();
		//03-Feb-16 : AkshayaM1: PTMX-809 :: The issue is when we user Press cancel after getting the VAS data.
		//Card Reader are Enabled with Non Contactless Payment mode in Phone
		//So , We need to disable the Card Readers and Enabled with VAS only Mode in C30 or S20
		if(bCardReadEnabled == PAAS_TRUE && isWalletEnabled())
		{
			disableCardReaders();
			bCardReadEnabled = getCardReadEnabled();
		}
	}
	else
	{
		disableCardReaders();	// FIXME: MukeshS3: This looks redundant here. Please remove if it is not required in main branch.
	}

	if(bDupTranDetectedflg == PAAS_TRUE)
	{
		locForm = WELCOME_DUP_DISP_FRM;
	}
	else
	{
		locForm = WELCOME_DISP_FRM;
	}

	/* initialize the form */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Get the main title of the display screen */
	fetchScrTitle(szTitle, WELCOME_TITLE);

	memset(ctrlIds, -1, sizeof(ctrlIds));

	ctrlIds[0] 	= PS_WELCOME_SCREEN_LBL_1;
	ctrlIds[1] 	= PS_WELCOME_SCREEN_LBL_2;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, NULL, ctrlIds, 2, giLabelLen, stUIReq.pszBuf);


	if( ( (isSwipeAheadEnabled() == PAAS_TRUE) && (isPreSwipeDone() == PAAS_FALSE) ) &&
		(bDupTranDetectedflg == PAAS_FALSE) && (isOnWelcomeScreenAfterVAS() == PAAS_FALSE) ) //Pre-Swipe is not done
	{
		debug_sprintf(szDbgMsg, "%s: Swipe Ahead is enabled, adding card read command to the request", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//Get EMV status
		bEmvEnabled = isEmvEnabledInDevice();

		memset(szTmp, 0x00, sizeof(szTmp));
		/* Get the display prompt/message from the .ini file */
		if(bEmvEnabled == PAAS_TRUE)
		{
			iEmvCrdPresence = EMVCrdPresenceOnPreswipe();
			if(iEmvCrdPresence == 1)
			{
				getDisplayMsg(szTmp, MSG_AFTER_PRE_SWIPE_EMV);
			}
			else
			{
				getDisplayMsg(szTmp, MSG_PRESWIPE_EMV);
				bCaptureCardDtls = PAAS_TRUE;
			}
		}
		else
		{
			getDisplayMsg(szTmp, MSG_PRE_SWIPE);
			bCaptureCardDtls = PAAS_TRUE;
		}

		memset(&stLblTitle, 0x00, sizeof(stLblTitle));
		//Split title string of required length(i.e Based on maxLabelLength defined for Mx915 or Mx925).
		splitStringOfReqLength(szTmp, giLabelLen-5, &stLblTitle);

		/* Set the label for the After-Pre-Swipe message on the screen */
		setStringValueWrapper(PS_WELCOME_SCREEN_LBL_3, PROP_STR_CAPTION, stLblTitle.szTitle1, stUIReq.pszBuf);
		if( strlen(stLblTitle.szTitle2) > 0 )
		{
			/* Set the label for the Before-Pre-Swipe message on the screen */
			setStringValueWrapper(PS_WELCOME_SCREEN_LBL_4, PROP_STR_CAPTION, stLblTitle.szTitle2, stUIReq.pszBuf);
		}

		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf); //Sending the XIFM before Q13 to avoid complications

	}
	else if( (bDupTranDetectedflg == PAAS_TRUE) || (isOnWelcomeScreenAfterVAS()) ||
			 (( (isSwipeAheadEnabled() == PAAS_TRUE) && (isPreSwipeDone() == PAAS_TRUE) ) &&
		         (bDupTranDetectedflg == PAAS_FALSE)) )
	{
		debug_sprintf(szDbgMsg, "%s: Pre-Swipe Done/duplicate transaction detected/waiting after sending VAS or Early Card data to POS, Waiting for cashier", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/*if only card data is captured then we need to send D41 to get VAS data*/
		if(!(bDupTranDetectedflg == PAAS_TRUE || isOnWelcomeScreenAfterVAS()))
		{
			if(PAAS_TRUE == isWalletEnabled() && PAAS_TRUE != isVASDataCaptured())
			{
				debug_sprintf(szDbgMsg, "%s: sending D41 to get VAS data", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				memset(szMerchIndex, 0x00, sizeof(szMerchIndex));
				getMerchantIndexIfAvaliable(szMerchIndex);
				sendD41cmdToEMVAgent(PAAS_FALSE, szMerchIndex, NULL);
			}
		}

		memset(szTmp, 0x00, sizeof(szTmp));
		/* Get the display prompt/message from the .ini file */
		/* Get the display prompt/message from the .ini file */
		if(EMVCrdPresenceOnPreswipe() == 1)
		{
			/* Set the label for the After-Pre-Swipe message on the screen */
			getDisplayMsg(szTmp, MSG_AFTER_PRE_SWIPE_EMV);
		}
		else
		{
			getDisplayMsg(szTmp, MSG_AFTER_PRE_SWIPE);
		}

		memset(&stLblTitle, 0x00, sizeof(LABEL_STYPE_TITLE));
		//Split title string of required length(i.e Based on maxLabelLength defined for Mx915 or Mx925).
		splitStringOfReqLength(szTmp, giLabelLen-5, &stLblTitle);

		/* Set the label for the After-Pre-Swipe message on the screen */
		setStringValueWrapper(PS_WELCOME_SCREEN_LBL_3, PROP_STR_CAPTION, stLblTitle.szTitle1, stUIReq.pszBuf);
		if( strlen(stLblTitle.szTitle2) > 0 )
		{
			/* Set the label for the After-Duplicate-Detected message on the screen */
			setStringValueWrapper(PS_WELCOME_SCREEN_LBL_4, PROP_STR_CAPTION, stLblTitle.szTitle2, stUIReq.pszBuf);
		}
	
		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);
	}
	else //Session start
	{
		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);
	}

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
															__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		whichForm		= locForm;

		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			svcWait(5);
		}
		if(bCaptureCardDtls == PAAS_TRUE)
		{
			if((getXPILibMode() && (!bCardReadEnabled)) || !getXPILibMode())
			{
				rv= captureCardDtlsForPreswipe();//Send Commands to XPI for card data capture
				bCaptureCardDtls = PAAS_FALSE;
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to send XPI cmds",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					return ERR_DEVICE_APP;
				}
			}
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Welcome Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showIdleScreen
 *
 * Description	: This API would be called to  show idle screen
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showIdleScreen()
{
	int    			rv					= SUCCESS;
	int				locForm				= 0;
	int				iDisableFormFlag	= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	int				idleScrnMode		= DEFAULT_IDLE;
	short			iAnimUpdtIntv		= DFLT_ANIM_UPDT_INTV;
	char			advtFile[128]		= "";
	char			szAppLogData[256]	= "";
	//char			szTmp[100]			= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(rv == SUCCESS)
	{
		if(isSessionInProgress())
		{
			setAppState(IN_SESSION_IDLE);
		}
		else
		{
			setAppState(NO_SESSION_IDLE);
		}
	}

	/* If the current form being displayed is already idle screen, no need to
	 * initialize and show the form again. */
	if((whichForm != IDLE_DISP_FRM)  && (whichForm != IDLE_DISP_IMG_FRM)  && (whichForm != IDLE_DISP_VID_FRM)  && (whichForm != IDLE_DISP_ANIM_FRM))
	{
		/*
		 * We enable MSR on the line item screen when preswipe is enabled
		 * if card is not swiped on that screen and we navigate to some other screen
		 * we need to reset so that MSR is disabled
		 */
		disableCardReaders();

		/* initialize the form */
		initUIReqMsg(&stUIReq, BATCH_REQ);

		idleScrnMode = getIdleScrnMode();
		switch(idleScrnMode)
		{
		case DEFAULT_IDLE:			// showing the Default Idle Screen with video and Image banner
			locForm = IDLE_DISP_FRM;
			initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);

			/* Set the image for banners */
			if(SUCCESS == getNxtImageFileName(advtFile, locForm))
			{
				setStringValueWrapper(PAAS_IDLESCREEN_IMG_1,PROP_STR_IMAGE_FILE_NAME
													, advtFile, stUIReq.pszBuf);
			}

			/* Set the video for advertisement */
			if(SUCCESS == getNxtVideoFileName(advtFile))
			{
				setStringValueWrapper(PAAS_IDLESCREEN_VID_1,PROP_STR_VIDEO_FILE_NAME
													, advtFile, stUIReq.pszBuf);
			}
			break;

		case FULL_IDLE_IMG:			// showing the Image on full Idle screen.
			locForm = IDLE_DISP_IMG_FRM;
			initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);

			break;

		case FULL_IDLE_VID:			// showing the video on full Idle screen.
			locForm = IDLE_DISP_VID_FRM;
			initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);

			break;

		case FULL_IDLE_ANIM:			// showing the Animation on full Idle screen.
			locForm = IDLE_DISP_ANIM_FRM;
			initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);

			iAnimUpdtIntv = getImageUpdateIntvl(locForm) * 1000; // convert to millisecs

			setShortValueWrapper(PAAS_IDLESCREEN_ANI_ANI_1, PROP_SHORT_ANIM_DELAY_INTERVAL, iAnimUpdtIntv, stUIReq.pszBuf);
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should never come here!!!",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			//rv = FAILURE; CID:67201:T_POLISETTYG1: Assigning Failure which has no effect. so commented it.
			break;
		}

		/* Show the form on the screen */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

#if 0
		/* Get the current volume controls from the UI agent */
		getCurVolWrapper(stUIReq.pszBuf);
#endif

		bBLDataRcvd = PAAS_FALSE;//Praveen_P1: Setting to false before sending the request to make sure that we wait for the response which we are sending

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: bWait = %d, bBLDataRcvd = %d", __FUNCTION__, bWait, bBLDataRcvd);
			APP_TRACE(szDbgMsg);

			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);
			}

			/* Set the media controls and buttons for any future updates */
			if(idleScrnMode == DEFAULT_IDLE)
			{
				giVolIncBtn		= PAAS_IDLESCREEN_BTN_1;
				giVolDcrBtn		= PAAS_IDLESCREEN_BTN_2;
				giImgCtrl		= PAAS_IDLESCREEN_IMG_1;
				giVidCtrl		= PAAS_IDLESCREEN_VID_1;
			}

			giImgUpdIntvl	= getImageUpdateIntvl(locForm);
			whichForm		= locForm;
			memset(szAppLogData, 0x00, sizeof(szAppLogData));
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Showing Idle Screen");
				addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showLaneClosedScreen
 *
 * Description	: This API would be called to  show lane closed screen
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showLaneClosedScreen(LANECLOSED_PTYPE pstLaneDtls)
{
	int     		rv					= SUCCESS;
	int				locForm				= 0;
	int				iDisableFormFlag	= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	int				idleScrnMode		= DEFAULT_IDLE;
	int				iFontSize			= 0;
	int				iPlatForm			= -1;
	char			szFontCol[10]		= "";
	char			advtFile[128]		= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	locForm = LANE_CLOSED_FRM;

	if(strlen(pstLaneDtls->szFontColor) == 0)
	{
		strcpy(pstLaneDtls->szFontColor, getLaneClosedFontCol());
	}

	if(pstLaneDtls->iFontSize == 0)
	{
		pstLaneDtls->iFontSize = getLaneClosedFontSize();
	}


	/* initialize the form */
	initUIReqMsg(&stUIReq, BATCH_REQ);

	idleScrnMode = getIdleScrnMode();
	switch(idleScrnMode)
	{
	case DEFAULT_IDLE:
		locForm = LANE_CLOSED_FRM;
		initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);
		if(strlen(pstLaneDtls->szDisplayText) > 0)
		{
			if(strlen(pstLaneDtls->szFontColor) > 0)
			{
				memset(szFontCol, 0x00, sizeof(szFontCol));
				sprintf(szFontCol, "%c%s%c", '#', pstLaneDtls->szFontColor, '#');

				setLongValueWrapper(PAAS_LANECLOSED_SCREEN_LBL_2, PROP_LONG_FG_COLOR, szFontCol, stUIReq.pszBuf);

				if(strcmp(pstLaneDtls->szFontColor, getLaneClosedFontCol()) != SUCCESS)
				{
					setLaneClosedFontCol(pstLaneDtls->szFontColor);
				}
			}

			if(pstLaneDtls->iFontSize > 0)
			{
				iFontSize = pstLaneDtls->iFontSize;
			    iPlatForm = getDevicePlatform();
				if(iPlatForm == MODEL_MX925)
				{
				    if(iFontSize < 8 || iFontSize > 72)
				    {
				    	iFontSize = 32;
				    }
				}
				else if(iPlatForm == MODEL_MX915)
				{
					if(iFontSize < 8 || iFontSize > 72)
				    {
				    	iFontSize = 24;
				    }
				}
				setShortValueWrapper(PAAS_LANECLOSED_SCREEN_LBL_2, PROP_SHORT_FONT_SIZE, iFontSize, stUIReq.pszBuf);

				if(iFontSize != getLaneClosedFontSize())
				{
					setLaneClosedFontSize(iFontSize);
				}
			}

			setStringValueWrapper(PAAS_LANECLOSED_SCREEN_LBL_2, PROP_STR_CAPTION, pstLaneDtls->szDisplayText, stUIReq.pszBuf);
		}
		/* Set the video for advertisement */
		if(SUCCESS == getNxtVideoFileName(advtFile))
		{
			setStringValueWrapper(PAAS_LANECLOSED_SCREEN_VID_1, PROP_STR_VIDEO_FILE_NAME, advtFile, stUIReq.pszBuf);
		}
		break;

	case FULL_IDLE_IMG:
		locForm = IDLE_DISP_IMG_FRM;
		initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);

		break;

	case FULL_IDLE_VID:
		locForm = IDLE_DISP_VID_FRM;
		initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);

		break;

	case FULL_IDLE_ANIM:
		locForm = IDLE_DISP_ANIM_FRM;
		initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);

		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should never come here!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;
	}

	/* Show the form on the screen */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	if((idleScrnMode == FULL_IDLE_IMG)  || (idleScrnMode == FULL_IDLE_ANIM)) //XDTX will be done on the screen immediately, it is better to show after show form so that new form is shown and then message on that form
	{
		setLanClosedText(stUIReq.pszBuf, pstLaneDtls);
	}

#if 0
	/* Get the current volume controls from the UI agent */
	getCurVolWrapper(stUIReq.pszBuf);
#endif

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = ERR_DEVICE_APP;
	}
	else
	{
		//Note: Don't wait for UI response here since processlocally function will handle this case.
		
		debug_sprintf(szDbgMsg, "%s: bWait = %d, bBLDataRcvd = %d", __FUNCTION__, bWait, bBLDataRcvd);
		APP_TRACE(szDbgMsg);
	
		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);
		}
		
		
		/* Set the media controls and buttons for any future updates */
		if(idleScrnMode == DEFAULT_IDLE)
		{
			giVolIncBtn		= PAAS_LANECLOSED_SCREEN_BTN_1;
			giVolDcrBtn		= PAAS_LANECLOSED_SCREEN_BTN_2;;
			giVidCtrl		= PAAS_LANECLOSED_SCREEN_VID_1;
		}
		whichForm		= locForm;
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Lane Closed Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setLanClosedText
 *
 * Description	: This API would get the Lane Closed Text for Display onto the screen.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void setLanClosedText(char * szBuf, LANECLOSED_PTYPE pstLaneDtls)
{
    char    tempMsg[400]  	= "";
    char	szFontColor[30]	= "";
    int		iFontSize		= 0;
    int		iPlatForm		= -1;
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

    memset(tempMsg, 0x00, sizeof(tempMsg));

    if(pstLaneDtls == NULL || strlen(pstLaneDtls->szDisplayText) == 0)
    {
    	debug_sprintf(szDbgMsg, "%s: No Lane Closed Details", __FUNCTION__);
    	APP_TRACE(szDbgMsg);

    	return;
    }

    memset(szFontColor, 0x00, sizeof(szFontColor));
    if(strlen(pstLaneDtls->szFontColor) > 0)
    {
    	sprintf(szFontColor, "FFFFFF|%s|FFFF00", pstLaneDtls->szFontColor);
    	if(strcmp(pstLaneDtls->szFontColor, getLaneClosedFontCol()) != SUCCESS)
    	{
    		setLaneClosedFontCol(pstLaneDtls->szFontColor);
    	}
    }
    else
    {
    	strcpy(szFontColor, "FFFFFF|FF0000|FFFF00");
    }
    iFontSize = pstLaneDtls->iFontSize;
   /*
    * <STX>XDTX<FS>X<FS>Y<FS>WrapRow<FS>WrapColumn<FS>OptionsMask<FS>FontName<FS>FontSize<FS>Text[<FS>Colors]<ETX>{LRC}
   */

    iPlatForm = getDevicePlatform();
	if(iPlatForm == MODEL_MX925)
	{
	    if(iFontSize < 8 || iFontSize > 72)
	    {
	    	iFontSize = 32;
	    }
		sprintf(tempMsg, "XDTX%c%d%c%d%c%d%c%d%c%d%c%s%c%d%c%s%c%s%c",
												  FS, 0/*X*/, FS, 368/*Y*/, FS, 110/*Wraprow*/, FS, 797/*Wrapcolumn*/, FS, 32769/*Options*/, FS,  "VeraMono|VeraMoBd|VeraMoIt|VeraMoBI" /*Font Name*/,
												  FS, iFontSize/*Font Size*/, FS, pstLaneDtls->szDisplayText/* Text */, FS, szFontColor /*Font color*/, RS);
	}
	else if(iPlatForm == MODEL_MX915)
	{
		if(iFontSize < 8 || iFontSize > 72)
	    {
	    	iFontSize = 24;
	    }
		sprintf(tempMsg, "XDTX%c%d%c%d%c%d%c%d%c%d%c%s%c%d%c%s%c%s%c",
												  FS, 0/*X*/, FS, 220/*Y*/, FS, 49/*Wraprow*/, FS, 478/*Wrapcolumn*/, FS, 32769/*Options*/, FS,  "VeraMono|VeraMoBd|VeraMoIt|VeraMoBI" /*Font Name*/,
												  FS, iFontSize/*Font Size*/, FS, pstLaneDtls->szDisplayText/* Text */, FS, szFontColor /*Font color*/, RS);
	}

    strcat(szBuf, tempMsg);

	if(iFontSize != getLaneClosedFontSize())
	{
		setLaneClosedFontSize(iFontSize);
	}
    return;
}


/*
 * ============================================================================
 * Function Name: getAppVersionInfo
 *
 * Description	: This API would get the version of applications.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getAppVersionInfo(SYSINFO_PTYPE pstSysInfo)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char*			pszDHIVersion		= NULL;
	char*			pszEMVVersion		= NULL;
	char			szAppLogData[256]	= "";

#ifdef DEBUG
	char			szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(pstSysInfo, 0x00, sizeof(SYSINFO_STYPE));

	while(1)
	{
		/* Get the UI agent and other system details from the UI agent */
		rv = getUISysInfo(pstSysInfo);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get VER from UI agent",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(isEmvEnabledInDevice() == PAAS_TRUE) //Check whether EMV is enabled or not
		{
			pszEMVVersion = getEMVVersion();
			if(strlen(pszEMVVersion) > 0)
			{
				memset(szAppLogData, 0x00, sizeof(szAppLogData));
				sprintf(szAppLogData, "XPI Application Version [%s]", pszEMVVersion);
				if(iAppLogEnabled == 1)
				{
					addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
				}
				strcpy(pstSysInfo->szXPIVer, pszEMVVersion);
			}
		}

		if(isDHIEnabled() == PAAS_TRUE)
		{
			pszDHIVersion = getDHIVersionInfo();
			if(strlen(pszDHIVersion) > 0)
			{
				memset(szAppLogData, 0x00, sizeof(szAppLogData));
				sprintf(szAppLogData, "DHI Application Version [%s]", pszDHIVersion);
				if(iAppLogEnabled == 1)
				{
					addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
				}
				strcpy(pstSysInfo->szDHIVer, pszDHIVersion);
				pstSysInfo->iDHIBuild = getDHIBuildNum();
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showSystemInformation
 *
 * Description	: This API would show the system information on the screen.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showSystemInformation(PAAS_BOOL bDHCP, char * pszIP, int listngPortNum)
{
	int				rv				= SUCCESS;
	char*			pszEMVVersion	= NULL;
	SYSINFO_STYPE	stSysInfo;

#ifdef DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&stSysInfo, 0x00, sizeof(SYSINFO_STYPE));

	while(1)
	{
		/* Get the UI agent and other system details from the UI agent */
		rv = getUISysInfo(&stSysInfo);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get VER from UI agent",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(isEmvEnabledInDevice() == PAAS_TRUE) //Check whether EMV is enabled or not
		{
			//check whether XPI App is running or not by sending S95 xpi command.
			pszEMVVersion = getEMVVersion();
			strcpy(stSysInfo.szXPIVer, pszEMVVersion);
		}

		if(isDHIEnabled() == PAAS_TRUE)
		{
			//check whether DHI App is running and if so get the version
			rv = getDHIVersionFromHost(&stSysInfo);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get DHI version. DHI is not up", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		/* Need to show this information on the device screen for the person
		 * operating the device */
		rv = displaySysInfo(&stSysInfo, bDHCP, pszIP, listngPortNum);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to display sys info on screen",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getConfigVarWrapper
 *
 * Description	: This API willget the config value from the formagent
 * 				  by sending the XGCD command to the FA
 *
 * Input Params	: Config Variables names, Values of the corresponding variables to be stored
 * 				  Total count of the number of the variables sent.
 *
 * Output Params: ENUM type value VSP, RSA, NO_ENC
 * ============================================================================
 */
int getConfigVarWrapper(char** pszVariables, char** pszConfigValues, unsigned iCount)
{
	int				i				= 0;
	int				rv				= SUCCESS;
	char			szCmd[50]		= "";
	PAAS_BOOL		bWait			= PAAS_TRUE;
	XGETVAR_PTYPE	pstConfigInfo	= NULL;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if (iCount > MAX_XGCD_PARAMS)
	{
		debug_sprintf(szDbgMsg, "%s: Maximum paramaeter limit 5 exceeded", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, GETCFGREQ);

	/*
	 * Forming the XGCD command
	 * Format:XGETVAR<FS>Parameter1<FS>Parameter2<FS>...<FS>ParameterN<ETX>
	 */
	for(i = 0; i < iCount; i++)
	{
		sprintf(szCmd, "%s%c", pszVariables[i], FS);
		strcat(stUIReq.pszBuf, szCmd);
	}

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		/* Wait for the user input to get the selected cashback amt */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XGETVAR_RESP)
				{
					pstConfigInfo = &(stBLData.stRespDtls.stConfigInfo);

					for(i = 0; i < iCount; i++)
					{
						pszConfigValues[i] = pstConfigInfo->szCfgValues[i];
					}
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCBackAmtOptFrmScrn
 *
 * Description	: This API would be used to display the Cash Back Amount Entry
 * 					screen once the user chooses the Other option in Select
 * 					Cashback screen.
 *
 * Input Params	: ppszCashBack -- List of cash back options.
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getCBackAmtOptFrmScrn(int * iCashBckAmts, int iTotCnt, int *piSelectedOpt)
{
	int				rv					= SUCCESS;
	int				iCnt				= 0;
	int				iSelOpt				= 0;
	int				iStartBtn			= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTmp[100]			= "";
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	int 			ctrlIds[4];
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	//MukeshS3: PTMX-1587
	if(iTotCnt == 0)
	{
		debug_sprintf(szDbgMsg, "%s: Cashback Preset Amount is not configured; Need to capture cashback amount manually", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return UI_OTHER_SELECTED;
	}

	whichForm = SEL_CBACK_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTitle, SELECT_CBACK_TITLE);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_CB_AMOUNT_SCREEN_LBL_1;
	ctrlIds[1] 	= PAAS_CB_AMOUNT_SCREEN_LBL_12;
	ctrlIds[2] 	= PAAS_CB_AMOUNT_SCREEN_LBL_13;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);

	/* Set the button labels */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_10);

	/* Set the label for "Other" button */
	setStringValueWrapper(PAAS_CB_AMOUNT_SCREEN_LBL_2, PROP_STR_CAPTION,
								(char *) stBtnLbl.szLblOne, stUIReq.pszBuf);
								
	/* Set the label for "None" button */
	setStringValueWrapper(PAAS_CB_AMOUNT_SCREEN_LBL_3, PROP_STR_CAPTION,
								(char *) stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Set the initial label and buttons for the list */
	iStartBtn = PAAS_CB_AMOUNT_SCREEN_BTN_3;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		/* Set the button as visible */
		setBoolValueWrapper(iCnt + iStartBtn, PROP_BOOL_VISIBLE, 1,
						stUIReq.pszBuf);

		/* Set the label of the button (display amount) */
		sprintf(szTmp, "$%d", iCashBckAmts[iCnt]);
		setStringValueWrapper(iCnt + iStartBtn, PROP_STR_CAPTION, szTmp,
						stUIReq.pszBuf);
	}

	/* Show the select cashback amount form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Cash Back Amount Selection Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		/* Wait for the user input to get the selected cashback amt */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_CB_AMOUNT_SCREEN_FK_1:
							debug_sprintf(szDbgMsg, "%s: CANCEL", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "User Pressed Cancel Button on Cashback Amount Selection Screen");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							rv = UI_CANCEL_PRESSED;
							break;

						case PAAS_CB_AMOUNT_SCREEN_BTN_1:
							debug_sprintf(szDbgMsg, "%s: OTHER", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "User Selected [Other] Option on Cashback Amount Selection Screen");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							rv = UI_OTHER_SELECTED;
							break;

						case PAAS_CB_AMOUNT_SCREEN_BTN_2:
							debug_sprintf(szDbgMsg, "%s: NONE", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "User Selected [No] Option on Cashback Amount Selection Screen");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							rv = UI_NO_SELECTED;
							break;

						default:
							iSelOpt = pstXevt->uiCtrlID - iStartBtn;
							debug_sprintf(szDbgMsg, "%s: Selected option = [%d]"
											, __FUNCTION__, iSelOpt);
							APP_TRACE(szDbgMsg);

							if(iAppLogEnabled == 1)
							{
								sprintf(szAppLogData, "User Selected [%d] Option on Cashback Amount Selection Screen", iSelOpt);
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}

							if((iSelOpt >= 0) && (iSelOpt < iTotCnt))
							{
								//rv = iSelOpt;
								*piSelectedOpt = iSelOpt;
							}
							else
							{
								rv = ERR_DEVICE_APP;
							}

							break;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showConfirmAmtScreen
 *
 * Description	: This API would be used to display the updated amount to the
 * 					users and to get their confirmation to go ahead with the
 * 					authorization for this amount.
 *
 * Input Params	: pszConfirmAmt -> Amount to be confirmed.
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showConfirmAmtScreen(char *pszConfirmAmt)
{
	int				rv					= SUCCESS;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTmp[100]			= "";
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = GEN_CFRM_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* ---------------- Set the labels ----------------- */

	/* Set the Title */
	memset(szTitle, 0x00, sizeof(szTitle));
	fetchScrTitle(szTitle, AMT_CFRM_TITLE);
	/* Set the amount label */
	memset(szAmtTitle, 0x00, sizeof(szAmtTitle));
	fetchScrTitle(szTmp, TOTAL_AMT_DUE_TITLE);
	sprintf(szAmtTitle, "%s $%s", szTmp, pszConfirmAmt);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_GEN_CRFM_LBL_1;
	ctrlIds[1] 	= PAAS_GEN_CRFM_LBL_7;
	ctrlIds[2] 	= PAAS_GEN_CRFM_LBL_8;
	ctrlIds[3] 	= PAAS_GEN_CRFM_LBL_6;

	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 4, giLabelLen, stUIReq.pszBuf);

	/* ------------- Set the labels for the buttons ----------- */

	/* "APPROVE" string */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_09);
	setStringValueWrapper(PAAS_GEN_CRFM_LBL_4, PROP_STR_CAPTION,
			(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);
	/* "CANCEL" string */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_08);
	setStringValueWrapper(PAAS_GEN_CRFM_LBL_3, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here
	/* Send batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Amount Confirmation Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		/* Wait for any event for the form */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_GEN_CRFM_FK_NO:
							debug_sprintf(szDbgMsg, "%s: NO", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_NO_SELECTED;
							break;

						case PAAS_GEN_CRFM_FK_YES:
							debug_sprintf(szDbgMsg, "%s: YES", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_YES_SELECTED;
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Unknown",__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
							break;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showCtrTipOptScrn
 *
 * Description	: This API would be used to display the already available
 * 					options for the counter tips to the user on the Select Tip
 * 					Amount screen. It will be called if the counter tip is
 * 					supported in the merchant configuration.
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showCtrTipOptScrn(int * iTipPcnts, int iTipCnt, char * cTranAmt, int *piSelectedOpt)
{
	int				rv					= SUCCESS;
	int				iCnt				= 0;
	int				iSelOpt				= 0;
	int				iStartBtn			= 0;
	int				iStartLbl			= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTmp[100]			= "";
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	int 			ctrlIds[4];
	double			tranAmt				= 0;
	double			tipAmt				= 0;
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = SEL_TIP_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* ---------------- Set the labels ----------------- */

	/* Set the Title */
	fetchScrTitle(szTitle, SELECT_TIP_TITLE);

	/* Set the sub-title displaying the net due amount */
	fetchScrTitle(szTmp, TOTAL_AMT_DUE_TITLE);
	sprintf(szAmtTitle, "%s $%s", szTmp, cTranAmt);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_SELECT_TIP_LBL_1;
	ctrlIds[1] 	= PAAS_SELECT_TIP_LBL_17;
	ctrlIds[2] 	= PAAS_SELECT_TIP_LBL_18;

	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);

	fetchBtnLabel(&stBtnLbl, BTN_LBLS_10);
	/* Set the label for "OTHER" button */
	setStringValueWrapper(PAAS_SELECT_TIP_LBL_4, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);
	/* Set the label for "NONE" button */
	setStringValueWrapper(PAAS_SELECT_TIP_LBL_9, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Set the initial label and buttons for the list */
	iStartBtn = PAAS_SELECT_TIP_BTN_2;
	iStartLbl = PAAS_SELECT_TIP_LBL_5;

	tranAmt = atof(cTranAmt);

	for(iCnt = 0; iCnt < iTipCnt; iCnt++)
	{
		/*AjayS2: 18 May 2016:
		 * PTMX 1306:
		 * SCA 2.19.24 B2 - Parameters - "presettip_<n>" where n = 1-4 are not displayed correctly when a preceding parameter has an invalid value.
		 *
		 */
		if(iTipPcnts[iCnt] > 0 && iTipPcnts[iCnt] <= 100)
		{
			/* Calculate the tip amount */
			tipAmt = (tranAmt * iTipPcnts[iCnt]) / 100;

			/* Set the button as visible */
			setBoolValueWrapper(iCnt + iStartBtn, PROP_BOOL_VISIBLE, 1,
					stUIReq.pszBuf);

			/* Set the label of the button (display tip %) */
			sprintf(szTmp, "%d%%", iTipPcnts[iCnt]);
			setStringValueWrapper(iCnt + iStartBtn, PROP_STR_CAPTION, szTmp,
					stUIReq.pszBuf);

			/* Set the label below the button (display tip amount) */
			sprintf(szTmp, "$%.2lf", tipAmt);
			setStringValueWrapper(iCnt + iStartLbl, PROP_STR_CAPTION, szTmp,
					stUIReq.pszBuf);
		}
		tipAmt = 0;
	}

	/* Show the select counter tip form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here
	/* Send the batch message to the UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Counter Tip Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		/* Wait for user input to get the counter tip selected by the user */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_SELECT_TIP_FK_1:
							debug_sprintf(szDbgMsg, "%s: CANCEL", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_CANCEL_PRESSED;
							break;

						case PAAS_SELECT_TIP_BTN_1:
							debug_sprintf(szDbgMsg, "%s: OTHER", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_OTHER_SELECTED;
							break;

						case PAAS_SELECT_TIP_BTN_6:
							debug_sprintf(szDbgMsg, "%s: NO TIP", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_SKIP_BUTTON_PRESSED;
							break;

						default:
							iSelOpt = pstXevt->uiCtrlID - iStartBtn;

							debug_sprintf(szDbgMsg, "%s: Selected option = [%d]"
											, __FUNCTION__, iSelOpt);
							APP_TRACE(szDbgMsg);

							if((iSelOpt >= 0) && (iSelOpt < iTipCnt))
							{
									//rv = iSelOpt;
									*piSelectedOpt = iSelOpt;
							}
							else
							{
								debug_sprintf(szDbgMsg,
											"%s: Selected option not in range",
													__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
							}

							break;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showEBTTypeScreenforPymtTrans
 *
 * Description	: This API would display the EBT Type Tender Selection screen,
 * 				   User Can select either of the CASH_BENEFITS or SNAP and proceed for EBT Transactions


 *
 * Input Params	: List of Tender options
 *
 * Output Params: controlId pressed/FAILURE
 * ============================================================================
 */
int showEBTTypeScreenforPymtTrans(char * szTranAmt, int *piSelectedOpt)
{
	int				rv					= SUCCESS;
	int				iCnt				= 0;
	int				iSelOpt				= 0;
	int				iStartBtn			= 0;
	int				iStartBtnLbl		= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTmp[100]			= "";
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szBtnLbl[32]		= "";
	char			szAppLogData[256]	= "";
	int 			ctrlIds[4];
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	char szFileName[MAX_FILE_NAME_LEN]  = "";
	int	 			iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[128]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = SEL_TENDER_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* ---------------- Set the labels ----------------- */

	/* Set the Title */
	fetchScrTitle(szTitle, SELECT_EBT_TITLE);

	if((szTranAmt != NULL) && (strlen(szTranAmt) > 0))
	{
		 /* Set the total amount in the sub-title */
		fetchScrTitle(szTmp, TOTAL_AMT_DUE_TITLE);
		sprintf(szAmtTitle, "%s $%s", szTmp, szTranAmt);
	}

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_TEND_SEL_SCREEN_LBL_1;
	ctrlIds[1] 	= PAAS_TEND_SEL_SCREEN_LBL_2;
	ctrlIds[2] 	= PAAS_TEND_SEL_SCREEN_LBL_16;

	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 4, giLabelLen, stUIReq.pszBuf);

	iStartBtn = PAAS_TEND_SEL_SCREEN_BTN_1;
	iStartBtnLbl = PAAS_TEND_SEL_SCREEN_LBL_3;

	while(iCnt < 2)
	{

		memset(szFileName, 0x00, sizeof(szFileName));
		strcpy(szFileName, "btn_ebt_sta.png");
		getFileNamewithPlatformPrefix(szFileName);
		/* Set the button image */
		setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
										szFileName, stUIReq.pszBuf);

		memset(szFileName, 0x00, sizeof(szFileName));
		strcpy(szFileName, "btn_ebt_act.png");
		getFileNamewithPlatformPrefix(szFileName);

		/* Set the button image */
		setStringValueWrapper(iCnt +iStartBtn,PROP_STR_DOWN_IMAGE_FILE_NAME,
										szFileName, stUIReq.pszBuf);

		/* Set the corresponding label */
		fetchBtnLabel(&stBtnLbl, BTN_LBLS_29);
		memset(szBtnLbl, 0x00, sizeof(szBtnLbl));

		if(iCnt == 0)
		{
			strcpy(szBtnLbl, (char *)stBtnLbl.szLblOne);
		}
		else
		{
			strcpy(szBtnLbl, (char *)stBtnLbl.szLblTwo);
		}
		stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);
		setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
								szBtnLbl, stUIReq.pszBuf);

		/* Set the button as visible */
		setBoolValueWrapper(iCnt + iStartBtn, PROP_BOOL_VISIBLE, 1,
						stUIReq.pszBuf);

		iCnt++;
	}

	/* Show the tender selection form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	/* Send the batch message to the UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing EBT Type Tender Selection Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		/* Wait for the user input to get the tender selected by the user */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == PAAS_TEND_SEL_SCREEN_FK_1)
						{
							debug_sprintf(szDbgMsg, "%s: CANCEL", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_CANCEL_PRESSED;
						}
						else
						{
							iCnt = 0;
							iSelOpt = pstXevt->uiCtrlID - iStartBtn;

							*piSelectedOpt = iSelOpt;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showTndrOptScrn
 *
 * Description	: This API would display the Tender Selection screen with only
 * 					those options as buttons, which have been configured.
 * 					Ideally, it can display the following options:
 * 					1.	Credit
 * 					2.	Gift Card
 * 					3.	Debit
 * 					4.	ISIS Wallet
 * 					5.	Google Wallet
 * 					6.	Pay Balance to Cashier
 *
 * Input Params	: List of Tender options
 *
 * Output Params: controlId pressed/FAILURE
 * ============================================================================
 */
int showTndrOptScrn(int * tenderList, char * szTranAmt, int *piSelectedOpt, int iCommand, POSTENDERDTLS_PTYPE pstPOSTenderDtls)
{
	int				rv								= SUCCESS;
	int				iCnt							= 0;
	int				iLen							= 0;
	int				iSelOpt							= 0;
	int				iStartBtn						= 0;
	int				iStartBtnLbl					= 0;
	int				iPOSTndrNum 					= 0;
	int				iAppLogEnabled					= isAppLogEnabled();
	int	 			iDisableFormFlag				= 0;
	int				iEMVSelLan						= 0;
	int				iStatusMsgDispInterval			= 0;
	int				eEncType;
	int 			ctrlIds[4];
	int				iEmvRespCode					= -1;
	char			szTmp[100]						= "";
	char			szTitle[100]					= "";
	char			szAmtTitle[100]					= "";
	char			szBtnLbl[32]					= "";
	char			szClrDt[10]						= "";
	char			szAppLogData[256]				= "";
	char 			szFileName[MAX_FILE_NAME_LEN]	= "";
	char *			cCurPtr							= NULL;
	char *			cNxtPtr							= NULL;
	char**			szTracks						= NULL;
	char**			szRsaTracks						= NULL;
	char**			szVsdEncData					= NULL;
	PAAS_BOOL		bWait							= PAAS_TRUE;
	PAAS_BOOL		bEmvEnabled						= PAAS_FALSE;
	PAAS_BOOL		bCardReadDone					= PAAS_FALSE;
	PAAS_BOOL		bfirstTime						= PAAS_TRUE;
	PAAS_BOOL		bInvalidCard					= PAAS_FALSE;
	XEVT_PTYPE		pstXevt							= NULL;
	BTNLBL_STYPE	stBtnLbl;
	CARD_TRK_PTYPE	pstCard							= NULL;
	EMVDTLS_PTYPE	pstEmvDtls						= NULL;
	CARDDTLS_STYPE	stCardDtls;
	BACKUP_CARDDTLS_STYPE	stBkupCrdDtls;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[128]		= "";
#endif

	memset(&stBkupCrdDtls, 0x00, sizeof(BACKUP_CARDDTLS_STYPE));

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */

	bEmvEnabled = isEmvEnabledInDevice();
	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	if(getCardReadEnabled() == PAAS_TRUE)
	{
		whichForm = SEL_TENDER_FRM;

		//Get EMV status
//		bEmvEnabled = isEmvEnabledInDevice();

		/* Disabling the card swipe on the UI agent. */
		cancelRequest();
		/* The below section of the code is commented and we will be using this logic after we send the XSFM for Tender Selection screen.
		 * This is because, the cancel request will no longer return any response and we dont wait for any cancel response.
		 * But there could be a scenario where card was swiped in the preswipe screen and we simulatenously moved to the Tender selection screen,
		 * in such a case , we will get the card response before the show form response of the Tender selection screen and store it in the pre-swipe
		 * details structure.
		 */
#if 0
		/*
		 * For RESET command, we get C3108 response for C30 and 03 for S20
		 * thats why waiting for the response here
		 */
		if(bEmvEnabled)
		{
			while(bWait == PAAS_TRUE)
			{
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_CARD_RESP)
					{
						iEmvRespCode = EMV_CARD_WAS_SWIPED;
					}
					else if (stBLData.uiRespType == UI_EMV_RESP)
					{
						if(strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_C31_RESP) == 0)
						{
							iEmvRespCode = atoi(stBLData.stRespDtls.stEmvDtls.szEMVRespCode);
						}
						else if (stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bEMVRetry == PAAS_TRUE)
						{
							iEmvRespCode = 100;
						}
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					switch(iEmvRespCode)
					{
					case EMV_CARD_WAS_SWIPED:
					case EMV_CMD_SUCCESS:
						bCardReadDone = PAAS_TRUE;
						bBLDataRcvd   = PAAS_FALSE;
						bWait = PAAS_FALSE;
						break;
					case EMV_FALLBACK_TO_MSR:
					case EMV_ERROR_BLOCKED_CARD:
					case EMV_ERROR_BAD_CARD:
					case EMV_ERROR_CANDIDATE_LISTEMPTY:
						clearScreen(1);
					case EMV_ERROR_TXN_CANCELLED:
						bBLDataRcvd   = PAAS_FALSE;
						bWait = PAAS_FALSE;
						break;
					case 100:
						clearScreen(1);
					}
				}
				svcWait(10);
			}
		}
		else
		{
			while(bWait == PAAS_TRUE)
			{
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_CARD_RESP )
					{
						if(stBLData.iStatus == SUCCESS)
						{
							bCardReadDone = PAAS_TRUE;
						}					

						bBLDataRcvd   = PAAS_FALSE;
						bWait		  = PAAS_FALSE;
					}
					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(10);
			}
		}
#endif
		setCardReadEnabled(PAAS_FALSE);
		bWait = PAAS_TRUE;
	}



	whichForm = SEL_TENDER_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* ---------------- Set the labels ----------------- */

	/* Set the Title */
	fetchScrTitle(szTitle, SELECT_PYMTTYPE_TITLE);

	/* Set the total amount in the sub-title */
	if(iCommand == SSI_CREDIT)
	{
		fetchScrTitle(szTmp, TOTAL_REFUND_AMT_TITLE);
		sprintf(szAmtTitle, "%s -$%s", szTmp, szTranAmt);
	}
	else
	{
		fetchScrTitle(szTmp, TOTAL_AMT_DUE_TITLE);
		sprintf(szAmtTitle, "%s $%s", szTmp, szTranAmt);
	}

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_TEND_SEL_SCREEN_LBL_1;
	ctrlIds[1] 	= PAAS_TEND_SEL_SCREEN_LBL_2;
	ctrlIds[2] 	= PAAS_TEND_SEL_SCREEN_LBL_16;

	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 4, giLabelLen, stUIReq.pszBuf);

	iStartBtn = PAAS_TEND_SEL_SCREEN_BTN_1;
	iStartBtnLbl = PAAS_TEND_SEL_SCREEN_LBL_3;

	iCnt = 0;
	/* Set the buttons and the corresponding labels */
	while((tenderList[iCnt] >= 0) && (iCnt < MAX_TENDERS)) {
		switch(tenderList[iCnt])
		{
		case TEND_CREDIT:
			debug_sprintf(szDbgMsg, "%s: Credit Button to be shown",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_cre_sta.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
												szFileName, stUIReq.pszBuf);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_cre_act.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt +iStartBtn,PROP_STR_DOWN_IMAGE_FILE_NAME,
												szFileName, stUIReq.pszBuf);

			/* Set the corresponding label */
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_04);
			memset(szBtnLbl, 0x00, sizeof(szBtnLbl));
			strcpy(szBtnLbl, (char *)stBtnLbl.szLblOne);
			stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

			setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
							szBtnLbl, stUIReq.pszBuf);

			break;

		case TEND_DEBIT:
			debug_sprintf(szDbgMsg, "%s: Debit Button to be shown",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_deb_sta.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
												szFileName, stUIReq.pszBuf);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_deb_act.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt +iStartBtn,PROP_STR_DOWN_IMAGE_FILE_NAME,
												szFileName, stUIReq.pszBuf);


			/* Set the corresponding label */
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_04);
			memset(szBtnLbl, 0x00, sizeof(szBtnLbl));
			strcpy(szBtnLbl, (char *)stBtnLbl.szLblTwo);
			stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

			setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
											szBtnLbl, stUIReq.pszBuf);

			break;

		case TEND_GIFT:
			debug_sprintf(szDbgMsg, "%s: Gift Card Button to be shown",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_gif_sta.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_gif_act.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt +iStartBtn,PROP_STR_DOWN_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			/* Set the corresponding label */
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_05);
			memset(szBtnLbl, 0x00, sizeof(szBtnLbl));
			strcpy(szBtnLbl, (char *)stBtnLbl.szLblOne);
			stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

			setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
							szBtnLbl, stUIReq.pszBuf);

			break;

		case TEND_ISIS:
			debug_sprintf(szDbgMsg, "%s: ISIS Wallet Button to be shown",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_tap_sta.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_tap_act.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt +iStartBtn,PROP_STR_DOWN_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			/* Set the corresponding label */
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_05);
			memset(szBtnLbl, 0x00, sizeof(szBtnLbl));
			strcpy(szBtnLbl, (char *)stBtnLbl.szLblTwo);
			stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

			setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
							szBtnLbl, stUIReq.pszBuf);

			break;

		case TEND_GOOGLE:
			debug_sprintf(szDbgMsg, "%s: Google Wallet Button to be shown",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_tap_sta.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_tap_act.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt +iStartBtn,PROP_STR_DOWN_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			/* Set the corresponding label */
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_06);
			memset(szBtnLbl, 0x00, sizeof(szBtnLbl));
			strcpy(szBtnLbl, (char *)stBtnLbl.szLblOne);
			stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

			setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
											szBtnLbl, stUIReq.pszBuf);

			break;

		case TEND_BALANCE:
			debug_sprintf(szDbgMsg, "%s: Pay Balance Button to be shown",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_bal_sta.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_bal_act.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt +iStartBtn,PROP_STR_DOWN_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			/* Set the corresponding label */
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_06);
			memset(szBtnLbl, 0x00, sizeof(szBtnLbl));
			//strcpy(szBtnLbl, (char *)screen.line_two[1]);
			strcpy(szBtnLbl, (char *)stBtnLbl.szLblTwo);
			stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

			setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
									szBtnLbl, stUIReq.pszBuf);

			break;

		case TEND_PRIVATE:
			debug_sprintf(szDbgMsg, "%s: Private Label Button to be shown",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_blank_static.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_blank_active.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt +iStartBtn,PROP_STR_DOWN_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			/* Set the corresponding label */
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_22);
			memset(szBtnLbl, 0x00, sizeof(szBtnLbl));
			//strcpy(szBtnLbl, (char *)screen.line_two[1]);
			strcpy(szBtnLbl, (char *)stBtnLbl.szLblOne);
			stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

			setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
									szBtnLbl, stUIReq.pszBuf);

			break;

		case TEND_MERCHCREDIT:
			debug_sprintf(szDbgMsg, "%s: Merchandise Credit Button to be shown",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_blank_static.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_blank_active.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt +iStartBtn,PROP_STR_DOWN_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			/* Set the corresponding label */
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_27);
			memset(szBtnLbl, 0x00, sizeof(szBtnLbl));
			//strcpy(szBtnLbl, (char *)screen.line_two[1]);
			strcpy(szBtnLbl, (char *)stBtnLbl.szLblOne);
			stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

			setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
									szBtnLbl, stUIReq.pszBuf);

			break;

		case TEND_PAYPAL:
			debug_sprintf(szDbgMsg, "%s: Pay Pal Button to be shown",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_paypal_sta.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_paypal_act.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt +iStartBtn,PROP_STR_DOWN_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			/* Set the corresponding label */
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_28);
			memset(szBtnLbl, 0x00, sizeof(szBtnLbl));
			//strcpy(szBtnLbl, (char *)screen.line_two[1]);
			strcpy(szBtnLbl, (char *)stBtnLbl.szLblOne);
			stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

			setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
									szBtnLbl, stUIReq.pszBuf);
			break;

		case TEND_EBT:
			debug_sprintf(szDbgMsg, "%s: EBT Button to be shown",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_ebt_sta.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_ebt_act.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt +iStartBtn,PROP_STR_DOWN_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			/* Set the corresponding label */
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_27);
			memset(szBtnLbl, 0x00, sizeof(szBtnLbl));
			strcpy(szBtnLbl, (char *)stBtnLbl.szLblTwo);
			stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

			setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
									szBtnLbl, stUIReq.pszBuf);
			break;

		case TEND_FSA:
			debug_sprintf(szDbgMsg, "%s: FSA Button to be shown",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_blank_static.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_blank_active.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt +iStartBtn,PROP_STR_DOWN_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

			/* Set the corresponding label */
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_28);
			memset(szBtnLbl, 0x00, sizeof(szBtnLbl));
			strcpy(szBtnLbl, (char *)stBtnLbl.szLblTwo);
			stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

			setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
									szBtnLbl, stUIReq.pszBuf);
			break;
		case TEND_POS_TENDER1:
		case TEND_POS_TENDER2:
		case TEND_POS_TENDER3:
			iPOSTndrNum = tenderList[iCnt] - TEND_FSA;		/* please change this, if  you are adding any new Tender Type before POS_TENDER1 */
			debug_sprintf(szDbgMsg, "%s: POS Tender%d Button to be shown",
										__FUNCTION__, iPOSTndrNum);
			APP_TRACE(szDbgMsg);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_blank_static.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
												szFileName, stUIReq.pszBuf);

			memset(szFileName, 0x00, sizeof(szFileName));
			strcpy(szFileName, "btn_blank_active.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the button image */
			setStringValueWrapper(iCnt +iStartBtn,PROP_STR_DOWN_IMAGE_FILE_NAME,
												szFileName, stUIReq.pszBuf);

			/* Set the corresponding label */
			memset(szBtnLbl, 0x00, sizeof(szBtnLbl));
			strncpy(szBtnLbl, pstPOSTenderDtls->szPOSTender[iPOSTndrNum - 1], 8);	// Max 8 bytes should be displayed
			stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

			setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
							szBtnLbl, stUIReq.pszBuf);

			break;
		}

		/* Set the button as visible */
		setBoolValueWrapper(iCnt + iStartBtn, PROP_BOOL_VISIBLE, 1,
						stUIReq.pszBuf);

		/* Move to next button/label to be displayed */
		iCnt++;
	}

	/* Show the tender selection form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

//	bBLDataRcvd = PAAS_FALSE; //T_DaivikP1: Not setting bldatarcvd to false because we could have recieved the card data and we need to handle it below
	/* Send the batch message to the UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Tender Selection Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		/* Wait for the user input to get the tender selected by the user */
		while(bWait == PAAS_TRUE)
		{
			/* Once we finish processing card data we are setting blDataRcvd to false again, so that we dont execute unecessary code in a loop.
			 * Once the show form response comes, this blDataRcvd will again be set to true in UImanager thread.
			 * Therefore we need to bring the check of bldatarcvd under the mutex lock.
			 */

			CHECK_POS_INITIATED_STATE;
			acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			if(bBLDataRcvd == PAAS_TRUE)
			{
				if((stBLData.uiRespType == UI_CARD_RESP) && !bEmvEnabled)
				{
					if(stBLData.iStatus == SUCCESS)
					{
						/* We will set card read done , whenever card was swiped/or we recieved C3100 response.
						 * Based on this value we will store the pre swipe details below.
						 */
						bCardReadDone = PAAS_TRUE;
					}
				}
				else if(bEmvEnabled)
				{
					if(stBLData.uiRespType == UI_CARD_RESP)
					{
						iEmvRespCode = EMV_CARD_WAS_SWIPED;
					}
					else if (stBLData.uiRespType == UI_EMV_RESP)
					{
						if(strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_C31_RESP) == 0)
						{
							iEmvRespCode = atoi(stBLData.stRespDtls.stEmvDtls.szEMVRespCode);
						}
						else if (stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bEMVRetry == PAAS_TRUE)
						{
							iEmvRespCode = 100;
						}
					}
					switch(iEmvRespCode)
					{
						case EMV_CARD_WAS_SWIPED:
						case EMV_CMD_SUCCESS:
						bCardReadDone = PAAS_TRUE;
						break;
						case EMV_FALLBACK_TO_MSR:
						case EMV_ERROR_BLOCKED_CARD:
						case EMV_ERROR_BAD_CARD:
						case EMV_ERROR_CANDIDATE_LISTEMPTY:
							break;
						//	clearScreen(1);/* Should we do clear screen? How to handle such error cases ? */
						case EMV_ERROR_TXN_CANCELLED:
						break;
						case 100:
						break;
						//clearScreen(1);/* Should we do clear screen? How to handle such error cases ?
					}
				}
				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == PAAS_TEND_SEL_SCREEN_FK_1)
						{
							debug_sprintf(szDbgMsg, "%s: CANCEL", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_CANCEL_PRESSED;
						}
						else
						{
							iCnt = 0;
							iSelOpt = pstXevt->uiCtrlID - iStartBtn;

							while( (tenderList[iCnt] >= 0) &&
														(iCnt < MAX_TENDERS))
							{
								if(iCnt == iSelOpt)
								{
									*piSelectedOpt = tenderList[iCnt];
									break;
								}

								iCnt++;
							}
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

				}
				if(bCardReadDone == PAAS_TRUE && bfirstTime)
				{
					bfirstTime = PAAS_FALSE;
					//Get the status message display interval
					iStatusMsgDispInterval = getStatusMsgDispInterval();

					//Get encryption type enabled in the device
					eEncType = getEncryptionType();

					//acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_CARD_RESP)
					{
						if(stBLData.iStatus == SUCCESS)
						{
							pstCard = &(stBLData.stRespDtls.stCrdTrkInfo);

							debug_sprintf(szDbgMsg, "%s: Received the Card Data", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							if(pstCard->iCardSrc == CRD_MSR)
							{
								strcpy(szAppLogData, "Card Details Captured From MSR Card");
							}
							else
							{
								strcpy(szAppLogData, "Card Details Captured From Contactless Card");
							}

							/* Allocating memory for tracks placeholder */
							/* 3 for the track info and the fourth for the eparms*/
							szTracks = (char **) malloc(sizeof(char *) * 4);
							if(szTracks == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								//CID 67448 (#1 of 1): Unused value (UNUSED_VALUE). T_raghavendranR1. Commenting the below stmt here, as rv gets overwritten below.
								//rv = FAILURE;
							}
							else//CID 67349 (#1 of 1): Dereference after null check (FORWARD_NULL) T_RaghavendranR1
							{
								memset(szTracks, 0x00, sizeof(char *) * 4);
							}

							if (eEncType == RSA_ENC)
							{
								szRsaTracks = (char **) malloc(sizeof(char *) * 2);
								if(szRsaTracks == NULL)
								{
									debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									//CID 67448 (#1 of 1): Unused value (UNUSED_VALUE). T_raghavendranR1. Commenting the below stmt here, as rv gets overwritten below.
									//rv = FAILURE;

								}
								else//CID 67402 (#1 of 3): Dereference after null check (FORWARD_NULL). T_RaghavendranR1
								{
									memset(szRsaTracks, 0x00, sizeof(char *) * 2);
								}
							}
							else if(eEncType == VSD_ENC)
							{
								szVsdEncData = (char **) malloc(sizeof(char *) * 4);
								if(szVsdEncData == NULL)
								{
									debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
								else
								{
									memset(szVsdEncData, 0x00, sizeof(char *) * 4);
								}
							}

							/* Track 1 --- */
							iLen = strlen(pstCard->szTrk1);
							if(iLen > 0 && szTracks != NULL)//CID 67349 (#1 of 1): Dereference after null check (FORWARD_NULL) T_RaghavendranR1
							{
								szTracks[0] = (char *)malloc(iLen + 1);
								memset(szTracks[0], 0x00, iLen + 1);
								memcpy(szTracks[0], pstCard->szTrk1, iLen);
							}

							/* Track 2 --- */
							iLen = strlen(pstCard->szTrk2);
							if(iLen > 0 && szTracks != NULL)//CID 67349 (#1 of 1): Dereference after null check (FORWARD_NULL) T_RaghavendranR1
							{
								szTracks[1] = (char *)malloc(iLen + 1);
								memset(szTracks[1], 0x00, iLen + 1);
								memcpy(szTracks[1], pstCard->szTrk2, iLen);
							}

							/* Track 3 --- */
							iLen = strlen(pstCard->szTrk3);
							if(iLen > 0 && szTracks != NULL)//CID 67349 (#1 of 1): Dereference after null check (FORWARD_NULL) T_RaghavendranR1
							{
								szTracks[2] = (char *)malloc(iLen + 1);
								memset(szTracks[2], 0x00, iLen + 1);
								memcpy(szTracks[2], pstCard->szTrk3, iLen);
							}

							/* Eparms Data  --- */
							iLen = strlen(pstCard->szEparms);
							if(iLen > 0 && szTracks != NULL)//CID 67349 (#1 of 1): Dereference after null check (FORWARD_NULL) T_RaghavendranR1
							{
								szTracks[3] = (char *)malloc(iLen + 1);
								memset(szTracks[3], 0x00, iLen + 1);
								memcpy(szTracks[3], pstCard->szEparms, iLen);
							}

							/* Copy clear expiry date*/
							iLen = strlen(pstCard->szClrExpDt);
							if(iLen > 0)
							{
								memset(szClrDt, 0x00, iLen + 1);
								memcpy(szClrDt, pstCard->szClrExpDt, iLen);

								debug_sprintf(szDbgMsg, "%s: Copying Clear Expiry date",
										__FUNCTION__);
								APP_TRACE(szDbgMsg);

							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: No Clear exp date to copy",
										__FUNCTION__);
								APP_TRACE(szDbgMsg);
							}

							/*Copy the RSA Track 1 */
							iLen = strlen(pstCard->szRsaTrk1);
							if(iLen > 0 && szRsaTracks != NULL)//CID 67402 (#1 of 3): Dereference after null check (FORWARD_NULL). T_RaghavendranR1
							{
								szRsaTracks[0] = (char *)malloc(iLen + 1);
								memset(szRsaTracks[0], 0x00, iLen + 1);
								memcpy(szRsaTracks[0], pstCard->szRsaTrk1, iLen);
							}

							/*Copy the RSA Track 2 */
							iLen = strlen(pstCard->szRsaTrk2);
							if(iLen > 0 && szRsaTracks != NULL)//CID 67402 (#1 of 3): Dereference after null check (FORWARD_NULL). T_RaghavendranR1
							{
								szRsaTracks[1] = (char *)malloc(iLen + 1);
								memset(szRsaTracks[1], 0x00, iLen + 1);
								memcpy(szRsaTracks[1], pstCard->szRsaTrk2, iLen);
							}

							if(szVsdEncData != NULL)
							{
								/* Copy VSD encrypted  data*/
								iLen = strlen(pstCard->szVsdEncBlob);
								if(iLen > 0)
								{
									szVsdEncData[0] = (char *)malloc(iLen + 1);
									memset(szVsdEncData[0], 0x00, iLen + 1);
									memcpy(szVsdEncData[0], pstCard->szVsdEncBlob, iLen);
								}

								/* Copy KSN associated with VSD encrypted  data*/
								iLen = strlen(pstCard->szVsdKSN);
								if(iLen > 0)
								{
									szVsdEncData[1] = (char *)malloc(iLen + 1);
									memset(szVsdEncData[1], 0x00, iLen + 1);
									memcpy(szVsdEncData[1], pstCard->szVsdKSN, iLen);
								}

								/* Copy IV associated with VSD encrypted  data*/
								iLen = strlen(pstCard->szVsdIV);
								if(iLen > 0)
								{
									szVsdEncData[2] = (char *)malloc(iLen + 1);
									memset(szVsdEncData[2], 0x00, iLen + 1);
									memcpy(szVsdEncData[2], pstCard->szVsdIV, iLen);
								}

								/* Copy Clear Track data if present*/
								if(strlen(pstCard->szClrTrk2))
								{
									iLen = strlen(pstCard->szClrTrk2);
									szVsdEncData[3] = (char *)malloc(iLen + 1);
									memset(szVsdEncData[3], 0x00, iLen + 1);
									memcpy(szVsdEncData[3], pstCard->szClrTrk2, iLen);
								}
								else if(strlen(pstCard->szClrTrk1))
								{
									iLen = strlen(pstCard->szClrTrk1);
									szVsdEncData[3] = (char *)malloc(iLen + 1);
									memset(szVsdEncData[3], 0x00, iLen + 1);
									memcpy(szVsdEncData[3], pstCard->szClrTrk1, iLen);
								}
							}

							memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

							/*Store EMV Fallback parameter*/
							/*stCardDtls.bEmvFallback = bEmvFallbackMode;
							bEmvFallbackMode = PAAS_FALSE;
							debug_sprintf(szDbgMsg, "%s: EMV FallBack Flag [%d]", __FUNCTION__, bEmvFallbackMode);
							APP_TRACE(szDbgMsg);*/


							/* Store the Card source */
							stCardDtls.iCardSrc = pstCard->iCardSrc;

							/* Saving the card type as private card type*/
							stCardDtls.iCardType = PL_CARD_TYPE;

							/* parse the card information */
							// validation of card data is not needed since we dont have tender type yet. These card details would be equivalent to the one we recived in pre-swipe screen.
							rv = getCardDtls(&stCardDtls, szTracks, szRsaTracks, szVsdEncData,"0.00", PAAS_FALSE, NULL /* Need to send STB data here*/);
							if(rv != SUCCESS)
							{
								debug_sprintf(szDbgMsg, "%s: Error while getting card deatails, rv[%d]", __FUNCTION__, rv);
								APP_TRACE(szDbgMsg);

								//Daivik-8/7/16:We are passing the bValidate flag as FALSE, so we would not be doing validation. We would not be entering the below condition.
								//This path will be entered when we preswipe a card and simultaneously send the capture command. In this case if we are in old flow we will enter here.(For Tender Selection)
								//TODO: Check if the below check can be removed.
								if(rv == ERR_CARD_INVALID)
								{
									showPSGenDispForm(MSG_CARD_INVALID, FAILURE_ICON, iStatusMsgDispInterval);
								}

								if(szTracks == NULL)
								{
									bInvalidCard = PAAS_TRUE;
								}
								else
								{
									if((szTracks[0] == NULL) && (szTracks[1] == NULL))
									{
										bInvalidCard = PAAS_TRUE;
									}
								}
								///If both tracks are not present , then we will not store backup details. we will follow the same flow as what it is happening today.
								if(!bInvalidCard)
								{
									stBkupCrdDtls.pszRSATrackDtlsBkup = szRsaTracks;
									stBkupCrdDtls.pszTrackDtlsBkup = szTracks;
									stBkupCrdDtls.pszVSDBkup = szVsdEncData;
									debug_sprintf(szDbgMsg, "%s: Storing the Backup Card Details", __FUNCTION__);
									APP_TRACE(szDbgMsg);
									if(szVsdEncData != NULL && szVsdEncData[3] != NULL)
									{
										//This code is required for sending unsolicited response to POS in case of VSD Data
										strcpy(stCardDtls.szClrTrkData, szVsdEncData[3]);
										// parse the clear PAN from clear track data if present.
										cCurPtr = szVsdEncData[3];
										if((cNxtPtr = strchr(cCurPtr, '=')) != NULL)
										{
											//If It was Track 2 Data
											strncpy(stCardDtls.szClrPAN, cCurPtr, cNxtPtr - cCurPtr);
										}
										else if((cNxtPtr = strchr(cCurPtr, '^')) != NULL)
										{
											cCurPtr++;
											strncpy(stCardDtls.szClrPAN, cCurPtr, cNxtPtr - cCurPtr);
										}
										else
										{
											/* Daivik:21/7/2016 - In case of some gift cards which will not have the field seperators (non ISO format).
											 * We need to consider the complete track as the PAN. Also we need to set the Track Indicator which will
											 * be used in getRawCardDtls function to populate either ClearTrack1 or ClearTrack2 Data.
											 */

											if(strlen(cCurPtr) > (sizeof(stCardDtls.szClrPAN) - 1))
											{
												strncpy(stCardDtls.szClrPAN, cCurPtr, sizeof(stCardDtls.szClrPAN) - 1);
											}
											else
											{
												strcpy(stCardDtls.szClrPAN, cCurPtr);
											}
											if(szTracks[1] != NULL)
											{
												stCardDtls.iTrkNo = TRACK2_INDICATOR;
											}
											else if(szTracks[0] != NULL)
											{
												stCardDtls.iTrkNo = TRACK1_INDICATOR;
											}
										}

									}
									if(storeBkupDtlsinSession(&stBkupCrdDtls) != SUCCESS)
									{
										debug_sprintf(szDbgMsg, "%s: Failure to Store the backup Card details", __FUNCTION__);
										APP_TRACE(szDbgMsg);
										rv = FAILURE;
										break;
									}
								}

							}

							//set the encryption type here.
							stCardDtls.iEncType = eEncType;

							/*
							 * The following function is called when Pre-Swipe is done and Priv label is selected
							 * from the consumer option.
							 * The second parameter conveys that pre-swipe is done
							 *
							 */
							rv = storePreSwipeCardDtls(&stCardDtls, PAAS_TRUE);
							if(rv != SUCCESS)
							{
								if(rv == CARD_TOKEN_NOT_PRESENT)
								{
									debug_sprintf(szDbgMsg, "%s: Could not get the CARD_TOKEN for this card!!!", __FUNCTION__);
									APP_TRACE(szDbgMsg);
									//We will enable pre-Swipe again
									/*
									 * We need to show message to the customer that card token not found
									 *
									 */
									showPSGenDispForm(MSG_CARDTOK_NOTFOUND, FAILURE_ICON, iStatusMsgDispInterval);
								}
								else
								{
									debug_sprintf(szDbgMsg, "%s: Failed to store/send pre-swipe/tap card details", __FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
							}

							rv = SUCCESS;
						}

						if(iAppLogEnabled == 1)
						{
							addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
						}

						//Daivik:8/7/16 - We avoid freeing the track data incase we have stored the pointer in backup details. It will be freed after we use the backup details.
						if(szTracks != NULL && (stBkupCrdDtls.pszTrackDtlsBkup == NULL))
						{
							for(iCnt = 0; iCnt < 4; iCnt++)
							{
								if(szTracks[iCnt] != NULL)
								{
									free(szTracks[iCnt]);
								}
							}

							free(szTracks);
						}

						if(szRsaTracks != NULL && (stBkupCrdDtls.pszRSATrackDtlsBkup == NULL))
						{
							for(iCnt = 0; iCnt < 2; iCnt++)
							{
								if(szRsaTracks[iCnt] != NULL)
								{
									free(szRsaTracks[iCnt]);
								}
							}

							free(szRsaTracks);
						}

						if(szVsdEncData != NULL && (stBkupCrdDtls.pszVSDBkup == NULL))
						{
							for(iCnt = 0; iCnt < 4; iCnt++)
							{
								if(szVsdEncData[iCnt] != NULL)
								{
									free(szVsdEncData[iCnt]);
								}
							}

							free(szVsdEncData);
						}
					}
					else if(stBLData.uiRespType == UI_EMV_RESP)
					{
						/* If we received EMV fallback then we set this flag to true, but if user insert any
						 * card after that, terminal will move back to non-fallback mode, so we are
						 * resetting this flag, if we got EMV response other than U02
						 */
				//		bEmvFallbackMode = PAAS_FALSE;

						debug_sprintf(szDbgMsg, "%s: Card Inserted on Pre-Swipe/Insert Screen", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "EMV Card Inserted on Pre-Swipe/Insert Screen");
							addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
						}

						memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

						pstEmvDtls = &(stBLData.stRespDtls.stEmvDtls);

						storeEmvCardData(pstEmvDtls,&stCardDtls);

						//Default Lang saved in session
						iEMVSelLan = stCardDtls.EMVlangSelctd -1;
						if(iEMVSelLan >= 0 && iEMVSelLan < MAX_SUPPORT_LANG)
						{
							setLangId(iEMVSelLan);
						}

						storePreSwipeCardDtls(&stCardDtls, PAAS_TRUE);

						memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
					}

					bBLDataRcvd = PAAS_FALSE;
					//releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

			}
			releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getManCardData
 *
 * Description	: This API would be used to get the card data entered manually.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getManCardData(int iOpt, char * szExpDt, char * szClrDt, CARDDTLS_PTYPE pstCardDtls, char **szSTBData)
{
	int		rv					= SUCCESS;
	static int	iGCManEntry		= -1;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(iGCManEntry == -1)
	{
		iGCManEntry = getGCManualEntryParam();
	}

	/* MukeshS3: 10-Feb-16: Adding just a comment
	 * When we have GIFT cards having PAN length more than 19 digits, at that time SCA will show our own FORM to capture PAN & EXP details
	 * because, XGCD will support max 19 digits on PAN entry FORM.
	 */
	if((iOpt == GIFT_CARD) && (iGCManEntry == 1))
	{
		rv = getGCManData(pstCardDtls->szPAN, szExpDt, szClrDt, pstCardDtls->szCVV);
	}
	else
	{
		rv = getManualCardData(iOpt, pstCardDtls->szPAN, szExpDt, pstCardDtls->szCVV, szClrDt, pstCardDtls->szEncPayLoad, pstCardDtls->szEncBlob, szSTBData, pstCardDtls->szEncPayLoad, pstCardDtls->szVSDInitVector, pstCardDtls->szClrPAN);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: getCardData
 *
 * Description	: This API would be used to get the card data.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getCardData(char ** szTracks, char** szRsaTracks, char** szVsdEncData,  int * cardSrc, char *szClrDt, char **szSTBData, 
				char* pszTranKey, PAAS_BOOL bEmvAllowed, char *szServiceCodeByteOne, VASDATA_DTLS_PTYPE pstVasDataDtls, char* pszPayPassType)
{
	int				rv										= SUCCESS;
	int				iLen									= 0;
	int				iDisableFormFlag						= 0;
	int				iFxn									= 0;
	int				iCmd									= -1;
	int				iAppLogEnabled							= isAppLogEnabled();
	char			szTitle1[100]							= "";
	char			szTitle[200]							= "";
	char			szAmtTitle[100]							= "";
	char			szAppLogData[256]						= "";
	char            szVASMode[4+1]  						= "";
	char            szFileName[100]						    = "";
	char			szBackgroundImage[MAX_FILE_NAME_LEN+1]	= "";
	char			szNFCVASMode[4+1]						= "";
	int 			ctrlIds[4];
	char *			pszTemp									= NULL;
	PAAS_BOOL		bWait									= PAAS_TRUE;
	PAAS_BOOL		bCardReadDone							= PAAS_FALSE;
	PAAS_BOOL		bC30Request								= isEmvEnabledInDevice();
	PAAS_BOOL		bCardReadEnabled						= PAAS_TRUE;
	PAAS_BOOL		bWaitForFirstC3108						= PAAS_FALSE;
	XEVT_PTYPE		pstXevt									= NULL;
	CARD_TRK_PTYPE	pstCard									= NULL;
	EMVDTLS_STYPE	stEmvDtls;
	AMTDTLS_STYPE	stAmtDtls;
	CUSTINFODTLS_STYPE stCustInfoDtls;
	UIRESP_STYPE	stLocUIResp;
	UIREQ_STYPE		stUIReq;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setDetailedAppState(CAPTURING_CARD_DETAILS);
	whichForm = SWIPE_STI_FRM;

	/* Daivik:27/1/2016 - Coverity : 67293 */
	memset(&stAmtDtls,0x00,sizeof(AMTDTLS_STYPE));
	memset(&stLocUIResp, 0x00, sizeof(UIRESP_STYPE));

	/*
	 * Setting flag to know if the card data is received already
	 */
	bCardReadEnabled = getCardReadEnabled();


	//Get EMV status
	bC30Request = isEmvEnabledInDevice();

	//If EMV is enabled and allowed, then only use EMV and send C30 request
	bC30Request = bC30Request & bEmvAllowed;

	/*
	 * SWIPE_STI_FRM - Swipe Form with Swipe Tap Insert Symbol
	 * SWIPE_ST_FRM  - Swipe Form with Swipe Tap        Symbol
	 * SWIPE_SI_FRM  - Swipe Form with Swipe     Insert Symbol
	 * SWIPE_SI_FRM  - Swipe Form with Swipe            Symbol
	 */


	if(bC30Request == PAAS_FALSE) //If EMV is not enabled, dont show the insert logo
	{
		/* KranthiK1:
		 * This step facilitates for sending the S20 when emv is enabled on the device
		 * and when preswipe is enabled.
		 */
		if(bCardReadEnabled && isEmvEnabledInDevice())
		{
			debug_sprintf(szDbgMsg, "%s: A Non EMV Transaction, so disabling the card readers",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			disableCardReaders();
			bCardReadEnabled = PAAS_FALSE;
		}

		if(isContactlessEnabledInDevice() == PAAS_TRUE)//Contactless is enabled
		{
			whichForm = SWIPE_ST_FRM;
		}
		else
		{
			strcpy(szBackgroundImage, "bg_s.png");//only swipe is allowed
			whichForm = SWIPE_S_FRM;
		}
	}
	else if(bC30Request == PAAS_TRUE && isContactlessEnabledInDevice() == PAAS_FALSE)//Contactless is disabled
	{
		whichForm = SWIPE_SI_FRM;
	}

	while(1)
	{
		/* Initialize the screen */
		initUIReqMsg(&stUIReq, BATCH_REQ);

		iDisableFormFlag = 1;

		initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

		if(pszTranKey != NULL)
		{
			rv = getAmtDtlsForPymtTran(pszTranKey, &stAmtDtls);
			if(rv != SUCCESS)	// CID-67283: 3-Feb-16: MukeshS3: Checking for return value
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get Amt details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			/* get the func and cmd of the current transaction instance */
			getFxnNCmdForSSITran(pszTranKey, &iFxn, &iCmd);

		}

		if(strlen(stAmtDtls.tranAmt) > 0)
		{
			/* Get the Amount Due Title */
			memset(szTitle, 0x00, sizeof(szTitle));
			memset(szTitle1, 0x00, sizeof(szTitle1));

			/*
			 * Praveen_P1: According to FRD3.50 we need show different
			 * message for the Activation command on the card swipe screen
			 */
			if(iCmd == SSI_ACTIVATE)
			{
				fetchScrTitle(szTitle1, ACTIVATION_AMT_TITLE);
			}
			else if(iCmd == SSI_ADDVAL)
			{
				fetchScrTitle(szTitle1, ADDVALUE_AMT_TITLE );
			}
			else
			{
				fetchScrTitle(szTitle1, TRAN_AMT_TITLE);
			}
			if (iCmd == SSI_CREDIT)
			{
				sprintf(szTitle, "%s -$%s", szTitle1, stAmtDtls.tranAmt);
			}
			else
			{
			sprintf(szTitle, "%s $%s", szTitle1, stAmtDtls.tranAmt);
			}

			/*Setting the ctrlids for the form*/
			ctrlIds[0] 	= PAAS_SWIPE_LBL_8;
			ctrlIds[1] 	= PAAS_SWIPE_LBL_9;

			/*Setting the screen title to show amount due labels*/
			setScreenTitle(szTitle, szAmtTitle, ctrlIds, 2, giLabelLen, stUIReq.pszBuf);
		}
		else if(iCmd == SSI_BAL)
		{
			/*
			 * Praveen_P1: Showing message for the Balance Inquiry
			 */
			memset(szTitle, 0x00, sizeof(szTitle));
			fetchScrTitle(szTitle, BALANCE_CARD_TITLE);

			/*Setting the ctrlids for the form*/
			ctrlIds[0] 	= PAAS_SWIPE_LBL_8;
			ctrlIds[1] 	= PAAS_SWIPE_LBL_9;

			/*Setting the screen title to show amount due labels*/
			setScreenTitle(szTitle, szAmtTitle, ctrlIds, 2, giLabelLen, stUIReq.pszBuf);
		}
		else if (iCmd == SSI_TOKENQUERY)
		{
			/*
			 * AjayS2: Showing message for the Token Query
			 */
			memset(szTitle, 0x00, sizeof(szTitle));
			fetchScrTitle(szTitle, TOKEN_QUERY_CARD_TITLE);

			/*Setting the ctrlids for the form*/
			ctrlIds[0] 	= PAAS_SWIPE_LBL_8;
			ctrlIds[1] 	= PAAS_SWIPE_LBL_9;

			/*Setting the screen title to show amount due labels*/
			setScreenTitle(szTitle, szAmtTitle, ctrlIds, 2, giLabelLen, stUIReq.pszBuf);
		}

		/*checking for command and VAS data to set VAS_MODE*/
		if(PAAS_TRUE == isWalletEnabled())
		{
			//AjayS2: 13-Jan-2016
			//If wallet is enabled we have to send TERM_CAP as Payment only, because from next release
			//default TERM_CAP is taken as VAS and PAYMENT
			memset(szVASMode, 0x00, sizeof(szVASMode));
			if((iCmd == SSI_SALE)|| (iCmd == SSI_PREAUTH)|| (iCmd == SSI_POSTAUTH)|| (iCmd == SSI_VOICEAUTH))
			{
				if(pszTranKey != NULL)
				{
					memset(&stCustInfoDtls, 0x00, sizeof(CUSTINFODTLS_STYPE));
					getCustInfoDtlsForPymtTran(pszTranKey, &stCustInfoDtls);

					if((strlen(stCustInfoDtls.szNFCVASMode) > 0) && ((strcmp(stCustInfoDtls.szNFCVASMode, VAS_ONLY) == SUCCESS)
							|| (strcmp(stCustInfoDtls.szNFCVASMode, VAS_AND_PAYMENT) == SUCCESS) || (strcmp(stCustInfoDtls.szNFCVASMode, VAS_OR_PAYMENT) == SUCCESS) ))
					{
						strcpy(szNFCVASMode, stCustInfoDtls.szNFCVASMode);
					}
				}
				if(isProvisionSentDuringPymt() == PAAS_FALSE && (strcmp(stCustInfoDtls.szNFCVASMode, PAYMENT_ONLY) != SUCCESS))
				{
					if(strlen(szNFCVASMode) > 0)
					{
						strcpy(szVASMode, szNFCVASMode);
					}
					else
					{
						strcpy(szVASMode, VAS_AND_PAYMENT);
					}
					sprintf(szFileName, "%s", "cardscreen_wallet_banner.png");
					getFileNamewithPlatformPrefix(szFileName);
					setStringValueWrapper(PAAS_SWIPE_WALLET_IMG, PROP_STR_IMAGE_FILE_NAME, szFileName, stUIReq.pszBuf);
					setBoolValueWrapper(PAAS_SWIPE_WALLET_IMG, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
				}
				else
				{
					strcpy(szVASMode, PAYMENT_ONLY);
					debug_sprintf(szDbgMsg, "%s: Provision Paas Sent or Currently Out of Session, May be Dummy Sale Not Showing VAS Image/text For Dummy Sale ",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				strcpy(szVASMode, PAYMENT_ONLY);
			}
		}

		/* Show the screen */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		if(getCardReadEnabled() == PAAS_FALSE && bCardReadEnabled)
		{
			debug_sprintf(szDbgMsg, "%s: Card Reader is not enabled, so would have received the card data",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		/* Send the message to UI agent for showing the swipe card screen */
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while showing form [%s]",
					__FUNCTION__, frmName[whichForm]);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;

			debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);

			return rv;
			//	break;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				if(bBLDataRcvd == PAAS_TRUE)
				{
					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait 		= PAAS_FALSE;
					}
					else if((stBLData.uiRespType == UI_CARD_RESP || stBLData.uiRespType == UI_EMV_RESP) && (stBLData.iStatus == SUCCESS))
					{
						/* KranthiK1: Doing this step so that we set card read done only in the case of U02 response,
						 * C31 Response, S20 response not for any other case.
						 */
						if(stBLData.uiRespType == UI_CARD_RESP)
						{
							bCardReadDone = PAAS_TRUE;
						}
						else if(strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_U02_REQ) == 0 ||
								 strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_C31_RESP) == 0)
						{
							bCardReadDone = PAAS_TRUE;
						}
						/* Daivik - 20-11-2015 - Based on read card done we will be using the same card data ( which was recieved due to the swipe in pre-swipe screen ) to store in card details
						 * structure. But we process this after we get the Show form response, and hence we are keeping a copy of the BLData structure to process the card response later.
						 */
						memcpy(&(stLocUIResp.stRespDtls),&(stBLData.stRespDtls),sizeof(ALLRESP_STYPE));
						memcpy(&(stLocUIResp.iStatus),&(stBLData.iStatus),sizeof(int));
						memcpy(&(stLocUIResp.uiRespType),&(stBLData.uiRespType),sizeof(UIRESP_ENUM));
//						bBLDataRcvd = PAAS_FALSE; //Once we get the XIFM response , this will be made true.
						releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
						break;
					}
					else
					{
						bBLDataRcvd = PAAS_FALSE;
					}
				}
				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(10);
			}
			bWait = PAAS_TRUE;
		}

		if(!bCardReadDone)
		{
			if(bC30Request == PAAS_TRUE)
			{
				if(bCardReadEnabled)
				{
					bWaitForFirstC3108 = PAAS_TRUE;
				}
				sendC30CmdToGetChipCardData(pszTranKey ,szVASMode, stCustInfoDtls.szMerchantIndex, stCustInfoDtls.szCustData);
			}
			else
			{
				if(getCardReadEnabled() == PAAS_FALSE)
				{
					sendS20CmdToGetMSRCardData(szVASMode, stCustInfoDtls.szMerchantIndex, stCustInfoDtls.szCustData);
				}
				else
				{
					if(isWalletEnabled())
					{
						debug_sprintf(szDbgMsg, "%s: Resending S20 because Wallet is Enabled VAS Related Fields May have been Changed", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						disableCardReaders();
						sendS20CmdToGetMSRCardData(szVASMode, stCustInfoDtls.szMerchantIndex, stCustInfoDtls.szCustData);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Not sending Card read command, since card read is enabled already", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
			}
		}
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			if(whichForm == SWIPE_STI_FRM)
			{
				strcpy(szAppLogData, "Showing Swipe/Tap/Insert Card Screen");
			}
			else if(whichForm == SWIPE_SI_FRM)
			{
				strcpy(szAppLogData, "Showing Swipe/Insert Card Screen");
			}
			else if(whichForm == SWIPE_ST_FRM)
			{
				strcpy(szAppLogData, "Showing Swipe/Tap Card Screen");
			}
			else if(whichForm == SWIPE_S_FRM)
			{
				strcpy(szAppLogData, "Showing Swipe Card Screen");
			}
			else if(whichForm == SWIPE_T_FRM)
			{
				strcpy(szAppLogData, "Showing Tap Card Screen");
			}
			else
			{
				strcpy(szAppLogData, "Showing Card Read Screen");
			}
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}

		break;
	}

	/* Wait for the card data to be received from the UI agent */
	while(bWait == PAAS_TRUE)
	{
		CHECK_POS_INITIATED_STATE;
		if(bBLDataRcvd == PAAS_TRUE)
		{
			acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			if(bCardReadDone)
			{
				/* Daivik:20-11-2015 - If card read was done.i.e if card was swiped in pre-swipe screen and we recieved the response after coming to this function,
				 * then we need to use that data to process. We had saved the card response earlier in the local structure which we are copying back to the main BLdata structure.
				 */
				memcpy(&(stBLData.stRespDtls),&(stLocUIResp.stRespDtls),sizeof(ALLRESP_STYPE));
				memcpy(&(stBLData.iStatus),&(stLocUIResp.iStatus),sizeof(int));
				memcpy(&(stBLData.uiRespType),&(stLocUIResp.uiRespType),sizeof(UIRESP_ENUM));
				/* Once we are done using this , then we should set this variable to false, so that we dont enter into this section of code again
				 */
				bCardReadDone = PAAS_FALSE;
			}

			if(stBLData.uiRespType == UI_XEVT_RESP)
			{
				pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

				if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
				{
					if(pstXevt->uiCtrlID == PAAS_SWIPE_FK_1)
					{
						debug_sprintf(szDbgMsg, "%s: CANCEL", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "User Pressed Cancel Button on Swipe Card Screen");
							addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
						}

						rv = UI_CANCEL_PRESSED;
						bWait = PAAS_FALSE;
					}
					else if(pstXevt->uiCtrlType == 50) // Smart card event
					{
						debug_sprintf(szDbgMsg, "%s: Smart card event", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						strcpy(szAppLogData, "Card Inserted");

						rv = UI_CARD_INSERTED;
						bWait = PAAS_FALSE;
					}
					else if(pstXevt->uiCtrlType == 90) //STB Data Event
					{
						debug_sprintf(szDbgMsg, "%s: STB Event Received", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						/* Assisnging the Data pointer, please make sure that it is freed in the caller function*/
						*szSTBData = pstXevt->pszData;

						bWait = PAAS_FALSE;
					}
					bBLDataRcvd = PAAS_FALSE;
					//bWait = PAAS_FALSE;//ArjunU1: Moved up to wait until particular event generated.
				}
			}
			else if(stBLData.uiRespType == UI_CARD_RESP)
			{
				if(stBLData.iStatus == SUCCESS)
				{
					pstCard = &(stBLData.stRespDtls.stCrdTrkInfo);

					debug_sprintf(szDbgMsg, "%s: Received the Card Data",
							__FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(pstCard->iCardSrc == CRD_MSR)
					{
						strcpy(szAppLogData, "Card Details Captured From MSR Card");
					}
					else
					{
						strcpy(szAppLogData, "Card Details Captured From Contactless Card");
					}

					/* Track 1 --- */
					iLen = strlen(pstCard->szTrk1);
					if(iLen > 0)
					{
						szTracks[0] = (char *)malloc(iLen + 1);
						memset(szTracks[0], 0x00, iLen + 1);
						memcpy(szTracks[0], pstCard->szTrk1, iLen);
					}

					/* Track 2 --- */
					iLen = strlen(pstCard->szTrk2);
					if(iLen > 0)
					{
						szTracks[1] = (char *)malloc(iLen + 1);
						memset(szTracks[1], 0x00, iLen + 1);
						memcpy(szTracks[1], pstCard->szTrk2, iLen);
					}

					/* Track 3 --- */
					iLen = strlen(pstCard->szTrk3);
					if(iLen > 0)
					{
						szTracks[2] = (char *)malloc(iLen + 1);
						memset(szTracks[2], 0x00, iLen + 1);
						memcpy(szTracks[2], pstCard->szTrk3, iLen);
					}

					/* Eparms Data  --- */
					iLen = strlen(pstCard->szEparms);
					if(iLen > 0)
					{
						szTracks[3] = (char *)malloc(iLen + 1);
						memset(szTracks[3], 0x00, iLen + 1);
						memcpy(szTracks[3], pstCard->szEparms, iLen);
					}

					/* Service Code Byte One --- */
					iLen = strlen(pstCard->szServiceCodeByteOne);
					if(iLen > 0 && szServiceCodeByteOne != NULL)
					{
						memcpy(szServiceCodeByteOne, pstCard->szServiceCodeByteOne, iLen);
					}

					/*PayPass Type */
					iLen = strlen(pstCard->szPayPassType);
					if(iLen > 0 && pszPayPassType != NULL)
					{
						memcpy(pszPayPassType, pstCard->szPayPassType, iLen);
					}

					/* Copy clear expiry date*/
					iLen = strlen(pstCard->szClrExpDt);
					if(iLen > 0)
					{
						memset(szClrDt, 0x00, iLen + 1);
						memcpy(szClrDt, pstCard->szClrExpDt, iLen);

						debug_sprintf(szDbgMsg, "%s: Copying Clear Expiry date",
								__FUNCTION__);
						APP_TRACE(szDbgMsg);

					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: No Clear exp date to copy",
								__FUNCTION__);
						APP_TRACE(szDbgMsg);
					}

					/* Card source */
					* cardSrc = pstCard->iCardSrc;

					if(szRsaTracks != NULL) // T_RaghavendranR1 CID 83342 (#1 of 1): Explicit null dereferenced (FORWARD_NULL)
					{
						/*Copy the RSA Track 1 */
						iLen = strlen(pstCard->szRsaTrk1);
						if(iLen > 0)
						{
							szRsaTracks[0] = (char *)malloc(iLen + 1);
							memset(szRsaTracks[0], 0x00, iLen + 1);
							memcpy(szRsaTracks[0], pstCard->szRsaTrk1, iLen);
						}

						/*Copy the RSA Track 2 */
						iLen = strlen(pstCard->szRsaTrk2);
						if(iLen > 0)
						{
							szRsaTracks[1] = (char *)malloc(iLen + 1);
							memset(szRsaTracks[1], 0x00, iLen + 1);
							memcpy(szRsaTracks[1], pstCard->szRsaTrk2, iLen);
						}
					}

					if(szVsdEncData != NULL)
					{
						/* Copy VSD encrypted  data*/
						iLen = strlen(pstCard->szVsdEncBlob);
						if(iLen > 0)
						{
							szVsdEncData[0] = (char *)malloc(iLen + 1);
							memset(szVsdEncData[0], 0x00, iLen + 1);
							memcpy(szVsdEncData[0], pstCard->szVsdEncBlob, iLen);
						}

						/* Copy KSN associated with VSD encrypted  data*/
						iLen = strlen(pstCard->szVsdKSN);
						if(iLen > 0)
						{
							szVsdEncData[1] = (char *)malloc(iLen + 1);
							memset(szVsdEncData[1], 0x00, iLen + 1);
							memcpy(szVsdEncData[1], pstCard->szVsdKSN, iLen);
						}

						/* Copy IV associated with VSD encrypted  data*/
						iLen = strlen(pstCard->szVsdIV);
						if(iLen > 0)
						{
							szVsdEncData[2] = (char *)malloc(iLen + 1);
							memset(szVsdEncData[2], 0x00, iLen + 1);
							memcpy(szVsdEncData[2], pstCard->szVsdIV, iLen);
						}

						/* Copy Clear Track data if present*/
						if(strlen(pstCard->szClrTrk2))
						{
							iLen = strlen(pstCard->szClrTrk2);
							szVsdEncData[3] = (char *)malloc(iLen + 1);
							memset(szVsdEncData[3], 0x00, iLen + 1);
							memcpy(szVsdEncData[3], pstCard->szClrTrk2, iLen);
						}
						else if(strlen(pstCard->szClrTrk1))
						{
							iLen = strlen(pstCard->szClrTrk1);
							szVsdEncData[3] = (char *)malloc(iLen + 1);
							memset(szVsdEncData[3], 0x00, iLen + 1);
							memcpy(szVsdEncData[3], pstCard->szClrTrk1, iLen);
						}
					}

					if(pstCard->stVasDataDtls.bVASPresent)
					{
						if(isWalletEnabled())
						{
							debug_sprintf(szDbgMsg, "%s: Received VAS Data from Card Insert/Swipe Screen", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							memcpy(pstVasDataDtls, &pstCard->stVasDataDtls, sizeof(VASDATA_DTLS_STYPE));
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: ERROR:: Wallet is NOT Enabled but Received VAS Data from Card Insert/Swipe Screen", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
					}
					else if(pstCard->stVasDataDtls.bProvisionPassSent && strlen(stCustInfoDtls.szCustData) > 0)
					{
						if(isWalletEnabled())
						{
							debug_sprintf(szDbgMsg, "%s: Provision Pass Sent On Swipe/Insert Screen", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							memcpy(pstVasDataDtls, &pstCard->stVasDataDtls, sizeof(VASDATA_DTLS_STYPE));
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: ERROR:: Wallet is NOT Enabled but Sent Provision Pass On Card Insert/Swipe Screen", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
					}
					rv = SUCCESS;
				}
				else if(stBLData.iStatus == ERR_BAD_CARD)
				{
					debug_sprintf(szDbgMsg, "%s: Max bad card reads done",
							__FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(szAppLogData, "Bad Card Read from User");

					rv = ERR_BAD_CARD;

				}
				else if(stBLData.iStatus == ERR_USR_TIMEOUT)
				{
					debug_sprintf(szDbgMsg, "%s: User Timed Out during Card Swipe", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(szAppLogData, "User Timed Out During Card Swipe");

					rv = ERR_USR_TIMEOUT;

				}
				if(iAppLogEnabled == 1)
				{
					addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
				}
				bBLDataRcvd = PAAS_FALSE;

				/*
				 * If STB is enabled, then we need to wait for the XEVT data
				 * thats why not setting bwait to false here
				 */
				if(!isSTBLogicEnabled() || rv)
				{
					bWait = PAAS_FALSE;
				}
				else if (isSTBLogicEnabled() && *szSTBData)
				{
					bWait = PAAS_FALSE;
				}
			}
			else if(stBLData.uiRespType == UI_EMV_RESP)
			{
				if( strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd,EMV_U00_REQ) ==0 || strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_U02_REQ) ==0 )
				{
					if(stBLData.iStatus == SUCCESS)
					{
						//DAIVIK: When we receive U02 - this indicates card was inserted, so if we have set bWaitForFirstC3108, then we can unset it.
						bWaitForFirstC3108 = PAAS_FALSE;
						debug_sprintf(szDbgMsg, "%s: Received U02 EMV Response",__FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bEMVRetry == PAAS_TRUE)
						{
							/* Initialize the screen */
							if(iAppLogEnabled == 1)
							{
								if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bCardInserted == PAAS_TRUE)
								{
									strcpy(szAppLogData, "Error While Inserting the Card");
								}
								else if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bCardSwiped == PAAS_TRUE)
								{
									strcpy(szAppLogData, "Error While Swiping the Card");
								}
								else if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bCardTapped == PAAS_TRUE)
								{
									strcpy(szAppLogData, "Error While Tapping the Card");
								}
								else
								{
									strcpy(szAppLogData, "Error While Inserting/Swiping/Tapping card");
								}
								addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							initUIReqMsg(&stUIReq, BATCH_REQ);


							if(getXPILibMode())
							{
								// 10-Dec-15: MukeshS3: Need to intialize the FORM & set all the properties again to show the FORM.
								// when XPI runs as library, because XPI would have painted his FORM at last.
								initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);	//T_MukeshS3

								/*Setting the screen title to show amount due labels*/
								setScreenTitle(szTitle, szAmtTitle, ctrlIds, 2, giLabelLen, stUIReq.pszBuf);			//T_MukeshS3
								//AkshayaM1 : when a Bad swipe is done , after getting the card read error message.
								//When we retry to the old screen , the apple pay logo and Loyalty information recevied message disappers.
								//Adding in XPILIBMODE
								if(isWalletEnabled() == PAAS_TRUE && ((iCmd == SSI_SALE)|| (iCmd == SSI_PREAUTH)|| (iCmd == SSI_POSTAUTH)|| (iCmd == SSI_VOICEAUTH)))
								{

									debug_sprintf(szDbgMsg, "%s:Wallet enabled and showing the Form with Apple Pay Logo",__FUNCTION__);
									APP_TRACE(szDbgMsg);
									if((strlen(stCustInfoDtls.szNFCVASMode) > 0) && ((strcmp(stCustInfoDtls.szNFCVASMode, VAS_ONLY) == SUCCESS)
											|| (strcmp(stCustInfoDtls.szNFCVASMode, VAS_AND_PAYMENT) == SUCCESS) || (strcmp(stCustInfoDtls.szNFCVASMode, VAS_OR_PAYMENT) == SUCCESS) ))
									{
										strcpy(szNFCVASMode, stCustInfoDtls.szNFCVASMode);
									}

									if(isProvisionSentDuringPymt() == PAAS_FALSE && (strcmp(stCustInfoDtls.szNFCVASMode, PAYMENT_ONLY) != SUCCESS))
									{
										if(strlen(szNFCVASMode) > 0)
										{
											strcpy(szVASMode, szNFCVASMode);
										}
										else
										{
											strcpy(szVASMode, VAS_AND_PAYMENT);
										}
										sprintf(szFileName, "%s", "cardscreen_wallet_banner.png");
										getFileNamewithPlatformPrefix(szFileName);
										setStringValueWrapper(PAAS_SWIPE_WALLET_IMG, PROP_STR_IMAGE_FILE_NAME, szFileName, stUIReq.pszBuf);
										setBoolValueWrapper(PAAS_SWIPE_WALLET_IMG, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
									}
//									else if(isProvisionSentDuringPymt() == PAAS_FALSE && pstVasDataDtls->bVASsentToPOS == PAAS_TRUE)
//									{
//										/*Removing the Loyalty Information Screen and Showing only Payment Only mode on doing Bad Card Read on Payment only mode*/
//										strcpy(szVASMode, PAYMENT_ONLY);
//									}
									else
									{
										strcpy(szVASMode, PAYMENT_ONLY);
										debug_sprintf(szDbgMsg, "%s: Provision Paas Sent or Currently Out of Session, May be Dummy Sale Not Showing VAS Image/text For Dummy Sale ",__FUNCTION__);
										APP_TRACE(szDbgMsg);
									}
								}
							}
							showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

							if(sendUIReqMsg(stUIReq.pszBuf) != SUCCESS) // T_RaghavendranR1 CID 83344 (#1 of 1): Unchecked return value (CHECKED_RETURN)
							{
								debug_sprintf(szDbgMsg, "%s: Error while communicating with UI Agent", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}

						}
						else if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bCardInserted == PAAS_TRUE)
						{
							/* KranthiK1: Added this only to show the processing form
							 * as C30 is taking a long time to be respond.
							 * Remove later if not required
							 */
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "EMV Card Inserted");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							bBLDataRcvd = PAAS_FALSE;

							releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

							//showPSGenDispForm(MSG_PROCESSING, STANDBY_ICON, 0);
						}
						else if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bSTBDataReceived == PAAS_TRUE)
						{
							debug_sprintf(szDbgMsg, "%s: Received STB data",__FUNCTION__);
							APP_TRACE(szDbgMsg);

							if(strlen(stBLData.stRespDtls.stEmvDtls.szSpinBinData) > 0)
							{
								pszTemp = (char *) malloc(sizeof(char) * (strlen(stBLData.stRespDtls.stEmvDtls.szSpinBinData) + 1));
								if(pszTemp == NULL)
								{
									debug_sprintf(szDbgMsg, "%s: Error while allocating memory", __FUNCTION__);
									APP_TRACE(szDbgMsg);
									rv = FAILURE;
									break;
								}
								memset(pszTemp, 0x00, (strlen(stBLData.stRespDtls.stEmvDtls.szSpinBinData) + 1));

								memcpy(pszTemp, stBLData.stRespDtls.stEmvDtls.szSpinBinData, strlen(stBLData.stRespDtls.stEmvDtls.szSpinBinData));

								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "STB Data Received");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								/* Assisnging the Data pointer, please make sure that it is freed in the caller function*/
								*szSTBData = pszTemp;
							}

							if(*cardSrc)
							{
								bWait = PAAS_FALSE;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Received STB Data Waiting now for Card Data", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}
						}
						bBLDataRcvd = PAAS_FALSE;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Got BL Data Status = %d.",__FUNCTION__, stBLData.iStatus);
						APP_TRACE(szDbgMsg);
					}
				}
				else if ( strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_C31_RESP) == 0)
				{
					memset(&stEmvDtls, 0x00, sizeof(EMVDTLS_STYPE));
					memcpy(&stEmvDtls, &(stBLData.stRespDtls.stEmvDtls), sizeof(EMVDTLS_STYPE));

					debug_sprintf(szDbgMsg, "%s: Got EMV Command Response = %s",__FUNCTION__, stEmvDtls.szEMVRespCode);
					APP_TRACE(szDbgMsg);

					if(atoi(stEmvDtls.szEMVRespCode) == EMV_ERROR_TXN_CANCELLED && bWaitForFirstC3108)
					{
						/* KranthiK1:
						 * Setting it false as it would be referring to the previous C30 which got cancelled
						 * because a new C30 is sent with additional parameters (Amount) and no card read was
						 * done on the previuos C30
						 */
						bBLDataRcvd = PAAS_FALSE;

						bWaitForFirstC3108 = PAAS_FALSE;
						/* Setting the card Read back to true as the C3108 received refers to the first C30
						 * We need to Wait for the second C30 response
						 */
						setCardReadEnabled(PAAS_TRUE);
					}
					else
					{
						rv = getUIRespCodeOfEmvCmdResp(EMV_C30, (atoi(stEmvDtls.szEMVRespCode)));
						if(rv == SUCCESS && pszTranKey != NULL)
						{
							updateEmvDtlsinCardDtlsForPymtTran(pszTranKey, &stEmvDtls);
							*cardSrc = stEmvDtls.iCardSrc;
							debug_sprintf(szDbgMsg, "%s: EMV Details Stored with Card source: %d",__FUNCTION__, *cardSrc);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								if(stEmvDtls.iCardSrc == CRD_EMV_CT)
								{
									strcpy(szAppLogData, "EMV Card was Inserted and Read Successfully");
								}
								else if(stEmvDtls.iCardSrc == CRD_EMV_CTLS || stEmvDtls.iCardSrc == CRD_RFID)
								{
									strcpy(szAppLogData, "EMV Contactless Card was Tapped and Read Successfully");
								}
								else if(stEmvDtls.iCardSrc == CRD_EMV_MSD_CTLS)
								{
									strcpy(szAppLogData, "MSD Contactless Card was Tapped and Read Successfully");
								}
								else
								{
									strcpy(szAppLogData, "Card Data Read Successfully");
								}
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}

							if(stEmvDtls.stVasDataDtls.bVASPresent)
							{
								if(isWalletEnabled())
								{
									debug_sprintf(szDbgMsg, "%s: Received VAS Data from Card insert/Swipe Screen", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									memcpy(pstVasDataDtls, &(stEmvDtls.stVasDataDtls), sizeof(VASDATA_DTLS_STYPE));
								}
								else
								{
									debug_sprintf(szDbgMsg, "%s: ERROR:: Wallet is NOT Enabled but Received VAS Data from Card Insert/Swipe Screen", __FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
							}
							else if(stEmvDtls.stVasDataDtls.bProvisionPassSent && strlen(stCustInfoDtls.szCustData) > 0)
							{
								if(isWalletEnabled())
								{
									debug_sprintf(szDbgMsg, "%s: Provision Pass Sent On Swipe/Insert Screen", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									memcpy(pstVasDataDtls, &(stEmvDtls.stVasDataDtls), sizeof(VASDATA_DTLS_STYPE));
								}
								else
								{
									debug_sprintf(szDbgMsg, "%s: ERROR:: Wallet is NOT Enabled but Sent Provision Pass On Card Insert/Swipe Screen", __FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
							}
						}
						else
						{
							//Setting CRD_EMV_CT as card response to differentiate b/w error for EMV card and MSD card
							*cardSrc = CRD_EMV_CT;
						}

						bBLDataRcvd = PAAS_FALSE;

						/*
						 * If STB is enabled, then we need to wait for the XEVT data
						 * thats why not setting bwait to false here
						 */
						if(!isSTBLogicEnabled() || rv)
						{
							bWait = PAAS_FALSE;
						}
						else if (isSTBLogicEnabled() && *szSTBData)
						{
							bWait = PAAS_FALSE;
						}
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: EMV Response received is neither U02 not C31, so ignoring it", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					bBLDataRcvd = PAAS_FALSE;
				}
			}
			else if(stBLData.iStatus == ERR_BAD_CARD)
			{
				debug_sprintf(szDbgMsg, "%s: Max bad card reads done",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(szAppLogData, "Bad Card Read from User");

				bBLDataRcvd = PAAS_FALSE;
				bWait = PAAS_FALSE;

				rv = ERR_BAD_CARD;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Response other than Card Data is received. Ignoring it", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				bBLDataRcvd = PAAS_FALSE;
			}
		}
#if 0
		else if(stBLData.iStatus == ERR_BAD_CARD)
		{
			debug_sprintf(szDbgMsg, "%s: Max bad card reads done", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			bBLDataRcvd = PAAS_FALSE;
			bWait = PAAS_FALSE;

			rv = ERR_BAD_CARD;

		}
#endif
		releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
		if(bWait == PAAS_FALSE)
		{
			break;
		}
		/* Wait for message from the UI agent */
		svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
	}

	/* KranthiK1:
	 * Sending the cancel request just to make sure that lights are turned off if there are any being on
	 * because of cross over of the responses.
	 *
	 * Not Sending in EMV Card Insert because card is still inserted.
	 * Issue was observed:When second C30 reached XPI after processing of 1st C30. We just pick up the response
	 * and move ahead, but due to second C30,card readers were enabled.
	 */
	/* Daivik:10/8/16-With back to back C30 command , there is always a confusion whether the response that we have recieved is for the first C30 or the second C30.
	 * There is a possibility that we assume we got response for second C30 , but the response could have actually been from first C30.
	 * This can lead to situation where the Card reader lights are still enabled. We need to send cancel command to ensure we disable the card readers.
	 * TODO: We could still get other C30 responses (C3102,C3122,C3142) for which we are not doing cancel
	 */
	//if((!((*cardSrc == CRD_EMV_CT) && (rv == SUCCESS))) && isEmvEnabledInDevice())
	if(isEmvEnabledInDevice() && ((*cardSrc == CRD_MSR) || (rv == UI_CANCEL_PRESSED)))
	{
		debug_sprintf(szDbgMsg, "%s: Sending the cancel request command since CardSrc=%d rv=%d", __FUNCTION__,*cardSrc,rv);
		APP_TRACE(szDbgMsg);

		cancelRequest();
		setCardReadEnabled(PAAS_FALSE);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSTBData
 *
 * Description	: This API would be used to get the STB data.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getSTBData(char **szSTBData)
{
	int				rv					= SUCCESS;
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Wait for the card data to be received from the UI agent */
	while(bWait == PAAS_TRUE)
	{
		CHECK_POS_INITIATED_STATE;

		if(bBLDataRcvd == PAAS_TRUE)
		{
			acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

			if(stBLData.uiRespType == UI_XEVT_RESP)
			{
				pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

				if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
				{
					if(pstXevt->uiCtrlType == 90) //STB Data Event
					{
						debug_sprintf(szDbgMsg, "%s: STB Event Received", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						/* Assisnging the Data pointer, please make sure that it is freed in the caller function*/
						*szSTBData = pstXevt->pszData;

						bWait = PAAS_FALSE;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Some other XEVT received", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					bBLDataRcvd = PAAS_FALSE;
				}
			}
			releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
		}

		/* Wait for message from the UI agent */
		svcWait(2);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCardDataInfo
 *
 * Description	: This API would be used to get the RFID/NFC card data.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getCardDataInfo()
{
	int				rv				= SUCCESS;
	UIREQ_STYPE		stUIReq;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = SWIPE_STI_FRM;

	/* Initialize the screen */
	initUIReqMsg(&stUIReq, BATCH_REQ);

	/* Enable RFID/NFC card read modes at the UI agent (TRACK 1 and TRACK 2 request) */
	getCardDataUtil(PAAS_TRUE, PAAS_TRUE, PAAS_FALSE, stUIReq.pszBuf);

	/* Send the message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while sending Q13 command", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	//Don't wait for response here. Waiting for response in calling function.

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getVclCardData
 *
 * Description	: This API would be used to get the card data for XVCL's
 * 					DEVICE_REG command.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getVclCardData(char ** szTracks, char *szClrDt, int eReqType)
{
	int				rv				= SUCCESS;
	int				iLen			= 0;
	PAAS_BOOL		bWait			= PAAS_TRUE;
	CARD_TRK_PTYPE	pstCard			= NULL;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Initialize the screen */
	initUIReqMsg(&stUIReq, BATCH_REQ);

	/* Form XVCL command */
	xvclCmdWrapper(PAAS_TRUE, stUIReq.pszBuf, eReqType);

	/* This while loop will execute only once, being used as a replacement for
	 * GOTO statement. The purpose is to have only one return statement at the
	 * end of the function, while the return values being assigned anywhere in
	 * the function. */
	while(1)
	{
		/* Send the message to UI agent for for device registration */
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while sending the command",
											__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;

			break;
		}

		/* Wait for the device reg card data to be received from the UI agent */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_VCL_RESP)
				{
					if(stBLData.iStatus == SUCCESS)
					{
						pstCard = &(stBLData.stRespDtls.stCrdTrkInfo);

						debug_sprintf(szDbgMsg, "%s: Received the Card Data",
										__FUNCTION__);
						APP_TRACE(szDbgMsg);

						/* Track 1 --- */
						iLen = strlen(pstCard->szTrk1);
						if(iLen > 0)
						{
							szTracks[0] = (char *)malloc(iLen + 1);
							memset(szTracks[0], 0x00, iLen + 1);
							memcpy(szTracks[0], pstCard->szTrk1, iLen);
						}

						/* Track 2 --- */
						iLen = strlen(pstCard->szTrk2);
						if(iLen > 0)
						{
							szTracks[1] = (char *)malloc(iLen + 1);
							memset(szTracks[1], 0x00, iLen + 1);
							memcpy(szTracks[1], pstCard->szTrk2, iLen);
						}

						/* Track 3 --- */
						iLen = strlen(pstCard->szTrk3);
						if(iLen > 0)
						{
							szTracks[2] = (char *)malloc(iLen + 1);
							memset(szTracks[2], 0x00, iLen + 1);
							memcpy(szTracks[2], pstCard->szTrk3, iLen);
						}

						/*Eparms data ----*/
						iLen = strlen(pstCard->szEparms);
						if(iLen > 0)
						{
							szTracks[3] = (char *)malloc(iLen + 1);
							memset(szTracks[3], 0x00, iLen + 1);
							memcpy(szTracks[3], pstCard->szEparms, iLen);
						}

						/* Copy clear expiry date*/
						iLen = strlen(pstCard->szClrExpDt);
						if(iLen > 0)
						{
							memset(szClrDt, 0x00, iLen + 1);
							memcpy(szClrDt, pstCard->szClrExpDt, iLen);
						}
					}
					else
					{
						debug_sprintf(szDbgMsg,"%s: Error, Device not prepped",
											__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
					}

					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			/* Wait for message from the UI agent */
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addMerchandiseToLIScreen
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int addMerchandiseToLIScreen_1(int pos, char * szDesc, char * szUnitPrice,
												char * szExtPrice, char * szQty, char *szFontCol, int iAllowLITran, char* pszBuf)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	int     iTemp  		    = 0;
	int     iDesLen         = 0;
	int     iplatform       = 0;
	int     iTotalLen		= 0;
	char	szLine[64]		= "";
	char    szColLine[73]   = "";
	char	szTmp[25]		= "";
	char	szTmpAmt[11]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	if((!iAllowLITran) && (whichForm != LI_DISP_FRM) && (whichForm != LI_DISP_OPT_FRM) && (whichForm != LI_FULL_DISP_FRM))
	{
		debug_sprintf(szDbgMsg, "%s: Cant update this screen [%s] (not LI)",
											__FUNCTION__, frmName[whichForm]);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		if(isFullLineItemDisplayEnabled() == PAAS_TRUE)
		{
			iDesLen = 40;

			iplatform = getDevicePlatform();
			if(iplatform == MODEL_MX915)
			{
				iTemp = 0;
			}
			else if(iplatform == MODEL_MX925)
			{
				iTemp = 7;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Should not come here ",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv= FAILURE;
				return rv;
			}
		}
		else
		{
			iDesLen = 17;
		}

		/* Proceed with the updation of the list item screen */
		memset(szLine, ' ', sizeof(szLine) - 1);

		/* Add the description */
		iLen = strlen(szDesc);
		if(iLen > iDesLen)
		{
			iLen = iDesLen;
		}

		memcpy(szLine, szDesc, iLen);

		iTotalLen = iDesLen + iTemp;

		/* Add the quantity */
		iLen = strlen(szQty);
		/*
		 * Displaying up to three char length for Quantity,
		 * but if more than 3 char len we are not showing it on the screen
		 * This is as per discussion with Sharvani/Wes
		 */
		if(iLen < 4)
		{
			memcpy(szLine + iTotalLen + 1 , szQty, iLen);
		}

		iTotalLen = iTotalLen + 4;

		/* Add the extended amount */
		formatAmount(szExtPrice, szTmpAmt, AMT_POSITIVE);
		if(szExtPrice[0] == '-')
		{
			sprintf(szTmp, "-$%s", szTmpAmt);
			iLen = strlen(szTmp);
			memcpy(szLine + iTotalLen + 1, szTmp, iLen);
		}
		else
		{
			if(strlen(szExtPrice) > 0)
			{
				sprintf(szTmp, "$%s", szTmpAmt);
			}
			else
			{
				sprintf(szTmp, "%s", szTmpAmt);
			}
			iLen = strlen(szTmp);
			memcpy(szLine + iTotalLen + 2, szTmp, iLen);
		}

		iTotalLen = iTotalLen + 2 + iLen;
		szLine[iTotalLen] = '\0';


		/* Add line in the list box */
		/* addListboxItemWrapper(PAAS_LINEITEMSCREEN_LST_1, pos, szLine, PAAS_FALSE
															, stUIReq.pszBuf); */
		/*
		 * Setting the KeepCopyFlag in XALI command to support to add lineitems
		 * anywhere with in the session
		 */
		if((szFontCol != NULL) && (strlen(szFontCol) > 0)) //Font color is passed
		{
			memset(szColLine, 0x00, sizeof(szColLine));
			sprintf(szColLine, "%c%s%c%s", '#', szFontCol, '#', szLine);
			addListboxItemWrapper(PAAS_LINEITEMSCREEN_LST_1, pos, szColLine, PAAS_TRUE
																				, pszBuf);

			debug_sprintf(szDbgMsg, "%s: After adding font color to the line [%s]",
														__FUNCTION__, szColLine);
			APP_TRACE(szDbgMsg);

		}
		else
		{
			addListboxItemWrapper(PAAS_LINEITEMSCREEN_LST_1, pos, szLine, PAAS_TRUE
																	, pszBuf);
		}
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addOfferItemToLIScreen
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int addOfferItemToLIScreen_1(int pos, char * szDesc, char * szOfferAmt, char *szFontCol, int iAllowLITran, char* pszBuf)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	int     iTemp  		    = 0;
	int     iDesLen         = 0;
	int     iplatform       = 0;
	int     iTotalLen		= 0;
	char	szTmpAmt[11]	= "";
	char	szLine[64]		= "";
	char    szColLine[73]   = "";
	char	szTmp[25]		= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if((!iAllowLITran) && (whichForm != LI_DISP_FRM) && (whichForm != LI_DISP_OPT_FRM) && (whichForm != LI_FULL_DISP_FRM))
	{
		debug_sprintf(szDbgMsg, "%s: Cant update this screen [%s] (not LI)",
											__FUNCTION__, frmName[whichForm]);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		if(isFullLineItemDisplayEnabled() == PAAS_TRUE)
		{
			iDesLen = 40;

			iplatform = getDevicePlatform();
			if(iplatform == MODEL_MX915)
			{
				iTemp = 0;
			}
			else if(iplatform == MODEL_MX925)
			{
				iTemp = 7;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Should not come here ",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv= FAILURE;
				return rv;
			}
		}
		else
		{
			iDesLen = 17;
		}

		/* Proceed with the updation of the list item screen */
		memset(szLine, ' ', sizeof(szLine) - 1);

		/* Add the description */
		iLen = strlen(szDesc);
		if(iLen > iDesLen)
		{
			iLen = iDesLen;
		}
		memcpy(szLine, szDesc, iLen);

		iTotalLen = iDesLen + iTemp;

		/* Add the offer amount */
		formatAmount(szOfferAmt, szTmpAmt, AMT_POSITIVE);
		sprintf(szTmp, "-$%s", szTmpAmt);
		iLen = strlen(szTmp);
		memcpy(szLine + iTotalLen + 5, szTmp, iLen);
		iTotalLen = iTotalLen + 5 + iLen;
		szLine[iTotalLen] = '\0';

		/* Add line in the list box */
		/*addListboxItemWrapper(PAAS_LINEITEMSCREEN_LST_1, pos, szLine, PAAS_FALSE
															, stUIReq.pszBuf);*/
		/*
		 * Setting the KeepCopyFlag in XALI command to support to add offer lineitems
		 * anywhere with in the session
		 */
		if((szFontCol != NULL) && (strlen(szFontCol) > 0)) //Font color is passed
		{
			memset(szColLine, 0x00, sizeof(szColLine));
			sprintf(szColLine, "%c%s%c%s", '#', szFontCol, '#', szLine);
			addListboxItemWrapper(PAAS_LINEITEMSCREEN_LST_1, pos, szColLine, PAAS_TRUE
																				, pszBuf);

			debug_sprintf(szDbgMsg, "%s: After adding font color to the line [%s]",
														__FUNCTION__, szColLine);
			APP_TRACE(szDbgMsg);

		}
		else
		{
			addListboxItemWrapper(PAAS_LINEITEMSCREEN_LST_1, pos, szLine, PAAS_TRUE
																	, pszBuf);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: overrideLineItemOnScreen_1
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int overrideLineItemOnScreen_1(int pos, char * szDesc, char * szQty,
															char * szExtPrice, char *szFontCol, int iAllowLITran, char* pszBuf)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	int     iTemp  		    = 0;
	int     iDesLen         = 0;
	int     iplatform       = 0;
	int     iTotalLen		= 0;
	char	szTmpAmt[11]	= "";
	char	szLine[64]		= "";
	char	szTmp[25]		= "";
	char    szColLine[73]   = "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if((!iAllowLITran) && (whichForm != LI_DISP_FRM) && (whichForm != LI_FULL_DISP_FRM) && (whichForm != LI_DISP_OPT_FRM))
	{
		debug_sprintf(szDbgMsg, "%s: Cant update this screen [%s] (not LI)",
											__FUNCTION__, frmName[whichForm]);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		if(isFullLineItemDisplayEnabled() == PAAS_TRUE)
		{
			iDesLen = 40;

			iplatform = getDevicePlatform();
			if(iplatform == MODEL_MX915)
			{
				iTemp = 0;
			}
			else if(iplatform == MODEL_MX925)
			{
				iTemp = 7;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Should not come here ",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv= FAILURE;
				return rv;
			}
		}
		else
		{
			iDesLen = 17;
		}

		/* Proceed with the updation of the list item screen */
		memset(szLine, ' ', sizeof(szLine) - 1);

		/* Add the description */
		iLen = strlen(szDesc);
		if(iLen > iDesLen)
		{
			iLen = iDesLen;
		}

		memcpy(szLine, szDesc, iLen);

		iTotalLen = iDesLen + iTemp;

		/* Add the quantity */
		iLen = strlen(szQty);
		/*
		 * Displaying up to three char length for Quantity,
		 * but if more than 3 char len we are not showing it on the screen
		 * This is as per discussion with Sharvani/Wes
		 */
		if(iLen < 4)
		{
			memcpy(szLine + iTotalLen + 1 , szQty, iLen);
		}

		iTotalLen = iTotalLen + 4;

		/* Add the extended amount */
		formatAmount(szExtPrice, szTmpAmt, AMT_POSITIVE);
		if(szExtPrice[0] == '-')
		{
			sprintf(szTmp, "-$%s", szTmpAmt);
			iLen = strlen(szTmp);
			memcpy(szLine + iTotalLen + 1, szTmp, iLen);
		}
		else
		{
			if(strlen(szExtPrice) > 0)
			{
				sprintf(szTmp, "$%s", szTmpAmt);
			}
			else
			{
				sprintf(szTmp, "%s", szTmpAmt);
			}
			iLen = strlen(szTmp);
			memcpy(szLine + iTotalLen + 2, szTmp, iLen);
		}

		iTotalLen = iTotalLen + 2 + iLen;
		szLine[iTotalLen] = '\0';

		/*
		 * Praveen_P1: 04 August 2014
		 * To override/update line item, we are doing remove line item and add line item
		 * From 3.03 FA version onwards new command has been added to update the
		 * particular line item i.e. XULI
		 * instead of XRLI and then XALI..we can send one command i.e. XULI
		 */

		/* Update the line in the list box */
		/*removeListboxItemWrapper(PAAS_LINEITEMSCREEN_LST_1, pos,
															stUIReq.pszBuf); */

		/*
		 * Setting the KeepCopyFlag in XALI command to support to add lineitems
		 * anywhere with in the session
		 */
		if((szFontCol != NULL) && (strlen(szFontCol) > 0)) //Font color is passed
		{
			memset(szColLine, 0x00, sizeof(szColLine));
			sprintf(szColLine, "%c%s%c%s", '#', szFontCol, '#', szLine);

			debug_sprintf(szDbgMsg, "%s: After adding font color to the line [%s]",
																	__FUNCTION__, szColLine);
			APP_TRACE(szDbgMsg);

			updateListboxItemWrapper(PAAS_LINEITEMSCREEN_LST_1, pos, szColLine, pszBuf);
			/* addListboxItemWrapper(PAAS_LINEITEMSCREEN_LST_1, pos, szColLine, PAAS_TRUE
															, stUIReq.pszBuf); */
		}
		else
		{
			/*addListboxItemWrapper(PAAS_LINEITEMSCREEN_LST_1, pos, szLine, PAAS_TRUE
																		, stUIReq.pszBuf); */
			updateListboxItemWrapper(PAAS_LINEITEMSCREEN_LST_1, pos, szLine, pszBuf);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addMerchandiseToLIScreen
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int removeLineItemFromScreen_1(int pos, int iAllowLITran, char* pszBuf)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if((!iAllowLITran) && (whichForm != LI_DISP_FRM) && (whichForm != LI_DISP_OPT_FRM) && (whichForm != LI_FULL_DISP_FRM))
	{
		debug_sprintf(szDbgMsg, "%s: Cant update this screen [%s] (not LI)",
											__FUNCTION__, frmName[whichForm]);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		removeListboxItemWrapper(PAAS_LINEITEMSCREEN_LST_1, pos, pszBuf);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateTaxAndTotalOnLIScreen
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int updateTaxAndTotalOnLIScreen(char * szTaxAmt, char * szTotAmt, char * szSubTotAmt, int iAllowLITran, char* pszBuf)
{
	int				rv				= SUCCESS;
	char			szTmpAmt[11]	= "";
	char			szFmtAmt[12]	= ""; // Includes $
	BTNLBL_STYPE	stBtnLbl;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if((!iAllowLITran) && (whichForm != LI_DISP_FRM) && (whichForm != LI_DISP_OPT_FRM) && (whichForm != LI_FULL_DISP_FRM))
	{
		debug_sprintf(szDbgMsg, "%s: Cant update this screen [%s] (not LI)",
											__FUNCTION__, frmName[whichForm]);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		/*
		 * Praveen_P1: 11 August 2016
		 * Tax Amount will be shown on the screen if that field is present
		 * if it is not present, TAX amount label will not be shown
		 */
		/* Proceed with the updation of the list item screen */
		/* TAX */
		if(szTaxAmt != NULL && (strlen(szTaxAmt) > 0))
		{
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_LBL_2, PROP_BOOL_VISIBLE, 1, pszBuf); //Setting visible property to TRUE incase previously its set to FALSE

			setBoolValueWrapper(PAAS_LINEITEMSCREEN_LBL_4, PROP_BOOL_VISIBLE, 1, pszBuf); //Setting visible property to TRUE incase previously its set to FALSE

			formatAmount(szTaxAmt, szTmpAmt, AMT_POSITIVE);
			if(szTaxAmt[0] == '-')
			{
				sprintf(szFmtAmt, "-$%s", szTmpAmt);
			}
			else
			{
				sprintf(szFmtAmt, " $%s", szTmpAmt);
			}
			setStringValueWrapper(PAAS_LINEITEMSCREEN_LBL_4, PROP_STR_CAPTION, szFmtAmt, pszBuf);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Tax Amount is not present in the request, not updating the label", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			setBoolValueWrapper(PAAS_LINEITEMSCREEN_LBL_2, PROP_BOOL_VISIBLE, 0, pszBuf); //Setting visible property to FALSE so that TAX label wont be visible

			setBoolValueWrapper(PAAS_LINEITEMSCREEN_LBL_4, PROP_BOOL_VISIBLE, 0, pszBuf); //Setting visible property to FALSE so that TAX label wont be visible
		}

		/* SUB TOTAL */
		if(szSubTotAmt != NULL && strlen(szSubTotAmt) > 0)
		{
			debug_sprintf(szDbgMsg, "%s: Sub Total is present, updating the screen", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			setBoolValueWrapper(PAAS_LINEITEMSCREEN_LBL_14, PROP_BOOL_VISIBLE, 1, pszBuf);

			setBoolValueWrapper(PAAS_LINEITEMSCREEN_LBL_13, PROP_BOOL_VISIBLE, 1, pszBuf);

			/*SUB TOTAL */
			formatAmount(szSubTotAmt, szTmpAmt, AMT_POSITIVE);
			if(szSubTotAmt[0] == '-')
			{
				sprintf(szFmtAmt, "-$%s", szTmpAmt);
			}
			else
			{
				sprintf(szFmtAmt, " $%s", szTmpAmt);
			}
			setStringValueWrapper(PAAS_LINEITEMSCREEN_LBL_14, PROP_STR_CAPTION, szFmtAmt, pszBuf);

			/*Set the sub total amount label*/
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_24);
			setStringValueWrapper(PAAS_LINEITEMSCREEN_LBL_13, PROP_STR_CAPTION,
											(char *)stBtnLbl.szLblOne, pszBuf);
		}
		else
		{
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_LBL_14, PROP_BOOL_VISIBLE, 0, pszBuf);

			setBoolValueWrapper(PAAS_LINEITEMSCREEN_LBL_13, PROP_BOOL_VISIBLE, 0, pszBuf);
		}

		/*TOTAL */
		formatAmount(szTotAmt, szTmpAmt, AMT_POSITIVE);
		if(szTotAmt[0] == '-')
		{
			sprintf(szFmtAmt, "-$%s", szTmpAmt);
		}
		else
		{
			sprintf(szFmtAmt, " $%s", szTmpAmt);
		}
		setStringValueWrapper(PAAS_LINEITEMSCREEN_LBL_5, PROP_STR_CAPTION,
													szFmtAmt, pszBuf);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: initLineItemScreen
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int initUpdateForLineItemScreen(int iAllowLITran, UIREQ_PTYPE pstUIReq)
{
	int		rv				= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * Praveen_P1: 28 March 2016
	 * Passing new value 2 at some cases where whichform is not changed but XPI form is
	 * being shown, at those places we dont change whichform so need to init form
	 * of line item screen explicitly
	 */
	if( (iAllowLITran == 2) ||
			((!iAllowLITran) && (whichForm != LI_DISP_FRM) && (whichForm != LI_DISP_OPT_FRM) && (whichForm != LI_FULL_DISP_FRM)) )
	{
#if 0
		debug_sprintf(szDbgMsg, "%s: Cant update this screen [%s] (not LI)",
											__FUNCTION__, frmName[whichForm]);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
#endif
		/*
		 * We are supporting adding/removing/overriding the line items
		 * from anywhere within the session
		 */
		debug_sprintf(szDbgMsg, "%s: Current Form is not Line Item form, showing Line Item screen", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Showing the line item screen */
		showLineItemScreen();
		/*
		 * Praveen_p1: 28 March 2016
		 * Since we are waiting for the response in showlineItemscreen, we dont require below wait anymore
		 */
#if 0
		/*
		 * Praveen_P1: In case if we are not on the line item screen, then we
		 * are showing lineitem screen again..in that we need to wait for processing
		 * of that command to show the line item before doing the next action
		 * since we are not waiting for the response of the showlineitem command
		 * ..thats why putting this wait.
		 * TO_DO: Please do this in the better way..please see if this svcwait can
		 * be removed
		 */


		svcWait(800); //This is not correct way of doing it..please revisit
#endif
	}

	/* Initialize the batch message */
	initUIReqMsg(pstUIReq, BATCH_REQ);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: bufferFailedLICmds
 *
 * Description	: This function creates the XBATCH requests for the failed commands
 * and buffers them in the global LIBuffer.
 *
 * Input Params	:pszCurPtr - This is the XBATCH Request, pXBatchRespInfo - This indicates the failed
 * commands within the XBATCH request.
 *
 * Output Params:SUCCESS , Device Busy if there is no buffer left
 * ============================================================================
 */
static int bufferFailedLICmds(char *pszCurPtr,XBATCH_PTYPE	pXBatchRespInfo,LI_BUFFER_NODE_PTYPE pTmpLIBuf)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	int				iCmds 				= 0;
	int				iTemp				= 1;
	//int	    		iIndex				= 0;
	char			szAppLogData[256]	= "";
	char			szCmdBuf[4096];
	char			sztempMsg[20];
	char			*pszNextPtr			= NULL;
	char			*pszTmpPtr			= NULL;
	LI_BUFFER_NODE_PTYPE pNewBufNode = NULL;
#ifdef DEBUG
	char	szDbgMsg[4096]	= ""; //Declaring a buffer message big enough to hold the XBATCH request, because we are printing it below
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);


	//acquireMutexLock(&gptLIBufferMsgMutex, "LI Buffer Message");

	while (1)
	{
		/*
		 * Storing that command to local buffer to send it later
		 */

		if(strlen(pszCurPtr) < 4090)
		{
			debug_sprintf(szDbgMsg, "%s: XBATCH Request with failed commands %s",__FUNCTION__,pszCurPtr);
			APP_TRACE(szDbgMsg);
		}
		/* Example XBATCH request: <STX>XBATCH<FS>XRLI<FS>2<FS>3<RS>XSPV<FS>29<FS>CAPTION<FS>STRING<FS> $     1.10<RS>
		 * XSPV<FS>22<FS>VISIBLE<FS>BOOL<FS>0<RS>XSPV<FS>21<FS>VISIBLE<FS>BOOL<FS>0<RS>XSPV
		 * <FS>30<FS>CAPTION<FS>STRING<FS> $     1.01<RS>XSFM<FS>1<ETX>
		 */

		pszCurPtr = strchr(pszCurPtr,FS); // Ignore the first XBATCH and then go to the First <FS> after the XBATCH. AFter this <FS> our command will start
		//At this point curPtr will be pointing just before the Command

		memset(szCmdBuf,0x00,sizeof(szCmdBuf));
		memset(sztempMsg,0x00,sizeof(sztempMsg));
		//The buffered Command needs to start with XBATCH<FS>
		sprintf(sztempMsg, "%cXBATCH%c", STX, FS);
		strcat(szCmdBuf,sztempMsg);
		pszTmpPtr = pszCurPtr;

		for(iCmds = 0; iCmds < MAX_FAILED_CMDS; iCmds++)
		{
			if(pXBatchRespInfo->lXbatchResp[iCmds] == 0)
			{
				//Once we see that there are no more failed commands , we can break.
				break;
			}
			else
			{

				//Move to the point such that TmpPtr will point to the <FS> before the first command. TmpPtr is used to parse thie xbatch request and find the required command
				//Then point pszNextPtr to the next <RS>. So everything in between these two pointers will be the failed command which we need to buffer again.
				while(1)
				{
					if((iTemp == (pXBatchRespInfo->lXbatchResp[iCmds])) || (pszTmpPtr == NULL))
					{
						break;
					}
					pszTmpPtr = pszTmpPtr + 1;
					pszTmpPtr = strchr(pszTmpPtr,RS);
					iTemp++;
				}

				if(pszTmpPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Failed Command not found in XBATCH Req , Should not come here !!",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
				//CurPtr will point to the starting of the failed command
				pszCurPtr = pszTmpPtr + 1;
				pszNextPtr = strchr(pszCurPtr,RS);
				pszNextPtr = pszNextPtr + 1; // Include <RS> in the Command Buffer
				strncat(szCmdBuf,pszCurPtr,pszNextPtr-pszCurPtr);

			}
		}

		//Every LI Command will go with XSFM so that Form is displayed.
		memset(sztempMsg,0x00,sizeof(sztempMsg));
		sprintf(sztempMsg, "XSFM%c%d%c", FS, 1, RS);
		strcat(szCmdBuf,sztempMsg);

		if(strlen(szCmdBuf) < 4090)
		{
			debug_sprintf(szDbgMsg, "%s: Command Prepared for the failed commands is:%s",__FUNCTION__,szCmdBuf);
			APP_TRACE(szDbgMsg);
		}

		if(pTmpLIBuf)
		{
			/* This indicates that we failed while updating the buffered Line Items , so we will rebuffer this */
			free(pTmpLIBuf->bufferMsg);
			pTmpLIBuf->bufferMsg = (char *)malloc(strlen(szCmdBuf) + 10/* extra buffer*/);
			if(pTmpLIBuf->bufferMsg == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc failed!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = ERR_DEVICE_APP;
				break;
			}
			memset(pTmpLIBuf->bufferMsg, 0x00, (strlen(szCmdBuf) + 10));
			strncpy(pTmpLIBuf->bufferMsg, szCmdBuf, strlen(szCmdBuf));

		}
		else
		{
			pNewBufNode = malloc(sizeof(LI_BUFFER_NODE_STYPE));
			if(pNewBufNode == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc failed!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = ERR_DEVICE_APP;
				break;
			}

			pNewBufNode->nxtNode = NULL;
			pNewBufNode->bufferMsg = NULL;

			pNewBufNode->bufferMsg = (char *)malloc(strlen(szCmdBuf) + 10/* extra buffer*/);
			if(pNewBufNode->bufferMsg == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc failed!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = ERR_DEVICE_APP;
				break;
			}
			memset(pNewBufNode->bufferMsg, 0x00, (strlen(szCmdBuf) + 10));
			strncpy(pNewBufNode->bufferMsg, szCmdBuf, strlen(szCmdBuf));


			//First Node in the queue
			if(liBufferHead.tailNode == NULL)
			{
				liBufferHead.headNode = pNewBufNode;
				liBufferHead.tailNode = pNewBufNode;
				liBufferHead.bufferCnt = 1;
				debug_sprintf(szDbgMsg, "%s: First Node - Adding to the start of list", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				//Not first node in the list, we need to add it at end of tail node
				liBufferHead.tailNode->nxtNode = pNewBufNode;
				liBufferHead.tailNode = pNewBufNode;
				liBufferHead.bufferCnt++;
				debug_sprintf(szDbgMsg, "%s: Queue already present, adding node to end of list", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Buffering the Failed Line Items");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

//		for(iIndex = 0; iIndex < MAX_LI_BUFFER; iIndex++)
//		{
//			if(pszLIMsg[iIndex] == NULL)
//			{
//				break;
//			}
//		}
//		// CID-67475: 22-Jan-16: MukeshS3: We'll buffer LI request for maximum MAX_LI_BUFFER number of requests, after that SCA will return device busy response to POS
//		if(iIndex < MAX_LI_BUFFER)
//		{
//			pszLIMsg[iIndex] = (char *)malloc(strlen(szCmdBuf) + 10/* extra buffer*/);
//			if(pszLIMsg[iIndex] == NULL)
//			{
//				debug_sprintf(szDbgMsg, "%s: Malloc failed!!!", __FUNCTION__);
//				APP_TRACE(szDbgMsg);
//				rv = ERR_DEVICE_APP;
//				break;
//			}
//
//			memset(pszLIMsg[iIndex], 0x00, (strlen(szCmdBuf) + 10));
//			strncpy(pszLIMsg[iIndex], szCmdBuf, strlen(szCmdBuf));
//			memset(szAppLogData, 0x00, sizeof(szAppLogData));
//			if(iAppLogEnabled == 1)
//			{
//				strcpy(szAppLogData, "Buffering the Failed Line Items");
//				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
//			}
//			iIndex++;
//			if(iIndex == MAX_LI_BUFFER)
//			{
//				//Indicates that LI buffer is now full and for next command we will return DEV busy unless we flush buffered LI.
//				setLIBufferStatus(PAAS_TRUE);
//			}
//		}
//		else
//		{
//			//if we dont have buffers available return Device Busy
//			rv = ERR_DEVICE_BUSY;
//		}
		break;
	}

	//releaseMutexLock(&gptLIBufferMsgMutex, "LI Buffer Message");

	return rv;
}
/*
 * ============================================================================
 * Function Name: updateLineItemScreen
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int updateLineItemScreen(int iAllowLITran, char* pszBuf)
{
	int						rv					= SUCCESS;
	//int	    		iIndex				= 0;
	int						iAppLogEnabled		= isAppLogEnabled();
	char					szAppLogData[256]	= "";
	char					szXBatchBkup[4096];
	PAAS_BOOL				bWait				= PAAS_TRUE;
	PAAS_BOOL				bBufferCmds			= PAAS_FALSE;
	XBATCH_PTYPE			pXBatchRespInfo		= NULL;
	LI_BUFFER_NODE_PTYPE 	pNewBufNode = NULL;
	LI_BUFFER_NODE_PTYPE 	pTmpLIBuf = NULL;
	XBATCH_STYPE		    xBatchRespDet;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check if the form is a line display form */
		if((!iAllowLITran) && (whichForm != LI_DISP_FRM) && (whichForm != LI_DISP_OPT_FRM) && (whichForm != LI_FULL_DISP_FRM))
		{
			debug_sprintf(szDbgMsg, "%s: Cant update this screen [%s] (not LI)",
											__FUNCTION__, frmName[whichForm]);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
			break;
		}

		/* Show the form */
		showFormWrapper(PM_NORMAL, pszBuf);

		acquireMutexLock(&gptLITailBufferMutex, "LI Tail Buffer");

		iAllowLITran = getAllowLITran();

		/*
		 * Praveen_P1: This iAllowLITran is set when LI command comes, but
		 * UI is busy with some some other command screen
		 * We are buffering LIs in that case, thats why
		 * we are storing the line item request here
		 */

		if(iAllowLITran == 0)
		{
			bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here
			/* Send the batch message to UI agent */

			memset(szXBatchBkup,0x00,sizeof(szXBatchBkup));
			strcpy(szXBatchBkup, pszBuf); //Keep a copy of the XBATCH request , so that we can reform the request , incase a particular LI command fails.

			rv = sendUIReqMsg(pszBuf);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_DEVICE_APP;
			}
			else
			{
				/* Wait for the UI Agent resp */
				while(bWait == PAAS_TRUE)
				{
					CHECK_POS_INITIATED_STATE;
					if(bBLDataRcvd == PAAS_TRUE)
					{
						acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

						if(stBLData.uiRespType == UI_LISTBOX_RESP)
						{
							bBLDataRcvd = PAAS_FALSE;
							bWait = PAAS_FALSE;
							/* Daivik:11/4/2016 - In parseUIAgentResp , if XALI/XRLI/XULI has failed, then we will be setting the status to the below one.
							 * Based on this status , we will have to buffer the failed commands and retry once the UI tran completes.
							 * Here we have made an assumption that that these commands failed due to UI transaction (Card inserted etc) where
							 * XPI would try to show a form.
							 */
							if(stBLData.iStatus == ERR_XBATCH_FAILED)
							{
								debug_sprintf(szDbgMsg, "%s: Some List box response has failed, Need to buffer failed commands",__FUNCTION__);
								APP_TRACE(szDbgMsg);
								memset(&xBatchRespDet,0x00,sizeof(XBATCH_STYPE));
								memcpy(&xBatchRespDet, &(stBLData.stRespDtls.stXBatchInfo), sizeof(XBATCH_STYPE));
								pXBatchRespInfo = &xBatchRespDet;
								bBufferCmds = PAAS_TRUE;
							}
						}

						releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
					}
					svcWait(10);
				}

				if(bBufferCmds)
				{
					/* Until we complete buffering the failed Line Items, the  giOnlineLITranRetry should not be read and used.
					 */
					rv = bufferFailedLICmds(szXBatchBkup,pXBatchRespInfo,pTmpLIBuf);

					if(rv == SUCCESS)
					{
						setRetryLITran(1);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Buffering the Line Items has failed",__FUNCTION__);
						APP_TRACE(szDbgMsg);
					}

				}

				memset(szAppLogData, 0x00, sizeof(szAppLogData));
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Updating the Line Item Screen");
					addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Since AllowLITran is set, not sending the LI request", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/*
			 * Praveen_P1: Putting under lock while updating
			 * the message buffer since BLogic thread clears this buffer
			 */
			//acquireMutexLock(&gptLIBufferMsgMutex, "LI Buffer Message");

			/*
			 * Storing that command to local buffer to send it later
			 */

			pNewBufNode = malloc(sizeof(LI_BUFFER_NODE_STYPE));
			if(pNewBufNode == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc failed!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = ERR_DEVICE_APP;
				break;
			}
			pNewBufNode->nxtNode = NULL;
			pNewBufNode->bufferMsg = NULL;

			pNewBufNode->bufferMsg = (char *)malloc(strlen(pszBuf) + 10/* extra buffer*/);
			if(pNewBufNode->bufferMsg == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc failed!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = ERR_DEVICE_APP;
				break;
			}
			memset(pNewBufNode->bufferMsg, 0x00, (strlen(pszBuf) + 10));
			strncpy(pNewBufNode->bufferMsg, pszBuf, strlen(pszBuf));

			//First Node in the queue
			if(liBufferHead.tailNode == NULL)
			{
				liBufferHead.headNode = pNewBufNode;
				liBufferHead.tailNode = pNewBufNode;
				liBufferHead.bufferCnt = 1;
				debug_sprintf(szDbgMsg, "%s: First Node - Adding to the start of list", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				//Not first node in the list, we need to add it at end of tail node
				liBufferHead.tailNode->nxtNode = pNewBufNode;
				liBufferHead.tailNode = pNewBufNode;
				liBufferHead.bufferCnt++;
				debug_sprintf(szDbgMsg, "%s: Queue already present, adding node to end of list:node count:%d", __FUNCTION__,liBufferHead.bufferCnt);
				APP_TRACE(szDbgMsg);
			}
//			for(iIndex = 0; iIndex < MAX_LI_BUFFER; iIndex++)
//			{
//				if(pszLIMsg[iIndex] == NULL)
//				{
//					break;
//				}
//			}
//			// CID-67475: 22-Jan-16: MukeshS3: We'll buffer LI request for maximum MAX_LI_BUFFER number of requests, after that SCA will return device busy response to POS
//			if(iIndex < MAX_LI_BUFFER)
//			{
//				pszLIMsg[iIndex] = (char *)malloc(strlen(stUIReq.pszBuf) + 10/* extra buffer*/);
//				if(pszLIMsg[iIndex] == NULL)
//				{
//					debug_sprintf(szDbgMsg, "%s: Malloc failed!!!", __FUNCTION__);
//					APP_TRACE(szDbgMsg);
//					rv = ERR_DEVICE_APP;
//					break;
//				}
//
//				memset(pszLIMsg[iIndex], 0x00, (strlen(stUIReq.pszBuf) + 10));
//				strncpy(pszLIMsg[iIndex], stUIReq.pszBuf, strlen(stUIReq.pszBuf));
//
//				iIndex++;
//				if(iIndex == MAX_LI_BUFFER)
//				{
//					//Indicates that LI buffer is now full and for next command we will return DEV busy unless we flush buffered LI.
//					setLIBufferStatus(PAAS_TRUE);
//				}
//			}
//			else
//			{
//				rv = ERR_DEVICE_BUSY;
//			}

			//releaseMutexLock(&gptLIBufferMsgMutex, "LI Buffer Message");
		}

		releaseMutexLock(&gptLITailBufferMutex, "LI Tail Buffer");
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: clearUIListBox
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void clearUIListBox(char* pszBuf)
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	clearListBoxfromCurrentForm(PAAS_LINEITEMSCREEN_LST_1, pszBuf);

	return;
}

/*
 * ============================================================================
 * Function Name: showUpdatedLIScreen
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int showUpdatedLIScreen()
{
	int				rv				= SUCCESS;
	//int				iIndex			= 0;
	int				iAppLogEnabled					 = isAppLogEnabled();
	int				iUpdateLINeeded		= PAAS_FALSE;
	static int		iUpdateInProgress	= PAAS_FALSE;
	PAAS_BOOL		bWait			= PAAS_TRUE;
	PAAS_BOOL		bUpdatedLI		= PAAS_FALSE;
	PAAS_BOOL		bFirstTime		= PAAS_FALSE;
	PAAS_BOOL		bReleaseMutex	= PAAS_FALSE;
	PAAS_BOOL		bBufferCmds		= PAAS_FALSE;
	char			szAppLogData[256]				 = "";
	char			szXBatchBkup[4096];
	LI_BUFFER_NODE_PTYPE pTmpLIBuf = NULL;
	XBATCH_STYPE		 xBatchRespDet;
	XBATCH_PTYPE		pXBatchRespInfo		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptUpdateLIScreen, "Updating the Buffered Lineitems");

	if(iUpdateInProgress)
	{
		iUpdateLINeeded = PAAS_FALSE;
	}
	else
	{
		iUpdateLINeeded 	= PAAS_TRUE;
		iUpdateInProgress 	= PAAS_TRUE;

		debug_sprintf(szDbgMsg, "%s: Setting iUpdateInProgress to %d ", __FUNCTION__, iUpdateInProgress);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Setting iUpdateLINeeded to %d ", __FUNCTION__, iUpdateLINeeded);
	APP_TRACE(szDbgMsg);

	releaseMutexLock(&gptUpdateLIScreen, "Finished Updation of Buffered Line Items");

	while(iUpdateLINeeded)
	{
		//acquireMutexLock(&gptLIBufferMsgMutex, "LI Buffer Message");

		if(getAppState() != IN_LINEITEM_QRCODE)
		{
			setAppState(IN_LINEITEM);
		}
		setDetailedAppState(IN_STATE_LINEITEM);

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Sending Any buffered Line Item Commands");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		/* If there is no node in the list , then we can set allowLITran to false.
		 * But we can decide this only after obtaining the mutex, since there is a chance that a new node was just added to the list. In this case bufferCnt > 0
		 * If no new  buffer was added , we continue as before and exit out from the function. If there was a new node added to queue, then we can send it to the UI agent.
		 */
		if(liBufferHead.headNode == NULL)
		{
			acquireMutexLock(&gptLITailBufferMutex,"LI Tail Buffer");

			if(liBufferHead.bufferCnt == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Setting giAllowLITran to 0 since Queue is empty",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				setAllowLITran(PAAS_FALSE);

				/* KranthiK1:
				 * Setting the retry allow LI Tran to False after sending all the buffered line item commands
				 */
				setRetryLITran(PAAS_FALSE);
			}

			releaseMutexLock(&gptLITailBufferMutex,"LI Tail Buffer");
		}
		//for(iIndex = 0; iIndex < MAX_LI_BUFFER; iIndex++)
		while(liBufferHead.headNode != NULL)
		{
			bBufferCmds = PAAS_FALSE;
			pTmpLIBuf = liBufferHead.headNode;
			if(pTmpLIBuf == liBufferHead.tailNode)
			{
				bReleaseMutex = PAAS_TRUE;
				acquireMutexLock(&gptLITailBufferMutex, "LI Tail Buffer");
			}
			if(pTmpLIBuf->bufferMsg)
			{
				bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here
				bWait		= PAAS_TRUE; //DaivikP1 :24/03/2016 This is required to ensure we wait for the XBATCH response before sending the next line item command.
				/* Send the batch message to UI agent */
				memset(szXBatchBkup, 0x00, sizeof(szXBatchBkup));
				strcpy(szXBatchBkup, pTmpLIBuf->bufferMsg);
				rv = sendUIReqMsg(pTmpLIBuf->bufferMsg);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																		__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = ERR_DEVICE_APP;
				}
				else
				{
					/* Wait for the UI Agent resp */
					while(bWait == PAAS_TRUE)
					{
						CHECK_POS_INITIATED_STATE;
						if(bBLDataRcvd == PAAS_TRUE)
						{
							acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

							//DaivikP1 :24/3/2016 We were not waiting for GENERL response. But when we want to set the totals /subtotal value, we send XSPV , which is categorized as GENERAL Resp
							if((stBLData.uiRespType == UI_LISTBOX_RESP) || (stBLData.uiRespType == UI_GENRL_RESP))
							{
								bBLDataRcvd = PAAS_FALSE;
								bWait = PAAS_FALSE;

								bUpdatedLI = PAAS_TRUE;//Setting that we updated line item screen

								if(stBLData.iStatus == ERR_XBATCH_FAILED)
								{
									memset(&xBatchRespDet,0x00,sizeof(XBATCH_STYPE));
									memcpy(&xBatchRespDet, &(stBLData.stRespDtls.stXBatchInfo), sizeof(XBATCH_STYPE));
									pXBatchRespInfo = &xBatchRespDet;
									bBufferCmds = PAAS_TRUE;
									debug_sprintf(szDbgMsg, "%s: XBATCH failed while sending the buffered Line Items",__FUNCTION__);
									APP_TRACE(szDbgMsg);
								}

							}

							releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

							if(bBufferCmds)
							{
								rv = bufferFailedLICmds(szXBatchBkup,pXBatchRespInfo,pTmpLIBuf);
								if(rv != SUCCESS)
								{
									debug_sprintf(szDbgMsg, "%s: Buffering the Line Items has failed",__FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
							}
						}
						if(bWait == PAAS_FALSE)
						{
							break;
						}
						svcWait(15);
					}
				}

				if(bBufferCmds && (rv == SUCCESS))
				{

					if(bReleaseMutex == PAAS_TRUE)
					{
						releaseMutexLock(&gptLITailBufferMutex, "LI Tail Buffer");
					}
					//bBufferCmds = PAAS_FALSE;
					pTmpLIBuf = NULL;
					//continue;
					break;
				}
				else
				{
					free(pTmpLIBuf->bufferMsg);
					pTmpLIBuf->bufferMsg = NULL;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Current LI node has a NULL buffer, should not come here !!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				//break;
			}
			if(bReleaseMutex == PAAS_TRUE)
			{
				if(pTmpLIBuf == liBufferHead.tailNode)
				{
					debug_sprintf(szDbgMsg, "%s: Setting giAllowLITran to 0 since this is the last node",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					setAllowLITran(PAAS_FALSE);
				}
				releaseMutexLock(&gptLITailBufferMutex, "LI Tail Buffer");
			}
			liBufferHead.headNode = liBufferHead.headNode->nxtNode;
			liBufferHead.bufferCnt--;
			free(pTmpLIBuf);
			pTmpLIBuf = NULL;
		}

		if(bBufferCmds == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Returning Since XBATCH of Buffered LI failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS;
			break;
		}
		// At this point we should have flushed the entire list and we will not have anything in the list. Can set the tail pointer to NULL
		if(liBufferHead.bufferCnt != 0 )
		{
			debug_sprintf(szDbgMsg, "%s: Should not come here since there should be no buffer in the list at this point!!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		liBufferHead.bufferCnt = 0;
		liBufferHead.headNode = NULL;
		liBufferHead.tailNode = NULL;

		//LI Buffer is now free for Buffering
		//setLIBufferStatus(PAAS_FALSE);
		//releaseMutexLock(&gptLIBufferMsgMutex, "LI Buffer Message");

		if(bUpdatedLI == PAAS_TRUE || bFirstTime == PAAS_TRUE)
		{
			if(bFirstTime == PAAS_TRUE)
			{
				debug_sprintf(szDbgMsg, "%s: Waited for once, not waiting anymore!!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No LI in buffer to update, waiting for sometime to check once again", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			bFirstTime = PAAS_TRUE;

			svcWait(50);
		}
	}

	if(iUpdateLINeeded)
	{
		acquireMutexLock(&gptUpdateLIScreen, "Updating the Buffered Lineitems");

		iUpdateInProgress = PAAS_FALSE;

		debug_sprintf(szDbgMsg, "%s: Setting iUpdateInProgress to %d ", __FUNCTION__, iUpdateInProgress);
		APP_TRACE(szDbgMsg);

		releaseMutexLock(&gptUpdateLIScreen, "Finished Updation of Buffered Line Items");
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showLineItemScreen()
 *
 * Description	: This API would be used to start the line item display on the
 * 					device screen.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showLineItemScreen()
{
	int				rv								 = SUCCESS;
	int				locForm							 = 0;
	int				iDisableFormFlag				 = 0;
	int				consOptionLst[MAX_CONSUMER_OPTS][2];
	int				iCnt							 = 0;
	int				iStartBtn						 = 0;
	int				iStartBtnLbl					 = 0;
	int				iStartBtnLblNum				 	 = 0;
	int				iAppLogEnabled					 = isAppLogEnabled();
	int				iEmvCrdPresence					 = 0;
	char			szBtnLbl[32]					 = "";
//	char			szFileName[32]					 = "";	// CID-67469: 22-Jan-16: MukeshS3: Increasing file name size to MAX_FILE_NAME_LEN
	char			szFileName[MAX_FILE_NAME_LEN]	 = "";	// because, at some places, it is being memset with size MAX_FILE_NAME_LEN i.e. 50
	char			szTaxAmount[11]					 = "";
	char			szTotalAmount[11]				 = "";
	char			szSubTotAmount[11]				 = "";
	char			szTmp[100]						 = "";
//	char			szImgName[32]					 = "";
	char			szAppLogData[256]				 = "";
	char			szPymtCode[15]					 = "";
	char			szMerchIndex[10+1]				 = "";
	char			szNFCVASMODE[4+1]				 = "";
	PAAS_BOOL		bWait							 = PAAS_TRUE;
	PAAS_BOOL		bEmvEnabled						 = PAAS_FALSE;
	PAAS_BOOL		bCaptureCardDtls				 = PAAS_FALSE;
	PAAS_BOOL		bCardReadEnabled				 = PAAS_FALSE;
	PAAS_BOOL		bNFCVASMode						 = PAAS_FALSE;
	PAAS_BOOL		bPreSwipe						 = PAAS_FALSE;
	PAAS_BOOL		bPaymentOnlyMode				= PAAS_TRUE;
	BTNLBL_STYPE	stBtnLbl;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(IN_LINEITEM);
	setDetailedAppState(IN_STATE_LINEITEM);

	bCardReadEnabled = getCardReadEnabled();

	//The reason we are removing this is because , we have done the disablecard readers in the doConsumerOptCapture
	//when we recieved the cancel command. Doing it here is not correct.
#if 0
	//03-Feb-16 : AkshayaM1: PTMX-809 :: The issue is when we user Press cancel after getting the VAS data.
	//Card Reader are Enabled with Non Contactless Payment mode in Phone.
	//So , We need to disable the Card Readers and Enabled with VAS only Mode in C30 or S20.
	if(bCardReadEnabled == PAAS_TRUE && isWalletEnabled() && isVASDataCaptured() ==  PAAS_FALSE)
	{
		disableCardReaders();
		bCardReadEnabled = getCardReadEnabled();
	}
#endif
	/*T_POLISETTYG1:31-12-2015: According to FRD 3.72 if full_lineitem_display & lineitem_display parameters are set, then we are showing new form PAAS_LINEITEMSCREEN2.
	 * In full line item screen line items, label for asking swipe/insert card , and banner will be present, if full_lineitem_display is enabled we are Showing
	 * full form by neglecting the consumer options even they are enabled. For Full line item display we are not displaying  paypal button & wallet image/text.
	 */
	if(isFullLineItemDisplayEnabled() == PAAS_TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: Full line item screen enabled", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		locForm = LI_FULL_DISP_FRM;

		/* Initialize the batch message */
		initUIReqMsg(&stUIReq, BATCH_REQ);
		initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);

		/* Restoring the List BOX state*/
		setListBoxRestoreState(PAAS_LINEITEMSCREEN_LST_1, stUIReq.pszBuf);

		whichForm     = locForm;
	}

	else if(isConsumerOptsEnabled() == PAAS_FALSE)
	{
		debug_sprintf(szDbgMsg, "%s: Consumer options are not enabled", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		locForm = LI_DISP_FRM;

		/* Initialize the batch message */
		initUIReqMsg(&stUIReq, BATCH_REQ);
		initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);
		/*
		 * Praveen_P1: 21 July 2016
		 * To Fix PTMX-1478/1480/1333
		 * One Line Item screen changed the Image control to Animation control
		 * to support rotation of advt images on the left side of the line item
		 * screen
		 */
#if 0
		/* Set the image for advertisement */
		if(SUCCESS == getNxtImageFileName(szImgName, locForm))
		{
			/* Set the label as visible */
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_IMG_1, PROP_BOOL_VISIBLE, 1,
									stUIReq.pszBuf);

			setStringValueWrapper(PAAS_LINEITEMSCREEN_IMG_1,
					PROP_STR_IMAGE_FILE_NAME, szImgName, stUIReq.pszBuf);
		}
#endif
		/* Set the label as visible */
		setBoolValueWrapper(PAAS_LINEITEMSCREEN_ANI_1, PROP_BOOL_VISIBLE, 1,
											stUIReq.pszBuf);

		setBoolValueWrapper(PAAS_LINEITEMSCREEN_ANI_1, PROP_BOOL_ANIMATE_RUN, 1,
													stUIReq.pszBuf);

		/* Daivik:17/8/2016 - Opening this up , to have ability to control interval via imaglist.dat file. PTMX- 1549 */
		setShortValueWrapper(PAAS_LINEITEMSCREEN_ANI_1, PROP_SHORT_ANIM_DELAY_INTERVAL, (getImageUpdateIntvl(locForm) * 1000), stUIReq.pszBuf);

		/* Restoring the List BOX state*/
		setListBoxRestoreState(PAAS_LINEITEMSCREEN_LST_1, stUIReq.pszBuf);

		//giImgCtrl = PAAS_LINEITEMSCREEN_IMG_1;
		//giImgUpdIntvl = getImageUpdateIntvl(locForm);
		whichForm     = locForm;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Consumer options are enabled", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		locForm = LI_DISP_OPT_FRM;

		/* Initialize the batch message */
		initUIReqMsg(&stUIReq, BATCH_REQ);
		initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);

		/* Get the enabled consumer options */
		getConsOptions(consOptionLst);

		iStartBtn    	 = PAAS_LINEITEMSCREEN1_BTN_1;
		iStartBtnLbl 	 = PAAS_LINEITEMSCREEN1_LBL_7;
		iStartBtnLblNum = BTN_LBLS_16;

		for(iCnt = 0; iCnt < MAX_CONSUMER_OPTS; iCnt++)
		{
			switch(consOptionLst[iCnt][0])
			{
			case -1:
				debug_sprintf(szDbgMsg, "%s: No Consumer option is set for %d button position", __FUNCTION__, iCnt+1);
				APP_TRACE(szDbgMsg);
				break;

			case GIFTRECEIPT_OPT:
				debug_sprintf(szDbgMsg, "%s: Gift Receipt Consumer option is set for %d button position", __FUNCTION__, iCnt+1);
				APP_TRACE(szDbgMsg);

				break;

			case EMAILRECEIPT_OPT:
				debug_sprintf(szDbgMsg, "%s: Email Receipt Consumer option is set for %d button position", __FUNCTION__, iCnt+1);
				APP_TRACE(szDbgMsg);

				break;

			case EMAILOFFER_OPT:
				debug_sprintf(szDbgMsg, "%s: Email Offer Consumer option is set for %d button position", __FUNCTION__, iCnt+1);
				APP_TRACE(szDbgMsg);
				break;

			case PRIVCARD_OPT:
				debug_sprintf(szDbgMsg, "%s: Private Card Consumer option is set for %d button position", __FUNCTION__, iCnt+1);
				APP_TRACE(szDbgMsg);
				break;

			default:
				break;
			}
			if(consOptionLst[iCnt][0] != -1) //If the option is set for this position then show
			{
				/* Set the corresponding label */

				debug_sprintf(szDbgMsg, "%s: iStartBtnLblNum = [%d]", __FUNCTION__, iStartBtnLblNum + iCnt);
				APP_TRACE(szDbgMsg);

				fetchBtnLabel(&stBtnLbl, iStartBtnLblNum + iCnt);

				debug_sprintf(szDbgMsg, "%s: szLblOne = [%s], szLblTwo = [%s]", __FUNCTION__, (char *)stBtnLbl.szLblOne, (char *)stBtnLbl.szLblTwo);
				APP_TRACE(szDbgMsg);

				memset(szBtnLbl, 0x00, sizeof(szBtnLbl));
				strcpy(szBtnLbl, (char *)stBtnLbl.szLblOne);

				debug_sprintf(szDbgMsg, "%s: szBtnLbl = [%s]", __FUNCTION__, szBtnLbl);
				APP_TRACE(szDbgMsg);

				stripSpaces(szBtnLbl, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

				setStringValueWrapper(iCnt + iStartBtnLbl, PROP_STR_CAPTION,
								szBtnLbl, stUIReq.pszBuf);

				/* Set the label as visible */
				setBoolValueWrapper(iCnt + iStartBtnLbl, PROP_BOOL_VISIBLE, 1,
										stUIReq.pszBuf);

				/* Set the button as visible */
				setBoolValueWrapper(iCnt + iStartBtn, PROP_BOOL_VISIBLE, 1,
										stUIReq.pszBuf);
				if(consOptionLst[iCnt][1] == 1)
				{
					debug_sprintf(szDbgMsg, "%s: This option is selected", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					memset(szFileName, 0x00, sizeof(szFileName));
					sprintf(szFileName, "%s", "btn_active_check.png");
					getFileNamewithPlatformPrefix(szFileName);

					/* Set the up button image */
					setStringValueWrapper(iCnt + iStartBtn, PROP_STR_UP_IMAGE_FILE_NAME,
															szFileName, stUIReq.pszBuf);

					setBoolValueWrapper(iCnt + iStartBtn, PROP_BOOL_ENABLE, 0, stUIReq.pszBuf);
				}
			}
		}
		/* Restoring the List BOX state*/
		setListBoxRestoreState(PAAS_LINEITEMSCREEN_LST_1, stUIReq.pszBuf);

		whichForm = locForm;
	}

	if(isPaypalTenderEnabled() && isFullLineItemDisplayEnabled() == PAAS_FALSE)
	{
		setBoolValueWrapper(PAAS_LINEITEMSCREEN_PAYPAL_BTN, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);

		rv = getPaypalPymtCodeDtlsInSession(szPymtCode);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Paypal Payment Code Details in Session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return rv;
		}

		if((strlen(szPymtCode) > 0) || (isPreSwipeDone() == TRUE))
		{
			memset(szFileName, 0x00, sizeof(szFileName));
			sprintf(szFileName, "%s", "Paypal-btn-pressed.png");
			getFileNamewithPlatformPrefix(szFileName);

			/* Set the down button image */
			setStringValueWrapper(PAAS_LINEITEMSCREEN_PAYPAL_BTN, PROP_STR_DOWN_IMAGE_FILE_NAME,
													szFileName, stUIReq.pszBuf);

			setBoolValueWrapper(PAAS_LINEITEMSCREEN_PAYPAL_BTN, PROP_BOOL_ENABLE, 0, stUIReq.pszBuf);
		}
	}

	/* Set the Title */
	fetchScrTitle(szTmp, LINE_ITEMS_TITLE);
	setStringValueWrapper(PAAS_LINEITEMSCREEN_LBL_1, PROP_STR_CAPTION, szTmp,
					stUIReq.pszBuf);

	/* Set the TAX label */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_03);
	setStringValueWrapper(PAAS_LINEITEMSCREEN_LBL_2, PROP_STR_CAPTION,
							(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);
	/* Set the total amount label */
	setStringValueWrapper(PAAS_LINEITEMSCREEN_LBL_3, PROP_STR_CAPTION,
							(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/*Setting the tax amount and total amount*/
	getLIRunningAmounts(szTaxAmount, szTotalAmount, szSubTotAmount);

	getVASTermModeIfAvaliable(szNFCVASMODE);

	if ( (strlen(szTaxAmount) > 0) || (strlen(szTotalAmount) > 0) || (strlen(szSubTotAmount) > 0))
	{
		updateTaxAndTotalOnLIScreen(szTaxAmount, szTotalAmount, szSubTotAmount, 0, stUIReq.pszBuf);
	}

	if(isSwipeAheadEnabled() == PAAS_TRUE)
	{
		if((isPreSwipeDone() == PAAS_FALSE) && (strlen(szPymtCode) <= 0)) //Pre-Swipe is not done
		{
			debug_sprintf(szDbgMsg, "%s: Swipe Ahead is enabled, adding card read command to the request", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//Get EMV status
			bEmvEnabled = isEmvEnabledInDevice();

			memset(szTmp, 0x00, sizeof(szTmp));
			if(bEmvEnabled == PAAS_TRUE)
			{
				iEmvCrdPresence = EMVCrdPresenceOnPreswipe();
				if(iEmvCrdPresence == 1)
				{
					getDisplayMsg(szTmp, MSG_AFTER_PRE_SWIPE_EMV);
					setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
				}
				else
				{
					getDisplayMsg(szTmp, MSG_PRESWIPE_EMV);
					setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
					bCaptureCardDtls = PAAS_TRUE;
				}
			}
			else
			{
				getDisplayMsg(szTmp, MSG_PRE_SWIPE);
				//Temp: Remove after putting req string in display prompts
				setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
				bCaptureCardDtls = PAAS_TRUE;
			}

			/* Set the label for the Pre-Swipe message on the screen */
			//setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);

			/* Show the form */
			//showFormWrapper(PM_NORMAL, stUIReq.pszBuf); //Sending the XIFM before Q13 to avoid complications

		}
		else //Pre-Swipe is done
		{

			bPreSwipe = PAAS_TRUE;

			/*if only card data is captured then we need to send D41 to get VAS data*/
			/*Akshaya: Here Checking for EMVContactlessEnabled = Y or PAYMENT_ONLY from NFCVAS_MODE, then we are sending the D41 Command*/
			/*06-04-2016 : Checking Whether D41 is Active */
			if(PAAS_TRUE == isWalletEnabled() && isVASDataCaptured() == PAAS_FALSE && (getD41Active() == PAAS_FALSE) && (isContactlessEmvEnabledInDevice() || strcmp(szNFCVASMODE,PAYMENT_ONLY) != SUCCESS))
			{
				memset(szMerchIndex, 0x00, sizeof(szMerchIndex));
				getMerchantIndexIfAvaliable(szMerchIndex);
				sendD41cmdToEMVAgent(PAAS_FALSE, szMerchIndex, NULL);
			}

			memset(szTmp, 0x00, sizeof(szTmp));
			/* Get the display prompt/message from the .ini file */
			if(EMVCrdPresenceOnPreswipe() == 1)
			{
				getDisplayMsg(szTmp, MSG_AFTER_PRE_SWIPE_EMV);
				/* Set the label for the After-Pre-Swipe message on the screen */
				setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
			}
			else
			{
				getDisplayMsg(szTmp, MSG_AFTER_PRE_SWIPE);
				/* Set the label for the After-Pre-Swipe message on the screen */
				setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
			}

			/* Show the form */
			//showFormWrapper(PM_NORMAL, stUIReq.pszBuf);
		}

		//For Pre-Swipe cases

		/*When NFCVAS_MODE is Payment Only then we set a Bool Variable bNFCVASMode to TRUE and other than Payment Only mode received as NFCVAS_MDOE then we set to FALSE.*/
		/*The Bool Variable bPreSwipe is for Pre Swipe done or not cases, in that case the bPaymentOnlyMode is set to FALSE.*/
		/*When No VAS data Captured ans Pre-Swipe is Done. Then also we sent bPaymentOnlyMode is set to FALSE*/
		if(isWalletEnabled() == PAAS_TRUE)
		{
			if(strcmp(szNFCVASMODE, PAYMENT_ONLY) == SUCCESS)
			{
				bNFCVASMode = PAAS_TRUE;
				if(isVASDataCaptured() == PAAS_TRUE && bPreSwipe == PAAS_FALSE)
				{
					debug_sprintf(szDbgMsg, "%s: VAS details Captured and Pre-Swipe not done", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					bPaymentOnlyMode = PAAS_TRUE;
				}
				else if(isContactlessEmvEnabledInDevice())
				{
					debug_sprintf(szDbgMsg, "%s: Contactless EMV Enabled in Device", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					bPaymentOnlyMode = PAAS_TRUE;
				}
				else if(bPreSwipe == PAAS_FALSE)
				{
					debug_sprintf(szDbgMsg, "%s: Pre-Swipe not done", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					bPaymentOnlyMode = PAAS_FALSE;
				}
				else if(isVASDataCaptured() == PAAS_FALSE && bPreSwipe == PAAS_TRUE)
				{
					debug_sprintf(szDbgMsg, "%s: VAS details not Captured and Pre-Swipe done", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					bPaymentOnlyMode = PAAS_FALSE;
				}
			}
			else
			{
				bNFCVASMode = PAAS_FALSE;
			}
		}
		/*Here when Pre-Swipe is not Done . only then the Card Readers will be Enabled for NFCVAS_MODE Sent from the POS for Both Start Session ans Show Line Item Command */
		if(isWalletEnabled() == PAAS_TRUE && (bPreSwipe == PAAS_FALSE) && isContactlessEmvEnabledInDevice() == PAAS_FALSE && strlen(szNFCVASMODE) > 0 )
		{
			if(isVASDataCaptured() == PAAS_FALSE)
			{
				bCaptureCardDtls = PAAS_TRUE;
			}
		}

		/*If the PaymentOnlyMode is True which is set by default will Take care of Displaying Apple Logo and Message when Loyalty Information Received */
		if(bPaymentOnlyMode == PAAS_TRUE)
		{
			if(isWalletEnabled() == PAAS_TRUE && isFullLineItemDisplayEnabled() == PAAS_FALSE)
			{
				if(isVASDataCaptured() == PAAS_FALSE)
				{
					sprintf(szFileName, "%s", "liscreen_wallet_banner.png");
					getFileNamewithPlatformPrefix(szFileName);
					setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_LBL, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
					setStringValueWrapper(PAAS_LINEITEMSCREEN_WALLET_IMG, PROP_STR_IMAGE_FILE_NAME, szFileName, stUIReq.pszBuf);
					setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_IMG, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
				}
				else
				{
					memset(szTmp, 0x00, sizeof(szTmp));
					getDisplayMsg(szTmp, MSG_LOYALTY_CAPTURED);
					setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_IMG, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
					setStringValueWrapper(PAAS_LINEITEMSCREEN_WALLET_LBL, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
					setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_LBL, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
				}
			}
		}

		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf); //Sending the XIFM before Q13 to avoid complications

	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Swipe Ahead is NOT enabled", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		setBoolValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
		setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_IMG, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
		setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_LBL, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);

		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);
	}

	bBLDataRcvd = PAAS_FALSE;//Praveen_P1: Setting to false before sending the request to make sure that we wait for the response which we are sending
	/* Send batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to send UI Request message", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Line Item Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}

		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_LISTBOX_RESP || stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait 		= PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);
		}

		if(bCaptureCardDtls == PAAS_TRUE && (!bCardReadEnabled) && (!isCardDataInBQueueFlag()))
		{
			rv= captureCardDtlsForPreswipe();//Send Commands to XPI for card data capture
			bCaptureCardDtls = PAAS_FALSE;
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to send XPI cmds",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				return ERR_DEVICE_APP;
			}
		}
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showPaypalAmountOKScreen()
 *
 * Description	: This API would be used to ask the users whether they want
 * 					their transaction amount to be authorised.
 *
 * Input Params	: None
 *
 * Output Params: UI_YES_SELECTED/UI_NO_SELECTED
 * ============================================================================
 */
int showPaypalAmountOKScreen(char *pszTranAmt)
{
	int				rv					= PAAS_FALSE;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256] = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = PAAS_PAYPAL_AMTOK_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTitle, PAYPAL_AMT_OK_TITLE);

	debug_sprintf(szDbgMsg, "%s: szTitle for Amt OK [%s]", __FUNCTION__, szTitle);
	APP_TRACE(szDbgMsg);

	/* Set the amount label */
	memset(szAmtTitle, 0x00, sizeof(szAmtTitle));
	sprintf(szAmtTitle, "$%s", pszTranAmt);

	debug_sprintf(szDbgMsg, "%s: szAmtTitle for Amt OK [%s]", __FUNCTION__, szAmtTitle);
	APP_TRACE(szDbgMsg);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_PAYPAL_LBL_1;
	ctrlIds[1] 	= PAAS_PAYPAL_LBL_2;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 2, giLabelLen, stUIReq.pszBuf);



	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	/* Send the batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Paypal Amount OK Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					/* Check if the xevt response got is for the same form */
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_PAYPAL_DATA_FK_2:
							debug_sprintf(szDbgMsg, "%s: YES", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							strcpy(szAppLogData, "User Selected [Yes] Option on Paypal Amount OK Screen");

							rv = UI_YES_SELECTED;
							break;

						case PAAS_PAYPAL_DATA_FK_1:
							debug_sprintf(szDbgMsg, "%s: NO", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							strcpy(szAppLogData, "User Selected [NO] Option on Paypal Amount OK Screen");

							rv = UI_NO_SELECTED;
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Invalid option!!!!",
																__FUNCTION__);
							APP_TRACE(szDbgMsg);

							strcpy(szAppLogData, "Invalid Option selected From Paypal Amount OK Screen");

							rv = ERR_DEVICE_APP;
							break;
						}
						if(iAppLogEnabled == 1)
						{
							addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showSplitTenderOptScreen()
 *
 * Description	: This API would be used to ask the users whether they want
 * 					their transaction amount to be authorised on a single card.
 * 					It will be called if the split tendering is enabled in the
 * 					merchant configuration.
 *
 * Input Params	: None
 *
 * Output Params: UI_YES_SELECTED/UI_NO_SELECTED
 * ============================================================================
 */
int showSplitTenderOptScreen(char *pszTranAmt)
{
	int				rv					= PAAS_FALSE;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szTmp[100]			= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256] = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = GEN_CFRM_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	initUIReqMsg(&stUIReq, BATCH_REQ);

#if 0
	if(bEmvEnabled) //Praveen_P1: Doing clear screen before showing the split tender
	{
		memset(szTmp, 0x00, sizeof(szTmp));
		sprintf(szTmp, "%s%c", "XCLS", RS);
		strcat(stUIReq.pszBuf, szTmp);
	}
#endif

	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTitle, SPLITTNDR_CFRM_TITLE);

	/*
	 * Kohls Requirements:
	 * we have to display Total amount due
	 * on the split tender option screen
	 * to reiterate to the consumer the transactional total.
	 */
	/* Set the amount label */
	memset(szAmtTitle, 0x00, sizeof(szAmtTitle));
	fetchScrTitle(szTmp, TOTAL_AMT_DUE_TITLE);
	sprintf(szAmtTitle, "%s $%s", szTmp, pszTranAmt);


	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_GEN_CRFM_LBL_1;
	ctrlIds[1] 	= PAAS_GEN_CRFM_LBL_7;
	ctrlIds[2] 	= PAAS_GEN_CRFM_LBL_8;
	ctrlIds[3] 	= PAAS_GEN_CRFM_LBL_6;

	/*Setting the screen title for mutiple labels*/
	//setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen);
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 4, giLabelLen, stUIReq.pszBuf);

	/* -------------- Set the labels for the buttons ------------ */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_07);
	/* Set the label for the YES(ENTER/GREEN) key on the screen */
	setStringValueWrapper(PAAS_GEN_CRFM_LBL_4, PROP_STR_CAPTION,
			(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);

	/* Set the label for the NO(CANCEL/RED) key on the screen */
	setStringValueWrapper(PAAS_GEN_CRFM_LBL_3, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	/* Send the batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Split Payment Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					/* Check if the xevt response got is for the same form */
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_GEN_CRFM_FK_YES:
							debug_sprintf(szDbgMsg, "%s: YES", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							strcpy(szAppLogData, "User Selected [Yes] Option on Split Payment Screen");

							rv = UI_YES_SELECTED;
							break;

						case PAAS_GEN_CRFM_FK_NO:
							debug_sprintf(szDbgMsg, "%s: NO", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							strcpy(szAppLogData, "User Selected [NO] Option on Split Payment Screen");

							rv = UI_NO_SELECTED;
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Invalid option!!!!",
																__FUNCTION__);
							APP_TRACE(szDbgMsg);

							strcpy(szAppLogData, "Invalid Option selected From Split Payment Screen");

							rv = ERR_DEVICE_APP;
							break;
						}
						if(iAppLogEnabled == 1)
						{
							addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showTranContinueOptScreen()
 *
 * Description	: This API would be used to ask the users whether they want
 * 					to continue the transaction.
 * 					It will be called if in case of EMV transaction when host
 * 					approved partially.
 *
 * Input Params	: None
 *
 * Output Params: UI_YES_SELECTED/UI_NO_SELECTED
 * ============================================================================
 */
int showTranContinueOptScreen(char *pszTranAmt)
{
	int				rv					= PAAS_FALSE;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szTmp[100]			= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256] = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = GEN_CFRM_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	initUIReqMsg(&stUIReq, BATCH_REQ);

	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTitle, TRAN_CONT_CFRM_TITLE);

	/* Set the Approved Amount label */
	memset(szAmtTitle, 0x00, sizeof(szAmtTitle));
	fetchScrTitle(szTmp, TRAN_APR_AMT_TITTLE);
	sprintf(szAmtTitle, "%s $%s", szTmp, pszTranAmt);


	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_GEN_CRFM_LBL_1;
	ctrlIds[1] 	= PAAS_GEN_CRFM_LBL_7;
	ctrlIds[2] 	= PAAS_GEN_CRFM_LBL_8;
	ctrlIds[3] 	= PAAS_GEN_CRFM_LBL_6;

	/*Setting the screen title for mutiple labels*/
	//setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen);
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 4, giLabelLen, stUIReq.pszBuf);

	/* -------------- Set the labels for the buttons ------------ */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_07);
	/* Set the label for the YES(ENTER/GREEN) key on the screen */
	setStringValueWrapper(PAAS_GEN_CRFM_LBL_4, PROP_STR_CAPTION,
			(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);

	/* Set the label for the NO(CANCEL/RED) key on the screen */
	setStringValueWrapper(PAAS_GEN_CRFM_LBL_3, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	/* Send the batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Split Payment Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					/* Check if the xevt response got is for the same form */
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_GEN_CRFM_FK_YES:
							debug_sprintf(szDbgMsg, "%s: YES", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							strcpy(szAppLogData, "User Selected [Yes] Option on Split Payment Screen");

							rv = UI_YES_SELECTED;
							break;

						case PAAS_GEN_CRFM_FK_NO:
							debug_sprintf(szDbgMsg, "%s: NO", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							strcpy(szAppLogData, "User Selected [NO] Option on Split Payment Screen");

							rv = UI_NO_SELECTED;
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Invalid option!!!!",
																__FUNCTION__);
							APP_TRACE(szDbgMsg);

							strcpy(szAppLogData, "Invalid Option selected From Split Payment Screen");

							rv = ERR_DEVICE_APP;
							break;
						}
						if(iAppLogEnabled == 1)
						{
							addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showIPEntryScreen
 *
 * Description	:
 *
 * Input Params	: szIPAddr -> placeholder for IP address
 *
 * Output Params:
 * ============================================================================
 */
int showIPEntryScreen(char * szIPAddr, int iMsgType)
{
	int				rv					= SUCCESS;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTmp[100]			= "";
	char			szTitle[200]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	unsigned 		shOptions 			= 0;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		whichForm = NUM_ENTRY_FRM;

		iDisableFormFlag = 1;

		/* Initialize the form */
		initUIReqMsg(&stUIReq, BATCH_REQ);
		initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

		shOptions = getOptionsForCtrlid(PS_NUM_ENTRY_EDT_1, stUIReq.pszBuf);//Getting the current options for the ctrlid of the form

		shOptions &=  ~OPT_TB_RL_MASK;

		initUIReqMsg(&stUIReq, BATCH_REQ);

		setShortValueWrapper(PS_NUM_ENTRY_EDT_1 , PROP_SHORT_OPTIONS, shOptions, stUIReq.pszBuf);
		setShortValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_SHORT_MIN_ENTRIES, 12, stUIReq.pszBuf);
		setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_DISPLAY_STRING,"XXX.XXX.XXX.XXX", stUIReq.pszBuf);
		setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_FORMAT_STRING, "NNNCNNNCNNNCNNN", stUIReq.pszBuf);

		setBoolValueWrapper(PS_NUM_ENTRY_FK_CANCEL, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
		setBoolValueWrapper(PS_NUM_ENTRY_FK_ENTER,  PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);

		switch(iMsgType)
		{
		case 0:	/* IP Address */
			/* Set the Title */
			fetchScrTitle(szTitle, ENTER_IP_TITLE);
			break;

		case 1: /* Netmask */
			/* Set the Title */
			fetchScrTitle(szTitle, ENTER_SUBNET_TITLE);
			break;

		case 2: /* Gateway */
			/* Set the Title */
			fetchScrTitle(szTitle, ENTER_GATEWAY_TITLE);
			break;

		case 3:	/* DNS 1 */
		case 4:	/* DNS 2 */
			/* Set the Title */
			fetchScrTitle(szTmp, ENTER_DNS_TITLE);
			if(iMsgType == 3)
			{
				sprintf(szTitle, "%s 1", szTmp);
			}
			else
			{
				sprintf(szTitle, "%s 2", szTmp);
			}

			/* Set the label for the button - "Skip DNS Entry"*/
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_02);
			setStringValueWrapper(PS_NUM_ENTRY_LBL_5, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);
			setBoolValueWrapper(PS_NUM_ENTRY_BTN_1, PROP_BOOL_VISIBLE, 1,
							stUIReq.pszBuf);

			break;
		case 5: /* IP For PING test */
			fetchScrTitle(szTitle, ENTER_PING_IP_TITLE);

			/* Set the label for the button - "Skip "*/
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_11);
			setStringValueWrapper(PS_NUM_ENTRY_LBL_5, PROP_STR_CAPTION,
									(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);
			setBoolValueWrapper(PS_NUM_ENTRY_BTN_1, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);

			break;
		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Do not proceed further if an error is encountered while executing
		 * this function */
		if(rv != SUCCESS)
		{
			break;
		}

		/*Setting the ctrlids for the form*/
		ctrlIds[0] 	= PS_NUM_ENTRY_LBL_1;
		ctrlIds[1] 	= PS_NUM_ENTRY_LBL_11;
		ctrlIds[2] 	= PS_NUM_ENTRY_LBL_12;

		/*Setting the screen title for mutiple labels*/
		setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);

		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
			break;
		}
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing IP Entry Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}

		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					/* Check if the xevt response got is for the same form */
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PS_NUM_ENTRY_EDT_1:
							if(pstXevt->uiKeypadEvt == 2) /* ON ENTER */
							{
								debug_sprintf(szDbgMsg, "%s: Value = [%s]",
										__FUNCTION__, pstXevt->szKpdVal);
								APP_TRACE(szDbgMsg);

								strcpy(szIPAddr, pstXevt->szKpdVal);

								rv = UI_ENTER_PRESSED;
							}
							else if(pstXevt->uiKeypadEvt == 3)/*ON CANCEL*/
							{
								debug_sprintf(szDbgMsg, "%s: CANCEL",
																__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = UI_CANCEL_PRESSED;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Invalid kpd event",
														__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
							}
							break;

						case PS_NUM_ENTRY_BTN_1:
							debug_sprintf(szDbgMsg, "%s: SKIP",__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_SKIP_BUTTON_PRESSED;
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Invalid control id",
											__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
							break;
						}

						/* Resetting the flag */
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showNetworkOptionScreen
 *
 * Description	:
 *
 * Input Params	: None
 *
 * Output Params: UI_YES_SELECTED/UI_NO_SELECTED/ERR_DEVICE_APP;
 * ============================================================================
 */
int showNetworkOptionScreen()
{
	int				rv					= SUCCESS;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = NET_CFG_OPT_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	/* Initialize the form */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTitle, NWK_CFG_TITLE);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PS_NET_OPT_LBL_1;
	ctrlIds[1] 	= PS_NET_OPT_LBL_5;
	ctrlIds[2] 	= PS_NET_OPT_LBL_6;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);


	/* -------------- Set the labels for the buttons ------------ */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_01);
	/* Set the label "STATIC" for first key on the screen */
	setStringValueWrapper(PS_NET_OPT_LBL_2, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);
	/* Set the label "DHCP" for second key on the screen */
	setStringValueWrapper(PS_NET_OPT_LBL_3, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Set the label "DHCP ALWAYS" for third key on the screen */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_02);
	setStringValueWrapper(PS_NET_OPT_LBL_4, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);
	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	while(1)
	{
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
									__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
			break;
		}
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Network Selection Option Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}

		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					/* Check if the xevt response got is for the same form */
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PS_NET_OPT_BTN_1:
							debug_sprintf(szDbgMsg, "%s: STATIC", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_STATIC_SELECTED;
							break;

						case PS_NET_OPT_BTN_2:
							debug_sprintf(szDbgMsg, "%s: DHCP", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_DHCP_SELECTED;
							break;

						case PS_NET_OPT_BTN_3:
							debug_sprintf(szDbgMsg, "%s: DHCP ALWAYS",
																__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_DHCP_ALWAYS_SELECTED;
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Invalid option !!!!",
																__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
							break;
						}

						/* Resetting the flag */
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showSampleSaleCfrmScreen
 *
 * Description	:
 *
 * Input Params	: None
 *
 * Output Params: UI_YES_SELECTED/UI_NO_SELECTED/ERR_DEVICE_APP;
 * ============================================================================
 */
int showSampleSaleCfrmScreen(char * pszTranAmt)
{
	int				rv					= SUCCESS;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTmp[100]			= "";
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = GEN_CFRM_FRM;

	debug_sprintf(szDbgMsg, "%s: Form to be displayed is [%s]", __FUNCTION__,
					frmName[whichForm]);
	APP_TRACE(szDbgMsg);

	iDisableFormFlag = 1;

	/* Initialize the form */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTitle, PREAMBLE_CFRM1_TITLE);

	fetchScrTitle(szTmp, PREAMBLE_CFRM2_TITLE);
	sprintf(szAmtTitle, szTmp, pszTranAmt);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_GEN_CRFM_LBL_1;
	ctrlIds[1] 	= PAAS_GEN_CRFM_LBL_7;
	ctrlIds[2] 	= PAAS_GEN_CRFM_LBL_8;
	ctrlIds[3] 	= PAAS_GEN_CRFM_LBL_6;

	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 4, giLabelLen, stUIReq.pszBuf);

	/* -------------- Set the labels for the buttons ------------ */
	memset(&stBtnLbl, 0x00, sizeof(BTNLBL_STYPE));
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_08);

	/* Set the label for the CONTINUE(ENTER/GREEN) key on the screen */
	setStringValueWrapper(PAAS_GEN_CRFM_LBL_4, PROP_STR_CAPTION,
			(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);

	/* Set the label for the CANCEL(CANCEL/RED) key on the screen */
	setStringValueWrapper(PAAS_GEN_CRFM_LBL_3, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	/* Send the batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Sample Sale Confirmation Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_GEN_CRFM_FK_YES:
							debug_sprintf(szDbgMsg, "%s: YES", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_YES_SELECTED;
							break;

						case PAAS_GEN_CRFM_FK_NO:
							debug_sprintf(szDbgMsg, "%s: NO", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_NO_SELECTED;
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Invalid option !!!!",
											__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
							break;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showRetryScreen
 *
 * Description	:
 *
 * Input Params	: None
 *
 * Output Params: UI_YES_SELECTED/UI_NO_SELECTED/ERR_DEVICE_APP;
 * ============================================================================
 */
int showRetryScreen()
{
	int				rv					= SUCCESS;
	int 			ctrlIds[4];
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = GEN_CFRM_FRM;

	debug_sprintf(szDbgMsg, "%s: Form to be displayed is [%s]", __FUNCTION__,
					frmName[whichForm]);
	APP_TRACE(szDbgMsg);

	iDisableFormFlag = 1;

	/* Initialize the form */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTitle, RETRY_CFRM_TITLE);

	debug_sprintf(szDbgMsg, "%s: Title = [%s]", __FUNCTION__, szTitle);
	APP_TRACE(szDbgMsg);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_GEN_CRFM_LBL_1;
	ctrlIds[1] 	= PAAS_GEN_CRFM_LBL_7;
	ctrlIds[2] 	= PAAS_GEN_CRFM_LBL_8;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);

	/* -------------- Set the labels for the buttons ------------ */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_07);
	/* Set the label for the YES(ENTER/GREEN) key on the screen */
	setStringValueWrapper(PAAS_GEN_CRFM_LBL_4, PROP_STR_CAPTION,
			(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);
	/* Set the label for the NO(CANCEL/RED) key on the screen */
	setStringValueWrapper(PAAS_GEN_CRFM_LBL_3, PROP_STR_CAPTION,
							(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	/* Send batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Retry Option Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_GEN_CRFM_FK_YES:
							debug_sprintf(szDbgMsg, "%s: YES", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_YES_SELECTED;
							break;

						case PAAS_GEN_CRFM_FK_NO:
							debug_sprintf(szDbgMsg, "%s: NO", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_NO_SELECTED;
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Invalid option !!!!",
											__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
							break;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showPSGenMsgWithRetryScreen
 *
 * Description	: This Function Will Show Message sent in iPromptMsg argument
 * with the RETRY Button, so that user can select RETRY button. If the screen is to be shown only for defined time, pass the iWaitTime with the function, else pass 0.
 *
 * Input Params	: None
 *
 * Output Params: UI_YES_SELECTED/ERR_DEVICE_APP;
 * ============================================================================
 */
int showPSGenMsgWithRetryScreen(int iPromptMsg, int iWaitTime)
{
	int					rv								= SUCCESS;
	int					iDisableFormFlag				= 0;
	ullong				endTime							= 0L;
	ullong				curTime							= 0L;
	char				szFileName[MAX_FILE_NAME_LEN]	= "";
	char				szTmp[256]						= "";
	PAAS_BOOL			bWait							= PAAS_TRUE;
	LABEL_STYPE_TITLE 	stLblTitle;
	BTNLBL_STYPE		stBtnLbl;
	XEVT_PTYPE			pstXevt							= NULL;
	UIREQ_STYPE			stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256]					= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = GEN_DISP_FRM;

	/* Initialize the batch message */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	setBoolValueWrapper(PAAS_GEN_DISPLAY_BTN_RETRY, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);

	setBoolValueWrapper(PAAS_GEN_DISPLAY_LBL_2, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
	/* -------------- Set the labels for the buttons ------------ */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_30);
	setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_2, PROP_STR_CAPTION, (char *)stBtnLbl.szLblOne, stUIReq.pszBuf);

	memset(&stLblTitle, 0x00, sizeof(stLblTitle));
	//Split title string of required length(i.e Based on maxLabelLength defined for Mx915 or Mx925).
	getDisplayMsg(szTmp, iPromptMsg);
	splitStringOfReqLength(szTmp, giLabelLen + 2, &stLblTitle);

	debug_sprintf(szDbgMsg, "%s: szTitle1 =%s, szTitle2 =%s, szTitle3 =%s",__FUNCTION__, stLblTitle.szTitle1, stLblTitle.szTitle2, stLblTitle.szTitle3);
	APP_TRACE(szDbgMsg);

	if(iWaitTime > 0)
	{
		curTime = svcGetSysMillisec();
		endTime = curTime + (iWaitTime * 1000L);
	}

	/*Set the title */
	if ( strlen(stLblTitle.szTitle3) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_1, PROP_STR_CAPTION,
												stLblTitle.szTitle1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
												stLblTitle.szTitle2, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_5, PROP_STR_CAPTION,
												stLblTitle.szTitle3, stUIReq.pszBuf);
	}
	else if ( strlen(stLblTitle.szTitle2) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
												stLblTitle.szTitle1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_5, PROP_STR_CAPTION,
												stLblTitle.szTitle2, stUIReq.pszBuf);
	}
	else if ( strlen(stLblTitle.szTitle1) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
												stLblTitle.szTitle1, stUIReq.pszBuf);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Title not found",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	strcpy(szFileName, "indicator_red.png");
	getFileNamewithPlatformPrefix(szFileName);

	/* Set the image on the screen */
	setStringValueWrapper(PAAS_GEN_DISPLAY_IMG_1, PROP_STR_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	/* Send batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			svcWait(5);
		}


		bWait = PAAS_TRUE;
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_GEN_DISPLAY_BTN_RETRY:
							debug_sprintf(szDbgMsg, "%s: Retry Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_YES_SELECTED;
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Invalid option !!!!",
											__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
							break;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(endTime != 0)
			{
				if(curTime > endTime)
				{
					bWait 		= PAAS_FALSE;
				}
				curTime 	= svcGetSysMillisec();
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showCounterTipAmtScreen
 *
 * Description	:
 *
 * Input Params	: None
 *
 * Output Params: UI_ENTER_PRESSED/UI_CANCEL_PRESSED
 * ============================================================================
 */
int showCounterTipAmtScreen(char * pszTranAmt, char * pszOutAmt)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTmp[100]			= "";
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	int 			ctrlIds[4];
	unsigned short  shOptions = 0;
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = NUM_ENTRY_FRM;

	iDisableFormFlag = 1;

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	//Getting the current options for the ctrlid of the form
	shOptions = getOptionsForCtrlid(PS_NUM_ENTRY_EDT_1, stUIReq.pszBuf);

	//To make sure that the option of right to left entry is enabled.
	shOptions |= OPT_TB_RL_MASK;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	setShortValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_SHORT_OPTIONS, shOptions, stUIReq.pszBuf);

	/* ---------------- Set the labels ----------------- */

	/* Set the Title */
	fetchScrTitle(szTitle, ENTER_TIP_TITLE);


	/* Set the sub-title "amount" */
	fetchScrTitle(szTmp, TOTAL_AMT_DUE_TITLE);
	sprintf(szAmtTitle, "%s $%s", szTmp, pszTranAmt);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PS_NUM_ENTRY_LBL_1;
	ctrlIds[1] 	= PS_NUM_ENTRY_LBL_11;
	ctrlIds[2] 	= PS_NUM_ENTRY_LBL_12;
	ctrlIds[3] 	= PS_NUM_ENTRY_LBL_2;

	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 4, giLabelLen, stUIReq.pszBuf);

	/* Set the button labels */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_08);
	setStringValueWrapper(PS_NUM_ENTRY_LBL_4, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	fetchBtnLabel(&stBtnLbl, BTN_LBLS_09);
	setStringValueWrapper(PS_NUM_ENTRY_LBL_3, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Set the display string on the textbox */
	setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_DISPLAY_STRING,
														"$      .00", stUIReq.pszBuf);

	/* Set the format string for the textbox */
	setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_FORMAT_STRING,
														"CNNNNNNCNN", stUIReq.pszBuf);
	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	/* Send batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv == SUCCESS)
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Counter Tip Amount Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		/* Wait for the data(tip amount) from the UI agent */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					/* Check if the xevt response got is for the same form */
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == PS_NUM_ENTRY_EDT_1)
						{
							switch(pstXevt->uiKeypadEvt)
							{
							case 2:	/* ON ENTER */
								debug_sprintf(szDbgMsg, "%s: Value Entered %s",
											__FUNCTION__, pstXevt->szKpdVal);
								APP_TRACE(szDbgMsg);

								strcpy(pszOutAmt, pstXevt->szKpdVal);

								rv = UI_ENTER_PRESSED;
								break;

							case 3: /* ON CANCEL */
								debug_sprintf(szDbgMsg, "%s: CANCEL pressed",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Pressed Cancel Pressed on Tip Amount Entry Screen");
									addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
								}

								rv = UI_CANCEL_PRESSED;
								break;

							default:
								debug_sprintf(szDbgMsg, "%s: Invalid Kpd event",
																__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
								break;
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Invalid control Id",
																__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showSplitTenderAmtScreen()
 *
 * Description	: This API would be used to prompt the users to enter the
 * 					amount to be authorised for their current card. It will be
 * 					called if the users select option NO from the Split Tender
 * 					Opt screen.
 * Input Params	: None
 *
 * Output Params: UI_ENTER_PRESSED/UI_CANCEL_PRESSED
 * ============================================================================
 */
int showSplitTenderAmtScreen(char * pszTranAmt)
{
	int				rv					= PAAS_FALSE;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTmp[100]			= "";
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	double			fCurTranAmt			= 0.0;
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	unsigned short shOptions = 0;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: -- enter --", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	fCurTranAmt = atof(pszTranAmt);

	whichForm = NUM_ENTRY_FRM;

	iDisableFormFlag = 1;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	//Getting the current options for the ctrlid of the form
	shOptions = getOptionsForCtrlid(PS_NUM_ENTRY_EDT_1, stUIReq.pszBuf);

	//To make sure that the option of right to left entry is enabled.
	shOptions |= OPT_TB_RL_MASK;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	setShortValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_SHORT_OPTIONS,
												shOptions, stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTitle, ENTER_SPLITAMT_TITLE);

	/* Set the sub-title "amount" */
	fetchScrTitle(szTmp, TOTAL_AMT_DUE_TITLE);
	sprintf(szAmtTitle, "%s $%s", szTmp, pszTranAmt);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PS_NUM_ENTRY_LBL_1;
	ctrlIds[1] 	= PS_NUM_ENTRY_LBL_11;
	ctrlIds[2] 	= PS_NUM_ENTRY_LBL_12;
	ctrlIds[3] 	= PS_NUM_ENTRY_LBL_2;

	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 4, giLabelLen, stUIReq.pszBuf);

	/* Set the button labels */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_08);
	setStringValueWrapper(PS_NUM_ENTRY_LBL_4, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	fetchBtnLabel(&stBtnLbl, BTN_LBLS_09);
	setStringValueWrapper(PS_NUM_ENTRY_LBL_3, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Set the display string on the textbox */
	setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_DISPLAY_STRING,
														"$       .00", stUIReq.pszBuf);

	/* Set the format string for the textbox */
	setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_FORMAT_STRING,
														"CNNNNNNNCNN", stUIReq.pszBuf);
	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	/* Send batch message to the UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while sending message to UI agent",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Split Tender Amount Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		/* Wait for the data(tip amount) from the UI agent */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					/* Check if the xevt response got is for the same form */
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == PS_NUM_ENTRY_EDT_1)
						{
							switch(pstXevt->uiKeypadEvt)
							{
							case 2:	/* ON ENTER */
								debug_sprintf(szDbgMsg, "%s: Value Entered %s",
											__FUNCTION__, pstXevt->szKpdVal);
								APP_TRACE(szDbgMsg);

								strcpy(pszTranAmt, pstXevt->szKpdVal);

								rv = UI_ENTER_PRESSED;
								break;

							case 3: /* ON CANCEL */
								debug_sprintf(szDbgMsg, "%s: CANCEL pressed",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = UI_CANCEL_PRESSED;
								break;

							default:
								debug_sprintf(szDbgMsg, "%s: Invalid Kpd event",
																__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
								break;
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Invalid control Id",
																__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPINDetailsFromUser()
 *
 * Description	: This API would be used to prompt the user for entering the
 * 					PIN.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE / Other return values
 * ============================================================================
 */
int getPINDetailsFromUser(char * szPAN, char * szPIN, char * szKSN, int iPINPromptType)
{
	int						rv						= SUCCESS;
	static int				pinOptional				= -1;
	PAAS_BOOL				bWait					= PAAS_TRUE;
	//static PAAS_BOOL		bFirstTime				= PAAS_TRUE;
	XEVT_PTYPE				pstXevt					= NULL;
	PINDATA_PTYPE			pstPIN					= NULL;
	char					szTmp[100]				= "";
	UIREQ_STYPE				stUIReq;

#ifdef DEBUG
	char				szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---",__FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Set the current form */
	whichForm = FA_PIN_ENTRY_FRM;

	debug_sprintf(szDbgMsg, "%s: Prompt Type %d",__FUNCTION__, iPINPromptType);
	APP_TRACE(szDbgMsg);

	/* Daivik:16/2/2016 - The below check ensures that when PinPromptType is not set ( happens in case of CDT ) and if SigOptionRequiredOnPinEntry screen is
	 * configured as disabled (Signature OPtion not required on PIN entry ) then, we override the pin prompt type to compulsory.
	 */
	if((isSigOptionReqOnPINEntryScreen() == FALSE) && (iPINPromptType == 0) )
	{
		iPINPromptType = 3;
	}
		/*
		 * We have to show the button to skip the PIN entry based on the
		 * PIN Prompt type. since this form is shown by the FormAgent
		 * we dont have control to hide some buttons, thats why
		 * remaining the XXX_FA_PINE.FRM
		 */
		switch(iPINPromptType)
		{
		/* The case for CDT was added here because , in CDT case as well, we need to switch the below config based on the pin prompt type.
		 * For CTLS EMV transactions we are setting pinPromptType to compulsory (3), so we would have set the z62_ccnv_enable to 0, if the next transaction
		 * was not CTLS, then z62_ccnv_enable should be set depending on pin prompt type for that transaction.
		 */
		case 0://This will be the value for CDT transactions
		case 1: //Optional Pin prompt
		case 2: //Optional PIN Prompt
			//putEnvFile("iab", "z62_ccnv_enable", "1"); //set corresponding XPI variable
			if(pinOptional != 1)
			{
				setXPIParameter("z62_ccnv_enable", "1");
				pinOptional = 1;
			}

			break;

		case 3: //PIN is compulsory
			//putEnvFile("iab", "z62_ccnv_enable", "0"); //set corresponding XPI variable
			if(pinOptional != 0)
			{
				setXPIParameter("z62_ccnv_enable", "0");
				pinOptional = 0;
			}
			break;
			//We should not show the button on the pin entry form
			break;

		default:
			break;
		}

	/* Initialize the message to be sent to the UI agent */
	initUIReqMsg(&stUIReq, INIT_REQ);

	if(PAAS_TRUE == getPinPadMode())
	{
		memset(szTmp, 0x00, sizeof(szTmp));
		/* Get the Label and Title from .ini file */
		fetchScrTitle(szTmp, ENTER_PIN_TITLE);

		formDukptPinRequest(szPAN, stUIReq.pszBuf, szTmp);
	}
	else
	{
		formMkPinRequest(szPAN, stUIReq.pszBuf);
	}

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	/* Send the message to the UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					/* Check if the xevt response got is for the same form */
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == FA_PINE_BTN_1)
						{
							debug_sprintf(szDbgMsg, "%s: SIG Button Pressed",__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_SIGN_BUTTON_PRESSED;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}
				else if(stBLData.uiRespType == UI_PIN_RESP)
				{
					switch(stBLData.iStatus)
					{
					case SUCCESS:
						pstPIN = &(stBLData.stRespDtls.stPINInfo);

						/* Copy the data into the corresponding variables
						 * passed in the parameters */
						strcpy(szPIN, pstPIN->szPIN);
						strcpy(szKSN, pstPIN->szKSN);

						rv = SUCCESS;
						break;

					case UI_CANCEL_PRESSED:
						debug_sprintf(szDbgMsg, "%s: CANCEL", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = UI_CANCEL_PRESSED;
						break;

					case ERR_NO_KEYS_PRESENT:
						debug_sprintf(szDbgMsg, "%s: Keys are not injected",
																__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_NO_KEYS_PRESENT;
						break;

					case ERR_NULL_PIN_ENTERED:
						debug_sprintf(szDbgMsg, "%s: User did not enter the PIN"
																, __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_NULL_PIN_ENTERED;
						break;

					case UI_SIGN_BUTTON_PRESSED:
						debug_sprintf(szDbgMsg, "%s: SIG Button Pressed",__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = UI_SIGN_BUTTON_PRESSED;

						break;

					default:
						debug_sprintf(szDbgMsg, "%s: Error while capturing PIN",
										__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_DEVICE_APP;
						break;

					}

					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setSIGBOXArea
 *
 * Description	: Set The SIGBOX Area to the Default Value, which is available in the config.usr1 file
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int setSIGBOXArea()
{
	int				rv					= SUCCESS;
	int				startX 				= 0;
	int				startY 				= 0;
	int				endX   				= 0;
	int				endY   				= 0;
	UIREQ_STYPE		stUIReq;

	/*
	 * Raghavendran_R1: We need top set the Signature Coordinates Area (SIGBAOX parameter) back to the
	 * Default/Whatever given in the  config.usr1.
	 */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	getSigCapCoordinates(&startX, &startY, &endX, &endY);
	setSigCapBoxArea(startX, startY, endX, endY, 0, stUIReq.pszBuf);
	rv = sendUIReqMsg(stUIReq.pszBuf);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showSignatureExScreenForDevTran
 *
 * Description	: Gets the signature Ex, with limited Disclaimer text which will be displayed in the screen with scroll bar.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showSignatureExScreenForDevTran(char * szDisclaimer, uchar ** pszSignData, int * iLen)
{
	int				rv					= SUCCESS;
	int				startX 				= 0;
	int				startY 				= 0;
	int				endX   				= 0;
	int				endY   				= 0;
	int				devModel			= 0;
	int				iSaveMode			= 0;
	int				iDiscLen			= 0;
	int 			iCurPosition		= 0;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	int				iDisableFormFlag	= 0;
	char			szTmp[1025]			= "";
	char			szImageType[10]		= "";
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	SIGDATA_PTYPE	pstSign				= NULL;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	/* Initialize the elements of the form */
	whichForm = SIG_EX_CAP_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* --------- Set the Labels ----------- */

	/* Set the Title */
	fetchScrTitle(szTitle, ENTER_SIGN_TITLE);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_SIG_CAP_EX_LBL_1;
	ctrlIds[1] 	= PAAS_SIG_CAP_EX_LBL_4;
	ctrlIds[2] 	= 0;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);

	devModel = svcInfoPlatform(7);
	/* Value Not Found  - Set Default */
	if(devModel == MODEL_MX915)
	{
		startX 	= DLFT_915_SIG_EX_BEGX;
		startY 	= DLFT_915_SIG_EX_BEGY;
		endX 	= DLFT_915_SIG_EX_ENDX;
		endY 	= DLFT_915_SIG_EX_ENDY;
	}
	else
	{
		startX 	= DLFT_925_SIG_EX_BEGX;
		startY 	= DLFT_925_SIG_EX_BEGY;
		endX 	= DLFT_925_SIG_EX_ENDX;
		endY 	= DLFT_925_SIG_EX_ENDY;
	}
	setSigCapBoxArea(startX, startY, endX, endY, 0, stUIReq.pszBuf);

	/* Set the signature parameters like image type, resolution, endianness etc
	 * which would be required by the UI agent to capture the signature
	 * effectively */
	getSigImageType(szImageType);
	if(strcmp(szImageType, SIGN_IMAGE_TYPE_TIFF) == 0)
	{
		/* Save as 200 DPI TIF File */
		iSaveMode = 2;
	}
	else if(strcmp(szImageType, SIGN_IMAGE_TYPE_BMP) == 0)
	{
		/* Save as Windows Bitmap File (BMP) */
		iSaveMode = 6;
	}
	else if(strcmp(szImageType, SIGN_IMAGE_TYPE_3BA) == 0)
	{
		/* Save as 3-Byte ASCII (3BA) */
		iSaveMode = 5;
	}
	else
	{
		/* Should not come here..incase if it comes */
		/* Save as 200 DPI TIF File */
		iSaveMode = 2;
	}

	setSigCapParams(0, 0, 0, B_ENDIAN, 100, iSaveMode, stUIReq.pszBuf);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while communicating with UI agent",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
		return rv;
	}

	/* Set the disclaimer string */
	if((szDisclaimer == NULL) || (strlen(szDisclaimer) <= 0))
	{
		initUIReqMsg(&stUIReq, BATCH_REQ);

		getDisplayMsg(szTmp, MSG_SIGN_DISCLAIM);
		addTextboxTextWrapper(PAAS_SIG_CAP_EX_TXT_1, szTmp, stUIReq.pszBuf);
	}
	else
	{
		iDiscLen = strlen(szDisclaimer);
		while(iDiscLen > 0)
		{
			initUIReqMsg(&stUIReq, BATCH_REQ);

			memset(szTmp, 0x00, sizeof(szTmp));
			if (iDiscLen > 1024)
			{
				strncpy(szTmp, szDisclaimer+iCurPosition, 1024);
			}
			else
			{
				strcpy(szTmp, szDisclaimer+iCurPosition);
			}
			iDiscLen 	 -= 1024;
			iCurPosition += 1024;

			addTextboxTextWrapper(PAAS_SIG_CAP_EX_TXT_1, szTmp, stUIReq.pszBuf);

			bWait = PAAS_TRUE;
			rv = sendUIReqMsg(stUIReq.pszBuf);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while communicating with UI agent",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_DEVICE_APP;
				break;
			}
			else
			{
				/* Wait for the UI Agent resp */
				while(bWait == PAAS_TRUE)
				{
					if(getPOSInitiatedState())
					{
						rv = retValForPosIniState();
						break;
					}
					if(bBLDataRcvd == PAAS_TRUE)
					{
						acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

						if(stBLData.uiRespType == UI_GENRL_RESP)
						{
							bBLDataRcvd = PAAS_FALSE;
							bWait = PAAS_FALSE;
						}

						releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
					}
					if(bWait == PAAS_FALSE)
					{
						break;
					}
					svcWait(15);
				}
			}
		}
		initUIReqMsg(&stUIReq, BATCH_REQ);

		if((devModel == 925 && strlen(szDisclaimer) > 360) || (devModel == 915 && strlen(szDisclaimer) > 240))
		{
			setBoolValueWrapper(PAAS_SIG_CAP_EX_TXT_1, PROP_BOOL_LISTBOX_NO_SCROLL, 0, stUIReq.pszBuf);
		}
	}

	if(rv != SUCCESS)
	{
		return rv; // CID 67418 (#1 of 2): Unused value (UNUSED_VALUE) T_RaghavendranR1
	}

	if(rv == SUCCESS)
	{
		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		/* Command to UI agent to send the captured signature to our application */
		getSignature(stUIReq.pszBuf);

		bWait = PAAS_TRUE;
		/* Send the batch message to UI agent */
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while communicating with UI agent",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
			return rv;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				if(getPOSInitiatedState())
				{
					rv = retValForPosIniState();
					break;
				}
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);
			}
			if(rv == SUCCESS)
			{
				memset(szAppLogData, 0x00, sizeof(szAppLogData));
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Showing Signature Capture Screen");
					addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
				}

				bWait = PAAS_TRUE;
				while(bWait == PAAS_TRUE)
				{
					if(getPOSInitiatedState())
					{
						rv = retValForPosIniState();

						cancelRequest();
						break;
					}
					if(bBLDataRcvd == PAAS_TRUE)
					{
						acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

						if(stBLData.uiRespType == UI_SIGN_RESP)
						{
							switch(stBLData.iStatus)
							{
							case SUCCESS:
								pstSign = &(stBLData.stRespDtls.stSigInfo);

								*pszSignData = (uchar *) malloc(pstSign->iSigLen + 1);
								if(*pszSignData == NULL)
								{
									debug_sprintf(szDbgMsg, "%s: Malloc FAILED",
											__FUNCTION__);
									APP_TRACE(szDbgMsg);

									rv = ERR_SYSTEM;
									break;
								}

								*iLen = pstSign->iSigLen + 1;
								memset(*pszSignData, 0x00, *iLen);
								memcpy(*pszSignData, pstSign->szSigBuf, *iLen);

								/* VDR: TODO: Instead of allocating the memory and
								 * doing a memcpy, think of assigning the same pointer
								 * ahead */
								free(pstSign->szSigBuf);

								break;

							case ERR_DEVICE_APP:
							case FAILURE:
								debug_sprintf(szDbgMsg,"%s: FAILED to get Sign data",
										__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
								break;

							case UI_CANCEL_PRESSED:
								debug_sprintf(szDbgMsg,"%s: Signature capture CANCELLED"
										, __FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = UI_CANCEL_PRESSED;
								break;

							default:
								debug_sprintf(szDbgMsg, "%s: Unknown", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
								break;
							}

							bBLDataRcvd = PAAS_FALSE;
							bWait = PAAS_FALSE;
						}

						releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
					}
					if(bWait == PAAS_FALSE)
					{
						break;
					}
					svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
				}
			}
		}
	}

	setSIGBOXArea();

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showSignatureScreenForDevTran
 *
 * Description	: Gets the signature of the user.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showSignatureScreenForDevTran(char * szDisclaimer, uchar ** pszSignData, int * iLen)
{
	int				rv					= SUCCESS;
#if 0
	int				startX 			= 0;
	int				startY 			= 0;
	int				endX   			= 0;
	int				endY   			= 0;
#endif
	int				iSaveMode			= 0;
	int				iDiscLen			= 0;
	int 			iCurPosition		= 0;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTmp[1025]			= "";
	char			szImageType[10]		= "";
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	SIGDATA_PTYPE	pstSign				= NULL;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	/* Initialize the elements of the form */
	whichForm = SIG_CAP_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* --------- Set the Labels ----------- */

	/* Set the Title */
	fetchScrTitle(szTitle, ENTER_SIGN_TITLE);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_SIG_CAP_LBL_1;
	ctrlIds[1] 	= PAAS_SIG_CAP_LBL_4;
	ctrlIds[2] 	= 0;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);

#if 0
	/*
	 * Praveen_P1: FA supports setting the sigcap area through config parameter
	 * 			   Setting SIGBOX at the start of the application, so
	 * 			   not setting it here
	 * 			   Please use FA ver 3.0.1-BUILD18 onwards
	 */
	getSigCapCoordinates(&startX, &startY, &endX, &endY);
	setSigCapBoxArea(startX, startY, endX, endY, 0, stUIReq.pszBuf);
#endif
	/* Set the signature parameters like image type, resolution, endianness etc
	 * which would be required by the UI agent to capture the signature
	 * effectively */
	getSigImageType(szImageType);
	if(strcmp(szImageType, SIGN_IMAGE_TYPE_TIFF) == 0)
	{
		/* Save as 200 DPI TIF File */
		iSaveMode = 2;
	}
	else if(strcmp(szImageType, SIGN_IMAGE_TYPE_BMP) == 0)
	{
		/* Save as Windows Bitmap File (BMP) */
		iSaveMode = 6;
	}
	else if(strcmp(szImageType, SIGN_IMAGE_TYPE_3BA) == 0)
	{
		/* Save as 3-Byte ASCII (3BA) */
		iSaveMode = 5;
	}
	else
	{
		/* Should not come here..incase if it comes */
		/* Save as 200 DPI TIF File */
		iSaveMode = 2;
	}

	setSigCapParams(0, 0, 0, B_ENDIAN, 100, iSaveMode, stUIReq.pszBuf);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while communicating with UI agent",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
		return rv;
	}
#if 0 	//TODO: Please check whether we are waiting for proper resp type and uncomment it if required.
	else
	{
		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			svcWait(5);
		}
	}
#endif
	/* Set the disclaimer string */
	if((szDisclaimer == NULL) || (strlen(szDisclaimer) <= 0))
	{
		initUIReqMsg(&stUIReq, BATCH_REQ);

		getDisplayMsg(szTmp, MSG_SIGN_DISCLAIM);
		addTextboxTextWrapper(PAAS_SIG_CAP_TXT_1, szTmp, stUIReq.pszBuf);
	}
	else
	{
		iDiscLen = strlen(szDisclaimer);
		while(iDiscLen > 0)
		{
			initUIReqMsg(&stUIReq, BATCH_REQ);

			memset(szTmp, 0x00, sizeof(szTmp));
			if (iDiscLen > 1024)
			{
				strncpy(szTmp, szDisclaimer+iCurPosition, 1024);
			}
			else
			{
				strcpy(szTmp, szDisclaimer+iCurPosition);
			}
			iDiscLen 	 -= 1024;
			iCurPosition += 1024;

			addTextboxTextWrapper(PAAS_SIG_CAP_TXT_1, szTmp, stUIReq.pszBuf);

			bWait = PAAS_TRUE;
			rv = sendUIReqMsg(stUIReq.pszBuf);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while communicating with UI agent",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_DEVICE_APP;
				break;
			}
			else
			{
				/* Wait for the UI Agent resp */
				while(bWait == PAAS_TRUE)
				{
					CHECK_POS_INITIATED_STATE;
					if(bBLDataRcvd == PAAS_TRUE)
					{
						acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

						if(stBLData.uiRespType == UI_GENRL_RESP)
						{
							bBLDataRcvd = PAAS_FALSE;
							bWait = PAAS_FALSE;
						}

						releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
					}
					svcWait(5);
				}
			}
		}
		initUIReqMsg(&stUIReq, BATCH_REQ);

		if (strlen(szDisclaimer) >60)
		{
			setBoolValueWrapper(PAAS_SIG_CAP_TXT_1, PROP_BOOL_LISTBOX_NO_SCROLL, 0, stUIReq.pszBuf);
		}
	}

	//CID 67300 (#1 of 2): Unused value (UNUSED_VALUE). T_RaghavendranR1. Check for rv added.
	if(rv != SUCCESS)
	{
		rv = ERR_DEVICE_APP;
		return rv;
	}

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	/* Command to UI agent to send the captured signature to our application */
	getSignature(stUIReq.pszBuf);

	bWait = PAAS_TRUE;
	/* Send the batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while communicating with UI agent",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Signature Capture Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_SIGN_RESP)
				{
					switch(stBLData.iStatus)
					{
					case SUCCESS:
						pstSign = &(stBLData.stRespDtls.stSigInfo);

						*pszSignData = (uchar *) malloc(pstSign->iSigLen + 1);
						if(*pszSignData == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc FAILED",
														__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_SYSTEM;
							break;
						}

						*iLen = pstSign->iSigLen + 1;
						memset(*pszSignData, 0x00, *iLen);
						memcpy(*pszSignData, pstSign->szSigBuf, *iLen);

						/* VDR: TODO: Instead of allocating the memory and
						 * doing a memcpy, think of assigning the same pointer
						 * ahead */
						free(pstSign->szSigBuf);

						break;

					case ERR_DEVICE_APP:
					case FAILURE:
						debug_sprintf(szDbgMsg,"%s: FAILED to get Sign data",
												__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_DEVICE_APP;
						break;

					case UI_CANCEL_PRESSED:
						debug_sprintf(szDbgMsg,"%s: Signature capture CANCELLED"
										, __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = UI_CANCEL_PRESSED;
						break;

					default:
						debug_sprintf(szDbgMsg, "%s: Unknown", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_DEVICE_APP;
						break;
					}

					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showSignatureScreen
 *
 * Description	: Gets the signature of the user.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showSignatureScreen(char * szDisclaimer, uchar ** pszSignData, int * iLen, int iCmdType)
{
	int				rv					= SUCCESS;
#if 0
	int				startX 				= 0;
	int				startY 				= 0;
	int				endX   				= 0;
	int				endY   				= 0;
#endif
	int				iSaveMode			= 0;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTmp[100]			= "";
	char			szImageType[10]		= "";
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	SIGDATA_PTYPE	pstSign				= NULL;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Initialize the elements of the form */
	whichForm = SIG_CAP_FRM;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* --------- Set the Labels ----------- */

	/* Set the Title */
	fetchScrTitle(szTitle, ENTER_SIGN_TITLE);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_SIG_CAP_LBL_1;
	ctrlIds[1] 	= PAAS_SIG_CAP_LBL_4;
	ctrlIds[2] 	= 0;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);


	/* Set the disclaimer string */
	if((szDisclaimer == NULL) || (strlen(szDisclaimer) <= 0))
	{
		if( iCmdType == SSI_CREDIT)
		{
			getDisplayMsg(szTmp, MSG_REFUND_SIGN_DISCLAIM);
		}
		else
		{
			getDisplayMsg(szTmp, MSG_SIGN_DISCLAIM);
		}
		addTextboxTextWrapper(PAAS_SIG_CAP_TXT_1, szTmp, stUIReq.pszBuf);

		if (strlen(szTmp) >60)
		{
			setBoolValueWrapper(PAAS_SIG_CAP_TXT_1, PROP_BOOL_LISTBOX_NO_SCROLL, 0, stUIReq.pszBuf);
		}
	}
	else
	{
		addTextboxTextWrapper(PAAS_SIG_CAP_TXT_1, szDisclaimer, stUIReq.pszBuf);

		if (strlen(szDisclaimer) >60)
		{
			setBoolValueWrapper(PAAS_SIG_CAP_TXT_1, PROP_BOOL_LISTBOX_NO_SCROLL, 0, stUIReq.pszBuf);
		}
	}

	/*
	 * Praveen_P1: We are setting sigbox parameters at the start of the application
	 * So, we need not set the S04 command every time, once we use
	 * compatible FA, we can comment the following two functions
	 */
#if 0
	/*
	 * Praveen_P1: FA supports setting the sigcap area through config parameter
	 * 			   Setting SIGBOX at the start of the application, so
	 * 			   not setting it here
	 * 			   Please use FA ver 3.0.1-BUILD18 onwards
	 */
	getSigCapCoordinates(&startX, &startY, &endX, &endY);
	setSigCapBoxArea(startX, startY, endX, endY, 0, stUIReq.pszBuf);
#endif
	/* Set the signature parameters like image type, resolution, endianness etc
	 * which would be required by the UI agent to capture the signature
	 * effectively */
	getSigImageType(szImageType);
	if(strcmp(szImageType, SIGN_IMAGE_TYPE_TIFF) == 0)
	{
		/* Save as 200 DPI TIF File */
		iSaveMode = 2;
	}
	else if(strcmp(szImageType, SIGN_IMAGE_TYPE_BMP) == 0)
	{
		/* Save as Windows Bitmap File (BMP) */
		iSaveMode = 6;
	}
	else if(strcmp(szImageType, SIGN_IMAGE_TYPE_3BA) == 0)
	{
		/* Save as 3-Byte ASCII (3BA) */
		iSaveMode = 5;
	}
	else
	{
		/* Should not come here..incase if it comes */
		/* Save as 200 DPI TIF File */
		iSaveMode = 2;
	}

	setSigCapParams(0, 0, 0, B_ENDIAN, 100, iSaveMode, stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	/* Command to UI agent to send the captured signature to our application */
	getSignature(stUIReq.pszBuf);

	/* Send the batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while communicating with UI agent",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Signature Capture Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_SIGN_RESP)
				{
					switch(stBLData.iStatus)
					{
					case SUCCESS:
						pstSign = &(stBLData.stRespDtls.stSigInfo);

						*pszSignData = (uchar *) malloc(pstSign->iSigLen + 1);
						if(*pszSignData == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc FAILED",
														__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_SYSTEM;
							break;
						}

						*iLen = pstSign->iSigLen + 1;
						memset(*pszSignData, 0x00, *iLen);
						memcpy(*pszSignData, pstSign->szSigBuf, *iLen);

						/* VDR: TODO: Instead of allocating the memory and
						 * doing a memcpy, think of assigning the same pointer
						 * ahead */
						free(pstSign->szSigBuf);

						break;

					case ERR_DEVICE_APP:
					case FAILURE:
						debug_sprintf(szDbgMsg,"%s: FAILED to get Sign data",
												__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_DEVICE_APP;
						break;

					case UI_CANCEL_PRESSED:
						debug_sprintf(szDbgMsg,"%s: Signature capture CANCELLED"
										, __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = UI_CANCEL_PRESSED;
						break;

					default:
						debug_sprintf(szDbgMsg, "%s: Unknown", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_DEVICE_APP;
						break;
					}

					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: captureCustQuestion
 *
 * Description	: This API would be used to prompt the users for question
 *
 *
 * Input Params	: Data(to be filled)
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int captureCustQuestion(SURVEYDTLS_PTYPE pstSurveyDtls)
{
	int				rv					= SUCCESS;
	int				iCurPosition		= 0;
	int				iDisplayTextLen		= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTmp[1025]			= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	initUIReqMsg(&stUIReq, BATCH_REQ);

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	if(strlen(pstSurveyDtls->szDispBulkTxt) > 0)
	{
		/* Initialize the elements of the form */
		whichForm = CUST_QUES_FRM2;
		initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

		setBoolValueWrapper(PAAS_CUST_QUES_SCREEN_TXT_1, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);

		bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while communicating with UI agent",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
			return rv;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);
			}
		}

		iDisplayTextLen = strlen(pstSurveyDtls->szDispBulkTxt);
		while(iDisplayTextLen > 0)
		{
			initUIReqMsg(&stUIReq, BATCH_REQ);

			memset(szTmp, 0x00, sizeof(szTmp));
			if (iDisplayTextLen > 1024)
			{
				strncpy(szTmp, pstSurveyDtls->szDispBulkTxt+iCurPosition, 1024);
			}
			else
			{
				strcpy(szTmp, pstSurveyDtls->szDispBulkTxt+iCurPosition);
			}
			iDisplayTextLen -= 1024;
			iCurPosition    += 1024;

			addTextboxTextWrapper(PAAS_CUST_QUES_SCREEN_TXT_1, szTmp, stUIReq.pszBuf);

			bWait = PAAS_TRUE;
			rv = sendUIReqMsg(stUIReq.pszBuf);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while communicating with UI agent",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_DEVICE_APP;
				break;
			}
			else
			{
				/* Wait for the UI Agent resp */
				while(bWait == PAAS_TRUE)
				{
					CHECK_POS_INITIATED_STATE;
					if(bBLDataRcvd == PAAS_TRUE)
					{
						acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

						if(stBLData.uiRespType == UI_GENRL_RESP)
						{
							bBLDataRcvd = PAAS_FALSE;
							bWait = PAAS_FALSE;
						}

						releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
					}
					if(bWait == PAAS_FALSE)
					{
						break;
					}
					svcWait(15);
				}
			}
		}
		initUIReqMsg(&stUIReq, BATCH_REQ);

		if (strlen(pstSurveyDtls->szDispBulkTxt) > 380) //Praveen_P1: TO_DO Check how much will fit on 915
		{
			setBoolValueWrapper(PAAS_CUST_QUES_SCREEN_TXT_1, PROP_BOOL_LISTBOX_NO_SCROLL, 0, stUIReq.pszBuf);
		}
	}
	else
	{
		/* Initialize the elements of the form */
		whichForm = CUST_QUES_FRM1;
		initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

		if(strlen(pstSurveyDtls->szDispTxt1) > 0)
		{
			setStringValueWrapper(PAAS_CUST_QUES_SCREEN_LBL_1, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt1, stUIReq.pszBuf);
		}
		if(strlen(pstSurveyDtls->szDispTxt2) > 0)
		{
			setStringValueWrapper(PAAS_CUST_QUES_SCREEN_LBL_2, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt2, stUIReq.pszBuf);
		}
		if(strlen(pstSurveyDtls->szDispTxt3) > 0)
		{
			setStringValueWrapper(PAAS_CUST_QUES_SCREEN_LBL_3, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt3, stUIReq.pszBuf);
		}
		if(strlen(pstSurveyDtls->szDispTxt4) > 0)
		{
			setStringValueWrapper(PAAS_CUST_QUES_SCREEN_LBL_4, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt4, stUIReq.pszBuf);
		}
		if(strlen(pstSurveyDtls->szDispTxt5) > 0)
		{
			setStringValueWrapper(PAAS_CUST_QUES_SCREEN_LBL_5, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt5, stUIReq.pszBuf);
		}
		if(strlen(pstSurveyDtls->szDispTxt6) > 0)
		{
			setStringValueWrapper(PAAS_CUST_QUES_SCREEN_LBL_6, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt6, stUIReq.pszBuf);
		}
		if(strlen(pstSurveyDtls->szDispTxt7) > 0)
		{
			setStringValueWrapper(PAAS_CUST_QUES_SCREEN_LBL_7, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt7, stUIReq.pszBuf);
		}
		if(strlen(pstSurveyDtls->szDispTxt8) > 0)
		{
			setStringValueWrapper(PAAS_CUST_QUES_SCREEN_LBL_8, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt8, stUIReq.pszBuf);
		}
		if(strlen(pstSurveyDtls->szDispTxt9) > 0)
		{
			setStringValueWrapper(PAAS_CUST_QUES_SCREEN_LBL_9, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt9, stUIReq.pszBuf);
		}
		if(strlen(pstSurveyDtls->szDispTxt10) > 0)
		{
			setStringValueWrapper(PAAS_CUST_QUES_SCREEN_LBL_10, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt10, stUIReq.pszBuf);
		}
	}

	//CID 67373 (#1 of 1): Unused value (UNUSED_VALUE). T_RaghavendranR1. Below condition added.
	if(rv == SUCCESS)
	{
		/* -------------- Set the labels for the buttons ------------ */
		fetchBtnLabel(&stBtnLbl, BTN_LBLS_07);

		/* Set the label for the YES(ENTER/GREEN) key on the screen */
		setStringValueWrapper(PAAS_CUST_QUES_SCREEN_YES_LBL, PROP_STR_CAPTION,
				(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);

		/* Set the label for the NO(CANCEL/RED) key on the screen */
		setStringValueWrapper(PAAS_CUST_QUES_SCREEN_NO_LBL, PROP_STR_CAPTION,
				(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		bWait = PAAS_TRUE;

		bBLDataRcvd = PAAS_FALSE;//Praveen_P1: Setting to false before sending the request to make sure that we wait for the response which we are sending

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);
			}
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Showing Customer Question Capture Screen");
				addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
			}

			bWait = PAAS_TRUE;
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_XEVT_RESP)
					{
						pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
						if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
						{
							switch(pstXevt->uiCtrlID)
							{
							case PAAS_CUST_QUES_SCREEN_FK_NO:
								debug_sprintf(szDbgMsg, "%s: NO Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								strcpy(pstSurveyDtls->szResultData, "NO");
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Selected [NO] While Capturing Customer Question");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}
								rv = SUCCESS;
								break;

							case PAAS_CUST_QUES_SCREEN_FK_YES:
								debug_sprintf(szDbgMsg, "%s: YES Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								strcpy(pstSurveyDtls->szResultData, "YES");
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Selected [YES] While Capturing Customer Question");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}
								rv = SUCCESS;
								break;

							default:
								debug_sprintf(szDbgMsg, "%s: Unknown",__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
								break;
							}

							bBLDataRcvd = PAAS_FALSE;
							bWait = PAAS_FALSE;
						}
						/*
						 * Praveen_P1: Not waiting only if the response received from the correct form
						 * 			   so moved the below line inside the above if condition
						 */
						//bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: captureEmpID
 *
 * Description	: This API would be used to display the Cash Back Amount Entry
 * 					screen once the user chooses the Other option in Select
 * 					Cashback screen.
 *
 * Input Params	: None
 *
 * Output Params:
 * ============================================================================
 */
int captureEmpID(char * pszEmpID)
{
	int				rv					= SUCCESS;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	UIREQ_STYPE		stUIReq;
	int				iDisableFormFlag	= 0;
	unsigned shOptions = 0;
#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = NUM_ENTRY_FRM;

	iDisableFormFlag = 1;

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	//Getting the current options for the ctrlid of the form
	shOptions = getOptionsForCtrlid(PS_NUM_ENTRY_EDT_1, stUIReq.pszBuf );

	initUIReqMsg(&stUIReq, BATCH_REQ);

	//To make sure that right to left entry is disabled
	shOptions &= ~OPT_TB_RL_MASK;

	setShortValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_SHORT_OPTIONS, shOptions, stUIReq.pszBuf);

	//setShortValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_SHORT_MIN_ENTRIES, 10, stUIReq.pszBuf);
	/* Set the display string on the textbox */
	setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_DISPLAY_STRING, "                    ", stUIReq.pszBuf);
	/* Set the format string for the textbox */
	setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_FORMAT_STRING,  "NNNNNNNNNNNNNNNNNNNN", stUIReq.pszBuf);

	/* ---------------- Set the labels ----------------- */

	/* Set the Title */
	fetchScrTitle(szTitle, ENTER_EMP_ID_TITLE);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PS_NUM_ENTRY_LBL_1;
	ctrlIds[1] 	= PS_NUM_ENTRY_LBL_11;
	ctrlIds[2] 	= PS_NUM_ENTRY_LBL_12;

	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);

	/* Set the button labels */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_08);
	setStringValueWrapper(PS_NUM_ENTRY_LBL_4, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Set the label for the button - "Submit "*/
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_09);
	setStringValueWrapper(PS_NUM_ENTRY_LBL_3, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	/* Send the batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv == SUCCESS)
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Employee Id Capture Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == PS_NUM_ENTRY_EDT_1)
						{
							switch(pstXevt->uiKeypadEvt)
							{
							case 2: /* ON ENTER */
								debug_sprintf(szDbgMsg, "%s: Value = [%s]",
											__FUNCTION__, pstXevt->szKpdVal);
								APP_TRACE(szDbgMsg);

								strcpy(pszEmpID, pstXevt->szKpdVal);

								rv = UI_ENTER_PRESSED;
								break;

							case 3: /* ON CANCEL */
								debug_sprintf(szDbgMsg, "%s: CANCEL pressed",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = UI_CANCEL_PRESSED;
								break;

							default:
								debug_sprintf(szDbgMsg, "%s: Invalid Kpd event",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
								break;
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Invalid control Id",
												__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: captureCharity
 *
 * Description	: This API would be used to prompt the users for taking charity
 *
 *
 * Input Params	: Data(to be filled)
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int captureCharity(CHARITYDTLS_PTYPE pstCharityDtls)
{
	int				rv					= SUCCESS;
	int				iSelectedOption 	= 0;
	int				iDisableFormFlag	= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	/*
	 * Setting it to False so that will not take any
	 * available BL data before showing this form
	 * This is to FIX the issue i.e. when multiple times
	 * buttons are pressed on this screen, for the next same command
	 * last press from the previous screen is taken since
	 * BL data is available
	 */
	bBLDataRcvd = PAAS_FALSE;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	/* Initialize the elements of the form */
	whichForm = CHARITY_FRM;
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	if(strlen(pstCharityDtls->szDispTxt1) > 0)
	{
		setStringValueWrapper(PAAS_CHARITY_SCREEN_LBL_1, PROP_STR_CAPTION, pstCharityDtls->szDispTxt1, stUIReq.pszBuf);
	}
	if(strlen(pstCharityDtls->szDispTxt2) > 0)
	{
		setStringValueWrapper(PAAS_CHARITY_SCREEN_LBL_2, PROP_STR_CAPTION, pstCharityDtls->szDispTxt2, stUIReq.pszBuf);
	}
	if(strlen(pstCharityDtls->szDispTxt3) > 0)
	{
		setStringValueWrapper(PAAS_CHARITY_SCREEN_LBL_3, PROP_STR_CAPTION, pstCharityDtls->szDispTxt3, stUIReq.pszBuf);
	}
	if(strlen(pstCharityDtls->szDispTxt4) > 0)
	{
		setStringValueWrapper(PAAS_CHARITY_SCREEN_LBL_4, PROP_STR_CAPTION, pstCharityDtls->szDispTxt4, stUIReq.pszBuf);
	}
	if(strlen(pstCharityDtls->szDispTxt5) > 0)
	{
		setStringValueWrapper(PAAS_CHARITY_SCREEN_LBL_5, PROP_STR_CAPTION, pstCharityDtls->szDispTxt5, stUIReq.pszBuf);
	}

	if(strlen(pstCharityDtls->szAmount1) > 0)
	{
		setBoolValueWrapper(PAAS_CHARITY_SCREEN_OPT_1, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_CHARITY_SCREEN_OPT_1, PROP_STR_CAPTION, pstCharityDtls->szAmount1, stUIReq.pszBuf);
	}
	if(strlen(pstCharityDtls->szAmount2) > 0)
	{
		setBoolValueWrapper(PAAS_CHARITY_SCREEN_OPT_2, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_CHARITY_SCREEN_OPT_2, PROP_STR_CAPTION, pstCharityDtls->szAmount2, stUIReq.pszBuf);
	}
	if(strlen(pstCharityDtls->szAmount3) > 0)
	{
		setBoolValueWrapper(PAAS_CHARITY_SCREEN_OPT_3, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_CHARITY_SCREEN_OPT_3, PROP_STR_CAPTION, pstCharityDtls->szAmount3, stUIReq.pszBuf);
	}
	if(strlen(pstCharityDtls->szAmount4) > 0)
	{
		setBoolValueWrapper(PAAS_CHARITY_SCREEN_OPT_4, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_CHARITY_SCREEN_OPT_4, PROP_STR_CAPTION, pstCharityDtls->szAmount4, stUIReq.pszBuf);
	}
	if(strlen(pstCharityDtls->szAmount5) > 0)
	{
		setBoolValueWrapper(PAAS_CHARITY_SCREEN_OPT_5, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_CHARITY_SCREEN_OPT_5, PROP_STR_CAPTION, pstCharityDtls->szAmount5, stUIReq.pszBuf);
	}


	/* Set the label for the button - "Skip "*/
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_11);
	setStringValueWrapper(PAAS_CHARITY_SCREEN_SKIP_LBL, PROP_STR_CAPTION, (char *)stBtnLbl.szLblOne, stUIReq.pszBuf);

	/* Set the label for the button - "Submit "*/
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_09);
	setStringValueWrapper(PAAS_CHARITY_SCREEN_SUBMIT_LBL, PROP_STR_CAPTION, (char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Charity Donation Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_CHARITY_SCREEN_FK_SKIP:
							debug_sprintf(szDbgMsg, "%s: SKIP Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							strcpy(pstCharityDtls->szResultData, "0");
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "User Skipped While Capturing Charity Details");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							rv = SUCCESS;
							bWait = PAAS_FALSE;
							break;

						case PAAS_CHARITY_SCREEN_FK_SUBMIT:
							debug_sprintf(szDbgMsg, "%s: Submit Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							if(iSelectedOption != 0)
							{
								debug_sprintf(szDbgMsg, "%s: Selected Option is %d", __FUNCTION__, iSelectedOption);
								APP_TRACE(szDbgMsg);
								if(iAppLogEnabled == 1)
								{
									sprintf(szAppLogData, "Option[%d] Selected By User While Capturing Charity Details", iSelectedOption);
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								sprintf(pstCharityDtls->szResultData, "%d", iSelectedOption);
								bWait = PAAS_FALSE;
								rv = SUCCESS;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: No option is selected, will remain on the same form", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}
							break;

						case PAAS_CHARITY_SCREEN_OPT_1:
							debug_sprintf(szDbgMsg, "%s: Option 1 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 1;
							break;
						case PAAS_CHARITY_SCREEN_OPT_2:
							debug_sprintf(szDbgMsg, "%s: Option 2 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 2;
							break;
						case PAAS_CHARITY_SCREEN_OPT_3:
							debug_sprintf(szDbgMsg, "%s: Option 3 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 3;
							break;
						case PAAS_CHARITY_SCREEN_OPT_4:
							debug_sprintf(szDbgMsg, "%s: Option 4 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 4;
							break;
						case PAAS_CHARITY_SCREEN_OPT_5:
							debug_sprintf(szDbgMsg, "%s: Option 5 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 5;
							break;
						default:
							debug_sprintf(szDbgMsg, "%s: Unknown",__FUNCTION__);
							APP_TRACE(szDbgMsg);
							bWait = PAAS_FALSE;
							rv = ERR_DEVICE_APP;
							break;
						}

						bBLDataRcvd = PAAS_FALSE;
					}
					/*
					 * Praveen_P1: Not waiting only if the response received from the correct form
					 * 			   so moved the below line inside the above if condition
					 */
					//bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: captureCardDataForDevCmd
 *
 * Description	: This API would be used to prompt the users for taking charity
 *
 *
 * Input Params	: Data(to be filled)
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int captureCardDataForDevCmd(char * szTranStkKey, char* szStatMsg, GET_CARDDATA_PTYPE pstGetCardDataDtls)
{
	int				rv						= SUCCESS;
	int				iCrdSrc					= 0;
	int 			iLen					= 0;
    int				iDiffTemp				= 0;
	int				iDisableFormFlag		= 0;
	int				iAppLogEnabled			= isAppLogEnabled();
//	float			transAmt				= 0.00;
	char			szAppLogData[256]		= "";
	char *			cPtr					= NULL;
	char *			pszTemp					= NULL;
	char *			pszCurPtr				= NULL;
	char *			pszNxtPtr				= NULL;
	PAAS_BOOL		bSwipe					= PAAS_FALSE;
	PAAS_BOOL		bTAP					= PAAS_FALSE;
	PAAS_BOOL		bInsert					= PAAS_FALSE;
	PAAS_BOOL		bEmvEnabled				= PAAS_TRUE;
	PAAS_BOOL		bContactlessEmvEnabled 	= PAAS_FALSE;
	PAAS_BOOL		bContactlessEnabled		= PAAS_FALSE;
	PAAS_BOOL		bWaitForFirstC3108		= PAAS_FALSE;
	PAAS_BOOL		bWait					= PAAS_TRUE;
	PAAS_BOOL		bSTBDataReceived		= PAAS_FALSE;
	PAAS_BOOL		bCardDataReceived		= PAAS_FALSE;
	PAAS_BOOL		bFallBackFlag			= PAAS_FALSE;
	PAAS_BOOL		bI02Flag					= PAAS_FALSE;
	XEVT_PTYPE		pstXevt					= NULL;
	CARD_TRK_PTYPE 	pstCard;
	EMVDTLS_STYPE	stEmvDtls;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter---", __FUNCTION__);
	APP_TRACE(szDbgMsg);


	while(1)
	{
		/* Validate the parameters */
		if((szTranStkKey == NULL) || (szStatMsg == NULL) || pstGetCardDataDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;	// T_RaghavendranR1 CID 83343 (#1 of 1): Dereference after null check (FORWARD_NULL) Moved the above NULL NULL inside while loop and break added
		}
		memset(szAppLogData, 0x00, sizeof(szAppLogData));

		/* disabiling the Card Readers when we are in the Pre-swipe screen as well*/
		disableCardReaders();

		bContactlessEmvEnabled = isContactlessEmvEnabledInDevice();
		bContactlessEnabled = isContactlessEnabledInDevice();
		bEmvEnabled = isEmvEnabledInDevice();
		/*
		 * Setting it to False so that will not take any
		 * available BL data before showing this form
		 * This is to FIX the issue i.e. when multiple times
		 * buttons are pressed on this screen, for the next same command
		 * last press from the previous screen is taken since
		 * BL data is available
		 */
		bBLDataRcvd = PAAS_FALSE;

		initUIReqMsg(&stUIReq, BATCH_REQ);

		if(strlen(pstGetCardDataDtls->szInputMethod) == 0)
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Missing Input Method");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				rv = ERR_FLD_REQD;
				return rv;
			}
		}

		cPtr= strtok(pstGetCardDataDtls->szInputMethod, "|");
		while(cPtr != NULL)
		{
			if(strcmp(cPtr,"SWIPE") == 0)
			{
				bSwipe = PAAS_TRUE;
			}
			else if(strcmp(cPtr,"TAP") == 0)
			{
				bTAP = PAAS_TRUE;
			}
			else if(strcmp(cPtr,"INSERT") == 0)
			{
				bInsert = PAAS_TRUE;
			}
			else
			{
				rv = ERR_INV_FLD;
				return rv;
				break;
				//give some error
			}
			cPtr = strtok(NULL, "|");
		}

		if((!bEmvEnabled && bInsert) ||(!bContactlessEnabled && bTAP))
		{
			debug_sprintf(szDbgMsg, "%s: ---Not Accepted Form ---", __FUNCTION__ );
			APP_TRACE(szDbgMsg);
			rv = ERR_NOT_ACCEPTED_CONFIG;
			return rv;
		}
		else
		{
			if(bTAP && bSwipe && bInsert)		//C30
			{
				whichForm = SWIPE_STI_FRM;
			}
			else if(bTAP && bSwipe)			//S20
			{
				whichForm = SWIPE_ST_FRM;
			}
			else if(bInsert && bSwipe)			//C30
			{
				whichForm = SWIPE_SI_FRM;
			}
			else if(bSwipe)
			{
				whichForm = SWIPE_S_FRM;
			}
			else if(bTAP)						// S20 or C30
			{
				whichForm = SWIPE_T_FRM;
			}
			else if(bInsert)
			{
				whichForm = SWIPE_SI_FRM;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: ---Not Accepted Form ---", __FUNCTION__ );
				APP_TRACE(szDbgMsg);
				rv = ERR_NOT_ACCEPTED_CONFIG;
				return rv;
			}
		}

		if(bTAP && bContactlessEmvEnabled)
		{
			pstGetCardDataDtls->tranAmt = PAAS_TRUE;
		}
		initUIReqMsg(&stUIReq, BATCH_REQ);
		initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

		if(strlen(pstGetCardDataDtls->szDispTxt1))
		{
			setStringValueWrapper(PAAS_SWIPE_LBL_10, PROP_STR_CAPTION, pstGetCardDataDtls->szDispTxt1, stUIReq.pszBuf);
		}
		if(strlen(pstGetCardDataDtls->szDispTxt2))
		{
			setStringValueWrapper(PAAS_SWIPE_LBL_8, PROP_STR_CAPTION, pstGetCardDataDtls->szDispTxt2, stUIReq.pszBuf);
		}
		if(strlen(pstGetCardDataDtls->szDispTxt3))
		{
			setStringValueWrapper(PAAS_SWIPE_LBL_9, PROP_STR_CAPTION, pstGetCardDataDtls->szDispTxt3, stUIReq.pszBuf);
		}

		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;

			break; //Praveen_P1: Fix for Coverity CID# 83368
		}
		else
		{
			if(iAppLogEnabled == 1)
			{
				if(whichForm == SWIPE_STI_FRM)
				{
					strcpy(szAppLogData, "Showing Swipe/Tap/Insert Card Screen");
				}
				else if(whichForm == SWIPE_SI_FRM)
				{
					strcpy(szAppLogData, "Showing Swipe/Insert Card Screen");
				}
				else if(whichForm == SWIPE_ST_FRM)
				{
					strcpy(szAppLogData, "Showing Swipe/Tap Card Screen");
				}
				else if(whichForm == SWIPE_S_FRM)
				{
					strcpy(szAppLogData, "Showing Swipe Card Screen");
				}
				else if(whichForm == SWIPE_T_FRM)
				{
					strcpy(szAppLogData, "Showing Tap Card Screen");
				}
				else
				{
					strcpy(szAppLogData, "Showing Card Read Screen");
				}
				addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
			}
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(5);
			}
			bWait = PAAS_TRUE;
		}

		rv = updateGetCardDataDtlsForDevTran(szTranStkKey, pstGetCardDataDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to update Card Data dtls", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Update Card Data Details");
				addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		if(bInsert)
		{
			rv = sendC30CmdToGetChipCardData(szTranStkKey, NULL, NULL, NULL);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to send XPI C30 cmd",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				return ERR_DEVICE_APP;
			}
		}
		else
		{
			//Swipe , tap
			rv = sendS20CmdToGetMSRCardData(NULL, NULL, NULL);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to send XPI S20 cmd to enable MSR",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				return ERR_DEVICE_APP;
			}
		}

		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == PAAS_SWIPE_FK_1)
						{
							debug_sprintf(szDbgMsg, "%s: CANCEL", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "User Pressed Cancel Button on card read Screen");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							rv = UI_CANCEL_PRESSED;
							bWait = PAAS_FALSE;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					/* Disabling the card swipe on the UI agent. */
					disableCardReaders();
				}
				else if(stBLData.uiRespType == UI_CARD_RESP)
				{
					if(stBLData.iStatus == SUCCESS)
					{
						pstCard = &(stBLData.stRespDtls.stCrdTrkInfo);

						/* Track 1 --- */
						iLen = strlen(pstCard->szTrk1);
						if(iLen > 0)
						{
							memset(pstGetCardDataDtls->szTrack1Data, 0x00 ,sizeof(pstGetCardDataDtls->szTrack1Data));
							memcpy(pstGetCardDataDtls->szTrack1Data, pstCard->szTrk1, iLen);
							pszCurPtr = pstCard->szTrk1;
							pszCurPtr++;
							pszNxtPtr = strchr(pszCurPtr, '^');
							if(pszNxtPtr == NULL)
							{
								/* FIXME: Currently done under assumption that some gift cards dont
								 * have the '^' sentinel in their track 1 information */
								pszCurPtr = pstCard->szTrk1;
								pszCurPtr++;
								strcpy(pstGetCardDataDtls->szPAN, pszCurPtr);
							}
							else
							{
								memset(pstGetCardDataDtls->szPAN, 0x00 ,sizeof(pstGetCardDataDtls->szPAN));
								memcpy(pstGetCardDataDtls->szPAN, pszCurPtr, pszNxtPtr - pszCurPtr);
							}
						}
						/* Track 2 --- */
						iLen = strlen(pstCard->szTrk2);
						if(iLen > 0)
						{
							memset(pstGetCardDataDtls->szTrack2Data, 0x00 ,sizeof(pstGetCardDataDtls->szTrack2Data));
							memcpy(pstGetCardDataDtls->szTrack2Data, pstCard->szTrk2, iLen);
							pszNxtPtr = strchr(pstCard->szTrk2, '=');
							if(pszNxtPtr == NULL)
							{
								/* FIXME: Currently done under assumption that some gift cards dont
								 * have the '=' sentinel in their track 2 */
								if(strlen(pstGetCardDataDtls->szPAN) <= 0)
								{
									strncpy(pstGetCardDataDtls->szPAN, pstCard->szTrk2, (sizeof(pstCard->szTrk2) - 1));
								}
							}
							else if(strlen(pstGetCardDataDtls->szPAN) <= 0)				// if The PAN is Already Got in Track 1. it is not need to Fill again
							{
								memset(pstGetCardDataDtls->szPAN, 0x00 ,sizeof(pstGetCardDataDtls->szPAN));
								memcpy(pstGetCardDataDtls->szPAN, pstCard->szTrk2, pszNxtPtr - (pstCard->szTrk2));
							}
						}
						/* Track 3 --- */
						iLen = strlen(pstCard->szTrk3);
						if(iLen > 0)
						{
							memset(pstGetCardDataDtls->szTrack3Data, 0x00 ,sizeof(pstGetCardDataDtls->szTrack3Data));
							memcpy(pstGetCardDataDtls->szTrack3Data, pstCard->szTrk3, iLen);
							pszNxtPtr = strchr(pstCard->szTrk3, '=');
							if(pszNxtPtr == NULL)
							{
								/* FIXME: Currently done under assumption that some gift cards dont
								 * have the '=' sentinel in their track 3 information */
								if(strlen(pstGetCardDataDtls->szPAN) <= 0)
								{
									strncpy(pstGetCardDataDtls->szPAN, pstCard->szTrk3, (sizeof(pstCard->szTrk3) - 1));
								}
							}
							else if(strlen(pstGetCardDataDtls->szPAN) <= 0)									// if The PAN is Already Got in Track 1 /Track 2. it is not need to Fill again
							{
								memset(pstGetCardDataDtls->szPAN, 0x00 ,sizeof(pstGetCardDataDtls->szPAN));
								memcpy(pstGetCardDataDtls->szPAN, pstCard->szTrk3, pszNxtPtr - (pstCard->szTrk3));
							}
						}

						/* Copy Clear Track data if present. Added for VSD support*/
						if(strlen(pstCard->szClrTrk2))
						{
							memcpy(pstGetCardDataDtls->szClrTrk2, pstCard->szClrTrk2, sizeof(pstCard->szClrTrk2));
						}
						if(strlen(pstCard->szClrTrk1))
						{
							memcpy(pstGetCardDataDtls->szClrTrk1, pstCard->szClrTrk1, sizeof(pstCard->szClrTrk1));
						}

						/* Expiry Year and Month*/
						if(strlen(pstCard->szClrExpDt) != '\0') // copy clear expiry month
						{
							memcpy(pstGetCardDataDtls->szClrYear, pstCard->szClrExpDt, 2);
							memcpy(pstGetCardDataDtls->szClrMon, pstCard->szClrExpDt + 2, 2);
						}
						else
						{
							if(strlen(pstCard->szTrk2) != '\0')
							{
								pszCurPtr = strchr(pstCard->szTrk2, '=');
								if(pszCurPtr == NULL)
								{
									debug_sprintf(szDbgMsg, "%s:Expiry Year and Date Not Present in Track 2", __FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
								else
								{
									pszCurPtr++;
									memcpy(pstGetCardDataDtls->szClrYear, pszCurPtr, 2);
									memcpy(pstGetCardDataDtls->szClrMon, pszCurPtr + 2, 2);
									debug_sprintf(szDbgMsg, "%s:Expiry Year and Date Captured Track 2", __FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
							}
							else if(strlen(pstCard->szTrk1) != '\0')
							{
								pszCurPtr = strchr(pstCard->szTrk1, '^');
								if(pszCurPtr == NULL)
								{
									debug_sprintf(szDbgMsg, "%s:Expiry Year and Date Not Present in Track 1", __FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
								else
								{
									pszCurPtr++;
									pszNxtPtr = strchr(pszCurPtr, '^');
									pszNxtPtr++;
									memcpy(pstGetCardDataDtls->szClrYear, pszNxtPtr, 2);
									memcpy(pstGetCardDataDtls->szClrMon, pszNxtPtr + 2, 2);
									debug_sprintf(szDbgMsg, "%s:Expiry Year and Date Captured in Track 1", __FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
							}
						}
						/*CardHolder Name*/
						iLen=strlen(pstGetCardDataDtls->szTrack1Data);
						if(iLen > 0)
						{
							pszCurPtr = strchr(pstGetCardDataDtls->szTrack1Data,'^');
							if(pszCurPtr == NULL)
							{
								debug_sprintf(szDbgMsg, "%s:Invalid CardHolder Name", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}
							else
							{
								pszCurPtr ++;
								pszNxtPtr = strchr(pszCurPtr,'^');
								if(pszNxtPtr == NULL)
								{
									debug_sprintf(szDbgMsg, "%s:Invalid CardHolder Name", __FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
								else
								{
									iDiffTemp = pszNxtPtr - pszCurPtr;
									memcpy(pstGetCardDataDtls->szName,pszCurPtr, iDiffTemp);
								}
							}
						}

						/*card source*/
						iCrdSrc = pstCard->iCardSrc;
						pstGetCardDataDtls->iCardEntryMode = iCrdSrc;

						rv = getCardDtlsForDevCmd(pstGetCardDataDtls);
						if(rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: FAILED to get Card dtls for Device command",__FUNCTION__);
							APP_TRACE(szDbgMsg);
							rv = FAILURE;

						}
					}
					else if(stBLData.iStatus == UI_CANCEL_PRESSED)
					{
						debug_sprintf(szDbgMsg, "%s: UI Cancel Pressed",
								__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = UI_CANCEL_PRESSED;
					}
					else if(stBLData.iStatus == ERR_BAD_CARD)
					{
						debug_sprintf(szDbgMsg, "%s: Max bad card reads done",
								__FUNCTION__);
						APP_TRACE(szDbgMsg);
						strcpy(szAppLogData, "Bad Card Read from User");

						rv = ERR_BAD_CARD;

					}
					else if(stBLData.iStatus == ERR_USR_TIMEOUT)
					{
						debug_sprintf(szDbgMsg, "%s: User Timed Out during Card Swipe", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						strcpy(szAppLogData, "User Timed Out During Card Swipe");

						rv = ERR_USR_TIMEOUT;

					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Failed ", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
					}

					bWait = PAAS_FALSE;
					bBLDataRcvd = PAAS_FALSE;
				}
				else if (stBLData.uiRespType == UI_EMV_RESP)
				{
					if( strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd,EMV_U00_REQ) ==0 || strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_U02_REQ) ==0 )
					{
						if(stBLData.iStatus == SUCCESS)
						{
							//DAIVIK: When we receive U02 - this indicates card was inserted, so if we have set bWaitForFirstC3108, then we can unset it.
							bWaitForFirstC3108 = PAAS_FALSE;
							debug_sprintf(szDbgMsg, "%s: Received U02 EMV Response",__FUNCTION__);
							APP_TRACE(szDbgMsg);

							if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bEMVRetry == PAAS_TRUE)
							{
								/* Initialize the screen */
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "Error While Inserting/Swiping/Tapping card");
									addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, NULL);
								}
								initUIReqMsg(&stUIReq, BATCH_REQ);

								if(getXPILibMode())
								{
									// 10-Dec-15: MukeshS3: Need to intialize the FORM & set all the properties again to show the FORM.
									// when XPI runs as library, because XPI would have painted his FORM at last.


									initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);	//T_MukeshS3

									setStringValueWrapper(PAAS_SWIPE_LBL_10, PROP_STR_CAPTION, pstGetCardDataDtls->szDispTxt1, stUIReq.pszBuf);
									setStringValueWrapper(PAAS_SWIPE_LBL_8, PROP_STR_CAPTION, pstGetCardDataDtls->szDispTxt2, stUIReq.pszBuf);
									setStringValueWrapper(PAAS_SWIPE_LBL_9, PROP_STR_CAPTION, pstGetCardDataDtls->szDispTxt3, stUIReq.pszBuf);

									/*Setting the screen title to show amount due labels*/
									//setScreenTitle(szTitle, szAmtTitle, ctrlIds, 2, giLabelLen);			//T_MukeshS3
								}

								showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

								if(sendUIReqMsg(stUIReq.pszBuf) != SUCCESS)
								{
									debug_sprintf(szDbgMsg, "%s: Error while communicating with UI Agent", __FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
							}
							else if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bCardInserted == PAAS_TRUE)
							{

								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "EMV Card Inserted");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}
								bBLDataRcvd = PAAS_FALSE;

								releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
							}
							else if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bSTBDataReceived == PAAS_TRUE)
							{
								debug_sprintf(szDbgMsg, "%s: Received STB data",__FUNCTION__);
								APP_TRACE(szDbgMsg);

								if(strlen(stBLData.stRespDtls.stEmvDtls.szSpinBinData) > 0)
								{
									pszTemp = (char *) malloc(sizeof(char) * (strlen(stBLData.stRespDtls.stEmvDtls.szSpinBinData) + 1));
									if(pszTemp == NULL)
									{
										debug_sprintf(szDbgMsg, "%s: Error while allocating memory", __FUNCTION__);
										APP_TRACE(szDbgMsg);
										rv = FAILURE;
										break;
									}
									memset(pszTemp, 0x00, (strlen(stBLData.stRespDtls.stEmvDtls.szSpinBinData) + 1));

									memcpy(pszTemp, stBLData.stRespDtls.stEmvDtls.szSpinBinData, strlen(stBLData.stRespDtls.stEmvDtls.szSpinBinData));

									/* Assisnging the Data pointer, please make sure that it is freed in the caller function*/
									//*szSBTData = pszTemp;
								}

								bSTBDataReceived = PAAS_TRUE;

								if(bCardDataReceived == PAAS_TRUE) //If we have received the card data then we need not wait
								{
									bWait = PAAS_FALSE;
								}
							}
							bBLDataRcvd = PAAS_FALSE;
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Got BL Data Status = %d.",__FUNCTION__, stBLData.iStatus);
							APP_TRACE(szDbgMsg);
						}
					}
					else
					{
						memset(&stEmvDtls, 0x00, sizeof(EMVDTLS_STYPE));
						memcpy(&stEmvDtls, &(stBLData.stRespDtls.stEmvDtls), sizeof(EMVDTLS_STYPE));

						debug_sprintf(szDbgMsg, "%s: Got EMV Command Response = %s",__FUNCTION__, stEmvDtls.szEMVRespCode);
						APP_TRACE(szDbgMsg);

						if(atoi(stEmvDtls.szEMVRespCode) == EMV_ERROR_TXN_CANCELLED && bWaitForFirstC3108)
						{
							/* KranthiK1:
							 * Setting it false as it would be referring to the previous C30 which got cancelled
							 * because a new C30 is sent with additional parameters (Amount) and no card read was
							 * done on the previuos C30
							 */
							bBLDataRcvd = PAAS_FALSE;

							bWaitForFirstC3108 = PAAS_FALSE;
							/* Setting the card Read back to true as the C3108 received refers to the first C30
							 * We need to Wait for the second C30 response
							 */
							setCardReadEnabled(PAAS_TRUE);
						}
						else
						{
							rv = getUIRespCodeOfEmvCmdResp(EMV_C30, (atoi(stEmvDtls.szEMVRespCode)));
							if(rv == SUCCESS && szTranStkKey != NULL)
							{
								//updateEmvDtlsinCardDtlsForPymtTran(szTranStkKey, &stEmvDtls);
								iCrdSrc = stEmvDtls.iCardSrc;
								//debug_sprintf(szDbgMsg, "%s: EMV Details Stored with Card source: %d",__FUNCTION__, iCrdSrc);
								//APP_TRACE(szDbgMsg);
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "EMV Card was Inserted and Read Successfully");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}
							}
							else if(rv == UI_CANCEL_PRESSED)
							{
								/* In Application selection Scree if User Press Cancel */
								debug_sprintf(szDbgMsg, "%s: Got C3108 , User Pressed Cancel",__FUNCTION__);
								APP_TRACE(szDbgMsg);
							}
							else
							{
								//Setting CRD_EMV_CT as card response to differentiate b/w error for EMV card and MSD card
								iCrdSrc = CRD_EMV_CT;
							}

							bBLDataRcvd = PAAS_FALSE;
							bCardDataReceived = PAAS_TRUE;
							/*
							 * If STB is enabled, then we need to wait for the XEVT data
							 * thats why not setting bwait to false here
							 */
							if(!isSTBLogicEnabled())
							{
								bWait = PAAS_FALSE;
							}
							else
							{
								/*
								 * If STB is enabled..we have to wait only if we have received the card data, thats why checking rv value
								 * If we have received the STB data already then we need not wait
								 */
								if(rv != SUCCESS || bSTBDataReceived == PAAS_TRUE)
								{
									bWait = PAAS_FALSE;
								}
							}
							if(rv == EMV_FALLBACK_TO_MSR || rv == EMV_ERROR_CANDIDATE_LISTEMPTY)
							{
								bFallBackFlag = PAAS_TRUE;
							}
							if(rv ==  ERR_BAD_CARD)
							{
								releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
								bBLDataRcvd = PAAS_FALSE;
								bWait = PAAS_FALSE;
								break;

							}
						}
					}

					if(rv == SUCCESS && bCardDataReceived == PAAS_TRUE)
					{
						/* --Track Data --- */
						iLen = strlen(stEmvDtls.szTrackData);
						if(iLen > 0)
						{
							memset(pstGetCardDataDtls->szTrack2Data, 0x00 ,sizeof(pstGetCardDataDtls->szTrack2Data));
							memcpy(pstGetCardDataDtls->szTrack2Data, stEmvDtls.szTrackData, iLen);
							pszNxtPtr = strchr(stEmvDtls.szTrackData, '=');
							if(pszNxtPtr == NULL)
							{
								/* FIXME: Currently done under assumption that some gift cards dont
								 * have the '=' sentinel in their track 2 */
								strncpy(pstGetCardDataDtls->szPAN, stEmvDtls.szTrackData, (sizeof(stEmvDtls.szTrackData) - 1));
							}
							else
							{
								memset(pstGetCardDataDtls->szPAN, 0x00 ,sizeof(pstGetCardDataDtls->szPAN));
								memcpy(pstGetCardDataDtls->szPAN, stEmvDtls.szTrackData, pszNxtPtr - (stEmvDtls.szTrackData));
							}
						}

						/* Copy Clear Track data if present. Added for VSD support*/
						if(strlen(stEmvDtls.szClrTrk2))
						{
							memcpy(pstGetCardDataDtls->szClrTrk2, stEmvDtls.szClrTrk2, sizeof(stEmvDtls.szClrTrk2));
						}
						if(strlen(stEmvDtls.szClrTrk1))
						{
							memcpy(pstGetCardDataDtls->szClrTrk1, stEmvDtls.szClrTrk1, sizeof(stEmvDtls.szClrTrk1));
						}

						/*card source*/
						pstGetCardDataDtls->iCardEntryMode = iCrdSrc;

						/*card holder name*/
						iLen =  strlen(stEmvDtls.szName);
						memcpy(pstGetCardDataDtls->szName, stEmvDtls.szName, iLen);

						/*expiry Date*/
						//iLen = strlen(stEmvDtls.stEmvAppDtls.szClearAppExpiryDate); //Praveen_P1: Coverity CID# 83352 fix
						memcpy(pstGetCardDataDtls->szClrYear, stEmvDtls.stEmvAppDtls.szClearAppExpiryDate, 2);
						memcpy(pstGetCardDataDtls->szClrMon, stEmvDtls.stEmvAppDtls.szClearAppExpiryDate + 2, 2);
						rv = getCardDtlsForDevCmd(pstGetCardDataDtls);
						if(rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: FAILED to get Card dtls for Device command",__FUNCTION__);
							APP_TRACE(szDbgMsg);
							rv = FAILURE;
						}
					}
				}
				else if(stBLData.iStatus == ERR_BAD_CARD)
				{
					debug_sprintf(szDbgMsg, "%s: Max bad card reads done",
							__FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(szAppLogData, "Bad Card Read from User");
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;

					rv = ERR_BAD_CARD;
				}
				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stEmvDtls.iCardSrc == CRD_EMV_CT || rv == UI_CANCEL_PRESSED)
				{
					checkCrdPrsnceAndShwMsgToRemove();
				}

				if(bFallBackFlag == PAAS_TRUE)
				{
					bFallBackFlag = PAAS_FALSE;
					checkCrdPrsnceAndShwMsgToRemove();
					bI02Flag = PAAS_TRUE;
				}
			}
			else if(stBLData.iStatus == ERR_BAD_CARD)
			{
				debug_sprintf(szDbgMsg, "%s: Max bad card reads done", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				bBLDataRcvd = PAAS_FALSE;
				bWait = PAAS_FALSE;

				rv = ERR_BAD_CARD;
			}
			svcWait(10);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
		if(bI02Flag == PAAS_TRUE)
		{
			bI02Flag = PAAS_FALSE;
			continue;
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;

}


/*
 * ============================================================================
 * Function Name: captureCreditAppDetails
 *
 * Description	: This API would be used to prompt the users for taking credit application request by throwing
 * various prompts available in the POS requests
 *
 *
 * Input Params	: Data(to be filled)
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int captureCreditAppDetails(PROMPTDTLS_PTYPE pstCreditAppPrompt, int iPromptNo)
{
	int				rv					= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCreditAppPrompt == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Parameters Passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;

		return rv;
	}

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	debug_sprintf(szDbgMsg, "%s: pstCreditAppPrompt->iPromptType [%d]", __FUNCTION__, pstCreditAppPrompt->iPromptType);
	APP_TRACE(szDbgMsg);

	pstCreditAppPrompt->iTitle = -1;
	if((pstCreditAppPrompt->iPromptType == PROMPTTYPE_NUMERICS) || (pstCreditAppPrompt->iPromptType == PROMPTTYPE_DOLLAR))
	{
		rv = getCreditAppNumericDataFromUser(pstCreditAppPrompt, iPromptNo); //Open Numeric KeyPad Entry Form
	}
	else if(pstCreditAppPrompt->iPromptType == PROMPTTYPE_QWERTY_SWIPE)
	{
		rv = getCreditAppSwipeDataFromUser(pstCreditAppPrompt); //Open QWERTY KeyPad Entry Form and Enable Swipe to Capture Card Holder Name
	}
	else
	{
		rv = getCreditAppStringFromUser(pstCreditAppPrompt); //Open QWERTY KeyPad Entry Form
	}

	debug_sprintf(szDbgMsg, "%s: Returning rv [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCardHolderNameFrmTrackData
 *
 * Description	: This API would be used to get the Card Holder Name From the Track 1 Data Passed
 *
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCardHolderNameFrmTrackData(char* pszTrackData, char* pszCardHolderName)
{
	int				rv			=	SUCCESS;
	char *			cCurPtr		= NULL;
	char *			cNxtPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif


	cCurPtr = pszTrackData;
	cNxtPtr = strchr(cCurPtr, '^');
	if(cNxtPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid track; no PAN", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_CARD_INVALID;
	}
	else
	{
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, '^');
		if(cNxtPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid track; no Name", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_CARD_INVALID;
		}
		else
		{
			memcpy(pszCardHolderName, cCurPtr, cNxtPtr - cCurPtr);

			debug_sprintf(szDbgMsg, "%s: Card Holder Name [%s]", __FUNCTION__, pszCardHolderName);
			APP_TRACE(szDbgMsg);
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: createDisplayString
 *
 * Description	: This API would be used to create the Display String according to the format string
 *
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int createDisplayString(char* frmtStr, char* dispStr)
{
	int		rv 				= SUCCESS;
	int		i				= 0;

#ifdef DEBUG
char			szDbgMsg[256]	= "";
#endif

	if((frmtStr == NULL) || (strlen(frmtStr) == 0))
	{
		rv = FAILURE;
		return rv;
	}

	debug_sprintf(szDbgMsg, "%s: frmtStr [%s]", __FUNCTION__, frmtStr);
	APP_TRACE(szDbgMsg);

	for(i = 0; frmtStr[i] != '\0'; i++)
	{
		if((frmtStr[i] == 'X') || (frmtStr[i] == 'N'))
		{
			dispStr[i] = ' ';
		}
		else
		{
			dispStr[i] = frmtStr[i];
		}
	}
	dispStr[i] = '\0';

	debug_sprintf(szDbgMsg, "%s: Display String to be Given to Form [%s]", __FUNCTION__, dispStr);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: createInputStrWithLiterals
 *
 * Description	: This API would be used to create/convert the entered input data string
 * according to the format string (along with the literals)
 *
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int createInputStrWithLiterals(PROMPTDTLS_PTYPE pstPromptDtls)
{
	int		rv 				= SUCCESS;
	int		i				= 0;
	int		j				= 0;
	char *	frmtStr			= NULL;
	char *	ipStr			= NULL;
	char *	dstStr			= NULL;

#ifdef DEBUG
char			szDbgMsg[256]	= "";
#endif

	if(pstPromptDtls == NULL)
	{
		rv = FAILURE;
		return rv;
	}

	frmtStr = pstPromptDtls->szPromptFormat;
	ipStr = pstPromptDtls->szPromptData;
	dstStr = pstPromptDtls->szPromptDataWithLiterals;

	debug_sprintf(szDbgMsg, "%s: frmtStr [%s], ipStr [%s] ", __FUNCTION__, frmtStr, ipStr);
	APP_TRACE(szDbgMsg);

	/* T_RaghavendranR1, For DOLLAR Prompt type, the data is entered from RIGHT to LEFT, so when we add the Literals to the input string we go the end of the input and format string
	 * and the literals from there to beginning so as to send the correct Data with format string(literals) to POS Response
	 */
	if(pstPromptDtls->iPromptType == PROMPTTYPE_DOLLAR)
	{
		if(frmtStr && (strlen(frmtStr) > 0))
		{
			for(i = (strlen(frmtStr)-1), j = (strlen(ipStr)-1); i >= 0; i--)
			{
				if((frmtStr[i] == 'X') || (frmtStr[i] == 'N'))
				{
					if(j >= 0 && ipStr[j] != '\0')
					{
						dstStr[i] = ipStr[j];
						j--;
					}
					else
					{
						dstStr[i] = ' ';
					}
				}
				else
				{
					dstStr[i] = frmtStr[i];
				}
			}
		}
		else
		{
			strcpy(dstStr, ipStr);
		}
	}
	else
	{
		if(frmtStr && (strlen(frmtStr) > 0))
		{
			for(i = 0, j = 0; frmtStr[i] != '\0'; i++)
			{
				if((frmtStr[i] == 'X') || (frmtStr[i] == 'N'))
				{
					if(ipStr[j] != '\0')
					{
						dstStr[i] = ipStr[j];
						j++;
					}
					else
					{
						dstStr[i] = ' ';
					}
				}
				else
				{
					dstStr[i] = frmtStr[i];
				}
			}
			dstStr[i] = '\0';
		}
		else
		{
			strcpy(dstStr, ipStr);
		}
	}


	debug_sprintf(szDbgMsg, "%s: Data String after converting with literals [%s]", __FUNCTION__, dstStr);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: createFormatString
 *
 * Description	: This API would be used to covert the format string in the request to verifone format string type
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int createFormatString(char* promptFrmtStr, char* finalFrmtStr)
{
	int		rv 				= SUCCESS;
	int		i				= 0;
	int		j				= 0;
	char	srchChars[] 	= {'/', '\\', '-', '$', '.', '#', '(', ')', '@', ' ', '%', 0};
	char*	srcTxt			= NULL;

#ifdef DEBUG
char			szDbgMsg[256]	= "";
#endif

	if((promptFrmtStr == NULL) || (strlen(promptFrmtStr) == 0) || (finalFrmtStr == NULL))
	{
		rv = FAILURE;
		return rv;
	}
	strcpy(finalFrmtStr, promptFrmtStr);

	for(i = 0; srchChars[i] != 0; i++)
	{
		srcTxt = finalFrmtStr;
		j = 0;
		while(srcTxt[j] !='\0')
		{
			if(srcTxt[j] == srchChars[i])
			{
				srcTxt[j] = 'C';
			}
			j++;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Format String to be Given to Form [%s]", __FUNCTION__, finalFrmtStr);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getNumericPromptFromUser
 *
 * Description	: This API would be used to prompt the users to collect Numeric Data from the
 * user according to the details specified in the prompt structure
 *
 * Input Params	: Data(to be filled)
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getCreditAppNumericDataFromUser(PROMPTDTLS_PTYPE pstPromptDtls, int iPromptNo)
{
	int				rv						= SUCCESS;
	unsigned 		shOptions 				= 0;
	int 			iTitle					= -1;
	int 			ctrlIds[6];
	int				iDisableFormFlag		= 0;
	int				iMinLength				= 0;
	int				iMaxLength				= 0;
	char			szTitle[300 + 1]		= "";
	char			szTempFormatString[100]	= "NNNNNNNNNNNNNNNNNNNNNNNNN";
	char			szFormatString[100]		= "NNNNNNNNNNNNNNNNNNNNNNNNN";
	char			szTempDispString[100]	= "                         ";
	char			szDispString[100]		= "                         ";
	PAAS_BOOL		bWait					= PAAS_TRUE;
	XEVT_PTYPE		pstXevt					= NULL;
	BTNLBL_STYPE	stBtnLbl;
	UIREQ_STYPE		stUIReq;

	#ifdef DEBUG
	char			szDbgMsg[512]	= "";
	#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = CREDITAPP_NUMERIC;

	iDisableFormFlag = 1;
	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	if(pstPromptDtls)
	{
		iTitle = pstPromptDtls->iTitle;
		if(iTitle <= -1)
		{
			/* Convert string to UTF string */
			convStrToUTFEscStr(pstPromptDtls->szPromptText, szTitle);
		}
		else
		{
			fetchScrTitle(szTitle, iTitle);
		}

		debug_sprintf(szDbgMsg, "%s: Got Screen Title=%s", __FUNCTION__, szTitle);
		APP_TRACE(szDbgMsg);

		/*Setting the ctrlids for the form*/
		ctrlIds[0] 	= PS_NUM_ENTRY_LBL_1;
		ctrlIds[1] 	= PS_NUM_ENTRY_LBL_11;
		ctrlIds[2] 	= PS_NUM_ENTRY_LBL_12;
		ctrlIds[3] 	= PS_NUM_ENTRY_LBL_2;
		ctrlIds[4]  = PS_NUM_ENTRY_LBL_8;
		ctrlIds[5]  = PS_NUM_ENTRY_LBL_9;

		setCreditAppScreenTitle(szTitle, ctrlIds, 6, giLabelLen, stUIReq.pszBuf);

		/*Getting the current options for the Number Entry ctrlid of the form*/
		shOptions = getOptionsForCtrlid(PS_NUM_ENTRY_EDT_1, stUIReq.pszBuf);
		if(pstPromptDtls->iPromptMaskType == PROMPTMASK_1 || pstPromptDtls->iPromptMaskType == PROMPTMASK_TRUE || pstPromptDtls->iPromptMaskType == PROMPTMASK_T || pstPromptDtls->iPromptMaskType == PROMPTMASK_YES)
		{
			shOptions |= OPT_TB_PWD_MASK; // Enable Password Mask if requested
		}
		else if(pstPromptDtls->iPromptMaskType == PROMPTMASK_TRAILING)
		{
			shOptions |= OPT_TB_DELAYED_PWD_MASK; // Enable Delayed Password Mask if requested
		}
		/* If the numeric data to be entered for ex contact no, we keep the left to right entry so we mask
		 * the Right to left entry, else if the data to be entered is is dollars/amount we keep the existing
		 * default right to left entry from the form.*/
		if(pstPromptDtls->iPromptType != PROMPTTYPE_DOLLAR)
		{
			//To make sure that right to left entry is disabled
			shOptions &= ~OPT_TB_RL_MASK;
		}
		//To make sure that Literals are not skipped for Numeric Data Entry
		/*T_Raghavendran_28092016, No need to take complement the flag below. Let FA send the data without literals
		 * and SCA will add literals according to Format String From POS and Input string received from FA */
		//shOptions &= ~OPT_TB_SKIP_LITERALS_MASK;

		initUIReqMsg(&stUIReq, BATCH_REQ);

		setShortValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_SHORT_OPTIONS, shOptions, stUIReq.pszBuf);

		iMinLength = pstPromptDtls->iPromptMinChars;
		setShortValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_SHORT_MIN_ENTRIES, pstPromptDtls->iPromptMinChars, stUIReq.pszBuf);

		/* Restricting the format length to iLength*/
		/* Set the format string for the textbox */
		iMaxLength = pstPromptDtls->iPromptMaxChars;
		if(iMaxLength == 0)
		{
			if((iMaxLength = strlen(pstPromptDtls->szPromptFormat)) > 0)
			{
				createFormatString(pstPromptDtls->szPromptFormat, szFormatString);
			}
			else
			{
				iMaxLength = 25;
				sprintf(szFormatString,"%.*s", iMaxLength, szTempFormatString);
			}
		}
		else if (strlen(pstPromptDtls->szPromptFormat) > 0)
		{
			createFormatString(pstPromptDtls->szPromptFormat, szTempFormatString);
			iMaxLength = strlen(szTempFormatString);
			sprintf(szFormatString,"%.*s", iMaxLength, szTempFormatString);
		}
		else
		{
			sprintf(szFormatString,"%.*s", iMaxLength, szTempFormatString);
		}

		setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_FORMAT_STRING,  szFormatString, stUIReq.pszBuf);

		sprintf(szDispString,"%.*s", iMaxLength, szTempDispString);
		if(strlen(pstPromptDtls->szPromptFormat))
		{
			/* Covert the prompt data to be populated according to the format string and send back in szDispString*/
			createDisplayString(pstPromptDtls->szPromptFormat, szTempDispString);
			sprintf(szDispString,"%.*s", iMaxLength, szTempDispString);
		}

		debug_sprintf(szDbgMsg, "%s: szFormatString [%s], szDispString [%s], ", __FUNCTION__, szFormatString, szDispString);
		APP_TRACE(szDbgMsg);

		/* Set the display string on the textbox */
		setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_DISPLAY_STRING, szDispString, stUIReq.pszBuf);
		if(strlen(pstPromptDtls->szPromptData) > 0)
		{
			setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_INPUT_STRING, pstPromptDtls->szPromptData, stUIReq.pszBuf);
		}

		/* Set the button labels */
		if(iPromptNo == 0)
		{
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_08);
			setStringValueWrapper(PS_NUM_ENTRY_LBL_4, PROP_STR_CAPTION, (char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);
		}
		else
		{
			fetchBtnLabel(&stBtnLbl, BTN_LBLS_26);
			setStringValueWrapper(PS_NUM_ENTRY_LBL_4, PROP_STR_CAPTION, (char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);
		}

		/* Set the label for the button - "Submit "*/
		fetchBtnLabel(&stBtnLbl, BTN_LBLS_09);
		setStringValueWrapper(PS_NUM_ENTRY_LBL_3, PROP_STR_CAPTION, (char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

		/* Send the batch message to UI agent */
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv == SUCCESS)
		{
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_XEVT_RESP)
					{
						pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
						if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
						{
							if(pstXevt->uiCtrlID == PS_NUM_ENTRY_EDT_1)
							{
								switch(pstXevt->uiKeypadEvt)
								{
								case 2: /* ON ENTER */
									debug_sprintf(szDbgMsg, "%s: Value = [%s]",
											__FUNCTION__, pstXevt->szKpdVal);
									APP_TRACE(szDbgMsg);

									strcpy(pstPromptDtls->szPromptData, pstXevt->szKpdVal);
									/* T_RaghavendranR1 : It is Noticed that the data returned in response from FA for TRAILING Masking does not contain the literals in given in Format string
									 * Hence adding it in SCA only for TRAILING Masking prompt Type.
									 * For Other Prompt Type the data is returned with literals from FA.*/
									/* T_Raghavendran_28092016, We commented complementing the Flag OPT_TB_SKIP_LITERALS_MASK, so that the data returned from FA for all Mask type will be without Literals.
									 * SAC will externally add the literals for all mask type accoring to Prompt Formal and input string received from FA, Similar to QWERTY Keypad*/
									//if(pstPromptDtls->iPromptMaskType == PROMPTMASK_TRAILING)
									//{
										createInputStrWithLiterals(pstPromptDtls);
									//}
									//else
									//{
										//strcpy(pstPromptDtls->szPromptDataWithLiterals, pstXevt->szKpdVal);
									//}

									rv = SUCCESS;
									break;

								case 3: /* ON CANCEL */
									debug_sprintf(szDbgMsg, "%s: CANCEL pressed",
											__FUNCTION__);
									APP_TRACE(szDbgMsg);

									rv = UI_CANCEL_PRESSED;
									break;

								default:
									debug_sprintf(szDbgMsg, "%s: Invalid Kpd event", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									rv = ERR_DEVICE_APP;
									break;
								}
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Invalid control Id", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
							}

							bBLDataRcvd = PAAS_FALSE;
							bWait = PAAS_FALSE;
						}
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: captureCustButton
 *
 * Description	: This API would be used to prompt the users for taking customer Button input
 *
 *
 * Input Params	: Data(to be filled)
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int captureCustButton(CUSTBUTTONDTLS_PTYPE pstCustButtonDtls)
{
	int				rv					= SUCCESS;
	int				iSelectedOption 	= 0;
	int				iDisableFormFlag	= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	/*
	 * Setting it to False so that will not take any
	 * available BL data before showing this form
	 * This is to FIX the issue i.e. when multiple times
	 * buttons are pressed on this screen, for the next same command
	 * last press from the previous screen is taken since
	 * BL data is available
	 */
	bBLDataRcvd = PAAS_FALSE;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	/* Initialize the elements of the form */
	whichForm = CUST_BUTTON_FRM;
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	if(strlen(pstCustButtonDtls->szDispTxt1) > 0)
	{
		setStringValueWrapper(PAAS_CUSTBTN_SCREEN_LBL_1, PROP_STR_CAPTION, pstCustButtonDtls->szDispTxt1, stUIReq.pszBuf);
	}
	if(strlen(pstCustButtonDtls->szDispTxt2) > 0)
	{
		setStringValueWrapper(PAAS_CUSTBTN_SCREEN_LBL_2, PROP_STR_CAPTION, pstCustButtonDtls->szDispTxt2, stUIReq.pszBuf);
	}
	if(strlen(pstCustButtonDtls->szDispTxt3) > 0)
	{
		setStringValueWrapper(PAAS_CUSTBTN_SCREEN_LBL_3, PROP_STR_CAPTION, pstCustButtonDtls->szDispTxt3, stUIReq.pszBuf);
	}
	if(strlen(pstCustButtonDtls->szDispTxt4) > 0)
	{
		setStringValueWrapper(PAAS_CUSTBTN_SCREEN_LBL_4, PROP_STR_CAPTION, pstCustButtonDtls->szDispTxt4, stUIReq.pszBuf);
	}
	if(strlen(pstCustButtonDtls->szDispTxt5) > 0)
	{
		setStringValueWrapper(PAAS_CUSTBTN_SCREEN_LBL_5, PROP_STR_CAPTION, pstCustButtonDtls->szDispTxt5, stUIReq.pszBuf);
	}

	if(strlen(pstCustButtonDtls->szButtonLabel1) > 0)
	{
		setBoolValueWrapper(PAAS_CUSTBTN_SCREEN_BTN_1, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_CUSTBTN_SCREEN_LBL_6, PROP_STR_CAPTION, pstCustButtonDtls->szButtonLabel1, stUIReq.pszBuf);
	}
	if(strlen(pstCustButtonDtls->szButtonLabel2) > 0)
	{
		setBoolValueWrapper(PAAS_CUSTBTN_SCREEN_BTN_2, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_CUSTBTN_SCREEN_LBL_7, PROP_STR_CAPTION, pstCustButtonDtls->szButtonLabel2, stUIReq.pszBuf);
	}
	if(strlen(pstCustButtonDtls->szButtonLabel3) > 0)
	{
		setBoolValueWrapper(PAAS_CUSTBTN_SCREEN_BTN_3, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_CUSTBTN_SCREEN_LBL_8, PROP_STR_CAPTION, pstCustButtonDtls->szButtonLabel3, stUIReq.pszBuf);
	}
	if(strlen(pstCustButtonDtls->szButtonLabel4) > 0)
	{
		setBoolValueWrapper(PAAS_CUSTBTN_SCREEN_BTN_4, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_CUSTBTN_SCREEN_LBL_9, PROP_STR_CAPTION, pstCustButtonDtls->szButtonLabel4, stUIReq.pszBuf);
	}
	if(strlen(pstCustButtonDtls->szButtonLabel5) > 0)
	{
		setBoolValueWrapper(PAAS_CUSTBTN_SCREEN_BTN_5, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_CUSTBTN_SCREEN_LBL_10, PROP_STR_CAPTION, pstCustButtonDtls->szButtonLabel5, stUIReq.pszBuf);
	}
	if(strlen(pstCustButtonDtls->szButtonLabel6) > 0)
	{
		setBoolValueWrapper(PAAS_CUSTBTN_SCREEN_BTN_6, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_CUSTBTN_SCREEN_LBL_11, PROP_STR_CAPTION, pstCustButtonDtls->szButtonLabel6, stUIReq.pszBuf);
	}

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Customer Button Capture Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{

						case PAAS_CUSTBTN_SCREEN_FK_1:
							debug_sprintf(szDbgMsg, "%s: Cancel button pressed by user from customer button screen", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							bWait = PAAS_FALSE;
							sprintf(pstCustButtonDtls->szResultData, "0");
							rv = SUCCESS;
							break;

						case PAAS_CUSTBTN_SCREEN_BTN_1:
							debug_sprintf(szDbgMsg, "%s: Option 1 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 1;
							sprintf(pstCustButtonDtls->szResultData, "%d", iSelectedOption);
							bWait = PAAS_FALSE;
							rv = SUCCESS;
							break;

						case PAAS_CUSTBTN_SCREEN_BTN_2:
							debug_sprintf(szDbgMsg, "%s: Option 2 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 2;
							sprintf(pstCustButtonDtls->szResultData, "%d", iSelectedOption);
							bWait = PAAS_FALSE;
							rv = SUCCESS;
							break;

						case PAAS_CUSTBTN_SCREEN_BTN_3:
							debug_sprintf(szDbgMsg, "%s: Option 3 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 3;
							sprintf(pstCustButtonDtls->szResultData, "%d", iSelectedOption);
							bWait = PAAS_FALSE;
							rv = SUCCESS;
							break;

						case PAAS_CUSTBTN_SCREEN_BTN_4:
							debug_sprintf(szDbgMsg, "%s: Option 4 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 4;
							sprintf(pstCustButtonDtls->szResultData, "%d", iSelectedOption);
							bWait = PAAS_FALSE;
							rv = SUCCESS;
							break;

						case PAAS_CUSTBTN_SCREEN_BTN_5:
							debug_sprintf(szDbgMsg, "%s: Option 5 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 5;
							sprintf(pstCustButtonDtls->szResultData, "%d", iSelectedOption);
							bWait = PAAS_FALSE;
							rv = SUCCESS;
							break;

						case PAAS_CUSTBTN_SCREEN_BTN_6:
							debug_sprintf(szDbgMsg, "%s: Option 6 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 6;
							sprintf(pstCustButtonDtls->szResultData, "%d", iSelectedOption);
							bWait = PAAS_FALSE;
							rv = SUCCESS;
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Unknown",__FUNCTION__);
							APP_TRACE(szDbgMsg);
							bWait = PAAS_FALSE;
							rv = ERR_DEVICE_APP;
							break;
						}
						if(iAppLogEnabled == 1)
						{
							sprintf(szAppLogData, "Option[%d] Selected By User While Capturing Customer Button Details", iSelectedOption);
							addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
						}

						bBLDataRcvd = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getHostSelectionFromUser
 *
 * Description	: This API would be used to prompt the users for taking survey
 *
 *
 * Input Params	: Data(to be filled)
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getHostSelectionFromUser(char *pszHostSection)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[256]	= "";
	char			szTmp[300 + 1]		= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	bBLDataRcvd = PAAS_FALSE;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	/* Initialize the elements of the form */
	whichForm = HOST_SEL_FRM;
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTmp, HOST_SELECTION_TITLE);
	setStringValueWrapper(PAAS_HOST_SELECTION_LBL_1, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Host Selection Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_HOST_SELECTION_FK_SKIP:
							debug_sprintf(szDbgMsg, "%s: SKIP Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							rv = UI_CANCEL_PRESSED;
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "User Skipped Capturing Host Selection");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							bWait = PAAS_FALSE;
							break;

						case PAAS_HOST_SELECTION_FK_SUBMIT:
							debug_sprintf(szDbgMsg, "%s: Submit Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__); //Will be sending request to UI Agent here

							if(strlen(pszHostSection) != 0)
							{
								debug_sprintf(szDbgMsg, "%s: Selected Option is [%s]", __FUNCTION__, pszHostSection);
								APP_TRACE(szDbgMsg);

								if(iAppLogEnabled == 1)
								{
									sprintf(szAppLogData, "Host [%s] Selected By User", pszHostSection);
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								bWait = PAAS_FALSE;
								rv = SUCCESS;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: No option is selected, will remain on the same form", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "No Option Selected By User While Capturing Host Selection Details");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}
							}
							break;
						case PAAS_HOST_SELECTION_OPT_1:
							debug_sprintf(szDbgMsg, "%s: RC Host Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							strcpy(pszHostSection, "rchi");
							break;
						case PAAS_HOST_SELECTION_OPT_2:
							debug_sprintf(szDbgMsg, "%s: Chase Payment Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							strcpy(pszHostSection, "cphi");
							break;
						case PAAS_HOST_SELECTION_OPT_3:
							debug_sprintf(szDbgMsg, "%s: Vantiv Host Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							strcpy(pszHostSection, "dhi");
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Unknown",__FUNCTION__);
							APP_TRACE(szDbgMsg);
							bWait = PAAS_FALSE;
							rv = ERR_DEVICE_APP;
							break;
						}
						bBLDataRcvd = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: captureSurvey
 *
 * Description	: This API would be used to prompt the users for taking survey
 *
 *
 * Input Params	: Data(to be filled)
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int captureSurvey(SURVEYDTLS_PTYPE pstSurveyDtls, int iSurveyOption)
{
	int				rv					= SUCCESS;
	int				iSelectedOption 	= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */

	disableCardReaders();

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	/*
	 * Setting it to False so that will not take any
	 * available BL data before showing this form
	 * This is to FIX the issue i.e. when multiple times
	 * buttons are pressed on this screen, for the next same command
	 * last press from the previous screen is taken since
	 * BL data is available
	 */
	bBLDataRcvd = PAAS_FALSE;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	/* Initialize the elements of the form */
	whichForm = CUST_SURVEY_FRM;
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	if(strlen(pstSurveyDtls->szDispTxt1) > 0)
	{
		setStringValueWrapper(PAAS_SURVEY_SCREEN_LBL_1, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt1, stUIReq.pszBuf);
	}
	if(strlen(pstSurveyDtls->szDispTxt2) > 0)
	{
		setStringValueWrapper(PAAS_SURVEY_SCREEN_LBL_2, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt2, stUIReq.pszBuf);
	}
	if(strlen(pstSurveyDtls->szDispTxt3) > 0)
	{
		setStringValueWrapper(PAAS_SURVEY_SCREEN_LBL_3, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt3, stUIReq.pszBuf);
	}
	if(strlen(pstSurveyDtls->szDispTxt4) > 0)
	{
		setStringValueWrapper(PAAS_SURVEY_SCREEN_LBL_4, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt4, stUIReq.pszBuf);
	}
	if(strlen(pstSurveyDtls->szDispTxt5) > 0)
	{
		setStringValueWrapper(PAAS_SURVEY_SCREEN_LBL_5, PROP_STR_CAPTION, pstSurveyDtls->szDispTxt5, stUIReq.pszBuf);
	}

	if(iSurveyOption == SURVEY_OPTION_10)
	{
		debug_sprintf(szDbgMsg, "%s: Its Survey10, need to show ten options", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		setBoolValueWrapper(PAAS_SURVEY_SCREEN_OPT_6, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setBoolValueWrapper(PAAS_SURVEY_SCREEN_OPT_7, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setBoolValueWrapper(PAAS_SURVEY_SCREEN_OPT_8, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setBoolValueWrapper(PAAS_SURVEY_SCREEN_OPT_9, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		setBoolValueWrapper(PAAS_SURVEY_SCREEN_OPT_10, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
	}

	/* Set the label for the button - "Skip "*/
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_11);
	setStringValueWrapper(PAAS_SURVEY_SCREEN_SKIP_LBL, PROP_STR_CAPTION, (char *)stBtnLbl.szLblOne, stUIReq.pszBuf);

	/* Set the label for the button - "Submit "*/
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_09);
	setStringValueWrapper(PAAS_SURVEY_SCREEN_SUBMIT_LBL, PROP_STR_CAPTION, (char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Survey Capture Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_SURVEY_SCREEN_FK_SKIP:
							debug_sprintf(szDbgMsg, "%s: SKIP Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							strcpy(pstSurveyDtls->szResultData, "0");
							rv = SUCCESS;
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "User Skipped Capturing Survey Details");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							bWait = PAAS_FALSE;
							break;

						case PAAS_SURVEY_SCREEN_FK_SUBMIT:
							debug_sprintf(szDbgMsg, "%s: Submit Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__); //Will be sending request to UI Agent here

							if(iSelectedOption != 0)
							{
								debug_sprintf(szDbgMsg, "%s: Selected Option is %d", __FUNCTION__, iSelectedOption);
								APP_TRACE(szDbgMsg);

								sprintf(pstSurveyDtls->szResultData, "%d", iSelectedOption);
								if(iAppLogEnabled == 1)
								{
									sprintf(szAppLogData, "Option[%d] Selected By User While Capturing Survey Details", iSelectedOption);
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								bWait = PAAS_FALSE;
								rv = SUCCESS;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: No option is selected, will remain on the same form", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "No Option Selected By User While Capturing Survey Details");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}
							}
#if 0
							rv = getSelectedSurveyOption(&iSelectedOption, iSurveyOption);
							if(rv == SUCCESS)
							{
								if(iSelectedOption != 0)
								{
									debug_sprintf(szDbgMsg, "%s: Selected Option is %d", __FUNCTION__, iSelectedOption);
									APP_TRACE(szDbgMsg);

									sprintf(pstSurveyDtls->szResultData, "%d", iSelectedOption);
									bWait = PAAS_FALSE;
								}
								else
								{
									debug_sprintf(szDbgMsg, "%s: No option is selected, will remain on the same form", __FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Error while getting the selected option on Survey form", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}
#endif
							break;
						case PAAS_SURVEY_SCREEN_OPT_1:
							debug_sprintf(szDbgMsg, "%s: Option 1 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 1;
							break;
						case PAAS_SURVEY_SCREEN_OPT_2:
							debug_sprintf(szDbgMsg, "%s: Option 2 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 2;
							break;
						case PAAS_SURVEY_SCREEN_OPT_3:
							debug_sprintf(szDbgMsg, "%s: Option 3 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 3;
							break;
						case PAAS_SURVEY_SCREEN_OPT_4:
							debug_sprintf(szDbgMsg, "%s: Option 4 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 4;
							break;
						case PAAS_SURVEY_SCREEN_OPT_5:
							debug_sprintf(szDbgMsg, "%s: Option 5 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 5;
							break;
						case PAAS_SURVEY_SCREEN_OPT_6:
							debug_sprintf(szDbgMsg, "%s: Option 6 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 6;
							break;
						case PAAS_SURVEY_SCREEN_OPT_7:
							debug_sprintf(szDbgMsg, "%s: Option 7 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 7;
							break;
						case PAAS_SURVEY_SCREEN_OPT_8:
							debug_sprintf(szDbgMsg, "%s: Option 8 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 8;
							break;
						case PAAS_SURVEY_SCREEN_OPT_9:
							debug_sprintf(szDbgMsg, "%s: Option 9 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 9;
							break;
						case PAAS_SURVEY_SCREEN_OPT_10:
							debug_sprintf(szDbgMsg, "%s: Option 10 Selected", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							iSelectedOption = 10;
							break;
						default:
							debug_sprintf(szDbgMsg, "%s: Unknown",__FUNCTION__);
							APP_TRACE(szDbgMsg);
							bWait = PAAS_FALSE;
							rv = ERR_DEVICE_APP;
							break;
						}

						bBLDataRcvd = PAAS_FALSE;
					}
					/*
					 * Praveen_P1: Not waiting only if the response received from the correct form
					 * 			   so moved the below line inside the above if condition
					 */
					//bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCreditAppSwipeDataFromUser
 *
 * Description	: This API would be used to prompt the users with a QWERTY Keypad and with MSR Enabled for CREDIT_APP
 * Application, Users can either enter their Name or Swipe the Card.
 * If Card is Swiped the CardHolder Name is Captured from the Track Data and sent back to the POS.
 * Its upto the POS to parse the First Name and Last Name
 * New Prompt Type QWERTY_SWIPE is introduced for this, to enable the MSR SWIPE.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getCreditAppSwipeDataFromUser(PROMPTDTLS_PTYPE pstPromptDtls)
{
	ushort			options						= 0;
	int				rv							= SUCCESS;
	int 			ctrlIds[4];
	int				iDisableFormFlag			= 0;
	int				iTitle						= -1;
	int 			iMinLength					= 0;
	int 			iMaxLength					= 0;
	int				iLen						= 0;
	int				iAppLogEnabled				= isAppLogEnabled();
	char			szAppLogData[256]			= "";
	char			szTmp[300 + 1]				= "";
	char			szTempFormatString[100]		= "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	char			szFormatString[100]			= "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	char			szTempDispString[100]		= "                                        ";
	char			szDispString[100]			= "                                        ";
	char			szTrack1[256]				= "";
	char			szCardHolderName[100]		= "";
	PAAS_BOOL		bWait						= PAAS_TRUE;
	PAAS_BOOL		bSTBDataReceived			= PAAS_FALSE;
	PAAS_BOOL		bCardDataReceived			= PAAS_FALSE;
	XEVT_PTYPE		pstXevt						= NULL;
	CARD_TRK_PTYPE	pstCard						= NULL;
	UIREQ_STYPE		stUIReq;


#ifdef DEBUG
	char		szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */

	disableCardReaders();

	whichForm = CREDITAPP_QWERTY;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	if(pstPromptDtls)
	{
		iTitle = pstPromptDtls->iTitle;
		if(iTitle <= -1) // Title String available in the Prompt Text
		{
			/* Convert string to UTF string */
			convStrToUTFEscStr(pstPromptDtls->szPromptText, szTmp);
		}
		else //Need to get the title string from iTitle enum
		{
			fetchScrTitle(szTmp, iTitle);
		}
		debug_sprintf(szDbgMsg, "%s: Got Screen Title=%s", __FUNCTION__, szTmp);
		APP_TRACE(szDbgMsg);


		/*Setting the ctrlids for the form*/
		ctrlIds[0] 	= PAAS_EMAILSCREEN_LBL_1;
		ctrlIds[1] 	= PAAS_EMAILSCREEN_LBL_2;
		ctrlIds[2] 	= PAAS_EMAILSCREEN_LBL_3;

		setCreditAppScreenTitle(szTmp, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);

		/*Getting the current options for the ctrlid of the form*/
		options = getOptionsForCtrlid(PAAS_EMAILSCREEN_KBD_1, stUIReq.pszBuf);
		if(pstPromptDtls->iPromptMaskType == PROMPTMASK_1 || pstPromptDtls->iPromptMaskType == PROMPTMASK_TRUE || pstPromptDtls->iPromptMaskType == PROMPTMASK_T || pstPromptDtls->iPromptMaskType == PROMPTMASK_YES)
		{
			options |= OPT_TB_PWD_MASK; // Enable Password Mask if requested
		}
		else if(pstPromptDtls->iPromptMaskType == PROMPTMASK_TRAILING)
		{
			options |= OPT_TB_DELAYED_PWD_MASK; // Enable Delayed Password Mask if requested
		}

		initUIReqMsg(&stUIReq, BATCH_REQ);

		setShortValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_SHORT_OPTIONS, options, stUIReq.pszBuf);

		iMinLength = pstPromptDtls->iPromptMinChars;
		setShortValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_SHORT_MIN_ENTRIES, iMinLength, stUIReq.pszBuf);

		/* Restricting the format length to iLength*/
		/* Set the format string for the textbox */
		iMaxLength = pstPromptDtls->iPromptMaxChars;
		if(iMaxLength == 0)
		{
			if((iMaxLength = strlen(pstPromptDtls->szPromptFormat)) > 0)
			{
				createFormatString(pstPromptDtls->szPromptFormat, szFormatString);
			}
			else
			{
				iMaxLength = 40;
				sprintf(szFormatString,"%.*s", iMaxLength, szTempFormatString);
			}
		}
		else if(strlen(pstPromptDtls->szPromptFormat) > 0)
		{
			createFormatString(pstPromptDtls->szPromptFormat, szTempFormatString);
			iMaxLength = strlen(szTempFormatString);
			sprintf(szFormatString,"%.*s", iMaxLength, szTempFormatString);
		}
		else
		{
			sprintf(szFormatString,"%.*s", iMaxLength, szTempFormatString);
		}

		setStringValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_STR_FORMAT_STRING, szFormatString, stUIReq.pszBuf);

		sprintf(szDispString,"%.*s", iMaxLength, szTempDispString);
		if(strlen(pstPromptDtls->szPromptFormat))
		{
			/* Covert the prompt data to be populated according to the format string and send back in szDispString*/
			createDisplayString(pstPromptDtls->szPromptFormat, szTempDispString);
			iMaxLength = ((iMaxLength > 40) ? 40 : iMaxLength);
			sprintf(szDispString,"%.*s", iMaxLength, szTempDispString);
		}

		debug_sprintf(szDbgMsg, "%s:  szFormatString [%s], szDispString [%s]", __FUNCTION__,  szFormatString, szDispString);
		APP_TRACE(szDbgMsg);
		/* Set the display string on the textbox */
		setStringValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_STR_DISPLAY_STRING, szDispString, stUIReq.pszBuf);

		if(strlen(pstPromptDtls->szPromptData) > 0)
		{
			setStringValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_STR_INPUT_STRING, pstPromptDtls->szPromptData, stUIReq.pszBuf);
		}

		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			bBLDataRcvd = PAAS_FALSE;
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(5);
			}
			bWait = PAAS_TRUE;

			/* Sending it here so that first form is shown and then swipe is enabled*/
			//AjayS2: 13-Jan-2016
			//If wallet is enabled we have to send TERM_CAP as Payment only, because from next release
			//default TERM_CAP is taken as VAS and PAYMENT
			if(isWalletEnabled())
			{
				rv = sendS20CmdToGetMSRCardData(PAYMENT_ONLY,  NULL, NULL);
			}
			else
			{
				rv = sendS20CmdToGetMSRCardData(NULL,  NULL, NULL);
			}
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to send XPI S20 cmd to enable MSR", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return ERR_DEVICE_APP;
			}

			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_XEVT_RESP)
					{
						pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
						if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
						{
							if(pstXevt->uiCtrlID == PAAS_EMAILSCREEN_KBD_1)
							{
								if(pstXevt->uiKeypadEvt == 2)
								{
									/* Pressed ENTER on keypad */
									debug_sprintf(szDbgMsg, "%s: Value = [%s]", __FUNCTION__, pstXevt->szKpdVal);
									APP_TRACE(szDbgMsg);
									strcpy(pstPromptDtls->szPromptData, pstXevt->szKpdVal);
									createInputStrWithLiterals(pstPromptDtls);
									rv = SUCCESS;
								}
								else if(pstXevt->uiKeypadEvt == 3)
								{
									/* Pressed CANCEL on keypad */
									debug_sprintf(szDbgMsg, "%s: CANCEL pressed", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									setCardReadEnabled(PAAS_FALSE);
									rv = UI_CANCEL_PRESSED;
								}
								else
								{
									debug_sprintf(szDbgMsg, "%s: Invalid Kpd event", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									rv = ERR_DEVICE_APP;
								}
							}
							else if(pstXevt->uiCtrlType == 90) //STB Data Event
							{
								debug_sprintf(szDbgMsg, "%s: STB Event Received", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								bWait = PAAS_FALSE;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Invalid control Id", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
							}
							/* Setting Card Read enabled as false , after we send the Cancel request */
							cancelRequest();
							setCardReadEnabled(PAAS_FALSE);
							/* Disabling the card swipe on the UI agent. */
							bBLDataRcvd = PAAS_FALSE;
							bWait = PAAS_FALSE;
						}
						/*
						 * Praveen_P1: Not waiting only if the response received from the correct form
						 * 			   so moved the below line inside the above if condition
						 */
						//bWait = PAAS_FALSE;
					}
					else if(stBLData.uiRespType == UI_CARD_RESP)
					{
						if(stBLData.iStatus == SUCCESS)
						{
							pstCard = &(stBLData.stRespDtls.stCrdTrkInfo);

							debug_sprintf(szDbgMsg, "%s: Received the Card Data", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							/* Track 1 --- */
							iLen = strlen(pstCard->szTrk1);
							if(iLen > 0)
							{
								memset(szTrack1, 0x00, sizeof(szTrack1));
								memcpy(szTrack1, pstCard->szTrk1, iLen);

#ifdef DEVDEBUG
								debug_sprintf(szDbgMsg, "%s: Track1 from Card [%s]", __FUNCTION__, szTrack1);
								APP_TRACE(szDbgMsg);
#endif

								rv = getCardHolderNameFrmTrackData(szTrack1, szCardHolderName);

								strcpy(pstPromptDtls->szPromptData, szCardHolderName);
								createInputStrWithLiterals(pstPromptDtls);
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Track1 Data Not Present", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_CARD_INVALID;
							}
						}
						else if(stBLData.iStatus == ERR_BAD_CARD)
						{
							debug_sprintf(szDbgMsg, "%s: Max bad card reads done", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_BAD_CARD;
						}

						bBLDataRcvd = PAAS_FALSE;
						bCardDataReceived = PAAS_TRUE;

						/*
						 * If STB is enabled, then we need to wait for the XEVT data
						 * thats why not setting bwait to false here
						 */
						if(!isSTBLogicEnabled())
						{
							bWait = PAAS_FALSE;
						}
						else
						{
							/*
							 * If STB is enabled..we have to wait only if we have received the card data, thats why checking rv value
							 * If we have received the STB data already then we need not wait
							 */
							if(rv != SUCCESS || bSTBDataReceived == PAAS_TRUE)
							{
								bWait = PAAS_FALSE;
							}
						}
					}
					else if(stBLData.uiRespType == UI_EMV_RESP)
					{
						if( strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd,EMV_U00_REQ) ==0 || strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_U02_REQ) ==0 )
						{
							if(stBLData.iStatus == SUCCESS)
							{
								debug_sprintf(szDbgMsg, "%s: Received U02 EMV Response",__FUNCTION__);
								APP_TRACE(szDbgMsg);

								if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bEMVRetry == PAAS_TRUE)
								{
									/* Initialize the screen */
									if(iAppLogEnabled == 1)
									{
										strcpy(szAppLogData, "Error While Swiping/Tapping card");
										addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, NULL);
									}
									initUIReqMsg(&stUIReq, BATCH_REQ);

									showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

									if(sendUIReqMsg(stUIReq.pszBuf) != SUCCESS)
									{
										debug_sprintf(szDbgMsg, "%s: Error while communicating with UI Agent", __FUNCTION__);
										APP_TRACE(szDbgMsg);
									}
								}
								else if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bCardInserted == PAAS_TRUE)
								{
									/* KranthiK1: Added this only to show the processing form
									 * as C30 is taking a long time to be respond.
									 * Remove later if not required
									 */
									if(iAppLogEnabled == 1)
									{
										strcpy(szAppLogData, "EMV Card Inserted");
										addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
									}
									bBLDataRcvd = PAAS_FALSE;

									releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
								}
								else if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bSTBDataReceived == PAAS_TRUE)
								{

									if(strlen(stBLData.stRespDtls.stEmvDtls.szSpinBinData) > 0)
									{
										/*T_RaghavendranR1: Doing Nothing Here. No need to collect the STB Data and Process.
										 * It is Enough to Check whether we received the STB Data or not, incase STb is enabled.
										 */
									}

									bSTBDataReceived = PAAS_TRUE;

									/*
									 * If STB is enabled..we have to wait only if we have received the card data, thats why checking rv value
									 * If we have received the STB data already then we need not wait
									 */
									if(bCardDataReceived == PAAS_TRUE)
									{
										bWait = PAAS_FALSE;
									}
								}
								bBLDataRcvd = PAAS_FALSE;
							}
						}
					}
					else if(stBLData.iStatus == ERR_BAD_CARD)
					{
						debug_sprintf(szDbgMsg, "%s: Max bad card reads done",
								__FUNCTION__);
						APP_TRACE(szDbgMsg);
						strcpy(szAppLogData, "Bad Card Read from User");

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;

						rv = ERR_BAD_CARD;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: getCreditAppStringFromUser
 *
 * Description	: This API would be used to prompt the users for entering their
 * 					email.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getCreditAppStringFromUser(PROMPTDTLS_PTYPE pstPromptDtls)
{
	ushort		options					= 0;
	int			rv						= SUCCESS;
	int 		ctrlIds[4];
	int			iDisableFormFlag		= 0;
	int			iTitle					= -1;
	int 		iMinLength				= 0;
	int 		iMaxLength				= 0;
	char		szTmp[300 + 1]			= "";
	char		szTempFormatString[100]	= "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	char		szFormatString[100]		= "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	char		szTempDispString[100]	= "                                        ";
	char		szDispString[100]		= "                                        ";
	PAAS_BOOL	bWait					= PAAS_TRUE;
	XEVT_PTYPE	pstXevt					= NULL;
	UIREQ_STYPE	stUIReq;


#ifdef DEBUG
	char		szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = CREDITAPP_QWERTY;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	if(pstPromptDtls)
	{
		iTitle = pstPromptDtls->iTitle;
		if(iTitle <= -1) // Title String available in the Prompt Text
		{
			/* Convert string to UTF string */
			convStrToUTFEscStr(pstPromptDtls->szPromptText, szTmp);
		}
		else //Need to get the title string from iTitle enum
		{
			fetchScrTitle(szTmp, iTitle);
		}
		debug_sprintf(szDbgMsg, "%s: Got Screen Title=%s", __FUNCTION__, szTmp);
		APP_TRACE(szDbgMsg);


		/*Setting the ctrlids for the form*/
		ctrlIds[0] 	= PAAS_EMAILSCREEN_LBL_1;
		ctrlIds[1] 	= PAAS_EMAILSCREEN_LBL_2;
		ctrlIds[2] 	= PAAS_EMAILSCREEN_LBL_3;

		setCreditAppScreenTitle(szTmp, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);

		/*Getting the current options for the ctrlid of the form*/
		options = getOptionsForCtrlid(PAAS_EMAILSCREEN_KBD_1, stUIReq.pszBuf);
		if(pstPromptDtls->iPromptMaskType == PROMPTMASK_1 || pstPromptDtls->iPromptMaskType == PROMPTMASK_TRUE || pstPromptDtls->iPromptMaskType == PROMPTMASK_T || pstPromptDtls->iPromptMaskType == PROMPTMASK_YES)
		{
			options |= OPT_TB_PWD_MASK; // Enable Password Mask if requested
		}
		else if(pstPromptDtls->iPromptMaskType == PROMPTMASK_TRAILING)
		{
			options |= OPT_TB_DELAYED_PWD_MASK; // Enable Delayed Password Mask if requested
		}

		initUIReqMsg(&stUIReq, BATCH_REQ);

		setShortValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_SHORT_OPTIONS, options, stUIReq.pszBuf);

		iMinLength = pstPromptDtls->iPromptMinChars;
		setShortValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_SHORT_MIN_ENTRIES, iMinLength, stUIReq.pszBuf);

		/* Restricting the format length to iLength*/
		/* Set the format string for the textbox */
		iMaxLength = pstPromptDtls->iPromptMaxChars;
		if(iMaxLength == 0)
		{
			if((iMaxLength = strlen(pstPromptDtls->szPromptFormat)) > 0)
			{
				createFormatString(pstPromptDtls->szPromptFormat, szFormatString);
			}
			else
			{
				iMaxLength = 40;
				sprintf(szFormatString,"%.*s", iMaxLength, szTempFormatString);
			}
		}
		else if(strlen(pstPromptDtls->szPromptFormat) > 0)
		{
			createFormatString(pstPromptDtls->szPromptFormat, szTempFormatString);
			iMaxLength = strlen(szTempFormatString);
			sprintf(szFormatString,"%.*s", iMaxLength, szTempFormatString);
		}
		else
		{
			sprintf(szFormatString,"%.*s", iMaxLength, szTempFormatString);
		}

		setStringValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_STR_FORMAT_STRING, szFormatString, stUIReq.pszBuf);

		sprintf(szDispString,"%.*s", iMaxLength, szTempDispString);
		if(strlen(pstPromptDtls->szPromptFormat))
		{
			/* Covert the prompt data to be populated according to the format string and send back in szDispString*/
			createDisplayString(pstPromptDtls->szPromptFormat, szTempDispString);
			iMaxLength = ((iMaxLength > 40) ? 40 : iMaxLength);
			sprintf(szDispString,"%.*s", iMaxLength, szTempDispString);
		}

		debug_sprintf(szDbgMsg, "%s:  szFormatString [%s], szDispString [%s]", __FUNCTION__,  szFormatString, szDispString);
		APP_TRACE(szDbgMsg);
		/* Set the display string on the textbox */
		setStringValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_STR_DISPLAY_STRING, szDispString, stUIReq.pszBuf);

		/*
		 * Praveen_P1: Kohls Requirement
		 * Stand alone email capture function will be enhanced to include a prepopulated_email tag
		 * If the tag is present, the email address form will populate this value onto the screen
		 */
		if(strlen(pstPromptDtls->szPromptData) > 0)
		{
			setStringValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_STR_INPUT_STRING, pstPromptDtls->szPromptData, stUIReq.pszBuf);
		}

		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_XEVT_RESP)
					{
						pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
						if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
						{
							if(pstXevt->uiCtrlID == PAAS_EMAILSCREEN_KBD_1)
							{
								if(pstXevt->uiKeypadEvt == 2)
								{
									/* Pressed ENTER on keypad */
									debug_sprintf(szDbgMsg, "%s: Value = [%s]",
												__FUNCTION__, pstXevt->szKpdVal);
									APP_TRACE(szDbgMsg);
#if 0 //TODO: Remove it later because No need to memset. strcpy will take care.
									/*
									 * Praveen_P1: For stand alone email, szEmail will be populated
									 * sometimes, need to memset before copying the updated/current value
									 * TO_DO: Doing memset for 41 (which is size of szemail in Loyalty structure
									 */
									memset(szData, 0x00, 41);
#endif
									strcpy(pstPromptDtls->szPromptData, pstXevt->szKpdVal);
									createInputStrWithLiterals(pstPromptDtls);
									rv = SUCCESS;
								}
								else if(pstXevt->uiKeypadEvt == 3)
								{
									/* Pressed CANCEL on keypad */
									debug_sprintf(szDbgMsg, "%s: CANCEL pressed",
																	__FUNCTION__);
									APP_TRACE(szDbgMsg);

									rv = UI_CANCEL_PRESSED;
								}
								else
								{
									debug_sprintf(szDbgMsg, "%s: Invalid Kpd event",
																__FUNCTION__);
									APP_TRACE(szDbgMsg);

									rv = ERR_DEVICE_APP;
								}
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Invalid control Id",
													__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
							}

							bBLDataRcvd = PAAS_FALSE;
							bWait = PAAS_FALSE;
						}
						/*
						 * Praveen_P1: Not waiting only if the response received from the correct form
						 * 			   so moved the below line inside the above if condition
						 */
						//bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getStringFromUser
 *
 * Description	: This API would be used to prompt the users for entering their
 * 					email.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getStringFromUser(PROMPTDTLS_PTYPE pstPromptDtls)
{
	ushort		options					= 0;
	int			rv						= SUCCESS;
	int			iDisableFormFlag		= 0;
	int			iTitle					= -1;
	int 		iMinLength				= 0;
	int 		iMaxLength				= 0;
	char		szTmp[300 + 1]			= "";
	char		szFormatString[100]		= "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	char		szTempFormatString[100]	= "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	char		szDispString[100]		= "                                        ";
	char		szTempDispString[100]	= "                                        ";
	PAAS_BOOL	bWait					= PAAS_TRUE;
	XEVT_PTYPE	pstXevt					= NULL;
	UIREQ_STYPE	stUIReq;

#ifdef DEBUG
	char		szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = EMAIL_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	if(pstPromptDtls)
	{
		iTitle = pstPromptDtls->iTitle;
		if(iTitle <= -1) // Title String available in the Prompt Text
		{
			/* Convert string to UTF string */
			convStrToUTFEscStr(pstPromptDtls->szPromptText, szTmp);
		}
		else //Need to get the title string from iTitle enum
		{
			fetchScrTitle(szTmp, iTitle);
		}
		debug_sprintf(szDbgMsg, "%s: Got Screen Title=%s", __FUNCTION__, szTmp);
		APP_TRACE(szDbgMsg);
		setStringValueWrapper(PAAS_EMAILSCREEN_LBL_1, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);

		/*Getting the current options for the ctrlid of the form*/
		options = getOptionsForCtrlid(PAAS_EMAILSCREEN_KBD_1, stUIReq.pszBuf);
		if(pstPromptDtls->iPromptMaskType == PROMPTMASK_1 || pstPromptDtls->iPromptMaskType == PROMPTMASK_TRUE || pstPromptDtls->iPromptMaskType == PROMPTMASK_T || pstPromptDtls->iPromptMaskType == PROMPTMASK_YES)
		{
			options |= OPT_TB_PWD_MASK; // Enable Password Mask if requested
		}
		else if(pstPromptDtls->iPromptMaskType == PROMPTMASK_TRAILING)
		{
			options |= OPT_TB_DELAYED_PWD_MASK; // Enable Delayed Password Mask if requested
		}

		initUIReqMsg(&stUIReq, BATCH_REQ);

		setShortValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_SHORT_OPTIONS, options, stUIReq.pszBuf);

		iMinLength = pstPromptDtls->iPromptMinChars;
		setShortValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_SHORT_MIN_ENTRIES, iMinLength, stUIReq.pszBuf);

		/* Restricting the format length to iLength*/
		/* Set the format string for the textbox */
		iMaxLength = pstPromptDtls->iPromptMaxChars;
		if(iMaxLength == 0)
		{
			if((iMaxLength = strlen(pstPromptDtls->szPromptFormat)) > 0)
			{
				createFormatString(pstPromptDtls->szPromptFormat, szFormatString);
			}
			else
			{
				iMaxLength = 40;
				sprintf(szFormatString,"%.*s", iMaxLength, szTempFormatString);
			}
		}
		else if(strlen(pstPromptDtls->szPromptFormat) > 0)
		{
			createFormatString(pstPromptDtls->szPromptFormat, szTempFormatString);
			iMaxLength = strlen(szTempFormatString);
			sprintf(szFormatString,"%.*s", iMaxLength, szTempFormatString);
		}
		else
		{
			sprintf(szFormatString,"%.*s", iMaxLength, szTempFormatString);
		}

		setStringValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_STR_FORMAT_STRING, szFormatString, stUIReq.pszBuf);

		sprintf(szDispString,"%.*s", iMaxLength, szTempDispString);
		if(strlen(pstPromptDtls->szPromptFormat))
		{
			/* Covert the prompt data to be populated according to the format string and send back in szDispString*/
			createDisplayString(pstPromptDtls->szPromptFormat, szTempDispString);
			iMaxLength = ((iMaxLength > 40) ? 40 : iMaxLength);
			sprintf(szDispString,"%.*s", iMaxLength, szTempDispString);
		}

		debug_sprintf(szDbgMsg, "%s:  szFormatString [%s], szDispString [%s]", __FUNCTION__,  szFormatString, szDispString);
		APP_TRACE(szDbgMsg);
		/* Set the display string on the textbox */
		setStringValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_STR_DISPLAY_STRING, szDispString, stUIReq.pszBuf);

		/*
		 * Praveen_P1: Kohls Requirement
		 * Stand alone email capture function will be enhanced to include a prepopulated_email tag
		 * If the tag is present, the email address form will populate this value onto the screen
		 */
		if(strlen(pstPromptDtls->szPromptData) > 0)
		{
			setStringValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_STR_INPUT_STRING, pstPromptDtls->szPromptData, stUIReq.pszBuf);
		}

		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_XEVT_RESP)
					{
						pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
						if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
						{
							if(pstXevt->uiCtrlID == PAAS_EMAILSCREEN_KBD_1)
							{
								if(pstXevt->uiKeypadEvt == 2)
								{
									/* Pressed ENTER on keypad */
									debug_sprintf(szDbgMsg, "%s: Value = [%s]",
												__FUNCTION__, pstXevt->szKpdVal);
									APP_TRACE(szDbgMsg);
#if 0 //TODO: Remove it later because No need to memset. strcpy will take care.
									/*
									 * Praveen_P1: For stand alone email, szEmail will be populated
									 * sometimes, need to memset before copying the updated/current value
									 * TO_DO: Doing memset for 41 (which is size of szemail in Loyalty structure
									 */
									memset(szData, 0x00, 41);
#endif
									strcpy(pstPromptDtls->szPromptData, pstXevt->szKpdVal);
									createInputStrWithLiterals(pstPromptDtls);
									rv = SUCCESS;
								}
								else if(pstXevt->uiKeypadEvt == 3)
								{
									/* Pressed CANCEL on keypad */
									debug_sprintf(szDbgMsg, "%s: CANCEL pressed",
																	__FUNCTION__);
									APP_TRACE(szDbgMsg);

									rv = UI_CANCEL_PRESSED;
								}
								else
								{
									debug_sprintf(szDbgMsg, "%s: Invalid Kpd event",
																__FUNCTION__);
									APP_TRACE(szDbgMsg);

									rv = ERR_DEVICE_APP;
								}
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Invalid control Id",
													__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
							}

							bBLDataRcvd = PAAS_FALSE;
							bWait = PAAS_FALSE;
						}
						/*
						 * Praveen_P1: Not waiting only if the response received from the correct form
						 * 			   so moved the below line inside the above if condition
						 */
						//bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: captureDevName
 *
 * Description	: This API would be used to prompt for entering the device name
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int captureDevName(char * szDevName)
{
	int			rv					= SUCCESS;
	int			iDisableFormFlag	= 0;
	int			iDispTime			= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szTmp[100]			= "";
	char		szAppLogData[256]	= "";
	PAAS_BOOL	bWait				= PAAS_TRUE;
	XEVT_PTYPE	pstXevt				= NULL;
	ullong		endTime				= 0L;
	ullong		curTime				= 0L;
	UIREQ_STYPE	stUIReq;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = EMAIL_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	/* Initialise the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTmp, ENTER_DEVNAME_TITLE);
	setStringValueWrapper(PAAS_EMAILSCREEN_LBL_1, PROP_STR_CAPTION, szTmp,
					stUIReq.pszBuf);

	/*Settting the maximum length of device name to capture as 20*/
	setStringValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_STR_FORMAT_STRING, "XXXXXXXXXXXXXXXXXXXX", stUIReq.pszBuf);

	/*
	 * KranthiK1: Pre Populating the device name if it is already existing
	 */
	if(szDevName != NULL && strlen(szDevName) > 0)
	{
		setStringValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_STR_INPUT_STRING, szDevName,
																	stUIReq.pszBuf);

		/* Setting the amount of time the form needs to be shown if
		 * device name is prepopulated
		 */
		iDispTime = getMsgDispIntvl(EMAIL_FRM);

		curTime = svcGetSysMillisec();
		endTime = curTime + (iDispTime * 1000L);
	}

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);


	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Device Name Capture Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == PAAS_EMAILSCREEN_KBD_1)
						{
							if(pstXevt->uiKeypadEvt == 2)
							{
								/* Pressed ENTER on keypad */
								debug_sprintf(szDbgMsg, "%s: Value = [%s]",
										__FUNCTION__, pstXevt->szKpdVal);
								APP_TRACE(szDbgMsg);

								/*
								 * KranthiK1: Device Name will be populated
								 * sometimes, need to memset before copying the updated/current value
								 * TO_DO: Doing memset for 21 (which is size of szemail in Loyalty structure
								 */

								//CID 67212 (#1 of 1): Dereference after null check (FORWARD_NULL). Check for NULL condition. T_RaghavendranR1
								if(szDevName != NULL)
								{
									memset(szDevName, 0x00, 21);
									strcpy(szDevName, pstXevt->szKpdVal);
								}
								rv = SUCCESS;
							}
							else if(pstXevt->uiKeypadEvt == 3)
							{
								/* Pressed CANCEL on keypad */
								debug_sprintf(szDbgMsg, "%s: CANCEL pressed",
										__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = UI_CANCEL_PRESSED;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Invalid Kpd event",
										__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Invalid control Id",
									__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
					/*
					 * Praveen_P1: Not waiting only if the response received from the correct form
					 * 			   so moved the below line inside the above if condition
					 */
					//bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			if(endTime != 0)
			{
				if(curTime > endTime)
				{
					bWait 		= PAAS_FALSE;
				}
				curTime 	= svcGetSysMillisec();
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setTime
 *
 * Description	: This api will set the time given
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int setTime(char * szTimeTxt)
{
	int				rv				= SUCCESS;
	PAAS_BOOL		bWait			= PAAS_TRUE;
	SYSINFO_STYPE	stSysInfo;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);

	setTimeWrapper(szTimeTxt, stUIReq.pszBuf);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XCLOCK_RESP)
				{
					rv = stBLData.iStatus;
					bWait		= PAAS_FALSE;
				}

				bBLDataRcvd = PAAS_FALSE;
				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	/* Get the UI agent and other system details from the UI agent */
	if (rv == SUCCESS)
	{
		rv = getUISysInfo(&stSysInfo);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get VER from UI agent",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		strcpy(szTimeTxt, stSysInfo.szClock);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showCashBackSelectionScreen
 *
 * Description	: This API would be used to ask the users whether they want to
 * 					have Cash Back.
 *
 * Input Params	: None
 *
 * Output Params: UI_YES_SELECTED/UI_NO_SELECTED
 * ============================================================================
 */
int showCashBackSelectionScreen()
{
	int				rv					= SUCCESS;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	//char			szTmp[100]			= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	PAAS_BOOL		bEmvEnabled			= PAAS_FALSE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	int				iDisableFormFlag	= 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = GEN_CFRM_FRM;

	//Get EMV status
	bEmvEnabled = isEmvEnabledInDevice();

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	initUIReqMsg(&stUIReq, BATCH_REQ);
#if 0
	if(bEmvEnabled) //Praveen_P1: Doing clear screen before showing the cash back screen
	{
		memset(szTmp, 0x00, sizeof(szTmp));
		sprintf(szTmp, "%s%c", "XCLS", RS);
		strcat(stUIReq.pszBuf, szTmp);
	}
#endif
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTitle, CBACK_CFRM_TITLE);


	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_GEN_CRFM_LBL_1;
	ctrlIds[1] 	= PAAS_GEN_CRFM_LBL_7;
	ctrlIds[2] 	= PAAS_GEN_CRFM_LBL_8;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);

	/* -------------- Set the labels for the buttons ------------ */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_07);
	setStringValueWrapper(PAAS_GEN_CRFM_LBL_4, PROP_STR_CAPTION,
			(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);

	setStringValueWrapper(PAAS_GEN_CRFM_LBL_3, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	/* Send the batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != 0)
	{
		debug_sprintf(szDbgMsg,"%s: Send Message to FA failed!!!",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Cashback Selection Confirmation Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_GEN_CRFM_FK_YES:
							debug_sprintf(szDbgMsg,"%s: YES", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "User Selected [YES] Option on Cashback Selection Confirmation Screen");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							rv = UI_YES_SELECTED;
							break;

						case PAAS_GEN_CRFM_FK_NO:
							debug_sprintf(szDbgMsg,"%s: NO", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "User Selected [NO] Option on Cashback Selection Confirmation Screen");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							rv = UI_NO_SELECTED;
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Invalid control ID",
																__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
							break;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showCashBackAmtScreen
 *
 * Description	: This API would be used to display the Cash Back Amount Entry
 * 					screen once the user chooses the Other option in Select
 * 					Cashback screen.
 *
 * Input Params	: None
 *
 * Output Params:
 * ============================================================================
 */
int showCashBackAmtScreen(char * pszOutAmt)
{
	int				rv					= SUCCESS;
	int				iDisableFormFlag	= 0;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[256]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	unsigned shOptions = 0;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = NUM_ENTRY_FRM; /* VDR: Temporary fix */

	iDisableFormFlag = 1;

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	//Getting the current options for the ctrlid of the form
	shOptions = getOptionsForCtrlid(PS_NUM_ENTRY_EDT_1, stUIReq.pszBuf );

	//To make sure that the option of right to left entry is enabled.
	shOptions |= OPT_TB_RL_MASK;

	initUIReqMsg(&stUIReq, BATCH_REQ);

	setShortValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_SHORT_OPTIONS, shOptions, stUIReq.pszBuf);

	/* ---------------- Set the labels ----------------- */

	/* Set the Title */
	fetchScrTitle(szTitle, ENTER_CBACK_TITLE);


	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PS_NUM_ENTRY_LBL_1;
	ctrlIds[1] 	= PS_NUM_ENTRY_LBL_11;
	ctrlIds[2] 	= PS_NUM_ENTRY_LBL_12;

	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);

	/* Set the button labels */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_08);
	setStringValueWrapper(PS_NUM_ENTRY_LBL_4, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	fetchBtnLabel(&stBtnLbl, BTN_LBLS_09);
	setStringValueWrapper(PS_NUM_ENTRY_LBL_3, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Set the display string on the textbox */
	setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_DISPLAY_STRING,
														"$      .00", stUIReq.pszBuf);

	/* Set the format string for the textbox */
	setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_FORMAT_STRING,
														"CNNNNNNCCC", stUIReq.pszBuf);
	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	/* Send the batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv == SUCCESS)
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Caskback Amount Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == PS_NUM_ENTRY_EDT_1)
						{
							switch(pstXevt->uiKeypadEvt)
							{
							case 2: /* ON ENTER */
								debug_sprintf(szDbgMsg, "%s: Value = [%s]",
											__FUNCTION__, pstXevt->szKpdVal);
								APP_TRACE(szDbgMsg);

								strcpy(pszOutAmt, pstXevt->szKpdVal);

								rv = UI_ENTER_PRESSED;
								break;

							case 3: /* ON CANCEL */
								debug_sprintf(szDbgMsg, "%s: CANCEL pressed",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = UI_CANCEL_PRESSED;
								break;

							default:
								debug_sprintf(szDbgMsg, "%s: Invalid Kpd event",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
								break;
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Invalid control Id",
												__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showAmtProcDispForm
 *
 * Description	: This API would be called to display message with amount.
 *                On this form will display appropriate prompts/messages.
 *
 * Input Params	: Prompt or message number.
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showAmtProcDispForm(int iPromptMsgNo, int iOption, int timeOut, char *pszTranAmt, int iAmtTitle)
{
	int						rv								= SUCCESS;
	int						locTimeOutVal					= 0;
	int						iDisableFormFlag				= 0;
	int						iAppLogEnabled					= isAppLogEnabled();
	char					szFileName[MAX_FILE_NAME_LEN]	= "";
	char					szAmtMsg[MAX_DISP_MSG_SIZE]		= "";
	char					szAppLogData[300]				= "";
	PAAS_BOOL				bWait							= PAAS_TRUE;
	LABEL_STYPE_TITLE 		stLblTitle;
	char					szTitle1[100]		= "";
	char					szTitle[200]		= "";
	char					szAmtTitle[100]		= "";
	char*  					pszTemp= NULL;
	int 					ctrlIds[4];
	UIREQ_STYPE				stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256]					= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = GEN_DISP_FRM;

	/* Initialize the batch message */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	if(pszTranAmt != NULL && (strlen(pszTranAmt) > 0))
	{
	/* Get the Amount Due Title */
		memset(szTitle, 0x00, sizeof(szTitle));
		memset(szTitle1, 0x00, sizeof(szTitle1));

		if(iOption == STANDBY_ICON)
		{
			fetchScrTitle(szTitle1, TRAN_AMT_TITLE);
		}
		else if(iOption == SUCCESS_ICON)
		{
			switch(iAmtTitle)
			{
			case 1:
			case 4:
				fetchScrTitle(szTitle1, APR_AMT_TITLE);
				break;
			case 2:
				fetchScrTitle(szTitle1, BAL_AMT_TITLE);
				break;
			case 3:
				fetchScrTitle(szTitle1, VOID_AMT_TITLE);
				break;
			}
		}

		if(iAmtTitle == 4)
		{
			sprintf(szTitle, "%s %s", szTitle1, pszTranAmt);
		}
		else
		{
			pszTemp = strchr(pszTranAmt, '-');
			if(pszTemp != NULL)
			{
				pszTranAmt++;
				sprintf(szTitle, "%s -$%s", szTitle1, pszTranAmt);
			}
			else
			{
				sprintf(szTitle, "%s $%s", szTitle1, pszTranAmt);
			}
		}
		/*Setting the ctrlids for the form*/
		ctrlIds[0] 	= PAAS_GEN_DISPLAY_LBL_6;
		ctrlIds[1] 	= PAAS_GEN_DISPLAY_LBL_7;

		/*Setting the screen title to show amount due labels*/
		/*Substracting 10 from the label length to not to overlap with the image on the form*/
		setScreenTitle(szTitle, szAmtTitle, ctrlIds, 1, giLabelLen, stUIReq.pszBuf);
	}

	/* Get the display prompt/message from the .ini file */
	getDisplayMsg(szAmtMsg, iPromptMsgNo);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, szAmtMsg);
		addAppEventLog(SCA, PAAS_INFO, DISPLAY_MESSAGE, szAppLogData, NULL);
	}

	memset(&stLblTitle, 0x00, sizeof(stLblTitle));

	//Split title string of required length(i.e Based on maxLabelLength defined for Mx915 or Mx925).
	splitStringOfReqLength(szAmtMsg, giLabelLen, &stLblTitle);
	debug_sprintf(szDbgMsg, "%s: szTitle1 =%s, szTitle2 =%s, szTitle3 =%s",__FUNCTION__,
			stLblTitle.szTitle1, stLblTitle.szTitle2, stLblTitle.szTitle3);
	APP_TRACE(szDbgMsg);

	/*Set the title */
	if ( strlen(stLblTitle.szTitle3) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_1, PROP_STR_CAPTION,
												stLblTitle.szTitle1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
												stLblTitle.szTitle2, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_5, PROP_STR_CAPTION,
												stLblTitle.szTitle3, stUIReq.pszBuf);
	}
	else if ( strlen(stLblTitle.szTitle2) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
												stLblTitle.szTitle1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_5, PROP_STR_CAPTION,
												stLblTitle.szTitle2, stUIReq.pszBuf);
	}
	else if ( strlen(stLblTitle.szTitle1) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
												stLblTitle.szTitle1, stUIReq.pszBuf);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Title not found",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* Choose the image file */
	switch(iOption)
	{
	case STANDBY_ICON:
		strcpy(szFileName, "indicator_yellow.png");
		getFileNamewithPlatformPrefix(szFileName);

		break;

	case SUCCESS_ICON:
		strcpy(szFileName, "indicator_green.png");
		getFileNamewithPlatformPrefix(szFileName);

		break;

	case FAILURE_ICON:
		strcpy(szFileName, "indicator_red.png");
		getFileNamewithPlatformPrefix(szFileName);

		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should never come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	if(rv == SUCCESS)
	{
		/* Set the image on the screen */
		setStringValueWrapper(PAAS_GEN_DISPLAY_IMG_1, PROP_STR_IMAGE_FILE_NAME,
				szFileName, stUIReq.pszBuf);

		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		/* Send batch message to UI agent */
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if( rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(15);
			}
		}

		locTimeOutVal = timeOut;

		if(locTimeOutVal > 0)
		{
			svcWait(locTimeOutVal);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showPSGenDispForm
 *
 * Description	: This API would be called to  general display screen screen.
 *                On this form will display appropriate prompts/messages.
 *
 * Input Params	: Prompt or message number.
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showPSGenDispForm(int iPromptMsgNo, int iOption, int timeOut)
{
	int						rv								= SUCCESS;
	int						locTimeOutVal					= 0;
	int						iDisableFormFlag				= 0;
	int						iAppLogEnabled					= isAppLogEnabled();
	char					szFileName[MAX_FILE_NAME_LEN]	= "";
	char 					szMsg[MAX_DISP_MSG_SIZE]		= "";
	char					szAppLogData[300]				= "";
	PAAS_BOOL				bWait							= PAAS_TRUE;
	LABEL_STYPE_TITLE 		stLblTitle;
	UIREQ_STYPE				stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256]					= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = GEN_DISP_FRM;

	/* Initialize the batch message */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Get the display prompt/message from the .ini file */
	getDisplayMsg(szMsg, iPromptMsgNo);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, szMsg);
		addAppEventLog(SCA, PAAS_INFO, DISPLAY_MESSAGE, szAppLogData, NULL);
	}

	memset(&stLblTitle, 0x00, sizeof(stLblTitle));

	//Split title string of required length(i.e Based on maxLabelLength defined for Mx915 or Mx925).
	splitStringOfReqLength(szMsg, giLabelLen-5, &stLblTitle);
	debug_sprintf(szDbgMsg, "%s: szTitle1 =%s, szTitle2 =%s, szTitle3 =%s",__FUNCTION__,
			stLblTitle.szTitle1, stLblTitle.szTitle2, stLblTitle.szTitle3);
	APP_TRACE(szDbgMsg);

	/*Set the title */
	if ( strlen(stLblTitle.szTitle3) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_1, PROP_STR_CAPTION,
												stLblTitle.szTitle1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
												stLblTitle.szTitle2, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_5, PROP_STR_CAPTION,
												stLblTitle.szTitle3, stUIReq.pszBuf);
	}
	else if ( strlen(stLblTitle.szTitle2) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
												stLblTitle.szTitle1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_5, PROP_STR_CAPTION,
												stLblTitle.szTitle2, stUIReq.pszBuf);
	}
	else if ( strlen(stLblTitle.szTitle1) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
												stLblTitle.szTitle1, stUIReq.pszBuf);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Title not found",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* Choose the image file */
	switch(iOption)
	{
	case STANDBY_ICON:
		strcpy(szFileName, "indicator_yellow.png");
		getFileNamewithPlatformPrefix(szFileName);

		break;

	case SUCCESS_ICON:
		strcpy(szFileName, "indicator_green.png");
		getFileNamewithPlatformPrefix(szFileName);

		break;

	case FAILURE_ICON:
		strcpy(szFileName, "indicator_red.png");
		getFileNamewithPlatformPrefix(szFileName);

		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should never come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	if(rv == SUCCESS)
	{
		/* Set the image on the screen */
		setStringValueWrapper(PAAS_GEN_DISPLAY_IMG_1, PROP_STR_IMAGE_FILE_NAME,
				szFileName, stUIReq.pszBuf);

		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here
		/* Send batch message to UI agent */
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if( rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(15);
			}
		}

		locTimeOutVal = timeOut;

		if(locTimeOutVal > 0)
		{
			svcWait(locTimeOutVal);
		}

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showPSMessageDispForm
 *
 * Description	: This API would be called to  general display screen screen.
 *                On this form will display appropriate prompts/messages.
 *
 * Input Params	: Prompt or message number.
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showPSMessageDispForm(char *pszMsg, int iPromptMsg, int iOption, int timeOut)
{
	int						rv								= SUCCESS;
	int						locTimeOutVal					= 0;
	int						iDisableFormFlag				= 0;
	int						iAppLogEnabled					= isAppLogEnabled();
	char					szFileName[MAX_FILE_NAME_LEN]	= "";
	char 					szMsg1[MAX_DISP_MSG_SIZE]		= "";
	char 					szMsg2[MAX_DISP_MSG_SIZE]		= "";
	char 					szDispMsg[MAX_DISP_MSG_SIZE]	= "";
	char					szAppLogData[300]				= "";
	PAAS_BOOL				bWait							= PAAS_TRUE;
	LABEL_STYPE_TITLE 		stLblTitle;
	UIREQ_STYPE				stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256]					= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = GEN_DISP_FRM;

	/* Initialize the batch message */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Get the display prompt/message from the .ini file */

	if(iPromptMsg == MSG_POS_ACK_RECEIVED)
	{
		getDisplayMsg(szMsg1, MSG_POS_ACK_RECEIVED);
		sprintf(szDispMsg, "%s (POS IP : %s)",  szMsg1, pszMsg);
	}
	else if(iPromptMsg == MSG_BROADCASTED_TO_POS)
	{
		getDisplayMsg(szMsg1, MSG_BROADCASTED_TO_POS);
		getDisplayMsg(szMsg2, MSG_SECS_REMAINING_FOR_POS_ACK);
		sprintf(szDispMsg, "%s %s%s",  szMsg1, pszMsg, szMsg2);
	}
	else if(iPromptMsg == MSG_VSPREG_FAILED || iPromptMsg == MSG_DEVREG_FAILED || iPromptMsg == MSG_DUMMYSALE_FAILED ||
			iPromptMsg == MSG_DUMMYVOID_FAILED || iPromptMsg ==	MSG_EMV_INIT_ERR)
	{
		getDisplayMsg(szMsg1, iPromptMsg);
		sprintf(szDispMsg, "%s. %s", szMsg1, pszMsg);
	}
	else if(iPromptMsg == MSG_TRAN_AMT_LESS_THAN_CB_TIP_TAX)
	{
		//Here the Message itself is framed by the caller function. So Directly copying the Message
		sprintf(szDispMsg, "%s", pszMsg);
	}
	else if(iPromptMsg == MSG_VSP_DDK_ADV_RETRY)
	{
		//Here the Message itself is framed by the caller function. So Directly copying the Message
		sprintf(szDispMsg, "%s", pszMsg);
	}
	else
	{
		getDisplayMsg(szDispMsg, iPromptMsg);
	}

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, szDispMsg);
		addAppEventLog(SCA, PAAS_INFO, DISPLAY_MESSAGE, szAppLogData, NULL);
	}

	memset(&stLblTitle, 0x00, sizeof(stLblTitle));

	//Split title string of required length(i.e Based on maxLabelLength defined for Mx915 or Mx925).
	splitStringOfReqLength(szDispMsg, giLabelLen + 2, &stLblTitle);
	debug_sprintf(szDbgMsg, "%s: szTitle1 =%s, szTitle2 =%s, szTitle3 =%s",__FUNCTION__,
			stLblTitle.szTitle1, stLblTitle.szTitle2, stLblTitle.szTitle3);
	APP_TRACE(szDbgMsg);

	/*Set the title */
	if ( strlen(stLblTitle.szTitle3) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_1, PROP_STR_CAPTION,
												stLblTitle.szTitle1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
												stLblTitle.szTitle2, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_5, PROP_STR_CAPTION,
												stLblTitle.szTitle3, stUIReq.pszBuf);
	}
	else if ( strlen(stLblTitle.szTitle2) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
												stLblTitle.szTitle1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_5, PROP_STR_CAPTION,
												stLblTitle.szTitle2, stUIReq.pszBuf);
	}
	else if ( strlen(stLblTitle.szTitle1) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
												stLblTitle.szTitle1, stUIReq.pszBuf);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Title not found",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* Choose the image file */
	switch(iOption)
	{
	case STANDBY_ICON:
		strcpy(szFileName, "indicator_yellow.png");
		getFileNamewithPlatformPrefix(szFileName);

		break;

	case SUCCESS_ICON:
		strcpy(szFileName, "indicator_green.png");
		getFileNamewithPlatformPrefix(szFileName);

		break;

	case FAILURE_ICON:
		strcpy(szFileName, "indicator_red.png");
		getFileNamewithPlatformPrefix(szFileName);

		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should never come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	if(rv == SUCCESS)
	{
		/* Set the image on the screen */
		setStringValueWrapper(PAAS_GEN_DISPLAY_IMG_1, PROP_STR_IMAGE_FILE_NAME,
				szFileName, stUIReq.pszBuf);

		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here
		/* Send batch message to UI agent */
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if( rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(5);
			}
		}

		locTimeOutVal = timeOut;

		if(locTimeOutVal > 0)
		{
			svcWait(locTimeOutVal);
		}

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: showNetworkInfoScreen
 *
 * Description	: This API will be Called to Display the Network information Details.
 * The dEVICE IP, IP Mode (DHCP/Static), Device name (optional) are Displayed in the Screen.
 * MAC Address and Device Sl No are also Display with Broadcast Option, if Terminal Identification to the Network is Enabled.
 *
 * Input Params	: N/W mode and IP address, Device Name, MAC Addr.
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showNetworkInfoScreen(PAAS_BOOL bDHCP, char * pszIp, char* pszDevName, char * pszMACAddr)
{
	int				rv						= SUCCESS;
	int				iLen					= 0;
	int				iDispTime				= DFLT_IP_SHOW_TIME;
	int 			ctrlIds[4];
	int				iDisableFormFlag		= PAAS_TRUE;
	int				iAppLogEnabled			= isAppLogEnabled();
	char			szTmp[100]				= "";
	char			szIP[50]				= "";
	char			szIPMode[50]			= "";
	char			szDevNameTitle[100]		= "";
	char			szMacAddrTitle[100]		= "";
	char			szDevSrlNoTitle[100]	= "";
	char			szTitle[100]			= "";
	char			szDevSrlNum[21]			= "";
	char			szAmtTitle[100]			= "";
	char			szAppLogData[300]		= "";
	ullong			endTime					= 0L;
	ullong			curTime					= 0L;
	PAAS_BOOL		bEnterPressed			= PAAS_FALSE;
	PAAS_BOOL		bBroadcastIdenPressed	= PAAS_FALSE;
	XEVT_PTYPE		pstXevt					= NULL;
	BTNLBL_STYPE	stBtnLbl;
	PAAS_BOOL		bWait					= PAAS_TRUE;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = IP_DISP_FRM;


	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the title */
	fetchScrTitle(szTitle, NWKINFO_DISP_TITLE);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PS_IP_ADDRESS_DISPLAY_LBL_5;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);

	/* ---------------- Set the labels ----------------- */
	memset(&stBtnLbl, 0x00, sizeof(BTNLBL_STYPE));
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_12);

	/* Find out the max string length to append that many spaces to other
	 * strings to display properly aligned strings on screen.*/
	strcpy(szIP, (char *)stBtnLbl.szLblOne);
	strcpy(szIPMode, (char *)stBtnLbl.szLblTwo);

	iLen = strlen(szIP);
	if(strlen(szIPMode) > iLen)
	{
		iLen = strlen(szIPMode);
	}

	memset(&stBtnLbl, 0x00, sizeof(BTNLBL_STYPE));
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_31);
	strcpy(szMacAddrTitle, (char*)stBtnLbl.szLblOne);
	if(strlen(szMacAddrTitle) > iLen)
	{
		iLen = strlen(szMacAddrTitle);
	}

	strcpy(szDevSrlNoTitle, (char*)stBtnLbl.szLblTwo);
	if(strlen(szDevSrlNoTitle) > iLen)
	{
		iLen = strlen(szDevSrlNoTitle);
	}

	/* T_RaghavendranR1: When the Terminal Identification Interval is Present. We need to Show Initiate Broadcast to the Network Screen.
	 * Refer FRD 3.62 : When the Broadcast to the Network is Initiated, The Terminal will Broadcast the Network Information.
	 * Network Data will be Picked by the Listening POS in the Network and send the Acknowledgment to the Terminal
	 * it wishes to connect with. By This Way the POS will come to know the IP Address of the Terminal avoiding the Manual Entry.
	 * */
	if(getTermIdenDisplayInterval() > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Need to Initiate Terminal Identification Screen", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Terminal Identification is Enabled, Showing Initiate Broadcast To Network Screen.");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		/* Get time interval to Show Initiate Broadcast Screen */
		iDispTime = getTermIdenDisplayInterval();

		if(strlen(pszMACAddr) > 0)
		{
			appendSpacesToString(szMacAddrTitle, iLen - strlen(szMacAddrTitle));

			sprintf(szTmp, "%s: %s", szMacAddrTitle, pszMACAddr);
			strcpy(szMacAddrTitle, szTmp);

			setStringValueWrapper(PS_IP_ADDRESS_DISPLAY_LBL_7, PROP_STR_CAPTION, szMacAddrTitle, stUIReq.pszBuf);
		}

		svcInfoSerialNum(szDevSrlNum);
		if(strlen(szDevSrlNum) > 0)
		{

			appendSpacesToString(szDevSrlNoTitle, iLen - strlen(szDevSrlNoTitle));

			sprintf(szTmp, "%s: %s", szDevSrlNoTitle, szDevSrlNum);
			strcpy(szDevSrlNoTitle, szTmp);

			if(strlen(pszDevName) > 0)
			{
				setStringValueWrapper(PS_IP_ADDRESS_DISPLAY_LBL_4, PROP_STR_CAPTION, szDevSrlNoTitle, stUIReq.pszBuf);
			}
			else
			{
				setStringValueWrapper(PS_IP_ADDRESS_DISPLAY_LBL_8, PROP_STR_CAPTION, szDevSrlNoTitle, stUIReq.pszBuf);
			}
		}

		setBoolValueWrapper(PS_IP_ADDRESS_DISPLAY_FK_2, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);

		setBoolValueWrapper(PS_IP_ADDRESS_DISPLAY_FK_2, PROP_BOOL_ENABLE, 1, stUIReq.pszBuf);

		memset(&stBtnLbl, 0x00, sizeof(BTNLBL_STYPE));
		fetchBtnLabel(&stBtnLbl, BTN_LBLS_32);
		sprintf(szTmp, "%s", (char *) stBtnLbl.szLblOne);
		setStringValueWrapper(PS_IP_ADDRESS_DISPLAY_LBL_12, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Device Network Information Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}

		/* Get time interval to show ip and n/w mode info screen */
		iDispTime = getMsgDispIntvl(whichForm);

		setBoolValueWrapper(PS_IP_ADDRESS_DISPLAY_FK_2, PROP_BOOL_ENABLE, 0, stUIReq.pszBuf);
	}

	if(strlen(pszDevName) > 0)
	{
		memset(&stBtnLbl, 0x00, sizeof(BTNLBL_STYPE));
		fetchBtnLabel(&stBtnLbl, BTN_LBLS_25);
		strcpy(szDevNameTitle, (char*)stBtnLbl.szLblOne);

		if(strlen(szDevNameTitle) > iLen)
		{
			iLen = strlen(szDevNameTitle);
		}
		appendSpacesToString(szDevNameTitle, iLen - strlen(szDevNameTitle));

		sprintf(szTmp, "%s: %s", szDevNameTitle, pszDevName);
		strcpy(szDevNameTitle, szTmp);

		setStringValueWrapper(PS_IP_ADDRESS_DISPLAY_LBL_8, PROP_STR_CAPTION,
													szDevNameTitle, stUIReq.pszBuf);
	}

	/* Format the strings */
	appendSpacesToString(szIP, iLen - strlen(szIP));
	appendSpacesToString(szIPMode, iLen - strlen(szIPMode));

	sprintf(szTmp, "%s: %s", szIP, pszIp);
	strcpy(szIP, szTmp);

	memset(&stBtnLbl, 0x00, sizeof(BTNLBL_STYPE));
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_01);
	if(bDHCP == PAAS_TRUE)
	{
		sprintf(szTmp, "%s: %s", szIPMode, (char *) stBtnLbl.szLblTwo);
		strcpy(szIPMode, szTmp);
	}
	else
	{
		sprintf(szTmp, "%s: %s", szIPMode, (char *) stBtnLbl.szLblOne);
		strcpy(szIPMode, szTmp);
	}

	if(iAppLogEnabled == 1)
	{
		sprintf(szAppLogData, "Obtained Network Configuration [%s] and [%s]", szIPMode, szIP);
		addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
	}

	setStringValueWrapper(PS_IP_ADDRESS_DISPLAY_LBL_1, PROP_STR_CAPTION, szIP,
																stUIReq.pszBuf);
	setStringValueWrapper(PS_IP_ADDRESS_DISPLAY_LBL_3, PROP_STR_CAPTION,
													szIPMode, stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	while(1)
	{
		bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
								__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
			break;
		}
		else //Praveen_P1: After showing the screen we are not waiting for the response of XBATCH here
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);
			}
		}

		curTime = svcGetSysMillisec();
		endTime = curTime + (iDispTime * 1000L);

		/* Wait for the user button press or wait for 30sec */
		while(endTime > curTime)
		{
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == PS_IP_ADDRESS_DISPLAY_FK_1)
						{
							debug_sprintf(szDbgMsg, "%s: Enter PRESSED",
											__FUNCTION__);
							APP_TRACE(szDbgMsg);

							bEnterPressed = PAAS_TRUE;

							rv = SUCCESS;
						}
						else if(pstXevt->uiCtrlID == PS_IP_ADDRESS_DISPLAY_FK_2)
						{
							debug_sprintf(szDbgMsg, "%s: Initiate Identification PRESSED",
											__FUNCTION__);
							APP_TRACE(szDbgMsg);

							bBroadcastIdenPressed = PAAS_TRUE;

							rv = SUCCESS;
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Invalid control id",
											__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = FAILURE;
						}

						bBLDataRcvd = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bEnterPressed == PAAS_TRUE || bBroadcastIdenPressed == PAAS_TRUE || (rv != SUCCESS))
			{
				break;
			}

			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			curTime = svcGetSysMillisec();
		}

		if(bBroadcastIdenPressed == PAAS_TRUE)
		{
			rv = broadcastTermIdenAndWaitForPOSACK(pszIp, szDevSrlNum, pszMACAddr, pszDevName);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Broadcast the Terminal Identification to Network", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: broadcastTermIdenAndWaitForPOSACK
 *
 * Description	: This function will initiate Broadcasting Terminal Information and waits for the acknowledgment to come from POS after broadcasting the Data to network.
 * Now Terminal is listening for the data.
 *
 * Input Params	: N/W mode and IP address.
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int broadcastTermIdenAndWaitForPOSACK(char *pszIP, char *pszDevSrlNum,  char *pszMACAddr, char *pszDevName)
{
	int					rv							= SUCCESS;
	int 				iSocket						= -1;
	int					iMsgBytes					= 0;
	int					iStatusMsgDispInterval		= 0;
	int					iPOSACKIntv					= 0;
	int					iWaitTime					= 0;
	int					iAppLogEnabled				= isAppLogEnabled();
	char				szAppLogData[300]			= "";
	char				*pszSerialNum				= NULL;
	char 				szDispMsg[MAX_DISP_MSG_SIZE]= "";
	char 				szBufData[TERM_IDEN_BUFLEN]	= "";
	socklen_t 			slen						= 0;
	PAAS_BOOL			bRetry						= PAAS_FALSE;
	struct 	timeval 	tv;
	struct  sockaddr_in server_address;
	struct  sockaddr_in client_address;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iStatusMsgDispInterval = getStatusMsgDispInterval();
	while(1)
	{

		if((iSocket=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)
		{
			debug_sprintf(szDbgMsg, "%s: POS ACK Socket Creation Failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			return rv;
		}

		memset((char *) &server_address, 0, sizeof(server_address));
		server_address.sin_family = AF_INET;
		server_address.sin_port = htons(getPOSACKPort());
		if (inet_aton(pszIP, &server_address.sin_addr)==0)
		{
			debug_sprintf(szDbgMsg, "%s: inet_aton Failure on POS_ACK", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(&tv, 0x00, sizeof(tv));
		tv.tv_sec = 1;
		if (setsockopt(iSocket, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to set the Timeout for POS_ACK", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;

		}

		if (bind(iSocket, (struct sockaddr *) &server_address, sizeof(server_address))==-1)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to bind on POS_ACK", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = broadcastTerminalIdentification(pszIP, pszDevSrlNum, pszMACAddr, pszDevName);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Broadcast the Terminal Identification to Network", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			showPSGenDispForm(MSG_BROADCAST_TO_POS_FAILED, FAILURE_ICON, iStatusMsgDispInterval);

			rv = FAILURE;
			break;
		}

		slen = sizeof(client_address);
		iPOSACKIntv = getTermIdenPOSACKInterval();
		sprintf(szDispMsg, "%d", iPOSACKIntv);
		showPSMessageDispForm(szDispMsg, MSG_BROADCASTED_TO_POS, STANDBY_ICON, 0);

		memset(szBufData, 0x00, sizeof(szBufData));

		iPOSACKIntv--;
		while(iPOSACKIntv)
		{
			bRetry = PAAS_FALSE;
			sprintf(szDispMsg, "%d", iPOSACKIntv);
			updatePOSACKIntvSecsInScreen(szDispMsg);
			if ((iMsgBytes = recvfrom(iSocket, szBufData, TERM_IDEN_BUFLEN, 0, (struct sockaddr *) &client_address, &slen)) == -1)
			{

				debug_sprintf(szDbgMsg, "%s: Failed to Receive Data during POS_ACK", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				bRetry = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Read [%d] bytes from IP %s:%d [%s]", __FUNCTION__, iMsgBytes, inet_ntoa(client_address.sin_addr), ntohs(client_address.sin_port), szBufData);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Acknowledgment Packet Received From POS.");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}

				if(iMsgBytes && (strstr(szBufData, "serial") != NULL) && ((pszSerialNum = strchr(szBufData, '=')) != NULL))
				{
					pszSerialNum = pszSerialNum + 1;
					stripSpaces(pszSerialNum, STRIP_TRAILING_SPACES | STRIP_LEADING_SPACES);

					debug_sprintf(szDbgMsg, "%s: Serial No received From POS [%s], Device Serial No [%s]", __FUNCTION__, pszSerialNum, pszDevSrlNum);
					APP_TRACE(szDbgMsg);

					if(strcmp(pszSerialNum, pszDevSrlNum) == SUCCESS)
					{
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "Serial No of the Terminal Matches With the One Sent From POS.");
							addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
						}

						showPSMessageDispForm((char*)inet_ntoa(client_address.sin_addr),  MSG_POS_ACK_RECEIVED, SUCCESS_ICON, iStatusMsgDispInterval);

						debug_sprintf(szDbgMsg, "%s: Sl No of the Device Matches with the one Sent to Network", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						bRetry = PAAS_FALSE;
						break;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid Sl No Received From POS", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "Invalid Serial No Received From POS in the ACK Packet. Showing Retry Screen.");
							addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, NULL);
						}

						bRetry = PAAS_TRUE;
						break;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Invalid Ack data Received From POS", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Invalid ACK Packet Received From POS. Showing Retry Screen.");
						addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, NULL);
					}

					bRetry = PAAS_TRUE;
					break;
				}
			}
			iPOSACKIntv--;
		}

		if(bRetry == PAAS_TRUE)
		{
			bRetry = PAAS_FALSE;

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "POS ACK For Terminal Identification Not Received, Showing  Retry Screen");
				addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
			}

			iWaitTime = getTermIdenDisplayInterval();
			rv = showPSGenMsgWithRetryScreen(MSG_POS_ACK_NOT_RECEIVED, iWaitTime);
			if(rv == UI_YES_SELECTED)
			{
				debug_sprintf(szDbgMsg, "%s: User wants to Re-Try Terminal Identification Again", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				close(iSocket);

				continue;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get the POS ACK for Terminal Identification", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		break;
	}


	close(iSocket);
	return rv;
}

/*
 * ============================================================================
 * Function Name: broadcastTerminalIdentification
 *
 * Description	: This API would be called to  broadcast the Terminal Network Details and Sl No to the network.
 * The POS in the network listening for the data will pick it and act upon it
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int broadcastTerminalIdentification(char *pszIP, char *pszDevSrlNum,  char *pszMACAddr, char *pszDevName)
{
	int				rv								= SUCCESS;
	int 			iSocket							= -1;
	int 			isendStrLen 						= 0;
	int				iPort							= 0;
	int 			iBroadcastOn 					= 1;
	int				iBroadcastCnt					= 3;
	int				iAppLogEnabled					= isAppLogEnabled();
	char			szAppLogData[300]				= "";
	char			szAppLogDiag[300]				= "";
	char 			szBufData[TERM_IDEN_BUFLEN]	 	= "";
	char 			szTempBuf[TERM_IDEN_BUFLEN]	 	= "";
	struct 			sockaddr_in 					server_address;
#ifdef DEBUG
	char			szDbgMsg[256]					= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if ((iSocket=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
	{
		debug_sprintf(szDbgMsg, "%s: Socket Creation  Failure", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	debug_sprintf(szDbgMsg, "%s: Socket Created", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(setsockopt(iSocket, SOL_SOCKET, SO_BROADCAST, &iBroadcastOn, 4) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: setsockopt Failure", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Socket Opt Set", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		memset((char *) &server_address, 0, sizeof(server_address));
		server_address.sin_family = AF_INET;
		iPort = getTermIdenBroadcastPort();
		server_address.sin_port = htons(iPort);
		if (inet_aton(BROADCAST_SERVER_IP, &server_address.sin_addr)==0)
		{
			debug_sprintf(szDbgMsg, "%s: inet_aton Failure", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szTempBuf, 0x00, sizeof(szTempBuf));
		memset(szBufData, 0x00, sizeof(szBufData));
		if(strlen(pszDevSrlNum) > 0)
		{
			strcpy(szTempBuf, "serial = ");
			strcat(szTempBuf, pszDevSrlNum);
			strcat(szTempBuf, " ");
		}

		if(strlen(pszMACAddr) > 0)
		{
			strcat(szTempBuf, "mac = ");
			strcat(szTempBuf, pszMACAddr);
			strcat(szTempBuf, " ");
		}

		if(strlen(pszDevName) > 0)
		{
			strcat(szTempBuf, "devname = ");
			strcat(szTempBuf, pszDevName);
			strcat(szTempBuf, " ");
		}

		sprintf(szBufData, "%sack_port = %d", szTempBuf, POS_ACK_PORT);
		isendStrLen = strlen(szBufData);

		debug_sprintf(szDbgMsg, "%s: Broadcast Packet to Network [%s] ", __FUNCTION__, szBufData);
		APP_TRACE(szDbgMsg);
		while(iBroadcastCnt)
		{
			if (sendto(iSocket, szBufData, isendStrLen + 1, 0, (struct sockaddr *) &server_address, sizeof(server_address)) == -1)
			{
				iBroadcastCnt--; // Retry for 3 times and break with error
			}
			else
			{
				break; // Broadcast Success
			}
		}

		if(iBroadcastCnt == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Broadcasting Packet to POS Failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Broadcast the Network Information.");
				strcpy(szAppLogDiag, "Socket sendto Failed.");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE; // Broadcast Failed
			break;
		}

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Broadcasted Terminal Network Information. Waiting For the Acknowledgment From POS.");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		break;
	}

	close(iSocket);
	return rv;
}

/*
 * ============================================================================
 * Function Name: updatePOSACKIntvSecsInScreen
 *
 * Description	: This will update the time in secs POSACK Interval in the screen.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updatePOSACKIntvSecsInScreen(char* pszMsg)
{
	int						rv								= SUCCESS;
	char 					szMsg1[MAX_DISP_MSG_SIZE]		= "";
	char 					szMsg2[MAX_DISP_MSG_SIZE]		= "";
	char 					szDispMsg[MAX_DISP_MSG_SIZE]	= "";
	PAAS_BOOL				bWait							= PAAS_TRUE;
	LABEL_STYPE_TITLE 		stLblTitle;
	UIREQ_STYPE				stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256]					= "";
#endif

	initUIReqMsg(&stUIReq, BATCH_REQ);

	getDisplayMsg(szMsg1, MSG_BROADCASTED_TO_POS);
	getDisplayMsg(szMsg2, MSG_SECS_REMAINING_FOR_POS_ACK);
	sprintf(szDispMsg, "%s %s%s",  szMsg1, pszMsg, szMsg2);

	memset(&stLblTitle, 0x00, sizeof(stLblTitle));

	//Split title string of required length(i.e Based on maxLabelLength defined for Mx915 or Mx925).
	splitStringOfReqLength(szDispMsg, giLabelLen + 2, &stLblTitle);
	debug_sprintf(szDbgMsg, "%s: szTitle1 =%s, szTitle2 =%s, szTitle3 =%s",__FUNCTION__,
			stLblTitle.szTitle1, stLblTitle.szTitle2, stLblTitle.szTitle3);
	APP_TRACE(szDbgMsg);

	/*Set the title */
	if ( strlen(stLblTitle.szTitle3) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_1, PROP_STR_CAPTION,
				stLblTitle.szTitle1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
				stLblTitle.szTitle2, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_5, PROP_STR_CAPTION,
				stLblTitle.szTitle3, stUIReq.pszBuf);
	}
	else if ( strlen(stLblTitle.szTitle2) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
				stLblTitle.szTitle1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_5, PROP_STR_CAPTION,
				stLblTitle.szTitle2, stUIReq.pszBuf);
	}
	else if ( strlen(stLblTitle.szTitle1) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION,
				stLblTitle.szTitle1, stUIReq.pszBuf);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Title not found",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here
	/* Send batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if( rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			svcWait(5);
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: show4DigitPOSPinEntryScreen
 *
 * Description	: This API would be called to show the PIN entry screen for POS
 * 					register/unregister; and to capture the PIN entered/ CANCEL
 * 					pressed event by the user.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/ERR_DEVICE_APP/UI_CANCEL_PRESSED
 * ============================================================================
 */
int show4DigitPOSPinEntryScreen(char * szPIN, int iTitle)
{
	int				rv					= SUCCESS;
	int 			ctrlIds[4];
	int				iDisableFormFlag	= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTitle[100]		= "";
	char			szAmtTitle[100]		= "";
	char			szAppLogData[300]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = POS_PIN_FRM;

	/* Initialize the form */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTitle, iTitle);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_POS_PIN_LBL_1;
	ctrlIds[1] 	= PAAS_POS_PIN_LBL_3;
	ctrlIds[2] 	= PAAS_POS_PIN_LBL_4;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);

	/* Setting Minimum entries as 4 */
	setShortValueWrapper(PAAS_POS_PIN_EDT_1, PROP_SHORT_MIN_ENTRIES, 4, stUIReq.pszBuf);

	/* Setting Maximum entries as 4 */
	setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_DISPLAY_STRING, "    ", stUIReq.pszBuf);
	setStringValueWrapper(PAAS_POS_PIN_EDT_1, PROP_STR_FORMAT_STRING,  "NNNN", stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	/* Send the batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing 4-Digit POS PIN Entry Screen for Authentication");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiKeypadEvt)
						{
						case 2: /* On Enter */
							sprintf(szPIN, "%s", pstXevt->szKpdVal);

							rv = SUCCESS;
							break;

						case 3: /* ON CANCEL */
							/* Upon pressing CANCEL on the keypad */
							debug_sprintf(szDbgMsg, "%s: User pressed CANCEL",
											__FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "User Pressed Cancel Button on POS Register PIN Entry Screen");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}

							rv = UI_CANCEL_PRESSED;
							break;

						default:
							/* Unknown keypad event for this screen */
							rv = ERR_DEVICE_APP;
							break;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: show8DigitPOSPinEntryScreen
 *
 * Description	: This API would be called to show the PIN entry screen for POS
 * 					register/unregister; and to capture the PIN entered/ CANCEL
 * 					pressed event by the user.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/ERR_DEVICE_APP/UI_CANCEL_PRESSED
 * ============================================================================
 */
int show8DigitPOSPinEntryScreen(char * szPIN, int iTitle)
{
	int				rv						= SUCCESS;
	int				i						= 0;
	int 			ctrlIds[4];
	int				iDisableFormFlag		= 0;
	int				iAppLogEnabled			= isAppLogEnabled();
	char			szTitle[100]			= "";
	char			szAmtTitle[100]			= "";
	char			szAppLogData[300]		= "";
	char			szFormatString[41]		= "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	char			szTempFormatString[41]	= "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	PAAS_BOOL		bWait					= PAAS_TRUE;
	XEVT_PTYPE		pstXevt					= NULL;
	int				iMaxLength				= 8;
	int				iMinLength				= 8;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = EMAIL_FRM;

	/* Initialize the form */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTitle, iTitle);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_EMAILSCREEN_LBL_1;
/*	ctrlIds[1] 	= PAAS_POS_PIN_LBL_3;
	ctrlIds[2] 	= PAAS_POS_PIN_LBL_4;*/

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 1, giLabelLen, stUIReq.pszBuf);

	setShortValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_SHORT_MIN_ENTRIES, iMinLength, stUIReq.pszBuf);

	/* Restricting the format length to iLength*/
	sprintf(szFormatString,"%.*s", iMaxLength, szTempFormatString);

	/*Settting the maximum length as 8*/
	setStringValueWrapper(PAAS_EMAILSCREEN_KBD_1, PROP_STR_FORMAT_STRING, szFormatString, stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	/* Send the batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing 8-Digit POS PIN Entry Screen for Authentication");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == PAAS_EMAILSCREEN_KBD_1)
						{
							if(pstXevt->uiKeypadEvt == 2)
							{
								/* Pressed ENTER on keypad */
								debug_sprintf(szDbgMsg, "%s: Value = [%s]",
											__FUNCTION__, pstXevt->szKpdVal);
								APP_TRACE(szDbgMsg);

								strcpy(szPIN, pstXevt->szKpdVal);
								for (i = 0; i < 4; i++)
								{
									szPIN[i] = toupper(szPIN[i]);
								}
								debug_sprintf(szDbgMsg, "%s: After converting to upper case Value = [%s]",
											__FUNCTION__, szPIN);
								APP_TRACE(szDbgMsg);
								rv = SUCCESS;
							}
							else if(pstXevt->uiKeypadEvt == 3)
							{
								/* Pressed CANCEL on keypad */
								debug_sprintf(szDbgMsg, "%s: CANCEL pressed",
															__FUNCTION__);
								APP_TRACE(szDbgMsg);
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Pressed Cancel Button on PIN details Entry Screen");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								rv = UI_CANCEL_PRESSED;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Invalid Kpd event",
														__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Invalid control Id",
												__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
						}


						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPaypalPaymentCodeDtlsFrmUser()
 *
 * Description	: This API would be used to prompt the users to enter either
 * 					their PayPal Payment Code or swipe their Paypal cards. It will
 * 					be called if the paypal tender capture is enabled in the
 * 					configuration.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getPaypalPaymentCodeDtlsFrmUser(char * pszPymtCode, char ** pszTracks, char ** pszRSATracks, char** szVsdEncData, char * pszClrExpDate, int * cardSrc, char **szSBTData)
{
	int					rv					= SUCCESS;
	int					iLen				= 0;
	int 				ctrlIds[4];
	int					iDisableFormFlag	= 0;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szTitle[100]		= "";
	char				szAmtTitle[100]		= "";
	char				szAppLogData[300]	= "";
	PAAS_BOOL			bWait				= PAAS_TRUE;
	XEVT_PTYPE			pstXevt				= NULL;
	CARD_TRK_PTYPE		pstCard				= NULL;
	UIREQ_STYPE			stUIReq;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = PAAS_PAYPAL_DATA_FRM;

	setAppState(CAPTURING_PAYPAL_PAYMENT_CODE);

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	debug_sprintf(szDbgMsg, "%s: frmName[whichForm] [%s]", __FUNCTION__, frmName[whichForm]);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);
	//showPaypalFormWrapper(stUIReq.pszBuf);

	/* Set the Title */
	fetchScrTitle(szTitle, ENTER_PAYMENT_CODE_TITLE);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_PAYPAL_DATA_LBL_1;
	ctrlIds[1] 	= PAAS_PAYPAL_DATA_LBL_2;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 2, giLabelLen-5 /*Paypal screens have lesser width thats why */, stUIReq.pszBuf);
	setShortValueWrapper(PAAS_PAYPAL_DATA_KBD_1, PROP_SHORT_MIN_ENTRIES, 4, stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	/* Send the message to the UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		/* Sending it here so that first form is shown and then swipe is enabled*/
		//AjayS2: 13-Jan-2016
		//If wallet is enabled we have to send TERM_CAP as Payment only, because from next release
		//default TERM_CAP is taken as VAS and PAYMENT
		if(isWalletEnabled())
		{
			rv = sendS20CmdToGetMSRCardData(PAYMENT_ONLY,  NULL, NULL);
		}
		else
		{
			rv = sendS20CmdToGetMSRCardData(NULL, NULL, NULL);
		}
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to send XPI S20 cmd to enable MSR",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			return ERR_DEVICE_APP;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing PayPal Payment Code Details Capture Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}

		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiKeypadEvt == 2) /* On Enter */
						{
							debug_sprintf(szDbgMsg, "%s: User Pressed Enter ", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							strcpy(pszPymtCode, pstXevt->szKpdVal);

							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "Payment Code Captured Successfully From User");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							rv = SUCCESS;
						}
						else if(pstXevt->uiKeypadEvt == 3) /* On Cancel */
						{
							/* Upon pressing CANCEL on the keypad */
							debug_sprintf(szDbgMsg, "%s: User pressed CANCEL", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "User Pressed cancel Button From Paypal Payment Capture Screen");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							rv = UI_CANCEL_PRESSED;
						}
						else if(pstXevt->uiCtrlType == 90) //STB Data Event
						{
							debug_sprintf(szDbgMsg, "%s: STB Event Received", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							/* Assinging the Data pointer, please make sure that it is freed in the caller function*/
							*szSBTData = pstXevt->pszData;
						}
						else
						{
							/* Unknown keypad event for this screen */
							rv = ERR_DEVICE_APP;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					/* Disabling the card swipe on the UI agent. */
					disableCardReaders();

				}
				else if(stBLData.uiRespType == UI_CARD_RESP)
				{
					if(stBLData.iStatus == SUCCESS)
					{
						pstCard = &(stBLData.stRespDtls.stCrdTrkInfo);

						debug_sprintf(szDbgMsg, "%s: Received the Card Data",
										__FUNCTION__);
						APP_TRACE(szDbgMsg);

						/* Track 1 --- */
						iLen = strlen(pstCard->szTrk1);
						if(iLen > 0)
						{
							pszTracks[0] = (char *)malloc(iLen + 1);
							memset(pszTracks[0], 0x00, iLen + 1);
							memcpy(pszTracks[0], pstCard->szTrk1, iLen);
						}

						/* Track 2 --- */
						iLen = strlen(pstCard->szTrk2);
						if(iLen > 0)
						{
							pszTracks[1] = (char *)malloc(iLen + 1);
							memset(pszTracks[1], 0x00, iLen + 1);
							memcpy(pszTracks[1], pstCard->szTrk2, iLen);
						}

						/* Track 3 --- */
						iLen = strlen(pstCard->szTrk3);
						if(iLen > 0)
						{
							pszTracks[2] = (char *)malloc(iLen + 1);
							memset(pszTracks[2], 0x00, iLen + 1);
							memcpy(pszTracks[2], pstCard->szTrk3, iLen);
						}

						/* Eparms Data  --- */
						iLen = strlen(pstCard->szEparms);
						if(iLen > 0)
						{
							pszTracks[3] = (char *)malloc(iLen + 1);
							memset(pszTracks[3], 0x00, iLen + 1);
							memcpy(pszTracks[3], pstCard->szEparms, iLen);
						}

						/* Copy clear expiry date*/
						iLen = strlen(pstCard->szClrExpDt);
						if(iLen > 0)
						{
							debug_sprintf(szDbgMsg, "%s: Copying Clear Expiry date",
																	__FUNCTION__);
							APP_TRACE(szDbgMsg);

							memcpy(pszClrExpDate, pstCard->szClrExpDt, iLen);
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: No Clear exp date to copy",
									__FUNCTION__);
							APP_TRACE(szDbgMsg);
						}

						/* Card source */
						*cardSrc = pstCard->iCardSrc;

						/*Copy the RSA Track 1 */
						iLen = strlen(pstCard->szRsaTrk1);
						if(iLen > 0)
						{
							pszRSATracks[0] = (char *)malloc(iLen + 1);
							memset(pszRSATracks[0], 0x00, iLen + 1);
							memcpy(pszRSATracks[0], pstCard->szRsaTrk1, iLen);
						}

						/*Copy the RSA Track 2 */
						iLen = strlen(pstCard->szRsaTrk2);
						if(iLen > 0)
						{
							pszRSATracks[1] = (char *)malloc(iLen + 1);
							memset(pszRSATracks[1], 0x00, iLen + 1);
							memcpy(pszRSATracks[1], pstCard->szRsaTrk2, iLen);
						}

						if(szVsdEncData != NULL)
						{
							/* Copy VSD encrypted  data*/
							iLen = strlen(pstCard->szVsdEncBlob);
							if(iLen > 0)
							{
								szVsdEncData[0] = (char *)malloc(iLen + 1);
								memset(szVsdEncData[0], 0x00, iLen + 1);
								memcpy(szVsdEncData[0], pstCard->szVsdEncBlob, iLen);
							}

							/* Copy KSN associated with VSD encrypted  data*/
							iLen = strlen(pstCard->szVsdKSN);
							if(iLen > 0)
							{
								szVsdEncData[1] = (char *)malloc(iLen + 1);
								memset(szVsdEncData[1], 0x00, iLen + 1);
								memcpy(szVsdEncData[1], pstCard->szVsdKSN, iLen);
							}

							/* Copy IV associated with VSD encrypted  data*/
							iLen = strlen(pstCard->szVsdIV);
							if(iLen > 0)
							{
								szVsdEncData[2] = (char *)malloc(iLen + 1);
								memset(szVsdEncData[2], 0x00, iLen + 1);
								memcpy(szVsdEncData[2], pstCard->szVsdIV, iLen);
							}

							/* Copy Clear Track data if present*/
							if(strlen(pstCard->szClrTrk2))
							{
								iLen = strlen(pstCard->szClrTrk2);
								szVsdEncData[3] = (char *)malloc(iLen + 1);
								memset(szVsdEncData[3], 0x00, iLen + 1);
								memcpy(szVsdEncData[3], pstCard->szClrTrk2, iLen);
							}
							else if(strlen(pstCard->szClrTrk1))
							{
								iLen = strlen(pstCard->szClrTrk1);
								szVsdEncData[3] = (char *)malloc(iLen + 1);
								memset(szVsdEncData[3], 0x00, iLen + 1);
								memcpy(szVsdEncData[3], pstCard->szClrTrk1, iLen);
							}
						}

						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "User Swiped Card From Paypal Payment Capture Screen");
							addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
						}

						rv = SUCCESS;
					}
					else if(stBLData.iStatus == ERR_BAD_CARD)
					{
						debug_sprintf(szDbgMsg, "%s: Max Bad card reads done",
											__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_BAD_CARD;
					}
					bBLDataRcvd = PAAS_FALSE;

					/*
					 * If STB is enabled, then we need to wait for the XEVT data
					 * thats why not setting bwait to false here
					 */
					if(!isSTBLogicEnabled())
					{
						bWait = PAAS_FALSE;
					}
					else
					{
						if(rv != SUCCESS) //If STB is enabled..we have to wait only if we have received the card data, thats why checking rv value
						{
							bWait = PAAS_FALSE;
						}
					}
				}
				else if(stBLData.uiRespType == UI_EMV_RESP)
				{
					debug_sprintf(szDbgMsg, "%s: Received the EMV Data", __FUNCTION__);

					if( strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd,EMV_U00_REQ) ==0 || strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_U02_REQ) ==0 )
					{
						if(stBLData.iStatus == SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: Received U02",__FUNCTION__);
							APP_TRACE(szDbgMsg);

							if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bEMVRetry == PAAS_TRUE)
							{
								/* Initialize the screen */
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "Error While Inserting/Swiping/Tapping card");
									addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								initUIReqMsg(&stUIReq, BATCH_REQ);

								showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

								if(sendUIReqMsg(stUIReq.pszBuf) != SUCCESS)
								{
									debug_sprintf(szDbgMsg, "%s: Error while communicating with UI Agent", __FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
							}
							else if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bCardInserted == PAAS_TRUE)
							{
								/* KranthiK1: Added this only to show the processing form
								 * as C30 is taking a long time to be respond.
								 * Remove later if not required
								 */
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "EMV Card Inserted");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}
								bBLDataRcvd = PAAS_FALSE;

								releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

								//showPSGenDispForm(MSG_PROCESSING, STANDBY_ICON, 0);
							}
							bBLDataRcvd = PAAS_FALSE;
						}
					}
				}
				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg,"%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getLoyaltyDtlsFrmUser()
 *
 * Description	: This API would be used to prompt the users to enter either
 * 					their phone numbers or swipe their loyalty cards. It will
 * 					be called if the loyalty capture is enabled in the merchant
 * 					configuration.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getLoyaltyDtlsFrmUser(char * szPhNo, char ** szTracks, char * pszClrExpDate, int * cardSrc, char * szInputType, VASDATA_DTLS_PTYPE pstVasDataDtls, PAAS_BOOL bVASRequired, char * szTranKey)
{
	int				rv					= SUCCESS;
	int				iLen				= 0;
	int 			ctrlIds[4];
	int				iDisableFormFlag	= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTitle[100]		= "";
	char            szVASMode[4+1]      = "";
	char			szAmtTitle[100]		= "";
	char 		    szFileName[100] 	= "";
	char			szAppLogData[300]	= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	PAAS_BOOL		bEmvEnabled			= PAAS_FALSE;
	XEVT_PTYPE		pstXevt				= NULL;
	CARD_TRK_PTYPE	pstCard				= NULL;
	BTNLBL_STYPE	stBtnLbl;
	CUSTINFODTLS_STYPE stCustInfoDtls;
	unsigned short shOptions 			= 0;
	UIREQ_STYPE		stUIReq;


#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = NUM_ENTRY_FRM;

	//Get EMV status
	bEmvEnabled = isEmvEnabledInDevice();
	
	//AjayS2: 13-Jan-2016
	//If wallet is enabled we have to send TERM_CAP as Payment only, because from next release
	//default TERM_CAP is taken as VAS and PAYMENT		
	if(bVASRequired == PAAS_TRUE)
	{
		strcpy(szVASMode,VAS_OR_PAYMENT);
	}
	else if(isWalletEnabled())
	{
		strcpy(szVASMode,PAYMENT_ONLY);
	}

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	//Getting the current options for the ctrlid of the form
	shOptions = getOptionsForCtrlid(PS_NUM_ENTRY_EDT_1, stUIReq.pszBuf);

	//To make sure that right to left entry is disabled
	shOptions &= ~OPT_TB_RL_MASK;

	initUIReqMsg(&stUIReq, BATCH_REQ);

	setShortValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_SHORT_OPTIONS, shOptions, stUIReq.pszBuf);
	setShortValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_SHORT_MIN_ENTRIES, 10, stUIReq.pszBuf);

	if((szInputType != NULL) && strlen(szInputType) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Input Format Type [%s]", __FUNCTION__, szInputType);
		APP_TRACE(szDbgMsg);
		/*
		 * The request tag will be <TYPE> and when set to NUMBER will
		 * initiate the prompting to be Enter Loyalty Number
		 */
		if(strcasecmp(szInputType, "NUMBER") == 0)
		{
			//Praveen_P1: FIXME: Need to have the correct value of minimum length here
			setShortValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_SHORT_MIN_ENTRIES, 5, stUIReq.pszBuf);

			setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_DISPLAY_STRING, "                    ", stUIReq.pszBuf);
			setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_FORMAT_STRING,  "NNNNNNNNNNNNNNNNNNNN", stUIReq.pszBuf);
			/* Set the Title */
			fetchScrTitle(szTitle, ENTER_LTY_NUM_TITLE);
		}
		else
		{
			//Should not enter this case..something wrong
			debug_sprintf(szDbgMsg, "%s: Should not enter this case!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return ERR_INV_FLD;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Input Format Type is not sent, so setting format as PHONE format", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_DISPLAY_STRING, "(   )   -    ", stUIReq.pszBuf);
		setStringValueWrapper(PS_NUM_ENTRY_EDT_1, PROP_STR_FORMAT_STRING, "CNNNCNNNCNNNN", stUIReq.pszBuf);
		/* Set the Title */
		fetchScrTitle(szTitle, ENTER_LTY_TITLE);
	}

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PS_NUM_ENTRY_LBL_1;
	ctrlIds[1] 	= PS_NUM_ENTRY_LBL_11;
	ctrlIds[2] 	= PS_NUM_ENTRY_LBL_12;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, szAmtTitle, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);


	memset(szFileName, 0x00, sizeof(szFileName));
	sprintf(szFileName, "%s", "btn_blank_static.png");
	getFileNamewithPlatformPrefix(szFileName);
	setStringValueWrapper(PS_NUM_ENTRY_FK_CANCEL, PROP_STR_UP_IMAGE_FILE_NAME, szFileName, stUIReq.pszBuf);

	memset(szFileName, 0x00, sizeof(szFileName));
	sprintf(szFileName, "%s", "btn_blank_active.png");
	getFileNamewithPlatformPrefix(szFileName);
	setStringValueWrapper(PS_NUM_ENTRY_FK_CANCEL, PROP_STR_DOWN_IMAGE_FILE_NAME, szFileName, stUIReq.pszBuf);

	/* --------- Set the labels ------------ */

	/* Set the label on the buttons */

	/* "Skip Loyalty Entry" string */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_11);
	setStringValueWrapper(PS_NUM_ENTRY_LBL_4, PROP_STR_CAPTION,
			(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);

	/* "SUBMIT" string */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_09);
	setStringValueWrapper(PS_NUM_ENTRY_LBL_3, PROP_STR_CAPTION,
			(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	/* Enable card swipe at the UI agent(FormAgent) (TRACK 1 and TRACK 2 request) */
	//	getCardDataUtil(PAAS_TRUE, PAAS_TRUE, PAAS_FALSE, stUIReq.pszBuf);
	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	/* Send the message to the UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);
		}
		bWait = PAAS_TRUE;

		memset(&stCustInfoDtls, 0x00, sizeof(CUSTINFODTLS_STYPE));
		rv = getCustInfoDtlsForPymtTran(szTranKey, &stCustInfoDtls);
		/* Enable card swipe at the UI agent(XPI) (TRACK 1 and TRACK 2 request) */
		rv = sendS20CmdToGetMSRCardData(szVASMode, stCustInfoDtls.szMerchantIndex, stCustInfoDtls.szCustData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to send XPI S20 cmd to enable MSR",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			return ERR_DEVICE_APP;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Loyalty Details Capture Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}

		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiKeypadEvt)
						{
						case 2: /* On Enter */
							strcpy(szPhNo, pstXevt->szKpdVal);

							debug_sprintf(szDbgMsg, "%s: Value entered = [%s]",
									__FUNCTION__, szPhNo);
							APP_TRACE(szDbgMsg);

							rv = SUCCESS;
							break;

						case 3: /* ON CANCEL */
							/* Upon pressing CANCEL on the keypad */
							debug_sprintf(szDbgMsg, "%s: User pressed CANCEL",
									__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = UI_CANCEL_PRESSED;
							break;

						default:
							/* Unknown keypad event for this screen */
							rv = ERR_DEVICE_APP;
							break;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					/* Disabling the card swipe on the UI agent. */
					disableCardReaders();
				}
				else if(stBLData.uiRespType == UI_CARD_RESP)
				{
					if(stBLData.iStatus == SUCCESS)
					{
						pstCard = &(stBLData.stRespDtls.stCrdTrkInfo);

						debug_sprintf(szDbgMsg, "%s: Received the Card Data", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(pstCard->stVasDataDtls.bProvisionPassSent == PAAS_TRUE && strlen(stCustInfoDtls.szCustData) > 0)
						{
							if(isWalletEnabled())
							{
								memcpy(pstVasDataDtls, &pstCard->stVasDataDtls, sizeof(VASDATA_DTLS_STYPE));
								debug_sprintf(szDbgMsg, "%s: Provision Pass Sent On Loyalty Screen during Payment", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								rv = SUCCESS;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: ERROR:: Wallet is NOT Enabled but Received Provision Pass Sent Response On Loyalty Screen", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								rv = FAILURE;
							}
						}
						else if(pstCard->stVasDataDtls.bVASPresent)
						{
							if(isWalletEnabled())
							{
								debug_sprintf(szDbgMsg, "%s: Received VAS Data from Loyalty Screen", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = SUCCESS;
								memcpy(pstVasDataDtls, &pstCard->stVasDataDtls, sizeof(VASDATA_DTLS_STYPE));
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: ERROR:: Wallet is NOT Enabled but Received VAS Data from Loyalty Screen", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								rv = FAILURE;
							}
						}
						else
						{
							/* Track 1 --- */
							iLen = strlen(pstCard->szTrk1);
							if(iLen > 0)
							{
								szTracks[0] = (char *)malloc(iLen + 1);
								memset(szTracks[0], 0x00, iLen + 1);
								memcpy(szTracks[0], pstCard->szTrk1, iLen);
							}

							/* Track 2 --- */
							iLen = strlen(pstCard->szTrk2);
							if(iLen > 0)
							{
								szTracks[1] = (char *)malloc(iLen + 1);
								memset(szTracks[1], 0x00, iLen + 1);
								memcpy(szTracks[1], pstCard->szTrk2, iLen);
							}

							/* Track 3 --- */
							iLen = strlen(pstCard->szTrk3);
							if(iLen > 0)
							{
								szTracks[2] = (char *)malloc(iLen + 1);
								memset(szTracks[2], 0x00, iLen + 1);
								memcpy(szTracks[2], pstCard->szTrk3, iLen);
							}
							/* Copy clear expiry date*/
							iLen = strlen(pstCard->szClrExpDt);
							if(iLen > 0)
							{
								debug_sprintf(szDbgMsg, "%s: Copying Clear Expiry date",
										__FUNCTION__);
								APP_TRACE(szDbgMsg);

								memcpy(pszClrExpDate, pstCard->szClrExpDt, iLen);
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: No Clear exp date to copy",
										__FUNCTION__);
								APP_TRACE(szDbgMsg);
							}

							/* Card source */
							* cardSrc = pstCard->iCardSrc;

							rv = SUCCESS;
						}
					}
					else if(stBLData.iStatus == ERR_USR_TIMEOUT)
					{
						debug_sprintf(szDbgMsg, "%s: USR Timeout",
								__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_USR_TIMEOUT;
					}
					else if(stBLData.iStatus == UI_CANCEL_PRESSED)
					{
						debug_sprintf(szDbgMsg, "%s: UI Cancel Pressed",
								__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = UI_CANCEL_PRESSED;
					}
					else
					{
						/* Any other case pass as Bad Card . We should come here only for
						 * when S20 Response contains 01 Response code.
						 * We also enter here when any wrong response code is received.
						 */
						debug_sprintf(szDbgMsg, "%s: Max Bad card reads done",
								__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_BAD_CARD;
					}

					bWait = PAAS_FALSE;
					bBLDataRcvd = PAAS_FALSE;
				}
				else if(stBLData.uiRespType == UI_EMV_RESP)
				{
					debug_sprintf(szDbgMsg, "%s: Card Read -----VERIFONE",
							__FUNCTION__);
					APP_TRACE(szDbgMsg);
					if( strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd,EMV_U00_REQ) == 0 || strcmp(stBLData.stRespDtls.stEmvDtls.szEMVRespCmd, EMV_U02_REQ) ==0 )
					{

						debug_sprintf(szDbgMsg, "%s: Received U02  here",__FUNCTION__);
						APP_TRACE(szDbgMsg);
//						//						memset(&stEmvDtls, 0x00, sizeof(EMVDTLS_STYPE));
//						//						memcpy(&stEmvDtls, &(stBLData.stRespDtls.stEmvDtls), sizeof(EMVDTLS_STYPE));
						if(stBLData.stRespDtls.stEmvDtls.stEMVCardStatusU02.bEMVRetry == PAAS_TRUE)
						{
							/* Initialize the screen */
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "Error While Inserting/Swiping/Tapping card");
								addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							initUIReqMsg(&stUIReq, BATCH_REQ);

							showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

							if(sendUIReqMsg(stUIReq.pszBuf) != SUCCESS)
							{
								debug_sprintf(szDbgMsg, "%s: Error while communicating with UI Agent", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}
						}

					}

					bBLDataRcvd = PAAS_FALSE;
				}
				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg,"%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: clearUIListBoxState
 *
 * Description	: This function is clear the Listbox state stored in the FA
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
void clearUIListBoxState()
{
	int					rv					= SUCCESS;
//	int					iIndex				= 0;
	LI_BUFFER_NODE_PTYPE pTmpLIBuf = NULL;
	UIREQ_STYPE			stUIReq;

	//PAAS_BOOL			bWait				= PAAS_TRUE;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * Daivik:12/4/2016 -We need to remove any stored Line Items as well.
	 * We should not hit this condition, but still including this incase we do.
	 */
	//acquireMutexLock(&gptLIBufferMsgMutex, "LI Buffer Message");
	//for(iIndex = 0; iIndex < MAX_LI_BUFFER; iIndex++)
	setAllowLITran(PAAS_FALSE);
	while(liBufferHead.headNode != NULL)
	{
		pTmpLIBuf = liBufferHead.headNode;
		liBufferHead.headNode = liBufferHead.headNode->nxtNode;
		liBufferHead.bufferCnt--;
		free(pTmpLIBuf);
		pTmpLIBuf = NULL;
	}
	if(liBufferHead.bufferCnt != 0 )
	{
		debug_sprintf(szDbgMsg, "%s: Should not come here since there should be no buffer in the list at this point!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	liBufferHead.headNode = NULL;
	liBufferHead.tailNode = NULL;
	//releaseMutexLock(&gptLIBufferMsgMutex, "LI Buffer Message");

	/*
	 * Praveen_P1: Clear List command(XLCS) will not send any response
	 * so sending with out XBATCH like 72 command
	 */

	initUIReqMsg(&stUIReq, XLCS_REQ);
	rv = sendUIReqMsg(stUIReq.pszBuf); // T_RaghavendranR1 CID 67453 (#1 of 1): Unchecked return value (CHECKED_RETURN)
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Cleared the List box state",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

#if 0
	initUIReqMsg(&stUIReq, BATCH_REQ);

	clearListBoxRestoreState(stUIReq.pszBuf);

	/* Send the message to the UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Bwait Value %d BLData Received Value %d before getting response",__FUNCTION__, bWait, bBLDataRcvd);
		APP_TRACE(szDbgMsg);

		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			svcWait(5);
		}
		debug_sprintf(szDbgMsg, "%s: Cleared the List box state",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
#endif

	debug_sprintf(szDbgMsg,"%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return ;
}

/*
 * ============================================================================
 * Function Name: getStreetAddr
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getStreetAddr(char * szStreetAddr)
{
	int				rv				= SUCCESS;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* TODO: Create a different form for address entry. A generic alphabetic
	 * form (updated MHS file with png images of the keyboard) need to be
	 * created. */

	/* AVS supported in US CANADA and UK atleast. */

	debug_sprintf(szDbgMsg,"%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: cancelRequest()
 *
 * Description	: This API would cancel the request by sending RESET command
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
void cancelRequest()
{
	int rv = SUCCESS;
	UIREQ_STYPE		stUIReq;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: Sending the Cancel Request",__FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stUIReq, RESET_REQ);
	rv = sendUIReqMsg(stUIReq.pszBuf);
	//CID 67311 (#1 of 1): Unchecked return value (CHECKED_RETURN). T_RaghavendranR1
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	debug_sprintf(szDbgMsg, "%s: Cancel Ack Recieved",__FUNCTION__);
	APP_TRACE(szDbgMsg);
	return;
}

/*
 * ============================================================================
 * Function Name: cancelXPIRequest()
 *
 * Description	: This API would cancel the request by sending RESET command
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
void cancelXPIRequest()
{
	int rv = SUCCESS;
	UIREQ_STYPE		stUIReq;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	initUIReqMsg(&stUIReq, XPI_RESET_REQ);
	rv = sendUIReqMsg(stUIReq.pszBuf);
	//CID 67393 (#1 of 1): Unchecked return value (CHECKED_RETURN). T_RaghavendranR1
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: clearScreen()
 *
 * Description	: This API would clear the screen on the terminal
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
void clearScreen(int iMode)
{
	int						rv						= SUCCESS;
	PAAS_BOOL				bWait					= PAAS_TRUE;
	UIREQ_STYPE		stUIReq;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
	if(getXPILibMode())
	{
		// 10-Dec-15: MukeshS3: Removing all the Clear screens from SCA, when XPI is running as library.
		return ;
	}
	switch(iMode)
	{
	case 1:
		initUIReqMsg(&stUIReq, XCLS_REQ);
		break;
	case 2:
		initUIReqMsg(&stUIReq, S002_REQ);
		break;
	default:
		initUIReqMsg(&stUIReq, XCLS_REQ);
		break;
	}

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)	// CID-67259: 2-Feb-16: MukeshS3: Checking for FAILURE case
	{
		debug_sprintf(szDbgMsg, "%s: Error while communicating with UI agent",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);
		return ;
	}
	if(iMode == 1)
	{
		while(bWait == PAAS_TRUE)
		{
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				if(stBLData.uiRespType == UI_XCLS_RESP)
				{
					bWait 		= PAAS_FALSE;
				}
				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				bBLDataRcvd = PAAS_FALSE;
			}
			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);
		}
	}

	return;
}

/*
 * ============================================================================
 * Function Name: getGCManData
 *
 * Description	: This function gets the Manual Entry data for the Gift Card
 *
 * Input Params	: nothing
 *
 * Output Params: void
 * ============================================================================
 */
static int getGCManData(char * szPAN, char * szExpDt, char * szClrDt, char *szCVV)
{
	int		rv					= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Show the PAN screen and get the Gift Card PAN from the user */
		rv = getPAN(szPAN);
		if(rv != SUCCESS)
		{
			if(rv == UI_CANCEL_PRESSED)
			{
				debug_sprintf(szDbgMsg, "%s: PAN entry CANCELLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get PAN", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;
		}

		if(isExpReqdForGiftCard())
		{
			/* Show the expiry date screen and get the expiry date from the user */
			rv = getExpiryDate(szExpDt, szClrDt);
			if(rv != SUCCESS)
			{
				if(rv == UI_CANCEL_PRESSED)
				{
					debug_sprintf(szDbgMsg, "%s: EXP DATE CANCELLED",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get Expiry Date",
																	__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

				break;
			}
		}

		if(isCvvReqdForGiftCard())
		{
			/* Show the CVV screen and get the CVV number from the user */
			rv = getCVV(szCVV);
			if(rv != SUCCESS)
			{
				if(rv == UI_CANCEL_PRESSED)
				{
					debug_sprintf(szDbgMsg, "%s: CVV CANCELLED",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get CVV Number",
																	__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

				break;
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPAN
 *
 * Description	:
 *
 * Input Params	: Prompt to display on the screen
 *
 * Output Params:
 * ============================================================================
 */
static int getPAN(char * szPAN)
{
	int			rv					= SUCCESS;
	int			iDisableFormFlag	= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	ushort		options				= 0;
	char		szTmp[100]			= "";
	char		szAppLogData[300]	= "";
	PAAS_BOOL	bWait				= PAAS_TRUE;
	XEVT_PTYPE	pstXevt				= NULL;
	UIREQ_STYPE	stUIReq;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = MANUAL_ENTRY_FRM;

	/* Initialize the form */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title of the screen */
	fetchScrTitle(szTmp, ENTER_PAN_TITLE);
	setStringValueWrapper(FA_XENCDATA_LBL_1, PROP_STR_CAPTION, szTmp,
																stUIReq.pszBuf);

	/* Set the keypad options */
	options = 0x4351;
	options |= OPT_TB_PWD_MASK;
	setShortValueWrapper(FA_XENCDATA_KBD_1, PROP_SHORT_OPTIONS, options,
																stUIReq.pszBuf);
	//Praveen_P1: FIXME: Need to have the correct value of minimum length here
	setShortValueWrapper(FA_XENCDATA_KBD_1, PROP_SHORT_MIN_ENTRIES, 8,
																stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Manual PAN Entry Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					/* Check if the xevt response got is for the same form */
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == FA_XENCDATA_KBD_1)
						{
							switch(pstXevt->uiKeypadEvt)
							{
							case 2: /* ON ENTER */
								strcpy(szPAN, pstXevt->szKpdVal);

								rv = SUCCESS;
								break;

							case 3: /* ON CANCEL */
								debug_sprintf(szDbgMsg, "%s: CANCEL pressed",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Pressed Cancel Button From Manual Card PAN Details Entry Screen");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								rv = UI_CANCEL_PRESSED;
								break;

							default:
								debug_sprintf(szDbgMsg, "%s: Invalid kpd event",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
								break;
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Invalid control id",
												__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			svcWait(10);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: capturePINCodeDtls
 *
 * Description	:
 *
 * Input Params	: Prompt For PIN Code on screen
 *
 * Output Params:
 * ============================================================================
 */
int capturePINCodeDtls(char * szPINCode)
{
	int			rv					= SUCCESS;
	int			iDisableFormFlag	= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	ushort		options				= 0;
	char		szTmp[100]			= "";
	char		szDisp[10+1]		= "";
	char		szFmt[10+1]			= "";
	char		szAppLogData[300]	= "";
	PAAS_BOOL	bWait				= PAAS_TRUE;
	XEVT_PTYPE	pstXevt				= NULL;
	UIREQ_STYPE		stUIReq;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = MANUAL_ENTRY_FRM;

	/* Initialize the form */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title of the screen */
	fetchScrTitle(szTmp, ENTER_PIN_CODE_TITLE);
	setStringValueWrapper(FA_XENCDATA_LBL_1, PROP_STR_CAPTION, szTmp,
																stUIReq.pszBuf);

	/* Set the keypad options */
	options = 0x4351;
	options |= OPT_TB_PWD_MASK;
	setShortValueWrapper(FA_XENCDATA_KBD_1, PROP_SHORT_OPTIONS, options,
																stUIReq.pszBuf);
	//Praveen_P1: FIXME: Need to have the correct value of minimum length here
	setShortValueWrapper(FA_XENCDATA_KBD_1, PROP_SHORT_MIN_ENTRIES, 1,
																stUIReq.pszBuf);
	/* Clear the display string for the keypad */
	memset(szDisp, SPACE, sizeof(szDisp) - 1 /* keep one for '\0' always*/);
	setStringValueWrapper(FA_XENCDATA_KBD_1, PROP_STR_DISPLAY_STRING, szDisp,
																stUIReq.pszBuf);
	/* Set the format string for the keypad */
	strcpy(szFmt, "NNNNNNNNNN");
	setStringValueWrapper(FA_XENCDATA_KBD_1,PROP_STR_FORMAT_STRING, szFmt,
																stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing PIN Code Entry Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					/* Check if the xevt response got is for the same form */
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == FA_XENCDATA_KBD_1)
						{
							switch(pstXevt->uiKeypadEvt)
							{
							case 2: /* ON ENTER */
								strcpy(szPINCode, pstXevt->szKpdVal);

								rv = SUCCESS;
								break;

							case 3: /* ON CANCEL */
								debug_sprintf(szDbgMsg, "%s: CANCEL pressed",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Pressed Cancel Button From PIN Code Details Entry Screen");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								rv = UI_CANCEL_PRESSED;
								break;

							default:
								debug_sprintf(szDbgMsg, "%s: Invalid kpd event",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
								break;
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Invalid control id",
												__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			svcWait(10);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: getCVV
 *
 * Description	:
 *
 * Input Params	: Prompt For CVV on screen
 *
 * Output Params:
 * ============================================================================
 */
static int getCVV(char * szCVV)
{
	int			rv					= SUCCESS;
	int			iDisableFormFlag	= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	ushort		options				= 0;
	char		szTmp[100]			= "";
	char		szDisp[10+1]		= "";
	char		szFmt[10+1]			= "";
	char		szAppLogData[300]	= "";
	PAAS_BOOL	bWait				= PAAS_TRUE;
	XEVT_PTYPE	pstXevt				= NULL;
	UIREQ_STYPE		stUIReq;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = MANUAL_ENTRY_FRM;

	/* Initialize the form */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title of the screen */
	fetchScrTitle(szTmp, ENTER_CVV_TITLE);
	setStringValueWrapper(FA_XENCDATA_LBL_1, PROP_STR_CAPTION, szTmp,
																stUIReq.pszBuf);

	/* Set the keypad options */
	options = 0x4351;
	options |= OPT_TB_PWD_MASK;
	setShortValueWrapper(FA_XENCDATA_KBD_1, PROP_SHORT_OPTIONS, options,
																stUIReq.pszBuf);
	//Praveen_P1: FIXME: Need to have the correct value of minimum length here
	setShortValueWrapper(FA_XENCDATA_KBD_1, PROP_SHORT_MIN_ENTRIES, 0,		//MukeshS3: Setting same as FA does for XGCD command
																stUIReq.pszBuf);
	/* Clear the display string for the keypad */
	strcpy(szDisp, "____");
	setStringValueWrapper(FA_XENCDATA_KBD_1, PROP_STR_DISPLAY_STRING, szDisp,
																stUIReq.pszBuf);
	/* Set the format string for the keypad */
	strcpy(szFmt, "NNNN");
	setStringValueWrapper(FA_XENCDATA_KBD_1,PROP_STR_FORMAT_STRING, szFmt,
																stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing CVV Entry Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					/* Check if the xevt response got is for the same form */
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == FA_XENCDATA_KBD_1)
						{
							switch(pstXevt->uiKeypadEvt)
							{
							case 2: /* ON ENTER */
								strcpy(szCVV, pstXevt->szKpdVal);

								rv = SUCCESS;
								break;

							case 3: /* ON CANCEL */
								debug_sprintf(szDbgMsg, "%s: CANCEL pressed",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Pressed Cancel Button From CVV Entry Screen");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								rv = UI_CANCEL_PRESSED;
								break;

							default:
								debug_sprintf(szDbgMsg, "%s: Invalid kpd event",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
								break;
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Invalid control id",
												__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			svcWait(10);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: getSelectedSurveyOption
 *
 * Description	:
 *
 * Input Params	: Option to be filled
 *
 * Output Params:
 * ============================================================================
 */
static int getSelectedSurveyOption(int *piOption, int iNumOptions)
{
	int			rv				= SUCCESS;
	int			iCount			= 0;
	int			iStartOption    = 0;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iStartOption = PAAS_SURVEY_SCREEN_OPT_1;

	for(iCount = 0; iCount < iNumOptions; iCount++)
	{
		initUIReqMsg(&stUIReq, BATCH_REQ);
		rv = getSelectedRadioOptForCtrlid(iCount + iStartOption, stUIReq.pszBuf, piOption);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while getting Selected radio option", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		debug_sprintf(szDbgMsg, "%s: Selected Option value for %d is %d", __FUNCTION__, iCount + iStartOption, *piOption);
		APP_TRACE(szDbgMsg);
		if(*piOption != 0)
		{
			debug_sprintf(szDbgMsg, "%s: Survey option is selected!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			*piOption = iCount + iStartOption; //Putting the correct option number
			break;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: getExpiryDate
 *
 * Description	:
 *
 * Input Params	: Buffer to show the PAN
 *
 * Output Params:
 * ============================================================================
 */
static int getExpiryDate(char * szExpDt, char * szClrDt)
{
	int			rv					= SUCCESS;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szTmp[100]			= "";
	char		szDisp[10]			= "";
	char		szFmt[10]			= "";
	char		szAppLogData[300]	= "";
	ushort		options				= 0;
	PAAS_BOOL	bWait				= PAAS_TRUE;
	XEVT_PTYPE	pstXevt				= NULL;
	int			iDisableFormFlag	= 0;
	UIREQ_STYPE	stUIReq;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = MANUAL_ENTRY_FRM;

	/* Initialize the form */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Set the Title of the screen */
	fetchScrTitle(szTmp, ENTER_EXPDT_TITLE);
	setStringValueWrapper(FA_XENCDATA_LBL_1, PROP_STR_CAPTION, szTmp,
																stUIReq.pszBuf);

	/* Set the keypad options */
	options = 0x4351;
	options |= OPT_TB_PWD_MASK;
	//Praveen_P1: FIXME: Need to have the correct value of minimum length here
	setShortValueWrapper(FA_XENCDATA_KBD_1, PROP_SHORT_MIN_ENTRIES, 0,
																stUIReq.pszBuf);
	setShortValueWrapper(FA_XENCDATA_KBD_1, PROP_SHORT_OPTIONS, options,
																stUIReq.pszBuf);

	/* Set the display string for the keypad */
	strcpy(szDisp, "MM/YY");
	setStringValueWrapper(FA_XENCDATA_KBD_1, PROP_STR_DISPLAY_STRING, szDisp,
																stUIReq.pszBuf);

	/* Set the format string for the desired input from keypad */
	strcpy(szFmt,  "NNCNN");
	setStringValueWrapper(FA_XENCDATA_KBD_1, PROP_STR_FORMAT_STRING, szFmt,
																stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Manual Expiry Date Capture Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == FA_XENCDATA_KBD_1)
						{
							switch(pstXevt->uiKeypadEvt)
							{
							case 2: /* ON ENTER */
								strcpy(szExpDt, pstXevt->szKpdVal);
								strcpy(szClrDt, pstXevt->szKpdVal);

								rv = SUCCESS;
								break;

							case 3: /* ON CANCEL */
								debug_sprintf(szDbgMsg, "%s: CANCEL pressed",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Pressed Cancel Button From Manual Card Expiry Details Entry Screen");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								rv = UI_CANCEL_PRESSED;
								break;

							default:
								debug_sprintf(szDbgMsg, "%s: Invalid kpd event",
												__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
								break;
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Invalid control id",
												__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = ERR_DEVICE_APP;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getFormattedLabels
 *
 * Description	:
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getFormattedLabels(char szPrompts[14][50], PAAS_BOOL bDHCP)
{
	int				rv				= SUCCESS;
	int				iLen			= 0;
	int				iTmpLen			= 0;
	int				iCnt			= 0;
	BTNLBL_STYPE	stBtnLbl;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * Contents to be stored in the double char array
	 * 1.  Name
	 * 2.  Version
	 * 3.  IP
	 * 4.  Network Mode
	 * 5.  Port
	 * 6.  UI agent
	 * 7.  RFID FW
	 * 8.  Nwk mode value
	 * 9.  EMV agent
	 * 10. Build Number
	 * 11. DHI Version
	 * 12. Secondary Port
	 */

	/* Get the labels for NAME and VERSION */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_13);
	strcpy(szPrompts[0], (char *)stBtnLbl.szLblOne);
	strcpy(szPrompts[1], (char *)stBtnLbl.szLblTwo);

	/* Get the labels for IP and network mode */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_12);
	strcpy(szPrompts[2], (char *)stBtnLbl.szLblOne);
	strcpy(szPrompts[3], (char *)stBtnLbl.szLblTwo);

	/* Get the labels for PORT and UI agent */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_14);
	strcpy(szPrompts[4], (char *)stBtnLbl.szLblOne);
	strcpy(szPrompts[5], (char *)stBtnLbl.szLblTwo);

	/* Get the label for RFID firmware */
	strcpy(szPrompts[6], "CTLS FW");

	/* Get the network mode value */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_01);
	if(bDHCP == PAAS_TRUE)
	{
		strcpy(szPrompts[7], (char *) stBtnLbl.szLblTwo);
	}
	else
	{
		strcpy(szPrompts[7], (char *) stBtnLbl.szLblOne);
	}

	/* Get the labels for XPI Agent */
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_15);		
	strcpy(szPrompts[8], (char *)stBtnLbl.szLblOne);
	
	/* Get the labels for Build Number */
	strcpy(szPrompts[9], (char *)stBtnLbl.szLblTwo);

	fetchBtnLabel(&stBtnLbl, BTN_LBLS_24);

	/* Get the labels for the DHI Version*/
	strcpy(szPrompts[10], (char*)stBtnLbl.szLblTwo);

	fetchBtnLabel(&stBtnLbl, BTN_LBLS_25);

	/* Get the labels for the Device Name*/
	strcpy(szPrompts[11], (char*)stBtnLbl.szLblOne);
	strcpy(szPrompts[12], (char*)stBtnLbl.szLblTwo);

	fetchBtnLabel(&stBtnLbl, BTN_LBLS_30);
	strcpy(szPrompts[13], (char*)stBtnLbl.szLblTwo);

	/* Get the length of the biggest label string. The formatting of the labels
	 * would be done according to the length of that big string */
	for(iCnt = 0; iCnt < 14; iCnt++)
	{
		if( (iTmpLen = strlen(szPrompts[iCnt])) > iLen )
		{
			iLen = iTmpLen;
		}
	}

	/* Format the other labels according to the maximum length */
	for(iCnt = 0; iCnt < 14; iCnt++)
	{
		appendSpacesToString(szPrompts[iCnt], iLen - strlen(szPrompts[iCnt]));
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: displaySysInfo
 *
 * Description	:
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int displaySysInfo(SYSINFO_PTYPE pstSysInfo, PAAS_BOOL bDHCP, char * IP,
																	int iPort)
{
	int			rv					= SUCCESS;
	int			iLen				= 0;
	int			iDispTime			= DFLT_SYSINFO_DISP_TIME;
	int			iDisableFormFlag	= 0;
	int			ctrlIds[4];
	int			iAppLogEnabled		= isAppLogEnabled();
	ullong		endTime				= 0L;
	ullong		curTime				= 0L;
	char		szTitle[100]		= "";
	char		szEMVInfo[100]		= "";
	char		szDHIInfo[100]		= "";
	char		szTmpMsg[4096]		= "";
	char 		szAppSubTitle[100] 	= "";
	char 		szOsSubTitle[100] 	= "";
	char		szDevName[100]		= "";
	char		szScndPort[100]		= "";
	char		szTmp[100]			= "";
	char		szPrompts[14][50]	= {""};
	char		szAppLogData[300]	= "";
	PAAS_BOOL	bEnterPressed		= PAAS_FALSE;
	//PAAS_BOOL	bWait				= PAAS_FALSE;	// CID-67298: 3-Feb-16: MukeshS3:
	PAAS_BOOL	bWait				= PAAS_TRUE;	// It should be always initialized with TRUE value.
	XEVT_PTYPE	pstXevt				= NULL;
	UIREQ_STYPE	stUIReq;
#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	whichForm = SYSINFO_FRM;

	/* Get time interval to show system info screen */
	iDispTime = getMsgDispIntvl(whichForm);

	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* Get the main title of the display screen */
	fetchScrTitle(szTitle, SYSINFO_TITLE);
	
	memset(ctrlIds, -1, sizeof(ctrlIds));

	ctrlIds[0] 	= PAAS_SYSTEM_INFO_LBL_2;
	ctrlIds[1] 	= PAAS_SYSTEM_INFO_LBL_4;
	ctrlIds[2] 	= PAAS_SYSTEM_INFO_LBL_5;

	/*Setting the screen title for mutiple labels*/
	setScreenTitle(szTitle, NULL, ctrlIds, 3, giLabelLen, stUIReq.pszBuf);


	/* Get the sub-titles of the display screen */
	fetchScrTitle(szAppSubTitle, APP_INFO_SUBTITLE);
	fetchScrTitle(szOsSubTitle, OS_INFO_SUBTITLE);

	/* Fetch the other string labels or values which are language dependent */
	getFormattedLabels(szPrompts, bDHCP);

	/*
	 * Contents to be stored in the szPrompts double array
	 * 1.  Name
	 * 2.  Version
	 * 3.  IP
	 * 4.  Network Mode
	 * 5.  Port
	 * 6.  UI agent
	 * 7.  RFID FW
	 * 8.  Nwk mode value
	 * 9.  EMV Agent
	 * 10. Build Number
	 * 11. DHI Version
	 * 12. Secondary Port
	 */

	//Build XPI info packet to display if EMV enabled
	memset(szEMVInfo, 0x00, sizeof(szEMVInfo));
	iLen = strlen(pstSysInfo->szXPIVer);
	if( iLen > 0)
	{
		sprintf(szEMVInfo, "%s: XPI\n %s: %s\n ", szPrompts[8], szPrompts[1], pstSysInfo->szXPIVer);
	}

	if((strlen(pstSysInfo->szDHIProcessor) > 0) && (strlen(pstSysInfo->szDHIVer) > 0))
	{
		sprintf(szDHIInfo, "%s: %s\n %s: %s\n %s: %d\n ", szPrompts[13], pstSysInfo->szDHIProcessor, szPrompts[10], pstSysInfo->szDHIVer,
											 szPrompts[9], pstSysInfo->iDHIBuild);
	}
	else if(strlen(pstSysInfo->szDHIVer) > 0)
	{
		sprintf(szDHIInfo, "%s: %s\n %s: %d\n ",szPrompts[10], pstSysInfo->szDHIVer,
											 szPrompts[9], pstSysInfo->iDHIBuild);
	}

	if(isDevNameEnabled())
	{
		getDeviceName(szDevName);
		if(strlen(szDevName) > 0)
		{
			sprintf(szTmp, "%s: %s\n",szPrompts[11], szDevName);
			strcpy(szDevName, szTmp);
		}
	}

	if(isSecondaryPortEnabled())
	{
		sprintf(szScndPort, " %s: %d\n",szPrompts[12], getListeningScndPortNum());
	}

	iLen = sprintf(szTmpMsg, "\n%s:\n %s: Point Solutions\n %s: %s\n %s: %s\n %s: FormAgent\n %s: %s\n %s%s%s: %s\n %s: %s\n %s: %d\n%s %s \n\n%s: \n %s: %s\n %s: %s\n",
						szAppSubTitle, szPrompts[0], szPrompts[1],
						pstSysInfo->szSelfVer, szPrompts[9], pstSysInfo->szSelfBuildNum, szPrompts[5], szPrompts[1],
						pstSysInfo->szFAVer, szEMVInfo, szDHIInfo, szPrompts[2], IP,
						szPrompts[3], szPrompts[7], szPrompts[4], iPort, szScndPort, szDevName,
						szOsSubTitle, szPrompts[1], pstSysInfo->szOSVer,
						szPrompts[6], getEMVCTLSVersion());

	/* Add the data created above in the text box on the screen */
	addTextboxTextWrapper(PAAS_SYSTEM_INFO_TXT_1, szTmpMsg, stUIReq.pszBuf);

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	while(1)
	{
		bBLDataRcvd = PAAS_FALSE;//Praveen_P1: Setting to false before sending the request to make sure that we wait for the response which we are sending

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while showing form [%s]",
											__FUNCTION__, frmName[whichForm]);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
			break;
		}
		else //Praveen_P1: After showing the screen we are not waiting for the response of XBATCH here
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);
			}
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing System Information Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}

		curTime = svcGetSysMillisec();
		endTime = curTime + (iDispTime * 1000L);

		/* Wait for the user button press or wait for 30sec */
		while(endTime > curTime)
		{
			curTime = svcGetSysMillisec();

			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == PAAS_SYSTEM_INFO_FK_1)
						{
							debug_sprintf(szDbgMsg, "%s: Enter PRESSED",
											__FUNCTION__);
							APP_TRACE(szDbgMsg);

							bEnterPressed = PAAS_TRUE;
						}

						else if(pstXevt->uiCtrlID == PAAS_SYSTEM_INFO_TXT_1)
						{
							debug_sprintf(szDbgMsg, "%s: UP/DOWN arrow PRESSED",
																		__FUNCTION__);
							APP_TRACE(szDbgMsg);
						}						else
						{
							debug_sprintf(szDbgMsg, "%s: Invalid control id",
											__FUNCTION__);
							APP_TRACE(szDbgMsg);

							rv = FAILURE;
						}

						bBLDataRcvd = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bEnterPressed == PAAS_TRUE || (rv != SUCCESS))
			{
				break;
			}

			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getUISysInfo
 *
 * Description	: This API would build/prepare the system information
 *                to be displayed on the screen.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getUISysInfo(SYSINFO_PTYPE pstSysInfo)
{
	int			rv					= SUCCESS;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
	PAAS_BOOL	bWait				= PAAS_TRUE;
	UIREQ_STYPE	stUIReq;

#ifdef DEBUG
	char		szDbgMsg[128]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Initialize the response details */
	memset(pstSysInfo, 0x00, sizeof(SYSINFO_STYPE));	//CID:67407:T_POLISETTYG1: Wrong sizeof argument

	initUIReqMsg(&stUIReq, FAVER_REQ);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to communicate with UI agent",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XVER_RESP)
				{
					memcpy(pstSysInfo, &(stBLData.stRespDtls.stSysInfo),
														sizeof(SYSINFO_STYPE));

					if(iAppLogEnabled == 1)
					{
						sprintf(szAppLogData, "UI Agent Version No [%s]", stBLData.stRespDtls.stSysInfo.szFAVer);
						addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
					}

					/* Reset the BL data flag */
					bBLDataRcvd = PAAS_FALSE;

					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}

		/* Adding SCA Version information to the structure */
		strcpy(pstSysInfo->szSelfVer, SCA_APP_VERSION);
		memset(szAppLogData, 0x00, sizeof(szAppLogData));


		/* Adding SCA Build Number to the structure */
		strcpy(pstSysInfo->szSelfBuildNum, SCA_BUILD_NUMBER);
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "SCA Application Version No [%s], SCA Application Build No [%s]", SCA_APP_VERSION, SCA_BUILD_NUMBER);
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: appendSpacesToString
 *
 * Description	: This function adds spaces to the given string.
 *
 * Input Params	: String.
 *
 * Output Params: String with numOfSpaces appended.
 * ============================================================================
 */
static void appendSpacesToString(char *ptrStr, int numOfSpaces)
{
	int		iLen	= 0;

	iLen = strlen(ptrStr);
	if(iLen > 0)
	{
		while(numOfSpaces > 0)
		{
			strcat(ptrStr, " ");
			numOfSpaces--;
		}
	}

	return;
}

/*
 * ============================================================================
 * Function Name: getFileNamewithPlatformPrefix
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void getFileNamewithPlatformPrefix(char *pszFileName)
{
	static int iPlatForm = -1;
	char szFileName[MAX_FILE_NAME_LEN]  = "";

	if(iPlatForm == -1) //For the first call it would get the platform
	{
		iPlatForm = getDevicePlatform();
	}

	switch(iPlatForm)
	{
		case MODEL_MX925:
			sprintf(szFileName, "925_%s", pszFileName);
			break;
		case MODEL_MX915:

		default:
			sprintf(szFileName, "915_%s", pszFileName);
			break;
	}

	memset(pszFileName, 0x00, MAX_FILE_NAME_LEN);
	strcpy(pszFileName, szFileName);
}

/*
 * ============================================================================
 * Function Name: getManualCardData
 *
 * Description	:
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getManualCardData(int iOpt, char * szPAN, char * szExpDt,
				char * szCVV, char * szClrDt, char* szEparms, char* szEncBlob, char **szSBTData, char *szEncPayLoad, char *szVsdInitVector, char *szClrPAN)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	char			szPANTitle[100]		= "";
	char			szExpDtTitle[100]	= "";
	char			szCVVTitle[100]		= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	PAAS_BOOL		bEmvEnabled			= PAAS_FALSE;
	MANUAL_PTYPE	pstManDtls			= NULL;
	XEVT_PTYPE		pstXevt				= NULL;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	//Get EMV status
	bEmvEnabled = isEmvEnabledInDevice();

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */

	disableCardReaders();

	/* Currently these forms will be displayed by FA. XGCD command needs
	 * to be sent for the same purpose */
	whichForm = MANUAL_ENTRY_FRM;

	/* Initialize the request buffer */
	initUIReqMsg(&stUIReq, INIT_REQ);

	/* Get the Enter Card Number Prompt */
	fetchScrTitle(szPANTitle, ENTER_PAN_TITLE);

	/* Get the Enter Expiry Prompt */
	fetchScrTitle(szExpDtTitle, ENTER_EXPDT_TITLE);

	/* Get the Enter CVV Prompt */
	fetchScrTitle(szCVVTitle, ENTER_CVV_TITLE);

	/* Frame the Manual Card Entry Request */
	getManualCardDataUtil(iOpt, szPANTitle, szExpDtTitle, szCVVTitle,
																stUIReq.pszBuf);

	/* Send the batch message to the UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XGCD_RESP)
				{
					pstManDtls = &(stBLData.stRespDtls.stManualInfo);

					if(stBLData.iStatus == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Copying the man card data",
											__FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(szPAN, pstManDtls->szPAN);
						strcpy(szCVV, pstManDtls->szCVV);
						strcpy(szExpDt, pstManDtls->szExpDt);
						strcpy(szClrDt, pstManDtls->szExpClrDt);
						strcpy(szEncBlob, pstManDtls->szEncBlob);
						strcpy(szEparms, pstManDtls->szEparms);
						if(getEncryptionType() == VSD_ENC)
						{
							strcpy(szEncPayLoad, pstManDtls->szVsdKSN);
							strcpy(szVsdInitVector, pstManDtls->szVsdIV);
							strcpy(szClrPAN, pstManDtls->szClearPAN);
#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: Clear PAN = [%s]", DEVDEBUGMSG,
														__FUNCTION__, szClrPAN);
					APP_TRACE(szDbgMsg);
#endif
						}
/* KranthiK1:
 * Commented this part as the functionality is same for all the card types
 */
#if 0
						switch(iOpt)
						{
						case GIFT_CARD:
							debug_sprintf(szDbgMsg, "%s: Copying for gift card",
												__FUNCTION__);
							APP_TRACE(szDbgMsg);

							strcpy(szPAN, pstManDtls->szPAN);
							strcpy(szExpDt, pstManDtls->szExpDt);
							strcpy(szClrDt, pstManDtls->szExpClrDt);
							strcpy(szEparms, pstManDtls->szEparms);
							strcpy(szEncBlob, pstManDtls->szEncBlob);
							strcpy(szCVV, pstManDtls->szCVV);
							break;

						case PRIVATE_LABEL:
						case PAYACCOUNT:
						case NON_GIFT_CARD:
							debug_sprintf(szDbgMsg, "%s: Copying for pymt or private card",
												__FUNCTION__);
							APP_TRACE(szDbgMsg);

							strcpy(szPAN, pstManDtls->szPAN);
							strcpy(szCVV, pstManDtls->szCVV);
							strcpy(szExpDt, pstManDtls->szExpDt);
							strcpy(szClrDt, pstManDtls->szExpClrDt);
							strcpy(szEncBlob, pstManDtls->szEncBlob);
							strcpy(szEparms, pstManDtls->szEparms);
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Should not come here",
												__FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
#endif
						rv = SUCCESS;
					}
					else if(stBLData.iStatus == UI_CANCEL_PRESSED)
					{
						/* User pressed CANCEL */
						debug_sprintf(szDbgMsg, "%s: CANCELLED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "User Pressed Cancel Button From Manual Card Details Entry Screen");
							addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
						}

						rv = UI_CANCEL_PRESSED;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to get details",
																__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_DEVICE_APP;
					}

					bBLDataRcvd = PAAS_FALSE;

					/*
					 * If STB is enabled, then we need to wait for the XEVT data
					 * thats why not setting bwait to false here
					 */
					if(!isSTBLogicEnabled())
					{
						bWait = PAAS_FALSE;
					}
					else
					{
						if(rv != SUCCESS) //If STB is enabled, checking we have received card data..have to wait only we have received the card
						{
							bWait = PAAS_FALSE;
						}
					}
				}
				else if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlType == 90) //STB Data Event
						{
							debug_sprintf(szDbgMsg, "%s: STB Event Received", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							/* Assisnging the Data pointer, please make sure that it is freed in the caller function*/
							*szSBTData = pstXevt->pszData;

							bWait = PAAS_FALSE;
						}
						bBLDataRcvd = PAAS_FALSE;
					}
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showErrorDispForm
 *
 * Description	:
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showErrorDispForm(char * szMsg, int iTimeOut)
{
	int					rv								= SUCCESS;
	int					iDisableFormFlag				= 0;
	int					locTimeOutVal					= 0;
	int					iAppLogEnabled					= isAppLogEnabled();
	char				szFileName[MAX_FILE_NAME_LEN]	= "";
	char				szAppLogData[300]				= "";
	PAAS_BOOL			bWait							= PAAS_TRUE;
	LABEL_STYPE_TITLE 	stLblTitle;
	UIREQ_STYPE			stUIReq;

#ifdef DEBUG
	char	szDbgMsg[256]					= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = GEN_DISP_FRM;

	/* Initialize the batch message */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	memset(&stLblTitle, 0x00, sizeof(stLblTitle));
	//Split title string of required length(i.e Based on maxLabelLength defined for Mx915 or Mx925).
	splitStringOfReqLength(szMsg, giLabelLen-5, &stLblTitle);
	debug_sprintf(szDbgMsg, "%s: szTitle1 =%s, szTitle2 =%s, szTitle3 =%s",__FUNCTION__,
			stLblTitle.szTitle1, stLblTitle.szTitle2, stLblTitle.szTitle3);
	APP_TRACE(szDbgMsg);



	/*Set the title */
	if ( strlen(stLblTitle.szTitle3) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_1, PROP_STR_CAPTION, 
												stLblTitle.szTitle1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION, 
												stLblTitle.szTitle2, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_5, PROP_STR_CAPTION, 
												stLblTitle.szTitle3, stUIReq.pszBuf);
	}
	else if ( strlen(stLblTitle.szTitle2) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION, 
												stLblTitle.szTitle1, stUIReq.pszBuf);
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_5, PROP_STR_CAPTION, 
												stLblTitle.szTitle2, stUIReq.pszBuf);
	}
	else if ( strlen(stLblTitle.szTitle1) > 0 )
	{
		setStringValueWrapper(PAAS_GEN_DISPLAY_LBL_4, PROP_STR_CAPTION, 
												stLblTitle.szTitle1, stUIReq.pszBuf);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Title not found",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	strcpy(szFileName, "indicator_red.png");
	getFileNamewithPlatformPrefix(szFileName);

	/* Set the image on the screen */
	setStringValueWrapper(PAAS_GEN_DISPLAY_IMG_1, PROP_STR_IMAGE_FILE_NAME,
											szFileName, stUIReq.pszBuf);

	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	/* Send batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);
		}
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Error Information Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}
	}

	locTimeOutVal = iTimeOut * 1000;
	if(locTimeOutVal > 0)
	{
		svcWait(locTimeOutVal);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getOptionsForCtrlid
 *
 * Description	: Gets the current options for a control id in the initialized form.
 *
 * Input Params	: Control Id
 *
 * Output Params:
 * ============================================================================
 */
static int getOptionsForCtrlid (int ctrlId, char * szBuf)
{
	int result = SUCCESS;
	unsigned short options = 0;
	PAAS_BOOL bWait = PAAS_TRUE;
	PROPDTLS_PTYPE stPropInfo;
#ifdef DEBUG
	char	szDbgMsg[256]= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	getShortValueWrapper(ctrlId, PROP_SHORT_OPTIONS, szBuf);

	/* Send batch message to UI agent */

	result = sendUIReqMsg(szBuf);
	if(result != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		result = ERR_DEVICE_APP;
	}
	else
	{
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XGPV_RESP)
				{
					stPropInfo = &(stBLData.stRespDtls.stPropInfo);
					options = stPropInfo->propValue;
					debug_sprintf (szDbgMsg, "%s: The old options value of the form is %d",
										       					__FUNCTION__, options);
					APP_TRACE(szDbgMsg);

					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	return options;
}
#if 0
/*
 * ============================================================================
 * Function Name: getSelectedRadioOptForCtrlid
 *
 * Description	: Gets the selected option for a control id in the initialized form.
 *
 * Input Params	: Control Id
 *
 * Output Params:
 * ============================================================================
 */
static int getSelectedRadioOptForCtrlid (int ctrlId, char * szBuf, int *piSelectedPropValue)
{
	int 		rv 		= SUCCESS;
	PAAS_BOOL 	bWait 	= PAAS_TRUE;
	PROPDTLS_PTYPE stPropInfo;

#ifdef DEBUG
	char	szDbgMsg[256]= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	getBoolValueWrapper(ctrlId, PROP_BOOL_SELECTED, szBuf);

	/* Send batch message to UI agent */

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		while(bWait == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Waiting for data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(bBLDataRcvd == PAAS_TRUE)
			{
				debug_sprintf(szDbgMsg, "%s: BL Data is received!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				debug_sprintf(szDbgMsg, "%s: Acquired Mutex", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(stBLData.uiRespType == UI_XGPV_RESP)
				{
					debug_sprintf(szDbgMsg, "%s: XGPV response is received!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					stPropInfo = &(stBLData.stRespDtls.stPropInfo);
					*piSelectedPropValue = stPropInfo->propValue;

					debug_sprintf (szDbgMsg, "%s: The selected prop value is %d",
										       					__FUNCTION__, *piSelectedPropValue);
					APP_TRACE(szDbgMsg);

					initUIReqMsg(&stUIReq, BATCH_REQ);
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			svcWait(10);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}

	return rv;
}
#endif

/*
* ============================================================================
* Function Name: setCreditAppScreenTitle
*
* Description    : Sets the screen Title which fits into the multi labels.
*
* Input Params   : title, amount title
*
* Output Params:
* ============================================================================
*/
static int setCreditAppScreenTitle(char* szTitle, int* ctrlids, int count, int iLabelLen, char* pszBuf)
{
      LABEL_STYPE_TITLE     stLblTitle;
      int 					iPlatForm 	= -1;

#ifdef DEBUG
      char              szDbgMsg[1024]     = "";
#endif

      debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
      APP_TRACE(szDbgMsg);

      if(szTitle == NULL || strlen(szTitle) == 0)
      {
            debug_sprintf(szDbgMsg, "%s: Null Title is passed!!!", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            return FAILURE;
      }

      iPlatForm = getDevicePlatform();
      if(iPlatForm == MODEL_MX925)
      {
    	  iLabelLen += 15;
      }

      memset(&stLblTitle, 0x00, sizeof(stLblTitle));
      //Split title string of required length(i.e Based on maxLabelLength defined for Mx915 or Mx925).
      splitStringOfReqLength(szTitle, iLabelLen, &stLblTitle);

      debug_sprintf(szDbgMsg, "%s: szTitle1 =[%s], szTitle2 =[%s], szTitle3 =[%s], szTitle4 =[%s], szTitle5 =[%s], szTitle6 =[%s]",__FUNCTION__,
                  stLblTitle.szTitle1, stLblTitle.szTitle2, stLblTitle.szTitle3, stLblTitle.szTitle4, stLblTitle.szTitle5, stLblTitle.szTitle6);
      APP_TRACE(szDbgMsg);
      // CID-67952,67351,67343: 27-Jan-16: MukeshS3:
      // We must make use of count values passed from different-different locations, before trying to access any element of ctrlids[] array
      // So, adding one more check of count value everywhere, just before setting the String property.
      if(count >= 1)
      {
      setStringValueWrapper(ctrlids[0], PROP_STR_CAPTION, stLblTitle.szTitle1, pszBuf);
      }
      if ( strlen(stLblTitle.szTitle2) > 0 && (count >= 2))
      {
            setStringValueWrapper(ctrlids[1], PROP_STR_CAPTION,
									stLblTitle.szTitle2, pszBuf);
            if ( strlen(stLblTitle.szTitle3) > 0 && (count >= 3) )
            {
                  setStringValueWrapper(ctrlids[2], PROP_STR_CAPTION,
									stLblTitle.szTitle3, pszBuf);
                  if ( strlen(stLblTitle.szTitle4) > 0 && (count >= 4))
                  {
                        setStringValueWrapper(ctrlids[3], PROP_STR_CAPTION,
									stLblTitle.szTitle4, pszBuf);

                        if ( strlen(stLblTitle.szTitle5) > 0 && (count >= 5) )
                        {
                              setStringValueWrapper(ctrlids[4], PROP_STR_CAPTION,
      									stLblTitle.szTitle5, pszBuf);
                              if ( strlen(stLblTitle.szTitle6) > 0 && (count >= 6) )
                              {
                                    setStringValueWrapper(ctrlids[5], PROP_STR_CAPTION,
            									stLblTitle.szTitle6, pszBuf);
                              }
                        }
                  }
            }
      }


      debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
      APP_TRACE(szDbgMsg);

      return SUCCESS;
}

/*
* ============================================================================
* Function Name: setScreenTitle
*
* Description    : Sets the screen Title which fits into the multi labels.
*
* Input Params   : title, amount title
*
* Output Params:
* ============================================================================
*/
static int setScreenTitle(char* szTitle, char* szAmtTitle, int* ctrlids, int count, int iLabelLen, char* pszBuf)
{
      LABEL_STYPE_TITLE       stLblTitle;
      PAAS_BOOL               bTranAmtSet = PAAS_FALSE;

#ifdef DEBUG
      char              szDbgMsg[512]     = "";
#endif

      debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
      APP_TRACE(szDbgMsg);

      if(count == 3)
      {
            bTranAmtSet = PAAS_TRUE;
      }

      if(szTitle == NULL || strlen(szTitle) == 0)
      {
            debug_sprintf(szDbgMsg, "%s: Null Title is passed!!!", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            return FAILURE;
      }

      memset(&stLblTitle, 0x00, sizeof(stLblTitle));
      //Split title string of required length(i.e Based on maxLabelLength defined for Mx915 or Mx925).
      splitStringOfReqLength(szTitle, iLabelLen, &stLblTitle);

      debug_sprintf(szDbgMsg, "%s: szTitle1 =%s, szTitle2 =%s, szTitle3 =%s",__FUNCTION__,
                  stLblTitle.szTitle1, stLblTitle.szTitle2, stLblTitle.szTitle3);
      APP_TRACE(szDbgMsg);
      // CID-67952,67351,67343: 27-Jan-16: MukeshS3:
      // We must make use of count values passed from different-different locations, before trying to access any element of ctrlids[] array
      // So, adding one more check of count value everywhere, just before setting the String property.
      if(count >= 1)
      {
      setStringValueWrapper(ctrlids[0], PROP_STR_CAPTION, stLblTitle.szTitle1, pszBuf);
      }
      if ( strlen(stLblTitle.szTitle2) > 0 && (count >= 2))
      {
            setStringValueWrapper(ctrlids[1], PROP_STR_CAPTION, 
									stLblTitle.szTitle2, pszBuf);
            if ( strlen(stLblTitle.szTitle3) > 0 && (count >= 3) )
            {
                  setStringValueWrapper(ctrlids[2], PROP_STR_CAPTION, 
									stLblTitle.szTitle3, pszBuf);
                  if ( strlen(stLblTitle.szTitle4) > 0 && (count >= 4) )
                  {
                        setStringValueWrapper(ctrlids[3], PROP_STR_CAPTION, 
									stLblTitle.szTitle4, pszBuf);
                        
						bTranAmtSet = PAAS_TRUE;
                  }
            }
            else
            {
                  /* Set the sub-title to display the net due amount */
                  if (szAmtTitle != NULL && (strlen(szAmtTitle) > 0) && (count >= 3))
                  {
                        setStringValueWrapper(ctrlids[2], PROP_STR_CAPTION, 
												szAmtTitle, pszBuf);
                  }
                  bTranAmtSet = PAAS_TRUE;
            }
      }
      else
      {
            /* Set the sub-title to display the net due amount */
    	  if (szAmtTitle != NULL && (strlen(szAmtTitle) > 0) && (count >= 2))
            {
                  setStringValueWrapper(ctrlids[1], PROP_STR_CAPTION, 
												szAmtTitle, pszBuf);
            }
            bTranAmtSet = PAAS_TRUE;
      }
      if(bTranAmtSet == PAAS_FALSE)
      {
            /* Set the sub-title to display the net due amount */
    	  if (szAmtTitle != NULL && (strlen(szAmtTitle) > 0) && (count >= 4))
            {
                  setStringValueWrapper(ctrlids[3], PROP_STR_CAPTION, 
												szAmtTitle, pszBuf);
            }
      }


      debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
      APP_TRACE(szDbgMsg);

      return SUCCESS;
}


/*
 * ============================================================================
 * Function Name: splitStringOfReqLength
 *
 * Description	: Splits the given string into equal length.
 *
 * Input Params	: string, length, structure for storing the strings
 *
 * Output Params:
 * ============================================================================
 */
int splitStringOfReqLength(char *szTmp, int maxLblLength, LABEL_PTYPE_TITLE pstLblTitle)
{
	int rv  = SUCCESS;
	int len = 0, tempLen = 0;

#ifdef DEBUG
	//char	szDbgMsg[256]= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);
	if( pstLblTitle == NULL)
	{
		return FAILURE;
	}
	len = strlen(szTmp);
	while(1)
	{
		if( len > maxLblLength )
		{
			tempLen = maxLblLength;
			while((szTmp[tempLen] != ' ') && (tempLen > 0))
			{
				tempLen--;
			}
			if(tempLen == 0)
			{
				tempLen = maxLblLength;
			}
			memcpy(pstLblTitle->szTitle1, szTmp, tempLen);
			szTmp += tempLen;
			len = len - tempLen;
			if( len > maxLblLength)
			{

				tempLen = maxLblLength;
				while((szTmp[tempLen] != ' ') && (tempLen > 0))
				{
					tempLen--;
				}
				if(tempLen == 0)
				{
					tempLen = maxLblLength;
				}
				memcpy(pstLblTitle->szTitle2, szTmp, tempLen);
				szTmp += tempLen;
				len = len -tempLen;
			}
			else
			{
				memcpy(pstLblTitle->szTitle2, szTmp, len);
				break;
			}
			if( len > maxLblLength)
			{

				tempLen = maxLblLength;
				while((szTmp[tempLen] != ' ') && (tempLen > 0))
				{
					tempLen--;
				}
				if(tempLen == 0)
				{
					tempLen = maxLblLength;
				}
				memcpy(pstLblTitle->szTitle3, szTmp, tempLen);
				szTmp += tempLen;
				len = len -tempLen;
			}
			else
			{
				memcpy(pstLblTitle->szTitle3, szTmp, len);
				break;
			}
			if( len > maxLblLength)
			{

				tempLen = maxLblLength;
				while((szTmp[tempLen] != ' ') && (tempLen > 0))
				{
					tempLen--;
				}
				if(tempLen == 0)
				{
					tempLen = maxLblLength;
				}
				memcpy(pstLblTitle->szTitle4, szTmp, tempLen);
				szTmp += tempLen;
				len = len -tempLen;
			}
			else
			{
				memcpy(pstLblTitle->szTitle4, szTmp, len);
				break;
			}
			if( len > maxLblLength)
			{

				tempLen = maxLblLength;
				while((szTmp[tempLen] != ' ') && (tempLen > 0))
				{
					tempLen--;
				}
				if(tempLen == 0)
				{
					tempLen = maxLblLength;
				}
				memcpy(pstLblTitle->szTitle5, szTmp, tempLen);
				szTmp += tempLen;
				len = len -tempLen;
			}
			else
			{
				memcpy(pstLblTitle->szTitle5, szTmp, len);
				break;
			}
			if( len > maxLblLength)
			{

				tempLen = maxLblLength;
				while((szTmp[tempLen] != ' ') && (tempLen > 0))
				{
					tempLen--;
				}
				if(tempLen == 0)
				{
					tempLen = maxLblLength;
				}
				memcpy(pstLblTitle->szTitle6, szTmp, tempLen);
				szTmp += tempLen;
				len = len -tempLen;
				break;
			}
			else
			{
				memcpy(pstLblTitle->szTitle6, szTmp, len);
				break;
			}
			break;
		}
		else
		{
			memcpy(pstLblTitle->szTitle1, szTmp, strlen(szTmp));
			break;
		}
	}

	//debug_sprintf(szDbgMsg, "%s: --- Returning rv =%d ---", __FUNCTION__, rv);
	//APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCurFormNumber
 *
 * Description	: Gets the  form number of the current form shown on the device screen.
 *
 * Input Params	: None
 *
 * Output Params: Form Number
 * ============================================================================
 */
int getCurFormNumber()
{
	return whichForm;
}

/*
 * ============================================================================
 * Function Name: captureCardDtlsForPreswipe
 *
 * Description	: This API sends the command for capturing card dtls to UIAgent.
 * 					This function will be called only if we do not have Payment Data
 * 					on Preswipe. For VAS only case, other function is used
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int captureCardDtlsForPreswipe()
{
	int 			rv				 	= SUCCESS;
	int 			iLen 			 	= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char            szVASMode[4+1]   	= "";
	char			szNFCVASMode[4+1]	= "";
	char			szMerchIndex[10+1]	= "";
	char			szAppLogData[300]	= "";
	PAAS_BOOL		bEmvEnabled;
	PAAS_BOOL		bNFCVASModeSuccess 	= PAAS_FALSE;

#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Entering----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	bEmvEnabled = isEmvEnabledInDevice();
#if 0
	/*AjayS2: 16-Feb-2016:
	 * bVASRequired is TRUE if Wallet is enabled and we still not received any VAS data
	 */
	if(PAAS_TRUE == isWalletEnabled() && PAAS_TRUE != isVASDataCaptured())
	{
		bVASRequired = PAAS_TRUE;
	}
#endif

	//If we have Sent D41 and Card readers are enable need to send 72
	//If VAS received on Preswipe, we send C30/S20 without D41 values, but now if someone press cancel, we are clearing VAS data,
	// so we need to resend C30 now with D41 values, so we have to cancel the current C30/S20, so need disablecardeader here
	//If VAS has been captured, we should not send any amount or any VAS field
	disableCardReaders();

	memset(szMerchIndex, 0x00, sizeof(szMerchIndex));

	getMerchantIndexIfAvaliable(szMerchIndex);

	memset(szNFCVASMode, 0x00, sizeof(szNFCVASMode));

	getVASTermModeIfAvaliable(szNFCVASMode);


	iLen = strlen(szNFCVASMode);
	if(isWalletEnabled() == PAAS_TRUE && (iLen >= 4)/* && (szNFCVASMode != NULL) */) // T_raghavendran R1 CID 83365 (#1 of 1): Array compared against 0 (NO_EFFECT)
	{
		if(((strcmp(szNFCVASMode, VAS_ONLY) == SUCCESS) ||  (strcmp(szNFCVASMode, PAYMENT_ONLY) == SUCCESS)
				|| (strcmp(szNFCVASMode, VAS_AND_PAYMENT) == SUCCESS) || (strcmp(szNFCVASMode, VAS_OR_PAYMENT) == SUCCESS) ))
		{
			bNFCVASModeSuccess = PAAS_TRUE;
		}
		else
		{
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "POS sends [%s] mode as NFCVAS MODE,Cannot honour. So Moving to Default Mode", szNFCVASMode);
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			bNFCVASModeSuccess = PAAS_FALSE;
		}
	}

	if(bEmvEnabled == PAAS_TRUE)
	{
#if 0
		/*
		 * Praveen_P1: Not toggling during the transaction flow
		 */
		rv = processEmvD25Cmd(PAAS_FALSE);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to send D25 command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
#endif

		/*AjayS2: 16-Feb-2016:
		 * If emv is enabled and emvctls is also enabled, we do not have to send any D41 parameters, if VAS is already received,
		 * But if emvenabled and emvctls is disabled, we have to send PYMNT only in D41 parameters in Preswipe screen if vas is already received.
		 * On Preswipe, we do not send any amount in both of the above cases.
		 * If emv is enabled and emvctls is also enabled, we will send VAS_ONLY in D41 parameters, with amount as zero, if VAS is NOT received yet,
		 * If emv is enabled and emvctls is disabled, we will send VAS and Pymnt in D41 parameters, if VAS is NOT received yet,
		 * Note: We do not have to send amy amount field if emvctls is disabled.
		 */
		if(isWalletEnabled())
		{
			if(isVASDataCaptured())
			{
				//Wallet Enabled and VAS data already Captured
				if(isContactlessEmvEnabledInDevice())
				{
					//AjayS2: 16 Feb-2016
					//Do Nothing, VAS already captured, We wont send any amount So No Tap will work.
				}
				else
				{
					//Need to send Pymnt Only, without any amount Tag
					strcpy(szVASMode, PAYMENT_ONLY);
				}
			}
			else
			{
				//Wallet Enabled and VAS Not Captured
				if(isContactlessEmvEnabledInDevice())
				{
					//AjayS2: 16 Feb-2016
					//Need to send amount as zero so Tap will work, and Mode as VAS_Only.
					strcpy(szVASMode, VAS_ONLY);
				}
				else
				{
					if(bNFCVASModeSuccess == PAAS_TRUE)
					{
						strcpy(szVASMode, szNFCVASMode);
					}
					else
					{
						//Can send VAS and Payment, without any amount Tag,
						//In this case emvctls is off, so we can get VAS and Pymnt data
						strcpy(szVASMode, VAS_AND_PAYMENT);
					}
				}
			}
		}
		rv = sendC30CmdToGetChipCardData(NULL, szVASMode, szMerchIndex, NULL);
	}
	else
	{
		if(isWalletEnabled())
		{
			if(isVASDataCaptured())
			{
				//Wallet enabled, VAS Captured already
				strcpy(szVASMode, PAYMENT_ONLY);
			}
			else
			{
				if(bNFCVASModeSuccess == PAAS_TRUE)
				{
					strcpy(szVASMode, szNFCVASMode);
				}
				else
				{
					//Wallet Enable, Vas not received
					strcpy(szVASMode, VAS_AND_PAYMENT);
				}
			}
		}
		rv = sendS20CmdToGetMSRCardData(szVASMode, szMerchIndex, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: updateLabelOnLIScreen()
 *
 * Description	: This API would be used to update a label on the lineitemscreen
 * 				  or welcome screen
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateLabelOnLIScreen()
{
	int			rv			= SUCCESS;
	char		szTmp[100]	= "";
	PAAS_BOOL	bWait		= PAAS_TRUE;

#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stUIReq, BATCH_REQ);

	getDisplayMsg(szTmp, MSG_PROCESSING);
	setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);

	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		bBLDataRcvd = PAAS_FALSE;
		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			svcWait(5);
		}
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: showLIForm()
 *
 * Description	: This API would be used to
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showLIForm(PAAS_BOOL bEnablePreSwipeifReqd, PAAS_BOOL bCardRemovalMessage)
{
	int			rv						= SUCCESS;
	int			iEmvCrdPresence			= 0;
	char		szTmp[100]				= "";
//	char		szFileName[32]			= "";		// CID-67462: 22-Jan-16: MukeshS3: Increasing file name size to MAX_FILE_NAME_LEN
	char		szFileName[MAX_FILE_NAME_LEN] = "";	// because, at some places, it is being memset with size MAX_FILE_NAME_LEN i.e. 50
	char		szPymtCode[15]			= "";
	char		szMerchIndex[10 + 1]	= "";
	char		szNFCVASMode[4+1]		= "";
	PAAS_BOOL	bWait					= PAAS_TRUE;
	PAAS_BOOL	bEmvEnabled				= PAAS_FALSE;
	PAAS_BOOL	bNFCVASMode				= PAAS_FALSE;
	LABEL_STYPE_TITLE 	stLblTitle;
	UIREQ_STYPE	stUIReq;

#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stUIReq, BATCH_REQ);

#if 0
	getDisplayMsg(szTmp, MSG_PRESWIPE_EMV);
	setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);

	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		bBLDataRcvd = PAAS_FALSE;
		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			svcWait(5);
		}
	}
#endif
	if(bCardRemovalMessage)
	{
		memset(szTmp, 0x00, sizeof(szTmp));
		getDisplayMsg(szTmp, MSG_CARD_REMOVED);

		if(isLineItemDisplayEnabled())
		{
			setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
		}
        else
        {
               memset(&stLblTitle, 0x00, sizeof(stLblTitle));
               //Split title string of required length(i.e Based on maxLabelLength defined for Mx915 or Mx925).
               splitStringOfReqLength(szTmp, giLabelLen-5, &stLblTitle);

               /* Set the label 1 for Card Removal message on the screen */
               setStringValueWrapper(PS_WELCOME_SCREEN_LBL_3, PROP_STR_CAPTION, stLblTitle.szTitle1, stUIReq.pszBuf);
               if( strlen(stLblTitle.szTitle2) > 0 )
               {
                     /* Set the label 2 for Card Removal message on the screen */
                     setStringValueWrapper(PS_WELCOME_SCREEN_LBL_4, PROP_STR_CAPTION, stLblTitle.szTitle2, stUIReq.pszBuf);
               }
               else
               {
                     /* Clear the label 2 for the Card Removal on the screen */
                     setStringValueWrapper(PS_WELCOME_SCREEN_LBL_4, PROP_STR_CAPTION, "", stUIReq.pszBuf);
               }
        }

		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);
	}
	else
	{
		if(isPaypalTenderEnabled() && isFullLineItemDisplayEnabled() == PAAS_FALSE)
		{
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_PAYPAL_BTN, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);

			rv = getPaypalPymtCodeDtlsInSession(szPymtCode);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get Paypal Payment Code Details in Session", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return rv;
			}

			if((strlen(szPymtCode) > 0) || (isPreSwipeDone() == TRUE))
			{
				memset(szFileName, 0x00, sizeof(szFileName));
				sprintf(szFileName, "%s", "Paypal-btn-pressed.png");
				getFileNamewithPlatformPrefix(szFileName);

				/* Set the down button image */
				setStringValueWrapper(PAAS_LINEITEMSCREEN_PAYPAL_BTN, PROP_STR_DOWN_IMAGE_FILE_NAME,
						szFileName, stUIReq.pszBuf);

				setBoolValueWrapper(PAAS_LINEITEMSCREEN_PAYPAL_BTN, PROP_BOOL_ENABLE, 0, stUIReq.pszBuf);
			}
			else
			{
				setBoolValueWrapper(PAAS_LINEITEMSCREEN_PAYPAL_BTN, PROP_BOOL_ENABLE, 1, stUIReq.pszBuf);
			}
		}

		if(isSwipeAheadEnabled() == PAAS_TRUE)
		{
			if(isPreSwipeDone() == PAAS_FALSE) //Pre-Swipe is not done
			{
				debug_sprintf(szDbgMsg, "%s: Swipe Ahead is enabled, adding card read command to the request", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				//Get EMV status
				bEmvEnabled = isEmvEnabledInDevice();

				memset(szTmp, 0x00, sizeof(szTmp));
				if(bEmvEnabled == PAAS_TRUE)
				{
					iEmvCrdPresence = EMVCrdPresenceOnPreswipe();
					if(iEmvCrdPresence == 1)
					{
						getDisplayMsg(szTmp, MSG_AFTER_PRE_SWIPE_EMV);
						setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
					}
					else
					{
						getDisplayMsg(szTmp, MSG_PRESWIPE_EMV);
						setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);

					}
				}
				else
				{
					getDisplayMsg(szTmp, MSG_PRE_SWIPE);
					//Temp: Remove after putting req string in display prompts
					setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
				}

				/* Set the label for the Pre-Swipe message on the screen */
				//setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);

				/* Show the form */
				//showFormWrapper(PM_NORMAL, stUIReq.pszBuf); //Sending the XIFM before Q13 to avoid complications

			}
			else //Pre-Swipe is done
			{
				/*Need to remove logo and Message related to Apple Pay.Hence removing those when Payment only mode is sent as a NFCVAS_MODE in Start Session.*/
				//Get EMV status
				//bEmvEnabled = isEmvEnabledInDevice();
				//if(bEmvEnabled == PAAS_TRUE)
				//{
				/*Akshaya: Removing unwanted Checks here*/
				getVASTermModeIfAvaliable(szNFCVASMode);
				if(strcmp(szNFCVASMode, PAYMENT_ONLY) == SUCCESS)
				{
					bNFCVASMode = PAAS_TRUE;
				}
				else
				{
					bNFCVASMode = PAAS_FALSE;
				}
				//}
				/*if only card data is captured then we need to send D41 to get VAS data*/
				if(PAAS_TRUE == isWalletEnabled() && isVASDataCaptured() == PAAS_FALSE && bNFCVASMode == PAAS_FALSE)
				{
					memset(szMerchIndex, 0x00, sizeof(szMerchIndex));
					getMerchantIndexIfAvaliable(szMerchIndex);
					sendD41cmdToEMVAgent(PAAS_FALSE, szMerchIndex, NULL);
				}

				memset(szTmp, 0x00, sizeof(szTmp));
				/* Get the display prompt/message from the .ini file */
				if(EMVCrdPresenceOnPreswipe() == 1)
				{
					getDisplayMsg(szTmp, MSG_AFTER_PRE_SWIPE_EMV);
					/* Set the label for the After-Pre-Swipe message on the screen */
					setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
				}
				else
				{
					getDisplayMsg(szTmp, MSG_AFTER_PRE_SWIPE);
					/* Set the label for the After-Pre-Swipe message on the screen */
					setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
				}

				/* Show the form */
				//showFormWrapper(PM_NORMAL, stUIReq.pszBuf);
			}
			/*To check the NFCVASMode sent in Start session. This is to check here whether 0003(Payment only) comes from start session*/
			/* we need to remove logo and Message related to Apple Pay.Hence removing those when Payment only mode is sent*/
			if(bNFCVASMode == PAAS_FALSE)
			{
				if(PAAS_TRUE == isWalletEnabled() && isFullLineItemDisplayEnabled() == PAAS_FALSE)
				{
					if(isVASDataCaptured() == PAAS_FALSE)
					{
						sprintf(szFileName, "%s", "liscreen_wallet_banner.png");
						getFileNamewithPlatformPrefix(szFileName);
						setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_LBL, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
						setStringValueWrapper(PAAS_LINEITEMSCREEN_WALLET_IMG, PROP_STR_IMAGE_FILE_NAME, szFileName, stUIReq.pszBuf);
						setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_IMG, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
					}
					else
					{
						memset(szTmp, 0x00, sizeof(szTmp));
						getDisplayMsg(szTmp, MSG_LOYALTY_CAPTURED);
						setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_IMG, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
						setStringValueWrapper(PAAS_LINEITEMSCREEN_WALLET_LBL, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
						setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_LBL, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
					}
				}
			}

			/* Show the form */
			showFormWrapper(PM_NORMAL, stUIReq.pszBuf); //Sending the XIFM before Q13 to avoid complications

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Swipe Ahead is NOT enabled", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			setBoolValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_IMG, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_LBL, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);

			/* Show the form */
			showFormWrapper(PM_NORMAL, stUIReq.pszBuf);
		}
	}

	/* Send batch message to UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to send UI Request message", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_LISTBOX_RESP || stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait 		= PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);
		}

		/* If Card Capture is Required, Send the Corresponding commnad */
		if(isSwipeAheadEnabled() == PAAS_TRUE && isPreSwipeDone() == PAAS_FALSE && bEnablePreSwipeifReqd == PAAS_TRUE)
		{
			rv= captureCardDtlsForPreswipe();//Send Commands to XPI for card data capture
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to send XPI cmds",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				return ERR_DEVICE_APP;
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: setWaterMarkText()
 *
 * Description	: This API would be used to set or clear WaterMark Text onto the screen for Demo mode session
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int setWaterMarkText(PAAS_BOOL bShow)
{
	int			rv				= SUCCESS;
	int			iFontSize		= 0;
	char		szTmpDemo[1024]	= "";
	char		tempMsg[100]	= "";
	PAAS_BOOL	bWait			= PAAS_TRUE;
	UIREQ_STYPE	stUIReq;
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initUIReqMsg(&stUIReq, XSWT_REQ);

	memset(szTmpDemo, 0x00, sizeof(szTmpDemo));

	if(bShow)
	{
		getDisplayMsg(szTmpDemo, MSG_DEMO_MODE);
		//strcpy(szTmpDemo, "****TAIADSAIHDU****");
	}

	//Setting Font size based on device platform and increasing by 10 units as per
	iFontSize = getDevicePlatform();
	if(iFontSize == MODEL_MX915)
	{
		iFontSize = 48;
	}
	else
	{
		iFontSize = 48;
	}

	sprintf(tempMsg, "%s%c%s%c%d%c", szTmpDemo, FS, "VeraMoBd" /*Font Name*/, FS, iFontSize/*Font Size*/, FS);
	strcat(stUIReq.pszBuf, tempMsg);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		bBLDataRcvd = PAAS_FALSE;
		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				if(stBLData.uiRespType == UI_XSWT_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
					if(stBLData.iStatus == 1)
					{
						debug_sprintf(szDbgMsg, "%s: WaterMark set/removed Successfully for DemoMode", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error in setting/removing WaterMark for DemoMode", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = FAILURE;
					}
				}
				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);
		}
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: displayMsgBox()
 *
 * Description	: This API would be used to display a message box with input text for the specified time period.
 *
 * Input Params	: display text string, timeout in seconds
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int displayMsgBox(DISPMSG_PTYPE	pstDispMsgDtls)
{
	int			button			= 1; // By Default we normally set Ok Button
	int			icon			= 2; //By Default we normally set Info Icon
	int			rv				= SUCCESS;
	int 		iTimeOut		= 0;
	int			iCnt			= 0;
	int			iStartBtn		= 0;
	int			iStartBtnLbl	= 0;
	int			iEmvCrdPresence	= 0;
	char		szTmp[100]		= "";
	char		szBuf[300]		= "";
	char		szFileName[MAX_FILE_NAME_LEN]	= "";
	ullong		endTime			= 0L;
	ullong		curTime			= 0L;
	PAAS_BOOL	bWait			= PAAS_TRUE;
	PAAS_BOOL	bEnterPressed	= PAAS_FALSE;
	PAAS_BOOL	bEmvEnabled		= PAAS_FALSE;
	APPLN_STATE	iLastAppState   = -1;
	UIREQ_STYPE	stUIReq;

#ifdef DEBUG
	char szDbgMsg[512]		= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/* Need to validate the input parameter */
	if((strlen(pstDispMsgDtls->szDispTxt) == 0) && (pstDispMsgDtls->iTimeOut < 0L ))
	{
		debug_sprintf(szDbgMsg, "%s: Display Text and/or Time Out is missing in request", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = ERR_FLD_REQD;
		return rv;
	}
	iLastAppState = getAppState();
	setAppState(PROCESSING_DISPLAY_MESSAGE);
	if(whichForm == LI_DISP_FRM || whichForm == LI_DISP_OPT_FRM )
	{
		initUIReqMsg(&stUIReq, BATCH_REQ);

		if(whichForm == LI_DISP_FRM)
		{
			/* Set the Image as Invisible */
			/*setBoolValueWrapper(PAAS_LINEITEMSCREEN_IMG_1, PROP_BOOL_VISIBLE, 0,
					stUIReq.pszBuf);*/ //Praveen_P1: Changed IMAGE control to Animation
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_ANI_1, PROP_BOOL_VISIBLE, 0,
								stUIReq.pszBuf);
		}
		else if(whichForm == LI_DISP_OPT_FRM)
		{
			iStartBtn    	 = PAAS_LINEITEMSCREEN1_BTN_1;
			iStartBtnLbl 	 = PAAS_LINEITEMSCREEN1_LBL_7;

			for(iCnt = 0; iCnt < MAX_CONSUMER_OPTS; iCnt++)
			{
				/* Set the label as visible */
				setBoolValueWrapper(iCnt + iStartBtnLbl, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);

				/* Set the button as visible */
				setBoolValueWrapper(iCnt + iStartBtn, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
			}
		}
		if(iLastAppState == IN_LINEITEM_QRCODE)
		{
			// Hide QR code image, display text box & button
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_TXT, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_BTN, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_IMG, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
			// Enable Other configured option on left side
			if(isSwipeAheadEnabled())
			{
				if(isPreSwipeDone() == PAAS_FALSE) //Pre-Swipe is not done
				{
					//Get EMV status
					bEmvEnabled = isEmvEnabledInDevice();
					memset(szTmp, 0x00, sizeof(szTmp));
					if(bEmvEnabled == PAAS_TRUE)
					{
						iEmvCrdPresence = EMVCrdPresenceOnPreswipe();
						if(iEmvCrdPresence == 1)
						{
							getDisplayMsg(szTmp, MSG_AFTER_PRE_SWIPE_EMV);
							setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
						}
						else
						{
							getDisplayMsg(szTmp, MSG_PRESWIPE_EMV);
							setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
						}
					}
					else
					{
						getDisplayMsg(szTmp, MSG_PRE_SWIPE);
						//Temp: Remove after putting req string in display prompts
						setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
					}
				}
				else
				{
					memset(szTmp, 0x00, sizeof(szTmp));
					/* Get the display prompt/message from the .ini file */
					if(EMVCrdPresenceOnPreswipe() == 1)
					{
						getDisplayMsg(szTmp, MSG_AFTER_PRE_SWIPE_EMV);
						/* Set the label for the After-Pre-Swipe message on the screen */
						setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
					}
					else
					{
						getDisplayMsg(szTmp, MSG_AFTER_PRE_SWIPE);
						/* Set the label for the After-Pre-Swipe message on the screen */
						setStringValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
					}
				}
				setBoolValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);

				if(isWalletEnabled())
				{
					if(isVASDataCaptured() == PAAS_FALSE)
					{
						sprintf(szFileName, "%s", "liscreen_wallet_banner.png");
						getFileNamewithPlatformPrefix(szFileName);
						setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_LBL, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
						setStringValueWrapper(PAAS_LINEITEMSCREEN_WALLET_IMG, PROP_STR_IMAGE_FILE_NAME, szFileName, stUIReq.pszBuf);
						setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_IMG, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
					}
					else
					{
						memset(szTmp, 0x00, sizeof(szTmp));
						getDisplayMsg(szTmp, MSG_LOYALTY_CAPTURED);
						setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_IMG, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
						setStringValueWrapper(PAAS_LINEITEMSCREEN_WALLET_LBL, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
						setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_LBL, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
					}
				}
			}
			// not handling for paypal
		}
		/* Set the Label as visible */
		setBoolValueWrapper(PAAS_LINEITEMSCREEN1_TXT_1, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);

		//Clear and Add the Text Box, so that we do not append the Texts in the text Box.
		clearTextboxTextWrapper(PAAS_LINEITEMSCREEN1_TXT_1, stUIReq.pszBuf);

		addTextboxTextWrapper(PAAS_LINEITEMSCREEN1_TXT_1, pstDispMsgDtls->szDispTxt, stUIReq.pszBuf);

		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		/* Send batch message to UI agent */
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(5);
			}
		}
	}
	else
	{
		/*	Message Box
		 *  REQ: <STX>XMBX<FS>Title<FS>Text<FS>Icon<FS>Buttons<FS>Blocked<FS>{LRC}
		 */
		initUIReqMsg(&stUIReq, XMBX_REQ);

		memset(szBuf, 0x00, sizeof(szBuf));
		//Daivik:16/9/2016 - Allowing the caller of this function to set up Message Box details as required.
		if(pstDispMsgDtls->cButton != 0)
		{
			button = pstDispMsgDtls->cButton - '0';
		}
		if(pstDispMsgDtls->cIcon != 0)
		{
			icon = pstDispMsgDtls->cIcon - '0';
		}
		sprintf(szBuf, "%s%c%s%c%d%c%d%c%d%c", "Message" /*Title */, FS, pstDispMsgDtls->szDispTxt /*Text Msg*/, FS, icon, FS, button, FS, 0, FS);

		strcat(stUIReq.pszBuf, szBuf);

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			/* Wait for the UI Agent resp */
			bBLDataRcvd = PAAS_FALSE;

			while(bWait == PAAS_TRUE)
			{
				if(getPOSInitiatedState())
				{
					rv = retValForPosIniState();
					break;
				}

				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_XMBX_RESP)
					{
						bWait = PAAS_FALSE;
						bBLDataRcvd = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);
			}
			if(rv == SUCCESS)
			{
				iTimeOut = pstDispMsgDtls->iTimeOut;
				/* Wait for the User resp */
				curTime = svcGetSysMillisec();
				endTime = curTime + (iTimeOut * 1000L);

				/* Wait for the user button press or wait for timeout */
				while(endTime > curTime || iTimeOut == 0)
				{
					if(bBLDataRcvd == PAAS_TRUE)
					{
						acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
						if(stBLData.uiRespType == UI_XMBX_RESP)
						{
							bBLDataRcvd = PAAS_FALSE;
							bEnterPressed = PAAS_TRUE;
						}
						releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
					}
					if(getPOSInitiatedState())
					{
						rv = retValForPosIniState();
						break;
					}
					if(bEnterPressed == PAAS_TRUE)
					{
						strcpy(pstDispMsgDtls->szTimeOutResp, "SUCCESS");
						break;
					}

					svcWait(15);
					curTime = svcGetSysMillisec();
				}
			}
			if(bEnterPressed == PAAS_FALSE)
			{
				debug_sprintf(szDbgMsg, "%s: TimeOUT Occurred", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				strcpy(pstDispMsgDtls->szTimeOutResp, "TIMEOUT");
				//  Hide Message Box
				hideMessageBox();
			}
		}
		//MukeshS3: 15-July-16: If its just a message box on top of any screen, so we can always go back to the last app state as we are hiding the message box from here
		setAppState(iLastAppState);
	}
	debug_sprintf(szDbgMsg, "%s: ---Returning[%d]----", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: hideMessageBox()
 *
 * Description	: This API would be used to hide a displayed message box
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int hideMessageBox()
{
	int			rv				= SUCCESS;
	UIREQ_STYPE	stUIReq;
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* REQ: <STX>XMBH<ETX>{LRC} */
	initUIReqMsg(&stUIReq, XMBH_REQ);
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to send XMBH", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning[%d]----", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: restartSCA
 *
 * Description	: Restart the SCA App
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int restartSCA()
{
	int 			rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[256]	= "";
	char			szErrMsg[256]		= "";
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

#if 0
	initUIReqMsg(&stUIReq, RSTART_REQ);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
#endif

	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "Parameter Installation Successful. Application is Going to Restart...");
		addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
	}
	strcpy(szErrMsg, "Parameter Installation Successful. Application is Going to Restart...");
	syslog(LOG_NOTICE|LOG_INFO, szErrMsg); //Logging to syslog

	rv = Secins_start_user_apps();
	if(rv != SUCCESS)
	{
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Failed to Restart the Application After Parameter Installation");
			addAppEventLog(SCA, PAAS_ERROR, DISPLAY_SCREEN, szAppLogData, NULL);
		}
		strcpy(szErrMsg, "Failed to Restart the Application After Parameter Installation");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: disableCardReaders()
 *
 * Description	: This API would be used to disable all the card readers on the screen
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void disableCardReaders()
{
	int			iPrevWhichForm 	= -1;

#ifdef DEBUG
	char szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(getCardReadEnabled() == PAAS_TRUE)
	{
		iPrevWhichForm = whichForm;

		whichForm = DUMMY;

		/* Disabling the card swipe on the UI agent. */
		cancelRequest();

		setCardReadEnabled(PAAS_FALSE);

		debug_sprintf(szDbgMsg, "%s: Disabled the card Readers ", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		whichForm = iPrevWhichForm;
	}


	debug_sprintf(szDbgMsg, "%s: ---Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: sendD41ProvisionPass()
 *
 * Description	: This API would be used to prompt send Provision Pass to Phone
 *
 * Input Params	: Merchant Index, Customer Data
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int sendD41ProvisionPass(char* szMerchIndex, char * pszCustData)
{
	int				rv					= SUCCESS;
	int				iDisableFormFlag	= 0;
	int				ctrlIds[2];
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	char			szTitle[100]		= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = SWIPE_T_FRM;

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	memset(szTitle, 0x00, sizeof(szTitle));
	fetchScrTitle(szTitle, PROVISION_PASS_TITLE);

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_SWIPE_LBL_8;
	ctrlIds[1] 	= PAAS_SWIPE_LBL_9;

	/*Setting the screen title to show labels*/
	setScreenTitle(szTitle, NULL, ctrlIds, 2, giLabelLen, stUIReq.pszBuf);

	bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	/* Send the message to the UI agent */
	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		/* Wait for the UI Agent resp */
		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
					bWait = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			svcWait(5);
		}
		bWait = PAAS_TRUE;

		/*Sending D41 only currently need to add cust data also*/
		rv = sendD41cmdToEMVAgent(PAAS_TRUE, szMerchIndex, pszCustData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to send XPI D41 cmd to enable MSR",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			return ERR_DEVICE_APP;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Provision Pass Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}

		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						if(pstXevt->uiCtrlID == PAAS_SWIPE_FK_1)
						{
							debug_sprintf(szDbgMsg, "%s: CANCEL", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "User Pressed Cancel Button on Provision Pass Screen");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}

							rv = UI_CANCEL_PRESSED;
							bWait = PAAS_FALSE;
						}

						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}
				}
				else if(stBLData.uiRespType == UI_CARD_RESP)
				{
					if(stBLData.iStatus == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Received the VAS Data", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else if(stBLData.iStatus == UI_CANCEL_PRESSED)
					{
						debug_sprintf(szDbgMsg, "%s: UI Cancel Pressed",
								__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = UI_CANCEL_PRESSED;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: No VAS Data present in phone", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_NO_MATCHING_PROV_PASS;
					}

					bWait = PAAS_FALSE;
					bBLDataRcvd = PAAS_FALSE;
				}
				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			svcWait(10);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}
	}
	debug_sprintf(szDbgMsg,"%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: scaPutEnvFile()
 *
 * Description	: This API is a wrapper for the putEnvFile so that SCA will not do
 * 					sigjmp when execution in progress
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int scaPutEnvFile(char* pszSection, char* pszParam, char* pszParamValue)
{
	int				rv = -1;

//#ifdef DEBUG
//	char szDbgMsg[256]			= "";
//#endif
//
//	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	giEnvCommInProgress = 1;

	rv = putEnvFile(pszSection, pszParam, pszParamValue);

	giEnvCommInProgress = 0;

//	debug_sprintf(szDbgMsg, "%s: ---Returning", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: scaPutEnvFile()
 *
 * Description	: This API is a wrapper for the putEnvFile so that SCA will not do
 * 					sigjmp when execution in progress
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int scaGetEnvFile(char* pszSection, char* pszParam, char* pszParamValue, int iLen)
{
	int				rv = -1;

//#ifdef DEBUG
//	char szDbgMsg[256]			= "";
//#endif
//
//	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	giEnvCommInProgress = 1;

	rv = getEnvFile(pszSection, pszParam, pszParamValue, iLen);

	giEnvCommInProgress = 0;

//	debug_sprintf(szDbgMsg, "%s: ---Returning", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showDCCOptScreen
 *
 * Description	:
 *
 * Input Params	: pszConfirmAmt -> Amount to be confirmed.
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showDCCOptScreen(DCCDTLS_PTYPE pstDccDtls, char* szTranAmt)
{
	int				rv					= SUCCESS;
	int 			ctrlIds[4];
	int				iAppLogEnabled		= isAppLogEnabled();
	int				iDisableFormFlag	= 1;
	char			szAmnt[20]			= "";
	char			szTmp[100]			= "";
	char			szTitle[100]		= "";
	char			szAppLogData[256]	= "";
	char			szFgnCurrImg[10]	= "";
	char			szFgnCurrImgPath[20]= "./flash/";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	XEVT_PTYPE		pstXevt				= NULL;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	whichForm = DCC_OPT_FRM;

	setAppState(CAPTURING_PAYMENT);

	/*
	 * Form to be automatically locked after 1 event has been generated
	 */
	iDisableFormFlag = 1;

	/* Initialize the screen elements */
	initUIReqMsg(&stUIReq, BATCH_REQ);
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	/* ---------------- Set the labels ----------------- */

	/* Set the Title */
	memset(szTitle, 0x00, sizeof(szTitle));
	fetchScrTitle(szTitle, DCC_TITLE);
	/* Set the amount label */

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= SCA_DCC_TITLE1;
	ctrlIds[1] 	= SCA_DCC_TITLE2;

	setScreenTitle(szTitle, NULL, ctrlIds, 2, giLabelLen, stUIReq.pszBuf);

	while(1)
	{
		if(strlen(pstDccDtls->szFgnCurCode))
		{
			/*Setting the Images on the screen based on the currency code*/
			sprintf(szFgnCurrImg, "%s.png", pstDccDtls->szFgnCurCode);
			strcat(szFgnCurrImgPath, szFgnCurrImg);
			if (doesFileExist(szFgnCurrImgPath) == SUCCESS)
			{
			//	setStringValueWrapper(SCA_DCC_IMG_1, PROP_STR_CAPTION, "840.png", stUIReq.pszBuf);
				setStringValueWrapper(SCA_DCC_FGN_FLAG, PROP_STR_IMAGE_FILE_NAME, szFgnCurrImg, stUIReq.pszBuf);
			}
			else
			{
				sprintf(szAppLogData, "Foreign Currency Image %s Not Available. Not Showing DCC Options", szFgnCurrImgPath);
				addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);

				APP_TRACE(szAppLogData);
				rv = DCC_NOT_ELIGIBLE;
				break;
			}
		}
		else
		{
			strcpy(szAppLogData, "Currency Code Not Received From Host. Not Showing DCC Options");
			addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);

			APP_TRACE(szAppLogData);
			rv = DCC_NOT_ELIGIBLE;
			break;
		}

		memset(szTmp, 0x00, sizeof(szTmp));
		sprintf(szTmp, "USD %s", szTranAmt);
		setStringValueWrapper(SCA_DCC_LBL_LOCAL_CURRENCY, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);

		if(strlen(pstDccDtls->szFgnAmount))
		{
			memset(szTmp, 0x00, sizeof(szTmp));
			if(pstDccDtls->iMinorUnits > 0)
			{
				getFGNAmntWithDecimals(pstDccDtls->szFgnAmount, pstDccDtls->iMinorUnits, szAmnt);
				sprintf(szTmp, "%s %s", pstDccDtls->szAlphaCurrCode, szAmnt);
			}
			else
			{
				sprintf(szTmp, "%s %s", pstDccDtls->szAlphaCurrCode, pstDccDtls->szFgnAmount);
			}
			setStringValueWrapper(SCA_DCC_LBL_FGN_CURRENCY, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
		}
		else
		{
			strcpy(szAppLogData, "Foreign Amount Not Received From Host. Not Showing DCC Options");
			addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);

			APP_TRACE(szAppLogData);
			rv = DCC_NOT_ELIGIBLE;
			break;
		}

		if(strlen(pstDccDtls->szExchngRate))
		{
			memset(szTmp, 0x00, sizeof(szTmp));
			sprintf(szTmp, "1 USD = %.4lf %s", atof(pstDccDtls->szExchngRate), pstDccDtls->szAlphaCurrCode);
			setStringValueWrapper(SCA_DCC_LBL_EXCHANGE_RATE, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
		}
		else
		{
			strcpy(szAppLogData, "Exchange Rate Not Received From Host. Not Showing DCC Options");
			addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);

			APP_TRACE(szAppLogData);
			rv = DCC_NOT_ELIGIBLE;
			break;
		}

		if(strlen(pstDccDtls->szMarginRatePercentage))
		{
			memset(szTmp, 0x00, sizeof(szTmp));
			sprintf(szTmp, "%.2lf%% margin", atof(pstDccDtls->szMarginRatePercentage));
			setStringValueWrapper(SCA_DCC_LBL_MARGIN, PROP_STR_CAPTION, szTmp, stUIReq.pszBuf);
		}
		else
		{
			strcpy(szAppLogData, "Margin Rate Not Received From Host. Not Showing DCC Options");
			addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);

			APP_TRACE(szAppLogData);
			rv = DCC_NOT_ELIGIBLE;
			break;
		}

		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

		/* Send batch message to UI agent */
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			memset(szAppLogData, 0x00, sizeof(szAppLogData));
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Showing DCC Flag Screen");
				addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
			}
			/* Wait for any event for the form */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE;
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_XEVT_RESP)
					{
						pstXevt = &(stBLData.stRespDtls.stXEvtInfo);

						if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
						{
							switch(pstXevt->uiCtrlID)
							{
							case SCA_DCC_LOCAL_FLAG:
								debug_sprintf(szDbgMsg, "%s: User Opted For Transaction in Local Currency", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Opted For Transaction in Local Currency");
									addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
								}

								rv = DCC_ELIGIBLE_USER_NOT_OPTED;
								break;

							case SCA_DCC_FGN_FLAG:
								debug_sprintf(szDbgMsg, "%s: User Opted For DCC", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Opted For DCC");
									addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
								}

								rv = DCC_ELIGIBLE_USER_OPTED;
								break;
							case SCA_DCC_CANCEL_KEY:
								debug_sprintf(szDbgMsg, "%s: CANCEL", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Pressed Cancel Button DCC Screen");
									addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
								}

								rv = ERR_USR_CANCELED;
								bWait = PAAS_FALSE;
								break;

							default:
								debug_sprintf(szDbgMsg, "%s: Unknown",__FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = ERR_DEVICE_APP;
								break;
							}

							bBLDataRcvd = PAAS_FALSE;
							bWait = PAAS_FALSE;
						}
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				if(bWait == PAAS_FALSE)
				{
					break;
				}
				svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCardDtlsForDevCmd()
 *
 * Description	: This Function is used to check whether the Card is PCI or NON-PCI card.
 *
 * Input Params	: GetCardData Structure,
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

static int getCardDtlsForDevCmd(GET_CARDDATA_PTYPE pstGetCrdDtls)
{
       int          rv              		= SUCCESS;
       char         szBIN[6+1]              = "";
       char         szClearPAN[100+1]        = "";
       char         szMaskedPAN[50+1]       = "";
       char         szClearTrack1[150+1]    = "";
       char         szClearTrack2[150+1]    = "";
       char         szClearTrack3[150+1]    = "";
       char			*cCurPtr				= NULL;
       char			*cNxtPtr				= NULL;
       ENC_TYPE		iEncType				= getEncryptionType();

#ifdef DEBUG
       char   szDbgMsg[256] = "";
#endif

       debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
       APP_TRACE(szDbgMsg);
       /* Intializations */
       memset(szBIN, 0x00, sizeof(szBIN));
       memset(szClearPAN, 0x00, sizeof(szClearPAN));
       memset(szMaskedPAN, 0x00, sizeof(szMaskedPAN));
       memset(szClearTrack1, 0x00, sizeof(szClearTrack1));
       memset(szClearTrack2, 0x00, sizeof(szClearTrack2));
       memset(szClearTrack3, 0x00, sizeof(szClearTrack3));

       while(1)
       {
    	   strncpy(szBIN, pstGetCrdDtls->szPAN, 6);

    	   if(checkBinInclusion(szBIN))
    	   {
    		   debug_sprintf(szDbgMsg, "%s: Card is Included in PCI Card Range", __FUNCTION__);
    		   APP_TRACE(szDbgMsg);
    		   //Masking the PAN data
    		   getMaskedPAN(pstGetCrdDtls->szPAN, szMaskedPAN);
    	   }
    	   else
    	   {
    		   debug_sprintf(szDbgMsg, "%s: Card is NOT Included in PCI Card Range", __FUNCTION__);
    		   APP_TRACE(szDbgMsg);
    		   if(iEncType == VSP_ENC)
    		   {
    			   // copy the encrypted PAN first, VCL needs it when clear PAN is request for manual entry transaction.
    			   strcpy(szClearPAN, pstGetCrdDtls->szPAN);
    			   /* Getting the clear PAN data from VCL */
        		   memset(pstGetCrdDtls->szTrack1Data, 0x00 ,sizeof(pstGetCrdDtls->szTrack1Data));
        		   memset(pstGetCrdDtls->szTrack2Data, 0x00 ,sizeof(pstGetCrdDtls->szTrack2Data));
        		   memset(pstGetCrdDtls->szTrack3Data, 0x00 ,sizeof(pstGetCrdDtls->szTrack3Data));
    			   rv = getDataFromVCL(szClearPAN, szClearTrack1, szClearTrack2, szClearTrack3, PAAS_FALSE);
    			   if(rv != SUCCESS)
    			   {
    				   debug_sprintf(szDbgMsg, "%s: Failed to get data from VCL", __FUNCTION__);
    				   APP_TRACE(szDbgMsg);
    				   break;;
    			   }
    		   }
    		   else if(iEncType == VSD_ENC)
    		   {
    			   memset(pstGetCrdDtls->szTrack1Data, 0x00 ,sizeof(pstGetCrdDtls->szTrack1Data));
    			   memset(pstGetCrdDtls->szTrack2Data, 0x00 ,sizeof(pstGetCrdDtls->szTrack2Data));
    			   memset(pstGetCrdDtls->szTrack3Data, 0x00 ,sizeof(pstGetCrdDtls->szTrack3Data));
    			   memcpy(pstGetCrdDtls->szTrack1Data, pstGetCrdDtls->szClrTrk1 ,sizeof(pstGetCrdDtls->szClrTrk1));
    			   memcpy(pstGetCrdDtls->szTrack2Data, pstGetCrdDtls->szClrTrk2 ,sizeof(pstGetCrdDtls->szClrTrk2));
    			   if((cNxtPtr = strchr(pstGetCrdDtls->szTrack2Data, '=')) != NULL)
    			   {
    				   cCurPtr = pstGetCrdDtls->szTrack2Data;
    				   strncpy(szClearPAN, cCurPtr, cNxtPtr - cCurPtr);
    			   }
    			   else if((cNxtPtr = strchr(pstGetCrdDtls->szTrack1Data, '^')) != NULL)
    			   {
    				   cCurPtr = pstGetCrdDtls->szTrack1Data + 1;
    				   strncpy(szClearPAN, cCurPtr, cNxtPtr - cCurPtr);
    			   }
    		   }
    		   else
    		   {
    			   // will send PAN data which was parsed in S20/C30 response. it could be randomized or clear PAN according to BIN range configuration
    			   strcpy(szClearPAN, pstGetCrdDtls->szPAN);
    		   }
    	   }

    	   memset(pstGetCrdDtls->szPAN , 0x00, sizeof(pstGetCrdDtls->szPAN));

    	   if(strlen(szMaskedPAN))           // copy masked PAN if available
    	   {
    		   strcpy(pstGetCrdDtls->szPAN,szMaskedPAN);
    		   memset(pstGetCrdDtls->szTrack1Data, 0x00 ,sizeof(pstGetCrdDtls->szTrack1Data));
    		   memset(pstGetCrdDtls->szTrack2Data, 0x00 ,sizeof(pstGetCrdDtls->szTrack2Data));
    		   memset(pstGetCrdDtls->szTrack3Data, 0x00 ,sizeof(pstGetCrdDtls->szTrack3Data));
    	   }
    	   else if(strlen(szClearPAN))
    	   {
    		   strcpy(pstGetCrdDtls->szPAN,szClearPAN);
    	   }
    	   if(strlen(szClearTrack1))  // copy track1 data if available
    	   {
    		   strcpy(pstGetCrdDtls->szTrack1Data,szClearTrack1);
    	   }
    	   if(strlen(szClearTrack2))  // copy track2 data if available
    	   {
    		   strcpy(pstGetCrdDtls->szTrack2Data,szClearTrack2);
    	   }
    	   if(strlen(szClearTrack3))  // copy track3 data if available
    	   {
    		   strcpy(pstGetCrdDtls->szTrack3Data,szClearTrack3);
    	   }
    	  break;
       }      // break from while loop
       debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
       APP_TRACE(szDbgMsg);
       return rv;
}

/*
 * ============================================================================
 * Function Name: showMessageBox
 *
 * Description	: This Function is used to display text information on the screen in a message box
 *
 * Input Params	: msg box title, msg box text
 *
 * Output Params: NONE
 * ============================================================================
 */
void showMessageBox(char *title, char *text)
{
	int 		rv 				= SUCCESS;
	int			iAppLogEnabled	= isAppLogEnabled();
	char		szAppLogData[256] = "";
	char		szBuf[512]		= "";
	PAAS_BOOL	bWait			= PAAS_TRUE;
	UIREQ_STYPE	stUIReq;
#ifdef DEBUG
       char   szDbgMsg[256] = "";
#endif

	/*	Message Box
	 *  REQ: <STX>XMBX<FS>Title<FS>Text<FS>Icon<FS>Buttons<FS>Blocked<FS>{LRC}
	 */
	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, text);
		addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
	}

	initUIReqMsg(&stUIReq, XMBX_REQ);

	memset(szBuf, 0x00, sizeof(szBuf));
	sprintf(szBuf, "%s%c%s%c%d%c%d%c%d%c", title , FS, text , FS, 1 /*Error Icon*/, FS, 0 /*No buttons*/, FS, 0 /*Non Blocked*/, FS);
	strcat(stUIReq.pszBuf, szBuf);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		/* Wait for the UI Agent resp */
		bBLDataRcvd = PAAS_FALSE;

		while(bWait == PAAS_TRUE)
		{
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_XMBX_RESP)
				{
					bWait = PAAS_FALSE;
					bBLDataRcvd = PAAS_FALSE;
				}

				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}
			if(bWait == PAAS_FALSE)
			{
				break;
			}
			svcWait(15);
		}
	}
	return ;
}

/*
 * ============================================================================
 * Function Name: showQRCodeAtFullScrn
 *
 * Description	: This API would be used to display QR Code on full screen with appropriate button label & title text.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showQRCodeAtFullScrn(DISPQRCODE_DTLS_PTYPE	pstDispQRCodeDtls, char *szStatMsg)
{
	int			rv						= SUCCESS;
	int			locForm					= 0;
	int			iDisableFormFlag		= 0;
	char		szAppLogData[512]		= "";
	char		szTitle[150+1]			= "";
	PAAS_BOOL	bWait					= PAAS_TRUE;
	UIREQ_STYPE	stUIReq;
#ifdef DEBUG
	char szDbgMsg[512]		= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	while(1)
	{
		/* Need to validate the input parameter */
		if(pstDispQRCodeDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		disableCardReaders();
		setAppState(DISPLAYING_QRCODE_SCREEN);
		locForm = QRCODE_DISP_FRM;
		whichForm = locForm;

		// Initialize the QR CODE Screen FORM
		memset(&stUIReq, 0x00, sizeof(UIREQ_STYPE));
		initUIReqMsg(&stUIReq, BATCH_REQ);
		initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);

		// Check for screen title sent from POS
		if(strlen(pstDispQRCodeDtls->szDispText))
		{
			strcpy(szTitle, pstDispQRCodeDtls->szDispText);
		}
		else
		{
			/* Get the main title of the display screen */
			fetchScrTitle(szTitle, QRCODE_DEFAULT_TITLE);
		}

		/*Setting the screen title in text box*/
		addTextboxTextWrapper(PAAS_QRCODE_SCREEN_TXT, szTitle, stUIReq.pszBuf);

		// Check whether DONE button with label has to be shown on screen or not; If it's not sent from POS, we need not show any button on screen
		if(strlen(pstDispQRCodeDtls->szBtnLabel))
		{
			setBoolValueWrapper(PAAS_QRCODE_SCREEN_BTN, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
			/* Set the label for DONE button */
			setStringValueWrapper(PAAS_QRCODE_SCREEN_BTNLBL, PROP_STR_CAPTION, pstDispQRCodeDtls->szBtnLabel, stUIReq.pszBuf);
		}

		setStringValueWrapper(PAAS_QRCODE_SCREEN_IMG, PROP_STR_IMAGE_FILE_NAME, QRCODE_IMG_FILE_NAME, stUIReq.pszBuf);

		setBoolValueWrapper(PAAS_QRCODE_SCREEN_IMG, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		// show the form once here before genrating the QR image
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		bWait = PAAS_TRUE;
		/* Send batch message to UI agent */
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE

				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(15);
			}
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: ---Returning[%d]----", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: updateQRCodeAtLIScrn
 *
 * Description	: This API would be used to hide all the enabled items on left panel of LI screen
 *  	  	  	  	  & show QR Code at LI screen on the left panel.
 *  	  	  	  	  This function is just for updating the left panel of screen, it will not initialize the screen.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateQRCodeAtLIScrn(DISPQRCODE_DTLS_PTYPE	pstDispQRCodeDtls, char *szStatMsg)
{
	int			rv									= SUCCESS;
	int			iCnt								= 0;
	int			iBtnCnt								= 0;
	int			iLblCnt								= 0;
	int			consOptionLst[MAX_CONSUMER_OPTS][2];
	char		szAppLogData[512]					= "";
	char		szTitle[150+1]						= "";
	PAAS_BOOL	bWait								= PAAS_TRUE;
	PAAS_BOOL	bConsOptEnabled 					= PAAS_FALSE;
	UIREQ_STYPE	stUIReq;
#ifdef DEBUG
	char szDbgMsg[512]		= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	while(1)
	{
		/* Need to validate the input parameter */
		if(pstDispQRCodeDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		setAppState(IN_LINEITEM_QRCODE);
		memset(&stUIReq, 0x00, sizeof(UIREQ_STYPE));
		initUIReqMsg(&stUIReq, BATCH_REQ);

		// First we need to hide all the objects from the left panel of LI screen
		// Hide all items from PAAS_LINEITEMSCREEN
		setBoolValueWrapper(PAAS_LINEITEMSCREEN1_LBL_6, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);	// "Pre Swipe Card" Label

		/* Get the enabled consumer options */
		getConsOptions(consOptionLst);

		for(iCnt = 0, iBtnCnt = PAAS_LINEITEMSCREEN1_BTN_1, iLblCnt = PAAS_LINEITEMSCREEN1_LBL_7; iCnt < MAX_CONSUMER_OPTS; iCnt++, iBtnCnt++, iLblCnt++)
		{
			if(consOptionLst[iCnt][0] != -1)
			{
				setBoolValueWrapper(iBtnCnt, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
				setBoolValueWrapper(iLblCnt, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
				bConsOptEnabled = PAAS_TRUE;
			}
		}

		if(bConsOptEnabled == PAAS_FALSE)
		{
			//setBoolValueWrapper(PAAS_LINEITEMSCREEN_IMG_1, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);	// Praveen_P1: Changed the Image control to Animation
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_ANI_1, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);	// Advertisement Image
		}

		// Paypal Button
		if(isPaypalTenderEnabled())
		{
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_PAYPAL_BTN, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
		}

		if(isWalletEnabled())
		{
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_IMG, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_WALLET_LBL, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
		}
		setBoolValueWrapper(PAAS_LINEITEMSCREEN1_TXT_1, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);	// text box


		// Check for screen title sent from POS
		if(strlen(pstDispQRCodeDtls->szDispText))
		{
			strcpy(szTitle, pstDispQRCodeDtls->szDispText);
		}
		else
		{
			/* Get the main title of the display screen */
			fetchScrTitle(szTitle, QRCODE_DEFAULT_TITLE);
		}


		/*Setting the screen title in text box*/
		replaceTextboxTextWrapper(PAAS_LINEITEMSCREEN_QRCODE_TXT, szTitle, stUIReq.pszBuf);

		// Check if more than 72 characters are present in SCI DISPLAY_TEXT field, than we need to enable scroll bar on TEXT BOX(by default its hidden)
		if(strlen(szTitle) > 72)
		{
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_TXT, PROP_BOOL_LISTBOX_NO_SCROLL, 0, stUIReq.pszBuf);
		}
		else
		{
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_TXT, PROP_BOOL_LISTBOX_NO_SCROLL, 1, stUIReq.pszBuf);
		}
		// Check whether DONE button with label has to be shown on screen or not; If it's not sent from POS, we need not show any button on screen
		if(strlen(pstDispQRCodeDtls->szBtnLabel))
		{
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_BTN, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
			/* Set the label for DONE button */
			setStringValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_BTN, PROP_STR_CAPTION, pstDispQRCodeDtls->szBtnLabel, stUIReq.pszBuf);
		}
		else
		{
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_BTN, PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
		}

		setStringValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_IMG, PROP_STR_IMAGE_FILE_NAME, QRCODE_IMG_FILE_NAME, stUIReq.pszBuf);

		setBoolValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_IMG, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);

		// show the form once here before genrating the QR image
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		bWait = PAAS_TRUE;
		/* Send batch message to UI agent */
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE

				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(15);
			}
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: ---Returning[%d]----", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}


/*
 * ============================================================================
 * Function Name: showQRCodeAtLIScrn
 *
 * Description	: This API would be used to initialize the LI screen & show only the QR code related details on the left panel of the screen.
 * 					The details required for QR image data, display text etc will be picked up from Session
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int showQRCodeAtLIScrn()
{
	int				rv								 = SUCCESS;
	int				locForm							 = 0;
	int				iDisableFormFlag				 = 0;
	//int				iAppLogEnabled			  		 = isAppLogEnabled();
	char			szTaxAmount[11]					 = "";
	char			szTotalAmount[11]				 = "";
	char			szSubTotAmount[11]				 = "";
	char			szTmp[100]						 = "";
	char			szAppLogData[256]				 = "";
	char			szMerchIndex[10+1]				 = "";
	char			szTitle[150+1]					 = "";
	PAAS_BOOL		bWait							 = PAAS_TRUE;
	PAAS_BOOL		bCaptureCardDtls				 = PAAS_FALSE;
	PAAS_BOOL		bCardReadEnabled				 = PAAS_FALSE;
	BTNLBL_STYPE	stBtnLbl;
	UIREQ_STYPE		stUIReq;
	DISPQRCODE_DTLS_STYPE stDispQRCodeDtls;

#ifdef DEBUG
	char szDbgMsg[512]		= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	while(1)
	{
		// Get the QR Code details from Session
		memset(&stDispQRCodeDtls, 0x00, sizeof(DISPQRCODE_DTLS_STYPE));
		rv = getQRCodeDtlsFromSession(&stDispQRCodeDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get QR Code details from session",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		// If we are able to genrate the QR image successfully, than we can change the App state & FORM to enable the QR image on screen
		// Set the app state
		setAppState(IN_LINEITEM_QRCODE);

		bCardReadEnabled = getCardReadEnabled();

		if(isConsumerOptsEnabled())
		{
			locForm = LI_DISP_OPT_FRM;
		}
		else
		{
			locForm = LI_DISP_FRM;
		}
		whichForm = locForm;

		// Initialize the LI form & enable the QR image on this
		memset(&stUIReq, 0x00, sizeof(UIREQ_STYPE));
		initUIReqMsg(&stUIReq, BATCH_REQ);
		initFormWrapper(frmName[locForm], iDisableFormFlag, stUIReq.pszBuf);

		/* Restoring the List BOX state*/
		setListBoxRestoreState(PAAS_LINEITEMSCREEN_LST_1, stUIReq.pszBuf);

		/* Set the Title */
		fetchScrTitle(szTmp, LINE_ITEMS_TITLE);
		setStringValueWrapper(PAAS_LINEITEMSCREEN_LBL_1, PROP_STR_CAPTION, szTmp,
						stUIReq.pszBuf);

		/* Set the TAX label */
		fetchBtnLabel(&stBtnLbl, BTN_LBLS_03);
		setStringValueWrapper(PAAS_LINEITEMSCREEN_LBL_2, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblOne, stUIReq.pszBuf);
		/* Set the total amount label */
		setStringValueWrapper(PAAS_LINEITEMSCREEN_LBL_3, PROP_STR_CAPTION,
								(char *)stBtnLbl.szLblTwo, stUIReq.pszBuf);

		/*Setting the tax amount and total amount*/
		getLIRunningAmounts(szTaxAmount, szTotalAmount, szSubTotAmount);

		if ( (strlen(szTaxAmount) > 0) || (strlen(szTotalAmount) > 0) || (strlen(szSubTotAmount) > 0))
		{
			updateTaxAndTotalOnLIScreen(szTaxAmount, szTotalAmount, szSubTotAmount, 0, stUIReq.pszBuf);
		}

		// Check for screen title(For QR Code Image) sent from POS
		if(strlen(stDispQRCodeDtls.szDispText))
		{
			strcpy(szTitle, stDispQRCodeDtls.szDispText);
		}
		else
		{
			/* Get the main title of the display screen */
			fetchScrTitle(szTitle, QRCODE_DEFAULT_TITLE);
		}

		/*Setting the screen title in text box*/
		addTextboxTextWrapper(PAAS_LINEITEMSCREEN_QRCODE_TXT, szTitle, stUIReq.pszBuf);

		// Check if more than 72 characters are present in SCI DISPLAY_TEXT field, than we need to enable scroll bar on TEXT BOX(by default its hidden)
		if(strlen(szTitle) > 72)
		{
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_TXT, PROP_BOOL_LISTBOX_NO_SCROLL, 0, stUIReq.pszBuf);
		}
		// Check whether DONE button with label has to be shown on screen or not; If it's not sent from POS, we need not show any button on screen
		if(strlen(stDispQRCodeDtls.szBtnLabel))
		{
			setBoolValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_BTN, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
			/* Set the label for DONE button */
			setStringValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_BTN, PROP_STR_CAPTION, stDispQRCodeDtls.szBtnLabel, stUIReq.pszBuf);
		}

		setStringValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_IMG, PROP_STR_IMAGE_FILE_NAME, QRCODE_IMG_FILE_NAME, stUIReq.pszBuf);

		setBoolValueWrapper(PAAS_LINEITEMSCREEN_QRCODE_IMG, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
		// show the form once here before genrating the QR image
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		if(isSwipeAheadEnabled() == PAAS_TRUE)
		{
			if(isPreSwipeDone() == PAAS_FALSE)
			{
				debug_sprintf(szDbgMsg, "%s: Swipe Ahead is enabled, adding card read command to the request", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				bCaptureCardDtls = PAAS_TRUE;
			}
			else
			{
				/*if only card data is captured then we need to send D41 to get VAS data*/
				/*06-04-2016 : Checking Whether D41 is Active */
				if(PAAS_TRUE == isWalletEnabled() && isVASDataCaptured() == PAAS_FALSE && getD41Active() == PAAS_FALSE)
				{
					debug_sprintf(szDbgMsg, "%s: Sending D41 since Vas wasnt captured,wallet is enabled,preswipe done and D41 is not active", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					memset(szMerchIndex, 0x00, sizeof(szMerchIndex));
					getMerchantIndexIfAvaliable(szMerchIndex);
					sendD41cmdToEMVAgent(PAAS_FALSE, szMerchIndex, NULL);
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Swipe Ahead is NOT enabled", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		bWait = PAAS_TRUE;
		/* Send batch message to UI agent */
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE

				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_GENRL_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(15);
			}

			if(bCaptureCardDtls == PAAS_TRUE && (!bCardReadEnabled))
			{
				rv= captureCardDtlsForPreswipe();//Send Commands to XPI for card data capture
				bCaptureCardDtls = PAAS_FALSE;
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to send XPI cmds",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					return ERR_DEVICE_APP;
				}
			}
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: ---Returning[%d]----", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: generateQRCodeImage
 *
 * Description	: This API would be used to generate the QR code image for the passed string data
 *
 * Input Params	:	input qr code data, status msg
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int generateQRCodeImage(char *pszQRCodeData, char *szStatMsg)
{
	int			rv									= SUCCESS;
	int			iAppLogEnabled						= isAppLogEnabled();
	char		szAppLogData[512]					= "";
	char 		szFileName[50+1] 					= "/home/usr1/flash/";
	char		szRespMsg[100+1]						= "";
	PAAS_BOOL	bWait								= PAAS_TRUE;
	UIREQ_STYPE	stUIReq;
#ifdef DEBUG
	char szDbgMsg[512]		= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	while(1)
	{
		/* Need to validate the input parameter */
		if(pszQRCodeData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		// Before generating the new QR code image, we are removing the previously present image at flash location.
		strcat(szFileName, QRCODE_IMG_FILE_NAME);
		if(remove(szFileName) != SUCCESS) // T_raghavendranR1 CID 83372 (#1 of 1): Unchecked return value from library (CHECKED_RETURN)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to remove the File", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		memset(&stUIReq, 0x00, sizeof(UIREQ_STYPE));
		initUIReqMsg(&stUIReq, BATCH_REQ);

		/* format for XQRC command:		<STX>XQRC<FS>�QRCODE_DATA�<FS>X<FS>Y<FS>SIZE<FS>FILENAME=<STX>*/
		if(whichForm == LI_DISP_OPT_FRM || whichForm == LI_DISP_FRM)
		{
			if(getDevicePlatform() == MODEL_MX915)
			{
				addQRCodeInfoWrapper(pszQRCodeData, -1, -1, 120, stUIReq.pszBuf);
			}
			else
			{
				addQRCodeInfoWrapper(pszQRCodeData, -1, -1, 240, stUIReq.pszBuf);
			}
		}
		else
		{
			if(getDevicePlatform() == MODEL_MX915)
			{
				addQRCodeInfoWrapper(pszQRCodeData, -1, -1, 175, stUIReq.pszBuf);
			}
			else
			{
				addQRCodeInfoWrapper(pszQRCodeData, -1, -1, 340, stUIReq.pszBuf);
			}
		}

		/* Send batch message to UI agent */
		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
			break;
		}
		else
		{
			/* Wait for the UI Agent resp */
			while(bWait == PAAS_TRUE)
			{
				CHECK_POS_INITIATED_STATE

				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_XQRC_RESP)
					{
						bBLDataRcvd = PAAS_FALSE;
						bWait = PAAS_FALSE;
						rv = stBLData.iStatus;
						strcpy(szRespMsg, stBLData.stRespDtls.stQRCodeInfo.szRespText);
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}
				svcWait(15);
			}
		}
		//Mukesh: 28-7-16: PTMX-1529: Checking for qrcode.png file at ./flash location, whether it has been generated or not
		if(rv == SUCCESS)
		{
			if((rv = doesFileExist(szFileName)) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: XQRC Command received Success response but file [%s] is not present",__FUNCTION__, szFileName);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "XQRC Command Received Success Response But File [%s] is Not Present", szFileName);
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
				memset(szRespMsg, 0x00, sizeof(szRespMsg));
			}
		}

		if(rv != SUCCESS)
		{
			strcpy(szStatMsg, "Failed to Generate QR Code Image For The Given Data");
			if(strlen(szRespMsg))
			{
				strcat(szStatMsg, ": ");
				strcat(szStatMsg, szRespMsg);
			}
			debug_sprintf(szDbgMsg, "%s: Failed to generate QR Code Image [%s]",
					__FUNCTION__, szRespMsg);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, szStatMsg);
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, NULL);
			}
			rv = ERR_QRCODE_GENE_FAILED;
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: ---Returning[%d]----", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: capturePreambleOptionsFromUser
 *
 * Description	: This API would be used to display various preamble steps available
 *
 * Input Params	:	input qr code data, status msg
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int capturePreambleOptionsFromUser()
{
	int				rv								= SUCCESS;
	int				i, iCount						= 0;
	int				iDisableFormFlag				= 0;
	int				iAppLogEnabled					= isAppLogEnabled();
	int				iCtrlId							= 0;
	int 			ctrlIds[4]						= {0};
	int				iWaitTime						= 0;
	int				iEmvSetUpType					= 0;
	ullong			endTime							= 0L;
	ullong			curTime							= 0L;
	char			szEmvSetupType[10]				= "";
	char			szAppLogData[512]				= "";
	char			szStepName[50]					= "";
	char			szDisplayText[150]				= "";
	int				iEncType						= getEncryptionType();
	int				mandSteps[15]					= {0};
	int				stepsCtrlIds[]					= {CONFIGURE_NETWORK_CHK, DEVICE_REGISTRATION_CHK, VSP_REGISTRATION_CHK, INIT_PAYMENT_ENGINE_CHK, DUMMY_SALE_CHK};
	int				statusImgCtrlIds[]				= {CONFIGURE_NETWORK_STATUS_IMG, DEVICE_REGISTRATION_STATUS_IMG, VSP_REGISTRATION_STATUS_IMG, INIT_PAYMENT_ENGINE_STATUS_IMG, DUMMY_SALE_STATUS_IMG};
	char			szFileName[MAX_FILE_NAME_LEN]	= "";
	char			szTitle[100]					= "";
	PAAS_BOOL		bWait							= PAAS_TRUE;
	PAAS_BOOL		bCannotSkip						= PAAS_FALSE;
	PAAS_BOOL		bResetTimer						= PAAS_FALSE;
	XEVT_PTYPE		pstXevt					= NULL;
	UIREQ_STYPE		stUIReq;
	DISPMSG_STYPE	stDispMsgDtls;
	BTNLBL_STYPE	stBtnLbl;

#ifdef DEBUG
	char szDbgMsg[512]		= "";
#endif

	whichForm = PREAMBLE_STEPS;

	for( i = 0; i < 5; i++)
	{
		debug_sprintf(szDbgMsg, "%s: iCount [%d], statusImgCtrlIds[%d] [%d]",__FUNCTION__, i, i, statusImgCtrlIds[i]);
		APP_TRACE(szDbgMsg);
	}

	rv = getEnvFile("", "emvsetuptype", szEmvSetupType, 10); // T_RaghavendranR1 CID 84332 (#1 of 1): Unchecked return value (CHECKED_RETURN)
	if(rv > 0) //
	{
		iEmvSetUpType = atoi(szEmvSetupType);
	}

	memset(&stDispMsgDtls, 0x00, sizeof(DISPMSG_STYPE));
	stDispMsgDtls.iTimeOut = 3;

	getDisplayMsg(szDisplayText, MSG_PREAMBLE_NOT_SKIPPING);

	memset(&stUIReq, 0x00, sizeof(UIREQ_STYPE));
	initUIReqMsg(&stUIReq, BATCH_REQ);

	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	strcpy(szFileName, "indicator_red.png");
	getFileNamewithPlatformPrefix(szFileName);

	/* Set the Title */
	memset(szTitle, 0x00, sizeof(szTitle));
	fetchScrTitle(szTitle, PREAMBLE_STEPS_TITLE);
	/* Set the amount label */

	/*Setting the ctrlids for the form*/
	ctrlIds[0] 	= PAAS_PREAMBLE_STEPS_TITLE1;
	ctrlIds[1] 	= PAAS_PREAMBLE_STEPS_TITLE2;

	setScreenTitle(szTitle, NULL, ctrlIds, 2, giLabelLen, stUIReq.pszBuf);

	/* Setting the Status label*/
	memset(&stBtnLbl, 0x00, sizeof(BTNLBL_STYPE));
	fetchBtnLabel(&stBtnLbl, BTN_LBLS_32);

	/* Set the label for "Other" button */
	setStringValueWrapper(PAAS_PREAMBLE_STEPS_STATUS, PROP_STR_CAPTION, (char *) stBtnLbl.szLblTwo, stUIReq.pszBuf);

	/* Set the First Step Configure Network*/
	if(isNwCfgReqd())
	{
		setStringValueWrapper(statusImgCtrlIds[iCount], PROP_STR_IMAGE_FILE_NAME, szFileName, stUIReq.pszBuf);
		setBoolValueWrapper(stepsCtrlIds[iCount], PROP_BOOL_SELECTED, 1, stUIReq.pszBuf);
		mandSteps[stepsCtrlIds[iCount]] = PAAS_TRUE;
	}
	setStringValueWrapper(stepsCtrlIds[iCount], PROP_STR_CAPTION, "Configure Network", stUIReq.pszBuf);
	iCount++;

	if(getDevAdminRqdStatus())
	{
		setStringValueWrapper(statusImgCtrlIds[iCount], PROP_STR_IMAGE_FILE_NAME, szFileName, stUIReq.pszBuf);
		setBoolValueWrapper(stepsCtrlIds[iCount], PROP_BOOL_SELECTED, 1, stUIReq.pszBuf);
		mandSteps[stepsCtrlIds[iCount]] = PAAS_TRUE;
	}
	setStringValueWrapper(stepsCtrlIds[iCount], PROP_STR_CAPTION, "Device Registration", stUIReq.pszBuf);
	iCount++;

	if(iEncType == VSP_ENC)
	{
		if(isVSPActivated() == PAAS_FALSE)
		{
			//printf("Image Control Id %d\n", statusImgCtrlIds[iCount]);
			setStringValueWrapper(statusImgCtrlIds[iCount], PROP_STR_IMAGE_FILE_NAME, szFileName, stUIReq.pszBuf);
			setBoolValueWrapper(stepsCtrlIds[iCount], PROP_BOOL_SELECTED, 1, stUIReq.pszBuf);
			mandSteps[stepsCtrlIds[iCount]] = PAAS_TRUE;
		}
		/* Set the First Step Configure Network*/
		setStringValueWrapper(stepsCtrlIds[iCount], PROP_STR_CAPTION, "VSP Registration", stUIReq.pszBuf);
		iCount++;
	}
	else
	{
		setBoolValueWrapper(stepsCtrlIds[4], PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
		setBoolValueWrapper(statusImgCtrlIds[4], PROP_BOOL_VISIBLE, 0, stUIReq.pszBuf);
	}

	if(isDevEmvSetupRqd())
	{
		iEmvSetUpType = isDevEmvSetupRqd();
		sprintf(szEmvSetupType, "%d", iEmvSetUpType);
		putEnvFile("", "emvsetuptype", szEmvSetupType);

		setStringValueWrapper(statusImgCtrlIds[iCount], PROP_STR_IMAGE_FILE_NAME, szFileName, stUIReq.pszBuf);
		setBoolValueWrapper(stepsCtrlIds[iCount], PROP_BOOL_SELECTED, 1, stUIReq.pszBuf);
		mandSteps[stepsCtrlIds[iCount]] = PAAS_TRUE;
	}
	setStringValueWrapper(stepsCtrlIds[iCount], PROP_STR_CAPTION, "Payment Engine Initialization", stUIReq.pszBuf);
	iCount++;

	debug_sprintf(szDbgMsg, "%s: iCount [%d], statusImgCtrlIds[%d] [%d], stepsCtrlIds[%d] [%d]",__FUNCTION__, iCount, iCount, statusImgCtrlIds[iCount], iCount, stepsCtrlIds[iCount]);
	APP_TRACE(szDbgMsg);

	if(isPreambleSuccessful() == PAAS_FALSE)
	{
		setStringValueWrapper(statusImgCtrlIds[iCount], PROP_STR_IMAGE_FILE_NAME, szFileName, stUIReq.pszBuf);
		setBoolValueWrapper(stepsCtrlIds[iCount], PROP_BOOL_SELECTED, 1, stUIReq.pszBuf);
		mandSteps[stepsCtrlIds[iCount]] = PAAS_TRUE;
	}
	setStringValueWrapper(stepsCtrlIds[iCount], PROP_STR_CAPTION, "Dummy Sale", stUIReq.pszBuf);
	//iCount++;
	while(1)
	{
		/* Show the form */
		showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

		bCannotSkip = PAAS_FALSE;
		bWait 		= PAAS_TRUE;
		bBLDataRcvd = PAAS_FALSE; //Praveen_P1: setting it to make sure that we wait for the response that is been sent here

		rv = sendUIReqMsg(stUIReq.pszBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
		}
		else
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Showing Preamble Steps Screen");
				addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
			}
			iWaitTime = getPreambleSelectWaitTime();
			/* Wait for the User resp */
			curTime = svcGetSysMillisec();
			endTime = curTime + (iWaitTime * 1000L);

			while(bWait == PAAS_TRUE && endTime > curTime)
			{
				if(bBLDataRcvd == PAAS_TRUE)
				{
					acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

					if(stBLData.uiRespType == UI_XEVT_RESP)
					{
						pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
						if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
						{
							switch(pstXevt->uiCtrlID)
							{
							case PAAS_PREAMBLE_STEPS_ENTER:
								debug_sprintf(szDbgMsg, "%s: Enter Pressed", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Pressed Enter on Preamble Steps Screen");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}
								rv = SUCCESS;
								bWait = PAAS_FALSE;
								break;

							case CONFIGURE_NETWORK_CHK:
								debug_sprintf(szDbgMsg, "%s: Configure Network Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								bResetTimer = PAAS_TRUE;
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Selected Network Configuration");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								if(pstXevt->uiCtrlData == PAAS_FALSE)
								{
									if(mandSteps[CONFIGURE_NETWORK_CHK] == PAAS_TRUE)
									{
										strncpy(szStepName, "Configure Network", sizeof(szStepName) - 1);
										bCannotSkip = PAAS_TRUE;
										iCtrlId = pstXevt->uiCtrlID;
									}
									else
									{
										setNwCfgReqd(PAAS_FALSE);
									}
								}
								else
								{
									setNwCfgReqd(PAAS_TRUE);
								}
								break;
							case DEVICE_REGISTRATION_CHK:
								debug_sprintf(szDbgMsg, "%s: Device Registration Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								bResetTimer = PAAS_TRUE;
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Selected Device Registration");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								if(pstXevt->uiCtrlData == PAAS_FALSE)
								{
									if(mandSteps[DEVICE_REGISTRATION_CHK] == PAAS_TRUE)
									{
										strncpy(szStepName, "Device Registration", sizeof(szStepName) - 1);
										bCannotSkip = PAAS_TRUE;

										iCtrlId = pstXevt->uiCtrlID;
									}
									else
									{
										setDevAdminRqd(PAAS_FALSE);
									}
								}
								else
								{
									setDevAdminRqd(PAAS_TRUE);
								}
								break;
							case VSP_REGISTRATION_CHK:
								bResetTimer = PAAS_TRUE;
								if(iEncType == VSP_ENC)
								{
									debug_sprintf(szDbgMsg, "%s: VSP_REGISTRATION Selected", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									if(iAppLogEnabled == 1)
									{
										strcpy(szAppLogData, "User Selected VSP Registration");
										addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
									}

									if(pstXevt->uiCtrlData == PAAS_FALSE)
									{
										if(mandSteps[VSP_REGISTRATION_CHK] == PAAS_TRUE)
										{
											strncpy(szStepName, "VSP Registration", sizeof(szStepName) - 1);

											bCannotSkip = PAAS_TRUE;
											iCtrlId = pstXevt->uiCtrlID;
										}
										else
										{
											setVSPActivatedStatus(PAAS_TRUE);
										}
									}
									else
									{
										setVSPActivatedStatus(PAAS_FALSE);
									}
								}
								else
								{
									debug_sprintf(szDbgMsg, "%s: Init Payment Engine Selected", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									if(iAppLogEnabled == 1)
									{
										strcpy(szAppLogData, "User Selected Payment Engine Initialization");
										addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
									}

									if(pstXevt->uiCtrlData == PAAS_FALSE)
									{
										if(mandSteps[VSP_REGISTRATION_CHK] == PAAS_TRUE)
										{
											strncpy(szStepName, "Payment Engine Initialization", sizeof(szStepName) - 1);

											bCannotSkip = PAAS_TRUE;
											iCtrlId = pstXevt->uiCtrlID;
										}
										else
										{
											setDevEmvSetupRqd(EMV_SETUP_NOT_REQ);
										}
									}
									else
									{
										setDevEmvSetupRqd(EMV_SETUP_REQ_WITHOUT_HOST);
									}
								}
								break;
							case INIT_PAYMENT_ENGINE_CHK:
								bResetTimer = PAAS_TRUE;
								if(iEncType == VSP_ENC)
								{
									debug_sprintf(szDbgMsg, "%s: Init Payment Engine Selected", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									if(iAppLogEnabled == 1)
									{
										strcpy(szAppLogData, "User Selected Payment Engine Initialization");
										addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
									}

									if(pstXevt->uiCtrlData == PAAS_FALSE)
									{
										if(mandSteps[INIT_PAYMENT_ENGINE_CHK] == PAAS_TRUE)
										{
											strncpy(szStepName, "Payment Engine Initialization", sizeof(szStepName) - 1);

											bCannotSkip = PAAS_TRUE;
											iCtrlId = pstXevt->uiCtrlID;
										}
										else
										{
											setDevEmvSetupRqd(EMV_SETUP_NOT_REQ);
										}
									}
									else
									{
										setDevEmvSetupRqd(EMV_SETUP_REQ_WITHOUT_HOST);
									}
								}
								else
								{
									debug_sprintf(szDbgMsg, "%s: Dummy Sale Selected", __FUNCTION__);
									APP_TRACE(szDbgMsg);

									if(iAppLogEnabled == 1)
									{
										strcpy(szAppLogData, "User Selected Dummy Sale");
										addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
									}

									if(pstXevt->uiCtrlData == PAAS_FALSE)
									{
										if(mandSteps[INIT_PAYMENT_ENGINE_CHK] == PAAS_TRUE)
										{
											strncpy(szStepName, "Dummy Sale", sizeof(szStepName) - 1);

											bCannotSkip = PAAS_TRUE;
											iCtrlId = pstXevt->uiCtrlID;
										}
										else
										{
											setPreambleStatus(PAAS_TRUE);
										}
									}
									else
									{
										setPreambleStatus(PAAS_FALSE);
									}
								}
								break;

							case DUMMY_SALE_CHK:
								debug_sprintf(szDbgMsg, "%s: Dummy Sale Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								bResetTimer = PAAS_TRUE;
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "User Selected Dummy Sale");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								if(pstXevt->uiCtrlData == PAAS_FALSE)
								{
									if(mandSteps[DUMMY_SALE_CHK] == PAAS_TRUE)
									{
										strncpy(szStepName, "Dummy Sale", sizeof(szStepName) - 1);

										bCannotSkip = PAAS_TRUE;
										iCtrlId = pstXevt->uiCtrlID;
									}
									else
									{
										setPreambleStatus(PAAS_TRUE);
									}
								}
								else
								{
									setPreambleStatus(PAAS_FALSE);
								}
								break;
							default:
								debug_sprintf(szDbgMsg, "%s: Unknown Response Received",__FUNCTION__);
								APP_TRACE(szDbgMsg);
						//		bWait = PAAS_FALSE;
						//		rv = ERR_DEVICE_APP;
								break;
							}

							bBLDataRcvd = PAAS_FALSE;
							if(bResetTimer && ! bCannotSkip)
							{
								endTime = svcGetSysMillisec() + (iWaitTime * 1000L);	// MukeshS3: Reset the timer in case of any UI activity
								bResetTimer = PAAS_FALSE;
								debug_sprintf(szDbgMsg, "%s: Reseting the Preamble screen timer",__FUNCTION__);
								APP_TRACE(szDbgMsg);
							}
						}
						/*
						 * Praveen_P1: Not waiting only if the response received from the correct form
						 * 			   so moved the below line inside the above if condition
						 */
						//bWait = PAAS_FALSE;
					}

					releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
				}

				if(bWait == PAAS_FALSE || bCannotSkip)
				{
					break;
				}
				svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
				curTime = svcGetSysMillisec();
			}
			if(bCannotSkip)
			{
				snprintf(stDispMsgDtls.szDispTxt, sizeof(stDispMsgDtls.szDispTxt) - 1, szDisplayText, szStepName);
				displayMsgBox(&stDispMsgDtls);

				if(iAppLogEnabled == 1)
				{
					addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, stDispMsgDtls.szDispTxt, NULL);
				}

				memset(&stUIReq, 0x00, sizeof(UIREQ_STYPE));
				initUIReqMsg(&stUIReq, BATCH_REQ);

				setBoolValueWrapper(iCtrlId, PROP_BOOL_SELECTED, 1, stUIReq.pszBuf);

				continue;
			}
		}
		break;
	}

	if(curTime > endTime)
	{
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "Timer [%d] Elapsed on Preamble Steps Screen", iWaitTime);
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}

		debug_sprintf(szDbgMsg, "%s:Timer [%d] Elapsed on Preamble Steps Screen", __FUNCTION__, iWaitTime);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning[%d]----", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: captureCustCheckbox
 *
 * Description	: This Function would be used to prompt the users for taking customer Button input
 *
 *
 * Input Params	: Data(to be filled)
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int captureCustCheckbox(CUSTCHECKBOX_DTLS_PTYPE pstCustCheckBoxDtls)
{
	int				rv					= SUCCESS;
	int 			iCnt				= 0;
	int				jCnt				= 0;
	int				iDisableFormFlag	= 0;
	int				iTemp				= 0;
	int				iCheckBoxOptMask 	= 0;
	int				iChkBoxCtrlID		= PAAS_CUST_CHECKBOX_1;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[256]	= "";
	char			szTitle[201]		= "";
	PAAS_BOOL		bWait				= PAAS_TRUE;
	PAAS_BOOL		bSubmitBtnVisible	= PAAS_FALSE;
	PAAS_BOOL		bSubmitBtnReqd		= PAAS_FALSE;
	PAAS_BOOL		bSubmitBtnData		= PAAS_FALSE;
	XEVT_PTYPE		pstXevt				= NULL;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * We enable MSR on the line item screen when preswipe is enabled
	 * if card is not swiped on that screen and we navigate to some other screen
	 * we need to reset so that MSR is disabled
	 */
	disableCardReaders();

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	/*
	 * Setting it to False so that will not take any
	 * available BL data before showing this form
	 * This is to FIX the issue i.e. when multiple times
	 * buttons are pressed on this screen, for the next same command
	 * last press from the previous screen is taken since
	 * BL data is available
	 */
	bBLDataRcvd = PAAS_FALSE;

	initUIReqMsg(&stUIReq, BATCH_REQ);
	/* Initialize the elements of the form */
	whichForm = CHECKBOX_FRM;
	initFormWrapper(frmName[whichForm], iDisableFormFlag, stUIReq.pszBuf);

	// Check for screen title sent from POS
	if(strlen(pstCustCheckBoxDtls->szTitletext) > 0)
	{
		strcpy(szTitle, pstCustCheckBoxDtls->szTitletext);
	}
	else
	{
		/* Get the main title of the display screen */
		fetchScrTitle(szTitle, CUST_CHECKBOX_DEFAULT_TITLE);
	}
	/*Setting the screen title in text box*/
	replaceTextboxTextWrapper(PAAS_CUST_CHECKBOX_TITLE, szTitle, stUIReq.pszBuf);

	// Check if more than 145 characters are present in SCI DISPLAY_TEXT field, than we need to enable scroll bar on TEXT BOX(by default its hidden)
	if(strlen(szTitle) > 145)
	{
		setBoolValueWrapper(PAAS_CUST_CHECKBOX_TITLE, PROP_BOOL_LISTBOX_NO_SCROLL, 0, stUIReq.pszBuf);
	}
	else
	{
		setBoolValueWrapper(PAAS_CUST_CHECKBOX_TITLE, PROP_BOOL_LISTBOX_NO_SCROLL, 1, stUIReq.pszBuf);
	}

	for(iCnt = 0; iCnt < MAX_CHECKBOX; iCnt++)
	{
		if(strlen(pstCustCheckBoxDtls->szCheckboxLabels[iCnt]) > 0 )
		{
			setBoolValueWrapper(iChkBoxCtrlID, PROP_BOOL_VISIBLE, 1, stUIReq.pszBuf);
			setStringValueWrapper(iChkBoxCtrlID, PROP_STR_CAPTION, pstCustCheckBoxDtls->szCheckboxLabels[iCnt], stUIReq.pszBuf);
			iChkBoxCtrlID++;
		}
	}

	/* Show the form */
	showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

	rv = sendUIReqMsg(stUIReq.pszBuf);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Communication with UI agent failed",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
	}
	else
	{
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Showing Customer Checkbox Capture Screen");
			addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
		}

		while(bWait == PAAS_TRUE)
		{
			CHECK_POS_INITIATED_STATE;
			if(bBLDataRcvd == PAAS_TRUE)
			{
				acquireMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);

				if(stBLData.uiRespType == UI_GENRL_RESP)
				{
					bBLDataRcvd = PAAS_FALSE;
				}
				else if(stBLData.uiRespType == UI_XEVT_RESP)
				{
					pstXevt = &(stBLData.stRespDtls.stXEvtInfo);
					if(!strcmp(pstXevt->szFrmName, frmName[whichForm]))
					{
						switch(pstXevt->uiCtrlID)
						{
						case PAAS_CUST_CHECKBOX_FK_SKIP:
							debug_sprintf(szDbgMsg, "%s: Cancel button pressed by user from customer Checkbox screen", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "User Skipped Capturing Customer Checkbox");
								addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
							}
							bWait = PAAS_FALSE;
							rv = UI_CANCEL_PRESSED;
							break;

						case PAAS_CUST_CHECKBOX_FK_SUBMIT:
							if(iCheckBoxOptMask > 0)
							{
								debug_sprintf(szDbgMsg, "%s: Submitted Successfully", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								if(iAppLogEnabled == 1)
								{
									sprintf(szAppLogData, "Option(s) Submitted Successfully");
									addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
								}

								bWait = PAAS_FALSE;
								rv = SUCCESS;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: None of the Checkboxes Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}
							break;

						case PAAS_CUST_CHECKBOX_1:
							if(pstXevt->uiCtrlData)
							{
								debug_sprintf(szDbgMsg, "%s: Checkbox 1 Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								iCheckBoxOptMask |= (0x3F & (1 << 0));
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Checkbox 1 De-Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								iCheckBoxOptMask &= (0x3F ^ (1 << 0));
							}
							break;

						case PAAS_CUST_CHECKBOX_2:
							if(pstXevt->uiCtrlData)
							{
								debug_sprintf(szDbgMsg, "%s: Checkbox 2 Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								iCheckBoxOptMask |= (0x3F & (1 << 1));
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Checkbox 2 De-Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								iCheckBoxOptMask &= (0x3F ^ (1 << 1));
							}
							break;

						case PAAS_CUST_CHECKBOX_3:
							if(pstXevt->uiCtrlData)
							{
								debug_sprintf(szDbgMsg, "%s: Checkbox 3 Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								iCheckBoxOptMask |= (0x3F & (1 << 2));
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Checkbox 3 De-Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								iCheckBoxOptMask &= (0x3F ^ (1 << 2));
							}
							break;

						case PAAS_CUST_CHECKBOX_4:
							if(pstXevt->uiCtrlData)
							{
								debug_sprintf(szDbgMsg, "%s: Checkbox 4 Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								iCheckBoxOptMask |= (0x3F & (1 << 3));
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Checkbox 4 De-Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								iCheckBoxOptMask &= (0x3F ^ (1 << 3));
							}
							break;

						case PAAS_CUST_CHECKBOX_5:
							if(pstXevt->uiCtrlData)
							{
								debug_sprintf(szDbgMsg, "%s: Checkbox 5 Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								iCheckBoxOptMask |= (0x3F & (1 << 4));
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Checkbox 5 De-Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								iCheckBoxOptMask &= (0x3F ^ (1 << 4));
							}
							break;

						case PAAS_CUST_CHECKBOX_6:
							if(pstXevt->uiCtrlData)
							{
								debug_sprintf(szDbgMsg, "%s: Checkbox 6 Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								iCheckBoxOptMask |= (0x3F & (1 << 5));
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Option 6 De-Selected", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								iCheckBoxOptMask &= (0x3F ^ (1 << 5));
							}
							break;

						case PAAS_CUST_CHECKBOX_TITLE:
							debug_sprintf(szDbgMsg, "%s: User selected Title Text to Scroll up and Down", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							break;

						default:
							debug_sprintf(szDbgMsg, "%s: Unknown",__FUNCTION__);
							APP_TRACE(szDbgMsg);
							bWait = PAAS_FALSE;
							rv = ERR_DEVICE_APP;
							break;
						}
						bBLDataRcvd = PAAS_FALSE;
					}
				}
				releaseMutexLock(&(stBLData.uiRespMutex), __FUNCTION__);
			}

			/*Below checks are required to show button in different scenarios */
			/* 1. On the Checkbox device command screen when Submit button is not Visible ,
			 * 		a.Check for Selected Checkbox, if none of them are selected, Submit button is not required.
			 * 		b. When Checkbox are selected, then Submit button is required, Make the data to enabled and make Button it visible
			 * 	2. On Checkbox device command, when Submit button is already Visible
			 * 		a.Check whether Checkbox are Selected, if none of them are selected, then Submit button is required, make the data to be disable and make the Button invisible
			 * 		b.When checkbox are selected, Submit button is already been displayed, Submit button is required.*/

			if(bSubmitBtnVisible)
			{
				if(iCheckBoxOptMask == 0)
				{
					// send Hide button
					bSubmitBtnReqd = PAAS_TRUE;
					bSubmitBtnData = PAAS_FALSE;
					bSubmitBtnVisible = PAAS_FALSE;
				}
				else
				{
					// Dont send
					bSubmitBtnReqd = PAAS_FALSE;
				}
			}
			else
			{
				if(iCheckBoxOptMask == 0)
				{
					// Dont show Submit Button
					bSubmitBtnReqd = PAAS_FALSE;
				}
				else
				{
					// Send Show Button
					bSubmitBtnReqd = PAAS_TRUE;
					bSubmitBtnData = PAAS_TRUE;
					bSubmitBtnVisible = PAAS_TRUE;
				}
			}
			if(bSubmitBtnReqd)
			{
				initUIReqMsg(&stUIReq, BATCH_REQ);
				setBoolValueWrapper(PAAS_CUST_CHECKBOX_FK_SUBMIT, PROP_BOOL_VISIBLE, bSubmitBtnData, stUIReq.pszBuf);

				showFormWrapper(PM_NORMAL, stUIReq.pszBuf);

				rv = sendUIReqMsg(stUIReq.pszBuf);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Communication with UI agent FAILED",
							__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = ERR_DEVICE_APP;
				}
			}
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
		}

		for(iCnt = 0;iCnt < MAX_CHECKBOX; iCnt++)
		{
			if(iCheckBoxOptMask & (1 << iCnt))
			{
				for(jCnt = 0 , iTemp = 0 ; jCnt < MAX_CHECKBOX; jCnt++)
				{
					if((strlen(pstCustCheckBoxDtls->szCheckboxLabels[jCnt]) > 0))
					{
						if(iCnt == iTemp)
						{
							strcpy(pstCustCheckBoxDtls->szRespCheckBox[jCnt], "Y");
						}
						iTemp ++;
					}
				}
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * End of file uiAPIs.c
 * ============================================================================
 */
