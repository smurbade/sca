/******************************************************************
*                       uiCommUtils.c                             *
*******************************************************************
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common/common.h"
#include "common/utils.h"
#include "uiAgent/guimgr.h"
#include "uiAgent/uiCfgDef.h"
#include "uiAgent/uiCfgData.h"
#include "uiAgent/uiMsgFmt.h"
#include "bLogic/bLogicCfgDef.h"

#define DFLT_PLATFORM	915

#define BOOL_PROP_TYPE	"BOOL"
#define STR_PROP_TYPE	"STRING"
#define SHORT_PROP_TYPE	"SHORT"
#define LONG_PROP_TYPE	"LONG"

#define TRACK_ENABLE1	1
#define TRACK_DISABLE1	0

#define KEY_1DES	"1234567890123456"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

//External functions
extern void setEmvReqCmd(ENUM_EMV_CMDS);

static int platform = 0;

/*
 * ============================================================================
 * Function Name: initBatchMsg
 *
 * Description	: This API would initialize the BATCH command.
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
void initUIReqMsg(UIREQ_PTYPE pstUIReq, REQ_MSG_TYPE eReqType)
{
//	int		iSize	= 0;
	char *	szBuf	= NULL;

//	iSize = pstUIReq->pszBuf;
	szBuf = pstUIReq->pszBuf;

#if 0
	if(szBuf == NULL)
	{
		/* NOTE: Assuming that this malloc would never create problem such as
		 * not allocating memory and returning error */
		szBuf = (char *) malloc(4096 * sizeof(char));
		pstUIReq->iSize = iSize = 4096;
		pstUIReq->pszBuf = szBuf;
	}
#endif

	pstUIReq->iSize = sizeof(pstUIReq->pszBuf);
	memset(szBuf, 0x00, sizeof(pstUIReq->pszBuf));

	switch(eReqType)
	{
	case RESET_REQ:
		sprintf(szBuf, "%c72%c", STX, FS);
		break;

	case BATCH_REQ:
		sprintf(szBuf, "%cXBATCH%c", STX, FS);
		break;

	case SETCFGREQ:
		sprintf(szBuf, "%cXSETVAR%c", STX, FS);
		break;

	case GETCFGREQ:
		sprintf(szBuf, "%cXGETVAR%c", STX, FS);
		break;

	case FAVER_REQ:
		sprintf(szBuf, "%cXVER%c", STX, FS);
		break;

	case XLCS_REQ:
		sprintf(szBuf, "%cXLCS%c", STX, FS);
		break;

	case XCLB_REQ:
		sprintf(szBuf, "%cXCLB%c", STX, FS);
		break;

	case XCLS_REQ:
		sprintf(szBuf, "%cXCLS%c", STX, FS);
		break;

	case S002_REQ:
		sprintf(szBuf, "%cS002%c", STX, FS);
		break;

	case INIT_EMV_REQ:
		sprintf(szBuf, "%c", STX);
		break;

	case XPI_RESET_REQ:
		sprintf(szBuf, "%cS0000%c", STX, FS);
		break;

	case INIT_EMV_CONFIG_REQ:
		sprintf(szBuf, "%cXCONFIG%c", STX, FS);
		break;

	case XSWT_REQ:
		sprintf(szBuf, "%cXSWT%c", STX, FS);
		break;

	case XMBX_REQ:
		sprintf(szBuf, "%cXMBX%c", STX, FS);
		break;

	case XMBH_REQ:
		sprintf(szBuf, "%cXMBH%c", STX, FS);
		break;

	case RSTART_REQ:
		sprintf(szBuf,"%cXRSTAPP%c", STX, FS);
		break;

	case INIT_REQ:
	default:
		/* Do nothing */
		break;
	}

	return;
}

/*
 * ============================================================================
 * Function Name: initFormWrapper
 *
 * Description	: This function creates the message to initialize the form
 *                REQ: <STX>XBATCH<FS>XIFM<FS>formName<RS>
 * Input Params	: formName -- Name of the form to be initialized.
 *
 * Output Params: None
 * ============================================================================
 */
void initFormWrapper(const char * formName, int iDisableFormFlag, char * szBuf)
{
	char	frmName[30]		= "";
	char	szTmp[200]	= "";
#if defined(DEBUG)
	char	szDbgMsg[4096]	= "";
#endif

	switch(platform)
	{
	case MODEL_MX915:
		sprintf(frmName, "915_%s", formName);
		break;

	case MODEL_MX925:
		sprintf(frmName, "925_%s", formName);
		break;

	default:
		/* This will happen only in the begining, once platform value is
		 * set, the upper two switch case would work always */
		platform = getDevicePlatform();

		switch(platform)
		{
		case MODEL_MX915:
			sprintf(frmName, "915_%s", formName);
			break;

		case MODEL_MX925:
			sprintf(frmName, "925_%s", formName);
			break;

		default:
			sprintf(frmName, "%d_%s", DFLT_PLATFORM, formName);
			break;
		}
		break;
	}

#if 0
	<STX>XBATCH<FS>XIFM<FS>formName<RS>
#endif

	sprintf(szTmp, "XIFM%c%s%c%d%c", FS, frmName, FS, iDisableFormFlag, RS);
	strcat(szBuf, szTmp);

    debug_sprintf(szDbgMsg,"%s: Msg = [%s]", __FUNCTION__, szBuf);
    APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: showFormWrapper
 *
 * Description	: This function creates the message to show the form
 *                REQ: <STX>XBATCH<FS>XSFM<RS>
 *
 * Input Params	: formShowOpt -- way to show form.i.e NORMAL, RIGHT TO LEFT,
 * 					LEFT TO RIGHT, BOTTOM UP, TOP DOWN, FADE.
 *
 * Output Params: None
 * ============================================================================
 */
void showFormWrapper(int iShowOpt, char * szBuf)
{
		char		tempMsg[400]	= "";

       /*
       * We would like to display DEMO MODE text when we are
       * running in the DEMO mode.
       * instead of adding this message at all places
       * we are adding them in the show form wrapper function
       */

#if 0
       if(isDemoModeEnabled()) //If Demo mode is enabled adding text on the screen
       {
              memset(tempMsg, 0x00, sizeof(tempMsg));
              memset(szTmpDemo, 0x00, sizeof(szTmpDemo));

              /*
              * <STX>XDTX<FS>X<FS>Y<FS>
                     WrapRow<FS>WrapColumn<FS>OptionsMask<FS>FontName<FS>
                     FontSize<FS>Text[<FS>Colors]<ETX>{LRC}
              */

              sprintf(szTmpDemo, "<B>%s", getDemoModeText());

              //Setting Font size based on device platform and increasing by 10 units as per
              iFontSize = getDevicePlatform();
              if(iFontSize == MODEL_MX915)
              {
            	  iFontSize = 23;
              }
              else
              {
            	  iFontSize = 29;
              }
#if 0
              debug_sprintf(szDbgMsg, "%s:Demotext [%s]", __FUNCTION__, szTmpDemo);
              APP_TRACE(szDbgMsg);
#endif

#if 0
              sprintf(tempMsg, "XDTX%c%d%c%d%c%d%c%d%c%d%c%s%c%d%c%s%c%s%c",
                                  FS, 50/*X*/, FS, 200/*Y*/, FS, 700/*Wraprow*/, FS, 700/*Wrapcolumn*/, FS, 32769/*Options*/, FS,  "VeraMono|VeraMoBd|VeraMoIt|VeraMoBI" /*Font Name*/,
                                  FS, 34/*Font Size*/, FS, szTmpDemo/* Text */, FS, "FFFFFF|FF0000|FFFF00" /*Font color*/, RS);
#endif

              sprintf(tempMsg, "XSWT%c%s%c%s%c%d%c", FS, szTmpDemo, FS, "VeraMono" /*Font Name*/, FS, iFontSize/*Font Size*/, RS);

              strcat(szBuf, tempMsg);
       }
#endif
       memset(tempMsg, 0x00, sizeof(tempMsg));

       sprintf(tempMsg, "XSFM%c%d%c", FS, iShowOpt, RS);

       strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: setTimeWrapper
 *
 * Description	: This function creates the message to show the form
 *                REQ: <STX>XCLOCK<FS>TIME_STRING<RS>
 *
 * Input Params	: TIME format: CCYYMMDDhhmmss
 *
 * Output Params: return value SUCCESS or FAILURE
 * ============================================================================
 */
int setTimeWrapper(char * szTime, char* szBuf)
{
	int 	rv			= SUCCESS;
	char	tmpBuf[200]	= "";

	sprintf(tmpBuf, "XCLOCK%c%s%c",FS, szTime, RS);
	strcat(szBuf, tmpBuf);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setStringValueWrapper
 *
 * Description	: This API would set the String value of the property.
 *                REQ-- XSPV<FS>controlId<FS>PropName<FS>PropType<FS>PropVal<RS>
 *
 * Input Params	: iCtrlId-- ControlID of the property.
 *                cName-- Property name.
 *                PropVal -- Property value.
 *
 * Output Params: None
 * ============================================================================
 */
void setStringValueWrapper(int iCtrlId, char cName, char * szVal, char * szBuf)
{
	char	tempMsg[500]		= "";
	char	propName[30]		= "";

	switch(cName)
	{
	case PROP_STR_CAPTION:
		strncpy(propName, "CAPTION", strlen("CAPTION"));

		break;

	case PROP_STR_UP_IMAGE_FILE_NAME:
		strncpy(propName, "UP_IMAGE_FILE_NAME", strlen("UP_IMAGE_FILE_NAME"));
		break;

	case PROP_STR_DOWN_IMAGE_FILE_NAME:
		strncpy(propName, "DOWN_IMAGE_FILE_NAME",
											strlen("DOWN_IMAGE_FILE_NAME"));
		break;

	case PROP_STR_IMAGE_FILE_NAME:
		strncpy(propName, "IMAGE_FILE_NAME", strlen("IMAGE_FILE_NAME"));
		break;

	case PROP_STR_UNCHECKED_FILE_NAME:
		strncpy(propName, "UNCHECKED_FILE_NAME", strlen("UNCHECKED_FILE_NAME"));
		break;

	case PROP_STR_CHECKED_FILE_NAME:
		strncpy(propName, "CHECKED_FILE_NAME", strlen("CHECKED_FILE_NAME"));
		break;

	case PROP_STR_DISPLAY_STRING:
		strncpy(propName, "DISPLAY_STRING", strlen("DISPLAY_STRING"));
		break;

	case PROP_STR_FORMAT_STRING:
		strncpy(propName, "FORMAT_STRING", strlen("FORMAT_STRING"));
		break;

	case PROP_STR_CUSTOM_IMAGE_FILE_NAME:
		strncpy(propName, "CUSTOM_IMAGE_FILE_NAME",
											strlen("CUSTOM_IMAGE_FILE_NAME"));
		break;

	case PROP_STR_ANIM_FILE_NAME:
		strncpy(propName, "ANIM_FILE_NAME", strlen("ANIM_FILE_NAME"));
		break;

	case PROP_STR_MARQUEE_TEXT:
		strncpy(propName, "MARQUEE_TEXT", strlen("MARQUEE_TEXT"));
		break;

	case PROP_STR_FORM_BACKGROUND:
		strncpy(propName, "FORM_BG", strlen("FORM_BG"));
		break;

	case PROP_STR_VIDEO_FILE_NAME:
		strncpy(propName, "VIDEO_FILE_NAME", strlen("VIDEO_FILE_NAME"));
		break;

	case PROP_STR_INPUT_STRING:
		strncpy(propName, "INPUT_STRING", strlen("INPUT_STRING"));
		break;

	case PROP_STR_FONT_NAME:
		strncpy(propName, "FONT_NAME", strlen("FONT_NAME"));
		break;

	default:
		break;
	}

	sprintf(tempMsg, "XSPV%c%d%c%s%c%s%c%s%c", FS, iCtrlId, FS, propName, FS
											, STR_PROP_TYPE, FS, szVal, RS);
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: setBoolValueWrapper
 *
 * Description	: This API would set the BOOL value of the property.
 *                REQ-- XSPV<FS>controlId<FS>PropName<FS>PropType<FS>PropVal<RS>
 *
 * Input Params	: iCtrlId-- ControlID of the property.
 *                cName -- Property name.
 *                propVal -- Property Value.
 *
 * Output Params: None
 * ============================================================================
 */
void setBoolValueWrapper(int iCtrlId, char cName, int propVal, char * szBuf)
{
	char	tempMsg[200]	= "";
	char	propName[30]	= "";

	switch(cName)
	{
	case PROP_BOOL_VISIBLE:
		strncpy(propName, "VISIBLE", strlen("VISIBLE"));
		break;

	case PROP_BOOL_BORDER_VISIBLE:
		strncpy(propName, "BORDER_VISIBLE", strlen("BORDER_VISIBLE"));
		break;

	case PROP_BOOL_AUTO_SIZE:
		strncpy(propName, "AUTO_SIZE", strlen("AUTO_SIZE"));
		break;

	case PROP_BOOL_TRANSPARENT:
		strncpy(propName, "TRANSPARENT", strlen("TRANSPARENT"));
		break;

	case PROP_BOOL_BEEP_ON_PRESS:
		strncpy(propName, "BEEP_ON_PRESS", strlen("BEEP_ON_PRESS"));
		break;

	case PROP_BOOL_STAY_PRESSED:
		strncpy(propName, "STAY_PRESSED", strlen("STAY_PRESSED"));
		break;

	case PROP_BOOL_SELECTED:
		strncpy(propName, "SELECTED", strlen("SELECTED"));
		break;

	case PROP_BOOL_BTN_VISUAL_EFFECT:
		strncpy(propName, "VISUAL_EFFECT", strlen("VISUAL_EFFECT"));
		break;

	case PROP_BOOL_LISTBOX_NO_SCROLL:
		strncpy(propName, "NO_SCROLL", strlen("NO_SCROLL"));
		break;

	case PROP_BOOL_ANIMATE_RUN:
		strncpy(propName, "RUN", strlen("RUN"));
		break;

	case PROP_BOOL_CAPTION_AREA_ACTIVE:
		strncpy(propName, "RUN", strlen("RUN"));
		break;

	case PROP_BOOL_VIDEO_REPEAT:
		strncpy(propName, "VIDEO_REPEAT", strlen("VIDEO_REPEAT"));
		break;

	case PROP_BOOL_ENABLE:
		strncpy(propName, "ENABLE", strlen("ENABLE"));
		break;

	default:
		break;
	}

	sprintf(tempMsg, "XSPV%c%d%c%s%c%s%c%d%c", FS, iCtrlId, FS, propName, FS,
											BOOL_PROP_TYPE, FS, propVal, RS);
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: getBoolValueWrapper
 *
 * Description	: This API would get BOOL value of the property.
 *                REQ-- XGPV<FS>controlId<FS>PropName<FS>PropType<FS>PropVal<RS>
 *
 * Input Params	: iCtrlId-- ControlID of the property.
 *                cPropName-- Property name.
 *                PropVal -- Property value.
 *
 * Output Params: None
 * ============================================================================
 */
void getBoolValueWrapper(int iCtrlId, char cPropName, char* szBuf)
{
	char	tempMsg[200]	= "";
	char	propName[30]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#if 0
	XGPV<FS>controlId<FS>PropName<FS>PropType<RS>
#endif

	switch(cPropName)
	{
	case PROP_BOOL_SELECTED:
		strncpy(propName, "SELECTED", strlen("SELECTED"));
		break;
	default:
		break;
	}

	sprintf(tempMsg, "XGPV%c%d%c%s%c%s%c", FS, iCtrlId, FS, propName, FS, BOOL_PROP_TYPE, RS);
	strcat(szBuf, tempMsg);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}
/*
 * ============================================================================
 * Function Name: getShortValueWrapper
 *
 * Description	: This API would get short value of the property.
 *                REQ-- XGPV<FS>controlId<FS>PropName<FS>PropType<FS>PropVal<RS>
 *
 * Input Params	: iCtrlId-- ControlID of the property.
 *                cPropName-- Property name.
 *                PropVal -- Property value.
 *
 * Output Params: None
 * ============================================================================
 */
void getShortValueWrapper(int iCtrlId, char cPropName, char* szBuf)
{
	char	tempMsg[200]	= "";
	char	propName[30]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#if 0
	XGPV<FS>controlId<FS>PropName<FS>PropType<RS>
#endif
	switch(cPropName)
	{
	case PROP_SHORT_OPTIONS:
		strncpy(propName, "OPTIONS", strlen("OPTIONS"));
		break;

	case  PROP_SHORT_TEXT_JUSTIFICATION:
		strncpy(propName, "TEXT_JUSTIFICATION", strlen("TEXT_JUSTIFICATION"));
		break;

	case PROP_SHORT_FONT_ATTRIBUTES:
		strncpy(propName, "FONT_ATTRIBUTES", strlen("FONT_ATTRIBUTES"));
		break;

	case PROP_SHORT_STYLE:
		strncpy(propName, "STYLE", strlen("STYLE"));
		break;

	case PROP_SHORT_ITEMS_TO_SCROLL:
		strncpy(propName, "ITEMS_TO_SCROLL", strlen("ITEMS_TO_SCROLL"));
		break;

	case PROP_SHORT_MIN_ENTRIES:
		strncpy(propName, "MIN_ENTRIES", strlen("MIN_ENTRIES"));
		break;

	case PROP_SHORT_KEYPAD_TYPE:
		strncpy(propName, "KEYPAD_TYPE", strlen("KEYPAD_TYPE"));
		break;

	case PROP_SHORT_KEYPAD_IMAGE_POS_LEFT:
		strncpy(propName, "KEYPAD_IMAGE_POS_LEFT",
											strlen("KEYPAD_IMAGE_POS_LEFT"));
		break;

	case PROP_SHORT_KEYPAD_IMAGE_POS_TOP:
		strncpy(propName, "KEYPAD_IMAGE_POS_TOP", strlen("KEYPAD_IMAGE_POS_TOP"));
		break;

	case PROP_SHORT_BTN_STATE:
		strncpy(propName, "BUTTON_STATE", strlen("BUTTON_STATE"));
		break;

	case PROP_SHORT_ANIM_DELAY_INTERVAL:
		strncpy(propName, "DELAY_INTERVAL", strlen("DELAY_INTERVAL"));
		break;

	case PROP_SHORT_ANIM_LOOP_COUNT:
		strncpy(propName, "LOOP_COUNT", strlen("LOOP_COUNT"));
		break;

	case PROP_SHORT_ANIM_PRES_EFFECT:
		strncpy(propName, "PRES_EFFECT", strlen("PRES_EFFECT"));
		break;

	case PROP_SHORT_BTN_RESET_STATE:
		strncpy(propName, "BUTTON_RESET_STATE", strlen("BUTTON_RESET_STATE"));
		break;

	case PROP_SHORT_VIDEO_MODE:
		strncpy(propName, "VIDEO_MODE", strlen("VIDEO_MODE"));
		break;

	case PROP_SHORT_FONT_SIZE:
		strncpy(propName, "FONT_SIZE", strlen("FONT_SIZE"));
		break;

	default:
		break;
	}

	sprintf(tempMsg, "XGPV%c%d%c%s%c%s%c", FS, iCtrlId, FS, propName, FS,
												SHORT_PROP_TYPE, RS);
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: setShortValueWrapper
 *
 * Description	: This API would short value of the property.
 *                REQ-- XSPV<FS>controlId<FS>PropName<FS>PropType<FS>PropVal<RS>
 *
 * Input Params	: iCtrlId-- ControlID of the property.
 *                cName-- Property name.
 *                PropVal -- Property value.
 *
 * Output Params: None
 * ============================================================================
 */
void setShortValueWrapper(int iCtrlId, char cName, short propVal, char * szBuf)
{
	char	tempMsg[200]	= "";
	char	propName[30]	= "";

#if 0
	XSPV<FS>controlId<FS>PropName<FS>PropType<FS>PropVal<RS>
#endif
	switch(cName)
	{
	case PROP_SHORT_OPTIONS:
		strncpy(propName, "OPTIONS", strlen("OPTIONS"));
		break;

	case  PROP_SHORT_TEXT_JUSTIFICATION:
		strncpy(propName, "TEXT_JUSTIFICATION", strlen("TEXT_JUSTIFICATION"));
		break;

	case PROP_SHORT_FONT_ATTRIBUTES:
		strncpy(propName, "FONT_ATTRIBUTES", strlen("FONT_ATTRIBUTES"));
		break;

	case PROP_SHORT_STYLE:
		strncpy(propName, "STYLE", strlen("STYLE"));
		break;

	case PROP_SHORT_ITEMS_TO_SCROLL:
		strncpy(propName, "ITEMS_TO_SCROLL", strlen("ITEMS_TO_SCROLL"));
		break;

	case PROP_SHORT_MIN_ENTRIES:
		strncpy(propName, "MIN_ENTRIES", strlen("MIN_ENTRIES"));
		break;

	case PROP_SHORT_KEYPAD_TYPE:
		strncpy(propName, "KEYPAD_TYPE", strlen("KEYPAD_TYPE"));
		break;

	case PROP_SHORT_KEYPAD_IMAGE_POS_LEFT:
		strncpy(propName, "KEYPAD_IMAGE_POS_LEFT",
											strlen("KEYPAD_IMAGE_POS_LEFT"));
		break;

	case PROP_SHORT_KEYPAD_IMAGE_POS_TOP:
		strncpy(propName, "KEYPAD_IMAGE_POS_TOP", strlen("KEYPAD_IMAGE_POS_TOP"));
		break;

	case PROP_SHORT_BTN_STATE:
		strncpy(propName, "BUTTON_STATE", strlen("BUTTON_STATE"));
		break;

	case PROP_SHORT_ANIM_DELAY_INTERVAL:
		strncpy(propName, "DELAY_INTERVAL", strlen("DELAY_INTERVAL"));
		break;

	case PROP_SHORT_ANIM_LOOP_COUNT:
		strncpy(propName, "LOOP_COUNT", strlen("LOOP_COUNT"));
		break;

	case PROP_SHORT_ANIM_PRES_EFFECT:
		strncpy(propName, "PRES_EFFECT", strlen("PRES_EFFECT"));
		break;

	case PROP_SHORT_BTN_RESET_STATE:
		strncpy(propName, "BUTTON_RESET_STATE", strlen("BUTTON_RESET_STATE"));
		break;

	case PROP_SHORT_VIDEO_MODE:
		strncpy(propName, "VIDEO_MODE", strlen("VIDEO_MODE"));
		break;

	case PROP_SHORT_FONT_SIZE:
		strncpy(propName, "FONT_SIZE", strlen("FONT_SIZE"));
		break;

	default:
		break;
	}

	sprintf(tempMsg, "XSPV%c%d%c%s%c%s%c%d%c", FS, iCtrlId, FS, propName, FS,
											SHORT_PROP_TYPE, FS, propVal, RS);
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: setLongValueWrapper
 *
 * Description	:  This API would long value of the property.
 *                 REQ: XSPV<FS>controlId<FS>PropName<FS>PropType<FS>PropVal<RS>
 *
 * Input Params	: iCtrlId-- ControlID of the property.
 *                cName-- Property name.
 *                PropVal -- Property value.
 *
 * Output Params: None
 * ============================================================================
 */
void setLongValueWrapper(int iCtrlId, char cName, char *propVal, char * szBuf)
{
	char	tempMsg[200]	= "";
	char	propName[30]	= "";

#if 0
	XSPV<FS>controlId<FS>PropName<FS>PropType<FS>PropVal<RS>
#endif

	switch(cName)
	{
	case PROP_LONG_BG_COLOR:
		strncpy(propName, "BG_COLOR", strlen("BG_COLOR"));
		break;

	case PROP_LONG_FG_COLOR:
		strncpy(propName, "FG_COLOR", strlen("FG_COLOR"));
		break;

	case PROP_LONG_TIMEOUT:
		strncpy(propName, "TIMEOUT", strlen("TIMEOUT"));
		break;

	default:
		break;
	}

	sprintf(tempMsg, "XSPV%c%d%c%s%c%s%c%s%c", FS, iCtrlId, FS, propName, FS,
											LONG_PROP_TYPE, FS, propVal, RS);
	strcat(szBuf, tempMsg);

    return;
}

/*
 * ============================================================================
 * Function Name: setCurVolWrapper
 *
 * Description	: 
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
void setCurVolWrapper(int vol, int bass, int treble, char * szBuf)
{
	char	tmpMsg[200]		= "";

	sprintf(tmpMsg, "XSETVOL%c%d%c%d%c%d%c", FS, vol, FS, bass, FS, treble, RS);
	strcat(szBuf, tmpMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: getCurVolWrapper
 *
 * Description	: 
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
void getCurVolWrapper(char * szBuf)
{
	char	tmpMsg[10]	= "";

	sprintf(tmpMsg, "XGETVOL%c", RS);
	strcat(szBuf, tmpMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: xvclCmdWrapper
 *
 * Description	: This function builds message to do device registration.
 *                REQ: <STX>XVCL<FS>COMMAND<ETX>{LRC}
 *                RES: <STX>XVCL<FS>COMMAND<FS><Track1><FS><Track2><FS><Track3>
 *					[<FS>Response]<ETX>{LRC}
 * Input Params	:
 *
 * Output Params: None
 * ============================================================================
 */
void xvclCmdWrapper(PAAS_BOOL bSREDEnabled, char * szBuf, REQ_MSG_TYPE eReqType)
{
	char	tempMsg[50]	= "";
	switch(eReqType)
	{
		case DEVICE_REG:
			if(bSREDEnabled == PAAS_TRUE)
			{
				/* SRED MODE */
				sprintf(tempMsg, "XVCL%cDEVICE_REG%c1%c", FS, FS, FS);
			}
			else
			{
				/* NON SRED MODE */
				sprintf(tempMsg, "XVCL%cDEVICE_REG%c", FS, FS);
			}
			break;
		case ADVANCE_DDK:
			/* SRED MODE or NON SRED Mode ADVANCE DDK is same */
			//sprintf(tempMsg, "XVCL%cADVANCE_DDK%c1%c", FS, FS, FS);
			sprintf(tempMsg, "XVCL%cADVANCE_DDK%c", FS, FS);

			break;
		default:
			break;
	}
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: AddTextboxTextWrapper
 *
 * Description	: Used to add a line of item text to a textbox control specified
 * 					by ControlID.
 *                REQ: <STX>XATT<FS>ControlID<FS>Text<ETX>{LRC}
 *                RSP: <STX>XATT<FS>RetVal<ETX>{LRC}
 * Input Params	: iCtrlId -- ControlID of the the text box.
 *                pszItemText -- Item to be added.
 *
 * Output Params: None
 * ============================================================================
 */
void addTextboxTextWrapper(short iCtrlId, char * pszTxt, char * szBuf)
{
	char	tempMsg[1024*5]		= "";
	char	tmpCaption[1024*5]	= "";
	
	/* Convert string to UTF string */
	convStrToUTFEscStr(pszTxt, tmpCaption);
	sprintf(tempMsg, "XATT%c%d%c%s%c", FS, iCtrlId, FS, tmpCaption, RS);

	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: clearTextboxTextWrapper
 *
 * Description	: Used to clear a line of item text to a textbox control specified
 * 					by ControlID.
 *                REQ: <STX>XCTT<FS>ControlID<FS>Text<ETX>{LRC}
 *                RSP: <STX>XCTT<FS>RetVal<ETX>{LRC}
 * Input Params	: iCtrlId -- ControlID of the the text box.
 *                pszItemText -- Item to be added.
 *
 * Output Params: None
 * ============================================================================
 */
void clearTextboxTextWrapper(short iCtrlId, char * szBuf)
{
	char	tempMsg[1024*5]		= "";

	sprintf(tempMsg, "XCTT%c%d%c", FS, iCtrlId, RS);

	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: replaceTextboxTextWrapper
 *
 * Description	: Used to replace text in a textbox control specified
 * 					by ControlID.
 *                REQ: <STX>XRTT<FS>ControlID<FS>Text<ETX>{LRC}
 *                RSP: <STX>XRTT<FS>RetVal<ETX>{LRC}
 * Input Params	: iCtrlId -- ControlID of the the text box.
 *                pszItemText -- Item to be added.
 *
 * Output Params: None
 * ============================================================================
 */
void replaceTextboxTextWrapper(short iCtrlId, char * pszTxt, char * szBuf)
{
	char	tempMsg[1024*5]		= "";
	char	tmpCaption[1024*5]	= "";

	/* Convert string to UTF string */
	convStrToUTFEscStr(pszTxt, tmpCaption);
	sprintf(tempMsg, "XRTT%c%d%c%s%c", FS, iCtrlId, FS, tmpCaption, RS);

	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: addListboxItemWrapper
 *
 * Description	: Used to add a line of item text to a listbox control specified
 * 					by ControlID.
 *                REQ: <STX>XALI<FS>ControlID<FS>ItemIndex<FS>ItemText<FS>
 *                		KeepCopyFlag<ETX>{LRC}
 *                RSP: <STX>XALI<FS>RetVal<ETX>{LRC}
 *
 * Input Params	: iCtrlId -- ControlID of the listbox.
 *                iIdx -- Index to add into the listbox.
 *                pszItemText -- Name of the item to be added.
 *                KeepCopyFlag -- Flag to keep a copy of the item.
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void addListboxItemWrapper(int id, int idx, char * szTxt, int flag,char * szBuf)
{
	char	tempMsg[200]	= "";

	sprintf(tempMsg, "XALI%c%d%c%d%c%s%c%d%c", FS, id, FS, idx, FS, szTxt, FS,
																	flag, RS);
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: removeListboxItemWrapper
 *
 * Description	: Used to remove a line of item text from the listbox control
 * 					specified by ControlID and index.
 *                REQ: <STX>XRLI<FS>ControlID<FS>ItemIndex<ETX>{LRC}
 *                RSP: <STX>XRLI<FS>RetVal<ETX>{LRC}
 *
 * Input Params	: iCtrlId -- ControlID of the listbox.
 *                iIdx -- Index of the item to be removed.
 *
 * Output Params: none
 * ============================================================================
 */
void removeListboxItemWrapper(int iCtrlId, int iIdx, char * szBuf)
{
	char	tempMsg[200]	= "";

	sprintf(tempMsg, "XRLI%c%d%c%d%c", FS, iCtrlId, FS, iIdx, RS);
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: updateListboxItemWrapper
 *
 * Description	: Used to remove a line of item text from the listbox control
 * 					specified by ControlID and index.
 *                REQ: <STX>XULI<FS>ControlID<FS>ItemIndex<FS>ItemText<ETX>{LRC}
 *                RSP: <STX>XULI<FS>RetVal<ETX>{LRC}
 *
 * Input Params	: iCtrlId -- ControlID of the listbox.
 *                iIdx -- Index of the item to be updated.
 *
 * Output Params: none
 * ============================================================================
 */
void updateListboxItemWrapper(int id, int idx, char * szTxt, char * szBuf)
{
	char	tempMsg[200]	= "";

	sprintf(tempMsg, "XULI%c%d%c%d%c%s%c", FS, id, FS, idx, FS, szTxt, RS);
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: setListBoxRestoreState
 *
 * Description	: Used in conjunction with the XALI command;
 * 				  used to update a listbox control with a cached copy of listbox items
 *                REQ: <STX>XLRS<FS>ControlID<ETX>{LRC}
 *                RSP: <STX>XLRS<FS>RetVal<ETX>{LRC}
 *						RetVal -> Count of listbox items restored
 * Input Params	: iCtrlId -- ControlID of the listbox.
 *
 *
 * Output Params: none
 * ============================================================================
 */
void setListBoxRestoreState(int iCtrId, char *szBuf)
{
	char	tempMsg[200]	= "";

	sprintf(tempMsg, "XLRS%c%d%c", FS, iCtrId, RS);
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: clearListBoxRestoreState
 *
 * Description	: Used in conjunction with the XALI and XLRS commands;
 * 				  used to clear the cache of listbox items
 *                REQ: <STX>XLCS<ETX>{LRC}
 *                RSP: None
 *
 * Input Params	: none
 *
 *
 * Output Params: none
 * ============================================================================
 */
void clearListBoxRestoreState(char *szBuf)
{
	char	tempMsg[200]	= "";

	sprintf(tempMsg, "XLCS%c", RS);
	strcat(szBuf, tempMsg);

	return;
}
/*
 * ============================================================================
 * Function Name: clearListBoxfromCurrentForm
 *
 * Description	: Used to clear the contents of a listbox control in the current form.
 * 				 (The listbox control must be present in the current form)
 *                REQ: <STX>XCLB<FS>ControlID<ETX>{LRC}
 *                RSP: <STX>XCLB<FS>RetVal<ETX>{LRC}
 *
 * Input Params	: iCtrlId -- ControlID of the listbox.
 *
 *
 * Output Params: none
 * ============================================================================
 */

void clearListBoxfromCurrentForm(int iCtrId,char *szBuf)
{
	char	tempMsg[200]	= "";

	sprintf(tempMsg, "XCLB%c%d%c", FS, iCtrId, RS);
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: setCfgVarWrapper
 *
 * Description	: This function builds message to set config parameters in
 * 					config.usr* file.
 *                REQ: XSETVAR<FS>Parameter1=Value1<FS>
 *                RES: XSETVAR<FS>RetVal1<FS><ETX>{LRC}
 *
 * Input Params	:
 *
 * Output Params: None
 * ============================================================================
 */
void setCfgVarWrapper(char * szKey, char * szVal, char * szBuf)
{
	char	szTmpMsg[512]	= "";
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	sprintf(szTmpMsg, "%s=%s%c", szKey, szVal, FS);

	debug_sprintf(szDbgMsg, "%s: Msg = [%s]", __FUNCTION__, szTmpMsg);
	APP_TRACE(szDbgMsg);

	strcat(szBuf, szTmpMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: setSigCapParams()
 *
 * Description	: Updates the signature capture parameters.
 *                REQ: <STX>S03sssdddxerrrmF{fn}<ETX>{LRC}
 *                RSP: None
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void setSigCapParams(int tmBfrSign, int tmAftrSign, int dispMode, int endian,
									int sigRes, int saveMode, char * szBuf)
{
	char	tempMsg[200]	= "";
	char	sztmBfr[4]		= "";
	char	sztmAftr[4]		= "";
	char	szsigRes[4]		= "";
	short	iEndianess		= 0;

	/* Check for timeout before signature */
	if(tmBfrSign > 300)
	{
		sprintf(sztmBfr, "300");
	}
	else if(tmBfrSign < 0)
	{
		sprintf(sztmBfr, "000");
	}
	else
	{
		sprintf(sztmBfr, "%03d", tmBfrSign);
	}

	/* Check for timeout after signature */
	if(tmAftrSign > 300)
	{
		sprintf(sztmAftr, "300");
	}
	else if(tmBfrSign < 0)
	{
		sprintf(sztmAftr, "000");
	}
	else
	{
		sprintf(sztmAftr, "%03d", tmAftrSign);
	}

	/* Check for signature resolution */
	if(sigRes > 999)
	{
		sprintf(szsigRes, "999");
	}
	else if(tmBfrSign < 0)
	{
		sprintf(szsigRes, "000");
	}
	else
	{
		sprintf(szsigRes, "%03d", sigRes);
	}

	/* Check for display mode  */

	/* Check for endianness  */
	if(endian == 0 )
	{
		iEndianess = 0;
	}
	else
	{
		iEndianess = 1;
	}

	/* Check for save mode  */

	sprintf(tempMsg, "S03%s%s%d%d%s%d%c", sztmBfr,sztmAftr,dispMode, iEndianess,
										szsigRes, saveMode, RS);
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: setSigCapBoxArea()
 *
 * Description	: Sets the coordinates of the signature box.
 *                REQ: <STX>S04xxxyyyXXXYYYC<ETX>
 *                RSP: None
 *
 * Input Params	: startX, startY -- start X,Y coordinates of sigbox
 *                endX, endY --  end X,Y coordinates of sigbox
 *                clearArea -- Clear area flag: 0=OFF, 1=ON
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void setSigCapBoxArea(int startX, int startY, int endX, int endY, int clearArea
																, char * szBuf)
{
	char	szStartX[4]	=	"";
	char	szStartY[4]	=	"";
	char	szEndX[4]	=	"";
	char	szEndY[4]	=	"";
	char	tempMsg[200]=	"";

	sprintf(szStartX, "%03d", startX);
	sprintf(szStartY, "%03d", startY);
	sprintf(szEndX, "%03d", endX);
	sprintf(szEndY, "%03d", endY);

	sprintf(tempMsg, "S04%s%s%s%s%d%c", szStartX, szStartY, szEndX, szEndY,
															clearArea, RS);
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: getSignature
 *
 * Description	: This API would send signature command.
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
void getSignature(char * szBuf)
{
	char tempMsg[50] = "";

	sprintf(tempMsg, "S00%c", RS);
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: formDukptPinRequest
 *
 * Description	: For DUKPT:
 * 					REQ: <STX>Z60.{AA..A}<ETX>
 *
 * Input Params	: szPAN -> account number
 *
 * Output Params: none
 * ============================================================================
 */
void formDukptPinRequest(char * szPAN, char * szBuf, char * szMessage)
{
	char	szTmp[200]	= "";

	//sprintf(szTmp, "%cZ60.%s%c", STX, szPAN, RS);

	/*
	 * Praveen_P1: Need to send Z62 since we are using XPI for PIN entry
	 */
	sprintf(szTmp, "%cZ62.%s%c0412N%s%c%c%c", STX, szPAN, FS, szMessage, FS, FS, RS);
	setEmvReqCmd(EMV_Z62);
	strcat(szBuf, szTmp);

	return;
}

/*
 * ============================================================================
 * Function Name: formMkPinRequest
 *
 * Description	: For Master/Session (M/S):
 * 					REQ: <STX>Z60.{AA..A}<FS>{WW..W}<ETX>
 *
 * Input Params	: szPAN -> Account Number
 *
 * Output Params: None
 * ============================================================================
 */
void formMkPinRequest(char * szPAN, char * szBuf)
{
	char	szTmp[200]	= "";

	sprintf(szBuf, "%cZ60.%s%c%s%c", STX, szPAN, FS,KEY_1DES, RS);

	setEmvReqCmd(EMV_Z60);

	strcat(szBuf, szTmp);

	return;
}

/*
 * ============================================================================
 * Function Name: getManualCardDataUtil
 *
 * Description	: This API frames the Manual Card Entry Request to FA.
 *
 * Input Params	: NULL, Fills the szBuf buffer.
 *
 * Output Params: SUCCESS/FAILURE.
 * ============================================================================
 */
void getManualCardDataUtil(int iOpt, char * szPANTitle, char * szExpDtTitle,
												char * szCVVTitle, char * szBuf)
{
	int		iMinPAN			= 8;
	int		iMaxPAN			= 19;
	int		iPANOption		= 0;
	int		iExpiryOption	= 2;
	int		iCVVOption		= 0;
	char	szXGCDMsg[1024]	= "";
#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

    debug_sprintf(szDbgMsg,"%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);

	while(1)
	{
		switch(iOpt)
		{
		case NON_GIFT_CARD:
			if(isSTBLogicEnabled())
			{
				/*
				 * When STB is enabled, we will not know the card type before prompting
				 * thats why making it optional
				 * TODO we need to change this later
				 */
				iCVVOption    = 0; //Masked + Optional + Prompt

				iExpiryOption = 0; //Masked + Optional + Prompt + Validate
			}
			else
			{
				iCVVOption    = 2; //Masked + Mandatory + Prompt
			}
			break;

		case PRIVATE_LABEL:
			if(isCvvReqdForPrivLabel() == PAAS_FALSE)
			{
				iCVVOption    = 7; //(Normal + Mandatory + No Prompt)
			}
			else
			{
				iCVVOption    = 0; //(Normal + Optioanl + Prompt)
			}

			if(isExpReqdForPrivLabel() == PAAS_FALSE)
			{
				iExpiryOption = 13; //Normal + Optional + No Prompt + Skip Validate
			}
			else
			{
				iExpiryOption = 0; //Masked + Optional + Prompt + Validate
			}
			break;

		case GIFT_CARD:
			if(isCvvReqdForGiftCard() == PAAS_FALSE)
			{
				iCVVOption    = 7; //(Normal + Mandatory + No Prompt)
			}
			else
			{
				iCVVOption    = 0; //(Normal + Optioanl + Prompt)
			}

			if(isExpReqdForGiftCard() == PAAS_FALSE)
			{
				iExpiryOption = 13; //Normal + Optional + No Prompt + Skip Validate
			}
			else
			{
				iExpiryOption = 0; //Masked + Optional + Prompt + Validate
			}
			break;

		case EBT_CARD:
			if(isCvvReqdForEBTCard() == PAAS_FALSE)
			{
				iCVVOption    = 7; //(Normal + Mandatory + No Prompt)
			}
			else
			{
				iCVVOption    = 0; //(Normal + Optioanl + Prompt)
			}

			if(isExpReqdForEBTCard() == PAAS_FALSE)
			{
				iExpiryOption = 13; //Normal + Optional + No Prompt + Skip Validate
			}
			else
			{
				iExpiryOption = 0; //Masked + Optional + Prompt + Validate
			}
			break;
		case PAYACCOUNT:
			iCVVOption		  = 7; //No prompt
			break;
		default:

			break;
		}

		/* Form the message for the UI agent */
		sprintf(szXGCDMsg, "XGCD%c%s%c%d%c%d%c%d%c%s%c%d%c%s%c%d", FS,
						szPANTitle, FS, iMinPAN, FS, iMaxPAN, FS, iPANOption,
						FS,  szExpDtTitle, FS, iExpiryOption, FS, szCVVTitle,
						FS, iCVVOption);

		sprintf(szBuf, "%c%s%c", STX, szXGCDMsg, ETX);

		break;
	}

	debug_sprintf(szDbgMsg,"%s: Framed Packet [%s]", __FUNCTION__, szBuf);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: getCardDataUtil
 *
 * Description	: This API would set the Tracks of the card to be read.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE.
 * ============================================================================
 */
void getCardDataUtil(PAAS_BOOL bTrk1, PAAS_BOOL bTrk2, PAAS_BOOL bTrk3,
																char * szBuf)
{
	char	crdMsg[4]	= "";
	char	tempMsg[6]	= "";

	/* Atleast bTrk1 or bTrk2 must be specified */
	if(bTrk1 == PAAS_TRUE)
	{
		if(bTrk2 == PAAS_TRUE)
		{
			if(bTrk3 == PAAS_TRUE)
			{
				/* track 1 , track 2, track 3 */
				sprintf(crdMsg, "Q14");
			}
			else
			{
				/* track 1 and track 2 */
				sprintf(crdMsg, "Q13");
			}
		}
		else if(bTrk3 == PAAS_TRUE)
		{
			/* track 1 and track 3 */
			sprintf(crdMsg, "Q15");
		}
		else
		{
			/* Only track 1 */
			sprintf(crdMsg, "Q11");
		}
	}
	else if(bTrk2 == PAAS_TRUE)
	{
		/* track 1 is not present */
		if(bTrk3 == PAAS_TRUE)
		{
			/* track 2 and track 3 */
			sprintf(crdMsg, "Q16");
		}
		else
		{
			/* Only track 2 */
			sprintf(crdMsg, "Q12");
		}
	}

	sprintf(tempMsg, "%s%c", crdMsg, RS);
	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: addQRCodeInfoWrapper
 *
 * Description	: This API is used for framing QR code command XQRC
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE.
 * ============================================================================
 */
void addQRCodeInfoWrapper(char * pszTxt, int x, int y, int size, char * szBuf)
{
	char	tempMsg[1024*5]		= "";
	char	tmpCaption[1024*5]	= "";

	/* Convert string to UTF string */
	convStrToUTFEscStr(pszTxt, tmpCaption);
	sprintf(tempMsg, "XQRC%c\"%s\"%c%d%c%d%c%d%cFILENAME=%s%c", FS, tmpCaption, FS, x, FS, y, FS, size, FS, QRCODE_IMG_FILE_NAME,RS);

	strcat(szBuf, tempMsg);

	return;
}

/*
 * ============================================================================
 * End of file uiMsgBuilder.c
 * ============================================================================
 */
