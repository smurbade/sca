/******************************************************************
*                       uiMsgParser.c                      *
*******************************************************************
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "common/common.h"
#include "uiAgent/uiControl.h"
#include "uiAgent/emvAPIs.h"
#include "common/tranDef.h"

#include "bLogic/bLogicCfgDef.h"

#define PART_SIG_CAPTURED	-205

#define STB_EVENT		90
#define NFC_DATA_EVENT	60

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

typedef struct
{
	int		iSigSize;
	int		iBlkSize;
	int		iCurSize;
	char *	szTmpSigBuf;
}
TMPSIGN_STYPE, * TMPSIGN_PTYPE;

/* Extern functions declarations */
extern int getPinPadMode();
extern int parseNValidateEMVResp(char *, int, ALLRESP_PTYPE, int *, char *, int, char *);
extern char * getEmvCmdType(char *, char *, int *);
extern int getEmvReqCmd(char *);

/* Static functions declarations */
static int parseXGETVARResp(char *, XGETVAR_PTYPE);
static int parseXGPVResp(char *, PROPDTLS_PTYPE);
static int parseXEVTResp(char *, XEVT_PTYPE);
static int parseForTrackData(char *, CARD_TRK_PTYPE, PAAS_BOOL);
static int parseForManCardData(char *, MANUAL_PTYPE);
static int parseXVERResp(char *, SYSINFO_PTYPE);
static int parseXGETVOLResp(char *, VOLDTLS_PTYPE);
static int parsePINData(char *, PINDATA_PTYPE);
static int parseSignResp(char *, char *, SIGDATA_PTYPE);
static int parseXCONFIGResp(char *, XCONFIG_PTYPE);
static int initSignData(char *, TMPSIGN_PTYPE);
static int savePartialSignData(char *, TMPSIGN_PTYPE);

static char * getListEvtValues(char *, XEVT_PTYPE);
static char * getVidEvtValues(char *, XEVT_PTYPE);
static char * getImgEvtValues(char *, XEVT_PTYPE);
static char * getKeyPadEvtValues(char *, XEVT_PTYPE);
static char * getSmartCardEvtValues(char *, XEVT_PTYPE);
static char * getFACmdType(char *, char *, char *);
static char * getSTBEvtValues(char * , XEVT_PTYPE );

/*
 * ============================================================================
 * Function Name: parseUIAgentResp()
 *
 * Description	: This API will be used to parse the message recieved from the
 * 					UI agent.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int parseUIAgentResp(uchar * szUIResp, int iSize, UIRESP_PTYPE pstUIResp)
{
	int				rv				= SUCCESS;
	int				iLen			= 0;
	int 			iResCode		= -1;
	int				iStatus			= -1;
	int 			iCmdNo			= 1;
	int 			iFailedCmd		= 0;
	char			szCmd[2500]		= "";
	char			szTmp[10]		= "";
	char *			curPtr			= NULL;
	char *			curTmpPtr		= NULL;
	char *			nxtPtr			= NULL;
	char *			cmdPtr			= NULL;
	ALLRESP_PTYPE	pstResp			= NULL;
#ifdef DEBUG
	char			szDbgMsg[4096 + 512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	if(strlen((char *)szUIResp) < 4096)
	{
		debug_sprintf(szDbgMsg, "%s:%s: UIResp = [%s]", DEVDEBUGMSG, __FUNCTION__, szUIResp);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s:%s: UIResp Len = [%d]", DEVDEBUGMSG, __FUNCTION__, strlen((char *)szUIResp));
		APP_TRACE(szDbgMsg);
	}
#endif

	/* Initialize the UI response data */
	pstResp = &(pstUIResp->stRespDtls);
	memset(pstResp, 0x00, sizeof(ALLRESP_STYPE));

	curPtr = (char *) szUIResp;

	curTmpPtr = (char *) szUIResp; //Praveen_P1: Having one more pointer which will not skip the XBATCH

	/* Skip the XBATCH from the message */
	if((strstr(curPtr, "XBATCH")) != NULL)
	{
		curPtr += strlen("XBATCH") + 1; /* skips XBATCH<FS> */
	}

	memset(szCmd, 0x00, sizeof(szCmd));
	memset(szTmp, 0x00, sizeof(szTmp));

	/* Check FA command Type */
	nxtPtr = getFACmdType(curPtr, szCmd, curTmpPtr);
	iLen = strlen(szCmd);

#ifdef DEVDEBUG
	if(strlen(szCmd) < 2450)
	{
		debug_sprintf(szDbgMsg, "%s: Command [%s]", __FUNCTION__, szCmd);
		APP_TRACE(szDbgMsg);
	}
#endif

	if(strcmp(szCmd, "XEVT") == SUCCESS)
	{
		/* Skip the "XEVT<FS>" string and pass the rest of message for further
		 * parsing */
		rv = parseXEVTResp(nxtPtr + iLen + 1, &(pstResp->stXEvtInfo));
		/*
		 * If STB is not enabled, if we get STB event from the FA we should
		 * ignore the data to avoid situations we wait for some XEVT event
		 * but will get this XEVT
		 */
		if( (pstResp->stXEvtInfo.uiCtrlType == 90) && ( !isSTBLogicEnabled() ) )
		{
			debug_sprintf(szDbgMsg, "%s: Received STB event, Ignoring it since STBLogic is not enabled", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstUIResp->iStatus = UI_IGNORE;
			pstUIResp->uiRespType = UI_IGNORE;

			if(pstResp->stXEvtInfo.pszData != NULL)
			{
				free(pstResp->stXEvtInfo.pszData);
			}
		}
		else
		{
			pstUIResp->iStatus = rv;
			pstUIResp->uiRespType = UI_XEVT_RESP;
		}

	}
	else if(strcmp(szCmd, "XGPV") == SUCCESS)
	{
		rv = parseXGPVResp(nxtPtr + iLen + 1, &(pstResp->stPropInfo));

		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_XGPV_RESP;
	}
	else if(strcmp(szCmd, "81") == SUCCESS)
	{
		/* Skip "81." string and pass the rest of message for further parsing */
		rv = parseForTrackData(nxtPtr + iLen + 1, &(pstResp->stCrdTrkInfo),
																	PAAS_TRUE);
		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_CARD_RESP;
	}
	else if(strcmp(szCmd, "XVCL") == SUCCESS)
	{
		/* Skip the "XVCL<FS><Command><FS>" string and pass the rest of message
		 * for further parsing */

		curPtr = nxtPtr + iLen + 1; /* Point to begining of <Command> */
		nxtPtr = strchr(curPtr, FS); /* Point to <FS> char after <Command> */

		rv = parseForTrackData(nxtPtr + 1,&(pstResp->stCrdTrkInfo), PAAS_FALSE);
		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_VCL_RESP;
	}
	else if(strcmp(szCmd, "XGCD") == SUCCESS)
	{
		/* Skip the "XGCD<FS>" string and pass the rest of message for further
		 * parsing */
		rv = parseForManCardData(nxtPtr + iLen + 1, &(pstResp->stManualInfo));

		
		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_XGCD_RESP;
	}
	else if(strcmp(szCmd, "XVER") == SUCCESS)
	{
		/* Skip the "XVER<FS>" string and pass the rest of message for further
		 * parsing */
		rv = parseXVERResp(nxtPtr + iLen + 1, &(pstResp->stSysInfo));
		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_XVER_RESP;
	}
	else if(strcmp(szCmd, "XGETVOL") == SUCCESS)
	{
		/* Skip the "XGETVOL<FS>" string and pass rest of message for further
		 * parsing */
		rv = parseXGETVOLResp(nxtPtr + iLen + 1,&(pstResp->stVolInfo));
		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_VOL_RESP;
	}
	else if (strcmp(szCmd, "XSETVAR") == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: It's XSETVAR Response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Set config response received */
		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_XSETVAR_RESP;
	}
	else if (strcmp(szCmd, "XGETVAR") == SUCCESS)
	{
		/* Skip the "XVER<FS>" string and pass the rest of message for further
		 * parsing */
		rv = parseXGETVARResp(nxtPtr + iLen + 1, &(pstResp->stConfigInfo));
		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_XGETVAR_RESP;
	}
	else if (strcmp(szCmd, "XCONFIG") == SUCCESS)
	{
		/* Skip the "XCONFIG<FS>" string and pass the rest of message for further
		 * parsing */
		// MukeshS3: 29-April-16: Parsing XCONFIG for result code as well as response string.
		debug_sprintf(szDbgMsg, "%s: EMV Initialization Response [%s]", __FUNCTION__, nxtPtr);
		APP_TRACE(szDbgMsg);
#if 0
		if(nxtPtr != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: EMV Initialization Response [%s]", __FUNCTION__, nxtPtr);
			APP_TRACE(szDbgMsg);
			// CID-67266: 2-Feb-16: MukeshS3:
			// Putting whole parsing under NULL check
			nxtPtr = nxtPtr + iLen + 1;
			if(nxtPtr != NULL && *nxtPtr == '1')
			{
				curPtr = nxtPtr;
				pstUIResp->iStatus = 0;
				nxtPtr = strchr(curPtr, FS);
				if(nxtPtr != NULL)
				{
					curPtr = strchr(nxtPtr, '|');
					if(curPtr != NULL && *(curPtr+1) == '1')
					{
						pstUIResp->iStatus = 1;
					}
				}
			}
			else
			{
				pstUIResp->iStatus = 0;
			}
		}
#endif
		rv = parseXCONFIGResp(nxtPtr + iLen + 1, &(pstResp->stXConfigInfo));
		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_XCONFIG_RESP;
	}
	else if (strcmp(szCmd, "XSWT") == SUCCESS)
	{
		/* Skip the "XSWT<FS>" string and pass the rest of message for further
		 * parsing */
		nxtPtr = nxtPtr + iLen + 1;
		if(*nxtPtr == '1')
		{
			pstUIResp->iStatus = 1;
		}
		else
		{
			pstUIResp->iStatus = 0;
		}
		pstUIResp->uiRespType = UI_XSWT_RESP;
	}
	else if (strcmp(szCmd, "XCLOCK") == SUCCESS)
	{
		/* Skip the "XCLOCK<FS>" string and pass rest of message for further
		 * parsing */
		curPtr = nxtPtr + strlen(szCmd) + 1;
		if(curPtr[0] == '1')
		{
			pstUIResp->iStatus = SUCCESS;
		}
		else
		{
			pstUIResp->iStatus = ERR_INV_FLD_VAL;
		}
		pstUIResp->uiRespType = UI_XCLOCK_RESP;
	}
	else if (strcmp(szCmd, "M41") == SUCCESS)
	{
		/* Skip the "M41<FS>" string and pass rest of message for further
		 * parsing */
		curPtr = nxtPtr + strlen(szCmd) + 1;
		if(curPtr[0] == '1')
		{
			pstUIResp->iStatus = SUCCESS;
		}
		else
		{
			pstUIResp->iStatus = ERR_INV_FLD_VAL;
		}
		pstUIResp->uiRespType = UI_EMV_RESP;
	}
	else if( (strcmp(szCmd, "71" ) == SUCCESS) ||
			(strcmp(szCmd, "73" ) == SUCCESS) ||
			( (getEmvReqCmd(szTmp) == EMV_Z60 || getEmvReqCmd(szTmp) == EMV_Z62) && ( (strcmp(szCmd, "01" ) == 0) || (strcmp(szCmd, "05" ) == 0) ) ) ||
			(strcmp(szCmd, "EOT") == SUCCESS) )
	{
		/* Pass the message for further parsing */
		rv = parsePINData(nxtPtr, &(pstResp->stPINInfo));
		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_PIN_RESP;
	}
	else if( (strcmp(szCmd, "S01") == SUCCESS) ||
			 (strcmp(szCmd, "S02") == SUCCESS) )
	{
		/* Skip the "S01"/"S02" string and pass rest of message for further
		 * parsing */
		rv = parseSignResp(nxtPtr + strlen(szCmd),szCmd,&(pstResp->stSigInfo));
		if(rv == PART_SIG_CAPTURED)
		{
			pstUIResp->iStatus = UI_IGNORE;
		}
		else
		{
			pstUIResp->iStatus = rv;
			pstUIResp->uiRespType = UI_SIGN_RESP;
		}
	}
	else if( (strcmp(szCmd, "XIFM") == SUCCESS) ||
			 (strcmp(szCmd, "XSFM") == SUCCESS) ||
			 (strcmp(szCmd, "XSPV") == SUCCESS) )
	{
		debug_sprintf(szDbgMsg, "%s: No need of parsing the UI agent response",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_GENRL_RESP;//UI_IGNORE;
	}
	else if( (strcmp(szCmd, "XCLS") == SUCCESS) )
	{
		debug_sprintf(szDbgMsg, "%s: CLear Screen Command Response",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_XCLS_RESP;
	}
	else if( (strcmp(szCmd, "XALI") == SUCCESS) ||
			 (strcmp(szCmd, "XRLI") == SUCCESS) ||
			 (strcmp(szCmd, "XLCS") == SUCCESS) ||
			 (strcmp(szCmd, "XULI") == SUCCESS) ||
			 (strcmp(szCmd, "XCLB") == SUCCESS))
	{
		debug_sprintf(szDbgMsg, "%s: List Box Command Response Received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		curPtr = strchr(nxtPtr,FS);//CurPtr will point to the <FS> before the first command's response
		cmdPtr = szCmd;//CmdPtr will point to the first command itself
		//Only if there some failed command in this XBATCH response , only then we will try and parse
		while((curPtr != NULL) && (cmdPtr != NULL) && (strstr(curPtr,"0")))
		{
			curPtr = curPtr + 1;
			/* CurPtr is now pointing to the response.
			 * We record the failed responses only for XALI,XULI and XRLI
			 */
			if((curPtr[0] == '0') && ((strncmp(cmdPtr,"XALI",4) == SUCCESS) ||
					(strncmp(cmdPtr,"XRLI",4) == SUCCESS) || //We were not handling XRLI failures due to FA-261. But this is fixed in 4.5.2, hence checking for XRLI too.
					(strncmp(cmdPtr,"XULI",4) == SUCCESS) ||
					(strncmp(cmdPtr,"XSPV",4) == SUCCESS)))
			{
				//This is an indicator that the ith Command in this xbatch has failed.
				debug_sprintf(szDbgMsg, "%s:Command %d failed", __FUNCTION__,iCmdNo);
				APP_TRACE(szDbgMsg);
				pstResp->stXBatchInfo.lXbatchResp[iFailedCmd++] = iCmdNo;
				rv = ERR_XBATCH_FAILED;
			}
			else if((curPtr[0] == '0'))
			{
				/* In the LIST BOX Resp , we can also have some XSPV or XSFM. Currently we dont store 
				 * these failed responses.
				 */
				debug_sprintf(szDbgMsg, "%s:Command Failed but considered success %d ", __FUNCTION__,iCmdNo);
				APP_TRACE(szDbgMsg);
			}
			cmdPtr = curPtr + 2;
			//cmdPtr is made to point to the next command
			curPtr = strchr(curPtr,FS);
			//curPtr will point to <FS> before the next command's response.
			iCmdNo++;
		}
		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_LISTBOX_RESP;
	}
	else if(strcmp(szCmd, "XMBX") == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Message Box Command Response Received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		/* Skip the "XMBX<FS>" string and pass rest of message for further
		 * parsing */
		curPtr = nxtPtr + strlen(szCmd) + 1;
		if(curPtr[0] == '1')		// '1' For OK Button
		{
			pstUIResp->iStatus = SUCCESS;
		}
		else
		{
			pstUIResp->iStatus = ERR_INV_FLD_VAL;
		}
		pstUIResp->uiRespType = UI_XMBX_RESP;
	}
	else if(strcmp(szCmd, "XQRC") == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: QR Code Command Response Received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		// <STX>XQRC<FS>RetVal<FS>RetValString<ETX>
		/* Skip the "XQRC<FS>" string and pass rest of message for further
		 * parsing */
		curPtr = nxtPtr + strlen(szCmd) + 1;
		if(curPtr[0] == '1')	// '1' For SUCCESS
		{
			pstUIResp->iStatus = SUCCESS;
		}
		else
		{
			pstUIResp->iStatus = ERR_QRCODE_GENE_FAILED;
		}
		curPtr += 2; // skip return value & <FS>
		nxtPtr = strchr(curPtr, ETX);
		if(nxtPtr != NULL)
		{
			strncpy(pstResp->stQRCodeInfo.szRespText, curPtr, nxtPtr - curPtr);
		}
		pstUIResp->uiRespType = UI_XQRC_RESP;
	}
	else if(strcmp(szCmd, "R01") == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unsolicited EMV Card removal Command R01 Received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pstUIResp->uiRespType = UI_R01_RESP;
	}
	else if(strcmp(szCmd, "E01") == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Received response for Get Encryption Mode(E00)",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_GENRL_RESP;
	}
	else if(strcmp(szCmd, "85") == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Received Busy Respone from the UIAgent", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pstUIResp->uiRespType = UI_EMV_RESP;
		pstUIResp->iStatus	  = UI_AGENT_BUSY;
	}
	//Check for XPI command response
	else if( (nxtPtr = getEmvCmdType(curPtr, szCmd, &iResCode)) != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Got XPI response", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		
		rv = parseNValidateEMVResp(curPtr, iSize, pstResp, &iStatus, szCmd, iResCode, nxtPtr);
		
		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = iStatus;
	}
	else
	{
		/* Note: If none of the command matches, currently we set resp type as general resp received.
		 * The control will come here mostly in case when command response is none.
		 */

		debug_sprintf(szDbgMsg, "%s: UI XBATCH command response received", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pstUIResp->iStatus = rv;
		pstUIResp->uiRespType = UI_GENRL_RESP;
	}
	if(rv != FAILURE)
	{
		rv = SUCCESS;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getFACmdType
 *
 * Description	: This function is responsible for extracting the command type
 * 					from the response received from the UI agent
 *
 * Input Params	: 	szMsg -> input message to be parsed for the command
 * 					szCmd -> place holder for the parsed command string
 *
 * Output Params: none
 * ============================================================================
 */
static char * getFACmdType(char * szMsg, char * szCmd, char * szTempMsg)
{
	char *	nxtPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif



	/* Check UI agent command Type */
#if 0
	/*
	 * Praveen_P1: strstr giving problem to determine exact command type
	 * if some other response contains this data in between, it will
	 * be determined wrongly so chnaging strstr to strncmp
	 */
	if(	( (nxtPtr = strstr(szMsg, "73.")) != NULL) ||
		( (nxtPtr = strstr(szMsg, "71.")) != NULL) ||
		( (nxtPtr = strstr(szMsg, "81.")) != NULL)   )
	{
		memcpy(szCmd, nxtPtr, 2);
	}
#endif
	if(	( (strncmp(szMsg, "73.", 3)) == 0) ||
		( (strncmp(szMsg, "71.", 3)) == 0) ||
		( (strncmp(szMsg, "81.", 3)) == 0)   ) //Praveen_P1: Since we moved to XPI for PIN and Card data, we dont get this response now
	{
		nxtPtr = szMsg;
		memcpy(szCmd, nxtPtr, 2);
	}
	else if(szMsg[0] == '0' && szMsg[1] == '1') //XPI returns the 71 for the PIN command
	{
		memcpy(szCmd, "01", 2);
		nxtPtr = szMsg;
	}
	else if(szMsg[0] == '7' && szMsg[1] == '1') //XPI returns the 71 for the PIN command
	{
		memcpy(szCmd, "71", 2);
		nxtPtr = szMsg;
	}
#if 0
	else if( ( (nxtPtr = strstr(szMsg, "S02")) != NULL ) ||
			 ( (nxtPtr = strstr(szMsg, "S01")) != NULL ) )
	{
		memcpy(szCmd, nxtPtr, 3);
	}
#endif
	else if( ( (strncmp(szMsg, "S02", 3)) == 0 ) ||
			 ( (strncmp(szMsg, "S01", 3)) == 0 ) )
	{
		nxtPtr = szMsg;
		memcpy(szCmd, nxtPtr, 3);
	}
#if 0
	else if( (nxtPtr = strstr(szMsg, "XEVT")) != NULL )
	{
		memcpy(szCmd, nxtPtr, strlen("XEVT"));
	}
#endif
	else if( (strncmp(szMsg, "XEVT", 4)) == 0 )
	{
		nxtPtr = szMsg;
		memcpy(szCmd, nxtPtr, strlen("XEVT"));
	}
#if 0
	else if( (nxtPtr = strstr(szMsg, "XGETVOL")) != NULL )
	{
		memcpy(szCmd, nxtPtr, strlen("XGETVOL"));
	}
#endif
	else if( (strncmp(szMsg, "XGETVOL", 7)) == 0 )
	{
		nxtPtr = szMsg;
		memcpy(szCmd, nxtPtr, strlen("XGETVOL"));
	}
#if 0
	else if( (nxtPtr = strstr(szMsg, "XGPV")) != NULL )
	{
		memcpy(szCmd, nxtPtr, strlen("XGPV"));
	}
#endif
	else if( ((nxtPtr = strstr(szMsg, "XGPV")) != NULL ) &&
			( (strncmp(szTempMsg, "XBATCH", 6)) == 0 ) ) //Praveen_P1; since we are doing strstr of XGPV to avoid the condition where card response can contain this data, extra check XBATCH is done here
	{
		memcpy(szCmd, nxtPtr, strlen("XGPV"));
	}
	else if(strncmp(szMsg, "E01", 3) == 0)
	{
		nxtPtr = szMsg + 3;
		memcpy(szCmd, szMsg, 3);
	}
	else if(szMsg[0] == EOT)
	{
		/* We get <STX><EOT><ETX>if PINpad is not injected with proper key(s)*/
		memcpy(szCmd, "EOT", strlen("EOT"));
		nxtPtr = szMsg;
	}
	else if((nxtPtr = strchr(szMsg, FS)) != NULL)
	{
		memcpy(szCmd, szMsg, nxtPtr - szMsg);
		nxtPtr = szMsg;
	}
	else if((nxtPtr = strchr(szMsg, ETX)) != NULL)
	{
		nxtPtr = strchr(szMsg, ETX);
		memcpy(szCmd, szMsg, nxtPtr - szMsg);
		nxtPtr = szMsg;
	}	
	else
	{
		debug_sprintf(szDbgMsg, "%s:Its not FA command", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		//szCmd = NULL;
		nxtPtr = szMsg;	
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%c]", __FUNCTION__, *nxtPtr);
	APP_TRACE(szDbgMsg);

	return nxtPtr;
}

/*
 * ============================================================================
 * Function Name: parseXGETVARResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXGETVARResp(char * pszMsg, XGETVAR_PTYPE pstConfigInfo)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	int		iCnt			= 0;
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
	PAAS_BOOL	bPaas			= PAAS_TRUE;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(pstConfigInfo, 0x00, sizeof(XGETVAR_STYPE));

	/* Store the volume */
	cCurPtr = pszMsg;

	while(bPaas == PAAS_TRUE)
	{
		/*Storing the name of the config param*/
		cNxtPtr = strchr(cCurPtr, '=');
		memcpy(pstConfigInfo->szVarialbles[iCnt], cCurPtr, cNxtPtr - cCurPtr);

		/* Skipping the '=' character*/
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, FS);
		if(cNxtPtr == NULL)
		{
			cNxtPtr = strchr(cCurPtr, ETX);
			bPaas 	= PAAS_FALSE;
		}

		iLen = cNxtPtr - cCurPtr;
		if (iLen > 0)
		{
			pstConfigInfo->szCfgValues[iCnt] = (char*) malloc (iLen + 1);
			if(pstConfigInfo->szCfgValues[iCnt] == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstConfigInfo->szCfgValues[iCnt], 0x00, iLen + 1);
			memcpy(pstConfigInfo->szCfgValues[iCnt], cCurPtr, iLen);
		}
		/* Not freeing the memory here. Need to free at the caller function*/

		/* Skipping the FS character*/
		cCurPtr = cNxtPtr + 1;


		debug_sprintf(szDbgMsg, "%s: Config Param = [%s], Value = [%s]",
				__FUNCTION__, pstConfigInfo->szVarialbles[iCnt], pstConfigInfo->szCfgValues[iCnt]);
		APP_TRACE(szDbgMsg);

		iCnt++;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXGETVOLResp
 *
 * Description	: VDR: NEW
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXGETVOLResp(char * pszMsg, VOLDTLS_PTYPE pstVol)
{
	int		rv				= SUCCESS;
	char	szTmp[20]		= "";
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(pstVol, 0x00, sizeof(VOLDTLS_STYPE));

	/* Store the volume */
	cCurPtr = pszMsg;
	cNxtPtr = strchr(cCurPtr, FS);
	memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
	pstVol->iVol = atoi(szTmp);

	/* Store the bass */
	cCurPtr = cNxtPtr + 1;
	cNxtPtr = strchr(cCurPtr, FS);
	memset(szTmp, 0x00, sizeof(szTmp));
	memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
	pstVol->iBass = atoi(szTmp);

	/* Store the treble */
	cCurPtr = cNxtPtr + 1;
	cNxtPtr = strchr(cCurPtr, ETX);
	memset(szTmp, 0x00, sizeof(szTmp));
	memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
	pstVol->iTrbl = atoi(szTmp);

	debug_sprintf(szDbgMsg, "%s: Vol = [%d], bass = [%d], treble = [%d]",
					__FUNCTION__, pstVol->iVol, pstVol->iBass, pstVol->iTrbl);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXVERResp
 *
 * Description	: VDR: NEW
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXVERResp(char * pszMsg, SYSINFO_PTYPE pstSysInfo)
{
	int		rv				= SUCCESS;
	char *	curPtr			= NULL;
	char *	nxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * The response message from the UI agent is shown below
	 *
	 * <STX>XVER<FS>APP=3.0.0V3<FS>OS=MX200001/RFS20120625/<FS>
	 * PACKAGES=PaaSAdvertisements:1.0.0; PaaSApplication:3.0.0;
	 * frmAgent:3.0.0V1<FS>GUIMGR=3.05<FS>UNITID=169-000-806/12000000<FS>
	 * MODEL=MX925<FS>RFIDFW=LINUX CTLS ver.04.01.24<FS>RFIDLIB=02.23<FS>
	 * TCPIP=2.0.10a<FS> CLOCK=20130107094527<FS>SECUREPACKETS=AES128<ETX>
	 * <RS>
	 */

	debug_sprintf(szDbgMsg, "%s: XVER RespStr = [%s]", __FUNCTION__, curPtr);
	APP_TRACE(szDbgMsg);

	/* Initialize the data */
	memset(pstSysInfo, 0x00, sizeof(SYSINFO_STYPE));

	/* Get the formagent version */
	curPtr = strstr(pszMsg, "APP=");
	if(curPtr != NULL)
	{
		curPtr += 4; /* Skip "APP=" */
		nxtPtr = strchr(curPtr, FS);
		memcpy(pstSysInfo->szFAVer, curPtr, nxtPtr - curPtr);
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: FA version NOT FOUND", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* Get the OS version details */
	curPtr = strstr(pszMsg, "OS=");
	if(curPtr != NULL)
	{
		curPtr += 3; /* Skip "OS=" */
		nxtPtr = strchr(curPtr, FS);
		memcpy(pstSysInfo->szOSVer, curPtr, nxtPtr - curPtr);
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: OS version NOT FOUND", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* Get the RFID firmware version information */
	curPtr = strstr(pszMsg, "RFIDFW=");
	if(curPtr != NULL)
	{
		curPtr += strlen("RFIDFW=");
		nxtPtr = strchr(curPtr, FS);
		memcpy(pstSysInfo->szRFIDVer, curPtr, nxtPtr - curPtr);
	}
	else
	{
		debug_sprintf(szDbgMsg," %s: RFID FW version NOT FOUND", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	curPtr = strstr(pszMsg, "CLOCK=");
	if(curPtr != NULL)
	{
		curPtr += strlen("CLOCK=");
		nxtPtr = strchr(curPtr, FS);
		memcpy(pstSysInfo->szClock, curPtr, nxtPtr - curPtr);
	}
	else
	{
		debug_sprintf(szDbgMsg," %s: Clock NOT FOUND", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg,"%s: FA Ver [%s];OS Ver [%s]; RFID Ver [%s] Clock= %s",
					__FUNCTION__, pstSysInfo->szFAVer, pstSysInfo->szOSVer,
					pstSysInfo->szRFIDVer, pstSysInfo->szClock);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseForManCardData
 *
 * Description	: VDR: NEW
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseForManCardData(char * szMsg, MANUAL_PTYPE pstManual)
{
	int			rv				= SUCCESS;
	int			iUIRetVal		= 0;
	char		szTmp[10]		= "";
	char		szRestMsg[1000]	= "";
	char		szMonth[4]		= "";
	char		szYear[4]		= "";
	char *		curPtr			= NULL;
	char *		nxtPtr			= NULL;

	int			iLen			= 0;
	PAAS_BOOL 	bEparms			= PAAS_FALSE;
#ifdef DEBUG
	char	szDbgMsg[1026]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	if(strlen(szMsg) < 1000)
	{
		debug_sprintf(szDbgMsg, "%s:%s: Packet to be parsed = [%s]", DEVDEBUGMSG,
						__FUNCTION__, szMsg);
		APP_TRACE(szDbgMsg);
	}
#endif

	/* Initialize the structure */
	memset(pstManual, 0x00, sizeof(MANUAL_STYPE));

	/* Format of the string recieved by this function is as shown below
	 * RetVal<FS>RetValString<ETX>{lrc} */

	/* Get the return value sent by UI agent in the response message */
	nxtPtr = strchr(szMsg, FS);
	if(nxtPtr != NULL)
	{
		curPtr = nxtPtr + 1;

		memcpy(szTmp, szMsg, nxtPtr - szMsg);
		iUIRetVal = atoi(szTmp);

		/* Save the rest of the message */
		nxtPtr = strchr(curPtr, ETX);
		memcpy(szRestMsg, curPtr, nxtPtr - curPtr);
	}
	else // CID 67433 (#1 of 1): Explicit null dereferenced (FORWARD_NULL). T_RaghavendranR1. Returning from here if <FS> could not be found.
	{
		debug_sprintf(szDbgMsg, "%s: No <FS> in szMsg, Not Parsing it Further.", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	switch(iUIRetVal)
	{
	case 1:		/* SUCCESS */
		/* Get the PAN */
		nxtPtr = strchr(curPtr, PIPE);
		if(nxtPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: PIPE char is missing for parsing PAN", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_DEVICE_APP;
			return rv;
		}
		memcpy(pstManual->szPAN, curPtr, nxtPtr - curPtr);

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s PAN = [%s]", DEVDEBUGMSG, __FUNCTION__,pstManual->szPAN);
		APP_TRACE(szDbgMsg);
#endif
		/* Get the expiry date */
		curPtr = nxtPtr + 1;
		nxtPtr = strchr(curPtr, PIPE);
		if(nxtPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: PIPE char is missing while parsing for Expiry, so CVV is not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			/*
			 * <STX>XGCD<FS>1<FS>7777076240014964|1249<FS>ENC=0<ETX><BS>
			 * one example when CVV is not present, thats why looking for FS
			 * we always get expiry date even if is not entered because we set
			 */
			nxtPtr = strchr(curPtr, FS);
		}
/*
#if 0
		if(memcmp(curPtr, "1249", nxtPtr - curPtr) == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Exp Date = 1249 (GIFT CARD);not saving"
																, __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
#endif*/
		/* Except VSP encryption type, Expiry date format is returned is YYMM. So need to convert into MMYY format */
		//MukeshS3: For VSD & VSP encryption, exp details comes in MMYY format.For RSA encryption Expiry date format is YYMM
		//if (getEncryptionType() != VSP_ENC)
		if (getEncryptionType() == RSA_ENC)
		{
			memcpy(szMonth, curPtr+2, 2);
			memcpy(szYear, curPtr, 2);
			sprintf(pstManual->szExpDt, "%s%s", szMonth, szYear);
		}
		else	// VSP & VSD
		{
			memcpy(pstManual->szExpDt, curPtr, nxtPtr - curPtr);
		}
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s Expiry Date = [%s]", DEVDEBUGMSG,
								__FUNCTION__, pstManual->szExpDt);
		APP_TRACE(szDbgMsg);
#endif
		//}

		/* Get the CVV */
		if(*nxtPtr == FS) //if CVV is not present, nxtPtr will be pointing to FS while parsing for Expiry date
		{
			debug_sprintf(szDbgMsg, "%s: CVV is not present, so not saving", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			curPtr = nxtPtr + 1;
			nxtPtr = strchr(curPtr, FS);
			if(nxtPtr == NULL)
			{
				nxtPtr = strchr(curPtr, ETX);
			}
			if(memcmp(curPtr, "0000", nxtPtr - curPtr) == SUCCESS)
			{
				/* CVV Number is not required for Gift Cards */
	#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s: CVV Num=0000 (GIFT CARD);not saving"
													, DEVDEBUGMSG, __FUNCTION__);
				APP_TRACE(szDbgMsg);
	#endif
			}
			else
			{
				/*
				 * Praveen_P1: We dont want to send CVV to the SSI host for
				 * keyed transactions, so not copying it (in RSA Encryption mode)
				 * FA sending CVV in clear (its fixed later), to avoid complications
				 * we are not sending CVV
				 */
				// MukeshS3: For ADE encryption also, we are sending PAN|CVV in encblob, so not sending CVV separatly here
				//if(getEncryptionType() != RSA_ENC)
				if(getEncryptionType() == VSP_ENC)
				{
					memcpy(pstManual->szCVV, curPtr, nxtPtr - curPtr);
				}
	#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s: CVV Number = [%s]", DEVDEBUGMSG,
											__FUNCTION__, pstManual->szCVV);
				APP_TRACE(szDbgMsg);
	#endif
			}
		}
		/*Copying the encrypted blob data when rsa encryption is enabled*/
		if(getEncryptionType() == RSA_ENC)
		{
			curPtr = nxtPtr + 1;
			nxtPtr = strchr(curPtr, FS);
			if(nxtPtr != NULL)
			{
				memcpy(pstManual->szEncBlob, curPtr, nxtPtr - curPtr);
				debug_sprintf(szDbgMsg, "%s:%s: Encrypted blob received = [%s]", DEVDEBUGMSG,
											__FUNCTION__, pstManual->szEncBlob);
				APP_TRACE(szDbgMsg);

			}
		}
		else if(getEncryptionType() == VSD_ENC)
		{
			/* Get the VSD Blob data */
			nxtPtr = strstr(szRestMsg , "VSD=");
			if(nxtPtr != NULL)
			{
				nxtPtr += 4; /* Skipping "VSD=" */
				iLen = strlen(szRestMsg) - (nxtPtr - szRestMsg);
				if(iLen > 0)
				{
					parseVSDBlobDataResp(nxtPtr, iLen , NULL, NULL, pstManual, 4);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: VSD Encryption Blob data is not present", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		/* Get the expiry date in clear */
		nxtPtr = strstr(szRestMsg , "EXP=");
		if(nxtPtr != NULL)
		{
			nxtPtr += 4; /* Skipping "EXP=" */
			memcpy(pstManual->szExpClrDt, nxtPtr, 4);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: ExpDate not encrypted", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

	 	curPtr = szMsg;
		nxtPtr = strstr(curPtr, "EPARMS");
		if(nxtPtr != NULL)
		{
			curPtr = nxtPtr + 7; //curPtr points to first byte after EPARMS=
			nxtPtr = strchr(curPtr, ETX); //nextPtr points to ETX
			iLen = nxtPtr - curPtr;
			if(iLen > 0)
			{
				debug_sprintf(szDbgMsg, "%s: iLen = %d", __FUNCTION__, iLen);
				APP_TRACE(szDbgMsg);

				memset(pstManual->szEparms, 0x00, sizeof(pstManual->szEparms));
				memcpy(pstManual->szEparms, curPtr, iLen);

				debug_sprintf(szDbgMsg, "%s: Eparms value present ", __FUNCTION__);
				APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
				if(strlen(pstManual->szEparms) < 1000)
				{
					debug_sprintf(szDbgMsg, "%s:%s: EPARMS = [%s]", DEVDEBUGMSG,
							__FUNCTION__, pstManual->szEparms);
					APP_TRACE(szDbgMsg);
				}
#endif
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Eparms value not present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			bEparms = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Eparms value not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	case 0:		/* FAILURE for VCL CASE*/
				/* RSA encryption 0 means data is not encrypted
				 * So need to parse the info and pass it on
				 */
		if(getEncryptionType() == RSA_ENC)
		{
			/* Get the PAN */
			nxtPtr = strchr(curPtr, PIPE);
			if(nxtPtr != NULL)
			{
				memcpy(pstManual->szPAN, curPtr, nxtPtr - curPtr);

	#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s PAN = [%s]", DEVDEBUGMSG, __FUNCTION__,pstManual->szPAN);
				APP_TRACE(szDbgMsg);
	#endif
				/* Get the expiry date */
				curPtr = nxtPtr + 1;
				nxtPtr = strchr(curPtr, PIPE);

				/* Encryption type is rsa then the format of the expiry date returned is yymm*/
				memcpy(szMonth, curPtr+2, 2);
				memcpy(szYear, curPtr, 2);
				sprintf(pstManual->szExpDt, "%s%s", szMonth, szYear);

	#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s Expiry Date = [%s]", DEVDEBUGMSG,
						__FUNCTION__, pstManual->szExpDt);
				APP_TRACE(szDbgMsg);
	#endif

				/* Get the CVV */
				curPtr = nxtPtr + 1;
				nxtPtr = strchr(curPtr, FS);
				if(nxtPtr == NULL)
				{
					nxtPtr = strchr(curPtr, ETX);
				}
				if(memcmp(curPtr, "0000", nxtPtr - curPtr) == SUCCESS)
				{
					/* CVV Number is not required for Gift Cards */
		#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: CVV Num=0000 (GIFT CARD);not saving"
														, DEVDEBUGMSG, __FUNCTION__);
					APP_TRACE(szDbgMsg);
		#endif
				}
				else
				{

					/*
					 * Praveen_P1: This is when RSA encryption is not succesful
					 * in that case we are sending this field
					 */
					memcpy(pstManual->szCVV, curPtr, nxtPtr - curPtr);
		#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: CVV Number = [%s]", DEVDEBUGMSG,
												__FUNCTION__, pstManual->szCVV);
					APP_TRACE(szDbgMsg);
		#endif
				}

				/* Get the expiry date in clear */
				nxtPtr = strstr(szRestMsg , "EXP=");
				if(nxtPtr != NULL)
				{
					nxtPtr += 4; /* Skipping "EXP=" */
					memcpy(pstManual->szExpClrDt, nxtPtr, 4);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: ExpDate not encrypted", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				if(strcmp(szRestMsg, "USER CANCELLED ENTRY") == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: User cancelled ManEntry",__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = UI_CANCEL_PRESSED;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Some error [%s] occured",
							__FUNCTION__, szRestMsg);
					APP_TRACE(szDbgMsg);

					rv = ERR_DEVICE_APP;
				}
			}
		}
		else
		{
			/* Get the FAILURE description string from the message for VSP case only */
			
			if(strcmp(szRestMsg, "USER CANCELLED ENTRY") == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: User cancelled ManEntry",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = UI_CANCEL_PRESSED;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Some error [%s] occured",
						__FUNCTION__, szRestMsg);
				APP_TRACE(szDbgMsg);

				rv = ERR_DEVICE_APP;
			}
		}
		break;

	default:
		debug_sprintf(szDbgMsg,"%s: Unknown return value [%d]", __FUNCTION__,
																	iUIRetVal);
		APP_TRACE(szDbgMsg);

		rv = ERR_DEVICE_APP;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseForTrackData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseForTrackData(char * szMsg, CARD_TRK_PTYPE pstTrk, PAAS_BOOL
																	isOrigCard)
{
	int			rv				= SUCCESS;
	int			iLen			= 0;
	char *		curPtr			= NULL;
	char *		nxtPtr			= NULL;
	PAAS_BOOL	bEparms			= PAAS_FALSE;
	PAAS_BOOL	bTrk1			= PAAS_FALSE;
	PAAS_BOOL	bTrk2			= PAAS_FALSE;
	PAAS_BOOL	bTrk3			= PAAS_FALSE;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Initialize the data */
	memset(pstTrk, 0x00, sizeof(CARD_TRK_STYPE));
	curPtr = szMsg;

	// 81.{Track1}<FS>{Track2}<FS>{Track3}<FS>M<FS>ENC=0<ETX>

	/* Get the first track information */
	nxtPtr = strchr(curPtr, FS);
	iLen = nxtPtr - curPtr;
	if(iLen > 0)
	{
		memcpy(pstTrk->szTrk1, curPtr, iLen);
		bTrk1 = PAAS_TRUE;

		debug_sprintf(szDbgMsg, "%s: Track1 is present", __FUNCTION__);
		APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: TRACK 1 DATA = [%s]", DEVDEBUGMSG,
										__FUNCTION__, pstTrk->szTrk1);
		APP_TRACE(szDbgMsg);
#endif
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Track1 is NOT present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* get the second track data */
	curPtr = nxtPtr + 1;
	nxtPtr = strchr(curPtr, FS);
	iLen = nxtPtr - curPtr;
	if(iLen > 0)
	{
		memcpy(pstTrk->szTrk2, curPtr, iLen);
		bTrk2 = PAAS_TRUE;

		debug_sprintf(szDbgMsg, "%s: Track2 is present", __FUNCTION__);
		APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: TRACK 2 DATA = [%s]", DEVDEBUGMSG,
										__FUNCTION__, pstTrk->szTrk2);
		APP_TRACE(szDbgMsg);
#endif
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Track2 is NOT present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* get the third track data */
	curPtr = nxtPtr + 1;
	nxtPtr = strchr(curPtr, FS);
	iLen = nxtPtr - curPtr;
	if(iLen > 0)
	{
		memcpy(pstTrk->szTrk3, curPtr, iLen);
		bTrk3 = PAAS_TRUE;

		debug_sprintf(szDbgMsg, "%s: Track3 is present", __FUNCTION__);
		APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: TRACK 3 DATA = [%s]", DEVDEBUGMSG,
										__FUNCTION__, pstTrk->szTrk2);
		APP_TRACE(szDbgMsg);
#endif
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Track3 is NOT present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}



	//if( (isOrigCard == PAAS_TRUE) && ((bTrk1 | bTrk2 | bTrk3) == PAAS_TRUE) )
	/*
	 * We should not check isOrigCard in the above if condition
	 * for the VCL we pass false for this parameter
	 * so even though track data is present, it is entering the else condition
	 */
	if( (bTrk1 | bTrk2 | bTrk3) == PAAS_TRUE )
	{
		if(isOrigCard == PAAS_TRUE)
		{
			/* Some track information is present */
			/* Get the card source */
			curPtr = nxtPtr + 1;
			pstTrk->iCardSrc = * curPtr;

			/*Checking if eparms data is available*/
			curPtr = szMsg;
			nxtPtr = strstr(curPtr, "EPARMS");
			if(nxtPtr != NULL)
			{
				bEparms = PAAS_TRUE;
			}

			debug_sprintf(szDbgMsg, "%s: Checking for clear expiry", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Check for Clear expiry date and copy */
			curPtr = szMsg;
			/*
			 * Praveen_P1: We are looking for EXP, if the card holder name
			 * contains EXP then we will have problem
			 * thats why looking for EXP=
			 */
			//nxtPtr = strstr(curPtr, "EXP");
			nxtPtr = strstr(curPtr, "EXP=");
			if(nxtPtr != NULL)
			{
				curPtr = nxtPtr + 4; //curPtr points to first byte after EXP=
				if (bEparms == PAAS_TRUE)
				{
					nxtPtr = strchr(curPtr, FS); //nextPtr points to FS since eparms is der
				}
				else
				{
					nxtPtr = strchr(curPtr, ETX); //nextPtr points to ETX
				}

				//iLen = curPtr - nxtPtr;
				iLen = nxtPtr - curPtr;
				if(iLen > 0)
				{
					debug_sprintf(szDbgMsg, "%s: iLen = %d", __FUNCTION__, iLen);
					APP_TRACE(szDbgMsg);

					memset(pstTrk->szClrExpDt, 0x00, sizeof(pstTrk->szClrExpDt));
					memcpy(pstTrk->szClrExpDt, curPtr, iLen);

					debug_sprintf(szDbgMsg, "%s: EXP value present", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: EXP value not present", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: ExpDate not encr", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				memset(pstTrk->szClrExpDt, 0x00, sizeof(pstTrk->szClrExpDt));
			}
		}
		else
		{
			/*Checking if eparms data is available*/
			curPtr = szMsg;
			nxtPtr = strstr(curPtr, "EPARMS");
			if(nxtPtr != NULL)
			{
				bEparms = PAAS_TRUE;
			}
		}

		debug_sprintf(szDbgMsg, "%s: Checking for Eparms data", __FUNCTION__);
		APP_TRACE(szDbgMsg);


		if(bEparms == PAAS_TRUE)
		{
			curPtr = szMsg;
			nxtPtr = strstr(curPtr, "EPARMS");
			curPtr = nxtPtr + 7; //curPtr points to first byte after EPARMS=
			nxtPtr = strchr(curPtr, ETX); //nextPtr points to ETX
			iLen = nxtPtr - curPtr;
			if(iLen > 0)
			{
				debug_sprintf(szDbgMsg, "%s: iLen = %d", __FUNCTION__, iLen);
				APP_TRACE(szDbgMsg);

				memset(pstTrk->szEparms, 0x00, sizeof(pstTrk->szEparms));
				memcpy(pstTrk->szEparms, curPtr, iLen);

				debug_sprintf(szDbgMsg, "%s: Eparms value present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Eparms value not present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Eparms value not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* In case of VCL data, no need for checking the card information or
		 * fetching the card source or clear expiry date in this case */

		/*Looking for the rsa track data information when enc type is rsa_enc*/
		if(getEncryptionType() == RSA_ENC)
		{
			curPtr = szMsg;
			if(bTrk1 == PAAS_TRUE)
			{
				curPtr = szMsg;
				nxtPtr = strstr(curPtr, "RSA_T1");
				if(nxtPtr != NULL)
				{
					curPtr = nxtPtr + 7;//curPtr points to first byte after RSA_T1=
					nxtPtr = strchr(curPtr, FS); //nextPtr points to ETX
					iLen = nxtPtr - curPtr;
					if(iLen > 0)
					{
						debug_sprintf(szDbgMsg, "%s: iLen = %d", __FUNCTION__, iLen);
						APP_TRACE(szDbgMsg);

						memset(pstTrk->szRsaTrk1, 0x00, sizeof(pstTrk->szRsaTrk1));
						memcpy(pstTrk->szRsaTrk1, curPtr, iLen);

						debug_sprintf(szDbgMsg, "%s: RSA Track1 present", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: RSA Track1 not present", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
			}

			if(bTrk2 == PAAS_TRUE)
			{
				curPtr = szMsg;
				nxtPtr = strstr(curPtr, "RSA_T2");
				if(nxtPtr != NULL)
				{
					curPtr = nxtPtr + 7;//curPtr points to first byte after RSA_T1=
					nxtPtr = strchr(curPtr, FS); //nextPtr points to ETX
					iLen = nxtPtr - curPtr;
					if(iLen > 0)
					{
						debug_sprintf(szDbgMsg, "%s: iLen = %d", __FUNCTION__, iLen);
						APP_TRACE(szDbgMsg);

						memset(pstTrk->szRsaTrk2, 0x00, sizeof(pstTrk->szRsaTrk2));
						memcpy(pstTrk->szRsaTrk2, curPtr, iLen);

						debug_sprintf(szDbgMsg, "%s: RSA Track2 present", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: RSA Track2 not present", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
			}
		}
		rv = SUCCESS;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Track data not present", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_BAD_CARD;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXEVTResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXEVTResp(char * szMsg, XEVT_PTYPE pstXevt)
{
	int			rv				= SUCCESS;
	char		szTmp[100]		= "";
	char *		cCurPtr			= NULL;
	char *		cNxtPtr			= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(pstXevt, 0x00, sizeof(XEVT_STYPE));

		/* 1. Get the control type */
		cCurPtr = szMsg;
		cNxtPtr = strchr(cCurPtr, FS);
		memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
		pstXevt->uiCtrlType = atoi(szTmp);

		debug_sprintf(szDbgMsg, "%s: Ctrl Type = [%d]", __FUNCTION__, pstXevt->uiCtrlType);
		APP_TRACE(szDbgMsg);

		/*
		 * XEVT could come from the following events
		 * STB (Bin File Management) Event
		 * NFC Data Event
		 * Form Event
		 */
		if(pstXevt->uiCtrlType != STB_EVENT &&
		   pstXevt->uiCtrlType != NFC_DATA_EVENT) //If it Form Event then Control ID would be present
		{
			debug_sprintf(szDbgMsg, "%s: Received the Form Event", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* 2. Get the control ID */
			cCurPtr = cNxtPtr + 1;
			cNxtPtr = strchr(cCurPtr, FS);
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
			pstXevt->uiCtrlID = atoi(szTmp);

			debug_sprintf(szDbgMsg, "%s: Ctrl ID   = [%d]", __FUNCTION__,
																pstXevt->uiCtrlID);
			APP_TRACE(szDbgMsg);
		}


		switch(pstXevt->uiCtrlType)
		{
		case 0: /* KEYPAD Event */
			cNxtPtr = getKeyPadEvtValues(cNxtPtr + 1, pstXevt);
			break;

		case 2: /* Button Event */
		case 6: /* Checkbox Event */
		case 8: /* Animation Event */
			cCurPtr = cNxtPtr + 1;
			cNxtPtr = strchr(cCurPtr, FS);
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
			pstXevt->uiCtrlData = atoi(szTmp);

			debug_sprintf(szDbgMsg, "%s: Ctrl Data = [%d]", __FUNCTION__,
														pstXevt->uiCtrlData);
			APP_TRACE(szDbgMsg);

			break;

		case 3: /* Image Event */
			cNxtPtr = getImgEvtValues(cNxtPtr + 1, pstXevt);
			break;

		case 7: /* List Box Event */
			cNxtPtr = getListEvtValues(cNxtPtr + 1, pstXevt);
			break;

		case 17: /* Video Event */
			cNxtPtr = getVidEvtValues(cNxtPtr + 1, pstXevt);
			break;

		case 50: /* Smart Card Event */
			cNxtPtr = getSmartCardEvtValues(cNxtPtr + 1, pstXevt);
			break;

		case 14: /* Radio Button selection */
			/* Pass the Group ID */
			cCurPtr = cNxtPtr + 1;
			cNxtPtr = strchr(cCurPtr, FS);
			/*Pass the Caption */
			cCurPtr = cNxtPtr + 1;
			cNxtPtr = strchr(cCurPtr, FS);

			break;

		case 90: /* STB Event */
			cNxtPtr = getSTBEvtValues(cNxtPtr + 1, pstXevt);
			break;

		case 60: /* NFC Data Event */
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Get the form name (without any prefixes like 915_ or 925_) */
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, ETX);
		memcpy(pstXevt->szFrmName, cCurPtr + 4, cNxtPtr - cCurPtr - 4);

		debug_sprintf(szDbgMsg, "%s: Form Name = [%s]", __FUNCTION__,
															pstXevt->szFrmName);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXGPVResonse
 *
 * Description	: parses through the xgpv response returned by the ui agent and then
 * extracts the property value
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXGPVResp(char* msg, PROPDTLS_PTYPE stPropInfo)
{
	char* curptr = NULL;
	int propValue;

	#ifdef DEBUG
		char szDbgMsg[4096] = "";
	#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	curptr = msg;
	sscanf(curptr, "%d", &propValue);
	stPropInfo->propValue = propValue;

	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: getListEvtValues
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static char * getListEvtValues(char * szMsg, XEVT_PTYPE pstXevt)
{
	char	szTmp[100]	= "";
	char *	cCurPtr		= NULL;
	char *	cNxtPtr		= NULL;

	/* 1. get the list event */
	cCurPtr = szMsg;
	cNxtPtr = strchr(cCurPtr, FS);
	memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
	pstXevt->uiLBEvt = atoi(szTmp);

	/* 2. get the list index */
	cCurPtr = cNxtPtr + 1;
	cNxtPtr = strchr(cCurPtr, FS);
	memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
	pstXevt->uiLBIdx = atoi(szTmp);

	return cNxtPtr;
}

/*
 * ============================================================================
 * Function Name: getVidEvtValues
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static char * getVidEvtValues(char * szMsg, XEVT_PTYPE pstXevt)
{
	char	szTmp[100]		= "";
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	/* 1. get the video event */
	cCurPtr = szMsg;
	cNxtPtr = strchr(cCurPtr, FS);
	memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
	pstXevt->uiVidEvt = atoi(szTmp);

	/* 2. get the Video name*/
	cCurPtr = cNxtPtr + 1;
	cNxtPtr = strchr(cCurPtr, FS);
	memcpy(pstXevt->szVidName, cCurPtr, cNxtPtr - cCurPtr);

	debug_sprintf(szDbgMsg, "%s: Video Event = [%d]", __FUNCTION__,
															pstXevt->uiVidEvt);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Video Name  = [%s]", __FUNCTION__,
															pstXevt->szVidName);
	APP_TRACE(szDbgMsg);

	return cNxtPtr;
}

/*
 * ============================================================================
 * Function Name: getImgEvtValues
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static char * getImgEvtValues(char * szMsg, XEVT_PTYPE pstXevt)
{
	char	szTmp[100]		= "";
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	/* 1. get the Image X coordinate */
	cCurPtr = szMsg;
	cNxtPtr = strchr(cCurPtr, FS);
	memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
	pstXevt->uiImgXcord = atoi(szTmp);

	/* 2. get the Image Y coordinate */
	cCurPtr = cNxtPtr + 1;
	cNxtPtr = strchr(cCurPtr, FS);
	memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
	pstXevt->uiImgYcord = atoi(szTmp);

	debug_sprintf(szDbgMsg, "%s: Img X coordinate = [%d]", __FUNCTION__,
														pstXevt->uiImgXcord);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Img Y coordinate = [%d]", __FUNCTION__,
														pstXevt->uiImgYcord);
	APP_TRACE(szDbgMsg);

	return cNxtPtr;
}

/*
 * ============================================================================
 * Function Name: getKeypadEvtValues
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static char * getKeyPadEvtValues(char * szMsg, XEVT_PTYPE pstXevt)
{
	char	szTmp[100]		= "";
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	/* 1. get the keypad event */
	cCurPtr = szMsg;
	cNxtPtr = strchr(cCurPtr, FS);
	memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
	pstXevt->uiKeypadEvt = atoi(szTmp);

	/* 2. Get the keypad data */
	cCurPtr = cNxtPtr + 1;
	cNxtPtr = strchr(cCurPtr, FS);
	memcpy(pstXevt->szKpdVal, cCurPtr, cNxtPtr - cCurPtr);

	debug_sprintf(szDbgMsg, "%s: KeyPad Evt  = [%d]", __FUNCTION__,
														pstXevt->uiKeypadEvt);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: KeyPad Data = [%s]", __FUNCTION__,
															pstXevt->szKpdVal);
	APP_TRACE(szDbgMsg);

	return cNxtPtr;
}

/*
 * ============================================================================
 * Function Name: getSTBEvtValues
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static char * getSTBEvtValues(char * szMsg, XEVT_PTYPE pstXevt)
{
	char	szTmp[100]		= "";
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
	int		iDataSize		= -1;

#ifdef DEBUG
	char	szDbgMsg[2048]	= "";
#endif

	/* <STX>XEVT<FS>ControlType<FS>DataSize:[Data]<FS>FormName<ETX>
	 * ControlType = 90 indicates an STB event
	 * DataSize = Size (number of bytes) of Data field. Is �0� with no trailing Data field if no matching records are found
	 * Data = Matched records from BIN.db in the Record1<RS>Record2�. Each record contains the <GS> separated key=value pairs where key is the column name
	 */
	/* 1. Get the Data Size */
	cCurPtr = szMsg;
	cNxtPtr = strchr(cCurPtr, COLON);
	memset(szTmp, 0x00, sizeof(szTmp));
	memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
	iDataSize = atoi(szTmp);

	debug_sprintf(szDbgMsg, "%s: Data size %d", __FUNCTION__, iDataSize);
	APP_TRACE(szDbgMsg);

	if(iDataSize > 0)
	{
		pstXevt->pszData = (char *)malloc(sizeof(char) * (iDataSize+1));
		if(pstXevt->pszData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory Allocation Failed for Data", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return NULL;
		}

		memset(pstXevt->pszData, 0x00, iDataSize+1);

		/*Get the Data */
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, FS);
		memcpy(pstXevt->pszData, cCurPtr, cNxtPtr - cCurPtr);

		if(strlen(pstXevt->pszData) < 2000)
		{
			debug_sprintf(szDbgMsg, "%s: Data [%s]", __FUNCTION__, pstXevt->pszData);
			APP_TRACE(szDbgMsg);
		}

	}
	else //No Data in the response
	{
		cNxtPtr = cNxtPtr + 1;
		pstXevt->pszData = NULL;
	}


	return cNxtPtr;
}

/*
 * ============================================================================
 * Function Name: getSmartCardEvtValues
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static char * getSmartCardEvtValues(char * szMsg, XEVT_PTYPE pstXevt)
{
	char	szTmp[100]		= "";
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	/* 1. get the control data*/
	cCurPtr = szMsg;
	cNxtPtr = strchr(cCurPtr, FS);
	memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
	pstXevt->uiCtrlData = atoi(szTmp);
	
	debug_sprintf(szDbgMsg, "%s: Control Data = [%d]", __FUNCTION__,
												pstXevt->uiCtrlData);
	APP_TRACE(szDbgMsg);

	return cNxtPtr;
}

/*
 * ============================================================================
 * Function Name: parseSignResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static int parseSignResp(char * szMsg, char * szCmd, SIGDATA_PTYPE pstSigDtls)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
	static TMPSIGN_STYPE	stTmpSigDtls;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Initialize the data structure */
	memset(pstSigDtls, 0x00, sizeof(SIGDATA_STYPE));

	if(strcmp(szCmd, "S01") == SUCCESS)
	{
		rv = initSignData(szMsg, &stTmpSigDtls);
		if(rv == SUCCESS)
		{
			/* Initialized the structure for temporary storage of the signature
			 * dat, this will be populated upon subsequent parsing of S02
			 * messages coming from the UI agent */
			rv = PART_SIG_CAPTURED;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Sig capture CANCELLED", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else /* for "S02" command */
	{
		rv = savePartialSignData(szMsg, &stTmpSigDtls);
		if(rv == SUCCESS)
		{
			pstSigDtls->iSigLen = stTmpSigDtls.iSigSize;
			pstSigDtls->szSigBuf = stTmpSigDtls.szTmpSigBuf;

			/* Flush out temporary details */
			memset(&stTmpSigDtls, 0x00, sizeof(TMPSIGN_STYPE));
		}
		else if(rv == FAILURE)
		{
			/* De-allocate any allocated memory */
			free(stTmpSigDtls.szTmpSigBuf);

			/* Flush out temporary details */
			memset(&stTmpSigDtls, 0x00, sizeof(TMPSIGN_STYPE));
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: savePartialSignData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static int savePartialSignData(char * szMsg, TMPSIGN_PTYPE pstTmpSign)
{
	int		rv				= SUCCESS;
	int		iCurLen			= 0;
	char	szTmp[10]		= "";
	char *	curPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Message is of type "Muuuu{sigdata}" where
	 * M    = More indicator (Y - Yes, N - No)
	 * uuuu = actual size of signature data */

	while(1)
	{
		/* Get the length of the current signature packet */
		memcpy(szTmp, szMsg + 1, 4);
		iCurLen = atoi(szTmp);

		curPtr = szMsg + 5;

		debug_sprintf(szDbgMsg, "%s: cur pkt len [%d], len of stored sig [%d]",
								__FUNCTION__, iCurLen, pstTmpSign->iCurSize);
		APP_TRACE(szDbgMsg);

		/* Check if there is enough space in the temporary signature data for
		 * storing the signature packet */
		if( (pstTmpSign->iCurSize + iCurLen) > pstTmpSign->iSigSize )
		{
			debug_sprintf(szDbgMsg, "%s: Not enough space; %d reqd & %d left",
			__FUNCTION__, iCurLen, pstTmpSign->iSigSize - pstTmpSign->iCurSize);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Store the packet in the temporary place holder */
		memcpy(pstTmpSign->szTmpSigBuf + pstTmpSign->iCurSize, curPtr, iCurLen);
		pstTmpSign->iCurSize += iCurLen;

		if(szMsg[0] == 'N')
		{
			/* This was supposedly last packet to be receieved from the UI
			 * agent. The allocated buffer should be full by now */
			if(pstTmpSign->iCurSize != pstTmpSign->iSigSize)
			{
				debug_sprintf(szDbgMsg, "%s: Captured only %d;needed %d bytes",
					__FUNCTION__, pstTmpSign->iCurSize, pstTmpSign->iSigSize);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Sig capture complete",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			rv = PART_SIG_CAPTURED;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: initSignData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static int initSignData(char * szMsg, TMPSIGN_PTYPE pstTmpSign)
{
	int		rv				= SUCCESS;
	int		iSize			= 0;
	char	szTmp[6]		= "";
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[768]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	if(strlen(szMsg) < 700)
	{
		debug_sprintf(szDbgMsg,"%s:%s: SigDat [%s]",DEVDEBUGMSG,__FUNCTION__,szMsg);
		APP_TRACE(szDbgMsg);
	}
#endif

	/* Message is "xxxxxyyyyzerrr" where
	 * xxxxx = total number of bytes (5 bytes long)
	 * yyyy  = number of bytes per packet
	 * z     = signature file format
	 * e     = endianness (0 - BIG, 1 - LITTLE)
	 * rrr   = signature filter resolution */

	while(1)
	{
		/* Initialize the data */
		if(pstTmpSign->szTmpSigBuf != NULL)
		{
			free(pstTmpSign->szTmpSigBuf);
		}
		memset(pstTmpSign, 0x00, sizeof(TMPSIGN_STYPE));

		memcpy(szTmp, szMsg, 5);
		iSize = atoi(szTmp);
		if(iSize <= 0)
		{
			debug_sprintf(szDbgMsg,"%s: Signature not captured", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = UI_CANCEL_PRESSED;
			break;
		}

		/* Allocate memory to store the temporary signature data */
		tmpPtr = (char *) malloc(iSize);
		if(tmpPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(tmpPtr, 0x00, iSize);

		/* Get the signature block size */
		memset(szTmp, 0x00, sizeof(szTmp));
		memcpy(szTmp, szMsg + 5, 4);

		/* Populate the temporary signature place holder */
		pstTmpSign->iSigSize = iSize;
		pstTmpSign->szTmpSigBuf = tmpPtr;
		pstTmpSign->iBlkSize = atoi(szTmp);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parsePINData
 *
 * Description	: This API would parse the PIN data and stores PIN data into
 * 					the structure.
 * 	For Master/Session (MK=0):
 * 				RSP: <STX>71.0LL01PPPPPPPPPPPPPPPP<ETX>
 * 				-or-
 * 				<STX>71.<EOT><ETX> if CANCEL is pressed or other error
 * 				[e.g., PINpad is in DUKPT mode (MK=1),
 * 				PIN form display failure, too many PIN retries in short time]
 * 				-or-
 * 				<STX>71.<ETX> if ENTER is pressed with Null PIN
 * 				-or-
 * 				<STX><EOT><ETX> if PINpad is not injected with proper key(s)
 * 				-or-
 * 				<EOT> if PINpad is not injected with proper key(s), in MultiPay
 * 				or PP2000 emulation mode, and ACTX=0 (i.e., [(MULTIPAY=1 or
 * 				PP2000=1) and ACTX=0]

      For DUKPT (MK=1):

                RSP: <STX>73.00000KKKKKKKKKKKKKKKKKKKKPPPPPPPPPPPPPPPP<ETX>
                -or-
                <STX>73.<EOT><ETX> if CANCEL is pressed or other error
                [e.g., PINpad is in M/S mode (MK=0),
                PIN form display failure]
                -or-
                <STX>73.<ETX> if ENTER is pressed with Null PIN
                -or-
                <STX><EOT><ETX> if PINpad is not injected with proper key(s)
                -or-
                <EOT> if PINpad is not injected with proper key(s), in MultiPay
                or PP2000 emulation mode, and ACTX=0
                (i.e., [(MULTIPAY=1 or PP2000=1) and ACTX=0]

 * Input Params	:
 *
 * Output Params: None
 * ============================================================================
 */
static int parsePINData(char * szMsg, PINDATA_PTYPE pstPINDtls)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	char	szPINData[50]	= "";
	char *	curPtr			= NULL;
	char *	nxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		curPtr = szMsg;
		memset(pstPINDtls, 0x00, sizeof(PINDATA_STYPE));

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: PIN message = [%s]", DEVDEBUGMSG,
														__FUNCTION__, szMsg);
		APP_TRACE(szDbgMsg);
#endif

		/* Check whether response is of type <STX><EOT><ETX>. It will be incase
		 * PINpad is not injected with proper key(s) */
		/*
		 * Praveen_P1: Since we are using XPI for PIN entry it sends EOT
		 * for user canceled
		 * it gives 01 also when user entered the PIN and then canceled
		 */
		if( *curPtr == EOT || (szMsg[0] == '0' && szMsg[1] == '1') )
		{
			debug_sprintf(szDbgMsg,"%s: CANCEL pressed or Time out happened",
																			__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = UI_CANCEL_PRESSED;
			break;
		}

		if( (szMsg[0] == '0' && szMsg[1] == '5') )
		{
			debug_sprintf(szDbgMsg,"%s: BUTTON pressed on PIN Entry screen",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = UI_SIGN_BUTTON_PRESSED;
			break;
		}

		/* Skip the 71. or 73. or 710 command string */
		curPtr += 3;


		/* A response 71.<EOT> or 73.<EOT> would be generated in case of a
		 * timeout or user pressing CANCEL on the PIN entry screen */
		if(*curPtr == EOT)
		{
			debug_sprintf(szDbgMsg,"%s: CANCEL pressed or Time out happened",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = UI_CANCEL_PRESSED;
			break;
		}

		/* Get the PIN data */
		nxtPtr = strchr (curPtr, ETX);
		memcpy (szPINData, curPtr, nxtPtr - curPtr);

		/* A response 71. or 73. would be generated if a user pressed ENTER on
		 * the PIN entry screen without entering the PIN */
		if( (iLen = strlen(szPINData)) <= 0 )
		{
			debug_sprintf(szDbgMsg, "%s: NULL PIN Entered", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_NULL_PIN_ENTERED;
			break;
		}

		/* Get the PIN data from the response string */

		if(PAAS_FALSE == getPinPadMode()) /* MASTER KEY SESSION */
		{
			/* Skip the 0LL01 after 71. */
			curPtr = szPINData + 5;
			memcpy (pstPINDtls->szPIN, curPtr, iLen - 5);

#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s:%s PIN = [%s]", DEVDEBUGMSG,
											__FUNCTION__, pstPINDtls->szPIN);
			APP_TRACE(szDbgMsg);
#endif
		}
		else /* DUKPT SESSION */
		{
			/* Skip the 00000 after 73. */
			curPtr = szPINData + 0; //Praveen_P1: there are no zeros in XPI response

			/* Point to the begining of the PIN block */
			nxtPtr = szPINData + iLen - 16;
#if 0 //Praveen_P1: Lets not strip 'F' some processsor may need that data..lets give whatever we get from FA
			/* Get the KSN */
			while(*curPtr == 'F')
			{
				curPtr++;
			}
#endif
			memcpy(pstPINDtls->szKSN, curPtr, nxtPtr - curPtr);

			/* Get the PIN */
			memcpy(pstPINDtls->szPIN, nxtPtr, 16);

#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s:%s: PIN = [%s], KSN = [%s]",DEVDEBUGMSG,
							__FUNCTION__, pstPINDtls->szPIN, pstPINDtls->szKSN);
			APP_TRACE(szDbgMsg);
#endif
		}

		break;
	}

	debug_sprintf(szDbgMsg,"%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXGETVARResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXCONFIGResp(char * pszMsg, XCONFIG_PTYPE pstXConfigInfo)
{
	int		rv				= 0;	// 0 - failure; 1 - success;  in this function
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(pstXConfigInfo, 0x00, sizeof(XCONFIG_STYPE));

	/* Store the volume */
	cCurPtr = pszMsg;

	/* Format of XCONFIG response
	 * XCONFIG<FS><XCONFIG Status>[<FS><Config Status 1><FS><Config Status 2>]
	 * where XCONFIG Status is �<RetVal>|<RetValString>�	RetVal = 0 (failure) | 1 (success)
	 * Config Status (1/2) will be present only if XCONFIG Status is 1|Success and is comprised of �<Device ID>|<RetVal>|<RetValString>�
	 */

	if(cCurPtr != NULL && *cCurPtr == '1')	// success
	{
		cCurPtr = cCurPtr + 2;	// for <RetVal>|
		cNxtPtr = strchr(cCurPtr, FS);
		if(cNxtPtr != NULL)
		{
			// copy XConfig status string
			strncpy(pstXConfigInfo->szRespStatus, cCurPtr, cNxtPtr - cCurPtr);
			cCurPtr = cNxtPtr + 1;	// for <FS>
			cNxtPtr = strchr(cCurPtr, '|');	// skip <Device ID>
			cNxtPtr = cNxtPtr + 1;
			if(cNxtPtr != NULL && *(cNxtPtr) == '1')	//check <RetVal>
			{
				rv = 1;
			}
			else	// failure
			{
				// clear the previous RetValString & fill in new one
				if(cNxtPtr != NULL)
				{
					cCurPtr = cNxtPtr + 2;	// for <RetVal>|
					memset(pstXConfigInfo->szRespStatus, 0x00, sizeof(pstXConfigInfo->szRespStatus));
					cNxtPtr = strchr(cCurPtr, ETX);
					if(cNxtPtr != NULL)
					{	// copy XConfig status string
						strncpy(pstXConfigInfo->szRespStatus, cCurPtr, cNxtPtr - cCurPtr);
					}
				}
				rv = 0;
			}
		}
	}
	else if(cCurPtr != NULL)		// failure // T_RaghavendranR1 CID 83345 (#1 of 1): Dereference after null check (FORWARD_NULL)
	{
		cCurPtr = cCurPtr + 2;	// for <RetVal>|
		cNxtPtr = strchr(cCurPtr, ETX);
		if(cNxtPtr != NULL)
		{	// copy XConfig status string
			strncpy(pstXConfigInfo->szRespStatus, cCurPtr, cNxtPtr - cCurPtr);
		}
		rv = 0;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * End of file uiMsgParser.c
 * ============================================================================
 */
