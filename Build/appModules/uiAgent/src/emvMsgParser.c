/******************************************************************
*                       emvMsgParser.c                      	 *
*******************************************************************
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                               *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "common/common.h"
#include "common/utils.h"
#include "uiAgent/emv.h"
#include "uiAgent/emvConstants.h"
#include "common/tranDef.h"
#include "uiAgent/uiControl.h"
#include "uiAgent/uiCfgDef.h"
#include "bLogic/bLogicCfgDef.h"
#include "bLogic/blAPIs.h"
#include "common/tranDef.h"
#include "uiAgent/emvAPIs.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

//Extern Function Declaration
extern int handleEMVErrCode(int);
extern char * getEmvCmdType(char *, char *, int *);
extern int getEmvReqCmd(char *);

//Static function declarations
//static int parseForCardPresenceReq(char *);
//static int parseForCardRemoveReq(char *);
static int parseEmvVerResp(char *, EMVDTLS_PTYPE);
static int parseEmvMacResp(char *, EMVDTLS_PTYPE);
static int parseEmvCardResp(char *, int, int, EMVDTLS_PTYPE);
static int parseEmvSwipeCardResp(unsigned char *, int, CARD_TRK_PTYPE);
static int parseEmvCTLSError(unsigned char *, char *);
static int parseCardRespForClrExpDate(unsigned char *, int, char *);
static int parseEmvSwipedS20CardResp(char *, int, CARD_TRK_PTYPE);
//static int parseU01Response(unsigned char *, int , EMVDTLS_PTYPE , int );
static int storeTagValue(char *, char *, int, int, EMVDTLS_PTYPE, char *, char *, char *, char [64][128]);
static int storeCardData( char *, unsigned char *, int ,
								CARD_TRK_PTYPE, int);
static int parseEmvEParamsDataResp(char *, int , CARD_TRK_PTYPE , EMVDTLS_PTYPE, int);
static int parseEmvCipheredDataResp(char *, int , CARD_TRK_PTYPE , EMVDTLS_PTYPE, int);
static int parseApplePayDataResp(unsigned char *,int , CARD_TRK_PTYPE , EMVDTLS_PTYPE, int);
static int search9F18TagandModifyLength(char * , int *, char ** , int * , int *, unsigned char * , int , char ** , char **);
extern void Char2Hex(char *, char *, int);
extern int storeTagsForEMVCTLS(char *, char *, char [64][128]);
#ifdef DEVDEBUG
static void printEMVResp(EMVDTLS_PTYPE);
#endif

static char *tagOneByte[]		= {	"C2", "C3", "94","4F","82","5A","89","8A","84","42","91","95","57","9A","9B","9C","50", "8E", NULL};

static char *tagTwoByte_5F[]	= {"5F25","5F24","5F34","5F20","5F2D","5F30","5F2A", NULL};

static char *tagTwoByte_9F[]	= { "9F02","9F03","9F26","9F06","9F36","9F09","9F34","9F27",
									"9F1E","9F10","9F39","9F38","9F33","9F1A","9F35","9F3C",
									"9F41","9F21","9F37","9F5B","9F7C","9F12","9F53","9F6E",
									"9F66","9F56","9F07","9F0D","9F0E","9F0F", "9F08", "9F11",  NULL};

static char *tagThreeByte[]		= {	"9FA005","9FA006","9FA007","9FA008", "9FA00B", "9FA00A", NULL};


/*
 * ============================================================================
 * Function Name: parseNValidateEMVResp
 *
 * Description	: This function would parse and validates the XPI response.
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int parseNValidateEMVResp(char * szXPIResp, int iLen, ALLRESP_PTYPE	pstResp, int *piStatus, char *szCmd, int iRespCode,char *nxtPtr)
{
	int				rv				= SUCCESS;
	int				iStatus			= -1;
	char 			szReqCmd[3+1]	= "";
	char *			curPtr			= NULL;
	char			szTmp[5]		= "";
	EMVDTLS_STYPE	stEmvDtls;

#ifdef DEBUG
	char			szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	APP_TRACE("parseNValidateEMVResp: Got EMV Response");
	APP_TRACE_EX(szXPIResp, iLen);
#endif
	
	//Reset the EMV details structure.
	memset(&stEmvDtls, 0x00, sizeof(stEmvDtls));

	//Assign message
	curPtr = szXPIResp;


	//Copy EMV Response command name
	memcpy(stEmvDtls.szEMVRespCmd, szCmd, 3);

	getEmvReqCmd(szReqCmd);

	//Copy EMV Requested command name
	strcpy(stEmvDtls.szEMVReqCmd, szReqCmd);
	
	//Get EMV command type
//	nxtPtr = getEmvCmdType(curPtr, szCmd, &iRespCode);

	while(1)
	{
		if((!strcmp(szCmd, EMV_C31_RESP)) ||
		   (!strcmp(szCmd, EMV_C33_RESP)) ||
		   (!strcmp(szCmd, EMV_C34_RESP)) ||
		   (!strcmp(szCmd, EMV_C36_RESP)) )
		{
			debug_sprintf(szDbgMsg, "%s: Its [C] command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(strcmp(szCmd, EMV_C31_RESP) == SUCCESS)
			{
				setCardReadEnabled(PAAS_FALSE);
			}
			switch(iRespCode)
			{
				case EMV_CMD_SUCCESS:
					debug_sprintf(szDbgMsg, "%s: %s returned [SUCCESS]", __FUNCTION__, szCmd);
					APP_TRACE(szDbgMsg);
					//AJAYS2: If we did not got POS Entry, it means card has been inserted and if we got 9F39,
					//card source will be overwritten
					if(!strcmp(szCmd, EMV_C31_RESP))
					{
						stEmvDtls.iCardSrc = CRD_EMV_CT;
					}
					
					rv = parseEmvCardResp(nxtPtr+7, iLen-9, 0, &stEmvDtls);
					if(rv == SUCCESS)
					{
						memcpy(stEmvDtls.szEMVRespCode, nxtPtr + 3, 2);
						memcpy(&pstResp->stEmvDtls, &stEmvDtls, SIZE_EMVDTLS);
						#ifdef DEVDEBUG
							printEMVResp(&stEmvDtls);
						#endif
					}
					
					iStatus = UI_EMV_RESP;
					break;		

				case EMV_CARD_WAS_SWIPED:
					debug_sprintf(szDbgMsg, "%s:[CARD SWIPED on %s]", __FUNCTION__, szCmd);
					APP_TRACE(szDbgMsg);

					rv = parseEmvSwipeCardResp((unsigned char *)nxtPtr+7, iLen-9, 
														&(pstResp->stCrdTrkInfo));

					iStatus = UI_CARD_RESP;					
					break;	

				case EMV_ERROR_CTLS:
					debug_sprintf(szDbgMsg, "%s:[EMV CTLS ERROR on %s]", __FUNCTION__, szCmd);
					APP_TRACE(szDbgMsg);

					memset(szTmp, 0x00, sizeof(szTmp));
					rv = parseEmvCTLSError((unsigned char *)nxtPtr, szTmp);
					if(rv == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: EMV CTLS Extended Error Code [%s]", __FUNCTION__, szTmp);
						APP_TRACE(szDbgMsg);

						if(strcasecmp(szTmp, "02") == 0)
						{
							memcpy(stEmvDtls.szEMVRespCode, "310", 3);
							debug_sprintf(szDbgMsg, "%s: Got EMV CTLS Extended Error Code 02", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
						else
						{
							memcpy(stEmvDtls.szEMVRespCode, nxtPtr + 3, 2);
						}
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error While Parsing EMV CTLS Error C3131", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}

					memcpy(&pstResp->stEmvDtls, &stEmvDtls, SIZE_EMVDTLS);
					iStatus = UI_EMV_RESP;
					
					break;
				default:
					/* Do nothing */
					debug_sprintf(szDbgMsg, "%s: EMV returned [Error=%d]", 
													__FUNCTION__, iRespCode);
					APP_TRACE(szDbgMsg);			
					
					//rv = iRespCode;
					rv = SUCCESS;

					memcpy(stEmvDtls.szEMVRespCode, nxtPtr + 3, 2);
					memcpy(&pstResp->stEmvDtls, &stEmvDtls, SIZE_EMVDTLS);
					
					iStatus = UI_EMV_RESP;
					break;
			}			
			break;		
		}
		else if(!strcmp(szCmd, EMV_C20_RESP))
		{
			if(iRespCode ==0)
			{
				strncpy(szTmp, nxtPtr + 5, 3);
				iLen = atoi(szTmp);
				memcpy(stEmvDtls.szTagValue, nxtPtr + 8, iLen*2);
			}
			memcpy(stEmvDtls.szEMVRespCode, nxtPtr + 3, 2);
			memcpy(&pstResp->stEmvDtls, &stEmvDtls, SIZE_EMVDTLS);
			debug_sprintf(szDbgMsg, "%s: EMV returned [C20] iLen %d szTags %s", __FUNCTION__, iLen, stEmvDtls.szEMVTags);
			APP_TRACE(szDbgMsg);
			rv = iRespCode;
			iStatus = UI_EMV_RESP;
			break;
		}
		else if(!strcmp(szCmd, EMV_C19_RESP))
		{
			rv = iRespCode;
			memcpy(stEmvDtls.szEMVRespCode, nxtPtr + 3, 2);
			memcpy(&pstResp->stEmvDtls, &stEmvDtls, SIZE_EMVDTLS);
			debug_sprintf(szDbgMsg, "%s: EMV returned [C19] %s", __FUNCTION__, stEmvDtls.szEMVRespCode);
			APP_TRACE(szDbgMsg);
			iStatus = UI_EMV_RESP;
			break;
		}
		else if(!strcmp(szCmd, EMV_U02_REQ))
		{
			if(iRespCode == 0)
			{
				rv = parseEmvCardResp(nxtPtr+3, iLen-5, 0, &stEmvDtls);
				debug_sprintf(szDbgMsg, "%s: Its U02 command received from XPI with Unsolicited response", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Its U02 command received from XPI[Error RespCode=%d]",
																	__FUNCTION__,iRespCode);
				APP_TRACE(szDbgMsg);
				rv = iRespCode;
			}
#ifdef DEVDEBUG
	printEMVResp(&stEmvDtls);
#endif

			memcpy(stEmvDtls.szEMVRespCode, nxtPtr + 3, 2);
			//Copy xpi resp data
			memcpy(&pstResp->stEmvDtls, &stEmvDtls, SIZE_EMVDTLS);

			if(stEmvDtls.stEMVCardStatusU02.bSTBDataReceived == PAAS_TRUE)
			{
				setCardReadEnabled(PAAS_FALSE);
			}

			iStatus = UI_EMV_RESP;
			break;
		}
		else if(!strcmp(szCmd, EMV_U01_REQ) && iRespCode == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Its U01 command Request received from XPI", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(isEmvKernelSwitchingAllowed())
			{
			//	rv = parseU01Response((unsigned char *)nxtPtr + 5, iLen-7, &stEmvDtls, 0);
				memcpy(stEmvDtls.szEMVRespCode, "00", 2);

				//Copy xpi resp data
				memcpy(&pstResp->stEmvDtls, &stEmvDtls, SIZE_EMVDTLS);

				iStatus = UI_EMV_RESP;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: EMV Kernel Switching is NOt Allowed. Some Configuration ERROR", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
			}
			break;
		}
		else if(!strcmp(szCmd, EMV_U00_REQ))
		{
			if(iRespCode == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Its U00 command received from XPI", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//TODO: AJAYS2: Need to Parse U00 req and send back U00 Resp to XPI
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Its U00 command received from XPI[Error RespCode=%d]",
																	__FUNCTION__,iRespCode);
				APP_TRACE(szDbgMsg);
				rv = iRespCode;
			}

			memcpy(stEmvDtls.szEMVRespCode, nxtPtr + 3, 2);
			//Copy xpi resp data
			memcpy(&pstResp->stEmvDtls, &stEmvDtls, SIZE_EMVDTLS);

			iStatus = UI_EMV_RESP;
			break;
		}
		else if(!strcmp(szCmd, EMV_C25_RESP))
		{
			if(iRespCode == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Its C25 command returned SUCCESS", __FUNCTION__);
				APP_TRACE(szDbgMsg);			
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Its C25 command Failed[Error RespCode=%d]", __FUNCTION__,
																					iRespCode);
				APP_TRACE(szDbgMsg);
				rv = iRespCode;
				break;
			}

			memcpy(stEmvDtls.szEMVRespCode, nxtPtr + 3, 2);
			//Copy xpi resp data
			memcpy(&pstResp->stEmvDtls, &stEmvDtls, SIZE_EMVDTLS);

			
			iStatus = UI_EMV_RESP;			
			break;			
		}		
		else if(!strcmp(szCmd, EMV_I02_RESP))
		{
			if(iRespCode == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Its I02 command, Card removed successfullly", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Its I02 command Failed[Error RespCode=%d]", __FUNCTION__,
																					iRespCode);
				APP_TRACE(szDbgMsg);
				rv = iRespCode;
				break;
			}
			//Copy xpi resp data
			memcpy(&pstResp->stEmvDtls, &stEmvDtls, SIZE_EMVDTLS);

			iStatus = UI_EMV_RESP;			
			break;			
		}
		else if(!strcmp(szCmd, EMV_I05_RESP))
		{
			if(iRespCode == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Its I05 command, Card NOT Present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = CARD_NOT_PRESENT;
			}
			else if(iRespCode == 1)
			{
				debug_sprintf(szDbgMsg, "%s: Its I05 command Card Present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = CARD_PRESENT;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Its I05 command Failed[Error RespCode=%d]", __FUNCTION__,
																					iRespCode);
				APP_TRACE(szDbgMsg);
				rv = iRespCode;
			}

			iStatus = UI_EMV_RESP;
			break;
		}
		else if( !strcasecmp(szReqCmd, "S95") )
		{
			if(iRespCode == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Parsing for EMV(XPI app) version request[Cmd=S95]", __FUNCTION__);
				APP_TRACE(szDbgMsg);

			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: We send S95 command and got [Error RespCode=%d]", __FUNCTION__,
																					iRespCode);
				APP_TRACE(szDbgMsg);
				rv = iRespCode;
				break;
			}
			/* Skip the "00" string and pass the rest of message for further
			 * parsing */
			rv = parseEmvVerResp(nxtPtr + 2, &stEmvDtls);

			//Copying request command name in resp cmd if there is no resp command
			memcpy(&stEmvDtls.szEMVRespCmd, szReqCmd, 3);
			//Copy xpi resp data 
			memcpy(&pstResp->stEmvDtls, &stEmvDtls, SIZE_EMVDTLS);
			
			iStatus = UI_EMV_RESP;			
			break;
		}
		else if( !(strcasecmp(szReqCmd, "S66")) )
		{
			if(iRespCode == 0)
			{
				debug_sprintf(szDbgMsg, "%s: parsing for EMV(XPI app) MAC request[Cmd=S66]", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: We send S66 command and got [Error RespCode=%d]", __FUNCTION__,
																					iRespCode);
				APP_TRACE(szDbgMsg);
				rv = iRespCode;
				break;
			}

			/* Skip the "00" string and pass the rest of message for further
			 * parsing */
			rv = parseEmvMacResp(nxtPtr + 2, &stEmvDtls);
			
			//Copying request command name in resp cmd if there is no resp command
			memcpy(&stEmvDtls.szEMVRespCmd, szReqCmd, 3);
			//Copy xpi resp data 
			memcpy(&pstResp->stEmvDtls, &stEmvDtls, SIZE_EMVDTLS);

			iStatus = UI_EMV_RESP;			
			break;
		}
		else if (!strcasecmp(szReqCmd, "D01"))
		{
			debug_sprintf(szDbgMsg, "%s: Parsing D01 Response Response Code:%d", __FUNCTION__,iRespCode);
			APP_TRACE(szDbgMsg);

			rv = iRespCode;
			iStatus = UI_EMV_RESP;
			break;
		}
		else if(!(strcasecmp(szReqCmd, "S20")))
		{
			if(iRespCode == 0)
			{
				//Copy Card Source here(POS Entry Mode)
				rv = parseEmvSwipedS20CardResp( nxtPtr+2, iLen-4, &(pstResp->stCrdTrkInfo));
				pstResp->stEmvDtls.iCardSrc = pstResp->stCrdTrkInfo.iCardSrc;
				debug_sprintf(szDbgMsg, "%s: Card Swipe S20 Command Response Success 00", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(iRespCode == 3)
			{
				rv = UI_CANCEL_PRESSED;
				debug_sprintf(szDbgMsg, "%s: We send S20 command and User Cancelled [Error RespCode=%d]", __FUNCTION__,
																					iRespCode);
				APP_TRACE(szDbgMsg);
			}
			else if(iRespCode == 2)
			{
				rv = ERR_USR_TIMEOUT;
				debug_sprintf(szDbgMsg, "%s: We send S20 command and User Timeout Occured [Error RespCode=%d]", __FUNCTION__,
																					iRespCode);
				APP_TRACE(szDbgMsg);
			}
			else //Treating any other error as bad card.
			{
				rv = ERR_BAD_CARD;
				debug_sprintf(szDbgMsg, "%s: We send S20 command and got [Error RespCode=%d]", __FUNCTION__,
																					iRespCode);
				APP_TRACE(szDbgMsg);
			}

			setCardReadEnabled(PAAS_FALSE);

			iStatus = UI_CARD_RESP;			
			break;			
		}
		else if(!(strcasecmp(szReqCmd, "D41")))
		{
			if(iRespCode == 0 && iLen-7 > 0 )
			{
				rv = parseApplePayDataResp((unsigned char*)nxtPtr+7, iLen-9 , &(pstResp->stCrdTrkInfo), NULL, 4);

				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s:Error in Parsing D41 Command", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else if(iRespCode == 4)
			{
				debug_sprintf(szDbgMsg, "%s:No VAS Data present in phone", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = ERR_NO_MATCHING_PROV_PASS;
			}
			else if(iRespCode == 8)
			{
				rv = UI_CANCEL_PRESSED;
				debug_sprintf(szDbgMsg, "%s: We send D41 command and User Cancelled [Error RespCode=%d]", __FUNCTION__, iRespCode);
				APP_TRACE(szDbgMsg);
			}
			setCardReadEnabled(PAAS_FALSE);

			iStatus = UI_CARD_RESP;
			break;
		}
		else if(!strcmp(szCmd, EMV_E11_RESP))
		{
			debug_sprintf(szDbgMsg, "%s: EMV command[E11] resp", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iStatus = UI_EMV_RESP;
			break;
		}
		else if( (!strcmp(szCmd, EMV_D11_RESP))|| (!strcmp(szCmd, EMV_D12_RESP)) ||
				 (!strcmp(szCmd, EMV_D13_RESP))|| (!strcmp(szCmd, EMV_D14_RESP)) ||
				(!strcmp(szCmd, EMV_D15_RESP)) || (!strcmp(szCmd, EMV_D16_RESP)) ||
				(!strcmp(szCmd, EMV_D17_RESP)) || (!strcmp(szCmd, EMV_D18_RESP)) ||
				(!strcmp(szCmd, EMV_D19_RESP)) || (!strcmp(szCmd, EMV_D20_RESP)) ||
				(!strcmp(szCmd, EMV_D25_RESP)) || (!strcmp(szCmd, EMV_D32_RESP)) )
		{
			switch(iRespCode)
			{
				case EMV_CMD_SUCCESS:
					debug_sprintf(szDbgMsg, "%s: D command %s returned [SUCCESS]", __FUNCTION__,
																				szCmd);
					APP_TRACE(szDbgMsg);				
					
					rv = SUCCESS;
					break;

				default:
					/* Error returned */
					debug_sprintf(szDbgMsg, "%s: D command %s returned error code[%d]", __FUNCTION__,
																		szCmd, iRespCode);
					APP_TRACE(szDbgMsg);
					
					rv = iRespCode;
					break;					
			}					
			iStatus = UI_EMV_RESP;			
			break;
		}		
		else
		{
			debug_sprintf(szDbgMsg, "%s: XPI cmd [%s] returned unknown resp code [%d]",
													__FUNCTION__, szReqCmd, iRespCode);
			APP_TRACE(szDbgMsg);
			
			rv = iRespCode;
			iStatus = UI_EMV_RESP;			
			break;
		}
	}	
	//Copy Response code into the structure
	*piStatus = iStatus;
		
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

#ifdef DEVDEBUG
/*
 * ============================================================================
 * Function Name: printEMVResp
 *
 * Description	: This function would display EMV response.
 *
 * Input Params	: @szMsg	   -> Input message to be displayed.
 *					
 * Output Params: None
 * ============================================================================
 */
static void printEMVResp(EMVDTLS_PTYPE pstEmvDtls)
{
#ifdef DEBUG
	char	szDbgMsg[2048]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__, 
					"*******************XPI Resp Data*********************");
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"AID_4F",pstEmvDtls->stEmvAppDtls.szAID);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_PAN_SEQ_NUM_5F34", pstEmvDtls->stEmvAppDtls.szAppSeqNum);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_LABEL_50",pstEmvDtls->stEmvAppDtls.szAppLabel);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_PREF_NAME_9F12", pstEmvDtls->stEmvAppDtls.szAppPrefName);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_EFFECTIVE_DATE_5F25",pstEmvDtls->stEmvAppDtls.szAppEffctiveDate);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"Clear APPLICATION_EXPIRY_DATE_5F24", pstEmvDtls->stEmvAppDtls.szClearAppExpiryDate);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"Enc APPLICATION_EXPIRY_DATE_5F24", pstEmvDtls->stEmvAppDtls.szEncAppExpiryDate);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_TRAN_COUNTER_9F36", pstEmvDtls->stEmvAppDtls.szAppTranCounter);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"AIP_82",pstEmvDtls->stEmvAppDtls.szAppIntrChngProfile);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_VERSION_NUM_9F09", pstEmvDtls->stEmvAppDtls.szAppVersNum);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_USAGE_CONTROL_9F07", pstEmvDtls->stEmvAppDtls.szAppUsageControl);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_ICC_VERSION_NUM_9F08", pstEmvDtls->stEmvAppDtls.szICCAppVersNum);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"AMOUNT_AUTHORISED_9F02", pstEmvDtls->stEmvtranDlts.szAuthAmnt);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"AUTH_RESP_CODE_8A", pstEmvDtls->stEmvtranDlts.szAuthRespCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"CVM_RESULTS_9F34", pstEmvDtls->stEmvtranDlts.szCVMResult);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"DEDICATED_FILE_NAME_84", pstEmvDtls->stEmvtranDlts.szDedicatedFileName);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"AMOUNT_OTHER_9F03", pstEmvDtls->stEmvtranDlts.szEMVCashBackAmnt);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"EMV_TRANS_TIME_9F21", pstEmvDtls->stEmvtranDlts.szTranTime);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TRANSACTION_CATEGORY_CODE_9F53", pstEmvDtls->stEmvtranDlts.szTranCategoryCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"ICC_FORM_FACTOR_9F6E", pstEmvDtls->stEmvtranDlts.szICCFormFactor);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TRANS_STATUS_INFO_9B", pstEmvDtls->stEmvtranDlts.szTranStatusInfo);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TERM_TRANS_CODE_9F1A", pstEmvDtls->stEmvtranDlts.szTermTranCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TRANS_DATE_9A", pstEmvDtls->stEmvtranDlts.szTermTranDate);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TERMINAL_TRAN_QUALIFIER_9F66", pstEmvDtls->stEmvtranDlts.szTermTranQualfr);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TRANS_SEQ_COUNTER_9F41", pstEmvDtls->stEmvtranDlts.szTermTranSeqCount);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TRANS_REF_CURR_CODE_9F3C", pstEmvDtls->stEmvtranDlts.szTranRefCurCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"KeySerialNum", pstEmvDtls->stEmvtranDlts.szKeySerialNum);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"PINBlock", pstEmvDtls->stEmvtranDlts.szPINBlock);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"POS_ENTRY_MODE_9F39", pstEmvDtls->stEmvtranDlts.szPOSEntryMode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"PinTryCounter", pstEmvDtls->stEmvtranDlts.szPinTryCounter);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"CurrencyCode", pstEmvDtls->stEmvtranDlts.szCurrencyCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bPINByPassed", pstEmvDtls->stEmvtranDlts.bPINByPassed);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"CRYPTGRM_INFO_DATA_9F27", pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmInfoData);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"APPLICATION_CRYPTGRM_9F26", pstEmvDtls->stEmvCryptgrmDtls.szAppCryptgrm);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TRANS_TYPE_9C", pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmTranType);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"CRYPTOGRM_CURR_CODE_5F2A", pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmCurrCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"UNPREDICTBLE_NUM_9F37", pstEmvDtls->stEmvCryptgrmDtls.szUnprdctbleNum);APP_TRACE(szDbgMsg);


	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"ISSUER_ID_42", pstEmvDtls->stEmvIssuerDtls.szIssuerID);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"CUSTOMER_EXCLUSIVE_DATA_9F7C", pstEmvDtls->stEmvIssuerDtls.szCustExclData);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"ISSUER_APP_DATA_9F10", pstEmvDtls->stEmvIssuerDtls.szIssuerAppData);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"ISSUER_AUTH_DATA_91", pstEmvDtls->stEmvIssuerDtls.szIssuerAuthData);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"ISSUER_SCRIPT_RESULTS_9F5B", pstEmvDtls->stEmvIssuerDtls.szIssueScrptResults);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"ISSUER_COUNTRY_CODE_9F56", pstEmvDtls->stEmvIssuerDtls.szIssuerCntryCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"IAC_DEFAULT_9F0D", pstEmvDtls->stEmvIssuerDtls.szIACDefault);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"IAC_DENIAL_9F0E", pstEmvDtls->stEmvIssuerDtls.szIACDenial);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"IAC_ONLINE_9F0F", pstEmvDtls->stEmvIssuerDtls.szIACOnline);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"ISSUER_CODE_TABLE_INDEX_9F11", pstEmvDtls->stEmvIssuerDtls.szIssuerCodeTableIndex);APP_TRACE(szDbgMsg);


	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TERMINAL_AID_9F06", pstEmvDtls->stEmvTerDtls.szTermAID);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TERM_CAPABILITIES_9F33", pstEmvDtls->stEmvTerDtls.szTermCapabilityProf);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TERM_TYPE_9F35", pstEmvDtls->stEmvTerDtls.szTermType);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TERMINAL_VERIF_RESULTS_95", pstEmvDtls->stEmvTerDtls.szTermVerificationResults);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"MACValue", pstEmvDtls->stEmvTerDtls.szMACValue);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"DEVICE_SERIAL_NUM_9F1E", pstEmvDtls->stEmvTerDtls.szDevSerialNum);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TAC_DEFAULT", pstEmvDtls->stEmvTerDtls.szTACDefault);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TAC_DENIAL", pstEmvDtls->stEmvTerDtls.szTACDenial);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"TAC_ONLINE", pstEmvDtls->stEmvTerDtls.szTACOnline);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"PINEnteredStatus", pstEmvDtls->stEmvtranDlts.PINEnteredStatus);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"CVMTpeChosen", pstEmvDtls->stEmvtranDlts.CVMTpeChosen);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bCardInserted", pstEmvDtls->stEMVCardStatusU02.bCardInserted);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bCardSwiped", pstEmvDtls->stEMVCardStatusU02.bCardSwiped);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bCardTapped", pstEmvDtls->stEMVCardStatusU02.bCardTapped);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bAppSelScreen", pstEmvDtls->stEMVCardStatusU02.bAppSelScreen);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bLangSelScreen", pstEmvDtls->stEMVCardStatusU02.bLangSelScreen);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bEMVPINEntry", pstEmvDtls->stEMVCardStatusU02.bEMVPINEntry);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bCardRemoved", pstEmvDtls->stEMVCardStatusU02.bCardRemoved);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bEMVRetry", pstEmvDtls->stEMVCardStatusU02.bEMVRetry);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"iCardSrc", pstEmvDtls->iCardSrc);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"iTrkNo", pstEmvDtls->iTrkNo);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"iEncType", pstEmvDtls->iEncType);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"iAccountType", pstEmvDtls->iAccountType);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"iTranSeqNum", pstEmvDtls->iTranSeqNum);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"iKeyPointer", pstEmvDtls->iKeyPointer);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"EMVlangSelctd", pstEmvDtls->EMVlangSelctd);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %d",__FUNCTION__,"bSignReqd", pstEmvDtls->bSignReqd);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szEMVRespCode", pstEmvDtls->szEMVRespCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szEMVRespCmd", pstEmvDtls->szEMVRespCmd);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szEMVReqCmd", pstEmvDtls->szEMVReqCmd);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szEMVlangPref", pstEmvDtls->szEMVlangPref);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szXPIVer", pstEmvDtls->szXPIVer);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szPAN", pstEmvDtls->szPAN);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szName", pstEmvDtls->szName);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szTrackData", pstEmvDtls->szTrackData);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szServiceCode", pstEmvDtls->szServiceCode);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szSpinBinData", pstEmvDtls->szSpinBinData);APP_TRACE(szDbgMsg);
	
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szEmvTags", pstEmvDtls->szEMVTags);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szEMVEncBlob", pstEmvDtls->szEMVEncBlob);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szEparms", pstEmvDtls->szEparms);APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: %s= %s",__FUNCTION__,"szRsaKeyId", pstEmvDtls->szRsaKeyId);APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: ---Returning---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

}
#endif


/*
 * ============================================================================
 * Function Name: getEmvCmdType
 *
 * Description	: This function is responsible for extracting the command type
 * 					from the response received from the UI agent
 *
 * Input Params	: 	szMsg -> input message to be parsed for the command
 * 					szCmd -> place holder for the parsed command string
 *
 * Output Params: none
 * ============================================================================
 */
char * getEmvCmdType(char * szMsg, char * szCmd, int *piRespCode)
{
	char *	nxtPtr			= NULL;
	char 	szRespCode[3]	= "";
	char	szReqCmd[4]		= "";
	int		EMVReqCmd		= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	EMVReqCmd = getEmvReqCmd(szReqCmd);
	while(1)
	{
#if 0
			if( ((nxtPtr = strstr(szMsg, EMV_U02_REQ)) != NULL) )
		{
			memcpy(szCmd, nxtPtr, 3);
			*piRespCode = 0;
			break;
		}
#endif

		switch(EMVReqCmd)
		{
			case EMV_C30:
				if( ((strncmp(szMsg, EMV_U00_REQ, 3)) == 0) ||
					((strncmp(szMsg, EMV_U01_REQ, 3)) == 0) ||
					((strncmp(szMsg, EMV_U02_REQ, 3)) == 0)
				)
				{
					nxtPtr = szMsg;
					memcpy(szCmd, nxtPtr, 3);
					//U00 or U01 send no response code
					//memcpy(szRespCode, nxtPtr+3, 1);
					*piRespCode = 0;
					break;
				}
				//AJAYS2: DO NOT ADD BREAK
			case EMV_C32:
			case EMV_C34:
			case EMV_C25:
			case EMV_C36:
			case EMV_E06:
			case EMV_C20:
			case EMV_C19:
				if( ((strncmp(szMsg, EMV_C31_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_C33_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_C34_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_C20_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_C19_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_C36_RESP, 3)) == 0)  )
				{
					nxtPtr = szMsg;
					memcpy(szCmd, nxtPtr, 3);
					memcpy(szRespCode, nxtPtr+3, 2);
					*piRespCode = atoi(szRespCode);
				}
				break;
			case EMV_E10:
				if( ((strncmp(szMsg, EMV_E11_RESP, 3)) == 0))
				{
					nxtPtr = szMsg;
					memcpy(szCmd, nxtPtr, 3);
					memcpy(szRespCode, nxtPtr+3, 3);
					*piRespCode = atoi(szRespCode);
				}
				break;
			case EMV_I02:
				if( ((strncmp(szMsg, EMV_I02_RESP, 3)) == 0) )
				{
					nxtPtr = szMsg;
					memcpy(szCmd, nxtPtr, 3);
					memcpy(szRespCode, nxtPtr+3, 1);
					*piRespCode = atoi(szRespCode);
				}
				break;
			case EMV_I05:
				if( ((strncmp(szMsg, EMV_I05_RESP, 3)) == 0) )
				{
					nxtPtr = szMsg;
					memcpy(szCmd, nxtPtr, 3);
					memcpy(szRespCode, nxtPtr+3, 1);
					*piRespCode = atoi(szRespCode);
				}
				break;
			case EMV_E02:
			case EMV_S20:
			case EMV_S66:
			case EMV_S95:
			case EMV_D01:
				if((strncmp(szMsg, EMV_U02_REQ, 3)) == 0)
				{
					nxtPtr = szMsg;
					memcpy(szCmd, nxtPtr, 3);
					*piRespCode = 0;
				}
				else if( ( ( (szMsg[0] == '0') && (szMsg[1] == '0') ) )  ||
					( ( (szMsg[0] == '0') && (szMsg[1] == '1') ) )  ||
					( ( (szMsg[0] == '0') && (szMsg[1] == '2') ) )  ||
					( ( (szMsg[0] == '0') && (szMsg[1] == '3') ) )  ||
					( ( (szMsg[0] == '7') && (szMsg[1] == '9') ) )  ||
					( ( (szMsg[0] == '9') && (szMsg[1] == '0') ) )  ||
					( ( (szMsg[0] == '9') && (szMsg[1] == '9') ) )  )
				{
					nxtPtr = szMsg;
					memcpy(szRespCode, nxtPtr, 2);
					*piRespCode = atoi(szRespCode);
					//For these commands the response does not have any commands name(like C31 etc), so pasting the req command names in resp cmd
					strcpy(szCmd, szReqCmd);
				}
				break;
		case EMV_D41:
			if(strncmp(szMsg, EMV_D41_RESP, 3) == 0)
			{
				nxtPtr = szMsg;
				memcpy(szCmd, nxtPtr, 3);
				memcpy(szRespCode, nxtPtr+3, 2);
				*piRespCode = atoi(szRespCode);
			}
			break;
			case EMV_D11:
			case EMV_D12:
			case EMV_D13:
			case EMV_D14:
			case EMV_D15:
			case EMV_D16:
			case EMV_D17:
			case EMV_D18:
			case EMV_D19:
			case EMV_D20:
			case EMV_D25:
			case EMV_D32:
				if( ((strncmp(szMsg, EMV_D11_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_D12_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_D13_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_D14_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_D15_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_D16_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_D17_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_D18_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_D19_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_D20_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_D25_RESP, 3)) == 0)  ||
					((strncmp(szMsg, EMV_D32_RESP, 3)) == 0)  )
				{
					nxtPtr = szMsg;
					memcpy(szCmd, nxtPtr, 3);
					memcpy(szRespCode, nxtPtr+3, 2);
					*piRespCode = atoi(szRespCode);
				}
				break;
			default:
				nxtPtr = NULL;
				debug_sprintf(szDbgMsg, "%s: Required EMV Response NOT found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
		}
		break;
	}
	szCmd[3]='\0';
	debug_sprintf(szDbgMsg, "%s: ---Returning--- Cmd [%s] Response [%s]", __FUNCTION__, szCmd, szRespCode);
	APP_TRACE(szDbgMsg);

	return nxtPtr;
}
/*
 * ============================================================================
 * Function Name: parseEmvVerResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseEmvVerResp(char * pszMsg, EMVDTLS_PTYPE pstEmvDtls)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	char	szTmp[50]		= "";
	char *	curPtr			= NULL;
	char *	nxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* 
	 * The response message from the UI agent is shown below
	 * <STX>00MX-XPI-5110A-DEB<FS>5110.<FS>RFS00190 000261300169027390<FS>MX200001 MX200001 00000000000<ETX>
	 */

	//Assign data 
	curPtr = pszMsg;

	debug_sprintf(szDbgMsg, "%s: XPI Version RespStr = [%s]", __FUNCTION__, curPtr);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Get the EMV version */

		nxtPtr = strchr(curPtr, FS);
		if(nxtPtr != NULL)
		{
			//Copy XPI version
			memcpy(pstEmvDtls->szXPIVer, curPtr, nxtPtr - curPtr);
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: XPI version NOT FOUND", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Get the EMV Kernel version */
		curPtr = nxtPtr + 1;
		if(curPtr != NULL)
		{
			nxtPtr = strchr(curPtr, FS);
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: Error while Parsing S95", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		curPtr = nxtPtr + 1;
		if(curPtr != NULL)
		{
			nxtPtr = strchr(curPtr, FS);
			if(nxtPtr != NULL)
			{
				memset(szTmp, 0x00, sizeof(szTmp));
				memcpy(szTmp, curPtr, nxtPtr - curPtr);

				if(strlen(szTmp) == 34)
				{
					//Copy EMV Kernel version
					iLen = 9;
					while(szTmp[iLen] == '0')
					{
						iLen++;
					}
					memcpy(pstEmvDtls->stEmvTerDtls.szEMVKernelVer, (char*)szTmp + iLen, 18 - iLen);
					debug_sprintf(szDbgMsg,"%s: EMV Kernel Ver %s ", __FUNCTION__, pstEmvDtls->stEmvTerDtls.szEMVKernelVer);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg,"%s: XPI version NOT FOUND", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Error while Parsing S95", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: Error while Parsing S95", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Get the CTLS FirmWare version */
		curPtr = nxtPtr + 1;
		if(curPtr != NULL)
		{
			nxtPtr = strchr(curPtr, GS);

			if(nxtPtr == NULL)
			{
				nxtPtr = strchr(curPtr, ETX);
			}
			if(nxtPtr != NULL)
			{
				memset(szTmp, 0x00, sizeof(szTmp));
				memcpy(szTmp, curPtr, nxtPtr - curPtr);

				if(strlen(szTmp) == 29)
				{
					//Copy EMV Kernel version
					iLen = 9;
					while(szTmp[iLen] == '0')
					{
						iLen++;
					}
					memcpy(pstEmvDtls->stEmvTerDtls.szEMVCTLSVer, (char*)szTmp + iLen, 29 - iLen);
					debug_sprintf(szDbgMsg,"%s: EMV CTLS Firmware Ver %s ", __FUNCTION__, pstEmvDtls->stEmvTerDtls.szEMVCTLSVer);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg,"%s: XPI version NOT FOUND", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Error while Parsing S95", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: Error while Parsing S95", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: parseEmvMacResp
 *
 * Description	: This function parse the response and stores MAC block and key serial number.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseEmvMacResp(char * pszMsg, EMVDTLS_PTYPE pstEmvDtls)
{
	int			rv					= SUCCESS;
	char 		szTmpSrlNum[17]	= "";
	char *		curPtr				= NULL;
	char *		nxtPtr				= NULL;
#ifdef DEBUG
	char	szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* 
	 * The sample response message from the UI agent is shown below
	 * <STX>00309A6532169022127       <ETX>
    */
	
	//Assign data 
	curPtr = pszMsg;
	
	debug_sprintf(szDbgMsg, "%s: XPI MAC RespStr = [%s]", __FUNCTION__, curPtr);
	APP_TRACE(szDbgMsg);

	/* Get the EMV MAC Value*/
	nxtPtr = strchr(curPtr, ETX);
	if(nxtPtr != NULL)
	{
		if((nxtPtr - curPtr) > 0)
		{
			memcpy(pstEmvDtls->stEmvTerDtls.szMACValue, curPtr, 8);
			
			nxtPtr = curPtr + 8;
			
			memset(szTmpSrlNum, 0x00, sizeof(szTmpSrlNum));
			memcpy(szTmpSrlNum, nxtPtr, 16);
			
			/* Strip any leading and trailing spaces if present in the field value */
			stripSpaces(szTmpSrlNum, STRIP_LEADING_SPACES|STRIP_TRAILING_SPACES);
			
			memcpy(pstEmvDtls->stEmvTerDtls.szDevSerialNum, szTmpSrlNum, strlen(szTmpSrlNum));
			
			rv = SUCCESS;
		}	
		else
		{
			debug_sprintf(szDbgMsg,"%s: Couldn't find MAC block and Device Serial Number", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			
			rv = FAILURE;
		}		
	}
	else
	{
			debug_sprintf(szDbgMsg,"%s: Couldn't find MAC block and Device Serial Number", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		
		rv = FAILURE;
	}
	
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

#if 0
/*
 * ============================================================================
 * Function Name: parseForCardPresenceReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseForCardPresenceReq(char * pszMsg)
{
	int		rv				= SUCCESS;
	char 	szTmp[2]		= "";
	char *	curPtr			= NULL;
	char *	nxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* 
	Response Format:
	<STX>I05<Response_code><ETX><LRC>    
	*/
	
	//Assign data 
	curPtr = pszMsg;
	
	nxtPtr = curPtr+3;	//Move ptr to reponse code
	if(nxtPtr != NULL)
	{
		memset(szTmp, 0x00, sizeof(szTmp));
		memcpy(szTmp, nxtPtr, 1);	
		if(!strcmp(szTmp, "1"))
		{
			debug_sprintf(szDbgMsg, "%s: Card is present", __FUNCTION__);
			APP_TRACE(szDbgMsg);		
			rv = CARD_PRESENT;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Card is not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);		
			rv = CARD_NOT_PRESENT;			
		}
	}
	else
	{
		rv = FAILURE;
		debug_sprintf(szDbgMsg, "%s: parse error", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseForCardRemoveReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseForCardRemoveReq(char * pszMsg)
{
	int		rv				= SUCCESS;
	char 	szTmp[2]		= "";
	char *	curPtr			= NULL;
	char *	nxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* 
	Response Format:
	<STX>I02<Response_code><ETX><LRC>   
	Response code - 0 - Successful, card removed
					1 - Unsuccessful
					2 - Card Removed - MSR read error	
	*/
	
	//Assign data 
	curPtr = pszMsg;
	
	nxtPtr = curPtr+3;	//Move ptr to reponse code
	if(nxtPtr != NULL)
	{
		memset(szTmp, 0x00, sizeof(szTmp));
		memcpy(szTmp, nxtPtr, 1);	
		if(!strcmp(szTmp, "0"))
		{
			debug_sprintf(szDbgMsg, "%s: Card is present", __FUNCTION__);
			APP_TRACE(szDbgMsg);		
			rv = CARD_REMOVED;
		}
		else if(!strcmp(szTmp, "1"))
		{
			debug_sprintf(szDbgMsg, "%s: Card is not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);		
			rv = CARD_NOT_REMOVED;			
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Card read error", __FUNCTION__);
			APP_TRACE(szDbgMsg);		
			rv = EMV_ERROR_READER_FAILED;			
		}		
	}
	else
	{
		rv = FAILURE;
		debug_sprintf(szDbgMsg, "%s: parse error", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: parseEmvSwipeCardResp
 *
 * Description	: This function would parse the XPI response.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseEmvSwipeCardResp(unsigned char *InBuffer, int iBuffSize, 
												CARD_TRK_PTYPE	pstCrdTrkInfo)
{
	int     				rv 			  		= SUCCESS;
	int						iEncType			= 0;
	int 					iLengthParsed 		= 0;
	int			 			iTrkNumber			= 0;
	char 					szTag[5]     		= "";
	char					szTmp[10]			= "";
	unsigned char	*		ptrTagValue			= NULL;
	unsigned int 			tag;
	unsigned short  		Temp;
	unsigned short 			len;
	PAAS_BOOL				btagAlreadyParsed	= PAAS_FALSE;
	CARD_TRK_STYPE			stCrdTrkInfo;
	
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	APP_TRACE_EX((char *) InBuffer, iBuffSize);	
#endif

	
	memset(&stCrdTrkInfo, 0x00, sizeof(CARD_TRK_STYPE));
	
	/* KranthiK1:
	 * Defaulting the card source to MSR so that if card source is not received
	 * This value will be set by default and if any value is received it will be overwritten
	 */
	stCrdTrkInfo.iCardSrc = CRD_MSR;

	if(ptrTagValue == NULL)
	{
		ptrTagValue = (unsigned char *) malloc (sizeof(char)* 1024);
		if( ptrTagValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s:memory allocation failure", __FUNCTION__);
			APP_TRACE(szDbgMsg);	
			return FAILURE;
		}	
	}	
	while(iBuffSize > 0)
	{
		btagAlreadyParsed = PAAS_FALSE;
		// Get tag
		tag = 0;
		tag= *InBuffer++;
		iLengthParsed= 1;
		if ((tag & 0x1f) == 0x1f)
		{
			do {
				tag = tag << 8;
				tag = tag | *(InBuffer++);
				iLengthParsed++;
			} while((tag & 0x80) == 0x80);
			//last byte in Tag name should be of 0xxxxxxx format, for a multi-byte tag name.
		}

		//Convert tag(Hex value) to ASCCI string
		memset(szTag, 0x00, sizeof(szTag));
		sprintf(szTag, "%X", tag);

		//AjayS2: Ignoring FS, Remove this if XPI fix
		if( strcmp(szTag, "1C") == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Got FS tag, Ignoring and moving to next Byte", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iBuffSize-=iLengthParsed;
			continue;
		}

		// Get length
		len= *InBuffer++;
		iLengthParsed += 1;
		if (len & 0x80)
		{	  
			Temp= len & 0x7f; 
			len= 0;
			while (Temp)
			{
				len *= 256;
				len += *InBuffer++;
				iLengthParsed += 1;
				Temp--; // Temp should decrimented for exiting from the closed loop.
			}
		}

		if((!strcmp("C2", szTag)) && (iTrkNumber >= 3))
		{
			len *= 256;
			len += *InBuffer++;
			iLengthParsed += 1;

			iEncType = getEncryptionType();
			memset(szTmp, 0x00, sizeof(szTmp));
			strncpy(szTmp, (char *)InBuffer, 3);

			if(iEncType == RSA_ENC)
			{
				if(strcmp(szTmp, "E07") == 0)
				{
					parseEmvCipheredDataResp((char*)InBuffer + 3, len - 3, &stCrdTrkInfo, NULL, 2);
				}
				debug_sprintf(szDbgMsg, "%s: RSA Type", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(iEncType == VSP_ENC)
			{
				if(strcmp(szTmp, "E07") != 0)
				{
					parseEmvEParamsDataResp((char*)InBuffer , len , &stCrdTrkInfo, NULL, 2);
				}
				debug_sprintf(szDbgMsg, "%s: VSP Type", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(iEncType == VSD_ENC)
			{
				if(! strcmp(szTmp, "E07"))
				{
					debug_sprintf(szDbgMsg, "%s: E07 response", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					parseVSDBlobDataResp((char*)InBuffer + 3 , len - 3 , &stCrdTrkInfo, NULL, NULL, 2);
				}
				debug_sprintf(szDbgMsg, "%s: VSD Type", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: No Enc Type", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			btagAlreadyParsed = PAAS_TRUE;
		}

		// store the value
		memset(ptrTagValue, 0x00, 1024);
		memcpy(ptrTagValue, InBuffer, len);
		iLengthParsed += len;
		InBuffer += len;

		if(btagAlreadyParsed == PAAS_FALSE)
		{
			/*AjayS2: 04-Feb-2016: PTMX-800 PointMx_SCA_2.19.23B2 - Terminal freeze in any transactions when EMVenabled=Y
			 * We are doubling and pasting in 1024 bytes memory. So whenever RSA blob is more than 512 bytes, and release mode
			 * Point will crash. Fixed by not calling storeCardData, because we already parsed RSA Blob
			 */
			/*store the tag value into the card track info data structure*/
			rv = storeCardData(szTag, ptrTagValue, len, &stCrdTrkInfo,
					++iTrkNumber);
		}
		iBuffSize-=iLengthParsed;
	}
	//Assign card source here.
	//stCrdTrkInfo.iCardSrc = CRD_MSR;
	
#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s:%s Track1[%s]",__FUNCTION__, DEVDEBUGMSG,
														stCrdTrkInfo.szTrk1);
	APP_TRACE(szDbgMsg);
	
	debug_sprintf(szDbgMsg, "%s:%s Track2[%s]",__FUNCTION__, DEVDEBUGMSG,
												stCrdTrkInfo.szTrk2);
	APP_TRACE(szDbgMsg);	

	debug_sprintf(szDbgMsg, "%s:%s Track3[%s], CardSource[%d]",__FUNCTION__,  DEVDEBUGMSG, 
							stCrdTrkInfo.szTrk3, stCrdTrkInfo.iCardSrc);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s:%s Clear ExpDate[%s]",__FUNCTION__, DEVDEBUGMSG, 
												stCrdTrkInfo.szClrExpDt);
	APP_TRACE(szDbgMsg);
	
#endif
	
	memcpy(pstCrdTrkInfo, &stCrdTrkInfo, sizeof(CARD_TRK_STYPE));	
	
	free(ptrTagValue);	
	
	debug_sprintf(szDbgMsg,"%s: Returning rv =%d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	
	return rv;	
}

/*
 * ============================================================================
 * Function Name: parseCardRespForClrExpDate
 *
 * Description	: This function would parse the XPI response for clear expiry
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseCardRespForClrExpDate(unsigned char *InBuffer, int iBuffSize, char *pszClrExp)
{
	int     				rv 			  		= SUCCESS;
	int						iTmp				= 0;
	char 					szTag[5]     		= "";
	char 					szTmp[500]			= "";
	char					szTmpMonth[3]		= "";
	unsigned char	*		ptrTagValue	= NULL;
	unsigned short 			tag;
	unsigned short  		Temp;
	unsigned short 			len;
	int 					iLengthParsed 		= 0;
	int						numBytesinCurrTAG	= 0;

#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	APP_TRACE_EX((char *) InBuffer, iBuffSize);
#endif

	if(ptrTagValue == NULL)
	{
		ptrTagValue = (unsigned char *) malloc (sizeof(char)* 64);
		if( ptrTagValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s:memory allocation failure", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
	}
	while(iLengthParsed < iBuffSize)
	{
		//Getting TAG
		tag = 0;
		tag = *(InBuffer++);
		iLengthParsed++;
		numBytesinCurrTAG = 1;

		if( (tag & 0x1F) == 0x1F )// tag value is atleast 2 bytes
		{
			do {
				tag = tag << 8;
				tag = tag | *(InBuffer++);
				iLengthParsed++;
				numBytesinCurrTAG++;
			} while((tag & 0x80) == 0x80);
		}

		if(numBytesinCurrTAG > 3)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR: Tag is more than three bytes Not supporting NOW", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}
		//Getting Length
		len= *InBuffer++;
		iLengthParsed += 1;
		if (len & 0x80)
		{
			Temp= len & 0x7f;
			len= 0;
			while (Temp)
			{
				len *= 256;
				len += *InBuffer++;
				iLengthParsed += 1;
				Temp--; // Temp should decremented for exiting from the closed loop.
			}
		}

		// store the value
		memset(ptrTagValue, 0x00, 64);
		memcpy(ptrTagValue, InBuffer, len);
		iLengthParsed += len;
		InBuffer += len;

		//Convert tag(Hex value) to ASCII string[for e.g., "0" is stores as "30" on szTag]
		memset(szTag, 0x00, sizeof(szTag));
		sprintf(szTag, "%X", tag);

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s: Tag [%s] Length [%d] Value [%s]", __FUNCTION__, szTag, len, ptrTagValue);
		APP_TRACE(szDbgMsg);
#endif

		if(!strcmp(tagTwoByte_5F[APPLICATION_EXPIRY_DATE_5F24], szTag))
		{
			if(len > 0)
			{
				Char2Hex((char *)ptrTagValue, szTmp, len);
				memcpy(pszClrExp, szTmp, len*2);
				debug_sprintf(szDbgMsg, "Got ClearExp[%s]", pszClrExp);
				APP_TRACE(szDbgMsg);

				memset(szTmpMonth, 0x00, 3);
				memcpy(szTmpMonth, (char*)(pszClrExp) + 2, 2);

				if(strspn(szTmpMonth, "1234567890") != 2)
				{
					iTmp = 100;//Setting as 100 as we need to remove clear expiry
				}
				else
				{
					iTmp = atoi(szTmpMonth);
				}
				if(iTmp < 1 || iTmp > 12)
				{
					debug_sprintf(szDbgMsg, "%s: Removing clear expiry from card dtls as XPI send incorrect TLV in 5F24",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					memset(pszClrExp, 0x00, len*2);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: 5F24 Tag Present with No data, Breaking",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			//We got expiry date so breaking and returning
			break;
		}
	}

	//Free allocated memory by malloc.
	if(ptrTagValue != NULL)
	{
		free(ptrTagValue);
	}
	debug_sprintf(szDbgMsg,"%s: Returning rv =%d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseEmvSwipedS20CardResp
 *
 * Description	: This function would parse the XPI S20 card data response.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseEmvSwipedS20CardResp(char *szMsg, int iBuffSize, CARD_TRK_PTYPE	pstTrk)
{
	int				rv					= SUCCESS;
	int				iLen				= 0;
	int				iEncType			= 0;
	char			szCardSrc[5]		= "";
	char			szTmp[50]			= "";
	char  			ptrValue[128] 		= "";
	char			szPayPassType[3]	= "";
	char *			curPtr				= NULL;
	char *			nxtPtr				= NULL;
	char *			tmpPtr				= NULL;
	unsigned short 	Temp;
	PAAS_BOOL		bTrk1				= PAAS_FALSE;
	PAAS_BOOL		bTrk2				= PAAS_FALSE;
	PAAS_BOOL		bTrk3				= PAAS_FALSE;
	PAAS_BOOL		bSwipedIndicator	= PAAS_FALSE;

#ifdef DEBUG
	char		szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Initialize the data */
	memset(pstTrk, 0x00, sizeof(CARD_TRK_STYPE));
	curPtr = szMsg;

	// {Track2}<FS>{Track1}<FS>{Track3}<FS>SwipedIndicator<ETX>

	/* Get the first track information */
	nxtPtr = strchr(curPtr, FS);
	if(*curPtr == ';')
	{
		curPtr++;
	}
	iLen = nxtPtr - curPtr;
	if(iLen > 0)
	{
		//Skip last sentinel and LRC from track2 data
		if( (tmpPtr = strchr(curPtr, '?')) )
		{
			iLen = tmpPtr - curPtr;		
			memcpy(pstTrk->szTrk2, curPtr, iLen);			
		}
		else
		{
			memcpy(pstTrk->szTrk2, curPtr, iLen);
		}

		//	memcpy(pstTrk->szTrk2, curPtr, iLen);

		bTrk2 = PAAS_TRUE;

		debug_sprintf(szDbgMsg, "%s: Track2 is present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: TRACK 2 DATA = [%s]", DEVDEBUGMSG,
				__FUNCTION__, pstTrk->szTrk2);
		APP_TRACE(szDbgMsg);
#endif
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Track2 NOT present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* get the second track One data */
	curPtr = nxtPtr + 1;
	if(*curPtr == '%')
	{
		curPtr++;
	}
	nxtPtr = strchr(curPtr, FS);
	iLen = nxtPtr - curPtr;
	if(iLen > 0)
	{

		//Skip last sentinel and LRC from track1 data
		if( (tmpPtr = strchr(curPtr, '?')) )
		{
			iLen = tmpPtr - curPtr;
			memcpy(pstTrk->szTrk1, curPtr, iLen);			
		}
		else
		{
			memcpy(pstTrk->szTrk1, curPtr, iLen);
		}	

		//memcpy(pstTrk->szTrk1, curPtr, iLen);

		bTrk1 = PAAS_TRUE;

		debug_sprintf(szDbgMsg, "%s: Track1 is present", __FUNCTION__);
		APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: TRACK 1 DATA = [%s]", DEVDEBUGMSG,
				__FUNCTION__, pstTrk->szTrk1);
		APP_TRACE(szDbgMsg);
#endif
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Track1 NOT present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* get the third track data */
	curPtr = nxtPtr + 1;
	if(*curPtr == ';')
	{
		curPtr++;
	}

    /*AjayS2: 21/Jan/2016: {Track2}<FS>{Track1}<FS>{Track3}{<FS>SwipedIndicator}<C2>Eparams<C2>RSABlob<E3>ClearExpiry<C3>ApplePayVASData<ETX>
    * After {Track3} FS, C2, C2, E3, C3 are optional fields in the respective order.
    * We have check for each one in order.
    *
    */
    if(isContactlessEnabledInDevice())
    {
           nxtPtr = strchr(curPtr, FS);
           if( nxtPtr == NULL)
           {
                  nxtPtr = strchr(curPtr, 0xC2);
                  if( nxtPtr == NULL)
                  {
                        nxtPtr = strchr(curPtr, 0xE3);
                        if( nxtPtr == NULL)
                        {
                               nxtPtr = strchr(curPtr, 0xC3);
                               if(nxtPtr == NULL)
                               {
                                      nxtPtr = strchr(curPtr, ETX);
                               }
                        }
                  }
           }
    }
    else
    {
           /*
           * AjayS2 28 Mar 2016: If ctls is disabled, and RSA or Eparams enabled C2 tag will be just after Track3, otherwise E3 or C3 or ETX tag will be there
           */
           nxtPtr = strchr(curPtr, 0xC2);
           if( nxtPtr == NULL)
           {
                  nxtPtr = strchr(curPtr, 0xE3);
                  if( nxtPtr == NULL)
                  {
                        nxtPtr = strchr(curPtr, 0xC3);
                        if(nxtPtr == NULL)
                        {
                               nxtPtr = strchr(curPtr, ETX);
                        }
                  }
           }
    }

	iLen = nxtPtr - curPtr;
	if(iLen > 0)
	{
		//Skip last sentinel and LRC from track2 data
		if( (tmpPtr = strchr(curPtr, '?')) )
		{
			iLen = tmpPtr - curPtr;
			memcpy(pstTrk->szTrk3, curPtr, iLen);
		}
		else
		{
			memcpy(pstTrk->szTrk3, curPtr, iLen);
		}

		//memcpy(pstTrk->szTrk1, curPtr, iLen);
		bTrk3 = PAAS_TRUE;

		debug_sprintf(szDbgMsg, "%s: Track3 is present", __FUNCTION__);
		APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: TRACK 3 DATA = [%s]", DEVDEBUGMSG,
				__FUNCTION__, pstTrk->szTrk3);
		APP_TRACE(szDbgMsg);
#endif
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Track3 NOT present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/*Need to get the Swipe Indicator now*/
	if( nxtPtr != NULL && isContactlessEnabledInDevice() == PAAS_TRUE)
	{
		/* Get the swiped indicator if present*/
		curPtr = nxtPtr + 1;
#if 0
		nxtPtr = strchr(curPtr, 0xC2);

		//If C2 is not present Search for E3
		if(nxtPtr == NULL)
		{
			nxtPtr = strchr(curPtr, 0xE3);
		}
#endif
		if(nxtPtr != NULL)
		{
			iLen = curPtr - nxtPtr;
			if(iLen > 0)
			{
				memset(szCardSrc, 0x00, sizeof(szCardSrc));
				memcpy(szCardSrc, curPtr, iLen);

				if(strncmp(szCardSrc, "0", 1) == SUCCESS)
				{
					pstTrk->iCardSrc = CRD_MSR;
				}
				else
				{
					pstTrk->iCardSrc = CRD_RFID;
				}
#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s: Card is swiped %s ", __FUNCTION__, szCardSrc);
				APP_TRACE(szDbgMsg);
#endif
				//Set swiped indicator as true.
				bSwipedIndicator = PAAS_TRUE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Swiped indicator is NOT Present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//If swiped indicator is not present means, card is swiped.
			pstTrk->iCardSrc = CRD_MSR;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Swiped indicator is not present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		//If swiped indicator is not present means, card is swiped.
		pstTrk->iCardSrc = CRD_MSR;
	}

	//Pointing curPtr to just after swipeIndicator
	if(curPtr != NULL)
	{
		/*AjayS2: 21/Jan/2016:
		 * Format of S20 CTLS enabled:
		 * {Track2}<FS>{Track1}<FS>{Track3}<FS>SwipedIndicator<C2>Eparams<C2>RSA_Blob<E3>ClearExpiry<C3>ApplePayVASData<ETX>
		 * Format of S20 CTLS Disabled:
		 * {Track2}<FS>{Track1}<FS>{Track3}<C2>Eparams<C2>RSA_Blob<E3>ClearExpiry<C3>ApplePayVASData<ETX>
		 */
		if(bSwipedIndicator == PAAS_TRUE)
		{
			/* AjayS2: 21/Jan/2016: If swipe Indicator is present , curptr is pointing to Swipe indicator currently, we  have to move curptr
			 *  one byte ahead, pointing it to the just next available Tag C2 or E3 or C3
			 */
			curPtr += 1;
		}
		else
		{
			/* AjayS2: 21/Jan/2016: If swipe Indicator is Not present , curptr is pointing to one byte after 2nd FS or starting of Track 3
			 * data, we  have to move curptr pointing it to the just next available Tag C2 or E3 or C3 wherever nxptr is pointing.
			 */
			curPtr = nxtPtr;
		}
	}

	if(curPtr != NULL)
	{
		nxtPtr = curPtr + 1;
		if(*curPtr ==  0x9F && *nxtPtr ==  0x6E)
		{
			curPtr += 2;
			debug_sprintf(szDbgMsg, "%s: Parsing the 9F6E tag from S20", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iLen = *curPtr;
			curPtr = curPtr + 1; //curPtr is pointing to Data of 9F6E

			szPayPassType[0]	= curPtr[4];
			szPayPassType[1]	= curPtr[5];

			debug_sprintf(szDbgMsg, "%s: PayPass Indicator: %s", __FUNCTION__, szPayPassType);
			APP_TRACE(szDbgMsg);

			sprintf(pstTrk->szPayPassType, "%d", atoi(szPayPassType));

			curPtr += iLen; //Moving the curPtr by iLen bytes so that it points to the next tag
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s:9F6E tag Not Found", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	while(1)
	{
		nxtPtr = curPtr;
		if(nxtPtr != NULL && *nxtPtr == 0xC2)
		{
			debug_sprintf(szDbgMsg, "%s: Copying E07 or E02 from S20", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			curPtr = nxtPtr +1;

			//Parsing Length
			iLen = (*curPtr)*256 + *(curPtr+1);
			curPtr = curPtr + 2; //curPtr is pointing to Data of C2

			//If any tracks are present or C2 data present, then need to copy encrypted data else no need to copy
			if( (bTrk1 | bTrk2 | bTrk3) && (iLen > 0) )
			{
				memset(szTmp, 0x00, sizeof(szTmp));
				strncpy(szTmp, (char *)curPtr, 3);

				debug_sprintf(szDbgMsg, "%s: szTmp %s iLen %d curPtr %s", __FUNCTION__, szTmp, iLen, curPtr);
				APP_TRACE(szDbgMsg);

				iEncType = getEncryptionType();
				if(iEncType == RSA_ENC)
				{
					if(strcmp(szTmp, "E07") == 0)
					{
						parseEmvCipheredDataResp((char*)curPtr + 3, iLen - 3, pstTrk, NULL, 3);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Curr Enc is RSA but got other resp in S20 <C2>", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					debug_sprintf(szDbgMsg, "%s:Enc is RSA Type", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else if(iEncType == VSP_ENC)
				{
					if(strcmp(szTmp, "E07") != 0)
					{
						parseEmvEParamsDataResp((char*)curPtr , iLen , pstTrk, NULL, 3);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Curr Enc is VSP but got E07 resp in S20 <C2>", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					debug_sprintf(szDbgMsg, "%s: Enc is VSP Type", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else if(iEncType == VSD_ENC)
				{
					debug_sprintf(szDbgMsg, "%s:Enc is VSD Type", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(! strcmp(szTmp, "E07"))
					{
						parseVSDBlobDataResp((char*)curPtr + 3, iLen - 3, pstTrk, NULL, NULL, 3);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Curr Enc is VSD but got other resp in S20 <C2>", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: No Enc Type Breaking from loop", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Tracks are not Present ", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			curPtr = curPtr + iLen;//curPtr is pointing to next tag now C2 or E3 or C3

			if(curPtr != NULL && *curPtr == 0xC2)
			{
				debug_sprintf(szDbgMsg, "%s: Got Second C2 Tag, Check the append variable in iab, Parsing this also", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No C2 Tag found", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(curPtr != NULL)
		{
			//nxtPtr = strchr(curPtr, 0xE3);
			nxtPtr = curPtr;

			if(nxtPtr != NULL && *nxtPtr == 0xE3)
			{
				debug_sprintf(szDbgMsg, "%s:Got E3 Tag", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				curPtr = nxtPtr + 1;

				//E3 is one byte tag only
				iLen = *(curPtr);
				curPtr = curPtr + 1;

				if( (bTrk1 | bTrk2 | bTrk3) && (iLen > 0) )
				{
					if(curPtr != NULL )
					{
						rv = parseCardRespForClrExpDate((unsigned char *)curPtr, iLen, pstTrk->szClrExpDt);
						if(rv == SUCCESS && ((strlen(pstTrk->szClrExpDt)) > 0) )
						{
							debug_sprintf(szDbgMsg, "%s: Clear expiry date is present!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Clear Expiry date is NOT present!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Clear Expiry date is not present!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s:Tracks are not Present ", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

				curPtr = curPtr + iLen;//curPtr is pointing to next tag now C2 or E3 or C3
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s:No E3 Tag Present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		break;
	}
	// C3 command
	if(curPtr != NULL)
	{
		//nxtPtr = strchr(curPtr, 0xC3);
		nxtPtr = curPtr;

		if(nxtPtr != NULL && *nxtPtr == 0xC3)
		{
			debug_sprintf(szDbgMsg, "%s:Got C3 Tag", __FUNCTION__);
					APP_TRACE(szDbgMsg);
			//Parsing Length for Apple Pay Data
			curPtr = nxtPtr + 1;

			iLen = *(curPtr++);
			if (iLen & 0x80 )
			{
				Temp= iLen & 0x7f;
				iLen= 0;
				while (Temp)
				{
					iLen *= 256;
					iLen += *curPtr++;
					Temp--; // Temp should decremented for exiting from the closed loop.
				}
			}

			if(iLen != 0)
			{
				memset(ptrValue, 0x00, sizeof(ptrValue));
				memcpy(ptrValue, curPtr, iLen);
				rv = parseApplePayDataResp((unsigned char*)ptrValue, iLen, pstTrk , NULL , 3);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error in Parsing Apple Pay Response", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Got C3 contains But With No Data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No C3 Tag", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: parseU01Response
 *
 * Description	: This function would parse the U01 for all AID present on card
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseU01Response(unsigned char *InBuffer, int iBuffSize, EMVDTLS_PTYPE pstEmvDtls, int iAIDNumber)
{
	int     				rv 			  		= SUCCESS;
	char 					szTag[5]     		= "";
	char					szTmp[256]			= "";
	int 					iLengthParsed 		= 0;
	int						iCountAID			= 0;
	unsigned char	*		ptrTagValue			= NULL;
	unsigned int 			tag;
	unsigned short  		Temp;
	unsigned short 			len;
	AID_NAME_NODE_PTYPE		pstAIDNameNode		= NULL;
	AID_LIST_INFO_PTYPE		pstAIDListInfo		= NULL;

#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	APP_TRACE_EX((char *) InBuffer, iBuffSize);
#endif


	if(pstEmvDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s:NULL params Passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(ptrTagValue == NULL)
	{
		ptrTagValue = (unsigned char *) malloc (sizeof(char)* 256);
		if( ptrTagValue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s:memory allocation failure", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
	}

	while(iBuffSize > 0)
	{
		// Get tag
		tag = 0;
		tag= *InBuffer++;
		iLengthParsed= 1;
		if ((tag & 0x1f) == 0x1f)
		{
			do {
				tag = tag << 8;
				tag = tag | *(InBuffer++);
				iLengthParsed++;
			} while((tag & 0x80) == 0x80);
			//last byte in Tag name should be of 0xxxxxxx format, for a multi-byte tag name.
		}

		//Convert tag(Hex value) to ASCCI string
		memset(szTag, 0x00, sizeof(szTag));
		sprintf(szTag, "%X", tag);

		//AjayS2: Ignoring FS, Remove this if XPI fix
		if( strcmp(szTag, "1C") == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Got FS tag, Ignoring and moving to next Byte", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iBuffSize-=iLengthParsed;
			continue;
		}

		// Get length
		len= *InBuffer++;
		iLengthParsed += 1;
		if (len & 0x80)
		{
			Temp= len & 0x7f;
			len= 0;
			while (Temp)
			{
				len *= 256;
				len += *InBuffer++;
				iLengthParsed += 1;
				Temp--; // Temp should decremented for exiting from the closed loop.
			}
		}

		// store the value
		memset(ptrTagValue, 0x00,256);
		memcpy(ptrTagValue, InBuffer, len);
		iLengthParsed += len;
		InBuffer += len;

		memset(szTmp, 0x00, sizeof(szTmp));
		Char2Hex((char *)ptrTagValue, szTmp, len);

		if(!strcmp("C2", szTag))
		{
			iCountAID++;
			parseU01Response(ptrTagValue, len, pstEmvDtls, iCountAID);
		}
		else if(!strcmp(tagOneByte[AID_4F], szTag))
		{
			pstAIDListInfo = &(pstEmvDtls->stEmvAppDtls.stAIDListInfo);

			debug_sprintf(szDbgMsg, "%s: AID no. %d is [%s]", __FUNCTION__, iAIDNumber, szTmp);
			APP_TRACE(szDbgMsg);

			pstAIDNameNode = (AID_NAME_NODE_PTYPE)malloc(sizeof(AID_NAME_NODE_STYPE));
			if(pstAIDNameNode == NULL)
			{
				debug_sprintf(szDbgMsg, "%s:memory allocation failure", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return FAILURE;
			}
			memset(pstAIDNameNode, 0x00, sizeof(AID_NAME_NODE_STYPE));
			strcpy(pstAIDNameNode->szAIDName, szTmp);

			if(iAIDNumber == 1)
			{
				memset(pstAIDListInfo, 0x00, sizeof(AID_LIST_INFO_STYPE));
				pstAIDListInfo->LstHead = pstAIDListInfo->LstTail = pstAIDNameNode;
			}
			else
			{
				pstAIDListInfo->LstTail->next = pstAIDNameNode;
				pstAIDListInfo->LstTail = pstAIDNameNode;
			}
			debug_sprintf(szDbgMsg, "%s: Successfully Added AID [%s] as AID no. %d in LinkList", __FUNCTION__, pstAIDNameNode->szAIDName,iAIDNumber);
			APP_TRACE(szDbgMsg);
		}

		iBuffSize-=iLengthParsed;
	}

	if(iAIDNumber == 0)
	{
		debug_sprintf(szDbgMsg,"%s: Total %d AID(s) present on card", __FUNCTION__, iCountAID);
		APP_TRACE(szDbgMsg);

		pstEmvDtls->stEmvAppDtls.stAIDListInfo.iTotalAIDs = iCountAID;
	}

	free(ptrTagValue);

	debug_sprintf(szDbgMsg,"%s: Returning rv =[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: parseEmvEParamsDataResp
 *
 * Description	: This function would parse the XPI E02 EParams data response.
 *				 source = 1 called from C31 command parsing
 * 				  source = 2 called from C3121 MSR Chip command parsing
 * 				  source = 3 called from S20 command parsing
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */

static int parseEmvEParamsDataResp(char *szMsg, int iBuffSize, CARD_TRK_PTYPE pstTrk, EMVDTLS_PTYPE pstEmvDtls, int iSource)
{
	int			rv				= SUCCESS;
	int			iResCode		= 0;
	char		szResCode[5]	= "";
#ifdef DEVDEBUG
	char 		szDbgMsg[4096]	= "";
#elif DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( (iSource == 1 && pstEmvDtls != NULL) || ( (iSource == 2 || iSource ==3) && (pstTrk != NULL) ) )
	{
		debug_sprintf(szDbgMsg, "%s: source called: %d", __FUNCTION__, iSource);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: NULL params or wrong source passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//CID 67312 (#1 of 1): Dereference after null check (FORWARD_NULL) T_RaghavendranR1
		rv = FAILURE;
		return rv;
	}

	memcpy(szResCode, szMsg, 2);

	szMsg += 2;
	iBuffSize -= 2;
	iResCode = atoi(szResCode);
	if(iResCode != 00)
	{
		debug_sprintf(szDbgMsg, "%s: Eparams data NOT present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}
	//<2bytesEncRespCode>E2<3bytessize>
	if(szMsg != NULL)
	{
		szMsg = szMsg + 5;
		if(szMsg != NULL)
		{
			/* Get the EParams data */
			iBuffSize = iBuffSize - 5;
#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s: iBufsize: %d szmsg: %s", __FUNCTION__, iBuffSize, szMsg);
			APP_TRACE(szDbgMsg);
#endif
			if(iSource == 1)
			{
				if(pstEmvDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: pstEmvDtls is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					return rv;
				}
				memcpy(pstEmvDtls->szEparms, szMsg, iBuffSize); //Skip a msg len of size 3N.
			}
			else
			{
				if(pstTrk == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: pstTrk is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					return rv;
				}
				memcpy(pstTrk->szEparms, szMsg, iBuffSize); //Skip a msg len of size 3N.
			}

			debug_sprintf(szDbgMsg, "%s: EParams data is present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
			if(iSource == 1)
			{
				debug_sprintf(szDbgMsg, "%s:%s: EParams data in EMV = [%s]", DEVDEBUGMSG,
						__FUNCTION__, pstEmvDtls->szEparms);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s:%s: EParams data = [%s]", DEVDEBUGMSG,
						__FUNCTION__, pstTrk->szEparms);
				APP_TRACE(szDbgMsg);
			}
#endif

		}
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseEmvCipheredDataResp
 *
 * Description	: This function would parse the XPI E07 Ciphered data response.
 *
 * Input Params	: source = 1 called from C31 command parsing
 * 				  source = 2 called from C3121 MSR Chip command parsing
 * 				  source = 3 called from S20 command parsing
 *
 * Output Params:
 * ============================================================================
 */
static int parseEmvCipheredDataResp(char *szMsg, int iBuffSize, CARD_TRK_PTYPE	pstTrk, EMVDTLS_PTYPE pstEmvDtls, int iSource)
{
	int			rv				= SUCCESS;
	int			iLen			= 0;
	int			iDataType		= -1;
	char		szEncType[2+1]	= "";
	char 		szTmp[50]		= "";
	int			iResCode		= 0;
	char		szResCode[5]	= "";
	char *		curPtr			= NULL;
	char *		nxtPtr			= NULL;
#ifdef DEVDEBUG
	char 		szDbgMsg[4096]	= "";
#elif DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( (iSource == 1 && pstEmvDtls != NULL) || ( (iSource == 2 || iSource ==3) && (pstTrk != NULL) ) )
	{
		debug_sprintf(szDbgMsg, "%s: Source called: %d", __FUNCTION__, iSource);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: NULL params or wrong source passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		//Coverity:Fix 83357 - Dereferencing the pstEMVDtls ahead.
		return FAILURE;
	}


	memcpy(szResCode, szMsg, 2);

	szMsg += 2;
	iBuffSize -= 2;
	iResCode = atoi(szResCode);

	while(1)
	{
		if( iResCode == 00 )
		{
			curPtr = szMsg;
			/* Copy the encryption type */
			memset(szEncType, 0x00, sizeof(szEncType));
			memcpy(szEncType, curPtr, 2);

			if(!strcmp(szEncType, "02"))	//PKI encryption
			{
				/* Get the PKI Ciphered data */
				nxtPtr = strchr(curPtr, FS);
				iLen = nxtPtr - curPtr;
				if(iLen > 0)
				{
					if(iSource == 1)
					{
						memcpy(pstEmvDtls->szRsaKeyId, curPtr+2, iLen-2); //Skip a encryption type of len 2N.
						pstEmvDtls->iEncType = RSA_ENC;
					}
					else
					{
						if(pstTrk != NULL) // CID 67340 (#1 of 1): Dereference after null check (FORWARD_NULL). T_RaghavendranR1. Added NULL Check.
						{
							memcpy(pstTrk->szRsaKeyId, curPtr+2, iLen-2); //Skip a encryption type of len 2N.
						}
					}

#ifdef DEVDEBUG
					if(iSource == 1)
					{
						debug_sprintf(szDbgMsg, "%s:%s: RSA KeyId = [%s]", DEVDEBUGMSG,
								__FUNCTION__, pstEmvDtls->szRsaKeyId);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						if(pstTrk != NULL) // CID 67340 (#1 of 1): Dereference after null check (FORWARD_NULL). T_RaghavendranR1. Added NULL Check.
						{
							debug_sprintf(szDbgMsg, "%s:%s: RSA KeyId = [%s]", DEVDEBUGMSG,
									__FUNCTION__, pstTrk->szRsaKeyId);
							APP_TRACE(szDbgMsg);
						}
					}
#endif
					/* Get the Data type returned.
					 * For example:
					 * 01 - Track Data
					 * 02 - EMV Data
					 * 03 - Manual Entry Data
					 */
					curPtr = nxtPtr + 1;
					nxtPtr = strchr(curPtr, FS);
					iLen = nxtPtr - curPtr;
					if(iLen > 0)
					{
						memset(szTmp, 0x00, sizeof(szTmp));
						memcpy(szTmp, curPtr, iLen);

						iDataType = atoi(szTmp);
						if( iDataType == 1 && iSource != 1)	//Need to parse track data
						{
							debug_sprintf(szDbgMsg, "%s: Data type returned [Track Data]", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							/* Get the ciphered data. i.e encryption blob1(trk1<GS>trk2) */
							curPtr = nxtPtr + 1;
							nxtPtr = strchr(curPtr, FS);
							if(nxtPtr != NULL)
							{
								iLen = nxtPtr - curPtr;

								if(iLen > 0 && (pstTrk != NULL))
								{
									memcpy(pstTrk->szRsaTrk2, curPtr, iLen);
#ifdef DEVDEBUG
									debug_sprintf(szDbgMsg, "%s:%s: Encryption blob1(Track2) = [%s]", DEVDEBUGMSG,
											__FUNCTION__, pstTrk->szRsaTrk2);
									APP_TRACE(szDbgMsg);
#endif
								}
							}

							/* Get the ciphered data. i.e encryption blob1(trk1<FS>trk2) */
							curPtr = nxtPtr + 1;
							nxtPtr = strchr(curPtr, FS);
							if(nxtPtr == NULL)
							{
								nxtPtr = strchr(curPtr, 0xC2);
								if(nxtPtr == NULL)
								{
									nxtPtr = strchr(curPtr, 0xE3);
								}
							}
							if(nxtPtr != NULL)
							{
								iLen = nxtPtr - curPtr;
								if(iLen > 0 && (pstTrk != NULL))
								{
									memcpy(pstTrk->szRsaTrk1, curPtr, iLen);
#ifdef DEVDEBUG
									debug_sprintf(szDbgMsg, "%s:%s: Encryption blob2(Track1) = [%s]", DEVDEBUGMSG,
											__FUNCTION__, pstTrk->szRsaTrk1);
									APP_TRACE(szDbgMsg);
#endif
								}
							}

/*							 Get the ciphered data. i.e encryption blob2(trk3)
							curPtr = nxtPtr + 1;
							if(curPtr != NULL)
							{
								nxtPtr = strchr(curPtr, ETX);
							}
							if(nxtPtr != NULL)
							{
								iLen = nxtPtr - curPtr;
								if(iLen > 0)
								{
									memcpy(pstTrk->szRsaTrk3, curPtr, iLen);

#ifdef DEVDEBUG
									debug_sprintf(szDbgMsg, "%s:%s: Encryption blob2(Track3) = [%s]", DEVDEBUGMSG,
											__FUNCTION__, pstTrk->szRsaTrk3);
									APP_TRACE(szDbgMsg);

#endif
								}
							}*/
						}
						else if(iDataType == 2 && iSource == 1)
						{
							debug_sprintf(szDbgMsg, "%s: Data type returned [EMV Data]", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							/* Get the ciphered data. i.e encryption blob1
							 * which contains data in the TLV format from the card
							 */
							curPtr = nxtPtr + 1;
							if(curPtr != NULL)
							{
								strcpy(pstEmvDtls->szEMVEncBlob, curPtr);
							}
#ifdef DEVDEBUG
							debug_sprintf(szDbgMsg, "%s:%s: Encryption blob1(EMV Tags) = [%s]", DEVDEBUGMSG,
									__FUNCTION__, pstEmvDtls->szEMVEncBlob);
							APP_TRACE(szDbgMsg);
#endif

/*							nxtPtr = strchr(curPtr, ETX);
							iLen = nxtPtr - curPtr;
							if(iLen > 0)
							{

								memcpy(pstEmvDtls->szEMVEncBlob, curPtr, iLen);

							}*/
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Data type returned [Manual Entry Data] Not  parsing as XPI is not for MANUAL Entry", __FUNCTION__);
							APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG

							debug_sprintf(szDbgMsg, "%s:%s: Encryption blob1(Manual Entry Data) = [%s]", DEVDEBUGMSG,
									__FUNCTION__, curPtr);
							APP_TRACE(szDbgMsg);
#endif
						}
					}
				}
			}
		}
		else if(!strcmp(szEncType, "01"))	//VSP encryption
		{
			debug_sprintf(szDbgMsg, "%s: Encryption Type is VSP, Not parsing for encryption blobs", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Encryption Type is None, Not parsing for encryption blobs", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTagValue
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getTagValue(char **strPtr, char *tagName, char *tagValue, int *tagLength)
{
	int		rv  = SUCCESS;
	int 	iTagLen		= 0;
	int		iLen		= 0;
	int		iCnt = 0;
	int		jCnt = 0;

	unsigned char	chTemp = 0;
	char	szTag[10+1]		= "";
	char	szTemp[10+1]		= "";
	unsigned char	szTmpName[10+1]		= "";
	char 	*curPtr		= NULL;
//	char	*nxtPtr		= NULL;
#if DEBUG
	char		szDbgMsg[256]	= "";
#endif
	while(1)
	{
		if(strPtr == NULL || *strPtr == NULL || tagLength == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		curPtr = *strPtr;

		iLen = strlen(tagName)/2;
		if(iLen > 0)
		{
			memcpy(szTmpName, curPtr, iLen);
		}

		//printf("%x%x%x\n",szTmpName[0],szTmpName[1],szTmpName[2]);
		hexToAscii(szTag, szTmpName, 2*iLen);

		
		// validate the tag name in the input buffer
		if(! strcasecmp(szTag, tagName))
		{
			curPtr += iLen;
			memset(szTemp, 0x00, sizeof(szTemp));

			// get the tag length
			szTemp[0] = *curPtr;
			if(szTemp[0] == 0x80)
			{
				//indefinite form of length. not supported yet
				debug_sprintf(szDbgMsg, "%s: Indefinite form of length", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				*tagLength  = 0;
				break;
			}
			else if(szTemp[0] & 0x80)
			{
				// lenght is composed of multiple octets
				iLen = szTemp[0] & 0x7F;
				curPtr += 1;

				if(iLen == 1)
				{
					iTagLen = *curPtr;
				}
				else
				{
					memset(szTemp, 0x00, sizeof(szTemp));
					memcpy(szTemp, curPtr, iLen);
					//
					for(iCnt = 0, jCnt = 0; iCnt < iLen; iCnt++, jCnt += 8)
					{
						chTemp = *(curPtr + iLen - iCnt - 1); // get single character at a time starting from last
						iTagLen += ( chTemp * (1 << jCnt) );
					}
				}
				curPtr += iLen;
			}
			else
			{
				// lenght is composed of single octet
				iTagLen = (int)szTemp[0];
				curPtr += 1;
			}
			if(iTagLen == 0)
			{
				*tagLength  = 0;;
				break;
			}
			// get the Tag value
			memcpy(tagValue, curPtr, iTagLen);
			curPtr += iTagLen;
			*tagLength = iTagLen;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Tag[%s] is not present in response data", __FUNCTION__,tagName);
			APP_TRACE(szDbgMsg);
			*tagLength  = 0;
		}

		*strPtr = curPtr;
		break;
	}
	return rv;
}
/*
 * ============================================================================
 * Function Name: storeCardData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeCardData( char *ptrTag, unsigned char *ptrValue, int len, CARD_TRK_PTYPE pstCard, int iTrkNumber)
{
	int 				rv 				= SUCCESS;
	//int					iTmp			= 0;
	char*				tmpPtr			= NULL;
	char 				szTmp[1024]		= "";
	char				szPayPassType[3]= "";
	//char				szTmpMonth[3]	= "";
	
#ifdef DEBUG
	char		szDbgMsg[2048]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s:----Enter-------", __FUNCTION__);
	APP_TRACE(szDbgMsg);	


	debug_sprintf(szDbgMsg, "%s: Got tag [%s]", __FUNCTION__, ptrTag);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( len <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No need to store", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;			
		}

		
		memset(szTmp, 0x00, sizeof(szTmp));	
		Char2Hex((char *)ptrValue, szTmp, len);


		//memcpy(szTmp, (char *)ptrValue, len);
		#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "Tag is %s Value is %s length is %d ", ptrTag, 
												(char *)ptrValue, len);
		APP_TRACE(szDbgMsg); 
		#endif

		if((!strcmp("C2", ptrTag)) && (iTrkNumber == 1))
		{
			//Skip Start Sentinel
			if(*ptrValue == ';')
			{
				ptrValue++;
				len--;
			}
			//Skip last sentinel and LRC from track2 data
			if( (tmpPtr = strchr((char *)ptrValue, '?')) )
			{
				len = tmpPtr - (char *)ptrValue;
			}
			memcpy(pstCard->szTrk2, (char *)ptrValue, len);
		}
		else if((!strcmp("C2", ptrTag)) && (iTrkNumber == 2))
		{
			//Skip Start Sentinel
			if(*ptrValue == '%')
			{
				ptrValue++;
				len--;
			}
			//Skip last sentinel and LRC from track2 data
			if( (tmpPtr = strchr((char *)ptrValue, '?')) )
			{
				len = tmpPtr - (char *)ptrValue;
			}
			memcpy(pstCard->szTrk1, (char *)ptrValue, len);
		}
		else if((!strcmp("C2", ptrTag)) && (iTrkNumber == 3))
		{
			//Skip Start Sentinel
			if(*ptrValue == ';')
			{
				ptrValue++;
				len--;
			}
			//Skip last sentinel and LRC from track2 data
			if( (tmpPtr = strchr((char *)ptrValue, '?')) )
			{
				len = tmpPtr - (char *)ptrValue;
			}
			memcpy(pstCard->szTrk3, (char *)ptrValue, len);
		}
		else if(!strcmp("C3",ptrTag))	//Apple Pay Tag
		{
			if(len > 0)
			{
				rv = parseApplePayDataResp(ptrValue, len, pstCard , NULL , 2);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "[%s]Error in Parsing Apple Pay Response", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "[%s] No C3 tag Present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(!strcmp("E3",ptrTag))	//Clear Exp Tag
		{
			if(len > 0)
			{
				//We need to parse the data starting with 5F24, If E3 data is not NULL, it will surely contains 5F24 tag
				rv = parseCardRespForClrExpDate((unsigned char *)ptrValue, len, pstCard->szClrExpDt);
				if(rv == SUCCESS && ((strlen(pstCard->szClrExpDt)) > 0) )
				{
					debug_sprintf(szDbgMsg, "%s: Clear expiry date is present!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Clear Expiry date is NOT present!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "[%s] E3 tag Present with No data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		//Clear Expiry will come in E3 Tag now, so removing below 5F24 parsing
#if 0
		else if(!strcmp(tagTwoByte_5F[APPLICATION_EXPIRY_DATE_5F24], ptrTag))
		{
			memcpy(pstCard->szClrExpDt, szTmp, len*2);
			debug_sprintf(szDbgMsg, "Got ClearExp[%s]", pstCard->szClrExpDt);
			APP_TRACE(szDbgMsg);

			memset(szTmpMonth, 0x00, 3);
			memcpy(szTmpMonth, (char*)(pstCard->szClrExpDt) + 2, 2);
			if(strspn(szTmpMonth, "1234567890") != 2)
			{
				iTmp = 100;//Setting as 100 as we need to remove clear expiry
			}
			else
			{
				iTmp = atoi(szTmpMonth);
			}
			if(iTmp < 1 || iTmp > 12)
			{
				debug_sprintf(szDbgMsg, "%s: Removing clear expiry from card dtls as XPI send incorrect TLV in 5F24",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				memset(pstCard->szClrExpDt, 0x00, len*2);
			}
		}
#endif
		else if(!strcmp(tagTwoByte_9F[POS_ENTRY_MODE_9F39], ptrTag))
		{
			pstCard->iCardSrc = atoi(szTmp);
			if(pstCard->iCardSrc == 2)
			{
				pstCard->iCardSrc = CRD_MSR;
			}
			else if(pstCard->iCardSrc == 91)
			{
				//pstCard->iCardSrc = CRD_EMV_MSD_CTLS; //Praveen_P1: Setting to RFID only
				pstCard->iCardSrc = CRD_RFID;
			}
			debug_sprintf(szDbgMsg, "Got CardSrc[%d]", pstCard->iCardSrc);
			APP_TRACE(szDbgMsg);			
		}
		else if(!strcmp(tagThreeByte[SERVICE_CODE_BYTE_ONE_9FA00B], ptrTag))
		{
			memcpy(pstCard->szServiceCodeByteOne, (char *)ptrValue, len);
			debug_sprintf(szDbgMsg, "Got Service Code Byte One [%s]", pstCard->szServiceCodeByteOne);
			APP_TRACE(szDbgMsg);
		}
		else if(!strcmp(tagTwoByte_9F[ICC_FORM_FACTOR_9F6E], ptrTag))
		{
			asciiToHex((unsigned char*)szPayPassType, (char*)szTmp + 8, 2);
			sprintf(pstCard->szPayPassType, "%d", atoi(szPayPassType));
			debug_sprintf(szDbgMsg, "Got Pay Pass Type [%s]", pstCard->szPayPassType);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "Tag %s Value is not stored", ptrTag);
			APP_TRACE(szDbgMsg);
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Return rv = [%d]",__FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;		
}

/*
 * ============================================================================
 * Function Name: parseEmvCardResp
 *
 * Description	: This function would parse the XPI response.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseEmvCardResp(char *InBuffer, int iBuffSize, int iCntSpclTag, EMVDTLS_PTYPE pstEmvDtls)
{
	int     				rv 			  		= SUCCESS;
	int						index				= 0;
	int 					iLengthParsed 		= 0;
	int						numBytesinCurrTAG	= 0;
	int						iEncType			= 0;
	char 					szTag[7]     		= "";
	char					szTmp[50]			= "";
	static char				szClearExpTag[25]	= "";
	char					szTmpEmvTags[1024]	= "";
	char 					szEmvTagsForCTLS[64][128];
	char					*szTmp2				= NULL;
	char 					*ptrTagValue		= NULL;
	char					*startTag			= NULL;
	char					*endTag				= NULL;
	PAAS_BOOL				bEmvCTLSEnabled		= isContactlessEmvEnabledInDevice();
	unsigned int 			tag;
	unsigned short  		Temp;
	unsigned short 			len;

#ifdef DEBUG
	char		szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	APP_TRACE_EX((char *) InBuffer, iBuffSize);	
#endif
	memset(pstEmvDtls->szEMVTags, 0x00, sizeof(pstEmvDtls->szEMVTags));
	memset(szEmvTagsForCTLS, 0x00, sizeof(szEmvTagsForCTLS));

	if(strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP) == 0)
	{
		memset(szClearExpTag, 0x00, sizeof(szClearExpTag));
	}

	while(iLengthParsed < iBuffSize)
	{
		/*Get first byte of the Tag Name. If first byte is of format xxx11111,
		 * then tag name contains more than one byte.
		 */
		tag = 0;
		startTag = InBuffer;
		tag = *(InBuffer++);
		iLengthParsed++;
		numBytesinCurrTAG = 1;

		if( (tag & 0x1F) == 0x1F )
		{
			do {
				tag = tag << 8;
				tag = tag | *(InBuffer++);
				iLengthParsed++;
				numBytesinCurrTAG++;
			} while((tag & 0x80) == 0x80);
			//last byte in Tag name should be of 0xxxxxxx format, for a multi-byte tag name.
		}

		memset(szTag, 0x00, sizeof(szTag));
		sprintf(szTag, "%X", tag);

		if(numBytesinCurrTAG > 3)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR: Tag More than three bytes", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		//AjayS2: Ignoring FS, Remove this if XPI fix
		if( strcmp(szTag, "1C") == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Got FS tag, Ignoring and moving to next Byte", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			continue;
		}

		//Getting Length
		/*If Length field is of format 0xxxxxxx, len is single byte, which represents len from 0 to 127
		 *If Length field is of format 1xxxxxxx, len is multi byte field.
		 *1st byte in multi-byte length field represents the number of bytes in length field.
		 */
		len= *InBuffer++;
		iLengthParsed += 1;
		if (len & 0x80 )
		{
			Temp= len & 0x7f;
			len= 0;
			while (Temp)
			{
				len *= 256;
				len += *InBuffer++;
				iLengthParsed += 1;
				Temp--; // Temp should decremented for exiting from the closed loop.
			}
		}

		/*For some Tags which are not standard EMV tags, like 9FA008, encrypted blobs etc,
		 * XPI is sending length in two bytes without BER-TLV format. For such cases,
		 * we are taking directly two bytes as length, since one byte is already copied,
		 * copying one more byte.
		 */
		//For Encrypted blobs, Tag C2 will be present in C31 response at second or third position
		if(		(!strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP) && (iCntSpclTag >= 1) && (!strcmp(szTag,tagOneByte[TAG_C2])))
			||  (!strcmp(szTag,tagThreeByte[SPIN_THE_BIN_DATA_9FA008]))
			)
		{
			len *= 256;
			len += *InBuffer++;
			iLengthParsed += 1;
		}

		//Allocating memory using malloc as we are calling this function again as recursive when we got E2 Tag
		if(ptrTagValue == NULL)
		{
			ptrTagValue = (char *) malloc (sizeof(char)* (len + 1));
			if( ptrTagValue == NULL)
			{
				debug_sprintf(szDbgMsg, "%s:memory allocation failure", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return FAILURE;
			}
		}

		// store the value
		memset(ptrTagValue, 0x00, (len + 1));

		memcpy(ptrTagValue, InBuffer, len);
		iLengthParsed += len;
		InBuffer += len;
		endTag = InBuffer;

		//Convert tag(Hex value) to ASCII string[for e.g., "0" is stores as "30" on szTag]
		memset(szTag, 0x00, sizeof(szTag));
		sprintf(szTag, "%X", tag);


		if(strcmp(szTag, "5A") && strcmp(szTag, "57"))
		{
			debug_sprintf(szDbgMsg, "%s: Tag [%s] Length [%d] Value [%s]", __FUNCTION__, szTag, len, ptrTagValue);
			APP_TRACE(szDbgMsg);
		}

		if(numBytesinCurrTAG == 1)
		{
			if(!strcmp(szTag,"E2") && ((!strcmp(pstEmvDtls->szEMVRespCmd, EMV_C33_RESP)) || (!strcmp(pstEmvDtls->szEMVRespCmd, EMV_C36_RESP))))
			{
				if(iCntSpclTag == 100)
				{
					debug_sprintf(szDbgMsg, "%s: ERROR in E2 Resp: Got E2 Tag in a E2 Tag Data", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				//Parse E2 Tag by calling parsing function again. Sending 100 to differentiate the caller function
				parseEmvCardResp(ptrTagValue, len, 100, pstEmvDtls);
			}
			else if(!strcmp(szTag,tagOneByte[TAG_C2]))
			{
				if(!strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					switch(iCntSpclTag)
					{
					case 0:

						pstEmvDtls->EMVlangSelctd = atoi((char *)ptrTagValue);
						debug_sprintf(szDbgMsg, "%s: Language ID send by card in [%d]", __FUNCTION__,pstEmvDtls->EMVlangSelctd);
						APP_TRACE(szDbgMsg);
						break;
					case 1:
					case 2:
						//2nd or/and 3rd C2 Tag in C31 response represents Encryption Blobs
						iEncType = getEncryptionType();

						memset(szTmp, 0x00, sizeof(szTmp));
						strncpy(szTmp, (char *)ptrTagValue, 3);

						if(iEncType == RSA_ENC)
						{
							if(strcmp(szTmp, "E07") == 0)
							{
								parseEmvCipheredDataResp((char*)ptrTagValue + 3, len - 3, NULL, pstEmvDtls, 1);
							}
							debug_sprintf(szDbgMsg, "%s: RSA Type", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
						else if(iEncType == VSP_ENC)
						{
							if(strcmp(szTmp, "E07") != 0)
							{
								parseEmvEParamsDataResp(startTag + 3 , endTag - startTag -3 , NULL, pstEmvDtls, 1);
							}
							debug_sprintf(szDbgMsg, "%s: VSP Type", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
						if(iEncType == VSD_ENC)
						{
							if(! strcmp(szTmp, "E07"))
							{
								parseVSDBlobDataResp((char*)ptrTagValue + 3, len - 3, NULL, pstEmvDtls, NULL, 1);
							}
							debug_sprintf(szDbgMsg, "%s: VSD Type", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: No Enc Type", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
						break;
					default:
						debug_sprintf(szDbgMsg, "%s: Should not come here as C2 count more than or equal to %d", __FUNCTION__, iCntSpclTag);
						APP_TRACE(szDbgMsg);
					}
					iCntSpclTag++;
				}
				else if(!strcmp(pstEmvDtls->szEMVRespCmd,EMV_C33_RESP))
				{
					switch(iCntSpclTag)
					{
					case 0:

						pstEmvDtls->bSignReqd = ( (*ptrTagValue == SOH)?PAAS_TRUE : PAAS_FALSE);
						debug_sprintf(szDbgMsg, "%s: Signature Flag from Card [%d]", __FUNCTION__, pstEmvDtls->bSignReqd);
						APP_TRACE(szDbgMsg);
						break;
					case 1:
						pstEmvDtls->EMVlangSelctd = atoi((char *)ptrTagValue);
						debug_sprintf(szDbgMsg, "%s: Language ID send by card in [%d]", __FUNCTION__,pstEmvDtls->EMVlangSelctd);
						APP_TRACE(szDbgMsg);
						break;
					default:
						debug_sprintf(szDbgMsg, "%s: Should not come here as C2 count more than or equal to %d NOT supported now.", __FUNCTION__,iCntSpclTag);
						APP_TRACE(szDbgMsg);
					}
					iCntSpclTag++;
				}
				else if(!strcmp(pstEmvDtls->szEMVRespCmd, EMV_C34_RESP))
				{
					switch(iCntSpclTag)
					{
					case 0:

						debug_sprintf(szDbgMsg, "%s: Number Of Issuer Script Results Present: [%s]", __FUNCTION__, ptrTagValue);
						APP_TRACE(szDbgMsg);

						//AjayS2: First C2 Tag shows the number of Issuer Script results
						//Char2Hex((char *)ptrTagValue, szTmpEmvTags, len);
						//strcpy(pstEmvDtls->stEmvIssuerDtls.szIssueScrptResults, szTmpEmvTags);
						break;
					default:
						debug_sprintf(szDbgMsg, "%s: Issuer Script result %d: [%s]", __FUNCTION__, iCntSpclTag, ptrTagValue);
						APP_TRACE(szDbgMsg);

						if(len + strlen(pstEmvDtls->stEmvIssuerDtls.szIssueScrptResults) > 199)
						{
							debug_sprintf(szDbgMsg, "%s:Issuer Script result Field OverFlow", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							break;
						}
						memset(szTmpEmvTags, 0x00, 1024);
						Char2Hex((char *)ptrTagValue, szTmpEmvTags, len);
						if(iCntSpclTag > 1)
						{
							strcat(pstEmvDtls->stEmvIssuerDtls.szIssueScrptResults, "|");
						}
						strcat(pstEmvDtls->stEmvIssuerDtls.szIssueScrptResults, szTmpEmvTags);
						break;
					}
					iCntSpclTag++;
				}
			}
			else if (!strcmp(szTag,tagOneByte[TAG_C3]))
			{
				if(!strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					debug_sprintf(szDbgMsg, "%s: Got C3 Tag", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(len > 0)
					{
						rv = parseApplePayDataResp((unsigned char *)ptrTagValue, len, NULL, pstEmvDtls ,1);
						if (rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: Error in parsing C3 Tag", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
					}
					else
					{
						debug_sprintf(szDbgMsg, "[%s] C3 tag Present with No Data", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
			}
			else if (!strcmp(szTag,"E3"))
			{
				if(!strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					debug_sprintf(szDbgMsg, "%s: Got E3 Tag", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(len > 0)
					{
						//We need to parse the data starting with 5F24, If E3 data is not NULL, it will surely contains 5F24 tag
						rv = parseCardRespForClrExpDate((unsigned char *)ptrTagValue, len, pstEmvDtls->stEmvAppDtls.szClearAppExpiryDate);
						if(rv == SUCCESS && ((strlen(pstEmvDtls->stEmvAppDtls.szClearAppExpiryDate)) > 0) )
						{
							debug_sprintf(szDbgMsg, "%s: Clear expiry date is present!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Clear Expiry date is NOT present!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
						sprintf(szClearExpTag, "%s%.2X%s", "5F24", strlen(pstEmvDtls->stEmvAppDtls.szClearAppExpiryDate)/2, pstEmvDtls->stEmvAppDtls.szClearAppExpiryDate);
						debug_sprintf(szDbgMsg, "%s: Stored Clear Exp EMVTag [%s]", __FUNCTION__, szClearExpTag);
						APP_TRACE(szDbgMsg);
						//AjayS2: Storing EMV Tag ClearExp in appropriate location, which will be used by EMV CTLS card
						if(bEmvCTLSEnabled && isClearExpiryToEmvHost())
						{
							storeTagsForEMVCTLS(szClearExpTag, "5F24", szEmvTagsForCTLS);
						}
					}
					else
					{
						debug_sprintf(szDbgMsg, "[%s] E3 tag Present with No data", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
			}
			else if(  (!strcmp(pstEmvDtls->szEMVRespCmd, EMV_C33_RESP))
					&&	( (!strcmp("30", szTag)) || (!strcmp("31", szTag)) || (!strcmp("32", szTag))  || (!strcmp("33", szTag)) )
					)
			{
				//PIN Response
				pstEmvDtls->stEmvtranDlts.PINEnteredStatus = tag - '0';
				debug_sprintf(szDbgMsg, "%s: PIN Status from card [%d]", __FUNCTION__, pstEmvDtls->stEmvtranDlts.PINEnteredStatus);
				APP_TRACE(szDbgMsg);
				if(pstEmvDtls->stEmvtranDlts.PINEnteredStatus == 3)
					pstEmvDtls->stEmvtranDlts.bPINByPassed = PAAS_TRUE;
				else
					pstEmvDtls->stEmvtranDlts.bPINByPassed = PAAS_FALSE;
			}
			else
			{
				rv = storeTagValue(szTag, ptrTagValue, len, 1, pstEmvDtls, startTag , endTag, szClearExpTag, szEmvTagsForCTLS);

				if( rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: NO SUCh tag: %s ", __FUNCTION__, szTag);
					APP_TRACE(szDbgMsg);
					break;
				}
			}
		}
		else if(numBytesinCurrTAG == 2)
		{
			if(!strcmp(szTag, tagTwoByte_9F[POS_ENTRY_MODE_9F39]))
			{
				Char2Hex((char *)ptrTagValue, szTmp, 2);
				strcpy(pstEmvDtls->stEmvtranDlts.szPOSEntryMode, szTmp);

				if(strstr(szTmp, "02") != NULL)
				{
					pstEmvDtls->iCardSrc = CRD_MSR;
				}
				else if(strstr(szTmp, "05") != NULL)
				{
					pstEmvDtls->iCardSrc = CRD_EMV_CT;
				}
				else if(strstr(szTmp, "07") != NULL)
				{
					pstEmvDtls->iCardSrc = CRD_EMV_CTLS;
				}
				else if(strstr(szTmp, "91") != NULL)
				{
					//pstEmvDtls->iCardSrc = CRD_EMV_MSD_CTLS; //Praveen_P1: Setting to RFID only
					pstEmvDtls->iCardSrc = CRD_RFID;

				}

				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, szTag, szEmvTagsForCTLS);
				}

			}
			else
			{
				if(strstr(szTag,"9F") != NULL)
				{
					rv = storeTagValue(szTag, ptrTagValue, len,  3, pstEmvDtls, startTag , endTag, szClearExpTag, szEmvTagsForCTLS);
				}
				else if(strstr(szTag,"5F") != NULL)
				{
					rv = storeTagValue(szTag, ptrTagValue, len,  2, pstEmvDtls, startTag , endTag, szClearExpTag, szEmvTagsForCTLS);
				}
				if( rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: NO SUCh tag: %s ", __FUNCTION__, szTag);
					APP_TRACE(szDbgMsg);
					break;
				}
			}
		}
		else if(numBytesinCurrTAG == 3)
		{
			if(!strcmp(szTag,tagThreeByte[KEY_SERIAL_AND_PIN_BLOCK_9FA005]))
			{
				index = 0;

				strcpy(szTmp,ptrTagValue);
				szTmp2 = strchr(szTmp, FS);
				index = szTmp2 - szTmp;
				if(index > 0)
				{
					memcpy(pstEmvDtls->stEmvtranDlts.szKeySerialNum, szTmp, index);
					memcpy(pstEmvDtls->stEmvtranDlts.szPINBlock, szTmp2 + 1, 16);
				}
			}
			else if(!strcmp(szTag,tagThreeByte[CVM_METHOD_CHOSEN_9FA006]))
			{
				index = atoi((char*)ptrTagValue);
				if(index == 0x01)
				{
					pstEmvDtls->stEmvtranDlts.CVMTpeChosen = EMV_ONLINE_ENCIPHERED_PIN;
				}
				else if(index == 0x02)
				{
					pstEmvDtls->stEmvtranDlts.CVMTpeChosen = EMV_OFFLINE_PLAIN_TEXT;
				}
				else if(index == 0x03)
				{
					pstEmvDtls->stEmvtranDlts.CVMTpeChosen = EMV_OFFLINE_ENCIPHERED;
				}
				else if(index == 0x04)
				{
					pstEmvDtls->stEmvtranDlts.CVMTpeChosen = EMV_SIGNATURE;
				}
				else if(index == 0x05)
				{
					pstEmvDtls->stEmvtranDlts.CVMTpeChosen = EMV_NO_CVM;
				}
			}
			else if(!strcmp(szTag,tagThreeByte[CARD_STATUS_ON_XPI_9FA007]))
			{
				if(ptrTagValue[0] == 'I')
				{
					pstEmvDtls->stEMVCardStatusU02.bCardInserted = PAAS_TRUE;
				}
				else if(ptrTagValue[0] == 'S')
				{
					pstEmvDtls->stEMVCardStatusU02.bCardSwiped = PAAS_TRUE;
				}
				else if(ptrTagValue[0] == 'T')
				{
					pstEmvDtls->stEMVCardStatusU02.bCardTapped = PAAS_TRUE;
				}

				if(ptrTagValue[1] == 'L')
				{
					pstEmvDtls->stEMVCardStatusU02.bLangSelScreen = PAAS_TRUE;
				}

				if(ptrTagValue[2] == 'A')
				{
					pstEmvDtls->stEMVCardStatusU02.bAppSelScreen = PAAS_TRUE;
				}

				if(ptrTagValue[3] == 'P')
				{
					pstEmvDtls->stEMVCardStatusU02.bEMVPINEntry = PAAS_TRUE;
				}

				if(ptrTagValue[len-1] == 'R')
				{
					pstEmvDtls->stEMVCardStatusU02.bEMVRetry = PAAS_TRUE;
				}
			}
			else if(!strcmp(szTag,tagThreeByte[SPIN_THE_BIN_DATA_9FA008]))
			{
				memset(pstEmvDtls->szSpinBinData, 0x00, sizeof(pstEmvDtls->szSpinBinData));
				strcpy(pstEmvDtls->szSpinBinData, (char*)ptrTagValue);

				pstEmvDtls->stEMVCardStatusU02.bSTBDataReceived = PAAS_TRUE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Total Bytes in Tag name more than three", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		if(ptrTagValue != NULL)
		{
			free(ptrTagValue);
			ptrTagValue = NULL;
		}
	}

	if(strcmp(pstEmvDtls->szEMVRespCmd, EMV_C33_RESP) == 0)
	{
		memset(szClearExpTag, 0x00, sizeof(szClearExpTag));
	}

	if(bEmvCTLSEnabled && strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP) == 0)
	{
		index = 0;
		memset(pstEmvDtls->szEMVTags, 0x00, sizeof(pstEmvDtls->szEMVTags));
		while(index < 64)
		{
			if(strlen(szEmvTagsForCTLS[index]) > 0 && pstEmvDtls->iCardSrc == CRD_EMV_CTLS)
			{
				strcat(pstEmvDtls->szEMVTags, szEmvTagsForCTLS[index]);
			}
			memset(szEmvTagsForCTLS[index], 0x00, sizeof(szEmvTagsForCTLS[index]));
			index++;
		}
	}

	if(ptrTagValue != NULL)
	{
		free(ptrTagValue);
		ptrTagValue = NULL;
	}

	debug_sprintf(szDbgMsg,"%s: Returning rv =%d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;	
}

/*
 * ============================================================================
 * Function Name: storeTagValue
 *
 * Description	: This function will Store a tag value to the Emvdtls Struct
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeTagValue(char *ptrTag, char *ptrValue, int len, int typeofTag,
												EMVDTLS_PTYPE pstEmvDtls, char *startTag, char *endTag, char *pszClearExpTag, char pszEmvTagsForCTLS[64][128])
{
	int 				rv 					= SUCCESS;
	int					iLen				= 0;
	int					iTempLen			= 0;
	char 				szTmp[500]			= "";
	char				szTmpEmvTags[500]	= "";
	char *				curPtr				= NULL;
	char *				nxtPtr				= NULL;
	PAAS_BOOL			bEmvCTLSEnabled		= isContactlessEmvEnabledInDevice();
	
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

#if DEVDEBUG
	debug_sprintf(szDbgMsg, "%s:----Enter-------", __FUNCTION__);
	APP_TRACE(szDbgMsg);
#endif

	
	while(1)
	{
		if( len <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No need to store", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;			
		}
		
		memset(szTmp, 0x00, sizeof(szTmp));	
		Char2Hex((char *)ptrValue, szTmp, len);
		//memcpy(szTmp, (char *)ptrValue, len);
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "Tag is %s Value is %s length is %d ", ptrTag, szTmp, len*2);
		APP_TRACE(szDbgMsg); 
#endif
		len = len*2;

		switch(typeofTag)
		{
		case 1:
			if(!strcmp(tagOneByte[APPLICATION_FILE_LOCATOR_94], ptrTag))
			{
				debug_sprintf(szDbgMsg, "%s: APPLICATION_FILE_LOCATOR_94: No need to store", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(!strcmp(tagOneByte[AID_4F], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvAppDtls.szAID, szTmp, len);
				//AjayS2: 20/Jan/2016: For EMV CTLS we have to send 4F as 9F06
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					memset(szTmpEmvTags, 0x00, sizeof(szTmpEmvTags));
					sprintf(szTmpEmvTags, "%s%.2d%s", "9F06", strlen(pstEmvDtls->stEmvAppDtls.szAID)/2, pstEmvDtls->stEmvAppDtls.szAID);
					storeTagsForEMVCTLS(szTmpEmvTags, "9F06", pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagOneByte[AIP_82], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvAppDtls.szAppIntrChngProfile, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagOneByte[APPLICATION_PAN_5A], ptrTag))
			{

				/* If PAN was copied previously using the 57 Tag, then reset that and store the value sent by the 5A Tag */
				if(strlen(pstEmvDtls->szPAN) > 0)
				{
					memset(pstEmvDtls->szPAN,0x00,sizeof(pstEmvDtls->szPAN));
				}
				memcpy(pstEmvDtls->szPAN, szTmp, len);
				/* KranthiK1: 
				 * Adding this as EMV adds F at the end for the odd pan length
				 * Need to remove that F before copying that data
				 */
				iTempLen = len;
				while(iTempLen > 0)
				{
					if(pstEmvDtls->szPAN[--iTempLen] == 'F')
					{
						pstEmvDtls->szPAN[iTempLen] = '\0';
					}
				}
			}
			else if(!strcmp(tagOneByte[AUTH_CODE_89], ptrTag))
			{
				debug_sprintf(szDbgMsg, "%s: AUTH_CODE_89: No need to store", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(!strcmp(tagOneByte[AUTH_RESP_CODE_8A], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvtranDlts.szAuthRespCode, (char*)ptrValue, len/2);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagOneByte[CARD_CVM_LIST], ptrTag))
			{
				//Daivik:16/9/2016 - We were including even tag name and length when saving CVM list, but we need value only
				//Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				//strcpy(pstEmvDtls->stEmvAppDtls.szIccCVMList, szTmpEmvTags);
				memcpy(pstEmvDtls->stEmvAppDtls.szIccCVMList, szTmp, len);

			}
			else if(!strcmp(tagOneByte[DEDICATED_FILE_NAME_84], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvtranDlts.szDedicatedFileName, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagOneByte[ISSUER_ID_42], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvIssuerDtls.szIssuerID, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagOneByte[ISSUER_AUTH_DATA_91], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvIssuerDtls.szIssuerAuthData, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagOneByte[TERMINAL_VERIF_RESULTS_95], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvTerDtls.szTermVerificationResults, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagOneByte[TRACK2_57], ptrTag))
			{
				//Replace Field Separator 'D' by '='
				curPtr = szTmp;
				nxtPtr = strchr(curPtr, 'D');
				if(nxtPtr != NULL)
				{
					iLen   = nxtPtr - curPtr;
					szTmp[iLen] = '=';
				}
				//Skip start sentinel character[at the begining(;) and end('F')] in track2 data.
				curPtr = szTmp;
				nxtPtr = strchr(curPtr, 'F');
				if(nxtPtr != NULL)
				{
					memcpy(pstEmvDtls->szTrackData, szTmp, len-1);
				}
				else
				{
					memcpy(pstEmvDtls->szTrackData, szTmp, len);
				}
				/* Daivik : 17/2/2016 - This is required because in some cases we dont recieve the PAN when the Tag 5A is not present in the card.
				 * So we are extracting it from the Track Data and storing it in PAN field. If we recieve it from Tag 5A again , then we will overwrite this value.
				 *
				 */
				if(!(strlen(pstEmvDtls->szPAN) > 0))
				{
					curPtr = szTmp;
					nxtPtr = strchr(curPtr, '=');

					if(nxtPtr != NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Track received and 5A not present yet , must store PAN ", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						memcpy(pstEmvDtls->szPAN, curPtr, nxtPtr - curPtr);
					}

				}
			}
			else if(!strcmp(tagOneByte[TRANS_DATE_9A], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvtranDlts.szTermTranDate, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagOneByte[TRANS_STATUS_INFO_9B], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvtranDlts.szTranStatusInfo, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagOneByte[TRANS_TYPE_9C], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmTranType, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagOneByte[APPLICATION_LABEL_50], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvAppDtls.szAppLabel, (char*)ptrValue, len/2);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else
			{
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			break;

		case 2:
			if(!strcmp(tagTwoByte_5F[APPLICATION_EFFECTIVE_DATE_5F25], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvAppDtls.szAppEffctiveDate, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_5F[APPLICATION_EXPIRY_DATE_5F24], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvAppDtls.szEncAppExpiryDate, szTmp, len);
				//AjayS2: Adding Clear Expiry or Enc Expiry based on clearexpirytoemvhost flag now
				if(isClearExpiryToEmvHost() == PAAS_FALSE)
				{
					Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
					strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
					if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
					{
						storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
					}
				}
				else if(pszClearExpTag != NULL && strlen(pszClearExpTag) > 0)
				{
					//If Clear Expiry needs to send to Host, so for EMV CTLS, we will populate when we got E3 tag
					strcat(pstEmvDtls->szEMVTags, pszClearExpTag);
				}
			}
			else if(!strcmp(tagTwoByte_5F[APPLICATION_PAN_SEQ_NUM_5F34], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvAppDtls.szAppSeqNum, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_5F[CARDHOLDER_NAME_5F20], ptrTag))
			{
				memcpy(pstEmvDtls->szName, (char*)ptrValue, len/2);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_5F[LANG_PREF_5F2D], ptrTag))
			{
				memcpy(pstEmvDtls->szEMVlangPref, (char*)ptrValue, len/2);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_5F[SERVICE_CODE_5F30], ptrTag))
			{
				memcpy(pstEmvDtls->szServiceCode, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_5F[CRYPTOGRM_CURR_CODE_5F2A], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmCurrCode, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else
			{
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			break;
		case 3:
			if(!strcmp(tagTwoByte_9F[AMOUNT_AUTHORISED_9F02], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvtranDlts.szAuthAmnt, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[APPLICATION_PREF_NAME_9F12], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvAppDtls.szAppPrefName, (char*)ptrValue, len/2);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[APPLICATION_USAGE_CONTROL_9F07], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvAppDtls.szAppUsageControl, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[AMOUNT_OTHER_9F03], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvtranDlts.szEMVCashBackAmnt, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[ICC_FORM_FACTOR_9F6E], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvtranDlts.szICCFormFactor, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[APPLICATION_CRYPTGRM_9F26], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvCryptgrmDtls.szAppCryptgrm, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[TERMINAL_AID_9F06], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvTerDtls.szTermAID, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[APPLICATION_TRAN_COUNTER_9F36], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvAppDtls.szAppTranCounter, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[APPLICATION_VERSION_NUM_9F09], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvAppDtls.szAppVersNum, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[APPLICATION_ICC_VERSION_NUM_9F08], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvAppDtls.szICCAppVersNum, szTmp, len);
			}
			else if(!strcmp(tagTwoByte_9F[CVM_RESULTS_9F34], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvtranDlts.szCVMResult, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[CRYPTGRM_INFO_DATA_9F27], ptrTag))
			{
				/* Daivik: Interac Function 21 Test requires the terminal to terminate the transaction
				 * when the card returns an AAR ( C0 ). Hence we are considering this as 00 ( AAC ) and the
				 * transaction will be offline declined.
				 */
				if(szTmp[0] == 'C')
				{
					debug_sprintf(szDbgMsg, "%s: Unknown Cryptogram :%s Change to 00(AAC)",__FUNCTION__,szTmp);
					APP_TRACE(szDbgMsg);
					memset(szTmp, 0x00, sizeof(szTmp));
					strcpy(szTmp,"00");
				}
				memcpy(pstEmvDtls->stEmvCryptgrmDtls.szCryptgrmInfoData, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[DEVICE_SERIAL_NUM_9F1E], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvTerDtls.szDevSerialNum, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[ISSUER_APP_DATA_9F10], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvIssuerDtls.szIssuerAppData, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[TERM_CAPABILITIES_9F33], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvTerDtls.szTermCapabilityProf, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[TERMINAL_TRAN_QUALIFIER_9F66], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvtranDlts.szTermTranQualfr, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[TERM_TRANS_CODE_9F1A], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvtranDlts.szTermTranCode, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[TERM_TYPE_9F35], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvTerDtls.szTermType, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[TRANS_SEQ_COUNTER_9F41], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvtranDlts.szTermTranSeqCount, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[EMV_TRANS_TIME_9F21], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvtranDlts.szTranTime, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[TRANSACTION_CATEGORY_CODE_9F53], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvtranDlts.szTranCategoryCode, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[UNPREDICTBLE_NUM_9F37], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvCryptgrmDtls.szUnprdctbleNum, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[ISSUER_SCRIPT_RESULTS_9F5B], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvIssuerDtls.szIssueScrptResults, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[CUSTOMER_EXCLUSIVE_DATA_9F7C], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvIssuerDtls.szCustExclData, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[IAC_DEFAULT_9F0D], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvIssuerDtls.szIACDefault, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[IAC_DENIAL_9F0E], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvIssuerDtls.szIACDenial, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[IAC_ONLINE_9F0F], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvIssuerDtls.szIACOnline, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else if(!strcmp(tagTwoByte_9F[ISSUER_CODE_TABLE_INDEX_9F11], ptrTag))
			{
				memcpy(pstEmvDtls->stEmvIssuerDtls.szIssuerCodeTableIndex, szTmp, len);
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			else
			{
				Char2Hex((char *)startTag, szTmpEmvTags, endTag - startTag);
				strcat(pstEmvDtls->szEMVTags, szTmpEmvTags);
				if(bEmvCTLSEnabled && !strcmp(pstEmvDtls->szEMVRespCmd, EMV_C31_RESP))
				{
					storeTagsForEMVCTLS(szTmpEmvTags, ptrTag, pszEmvTagsForCTLS);
				}
			}
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Tag: %s: No need to store", __FUNCTION__, ptrTag);
			APP_TRACE(szDbgMsg);
		}
		break;
	}
#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s: Returning rv = [%d]",__FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
#endif
	return rv;		
}

/*This function will convert a character buffer into two byte Hex buff*/
extern void  Char2Hex(char *inBuff, char *outBuff, int iLen)
{
	char temp[2 + 1];
	char chHex[] = "0123456789ABCDEF\0";
	int i = 0, iDec;
	while(i < iLen)
	{
		iDec = inBuff[i++];
		memset(temp, 0x00, sizeof(temp));

		temp[1] = chHex[iDec%16];
		iDec = iDec/16;
		temp[0] = chHex[iDec%16];

		strcat(outBuff, temp);
	}
}

/*
 * ============================================================================
 * Function Name: parseAndStoreHostEMVResponse
 *
 * Description	: This function will Parse the Host EMV_TAGS_RESPONSE and
 * 					store the 8A Tag if present, and search for 71 and/or 72
 * 					Tags for removing 9F18 Tag
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int parseAndStoreHostEMVResponse(EMV_RESP_DTLS_PTYPE pstEmvRespDtls, char* szEMvResp)
{
	int     				rv 			  		= SUCCESS;
	int						iAscIIBuffSize		= 0;
	int 					iLengthParsed 		= 0;
	int 					iLengthToReduce		= 0;
	int						iLenOffset			= 0;
	char 					szTag[7]     		= "";
	char					*ascIIBuffer		= NULL;
	char 					*ptrTagValue		= NULL;
	char					*pszStartEmvResp	= NULL;
	char					*pszStartEmvtag		= NULL;
	char 					*pszTo				= NULL;
	char					*pszFrom			= NULL;
	unsigned int 			tag;
	unsigned short  		Temp;
	unsigned short 			len;

#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);


	if(szEMvResp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s:NULL Host EMV Resp Passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	ascIIBuffer = (char *) malloc (sizeof(char)* 2048);
	if( ascIIBuffer == NULL)
	{
		debug_sprintf(szDbgMsg, "%s:memory allocation failure", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(ascIIBuffer, 0x00, 2048);
	iAscIIBuffSize = Hex2Bin((unsigned char *)szEMvResp, strlen(szEMvResp)/2, (unsigned char *)ascIIBuffer);

	if(iAscIIBuffSize <= 0)
	{
		debug_sprintf(szDbgMsg, "%s:Unable to Parse EMV_Tags Reseting all tags card will reject", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		memset(pstEmvRespDtls, 0x00, sizeof(EMV_RESP_DTLS_STYPE));
		// CID-67399: 25-Jan-16: MukeshS3: need to free the allocated memory for ascIIBuffer, if failed to parse the emv tags.
		if(ascIIBuffer != NULL)
		{
			free(ascIIBuffer);
		}
		return SUCCESS;
	}

#ifdef DEVDEBUG
	APP_TRACE_EX((char *) ascIIBuffer, iAscIIBuffSize);
#endif


	//storing start for freeing later
	pszStartEmvResp = ascIIBuffer;

	while(iLengthParsed < iAscIIBuffSize)
	{
		pszStartEmvtag = ascIIBuffer;
		tag = 0;
		tag = *ascIIBuffer++;
		iLengthParsed++;

		if( (tag & 0x1F) == 0x1F )// tag value is atleast 2 bytes
		{
			do {
				tag = tag << 8;
				tag = tag | *ascIIBuffer++;
				iLengthParsed++;
			} while((tag & 0x80) == 0x80);
		}
		debug_sprintf(szDbgMsg, "%s: Got TAG %d[%x] to Read", __FUNCTION__, tag, tag);
		APP_TRACE(szDbgMsg);

		//Getting Length
		len= *ascIIBuffer++;
		iLengthParsed += 1;
		if (len & 0x80)
		{
			Temp= len & 0x7f;
			len= 0;
			while (Temp)
			{
				len *= 256;
				len += *ascIIBuffer++;
				iLengthParsed += 1;
				Temp--; // Temp should decremented for exiting from the closed loop.
			}
		}

		ptrTagValue = NULL;
		ptrTagValue = ascIIBuffer;

		iLengthParsed += len;
		ascIIBuffer += len;
		memset(szTag, 0x00, sizeof(szTag));
		sprintf(szTag, "%X", tag);
		debug_sprintf(szDbgMsg, "%s: Got tag [%s] len [%d]", __FUNCTION__, szTag, len);
		APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	APP_TRACE_EX((char *) ptrTagValue, len);
#endif

		if(!strcmp(szTag,"8A"))
		{
			memcpy(pstEmvRespDtls->szAuthRespCode, ptrTagValue, len);
			pstEmvRespDtls->iAuthRespCodeLen = len;
		}
		else if(strcmp(szTag,"71") == 0 || strcmp(szTag,"72") == 0)
		{
			iLenOffset = 0;
			if( (iLengthToReduce = search9F18TagandModifyLength(pszStartEmvResp, &iAscIIBuffSize,  &ascIIBuffer, &iLengthParsed, &iLenOffset, (unsigned char*)ptrTagValue, len, &pszFrom, &pszTo)) > 0)
			{
				if(len < 128)
				{
					*(pszStartEmvtag + 1)= *(pszStartEmvtag + 1) - iLengthToReduce - iLenOffset;

					memcpy(pszFrom, pszTo, iAscIIBuffSize - (pszTo - (char *)pszStartEmvResp));
					ascIIBuffer -= iLengthToReduce;
					iAscIIBuffSize -= iLengthToReduce;
					iLengthParsed -= iLengthToReduce;
				}
				else if( len >= 128 && len < 256)
				{
					if(len - iLengthToReduce >= 128)
					{
						*(pszStartEmvtag + 2)= *(pszStartEmvtag + 2) - iLengthToReduce;
					}
					else
					{
						//Now len of 71 or 72 Tag can be accommodated in 1 byte field
						*(pszStartEmvtag + 2)= *(pszStartEmvtag + 2) - iLengthToReduce - iLenOffset;
						memcpy(pszStartEmvtag + 1, pszStartEmvtag + 2, iAscIIBuffSize - (pszStartEmvtag - pszStartEmvResp) - 1);
						ascIIBuffer -= 1;
						iAscIIBuffSize -= 1;
						iLengthParsed -= 1;
						pszFrom -= 1;
						pszTo -= 1;
					}

					memcpy(pszFrom, pszTo, iAscIIBuffSize - (pszTo - (char *)pszStartEmvResp));
					ascIIBuffer -= iLengthToReduce;
					iAscIIBuffSize -= iLengthToReduce;
					iLengthParsed -= iLengthToReduce;
				}
				else
				{
					if(len - iLengthToReduce >= 256)
					{
						len = len - iLengthToReduce;
						*(pszStartEmvtag + 2) = len >> 8;
						*(pszStartEmvtag + 3) = len & 0xFF;
					}
					else
					{
						//Now len of 71 or 72 Tag can be accommodated in 2 bytes field
						len = len - iLengthToReduce - iLenOffset;
						*(pszStartEmvtag + 3) = len;
						*(pszStartEmvtag + 1) = 0x81;
						memcpy(pszStartEmvtag + 2, pszStartEmvtag + 3, iAscIIBuffSize - (pszStartEmvtag - pszStartEmvResp) - 1);
						ascIIBuffer -= 1;
						iAscIIBuffSize -= 1;
						iLengthParsed -= 1;
						pszFrom -= 1;
						pszTo -= 1;
					}
					memcpy(pszFrom, pszTo, iAscIIBuffSize - (pszTo - (char *)pszStartEmvResp));
					ascIIBuffer -= iLengthToReduce;
					iAscIIBuffSize -= iLengthToReduce;
					iLengthParsed -= iLengthToReduce;
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Not 8A, 71 or 72 not parsing", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		debug_sprintf(szDbgMsg, "%s: iAscIIBuffSize[%d], iLengthParsed[%d]", __FUNCTION__, iAscIIBuffSize, iLengthParsed);
		APP_TRACE(szDbgMsg);
	}

	memcpy(pstEmvRespDtls->szEMVRespTags, pszStartEmvResp, iAscIIBuffSize);
	pstEmvRespDtls->iEMVRespTagsLen = iAscIIBuffSize;

	free(pszStartEmvResp);

//Testing AJAYS2
#ifdef DEVDEBUG
	APP_TRACE_EX((char *) pstEmvRespDtls->szEMVRespTags, iAscIIBuffSize);
	debug_sprintf(szDbgMsg, "%s: Got EMVResp %s len %d and value: %s", __FUNCTION__, "szAuthRespCode", pstEmvRespDtls->iAuthRespCodeLen, pstEmvRespDtls->szAuthRespCode);
	APP_TRACE(szDbgMsg);
#endif

	debug_sprintf(szDbgMsg, "%s: ---Returning----[%d]", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateEmvTagsForCardPresentVoid
 *
 * Description	: This API would add/modify the tags need to send in Card Present Void
 *
 * Input Params	: Card dtls and Tags
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateEmvTagsForCardPresentVoid(char* pszEmvTags, CARDDTLS_PTYPE pstCardDtls)
{
	int     				rv 			  		= SUCCESS;
	int						iAscIIBuffSize		= 0;
	int 					iLengthParsed 		= 0;
	char 					szTag[7]     		= "";
	char 					szTmp[200]     		= "";
	char					*ascIIBuffer		= NULL;
	char 					*ptrTagValue		= NULL;
	char					*pszStartEmvtag		= NULL;
	unsigned int 			tag;
	unsigned short  		Temp;
	unsigned short 			len;

#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);


	if(pszEmvTags == NULL)
	{
		debug_sprintf(szDbgMsg, "%s:NULL EMV Tags Passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	ascIIBuffer = (char *) malloc (sizeof(char)* 2048);
	if( ascIIBuffer == NULL)
	{
		debug_sprintf(szDbgMsg, "%s:memory allocation failure", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(ascIIBuffer, 0x00, 2048);
	iAscIIBuffSize = Hex2Bin((unsigned char *)pszEmvTags, strlen(pszEmvTags)/2, (unsigned char *)ascIIBuffer);

	if(iAscIIBuffSize <= 0)
	{
		debug_sprintf(szDbgMsg, "%s:Unable to Parse EMV_Tags", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		// CID-67443: 25-Jan-16: MukeshS3: Need to free the allocated memory for ascIIBuffer
		if(ascIIBuffer != NULL)
		{
			free(ascIIBuffer);
		}
		return FAILURE;
	}

#ifdef DEVDEBUG
	APP_TRACE_EX((char *) ascIIBuffer, iAscIIBuffSize);
#endif


	//storing start for freeing later
	pszStartEmvtag = ascIIBuffer;

	while(iLengthParsed < iAscIIBuffSize)
	{
		tag = 0;
		tag = *ascIIBuffer++;
		iLengthParsed++;

		if( (tag & 0x1F) == 0x1F )// tag value is atleast 2 bytes
		{
			do {
				tag = tag << 8;
				tag = tag | *ascIIBuffer++;
				iLengthParsed++;
			} while((tag & 0x80) == 0x80);
		}

		//Getting Length
		len= *ascIIBuffer++;
		iLengthParsed += 1;
		if (len & 0x80)
		{
			Temp= len & 0x7f;
			len= 0;
			while (Temp)
			{
				len *= 256;
				len += *ascIIBuffer++;
				iLengthParsed += 1;
				Temp--; // Temp should decremented for exiting from the closed loop.
			}
		}

		ptrTagValue = NULL;
		ptrTagValue = ascIIBuffer;

		iLengthParsed += len;
		ascIIBuffer += len;
		memset(szTag, 0x00, sizeof(szTag));
		sprintf(szTag, "%X", tag);
		debug_sprintf(szDbgMsg, "%s: Got tag [%s] len [%d]", __FUNCTION__, szTag, len);
		APP_TRACE(szDbgMsg);

		if(!strcmp(szTag, tagOneByte[TERMINAL_VERIF_RESULTS_95]))
		{
			memcpy((char*)(pszEmvTags + (iLengthParsed - len)*2), pstCardDtls->stEmvTerDtls.szTermVerificationResults, len*2);
		}
		else if(!strcmp(szTag, tagOneByte[TRANS_STATUS_INFO_9B]))
		{
			memcpy((char*)(pszEmvTags + (iLengthParsed - len)*2), pstCardDtls->stEmvtranDlts.szTranStatusInfo, len*2);
		}
		else if(!strcmp(szTag, tagTwoByte_9F[CRYPTGRM_INFO_DATA_9F27]))
		{
			memcpy((char*)(pszEmvTags + (iLengthParsed - len)*2), pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData, len*2);
		}
		else if(!strcmp(szTag, tagTwoByte_9F[APPLICATION_CRYPTGRM_9F26]))
		{
			memcpy((char*)(pszEmvTags + (iLengthParsed - len)*2), pstCardDtls->stEmvCryptgrmDtls.szAppCryptgrm, len*2);
		}
		else if(!strcmp(szTag, tagTwoByte_9F[APPLICATION_TRAN_COUNTER_9F36]))
		{
			memcpy((char*)(pszEmvTags + (iLengthParsed - len)*2), pstCardDtls->stEmvAppDtls.szAppTranCounter, len*2);
		}
		else if(!strcmp(szTag, tagTwoByte_9F[ISSUER_APP_DATA_9F10]))
		{
			memcpy((char*)(pszEmvTags + (iLengthParsed - len)*2), pstCardDtls->stEmvIssuerDtls.szIssuerAppData, len*2);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Not Updating", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	free(pszStartEmvtag);

	if(strlen(pstCardDtls->stEmvIssuerDtls.szIssueScrptResults) > 0)
	{
		memset(szTmp, 0x00, sizeof(szTmp));
		memset(szTag, 0x00, sizeof(szTag));
		/*AjayS2: TODO: Currently sending only one Issuer Script Results 9F5B, to Host
		 * for Card Present Void, Need to Rework here nad add all 9F5B tags as per Host
		 */
		ptrTagValue = strstr(pstCardDtls->stEmvIssuerDtls.szIssueScrptResults, "|");
		if(ptrTagValue != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: More than one 9F5B Present, Currently sending only first one, Need to rework", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strncpy(szTmp, pstCardDtls->stEmvIssuerDtls.szIssueScrptResults, ptrTagValue - pstCardDtls->stEmvIssuerDtls.szIssueScrptResults);
		}
		else
		{
			strcpy(szTmp, pstCardDtls->stEmvIssuerDtls.szIssueScrptResults);
		}
		strcat(pszEmvTags, "9F5B");
		sprintf(szTag, "%.2X", strlen(szTmp)/2);
		strcat(pszEmvTags, szTag);
		strcat(pszEmvTags, szTmp);
	}
	debug_sprintf(szDbgMsg, "%s: EMV_TAGS [%s]", __FUNCTION__, pszEmvTags);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: ---Returning----[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: search9F18TagandModifyLength
 *
 * Description	: This function  will remove the 9F18 Tags if present
 * 					by searching recursively for 71 or 72 Tags
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int search9F18TagandModifyLength(char * pszEmvStart, int *iEmvTotLen, char ** ascIIBuffMain, int * iLenParPrev, int *iLenOffset, unsigned char * pszBuff, int iBuffSize, char ** from, char **to)
{
	int 				rv 					= SUCCESS;
	int 				iLengthParsed 		= 0;
	int					len					= 0;
	char 				szTag[7]     		= "";
	unsigned int 		tag					= 0;
	unsigned short  	Temp				= 0;
	unsigned char 		*tagstart			= NULL;
	unsigned char		*tagValue			= NULL;

#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Entering----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(iLengthParsed < iBuffSize)
	{
		tagstart = pszBuff;
		tag = 0;
		tag = *pszBuff++;
		iLengthParsed++;

		if( (tag & 0x1F) == 0x1F )// tag value is atleast 2 bytes
		{
			do {
				tag = tag << 8;
				tag = tag | *pszBuff++;
				iLengthParsed++;
			} while((tag & 0x80) == 0x80);
		}

		//Getting Length
		len= *pszBuff++;
		iLengthParsed += 1;
		if (len & 0x80)
		{
			Temp= len & 0x7f;
			len= 0;
			while (Temp)
			{
				len *= 256;
				len += *pszBuff++;
				iLengthParsed += 1;
				Temp--; // Temp should decremented for exiting from the closed loop.
			}
		}

		tagValue = pszBuff;
		iLengthParsed += len;
		pszBuff += len;
		memset(szTag, 0x00, sizeof(szTag));
		sprintf(szTag, "%X", tag);

		debug_sprintf(szDbgMsg, "%s: Got tag [%s] len [%d]\n", __FUNCTION__, szTag, len);
		APP_TRACE(szDbgMsg);

		if(!strcmp(szTag,"9F18"))
		{
			*to = (char*)pszBuff;
			*from= (char*)tagstart;
			rv = len + 3;
		}
		else if(strcmp(szTag,"71") == 0 || strcmp(szTag,"72") == 0)
		{
			if( (rv = search9F18TagandModifyLength(pszEmvStart, iEmvTotLen, ascIIBuffMain, iLenParPrev, iLenOffset, tagValue, len, from, to)) > 0)
			{
				if(len < 128)
				{
					*(tagstart + 1)= *(tagstart + 1) - rv;
				}
				else if(len >= 128 && len < 256)
				{
					if(len - rv >= 128)
					{
						*(tagstart+2)= *(tagstart+2) - rv;
					}
					else
					{
						//Now len of 71 or 72 Tag can be accommodated in 1 byte field
						*(tagstart + 2)= *(tagstart + 2) - rv - *iLenOffset;
						memcpy((char*)tagstart + 1, (char*)tagstart + 2, *iEmvTotLen - ((char*)tagstart - (char*)pszEmvStart) - 1);
						pszBuff -= 1;
						iBuffSize = iBuffSize - 1;
						iLengthParsed -= 1;

						*iEmvTotLen = *iEmvTotLen - 1;
						*ascIIBuffMain = *ascIIBuffMain - 1;
						*iLenParPrev = *iLenParPrev - 1;

						*to = *to - 1;
						*from = *from - 1;
						(*iLenOffset) ++;
					}
				}
				else
				{
					if(len - rv >= 256)
					{
						len = len - rv;
						*(tagstart + 2) = len >> 8;
						*(tagstart + 3) = len & 0xFF;
					}
					else
					{
						//Now len of 71 or 72 Tag can be accommodated in 2 bytes field
						len = len - rv- *iLenOffset;
						*(tagstart + 3) = len;
						*(tagstart + 1) = 0x81;
						memcpy((char*)tagstart + 2, (char*)tagstart + 3, *iEmvTotLen - ((char*)tagstart - (char*)pszEmvStart) - 1);
						pszBuff -= 1;
						iBuffSize = iBuffSize - 1;
						iLengthParsed -= 1;

						*iEmvTotLen = *iEmvTotLen - 1;
						*ascIIBuffMain = *ascIIBuffMain - 1;
						*iLenParPrev = *iLenParPrev - 1;

						*to = *to - 1;
						*from = *from - 1;
						(*iLenOffset) ++;
					}
				}
			}
		}
	}
#ifdef DEVDEBUG
	APP_TRACE_EX((char *) pszEmvStart, *iEmvTotLen);
#endif
	debug_sprintf(szDbgMsg, "%s: ---Returning----[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: parseEmvCTLSError
 *
 * Description	: This function  will parse for extended EMV CTLS Error
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int parseEmvCTLSError(unsigned char *szMsg, char *szErrCode)
{
	int 				rv 					= SUCCESS;
	char *				curPtr				= NULL;
	char *				nxtPtr				= NULL;

#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: ---Entering----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(szMsg == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL params Passed, Sending Default 02 as Extended CTLS Error", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		strcpy(szErrCode, "02");
		rv = SUCCESS;
		return rv;
	}

	curPtr = (char *)szMsg;

	while(1)
	{
		nxtPtr = strchr(curPtr, FS);
		if(nxtPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: ---Entering----", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		curPtr = nxtPtr + 1;

		strncpy(szErrCode, curPtr, 2);
		break;
	}

	debug_sprintf(szDbgMsg, "%s: ---Returning----[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: parseApplePayDataResp
 *
 * Description	: This function  will parse for ApplyPay Response
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int parseApplePayDataResp(unsigned char *ptrValue, int iSize, CARD_TRK_PTYPE pstCard, EMVDTLS_PTYPE pstEMV , int iSource)
{
	int 				rv					= SUCCESS;
	int 				iLengthParsed 		= 0;
	char				szTemp[20]			= "";
	char 				szTagValue[256]		= "";
	char				szTagValueHex[512] = "";
	char				szTag[7]			= "";
	unsigned int		tag;
	unsigned int 		Temp;
	unsigned int 		len;
	VASDATA_DTLS_STYPE	stVasDataDtls;

#ifdef DEVDEBUG
	char 		szDbgMsg[4096]	= "";
#elif DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- with TotSize=[%d]", __FUNCTION__, iSize);
	APP_TRACE(szDbgMsg);
	// CID-67288: 3-Feb-16: MukeshS3: ptrValue pointer can't be NULL at any point of time. So returning failure in case if it is passed as NULL
	//if( (ptrValue != NULL) || (iSource == 1 && pstEMV != NULL) || ( (iSource == 2 || iSource ==3 || iSource == 4) && (pstCard != NULL) ) )
	if( (ptrValue != NULL) && ((iSource == 1 && pstEMV != NULL) || ( (iSource == 2 || iSource ==3 || iSource == 4) && (pstCard != NULL) ) ) )
	{
		debug_sprintf(szDbgMsg, "%s: Source Called: %d", __FUNCTION__, iSource);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: NULL params or wrong source passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	memset(szTemp, 0x00, sizeof(szTemp));
	memset(&stVasDataDtls, 0x00, sizeof(VASDATA_DTLS_STYPE));



	memcpy(szTemp, ptrValue, 3);

	if(!strcmp(szTemp, "APY"))
	{
		debug_sprintf(szDbgMsg, "%s: Apple Pay Wallet Loyalty", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(stVasDataDtls.szPubliserName, "Apple Pay");
		iLengthParsed += 3;
		ptrValue += 3;

		//Setting Provision Pass Sent to TRUE, if Apple Data is also Present we will set it to FALSE
		stVasDataDtls.bProvisionPassSent = PAAS_TRUE;

		if((stVasDataDtls.bProvisionPassOnPayment == PAAS_TRUE) && (stVasDataDtls.bProvisionPassSent == PAAS_TRUE))
		{
			stVasDataDtls.bProvisionPassSent = PAAS_FALSE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Not an Apply Pay Wallet Returning", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SUCCESS;
	}

	while(iSize > iLengthParsed ) // Need to change and check
	{
		tag = 0;
		tag= *ptrValue++;
		iLengthParsed ++;
		if ((tag & 0x1f) == 0x1f)
		{
			do {
				tag = tag << 8;
				tag = tag | *(ptrValue++);
				iLengthParsed++;
			} while((tag & 0x80) == 0x80);
			//last byte in Tag name should be of 0xxxxxxx format, for a multi-byte tag name.
		}

		//Convert tag(Hex value) to ASCCI string
		memset(szTag, 0x00, sizeof(szTag));
		sprintf(szTag, "%X", tag);

		if( strcmp(szTag, "1C") == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Got FS tag, Ignoring and moving to next Byte", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iSize -= iLengthParsed;
			continue;
		}

		//Get Length
		len= *ptrValue++;
		iLengthParsed += 1;
		if (len & 0x80)
		{
			Temp= len & 0x7f;
			len= 0;
			while (Temp)
			{
				len *= 256;
				len += *ptrValue++;
				iLengthParsed += 1;
				Temp--; // Temp should decrimented for exiting from the closed loop.
			}
		}

		// store the value
		memset(szTagValue, 0x00, sizeof(szTagValue));
		memcpy(szTagValue, ptrValue, len);

		iLengthParsed += len;
		ptrValue += len;

		debug_sprintf(szDbgMsg, "%s: Tag [%s] Length [%d] Value [%s]", __FUNCTION__, szTag, len, szTagValue);
		APP_TRACE(szDbgMsg);

		if(!strcmp(tagTwoByte_9F[APPLE_INDICATOR_TAG_9FA00A], szTag))
		{
			// Tag Value contain the Y or N indicator .. Need to Check
			debug_sprintf(szDbgMsg, "%s:Got 9FA00A tag, Ignoring and moving to next Tag", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else if(!strcmp("9F27", szTag) && len > 1)
		{

			Char2Hex(szTagValue, szTagValueHex, len);

			debug_sprintf(szDbgMsg, "%s:Got Apply Pay Wallet Data", __FUNCTION__);
			APP_TRACE(szDbgMsg);
#ifdef DEVDEBUG
			APP_TRACE_EX((char *) szTagValueHex, len*2);
#endif

			//Since Apple Data is Present, This response is Not a Provision Pass Response
			stVasDataDtls.bProvisionPassSent = PAAS_FALSE;
			stVasDataDtls.bVASPresent = PAAS_TRUE;
			memcpy(stVasDataDtls.szLoyaltyData, szTagValueHex, len*2);
			//Noting more to capture
			break;
		}
	}
	if(iSource == 1)
	{
		memcpy(&pstEMV->stVasDataDtls, &stVasDataDtls, sizeof(VASDATA_DTLS_STYPE));
		if(strlen(pstEMV->szTrackData) <= 0)
		{
			pstEMV->iCardSrc = CRD_VAS_ONLY;
		}
	}
	else
	{
		memcpy(&pstCard->stVasDataDtls, &stVasDataDtls, sizeof(VASDATA_DTLS_STYPE));
		if( (strlen(pstCard->szTrk2) <= 0) && (strlen(pstCard->szTrk1) <= 0) && (strlen(pstCard->szTrk3) <= 0))
		{
			pstCard->iCardSrc = CRD_VAS_ONLY;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Return rv = [%d]",__FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

#if 0
/*
 * ============================================================================
 * Function Name: convertAsciiToHex
 *
 * Description	: This function would convert from Ascii to Hex value.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int convertAsciiToHex(unsigned char *ptrAsciiVal, int iLenVal, char *ptrHexVal)
{
	int			rv 			= SUCCESS;
	int 		iIndex		= 0;
	int			iLen 		= 0;
	char 		szTmp[200]	= "";
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif
	
	if(ptrAsciiVal == NULL || ptrHexVal == NULL)
		return FAILURE;
	
	memset(szTmp, 0x00, sizeof(szTmp));
	while(iIndex < iLenVal)
	{
		debug_sprintf(szDbgMsg, "%s: HexVal at [%d] index=%X", __FUNCTION__, iIndex, *(ptrAsciiVal+iIndex));
		APP_TRACE(szDbgMsg);	
		iLen += sprintf(ptrHexVal + iLen, "%X", *(ptrAsciiVal+iIndex)); 
		++iIndex;
	}
	debug_sprintf(szDbgMsg, "%s: HexValue=%s", __FUNCTION__, (char *)ptrHexVal);
	APP_TRACE(szDbgMsg);		
	
	return rv;
}

/*
 * ============================================================================
 * Function Name: convertAsciiToHex
 *
 * Description	: This function would convert from Hex to Ascii value.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int convertHexToAscii(unsigned char *ptrHexVal, int iLenVal, char *ptrAsciiVal)
{
	int			rv 			= SUCCESS;
	int 		iIndex		= 0;
	int			iLen 		= 0;
	char 		szTmp[200]	= "";
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif
	
	if(ptrHexVal == NULL || ptrAsciiVal == NULL)
		return FAILURE;
	
	memset(szTmp, 0x00, sizeof(szTmp));
	while(iIndex < iLenVal)
	{
		debug_sprintf(szDbgMsg, "%s: ptrAsciiVal=%c", __FUNCTION__, *(ptrHexVal+iIndex));
		APP_TRACE(szDbgMsg);	
		iLen += sprintf(ptrAsciiVal + iLen, "%c", *(ptrHexVal+iIndex)); 
		++iIndex;
	}
	debug_sprintf(szDbgMsg, "%s: ASCII Value=%s", __FUNCTION__, (char *)ptrAsciiVal);
	APP_TRACE(szDbgMsg);		
	
	return rv;
}
#endif

/*
 * ============================================================================
 * End of file emvMsgParser.c
 * ============================================================================
 */
