/*
 * =============================================================================
 * Filename    : bLogic.c
 *
 * Application : Mx Point SCA
 *
 * Description : 
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <setjmp.h>
#include <signal.h>
#include <pthread.h>
#include <syslog.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <svc.h>
#include "common/common.h"
#include "common/metaData.h"
#include "common/utils.h"
#include "sci/sciMain.h"
#include "sci/sciDef.h"
#include "bLogic/bLogic.h"
#include "bLogic/blAPIs.h"
#include "bLogic/blStepAPIs.h"
#include "db/dataSystem.h"
#include "db/tranDS.h"
#include "db/safDataStore.h"
#include "db/posDataStore.h"
#include "uiAgent/uiAPIs.h"
#include "uiAgent/ctrlids.h"
#include "uiAgent/uiConstants.h"
#include "uiAgent/uiCfgDef.h"
#include "uiAgent/uiCfgData.h"
#include "bLogic/bLogicCfgDef.h"
#include "bLogic/bLogicCfgData.h"
#include "uiAgent/emvAPIs.h"
#include "uiAgent/uiCfgDef.h"
#include "appLog/appLogAPIs.h"
#include "bLogic/blVHQSupDef.h"
#include "uiAgent/uiMsgFmt.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

typedef enum
{
	BUSY,
	COMPLETE,
	SEMI_COMPLETE
}
TRAN_STATUS;

typedef enum
{
	ONLINE_TRAN,
	DEVICE_TRAN
}
TRAN_TYPE;

typedef struct
{
	char			szMACLbl[10];
	char			szTranKey[10];
	char			szConnInfo[25];
	pthread_t		thId;
	PAAS_BOOL		bUIRqd;
	TRAN_STATUS		iStatus;
	BLUIDATA_STYPE	stUIData; 		//Praveen_P1: Added to take care of Consumer option capture and Swipe ahead
}
BTRAN_STYPE, * BTRAN_PTYPE;

typedef struct
{
	pthread_t	thId;
	sigjmp_buf	jmpBuf;
}
JMP_STYPE, * JMP_PTYPE;

/* Static global variables */
static pthread_t		blThreadId;
static PAAS_BOOL		bRunBlogic;
//static JMP_STYPE		stJmpLst[2];
static PAAS_BOOL		bXPIswitchedScreen;

static int				giAppState;			//KranthiK1: Added to take care of the cancel command
static int				giDetailedAppState; //T_RaghavendranR1, for detailed status of the payment transaction while capturing from user
static int				giCardReadEnabled;	//KranthiK1: Card Read Enabled
static int				giActiveD41;		//Daivik: To check if D41 is active
static int 				giOnlineTranStatus;
static pthread_mutex_t  gptD41ActiveMutex;
static int				giScndData;
static int				giDetailedStatus;
static int				giAllowLITran;
static int				giOnlineLITranRetry;
static pthread_mutex_t  gptOnlineLITranRetryMutex;
static pthread_mutex_t  gptAllowLITranMutex;
static pthread_mutex_t	gptAllowAppStateMutex;
static pthread_mutex_t  gptCardReadEnabledMutex;
static pthread_mutex_t  gptOnlineTranStatus;
static pthread_mutex_t  gptCancelState = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t	gptLastFormMutex;
pthread_mutex_t	gptCardDataInBQueueMutex;

static PAAS_BOOL		bSTBDataReceived		= PAAS_FALSE;
PAAS_BOOL		bCardDataReceived		= PAAS_FALSE;
static int	gPOSInitiatedState			= 0;
static int  			giLastForm;

/* Static functions declarations */
static void * bLogicThread(void *);
static void * deviceTransaction(void *);
static void * onlineTransaction(void *);

static int	startBLTran(BLDATA_PTYPE, BTRAN_PTYPE *, PAAS_BOOL *, PAAS_BOOL *, int );
static int	getTranType(char *, int *, int *, int *);
static int	checkifUIRqd(char *, PAAS_BOOL *);
//static void blSigHandler(int);
static void freeBLData(BLDATA_PTYPE);
static void * doConsumerOptCature(void *);
static void * doPaypalPaymentCapture(void *);
static void *processCardData(void *arg);
//static int checkToAllowTran(int , int , PAAS_BOOL , int *);

extern int storeandSendConsOptInfo(UNSOLMSGINFO_PTYPE , int, char * );
extern int storePreSwipeCardDtls(CARDDTLS_PTYPE , PAAS_BOOL );
extern void saveLogs();
extern int flushCardDetails(char *);
extern void releaseSSICurlHandleLock();
extern int determineCardType(CARDDTLS_PTYPE , char * );
extern int parseNSaveSTBInfo(CARDDTLS_PTYPE , char *);
extern void acquireGlobUIReqMutex(int ,char * );
extern void releaseGlobUIReqMutex(int ,char * );
extern PAAS_BOOL getHostConnectionStatus();

extern UIRESP_STYPE		stBLData;
extern PAAS_BOOL		bBLDataRcvd; //Praveen_P1: Added for freezing thing
extern int				giFACommInProgress;
extern int 				whichForm;
extern int				giEnvCommInProgress;
#if 0
extern FILE* fptr;
#endif

/*
 * ============================================================================
 * Function Name: initBLogicModule
 *
 * Description	: 
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int initBLogicModule(char * szQId, char *pszErrMsg)
{
	int		rv					= SUCCESS;
	char 	szErrMsg[256]		= "";
	char	szTranStkKey[10]	= "";
	//char 	szCmd[100] 			= "";
	//int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Load the configuration parameters into the application memory */
		rv	= loadBLogicConfigParams(pszErrMsg);
		if(rv != SUCCESS)
		{
			/* Replacing debug_sprintf with sprintf so that syslogs get printed
			 * even with the execution of release version of the application. */
			sprintf(szErrMsg,"%s: FAILED to load BLogic config params",__FUNCTION__);
			APP_TRACE(szErrMsg);
			syslog(LOG_ERR|LOG_USER, szErrMsg);
			break;
		}

		if(szQId == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL Params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
#if 0
		debug_sprintf(szDbgMsg, "%s: Creating Queue between BL and SCI interface", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Create queue for reading the business logic data */
		rv = createQueue(szQId);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create rcv Q", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
#endif
		debug_sprintf(szDbgMsg, "%s: Queue Id created for BL and SCI module[%s]", __FUNCTION__, szQId);
		APP_TRACE(szDbgMsg);
#if 0
		rv = loadD25Commands();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Fail to load D25 Commands", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
#endif

		/* Need to display "Demo mode" watermark text on the screen, in case device is started in demo-mode */
		if(isDemoModeEnabled())
		{
			/* Need to set "TRAINING MODE" on the screen at startup.
			 * SetWaterMarkImage:	<STX>XSWT<FS>WaterMarkText<FS>Font-Style<FS>Font-Size<ETX>
			 */
			setWaterMarkText(PAAS_TRUE);
		}

		/* Do the preamble processing for the device application */
		rv = processSCAPreamble(pszErrMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to process SCA preamble",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(isEmvEnabledInDevice())
		{
			rv = loadEmvTagsReqForHost();
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while Reading OR File with EmvTags Req by Host Not Present, Will send the default ones", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			//Load AIDList After EMVInitialization only
			rv = loadAIDListAndConfig(pszErrMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Fail to load AIDs List from AIDList.txt", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(strlen(pszErrMsg) == 0)
				{
					getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
					sprintf(pszErrMsg, "%s: AIDList.txt ", pszErrMsg);
				}
				break;
			}

			if(isEmvKernelSwitchingAllowed())
			{
				/*AjayS2: 07 Nov 2016: Moved below code in loadAIDListAndConfig
				 */
#if 0
				/* Daivik:16/9/2016 - We anyway need the terminal capbilities from the INI files only when kernel switching is enabled.
				 * If it is disabled, the Tag 9F33 that we query XPI with will anyway give us the value.
				 */
				if(isCashBackEnabled()) // We will need Default TermCap only in case Cashback is Enabled.
				{
					if(doesFileExist(XPI_EMV_TABLES_PERM_PATH) == SUCCESS)
					{
						memset(szCmd, 0x00, sizeof(szCmd));
						sprintf(szCmd, "%s %s %s", "cp", XPI_EMV_TABLES_PERM_PATH, XPI_EMV_TABLES_TEMP_PATH);
						rv = local_svcSystem(szCmd);
						if(rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: Failed to execute command [%s]",__FUNCTION__, szCmd);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "Failed to Copy the XPI EMVTable Config File");
								addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
							}
						}
						else
						{
							rv = updateTermCapFromINItoAIDList();
							if(rv != SUCCESS)
							{
								debug_sprintf(szDbgMsg, "%s: Failed to update Terminal Cap", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}
							memset(szCmd, 0x00, sizeof(szCmd));
							sprintf(szCmd, "%s %s", "rm", XPI_EMV_TABLES_TEMP_PATH);
							rv = local_svcSystem(szCmd);
							if(rv != SUCCESS)
							{
								debug_sprintf(szDbgMsg, "%s: Failed to execute command [%s]",__FUNCTION__, szCmd);
								APP_TRACE(szDbgMsg);
								if(iAppLogEnabled == 1)
								{
									strcpy(szAppLogData, "Failed to Remove the Temporary EMV Config File");
									addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
								}
							}
						}
					}
					else
					{
						rv = FAILURE;
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "EMVTable Config File Not found in XPI Location");
							addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
						}
					}
					if(rv != SUCCESS)
					{
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "Failed to Load Default TermCap from EMVTable Config");
							addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
						}
					}
				}
#endif
				rv = loadPreferredDtlsFromPAID();
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Fail to load PAID File", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
					sprintf(pszErrMsg, "%s: PreferredAID", pszErrMsg);
					break;
				}
				//AjayS2: 14/Jan/2016: Setting send_u01 to 0, on startup of application
				setXPIParameter("send_u01", "0");
			}

			if(isContactlessEmvEnabledInDevice())
			{
				rv = createHashForEmvCTLSTags();
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Fail to Create Hash For EMV CTLS Tags", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
			}
		}

		rv = checkWalletSupportinDevice();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Check Wallet Support in Device", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
	    /* ArjunU1: Relevant in a scenario when the device is not a vanilla device and
		 * some VSP error occured when the device ran last time. Since the VSP
		 * registration will not be done in the preamble again, we need to do it
		 * separately here. If there are no VSP errors, VSP registration should not
		 * be done. Also is VSP is not enabled, then also should not be done. These
		 * conditions are being checked inside this function itself */
		if( isVSPRegReqdAgain() == PAAS_TRUE)
		{
			memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
			doVSPReg(NULL, szTranStkKey, PAAS_FALSE, NULL, DEVICE_REG);
		}

		
		/* Start the business logic thread */
		bRunBlogic = PAAS_TRUE;
		rv = pthread_create(&blThreadId, NULL, bLogicThread, (void *)szQId);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to start BLogic thread",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(isSAFEnabled() == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: SAF is enabled, initializing the SAF module", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Initialize the SAF part of the business logic */
			rv = initSAF();
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to init SAF", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: SAF is NOT enabled, skiping initializing the SAF module", __FUNCTION__);
			APP_TRACE(szDbgMsg);

		}

		/*
		 * This is Mutex is created for static global variable giAllowLITran
		 * This variable is shared across threads(Device and Online Transactions)
		 * so to prevent the variable, making it mutable
		 */
		if(pthread_mutex_init(&gptAllowLITranMutex, NULL))//Initialize the AllowLITran Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create SAF Records File mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		if(pthread_mutex_init(&gptOnlineLITranRetryMutex, NULL))//Initialize the AllowLITran Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to Online LI Tran retry mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		/*
		 * This is Mutex is created for global variable giAppState
		 * This variable is shared across threads(Device and Online Transactions)
		 * so to prevent the variable, making it mutable
		 */
		if(pthread_mutex_init(&gptAllowAppStateMutex, NULL))//Initialize the AllowLITran Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create Cancel command mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		/*
		 * This is Mutex is created for global variable giCardReadEnabled
		 * This variable is shared across threads(Device and Online Transactions)
		 * so to prevent the variable, making it mutable
		 */
		if(pthread_mutex_init(&gptCardReadEnabledMutex, NULL))//Initialize the AllowLITran Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create Card Read enabled command mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		/*
		 * This is Mutex is created for global variable giD41Active
		 * This variable is shared across threads(Device and Online Transactions)
		 * so to prevent the variable, making it mutable
		 */
		if(pthread_mutex_init(&gptD41ActiveMutex, NULL))//Initialize the AllowLITran Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create D41 Active mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		/*
		 * This is Mutex is created for global variable giLastForm
		 * This variable is shared across threads(Device and doconsumerOptionCapture Transactions)
		 * so to prevent the variable, making it mutable
		 */
		if(pthread_mutex_init(&gptLastFormMutex, NULL))//Initialize the Last Formn Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create Last Form mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		/*
		 * This is Mutex is created for session variable bCardDataInBQueue
		 * This variable is shared across threads(Blogic and uiManager Transactions)
		 * so to prevent the variable, making it mutable
		 */
		if(pthread_mutex_init(&gptCardDataInBQueueMutex, NULL))//Initialize the card data in BQueue Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create Card Data in BQueue mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		break;
	}

	if(rv != SUCCESS)
	{
		bRunBlogic = PAAS_FALSE;

		if(szQId != NULL && strlen(szQId) > 0) // T_RaghavendranR1 CID 83346 (#1 of 1): Dereference after null check (FORWARD_NULL)
		{
			/* Delete the queue */
			deleteQueue(szQId);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: closeBLocigModule
 *
 * Description	: 
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int closeBLogicModule()
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	bRunBlogic = PAAS_FALSE;

	pthread_join(blThreadId, NULL);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: checkForScreenChange
 *
 * Description	: Checking U02 res to check whether we need to switch screen or not
 *
 * Input Params	: none
 *
 * Output Params: NULL
 * ============================================================================
 */
int checkForScreenChange(EMVDTLS_PTYPE pstEmvDtls)
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	bXPIswitchedScreen = PAAS_FALSE;
	debug_sprintf(szDbgMsg, "%s: ---Entering--- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(	pstEmvDtls->stEMVCardStatusU02.bAppSelScreen == PAAS_TRUE  ||
			pstEmvDtls->stEMVCardStatusU02.bLangSelScreen == PAAS_TRUE ||
			pstEmvDtls->stEMVCardStatusU02.bEMVRetry == PAAS_TRUE ||
			pstEmvDtls->stEMVCardStatusU02.bEMVPINEntry == PAAS_TRUE )
	{
		bXPIswitchedScreen = PAAS_TRUE;
	}
	else
	{
		bXPIswitchedScreen = PAAS_FALSE;
	}
	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, bXPIswitchedScreen);
	APP_TRACE(szDbgMsg);
	return bXPIswitchedScreen;
}

/*
 * ============================================================================
 * Function Name: bLogicThread
 *
 * Description	: Main function for the business Logic Thread
 *
 * Input Params	: none
 *
 * Output Params: NULL 
 * ============================================================================
 */
static void * bLogicThread(void * arg)
{
	int						rv						= SUCCESS;
	int						iFxn					= 0;
	int						iCmd					= 0;
	int						iSize					= 0;
	int						iTranType				= 0;
	int						iStatusMsgDispInterval  = DFLT_STATUS_DISP_TIME;
	int						iAppLogEnabled			= isAppLogEnabled();
	int						iStatus					= 0;
	int						VHQRcvMsgQId			= 0;
	char					szQId[10]				= "";
	char					szMsg[100]				= "";
	char					szAppLogData[300]		= "";
	char					szAppLogDiag[300]		= "";
	PAAS_BOOL				bUIBusy					= PAAS_FALSE;
	PAAS_BOOL				bUIBusyAllow			= PAAS_FALSE; //Added this variable to allow even UI is busy
//	PAAS_BOOL				bSTBDataReceived		= PAAS_FALSE;
//	PAAS_BOOL				bCardDataReceived		= PAAS_FALSE;
	PAAS_BOOL				bOnlineUIReqd			= PAAS_FALSE;
	BLDATA_PTYPE			pstBLData				= NULL;
	BTRAN_PTYPE				pstDevTran				= NULL;
	BTRAN_PTYPE				pstOnlineTran			= NULL;
	BTRAN_PTYPE				pstUITran				= NULL;
	POSRESPDATA_VHQ_STYPE	stPOSRespDataForVHQ;
//	UIREQ_STYPE				stUIReq;
//	CARD_TRK_PTYPE	pstCard					= NULL;
//	int			iAllowTran				= 0;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	/* get the Queue Id */
	strcpy(szQId, (char *) arg);

	debug_sprintf(szDbgMsg, "%s: Queue Id passed [%s]", __FUNCTION__, szQId);
	APP_TRACE(szDbgMsg);

	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();

	while(bRunBlogic == PAAS_TRUE)
	{
		/* Cleanup the online BL thread if the thread has completed its
		 * execution */
		if((pstOnlineTran != NULL) && (pstOnlineTran->iStatus == COMPLETE))
		{
			rv = getFxnNCmdForTran(pstOnlineTran->szTranKey, &iFxn, &iCmd);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get function and command", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			bOnlineUIReqd = pstOnlineTran->bUIRqd;
			pthread_join(pstOnlineTran->thId, NULL);

			deleteTran(pstOnlineTran->szTranKey);
			free(pstOnlineTran);

			pstOnlineTran = NULL;

			setOnlineTranStatus(PAAS_FALSE);

			if(getEmvInitUpdateFlag())
			{
				debug_sprintf(szDbgMsg, "%s: Received EMV_UPDATE from Host", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				setEmvInitUpdateFlag(PAAS_FALSE);
				pstUITran = (BTRAN_PTYPE) malloc(sizeof(BTRAN_STYPE));
				if(pstUITran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(pstUITran, 0x00, sizeof(BTRAN_STYPE));

				pstUITran->stUIData.iDataType = UI_EMVADMIN_DATA_TYPE;

				rv = pthread_create(&(pstUITran->thId), NULL, processCardData, pstUITran);

				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to create processCardData thread for EMV_UPDATE", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				debug_sprintf(szDbgMsg, "%s: Setting the UIBusy flag to PAAS_TRUE", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				bUIBusy = PAAS_TRUE;
			}
			acquireMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");

			/*
			 * Setting it to False only if we dont have active LI Tran in the buffer
			 * Added one more condition to set UIBusy flag to false, if we dont have
			 * active UITran(to process the consumer options & pre-swipe data) then
			 * setting uiBusy flag to false
			 *
			 */
			debug_sprintf(szDbgMsg, "%s: giAllowLITran:%d UIReqd:%d pstUITran:%p iFxn:%d", __FUNCTION__,giAllowLITran,bOnlineUIReqd,pstUITran,iFxn);
			APP_TRACE(szDbgMsg);
			if(giAllowLITran == 0 && pstUITran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Setting the UIBusy flag to FALSE", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				bUIBusy = PAAS_FALSE;

				acquireMutexLock(&gptOnlineLITranRetryMutex, "Allow LI Transaction");
				if(giOnlineLITranRetry)
				{

					giAllowLITran = 1;
					debug_sprintf(szDbgMsg, "%s: Setting the AllowLITran and busyAllow , since some LI failed and was buffered", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					/* If a failed line item was buffered , we would have set AllowLITran  to TRUE.
					 * This is after setting UIBusy to false. But by then we get another LI Tran, then we set UI busy and when that completes,
					 * since AllowLITran is set we are not setting UI busy to false, causing the UI Data to not get processed. So setting UIbusyAllow to true
					 * so that UI data gets processed if it comes in.
					 */
					bUIBusyAllow = PAAS_TRUE;

					giOnlineLITranRetry = 0;
				}
				releaseMutexLock(&gptOnlineLITranRetryMutex, "Allow LI Transaction");

			}
			else if((giAllowLITran == 1) && (pstUITran == NULL) && bOnlineUIReqd && (iFxn != SCI_LI))// || ((iFxn == SCI_LI) && (iCmd != SCI_SHOW))
			{
				debug_sprintf(szDbgMsg, "%s: Setting the UIBusy flag to FALSE Since non LI Online Transaction was initiated.", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				bUIBusy = PAAS_FALSE;
				bOnlineUIReqd = PAAS_FALSE; //Reset the value once we have used it.
			}

			releaseMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");

			//memset(&(stJmpLst[0]), 0x00, sizeof(JMP_STYPE));

			debug_sprintf(szDbgMsg, "%s: ONLINE TRANSACTION COMPLETED", __FUNCTION__);
			APP_TRACE(szDbgMsg);
				
		}

		/* Cleanup the device BL thread if the thread has completed its
		 * execution */
		if((pstDevTran != NULL) && (pstDevTran->iStatus == COMPLETE))
		{
			pthread_join(pstDevTran->thId, NULL);

			rv = getFxnNCmdForTran(pstDevTran->szTranKey, &iFxn, &iCmd);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get function and command", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			if((iCmd == SCI_DISPLAY_MESSAGE) && (bUIBusyAllow == PAAS_TRUE))
			{
				debug_sprintf(szDbgMsg, "%s: Setting the bUIBusyAllow flag to FALSE for DISPLAY_MESSAGE cmd", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				bUIBusyAllow = PAAS_FALSE;
			}

			deleteTran(pstDevTran->szTranKey);	

			/*
			 * Setting it to False only if dev tran required UI
			 * Added one more condition to set UIBusy flag to false, if we dont have
			 * active UITran(to process the consumer options & pre-swipe data) then
			 * setting uiBusy flag to false
			 *
			 */

			if(pstDevTran->bUIRqd == PAAS_TRUE && pstUITran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Setting the UIBusy flag to FALSE", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				bUIBusy = PAAS_FALSE;
			}

			free(pstDevTran);
			pstDevTran = NULL;

			//memset(&(stJmpLst[1]), 0x00, sizeof(JMP_STYPE));
			
			debug_sprintf(szDbgMsg, "%s: DEVICE TRANSACTION COMPLETED", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if((pstUITran != NULL) && ((pstUITran->iStatus == COMPLETE) || (pstUITran->iStatus == SEMI_COMPLETE)))
		{
			pthread_join(pstUITran->thId, NULL);

			iStatus = pstUITran->iStatus; //Storing the status before freeing the structure

			free(pstUITran);
			pstUITran = NULL;

			if(iStatus == COMPLETE)
			{
				if(pstDevTran != NULL)
				{
					rv = getFxnNCmdForTran(pstDevTran->szTranKey, &iFxn, &iCmd);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to get function and command", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						break;
					}

					if((iCmd == SCI_DISPLAY_MESSAGE && whichForm == LI_FULL_DISP_FRM))
					{
						debug_sprintf(szDbgMsg, "%s:  Currently DISPLAY_MESSAGE cmd in Execution, So not updating the LI screen", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}

#if 0
				/* T_RaghavendranR1: If DISPLAY_MESSAGE command(Message Box is ON) execution in progress, and if we had some LI datas in buffer
				 * If User preSwiped, we were Overlapping, so to avoid that we added the below condition, saying not to update the LI screen in
				 * DISPLAY_MESSAGE command is in progress*/
				if(giAllowLITran && (iCmd != SCI_DISPLAY_MESSAGE))
				{
					//The below variable will be set to 0 in showupdatedLIScreen
					//giAllowLITran = 0;


					if(isLineItemDisplayEnabled()) //We need to show line item screen only line item display is enabled
					{
						debug_sprintf(szDbgMsg, "%s: AllowLITran is set so showing lineitem screen", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						/*
						 * Need to update line item screen first before updating with the new ones
						 * passing 0 as the parameter so that it does show up line item screen
						 */
						initUpdateForLineItemScreen(0, &stUIReq);

						/* Showing line item screen */
						showUpdatedLIScreen();
					}
				}
#endif
				iCmd = -1;

				if(pstDevTran != NULL)
				{
					rv = getFxnNCmdForTran(pstDevTran->szTranKey, &iFxn, &iCmd);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to get function and command", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						break;
					}
				}

				if((bUIBusy == PAAS_TRUE) && (iCmd == SCI_DISPLAY_MESSAGE && whichForm == LI_FULL_DISP_FRM))
				{
					debug_sprintf(szDbgMsg, "%s: Retaining the bUIBusy and bUIBusyAllow as PAAS_TRUE as Display_Message is under process", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					bUIBusy = PAAS_TRUE;
					bUIBusyAllow = PAAS_TRUE;

					iCmd = -1;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Setting the UIBusy flag to FALSE", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					bUIBusy = PAAS_FALSE;

					debug_sprintf(szDbgMsg, "%s: Setting the UIBusyAllow flag to FALSE", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					bUIBusyAllow = PAAS_FALSE;
					//If AllowLITran is true , and if LINE Item comes , then UI data may not get accepted , if UIBusy is TRUE
					if(getAllowLITran())
					{
						debug_sprintf(szDbgMsg, "%s: Setting the UIBusyAllow flag to TRUE, since allowLItran is TRUE", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						bUIBusyAllow = PAAS_TRUE;
					}
				}

				bCardDataReceived = PAAS_FALSE;
				bSTBDataReceived = PAAS_FALSE;

				debug_sprintf(szDbgMsg, "%s: UI CONSUMER_OPTION/CARD_DATA CAPTURE COMPLETED", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(iStatus == SEMI_COMPLETE)
			{
				/*
				 * Praveen_P1: Since CARD_DATA thread has finished the partial processing
				 * not setting the bUIBusy flag to FALSE, thats why setting bUIBusyallow to TRUE
				 * to accept the next pending data to the processcarddata thread
				 */
				debug_sprintf(szDbgMsg, "%s: Setting the UIBusyAllow flag to TRUE", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				bUIBusyAllow = PAAS_TRUE;
				//TO_DO: Need to figure out what needs to be done
				debug_sprintf(szDbgMsg, "%s: UI CARD_DATA CAPTURE SEMI COMPLETED", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				//Should not come here
			}

		}

		if( ((pstDevTran != NULL) && (pstDevTran->iStatus == COMPLETE))||((pstUITran != NULL) && (pstUITran->iStatus == COMPLETE)) ||
			((pstOnlineTran != NULL) && (pstOnlineTran->iStatus == COMPLETE)))
		{
			/* Daivik:19/4/2016 - Need to check whether Online Transaction is completed before we pick up the next data from the BL Queue.
			 * If Online transaction was actually complete , but thread not finished, then we would end up sending a device busy for any online
			 * command we recieve at this point. To avoid this we have this check.
			 */
			continue;
		}

		/* Daivik:10/6/2016 - We will be removing the data from the Queue only if we know that it will get processed.
		 * Else we just peek from the Q , get the Data to be processed and then we check if UI is busy , UI tran is not NULL etc.
		 * In those cases where UI was busy we used to requeue , but now since we are not removing from the queue itself, there is
		 * no requirement to requeue.
		 */
#if 0
		/* Try to get the next business data from the business logic queue */
		pstBLData = (BLDATA_PTYPE) getDataFrmQ(szQId, &iSize);
		if(pstBLData == NULL)
		{
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			continue;
		}
#endif

		pstBLData = (BLDATA_PTYPE) peekDataFrmQ(szQId, &iSize);
		if(pstBLData == NULL)
		{
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			continue;
		}
		if(pstBLData->iEvtType != UIDATA)
		{
			pstBLData = NULL;
			pstBLData = (BLDATA_PTYPE) getDataFrmQ(szQId, &iSize);
			if(pstBLData == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: SHOULD NOT COME HERE - Data not Present in Q", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				svcWait(15);//Should Not come here , because we just peeked from Q and saw the data ,so we should have data in Q
				continue;
			}
		}

		if(pstBLData->iEvtType == DATA)
		{
			/* Got some business logic data. The data (POS request) is
			 * authenticated before reaching here, so we need to check if it
			 * can be processed right now or not.
			 * Factors which can hinder the processing of a new transaction
			 * 1. The device is busy with other transaction */
			getTranType(pstBLData->szTranKey, &iTranType, &iFxn, &iCmd);

			debug_sprintf(szDbgMsg, "%s: Transaction Type = %d", __FUNCTION__, iTranType);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Allow Transaction = %d", __FUNCTION__, giAllowLITran);
			APP_TRACE(szDbgMsg);

			if((iTranType == ONLINE_TRAN) && (pstOnlineTran == NULL))
			{
				debug_sprintf(szDbgMsg, "%s: Starting Online BL Transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = startBLTran(pstBLData, &pstOnlineTran, &bUIBusy, &bUIBusyAllow, iTranType);
				if(rv == SUCCESS && iFxn != SCI_LI)
				{
					setOnlineTranStatus(PAAS_TRUE);
				}
			}
			else if((iTranType == DEVICE_TRAN) && (pstDevTran == NULL))
			{
				debug_sprintf(szDbgMsg, "%s: Starting Device BL Transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = startBLTran(pstBLData, &pstDevTran, &bUIBusy, &bUIBusyAllow, iTranType);
			}
			else
			{
				if(iAppLogEnabled == 1)
				{
					if(pstDevTran != NULL)
					{
						strcpy(szAppLogData, "Device is Busy, Device Currently Executing Device Transaction Command.");
					}
					else
					{
						strcpy(szAppLogData, "Device is Busy, Device Currently Executing Online Transaction Command.");
					}
					strcpy(szAppLogDiag, "Please Send the Command After Completion of the Currently Executing Command");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_DEVICE_BUSY;

				// We were seeing lock up's , mostly because this was being set to 0 , and we were sending LI out of order
				//					acquireMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");
				//
				//					giAllowLITran = 0;
				//
				//					releaseMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");
			}

			if(rv != SUCCESS)
			{
				/* Create a response and send it back to POS through the
				 * SCI module */
				setStatusInTran(pstBLData->szTranKey, rv);

				setGenRespDtlsinTran(pstBLData->szTranKey, NULL);

				sendSCIResp(pstBLData->szConnInfo, pstBLData->szTranKey, pstBLData->szMacLbl);
			}
		}
		else if(pstBLData->iEvtType == DISCONN)
		{
			/* Send the SIGUSR2 signal to the culprit thread if the thread is
			 * indeed the culprit. */

			debug_sprintf(szDbgMsg, "%s: Obtained a Disconnect Message :%d",__FUNCTION__,__LINE__);
			APP_TRACE(szDbgMsg);

			if((pstOnlineTran != NULL) &&
				!strcmp(pstOnlineTran->szConnInfo, pstBLData->szConnInfo) )
			{

				setPOSInitiatedState(POSDISCONNECTED);
#if 0
				acquireGlobUIReqMutex(__LINE__,(char *)__FUNCTION__);
				pthread_kill(pstOnlineTran->thId, SIGUSR2);
				svcWait(5); //Wait for 5 ms for Signal Jump to happen
				releaseGlobUIReqMutex(__LINE__,(char *)__FUNCTION__);
#endif
			}
			else if((pstDevTran != NULL) &&
				!strcmp(pstDevTran->szConnInfo, pstBLData->szConnInfo) )
			{
				setPOSInitiatedState(POSDISCONNECTED);
#if 0
				acquireGlobUIReqMutex(__LINE__,(char *)__FUNCTION__);
				pthread_kill(pstDevTran->thId, SIGUSR2);
				svcWait(5); //Wait for 5 ms for Signal Jump to happen
				releaseGlobUIReqMutex(__LINE__,(char *)__FUNCTION__);
#endif
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Can safely ignore disconnection",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(pstBLData->iEvtType == CANCEL)
		{
			/* Send the SIGUSR1 signal to the culprit thread if the thread is
			 * indeed the culprit. */
			debug_sprintf(szDbgMsg, "%s: Obtained a Cancel Message :%d",__FUNCTION__,__LINE__);
			APP_TRACE(szDbgMsg);
			if(isCancelRebootAllowed())
			{
				if(pstOnlineTran != NULL)
				{
					rv = getFxnNCmdForTran(pstOnlineTran->szTranKey, &iFxn, &iCmd);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to get function and command", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					if(iFxn == SCI_LI || iFxn == SCI_SESS)
					{
						debug_sprintf(szDbgMsg, "%s: Session Command or Line Item command is in progress. No need to CANCEL the transaction", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "Session or Line Item Command is In Progress; Cancel Not Required");
							addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
						}
						sprintf(szMsg, "Cancel not allowed now");
						setScndData(NOT_ALLOWED);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Online Transaction in progress, Cancelling it!!!",__FUNCTION__);
						APP_TRACE(szDbgMsg);
						strcpy(szAppLogData, "Online Transaction in Progress, Cancelling the Transaction ");
						addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
	/*
						if(giFACommInProgress || giEnvCommInProgress)
						{
							debug_sprintf(szDbgMsg, "%s: FA Communication is in progress so need to wait till it finishes",__FUNCTION__);
							APP_TRACE(szDbgMsg);

							while(giFACommInProgress == 1 || giEnvCommInProgress)
							{
								svcWait(5);
							}

							debug_sprintf(szDbgMsg, "%s: Finished ...we can send signal now",__FUNCTION__);
							APP_TRACE(szDbgMsg);//Praveen_P1: for testing purpose only
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: FA Communication is NOT in progress so NO need to wait till it finishes",__FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
	*/
#if 0
						acquireGlobUIReqMutex(__LINE__,(char *)__FUNCTION__);
						pthread_kill(pstOnlineTran->thId, SIGUSR1);
						svcWait(5); //Wait for 5 ms for Signal Jump to happen
						releaseGlobUIReqMutex(__LINE__,(char *)__FUNCTION__);
#endif
						setPOSInitiatedState(POSCANCELLED);

						/* Keep waiting till the cancelled state is set to False
						 * which means
						 * */
						while((getPOSInitiatedState() == POSCANCELLED))
						{
							svcWait(15);
						}

						/* Daivik:23/6/2016 - This logic was required to know whether Online Transaction
						 * had already processed the transaction and also sent the primary port response.
						 * So in that case we need to indicate cancel not allowed to POS on secondary port.
						 */
						if(getPOSInitiatedState() == TRANCOMPLETED)
						{
							debug_sprintf(szDbgMsg, "%s: Cancel Is not Allowed Now",__FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(szMsg, 0x00, sizeof(szMsg));
							sprintf(szMsg, "Cancel not allowed now");
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "Cancel Not Allowed");
								addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
							}

							setScndData(NOT_ALLOWED);
						}
						else
						{
							memset(szMsg, 0x00, sizeof(szMsg));
							sprintf(szMsg, "Transaction CANCELLED");

							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "Online Transaction Cancelled Successfully");
								addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
							}
							setScndData(CANCELLED);
						}
					}
					rv = SUCCESS;
				}
				else if(pstDevTran != NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Device Transaction in progress, Cancelling it!!!",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Device Transaction in Progress, Cancelling the Transaction ");
						addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
					}

					setPOSInitiatedState(POSCANCELLED);


					/* Keep waiting till the cancelled state is set to False
					 * which means
					 * */
					while((getPOSInitiatedState() == POSCANCELLED))
					{
						svcWait(15);
					}

					if(getPOSInitiatedState() == TRANCOMPLETED)
					{
						debug_sprintf(szDbgMsg, "%s: Cancel Is not Allowed Now",__FUNCTION__);
						APP_TRACE(szDbgMsg);
						memset(szMsg, 0x00, sizeof(szMsg));
						sprintf(szMsg, "Cancel not allowed now");
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "Cancel Not Allowed");
							addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
						}

						setScndData(NOT_ALLOWED);
					}
					else
					{
						memset(szMsg, 0x00, sizeof(szMsg));
						sprintf(szMsg, "Transaction CANCELLED");

						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "Online Transaction Cancelled Successfully");
							addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
						}
						setScndData(CANCELLED);
					}

					rv = SUCCESS;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Can safely ignore CANCEL",
							__FUNCTION__);
					APP_TRACE(szDbgMsg);

					memset(szMsg, 0x00, sizeof(szMsg));
					sprintf(szMsg, "No Transaction to CANCEL");
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "No Transaction to Cancel");
						addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
					}

					setScndData(NO_TRAN);

					rv = SUCCESS;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Cancel Is not Allowed Now",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				memset(szMsg, 0x00, sizeof(szMsg));
				sprintf(szMsg, "Cancel not allowed now");

				setScndData(NOT_ALLOWED);

				rv = SUCCESS;
			}

			setStatusInTran(pstBLData->szTranKey, rv);

			setGenRespDtlsinTran(pstBLData->szTranKey, szMsg);

			sendSCIResp(pstBLData->szConnInfo, pstBLData->szTranKey, pstBLData->szMacLbl);
		}
		else if(pstBLData->iEvtType == REBOOT)
		{
			if(isCancelRebootAllowed())
			{
				setStatusInTran(pstBLData->szTranKey, SUCCESS);

				memset(szMsg, 0x00, sizeof(szMsg));
				sprintf(szMsg, "Device Rebooting");

				setScndData(REBOOTING);

				setGenRespDtlsinTran(pstBLData->szTranKey, szMsg);

				sendSCIResp(pstBLData->szConnInfo, pstBLData->szTranKey, pstBLData->szMacLbl);

				if(pstOnlineTran != NULL)
				{
					/* Set the proper return status for the transaction */
					setStatusInTran(pstOnlineTran->szTranKey, ERR_DEV_REBOOTING);

					/* Set the response details for the transaction */
					setGenRespDtlsinTran(pstOnlineTran->szTranKey, "");


					sendSCIResp(pstOnlineTran->szConnInfo, pstOnlineTran->szTranKey, pstOnlineTran->szMACLbl);

					/*
					 * Praveen_P1: Putting svcWait down so that if any online or device tran is in process
					 * it will send the response back on their primary port
					 * we are also rebooting here to double make sure that if online and device threads
					 * are stuck somehow..they will not able to execute the reboot..thats why we
					 * are doing here also
					 */

//					svcWait(5000);
				}
				else if(pstDevTran != NULL)
				{
					/* Set the proper return status for the transaction */
					setStatusInTran(pstDevTran->szTranKey, ERR_DEV_REBOOTING);

					/* Set the response details for the transaction */
					setGenRespDtlsinTran(pstDevTran->szTranKey, "");


					sendSCIResp(pstDevTran->szConnInfo, pstDevTran->szTranKey, pstDevTran->szMACLbl);
					/*
					 * Praveen_P1: Putting svcWait down so that if any online or device tran is in process
					 * it will send the response back on their primary port
					 * we are also rebooting here to double make sure that if online and device threads
					 * are stuck somehow..they will not able to execute the reboot..thats why we
					 * are doing here also
					 */

//					svcWait(5000);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: No transaction in process, can safely reboot", __FUNCTION__);
					APP_TRACE(szDbgMsg);

				}

				debug_sprintf(szDbgMsg, "%s: Device Rebooting...", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* Show the "Device rebooting" message on the device screen */
				showPSGenDispForm(MSG_REBOOT, STANDBY_ICON, iStatusMsgDispInterval);

				//Savelogs before rebooting
				saveLogs();

				//reboot
				svcRestart();
			}
			else
			{
				memset(szMsg, 0x00, sizeof(szMsg));
				sprintf(szMsg, "Reboot not allowed now");

				setStatusInTran(pstBLData->szTranKey, SUCCESS);
				setGenRespDtlsinTran(pstBLData->szTranKey, szMsg);

				setScndData(NOT_ALLOWED);

				sendSCIResp(pstBLData->szConnInfo, pstBLData->szTranKey, pstBLData->szMacLbl);

				rv = SUCCESS;
			}
		}
		else if(pstBLData->iEvtType == STATUS)
		{
			setScndData(getAppState());
			setScndDetailedStatus(getDetailedAppState());

			setStatusInTran(pstBLData->szTranKey, SUCCESS);

			setGenRespDtlsinTran(pstBLData->szTranKey, NULL);

			sendSCIResp(pstBLData->szConnInfo, pstBLData->szTranKey, pstBLData->szMacLbl);
		}
		else if(pstBLData->iEvtType == UPDATE_QUERY)
		{
			setScndData(isVHQUpdateAvailable());

			setStatusInTran(pstBLData->szTranKey, SUCCESS);

			setGenRespDtlsinTran(pstBLData->szTranKey, NULL);

			sendSCIResp(pstBLData->szConnInfo, pstBLData->szTranKey, pstBLData->szMacLbl);
		}
		else if(pstBLData->iEvtType == UPDATE_STATUS)
		{
			setScndData(getVHQUpdateStatus());

			setStatusInTran(pstBLData->szTranKey, SUCCESS);

			setGenRespDtlsinTran(pstBLData->szTranKey, NULL);

			sendSCIResp(pstBLData->szConnInfo, pstBLData->szTranKey, pstBLData->szMacLbl);

			/* Once the Restart/Reboot is taken from the POS, Need to inform VHQ thread about it to proceed for Reboot/Restart.
			 * VHQ thread will be waiting there for Update status to be consumed by POS after parameter/software/content installation.
			 */
			if((getVHQUpdateStatus() == VHQ_UPDATE_SUCCESS_RESTARTING_APP) || (getVHQUpdateStatus() == VHQ_UPDATE_SUCCESS_REBOOTING))
			{
				memset(&stPOSRespDataForVHQ, 0x00, sizeof(POSRESPDATA_VHQ_STYPE));
				strcpy(stPOSRespDataForVHQ.szText, "NOTIFY_SUCCESS");
				stPOSRespDataForVHQ.iRespType = SCA_POS_NOTIFY_SUCCESS;

				VHQRcvMsgQId = getVHQRcvFrmPOSMsgQId();
				rv = msgsnd(VHQRcvMsgQId, (void *)&stPOSRespDataForVHQ, sizeof(POSRESPDATA_VHQ_STYPE), IPC_NOWAIT); //IPC system call
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to add reboot/restart notification data to SCA VHQ MSG Queue",__FUNCTION__);
					APP_TRACE(szDbgMsg);

					debug_sprintf(szDbgMsg, "%s: Something went wrong msgsnd(): [%d] [%s]", __FUNCTION__, errno, strerror(errno));
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: POS notification success response added to MsgQ for VHQ thread",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}

			//once the current update status(SUCCESS/FAILURE) is sent to POS, need to make it unavailable for subsequent UPDATE_STATUS request, unless we have any new update.
			if(isVHQUpdateAvailable())
			{
				setVHQUpdateStatus(VHQ_UPDATE_AVAILABLE);
			}
			else if(getVHQUpdateStatus() != VHQ_UPDATE_PROCESSING)
			{
				setVHQUpdateStatus(VHQ_NO_UPDATES);
			}
		}
		else if(pstBLData->iEvtType == UIDATA)
		{
			/* Got some business logic data. The data (UI Manager) is
			 * sent by UI Manager to act up on it.
			 */

			if(pstBLData->stUIData.iDataType == UI_XEVT_DATA_TYPE || pstBLData->stUIData.iDataType == UI_R01_DATA_TYPE)
			{
				debug_sprintf(szDbgMsg, "%s: Consumer option data received", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				 /* Factors which can hinder the processing of this data
				  * 1. The device UI is busy with other transaction */

				if(bUIBusy == PAAS_TRUE)
				{
					debug_sprintf(szDbgMsg, "%s: UI is Busy here, so cant process this data", __FUNCTION__);
					APP_TRACE(szDbgMsg);

#if 0
					/* Queue the Business Logic data node for the business logic process
					 * to look into and subsequently process */
					rv = addDataToQ(pstBLData, sizeof(BLDATA_STYPE), szQId);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to add BL tran to queue",__FUNCTION__);
						APP_TRACE(szDbgMsg);
					}

					/* Free the BL data */
					freeBLData(pstBLData);
					pstBLData = NULL;
#endif
					svcWait(15);

					continue;
				}

				pstBLData = NULL;
				pstBLData = (BLDATA_PTYPE) getDataFrmQ(szQId, &iSize);
				if(pstBLData == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: SHOULD NOT COME HERE - Data not Present in Q", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					svcWait(15);//Should Not come here , because we just peeked from Q and saw the data ,so we should have data in Q
					continue;
				}

				if(pstBLData->stUIData.iCtrlId == PAAS_LINEITEMSCREEN_PAYPAL_BTN)
				{

					/* Pay with Paypal Button Pressed, Prompt with Payment Code / Swipe Card Screen
					 *
					 */

					debug_sprintf(szDbgMsg, "%s: Setting the UIBusy flag to PAAS_TRUE", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					bUIBusy = PAAS_TRUE;

					pstUITran = (BTRAN_PTYPE) malloc(sizeof(BTRAN_STYPE));
					if(pstUITran == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}

					memset(pstUITran, 0x00, sizeof(BTRAN_STYPE));

					pstUITran->stUIData.iCtrlId = pstBLData->stUIData.iCtrlId;
					strcpy(pstUITran->stUIData.szData,  pstBLData->stUIData.szData);

					rv = pthread_create(&(pstUITran->thId), NULL, doPaypalPaymentCapture, pstUITran);

					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to create UI Paypal Payment Capture thread", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}

				}
				else
				{
					if(isSTBLogicEnabled())
					{
						if(pstBLData->stUIData.iCtrlId == 23) //Cancel Button pressed
						{
							/*
							 * Praveen_P1: Fix for PTMX-292 - When Preswipe is done and cancel button is pressed immediately ,the MSR lights are not reenabled
							 * We receive STB data followed by card data or card data followed by stb data, if we get CANCEL in between these two then we are
							 * not honoring that
							 */
							if((bCardDataReceived == PAAS_FALSE && bSTBDataReceived == PAAS_TRUE) ||
								(bCardDataReceived == PAAS_TRUE && bSTBDataReceived == PAAS_FALSE)) //Either STB or Card Data received
							{
								debug_sprintf(szDbgMsg, "%s: Cancel is Pressed, Received either STB Data or Card Data only so far, not honoring", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								/* Free the BL data */
								freeBLData(pstBLData);
								pstBLData = NULL;

								continue;
							}
						}
					}
					debug_sprintf(szDbgMsg, "%s: UI is Not Busy here, so can process this data", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					debug_sprintf(szDbgMsg, "%s: Setting the UIBusy flag to TRUE", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					bUIBusy = PAAS_TRUE;

					pstUITran = (BTRAN_PTYPE) malloc(sizeof(BTRAN_STYPE));
					if(pstUITran == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}

					memset(pstUITran, 0x00, sizeof(BTRAN_STYPE));

					pstUITran->stUIData.iCtrlId = pstBLData->stUIData.iCtrlId;
					strcpy(pstUITran->stUIData.szData,  pstBLData->stUIData.szData);

					if(pstBLData->stUIData.iDataType == UI_R01_DATA_TYPE)
					{
						//Copying the Unsolicited card removal command response R01
						strcpy(pstUITran->stUIData.stEmvDtls.szEMVRespCmd,  pstBLData->stUIData.stEmvDtls.szEMVRespCmd);
					}

					rv = pthread_create(&(pstUITran->thId), NULL, doConsumerOptCature, pstUITran);

					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to create UI consumer option capture thread", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}
				}
			}
			else if(pstBLData->stUIData.iDataType == UI_SWIPE_DATA_TYPE || pstBLData->stUIData.iDataType == UI_EMV_DATA_TYPE)
			{
				debug_sprintf(szDbgMsg, "%s: Pre-Swipe/EMV/Tap data received data type: %d ", __FUNCTION__,pstBLData->stUIData.iDataType);
				APP_TRACE(szDbgMsg);

				 /* Factors which can hinder the processing of this data
				  * 1. The device UI is busy with other transaction */

				//if(bUIBusy == PAAS_TRUE && pstBLData->stUIData.iDataType == 2)
				if(bUIBusy == PAAS_TRUE && bUIBusyAllow == PAAS_FALSE)
				{
					debug_sprintf(szDbgMsg, "%s: UI is Busy here, so cant process the Pre-Swipe/EMV/Tap data", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					/* Queue the Business Logic data node for the business logic process
					 * to look into and subsequently process */
					/* Daivik:4/12/2015-Adding an extra check to ensure we add the PreSwipe/Tap data to the queue only if are we are currently in LI form or the Welcome form.
					 * We were seeing an issue where the U02 was being recieved after the UI busy was set true because of the Capture Request.
					 * But the preswipe/card data was being queued and was getting processed after the previous capture request.
					 * This was causing Device Busy issue.
					 */
					if(whichForm == LI_DISP_FRM || whichForm == WELCOME_DISP_FRM || whichForm == LI_DISP_OPT_FRM || whichForm == LI_FULL_DISP_FRM)
					{
#if 0
						rv = addDataToQ(pstBLData, sizeof(BLDATA_STYPE), szQId);
						if(rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: FAILED to add BL tran to queue",__FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
#endif
						svcWait(15);
						continue;
					}
					else
					{
						
						pstBLData = NULL;
						pstBLData = (BLDATA_PTYPE) getDataFrmQ(szQId, &iSize);
						if(pstBLData == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: SHOULD NOT COME HERE - Data not Present in Q", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							svcWait(15);//Should Not come here , because we just peeked from Q and saw the data ,so we should have data in Q
							continue;
						}

						/* Free the BL data */
						freeBLData(pstBLData);
						pstBLData = NULL;

						svcWait(15);

						continue;
					}
				}
				else
				{
					if(bUIBusy == PAAS_FALSE)
					{
						debug_sprintf(szDbgMsg, "%s: UI is NOT Busy here, so can process the Pre-Swipe/EMV/Tap data", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else if(bUIBusy == PAAS_TRUE && bUIBusyAllow == PAAS_TRUE && pstUITran == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: UI is Busy here, but UIBusyAllow is set, so can process the Pre-Swipe/EMV/Tap data", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						debug_sprintf(szDbgMsg, "%s: UITran is NULL", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else if(pstUITran != NULL)
					{
						debug_sprintf(szDbgMsg, "%s: UITran is not NULL Writing the data back to the queue", __FUNCTION__);
						APP_TRACE(szDbgMsg);


						debug_sprintf(szDbgMsg, "%s: UI is Busy here, so cant process this data", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						/* Queue the Business Logic data node for the business logic process
						 * to look into and subsequently process */
#if 0
						rv = addDataToQ(pstBLData, sizeof(BLDATA_STYPE), szQId);
						if(rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: FAILED to add BL tran to queue",__FUNCTION__);
							APP_TRACE(szDbgMsg);
						}

						/* Free the BL data */
						freeBLData(pstBLData);
						pstBLData = NULL;
#endif
						svcWait(15);

						continue;
					}


					pstBLData = NULL;
					pstBLData = (BLDATA_PTYPE) getDataFrmQ(szQId, &iSize);
					if(pstBLData == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: SHOULD NOT COME HERE - Data not Present in Q", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						svcWait(15);//Should Not come here , because we just peeked from Q and saw the data ,so we should have data in Q
						continue;
					}

					debug_sprintf(szDbgMsg, "%s: Setting the UIBusy flag to TRUE", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					//Praveen_P1: Setting it to busy for now, need to check whether it is really required
					bUIBusy = PAAS_TRUE;

					pstUITran = (BTRAN_PTYPE) malloc(sizeof(BTRAN_STYPE));
					if(pstUITran == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}

					memset(pstUITran, 0x00, sizeof(BTRAN_STYPE));

					if(pstBLData->stUIData.iDataType == 2)
					{
						debug_sprintf(szDbgMsg, "%s: UI Data Type is 2, Copying Card Track Details", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						memcpy(&pstUITran->stUIData.stCrdTrkInfo,  &pstBLData->stUIData.stCrdTrkInfo, sizeof(CARD_TRK_STYPE));
					}
					else if(pstBLData->stUIData.iDataType == 3)
					{
						debug_sprintf(szDbgMsg, "%s: UI Data Type is 3, Copying EMV details", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						memcpy(&pstUITran->stUIData.stEmvDtls, &pstBLData->stUIData.stEmvDtls, sizeof(EMVDTLS_STYPE));
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: UI Data Type is 2, Copying Card Track Details", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}


					pstUITran->stUIData.iDataType = pstBLData->stUIData.iDataType;

					rv = pthread_create(&(pstUITran->thId), NULL, processCardData, pstUITran);

					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: FAILED to create processCardData thread", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}
				}
			}
			else if(pstBLData->stUIData.iDataType == UI_EMVADMIN_DATA_TYPE)
			{

				pstBLData = NULL;
				pstBLData = (BLDATA_PTYPE) getDataFrmQ(szQId, &iSize);
				if(pstBLData == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: SHOULD NOT COME HERE - Data not Present in Q", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					svcWait(15);//Should Not come here , because we just peeked from Q and saw the data ,so we should have data in Q
					continue;
				}

				debug_sprintf(szDbgMsg, "%s: UI thread is doing EMV init, setting blogicUI as busy", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				debug_sprintf(szDbgMsg, "%s: Setting the UIBusy flag to PAAS_TRUE", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				bUIBusy = PAAS_TRUE;
				continue;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Should never come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* Free the BL data */
		freeBLData(pstBLData);
		pstBLData = NULL;

		svcWait(100);
	}

	/* Close the queue */
	deleteQueue(szQId);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: startBLTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE 
 * ============================================================================
 */
static int startBLTran(BLDATA_PTYPE pstBLData, BTRAN_PTYPE * pstBLTran,
												PAAS_BOOL *bUIBusy, PAAS_BOOL *bUIBusyAllow, int tranType)
{
	int			rv					= SUCCESS;
	int			iCmd				= -1;
	int			iFxn				= -1;
	int			iAllowTran			= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
	PAAS_BOOL	bUIRqd				= PAAS_FALSE;
	BTRAN_PTYPE	pstTmp				= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		rv = checkifUIRqd(pstBLData->szTranKey, &bUIRqd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get UI info", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		rv = getFxnNCmdForTran(pstBLData->szTranKey, &iFxn, &iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get function and command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		// T_MukeshS3: Added after VHQ support to keep device busy while processing VHQ updates
		if(getVHQUpdateInProgressFlag() == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: User interface is BUSY",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Device is Busy, Currently VHQ Updates are in Progress");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			rv = ERR_DEVICE_BUSY;
			break;
		}
		if( (bUIRqd == PAAS_TRUE) && (*bUIBusy == PAAS_TRUE) )
		{
			debug_sprintf(szDbgMsg, "%s: User interface is BUSY",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			acquireMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");

			/* Daivik:19/4/2016 -The below code was required to ensure we dont change giAllowLITran for the non LI commands.
			 *
			 */
			if(iFxn == SCI_LI && iCmd != SCI_SHOW)
			{
				giAllowLITran = 1;
				iAllowTran = 1;
				debug_sprintf(szDbgMsg, "%s: Setting giAllowLITran to 1 , since UI is busy and this is LI command",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				iAllowTran = 0;
			}

			if(iAllowTran)
			{
				debug_sprintf(szDbgMsg, "%s: Need to allow LI transaction/command",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Device is Busy, Currently Command Execution in Progress");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
				debug_sprintf(szDbgMsg, "%s: Return Device Busy since non LI Command and UI is busy",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = ERR_DEVICE_BUSY;
				releaseMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");
				break;
			}
			releaseMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");

		}

		if(iCmd == SCI_DISPLAY_MESSAGE && *bUIBusy)
		{
			debug_sprintf(szDbgMsg, "%s: UI is busy,  not honoring display message command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "UI is Busy Curently, Not Honoring Display Message Command");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			rv = ERR_DEVICE_BUSY;
			break;
		}

		/*
		 * We need to set UIBUSY flag which is sent to this function
		 * If UI is required for this tran, then set UIBusy flag here
		 */
		if((iFxn == SCI_LI && iCmd != SCI_SHOW) && giAllowLITran)
		{
			/* Daivik:21/7/16-When giAllowLITran is set , any non SHOW LI command will be buffered. giAllowLITran being set is an indication
			 * that UI was busy. Once the UI is free, all the buffered line items will be flushed.
			 * This change was required to avoid the device busy issue observed when sending ADD/REMOVE line item command at the
			 * end of a transaction when allowLiTran had got set to 1.
			 */
			debug_sprintf(szDbgMsg, "%s: Non SHOW LI Command and AllowLiTran is set,do not set UIBusy", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else if((bUIRqd == PAAS_TRUE))
		{
			debug_sprintf(szDbgMsg, "%s: Setting the UIBusy flag to TRUE", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			*bUIBusy = bUIRqd;
		}

		if(iCmd == SCI_DISPLAY_MESSAGE)
		{
			debug_sprintf(szDbgMsg, "%s: Setting the bUIBusyAllow flag to TRUE for DISPLAY_MESSAGE cmd", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			*bUIBusyAllow = PAAS_TRUE;
		}

		pstTmp = (BTRAN_PTYPE) malloc(sizeof(BTRAN_STYPE));
		if(pstTmp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pstTmp, 0x00, sizeof(BTRAN_STYPE));
		strcpy(pstTmp->szTranKey, pstBLData->szTranKey);
		strcpy(pstTmp->szMACLbl, pstBLData->szMacLbl);
		strcpy(pstTmp->szConnInfo, pstBLData->szConnInfo);
		pstTmp->bUIRqd = bUIRqd;

		setPOSInitiatedState(NOSTATE);

		if(tranType == ONLINE_TRAN)
		{
			rv = pthread_create(&(pstTmp->thId), NULL, onlineTransaction, pstTmp);
		}
		else
		{
			rv = pthread_create(&(pstTmp->thId), NULL, deviceTransaction, pstTmp);
		}

		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create tran thread",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pstBLData->szTranKey, 0x00, strlen(pstBLData->szTranKey));
		*pstBLTran = pstTmp;

		break;
	}

	if(rv != SUCCESS)
	{
		/* De-allocate any allocated memory */
		if(pstTmp != NULL)
		{
			free(pstTmp);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: checkToAllowTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int checkToAllowTran(int iFxn, int iCmd, PAAS_BOOL bUIBusy, int *piAllowTran)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(iFxn == SCI_LI && iCmd != SCI_SHOW)
	{
		debug_sprintf(szDbgMsg, "%s: It is line item command", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		//TODO: Need to check if we need to check the command type under LI
		if(bUIBusy == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: UI is busy with some other command, still need to allow LI", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* Need to allow LI also */
			*piAllowTran = 1;
		}

	}
	else
	{
		/* This is Non-LI command so later logic takes care of allowing this command */
		*piAllowTran = 0;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: getTranType
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE 
 * ============================================================================
 */
static int getTranType(char * szTranKey, int * iTranType, int *iFxn, int *iCmd)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		rv = getFxnNCmdForTran(szTranKey, iFxn, iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get function and command",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		switch(*iFxn)
		{
		case SCI_SEC:
		case SCI_SAF:
		case SCI_DEV:
		case SCI_ADM:
			*iTranType = DEVICE_TRAN;
			break;
		case SCI_SESS:
		case SCI_LI:
		case SCI_PYMT:
		case SCI_BATCH:
		case SCI_RPT:
			*iTranType = ONLINE_TRAN;
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: checkifUIRqd
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE 
 * ============================================================================
 */
static int checkifUIRqd(char * szTranKey, PAAS_BOOL * bUIRqd)
{
	int			rv				= SUCCESS;
	int			iFxn			= 0;
	int			iCmd			= 0;
	VERDTLS_STYPE	stVerDtls;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		rv = getFxnNCmdForTran(szTranKey, &iFxn, &iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get function and command",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(iCmd == SCI_VERSION)
		{
			memset(&stVerDtls, 0x00, sizeof(stVerDtls));
			rv = getVerDtlsForDevTran(szTranKey, &stVerDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get Version details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			if(stVerDtls.iDisplayFlag)
			{
				*bUIRqd = PAAS_TRUE;
			}
			else
			{
				*bUIRqd = PAAS_FALSE;
			}
			break;
		}

		if(iCmd == SCI_QUERY_NFCINI)
		{
			*bUIRqd = PAAS_FALSE;
			break;
		}

		switch(iFxn)
		{
		case SCI_SEC:
		case SCI_DEV:
			if(iCmd == SCI_GET_DEVICENAME || iCmd == SCI_SET_DEVICENAME || iCmd == SCI_GET_PAYMENT_TYPES || iCmd == SCI_GET_PARM || (iCmd == SCI_DISPLAY_MESSAGE && (whichForm == LI_DISP_FRM || whichForm == LI_DISP_OPT_FRM )))
			{
				*bUIRqd = PAAS_FALSE;
			}
			else
			{
				*bUIRqd = PAAS_TRUE;
			}
			break;

		case SCI_ADM:
			if(iCmd == SCI_GETCOUNTER || iCmd == SCI_SETTIME)
			{
				*bUIRqd = PAAS_FALSE;
			}
			else
			{
				*bUIRqd = PAAS_TRUE;
			}
			break;

		case SCI_SAF:
			*bUIRqd = PAAS_FALSE;
			break;

		case SCI_SESS:
		case SCI_LI:
		case SCI_PYMT:
		case SCI_BATCH:
			*bUIRqd = PAAS_TRUE;
			break;

		case SCI_RPT:
			*bUIRqd = PAAS_FALSE;
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d] UI Required [%d]", __FUNCTION__, rv, *bUIRqd);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: deviceTransaction
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE 
 * ============================================================================
 */
static void * deviceTransaction(void * arg)
{
	int				rv						= SUCCESS;
	//int				iVal					= 0;
	int				iFxn					= 0;
	int				iCmd					= 0;
	int				iAppLogEnabled			= isAppLogEnabled();
	int				locCurForm				= 0;
	int				iIsSAFRecsPres			= 0;
	int				iSameScreen				= 0;
	int				iStatusMsgDispInterval  = DFLT_STATUS_DISP_TIME;
	int				iDispMsgTimeout			= 0;
	int				iTmpCurForm				= 0;
	char			szStatMsg[100]			= "";
	char			szAppLogData[300]		= "";
	char			szAppLogDiag[300]		= "";
	//char			szTranStkKey[10]		= "";
	PAAS_BOOL		bSessPresent			= PAAS_FALSE;
	BTRAN_PTYPE		pstBLTran				= NULL;
	PAAS_BOOL		bShowIdlescreen		 	= PAAS_TRUE;
	//TRAN_PTYPE		pstTran					= NULL;
	DISPMSG_STYPE	stDispMsgDtls;
	UIREQ_STYPE		stUIReq;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();

#if 0
	/* Add signal handler for SIG USR1 (POS cancellation) */
	if(SIG_ERR == signal(SIGUSR1, blSigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGUSR1",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}


	/* Add signal handler for SIG USR2 (POS disconnection) */
	if(SIG_ERR == signal(SIGUSR2, blSigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGUSR2",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* Add signal handler for SIGQUIT (Device Reboot) */
	if(SIG_ERR == signal(SIGQUIT, blSigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGQUIT",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	pstBLTran = (BTRAN_PTYPE) arg;
	//stJmpLst[1].thId = pthread_self();

	while(1)
	{

		/* Get the function and command of the transaction */
		rv = getFxnNCmdForTran(pstBLTran->szTranKey, &iFxn, &iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get function and command",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		iVal = sigsetjmp(stJmpLst[1].jmpBuf, 0);
		if(iVal == CANCEL)
		{
			debug_sprintf(szDbgMsg, "%s: Received CANCEL request from POS", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/*
			 * Praveen_P1: Since the execution of onlinetransaction comes here
			 * from the current execution so releasing mutex if its been
			 * acquired when signal jump happens
			 */
			releaseAcquiredMutexLock(&(stBLData.uiRespMutex), "UI Resp Mutex");

			releaseAcquiredMutexLock(&(gptCardReadEnabledMutex), "Card Read Enabled Mutex");

			releaseAcquiredMutexLock(&(gptD41ActiveMutex), "D41 Active Mutex");

			releaseAcquiredMutexLock(&(gptOnlineLITranRetryMutex), "Online LI Retry Tran");

			/* Get the reference to the transaction placeholder */
			if(pstBLTran != NULL)
			{

				rv = getTran(pstBLTran->szTranKey, &pstTran);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get access to new tran", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
					if(pstTran != NULL)
					{
						if((char *)pstTran->data != NULL)
						{
							strcpy(szTranStkKey, (char *)pstTran->data); //Getting the SSI stack key
							releaseListQueueMutexLocks(szTranStkKey);
						}
					}
				}
			}
			debug_sprintf(szDbgMsg, "%s: Mutex Locks Released", __FUNCTION__);
			APP_TRACE(szDbgMsg);
#if 0
			disableCardReaders();
#endif
			/* Irrespective of whether card readers are enabled or not, we can now send the Cancel Request.
			 * And we dont have to wait for any reponse since we use the silentreset.
			 */
			cancelRequest();
			setCardReadEnabled(PAAS_FALSE);
			
			if(iCmd == SCI_SIGN_EX)
			{
				rv = setSIGBOXArea();
				if(rv != SUCCESS) // CID 67459 (#1 of 1): Unused value (UNUSED_VALUE) T_RaghavendranR1
				{
					debug_sprintf(szDbgMsg, "%s: Failed to Set SIGBOX Area", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else if(iCmd == SCI_DISPLAY_MESSAGE)
			{
				rv = hideMessageBox();
				if(rv != SUCCESS) // CID 67452 (#1 of 1): Unused value (UNUSED_VALUE) T_RaghavendranR1
				{
					debug_sprintf(szDbgMsg, "%s: Failed to hide Message Box Area", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}

			/* POS Cancelled the transaction */
			showPSGenDispForm(MSG_POS_CANCEL, FAILURE_ICON, 0);
			sprintf(szStatMsg, "Cancelled by POS");

			iSameScreen = 1;

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received Cancel Request From POS");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			rv = POS_CANCELLED;
			break;
		}
		else if(iVal == DISCONN)
		{
			debug_sprintf(szDbgMsg, "%s: Disconnected from POS", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/*
			 * Praveen_P1: Since the execution of onlinetransaction comes here
			 * from the current execution so releasing mutex if its been
			 * acquired when signal jump happens
			 */
			releaseAcquiredMutexLock(&(stBLData.uiRespMutex), "UI Resp Mutex");

			releaseAcquiredMutexLock(&(gptCardReadEnabledMutex), "Card Read Enabled Mutex");

			releaseAcquiredMutexLock(&(gptD41ActiveMutex), "D41 Active Mutex");

			releaseAcquiredMutexLock(&(gptOnlineLITranRetryMutex), "Online LI Retry Tran");

			/* Get the reference to the transaction placeholder */
			if(pstBLTran != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Get Transaction Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = getTran(pstBLTran->szTranKey, &pstTran);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get access to new tran", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
					if(pstTran != NULL)
					{
						if((char *)pstTran->data != NULL)
						{
							strcpy(szTranStkKey, (char *)pstTran->data); //Getting the SSI stack key
							releaseListQueueMutexLocks(szTranStkKey);
						}
					}
				}
			}
			debug_sprintf(szDbgMsg, "%s: Mutex Locks Released", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			disableCardReaders();

			/* POS Disconnected */
			showPSGenDispForm(MSG_POS_DISCONN, FAILURE_ICON, 0);
			sprintf(szStatMsg, "Disconnected from POS");

			iSameScreen = 1;

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Disconnected From POS");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			rv = ERR_USR_CANCELED;
			break;
		}
		else if(iVal == REBOOT)
		{
			debug_sprintf(szDbgMsg, "%s: Received Device reboot command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received Device Reboot Command, Rebooting the Device");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}

			rv	= ERR_DEV_REBOOTING;

			/* Set the proper return status for the transaction */
			setStatusInTran(pstBLTran->szTranKey, rv);

			/* Set the response details for the transaction */
			setGenRespDtlsinTran(pstBLTran->szTranKey, NULL);

			/* Send the response to the POS, using the SCI module */
			sendSCIResp(pstBLTran->szConnInfo, pstBLTran->szTranKey, pstBLTran->szMACLbl);

			debug_sprintf(szDbgMsg, "%s: Device Rebooting...", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Show the "Device rebooting" message on the device screen */
			showPSGenDispForm(MSG_REBOOT, STANDBY_ICON, iStatusMsgDispInterval);

			//Savelogs before rebooting
			saveLogs();

			//reboot
			svcRestart();
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setjmp rv = [%d]", __FUNCTION__, iVal);
			APP_TRACE(szDbgMsg);
		}
#endif

		pstBLTran = (BTRAN_PTYPE) arg;

		while(1)
		{


			if(getPOSInitiatedState())
			{
				rv = retValForPosIniState();
				break;
			}

			/* Get the function and command of the transaction */
			rv = getFxnNCmdForTran(pstBLTran->szTranKey, &iFxn, &iCmd);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get function and command",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			pstBLTran->iStatus = BUSY;

#if 0
			/*
			 * Never run FUNCTION_TYPE DEVICE transactions during an �open� session
			 * between the POS and the payment device. The device will reject any
			 * FUNCTION_TYPE DEVICE transactions during an �open� session.
			 *
			 */
			bSessPresent = isSessionInProgress();
			if(bSessPresent == PAAS_TRUE)
			{
				debug_sprintf(szDbgMsg, "%s: Session is in progress", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = ERR_IN_SESSION;
				break;
			}
#endif
			/*
			 * Never run FUNCTION_TYPE SECURITY transactions during an �open� session
			 * between the POS and the payment device.
			 * AMDOC case 140506-7234
			 * The Point application is not supposed to allow any SECURITY commands (REGISTER, TEST_MAC or UNREGISTER)
			 * when there is an open session.
			 */

			if(iFxn == SCI_SEC)
			{
				bSessPresent = isSessionInProgress();
				if(bSessPresent == PAAS_TRUE)
				{
					debug_sprintf(szDbgMsg, "%s: Session is in progress", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						if(iCmd == SCI_TESTMAC)
						{
							strcpy(szAppLogData, "Error, Failed to Process the TEST_MAC Command From POS, Session Already in Progress.");
							strcpy(szAppLogDiag, "Please Close the Session and Send TEST_MAC Command");
						}
						else
						{
							strcpy(szAppLogData, "Error, Failed to Process the POS Register/UnRegister Command, Session Already in Progress.");
							strcpy(szAppLogDiag, "Please Close the Session and Register/Unregister the POS");
						}
						addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
					}
					rv = ERR_IN_SESSION;
					bShowIdlescreen = PAAS_FALSE;

					//ArjunU1: Send POS Response here and break if session is busy for unregister and unregister all command only.
					if( (iCmd == SCI_UNREG) || (iCmd == SCI_UNREGALL) )
					{
						/* Set the proper return status for the transaction */
						setStatusInTran(pstBLTran->szTranKey, rv);

						/* Set the response details for the transaction */
						setGenRespDtlsinTran(pstBLTran->szTranKey, szStatMsg);

						/* Send the response to the POS, using the SCI module */
						sendSCIResp(pstBLTran->szConnInfo, pstBLTran->szTranKey, pstBLTran->szMACLbl);
					}
					break;
				}
			}

			//Get the current form number
			locCurForm = getCurFormNumber();
			// save current form number to glbal variable
			if(locCurForm != QRCODE_DISP_FRM)
			{
				setLastForm(locCurForm);
			}

			switch(iFxn)
			{
			case SCI_SEC:
				switch(iCmd)
				{
				case SCI_REG:
					rv = registerPOS(pstBLTran->szTranKey, szStatMsg);
					break;

				case SCI_UNREG:
					rv = unregisterPOS(pstBLTran->szTranKey, pstBLTran->szConnInfo, szStatMsg);
					break;

				case SCI_UNREGALL:
					rv = unregisterAllPOS(pstBLTran->szTranKey, pstBLTran->szConnInfo, pstBLTran->szMACLbl, szStatMsg);
					break;

				case SCI_EREG:
					rv = registerEncryptedPOS(pstBLTran->szTranKey, szStatMsg);
					break;

				case SCI_TESTMAC:
					sprintf(szStatMsg, "Match");
					rv = SUCCESS;
					break;
				}
				break;

				case SCI_DEV:
					switch(iCmd)
					{
					case SCI_SIGN:
					case SCI_SIGN_EX:
						rv = getSignForDevTran(pstBLTran->szTranKey, szStatMsg, iCmd);
						break;

					case SCI_LTY:
						rv = doOfflineLtyCapture(pstBLTran->szTranKey, szStatMsg);
						break;

					case SCI_PROVISION_PASS:
						/*if wallet support is enabled in XPI only then we can proces this command*/
						if(isWalletEnabled() == PAAS_FALSE)
						{
							debug_sprintf(szDbgMsg, "%s: Wallet is NOT enabled Provision Pass is NOT_SUPPORTED", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "This Command Cant't be Executed when Wallet Support is Disabled in Device");
								addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
							}
							rv = ERR_UNSUPP_OPR;
							break;
						}

						bSessPresent = isSessionInProgress();
						if(bSessPresent == PAAS_TRUE)
						{
							debug_sprintf(szDbgMsg, "%s: Session is in progress", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "This Command Cant't be Executed while Session is in Progress.");
								strcpy(szAppLogDiag, "Please Close the Session and Set the Time");
								addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
							}
							rv = ERR_IN_SESSION;
							bShowIdlescreen = PAAS_FALSE;
							break;
						}
						rv = processProvisionPass(pstBLTran->szTranKey, szStatMsg);
						break;

					case SCI_EMAIL:
						rv = doOfflineEmailCapture(pstBLTran->szTranKey, szStatMsg);
						break;

					case SCI_CUST_QUES:
						rv = doCustQuestion(pstBLTran->szTranKey, szStatMsg, &iSameScreen, &locCurForm);
						break;

					case SCI_SURVEY5:
						rv = doSurvey(pstBLTran->szTranKey, SURVEY_OPTION_5, szStatMsg);
						break;

					case SCI_SURVEY10:
						rv = doSurvey(pstBLTran->szTranKey, SURVEY_OPTION_10, szStatMsg);
						break;

					case SCI_VERSION:
						rv = getApplicationVersionInfo(pstBLTran->szTranKey, &iSameScreen, szStatMsg);
						break;

					case SCI_CHARITY:
						rv = doCharity(pstBLTran->szTranKey, szStatMsg);
						break;

					case SCI_GET_DEVICENAME:
						if(isDevNameEnabled() == PAAS_TRUE)
						{
							rv = getDeviceNameForDevTran(pstBLTran->szTranKey, szStatMsg);
						}
						else
						{
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "Device Name Setting is Not Enabled For the Device.");
								strcpy(szAppLogDiag, "Please Enable the Device Name Settings to Get the Device Name");
								addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, szAppLogDiag);
							}
							setStatusInTran(pstBLTran->szTranKey, ERR_UNSUPP_CMD);
							rv = ERR_UNSUPP_CMD;
						}
						break;
					case SCI_SET_DEVICENAME:
						if(isDevNameEnabled() == PAAS_TRUE)
						{
							rv = setDeviceNameForDevTran(pstBLTran->szTranKey, szStatMsg);
						}
						else
						{
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "Device Name Setting is Not Enabled For the Device.");
								strcpy(szAppLogDiag, "Please Enable the Device Name Settings and Set the Device Name");
								addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, szAppLogDiag);
							}
							setStatusInTran(pstBLTran->szTranKey, ERR_UNSUPP_CMD);
							rv = ERR_UNSUPP_CMD;
						}
						break;

					case SCI_CUST_BUTTON:
						rv = doCustomerButtonOption(pstBLTran->szTranKey, szStatMsg);
						break;

					case SCI_CREDIT_APP:
						rv = doCreditApplication(pstBLTran->szTranKey, szStatMsg, &iSameScreen, &locCurForm);
						break;

					case SCI_GET_PAYMENT_TYPES:
						bShowIdlescreen = PAAS_FALSE; //We need not navigate to idle screen, we can remain on the same screen
						rv = getEnabledPaymentTypes(pstBLTran->szTranKey, szStatMsg);
						break;

					case SCI_GET_CARD_DATA:
						rv = doGetCardData(pstBLTran->szTranKey, szStatMsg);
						break;

					case SCI_SET_PARM:
						bSessPresent = isSessionInProgress();
						if(bSessPresent == PAAS_TRUE)
						{
							debug_sprintf(szDbgMsg, "%s: Session is in progress", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "This Command Cant't be Executed while Session is in Progress.");
								strcpy(szAppLogDiag, "Please Close the Session and Set the Time");
								addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
							}
							rv = ERR_IN_SESSION;
							bShowIdlescreen = PAAS_FALSE;
							break;
						}

						iIsSAFRecsPres = isSAFRecordsPendingInDevice();
						if(iIsSAFRecsPres > 0)
						{
							debug_sprintf(szDbgMsg, "%s: SAF Records are Present, Cannot Process SET_PARM", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "This Command Cannot be Executed While SAF Records are Present.");
								strcpy(szAppLogDiag, "SAF Records Must be Processed Before Processing the SET_PARM Command");
								addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
							}
							sprintf(szStatMsg, "SAF Records are Present, Cannot Process SET_PARM.");
							rv = ERR_DEVICE_BUSY;
							bShowIdlescreen = PAAS_FALSE;
							break;
						}
						if( ! getHostConnectionStatus())
						{
							debug_sprintf(szDbgMsg, "%s: Host Connection is not available, Cannot Process SET_PARM", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "This Command Cannot be Executed While HOST Connection Is Not Available.");
								strcpy(szAppLogDiag, "HOST Connection Must be Available Before Processing the SET_PARM Command");
								addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
							}
							sprintf(szStatMsg, "Host is Not Currently Available to Process This Request");
							rv = ERR_NO_CONN;
							bShowIdlescreen = PAAS_FALSE;
							break;
						}
						rv = processSetParm(pstBLTran->szTranKey, szStatMsg);
						break;

					case SCI_DISPLAY_MESSAGE:
						iSameScreen = 1;

						memset(&stDispMsgDtls, 0x00, sizeof(DISPMSG_STYPE));
						rv = getDispMsgDtlsForDevTran(pstBLTran->szTranKey, &stDispMsgDtls);
						iDispMsgTimeout = stDispMsgDtls.iTimeOut;
						rv = processDispMsg(pstBLTran->szTranKey, szStatMsg);
						//iSameScreen = 0;
						break;

					case SCI_GET_PARM:
						iSameScreen = 1;
						rv = processGetParm(pstBLTran->szTranKey, szStatMsg);
						break;

					case SCI_QUERY_NFCINI:
						/*if wallet support is enabled in XPI only then we can process this command*/
						if(isWalletEnabled() == PAAS_FALSE)
						{
							debug_sprintf(szDbgMsg, "%s: Wallet is NOT enabled QUERY_NFC_INI is NOT_SUPPORTED", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							if(iAppLogEnabled == 1)
							{
								strcpy(szAppLogData, "This Command Cant't be Executed when Apple Wallet Support is Disabled");
								addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
							}
							rv = ERR_UNSUPP_CMD;
							break;
						}
						rv = processQueryNfcIniFile(pstBLTran->szTranKey);
						break;

					case SCI_DISP_QRCODE:
						iSameScreen = 1;
						rv = processDispQRCode(pstBLTran->szTranKey, szStatMsg);
						break;

					case SCI_CANCEL_QRCODE:
						iSameScreen = 1;
						rv = processCancelQRCode(szStatMsg);
						break;
					case SCI_CUST_CHECKBOX:
						rv = processCustCheckbox(pstBLTran->szTranKey, szStatMsg);
						break;

					}
					break;

					case SCI_SAF:
						switch(iCmd)
						{
						case SCI_QUERY:
							rv = doSAFQuery(pstBLTran->szTranKey, szStatMsg);
							break;

						case SCI_REMOVE:
							rv = doSAFRemoval(pstBLTran->szTranKey, szStatMsg);
							break;
						}
						break;

						case SCI_ADM:
							switch(iCmd)
							{
#if 0
							case SCI_REBOOT:
								rv = doRebootDevice(pstBLTran->szTranKey, pstBLTran->szConnInfo, pstBLTran->szMACLbl, szStatMsg);
								break;
#endif

							case SCI_SETTIME:
								bSessPresent = isSessionInProgress();
								if(bSessPresent == PAAS_TRUE)
								{
									debug_sprintf(szDbgMsg, "%s: Session is in progress", __FUNCTION__);
									APP_TRACE(szDbgMsg);
									if(iAppLogEnabled == 1)
									{
										strcpy(szAppLogData, "Error Cannot Set Time, Session in Progress.");
										strcpy(szAppLogDiag, "Please Close the Session and Set the Time");
										addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
									}
									rv = ERR_IN_SESSION;
									bShowIdlescreen = PAAS_FALSE;
									break;
								}
								rv = setTimeForDevice(pstBLTran->szTranKey, szStatMsg);
								break;

							case SCI_GETCOUNTER:
								bShowIdlescreen = PAAS_FALSE; //We need not navigate to idle screen, we can remain on the same screen
								rv = getCurrentCounterValue(pstBLTran->szTranKey, szStatMsg);
								break;

							case SCI_LANE_CLOSED:
								bShowIdlescreen = PAAS_FALSE;
								rv = doLaneClose(pstBLTran->szTranKey, pstBLTran->szMACLbl, szStatMsg);
								break;

							case SCI_APPLYUPDATES:
								iSameScreen = 1;
								bSessPresent = isSessionInProgress();
								if(bSessPresent == PAAS_TRUE)
								{
									debug_sprintf(szDbgMsg, "%s: Session is in progress", __FUNCTION__);
									APP_TRACE(szDbgMsg);
									if(iAppLogEnabled == 1)
									{
										strcpy(szAppLogData, "Error Cannot Apply Updates, Session in Progress.");
										strcpy(szAppLogDiag, "Please Close the Session and Apply Updates");
										addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
									}
									rv = ERR_IN_SESSION;
									break;
								}
								rv = applyVHQUpdates(pstBLTran->szTranKey, szStatMsg);
								break;
							}
							break;

							default:
								debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								rv = FAILURE;
								break;
			}

			break;
		}

		if(rv == POS_CANCELLED || rv == POS_DISCONNECTED)
		{
			int iDispMsg;

			memset(szStatMsg, 0x00, sizeof(szStatMsg));

			if(rv == POS_DISCONNECTED)
			{
				sprintf(szStatMsg, "POS Disconnected");
				iDispMsg = MSG_POS_DISCONN;
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Disconnection Event From POS");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
			}
			else
			{
				sprintf(szStatMsg, "Cancelled by POS");
				iDispMsg = MSG_POS_CANCEL;
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Received Cancel Request From POS");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
			}

			setPOSInitiatedState(NOSTATE);

		/* KranthiK1: 24-08-16 Moved it to macro CHECK_POS_INITIATED_STATE*/
#if 0
		/* Irrespective of whether card readers are enabled or not, we can now send the Cancel Request.
		 * And we dont have to wait for any reponse since we use the silentreset.
		 */
		cancelRequest();
		setCardReadEnabled(PAAS_FALSE);
#endif
#if 0
			if(iCmd == SCI_SIGN_EX)
			{
				rv = setSIGBOXArea();
			}
			else if(iCmd == SCI_DISPLAY_MESSAGE)
			{
				rv = hideMessageBox();
			}
#endif

			/* POS Cancelled the transaction */
			showPSGenDispForm(iDispMsg, FAILURE_ICON, 0);

			iSameScreen = 1;

		}

	if( (iCmd != SCI_UNREG) && (iCmd != SCI_UNREGALL) ) //We are sending response for UnRegister command in unregisterPOS function
	{
		/* Set the proper return status for the transaction */
		setStatusInTran(pstBLTran->szTranKey, rv);

		/* Set the response details for the transaction */
		setGenRespDtlsinTran(pstBLTran->szTranKey, szStatMsg);
#if 0 //Praveen_P1: Moved it down to get thread schronization
		/* Send the response to the POS, using the SCI module */
		sendSCIResp(pstBLTran->szConnInfo, pstBLTran->szTranKey, pstBLTran->szMACLbl);
#endif
	}

#if 0
	//If session is present, we would not have done any Device transaction so no need to change the screen
	if((pstBLTran->bUIRqd == PAAS_TRUE) && (bSessPresent == PAAS_FALSE))
#endif

	/*
	 * Praveen_P1: We are supporting buffering of LI items when Device is busy
	 * with Device commands
	 * After doing the device command, we need to go back to line item screen
	 * if LI command came during device command processing
	 */
	/* T_RaghavendranR1: For DISPLAY_MESSAGE command and if user PreSwiped during when Message Box is displayed, we need to
	 * display the LI screen all over again so as to update the text properly.
	 * There we check for if LI Buffer is present, we update the LI screen*/
	/* Daivik:15/7/2016 - giAllowLITran could get set due to failed LI commands ( Ex. When some XPI Screen is shown ). If we move out of LI Screen (For a command that required UI)
	 * only then we need to show Updated Lineitem screen.
	 * This change is to solve the issue where giAllowLITran was set and then we send a command like GET_COUNTER, we are displaying the LI screen even though we were in the
	 * transaction status screen.
	 */
	iTmpCurForm = getCurFormNumber();
	/* For QR Code displayed on the FULL screen ( in non LI cases ), the update of LI screen is not required. */
	if((giAllowLITran || getRetryLITran()) && (iCmd != SCI_DISPLAY_MESSAGE) && (pstBLTran->bUIRqd == PAAS_TRUE) && (iTmpCurForm != QRCODE_DISP_FRM))
	{
		if(isLineItemDisplayEnabled()) //We need to show line item screen only line item display is enabled
		{
			debug_sprintf(szDbgMsg, "%s: AllowLITran is set so showing lineitem screen", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/*
			 * Need to update line item screen first before updating with the new ones
			 * passing 0 as the parameter so that it does show up line item screen
			 */
			initUpdateForLineItemScreen(0, &stUIReq);

			/* Showing line item screen */
			showUpdatedLIScreen();
		}

		//Within showUpdatedLIScreen we will be setting AllowLITran  to 0;
//		acquireMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");
//
//		giAllowLITran = 0;
//
//		releaseMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");
	}
	else
	{
		if((pstBLTran->bUIRqd == PAAS_TRUE) && (bShowIdlescreen == PAAS_TRUE))
		{
			if(iSameScreen ==1) //For VERSION command we may not need to change the screen
			{
				debug_sprintf(szDbgMsg, "%s: Keeping the same screen", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* T_RaghavendranR1: After getting finished with Display message Command, and if my previous screen is
				 * LI screen or Welcome screen, i need to update the screen label widgets with correct text as user could have
				 * preswiped*/
				if(iCmd == SCI_DISPLAY_MESSAGE)
				{
					if((locCurForm == LI_DISP_FRM || locCurForm == LI_FULL_DISP_FRM || locCurForm == LI_DISP_OPT_FRM) && isSessionInProgress())
					{
						/* If the Previous Form is LI,It is Enough to update the Labels and do not Enable PreSwipe(PAAS_FALSE), because if Preswipe is done, no need to enable the
						 * PreSwipe. Else whatever already there in LI will take care of PreSwipe.*/
						//showLineItemScreen();
						showLIForm(PAAS_FALSE, PAAS_FALSE);
						/* Also the LI Buffer would have been filled, So updating the LI screen if LI Buffer is added*/
						if(giAllowLITran || getRetryLITran())
						{
							initUpdateForLineItemScreen(0, &stUIReq);
							/* Showing line item screen */
							showUpdatedLIScreen();
						}
					}
					else if(locCurForm == WELCOME_DISP_FRM && isSessionInProgress())
					{
						if( isCustomerWaitingForCashier() == PAAS_TRUE )
						{
							showWelcomeScreen(PAAS_TRUE);
						}
						else
						{
							showWelcomeScreen(PAAS_FALSE);
						}
					}
				}
			}
			else
			{
				/*ArjunU1: According to new requirement, Application should revert back to its original form/state after
				 * 		   processing device commands.
				 */
				switch(locCurForm)
				{
				case LI_DISP_FRM:
				case LI_DISP_OPT_FRM:
				case LI_FULL_DISP_FRM:

					debug_sprintf(szDbgMsg, "%s: Showing Line Item Screen", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					/* Since the UI interaction is over for the current transaction, it
					 * should revert back to the previous form. i.e Line item Screen */
					if(isSessionInProgress())
					{
						rv = showLineItemScreen();
						if(rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: Error Showing LI Screen, So Switching to IDLE Screen", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							showIdleScreen();
						}
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Session Not in Progress, So Switching to IDLE Screen", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						showIdleScreen();
					}
					break;

				case WELCOME_DISP_FRM:

					debug_sprintf(szDbgMsg, "%s: Showing Welcome Screen", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					/* Since the UI interaction is over for the current transaction, it
					 * should revert back to the previous form. i.e Welcome Screen */
					if(isSessionInProgress())
					{
						if( isCustomerWaitingForCashier() == PAAS_TRUE )
						{
							showWelcomeScreen(PAAS_TRUE);
						}
						else
						{
							showWelcomeScreen(PAAS_FALSE);
						}
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Session Not in Progress, So Switching to IDLE Screen", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						showIdleScreen();
					}
					break;

				default:
					debug_sprintf(szDbgMsg, "%s: Showing Idle Screen (Default case)", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					/* Since the UI interaction is over for the current transaction, it
					 * should revert back to the previous form. i.e Idle Screen */
					showIdleScreen();
					break;

				}
			}
		}
	}

	if( (iCmd != SCI_UNREG) && (iCmd != SCI_UNREGALL) ) //We are sending response for UnRegister command in unregisterPOS function
	{
		/* Send the response to the POS, using the SCI module */
		sendSCIResp(pstBLTran->szConnInfo, pstBLTran->szTranKey, pstBLTran->szMACLbl);
	}

	if(pstBLTran->bUIRqd == PAAS_FALSE && iSameScreen ==1 && iCmd == SCI_DISPLAY_MESSAGE)
	{
		if(iDispMsgTimeout >0)
		{
			svcWait(iDispMsgTimeout * 1000L);
			locCurForm = getCurFormNumber();
			if((locCurForm == LI_DISP_FRM || locCurForm == LI_DISP_OPT_FRM) && isSessionInProgress())
			{
				if(giAllowLITran || getRetryLITran())
				{
					//If Line Items were buffered we should update the screen now.
					initUpdateForLineItemScreen(0, &stUIReq);
					/* Showing line item screen */
					showUpdatedLIScreen();
				}
				else
				{
				rv = showLineItemScreen();
			}
		}
	}
	}

	if(getPOSInitiatedState())
	{
		setPOSInitiatedState(TRANCOMPLETED);
	}

	/* Mark the status of the transaction as complete */
	pstBLTran->iStatus = COMPLETE;

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: onlineTransaction
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE 
 * ============================================================================
 */
static void * onlineTransaction(void * arg)
{
	int				rv						= SUCCESS;
	//int				iVal					= 0;
	int				iFxn					= 0;
	int				iCmd					= 0;
	int				iSameScreen				= 0;
	int				iPaymentType			= -1;
	int				iStatusMsgDispInterval  = DFLT_STATUS_DISP_TIME;
	int				iAppLogEnabled			= isAppLogEnabled();
	char			szStatMsg[100]			= "";
	char			szTranStkKey[10]		= "";
	char			szAppLogData[300]		= "";
	char			szAppLogDiag[300]		= "";
	PAAS_BOOL		bDupTranDetectedflg		= PAAS_FALSE;
	PAAS_BOOL		bPrvCrdSAFAllwd			= PAAS_FALSE;
	PAAS_BOOL		bPOSDisConnected		= PAAS_FALSE;
	TRAN_PTYPE		pstTran					= NULL;
	BTRAN_PTYPE		pstBLTran				= NULL;
	CARDDTLS_STYPE 	stCardDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();

#if 0
	/* Add signal handler for SIG USR1 (POS cancellation) */
	if(SIG_ERR == signal(SIGUSR1, blSigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGUSR1",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}


	/* Add signal handler for SIG USR2 (POS disconnection) */
	if(SIG_ERR == signal(SIGUSR2, blSigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGUSR2",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* Add signal handler for SIGQUIT (Device Reboot) */
	if(SIG_ERR == signal(SIGQUIT, blSigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGQUIT",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	pstBLTran = (BTRAN_PTYPE) arg;
	stJmpLst[0].thId = pthread_self();

	while(1)
	{
		iVal = sigsetjmp(stJmpLst[0].jmpBuf, 0);
		if(iVal == CANCEL)
		{
			debug_sprintf(szDbgMsg, "%s: Received CANCEL request from POS", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received Cancel Request From POS");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			/*
			 * Praveen_P1: Since the execution of onlinetransaction comes here
			 * from the current execution so releasing mutex if its been
			 * acquired when signal jump happens
			 */
			releaseAcquiredMutexLock(&(stBLData.uiRespMutex), "UI Resp Mutex");

			releaseAcquiredMutexLock(&(gptCardReadEnabledMutex), "Card Read Enabled Mutex");

			releaseAcquiredMutexLock(&(gptD41ActiveMutex), "D41 Active Mutex");

			releaseAcquiredMutexLock(&(gptOnlineLITranRetryMutex), "Online LI Retry Tran");

			/* Get the reference to the transaction placeholder */
			if(pstBLTran != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Get Transaction Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = getTran(pstBLTran->szTranKey, &pstTran);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get access to new tran", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
					if(pstTran != NULL)
					{
						if((char *)pstTran->data != NULL)
						{
							strcpy(szTranStkKey, (char *)pstTran->data); //Getting the SSI stack key
							releaseListQueueMutexLocks(szTranStkKey);
						}
					}
				}
			}
			debug_sprintf(szDbgMsg, "%s: Mutex Locks Released", __FUNCTION__);
			APP_TRACE(szDbgMsg);
#if 0
			if(getCardReadEnabled() == PAAS_TRUE)
			{
				disableCardReaders();
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Card Readers are not enabled, sending CANCEL request to disble current session", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(whichForm == FA_PIN_ENTRY_FRM) //Daivik: If we are cancelling on PIN entry screen it shows tender selection screen and then POS cancelled message
				{
					clearScreen(1);
				}

				/* Cancelling current event on the UI/EMV agent. */
				cancelRequest();
			}
#endif
			/* Irrespective of whether card readers are enabled or not, we can now send the Cancel Request.
			 * And we dont have to wait for any reponse since we use the silentreset.
			 */
			cancelRequest();
			setCardReadEnabled(PAAS_FALSE);

			setAppState(TRANSACTION_COMPLETED);
			setDetailedAppState(DETAILED_TRANSACTION_COMPLETED);

			/*
			 * Praveen_P1: If EMV card is present, we were not showing CARD REMOVE message
			 * Fix for PTMX-559
			 * Its better to send this message for SALE/REFUND only, otherwise
			 * it will sent for all commands when EMV is enabled
			 */
			if(isEmvEnabledInDevice())
			{
				checkCrdPrsnceAndShwMsgToRemove();
				if(isEmvKernelSwitchingAllowed())
				{
					//AjayS2: 14/Jan/2016: Setting send_u01 to 0, on Cancel Command
					setXPIParameter("send_u01", "0");
				}
			}

			/* POS Cancelled the transaction */
			showPSGenDispForm(MSG_POS_CANCEL, FAILURE_ICON, 0);
			sprintf(szStatMsg, "Cancelled by POS");

			iSameScreen = 1;

			/*
			 * If Pre-Swipe details are present in the memory, flushing them here
			 * since POS cancelled the transaction
			 *
			 */
			if(flushCardDetails(pstBLTran->szTranKey) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure while resetting pre-swipe details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			rv = POS_CANCELLED;

			break;
		}
		else if(iVal == DISCONN)
		{
			debug_sprintf(szDbgMsg, "%s: Disconnected from POS", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/*
			 * Praveen_P1: Since the execution of onlinetransaction comes here
			 * from the current execution so releasing mutex if its been
			 * acquired when signal jump happens
			 */
			releaseAcquiredMutexLock(&(stBLData.uiRespMutex), "UI Resp Mutex");

			releaseAcquiredMutexLock(&(gptCardReadEnabledMutex), "Card Read Enabled Mutex");

			releaseAcquiredMutexLock(&(gptD41ActiveMutex), "D41 Active Mutex");

			releaseAcquiredMutexLock(&(gptOnlineLITranRetryMutex), "Online LI Retry Tran");

			/* Get the reference to the transaction placeholder */
			if(pstBLTran != NULL)
			{

				rv = getTran(pstBLTran->szTranKey, &pstTran);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get access to new tran", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
					if(pstTran != NULL)
					{
						if((char *)pstTran->data != NULL)
						{
							strcpy(szTranStkKey, (char *)pstTran->data); //Getting the SSI stack key
							releaseListQueueMutexLocks(szTranStkKey);
						}
					}
				}
			}

			bPOSDisConnected = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Mutex Locks Released", __FUNCTION__);
			APP_TRACE(szDbgMsg);
#if 0
			if(getCardReadEnabled() == PAAS_TRUE)
			{
				disableCardReaders();
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Card Readers are not enabled, sending CANCEL request to disble current session", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(whichForm == FA_PIN_ENTRY_FRM) //Daivik: If we are cancelling on PIN entry screen it shows tender selection screen and then POS disconnection message
				{
					clearScreen(1);
				}

				/* Cancelling current event on the UI/EMV agent. */
				cancelRequest();
			}
#endif
			/* Irrespective of whether card readers are enabled or not, we can now send the Cancel Request.
			 * And we dont have to wait for any reponse since we use the silentreset.
			 */
			cancelRequest();
			setCardReadEnabled(PAAS_FALSE);

			setAppState(TRANSACTION_COMPLETED);
			setDetailedAppState(DETAILED_TRANSACTION_COMPLETED);

			/* POS Disconnected */
			showPSGenDispForm(MSG_POS_DISCONN, FAILURE_ICON, 0);
			sprintf(szStatMsg, "Disconnected from POS");

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Disconnected From POS");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			if(isEmvEnabledInDevice())
			{
				if(isEmvKernelSwitchingAllowed())
				{
					//AjayS2: 14/Jan/2016: Setting send_u01 to 0, on POS disconnected
					setXPIParameter("send_u01", "0");
				}
			}

			iSameScreen = 1;

			/*
			 * If Pre-Swipe details are present in the memory, flushing them here
			 * since we got disconnected during the payment transaction
			 *
			 */
			if(flushCardDetails(pstBLTran->szTranKey) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure while resetting pre-swipe details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			releaseSSICurlHandleLock();

			rv = ERR_USR_CANCELED;
			break;
		}
		else if(iVal == REBOOT)
		{
			debug_sprintf(szDbgMsg, "%s: Received Device reboot command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received Device Reboot Command, Rebooting the Device");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}
			rv	= ERR_DEV_REBOOTING;

			/* Set the proper return status for the transaction */
			setStatusInTran(pstBLTran->szTranKey, rv);

			/* Set the response details for the transaction */
			setGenRespDtlsinTran(pstBLTran->szTranKey, NULL);

			/* Send the response to the POS, using the SCI module */
			sendSCIResp(pstBLTran->szConnInfo, pstBLTran->szTranKey, pstBLTran->szMACLbl);

			debug_sprintf(szDbgMsg, "%s: Device Rebooting...", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Show the "Device rebooting" message on the device screen */
			showPSGenDispForm(MSG_REBOOT, STANDBY_ICON, iStatusMsgDispInterval);

			//Savelogs before rebooting
			saveLogs();

			//Appcleanup

			//reboot
			svcRestart();
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setjmp rv = [%d]", __FUNCTION__, iVal);
			APP_TRACE(szDbgMsg);
		}
#endif

		pstBLTran = (BTRAN_PTYPE) arg;

		while(1)
		{
			if(getPOSInitiatedState())
			{
				rv = retValForPosIniState();
				break;
			}

		pstBLTran->iStatus = BUSY;

		/* Get the function and command of the transaction */
		rv = getFxnNCmdForTran(pstBLTran->szTranKey, &iFxn, &iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get function and command",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Function = [%d], Command = [%d]", __FUNCTION__, iFxn, iCmd);
		APP_TRACE(szDbgMsg);

		switch(iFxn)
		{
		case SCI_SESS:

			debug_sprintf(szDbgMsg, "%s: Function Type = [%s]", __FUNCTION__, "SESSION");
			APP_TRACE(szDbgMsg);

			switch(iCmd)
			{
			case SCI_START:
				debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "START");
				APP_TRACE(szDbgMsg);

				rv = startNewSession(pstBLTran->szMACLbl, pstBLTran->szTranKey, 
																	szStatMsg);
				break;

			case SCI_FINISH:
				debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "FINISH");
				APP_TRACE(szDbgMsg);

				rv = finishSession(pstBLTran->szMACLbl, szStatMsg, PAAS_FALSE);
				/*
				 *Praveen_P1: Incase of error we are going to idle screen
				 *			  This error comes in multi POS situation
				 *			  One POS opened the session, other POS tried to close
				 *			  it with out opening the session
				 */
				if(rv == ERR_DEVICE_BUSY)
				{
					iSameScreen = 1;
				}
				break;
			}
			break;
		case SCI_LI:

#if 0
			fprintf(fptr, "Line item Command\n");
#endif

			debug_sprintf(szDbgMsg, "%s: Function Type = [%s]", __FUNCTION__, "LINEITEM");
			APP_TRACE(szDbgMsg);

			/*
			 * Added for logging/debugging purpose
			 */
			switch(iCmd)
			{
			case SCI_ADD:
				debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "ADD");
				APP_TRACE(szDbgMsg);
				strcpy(szAppLogData, "Received Add Line Item Command");
				break;
			case SCI_REMOVE:
				debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "REMOVE");
				APP_TRACE(szDbgMsg);
				strcpy(szAppLogData, "Received Remove Line Item Command");
				break;
			case SCI_REMOVEALL:
				debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "REMOVEALL");
				APP_TRACE(szDbgMsg);
				strcpy(szAppLogData, "Received RemoveAll Line Item Command");
				break;
			case SCI_OVERRIDE:
				debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "OVERRIDE");
				APP_TRACE(szDbgMsg);
				strcpy(szAppLogData, "Received Override Line Item Command");
				break;
			case SCI_SHOW:
				debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "SHOW");
				APP_TRACE(szDbgMsg);
				strcpy(szAppLogData, "Received Show Line Item Command");
				break;
			default:
				break;
			}
			if(iAppLogEnabled == 1)
			{
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}

			if(isLineItemDisplayEnabled())
			{
				rv = execLIFlow(pstBLTran->szMACLbl, pstBLTran->szTranKey, szStatMsg, iCmd, giAllowLITran);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: line Item Display Not Enabled, Command Not supported", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Line Item Display Setting Not Enabled.");
					strcpy(szAppLogDiag, "Please Enable the Line Item Display Setting and Execute Line Item Command");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, szAppLogDiag);
				}

				rv = ERR_UNSUPP_CMD;
			}

			break;

		case SCI_PYMT:
			debug_sprintf(szDbgMsg, "%s: Function Type = [%s]", __FUNCTION__, "PAYMENT");
			APP_TRACE(szDbgMsg);

			rv = execPymtFlow(pstBLTran->szMACLbl, pstBLTran->szTranKey,
															szStatMsg, iCmd);
			/*
			 *Praveen_P1: Incase of error we are going to idle screen
			 *			  This error comes in multi POS situation
			 *			  One POS opened the session, other POS tried to close
			 *			  it with out opening the session
			 *			  It is good not to change the screen if we get Device busy
			 */
			if(rv == ERR_DEVICE_BUSY)
			{
				iSameScreen = 1;
			}
			if(rv == ERR_DEVICE_APP)
			{
				showPSGenDispForm(MSG_ERROR, FAILURE_ICON, iStatusMsgDispInterval);
			}

			/*
			 * Praveen_P1: Irrespective of status of this function, want to remain on the same screen
			 * so that we gain time by not showing the idle screen
			 */
			iSameScreen = 1;

			break;

		case SCI_BATCH:
			debug_sprintf(szDbgMsg, "%s: Function Type = [%s]", __FUNCTION__, "BATCH");
			APP_TRACE(szDbgMsg);

			rv = execBatchTranFlow(pstBLTran->szTranKey, szStatMsg, iCmd);
			break;

		case SCI_RPT:
			debug_sprintf(szDbgMsg, "%s: Function Type = [%s]", __FUNCTION__, "REPORT");
			APP_TRACE(szDbgMsg);

			rv = execReportTranFlow(pstBLTran->szTranKey, szStatMsg, iCmd);
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		break; 
	}
	debug_sprintf(szDbgMsg, "%s: Need to send the Online Tran Response", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/* Don't check Resync key for function type "SESSION" and "LINE_ITEM" */
	if((iFxn != SCI_SESS) && (iFxn != SCI_LI))
	{
		/* Do device resync key if "adminPcktReq" is set in the memory */
		if(SUCCESS != procDeviceResyncKey())
		{
			debug_sprintf(szDbgMsg, "%s: Device Resync Key Failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	if(rv == POS_CANCELLED || rv == POS_DISCONNECTED)
	{
		int iDispMsg;

		if(rv == POS_DISCONNECTED)
		{
			sprintf(szStatMsg, "POS Disconnected");
			iDispMsg = MSG_POS_DISCONN;
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Disconnection Event From POS");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
		}
		else
		{
			sprintf(szStatMsg, "Cancelled by POS");
			iDispMsg = MSG_POS_CANCEL;
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received Cancel Request From POS");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
		}

		setPOSInitiatedState(NOSTATE);
/*KranthiK1: 24Oct-16 Moved it to macro CHECK_POS_INITIATED_STATE*/
#if 0
		/* Irrespective of whether card readers are enabled or not, we can now send the Cancel Request.
		 * And we dont have to wait for any reponse since we use the silentreset.
		 */
		cancelRequest();
		setCardReadEnabled(PAAS_FALSE);
#endif
		setAppState(TRANSACTION_COMPLETED);
		setDetailedAppState(DETAILED_TRANSACTION_COMPLETED);

		/*
		 * Praveen_P1: If EMV card is present, we were not showing CARD REMOVE message
		 * Fix for PTMX-559
		 * Its better to send this message for SALE/REFUND only, otherwise
		 * it will sent for all commands when EMV is enabled
		 */
		if(isEmvEnabledInDevice())
		{
			checkCrdPrsnceAndShwMsgToRemove();
			if(isEmvKernelSwitchingAllowed())
			{
				//AjayS2: 14/Jan/2016: Setting send_u01 to 0, on Cancel Command
				setXPIParameter("send_u01", "0");
			}
		}

		/* POS Cancelled the transaction */
		showPSGenDispForm(iDispMsg, FAILURE_ICON, 0);

		iSameScreen = 1;

		/*
		 * If Pre-Swipe details are present in the memory, flushing them here
		 * since POS cancelled the transaction
		 *
		 */
		if(flushCardDetails(pstBLTran->szTranKey) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failure while resetting pre-swipe details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Set the proper return status for the transaction */
	setStatusInTran(pstBLTran->szTranKey, rv);

	/* Set the response details for the transaction */
	setGenRespDtlsinTran(pstBLTran->szTranKey, szStatMsg);

#if 0 //Praveen_P1: Moved it down to get the thread schronization
	/* Send the response to the POS, using the SCI module */
	sendSCIResp(pstBLTran->szConnInfo, pstBLTran->szTranKey, pstBLTran->szMACLbl);
#endif
	if (iFxn == SCI_PYMT)
	{
		/* Get the reference to the transaction placeholder */
		rv = getTran(pstBLTran->szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get access to new tran", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
		strcpy(szTranStkKey, (char *)pstTran->data); //Getting the SSI stack key

		getPymtTypeForPymtTran(szTranStkKey, &iPaymentType);

		getPrivCrdSAFTranIndForPymtTran(szTranStkKey, &bPrvCrdSAFAllwd);

		if( (iPaymentType == PYMT_PRIVATE) && (iCmd != SCI_CREDIT) && ( bPrvCrdSAFAllwd != PAAS_TRUE) )
		{
			memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
			getCardDtlsForPymtTran(szTranStkKey, &stCardDtls);

			//if( strcmp(stCardDtls.szPymtMedia, "KOHLS") == SUCCESS)
			if(stCardDtls.bKohlsChargeCard == PAAS_TRUE)
			{
				iSameScreen = 1;
				debug_sprintf(szDbgMsg, "%s: Kohls Card is selected", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		if( isDupSwipeDetectEnabled() )
		{
			//Check whether duplicate swipe detected
			getDupTranIndicatorForPymtTran( szTranStkKey, &bDupTranDetectedflg);
			if( bDupTranDetectedflg == PAAS_TRUE)
			{
				iSameScreen = 1;
				debug_sprintf(szDbgMsg, "%s: Duplicate Swipe detected", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
	}

	/* Since the UI interaction is over for the current transaction, it
	 * should revert back the screen to idle screen */
	if(((iFxn == SCI_SESS) && (iCmd == SCI_START)
			&& bPOSDisConnected == PAAS_FALSE)     ||/* Praveen_P1: Sometimes POS getting disconnected on the start session, at that time it remains on the POS disconnection screen */
		(iFxn == SCI_LI
			&& bPOSDisConnected == PAAS_FALSE)     ||
		(iFxn == SCI_BATCH)                        || /*Praveen_P1: For Batch commands also keeping on the same screen since we are not using UI */
		iSameScreen 							   ||
		((iFxn == SCI_RPT) && (iCmd != SCI_LPTOKEN)))
	{
		debug_sprintf(szDbgMsg, "%s: Keeping the same screen", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		showIdleScreen();
	}

	/* Send the response to the POS, using the SCI module */
	sendSCIResp(pstBLTran->szConnInfo, pstBLTran->szTranKey, pstBLTran->szMACLbl);

	if(getPOSInitiatedState())
	{
		setPOSInitiatedState(TRANCOMPLETED);
	}

	/* Mark the status of the transaction as complete */
	pstBLTran->iStatus = COMPLETE;

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: freeBLData
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: none
 * ============================================================================
 */
static void freeBLData(BLDATA_PTYPE pstBLData)
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(pstBLData->szTranKey) > 0)
	{
		/* Free any transaction data */
		deleteTran(pstBLData->szTranKey);
	}

	free(pstBLData);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

#if 0
/*
 * ============================================================================
 * Function Name: blSigHandler
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: none
 * ============================================================================
 */
static void blSigHandler(int iSigNo)
{
	int			iCnt			= 0;
	pthread_t	thId			= 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	thId = pthread_self();

	if(iSigNo == SIGUSR1)
	{
		debug_sprintf(szDbgMsg, "%s: Signal SIGUSR1 delivered", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else if(iSigNo == SIGUSR2)
	{
		debug_sprintf(szDbgMsg, "%s: Signal SIGUSR2 delivered", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else if(iSigNo == SIGQUIT)
	{
		debug_sprintf(szDbgMsg, "%s: Signal SIGQUIT delivered", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Signal [%d]", __FUNCTION__, iSigNo);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Checking for thread id", __FUNCTION__);
	APP_TRACE(szDbgMsg);//Praveen_P1: for testing purpose, remove it later

	for(iCnt = 0; iCnt < 2; iCnt++)
	{
		debug_sprintf(szDbgMsg, "%s: setJmp thread id [%d] current thread id[%d]", __FUNCTION__, (int)stJmpLst[iCnt].thId, (int)thId);
		APP_TRACE(szDbgMsg);//Praveen_P1: for testing purpose, remove it later

		if(stJmpLst[iCnt].thId == thId)
		{
			if(iSigNo == SIGUSR1)
			{
				debug_sprintf(szDbgMsg, "%s: Doing SiglongJmp for CANCEL", __FUNCTION__);
				APP_TRACE(szDbgMsg);//Praveen_P1: for testing purpose, remove it later
				siglongjmp(stJmpLst[iCnt].jmpBuf, CANCEL);
			}
			else if(iSigNo == SIGUSR2)
			{
				debug_sprintf(szDbgMsg, "%s: Doing SiglongJmp for DISCONNECTION", __FUNCTION__);
				APP_TRACE(szDbgMsg);//Praveen_P1: for testing purpose, remove it later
				siglongjmp(stJmpLst[iCnt].jmpBuf, DISCONN);
			}
			else if(iSigNo == SIGQUIT)
			{
				debug_sprintf(szDbgMsg, "%s: Doing SiglongJmp for REBOOT", __FUNCTION__);
				APP_TRACE(szDbgMsg);//Praveen_P1: for testing purpose, remove it later
				siglongjmp(stJmpLst[iCnt].jmpBuf, REBOOT);
			}
		}
	}

	return;
}
#endif
/*
 * ============================================================================
 * Function Name: processCardData
 *
 * Description	: This API does the processing of card data received received
 * 					from pre-swipe/tap.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static void *processCardData(void *arg)
{
		int				rv						= SUCCESS;
		int				iStatusMsgDispInterval 	= DFLT_STATUS_DISP_TIME;
		int 			iCardSrc				= 0;
		int				iLen					= 0;
		int				iCnt					= 0;
		int				iEMVSelLan				= 0;
		int				iAppLogEnabled			= isAppLogEnabled();
		int				iStatus					= COMPLETE;
		char			szAppLogData[300]		= "";
static	char *			pszSBTData				= NULL;
		char *			cCurPtr					= NULL;
		char *			cNxtPtr					= NULL;
		char **			szTracks				= NULL;
		char **			szRsaTracks				= NULL;
		char **			szVsdEncData			= NULL;
		char			szClrDt[10]				= "";
		char			szTrkPAN[30]			= "";
		PAAS_BOOL		bEmvEnabled				= PAAS_FALSE;
		PAAS_BOOL		bTrackDataPresent 		= PAAS_FALSE;
		PAAS_BOOL		bValidate				= PAAS_FALSE;
//		PAAS_BOOL		bInvalidCard			= PAAS_FALSE;
		ENC_TYPE 		eEncType				= 0;
		BTRAN_PTYPE		pstUITran				= NULL;
		CARD_TRK_PTYPE	pstCard					= NULL;
		EMVDTLS_PTYPE	pstEmvDtls				= NULL;
		BACKUP_CARDDTLS_STYPE	stBkupCrdDtls;
static	CARDDTLS_STYPE	stCardDtls;
		//AID_NAME_NODE_PTYPE pstAIDName			= NULL;
		APPLN_STATE		iLastAppState;
		UIREQ_STYPE		stUIReq					= {0};

#ifdef DEBUG
	char szDbgMsg[256]							= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(&stBkupCrdDtls, 0x00, sizeof(BACKUP_CARDDTLS_STYPE));
	memset(szTrkPAN, 0x00, sizeof(szTrkPAN));

	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();

	//Get encryption type enabled in the device
	eEncType = getEncryptionType();

	//Get EMV status
	bEmvEnabled = isEmvEnabledInDevice();

	setCardDataInBQueueFlag(PAAS_FALSE);

	pstUITran = (BTRAN_PTYPE) arg;

	while(1)
	{
		if(pstUITran->stUIData.iDataType == UI_EMV_DATA_TYPE)
		{
			debug_sprintf(szDbgMsg, "%s: EMV res cmd: %s", __FUNCTION__,pstUITran->stUIData.stEmvDtls.szEMVRespCmd);
			APP_TRACE(szDbgMsg);

			if( strcmp(pstUITran->stUIData.stEmvDtls.szEMVRespCmd, EMV_U00_REQ) ==0 ||strcmp(pstUITran->stUIData.stEmvDtls.szEMVRespCmd, EMV_U02_REQ) ==0 )
			{
				if(isSTBLogicEnabled() && pstUITran->stUIData.stEmvDtls.stEMVCardStatusU02.bSTBDataReceived == PAAS_TRUE)
				{
					if(bCardDataReceived == PAAS_FALSE)
					{
						debug_sprintf(szDbgMsg, "%s: Setting bSTBDataReceived as PAAS_TRUE", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						bSTBDataReceived = PAAS_TRUE;

						iLen = strlen(pstUITran->stUIData.stEmvDtls.szSpinBinData);

						if(pszSBTData != NULL)
						{
							free(pszSBTData);
							pszSBTData = NULL;
						}

						pszSBTData = (char *)malloc(sizeof(char) * (iLen + 1));
						if(pszSBTData == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Failed to malloc for STB data!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							break; // Daivik:4/2/2016-This break statement is part of Coverity Fix for Defect 67322.
						}

						memset(pszSBTData, 0x00, (sizeof(char) * (iLen + 1)));

						memcpy(pszSBTData, pstUITran->stUIData.stEmvDtls.szSpinBinData, iLen);

						iStatus = SEMI_COMPLETE;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: STB Data is received, Card data also received", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						debug_sprintf(szDbgMsg, "%s: setting bCardDataReceived & bSTBDataReceived as PAAS_FALSE", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						bSTBDataReceived = PAAS_FALSE;
						bCardDataReceived = PAAS_FALSE;

						/*
						 * Praveen_P1: when both have received we need to check if we have card details or EMV details with us
						 */

						if(strlen(stCardDtls.szTrackData) > 0)
						{
							//Need to parse the STB data and store it

							rv = parseNSaveSTBInfo(&stCardDtls, pstUITran->stUIData.stEmvDtls.szSpinBinData);
							if(rv != SUCCESS)
							{
								debug_sprintf(szDbgMsg, "%s: Error while getting STB details for given card", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}

							rv = determineCardType(&stCardDtls, "0.00");
							if(rv != SUCCESS)
							{
								debug_sprintf(szDbgMsg, "%s: Error while determining card type based on STB details for given card", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}

							storePreSwipeCardDtls(&stCardDtls, PAAS_TRUE);

							if(isLineItemDisplayEnabled())
							{
								//clearScreen(1);//need to check if we have to do clear screen
								//MukeshS3: 8-July-16: FRD 3.95 QRCODE
								if(getAppState() == IN_LINEITEM_QRCODE)
								{
									showQRCodeAtLIScrn();
								}
								else
								{
									/* Enable PreSwipe if Required*/
									if(getXPILibMode())
									{
										// 10-Dec-15: MukeshS3: Need to intialize the FORM & set all the properties again to show the FORM.
										// when XPI runs as library, because XPI would have painted his FORM at last.
										showLineItemScreen();
									}
									else
									{
										showLIForm(PAAS_TRUE, PAAS_FALSE);
									}
								}
							}
							else
							{
								rv = showWelcomeScreen(PAAS_FALSE);
							}

							memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

							break;
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Should not come here, track data is not available but bCardDataReceived is set", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
					}
				}
				else if(pstUITran->stUIData.stEmvDtls.stEMVCardStatusU02.bEMVRetry == PAAS_TRUE)
				{
					/* Not Required to Enable PreSwipe, This is a Retry Scenario from EMV Response, so XPI will Reenable the CardCapture(C30/S20),
					 * SO Not Required to send CardCapture(C30/S20) from our end.
					 * Fix Done After it is noticed that we send multiple C30 Commnad to XPI without C31 Response.*/
					if(isLineItemDisplayEnabled())
					{
						//MukeshS3: 8-July-16: FRD 3.95 QRCODE
						if(getAppState() == IN_LINEITEM_QRCODE)
						{
							showQRCodeAtLIScrn();
						}
						else
						{
							// 10-Dec-15: MukeshS3: Need to intialize the FORM & set all the properties again to show the FORM.
							// when XPI runs as library, because XPI would have painted his FORM at last.
							if(getXPILibMode())
							{
								showLineItemScreen();
							}
							else
							{
								showLIForm(PAAS_FALSE, PAAS_FALSE);
							}
						}
					}
					else
					{
						showWelcomeScreen(PAAS_FALSE);
					}
					break;
				}
				else if(pstUITran->stUIData.stEmvDtls.stEMVCardStatusU02.bEMVRetry != PAAS_TRUE)
				{
					debug_sprintf(szDbgMsg, "%s: No Action Required for this Notification", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iStatus = SEMI_COMPLETE; //UIBusy flag should not be reset since these notifications between C30 and C31
					break;
				}
				else
				{
					//Check if we need to do anything
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: EMV res cmd: %s", __FUNCTION__, pstUITran->stUIData.stEmvDtls.szEMVRespCmd);
				APP_TRACE(szDbgMsg);

				rv = getUIRespCodeOfEmvCmdResp(EMV_C30, atoi(pstUITran->stUIData.stEmvDtls.szEMVRespCode));
				if( rv != SUCCESS)
				{
					//If  terminal in EMV fallback Mode set the Global variable to True
					if(rv == EMV_FALLBACK_TO_MSR || (rv == EMV_ERROR_CANDIDATE_LISTEMPTY && isEmptyCandidateCardAsFallBack()))
					{
						setEMVFallbackMode(PAAS_TRUE);
					}
					/*
					 * TODO: We need to determine which all places we need not show the card removed message
					 * one such place if card already removed
					 */
					if(rv != EMV_ERROR_CARD_REMOVED && rv != EMV_ERROR_CTLS &&
						rv != EMV_CTLS_FALLBACK_TO_CT)
					{
						checkCrdPrsnceAndShwMsgToRemove();
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: EMV Data is present Saving EMV DATA in Session resp cmd: %s", __FUNCTION__, pstUITran->stUIData.stEmvDtls.szEMVRespCmd);
					APP_TRACE(szDbgMsg);
					
					/* If we received EMV fallback then we set this flag to true, but if user insert any
					 * card after that, terminal will move back to non-fallback mode, so we are
					 * resetting this flag, if we got EMV response other than U02
					 */
					setEMVFallbackMode(PAAS_FALSE);
					
					debug_sprintf(szDbgMsg, "%s: Card Inserted on Pre-Swipe/Insert Screen", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "EMV Card Inserted on Pre-Swipe/Insert Screen");
						addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
					}

					pstEmvDtls = &(pstUITran->stUIData.stEmvDtls);

					//If VAS dtls has been received in EMV dtls struct, that means VAS and EMV both are present
					if(isWalletEnabled() && pstEmvDtls->stVasDataDtls.bVASPresent == PAAS_TRUE)
					{
						debug_sprintf(szDbgMsg, "%s: Apple Pay VAS Data Present", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

						stCardDtls.pstVasDataDtls = (VASDATA_DTLS_PTYPE) malloc(sizeof(VASDATA_DTLS_STYPE));
						if(stCardDtls.pstVasDataDtls != NULL)
						{
							memset(stCardDtls.pstVasDataDtls, 0x00, sizeof(VASDATA_DTLS_STYPE));
							memcpy(stCardDtls.pstVasDataDtls, &pstEmvDtls->stVasDataDtls, sizeof(VASDATA_DTLS_STYPE));
							rv = storePreSwipeCardDtls(&stCardDtls, PAAS_FALSE);
							if(rv != SUCCESS)
							{
								debug_sprintf(szDbgMsg, "%s: Fail to Store and Send VAS Data", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}
							free(stCardDtls.pstVasDataDtls)
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: VAS Memory Allcoation Failed", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
					}

					//AjayS2: TODO Need to add code to change the Apple icons to Loyalty Data Received in Line/Welcome showIdleScreen();

					memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
					storeEmvCardData(pstEmvDtls,&stCardDtls);

					//Default Lang saved in session
					iEMVSelLan = stCardDtls.EMVlangSelctd -1;
					if(iEMVSelLan >= 0 && iEMVSelLan < MAX_SUPPORT_LANG)
					{
						setLangId(iEMVSelLan);
					}

					if(isSTBLogicEnabled())
					{
						if(bSTBDataReceived == PAAS_FALSE) //We have not received
						{
							debug_sprintf(szDbgMsg, "%s: We have not received STBData still, setting UITran status to be semi complete", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							iStatus = SEMI_COMPLETE;

							debug_sprintf(szDbgMsg, "%s: setting bCardDataReceived as PAAS_TRUE", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							bCardDataReceived = PAAS_TRUE;

							/*
							 * If STB Data has not come, we cant process the card data here so breaking from here
							 */

							break;
						}
						else
						{
							//Need to parse the STB data and store it

							debug_sprintf(szDbgMsg, "%s: Card Data is received, STB data also received", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							debug_sprintf(szDbgMsg, "%s: setting bCardDataReceived & bSTBDataReceived as PAAS_FALSE", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							bSTBDataReceived = PAAS_FALSE;
							bCardDataReceived = PAAS_FALSE;

							rv = parseNSaveSTBInfo(&stCardDtls, pszSBTData);
							if(rv != SUCCESS)
							{
								debug_sprintf(szDbgMsg, "%s: Error while getting STB details for given card", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}

							rv = determineCardType(&stCardDtls, "0.00");
							if(rv != SUCCESS)
							{
								debug_sprintf(szDbgMsg, "%s: Error while determining card type based on STB details for given card", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}

							if(pszSBTData != NULL)
							{
								free(pszSBTData);
								pszSBTData = NULL;
							}
						}
					}

					storePreSwipeCardDtls(&stCardDtls, PAAS_TRUE);

					memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
				}

				if(isLineItemDisplayEnabled())
				{
					//clearScreen(1);
					//rv = showLineItemScreen();
					/* Enable PreSwipe in LI if Required*/
					if(getAppState() == IN_LINEITEM_QRCODE)
					{
						showQRCodeAtLIScrn();
					}
					else
					{
						if(getXPILibMode())
						{
							// 10-Dec-15: MukeshS3: Need to intialize the FORM & set all the properties again to show the FORM.
							// when XPI runs as library, because XPI would have painted his FORM at last.
							/* The below condition is required in order to send the buffered LI before sending C30. If we send C30 , there is a chance that the card will be inserted, and
							 * the buffered line items may also fail. So we need to first updated LI screen and then send C30.
							 * RecaptureCard will be true then card readers will not be enabled ( since we got C31 response )
							 * If the below conditions are satisfied then , we would have tried to send buffered LI in blogic thread. So we are using the same set of conditions
							 * to send the buffered LI  here.
							 */
	//						acquireMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");
	//						if(bReCaptureCard && (iStatus == COMPLETE) && (giAllowLITran == 1) && (isSwipeAheadEnabled() == PAAS_TRUE) && (isPreSwipeDone() == PAAS_FALSE))
	//						{
	//
	//							setCardReadEnabled(PAAS_TRUE);
	//							showLineItemScreen();
	//							showUpdatedLIScreen();
	//
	//							giAllowLITran = 0;
	//
	//							releaseMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");
	//							setCardReadEnabled(PAAS_FALSE);
	//							rvtemp = captureCardDtlsForPreswipe();
	//							if(rvtemp != SUCCESS)
	//							{
	//								debug_sprintf(szDbgMsg, "%s: FAILED to send XPI cmds",__FUNCTION__);
	//								APP_TRACE(szDbgMsg);
	//							}
	//						}
	//						else
	//						{
	//							releaseMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");
								//MukeshS3: 8-July-16: FRD 3.95 QRCODE


									showLineItemScreen();
	//						}

						}
						else
						{
							showLIForm(PAAS_TRUE, PAAS_FALSE);
						}
					}

				}
				else
				{
					rv = showWelcomeScreen(PAAS_FALSE);
				}

				break;
			}
		}
		else if(pstUITran->stUIData.iDataType == UI_EMVADMIN_DATA_TYPE)
		{
			debug_sprintf(szDbgMsg, "%s: Thread will be used for EMV_UPDATE", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = procEmvInitialization(EMV_SETUP_READ_AGAIN);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Fail to Process EMV Update", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			showIdleScreen();
			break;
		}
		else if(pstUITran->stUIData.iDataType == UI_SWIPE_DATA_TYPE)
		{
			pstCard = &(pstUITran->stUIData.stCrdTrkInfo);

			if(isWalletEnabled() && pstCard->stVasDataDtls.bVASPresent == PAAS_TRUE)
			{
				debug_sprintf(szDbgMsg, "%s: Apple Pay VAS Data Present", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

				stCardDtls.pstVasDataDtls = (VASDATA_DTLS_PTYPE) malloc(sizeof(VASDATA_DTLS_STYPE));

				if(stCardDtls.pstVasDataDtls != NULL)
				{
					memset(stCardDtls.pstVasDataDtls, 0x00, sizeof(VASDATA_DTLS_STYPE));
					memcpy(stCardDtls.pstVasDataDtls, &pstCard->stVasDataDtls, sizeof(VASDATA_DTLS_STYPE));

					rv = storePreSwipeCardDtls(&stCardDtls, PAAS_FALSE);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Fail to Store and Send VAS Data", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					free(stCardDtls.pstVasDataDtls);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: VAS Memory Allcoation Failed", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

				//AjayS2: TODO Need to add code to change the Apple icons to Loyalty Data Received in Line/Welcome showIdleScreen();
				if( !((strlen(pstCard->szTrk1) > 0) || (strlen(pstCard->szTrk2) > 0) || (strlen(pstCard->szTrk3) > 0)) )
				{
					debug_sprintf(szDbgMsg, "%s: Track Information is NOT present with Apple Pay VAS Data", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					//AjayS2: TODO:This wait is there because XPI is showing Please wait screen after 2-3 seconds, Need to remove it when XPI fixes this.
					//svcWait(3500);

					if(isLineItemDisplayEnabled())
					{
						//MukeshS3: 8-July-16: FRD 3.95 QRCODE
						if(getAppState() == IN_LINEITEM_QRCODE)
						{
							showQRCodeAtLIScrn();
						}
						else
						{
							if(getXPILibMode())
							{
								showLineItemScreen();
							}
							else
							{
								showLIForm(PAAS_TRUE, PAAS_FALSE);
							}
						}
					}
					else
					{
						showWelcomeScreen(PAAS_FALSE);
					}
					break;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Track Information is Also present with Apple Pay VAS Data", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			if(isSTBLogicEnabled())
			{
				if((strlen(pstCard->szTrk1) > 0) || (strlen(pstCard->szTrk2) > 0) || (strlen(pstCard->szTrk3) > 0))
				{
					debug_sprintf(szDbgMsg, "%s: Track Information is present", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else //For the bad card read, we will not get the STB data thats why
				{
					debug_sprintf(szDbgMsg, "%s: No Track Information, could be bad card read", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(isLineItemDisplayEnabled())
					{
						/* Enable PreSwipe if Required, Test This Case*/
						//MukeshS3: 8-July-16: FRD 3.95 QRCODE
						if(getAppState() == IN_LINEITEM_QRCODE)
						{
							showQRCodeAtLIScrn();
						}
						else
						{
							if(getXPILibMode())
							{
								// 10-Dec-15: MukeshS3: Need to intialize the FORM & set all the properties again to show the FORM.
								// when XPI runs as library, because XPI would have painted his FORM at last.
								showLineItemScreen();
							}
							else
							{
								showLIForm(PAAS_TRUE, PAAS_FALSE);
							}
						}

					}
					else
					{
						rv = showWelcomeScreen(PAAS_FALSE);
					}
					break;//Breaking from the loop
				}
			}
			/* Allocating memory for tracks placeholder */
			/* 3 for the track info and the fourth for the eparms*/
			szTracks = (char **) malloc(sizeof(char *) * 4);
			if(szTracks == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(szTracks, 0x00, sizeof(char *) * 4);

			if (eEncType == RSA_ENC)
			{
				szRsaTracks = (char **) malloc(sizeof(char *) * 2);
				if(szRsaTracks == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(szRsaTracks, 0x00, sizeof(char *) * 2);
			}
			else if(eEncType == VSD_ENC)
			{
				// First for encBlob, second for KSN, third for IV & fourth for clear track
				szVsdEncData = (char **) malloc(sizeof(char *) * 4);
				if(szVsdEncData == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				memset(szVsdEncData, 0x00, sizeof(char *) * 4);
			}

			/* Card source */
			iCardSrc = pstCard->iCardSrc;

			/* Track 1 --- */
			iLen = strlen(pstCard->szTrk1);
			if(iLen > 0)
			{
				szTracks[0] = (char *)malloc(iLen + 1);
				memset(szTracks[0], 0x00, iLen + 1);
				memcpy(szTracks[0], pstCard->szTrk1, iLen);
				bTrackDataPresent = PAAS_TRUE;
			}

			/* Track 2 --- */
			iLen = strlen(pstCard->szTrk2);
			if(iLen > 0)
			{
				szTracks[1] = (char *)malloc(iLen + 1);
				memset(szTracks[1], 0x00, iLen + 1);
				memcpy(szTracks[1], pstCard->szTrk2, iLen);
				bTrackDataPresent = PAAS_TRUE;
			}

			/* Track 3 --- */
			iLen = strlen(pstCard->szTrk3);
			if(iLen > 0)
			{
				szTracks[2] = (char *)malloc(iLen + 1);
				memset(szTracks[2], 0x00, iLen + 1);
				memcpy(szTracks[2], pstCard->szTrk3, iLen);
				bTrackDataPresent = PAAS_TRUE;
			}

			if(bTrackDataPresent == PAAS_FALSE)
			{
				debug_sprintf(szDbgMsg, "%s: No Track Information, could be bad card read", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(isLineItemDisplayEnabled())
				{
					//clearScreen(1);
					//rv = showLineItemScreen();
					/* Enable PreSwipe if Required*/
					//MukeshS3: 8-July-16: FRD 3.95 QRCODE
					if(getAppState() == IN_LINEITEM_QRCODE)
					{
						showQRCodeAtLIScrn();
					}
					else
					{
						if(getXPILibMode())
						{
							// 10-Dec-15: MukeshS3: Need to intialize the FORM & set all the properties again to show the FORM.
							// when XPI runs as library, because XPI would have painted his FORM at last.
							showLineItemScreen();
						}
						else
						{
							showLIForm(PAAS_TRUE, PAAS_FALSE);
						}
					}
				}
				else
				{
					rv = showWelcomeScreen(PAAS_FALSE);
				}

				break;//Breaking from the loop
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Track Information is present!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			/* Eparms Data  --- */
			iLen = strlen(pstCard->szEparms);
			if(iLen > 0)
			{
				szTracks[3] = (char *)malloc(iLen + 1);
				memset(szTracks[3], 0x00, iLen + 1);
				memcpy(szTracks[3], pstCard->szEparms, iLen);
			}

			/* Copy clear expiry date*/
			iLen = strlen(pstCard->szClrExpDt);
			if(iLen > 0)
			{
				memset(szClrDt, 0x00, iLen + 1);
				memcpy(szClrDt, pstCard->szClrExpDt, iLen);

				debug_sprintf(szDbgMsg, "%s: Copying Clear Expiry date",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: No Clear exp date to copy",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			/*Copy the RSA Track 1 */
			iLen = strlen(pstCard->szRsaTrk1);
			if(iLen > 0 && (szRsaTracks != NULL)) // CID 67365 (#1 of 2): Explicit null dereferenced (FORWARD_NULL) [select issue] T_RaghavendranR1
			{
				szRsaTracks[0] = (char *)malloc(iLen + 1);
				memset(szRsaTracks[0], 0x00, iLen + 1);
				memcpy(szRsaTracks[0], pstCard->szRsaTrk1, iLen);
			}

			/*Copy the RSA Track 2 */
			iLen = strlen(pstCard->szRsaTrk2);
			if(iLen > 0 &&  (szRsaTracks != NULL)) // CID 67365 (#2 of 2): Explicit null dereferenced (FORWARD_NULL) T_RaghavendranR1
			{
				szRsaTracks[1] = (char *)malloc(iLen + 1);
				memset(szRsaTracks[1], 0x00, iLen + 1);
				memcpy(szRsaTracks[1], pstCard->szRsaTrk2, iLen);
			}

			if(szVsdEncData != NULL)
			{
				/* Copy VSD encrypted  data*/
				iLen = strlen(pstCard->szVsdEncBlob);
				if(iLen > 0)
				{
					szVsdEncData[0] = (char *)malloc(iLen + 1);
					memset(szVsdEncData[0], 0x00, iLen + 1);
					memcpy(szVsdEncData[0], pstCard->szVsdEncBlob, iLen);
				}

				/* Copy KSN associated with VSD encrypted  data*/
				iLen = strlen(pstCard->szVsdKSN);
				if(iLen > 0)
				{
					szVsdEncData[1] = (char *)malloc(iLen + 1);
					memset(szVsdEncData[1], 0x00, iLen + 1);
					memcpy(szVsdEncData[1], pstCard->szVsdKSN, iLen);
				}

				/* Copy IV associated with VSD encrypted  data*/
				iLen = strlen(pstCard->szVsdIV);
				if(iLen > 0)
				{
					szVsdEncData[2] = (char *)malloc(iLen + 1);
					memset(szVsdEncData[2], 0x00, iLen + 1);
					memcpy(szVsdEncData[2], pstCard->szVsdIV, iLen);
				}

				/* Copy Clear Track data if present*/
				if(strlen(pstCard->szClrTrk2))
				{
					iLen = strlen(pstCard->szClrTrk2);
					szVsdEncData[3] = (char *)malloc(iLen + 1);
					memset(szVsdEncData[3], 0x00, iLen + 1);
					memcpy(szVsdEncData[3], pstCard->szClrTrk2, iLen);
				}
				else if(strlen(pstCard->szClrTrk1))
				{
					iLen = strlen(pstCard->szClrTrk1);
					szVsdEncData[3] = (char *)malloc(iLen + 1);
					memset(szVsdEncData[3], 0x00, iLen + 1);
					memcpy(szVsdEncData[3], pstCard->szClrTrk1, iLen);
				}
			}

			memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

			/*Copy the Service Code Byte One */
			iLen = strlen(pstCard->szServiceCodeByteOne);
			if(iLen > 0)
			{
				memcpy(stCardDtls.szServiceCodeByteOne, pstCard->szServiceCodeByteOne, iLen);
			}

			if(strlen(szClrDt) > 0)
			{
				/* Extract the month and year */
				memcpy(stCardDtls.szClrYear, szClrDt, 2);
				memcpy(stCardDtls.szClrMon, szClrDt + 2, 2);
			}
			/*Store EMV Fallback parameter*/
			stCardDtls.bEmvFallback = isInEMVFallbackMode();
			debug_sprintf(szDbgMsg, "%s: EMV FallBack Flag [%d]", __FUNCTION__, stCardDtls.bEmvFallback);
			APP_TRACE(szDbgMsg);

			setEMVFallbackMode(PAAS_FALSE);

			/* Store the Card source */
			stCardDtls.iCardSrc = iCardSrc;

			/* Daivik:8/7/2016 - If this is a Kohls Card we will need to validate the card data and also
			 * send a token query to the host. In order to make that determination we need to extract the PAN
			 * from the Track Data.
			 */

			if(getPANFromTrack(szTracks,szTrkPAN,sizeof(szTrkPAN) - 1) == SUCCESS)
			{
				if(strlen(szTrkPAN))
				{
					strncpy(stCardDtls.szPAN,szTrkPAN,sizeof(stCardDtls.szPAN) - 1);
				}
			}
			/*
			 * Praveen_P1: During pre-swipe we need to check
			 * if it is Kohls card to do the token query
			 * so CDT look up we are doing if only private label is enabled
			 * otherwise avoiding this step(CDT look up)
			 */
			//Daivik:28/9 We need to check for Kohls card only in CDT case
			if(isPrivLabelTenderEnabled() && !isSTBLogicEnabled())
			{
				/* Saving the card type as private card type*/
				stCardDtls.iCardType = PL_CARD_TYPE;

				/* Daivik:6/7/2016 - We need to perform validation only if this is a Kohls Card.
				 * For other cards , the validation is done at a later stage after tender selection.
				 */
				if(isKohlsChargeCard(&stCardDtls) == PAAS_TRUE)
				{
					bValidate = PAAS_TRUE;
				}
			}
			else
			{
				bValidate = PAAS_FALSE;
			}

			/* Daivik:6/7/2016 - We will be parsing track data irrespective of whether it is Kohls card or not.
			 * If it is Kohls Card , then validation could fail , which needs to be caught and required action to be taken.
			 * In STB case we will have card type, so we expect track/card validation should pass. If not , then we can throw the error.
			 * In CDT cases we will need the payment type to decide whether track validation is required or not. So we will do these
			 * validations once we have payment type. And currently we ignore the error if present due to track validation
			 */

			/* parse the card information */
			rv = getCardDtls(&stCardDtls, szTracks, szRsaTracks, szVsdEncData, "0.00", bValidate, pszSBTData /* Need to send STB data here*/);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting card deatails, rv[%d]", __FUNCTION__, rv);
				APP_TRACE(szDbgMsg);

				//bValidate will be true only for Kohls Card. So if the validation fails for Kohls Card , then we will throw an error in this case also
				//For STB case we anyway will have card type and hence we dont expect a bad card error.
				if(isSTBLogicEnabled() || bValidate)
				{
				if(rv == ERR_CARD_INVALID || rv == ERR_BAD_CARD || strlen(stCardDtls.szTrackData) == 0)
					{
						showPSGenDispForm(MSG_CARD_INVALID, FAILURE_ICON, iStatusMsgDispInterval);

						if(isLineItemDisplayEnabled())
						{
							//MukeshS3: 8-July-16: FRD 3.95 QRCODE
							if(getAppState() == IN_LINEITEM_QRCODE)
							{
								showQRCodeAtLIScrn();
							}
							else
							{
								showLineItemScreen();
							}
						}
						else
						{
							showWelcomeScreen(PAAS_FALSE);
						}
						break;
					}
				}
				else
				{
					//MukeshS3: 26-8-16: CID 83351 (#1 of 1): Logically dead code (DEADCODE)
/*					if(szTracks == NULL)
					{
						bInvalidCard = PAAS_TRUE;
					}
					else
					{
						if((szTracks[0] == NULL) && (szTracks[1] == NULL))
						{
							bInvalidCard = PAAS_TRUE;
						}
					}
					///If both tracks are not present , then we will not store backup details. we will follow the same flow as what it is happening today.
					if(bInvalidCard) 
*/
					if((szTracks[0] == NULL) && (szTracks[1] == NULL))
					{
						rv = ERR_CARD_INVALID;
						showPSGenDispForm(MSG_CARD_INVALID, FAILURE_ICON, iStatusMsgDispInterval);
						if(isLineItemDisplayEnabled())
						{
							showLineItemScreen();
						}
						else
						{
							showWelcomeScreen(PAAS_FALSE);
						}
//						bInvalidCard = PAAS_FALSE;
						break;
					}

					/* Daivik:6/7/2016 - The Card details need to be backed up in this case. For VSD Encryption we will need the clear data which we recieved
					 * in order to send it up in the unsolicited message to POS. So we will save these values in Card Details structure so that the unsolicited message is sent up
					 * in storePreSwipeCardDtls function.
					 */

					stBkupCrdDtls.pszRSATrackDtlsBkup = szRsaTracks;
					stBkupCrdDtls.pszTrackDtlsBkup = szTracks;
					stBkupCrdDtls.pszVSDBkup = szVsdEncData;
					/* Daivik:8/7/2016 - In Case of VSD Encryption , we will be getting the clear data which we need to store for Notification to POS.
					 * This is normally done at the end of getCardDtls function, but for non ISO/Gift Cards ,it is possible that we fail during track validation
					 * and we would not have stored the clear track information required for Unsolicitated message to POS. Doing that here.
					 */
					if(szVsdEncData != NULL && szVsdEncData[3] != NULL)
					{
						strcpy(stCardDtls.szClrTrkData, szVsdEncData[3]);
						// parse the clear PAN from clear track data if present.
						cCurPtr = szVsdEncData[3];
						if((cNxtPtr = strchr(cCurPtr, '=')) != NULL)
						{
							//If It was Track 2 Data
							strncpy(stCardDtls.szClrPAN, cCurPtr, cNxtPtr - cCurPtr);
						}
						else if((cNxtPtr = strchr(cCurPtr, '^')) != NULL)
						{
							cCurPtr++;
							strncpy(stCardDtls.szClrPAN, cCurPtr, cNxtPtr - cCurPtr);
						}
						else
						{

							/* Daivik:21/7/2016 - In case of some gift cards which will not have the field seperators (non ISO format).
							 * We need to consider the complete track as the PAN. Also we need to set the Track Indicator which will
							 * be used in getRawCardDtls function to populate either ClearTrack1 or ClearTrack2 Data.
							 */

							if(strlen(cCurPtr) > (sizeof(stCardDtls.szClrPAN) - 1))
							{
								strncpy(stCardDtls.szClrPAN, cCurPtr, sizeof(stCardDtls.szClrPAN) - 1);
							}
							else
							{
								strcpy(stCardDtls.szClrPAN, cCurPtr);
							}
							if(szTracks[1] != NULL)
							{
								stCardDtls.iTrkNo = TRACK2_INDICATOR;
							}
							else if(szTracks[0] != NULL)
							{
								stCardDtls.iTrkNo = TRACK1_INDICATOR;
							}
						}

					}
					if(storeBkupDtlsinSession(&stBkupCrdDtls) != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failure to Store the backup Card details", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
					///Set rv to SUCCESS , incase the rv after getCardDtls was something else.
					rv = SUCCESS;
				}
			}

			//set the encryption type here.
			stCardDtls.iEncType = eEncType;

			if( isSTBLogicEnabled() && bSTBDataReceived == PAAS_FALSE) //We have not received
			{
				debug_sprintf(szDbgMsg, "%s: We have not received STBData still, setting UITran status to be semi complete", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				iStatus = SEMI_COMPLETE;

				/*
				 * If STB Data has not come, we cant process the card data here so breaking from here
				 */

				debug_sprintf(szDbgMsg, "%s: setting bCardDataReceived as PAAS_TRUE", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				bCardDataReceived = PAAS_TRUE;

				break;
			}

			/*
			 * The following function is called when Pre-Swipe is done and Priv label is selected
			 * from the consumer option.
			 * The second parameter conveys that pre-swipe is done
			 *
			 */
			iLastAppState = getAppState();
			rv = storePreSwipeCardDtls(&stCardDtls, PAAS_TRUE);
			if(rv != SUCCESS)
			{
				if(rv == CARD_TOKEN_NOT_PRESENT)
				{
					debug_sprintf(szDbgMsg, "%s: Could not get the CARD_TOKEN for this card!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					//We will enable pre-Swipe again
					/*
					 * We need to show message to the customer that card token not found
					 *
					 */

					showPSGenDispForm(MSG_CARDTOK_NOTFOUND, FAILURE_ICON, iStatusMsgDispInterval);

					if(isLineItemDisplayEnabled())
					{
						//MukeshS3: 8-July-16: FRD 3.95 QRCODE
						if(iLastAppState == IN_LINEITEM_QRCODE)
						{
							showQRCodeAtLIScrn();
						}
						else
						{
							showLineItemScreen();
						}
					}
					else
					{
						showWelcomeScreen(PAAS_FALSE);
					}
					memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
					break;

				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Failed to store/send pre-swipe/tap card details", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			//Mukesh: Setting back the previous app state if it was changed in storePreSwipeCardDtls function
			if(iLastAppState == IN_LINEITEM_QRCODE)
			{
				setAppState(iLastAppState);
			}

			/* Showing line item screen
			 * After the card swipe we need to remove the text label on the screen
			 * so need to draw the line item screen
			 * */
			//TODO Instead of drawing full screen we can set only that label
			if(isLineItemDisplayEnabled())
			{
				//showLineItemScreen();
				/* Enable PreSwipe if Required*/
				//MukeshS3: 8-July-16: FRD 3.95 QRCODE: No need to update the screen for this case
				if(iLastAppState != IN_LINEITEM_QRCODE)
				{
					showLIForm(PAAS_TRUE, PAAS_FALSE);
				}
			}
			else
			{
				showWelcomeScreen(PAAS_FALSE);
			}

			memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

			break;
		}

		break;
	}

	if((iStatus == COMPLETE) && (giAllowLITran || getRetryLITran()))
	{
		//The below variable will be set to 0 in showupdatedLIScreen
		//giAllowLITran = 0;


		if(isLineItemDisplayEnabled()) //We need to show line item screen only line item display is enabled
		{
			debug_sprintf(szDbgMsg, "%s: AllowLITran is set so showing lineitem screen", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/*
			 * Need to update line item screen first before updating with the new ones
			 * passing 0 as the parameter so that it does show up line item screen
			 */
			initUpdateForLineItemScreen(0, &stUIReq);

			/* Showing line item screen */
			showUpdatedLIScreen();
		}
	}

	/*
	 * Praveen_P1: Moved down because we are breaking from the loop in some places
	 */
	//Daivik:8/7/16 - We avoid freeing the track data incase we have stored the pointer in backup details. It will be freed after we use the backup details.
	if(szTracks != NULL && (stBkupCrdDtls.pszTrackDtlsBkup == NULL))
	{
		for(iCnt = 0; iCnt < 4; iCnt++)
		{
			if(szTracks[iCnt] != NULL)
			{
				free(szTracks[iCnt]);
			}
		}

		free(szTracks);
	}

	if(szRsaTracks != NULL && (stBkupCrdDtls.pszRSATrackDtlsBkup == NULL))
	{
		for(iCnt = 0; iCnt < 2; iCnt++)
		{
			if(szRsaTracks[iCnt] != NULL)
			{
				free(szRsaTracks[iCnt]);
			}
		}

		free(szRsaTracks);
	}

	if(szVsdEncData != NULL && (stBkupCrdDtls.pszVSDBkup == NULL))
	{
		for(iCnt = 0; iCnt < 4; iCnt++)
		{
			if(szVsdEncData[iCnt] != NULL)
			{
				free(szVsdEncData[iCnt]);
			}
		}

		free(szVsdEncData);
	}

	debug_sprintf(szDbgMsg, "%s: Setting UITran Status as %d ", __FUNCTION__, iStatus);
	APP_TRACE(szDbgMsg);

	pstUITran->iStatus = iStatus;

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: doPaypalPaymentCapture
 *
 * Description	: This API does the action based on option selected by Consumer
 * 					application.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static void * doPaypalPaymentCapture(void *arg)
{
	int					rv						= SUCCESS;
	int					iCnt					= 0;
	int					iAppLogEnabled			= isAppLogEnabled();
	int					iCardSrc				= 0;
	int					iControlId				= -1;
	int					iStatusMsgDispInterval  = DFLT_STATUS_DISP_TIME;
	char 				szPymtCode[15]			= "";
	char **				szTracks				= NULL;
	char **				szRSATracks				= NULL;
	char **				szVsdEncData			= NULL;
	char *				szSBTData				= NULL;
	char				szClrExpDate[15]		= "";
	char				szAppLogData[300]		= "";
	char				szAppLogDiag[300]		= "";
	ENC_TYPE 			eEncType				= 0;
	BTRAN_PTYPE			pstUIOpt				= NULL;
	CARDDTLS_STYPE		stCardDtls;
	UNSOLMSGINFO_STYPE	stUnsolMsgInfo;

#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstUIOpt = (BTRAN_PTYPE) arg;

	iControlId = pstUIOpt->stUIData.iCtrlId;

	debug_sprintf(szDbgMsg, "%s: Button press Control ID [%d]", __FUNCTION__, iControlId);
	APP_TRACE(szDbgMsg);

	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();

	//Get encryption type enabled in the device
	eEncType = getEncryptionType();

	while(1)
	{
		szTracks = (char **) malloc(sizeof(char *) * 4);
		if(szTracks == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(szTracks, 0x00, sizeof(char *) * 4);

		szRSATracks = (char **) malloc(sizeof(char *) * 2);
		if(szRSATracks == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(szRSATracks, 0x00, sizeof(char *) * 2);

		if(getEncryptionType() == VSD_ENC)
		{
			szVsdEncData = (char **) malloc(sizeof(char *) * 4);
			if(szVsdEncData == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(szVsdEncData, 0x00, sizeof(char *) * 4);
		}

		memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));


		rv = getPaypalPaymentCodeDtlsFrmUser(szPymtCode, szTracks, szRSATracks, szVsdEncData, szClrExpDate, &iCardSrc, &szSBTData);
		if(rv != SUCCESS)
		{
			if(rv == UI_CANCEL_PRESSED)
			{
				debug_sprintf(szDbgMsg, "%s: Cancelled Pressed From Paypal Payment Code Entry", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "User Pressed Cancel Button on Paypal Payment Code Entry Screen");
					addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to Capture Paypal Payment Code", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}

		if(strlen(szPymtCode) <= 0)
		{
			/* Need to get the PAN from the track data */
			memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

			/* Saving the card type as Credit card type*/
			stCardDtls.iCardType = CREDIT_CARD_TYPE;
			stCardDtls.iCardSrc = iCardSrc;

			//Copy clear expiry date if present.
			if( strlen(szClrExpDate) > 0)
			{
				//Copy clear expiry dates
				memcpy(stCardDtls.szClrYear, szClrExpDate, 2);
				memcpy(stCardDtls.szClrMon, &szClrExpDate[2], 2);
			}

			rv = getCardDtls(&stCardDtls, szTracks, szRSATracks, szVsdEncData,"", PAAS_TRUE, szSBTData);

			if(rv != SUCCESS)
			{
				if(rv == ERR_NOT_IN_BINRANGE)
				{
					showPSGenDispForm(MSG_CARD_NOTACCEPTED, FAILURE_ICON, iStatusMsgDispInterval);
				}
				else if(rv == ERR_CARD_INVALID)
				{
					showPSGenDispForm(MSG_CARD_INVALID, FAILURE_ICON, iStatusMsgDispInterval);
				}
				else if(rv == ERR_MANUAL_NOTALLWD)
				{
					showPSGenDispForm(MSG_MANENTRY_NTALLWD, FAILURE_ICON, iStatusMsgDispInterval);
				}
				else if(rv == ERR_BAD_CARD)
				{
					showPSGenDispForm(MSG_CARD_INVALID, FAILURE_ICON, iStatusMsgDispInterval);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to Get and Validate PayPal card dtls", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Failed to Get Paypal Card Details");
						addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
					}

					break;
				}
				break;
			}
			else
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "PayPal Card Data Validated Successfully");
					addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
				}

				//set the encryption type here.
				stCardDtls.iEncType = eEncType;
				rv = storePreSwipeCardDtls(&stCardDtls, PAAS_TRUE);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to store/send pre-swipe/tap card details", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		else
		{
			/* Update the Payment Code in Session to Refer Later on when Sale/Capture Request is Received*/

#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s: Storing Payment Code in the Session [%s]", __FUNCTION__, szPymtCode);
			APP_TRACE(szDbgMsg);
#endif

			/* Send the PAY With Paypal Button Selected Unsolicited Message back to POS, after capturing the Payment Code
			 * This message will be sent if or if not consumer options are enabled*/

			rv = storePaypalPymtCodeDtlsInSession(szPymtCode);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Store Payment Code Details In Session", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			memset(&stUnsolMsgInfo, 0x00, sizeof(UNSOLMSGINFO_STYPE));
			stUnsolMsgInfo.iPayWithPaypal = 1;
			rv = storeandSendConsOptInfo(&stUnsolMsgInfo, 0, szAppLogDiag);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to store/send Consumer Option Info", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Send [PAYWITHPAYPAL] Consumer Option Notification to POS.");
					addAppEventLog(SCA, PAAS_INFO, UNSOLICITED_MESSAGE, szAppLogData, szAppLogDiag);
				}
			}
			else
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Sent [PAYWITHPAYPAL] Consumer Option Notification");
					addAppEventLog(SCA, PAAS_SUCCESS, UNSOLICITED_MESSAGE, szAppLogData, NULL);
				}
			}
		}

		break;
	}

	if(szTracks != NULL)
	{
		for(iCnt = 0; iCnt < 4; iCnt++)
		{
			if(szTracks[iCnt] != NULL)
			{
				free(szTracks[iCnt]);
			}
		}

		free(szTracks);
	}

	if(szRSATracks != NULL)
	{
		for(iCnt = 0; iCnt < 2; iCnt++)
		{
			if(szRSATracks[iCnt] != NULL)
			{
				free(szRSATracks[iCnt]);
			}
		}

		free(szRSATracks);
	}

	if(szVsdEncData != NULL)
	{
		for(iCnt = 0; iCnt < 4; iCnt++)
		{
			if(szVsdEncData[iCnt] != NULL)
			{
				free(szVsdEncData[iCnt]);
			}
		}

		free(szVsdEncData);
	}

	//TO_DO Instead of drawing full screen we can set only that label
	if(isLineItemDisplayEnabled())
	{
		clearScreen(1);
		showLineItemScreen();
	}
	else
	{
		showWelcomeScreen(PAAS_FALSE);
	}

	pstUIOpt->iStatus = COMPLETE;

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: doConsumerOption
 *
 * Description	: This API does the action based on option selected by Consumer
 * 					application.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static void * doConsumerOptCature(void *arg)
{
	int					rv						= SUCCESS;
	int					iCnt					= 0;
	int					iOptionSelected 		= -1;
	int					iStartBtn				= -1;
	int					iIndex					= -1;
	int					iStatusMsgDispInterval  = DFLT_STATUS_DISP_TIME;
	int					iControlId				= -1;
	int					iDupSwipeFlushed		= PAAS_FALSE;
	int					iAppLogEnabled			= isAppLogEnabled();
	int					iEmvCrdPresence			= 0;
	int					consOptionLst[MAX_CONSUMER_OPTS][2];
	char **				szTracks				= NULL;
	char **				szRsaTracks				= NULL;
	char **				szVsdEncData			= NULL;
	char *				szSBTData				= NULL;
	char				szClrDt[10]				= "";
	char				szAppLogData[300]		= "";
	char				szAppLogDiag[300]		= "";
	char				szPymtCode[15]			= "";
	BTRAN_PTYPE			pstUIConsOpt			= NULL;
	UNSOLMSGINFO_STYPE	stUnsolMsgInfo;
	CARDDTLS_STYPE		stCardDtls;
	ENC_TYPE 			eEncType		= 0;
	PAAS_BOOL			bEmvEnabled		= PAAS_FALSE;
	PAAS_BOOL			bRetainPreTapVasData	= PAAS_FALSE;
	PAAS_BOOL			bSendVasClearFlag		= PAAS_FALSE;
	PAAS_BOOL			bCardRemoveForm 		= PAAS_FALSE;
	UIREQ_STYPE			stUIReq;
	UI_FORM_ENUM		iCurForm;
	APPLN_STATE			iAppState				= -1;

#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();

	//Get encryption type enabled in the device
	eEncType = getEncryptionType();

	//Get EMV status
	bEmvEnabled = isEmvEnabledInDevice();

	pstUIConsOpt = (BTRAN_PTYPE) arg;

	iControlId = pstUIConsOpt->stUIData.iCtrlId;

	debug_sprintf(szDbgMsg, "%s: Button press Control ID [%d]", __FUNCTION__, iControlId);
	APP_TRACE(szDbgMsg);

	if(iControlId == 23)
	{
		debug_sprintf(szDbgMsg, "%s: Cancel button pressed on pre-swipe/welcome screen", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		/*
		 * If Pre-swipe is done, flush the card data
		 * and enable the pre-swipe
		 * If Pre-Swipe is not done, dont do anything
		 */

		/* Daivik - 4/feb/2016 Coverity - 67369 - Check for return value */
		rv = getPaypalPymtCodeDtlsInSession(szPymtCode);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while getting paypal code details!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(isDupSwipeDetectEnabled())
		{
			flushDupSwipeCardDetails(&iDupSwipeFlushed);
			if(iDupSwipeFlushed)
			{
				debug_sprintf(szDbgMsg, "%s: Flushing the duplicate swipe card details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "User Pressed Cancel on Dup Swipe detect Screen. Flushing the duplicate card Details");
					addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
				}
				//MukeshS3: 19-July-16: If EMV card is removed at QR code screen, than we need to stick to the same screen(QR code)
				// Once user presses DONE button there, it will go back to the idle screen.
				// NOTE: At QR code screen, we dont honor CANCEL button, so, we are sure that we dont even need to check for card presence here
				if(getCurFormNumber() != QRCODE_DISP_FRM)
				{
					checkCrdPrsnceAndShwMsgToRemove();
					//clearScreen(1);	//T_MukeshS3
					showPSGenDispForm(MSG_CANCELLED, FAILURE_ICON, iStatusMsgDispInterval);
					//MukeshS3: 18-March-16: There is no meaning of going to idle screen after cancelling the transaction.
					// So, we will be staying on the Transaction Cancelled screen.
					//MukeshS3: 19-July-16: Reverting this change back to the earlier behavior in this 2.19.26 B1 build.
					// As in the MCD(2.19.21) & KOHLS(2.19.23) builds, we are going back to IDLE screen after showing transaction cancelled screen
					showIdleScreen();
					setAppState(TRANSACTION_COMPLETED);
					setDetailedAppState(DETAILED_TRANSACTION_COMPLETED);
				}
				else
				{
					setLastForm(QRCODE_DISP_FRM);
				}
			}
		}

		if((isWalletEnabled() && isOnWelcomeScreenAfterVAS() && isVASDataCaptured()) || (isOnWelcomeScrnAfterEarlyCardCptr()))
		{
			if(isPreSwipeDtlsPresentInSession())
			{
				//MukeshS3: 19-July-16: If EMV card is removed at QR code screen, than we need to stick to the same screen(QR code)
				// Once user presses DONE button there, it will go back to the idle screen.
				// NOTE: At QR code screen, we dont honor CANCEL button, so, we are sure that we dont even need to check for card presence here
				if(getCurFormNumber() != QRCODE_DISP_FRM)
				{
					if((EMVCrdPresenceOnPreswipe() == 1) || (isOnWelcomeScrnAfterEarlyCardCptr())) // added in OR condition to avoid any break.
					{
						checkCrdPrsnceAndShwMsgToRemove();
						/* We are doing showLIForm below, so, doing a clearscreen will result in a blank screen*/
						//clearScreen(1);
					}
					showPSGenDispForm(MSG_CANCELLED, FAILURE_ICON, iStatusMsgDispInterval);
					//MukeshS3: 18-March-16: There is no meaning of going to idle screen after cancelling the transaction.
					// So, we will be staying on the Transaction Cancelled screen.
					//MukeshS3: 19-July-16: Reverting this change back to the earlier behavior in this 2.19.26 B1 build.
					// As in the MCD(2.19.21) & KOHLS(2.19.23) builds, we are going back to IDLE screen after showing transaction cancelled screen
					showIdleScreen();
					setAppState(TRANSACTION_COMPLETED);
					setDetailedAppState(DETAILED_TRANSACTION_COMPLETED);
				}
				else
				{
					setLastForm(QRCODE_DISP_FRM);
				}
				/*Akshaya : Moving FlushPreSwipeCardDetails Function Here .As we are not showing Please Remove Card Screen once Cancel is pressed on Welcome Screen*/
				//We are On welcome screen after sending VAS to POS, and Cancel is Pressed or EMV Card Has been Removed,
				//we will clear all data and show transaction cancelled followed by idle screen if card data is Present.
				flushPreSwipeCardDetails(PAAS_FALSE);
			}
		}

		else
		{
			if(isPreSwipeDone() || (isPaypalTenderEnabled() && (strlen(szPymtCode) > 0)))
			{
				debug_sprintf(szDbgMsg, "%s: Pre-Swipe or PayPal Payment Code Entered is Done, need to flush card details and enable pre-swipe", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				iEmvCrdPresence = EMVCrdPresenceOnPreswipe();

				if(strcasecmp(pstUIConsOpt->stUIData.stEmvDtls.szEMVRespCmd, "R01") == 0)
				{
					iEmvCrdPresence = 0;
					debug_sprintf(szDbgMsg, "%s: EMV Card Removed on LineItem/Welcome Screen: Flushing card dtls", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "EMV Card Removed");
						addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
					}

					if(isWalletEnabled() && isVASDataCaptured())
					{
						bRetainPreTapVasData= PAAS_TRUE;
						debug_sprintf(szDbgMsg, "%s: NOT Flushing VAS dtls", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
				else if(isWalletEnabled() && isVASDataCaptured())
				{
					//Need to send Clear Flag only if cancel Pressed and VAS Present in session, On card removal No need to send
					bSendVasClearFlag = 2;
					flushPreSwipeCardDetails(bRetainPreTapVasData);
				}
				else //KranthK1: Added this condition to flush the card details after showing the card removed form below.
				{
					flushPreSwipeCardDetails(bRetainPreTapVasData);
				}
				/* Akshaya:06-04-2016:Disable card readers has been moved here , because even in case preswipe was using a MSR card or CTLS Tap,
				 * and then if D41 is active , then we need to send disable card readers so that, in showlineitemscreen we will
				 * reinitiate a new card capture ( with both Vas / Payment - depending on the requirement )
				 * This change was required because we have now removed the disable card readers from showlineitemscreen function.
				 */
				debug_sprintf(szDbgMsg, "%s: Calling Disable Card readers since Cancel was pressed, This is preswipe done case", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				disableCardReaders();
				if(iEmvCrdPresence == 1)
				{
					checkCrdPrsnceAndShwMsgToRemove();
					/* We are doing showLIForm below, so, doing a clearscreen will result in a blank screen*/
					//clearScreen(1);
				}

				iOptionSelected = CANCELPRESWIPE_OPT;

				memset(&stUnsolMsgInfo, 0x00, sizeof(UNSOLMSGINFO_STYPE));
				stUnsolMsgInfo.iPreSwiped = 2;
				if(bSendVasClearFlag)
				{
					stUnsolMsgInfo.iVasLoyalty = 2;
				}
				stUnsolMsgInfo.iCancelPressed = PAAS_TRUE;	// MukeshS3: will set the response text accordingly for CANCEL pressed
				rv = storeandSendConsOptInfo(&stUnsolMsgInfo, CANCELPRESWIPE_OPT, szAppLogDiag);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to store/send Consumer Option Info", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Failed to Send [CANCEL PRE SWIPE] Notification to POS.");
						addAppEventLog(SCA, PAAS_INFO, UNSOLICITED_MESSAGE, szAppLogData, szAppLogDiag);
					}
				}
				else
				{
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Successfully Sent [CANCEL PRE SWIPE] Consumer Option Notification");
						addAppEventLog(SCA, PAAS_SUCCESS, UNSOLICITED_MESSAGE, szAppLogData, NULL);
					}
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Pre-Swipe is not done, no need to do anything Clear VAS Data if Present", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(isWalletEnabled() && isVASDataCaptured())
				{
					flushPreSwipeCardDetails(PAAS_FALSE);

					/*Added to Clear Every VAS data When Cancel is Pressed and Re-enabling the Card Readers */
					debug_sprintf(szDbgMsg, "%s: Cancel pressed and Vas Captured , so we need to flush Vas , disable card readers and send new C30/S20 ", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					disableCardReaders();

					iOptionSelected = CANCELPRESWIPE_OPT;

					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Successfully Flushed the Apple Pay Data");
						addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
					}

					memset(&stUnsolMsgInfo, 0x00, sizeof(UNSOLMSGINFO_STYPE));
					stUnsolMsgInfo.iVasLoyalty = 2;
					stUnsolMsgInfo.iCancelPressed = 1;	// MukeshS3: will set the response text accordingly for CANCEL pressed
					rv = storeandSendConsOptInfo(&stUnsolMsgInfo, CANCELPRESWIPE_OPT, szAppLogDiag);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to store/send Consumer Option Info", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "Failed to Send [CANCEL VAS LOYALTY DATA] Notification to POS.");
							addAppEventLog(SCA, PAAS_INFO, UNSOLICITED_MESSAGE, szAppLogData, szAppLogDiag);
						}
					}
					else
					{
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "Successfully Sent [CANCEL VAS LOYALTY DATA] Consumer Option Notification");
							addAppEventLog(SCA, PAAS_SUCCESS, UNSOLICITED_MESSAGE, szAppLogData, NULL);
						}
					}
				}
			}
		}
	}
	else if(iControlId == PAAS_QRCODE_SCREEN_BTN || iControlId == PAAS_LINEITEMSCREEN_QRCODE_BTN)	//MukeshS3: 8-July-16: FRD 3.95 QRCODE: Enter Button Pressed
	{
		debug_sprintf(szDbgMsg, "%s: Enter button pressed on Line item/QR Code screen", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Enter Button Pressed on Line Item/QR Code Screen");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}
		iCurForm = getCurFormNumber();
		if(iCurForm == LI_DISP_FRM || iCurForm == LI_DISP_OPT_FRM)
		{
			// Need to restore the Line item screen
			showLineItemScreen();
		}
		else if(iCurForm == QRCODE_DISP_FRM)
		{
			if(isSessionInProgress() && getLastForm() == WELCOME_DISP_FRM)
			{
				showWelcomeScreen(PAAS_FALSE);
			}
			else if(isSessionInProgress() && getLastForm() == WELCOME_DUP_DISP_FRM)
			{
				showWelcomeScreen(PAAS_TRUE);
			}
			else
			{
				showIdleScreen();
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: SCA has Already Moved to Some Other Screen; Ignoring This UI Event", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "SCA has Already Moved to Some Other Screen; Ignoring This UI Event");
				addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
			}
		}
	}
	else
	{
		/* Get the enabled consumer options */
		getConsOptions(consOptionLst);

		iStartBtn = PAAS_LINEITEMSCREEN1_BTN_1;

		iIndex = iControlId - iStartBtn;

		debug_sprintf(szDbgMsg, "%s: Index [%d]", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		/* set this option selected in the data system */
		rv = setConsOptSelected(iIndex, 1);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting the option selected!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		iOptionSelected = consOptionLst[iIndex][0];

		debug_sprintf(szDbgMsg, "%s: iOptionSelected [%d]", __FUNCTION__, iOptionSelected);
		APP_TRACE(szDbgMsg);

		switch(iOptionSelected)
		{
		case -1:
			debug_sprintf(szDbgMsg, "%s: No Consumer option is selected", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "No Consumer Option Selected");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}
			break;

		case GIFTRECEIPT_OPT:
			debug_sprintf(szDbgMsg, "%s: Gift Receipt Consumer option is selected", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Gift Receipt Consumer Option is Selected");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			memset(&stUnsolMsgInfo, 0x00, sizeof(UNSOLMSGINFO_STYPE));
			stUnsolMsgInfo.iGiftReceipt = 1;
			rv = storeandSendConsOptInfo(&stUnsolMsgInfo, GIFTRECEIPT_OPT, szAppLogDiag);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to store/send Consumer Option Info", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Send [Gift Receipt] Consumer Option Notification to POS.");
					addAppEventLog(SCA, PAAS_INFO, UNSOLICITED_MESSAGE, szAppLogData, szAppLogDiag);
				}
				/* Daivik:27/7/2016 - Irrespective of whether we could send the Consumer Option to POS IP/POS PORT, we will now show that consumer option is selected,
				 * if it was previously chosen.
				 */
#if 0
				if(setConsOptSelected(iIndex, 0) != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while setting the option selected!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
#endif
			}
			else
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Sent [Gift Receipt] Consumer Option Notification");
					addAppEventLog(SCA, PAAS_SUCCESS, UNSOLICITED_MESSAGE, szAppLogData, NULL);
				}
			}
			break;

		case EMAILRECEIPT_OPT:
			debug_sprintf(szDbgMsg, "%s: Email Receipt Consumer option is selected", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Email Receipt Consumer Option is Selected");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			memset(&stUnsolMsgInfo, 0x00, sizeof(UNSOLMSGINFO_STYPE));
			stUnsolMsgInfo.iEmailReceipt = 1;

			rv = storeandSendConsOptInfo(&stUnsolMsgInfo, EMAILRECEIPT_OPT, szAppLogDiag);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to store/send Consumer Option Info", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Send [Email Receipt] Consumer Option Notification to POS.");
					addAppEventLog(SCA, PAAS_INFO, UNSOLICITED_MESSAGE, szAppLogData, szAppLogDiag);
				}
				/* Daivik:27/7/2016 - Irrespective of whether we could send the Consumer Option to POS IP/POS PORT, we will now show that consumer option is selected,
				 * if it was previously chosen.
				 */
#if 0
				if(setConsOptSelected(iIndex, 0) != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while setting the option selected!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
#endif
			}
			else
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Sent [Email Receipt] Consumer Option Notification");
					addAppEventLog(SCA, PAAS_SUCCESS, UNSOLICITED_MESSAGE, szAppLogData, NULL);
				}
			}

			break;

		case EMAILOFFER_OPT:
			debug_sprintf(szDbgMsg, "%s: Email Offer Consumer option is selected", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Email Offer Consumer Option is Selected");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			memset(&stUnsolMsgInfo, 0x00, sizeof(UNSOLMSGINFO_STYPE));
			stUnsolMsgInfo.iEmailOffer = 1;
			rv = storeandSendConsOptInfo(&stUnsolMsgInfo, EMAILOFFER_OPT, szAppLogDiag);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to store/send Consumer Option Info", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Send [Email Offer] Consumer Option Notification to POS.");
					addAppEventLog(SCA, PAAS_INFO, UNSOLICITED_MESSAGE, szAppLogData, szAppLogDiag);
				}
				/* Daivik:27/7/2016 - Irrespective of whether we could send the Consumer Option to POS IP/POS PORT, we will now show that consumer option is selected,
				 * if it was previously chosen.
				 */
#if 0
				if(setConsOptSelected(iIndex, 0) != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while setting the option selected!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
#endif
			}
			else
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Sent [Email Offer] Consumer Option Notification");
					addAppEventLog(SCA, PAAS_SUCCESS, UNSOLICITED_MESSAGE, szAppLogData, NULL);
				}
			}

			break;

		case PRIVCARD_OPT:
			debug_sprintf(szDbgMsg, "%s: Private Card Consumer option is selected", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Private Card Consumer Option is Selected");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			/* Allocating memory for tracks placeholder */
			/* 3 for the track info and the fourth for the eparms*/
			szTracks = (char **) malloc(sizeof(char *) * 4);
			if(szTracks == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(szTracks, 0x00, sizeof(char *) * 4);

			if ( eEncType == RSA_ENC)
			{
				szRsaTracks = (char **) malloc(sizeof(char *) * 2);
				if(szRsaTracks == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				memset(szRsaTracks, 0x00, sizeof(char *) * 2);
			}
			else if ( eEncType == VSD_ENC)
			{
				szVsdEncData = (char **) malloc(sizeof(char *) * 4);
				if(szVsdEncData == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				memset(szVsdEncData, 0x00, sizeof(char *) * 4);
			}

			memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

			rv = getCardData(szTracks, szRsaTracks, szVsdEncData, &(stCardDtls.iCardSrc), szClrDt, &szSBTData, NULL /*Amount not passing since we dont have*/, PAAS_FALSE, NULL, NULL, stCardDtls.szPayPassType);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fetch card track info", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Fail to Fetch Data From Card With Result[%d]", rv);
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
				if(rv == UI_CANCEL_PRESSED)
				{
					debug_sprintf(szDbgMsg, "%s: User cancelled the card capture", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					/*
					 * Praveen_P1: Need to send reset command only when user pressed the cancel button
					 * for other error cases we dont require
					 */
					disableCardReaders();
				}

				/* reset this option, since swiped card is read/cancelled */
				rv = setConsOptSelected(iIndex, 0);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while setting the option selected!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

				break;
			}

			/* Settig the card type for the private card*/
			stCardDtls.iCardType = PL_CARD_TYPE;

			/* parse the card information */
			rv = getCardDtls(&stCardDtls, szTracks, szRsaTracks, szVsdEncData, "0.00", PAAS_TRUE, NULL /* Need to send STB data here*/);
			if(rv != SUCCESS)
			{
				if(rv == ERR_NOT_IN_BINRANGE)
				{
					showPSGenDispForm(MSG_CARD_NOTACCEPTED, FAILURE_ICON, iStatusMsgDispInterval);

				}
				else if(rv == ERR_CARD_INVALID)
				{
					showPSGenDispForm(MSG_CARD_INVALID, FAILURE_ICON, iStatusMsgDispInterval);

				}
				else if(rv == ERR_MANUAL_NOTALLWD)
				{
					showPSGenDispForm(MSG_MANENTRY_NTALLWD, FAILURE_ICON, iStatusMsgDispInterval);

				}
				else if(rv == ERR_BAD_CARD)
				{
					showPSGenDispForm(MSG_CARD_INVALID, FAILURE_ICON, iStatusMsgDispInterval);
				}
				/* reset this option, since swiped card is read/cancelled */
				rv = setConsOptSelected(iIndex, 0);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while setting the option selected!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				break;
			}

			//if(strcasecmp(stCardDtls.szPymtMedia, "KOHLS") != 0)
			if(stCardDtls.bKohlsChargeCard == PAAS_FALSE)
			{
				debug_sprintf(szDbgMsg, "%s: Swiped card is NON-KOHLS card", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* reset this option, since swiped card is non-Kohls card */
				rv = setConsOptSelected(iIndex, 0);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while setting the option selected!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				showPSGenDispForm(MSG_CARD_NOTACCEPTED, FAILURE_ICON, iStatusMsgDispInterval);
			}
			else
			{
				/*
				 * The following function is called when Pre-Swipe is done and Priv label is selected
				 * from the consumer option.
				 * The second parameter conveys that pre-swipe is not done
				 *
				 */
				//set the encryption type here.
				stCardDtls.iEncType = getEncryptionType();

				rv = storePreSwipeCardDtls(&stCardDtls, PAAS_FALSE);
				if(rv != SUCCESS)
				{
					if(rv == CARD_TOKEN_NOT_PRESENT)
					{
						debug_sprintf(szDbgMsg, "%s: Could not fetch the CARD TOKEN for this card", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						/* reset this option, since could not fetch the Kohls card token */
						rv = setConsOptSelected(iIndex, 0);
						if(rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: Error while setting the option selected!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}

						showPSGenDispForm(MSG_CARDTOK_NOTFOUND, FAILURE_ICON, iStatusMsgDispInterval);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Failed to store/send Priv Card Token details", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
			}
			// CID-67333, 67314: 27-Jan-16: MukeshS3:
			// We need to free the memory allocated to the tracks even if getCardDtls function fails
			// So, shifting this piece of code down, just after switch-case block
/*
			if(szTracks != NULL)
			{
				for(iCnt = 0; iCnt < 4; iCnt++)
				{
					if(szTracks[iCnt] != NULL)
					{
						free(szTracks[iCnt]);
					}
				}

				free(szTracks);
			}

			if(szRsaTracks != NULL)
			{
				for(iCnt = 0; iCnt < 2; iCnt++)
				{
					if(szRsaTracks[iCnt] != NULL)
					{
						free(szRsaTracks[iCnt]);
					}
				}

				free(szRsaTracks);
			}
*/
			break;

		default:
			break;
		}
		// CID-67333, 67314: 27-Jan-16: MukeshS3:
		// We need to free the memory allocated to the tracks even if getCardDtls function fails
		if(szTracks != NULL)
		{
			for(iCnt = 0; iCnt < 4; iCnt++)
			{
				if(szTracks[iCnt] != NULL)
				{
					free(szTracks[iCnt]);
				}
			}

			free(szTracks);
		}

		if(szRsaTracks != NULL)
		{
			for(iCnt = 0; iCnt < 2; iCnt++)
			{
				if(szRsaTracks[iCnt] != NULL)
				{
					free(szRsaTracks[iCnt]);
				}
			}

			free(szRsaTracks);
		}

		if(szVsdEncData != NULL)
		{
			for(iCnt = 0; iCnt < 4; iCnt++)
			{
				if(szVsdEncData[iCnt] != NULL)
				{
					free(szVsdEncData[iCnt]);
				}
			}

			free(szVsdEncData);
		}
	}

	/*
	 * For consumer options other than Priv label
	 * we dont navigate to some other screen so no need to show line item screen again
	 */

	if(iOptionSelected == PRIVCARD_OPT || iOptionSelected == CANCELPRESWIPE_OPT)
	{
		//releaseMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");
#if 0
		/* Showing line item screen after capturing the Consumer option */
		showLineItemScreen();
#endif
		iAppState = getAppState();
		if(iOptionSelected == CANCELPRESWIPE_OPT)
		{
			if(strcasecmp(pstUIConsOpt->stUIData.stEmvDtls.szEMVRespCmd, "R01") == 0)
			{
				//MukeshS3: 8-July-16: FRD 3.95 QRCODE
				if(iAppState != IN_LINEITEM_QRCODE && iAppState != DISPLAYING_QRCODE_SCREEN)
				{
					bCardRemoveForm = PAAS_TRUE;
					showLIForm(PAAS_FALSE, PAAS_TRUE);
					svcWait(iStatusMsgDispInterval);
				}
				flushPreSwipeCardDetails(bRetainPreTapVasData);
			}
		}

		if(isLineItemDisplayEnabled())
		{
			if(iOptionSelected == PRIVCARD_OPT)
			{
				clearScreen(1);
				showLineItemScreen();
			}
			else if(getAppState() == IN_LINEITEM_QRCODE)	//MukeshS3: 8-July-16: FRD 3.95 QRCODE
			{
				showQRCodeAtLIScrn();
			}
			else if(getXPILibMode() &&  ! bCardRemoveForm)
			{
				showLineItemScreen();
			}
			else
			{
				showLIForm(PAAS_TRUE, PAAS_FALSE);
			}
		}
		else if(iAppState != DISPLAYING_QRCODE_SCREEN)	// When card was inserted at welcome screen, POS sent DISPLAY_QRCODE command & than card was removed
		{												// at QR full screen, than we should not change the screen(will just flush the card data)
			showWelcomeScreen(PAAS_FALSE);
		}

		/* KranthiK1: This below needs to be at the end here as we need to
		 * show the updated line items at the end.
		 */
		//acquireMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");
		if(giAllowLITran || getRetryLITran())
		{
			if(isLineItemDisplayEnabled()) //We need to show line item screen only line item display is enabled
			{
				debug_sprintf(szDbgMsg, "%s: AllowLITran is set so updating lineitem screen", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/*
				 * Need to update line item screen first before updating with the new ones
				 * passing 0 as the parameter so that it does show up line item screen
				 */

				if(strcasecmp(pstUIConsOpt->stUIData.stEmvDtls.szEMVRespCmd, "R01") == 0)
				{
					initUpdateForLineItemScreen(0, &stUIReq);
				}
				else
				{
					/*
					 * Praveen_P1: 28 March 2016
					 * At this place, remove card is shown above so need to update the init form
					 * so that updated line item will be honoured
					 */
					initUpdateForLineItemScreen(2, &stUIReq);
				}

				/* Showing line item screen */
				showUpdatedLIScreen();
			}

			//Setting this to 0 within updatedLIScreen
			//giAllowLITran = 0;

		}
		/*
		 * KranthiK1: Coded cannot be added here. Anything that is required should be added
		 * above this if.
		 */
	}

	pstUIConsOpt->iStatus = COMPLETE;
	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: getScndData
 *
 * Description	: This API returns the secondary data
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getScndData()
{
	return giScndData;
}

/*
 * ============================================================================
 * Function Name: getScndDetailedStatus
 *
 * Description	: This API returns the detailed staus while capturing the card details like (capturing card details,capturing pin details, split amount screen, cashback, zip code capture)
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getScndDetailedStatus()
{
	return giDetailedStatus;
}

/*
 * ============================================================================
 * Function Name: setScndData
 *
 * Description	: This API sets the secondary data
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void setScndData(int iState)
{
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	giScndData = iState;

	debug_sprintf(szDbgMsg, "%s: Setting the Secondary Data state to [%d]", __FUNCTION__, giScndData);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: setScndDetailedStatus
 *
 * Description	: This API sets the secondary data
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void setScndDetailedStatus(int iState)
{
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	giDetailedStatus = iState;

	debug_sprintf(szDbgMsg, "%s: Setting the Detailed Secondary status to [%d]", __FUNCTION__, giDetailedStatus);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: setAppState
 *
 * Description	: This API sets the state of the application
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void setAppState(int iState)
{
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif
	acquireMutexLock(&gptAllowAppStateMutex, "Setting the Application State");

	giAppState = iState;

	debug_sprintf(szDbgMsg, "%s: Setting the APP state to [%d]", __FUNCTION__, giAppState);
	APP_TRACE(szDbgMsg);

	releaseMutexLock(&gptAllowAppStateMutex, "Setting the Application State");
}

/*
 * ============================================================================
 * Function Name: setDetailedAppState
 *
 * Description	: This API sets the state of the application
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void setDetailedAppState(int iState)
{
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif
	acquireMutexLock(&gptAllowAppStateMutex, "Setting the Application State");

	giDetailedAppState = iState;

	debug_sprintf(szDbgMsg, "%s: Setting the Detailed APP state to [%d]", __FUNCTION__, giDetailedAppState);
	APP_TRACE(szDbgMsg);

	releaseMutexLock(&gptAllowAppStateMutex, "Setting the Application State");
}

/*
 * ============================================================================
 * Function Name: getAppState
 *
 * Description	: This API sets the state of the application
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int	getAppState()
{
	int iCurState;

#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	acquireMutexLock(&gptAllowAppStateMutex, "Reading the application State");

	iCurState = giAppState;

	debug_sprintf(szDbgMsg, "%s: Current APP state is [%d]", __FUNCTION__, iCurState);
	APP_TRACE(szDbgMsg);

	releaseMutexLock(&gptAllowAppStateMutex, "Reading the application State");

	return iCurState;
}

/*
 * ============================================================================
 * Function Name: getDetailedAppState
 *
 * Description	: This API sets the detailed status of the application while capturing the details in payment transaction from user
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int	getDetailedAppState()
{
	int iCurState;

#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	acquireMutexLock(&gptAllowAppStateMutex, "Reading the application State");

	iCurState = giDetailedAppState;

	debug_sprintf(szDbgMsg, "%s: Current detailed APP status is [%d]", __FUNCTION__, giDetailedAppState);
	APP_TRACE(szDbgMsg);

	releaseMutexLock(&gptAllowAppStateMutex, "Reading the application State");

	return iCurState;
}

/*
 * ============================================================================
 * Function Name: setLastForm
 *
 * Description	: This API sets the last form number(except QR code form) before executing any device transaction
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void setLastForm(int iLastForm)
{
	acquireMutexLock(&gptLastFormMutex, "Setting the Last Form Number");

	giLastForm = iLastForm;

	releaseMutexLock(&gptLastFormMutex, "Setting the Last Form Number");
}

/*
 * ============================================================================
 * Function Name: getLastForm
 *
 * Description	: This API gets the last form number
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int	getLastForm()
{
	int iLastForm;

	acquireMutexLock(&gptLastFormMutex, "Geting the Last Form Number");

	iLastForm = giLastForm;

	releaseMutexLock(&gptLastFormMutex, "Geting the Last Form Number");

	return iLastForm;
}

/*
 * ============================================================================
 * Function Name: setCardReadEnabled
 *
 * Description	: This API sets the state of the application
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void setCardReadEnabled(int iCardReadEnabled)
{
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif
	acquireMutexLock(&gptCardReadEnabledMutex, "Setting card read state");

	giCardReadEnabled = iCardReadEnabled;

	releaseMutexLock(&gptCardReadEnabledMutex, "Setting card read state");

	if(iCardReadEnabled == PAAS_FALSE)
	{
		setD41Active(PAAS_FALSE);
	}

	debug_sprintf(szDbgMsg, "%s: Setting the Card Read Enabled state to [%d]", __FUNCTION__, iCardReadEnabled);
	APP_TRACE(szDbgMsg);

}

/* Function Name: getRetryLITran
 *
 * Description	: This API returns the giOnlineLITranRetry param.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getRetryLITran()
{
	return giOnlineLITranRetry;
}

void setRetryLITran(int iVal)
{
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	acquireMutexLock(&gptOnlineLITranRetryMutex, "Retry LI Transaction");

	giOnlineLITranRetry = iVal;

	releaseMutexLock(&gptOnlineLITranRetryMutex, "Retry LI Transaction");

	debug_sprintf(szDbgMsg, "%s: Setting the Retry LI Tran Value to [%d]", __FUNCTION__, giOnlineLITranRetry);
	APP_TRACE(szDbgMsg);
}

void setAllowLITran(int iVal)
{
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	acquireMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");

	giAllowLITran = iVal;

	debug_sprintf(szDbgMsg, "%s: Setting the Allow LI Tran value to [%d]", __FUNCTION__, giAllowLITran);
	APP_TRACE(szDbgMsg);

	releaseMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");
}
int getAllowLITran()
{
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	int iCurState;

	acquireMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");

	iCurState = giAllowLITran;

	releaseMutexLock(&gptAllowLITranMutex, "Allow LI Transaction");

	debug_sprintf(szDbgMsg, "%s: The Allow LI Tran value is [%d]", __FUNCTION__, iCurState);
	APP_TRACE(szDbgMsg);

	return iCurState;
}

/* Function Name: setD41Active
 *
 * Description	: This API sets the state of the application
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void setD41Active(int iActiveD41)
{
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	acquireMutexLock(&gptD41ActiveMutex, "Setting D41 Active State");

	giActiveD41 = iActiveD41;

	releaseMutexLock(&gptD41ActiveMutex, "Setting D41 Active State");

	debug_sprintf(szDbgMsg, "%s: Setting the D41 Active state to [%d]", __FUNCTION__, giActiveD41);
	APP_TRACE(szDbgMsg);

}
/*
 * ============================================================================
 * Function Name: getD41Active
 *
 * Description	: This API gets the information if D41 is active
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int	getD41Active()
{
	int isD41Active;
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif
	acquireMutexLock(&gptD41ActiveMutex, "Getting D41 Active State");

	isD41Active = giActiveD41;

	releaseMutexLock(&gptD41ActiveMutex, "Getting D41 Active State");

	debug_sprintf(szDbgMsg, "%s: D41 Active State [%d]", __FUNCTION__, isD41Active);
	APP_TRACE(szDbgMsg);

	return isD41Active;
}

/*
 * ============================================================================
 * Function Name: getCardReadEnabled
 *
 * Description	: This API sets the state of the application
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int	getCardReadEnabled()
{
	int iCardReadEnabled;

/*
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif
*/

	acquireMutexLock(&gptCardReadEnabledMutex, "Checking if card read is enabled");

	iCardReadEnabled = giCardReadEnabled;

//	debug_sprintf(szDbgMsg, "%s: Card Read Enabled State [%d]", __FUNCTION__, iCardReadEnabled);
//	APP_TRACE(szDbgMsg);

	releaseMutexLock(&gptCardReadEnabledMutex, "Checking if card read is enabled");

	return iCardReadEnabled;
}

/*
 * ============================================================================
 * Function Name: setCardReadEnabled
 *
 * Description	: This API sets the state of the application
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void setOnlineTranStatus(int iOnlineTranStatus)
{
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif
	acquireMutexLock(&gptOnlineTranStatus, "Online Tran Status");

	giOnlineTranStatus = iOnlineTranStatus;

	releaseMutexLock(&gptOnlineTranStatus, "Online Tran Status");

	debug_sprintf(szDbgMsg, "%s: Setting the online tran status to [%d]", __FUNCTION__, iOnlineTranStatus);
	APP_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: getOnlineTranStatus
 *
 * Description	: This API sets the state of the application
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int	getOnlineTranStatus()
{
	int iOnlineTranStatus;

#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	acquireMutexLock(&gptOnlineTranStatus, "Online Tran Status");

	iOnlineTranStatus = giOnlineTranStatus;

	debug_sprintf(szDbgMsg, "%s: Online Tran Status [%d]", __FUNCTION__, iOnlineTranStatus);
	APP_TRACE(szDbgMsg);

	releaseMutexLock(&gptOnlineTranStatus, "Online Tran Status");

	return iOnlineTranStatus;
}


/*
 * ============================================================================
 * Function Name: getPOSInitiatedState
 *
 * Description	: This API gets the POS cancelled state of the application
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int	getPOSInitiatedState()
{
	return gPOSInitiatedState;
}

/*
 * ============================================================================
 * Function Name: retValForPosIniState
 *
 * Description	: This API returns the value that needs to be returned to caller
 * when a Cancel / Disconnection event is initiated by POS
 *
 * Input Params	:
 *
 * Output Params: POS_CANCELLED/POS_DISCONNECTED
 * ============================================================================
 */
int	retValForPosIniState()
{
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	switch(gPOSInitiatedState)
	{
	case POSCANCELLED:
		return POS_CANCELLED;
	case POSDISCONNECTED:
		return POS_DISCONNECTED;
	default:
		debug_sprintf(szDbgMsg, "%s: POS initiated State %d", __FUNCTION__, gPOSInitiatedState);
		APP_TRACE(szDbgMsg);
		return 0;
	}
}
/*
 * ============================================================================
 * Function Name: setPOSInitiatedState
 *
 * Description	: This API gets the POS cancelled state of the application
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void setPOSInitiatedState(int iState)
{
#ifdef DEBUG
	char szDbgMsg[256]		= "";
#endif

	acquireMutexLock(&gptCancelState, "Set POS Cancel State");

	gPOSInitiatedState = iState;

	releaseMutexLock(&gptCancelState, "Set POS Cancel State");

	debug_sprintf(szDbgMsg, "%s: Setting POS Cancelled State %d", __FUNCTION__, gPOSInitiatedState);
	APP_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * End of file bLogic.c
 * ============================================================================
 */
