/*
 * =============================================================================
 * Filename    : blAPIs.c
 *
 * Application : Mx Point SCA
 *
 * Description : 
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/md5.h>
#include <openssl/rand.h>
#include <SemtekHTdes.h>

#include <svc.h>

#include "common/utils.h"
#include "common/tranDef.h"
#include "common/common.h"
#include "common/VCL.h"
#include "db/tranDS.h"
#include "db/posDataStore.h"
#include "db/safDataStore.h"
#include "db/dataSystem.h"
#include "uiAgent/uiAPIs.h"
#include "bLogic/blStepAPIs.h"
#include "bLogic/bLogicCfgDef.h"
#include "bLogic/bLogicCfgData.h"
#include "bLogic/blAPIs.h"
#include "bLogic/blVHQSupDef.h"
#include "sci/sciDef.h"
#include "ssi/ssiDef.h"
#include "ssi/ssiCfgDef.h"
#include "ssi/ssiMain.h"
#include "sci/sciMain.h"
#include "uiAgent/uiAPIs.h"
#include "uiAgent/emvAPIs.h"
#include "uiAgent/uiCfgDef.h"
#include "uiAgent/uiCfgData.h"
#include "uiAgent/uiMsgFmt.h"
#include "appLog/appLogAPIs.h"
#include "sci/sciConfigDef.h"
#include "bLogic/blVHQSupDef.h"
#include "common/cfgData.h"

#define MAC_128_BIT_KEYLEN	16

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

#define putEnvFile(dest, param, paramVal)		scaPutEnvFile(dest, param, paramVal);
#define getEnvFile(dest, param, paramVal, iLen)	scaGetEnvFile(dest, param, paramVal, iLen);

extern int		 giTotPOSConn;

extern void setDemoModeParamValue(PAAS_BOOL);
extern PAAS_BOOL isSigCapEnabled();
extern PAAS_BOOL isLoyaltyCapEnabled();
extern PAAS_BOOL isBapiEnabled();
extern PAAS_BOOL isEmailCapEnabled();
extern void getSigImageType(char *);
extern int isAdminPacketRequestRequired();
extern int sendSCIUnsolMsg(UNSOLMSGINFO_PTYPE , char *, int, char *);
extern int resetConsOptSelection();
extern int 	showSystemInfo();
extern void saveLogs();
extern int resetUIImageUpdationTimer();
extern int resetKAIntervalUpdationTimer();
extern int resetSAFPurgingTimer();
extern PAAS_BOOL isEmailValid(const char *);
extern int saveMcntStoreId(char *);
extern int saveMcntLaneId(char *);
extern PAAS_BOOL checkBinInclusion(char *);
extern int doDeviceRegistration(char *, char*);
extern PAAS_BOOL isDHIEnabled();
extern int getAppCurNumBckupLogFiles();
extern int getHostProtocolFormat();
extern int getKeepAliveInterval();
extern int getKeepAliveParameter();
extern PAAS_BOOL isCheckHostConnStatusEnable();
extern int getPAPymtTypeFormat();
extern int	 setPaymentTypesDtlsForPymtTran(int * tendersList, char *pszPymtTypes);
extern int getRawCardDtls(RAW_CARDDTLS_PTYPE *, CARDDTLS_PTYPE, PAAS_BOOL);
extern int getNumOfTimedOutTranToSAF();
extern int getQRCodeDtlsFromSession(DISPQRCODE_DTLS_PTYPE);

typedef struct
{
	char	szMACLbl[10];
	char	szSessKey[10];
}
SESSMAP_STYPE, * SESSMAP_PTYPE;

/* Static global variables */
static SESSMAP_PTYPE pstSessMap = NULL;

/* Static functions' declarations */
static int	 testCounter(char *, char *, char *);
static int	 promptClerkPIN(char *, int , char *, int);
static int   getSAFRequestType(SAFDTLS_PTYPE );
static int	 getSAFTORRequestType(SAFDTLS_PTYPE );
static int	 udpatePaymentCmd(char *, int *);
static int	 validatePOSPIN(char * , char *, char *, int );
static int 	 clearPymntDetailsForUserCancel(char *, char *);
static int	 validateXMLReqWithFormatStrForAllPrompts(CREDITAPPDTLS_PTYPE, char *);
static char* createAESKey(char *);
static int	 setPOSTendersDtlsToTenderList(int *, POSTENDERDTLS_PTYPE);
static void * VSPRegPostAdvanceDDK(void * );
static pthread_t	ptVSPRegPostAdvanceDDK		= 0;

extern pthread_mutex_t	gptCardDataInBQueueMutex;

int flushCardDetails(char *);

/*
 * ============================================================================
 * Function Name: processSCAPreamble
 *
 * Description	: 
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE 
 * ============================================================================
 */
int processSCAPreamble(char *pszErrMsg)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szTranStkKey[10]	= "";
	char			szAppLogData[300]	= "";
//	char			szErrMsg[256]		= "";
//	ENC_TYPE		eEncType;
	PAAS_BOOL		bEmvEnabled			= PAAS_FALSE;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	bEmvEnabled = isEmvEnabledInDevice();
	/*
	 * MukeshS3: 31-March-16: Removing the setting of encryption mode in XPI/sending E10 command.
	 * XPIDEFSEC parameter has been moved to /mnt/flash/userdata/share/xpiconfig.ini & POS is not having any access to use E10 command now in XPI.
	 */
#if 0
	/*If EMV is enabled, We need to set the encryption mode for EMV in XPI application here */
	if(bEmvEnabled == PAAS_TRUE)
	{
		//Get the encryption type enabled in the device.
		eEncType = getEncryptionType();
		//Set the encryption mode for EMV(In XPI application)
		if(SUCCESS != setEncryptionModeForEmv(eEncType))
		{
			debug_sprintf(szDbgMsg, "%s: Failed to set the encryption mode for EMV in XPI app",
																				__FUNCTION__);
			APP_TRACE(szDbgMsg);

			return rv;
		}
	}
#endif
	while(1)
	{
		/* KranthiK1:
		 * Listing the set of preamble steps
		 */
		preProcessPreambleSteps();


		/* Perform the following steps of the preamble in order. If any one of
		 * the steps fail, stop further execuation and restart from step 1 */

		/* Step 1. Configure DHI merchant Settings */
		if(SUCCESS != configureDHIMcntSettings())
		{
			debug_sprintf(szDbgMsg, "%s: Failed to configure merchant settings",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Configure Merchant Settings");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}

			continue;
		}

		/* Step 2. Do the network configuration */
		if(SUCCESS != configureNetwork())
		{
			debug_sprintf(szDbgMsg, "%s: Failed to configure network",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Configure Network Settings");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}

			continue;
		}

		memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
		/* Step 3. Download the device key if not done before */
		if(SUCCESS != devAdminSetup(szTranStkKey))
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the device key",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Complete Device Registration");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}

			continue;
		}

		memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
		/* Step 4. Do VSP registration */
		/*
		 * Following module pushes the transaction and pops
		 * at the end
		 */
		if(SUCCESS != preambleVSP(szTranStkKey))
		{
			debug_sprintf(szDbgMsg, "%s: Failed to do Device registration(VCL)",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Complete Device VSP Registration");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}

			continue;
		}
/*AJAYS2: Procedding with EMV ini even if EMV is disabled *
 */
		setCriticalStepInProgressFlag(PAAS_TRUE);
		rv = doEmvInitialization(pszErrMsg);
		setCriticalStepInProgressFlag(PAAS_FALSE);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to do EMV Key Load", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/*AjayS2: 04-Feb-2016: Updating CTLS mode in XPI before Dummy Sale
		 */
		setCriticalStepInProgressFlag(PAAS_TRUE);
		rv = updateCTLSMode(pszErrMsg);
		setCriticalStepInProgressFlag(PAAS_FALSE);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Update CTLS Mode", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

#if 0
		/* Step 5. EMV Initialization if EMV is enabled*/
		if(PAAS_TRUE == isEmvEnabledInDevice())
		{
			/* Do the EMV Parameter Download*/

			rv = doEmvInitialization();
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to do EMV Key Load", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
#endif
		memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
		/* Step 5. Do sample transaction */
		if(SUCCESS != doDummyTransaction(szTranStkKey))
		{
			debug_sprintf(szDbgMsg, "%s: Failed to complete dummy transaction",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Complete Dummy Transaction");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}

			continue;
		}

		/* If all steps are successfull then break from the loop */
		break;
	}

	if(strlen(szTranStkKey) > 0)
	{
		/* Remove the queue created from the memory */
		deleteQueue(szTranStkKey);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: procDeviceResyncKey
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int procDeviceResyncKey()
{
	int			rv					= SUCCESS;
#ifdef DEBUG	
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Check if the device resync with the host server needs to be done */
	if(PAAS_TRUE == isAdminPacketRequestRequired() )
	{
		/* Download the new device key from host*/
		if(SUCCESS != doDeviceResyncKey())
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the new device key",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doEmvInitialization
 *
 * Description	: This API would do necessary EMV configuartion settings by 
 *				  getting EMV config setting parameters from host and
 *				  updating the EMV ini files, and send XCONFIG to FA
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int doEmvInitialization(char * pszErrMsg)
{
	int 		rv 					= SUCCESS;
	int			iEmvReqd			= 0;

#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif	

	debug_sprintf(szDbgMsg, "%s: ---Enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Fixed MCD Issue NPO-8558: 09-June-2016: AjayS2: Changes to make code Fail safe in case, device reboots during EMV initialization.
	 */
	if(doesFileExist(TEMP_CTLS_INI_FILE_PATH) == SUCCESS)
	{
		if(rename(TEMP_CTLS_INI_FILE_PATH, CTLS_INI_FILE_PATH) != SUCCESS) // T_raghavendranR1 CID 83350 (#1 of 4): Unchecked return value from library (CHECKED_RETURN)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to rename the Files", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		debug_sprintf(szDbgMsg, "%s: Renamed Temp EMV Ini File[%s]", __FUNCTION__, TEMP_CTLS_INI_FILE_PATH);
		APP_TRACE(szDbgMsg);
	}
	if(doesFileExist(TEMP_EMV_TABLES_FILE_PATH) == SUCCESS)
	{
		if(rename(TEMP_EMV_TABLES_FILE_PATH, EMV_TABLES_FILE_PATH) != SUCCESS) // T_raghavendranR1 CID 83350 (#2 of 4): Unchecked return value from library (CHECKED_RETURN)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to rename the Files", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		debug_sprintf(szDbgMsg, "%s: Renamed Temp EMV Ini File[%s]", __FUNCTION__, TEMP_EMV_TABLES_FILE_PATH);
		APP_TRACE(szDbgMsg);
	}
	if(doesFileExist(TEMP_CAPK_DATA_INI_FILE_PATH) == SUCCESS)
	{
		if(rename(TEMP_CAPK_DATA_INI_FILE_PATH, CAPK_DATA_INI_FILE_PATH) != SUCCESS) // T_raghavendranR1 CID 83350 (#3 of 4): Unchecked return value from library (CHECKED_RETURN)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to rename the Files", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		debug_sprintf(szDbgMsg, "%s: Renamed Temp EMV Ini File[%s]", __FUNCTION__, TEMP_CAPK_DATA_INI_FILE_PATH);
		APP_TRACE(szDbgMsg);
	}
	if(doesFileExist(TEMP_OPT_FLAG_FILE_PATH) == SUCCESS)
	{
		if(rename(TEMP_OPT_FLAG_FILE_PATH, OPT_FLAG_FILE_PATH) != SUCCESS) // T_raghavendranR1 CID 83350 (#4 of 4): Unchecked return value from library (CHECKED_RETURN)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to rename the Files", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		debug_sprintf(szDbgMsg, "%s: Renamed Temp EMV Ini File[%s]", __FUNCTION__, TEMP_OPT_FLAG_FILE_PATH);
		APP_TRACE(szDbgMsg);
	}

	if( (iEmvReqd = isDevEmvSetupRqd()) != EMV_SETUP_NOT_REQ)
	{
		if(SUCCESS == procEmvInitialization(iEmvReqd, pszErrMsg))
		{
			debug_sprintf(szDbgMsg, "%s: EMV Initialization done Successfully", __FUNCTION__);
			APP_TRACE(szDbgMsg);	

			/*AjayS2: 07 Nov 2016: TACs will be read from EMVTables now in loadAIDListAndConfig() function
			 */
#if 0
			//Update The TACs in AID List File
			rv = updateTACFromINItoAIDList(pszErrMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating TACs in %s", __FUNCTION__, AID_LIST_FILE_NAME);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
			}
#endif
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: EMV Initialization Failed",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;		
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: EMV Initialization is not required",
															__FUNCTION__);
		APP_TRACE(szDbgMsg);		
	}
	
	debug_sprintf(szDbgMsg, "%s: ---Returning---[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);	
	
	return rv;
}
/*
 * ============================================================================
 * Function Name: authenticatePOS
 *
 * Description	: This function checks whether the given POS is authenticated
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int authenticatePOS(char * szTranCtr, char * mac, char * macLbl, int iEncFlag, char * errMsg)
{
	int		rv					= SUCCESS;
	int		iMACLen				= 0;
	int		iEncMACLen			= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAESKey[16]		= "";
	char	szEncMAC[64]		= "";
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";
	uchar	szLocMAC[128]		= "";
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		iMACLen = sizeof(szLocMAC);
		/*
		 * We need not authenticate MAC if the incoming packet is <ETRANSACTION>
		 * we have to do only counter check
		 */
		if(iEncFlag != 1)
		{
			/* Get the AES key for the corresponding label (from the data system) */
			rv = getAESKey(macLbl, (uchar *)szAESKey, errMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Couldnt get AES key", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error, POS Authentication Failed.");
					strcpy(szAppLogDiag, "Could Not Get the AES Key to Process POS Authentication");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
				}
				break;
			}

			/* Calculate the MAC for the counter data (supplied as parameter) */
			rv = calcMAC((uchar *)szAESKey, sizeof(szAESKey), (uchar *)szTranCtr,
											strlen(szTranCtr), szLocMAC, &iMACLen);
			if(rv != SUCCESS)
			{
				/* Couldnt calculate MAC */
				debug_sprintf(szDbgMsg,"%s: MAC calculation failed.",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error, POS Authentication Failed.");
					strcpy(szAppLogDiag, "MAC Calculation of Counter Data Failed, Please Check the POS Request");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
				}

				rv = FAILURE;
				break;
			}

			/* Calculated the MAC of the transaction counter value locally */
			/* Convert the locally calculated MAC to encoded base 64 string */
			rv = encdBase64((uchar *) szLocMAC, iMACLen, (uchar *)szEncMAC,
																	&iEncMACLen);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Base 64 encoding of MAC value failed",
																	 __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error, POS Authentication Failed.");
					strcpy(szAppLogDiag, "Base64 Encoding of MAC Value Failed");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
				}

				rv = FAILURE;
				break;
			}

			/* Compare the locally calculated MAC with the one received
			 * from the POS (supplied as parameter) */
			if(strcmp(mac, szEncMAC) != SUCCESS)
			{
				/* MAC values do not match, that means the MAC value sent by POS is
				 * incorrect */
				debug_sprintf(szDbgMsg, "%s: Both MAC values are not same",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error, POS Authentication Failed.");
					strcpy(szAppLogDiag, "Incorrect MAC values of POS and Transaction Counter, Please Check the POS Request");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
				}

				sprintf(errMsg, "Incorrect MAC Value from [%s]", macLbl);
				rv = ERR_INV_FLD;

				break;
			}
		}
		/* Test the counter value */
		if( (rv = testCounter(macLbl, szTranCtr, errMsg)) != SUCCESS )
		{
			debug_sprintf(szDbgMsg, "%s: Counter Test Failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error, POS Authentication Failed.");
				strcpy(szAppLogDiag, "Transaction Counter Testing Failed, Please Check the POS Request");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: registerPOS
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int registerPOS(char * szTranKey, char * szStatMsg)
{
	int				rv					= SUCCESS;
	int				keyLen				= 0;
	int				iRegCmdVersion		= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	int				iMaxPOSConn			= 0;
	char			szClrkPIN[9]		= "";
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";
	uchar			szAESKey[17]		= "";
	POSSEC_STYPE	stSecDtls;
#ifdef DEVDEBUG
	int				iCnt				= 0;
	char			szTmp[5]			= "";
	char			szDbgMsg[4096]		= "";
#elif DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(REGISTER);
	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Executing POS Register Command");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		iMaxPOSConn = getMaxPOSConnections();

		debug_sprintf(szDbgMsg, "%s: Max POSConnection Allowed [%d]", __FUNCTION__, iMaxPOSConn);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Total No of POS Currently Connected [%d]", __FUNCTION__, giTotPOSConn);
		APP_TRACE(szDbgMsg);

		if((iMaxPOSConn != -1) && (giTotPOSConn >= iMaxPOSConn))
		{
			debug_sprintf(szDbgMsg, "%s: Total POS Registered Exceeded the Allowed Maximum Limit", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			sprintf(szStatMsg, "POS Registration Exceeded the Allowed Maximum Limit");

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "POS Registration Exceeded the Allowed Maximum Limit.");
				strcpy(szAppLogDiag, "Please UnRegister the Other Registered POS to Proceed Further.");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_POS_REG_LIMIT_EXCEEDED;
			break;
		}

		/* get the security details from the transacion place holder */
		memset(&stSecDtls, 0x00, sizeof(stSecDtls));
		rv = getPOSSecDtlsForTran(szTranKey, &stSecDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get the security details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "FAILED to Get the POS Security Details");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			break;
		}

		if(strlen(stSecDtls.szRegCmdVer) > 0)
		{
			iRegCmdVersion = atoi(stSecDtls.szRegCmdVer);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Command version is not available, defaulting to 1", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRegCmdVersion = 1;
		}

		debug_sprintf(szDbgMsg, "%s: iRegCmdVersion %d", __FUNCTION__, iRegCmdVersion);
		APP_TRACE(szDbgMsg);

		if (iRegCmdVersion == 1)
		{
			if(strlen(stSecDtls.szPIN) == 0)
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "PIN Data Missing.");
					strcpy(szAppLogDiag, "Please Enter the PIN Details Field for POS Registration");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
				}
				rv = ERR_FLD_REQD;
				break;
			}
		}
		/* Prompt the Clerk PIN. */
		rv = promptClerkPIN(szClrkPIN, iRegCmdVersion, szStatMsg, ENTER_PAIRPIN_TITLE);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get PIN", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the PIN Details From User");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			break;
		}

		rv = validatePOSPIN(szClrkPIN, stSecDtls.szPIN, stSecDtls.szRsaKey, iRegCmdVersion);
		if(rv != SUCCESS)
		{
			sprintf(szStatMsg, "[PIN] mismatch");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "PIN Data Mismatch.");
				strcpy(szAppLogDiag, "Mismatch in PIN Details Entered, Please Check the PIN Entered");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}
			rv = ERR_FLD_MISMATCH;

			break;
		}

		/* PINs match; generate the AES Key */
		createAESKey((char *)szAESKey);
		if(strlen((char *)szAESKey) == 0) //CID 67455 (#1 of 1): Array compared against 0 (NO_EFFECT). T_RaghavendranR1. Checking for strlen instead of NULL check for Array.
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to create MAC key",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Create AES Key");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
			break;
		}

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: AES KEY = ", DEVDEBUGMSG, __FUNCTION__);

		for(iCnt = 0; iCnt < 16; iCnt++)
		{
			sprintf(szTmp, "%02X ", szAESKey[iCnt]);
			strcat(szDbgMsg, szTmp);
		}

		APP_TRACE(szDbgMsg);
#endif

		/* Generate new MAC Label for the key just generated */
		getMACLabel(stSecDtls.szMACLbl);
		debug_sprintf(szDbgMsg, "%s: MAC Label [%s] created", __FUNCTION__,
															stSecDtls.szMACLbl);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "New MAC Label Created For the Entered Key");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		APP_TRACE(szDbgMsg);

		/* Add the Label and Key into the persistent storage */
		rv = addPosInfoRecord(szAESKey, stSecDtls.szMACLbl);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to add MAC key & label",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Add MAC Key and Label");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		/* Encrypt the AES Key */
		rv = rsaEncryptData((uchar *) (stSecDtls.szRsaKey), szAESKey,
							(uchar *) (stSecDtls.szMACKey), &keyLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in encryption of MAC Key",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			sprintf(szStatMsg, "RSA Key Error");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error in Encrypting the MAC Label");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szStatMsg);
			}

			rv = FAILURE;
			break;
		}

		/* Encrypt Entry code with public key from POS */
		if(iRegCmdVersion == 2)
		{
#if 0	//TODO: Waiting for clarification whether to send plain entry code or encrypted entry code.
			debug_sprintf(szDbgMsg, "%s: Fill Entry code",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			//Copy plain PIN to send it to POS in the response.
			strcpy(stSecDtls.szPIN, szClrkPIN);

#endif
			debug_sprintf(szDbgMsg, "%s: Encrypting the Entry code",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Encrypt the Entry code */
			rv = rsaEncryptData((uchar *) (stSecDtls.szRsaKey), (uchar *)szClrkPIN,
								(uchar *) (stSecDtls.szEncEntryCode), &keyLen);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error in encryption of entry code",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error in Encrypting the Entry Code With POS Public Key");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
				}

				sprintf(szStatMsg, "RSA Key Error");

				rv = FAILURE;
				break;
			}
		}

		/* Update the security details in the transaction place holder */
		rv = updatePOSSecDtlsForTran(szTranKey, &stSecDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to updated security details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		giTotPOSConn++;
		sprintf(szStatMsg, "Registered [%s]", stSecDtls.szMACLbl);
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "POS got Registered Successfully, MacLabel for this POS is [%s]", stSecDtls.szMACLbl);
			addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: registerEncryptedPOS
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int registerEncryptedPOS(char * szTranKey, char * szStatMsg)
{
	int				rv					= SUCCESS;
	int				keyLen				= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	int				iMaxPOSConn			= 0;
	char			szClrkPIN[9]		= "";
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";
	uchar			szAESKey[17]		= "";
	POSSEC_STYPE	stSecDtls;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(REGISTER);
	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Executing the Register Encrypted POS Command");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}

		iMaxPOSConn = getMaxPOSConnections();

		debug_sprintf(szDbgMsg, "%s: Max POSConnection Allowed [%d]", __FUNCTION__, iMaxPOSConn);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Total No of POS Currently Connected [%d]", __FUNCTION__, giTotPOSConn);
		APP_TRACE(szDbgMsg);

		if((iMaxPOSConn != -1) && (giTotPOSConn >= iMaxPOSConn))
		{
			debug_sprintf(szDbgMsg, "%s: Total POS Registered Exceeded the Allowed Maximum Limit", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			sprintf(szStatMsg, "POS Registration Exceeded the Allowed Maximum Limit");

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "POS Registration Exceeded the Allowed Maximum Limit.");
				strcpy(szAppLogDiag, "Please UnRegister the Other Registered POS to Proceed Further.");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_POS_REG_LIMIT_EXCEEDED;
			break;
		}

		/* get the security details from the transacion place holder */
		memset(&stSecDtls, 0x00, sizeof(stSecDtls));
		rv = getPOSSecDtlsForTran(szTranKey, &stSecDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get the security details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Prompt the 8 digit alpha numeric Clerk PIN. */
		rv = promptClerkPIN(szClrkPIN, 2/* We need to pass 2 for 8 digit prompt*/, szStatMsg, ENTER_8PIN_TITLE);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get PIN", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the PIN Details From User");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			break;
		}

		/* Validate the first 4 digits*/
		/* Using the same validate pos pin function by sending the version as 2*/
		rv = validatePOSPIN(szClrkPIN, stSecDtls.szPIN, stSecDtls.szRsaKey, 2);
		if(rv != SUCCESS)
		{
			sprintf(szStatMsg, "[PIN] mismatch");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "PIN Data Mismatch.");
				strcpy(szAppLogDiag, "Mismatch in PIN Details Entered, Please check the PIN Entered");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}
			rv = ERR_FLD_MISMATCH;

			break;
		}

		/* Encrypt Entry code with POS public key */
		rv = rsaEncryptData((uchar *) (stSecDtls.szRsaKey), (uchar *)szClrkPIN, (uchar *) (stSecDtls.szEncEntryCode), &keyLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in encryption of entry code",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error in Encrypting the Entry Code With POS Public Key");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		/* Generate AES128 Key */
		createAESKey((char *)szAESKey);
		if(strlen((char *)szAESKey) == 0) // CID 67419 (#1 of 1): Array compared against 0 (NO_EFFECT) T_RaghavendranR1. Now checking the string length.
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to create MAC key",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Encrypt the AES128 key with POS public key */
		rv = rsaEncryptData((uchar *) (stSecDtls.szRsaKey), szAESKey,
							(uchar *) (stSecDtls.szMACKey), &keyLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in encryption of MAC Key",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error in Encrypting the AES Key");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		/* Generate new MAC Label for the key just generated */
		getMACLabel(stSecDtls.szMACLbl);
		debug_sprintf(szDbgMsg, "%s: MAC Label [%s] created", __FUNCTION__,
															stSecDtls.szMACLbl);
		APP_TRACE(szDbgMsg);

		/* Add the Label and Key into the persistent storage */
		rv = addPosInfoRecord(szAESKey, stSecDtls.szMACLbl);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to add MAC key & label",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Add MAC Key and Label");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		/* Update the security details in the transaction place holder */
		rv = updatePOSSecDtlsForTran(szTranKey, &stSecDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to updated security details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		giTotPOSConn++;
		sprintf(szStatMsg, "Registered [%s]", stSecDtls.szMACLbl);
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "POS Got Registered Successfully in Encryption Mode, Assigned MacLabel for this POS is [%s]", stSecDtls.szMACLbl);
			addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: unregisterPOS
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int unregisterPOS(char * szTranKey, char *pszConnInfo, char * szStatMsg)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	PAAS_BOOL		bPOSRegistered		= PAAS_FALSE;
	//char			szClrkPIN[5]		= "";
	POSSEC_STYPE	stSecDtls;
#ifdef DEVDEBUG
	char			szDbgMsg[512]		= "";
#elif DEBUG
	char			szDbgMsg[256]		= "";
#endif


	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(REGISTER);
	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Executing the POS UnRegister Command");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		/* get the security details from the transaction place holder */
		memset(&stSecDtls, 0x00, sizeof(stSecDtls));
		rv = getPOSSecDtlsForTran(szTranKey, &stSecDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get the security details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Security Details From POS");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			break;
		}
#if 0
		/*
		 * Praveen_P1: As per discussion with Wes/Sharvani we will not
		 * prompt for the entry code for the UnRegister command
		 * Even if it comes in the request, we will ignore
		 */
		/* Prompt the Clerk PIN. */
		rv = promptClerkPIN(szClrkPIN, 1, szStatMsg, ENTER_UNPAIRPIN_TITLE);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get PIN", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Compare the two PINs */
		if(strcmp(szClrkPIN, stSecDtls.szPIN) != SUCCESS)
		{
			/* PINs did not match */
#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s:%s: Both PINs don't MATCH [%s] vs [%s]",
						DEVDEBUGMSG, __FUNCTION__, stSecDtls.szPIN, szClrkPIN);
			APP_TRACE(szDbgMsg);
#else
			debug_sprintf(szDbgMsg, "%s: Both PINs don't MATCH", __FUNCTION__);
			APP_TRACE(szDbgMsg);
#endif

			sprintf(szStatMsg, "[PIN] mismatch");
			rv = ERR_FLD_MISMATCH;
			break;
		}
#endif

		//Check whether the requested POS is registered or not
		bPOSRegistered = isPOSRegistered(stSecDtls.szMACLbl);

		if( bPOSRegistered != PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Couldn't find the record for label [%s]",
							__FUNCTION__, stSecDtls.szMACLbl);
			APP_TRACE(szDbgMsg);

			sprintf(szStatMsg, "POS Record Not Found For[%s]", stSecDtls.szMACLbl);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, szStatMsg);
				addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			rv = ERR_NO_FLD;
		}

		/*
		 * Praveen_P1: Sending response to POS before deleting the POS records
		 * We used to send response in DeviceTransaction function, since
		 * we are deleting the POS records here, incase of encrypted registration
		 * no key available to encrypt the packet..thats why sending the response
		 * before deleting the records
		 */

		/* Set the proper return status for the transaction */
		setStatusInTran(szTranKey, rv);

		/* Set the response details for the transaction */
		setGenRespDtlsinTran(szTranKey, szStatMsg);

		/* Send the response to the POS, using the SCI module */
		sendSCIResp(pszConnInfo, szTranKey, stSecDtls.szMACLbl);

		if( bPOSRegistered == PAAS_TRUE ) //Delete POS Record only if requested POS is already registered with application.
		{
			/* Delete POS Record For a Given MAC Label */
			rv = deletePosInfoRecord(stSecDtls.szMACLbl);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to delete POS record for [%s]",
						__FUNCTION__, stSecDtls.szMACLbl);
				APP_TRACE(szDbgMsg);

				break;
			}

			sprintf(szStatMsg, "Unregistered POS For MAC Label[%s]", stSecDtls.szMACLbl);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "POS Unregistered Successfully");
				addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: unregisterAllPOS
 *
 * Description	: This function would unregister all the POS which are registered with the application.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int unregisterAllPOS(char * szTranKey, char *pszConnInfo, char *pszMacLbl, char * szStatMsg)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
//	PAAS_BOOL		bPOSRegistered		= PAAS_FALSE;
//	POSSEC_STYPE	stSecDtls;

#ifdef DEVDEBUG
	char			szDbgMsg[512]		= "";
#elif DEBUG
	char			szDbgMsg[256]		= "";
#endif


	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(REGISTER);
	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Executing the POS UnRegister All Command");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}

#if 0
		/* get the security details from the transaction place holder */
		memset(&stSecDtls, 0x00, sizeof(stSecDtls));
		rv = getPOSSecDtlsForTran(szTranKey, &stSecDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get the security details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Security Details From POS");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			break;
		}
#endif
		/*
		 * Praveen_P1: Removing the following check, It is decided that for UNREGISTERALL
		 * we will not get the MAC_LABEL, thats the security we are taking since anyone
		 * should able to unregisterall (this will be used when we cant unregister)
		 */
#if 0
		/*ArjunU1: Before sending unregister response to POS, check whether POS is allowed to unregister all POS.
		 * 		   Note: POS should be already register in order to perform unregister.
		 */

		//Check whether the requested POS is registered or not
		bPOSRegistered = isPOSRegistered(stSecDtls.szMACLbl);

		if( bPOSRegistered != PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Couldn't find the record for label [%s]",
							__FUNCTION__, stSecDtls.szMACLbl);
			APP_TRACE(szDbgMsg);

			sprintf(szStatMsg, "POS Record Not Found For MAC Label[%s]", stSecDtls.szMACLbl);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, szStatMsg);
				addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			rv = ERR_NO_FLD;
		}
#endif
		/*
		 * ArjunU1: Sending response to POS before deleting the POS records
		 * We used to send response in DeviceTransaction function, since
		 * we are deleting the POS records here, incase of encrypted registration
		 * no key available to encrypt the packet..thats why sending the response
		 * before deleting the records
		 */

		/* Set the proper return status for the transaction */
		setStatusInTran(szTranKey, rv);

		/* Set the response details for the transaction */
		setGenRespDtlsinTran(szTranKey, szStatMsg);

		/* Send the response to the POS, using the SCI module */
		sendSCIResp(pszConnInfo, szTranKey, pszMacLbl);

		/* Delete all POS records */
		rv = deleteAllPosInfoRecord();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to delete All POS records"
											, __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		sprintf(szStatMsg, "Unregistered All POS");
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "All Registered POS Got Unregistered Successfully");
			addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSignForDevTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getSignForDevTran(char * szTranKey, char * szStatMsg, int iCmd)
{
	int				rv					= SUCCESS;
	int				iLen				= 0;
	int				iEncLen				= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	uchar *			pszSign				= NULL;
	char *			pszTmp				= NULL;
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";
	SIGDTLS_STYPE	stSigDtls;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(&stSigDtls, 0x00, sizeof(SIGDTLS_STYPE));

		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		setAppState(CAPTURING_DEV_SIGNATURE);

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Capturing Signature Details From the User");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}

#if 0
		/*
		 * Praveen_P1: No need to check config.usr1 for standalone DEVICE commands
		 * like standalone signature, email and loyalty capture
		 * email from Sharvani dated 5 march 2014
		 */
		/* Check is signature capture is enabled in the device or not */
		if(PAAS_TRUE != isSigCapEnabled())
		{
			debug_sprintf(szDbgMsg, "%s: Signature capture is DISABLED",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);

			sprintf(szStatMsg, "Signature Capture is DISABLED");

			rv = ERR_UNSUPP_OPR;
			break;
		}
#endif
		/* Get the signature placeholder from the transaction instance */
		rv = getSigDtlsForDevTran(szTranKey, &stSigDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get signature placeholder",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);


			rv = FAILURE;
			break;
		}


		if(iCmd == SCI_SIGN_EX)
		{
			rv = showSignatureExScreenForDevTran(stSigDtls.szDispTxt, &pszSign, &iLen);
		}
		else
		{
			rv = showSignatureScreenForDevTran(stSigDtls.szDispTxt, &pszSign, &iLen);
		}

		if(rv == SUCCESS)
		{
			char	szImgType[10]	= "";

			/* Encode the signature data into ascii bytes using the base 64
			 * encoding mechanism and update these details in the transaction
			 * instance */
			iEncLen = iLen * 2;
			pszTmp = (char *) malloc(iEncLen);
			if(pszTmp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pszTmp, 0x00, iEncLen);
			encdBase64(pszSign, iLen, (uchar *)pszTmp, &iEncLen);

			/* Assign the signature in the placeholder */
			stSigDtls.szSign = pszTmp;
			pszTmp = NULL;

			/* Assign the signature type */
			getSigImageType(szImgType);
			if(strcmp(szImgType, "BMP") == SUCCESS)
			{
				sprintf(stSigDtls.szMime, "image/bmp");
			}
			else if(strcmp(szImgType, "3BA") == SUCCESS)
			{
				sprintf(stSigDtls.szMime, "image/3ba");
			}
			else
			{
				sprintf(stSigDtls.szMime, "image/tiff");
			}

			/* update the signature details in the transaction instance */
			rv = updateSigDtlsForDevTran(szTranKey, &stSigDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update sign dtls",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Update Signature Details in the Transaction");
					addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
				}

				rv = FAILURE;
				break;
			}

			sprintf(szStatMsg, "Signature Captured");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Signature Details Successfully Captured From User");
				addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			memset(&stSigDtls, 0x00, sizeof(SIGDTLS_STYPE));
		}
		else if(rv == ERR_DEVICE_APP)
		{
			sprintf(szStatMsg, "device application error FA");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Signature Details Not Captured From User.");
				strcpy(szAppLogDiag, "Device Application Internal Error");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}
		}
		else if(rv == UI_CANCEL_PRESSED)
		{
			debug_sprintf(szDbgMsg, "%s: User cancelled sig cap",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "User Pressed Cancel Button While Capturing the Signature Details");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}
			sprintf(szStatMsg, "CANCELED by Customer");
			rv = ERR_USR_CANCELED;
		}

		break;
	}

	/* De-allocate the signature details */
	if(pszSign != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: de-allocating psz sign", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		free(pszSign);
	}

	if( (stSigDtls.szSign) != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: de-allocating stSign", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		free(stSigDtls.szSign);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDeviceNameForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getDeviceNameForDevTran(char * szTranKey, char * szStatMsg)
{
	int					rv					= SUCCESS;
	int					iAppLogEnabled		= isAppLogEnabled();
	DEVICENAME_STYPE	stDevNameDtls;
	char				szAppLogData[512]	= "";
#ifdef DEBUG
	char				szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Getting the Device Name");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}
		memset(&stDevNameDtls, 0x00, sizeof(DEVICENAME_STYPE));

		getDeviceName(stDevNameDtls.szDeviceName);
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "Device Name is [%s]", stDevNameDtls.szDeviceName);
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		debug_sprintf(szDbgMsg, "%s: Device Name is [%s]", __FUNCTION__, stDevNameDtls.szDeviceName);
		APP_TRACE(szDbgMsg);

		getDevMacLabels(stDevNameDtls.szMacLabels);
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "MAC Label is [%s]", stDevNameDtls.szMacLabels);
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		debug_sprintf(szDbgMsg, "%s: Device Mac Labels are [%s]", __FUNCTION__, stDevNameDtls.szMacLabels);
		APP_TRACE(szDbgMsg);

		getDevSrlNum(stDevNameDtls.szSerialNum);
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "Device Serial Number is [%s]", stDevNameDtls.szSerialNum);
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		debug_sprintf(szDbgMsg, "%s: Device serial Number is [%s]", __FUNCTION__, stDevNameDtls.szSerialNum);
		APP_TRACE(szDbgMsg);

		/* update the Device Name details in the transaction instance */
		rv = updateDevNameDtlsForDevTran(szTranKey, &stDevNameDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to update counter dtls",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Update Device Name");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}
		debug_sprintf(szDbgMsg, "%s: Updated counter details in transaction instance", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setDeviceNameForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int setDeviceNameForDevTran(char * szTranKey, char * szStatMsg)
{
	int					 rv					= SUCCESS;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[512]	= "";
	DEVICENAME_STYPE 	stDevNameDtls;
#ifdef DEBUG
	char				szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Setting the Device Name");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		memset(&stDevNameDtls, 0x00, sizeof(DEVICENAME_STYPE));

		rv = getDevNameDtlsForDevTran(szTranKey, &stDevNameDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get device name dtls",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Device Name Details");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Device Name is [%s] needs to be set", __FUNCTION__, stDevNameDtls.szDeviceName);
		APP_TRACE(szDbgMsg);

		/* Update the device name into config file */
		updateDeviceName(stDevNameDtls.szDeviceName);
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "Successfully Updated the Device Name [%s] Details", stDevNameDtls.szDeviceName);
			addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
		}

		debug_sprintf(szDbgMsg, "%s: Updated device name in the config file", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/* If any New Payment Type is Added in Future we need to add that here too to get in the
 * GET_PAYMENT_TYPES command response*/
/*
 * ============================================================================
 * Function Name: getPaymentTypes
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getEnabledPaymentTypes(char * szTranKey, char * szStatMsg)
{
	int					rv							= SUCCESS;
	int					iAppLogEnabled				= isAppLogEnabled();
	int					tenderSetting[MAX_TENDERS]  = {0};
	int					iCnt						= 0;
	char				szAppLogData[512]			= "";
	char				szAppLogDiag[256]			= "";
	char				szPaymentTypes[100]			= "";
	char *				pszEndPtr					= NULL;
	PAYMENTTYPES_STYPE	stPymtTypesDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(&stPymtTypesDtls, 0x00, sizeof(PAYMENTTYPES_STYPE));

		debug_sprintf(szDbgMsg, "%s: Need to get the Enabled Payment Types from the App", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Getting the Payment Types from the Configuration");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		rv = getTendersListFromSettings(tenderSetting);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Tender List From Payment Settings!!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		memset(szPaymentTypes, 0x00, sizeof(szPaymentTypes));

		for(iCnt = 0; iCnt < MAX_TENDERS; iCnt++)
		{
			if(tenderSetting[iCnt] == PAAS_TRUE)
			{
				switch(iCnt)
				{
				case TEND_CREDIT:
					strcat(szPaymentTypes, "CREDIT");
					break;
			    case TEND_DEBIT:
			    	strcat(szPaymentTypes, "DEBIT");
					break;
				case TEND_GIFT:
					strcat(szPaymentTypes, "GIFT");
					break;
				case TEND_BALANCE:
					strcat(szPaymentTypes, "CASH");
					break;
				case TEND_PRIVATE:
					strcat(szPaymentTypes, "PRIV_LBL");
					break;
				case TEND_MERCHCREDIT:
					strcat(szPaymentTypes, "MERCH_CREDIT");
					break;
			    case TEND_PAYPAL:
			    	strcat(szPaymentTypes, "PAYPAL");
					break;
				case TEND_EBT:
					strcat(szPaymentTypes, "EBT");
					break;
				case TEND_FSA:
					strcat(szPaymentTypes, "FSA");
					break;
				}
				strcat(szPaymentTypes, "|");
			}
		}

		// Remove the last appended PIPE while sending the response back to POS.
		if(strlen(szPaymentTypes) > 0)
		{
			pszEndPtr = szPaymentTypes + strlen(szPaymentTypes);
			--pszEndPtr;
		}

		if(pszEndPtr)
		{
			if(*pszEndPtr == '|')
			{
				*pszEndPtr = '\0';
			}
		}

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Successfully Obtained the Payment Types from the Application Configuration");
			addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
		}

		strcpy(stPymtTypesDtls.szPaymentTypes, szPaymentTypes); //Copying the value to the structure variable


		/* update the Payment Types Details in the transaction instance */
		rv = updateGetPymtTypesDtlsForDevTran(szTranKey, &stPymtTypesDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to update Payment Types dtls", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		debug_sprintf(szDbgMsg, "%s: Updated Payment Types Details in Transaction instance", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processDispMsg
 *
 * Description	: This API is use to display a message box on to the current screen.
 * 				  The text contents & timeout details are being sent in SCI request.
 *
 * Input Params	: transaction key, status msg
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processDispMsg(char * szTranKey, char * szStatMsg)
{
	int					rv							= SUCCESS;
	int					iAppLogEnabled				= isAppLogEnabled();
	int					iAppState					= -1;
	int					iIdleScrnMode				= -1;
	char				szAppLogData[512]			= "";
	DISPMSG_STYPE		stDispMsgDtls;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		iAppState = getAppState();
		if(iAppState == NO_SESSION_IDLE || iAppState == IN_SESSION_IDLE)
		{
			iIdleScrnMode = getIdleScrnMode();
			if(iIdleScrnMode == DEFAULT_IDLE || iIdleScrnMode == FULL_IDLE_VID)
			{
				rv = ERR_UNSUPP_OPR;
				strcpy(szStatMsg, "MESSAGE Cannot be Displayed on Current Form");
				break;
			}
		}
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Displaying the Message On the Screen");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		debug_sprintf(szDbgMsg, "%s: Need to display message box on the screen", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		memset(&stDispMsgDtls, 0x00, sizeof(DISPMSG_STYPE));

		rv = getDispMsgDtlsForDevTran(szTranKey, &stDispMsgDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get Display message details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		rv = displayMsgBox(&stDispMsgDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to display Message Box", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Display the Message");
				addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
			}
			break;
		}
		else
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Successfully Displayed the Message");
				addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
			}
		}

		rv = updateDispMsgDtlsForDevTran(szTranKey, &stDispMsgDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Update Display message details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCurrentCounterValue
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getCurrentCounterValue(char * szTranKey, char * szStatMsg)
{
	int					rv					= SUCCESS;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[512]	= "";
	char				szAppLogDiag[256]	= "";
	ulong				ulCurCtr			= 0L;
	COUNTERDTLS_STYPE	stCounterDtls;
	TRAN_PTYPE			pstTran				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}


		memset(&stCounterDtls, 0x00, sizeof(COUNTERDTLS_STYPE));


		/* Get the Maclabel from the transaction instance */
		rv = getCounterDtlsForDevTran(szTranKey, &stCounterDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get maclabel placeholder",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Need to get current counter value for [%s] Mac label", __FUNCTION__, stCounterDtls.szMacLabel);
		APP_TRACE(szDbgMsg);

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Getting the Current Counter Value");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		rv = getTranCounter(stCounterDtls.szMacLabel, &ulCurCtr);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the counter!!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			sprintf(szStatMsg, "No record exists for MAC label[%s]", stCounterDtls.szMacLabel);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Current Counter Value");
				sprintf(szAppLogDiag, "No Record Exists for the MAC Label [%s]", stCounterDtls.szMacLabel);
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Current counter value for [%s] Mac label is [%lu]", __FUNCTION__, stCounterDtls.szMacLabel, ulCurCtr);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "Successfully Obtained the Current Counter Value, Counter Value is [%ld]", ulCurCtr);
			addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
		}

		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Transaction dtls", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		sprintf(pstTran->szTranCntr, "%lu", ulCurCtr); //Copying the value to the structure variable

/*KranthiK1: TODO: Remove it Later*/
#if 0
		/* update the counter details in the transaction instance */
		rv = updateCounterDtlsForDevTran(szTranKey, &stCounterDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to update counter dtls",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
#endif

		debug_sprintf(szDbgMsg, "%s: Updated counter details in transaction instance", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processSetParm
 *
 * Description	:
 *
 * Input Params	: This function set the given Parameters in the request to the
 * configuration File and memory and restart the Host Interface.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processSetParm(char * szTranKey, char * szStatMsg)
{
	int				rv						= SUCCESS;
	int				iStatusMsgDispInterval  = DFLT_STATUS_DISP_TIME;
	int				iAppLogEnabled			= isAppLogEnabled();
	int				iResultCode				= 0;
	int				iRetry					= 0;
	int				iHostType				= 0;
	PAAS_BOOL		bAdded					= PAAS_FALSE;
	char			szSection[30+1]			= "";
	char			szKey[50+1]				= "";
	char			szAppLogData[300]		= "";
	char			szAppLogDiag[300]		= "";
	char			szTranStkKey[10]		= "";
	char			szErrMsg[300]			= "";
	int				iParamPresent			= 0;
	SETPARM_STYPE	stSetParmDtls;
	RESPDTLS_STYPE	stRespDtls;

#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif


	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "Executing the SET_PARM Command");
		addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
	}

	while(1)
	{
		memset(&stSetParmDtls, 0x00, sizeof(SETPARM_STYPE));

		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		CHECK_POS_INITIATED_STATE;

		setAppState(PROCESSING_SET_PARM);

		/* Showing Stand by Status in the GEN_FORM */
		showPSGenDispForm(MSG_PROCESSING, STANDBY_ICON, 0);

		/* Get the time placeholder from the transaction instance */
		rv = getSetParmDtlsForDevTran(szTranKey, &stSetParmDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Set Parm Placeholder", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		iHostType = stSetParmDtls.iHostType;

		switch(iHostType)
		{
		case RC_HOST:
			strcpy(szSection, SECTION_RCHI);
			break;
		case VANTIV_HOST:
			strcpy(szSection, SECTION_DHI);
			break;
		case CP_HOST:
			strcpy(szSection, SECTION_CPHI);
			break;
		case 0:
			debug_sprintf(szDbgMsg, "%s: HOST Indicator Not provided in the POS Request for Set Param", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Host Indicator Not Provided in the POS Request.");
				strcpy(szAppLogDiag, "Please Provide the Host Indicator in POS Request.");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, szAppLogDiag);
			}
			strcpy(szStatMsg, "Host Indicator Parameter Not Provided");
			rv = ERR_FLD_REQD;
			break;
		default:
			debug_sprintf(szDbgMsg, "%s: Invalid HOST Indicator Provided in the POS Request for Set Param", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid Host Indicator Provided.");
				strcpy(szAppLogDiag, "Please Check the Host Indicator in POS Request.");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, szAppLogDiag);
			}
			strcpy(szStatMsg, "Invalid Host Indicator");
			rv = ERR_FLD_REQD;
			break;
		}

		if(rv == SUCCESS)
		{
			if(strlen(stSetParmDtls.szMID))
			{
				debug_sprintf(szDbgMsg, "%s: Setting MID to %s", __FUNCTION__, stSetParmDtls.szMID);
				APP_TRACE(szDbgMsg);

				if(atoi(stSetParmDtls.szMID) == 0)
				{
					debug_sprintf(szDbgMsg, "%s:Invalide MID value", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					showPSGenDispForm(MSG_ERROR, FAILURE_ICON, iStatusMsgDispInterval);

					sprintf(szStatMsg, "PARM_MID Field Contains Invalid Value [%s]", stSetParmDtls.szMID);
					rv = ERR_INV_FLD_VAL;
					break;
				}

				strcpy(szKey, MID);
				putEnvFile(szSection, szKey, stSetParmDtls.szMID);

				iParamPresent = 1;
			}
			if(strlen(stSetParmDtls.szTID))
			{
				debug_sprintf(szDbgMsg, "%s: Setting TID to %s", __FUNCTION__, stSetParmDtls.szTID);
				APP_TRACE(szDbgMsg);

				if(atoi(stSetParmDtls.szTID) == 0)
				{
					debug_sprintf(szDbgMsg, "%s:Invalide TID value", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					showPSGenDispForm(MSG_ERROR, FAILURE_ICON, iStatusMsgDispInterval);

					sprintf(szStatMsg, "PARM_TID Field Contains Invalid Value [%s]", stSetParmDtls.szTID);
					rv = ERR_INV_FLD_VAL;
					break;
				}

				strcpy(szKey, TID);
				putEnvFile(szSection, szKey, stSetParmDtls.szTID);

				iParamPresent = 1;
			}
			if(strlen(stSetParmDtls.szLane))
			{
				debug_sprintf(szDbgMsg, "%s: Setting Lane to %s", __FUNCTION__, stSetParmDtls.szLane);
				APP_TRACE(szDbgMsg);

				strcpy(szKey, LANEID);
				putEnvFile(szSection, szKey, stSetParmDtls.szLane);
				putEnvFile("DHI", szKey, stSetParmDtls.szLane);

				iParamPresent = 1;
			}
			if(strlen(stSetParmDtls.szAdminURL))
			{
				if(iHostType != VANTIV_HOST)
				{
					debug_sprintf(szDbgMsg, "%s: Setting Registration URL to %s", __FUNCTION__, stSetParmDtls.szAdminURL);
					APP_TRACE(szDbgMsg);

					strcpy(szKey, REG_HOST_URL);
					putEnvFile(szSection, szKey, stSetParmDtls.szAdminURL);
				}
				iParamPresent = 1;
			}
			if(strlen(stSetParmDtls.szPrimURL))
			{
				debug_sprintf(szDbgMsg, "%s: Setting Primary URL to %s", __FUNCTION__, stSetParmDtls.szPrimURL);
				APP_TRACE(szDbgMsg);
				if(iHostType == VANTIV_HOST)
				{
					strcpy(szKey, "hosturl");
				}
				else
				{
					strcpy(szKey, PRIM_HOST_URL);
				}
				putEnvFile(szSection, szKey, stSetParmDtls.szPrimURL);
				iParamPresent = 1;
			}
			if(strlen(stSetParmDtls.szScndURL))
			{
				debug_sprintf(szDbgMsg, "%s: Setting Secondary URL to %s", __FUNCTION__, stSetParmDtls.szScndURL);
				APP_TRACE(szDbgMsg);
				if(iHostType == VANTIV_HOST)
				{
					strcpy(szKey, "hostscndurl");
				}
				else
				{
					strcpy(szKey, SCND_HOST_URL);
				}
				putEnvFile(szSection, szKey, stSetParmDtls.szScndURL);
				iParamPresent = 1;
			}
			/*Akshaya 11-11-2016: Setting Alternate Merch ID and TimeZone */
			if(strcmp(szSection, "RCHI") == SUCCESS)
			{
				if(strlen(stSetParmDtls.szAltMerchId))
				{
					debug_sprintf(szDbgMsg, "%s: Setting Alternate Merch ID to %s", __FUNCTION__, stSetParmDtls.szAltMerchId);
					APP_TRACE(szDbgMsg);

					if(atoi(stSetParmDtls.szAltMerchId) == 0)
					{
						debug_sprintf(szDbgMsg, "%s:Invalid Alternate Merch ID value", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						showPSGenDispForm(MSG_ERROR, FAILURE_ICON, iStatusMsgDispInterval);

						sprintf(szStatMsg, "PARM_ALT_MERCH_ID Field Contains Invalid Value [%s]", stSetParmDtls.szAltMerchId);
						rv = ERR_INV_FLD_VAL;
						break;
					}

					strcpy(szKey, ALTMERCHID);
					putEnvFile(szSection, szKey, stSetParmDtls.szAltMerchId);

					iParamPresent = 1;
				}

				if(strlen(stSetParmDtls.szTimeZone))
				{
					debug_sprintf(szDbgMsg, "%s: Setting Time Zone to %s", __FUNCTION__, stSetParmDtls.szTimeZone);
					APP_TRACE(szDbgMsg);

					strcpy(szKey, TIME_ZONE);
					putEnvFile("REG", szKey, stSetParmDtls.szTimeZone);

					iParamPresent = 1;
				}
			}
			if(iParamPresent == 0)
			{
				debug_sprintf(szDbgMsg, "%s: No Parameter provided in the POS request to set", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "No Parameter Provided in the POS Request to Set.");
					strcpy(szAppLogDiag, "Please Provide the Parameters to Set in POS Request.");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, szAppLogDiag);
				}
				strcpy(szStatMsg, "No Parameter Provided to Set");
				rv = ERR_FLD_REQD;
				break;
			}

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Restarting the DHI After Setting the Given Parameters.");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			debug_sprintf(szDbgMsg, "%s: Restarting the DHI/VHI application!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = restartDHI();

			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while Restarting the DHI/VHI application!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error While Restarting DHI Application");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
				}
				break;
			}

			/* Push a new SSI transaction in the stack */
			memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
			rv = pushNewSSITran(szTranStkKey);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to add a new SSI tran", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			bAdded = PAAS_TRUE;
			/* Set the function and command as ADMIN and VERSION respectively for the
			 * getting the versio of the DHI */
			setFxnNCmdForSSITran(szTranStkKey, SSI_ADMIN, SSI_VERSION);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "DHI Restarting, Getting the DHI Version to know its status.");
				addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
			}

			iResultCode = 0;
			while((iResultCode != SUCCESS_PWC_RSLTCODE_CAPTURED) &&
					(iResultCode != SUCCESS_PWC_RSLTCODE_APPROVED) && (iRetry < 120))
			{
				svcWait(500);

				/* Send the request to the host and get the response */
				rv = doSSICommunication(szTranStkKey, szStatMsg);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: SSI communication FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					//break;
				}

				/* store the response details from the SSI tran instance to Main tran instance*/
				memset(&stRespDtls, 0x00, sizeof(RESPDTLS_STYPE));
				rv = getGenRespDtlsForSSITran(szTranStkKey, &stRespDtls);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error in storing the response details in main tran instance", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}

				if(strlen(stRespDtls.szRsltCode) == 0)
				{
					debug_sprintf(szDbgMsg, "%s: Result code for tran is NOT present",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

				iResultCode = atoi(stRespDtls.szRsltCode);

				debug_sprintf(szDbgMsg, "%s: Result code for Get DHI Version tran is [%d], iRetry [%d]",__FUNCTION__, iResultCode, iRetry);
				APP_TRACE(szDbgMsg);

				iRetry++;
			}

			if(bAdded == PAAS_TRUE)
			{
				/* The SSI transaction which was added to the stack for the device key
				 * registration has fulfilled its purpose and needs to be popped from
				 * the stack */
				popSSITran(szTranStkKey);
				deleteStack(szTranStkKey);
			}

			if( (iResultCode == SUCCESS_PWC_RSLTCODE_CAPTURED) || (iResultCode == SUCCESS_PWC_RSLTCODE_APPROVED) )
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "DHI Restarted Successfully.");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}

				debug_sprintf(szDbgMsg, "%s: AFter Restart DHI, Do Device registration ",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Doing Device Registration for the Parameters Set.");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}

				memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
				rv = doDeviceRegistration(szTranStkKey, szErrMsg);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Device Registration Failed ", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Failed to Complete Device Registration");
						addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
					}
					strcpy(szStatMsg, "Device Registration Failed");

					/* MukeshS3: 28-April-16: FRD 3.79-14
					 * We need to show the MSG_DEVREG_FAILED message along with the host result code & response text on screen
					 */
					//showPSGenDispForm(MSG_DEVREG_FAILED, FAILURE_ICON, iStatusMsgDispInterval);
					showPSMessageDispForm(szErrMsg, MSG_DEVREG_FAILED, FAILURE_ICON, iStatusMsgDispInterval);

					break;
				}
				else
				{
					showPSGenDispForm(MSG_DEVREG_SUCCESS, SUCCESS_ICON, iStatusMsgDispInterval);
				}
			}
			else
			{
				showPSGenDispForm(MSG_ERROR, FAILURE_ICON, iStatusMsgDispInterval);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Restart Host Interface.");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
				strcpy(szStatMsg, "Failed to Restart Host Interface");

				rv = FAILURE;
				break;
			}

			/* Showing Stand by Status in the GEN_FORM */
			showPSGenDispForm(MSG_PROCESSING, STANDBY_ICON, 0);

			memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
			rv = doVSPReg(NULL, szTranStkKey, PAAS_FALSE, szErrMsg, DEVICE_REG);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: VSP Registration Failed ", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "VSP Registration Failed for the New Set parameters.");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
				strcpy(szStatMsg, "VSP Registration Failed");

				/* If the VSP registration FAILED */
				//showPSGenDispForm(MSG_VSPREG_FAILED, FAILURE_ICON, iStatusMsgDispInterval);
				/* MukeshS3: 28-April-16: FRD 3.79-14
				 * We need to show the MSG_VSPREG_FAILED message along with the host result code & response text on screen
				 */
				showPSMessageDispForm(szErrMsg, MSG_VSPREG_FAILED, FAILURE_ICON, iStatusMsgDispInterval);
				break;
			}
			else
			{
				/* VSP registration was SUCCESSFULL */
				showPSGenDispForm(MSG_VSPREG_SUCCESS, SUCCESS_ICON, iStatusMsgDispInterval);
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processGetParm
 *
 * Description	:
 *
 * Input Params	: This function Get the given list of Parameters values from the config.usr1 file and sends it back to POS Response.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processGetParm(char * szTranKey, char * szStatMsg)
{
	int					rv							= SUCCESS;
	int					iLen						= 0;
	int					tenderSetting[MAX_TENDERS]	= {0};
	int					iAppState					= getAppState();
	char				*pszStartPtr				= NULL;
	char				*pszCurrPtr					= NULL;
	char				szGetParamReqList[250]		= "";
	char				szParam[51]					= "";
	char 				szValue[101]				= "";
	char				szSection[21]				= "";
	char				szParamValue[160]			= "";
	char				szGetParamRespList[512]		= "";
	char *				pszEndPtr					= NULL;
	PAAS_BOOL			bIsSAFEnabled				= PAAS_FALSE;
	GETPARM_STYPE		stGetParmDtls;
	CBACK_STYPE			stCbackDtls;
	CTRTIP_STYPE		stCtrTips;

#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);


	while(1)
	{
		memset(&stGetParmDtls, 0x00, sizeof(GETPARM_STYPE));

		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		setAppState(PROCESSING_GET_PARM);

		/* Get the time placeholder from the transaction instance */
		rv = getGetParmDtlsForDevTran(szTranKey, &stGetParmDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Get Parm Placeholder ", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Get param Parameter Value Request [%s]", __FUNCTION__, stGetParmDtls.szGetParmReq);
		APP_TRACE(szDbgMsg);

		memset(szGetParamReqList, 0x00, sizeof(szGetParamReqList));
		strcpy(szGetParamReqList, stGetParmDtls.szGetParmReq);

		getTendersListFromSettings(tenderSetting);

		bIsSAFEnabled = isSAFEnabled();

		pszStartPtr = szGetParamReqList;
		while(1)
		{
			memset(szParam, 0x00, sizeof(szParam));
			memset(szValue, 0x00, sizeof(szValue));
			memset(szSection, 0x00, sizeof(szSection));
			memset(szParamValue, 0x00, sizeof(szParamValue));

			if(pszStartPtr && *pszStartPtr && ((pszCurrPtr = strchr(pszStartPtr, '|')) != NULL)) //Parse with | separator
			{
				strncpy(szParam, pszStartPtr, (pszCurrPtr - pszStartPtr));
			}
			else if (pszStartPtr && *pszStartPtr)
			{
				strcpy(szParam, pszStartPtr); // If No | in the text, we might have come to END of the text
			}

			stripSpaces(szParam, STRIP_LEADING_SPACES|STRIP_TRAILING_SPACES);

			if(strlen(szParam))
			{
				iLen = sizeof(szValue) - 1;
				memset(szValue, 0x00, sizeof(szValue));

				if(strcasecmp(szParam, XPI_LIBRARY) == SUCCESS)
				{
					getEnvFile(SECTION_PERM, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, SCA_PACKAGE_NAME) == SUCCESS)
				{
					sprintf(szValue, "%s", getSCAPkgName());
				}
				else if(strcasecmp(szParam, PREAMBLE_SUCCESS) == SUCCESS)
				{
					getEnvFile(SECTION_REG, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, VSP_ACTIVATED) == SUCCESS)
				{
					getEnvFile(SECTION_REG, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, PINPAD_MODE) == SUCCESS)
				{
					getEnvFile(SECTION_REG, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, XML_CFGFILE_PATH) == SUCCESS)
				{
					getEnvFile(SECTION_REG, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, TIMEZONE) == SUCCESS)
				{
					getEnvFile(SECTION_REG, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, IDLE_IMG_LIST_FILE) == SUCCESS)
				{
					getEnvFile(SECTION_MEDIA, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, LI_IMG_LIST_FILE) == SUCCESS)
				{
					getEnvFile(SECTION_MEDIA, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, VIDEO_LIST_FILE) == SUCCESS)
				{
					getEnvFile(SECTION_MEDIA, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, IDLE_SCREEN_MODE_FILE) == SUCCESS)
				{
					getEnvFile(SECTION_MEDIA, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, ANAIMATION_UPDATE_INTV) == SUCCESS)
				{
					getEnvFile(SECTION_MEDIA, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, PS_GC_MANUAL_ENTRY) == SUCCESS)
				{
					getEnvFile(SECTION_MRCHNT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, CLIENT_ID) == SUCCESS)
				{
					getEnvFile(SECTION_MRCHNT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, CREDIT_PRCSR_ID) == SUCCESS)
				{
					getEnvFile(SECTION_MRCHNT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, DEBIT_PRCSR_ID) == SUCCESS)
				{
					getEnvFile(SECTION_MRCHNT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, GIFT_CRD_PRCSR_ID) == SUCCESS)
				{
					getEnvFile(SECTION_MRCHNT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, CHECK_PRCSR_ID) == SUCCESS)
				{
					getEnvFile(SECTION_MRCHNT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "925_begx") == SUCCESS)
				{
					getEnvFile(SECTION_SIGN, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "925_begy") == SUCCESS)
				{
					getEnvFile(SECTION_SIGN, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "925_endx") == SUCCESS)
				{
					getEnvFile(SECTION_SIGN, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "925_endy") == SUCCESS)
				{
					getEnvFile(SECTION_SIGN, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "915_begx") == SUCCESS)
				{
					getEnvFile(SECTION_SIGN, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "915_begy") == SUCCESS)
				{
					getEnvFile(SECTION_SIGN, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "915_endx") == SUCCESS)
				{
					getEnvFile(SECTION_SIGN, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "915_endy") == SUCCESS)
				{
					getEnvFile(SECTION_SIGN, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, SIGN_AMT_FLOOR) == SUCCESS)
				{
					getEnvFile(SECTION_SIGN, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, SIGN_IMAGE_TYPE) == SUCCESS)
				{
					getEnvFile(SECTION_SIGN, szParam, szValue, iLen);
				}
				else if((strstr(szParam, "presetamt") != NULL) || strcasecmp(szParam, CB_AMT_LIMIT) == SUCCESS)
				{
					memset(&stCbackDtls, 0x00, sizeof(CBACK_STYPE));
					rv = getPresetCbackAmtsInfo(&stCbackDtls);

					if(rv == SUCCESS)
					{
						if(strcasecmp(szParam, "presetamt_1") == SUCCESS)
						{
							sprintf(szValue, "%d", stCbackDtls.cbAmt[0]);
						}
						else if(strcasecmp(szParam, "presetamt_2") == SUCCESS)
						{
							sprintf(szValue, "%d", stCbackDtls.cbAmt[1]);
						}
						else if(strcasecmp(szParam, "presetamt_3") == SUCCESS)
						{
							sprintf(szValue, "%d", stCbackDtls.cbAmt[2]);
						}
						else if(strcasecmp(szParam, "presetamt_4") == SUCCESS)
						{
							sprintf(szValue, "%d", stCbackDtls.cbAmt[3]);
						}
						else if(strcasecmp(szParam, "presetamt_5") == SUCCESS)
						{
							sprintf(szValue, "%d", stCbackDtls.cbAmt[4]);
						}
						else if(strcasecmp(szParam, CB_AMT_LIMIT) == SUCCESS)
						{
							sprintf(szValue, "%d", stCbackDtls.maxCBAmt);
						}
					}
					else
					{
						getEnvFile(SECTION_CBACK, szParam, szValue, iLen);
					}
				}
				else if(strstr(szParam, "presettip") != NULL)
				{
					memset(&stCtrTips, 0x00, sizeof(CTRTIP_STYPE));
					rv = getPresetCtrTipsInfo(&stCtrTips);

					if(rv == SUCCESS)
					{
						if(strcasecmp(szParam, "presettip_1") == SUCCESS)
						{
							sprintf(szValue, "%d", stCtrTips.ctrTipPcnt[0]);
						}
						else if(strcasecmp(szParam,  "presettip_2") == SUCCESS)
						{
							sprintf(szValue, "%d", stCtrTips.ctrTipPcnt[1]);
						}
						else if(strcasecmp(szParam, "presettip_3") == SUCCESS)
						{
							sprintf(szValue, "%d", stCtrTips.ctrTipPcnt[2]);
						}
						else if(strcasecmp(szParam, "presettip_4") == SUCCESS)
						{
							sprintf(szValue, "%d", stCtrTips.ctrTipPcnt[3]);
						}
					}
					else
					{
						getEnvFile(SECTION_CTRTIP, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, DEMO_MODE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isDemoModeEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, VSP_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isVSPEnabledInDevice() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, VHQ_SUP_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isVHQSupEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, POS_APRV_REQD_FOR_UPDATE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isPOSApprReqdForUpdate() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, POS_UPDATE_NOTIFY_TIMEOUT) == SUCCESS)
				{
					sprintf(szValue, "%d", (int)getPOSNotificationTimoutValue());
				}
				else if(strcasecmp(szParam, IP_DISP_INTRVL) == SUCCESS)
				{
					sprintf(szValue, "%d", getMsgDispIntvl(IP_DISP_FRM));
				}
				else if(strcasecmp(szParam, SYS_INFO_DISP_INTRVL) == SUCCESS)
				{
					sprintf(szValue, "%d", getMsgDispIntvl(SYSINFO_FRM));
				}
				else if(strcasecmp(szParam, DEVNAME_DISP_INTRVL) == SUCCESS)
				{
					sprintf(szValue, "%d", getMsgDispIntvl(EMAIL_FRM));
				}
				else if(strcasecmp(szParam, NETWORK_CFG_REQD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isNwCfgReqd() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, DFLT_LANG) == SUCCESS)
				{
					strcpy(szValue, getDefaultLang());
				}
				else if(strcasecmp(szParam, HOST_PING_REQD) == SUCCESS)
				{
					getEnvFile(SECTION_DEVICE, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, EMV_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isEmvEnabledInDevice() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, CTLS_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isContactlessEnabledInDevice() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, CTLS_EMV_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isContactlessEmvEnabledInDevice() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, VERBOSERECPT_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isVerboseRecptEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, DESCRIPTIVE_ENTRY_MODE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isDescriptiveEntryModeEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, CUSTOM_TAG_REQD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isEmvCustomTagsAllowed() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, EMV_SETUP_REQD) == SUCCESS)
				{
					sprintf(szValue, "%d", isDevEmvSetupRqd());
				}
				else if(strcasecmp(szParam, EMV_SUPP_LANGS) == SUCCESS)
				{
					getEnvFile(SECTION_DEVICE, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, PREAMBLE_PING_REQD) == SUCCESS)
				{
					getEnvFile(SECTION_DEVICE, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, SUPPORTED_WALLETS) == SUCCESS)
				{
					getEnvFile(SECTION_DEVICE, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, BAPI_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isBapiEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, SWIPE_AHEAD_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isSwipeAheadEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, PRIV_LAB_CVV_REQD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isCvvReqdForPrivLabel() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, PRIV_LAB_EXP_REQD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isExpReqdForPrivLabel() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, GIFT_CVV_REQD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isCvvReqdForGiftCard() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, GIFT_EXP_REQD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isExpReqdForGiftCard() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, GIFT_PINCODE_REQD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isPINCodeReqdForGiftCard() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, DHI_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isDHIEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, STATUS_MSG_DISP_INTRVL) == SUCCESS)
				{
					sprintf(szValue, "%d", getStatusMsgDispInterval());
				}
				else if(strcasecmp(szParam, DEV_NAME_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isDevNameEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, DEBIT_OFFLINE_PIN_CVM) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isDebitOffPinCVMEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, SCND_PORT_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isSecondaryPortEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, MCNT_SETTINGS_REQD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isMerchantSettingsReqd() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, DUP_DETECT_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isDupSwipeDetectEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, LINEITEM_DISPLAY_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isLineItemDisplayEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, FULL_LINEITEM_DISPLAY) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isFullLineItemDisplayEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, APP_LOG_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isAppLogEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, MAX_BCKUP_LOG_FILES) == SUCCESS)
				{
					getEnvFile(SECTION_DEVICE, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, MAX_LOG_FILE_SIZE) == SUCCESS)
				{
					getEnvFile(SECTION_DEVICE, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, LOG_FILE_LOCATION) == SUCCESS)
				{
					getEnvFile(SECTION_DEVICE, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, DEBIT_VOID_CARD_DTLS_REQD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isCardDtlsRequiedforDebitnEBTVoid() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, SIG_OPT_ON_PINENTRY_REQD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isSigOptionReqOnPINEntryScreen() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, RETURN_EMBOSSED_NUM_FOR_GIFT) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(returnEmbossedNumForGiftType() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, RETURN_EMBOSSED_NUM_FOR_PL) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(returnEmbossedNumForPrivLblType() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, MAX_POS_CONNECTIONS) == SUCCESS)
				{
					sprintf(szValue, "%d", getMaxPOSConnections());
				}
				else if(strcasecmp(szParam, TOR_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isTOREnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, TOR_RETRY_COUNT) == SUCCESS)
				{
					sprintf(szValue, "%d",getTORretryCount());
				}
				else if(strcasecmp(szParam, CHECK_HOST_CONN_STATUS) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isCheckHostConnStatusEnable() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, EMBOSSED_NUM_CALC_REQD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isEmbossedCardNumCalcRequired() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, CUR_NUM_BCKUP_LOG_FILES) == SUCCESS)
				{
					sprintf(szValue, "%d", getAppCurNumBckupLogFiles());
				}
				else if(strcasecmp(szParam, LANE_CLOSED_FONT_COL) == SUCCESS)
				{
					strcpy(szValue, getLaneClosedFontCol());
				}
				else if(strcasecmp(szParam, LANE_CLOSED_FONT_SIZE) == SUCCESS)
				{
					sprintf(szValue, "%d", getLaneClosedFontSize());
				}
				else if(strcasecmp(szParam, HOST_PROTOCOL_FORMAT) == SUCCESS)
				{
					strcpy(szValue, "UGP");
					if(getHostProtocolFormat() == HOST_PROTOCOL_SSI)
					{
						strcpy(szValue, "SSI");
					}
				}
				else if(strcasecmp(szParam, PA_PYMT_TYPE_FORMAT) == SUCCESS)
				{
					strcpy(szValue, "UGP");
					if(getPAPymtTypeFormat() == HOST_PROTOCOL_SSI)
					{
						strcpy(szValue, "SSI");
					}
				}
				else if(strcasecmp(szParam, DEV_ADMIN_REQD) == SUCCESS)
				{
					getEnvFile(SECTION_DEVICE, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, LISTEN_PORT) == SUCCESS)
				{
					sprintf(szValue, "%d", getListeningPortNum());
				}
				else if(strcasecmp(szParam, LISTEN_SCND_PORT) == SUCCESS)
				{
					sprintf(szValue, "%d", getListeningScndPortNum());
				}
				else if(strcasecmp(szParam, ADVANCE_VSP_IN_X_DAYS) == SUCCESS)
				{
					sprintf(szValue, "%d", getAdvanceVSPDays());
				}
				else if(strcasecmp(szParam, REDO_VSP_REG) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isVSPReRegReqd() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, STORE_CARD_DTLS_POST_AUTH) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isStoreCardDtlsForPostAuthEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, STORE_PREV_TRANS_CARD_DTLS_GIFT_CLOSE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isStoreCardDtlsForGiftCloseEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, PARTIAL_EMV_ALLOWED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isPartialEmvAllowed() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, CLEAR_EXPIRY_TO_EMV_HOST) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isClearExpiryToEmvHost() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, TAGS_REQ_VOID_CARD_PRESENT) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isTagsReqInCardPresentVoid() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, CARD_READ_SCREEN_FIRST) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isCardBasedTenderSelEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, EMV_KERNEL_SWITCHING_ALLOWED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isEmvKernelSwitchingAllowed() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, SERVICE_CODE_CHECK_IN_FALLBACK) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isServiceCodeCheckInFallbackEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, EMPTY_CANDID_AS_FALLBACK) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isEmptyCandidateCardAsFallBack() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, SVS_NON_DENOMINATED_CARD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isSVSNonDenominatedEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, WAIT_TO_OBTAIN_NETWORK_IP) == SUCCESS)
				{
					sprintf(szValue, "%d", getWaitTimeToObtainNetworkIP());
				}
				else if(strcasecmp(szParam, CAPTURE_SIG_FOR_PRIV_LABEL) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isSigCaptureReqdForPrivTran() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, SEND_SIGN_DTLS_TO_SSI_HOST) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isSendSignDtlsToSSIHostEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, EBT_CVV_REQD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isCvvReqdForEBTCard() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, EBT_EXP_REQD) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isExpReqdForEBTCard() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, INCLUDE_TA_FLAG) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isIncludeTAFlagEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, TERM_IDEN_DISPLAY_INTERVAL) == SUCCESS)
				{
					sprintf(szValue, "%d", getTermIdenDisplayInterval());
				}
				else if(strcasecmp(szParam, TERM_IDEN_POS_ACK_INTERVAL) == SUCCESS)
				{
					sprintf(szValue, "%d", getTermIdenPOSACKInterval());
				}
				else if(strcasecmp(szParam, BROADCAST_PORT) == SUCCESS)
				{
					sprintf(szValue, "%d", getTermIdenBroadcastPort());
				}
				else if(strcasecmp(szParam, SIGCAP_ENABLED) == SUCCESS)
				{
					strcpy(szSection, SECTION_PAYMENT);
					strcpy(szValue, "N");
					if( isSigCapEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, SPLT_TNDR_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isSplitTenderEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, CASHBACK_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isCashBackEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, EBTCASHBACK_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isEBTCashBackEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, CTRTIP_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isCtrTipEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, LYLTYCAP_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isLoyaltyCapEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, EMAILCAP_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isEmailCapEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, PARTAUTH_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isPartAuthEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, SAF_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, TIP_SUPP_ENABLED) == SUCCESS)
				{
					getEnvFile(SECTION_PAYMENT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, RECEIPT_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isReceiptEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, GIFTREFUND_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isGiftRefundEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, SAF_TRAN_FLOOR_LIMIT) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						sprintf(szValue, "%.2lf", getSAFTranFloorAmount());
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_EMV_ONLINE_PIN_TRANS) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isSafAllowedForEmvOnlinePIN() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, SAF_TOTAL_FLOOR_LIMIT) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						sprintf(szValue, "%.2lf", getSAFTotalFloorAmount());
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_PRIV_LBL_TRAN_FLOOR_LIMIT) == SUCCESS)
				{
					getEnvFile(SECTION_SAF, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, SAF_DAYS_LIMIT) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						sprintf(szValue, "%d", getSAFOfflineDaysLimit());
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_PING_INTERVAL) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						sprintf(szValue, "%d", getSAFPingInterval());
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_PURGE_DAYS) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						sprintf(szValue, "%d", getSAFPurgeDays());
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_TRAN_POST_INTERVAL) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						sprintf(szValue, "%d", getSAFPostInterval());
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_THROTLING_ENABLED) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						strcpy(szValue, "N");
						if(isSAFThrottlingEnabled() == PAAS_TRUE)
						{
							strcpy(szValue, "Y");
						}
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_THROT_INTERVAL) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						sprintf(szValue, "%d", getSAFThrottlingInterval());
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_ALLOW_OFFLINE_TRAN_TOGOWITH_ONLINE) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						strcpy(szValue, "N");
						if(bProcSAFTranWithOnlineTran() == PAAS_TRUE)
						{
							strcpy(szValue, "Y");
						}
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_FORCE_DUP_TRAN) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						strcpy(szValue, "N");
						if(isForceFlgSetForSAFTran() == PAAS_TRUE)
						{
							strcpy(szValue, "Y");
						}
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_CHK_CONN_REQD) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						strcpy(szValue, "N");
						if(isCheckConnReqdForSAFTran() == PAAS_TRUE)
						{
							strcpy(szValue, "Y");
						}
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_INDICATOR_REQUIRED) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						strcpy(szValue, "N");
						if(isSAFIndReqdForSSIReq() == PAAS_TRUE)
						{
							strcpy(szValue, "Y");
						}
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_ALLOW_REFUND_TRAN) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						strcpy(szValue, "N");
						if(isSAFAllowedForRefundTran() == PAAS_TRUE)
						{
							strcpy(szValue, "Y");
						}
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_ALLOW_VOID_TRAN) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						strcpy(szValue, "N");
						if(isSAFAllowedForVoidTran() == PAAS_TRUE)
						{
							strcpy(szValue, "Y");
						}
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_ALLOW_GIFT_ACTIVATE) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						strcpy(szValue, "N");
						if(isSAFAllowedForGiftActivate() == PAAS_TRUE)
						{
							strcpy(szValue, "Y");
						}
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_CHK_FLOOR_LIMIT_TO_SAF_REFUND) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						strcpy(szValue, "N");
						if(isFloorLimitChkAllowedForSAFRefund() == PAAS_TRUE)
						{
							strcpy(szValue, "Y");
						}
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, SAF_ALLOW_PAYACCOUNT_TO_SAF) == SUCCESS)
				{
					if(bIsSAFEnabled == PAAS_TRUE)
					{
						strcpy(szValue, "N");
						if(isSAFAllowedForPayAccount() == PAAS_TRUE)
						{
							strcpy(szValue, "Y");
						}
					}
					else
					{
						getEnvFile(SECTION_SAF, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, CREDIT_TYPE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(tenderSetting[TEND_CREDIT] == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, DEBIT_TYPE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(tenderSetting[TEND_DEBIT] == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, GIFTCARD_TYPE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(tenderSetting[TEND_GIFT] == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, GWALLET_TYPE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(tenderSetting[TEND_GOOGLE] == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, IWALLET_TYPE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(tenderSetting[TEND_ISIS] == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, CASH_TYPE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(tenderSetting[TEND_BALANCE] == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, PRIVATE_TYPE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(tenderSetting[TEND_PRIVATE] == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, MERCHCREDIT_TYPE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(tenderSetting[TEND_MERCHCREDIT] == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, PAYPAL_TYPE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(tenderSetting[TEND_PAYPAL] == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, EBT_TYPE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(tenderSetting[TEND_EBT] == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, FSA_TYPE) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(tenderSetting[TEND_FSA] == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, STB_LOGIC_ENABLED) == SUCCESS)
				{
					strcpy(szSection, SECTION_SPINTHEBIN);
					strcpy(szValue, "N");
					if(isSTBLogicEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, DCC_ENABLED) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isDCCEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, STB_SIG_LIMIT) == SUCCESS)
				{
					if(isSTBLogicEnabled())
					{
						sprintf(szValue, "%.2lf", getSTBSignatureLimit());
					}
					else
					{
						getEnvFile(SECTION_SPINTHEBIN, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, STB_PIN_LIMIT) == SUCCESS)
				{
					if(isSTBLogicEnabled())
					{
						sprintf(szValue, "%.2lf", getSTBPINLimit());
					}
					else
					{
						getEnvFile(SECTION_SPINTHEBIN, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, STB_INTERCHANGE_MGMT_LIMIT) == SUCCESS)
				{
					if(isSTBLogicEnabled())
					{
						sprintf(szValue, "%.2lf", getSTBInterchangeMgmtLimit());
					}
					else
					{
						getEnvFile(SECTION_SPINTHEBIN, szParam, szValue, iLen);
					}
				}
				else if(strcasecmp(szParam, "consumer") == SUCCESS)
				{
					getEnvFile(SECTION_CONSUMEROPTS, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, CONSUMER_OPTION1) == SUCCESS)
				{
					getEnvFile(SECTION_CONSUMEROPTS, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, CONSUMER_OPTION2) == SUCCESS)
				{
					getEnvFile(SECTION_CONSUMEROPTS, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, CONSUMER_OPTION3) == SUCCESS)
				{
					getEnvFile(SECTION_CONSUMEROPTS, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, CONSUMER_OPTION4) == SUCCESS)
				{
					getEnvFile(SECTION_CONSUMEROPTS, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, CONSUMER_OPTION5) == SUCCESS)
				{
					getEnvFile(SECTION_CONSUMEROPTS, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, CONSUMER_OPTION6) == SUCCESS)
				{
					getEnvFile(SECTION_CONSUMEROPTS, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "header_1") == SUCCESS)
				{
					getEnvFile(SECTION_RECEIPT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "header_2") == SUCCESS)
				{
					getEnvFile(SECTION_RECEIPT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "header_3") == SUCCESS)
				{
					getEnvFile(SECTION_RECEIPT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "header_4") == SUCCESS)
				{
					getEnvFile(SECTION_RECEIPT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "footer_1") == SUCCESS)
				{
					getEnvFile(SECTION_RECEIPT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "footer_2") == SUCCESS)
				{
					getEnvFile(SECTION_RECEIPT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "footer_3") == SUCCESS)
				{
					getEnvFile(SECTION_RECEIPT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "footer_4") == SUCCESS)
				{
					getEnvFile(SECTION_RECEIPT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "dccdisclaimer_1") == SUCCESS)
				{
					getEnvFile(SECTION_RECEIPT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "dccdisclaimer_2") == SUCCESS)
				{
					getEnvFile(SECTION_RECEIPT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "dccdisclaimer_3") == SUCCESS)
				{
					getEnvFile(SECTION_RECEIPT, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, "dccdisclaimer_4") == SUCCESS)
				{
					getEnvFile(SECTION_RECEIPT, szParam, szValue, iLen);
				}
				else if(((strcmp(szParam, PRIM_HOST_URL) == SUCCESS) || (strcmp(szParam, SCND_HOST_URL) == SUCCESS) || (strcmp(szParam, REG_HOST_URL) == SUCCESS)
						|| (strcmp(szParam, MERCHANT_ID) == SUCCESS) || (strcmp(szParam, TERMINAL_ID) == SUCCESS) || (strcmp(szParam, LANE_ID) == SUCCESS)
						|| (strcmp(szParam, PRIM_CONN_TO) == SUCCESS) || (strcmp(szParam, SCND_CONN_TO) == SUCCESS) || (strcmp(szParam, REG_CONN_TO) == SUCCESS)
						|| (strcmp(szParam, PRIM_RESP_TO) == SUCCESS) || (strcmp(szParam, SCND_RESP_TO) == SUCCESS) || (strcmp(szParam, REG_RESP_TO) == SUCCESS)))
				{
					if(isDHIEnabled())
					{
						memset(szValue, 0x00, sizeof(szValue));
						getEnvFile(SECTION_DHI, PROCESSOR, szValue, iLen);
						if(strlen(szValue) > 0)
						{
							if(strcmp(szValue, "rc") == SUCCESS)
							{
								strcpy(szSection, SECTION_RCHI);
							}
							else if(strcmp(szValue, "cp") == SUCCESS)
							{
								strcpy(szSection, SECTION_CPHI);
							}
							else if(strcmp(szValue, "vantiv") == SUCCESS)
							{
								strcpy(szSection, SECTION_DHI);
							}
							else if(strcasecmp(szValue, "elvn") == SUCCESS)
							{
								strcpy(szSection, SECTION_EHI);
							}
							else
							{
								strcpy(szSection, SECTION_HOST);
							}
						}
						else
						{
							strcpy(szSection, SECTION_HOST);
						}
					}
					else
					{
						strcpy(szSection, SECTION_HOST);
					}
					getEnvFile(szSection, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, PROCESSOR) == SUCCESS)
				{
					getEnvFile(SECTION_DHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, CREDIT_PROCESSOR) == SUCCESS)
				{
					getEnvFile(SECTION_DHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, DEBIT_PROCESSOR) == SUCCESS)
				{
					getEnvFile(SECTION_DHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, GIFT_PROCESSOR) == SUCCESS)
				{
					getEnvFile(SECTION_DHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, BANK_ID) == SUCCESS)
				{
					getEnvFile(SECTION_DHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, DIRECT) == SUCCESS)
				{
					getEnvFile(SECTION_DHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, KEEP_ALIVE) == SUCCESS)
				{
					sprintf(szValue, "%d", getKeepAliveParameter());
				}
				else if(strcasecmp(szParam, KEEP_ALIVE_INTERVAL) == SUCCESS)
				{
					sprintf(szValue, "%d", getKeepAliveInterval());
				}
				else if(strcasecmp(szParam, TPP_ID) == SUCCESS)
				{
					getEnvFile(SECTION_RCHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, MERCHANT_CAT_CODE) == SUCCESS)
				{
					getEnvFile(SECTION_RCHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, TRANSACTION_CURRENCY) == SUCCESS)
				{
					getEnvFile(SECTION_RCHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, GROUP_ID) == SUCCESS)
				{
					getEnvFile(SECTION_RCHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, ADDTL_AMT_CURRENCY) == SUCCESS)
				{
					getEnvFile(SECTION_RCHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, ALTERNATE_MERCH_ID) == SUCCESS)
				{
					getEnvFile(SECTION_RCHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, FD_SERVICE_ID) == SUCCESS)
				{
					getEnvFile(SECTION_RCHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, FD_APP_ID) == SUCCESS)
				{
					getEnvFile(SECTION_RCHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, DOMAIN) == SUCCESS)
				{
					getEnvFile(SECTION_RCHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, BRAND) == SUCCESS)
				{
					getEnvFile(SECTION_RCHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, TOKEN_TYPE) == SUCCESS)
				{
					getEnvFile(SECTION_RCHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, ROUTING_IND) == SUCCESS)
				{
					getEnvFile(SECTION_CPHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, CLIENTNUM) == SUCCESS)
				{
					getEnvFile(SECTION_CPHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, USERNAME) == SUCCESS)
				{
					getEnvFile(SECTION_CPHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, PASSWORD) == SUCCESS)
				{
					getEnvFile(SECTION_CPHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, SW_IDENTIFIER) == SUCCESS)
				{
					getEnvFile(SECTION_CPHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, HW_IDENTIFIER) == SUCCESS)
				{
					getEnvFile(SECTION_CPHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, THIRDPARTYGIFT_ENABLED) == SUCCESS)
				{
					getEnvFile(SECTION_CPHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, CHAIN_CODE) == SUCCESS)
				{
					getEnvFile(SECTION_EHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, LOCATION) == SUCCESS)
				{
					getEnvFile(SECTION_EHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, TERMINAL_CURRENCY) == SUCCESS)
				{
					getEnvFile(SECTION_EHI, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, SAF_ON_RESP_TIMEOUT) == SUCCESS)
				{
					getEnvFile(SECTION_DEVICE, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, UNSOL_MSG_DURING_PYMT_TRAN) == SUCCESS)
				{
					strcpy(szValue, "N");
					if(isUnsolMsgDuringPymtTranEnabled() == PAAS_TRUE)
					{
						strcpy(szValue, "Y");
					}
				}
				else if(strcasecmp(szParam, SWITCH_TO_PRIMURL_AFTER_X_TRAN) == SUCCESS)
				{
					getEnvFile(SECTION_DEVICE, szParam, szValue, iLen);
				}
				else if(strcasecmp(szParam, TOGGLE_URL_AFTER_X_RESP_TIMEOUT) == SUCCESS)
				{
					getEnvFile(SECTION_DEVICE, szParam, szValue, iLen);
				}

				debug_sprintf(szDbgMsg, "%s: Config Parameter [%s] Value [%s]", __FUNCTION__, szParam, szValue);
				APP_TRACE(szDbgMsg);

				sprintf(szParamValue, "%s=%s|", szParam, szValue);

				strcat(szGetParamRespList, szParamValue);
			}

			// Move to Next Given Config Parameter
			if(pszCurrPtr)
			{
				pszStartPtr = pszCurrPtr + 1;
				pszCurrPtr = NULL;
			}
			else
			{
				break;
			}
		}

		if(strlen(szGetParamRespList) > 0)
		{
			// Remove the last appended PIPE while sending the response back to POS.
			pszEndPtr = szGetParamRespList + strlen(szGetParamRespList);
			--pszEndPtr;
			if(pszEndPtr && (*pszEndPtr == '|'))
			{
					*pszEndPtr = '\0';
			}

			strcpy(stGetParmDtls.szGetParmResp, szGetParamRespList);
			rv = updateGetParmDtlsForDevTran(szTranKey, &stGetParmDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update Set Parm Placeholder ", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		break;
	}

	//AjayS2: PTMX 1305: Setting App State to previous
	setAppState(iAppState);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setTimeForDevice
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int setTimeForDevice(char * szTranKey, char * szStatMsg)
{
	int				rv						= SUCCESS;
	int				iStatusMsgDispInterval  = DFLT_STATUS_DISP_TIME;
	int				iAppLogEnabled			= isAppLogEnabled();
	TIMEDTLS_STYPE	stTimeDtls;
	char			szAppLogData[300]		= "";

#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();

	while(1)
	{
		memset(&stTimeDtls, 0x00, sizeof(TIMEDTLS_STYPE));

		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the time placeholder from the transaction instance */
		rv = getTimeDtlsForDevTran(szTranKey, &stTimeDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get time placeholder",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/*Sending 72 command to FA to clear everything before sending XCLOCK*/
		cancelRequest();
		setCardReadEnabled(PAAS_FALSE);

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "Setting the Time [%s] to the Device", stTimeDtls.szTimeTxt);
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		rv = setTime(stTimeDtls.szTimeTxt);
		if(rv == SUCCESS)
		{

#if 0
			/*
			 * Praveen_P1: Removed the UI for the SETTIME commmand
			 */
			/* Show the "Setting date and time...Please wait" message on the device screen */
			showPSGenDispForm(MSG_SETTING_TIME, STANDBY_ICON, iStatusMsgDispInterval);
#endif
			/* update the time details in the transaction instance */
			rv = updateTimeDtlsForDevTran(szTranKey, &stTimeDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update time dtls",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Update the Time");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}

				rv = FAILURE;
				break;
			}
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Time Set to the Device Successfully");
				addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
			}

			sprintf(szStatMsg, "SUCCESS");

			memset(&stTimeDtls, 0x00, sizeof(TIMEDTLS_STYPE));

			/* Now that time is changed we need to update the timers running*/
			rv = resetUIImageUpdationTimer();
			if (rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Could not send the signal to UI thread",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;//Daivik:8 Feb 2016 - Coverity Defect 67228 - Unused rv value is overwritten - break and return with failure once rv!= SUCCESS.
			}

			if(isSAFEnabled()) //Need to reset only if SAF is enabled
			{
				rv = resetSAFPurgingTimer();
				if (rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Could not send the signal to UI thread",
																	__FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break; //Daivik:8 Feb 2016 - Coverity Defect 67228 - Unused rv value is overwritten - break and return with failure once rv!= SUCCESS.
				}
			}
			// Reset the Keep Alive Interval Timer Thread
			rv = resetKAIntervalUpdationTimer();
			if (rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Could not send the signal to KA Interval thread",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;//Daivik:8 Feb 2016 - Coverity Defect 67228 - Unused rv value is overwritten - break and return with failure once rv!= SUCCESS.
			}
		}
		else if(rv == ERR_DEVICE_APP)
		{
			sprintf(szStatMsg, "device application error FA");
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doRebootDevice
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doRebootDevice(char * szTranKey, char * szConnInfo, char *szMacLabel, char * szStatMsg)
{
	int			rv						= SUCCESS;
	int			iStatusMsgDispInterval   = DFLT_STATUS_DISP_TIME;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();

	while(1)
	{
		/*
		 * We send the response back to POS before rebooting
		 * so that POS understands that reboot operation
		 * is in progress
		 */

		/* Set the proper return status for the transaction */
		setStatusInTran(szTranKey, rv);

		strcpy(szStatMsg, "Device Rebooting");

		/* Set the response details for the transaction */
		setGenRespDtlsinTran(szTranKey, szStatMsg);

		/* Send the response to the POS, using the SCI module */
		sendSCIResp(szConnInfo, szTranKey, szMacLabel);

		/* Show the "Device rebooting" message on the device screen */
		showPSGenDispForm(MSG_REBOOT, STANDBY_ICON, iStatusMsgDispInterval);

		//Savelogs before rebooting
		saveLogs();

		//Appcleanup

		debug_sprintf(szDbgMsg, "%s: Device Rebooting...", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//reboot
		svcRestart();
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getApplicationVersionInfo
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getApplicationVersionInfo(char * szTranKey, int *piSameScreen, char * szStatMsg)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	VERDTLS_STYPE	stVerDtls;
	SYSINFO_STYPE	stSysInfo;
	char			szTemp[100];
	char			szAppLogData[300]	= "";
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		strcpy(szAppLogData, "Getting the Application Version Details");
		addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		/* get the Version details from the transaction instance */
		memset(&stVerDtls, 0x00, sizeof(stVerDtls));
		rv = getVerDtlsForDevTran(szTranKey, &stVerDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Version details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed To Get Version Details");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			break;
		}

		memset(&stSysInfo, 0x00, sizeof(SYSINFO_STYPE));
		rv = getAppVersionInfo(&stSysInfo);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get version details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get  Application Version Details");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			break;
		}

		memset(stVerDtls.szVersionInfo, 0x00, sizeof(stVerDtls.szVersionInfo));
		sprintf(stVerDtls.szVersionInfo, "SCA_VER=%s-B%s|UIAGENT_VER=%s", stSysInfo.szSelfVer, stSysInfo.szSelfBuildNum, stSysInfo.szFAVer);

		if(strlen(stSysInfo.szXPIVer) > 0)
		{
			sprintf(szTemp, "|EMVAGENT_VER=%s", stSysInfo.szXPIVer);
			strcat(stVerDtls.szVersionInfo, szTemp);
		}

		if(strlen(stSysInfo.szDHIVer) > 0)
		{
			sprintf(szTemp, "|DHI_VER=%s-B%d", stSysInfo.szDHIVer, stSysInfo.iDHIBuild);
			strcat(stVerDtls.szVersionInfo, szTemp);
		}

		debug_sprintf(szDbgMsg, "%s: Version Info [%s]", __FUNCTION__, stVerDtls.szVersionInfo);
		APP_TRACE(szDbgMsg);

		rv = updateVerDtlsForDevTran(szTranKey, &stVerDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to update version dtls",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Update Version Details");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		sprintf(szStatMsg, "Version Information Captured");
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Application Version Information Captured Successfully");
			addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
		}

		if(stVerDtls.iDisplayFlag == 1)
		{
			debug_sprintf(szDbgMsg, "%s: Need to display system information", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			setAppState(CAPTURING_DEV_VERSION);

			rv = showSystemInfo();
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to show System Information screen at the startup", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			*piSameScreen = 0;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No need to display system information", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			*piSameScreen = 1; //We need to be on the same screen for this command since we are not displaying anything
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doOfflineLtyCapture
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doOfflineLtyCapture(char * szTranKey, char * szStatMsg)
{
	int					rv					= SUCCESS;
	int					iCnt				= 0;
	int					iCardSrc			= 0;
	int					iAppLogEnabled		= isAppLogEnabled();
	char 				szClrExpDate[7] 	= "";
	char *				szTrks[4]			= {NULL, NULL, NULL, NULL};
	char				szAppLogData[300]	= "";
	char 				szBIN[6+1]			= "";
	LTYDTLS_STYPE		stLtyDtls;
	VASDATA_DTLS_STYPE	stVasDataDtls;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(CAPTURING_DEV_LOYALTY);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
#if 0
		/*
		 * Praveen_P1: No need to check config.usr1 for standalone DEVICE commands
		 * like standalone signature, email and loyalty capture
		 * email from Sharvani dated 5 march 2014
		 */
		/* Check if the loyalty capture is even enabled */
		if(PAAS_TRUE != isLoyaltyCapEnabled())
		{
			debug_sprintf(szDbgMsg, "%s: Loyalty capture not enabled",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);

			sprintf(szStatMsg, "Loyalty Capture is DISABLED");
			rv = ERR_UNSUPP_OPR;
			break;
		}
#endif
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Capturing the Loyalty Details From the User");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}

		/* get the loyalty details from the transaction instance */
		memset(&stLtyDtls, 0x00, sizeof(stLtyDtls));
		rv = getLtyDtlsForDevTran(szTranKey, &stLtyDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get loyalty details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(strlen(stLtyDtls.szInputType) > 0)
		{
			debug_sprintf(szDbgMsg, "%s: Input Format Type [%s]", __FUNCTION__, stLtyDtls.szInputType);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Input Format Type is not sent in the request, so default one[%s]", __FUNCTION__, "PHONE");
			APP_TRACE(szDbgMsg);
		}

		memset(szClrExpDate, 0x00, sizeof(szClrExpDate));
		memset(&stVasDataDtls, 0x00, sizeof(VASDATA_DTLS_STYPE));

		/* get the loyalty details entered by the user */
		rv = getLoyaltyDtlsFrmUser(stLtyDtls.szPh, szTrks, szClrExpDate, &iCardSrc, stLtyDtls.szInputType, &stVasDataDtls, isWalletEnabled(), NULL);
		if(rv == SUCCESS)
		{
			if(isWalletEnabled() && stVasDataDtls.bVASPresent)
			{
				rv = updateVASDtlsForDevTran(szTranKey, &stVasDataDtls);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to Update VAS Details", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Failed to Update VAS Details");
						addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
					}
					rv = FAILURE;
					break;
				}

				debug_sprintf(szDbgMsg, "%s: Got VAS data on Loyalty Screen, Need to send to POS", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Captured VAS Loyalty Successfully");
					addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
				}
				break;
			}

			if(strlen(stLtyDtls.szPh) <= 0)
			{
				CARDDTLS_STYPE	stCardDtls;

				/* Need to get the PAN from the track data */
				memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

				/*
				 * Praveen_P1: Setting card type as GIFT for loyalty
				 * so that copying of the PAN from the track would
				 * be fine. Some loyalty cards just have PAN with
				 * no separators, thats why setting card type as
				 * GIFT will take care of those cards also to copy
				 * the PAN
				 */
				/* Saving the card type as Gift card type*/
				stCardDtls.iCardType = GIFT_CARD_TYPE;

				//Copy clear expiry date if present.
				if( strlen(szClrExpDate) > 0)
				{
					//Copy clear expiry dates
					memcpy(stCardDtls.szClrYear, szClrExpDate, 2);
					memcpy(stCardDtls.szClrMon, &szClrExpDate[2], 2);
				}
				rv = getCardDtls(&stCardDtls, szTrks, NULL, NULL, "", PAAS_FALSE, NULL /* Need to send STB data here*/);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get lty card dtls",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Failed to Get the Loyalty Card Details");
						addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
					}

					break;
				}
				/*
				 *Since we are returning the clear text for the loyalty number field
				 *we need to check if this card falls under PCI range,
				 *if so then we should not send the clear field in response
				 */

				memset(szBIN, 0x00, sizeof(szBIN));
				strncpy(szBIN, stCardDtls.szPAN, 6);

				if(checkBinInclusion(szBIN))
				{
					debug_sprintf(szDbgMsg, "%s: Card is Included in PCI Card Range", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					memset(stLtyDtls.szPAN, 0x00, sizeof(stLtyDtls.szPAN));
					getMaskedPAN(stCardDtls.szPAN, stLtyDtls.szPAN); //Masking the PAN before sending
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Card is NOT Included in PCI Card Range", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(stLtyDtls.szPAN, stCardDtls.szPAN);
				}
			}

			/* Update the loyalty details in the transaction instance */
			rv = updateLtyDtlsForDevTran(szTranKey, &stLtyDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update lty dtls",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Update the Loyalty Card Details");
					addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
				}

				rv = FAILURE;
				break;
			}
			sprintf(szStatMsg, "Loyalty Details Captured");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Loyalty Details Captured From User Successfully");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}
		else if(rv == UI_CANCEL_PRESSED)
		{
			debug_sprintf(szDbgMsg, "%s: Cancelled Lty entry", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "User Pressed Cancel Button While Capturing the Loyalty Details");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			disableCardReaders();

			sprintf(szStatMsg, "CANCELLED by Customer");
			rv = ERR_USR_CANCELED;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to capture lty", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			disableCardReaders();
		}

		break;
	}

	for(iCnt = 0; iCnt < 4; iCnt++)
	{
		if(szTrks[iCnt] != NULL)
		{
			free(szTrks[iCnt]);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processProvisionPass
 *
 * Description	: This function will be used Push Data(PROVISION PASS) to Phone.
 *
 * Input Params	: Transaction key, status message
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processProvisionPass(char * szTranKey, char * szStatMsg)
{
	int						rv					= SUCCESS;
//	int						iMerchID			= 0;
	int						iAppLogEnabled		= isAppLogEnabled();
	char					szAppLogData[300]	= "";
	PROVISION_PASS_STYPE	stProvPassDtls;
	VASDATA_DTLS_STYPE		stVasDataDtls;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(PROCESSING_PROVISION_PASS);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Processing the Provision Pass");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		/* get the Provision Pass details from the transaction instance */
		memset(&stProvPassDtls, 0x00, sizeof(PROVISION_PASS_STYPE));
		rv = getProvisionPassDtlsForDevTran(szTranKey, &stProvPassDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Provision Pass details",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Search the nfc.ini file for Merchant Index Based on Merchant ID and Merchant URL*/
//		rv = getMerchantIdfromNfcFile(&iMerchID, &stProvPassDtls);
//		if(rv != SUCCESS)
//		{
//			debug_sprintf(szDbgMsg, "%s: FAILED to get Merchant details from Nfc File", __FUNCTION__);
//			APP_TRACE(szDbgMsg);
//			if(iAppLogEnabled == 1)
//			{
//				strcpy(szAppLogData, "Failed to Get Merchant ID and URL Details from Device");
//				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
//			}
//			break;
//		}
#if 0
		//MERCH Index is not mandatory, No check on Merch Index
		if(strlen(stProvPassDtls.szMerchIndex) > 0 && atoi(stProvPassDtls.szMerchIndex) <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: Merchant Index Value is less than Merchant Index Min value", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invaild Merchant Index Value");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			rv = ERR_INV_FLD;
			break;
		}
#endif
		rv = sendD41ProvisionPass(stProvPassDtls.szMerchIndex, stProvPassDtls.szCustData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to send Provision Pass to Phone", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(rv == UI_CANCEL_PRESSED)
			{
				rv = ERR_USR_CANCELED;
			}
			else
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed To Send Provision Pass to Phone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, NULL);
				}
			}
			break;
		}

		if(rv == SUCCESS && iAppLogEnabled == 1)
		{
			memset(&stVasDataDtls, 0x00, sizeof(VASDATA_DTLS_STYPE));
			stVasDataDtls.bProvisionPassSent = PAAS_TRUE;
			rv = updateVASDtlsForDevTran(szTranKey, &stVasDataDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Update VAS Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Update VAS Details");
					addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
				}
				rv = FAILURE;
				break;
			}
			strcpy(szAppLogData, "Provision Pass Sent To Phone Successfully");
			addAppEventLog(SCA, PAAS_INFO, SENT, szAppLogData, NULL);
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: processQueryNfcIniFile
 *
 * Description	: This function will be used parse the NFC ini file
 *
 * Input Params	: Transaction key, status message
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processQueryNfcIniFile(char * szTranKey)
{
	int							rv					= SUCCESS;
	int							iCount				= 0;
	int							iTotMerchID			= 0;
	int							iAppLogEnabled		= isAppLogEnabled();
	char						szAppLogData[300]	= "";
	char						szKey[256]			= "";
	char*						pszValue			= NULL;
	char* 						pszSectionName		= NULL;
	dictionary *				dict				= NULL;
	NFC_APPLE_SECT_DTLS_PTYPE	pstAppleSecDtlsHead	= NULL;
	NFC_APPLE_SECT_DTLS_PTYPE	pstAppleSecDtlsTail	= NULL;
	NFC_APPLE_SECT_DTLS_PTYPE	pstAppleSecDtlsTemp	= NULL;
	NFCINI_CONTENT_INFO_STYPE	stNFCContents;

#ifdef DEBUG
	char					szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(&stNFCContents, 0x00, sizeof(NFCINI_CONTENT_INFO_STYPE));
		if(doesFileExist(NFC_INI_FILE_NAME) == SUCCESS)
		{
			memset(szAppLogData, 0x00, sizeof(szAppLogData));
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Processing the Query NFC INI");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			dict = iniparser_load(NFC_INI_FILE_NAME);
			if(dict == NULL)
			{
				/* Unable to load the ini file */
				debug_sprintf(szDbgMsg, "%s: Unable to load ini file [%s]", __FUNCTION__, NFC_INI_FILE_NAME);
				APP_TRACE(szDbgMsg);

				rv = ERR_INI_PARSING_FAIL;
				break;
			}

			/*Getting Total Number of Merchant ID*/
			pszValue = iniparser_getstring(dict, "applepay:NUM_MERCHANT_IDS", NULL);
			if(pszValue == NULL)
			{
				/* Unable to load the ini file */
				debug_sprintf(szDbgMsg, "%s: Unable to Get Total Merch IDs from [%s]", __FUNCTION__, NFC_INI_FILE_NAME);
				APP_TRACE(szDbgMsg);

				rv = ERR_INI_PARSING_FAIL;
				break;
			}

			iTotMerchID = atoi(pszValue);
			strncpy(stNFCContents.szApplePayNumSections, pszValue, sizeof(stNFCContents.szApplePayNumSections)-1);

			debug_sprintf(szDbgMsg, "%s: Total Merch IDs Under Apple Pay are [%d]", __FUNCTION__, iTotMerchID);
			APP_TRACE(szDbgMsg);

			for(iCount = 1; iCount <= iTotMerchID; iCount++)
			{
				pstAppleSecDtlsTemp = (NFC_APPLE_SECT_DTLS_PTYPE)malloc(sizeof(NFC_APPLE_SECT_DTLS_STYPE));
				if(pstAppleSecDtlsTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Out Of Memory", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				memset(pstAppleSecDtlsTemp, 0x00, sizeof(NFC_APPLE_SECT_DTLS_STYPE));

				pszSectionName = iniparser_getsecname(dict, iCount);
				if(pszSectionName == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Unable to Section Name [%s]", __FUNCTION__, NFC_INI_FILE_NAME);
					APP_TRACE(szDbgMsg);
					rv = ERR_INI_PARSING_FAIL;
					break;
				}
				strncpy(pstAppleSecDtlsTemp->szSectionName, pszSectionName, sizeof(pstAppleSecDtlsTemp->szSectionName) - 1);

				debug_sprintf(szDbgMsg, "%s: Section Name %s", __FUNCTION__, pszSectionName);
				APP_TRACE(szDbgMsg);

				/*Getting the MERCHANT_INDEX*/
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s", pszSectionName, "MERCHANT_INDEX");
				pszValue = iniparser_getstring(dict, szKey, NULL);
				if(pszValue == NULL)
				{
					/* Unable to load the ini file */
					debug_sprintf(szDbgMsg, "%s: Unable to Get Merch%d Index from [%s]", __FUNCTION__, iCount, NFC_INI_FILE_NAME);
					APP_TRACE(szDbgMsg);
					rv = ERR_INI_PARSING_FAIL;
					break;
				}
				strncpy(pstAppleSecDtlsTemp->szMerchIndex, pszValue, sizeof(pstAppleSecDtlsTemp->szMerchIndex) - 1);

				debug_sprintf(szDbgMsg, "%s: Merchant Index %s", __FUNCTION__, pszValue);
				APP_TRACE(szDbgMsg);

				/*Getting the MERCHANT_ID*/
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s", pszSectionName, "MERCHANT_ID");
				pszValue = iniparser_getstring(dict, szKey, NULL);
				if(pszValue == NULL)
				{
					/* Unable to load the ini file */
					debug_sprintf(szDbgMsg, "%s: Unable to Get Merch%d Id from [%s]", __FUNCTION__, iCount, NFC_INI_FILE_NAME);
					APP_TRACE(szDbgMsg);
					rv = ERR_INI_PARSING_FAIL;
					break;
				}
				strncpy(pstAppleSecDtlsTemp->szMerchID, pszValue, sizeof(pstAppleSecDtlsTemp->szMerchID) - 1);

				debug_sprintf(szDbgMsg, "%s: Merchant ID %s", __FUNCTION__, pszValue);
				APP_TRACE(szDbgMsg);

				/*Getting Merchant_url*/
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s", pszSectionName, "MERCHANT_URL");
				pszValue = iniparser_getstring(dict, szKey, NULL);
				if(pszValue == NULL)
				{
					/* Unable to load the ini file */
					debug_sprintf(szDbgMsg, "%s: Unable to Get Merch%d Url from [%s]", __FUNCTION__, iCount, NFC_INI_FILE_NAME);
					APP_TRACE(szDbgMsg);
					rv = ERR_INI_PARSING_FAIL;
					break;
				}
				strncpy(pstAppleSecDtlsTemp->szMerchURL, pszValue, sizeof(pstAppleSecDtlsTemp->szMerchURL) - 1);

				debug_sprintf(szDbgMsg, "%s: Merchant URL %s", __FUNCTION__, pszValue);
				APP_TRACE(szDbgMsg);

				/*Getting Merchant_url*/
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%s:%s", pszSectionName, "MERCHANT_VAS_FILTER");
				pszValue = iniparser_getstring(dict, szKey, NULL);
				if(pszValue == NULL)
				{
					/* Unable to load the ini file */
					debug_sprintf(szDbgMsg, "%s: Unable to Get Merch%d Vas Filter from [%s]", __FUNCTION__, iCount, NFC_INI_FILE_NAME);
					APP_TRACE(szDbgMsg);
					rv = ERR_INI_PARSING_FAIL;
					break;
				}
				strncpy(pstAppleSecDtlsTemp->szMerchVASFilter, pszValue, sizeof(pstAppleSecDtlsTemp->szMerchVASFilter) - 1);

				debug_sprintf(szDbgMsg, "%s: Merchant Vas Filter %s", __FUNCTION__, pszValue);
				APP_TRACE(szDbgMsg);

				if(pstAppleSecDtlsHead == NULL)
				{
					pstAppleSecDtlsHead = pstAppleSecDtlsTemp;
					pstAppleSecDtlsTail = pstAppleSecDtlsTemp;
				}
				else
				{
					pstAppleSecDtlsTail->next = pstAppleSecDtlsTemp;
					pstAppleSecDtlsTail	= pstAppleSecDtlsTemp;
				}
			}

			if(rv != SUCCESS)
			{
				break;
			}
		}
		else
		{
			rv = ERR_FILE_NOT_FOUND;
		}
		break;
	}

	if(rv != SUCCESS)
	{
		while(pstAppleSecDtlsHead != NULL)
		{
			pstAppleSecDtlsTemp = pstAppleSecDtlsHead;
			pstAppleSecDtlsHead = pstAppleSecDtlsHead -> next;
			free(pstAppleSecDtlsTemp);
		}
	}
	else
	{
		stNFCContents.pstApplePaySectionList = pstAppleSecDtlsHead;

		rv = updateQueryNfcIniDtlsForDevTran(szTranKey, &stNFCContents);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to update NFC dtls for Device Tran",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Update NFC Details for Device Transaction");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			rv = FAILURE;
		}
	}

	if(dict != NULL)
	{
		iniparser_freedict(dict);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}



/* T_AkshayaM1 - As Per FRD.3.75.3 Getting the Card Details for Device Command in which <GET_CARD_DATA> is the Command*/
/*
 * =============================================================
 * Function Name: doGetCardData
 *
 * Description	:This Function will be used to get the Card Details
 *
 * Input Params	: Transaction Key , Status Message
 *
 * Output Params: SUCCESS / FAILURE
 * ==============================================================
 */
int doGetCardData(char * szTranKey, char * szStatMsg)
{
	int 				rv 							= 	SUCCESS;
	int					iAppLogEnabled				= isAppLogEnabled();
	char				szAppLogData[300]			= "";
//	PAAS_BOOL			bFallBack = PAAS_FALSE;
	GET_CARDDATA_STYPE	stGetCardData;

#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	setAppState(PROCESSING_GET_CARD_DATA);

	debug_sprintf(szDbgMsg, "%s: ---- Enter ---- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			/*Daivik:26/8/2016 Coverity Fix:83363 */
			break;
		}

		/* get the Card details from the transaction instance */
		memset(&stGetCardData, 0x00, sizeof(GET_CARDDATA_STYPE));

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Processing the Get Card Data");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		rv = getCardDataDtlsForDevTran(szTranKey, &stGetCardData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Card Details",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get Card Details");
				addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			break;
		}

		/*Capture the Card Details for the Device Command*/
		rv = captureCardDataForDevCmd(szTranKey, szStatMsg, &stGetCardData);
		if(rv == SUCCESS)
		{
			/* Update the Get Card Data details in the transaction instance */
			rv = updateGetCardDataDtlsForDevTran(szTranKey, &stGetCardData);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update Card Data dtls", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Update Card Data Details");
					addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
				}

				rv = FAILURE;
				break;
			}
			sprintf(szStatMsg, "Card Data Received");

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Captured Card Details Successfully From User");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}
		else if(rv == UI_CANCEL_PRESSED)
		{
			debug_sprintf(szDbgMsg, "%s: Cancelled Card data", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "User Pressed Cancel Button While Capturing Card Details");
				addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			sprintf(szStatMsg, "CANCELLED by Customer");
			rv = ERR_USR_CANCELED;
		}
		else if(rv == ERR_BAD_CARD)
		{
			debug_sprintf(szDbgMsg, "%s: Max Card Read Done", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Bad Card Read, Max Card Read Done");
				addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to Capture Card Data",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Capture Card Data");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}

		break;
	}

		debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: doOfflineEmailCapture
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doOfflineEmailCapture(char * szTranKey, char * szStatMsg)
{
	int					rv						= SUCCESS;
	int					iStatusMsgDispInterval  = DFLT_STATUS_DISP_TIME;
	int					iAppLogEnabled			= isAppLogEnabled();
	char				szAppLogData[300]		= "";
	PAAS_BOOL			brv						= PAAS_FALSE;
	LTYDTLS_STYPE		stLtyDtls;
	PROMPTDTLS_STYPE	stPromptDtls;
#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(CAPTURING_DEV_MAIL);
	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();
	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
#if 0
		/*
		 * Praveen_P1: No need to check config.usr1 for standalone DEVICE commands
		 * like standalone signature, email and loyalty capture
		 * email from Sharvani dated 5 march 2014
		 */
		/* Check if email capture is allowed or not on the device */
		if(PAAS_TRUE != isEmailCapEnabled())
		{
			debug_sprintf(szDbgMsg, "%s: Email capture is disabled",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = ERR_UNSUPP_OPR;

			sprintf(szStatMsg, "Email capture is disabled");
			break;
		}
#endif
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Capturing the Email Details From the User");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}

		/* get the loyalty details from the transaction instance */
		memset(&stLtyDtls, 0x00, sizeof(stLtyDtls));
		rv = getLtyDtlsForDevTran(szTranKey, &stLtyDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get loyalty details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		while(1)
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Showing Email Capture Screen");
				addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
			}

			memset(&stPromptDtls, 0x00, sizeof(PROMPTDTLS_STYPE));
			strcpy(stPromptDtls.szPromptData, stLtyDtls.szEmail);
			stPromptDtls.iPromptMinChars = 1;
			stPromptDtls.iPromptMaxChars = 40;
			stPromptDtls.iTitle = ENTER_EMAIL_TITLE;

			CHECK_POS_INITIATED_STATE; // Daivik:18/8/2016 - Check for Cancellation/Disconnection before we send the Email Capture Request.
			/* Capture the email by prompting the user on the device screen */
			rv = getStringFromUser(&stPromptDtls);

			if(rv == SUCCESS)
			{
				/*
				 * Need to do email address verification
				 */
				strcpy(stLtyDtls.szEmail, stPromptDtls.szPromptData);
				brv = isEmailValid(stLtyDtls.szEmail);
				if(brv == PAAS_FALSE)
				{
					debug_sprintf(szDbgMsg, "%s: Entered invalid email address",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Entered Invalid Email Address");
						addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
					}

					showPSGenDispForm(MSG_INVALID_EMAIL, FAILURE_ICON, iStatusMsgDispInterval);

					continue; //Need to capture email address again
				}
				else
				{
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Entered Email Address is valid");
						addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
					}
				}
				/* Update the loyalty details in the transaction instance */
				rv = updateLtyDtlsForDevTran(szTranKey, &stLtyDtls);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to update lty dtls",
																	__FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Failed to Update Loyalty Details");
						addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
					}

					rv = FAILURE;
					break;
				}
				sprintf(szStatMsg, "Email Captured");
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Email Address Captured From User Successfully");
					addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
				}
			}
			else if(rv == UI_CANCEL_PRESSED)
			{
				debug_sprintf(szDbgMsg, "%s: Cancelled email entry", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "User Pressed Cancel Button on Email Capture Screen");
					addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
				}

				sprintf(szStatMsg, "CANCELLED by Customer");
				rv = ERR_USR_CANCELED;
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to capture email",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Capture Email Address");
					addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
				}
			}
			break;
		}//end of while loop to capture email i.e. second while loop

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doCustQuestion
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doCustQuestion(char * szTranKey, char * szStatMsg, int *piSameScreen, int *locCurrForm)
{
	int					rv					= SUCCESS;
	int					iAppLogEnabled		= isAppLogEnabled();
	SURVEYDTLS_STYPE	stSurveyDtls;
	char				szAppLogData[300]	= "";
#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(CAPTURING_DEV_CUST_QUESTION);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Capturing Customer Question From User");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}

		/* get the Survey details from the transaction instance */
		memset(&stSurveyDtls, 0x00, sizeof(stSurveyDtls));
		rv = getSurveyDtlsForDevTran(szTranKey, &stSurveyDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Survey details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Capture the Cust Question by prompting the user on the device screen */
		rv = captureCustQuestion(&stSurveyDtls);
		if(rv == SUCCESS)
		{
			/* Update the survey details in the transaction instance */
			rv = updateSurveyDtlsForDevTran(szTranKey, &stSurveyDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update lty dtls",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			sprintf(szStatMsg, "Customer Question Captured");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Customer Question Captured Successfully From User");
				addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}
		else if(rv == UI_CANCEL_PRESSED)
		{
			debug_sprintf(szDbgMsg, "%s: Cancelled Customer Question entry", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "User Pressed Cancel Button While Capturing Customer Question");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			sprintf(szStatMsg, "CANCELLED by Customer");
			rv = ERR_USR_CANCELED;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to capture Cust Question",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Capture Customer Question");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}
		if(stSurveyDtls.iRetScreen == RET_SCREEN_IDLE)
		{
			*locCurrForm = IDLE_DISP_FRM;
		}
		else if(stSurveyDtls.iRetScreen == RET_SCREEN_LI)
		{
			*locCurrForm = LI_DISP_FRM;
		}
		else if(stSurveyDtls.iRetScreen == RET_SCREEN_WELCOME)
		{
			*locCurrForm = WELCOME_DISP_FRM;
		}
		else if(stSurveyDtls.iRetScreen == RET_SCREEN_STAY_CURRENT)
		{
			*piSameScreen = 1;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doEmpIDCapture
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doEmpIDCapture(char * szTranKey, char * szStatMsg)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	EMPID_STYPE    	stEmpIDDtls;
	char			szAppLogData[300]	= "";
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(&stEmpIDDtls, 0x00, sizeof(EMPID_STYPE));

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Capturing Employee Id From User");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}
		/* Capture the Employee ID by prompting the user on the device screen */
		rv = captureEmpID(stEmpIDDtls.szEmpID);
		if(rv == UI_ENTER_PRESSED)
		{
			/* Update the employee details in the transaction instance */
			rv = updateEmpIDDtlsForDevTran(szTranKey, &stEmpIDDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update Employee ID dtls",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			sprintf(szStatMsg, "Employee ID Captured");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Employee Id Captured Successfully From User");
				addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}
		else if(rv == UI_CANCEL_PRESSED)
		{
			debug_sprintf(szDbgMsg, "%s: Cancelled Employee ID entry", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "User Pressed Cancel Button While Capturing Employee Id");
				addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			sprintf(szStatMsg, "CANCELLED by Customer");
			rv = ERR_USR_CANCELED;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to capture Employee ID",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Capture Employee Id From User");
				addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: doCharity
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doCharity(char * szTranKey, char * szStatMsg)
{
	int					rv					= SUCCESS;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[300]	= "";
	CHARITYDTLS_STYPE	stCharityDtls;
#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(CAPTURING_DEV_CHARITY);
	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Capturing Charity Details From User");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}

		/* get the Charity details from the transaction instance */
		memset(&stCharityDtls, 0x00, sizeof(CHARITYDTLS_STYPE));
		rv = getCharityDtlsForDevTran(szTranKey, &stCharityDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Charity details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get Charity Details");
				addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			break;
		}

		/* Capture the Charity by prompting the user on the device screen */
		rv = captureCharity(&stCharityDtls);
		if(rv == SUCCESS)
		{
			/* Update the charity details in the transaction instance */
			rv = updateCharityDtlsForDevTran(szTranKey, &stCharityDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update charity dtls",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Update Charity Details");
					addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
				}

				rv = FAILURE;
				break;
			}
			sprintf(szStatMsg, "Charity Captured");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Charity Details Captured Successfully From User");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}
		else if(rv == UI_CANCEL_PRESSED)
		{
			debug_sprintf(szDbgMsg, "%s: Cancelled Charity entry", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "User Pressed Cancel Button While Capturing Charity Details");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			sprintf(szStatMsg, "CANCELLED by Customer");
			rv = ERR_USR_CANCELED;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to capture charity",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCharInputEntryCount
 *
 * Description	:
 *
 * Input Params	: This function will calculate the total input entries that user can make in the format string
 * (X, N) required for validation in the credit app xml Request.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getCharInputEntryCount(char *frmStr)
{
	int iCharCnt = 0;
	int i		 = 0;

	for(i = 0; (frmStr) && (frmStr[i] != '\0'); i++)
	{
		if(frmStr[i] == 'N' || frmStr[i] == 'X')
		{
			iCharCnt++;
		}
	}

	return iCharCnt;
}

/*
 * ============================================================================
 * Function Name: validateXMLReqWithFormatStrForAllPrompts
 *
 * Description	:
 *
 * Input Params	: This function will validate the min and max chars entry of credit Application xml req with the
 * format string of the prompts present in the request.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int validateXMLReqWithFormatStrForAllPrompts(CREDITAPPDTLS_PTYPE pstCreditAppDtls, char *szStatMsg)
{
	int			rv 					= SUCCESS;
	int			iPromptCnt			= 0;
	int 		iPromptLen 			= 0;
	int 		iFrmStrLen 			= 0;
	int 		iMinEntryLen 		= 0;
	int 		iMaxEntryLen 		= 0;
	int			iPromptType			= 0;
	int			iCharEntryCnt		= 0;
	int 		iPlatForm 			= -1;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
	char		szAppLogDiag[300]	= "";
	char *		frmtStr				= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iPlatForm = getDevicePlatform();

	for(iPromptCnt = CREDIT_APP_PROMPT_1; (iPromptCnt < CREDIT_APP_MAXPROMPTS) && (strlen(pstCreditAppDtls->stPromptDtls[iPromptCnt].szPromptText)); iPromptCnt++)
	{
		iPromptLen = strlen(pstCreditAppDtls->stPromptDtls[iPromptCnt].szPromptText);
		iMinEntryLen = pstCreditAppDtls->stPromptDtls[iPromptCnt].iPromptMinChars;
		iMaxEntryLen = pstCreditAppDtls->stPromptDtls[iPromptCnt].iPromptMaxChars;
		iPromptType = pstCreditAppDtls->stPromptDtls[iPromptCnt].iPromptType;
		frmtStr = pstCreditAppDtls->stPromptDtls[iPromptCnt].szPromptFormat;

		/* Daivik: 4/2/2016 - Coverity 67358 - Since szPromptFormat is an array frmStr cannot be NULL */
#if 0
		if(frmtStr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Format String is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
#endif
		iFrmStrLen = strlen(frmtStr);

		iCharEntryCnt = getCharInputEntryCount(frmtStr);

		if( ((iPromptType == PROMPTTYPE_QWERTY) || (iPromptType == PROMPTTYPE_QWERTY_SWIPE)) && ( (iPlatForm == MODEL_MX915 && iPromptLen > 40) ||  (iPlatForm == MODEL_MX925 && iPromptLen > 140) ) )
		{
			strcpy(szStatMsg, "PROMPT Length Exceeds the Range");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid XML Field Value, Length of PROMPT Field Exceeds the Range.");
				strcpy(szAppLogDiag, "Please Check the PROMPT Field in the XML Request");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}
			rv = ERR_INV_FLD_VAL;
			break;
		}
		else if( ((iPromptType == PROMPTTYPE_NUMERICS) || (iPromptType == PROMPTTYPE_DOLLAR)) && ( (iPlatForm == MODEL_MX915 && iPromptLen > 190) ||  (iPlatForm == MODEL_MX925 && iPromptLen > 280)) )
		{
			strcpy(szStatMsg, "PROMPT Length Exceeds the Range");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid XML Field Value, Length of PROMPT Field Exceeds the Range.");
				strcpy(szAppLogDiag, "Please Check the PROMPT Field in the XML Request");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}
			rv = ERR_INV_FLD_VAL;
			break;
		}

		if((iFrmStrLen) && ((iPromptType == PROMPTTYPE_NUMERICS) || (iPromptType == PROMPTTYPE_DOLLAR)))
		{
			if(iFrmStrLen > 25)
			{
				strcpy(szStatMsg, "PROMPT_FORMAT Length Exceeds the Range");
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Invalid XML Field Value, Length of PROMPT_FORMAT Exceeds the Range.");
					strcpy(szAppLogDiag, "Please Check the PROMPT_FORMAT in the XML Request");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				rv = ERR_INV_FLD_VAL;
				break;
			}

			if(strspn(frmtStr, "N-/\\$.()#% @") != iFrmStrLen)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid characters in the format string of Numeric/Dollar Prompt Entry [%s]",
															__FUNCTION__, frmtStr);
				APP_TRACE(szDbgMsg);

				strcpy(szStatMsg, "PROMPT_FORMAT Contains Invalid Characters");
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Invalid XML Field Value, PROMPT_FORMAT Contains Invalid Characters.");
					strcpy(szAppLogDiag, "Please Check the PROMPT_FORMAT in the XML Request");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				rv = ERR_INV_FLD_VAL;
				break;
			}
		}

		if((pstCreditAppDtls->stPromptDtls[iPromptCnt].iPromptType ==  PROMPTTYPE_NUMERICS)
				|| (pstCreditAppDtls->stPromptDtls[iPromptCnt].iPromptType ==  PROMPTTYPE_DOLLAR))
		{
			if(iMinEntryLen > 25)
			{
				strcpy(szStatMsg, "PROMPT_MIN Characters Entry is Not in Range");
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Invalid XML Field Value, PROMPT_MIN Characters Entry Range Exceeded.");
					strcpy(szAppLogDiag, "Please Check the PROMPT_MIN Characters in the XML Request");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				rv = ERR_INV_FLD_VAL;
				break;
			}

			if(iMaxEntryLen > 25)
			{
				strcpy(szStatMsg, "PROMPT_MAX Characters Entry is Not in Range");
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Invalid XML Field Value, PROMPT_MAX Characters Entry Range Exceeded.");
					strcpy(szAppLogDiag, "Please Check the PROMPT_MAX Characters in the XML Request");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				rv = ERR_INV_FLD_VAL;
				break;
			}
		}

		/* If the prompt minimum chars in the xml request is greater than length of format string
		 * or If the prompt maximum chars in the xml request is lesser than length of format string
		 * or If the prompt minimum chars is greater than prompt maximum chars then
		 * throw an error that the xml field is error.
		 * These cannot be validated at the beginning of the req parsing */

		if(iMinEntryLen && iCharEntryCnt && (iMinEntryLen > iCharEntryCnt))
		{
			strcpy(szStatMsg, "Length of PROMPT_FORMAT is Less than PROMPT_MIN");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid XML Field Value, PROMPT_MIN Characters Cannot be greater than Length of PROMPT_FORMAT.");
				strcpy(szAppLogDiag, "Please Check the PROMPT_MIN Characters in the XML Request");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}
			rv = ERR_INV_FLD_VAL;
			break;
		}

		if(iMaxEntryLen && iCharEntryCnt && (iMaxEntryLen < iCharEntryCnt))
		{
			strcpy(szStatMsg, "Length of PROMPT_FORMAT is Greater than PROMPT_MAX");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid XML Field Value, PROMPT_MAX Characters Cannot be Less than Length of PROMPT_FORMAT.");
				strcpy(szAppLogDiag, "Please Check the Prompt Maximum Characters in the XML Request");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}
			rv = ERR_INV_FLD_VAL;
			break;
		}

		if(iMinEntryLen && iMaxEntryLen && (iMinEntryLen > iMaxEntryLen))
		{
			strcpy(szStatMsg, "PROMPT_MIN is Greater than PROMPT_MAX");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid XML Field Value, PROMPT_MIN Characters Cannot be greater than PROMPT_MAX Characters.");
				strcpy(szAppLogDiag, "Please Check the PROMPT_MIN and PROMPT_MAX Characters in the XML Request");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}
			rv = ERR_INV_FLD_VAL;
			break;
		}
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning rv[%d] ---", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doCreditApplication
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doCreditApplication(char * szTranKey, char * szStatMsg, int *piSameScreen, int *locCurrForm)
{
	int					   	rv 						= SUCCESS;
	int					   	iProxyRetStatus			= SUCCESS;
	int						iPromptCnt				= CREDIT_APP_PROMPT_1;
	int						iAppLogEnabled			= isAppLogEnabled();
	int						iStatusMsgDispInterval	= DFLT_STATUS_DISP_TIME;
	char					szAppLogData[300]		= "";
	char					szAppLogDiag[300]		= "";
	char					szSSITranKey[30]		= "";
	char					szProxyResult[30]		= "";
	char					szProxyRef[30]			= "";
	char					szCreditAppPayLoad[101]	= "";
	TRAN_PTYPE				pstTran					= NULL;
	DEVTRAN_PTYPE			pstDevTran				= NULL;
	CREDITAPPDTLS_STYPE		stCreditAppDtls;
#ifdef DEBUG
	char					szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(CAPTURING_CREDIT_APP);

	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();
	// CID-67309: 29-Jan-16: MukeshS3: Initialiaing at beginning  as it is being used outside of while loop also
	memset(&stCreditAppDtls, 0x00, sizeof(CREDITAPPDTLS_STYPE));

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Capturing the Credit Application Details From User");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}

		/* get the credit Application details from the transaction instance */
		// CID-67309: 29-Jan-16: MukeshS3: Initialiaing at beginning  as it is being used outside of while loop also
		//memset(&stCreditAppDtls, 0x00, sizeof(CREDITAPPDTLS_STYPE));
		rv = getCreditAppDtlsForDevTran(szTranKey, &stCreditAppDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Credit Application Details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Credit Application Details From User");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			return rv;
		}

		rv = validateXMLReqWithFormatStrForAllPrompts(&stCreditAppDtls, szStatMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Validation of Minimum Prompt Data Entry With Format String Failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		for(iPromptCnt = CREDIT_APP_PROMPT_1; ((iPromptCnt < CREDIT_APP_MAXPROMPTS) && (iPromptCnt >= CREDIT_APP_PROMPT_1)); )
		{
			if((strlen(stCreditAppDtls.stPromptDtls[iPromptCnt].szPromptText)) > 0)
			{
				rv = captureCreditAppDetails(&stCreditAppDtls.stPromptDtls[iPromptCnt], iPromptCnt);
				if(rv == SUCCESS)
				{
					sprintf(szStatMsg, "Credit Application Prompt Captured");
					if(iAppLogEnabled == 1)
					{
						sprintf(szAppLogData, "Credit Application Prompt No[%d] [%s] Details Successfully Captured From User", iPromptCnt+1, stCreditAppDtls.stPromptDtls[iPromptCnt].szPromptText);
						addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
					}
				}
				else if(rv == UI_CANCEL_PRESSED)
				{
					if(iPromptCnt == CREDIT_APP_PROMPT_1)
					{
						debug_sprintf(szDbgMsg, "%s: Cancelled Credit Application Request", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						sprintf(szStatMsg, "CANCELLED by Customer");
						if(iAppLogEnabled == 1)
						{
							sprintf(szAppLogData, "User Pressed Cancel Button While Capturing Prompt No[%d] [%s] Credit Application Details", iPromptCnt+1, stCreditAppDtls.stPromptDtls[iPromptCnt].szPromptText);
							addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
						}
						rv = ERR_USR_CANCELED;
						break;
					}
					else
					{
						do
						{
							iPromptCnt--;
						}while((!strlen(stCreditAppDtls.stPromptDtls[iPromptCnt].szPromptText)) && (iPromptCnt >= CREDIT_APP_PROMPT_1));
						continue;
					}
				}
				else if(rv == ERR_CARD_INVALID)
				{
					debug_sprintf(szDbgMsg, "%s: Card Data Does not Contain Track1 to Fetch Customer Name", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					sprintf(szStatMsg, "Card Holder Name Not Available");
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Card Holder Name is Missing in the Track/Card Data. Please Check the Card.");
						addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
					}
					rv = ERR_CARD_INVALID;
					break;
				}
				else if(rv == ERR_BAD_CARD)
				{
					debug_sprintf(szDbgMsg, "%s: Bad Card Swiped, ", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Failed to Capture Credit Application Details From User. Bad Card Swipe Noticed.");
						strcpy(szAppLogDiag, "Please Swipe The Card Properly or Swipe Other Card.");
						addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
					}
					break;
				}
				else
				{
					debug_sprintf(szDbgMsg,"%s: FAILED to Capture Credit Application",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Failed to Capture Credit Application Details From User.");
						strcpy(szAppLogDiag, "Device Application Internal Error, Please Contact Verifone.");
						addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
					}
					break;
				}
			}
			iPromptCnt++;
		}

		if(rv == SUCCESS)
		{
			/* Update the Credit Application Prompt Details in the transaction instance */
			rv = updateCreditAppPromptDtlsForDevTran(szTranKey, &stCreditAppDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update Credit Application Prompt Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Update Credit Application Prompt Details");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}

				rv = FAILURE;
				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: rv Not Success - Capturing Failed rv[%d] ", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Incase the Forward proxy is present in the credit application request, we forward
		 * to the proxy and get the response and in turn send it back to POS
		 *
		 * If incase the forward proxy is not present to send the Captured data from user alone back to POS.
		 * Pusing it as a new ssi transaction and sending the request
		 */

		if(strlen(stCreditAppDtls.szForwardProxy) > 0)
		{
			/* Push a new SSI transaction in the stack */
			rv = pushNewSSITran(szSSITranKey);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to add a new SSI tran",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			/* Set the function and command as DEVICE and CREDITAPP respectively for the
			 * Credit app transaction with proxy*/
			setFxnNCmdForSSITran(szSSITranKey, SSI_DEV, SSI_CREDITAPP);

			rv = getDevTranDtls(szTranKey, &pstDevTran);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get dev tran details ", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			/* Get the transaction reference */
			rv = updateDevTranDtls(szSSITranKey, pstDevTran);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			/* Showing Stand by Status in the GEN_FORM */
			showPSGenDispForm(MSG_PROCESSING, STANDBY_ICON, 0);

			debug_sprintf(szDbgMsg, "%s: POST the Credit Application request to the SSI host", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			CHECK_POS_INITIATED_STATE;

			setAppState(POSTING_PAYMENT);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Framing the SSI Request For Credit Application Command");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			/* Send the data to the host and try getting the response */
			rv = doProxyCommunication(szSSITranKey, szStatMsg);

			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to communicate with the Proxy rv[%d]", __FUNCTION__, rv);
				APP_TRACE(szDbgMsg);

				/* Error while getting response from proxy */
				showPSGenDispForm(MSG_ERROR, FAILURE_ICON, iStatusMsgDispInterval);

				if(rv == ERR_RESP_TO)
				{
					/* setting the status message which will be sent as response text to POS*/
					strcpy(szStatMsg, "Proxy Response Timeout");
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Proxy Response Timeout, Failed to Get Response From PROXY");
						addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
					}
					iProxyRetStatus = ERR_PROXY_RESP_TO;
				}
				else
				{
					/* setting the status message which will be sent as response text to POS*/
					strcpy(szStatMsg, "Communication Error With Proxy");
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Failed to Communicate with PROXY, SSI PROXY Connection Failure");
						addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
					}
					iProxyRetStatus = ERR_PROXY_CONN_FAIL;
				}
			}

			rv = getTopSSITran(szSSITranKey, &pstTran);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get tran data",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* setting the status message which will be sent as response text to POS*/
				strcpy(szStatMsg, "Device Application Internal Error");
				break;
			}

			strcpy(szProxyRef, (((DEVTRAN_PTYPE)pstTran->data)->stCreditAppDtls.szProxyRef));
			strcpy(szProxyResult, (((DEVTRAN_PTYPE)pstTran->data)->stCreditAppDtls.szProxyResult));
			strcpy(szCreditAppPayLoad, (((DEVTRAN_PTYPE)pstTran->data)->stCreditAppDtls.szCreditAppPayLoad));

			if(strcmp(szProxyResult, "SUCCESS") == SUCCESS)
			{
				/* Got Success Response from Proxy */
				showPSGenDispForm(MSG_CAPTURED, SUCCESS_ICON, iStatusMsgDispInterval);
			}
			else if (strcmp(szProxyResult, "FAILURE") == SUCCESS)
			{
				/* Got Failure Response from Proxy */
				showPSGenDispForm(MSG_ERROR, FAILURE_ICON, iStatusMsgDispInterval);
			}

			/*pop and free the SSI transaction key instance data*/
			popSSITran(szSSITranKey);
			deleteStack(szSSITranKey);

			/* Get the transaction reference and fill the required credit app structure fields */
			rv = getTran(szTranKey, &pstTran);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			/* Copy the Response structure of device transaction instance key here itself, for Conn Failure with
			 * Proxy. We fill here only because even incase of Conn Failure we send rv as success only
			 * so that the credit app details will be filled when we send back the prompt data to POS */
			if(iProxyRetStatus == ERR_PROXY_CONN_FAIL)
			{
				// Updating the response details
				strcpy(szProxyResult, "CONNECTION ERROR");
				strcpy(pstTran->stRespDtls.szRsltCode, "59025");
				strcpy(pstTran->stRespDtls.szRespTxt, "Failed to Communicate With Proxy");
				strcpy(pstTran->stRespDtls.szRslt, "COMM_ERROR");
				strcpy(pstTran->stRespDtls.szTermStat, "FAILURE");
			}
			else if(iProxyRetStatus == ERR_PROXY_RESP_TO)
			{
				// Updating the response details
				strcpy(szProxyResult, "TIMEOUT");
				strcpy(pstTran->stRespDtls.szRsltCode, "59026");
				strcpy(pstTran->stRespDtls.szRespTxt, "Proxy Response Timeout");
				strcpy(pstTran->stRespDtls.szRslt, "COMM_ERROR");
				strcpy(pstTran->stRespDtls.szTermStat, "FAILURE");
			}

			/* Fill the device Transaction key instance credit app details with the ssi transaction
			 * key instance credit app details obtained after communicating with proxy*/

			strcpy(((DEVTRAN_PTYPE)pstTran->data)->stCreditAppDtls.szProxyResult, szProxyResult);
			strcpy(((DEVTRAN_PTYPE)pstTran->data)->stCreditAppDtls.szProxyRef, szProxyRef);
			strcpy(((DEVTRAN_PTYPE)pstTran->data)->stCreditAppDtls.szCreditAppPayLoad, szCreditAppPayLoad);

		}
		break;
	}

	if(stCreditAppDtls.iRetScreen == RET_SCREEN_IDLE)
	{
		*locCurrForm = IDLE_DISP_FRM;
	}
	else if(stCreditAppDtls.iRetScreen == RET_SCREEN_LI)
	{
		*locCurrForm = LI_DISP_FRM;
	}
	else if(stCreditAppDtls.iRetScreen == RET_SCREEN_WELCOME)
	{
		*locCurrForm = WELCOME_DISP_FRM;
	}
	else if(stCreditAppDtls.iRetScreen == RET_SCREEN_STAY_CURRENT)
	{
		*piSameScreen = 1;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doCustomerButtonOption
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doCustomerButtonOption(char * szTranKey, char * szStatMsg)
{
	int					    rv 					= SUCCESS;
	int						iAppLogEnabled		= isAppLogEnabled();
	char					szAppLogData[300]	= "";
	CUSTBUTTONDTLS_STYPE	stCustButtDtls;
#ifdef DEBUG
	char					szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(CAPTURING_DEV_CUST_BUTTON);
	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Capturing the Customer Button Details From User");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}
		/* get the customer button from the transaction instance */
		memset(&stCustButtDtls, 0x00, sizeof(CUSTBUTTONDTLS_STYPE));
		rv = getCustButtonDtlsForDevTran(szTranKey, &stCustButtDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get customer Button details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Customer Button Details From User");
				addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			break;
		}

		/* Capture the customer Button by prompting the user on the device screen */
		rv = captureCustButton(&stCustButtDtls);
		if(rv == SUCCESS)
		{
			/* Update the customer Button details in the transaction instance */
			rv = updateCustButtonDtlsForDevTran(szTranKey, &stCustButtDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update customer Button dtls",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Update Customer Button Details");
					addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
				}

				rv = FAILURE;
				break;
			}
			sprintf(szStatMsg, "Customer Button Captured");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Customer Button Details Successfully Captured From User");
				addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}
		else if(rv == UI_CANCEL_PRESSED)
		{
			debug_sprintf(szDbgMsg, "%s: Cancelled customer Button entry", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			sprintf(szStatMsg, "CANCELLED by Customer");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "User pressed Cancel Button While Capturing Customer Button Details");
				addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
			}
			rv = ERR_USR_CANCELED;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to capture customer Button",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Capture Customer Button Details From User");
				addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doSurvey
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doSurvey(char * szTranKey, int iSurveyOption, char * szStatMsg)
{
	int					rv					= SUCCESS;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[300]	= "";
	SURVEYDTLS_STYPE	stSurveyDtls;
#ifdef DEBUG
	char				szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	setAppState(CAPTURING_DEV_CUST_SURVEY);
	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Capturing Customer Survey From User");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}
		/* get the Survey details from the transaction instance */
		memset(&stSurveyDtls, 0x00, sizeof(stSurveyDtls));
		rv = getSurveyDtlsForDevTran(szTranKey, &stSurveyDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Survey details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Capture the survey by prompting the user on the device screen */
		rv = captureSurvey(&stSurveyDtls, iSurveyOption);
		if(rv == SUCCESS)
		{
			/* Update the survey details in the transaction instance */
			rv = updateSurveyDtlsForDevTran(szTranKey, &stSurveyDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update lty dtls",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Survey Details Captured Successfully From User");
				addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
			}
			sprintf(szStatMsg, "Survey Captured");
		}
		else if(rv == UI_CANCEL_PRESSED)
		{
			debug_sprintf(szDbgMsg, "%s: Cancelled survey entry", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "User Pressed Cancel Button While Capturing Survey Details");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			sprintf(szStatMsg, "CANCELLED by Customer");
			rv = ERR_USR_CANCELED;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to capture survey",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Capture Survey Details From User");
				addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: doSAFQuery
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doSAFQuery(char * szTranKey, char * szStatMsg)
{
	int			  	rv					= SUCCESS;
	int			  	rvTemp				= SUCCESS;
	int			  	iRetVal				= SUCCESS;
	int				iCurAppState		= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";
	SAFDTLS_STYPE 	stSAFDtls;
	PAAS_BOOL		bTOR 				= PAAS_TRUE;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iCurAppState = getAppState();

	setAppState(SAF_QUERY);
	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Querying the SAF for Details");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}
		/* Get the SAF Request details from the transaction instance */
		memset(&stSAFDtls, 0x00, sizeof(SAFDTLS_STYPE));
		rv = getSAFDtlsForDevTran(szTranKey, &stSAFDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SAF Details placeholder", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to get SAF Details");
				addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			rv = FAILURE;
			strcpy(szStatMsg, "Device Application Internal Error");
			break;
		}

		/*Need to find out what type of query:
			Status
			RANGE
			SINGLE
		 */
		rv = getSAFRequestType(&stSAFDtls);

		iRetVal = getSAFTORRequestType(&stSAFDtls);

		if(rv == FAILURE || iRetVal == FAILURE)
		{
			debug_sprintf(szDbgMsg, "%s: Error while getting the SAF Request type!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strcpy(szStatMsg, "Device Application Internal Error");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error While Getting the SAF Request Type.");
				strcpy(szAppLogDiag, "Device Application Internal Error");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}
			return ERR_DEVICE_APP;
		}

		debug_sprintf(szDbgMsg, "%s: SAF Request Type [%d]", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);
		bTOR = PAAS_FALSE;
		switch(rv)
		{
		case SAF_REQ_STATUS:
			debug_sprintf(szDbgMsg, "%s: Need to give information of the Queried SAF records that are filtered by the given status", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = getSAFDtlsByRecStatus(&stSAFDtls, bTOR);
			if(rv == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF Status and FOUND the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(rv == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF Status and Did NOT find the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while Querying the SAF Records Based on the Given SAF Status", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;
		case SAF_REQ_STATUS_SINGLE:
			debug_sprintf(szDbgMsg, "%s: Need to give information of the Queried SAF records that are filtered by the given status and the Record Number", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = getSAFDtlsByRecStatusCumRecNum(&stSAFDtls, bTOR);
			if(rv == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF Status and FOUND the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(rv == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF Status and did NOT find the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while Querying the SAF Records Based on the Given SAF Status", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		case SAF_REQ_STATUS_RANGE:
			debug_sprintf(szDbgMsg, "%s: Need to give information of the Queried SAF Records that are Filtered by the Given Status and the Range of Record Numbers", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = getSAFDtlsByRecStatusCumRange(&stSAFDtls, bTOR);
			if(rv == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF status and FOUND the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(rv == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF status and Did NOT find the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while Querying the SAF Records Based on the Given SAF Status", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		case SAF_REQ_RANGE:
			debug_sprintf(szDbgMsg, "%s: Need to give information of the queried SAF records in the given Range", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = getSAFDtlsByRecRange(&stSAFDtls, bTOR);
			if(rv == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF Record Number Range and FOUND the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(rv == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records based on the Given SAF Record Number Range and Did NOT Find the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while Querying the SAF Records Based on the Given SAF Status", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		case SAF_REQ_SINGLE:
			debug_sprintf(szDbgMsg, "%s: Need to give information of the queried single SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = getSAFDtlsByRecNum(&stSAFDtls, bTOR);
			if(rv == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF records Based on the Given SAF Record Number and FOUND the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(rv == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF records Based on the Given SAF Record Number and Did NOT find the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while Querying the SAF Records Based on the Given SAF Record Number", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		case SAF_REQ_ENTIRE:
			debug_sprintf(szDbgMsg, "%s: Need to give information of all SAF records", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = getSAFDtlsAll(&stSAFDtls, bTOR);
			if(rv == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the ALL SAF Records and FOUND the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(rv == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried ALL the SAF Records and Did NOT Find the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error While Querying ALL the SAF Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		case SAF_REQ_NOTHING:
			debug_sprintf(szDbgMsg, "%s: Need to give information of all SAF records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SAF_RECORD_NOT_FOUND;
			break;
		default: //FIXME: Should not come here...if it comes what to do???
			debug_sprintf(szDbgMsg, "%s: Invalid Request type, required field is missing to process", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//Praveen_P1: FIXME: Not sure whether we can send No Field as the error response
			rv = ERR_NO_FLD; //Required fields are not present
			break;
		}

		rvTemp = rv;//Storing rv for Payment SAF frecords
		bTOR = PAAS_TRUE;
		switch(iRetVal)
		{
		case SAF_REQ_STATUS:
			debug_sprintf(szDbgMsg, "%s: Need to give information of the Queried SAF records that are filtered by the given status", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = getSAFDtlsByRecStatus(&stSAFDtls, bTOR);
			if(rv == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF Status and FOUND the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(rv == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF Status and Did NOT find the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while Querying the SAF Records Based on the Given SAF Status", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;
		case SAF_REQ_STATUS_SINGLE:
			debug_sprintf(szDbgMsg, "%s: Need to give information of the Queried SAF records that are filtered by the given status and the Record Number", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = getSAFDtlsByRecStatusCumRecNum(&stSAFDtls, bTOR);
			if(rv == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF Status and FOUND the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(rv == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF Status and did NOT find the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while Querying the SAF Records Based on the Given SAF Status", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		case SAF_REQ_STATUS_RANGE:
			debug_sprintf(szDbgMsg, "%s: Need to give information of the Queried SAF Records that are Filtered by the Given Status and the Range of Record Numbers", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = getSAFDtlsByRecStatusCumRange(&stSAFDtls, bTOR);
			if(rv == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF status and FOUND the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(rv == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF status and Did NOT find the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while Querying the SAF Records Based on the Given SAF Status", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		case SAF_REQ_RANGE:
			debug_sprintf(szDbgMsg, "%s: Need to give information of the queried SAF records in the given Range", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = getSAFDtlsByRecRange(&stSAFDtls, bTOR);
			if(rv == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records Based on the Given SAF Record Number Range and FOUND the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(rv == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records based on the Given SAF Record Number Range and Did NOT Find the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while Querying the SAF Records Based on the Given SAF Status", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		case SAF_REQ_SINGLE:
			debug_sprintf(szDbgMsg, "%s: Need to give information of the queried single SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = getSAFDtlsByRecNum(&stSAFDtls, bTOR);
			if(rv == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF records Based on the Given SAF Record Number and FOUND the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(rv == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF records Based on the Given SAF Record Number and Did NOT find the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while Querying the SAF Records Based on the Given SAF Record Number", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		case SAF_REQ_ENTIRE:
			debug_sprintf(szDbgMsg, "%s: Need to give information of all SAF records", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = getSAFDtlsAll(&stSAFDtls, bTOR);
			if(rv == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried the ALL SAF Records and FOUND the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(rv == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Queried ALL the SAF Records and Did NOT Find the Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error While Querying ALL the SAF Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		case SAF_REQ_NOTHING:
			debug_sprintf(szDbgMsg, "%s: Need to give information of all SAF records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SAF_RECORD_NOT_FOUND;
			break;
		default: //FIXME: Should not come here...if it comes what to do???
			debug_sprintf(szDbgMsg, "%s: Invalid Request type, required field is missing to process", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//Praveen_P1: FIXME: Not sure whether we can send No Field as the error response
			rv = ERR_NO_FLD; //Required fields are not present
			break;
		}

		if(rvTemp == SAF_RECORD_FOUND || rv == SAF_RECORD_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF records and FOUND the Records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS;
		}
		else if(rvTemp == SAF_RECORD_NOT_FOUND && rv == SAF_RECORD_NOT_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Queried the SAF Records and Did NOT Find the Records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error While Querying SAF Records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* update the SAF details in the transaction instance */
		rv = updateSAFDtlsForDevTran(szTranKey, &stSAFDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to update SAF dtls", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strcpy(szStatMsg, "Device Application Internal Error");
			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Successfully updated SAF dtls", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Successfully Queried and Updated the SAF Details");
			addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
		}

		if(atoi(stSAFDtls.szRecCnt) == 0)
		{
			strcpy(szStatMsg, "NO SAF RECORDS FOUND");
		}
		else if(atoi(stSAFDtls.szRecCnt) == 1)
		{
			sprintf(szStatMsg, "%s", "ONE SAF RECORD FOUND");
		}
		else
		{
			sprintf(szStatMsg, "%s %s", stSAFDtls.szRecCnt, "SAF RECORDS FOUND");
		}


		break; //Breaking from the while loop
	}

	setAppState(iCurAppState);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doSAFRemoval
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doSAFRemoval(char * szTranKey, char * szStatMsg)
{
	int				rv					= SUCCESS;
	int				rvTemp				= SUCCESS;
	int				iCurAppState		= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	int				iRecInprogress 		= 0;
	PAAS_BOOL		bSAFRemovalAllowed  = PAAS_TRUE;
	SAFDTLS_STYPE 	stSAFDtls;
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iCurAppState = getAppState();

	setAppState(SAF_REMOVAL);
	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Removing SAF Record");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}
		/* Get the SAF Request details from the transaction instance */
		memset(&stSAFDtls, 0x00, sizeof(SAFDTLS_STYPE));
		rv = getSAFDtlsForDevTran(szTranKey, &stSAFDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SAF Details placeholder",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/*Need to find out what type of removal:
			ENTIRE - PYMT + TOR
			RANGE
			SINGLE
			 or
			NOT_ALLOWED
		*/
		if(strlen(stSAFDtls.szStatus) == 0 && strlen(stSAFDtls.szTORStatus) == 0)
		{
			rv = getSAFRequestType(&stSAFDtls);
			debug_sprintf(szDbgMsg, "%s: SAF Request Type [%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
			if(rv == SAF_REQ_ENTIRE || rv == SAF_REQ_RANGE || rv == SAF_REQ_SINGLE)
			{
				debug_sprintf(szDbgMsg, "%s: SAF removal command is allowed; processing it", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				// SAF removal comes with some status; Not allowed to remove
				bSAFRemovalAllowed = PAAS_FALSE;
				rv = SUCCESS;
			}
		}
		else
		{
			bSAFRemovalAllowed = PAAS_FALSE;
		}

		if(bSAFRemovalAllowed)
		{
			switch(rv)
			{
				case SAF_REQ_ENTIRE:
					debug_sprintf(szDbgMsg, "%s: Need to clear Entire SAF database", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = clearAllSAFRecords(&stSAFDtls);

					if(rv == SAF_RECORD_FOUND)
					{
						debug_sprintf(szDbgMsg, "%s: Cleared all SAF records successfully", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = SUCCESS;
					}
					else if(rv == SAF_RECORD_NOT_FOUND)
					{
						debug_sprintf(szDbgMsg, "%s: No SAF records to clear", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = SUCCESS;
					}
					else if(rv == SAF_RECORD_IN_PROGRESS)
					{
						debug_sprintf(szDbgMsg, "%s: SAF Record in IN_FLIGHT Status, Cannot delete the Record.", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						iRecInprogress = 1;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error while clearing all SAF records!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = ERR_DEVICE_APP;
					}

					break;
				case SAF_REQ_RANGE:
					debug_sprintf(szDbgMsg, "%s: Need to clear Range in SAF database", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = clearRangeofSAFRecords(&stSAFDtls);

					if(rv == SAF_RECORD_FOUND)
					{
						debug_sprintf(szDbgMsg, "%s: Cleared given range of SAF records successfully", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = SUCCESS;
					}
					else if(rv == SAF_RECORD_NOT_FOUND)
					{
						debug_sprintf(szDbgMsg, "%s: Given range of SAF records not found in the data base", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = SUCCESS;
					}
					else if(rv == SAF_RECORD_IN_PROGRESS)
					{
						debug_sprintf(szDbgMsg, "%s: SAF Record in IN_FLIGHT Status, Cannot delete the Record.", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						iRecInprogress = 1;
					}
					else if(rv == SAF_REC_PARTIALLY_FOUND)
					{
						debug_sprintf(szDbgMsg, "%s: Found Some SAF Record in Available range along with IN_PROCESS(IN_FLIGHT) Status, Couldn't delete one Record.", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rvTemp = SAF_REC_PARTIALLY_FOUND;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error while clearing given range of SAF records!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = ERR_DEVICE_APP;
					}

					break;
				case SAF_REQ_SINGLE:
					debug_sprintf(szDbgMsg, "%s: Need to clear one SAF record in SAF database", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = clearOneSAFRecord(&stSAFDtls);

					if(rv == SAF_RECORD_FOUND)
					{
						debug_sprintf(szDbgMsg, "%s: Cleared given SAF record successfully", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = SUCCESS;
					}
					else if(rv == SAF_RECORD_NOT_FOUND)
					{
						debug_sprintf(szDbgMsg, "%s: Given SAF record not found in the data base", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = SUCCESS;
					}
					else if(rv == SAF_RECORD_IN_PROGRESS)
					{
						debug_sprintf(szDbgMsg, "%s: SAF Record in IN_FLIGHT Status, Cannot delete the Record.", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						iRecInprogress = 1;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error while clearing given SAF record!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = ERR_DEVICE_APP;
					}

					break;
				default: //FIXME: Should not come here...if it comes what to do???
					debug_sprintf(szDbgMsg, "%s: Invalid CLEAR type", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = ERR_DEVICE_APP;
					break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: SAF removal command comes along with status; Remove command is not supported", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strcpy(stSAFDtls.szRecCnt, "0");
			strcpy(stSAFDtls.szTotAmt, "0.00");
			stSAFDtls.recList = NULL;
		}

		// CID 67318 (#1 of 4): Unused value (UNUSED_VALUE). T_RaghavendranR1. below check added.
		if(rv != ERR_DEVICE_APP)
		{
			/* update the SAF details in the transaction instance */
			rv = updateSAFDtlsForDevTran(szTranKey, &stSAFDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update SAF dtls", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(szStatMsg, "Device Application Internal Error");
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Update SAF Details.");
					strcpy(szAppLogDiag, "Device Application Internal Error");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, szAppLogDiag);
				}
				rv = FAILURE;
				break;
			}

			debug_sprintf(szDbgMsg, "%s: Successfully updated SAF dtls", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iRecInprogress == 1)
			{
				strcpy(szStatMsg, "Cannot Remove IN_PROCESS SAF Record, Currently Posting Record to Host");
				rv = ERR_UNSUPP_OPR;
			}
			else if(! bSAFRemovalAllowed)
			{
				strcpy(szStatMsg, "SAF REMOVAL Command Is Not Supported For Given Input Request");
				rv = ERR_UNSUPP_OPR;
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "SAF REMOVAL Command Is Not Supported For Given Input Request");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSING, szAppLogData, NULL);
				}
			}
			else
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Removed and Updated SAF Details");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSING, szAppLogData, NULL);
				}

				if(atoi(stSAFDtls.szRecCnt) == 0)
				{
					strcpy(szStatMsg, "NO SAF RECORDS REMOVED");
				}
				else if(atoi(stSAFDtls.szRecCnt) == 1)
				{
					sprintf(szStatMsg, "%s", "ONE SAF RECORD REMOVED");
				}
				else
				{
					sprintf(szStatMsg, "%s %s", stSAFDtls.szRecCnt, "SAF RECORDS REMOVED");
				}
				if(rvTemp == SAF_REC_PARTIALLY_FOUND)
				{
					strcat(szStatMsg, ". Coudn't Remove One IN_PROCESS Record");
				}
				if(iAppLogEnabled == 1)
				{
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szStatMsg, NULL);
				}
			}
		}

		break; //Breaking from the while loop
	}

	setAppState(iCurAppState);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: startNewSession
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int startNewSession(char * szMACLbl, char * szTranKey, char * szStatMsg)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	int				iDefLangBeforeEMV	= -1;
	char			szAppLogData[300]	= "";
	SESSMAP_PTYPE	pstTmpMap			= NULL;
	SESSDTLS_STYPE	stSess;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstSessMap != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Session is already present",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(szAppLogData, 0x00, sizeof(szAppLogData));
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Session Already Opened");
				addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
			}

			rv = ERR_IN_SESSION;
			break;
		}

		pstTmpMap = (SESSMAP_PTYPE) malloc(sizeof(SESSMAP_STYPE));
		if(pstTmpMap == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pstTmpMap, 0x00, sizeof(SESSMAP_STYPE));
		strcpy(pstTmpMap->szMACLbl, szMACLbl);

		/* Open a new Session */
		rv = openNewSession(pstTmpMap->szSessKey);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to open a new SESSION",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Open Session");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			break;
		}

		/* Get the session details from the POS request */
		memset(&stSess, 0x00, sizeof(SESSDTLS_STYPE));
		if (stSess.pstPYMNTInfo == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Payment info is null",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		getSessDtlsFrmPOSReq(szTranKey, &stSess);

		if( strlen(stSess.szBusinessDate) > 0)
		{
			setBusinessDate(stSess.szBusinessDate);
		}
		//Save Merchant Store Id
		if( strlen(stSess.szStoreNo) > 0)
		{
			saveMcntStoreId(stSess.szStoreNo);
		}
		//Save Lane Id
		if( strlen(stSess.szLaneNo) > 0)
		{
			saveMcntLaneId(stSess.szLaneNo);
		}

		pstSessMap = pstTmpMap;
		pstTmpMap = NULL;

		/*
		 * iSwipeAheadFlag possible values
		 * -1 - SWIPE_AHEAD field is not present in the SESSION START command
		 * 0  - SWIPE_AHEAD is disabled for this session
		 * 1  - SWIPE_AHEAD is enabled for this session
		 */

		if(stSess.iSwipeAheadFlag != -1) //If the value/field is sent in the command
		{
			debug_sprintf(szDbgMsg, "%s: Received SWIPE_AHEAD field [%d]", __FUNCTION__, stSess.iSwipeAheadFlag);
			APP_TRACE(szDbgMsg);

			/*
			 * Praveen_P1: We need to dynamically enable/disable swipe-ahead feature per session
			 * we get the flag(SWIPE_AHEAD) in the START SESSION command
			 * The value comes in the SESSION command overwrites the configuration
			 * set on the terminal
			 * we are storing the current config value in a variable and then
			 * update the config memory vairable (to avoid code changes in the lineitemscreen function)
			 */

			stSess.bSwipeAheadConfigVal = isSwipeAheadEnabled(); //Store the current value

			debug_sprintf(szDbgMsg, "%s: Current Swipe Ahead parameter value [%d]", __FUNCTION__, stSess.bSwipeAheadConfigVal);
			APP_TRACE(szDbgMsg);

			/*
			 * 0 would indicate no swipe ahead allowed.
			 * Any other value, preferably 1 would indicate swipe ahead allowed
			 */
			if(stSess.iSwipeAheadFlag == 0)
			{
				//Please note that this will set only in memory, not changing config.usr1 value
				setSwipeAheadParamValue(PAAS_FALSE);
			}
			else
			{
				//Please note that this will set only in memory, not changing config.usr1 value
				setSwipeAheadParamValue(PAAS_TRUE);
			}

			debug_sprintf(szDbgMsg, "%s: After setting Swipe Ahead parameter value [%d]", __FUNCTION__, isSwipeAheadEnabled());
			APP_TRACE(szDbgMsg);

		}
		/*
		 * iDemoModeFlag possible values
		 * -1 - TRAINING_MODE field is not present in the SESSION START command
		 * 0  - TRAINING_MODE is disabled for this session
		 * 1  - TRAINING_MODE is enabled for this session
		 */

		if(stSess.iDemoModeFlag != -1) //If the value/field is sent in the command
		{
			debug_sprintf(szDbgMsg, "%s: Received TRAINING_MODE field [%d]", __FUNCTION__, stSess.iDemoModeFlag);
			APP_TRACE(szDbgMsg);

			/*
			 * The value comes in the SESSION command overwrites the configuration
			 * set on the terminal
			 * we are storing the current config value in a variable and then
			 * update the config memory vairable (to avoid code changes in the SSI module)
			 */

			stSess.bDemoModeConfigVal = isDemoModeEnabled(); //Store the current value

			debug_sprintf(szDbgMsg, "%s: Current DemoMode parameter value [%d]", __FUNCTION__, stSess.bDemoModeConfigVal);
			APP_TRACE(szDbgMsg);

			if((stSess.iDemoModeFlag == PAAS_TRUE) && (stSess.bDemoModeConfigVal == PAAS_FALSE))
			{
				//Please note that this will set only in memory, not changing config.usr1 value
				setDemoModeParamValue(PAAS_TRUE);
				/* Need to set "TRAINING MODE" on the screen for this session.
				 * SetWaterMarkImage:	<STX>XSWT<FS>WaterMarkText<FS>Font-Style<FS>Font-Size<ETX>
				 */
				setWaterMarkText(PAAS_TRUE);
			}
			else if((stSess.iDemoModeFlag == PAAS_TRUE) && (stSess.bDemoModeConfigVal == PAAS_TRUE))
			{
				debug_sprintf(szDbgMsg, "%s: Demo Mode is already enabled", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			debug_sprintf(szDbgMsg, "%s: After setting DemoMode parameter value [%d]", __FUNCTION__, isDemoModeEnabled());
			APP_TRACE(szDbgMsg);

		}

		//Set initial value for payment type as -1 to indicate payment type is not present when session starts.
		stSess.iPaymentType= -1;

		iDefLangBeforeEMV = getCurLangId();
		stSess.iDefLangBeforeEMV = iDefLangBeforeEMV;

		/* Update the new session with these details */
		updateGenSessDtls(pstSessMap->szSessKey, &stSess);

		/*
		 * Praveen_P1: 11 August 2016
		 * Added new field in the START SESSION command
		 * SCREEN_CHANGE, default it is TRUE
		 * if this field comes with value FALSE, then
		 * we will not be moving to line item/welcome screen
		 * will remain on the idle screen itself
		 *
		 */

		debug_sprintf(szDbgMsg, "%s: stSess.bScreenChangereqd %d", __FUNCTION__, stSess.bScreenChangereqd);
		APP_TRACE(szDbgMsg);

		if(stSess.bScreenChangereqd)
		{
			/* Start showing the line item screen */
			if(isLineItemDisplayEnabled())
			{
				showLineItemScreen();
			}
			else
			{
				showWelcomeScreen(PAAS_FALSE);
			}
		}
		else
		{
			setAppState(IN_SESSION_IDLE);

			debug_sprintf(szDbgMsg, "%s: Screen Change is not required", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		sprintf(szStatMsg, "Session Started");
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Session Started Successfully");
			addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
		}
		break;
	}

	if(pstTmpMap != NULL)
	{
		/* Free any created session also for this temp map */
		if(strlen(pstTmpMap->szSessKey) > 0)
		{
			closeSession(pstTmpMap->szSessKey);
		}

		/* Close the session */
		free(pstTmpMap);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: finishSession
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int finishSession(char * szMACLbl, char * szStatMsg, PAAS_BOOL laneClose)
{
	int					rv					= SUCCESS;
	int					iTemprv				= SUCCESS;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[300]	= "";
	char				szAppLogDiag[300]	= "";
	char				szClearData[1024+1]	= "";
	char				szErrorBuf[256+1]		= "";
	int					iDefLangBeforeEMV	= -1;
	PAAS_BOOL			bEmvEnabled			= PAAS_FALSE;
	SESSDTLS_PTYPE		pstSess				= NULL;
	SESSDTLS_STYPE		stTempSess;

#ifdef DEVDEBUG
	char			szDbgMsg[256]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/*Akshaya: 25-07-16 :Checking for Emvenabled in device to Show Please Remove Card Screen at the Finish Session*/
	bEmvEnabled = isEmvEnabledInDevice();

	while(1)
	{
		/* Check if the session even exists */
		if(pstSessMap == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No session present to FINISH",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szAppLogData, 0x00, sizeof(szAppLogData));
			memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "No Session Present to Close the Session.");
				strcpy(szAppLogDiag, "Please Open a Session.");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, szAppLogDiag);
			}

			rv = ERR_NO_SESSION;
			break;
		}
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Processing Session Finish Command ");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		/* Check if it is the same POS trying to close the session, who opened
		 * it in first place */
		if(strcmp(pstSessMap->szMACLbl, szMACLbl) != SUCCESS)
		{
#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s: %s vs %s MAC lables", __FUNCTION__,
											szMACLbl, pstSessMap->szMACLbl);
			APP_TRACE(szDbgMsg);
#endif
			debug_sprintf(szDbgMsg, "%s: Session open by different POS",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			sprintf(szStatMsg, "SESSION in progress for [%s]",
														pstSessMap->szMACLbl);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Device is Busy, Session Already Opened by POS with MAC Label [%s]", pstSessMap->szMACLbl);
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			rv = ERR_DEVICE_BUSY;
			break;
		}

		/* Get the session reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get Reference to the Session");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			return PAAS_FALSE;
		}

		iDefLangBeforeEMV = pstSess->iDefLangBeforeEMV;
		setLangId(iDefLangBeforeEMV);

		//Free the Stored Gift Card Dtls if present
		if(pstSess->pstPrevGiftTranCardDtls != NULL)
		{
			free(pstSess->pstPrevGiftTranCardDtls);
		}
		//Free the Store Post_Auth Card Dtls if present
		if(pstSess->pstPostAuthTransCardDtls != NULL)
		{
			free(pstSess->pstPostAuthTransCardDtls);
		}
		//Free the Dup Card Dtls if present
		if(pstSess->pstDupSwipeCardDtls != NULL)
		{
			free(pstSess->pstDupSwipeCardDtls);
		}
		//Free the VAS Data if present
		if(pstSess->pstVasDataDtls != NULL)
		{
			free(pstSess->pstVasDataDtls);
		}

		/* Daivik:8/7/2016 : We normally reset the backup details during flushing of preswipe or carddetails.
		 * We also call the reset backup details function here as a precaution.
		 *
		 */
		rv = resetBkupDtlsInSession();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to Reset the Backup Card Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(pstSess->pstQRCodeDtls != NULL)
		{
			free(pstSess->pstQRCodeDtls);
		}
		
		memset(&stTempSess, 0x00, sizeof(SESSDTLS_STYPE));

		memcpy(&stTempSess, pstSess, sizeof(SESSDTLS_STYPE));

		/* Close the session */
		rv = closeSession(pstSessMap->szSessKey);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to close the session",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Close the Session");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			break;
		}

		/*
		 * Praveen_P1: We need to dynamically enable/disable swipe-ahead feature per session
		 * we get the flag(SWIPE_AHEAD) in the START SESSION command
		 * The value comes in the SESSION command overwrites the configuration
		 * set on the terminal
		 * we stored the current config value in a variable while opening session
		 * in the close session we are reverting back to original value
		 */
		if(stTempSess.iSwipeAheadFlag != -1) //If the value/field is sent in the command
		{
			debug_sprintf(szDbgMsg, "%s: SWIPE_AHEAD field was received in START SESSION command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Setting SwipeAhead Config param value with original value [%d] ", __FUNCTION__, stTempSess.bSwipeAheadConfigVal);
			APP_TRACE(szDbgMsg);

			setSwipeAheadParamValue(stTempSess.bSwipeAheadConfigVal); //Setting with the original value

			/*
			 * When pre-swipe is enabled for this session(swipeaheadenabled is set to N)
			 * MSR will be enabled....after opening the session and then close the session
			 * we need to disable MSR..For doing that following check is there
			 */
			if(stTempSess.bSwipeAheadConfigVal == 0 && stTempSess.bPreSwipeDone == PAAS_FALSE)
			{
				/* Disabling the card swipe on the UI agent. */
				disableCardReaders();
			}

			debug_sprintf(szDbgMsg, "%s: After setting Swipe Ahead parameter value [%d]", __FUNCTION__, isSwipeAheadEnabled());
			APP_TRACE(szDbgMsg);
		}

		/*Akshaya:25-07-16 If Emvenabled in device then checking for card Presence and Showing Please Remove Card Screen at the Finish Session*/
		if(bEmvEnabled)
		{
			disableCardReaders();

			checkCrdPrsnceAndShwMsgToRemove();
		}

		/* The value comes in the SESSION command overwrites the configuration
		 * set on the terminal
		 * we stored the current config value in a variable while opening session
		 * in the close session we are reverting back to original value
		 */
		if(stTempSess.iDemoModeFlag != -1) //If the value/field is sent in the command
		{
			debug_sprintf(szDbgMsg, "%s: TRAINING_MODE field was received in START SESSION command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: Setting DemoMode Config param value with original value [%d] ", __FUNCTION__, stTempSess.bDemoModeConfigVal);
			APP_TRACE(szDbgMsg);

			if((stTempSess.iDemoModeFlag == PAAS_TRUE) && (stTempSess.bDemoModeConfigVal == PAAS_FALSE))
			{
				setDemoModeParamValue(stTempSess.bDemoModeConfigVal); //Setting with the original value
				/* Need to remove "TRAINING MODE" from the screen.
				 * SetWaterMarkImage:	<STX>XSWT<FS><FS>Font-Style<FS>Font-Size<ETX>
				 */
				setWaterMarkText(PAAS_FALSE);
			}
			else if((stTempSess.iDemoModeFlag == PAAS_TRUE) && (stTempSess.bDemoModeConfigVal == PAAS_TRUE))
			{
				debug_sprintf(szDbgMsg, "%s: Demo Mode is always enabled, no need to remove Demo text from screen", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			debug_sprintf(szDbgMsg, "%s: After setting DemoMode parameter value [%d]", __FUNCTION__, isDemoModeEnabled());
			APP_TRACE(szDbgMsg);

			//whichForm = -1;
			//showIdleScreen();
		}

		free(pstSessMap);
		pstSessMap = NULL;

		/*
		 * Need to clear the Listbox state by sending command to UIAgent
		 * We are supporting to add/remove/override line items from anywhere
		 * within the session, we are using XLRS command(Used in conjunction with the XALI command;
		 * used to update a listbox control with a cached copy of listbox items) for this
		 * We need to send XLCS command(Used in conjunction with the XALI and XLRS commands;
		 * used to clear the cache of listbox items) at the end of the session to remove cached items
		 */
		if(isLineItemDisplayEnabled())
		{
			clearUIListBoxState();
		}

		/*
		 * At the end of the session, we need to reset the consumer options
		 *
		 */
		if(isConsumerOptsEnabled() == PAAS_TRUE)
		{
			resetConsOptSelection();
		}

		sprintf(szStatMsg, "Session Finished");
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Session Closed Successfully");
			addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
		}

		if(getEncryptionType() == VSP_ENC)
		{
			/*
			 * Praveen_P1: In VSP mode when we close the session, we will flush VSP clear data
			 * so that we will not have access to the clear data of current transaction
			 */
			iTemprv = ProcessVSPCommand(VSP_MSG_FLUSH_BUFFER_REQUEST, szClearData, szErrorBuf);

			debug_sprintf(szDbgMsg, "%s: ProcessVSPCommand Returning [%d]", __FUNCTION__, iTemprv);
			APP_TRACE(szDbgMsg);
		}

		if(laneClose == PAAS_FALSE)
		{
			showIdleScreen();
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isSessionInProgress
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
PAAS_BOOL isSessionInProgress()
{
	PAAS_BOOL		rv				= PAAS_FALSE;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pstSessMap != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Session in progress", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = PAAS_TRUE;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Session is not in progress", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = PAAS_FALSE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doLaneClose
 *
 * Description	: This function will close the session and display the Lane closed message in the idle screen
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doLaneClose(char * szTranKey, char * szMACLbl, char * szStatMsg)
{

	int		    		rv					= SUCCESS;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[300]	= "";
	PAAS_BOOL			bSessPresent		= PAAS_FALSE;
	LANECLOSED_STYPE	stLaneClosedDtls;
#ifdef DEBUG
	char				szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL || szMACLbl == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Closing the Lane");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}
		bSessPresent = isSessionInProgress();
		if(bSessPresent == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Session is in progress", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Session is Currently in Progress, Closing the Session");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			debug_sprintf(szDbgMsg, "%s: Closing the session for Lane Closed Command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

		    rv = finishSession(szMACLbl, szStatMsg, PAAS_TRUE);
		}

		if(rv == SUCCESS)
		{
			/* get the Lane Closed display text message details */
			memset(&stLaneClosedDtls, 0x00, sizeof(LANECLOSED_STYPE));
			rv = getLaneClosedDtlsForDevTran(szTranKey, &stLaneClosedDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get Lane Closed data details",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Successfully Executed the Lane Close Command");
				addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
			}
			sprintf(szStatMsg, "Lane Closed");
			showLaneClosedScreen(&stLaneClosedDtls);
		}

		break;
	}

	setAppState(LANE_CLOSED);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: execLIFlow
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int execLIFlow(char * szMACLbl, char * szTranKey, char * szStatMsg, int iCmd, int iAllowLITran)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[512]	= "";
	char	szAppLogDiag[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check if the session even exists */
		if(pstSessMap == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No session present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szAppLogData, 0x00, sizeof(szAppLogData));
			memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "No Session Present to Process the Line Item Transaction.");
				strcpy(szAppLogDiag, "Please Create a Session to Process the Line Item Transaction");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_NO_SESSION;
			break;
		}

		/* Check if it is the same POS trying to close the session, who opened
		 * it in first place */
		if(strcmp(pstSessMap->szMACLbl, szMACLbl) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Session open by different POS",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			sprintf(szStatMsg, "SESSION in progress for [%s]",
														pstSessMap->szMACLbl);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Device is Busy, Session Already Opened by POS with MAC Label [%s]", pstSessMap->szMACLbl);
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			rv = ERR_DEVICE_BUSY;
			break;
		}

		switch(iCmd)
		{
		case SCI_ADD:
			rv = addLI(szTranKey, pstSessMap->szSessKey, iAllowLITran, szStatMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to add line items",
															__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Add Line Item Details");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}

			break;

		case SCI_REMOVE:
			rv = removeLI(szTranKey, pstSessMap->szSessKey, iAllowLITran, szStatMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to remove line items",
															__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Remove Line Item(s)");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}

			break;

		case SCI_REMOVEALL:
			rv = removeAllLI(szTranKey, pstSessMap->szSessKey, iAllowLITran, szStatMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to remove all line items",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Remove all Line Item(s)");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			break;
		case SCI_OVERRIDE:
			rv = overrideLI(szTranKey, pstSessMap->szSessKey, iAllowLITran, szStatMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to Override line items",
															__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Override Line Item Details");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}

			break;
		case SCI_SHOW:
			rv = showLI(szTranKey, szStatMsg, pstSessMap->szSessKey);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to show line items",
															__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Show Line Item Details");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}

			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSAFRequestType
 *
 * Description	: Based on the input parameters this function decides what type
 * 				  request it is from the POS
 *
 * Input Params	: SAF Request Details
 *
 * Output Params: SAF Request Type
 * ============================================================================
 */
static int getSAFRequestType(SAFDTLS_PTYPE pstSAFDtls)
{
	int		rv		= SUCCESS;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s:---enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Param passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(strlen(pstSAFDtls->szStatus) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Request based on STATUS Range type, status is %s", __FUNCTION__, pstSAFDtls->szStatus);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Checking for presence of SAF_NUM_BEGIN and SAF_NUM_END fields", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(strcasecmp(pstSAFDtls->szStatus, "ALL") != SUCCESS && (strlen(pstSAFDtls->szBegNo) > 0) && (strlen(pstSAFDtls->szEndNo) > 0) )
		{
			debug_sprintf(szDbgMsg, "%s: Request based on Range [%s, %s] also along with Record Status %s", __FUNCTION__, pstSAFDtls->szBegNo, pstSAFDtls->szEndNo, pstSAFDtls->szStatus);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_STATUS_RANGE;
		}
		else if(strcasecmp(pstSAFDtls->szStatus, "ALL") != SUCCESS && ((strlen(pstSAFDtls->szBegNo) > 0) || (strlen(pstSAFDtls->szEndNo) > 0 ))) //Request contains SAF_NUM_BEGIN or SAF_NUM_END...  ONE
		{
			debug_sprintf(szDbgMsg, "%s: Request based on Single record number along with the Record status %s", __FUNCTION__, pstSAFDtls->szStatus);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_STATUS_SINGLE;
		}
		/*Akshaya - PTMX - 1591 2.19.27:SAF TOR Query not working for SAF_TOR_STATUS as ALL. These checkes are required when Query for SAF_STATUS as ALL and SAF_NUM_BEGIN and SAF_NUM_END are provided*/
		else if(strcasecmp(pstSAFDtls->szStatus, "ALL") == SUCCESS && (strlen(pstSAFDtls->szBegNo) > 0) && (strlen(pstSAFDtls->szEndNo) > 0))
		{
			debug_sprintf(szDbgMsg, "%s: Request based on Status All, Along with Range[%s, %s] ", __FUNCTION__, pstSAFDtls->szBegNo, pstSAFDtls->szEndNo);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_RANGE;
		}
		else if(strcasecmp(pstSAFDtls->szStatus, "ALL") == SUCCESS && (strlen(pstSAFDtls->szBegNo) > 0 || strlen(pstSAFDtls->szEndNo) > 0))
		{
			debug_sprintf(szDbgMsg, "%s: Request based on Status All, Along with Single Record Number ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_SINGLE;
		}
		else if(strcasecmp(pstSAFDtls->szStatus, "ALL") ==SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Request based on Status All  ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_ENTIRE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Request based on Record status %s alone", __FUNCTION__, pstSAFDtls->szStatus);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_STATUS;
		}
	}
	else if(strlen(pstSAFDtls->szBegNo) > 0 && strlen(pstSAFDtls->szEndNo) > 0 ) //Request contains SAF_NUM_BEGIN and SAF_NUM_END.. RANGE
	{
		debug_sprintf(szDbgMsg, "%s: Request based on Range, [%s, %s]", __FUNCTION__, pstSAFDtls->szBegNo, pstSAFDtls->szEndNo);
		APP_TRACE(szDbgMsg);
		rv = SAF_REQ_RANGE;
	}
	else if((strlen(pstSAFDtls->szBegNo) > 0)  || (strlen(pstSAFDtls->szEndNo) > 0 )) //Request contains SAF_NUM_BEGIN or SAF_NUM_END...  ONE
	{
		debug_sprintf(szDbgMsg, "%s: Request based on Single record number", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = SAF_REQ_SINGLE;
	}
	else if((strlen(pstSAFDtls->szBegNo) == 0) && (strlen(pstSAFDtls->szEndNo) == 0) && (strlen(pstSAFDtls->szStatus) == 0)) //Request does not contain any filter
	{
		if((strlen(pstSAFDtls->szTORBegNo) == 0) && (strlen(pstSAFDtls->szTOREndNo) == 0) && (strlen(pstSAFDtls->szTORStatus) == 0))
		{
			debug_sprintf(szDbgMsg, "%s: Request Need Entire Payment SAF Record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_ENTIRE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Request Do Not Need any Payment SAF Records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_NOTHING;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No Request Type available", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s:Returning with %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getSAFTORRequestType
 *
 * Description	: Based on the input parameters this function decides what type
 * 				  request it is from the POS
 *
 * Input Params	: SAF Request Details
 *
 * Output Params: SAF Request Type
 * ============================================================================
 */
static int getSAFTORRequestType(SAFDTLS_PTYPE pstSAFDtls)
{
	int		rv		= SUCCESS;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s:---enter----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Param passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(strlen(pstSAFDtls->szTORStatus) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Request based on STATUS Range type, status is %s", __FUNCTION__, pstSAFDtls->szTORStatus);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Checking for presence of SAF_TOR_NUM_BEGIN and SAF_TOR_NUM_END fields", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(strcasecmp(pstSAFDtls->szTORStatus, "ALL") != SUCCESS && (strlen(pstSAFDtls->szTORBegNo) > 0) && (strlen(pstSAFDtls->szTOREndNo) > 0) )
		{
			debug_sprintf(szDbgMsg, "%s: Request based on Range [%s, %s] also along with Record Status %s", __FUNCTION__, pstSAFDtls->szTORBegNo, pstSAFDtls->szTOREndNo, pstSAFDtls->szTORStatus);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_STATUS_RANGE;
		}
		else if(strcasecmp(pstSAFDtls->szTORStatus, "ALL") != SUCCESS && ((strlen(pstSAFDtls->szTORBegNo) > 0) || (strlen(pstSAFDtls->szTOREndNo) > 0 ))) //Request contains SAF_NUM_BEGIN or SAF_NUM_END...  ONE
		{
			debug_sprintf(szDbgMsg, "%s: Request based on Single record number along with the Record status %s", __FUNCTION__, pstSAFDtls->szTORStatus);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_STATUS_SINGLE;
		}
		/*Akshaya - PTMX - 1591 2.19.27:SAF TOR Query not working for SAF_TOR_STATUS as ALL. These checkes are required when Query for SAF_TOR_STATUS as ALL and SAF_TOR_NUM_BEGIN and SAF_TOR_NUM_END are provided*/
		else if(strcasecmp(pstSAFDtls->szTORStatus, "ALL") == SUCCESS && (strlen(pstSAFDtls->szTORBegNo) > 0) && (strlen(pstSAFDtls->szTOREndNo) > 0))
		{
			debug_sprintf(szDbgMsg, "%s: Request based on Status All, Along with Range[%s, %s] ", __FUNCTION__, pstSAFDtls->szTORBegNo, pstSAFDtls->szTOREndNo);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_RANGE;
		}
		else if(strcasecmp(pstSAFDtls->szTORStatus, "ALL") == SUCCESS && (strlen(pstSAFDtls->szTORBegNo) > 0 || strlen(pstSAFDtls->szTOREndNo) > 0))
		{
			debug_sprintf(szDbgMsg, "%s: Request based on Status All, Along with Single Record Number"
					"", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_SINGLE;
		}
		else if(strcasecmp(pstSAFDtls->szTORStatus, "ALL") ==SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Request based on Status All  ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_ENTIRE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Request based on Record status %s alone", __FUNCTION__, pstSAFDtls->szTORStatus);
			APP_TRACE(szDbgMsg);
			rv = SAF_REQ_STATUS;
		}
	}
	else if(strlen(pstSAFDtls->szTORBegNo) > 0 && strlen(pstSAFDtls->szTOREndNo) > 0 ) //Request contains SAF_NUM_BEGIN and SAF_NUM_END.. RANGE
	{
		debug_sprintf(szDbgMsg, "%s: Request based on Range, [%s, %s]", __FUNCTION__, pstSAFDtls->szTORBegNo, pstSAFDtls->szTOREndNo);
		APP_TRACE(szDbgMsg);
		rv = SAF_REQ_RANGE;
	}
	else if((strlen(pstSAFDtls->szTORBegNo) > 0)  || (strlen(pstSAFDtls->szTOREndNo) > 0 )) //Request contains SAF_NUM_BEGIN or SAF_NUM_END...  ONE
	{
		debug_sprintf(szDbgMsg, "%s: Request based on Single record number", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = SAF_REQ_SINGLE;
	}
	else if((strlen(pstSAFDtls->szTORBegNo) == 0) && (strlen(pstSAFDtls->szTOREndNo) == 0) && (strlen(pstSAFDtls->szTORStatus) == 0)) //Request does not contain any filter
	{
		debug_sprintf(szDbgMsg, "%s: Request Do Not Need any TOR SAF Records", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = SAF_REQ_NOTHING;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No Request Type available", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s:Returning with %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getLPTokenDetails
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getLPTokenDetails(char * szMACLbl, char * szTranKey, char * szStatMsg, int iCmd)
{
	int		rv				= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = processLPTokenQuery(szTranKey, szStatMsg);
	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Successfully did the LP Token Query", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failure while doing the LP Token Query", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: execPymtFlow
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int execPymtFlow(char * szMACLbl, char * szTranKey, char * szStatMsg, int iCmd)
{
	int				rv						= SUCCESS;
	int				iUpdatedCmd				= 0;
	int				iTranSuccess 			= FAILURE;
	int				iAppLogEnabled			= isAppLogEnabled();
	char			szPymtCode[15]			= "";
	char			szAppLogData[300]		= "";
	char			szAppLogDiag[300]		= "";
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
	
	while(1)
	{
		 /* The below Function Sets the Function Type alone for the SSI Transaction. It is Required to set the Payment Function Type, so that even if
		 * validation of the Payment Transaction fails or Payment Transaction is sent without session, we free the stored Payment Structures in SCI Request parsing.*/
		rv = setFxnForSSITran(szTranKey, SSI_PYMT);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to set SSI Function", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* setting the status message which will be sent as response text to POS*/
			strcpy(szStatMsg, "Device Application Internal Error");

			break;
		}

		/* Check if the session even exists */
		if(pstSessMap == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No session present to execute Payment Tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "No Session Present to Process the Transaction.");
				strcpy(szAppLogDiag, "Please Create a Session to Process the Transaction");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_NO_SESSION;
			break;
		}

		/* Check if it is the same POS trying to close the session, who opened
		 * it in first place */
		if(strcmp(pstSessMap->szMACLbl, szMACLbl) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Session open by different POS",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			sprintf(szStatMsg, "SESSION in progress for [%s]",
														pstSessMap->szMACLbl);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Device is Busy, Session Already Opened by POS with MAC Label [%s]", pstSessMap->szMACLbl);
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			rv = ERR_DEVICE_BUSY;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Current Command [%d] ", __FUNCTION__, iCmd);
		APP_TRACE(szDbgMsg);

		/*
		 * Need to update the command depends on the values present for
		 * AUTH CODE, CTROUD
		 */
		rv = udpatePaymentCmd(szTranKey, &iUpdatedCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Payment Command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* setting the status message which will be sent as response text to POS*/
			strcpy(szStatMsg, "Device Application Internal Error");

			rv = FAILURE;
			break;
		}

		if(getPOSInitiatedState())
		{
			rv = POS_CANCELLED;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Current Command [%d] Updated Command [%d]", __FUNCTION__, iCmd, iUpdatedCmd);
		APP_TRACE(szDbgMsg);

		/*
		 * Added for logging/debugging purpose
		 */
		switch(iUpdatedCmd)
		{
		case SCI_AUTH:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "AUTH");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_VOICEAUTH:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "VOICE AUTH");
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received Authorization VOICE AUTH Command");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}
			break;
		case SCI_PREAUTH:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "PRE AUTH");
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received Authorization PRE AUTH Command");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}
			break;
		case SCI_POSTAUTH:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "POST AUTH");
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received Authorization POST AUTH Command");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}
			break;
		case SCI_CAPTURE:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "SALE");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_CREDIT:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "REFUND");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_VOID:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "VOID");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_COMPLETION:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "COMPLETION");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_ADDTIP:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "ADDTIP");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_RESETTIP:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "RESETTIP");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_TOKEN:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "TOKEN QUERY");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_LPTOKEN:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "LP TOKEN");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_ACTIVATE:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "ACTIVATE");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_ADDVAL:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "ADD VALUE");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_BAL:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "BALANCE");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_GIFTCLOSE:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "CLOSE");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_DEACTIVATE:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "DEACTIVATE");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_REACTIVATE:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "REACTIVATE");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_REMVAL:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "REMOVAL");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_PAYACCOUNT:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "PAYACCOUNT");
			APP_TRACE(szDbgMsg);
			break;
		case SCI_TOKEN_UPDATE:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "TOKEN UPDATE");
			APP_TRACE(szDbgMsg);
			break;
		default:
			break;
		}

		if(isPaypalTenderEnabled() && (iUpdatedCmd != SCI_CAPTURE))
		{
			/* We take Paypal Payment Code for Paypal Transactions only for Capture , Refund and Void Transactions,
			 * For Other POS Commands We Flush out the Stored Payment Code From the Session and Proceed*/
			memset(szPymtCode, 0x00, sizeof(szPymtCode));
			storePaypalPymtCodeDtlsInSession(szPymtCode);
		}

		switch(iUpdatedCmd)
		{
		case SCI_AUTH:
		case SCI_VOICEAUTH:
		case SCI_PREAUTH:
		case SCI_POSTAUTH:
			rv = processAuthTran(szTranKey, pstSessMap->szSessKey, iUpdatedCmd, szStatMsg, &iTranSuccess);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully did the Auth transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Completed the Authorization Transaction");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
				if (isBapiEnabled() && (iTranSuccess == SUCCESS))
				{
					if (iUpdatedCmd == SCI_POSTAUTH )
					{
						rv = updatePymntDtlsForBAPI(pstSessMap->szSessKey, szTranKey, PYMNT);
					}
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while doing the AUTH transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Complete Authorization Transaction");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			/*
			 * If Pre-Swipe details are present in the memory, resetting it here
			 * irrespective of result of this transaction
			 */
			if(flushCardDetails(szTranKey) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure while resetting pre-swipe details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;

		case SCI_CAPTURE:
			rv = processSaleTran(szTranKey, pstSessMap->szSessKey, szStatMsg, &iTranSuccess);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully did the SALE transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Completed the SALE Transaction");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
				if (isBapiEnabled() && (iTranSuccess == SUCCESS))
					rv = updatePymntDtlsForBAPI(pstSessMap->szSessKey, szTranKey, PYMNT);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while doing the SALE transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Complete SALE Transaction");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			/*
			 * If Pre-Swipe details are present in the memmory, resetting it here
			 * irrespective of result of this transaction
			 */
			if(flushCardDetails(szTranKey) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure while resetting pre-swipe details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;

		case SCI_CREDIT:
			rv = processRefundTran(szTranKey, pstSessMap->szSessKey, szStatMsg, &iTranSuccess);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully refunded the given transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Completed the Refund Transaction");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
				if (isBapiEnabled() && (iTranSuccess == SUCCESS))
					rv = updatePymntDtlsForBAPI(pstSessMap->szSessKey, szTranKey, REFUND);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while refunding the given transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Complete Refund Transaction");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			/*
			 * If Pre-Swipe details are present in the memmory, resetting it here
			 * irrespective of result of this transaction
			 */
			if(flushCardDetails(szTranKey) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure while resetting pre-swipe details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;

		case SCI_VOID:
			rv = processVoidTran(szTranKey, pstSessMap->szSessKey, szStatMsg, &iTranSuccess);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Voided the given transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Completed the Void Transaction");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
				if (isBapiEnabled() && (iTranSuccess == SUCCESS))
					rv = updatePymntDtlsForBAPI(pstSessMap->szSessKey, szTranKey, REFUND);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while voiding the given transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Complete Void Transaction");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			/* Daivik:8/7/2016 - When we have swipe ahead enabled, we could swipe the card , but if it is a Void Command , then
			 * the preswiped details are not being used in today's code. But the card details remain in the session. Any further command
			 * results in these card details being picked up which is incorrect, hence need to flush the card details here.
			 */
			/*
			 * If Pre-Swipe details are present in the memmory, resetting it here
			 * irrespective of result of this transaction
			 */
			if(flushCardDetails(szTranKey) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure while resetting pre-swipe details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;

		case SCI_COMPLETION:
			rv = processCompletionTran(szTranKey, pstSessMap->szSessKey, szStatMsg, &iTranSuccess);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Completion of the given transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Completed the COMPLETION Transaction");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
				if (isBapiEnabled() && (iTranSuccess == SUCCESS))
					rv = updatePymntDtlsForBAPI(pstSessMap->szSessKey, szTranKey, PYMNT);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while completion the given transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Complete the COMPLETION Transaction");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			break;
		case SCI_ADDTIP:
		case SCI_RESETTIP:
			rv = processRestaurantTran(szTranKey, pstSessMap->szSessKey, iCmd, szStatMsg);
			if(rv == SUCCESS)
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Completed the Restaurant Transaction");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}

				debug_sprintf(szDbgMsg, "%s: Successfully completed the Restaurant transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while completing the Restaurant transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Complete Restaurant Transaction");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			break;
		case SCI_TOKEN:
			rv = processTokenQuery(szTranKey, pstSessMap->szSessKey, szStatMsg, PAAS_TRUE);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully did the Token Query", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Completed the Token Query");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while doing the Token Query", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Complete Token Query");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			/*
			 * If Pre-Swipe details are present in the memmory, resetting it here
			 * irrespective of result of this transaction
			 */
			if(flushCardDetails(szTranKey) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure while resetting pre-swipe details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		case SCI_LPTOKEN:
			rv = processLPTokenQuery(szTranKey, szStatMsg);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully did the LP Token Query", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Completed the LP Token Query");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while doing the LP Token Query", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Complete LP Token Transaction");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			break;
		case SCI_PAYACCOUNT:
			rv = processPayAccount(szTranKey, pstSessMap->szSessKey, szStatMsg, &iTranSuccess);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully did the PayAccount", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Completed the PayAccount");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
				if (isBapiEnabled() && (iTranSuccess == SUCCESS))
						rv = updatePymntDtlsForBAPI(pstSessMap->szSessKey, szTranKey, PYMNT);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while doing the PayAccount", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Complete PayAccount");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			/*
			 * If Pre-Swipe details are present in the memmory, resetting it here
			 * irrespective of result of this transaction
			 */
			if(flushCardDetails(szTranKey) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure while resetting pre-swipe details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		case SCI_ACTIVATE:
		case SCI_ADDVAL:
		case SCI_GIFTCLOSE:
		case SCI_DEACTIVATE:
		case SCI_REACTIVATE:
		case SCI_REMVAL:
			rv = processGiftCardTran(szTranKey, pstSessMap->szSessKey, iCmd, szStatMsg, PAAS_TRUE);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully completed the Gift card transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Completed the Gift Card Transaction");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while completing the gift card transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Complete Gift Card Transaction");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			/*
			 * If Pre-Swipe details are present in the memmory, resetting it here
			 * irrespective of result of this transaction
			 */
			if(flushCardDetails(szTranKey) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure while resetting pre-swipe details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;

		case SCI_BAL:
			rv = processBalanceTran(szTranKey, pstSessMap->szSessKey, szStatMsg);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully completed the Balance Enquiry transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Completed the Balance Enquiry Transaction");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while completing the Balance Enquiry transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Complete Balance Enquiry Transaction");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			/*
			 * If Pre-Swipe details are present in the memmory, resetting it here
			 * irrespective of result of this transaction
			 */
			if(flushCardDetails(szTranKey) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure while resetting pre-swipe details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;

		case SCI_TOKEN_UPDATE:
			debug_sprintf(szDbgMsg, "%s: Processing SCI_TOKEN_UPDATE", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		break;
	}
	/* check below condition for not return current tran general details and amount details.
	 * So clear current transaction general details like troutd, ctroutd..etc and amount details for user cancelled transaction.
	 */
	if( rv == ERR_USR_CANCELED )
	{
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "User Canceled the Payment Transaction");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}

		if( SUCCESS != clearPymntDetailsForUserCancel(szTranKey, szStatMsg) )
		{
			debug_sprintf(szDbgMsg, "%s: Failed to clear pymnt dtls for tran cancelled by user", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: clearPymntDetailsForUserCancel
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

static int clearPymntDetailsForUserCancel(char *szTranKey, char *szStatMsg)
{
	int					rv					= SUCCESS;
	char				szTranStkKey[10]	= "";
	TRAN_PTYPE			pstTran				= NULL;
	CTRANDTLS_STYPE		stCTranDtls;
	AMTDTLS_STYPE   	stAmountDtls;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if( szTranKey == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the reference to the transaction placeholder */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get access to new tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* setting the status message which will be sent as response text to POS*/
			strcpy(szStatMsg, "Device Application Internal Error");

			break;
		}
		memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
		strcpy(szTranStkKey, (char *)pstTran->data); //Getting the SSI stack key

		memset(&stAmountDtls, 0x00, sizeof(AMTDTLS_STYPE));
		rv = getAmtDtlsForPymtTran(szTranStkKey, &stAmountDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get amount details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		/*In case if transaction is approved but user cancelled on signature screen, need to clear approved
		 * amount details from transaction instance for not show approved amount on receipt for
		 * cancelled transaction and update it back.
		 */
		memset(stAmountDtls.apprvdAmt, 0x00, sizeof(stAmountDtls.apprvdAmt));
		if(SUCCESS != updateAmtDtlsForPymtTran(szTranStkKey, &stAmountDtls))
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to reset the amount dtls", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			strcpy(szStatMsg, "Device Application Internal Error");
			break;
		}

		//To clear tran general details from transaction instance, memset it and update it.
		memset(&stCTranDtls, 0x00, sizeof(CTRANDTLS_STYPE));
		if(SUCCESS != updateCurTranDtlsForPymt(szTranStkKey, &stCTranDtls))
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to reset the current tran general dtls", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			strcpy(szStatMsg, "Device Application Internal Error");
			break;
		}
		break;
	}//end of while

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: execBatchTranFlow
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int execBatchTranFlow(char * szTranKey, char * szStatMsg, int iCmd)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		switch(iCmd)
		{
		case SCI_STL:
			debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "SETTLE");
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Processing the BATCH SETTLE Transaction");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			rv = processBatchSettle(szTranKey, szStatMsg);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully initiated the batch settlement", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Initiated the Batch Settlement");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while initiating the batch settlement", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Initiate the Batch Settlement");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Invalid cmd [%d] for BATCH", __FUNCTION__, iCmd);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: execRptTranFlow
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int execReportTranFlow(char * szTranKey, char * szStatMsg, int iCmd)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	switch(iCmd)
	{
	case SCI_DAYSUMM:
		debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "DAY SUMMARY");
		APP_TRACE(szDbgMsg);
		break;
	case SCI_PRESTL:
		debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "PRE-SETTLEMENT");
		APP_TRACE(szDbgMsg);
		break;
	case SCI_STLERR:
		debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "SETTLE ERROR");
		APP_TRACE(szDbgMsg);
		break;
	case SCI_TRANSEARCH:
		debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "TRANSACTION SEARCH");
		APP_TRACE(szDbgMsg);
		break;
	case SCI_STLSUMM:
		debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "SETTLE SUMMARY");
		APP_TRACE(szDbgMsg);
		break;
	case SCI_LASTTRAN:
		debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "LAST TRANSACTION");
		APP_TRACE(szDbgMsg);
		break;
	case SCI_DUPCHK:
		debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "DUPLICATE CHECK");
		APP_TRACE(szDbgMsg);
		break;
	case SCI_CUTOVER:
		debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "CUTOVER");
		APP_TRACE(szDbgMsg);
		break;
	case SCI_SITETOTALS:
		debug_sprintf(szDbgMsg, "%s: Command = [%s]", __FUNCTION__, "SITE TOTALS");
		APP_TRACE(szDbgMsg);
		break;
	default:
		break;
	}

	while(1)
	{
		switch(iCmd)
		{
		case SCI_DAYSUMM:
		case SCI_PRESTL:
		case SCI_STLERR:
		case SCI_TRANSEARCH:
		case SCI_STLSUMM:
		case SCI_CUTOVER:
		case SCI_SITETOTALS:
			rv = getReportSummary(szTranKey, szStatMsg, iCmd);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully fetched the given report details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Fetched the Given Report Details");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while fetching the given report details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed While Fetching the Given Report Details");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			break;
		case SCI_LASTTRAN:
			rv = getLastTransaction(szTranKey, szStatMsg);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully fetched the Last transaction details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Fetched the Last Transaction Details");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while fetching the last transaction details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed While Fetching the Last Transaction Details");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}

			break;
		case SCI_DUPCHK:
			rv = getDuplicateTrans(szTranKey, szStatMsg);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Checked the Duplicate Transaction Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Successfully Checked the Duplicate Transaction Details");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failed While Checking the Duplicate Transaction Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed While Checking the Duplicate Transaction Details");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
			}
			break;
#if 0
		case SCI_LPTOKEN:
			rv = processLPTokenQuery(szTranKey, szStatMsg);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully did the LP Token Query", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while doing the LP Token Query", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
#endif
		default:
			debug_sprintf(szDbgMsg, "%s: Invalid cmd [%d] for REPORT",__FUNCTION__, iCmd);
			APP_TRACE(szDbgMsg);
			break;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPaypalPymtCodeDtlsInSession
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getPaypalPymtCodeDtlsInSession(char *pszPymtCode)
{
	int					rv						= SUCCESS;
	SESSDTLS_PTYPE		pstSess					= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif


	if(pszPymtCode == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Parameters passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		rv = getSession(pstSessMap->szSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return rv;
		}

		strcpy(pszPymtCode, pstSess->stCardDtls.szPayPalPymtCode);
	}
	else
	{
		rv = FAILURE;
		debug_sprintf(szDbgMsg, "%s: No session details!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		/* Should not come here */
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: storePaypalPymtCodeDtlsInSession
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int storePaypalPymtCodeDtlsInSession(char *pszPymtCode)
{
	int					rv						= SUCCESS;
	SESSDTLS_PTYPE		pstSess					= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif


	if(pszPymtCode == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Parameters passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}
	if(pstSessMap == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Session not present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}
	/* Get the transaction reference */
	rv = getSession(pstSessMap->szSessKey, &pstSess);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	strcpy(pstSess->stCardDtls.szPayPalPymtCode, pszPymtCode);

	if(strlen(pszPymtCode) > 0)
	{
		/* Since the Paypal Payment Code is Enter, we consider it as a Preswipe and we do not enable the MSR in LI SCreen */
		pstSess->bPreSwipeDone 	 = PAAS_TRUE;
	}

	return rv;
}
/*
 * ============================================================================
 * Function Name: storePreSwipeCardDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int storePreSwipeCardDtls(CARDDTLS_PTYPE pstCardDtls, PAAS_BOOL bPreSwiped)
{
	int					rv						= SUCCESS;
	int					iStatusMsgDispInterval  = DFLT_STATUS_DISP_TIME;
	int					iMsgPrompt				= 0;
	int					iAppLogEnabled			= isAppLogEnabled();
	char				szStatMsg[100]  		= "";
	char				szTranKey[10]   		= "";
	char				szAppLogData[300]		= "";
	char				szAppLogDiag[300]		= "";
	SESSDTLS_PTYPE		pstSess					= NULL;
	UNSOLMSGINFO_STYPE 	stLocUnsolMsgInfo;
	CTRANDTLS_STYPE		stCurTranDtls;
	PAAS_BOOL			bEmvEnabled;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();

	bEmvEnabled = isEmvEnabledInDevice();

	if(pstCardDtls == NULL)
	{
		 debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		 APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	if(pstSessMap == NULL)
	{
		 debug_sprintf(szDbgMsg, "%s: No Session Map Details", __FUNCTION__);
		 APP_TRACE(szDbgMsg);

		return rv;
	}

	/* Get the Session reference */
	rv = getSession(pstSessMap->szSessKey, &pstSess);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	//if(strcasecmp(pstCardDtls->szPymtMedia, "KOHLS") == 0)
	if(pstCardDtls->bKohlsChargeCard == PAAS_TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: Swiped card is KOHLS card", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "KOHLS Card is Swiped On PreSwipe Screen");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}

		if(isPrivLabelTenderEnabled() == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: PrivLabel Tender is enabled", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Need to get the card token for this card
			 * Need to send Token details in real time to POS
			 * store the card token details
			 */

			memset(szTranKey, 0x00, sizeof(szTranKey));
			/* Push a new SSI TOKEN_QUERY transaction in the stack */
			rv = pushNewSSITran(szTranKey);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to add a new SSI tran", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return rv;
			}

			//set the encryption type here.
			pstCardDtls->iEncType = getEncryptionType();

			updateCardDtlsForPymtTran(szTranKey, pstCardDtls);

			if(bPreSwiped == PAAS_TRUE)
			{
				iMsgPrompt = 0; //It is during Pre-Swipe so we need not show the message prompt
			}
			else
			{
				iMsgPrompt = 1; //It is during consumer option selection so we need show the message prompt
			}

			 /* The below Function Sets the Function Type alone for the SSI Transaction. It is Required to set the Payment Function Type, so that even if
			 * validation of the Payment Transaction fails or Payment Transaction is sent without session, we free the stored Payment Structures in SCI Request parsing.*/
			rv = setFxnNCmdForSSITran(szTranKey, SSI_PYMT, 0);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to set SSI Function", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* setting the status message which will be sent as response text to POS*/
				strcpy(szStatMsg, "Device Application Internal Error");

				popSSITran(szTranKey);
				deleteStack(szTranKey);
				return rv;
			}

			/* Get the CARD_TOKEN for this card */
			rv = getPrivCardToken(szTranKey, iMsgPrompt /* Show the processing message based on preswipe/consumer*/,szStatMsg);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully did the Token Query", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while doing the Token Query", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			memset(&stCurTranDtls, 0x00, sizeof(CTRANDTLS_STYPE));
			rv = getCurTranDtlsForPymt(szTranKey, &stCurTranDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get Current Tran Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				popSSITran(szTranKey);
				return rv;
			}
			memset(&stLocUnsolMsgInfo, 0x00, sizeof(UNSOLMSGINFO_STYPE));

			if(strlen(stCurTranDtls.szCardToken) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Received card token, sending to POS", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* Sending unsolicited response only if card token is present */
				//Storing the POS IP and Port to the local struct
				strcpy(stLocUnsolMsgInfo.szPOSIP, pstSess->stUnsolMsgInfo.szPOSIP);
				strcpy(stLocUnsolMsgInfo.szPOSPort, pstSess->stUnsolMsgInfo.szPOSPort);

				strcpy(stLocUnsolMsgInfo.szCardToken, stCurTranDtls.szCardToken);
				//strcpy(stLocUnsolMsgInfo.szTokenSource, pstSess->stUnsolMsgInfo.szTokenSource);

				/* Send the Token in real time to POS */
				rv = sendSCIUnsolMsg(&stLocUnsolMsgInfo, pstSess->szPOSId, pstSess->iEncFlag, szAppLogDiag);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while sending Kohls card token Details!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Failed to Send [CARD TOKEN] Notification to POS.");
						addAppEventLog(SCA, PAAS_INFO, UNSOLICITED_MESSAGE, szAppLogData, szAppLogDiag);
					}
				}
				else	// MukeshS3: Adding applog here
				{
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Successfully Sent [CARD TOKEN] Notification");
						addAppEventLog(SCA, PAAS_SUCCESS, UNSOLICITED_MESSAGE, szAppLogData, NULL);
					}
				}
				/*
				 * Praveen_P1: Setting preswipe done only when card token is present in the token_query
				 * command when the KOHLS card is swiped during pre-swipe
				 */
				if(bPreSwiped == PAAS_TRUE)
				{
					pstSess->bPreSwipeDone = PAAS_TRUE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Card Token is not present!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = CARD_TOKEN_NOT_PRESENT;
			}

			popSSITran(szTranKey);
			deleteStack(szTranKey);

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: PrivLabel Tender is NOT enabled", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			showPSGenDispForm(MSG_CARD_NOTACCEPTED, FAILURE_ICON, iStatusMsgDispInterval);

			if(bPreSwiped == PAAS_TRUE)
			{
				//This card is swiped during pre-swipe

			}
			else
			{
				//This card is swiped during consumer option(priv-card)
			}
		}
	}
	else if( isWalletEnabled() &&
			 (pstCardDtls->pstVasDataDtls != NULL && pstCardDtls->pstVasDataDtls->bVASPresent == PAAS_TRUE) &&
			 (pstSess->pstVasDataDtls == NULL || pstSess->pstVasDataDtls->bVASPresent == PAAS_FALSE ))
	{
		//If data is already received No need to store and send again in session
		debug_sprintf(szDbgMsg, "%s: Apple Pay VAS Data Received on PreSwipe Screen", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Apple Pay VAS Loyalty Data Received On PreSwipe Screen");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}

		if(pstSess->pstVasDataDtls == NULL)
		{
			pstSess->pstVasDataDtls = (VASDATA_DTLS_PTYPE) malloc(sizeof(VASDATA_DTLS_STYPE));
			if(pstSess->pstVasDataDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get Allocate Memory for VAS DATA", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return FAILURE;
			}
		}
		memset(pstSess->pstVasDataDtls, 0x00 , sizeof(VASDATA_DTLS_STYPE));

		/* Need to send VAS details in real time to POS*/
		memset(&stLocUnsolMsgInfo, 0x00, sizeof(UNSOLMSGINFO_STYPE));

		if( (strlen(pstSess->stUnsolMsgInfo.szPOSIP) > 0) && (strlen(pstSess->stUnsolMsgInfo.szPOSPort) > 0) )
		{
			/* Sending unsolicited response only if POS IP and PORT is present */
			//Storing the POS IP and Port to the local struct
			strcpy(stLocUnsolMsgInfo.szPOSIP, pstSess->stUnsolMsgInfo.szPOSIP);
			strcpy(stLocUnsolMsgInfo.szPOSPort, pstSess->stUnsolMsgInfo.szPOSPort);

			debug_sprintf(szDbgMsg, "%s: Received POS IP [%s] POS Port [%s], sending to POS", __FUNCTION__, stLocUnsolMsgInfo.szPOSIP,stLocUnsolMsgInfo.szPOSPort);
			APP_TRACE(szDbgMsg);

			strcpy(stLocUnsolMsgInfo.szPubliserName, pstCardDtls->pstVasDataDtls->szPubliserName);
			strcpy(stLocUnsolMsgInfo.szVASLoyaltyData, pstCardDtls->pstVasDataDtls->szLoyaltyData);

			/* Send the VAS Data in real time to POS */
			rv = sendSCIUnsolMsg(&stLocUnsolMsgInfo, pstSess->szPOSId, pstSess->iEncFlag, szAppLogDiag);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while sending VAS Data to POS!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error While Sending VAS Loyalty Data To POS, Storing in Session to Process Later.");
					addAppEventLog(SCA, PAAS_INFO, UNSOLICITED_MESSAGE, szAppLogData, szAppLogDiag);
				}

				pstCardDtls->pstVasDataDtls->bVASsentToPOS = PAAS_FALSE;
				memcpy(pstSess->pstVasDataDtls, pstCardDtls->pstVasDataDtls, sizeof(VASDATA_DTLS_STYPE));
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: VAS Data Sent to POS successfully", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "VAS Loyalty Data Sent To POS Successfully");
					addAppEventLog(SCA, PAAS_INFO, UNSOLICITED_MESSAGE, szAppLogData, NULL);
				}

				pstSess->pstVasDataDtls->bVASsentToPOS = PAAS_TRUE;
				pstSess->pstVasDataDtls->bVASPresent = PAAS_TRUE;
				//pstSess->bisVASDataSentToPos = PAAS_TRUE;				/*Once VAS got in Pre-Swipe, making the Session Variable bisVASDataSentToPos to TRUE, so Throughtout the Session it remains the Same*/
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: POS IP or/and PORT Missing Not Sending VAS data to POS, Storing in Session to Process Later", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "POS IP or/and PORT Missing, Not Sending VAS Loyalty Data To POS, Storing in Session to Process Later");
				addAppEventLog(SCA, PAAS_INFO, UNSOLICITED_MESSAGE, szAppLogData, NULL);
			}

			pstCardDtls->pstVasDataDtls->bVASsentToPOS = PAAS_FALSE;
			memcpy(pstSess->pstVasDataDtls, pstCardDtls->pstVasDataDtls, sizeof(VASDATA_DTLS_STYPE));
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Swiped card is NON-KOHLS card and NOT VAS Data Or VAS Already Received [%d]", __FUNCTION__, pstCardDtls->iCardSrc);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			if(pstCardDtls->iCardSrc == CRD_MSR || pstCardDtls->iCardSrc == CRD_EMV_FALLBACK_MSR)
			{
				strcpy(szAppLogData, "Card is Swiped On PreSwipe Screen");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}
			else if(pstCardDtls->iCardSrc == CRD_EMV_MSD_CTLS || pstCardDtls->iCardSrc == CRD_RFID)
			{
				strcpy(szAppLogData, "Card is Tapped On PreSwipe Screen");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}

		/*
		 * This function is called when Pre-Swipe is done and Priv label is selected
		 * from the consumer option.
		 * For Pre-swipe other cards are accepted, for Priv-Label only Kohls card are
		 * accepted
		 */
		if(bPreSwiped == PAAS_TRUE)
		{
			memset(&pstSess->stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

			/*For NON-KOHLS card, we are storing card details */

			memcpy(&pstSess->stCardDtls, pstCardDtls, sizeof(CARDDTLS_STYPE));

			debug_sprintf(szDbgMsg, "%s: Stored card details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(&stLocUnsolMsgInfo, 0x00, sizeof(UNSOLMSGINFO_STYPE));

			debug_sprintf(szDbgMsg, "%s: Received pre-swipe data, sending notification to POS", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Sending unsolicited response that card is pre-swiped */
			//Storing the POS IP and Port to the local struct
			strcpy(stLocUnsolMsgInfo.szPOSIP, pstSess->stUnsolMsgInfo.szPOSIP);
			strcpy(stLocUnsolMsgInfo.szPOSPort, pstSess->stUnsolMsgInfo.szPOSPort);

			stLocUnsolMsgInfo.iPreSwiped = 1;

			/* MukeshS3: 10-Feb-16: FRD 3.75.1
			 * We need to extend this unsolicited message with the card details captured on preswipe, which is going to be
			 * sent to the POS on given IP & PORT. So, we will populate the raw card details from the CARDDTLS_STYPE structure & store the necessary
			 * details to a local stLocUnsolMsgInfo.pstRawCrdDtls structure according to the PCI check on PAN.
			 */
			if((strlen(stLocUnsolMsgInfo.szPOSIP) > 0) && (strlen(stLocUnsolMsgInfo.szPOSPort) > 0))
			{
				rv = getPymtMediafromCDTandEnabledTndr(pstCardDtls);
				if(rv == ERR_NOT_IN_BINRANGE)
				{
					//TODO:
					debug_sprintf(szDbgMsg, "%s: Invalid card swiped; Not in Bin range", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				rv = getRawCardDtls(&(stLocUnsolMsgInfo.pstRawCrdDtls), pstCardDtls, PAAS_TRUE);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get Raw Card dtls",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

			}

			/* Send the status that Card is pre-swiped in real time to POS */
			rv = sendSCIUnsolMsg(&stLocUnsolMsgInfo, pstSess->szPOSId, pstSess->iEncFlag, szAppLogDiag);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while sending Pre-Swiped Info Details!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				pstSess->iEarlyCardDtlsStatus = DATA_SEND_ERROR_ON_PRESWIPE;
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Send [PRE-SWIPE] Notification to POS.");
					addAppEventLog(SCA, PAAS_INFO, UNSOLICITED_MESSAGE, szAppLogData, szAppLogDiag);
				}
			}
			else
			{
				pstSess->iEarlyCardDtlsStatus = DATA_SENT_ON_PRESWIPE;
				if(iAppLogEnabled == 1)
				{
					if(stLocUnsolMsgInfo.pstRawCrdDtls != NULL)
					{
						strcpy(szAppLogData, "Successfully Sent [PRE-SWIPE EARLY] Card Information");
						addAppEventLog(SCA, PAAS_SUCCESS, UNSOLICITED_MESSAGE, szAppLogData, NULL);
					}
					else
					{
						strcpy(szAppLogData, "Successfully Sent [PRE-SWIPE] Notification");
						addAppEventLog(SCA, PAAS_SUCCESS, UNSOLICITED_MESSAGE, szAppLogData, NULL);
					}
				}
			}
			/* After sending card data back to POS, we need to free the allocated memory for raw card data */
			if(stLocUnsolMsgInfo.pstRawCrdDtls != NULL)
			{
				free(stLocUnsolMsgInfo.pstRawCrdDtls);
			}
			//Storing in the session instance
			pstSess->stUnsolMsgInfo.iPreSwiped = stLocUnsolMsgInfo.iPreSwiped;

			//Setting Pre-Swipe flag
			pstSess->bPreSwipeDone = PAAS_TRUE;

			if(bEmvEnabled == PAAS_TRUE && pstCardDtls->bEmvData == PAAS_TRUE && pstCardDtls->iCardSrc == CRD_EMV_CT)
			{
				pstSess->iEMVCrdPresence = 1;
			}
			else
			{
				pstSess->iEMVCrdPresence = 0;
			}
		}
		else
		{
			//control will not come here!!!
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: flushCardDetails
 *
 * Description	: This function flushes the card details
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int flushCardDetails(char *pszTranKey)
{
	int					rv						= SUCCESS;
	int					iCmd					= 0;
	int					iFxn					= 0;
	char				szTranStkKey[10]		= "";
	PAAS_BOOL			bDupTranDetectedflg		= PAAS_FALSE;
	PAAS_BOOL			bPostAuthCardDtls		= PAAS_FALSE;
	PAAS_BOOL			bRetainPreSwipeDtls		= PAAS_FALSE;
	TRAN_PTYPE			pstTran					= NULL;
	SESSDTLS_PTYPE		pstSess					= NULL;
	VASDATA_DTLS_STYPE	stVasDataDtls;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Get the reference to the transaction placeholder */
		rv = getTran(pszTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get access to new tran", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(szTranStkKey, 0x00, sizeof(szTranStkKey));

		if(pstTran->data != NULL)
		{
			strcpy(szTranStkKey, (char *)pstTran->data); //Getting the SSI stack key
		}

		if(pstSessMap == NULL)
        {
               debug_sprintf(szDbgMsg, "%s: No Session Map Details", __FUNCTION__);
               APP_TRACE(szDbgMsg);
               break;
        }

        /* Get the transaction reference */
        rv = getSession(pstSessMap->szSessKey, &pstSess);
        if(rv != SUCCESS || pstSess == NULL)
        {
               debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
               APP_TRACE(szDbgMsg);

               rv = FAILURE;
               break;
        }

        if(isWalletEnabled())
        {
            /* Get the VAS Data */
            rv = getVASDtlsForPymtTran(szTranStkKey, &stVasDataDtls);
            if(rv != SUCCESS)
            {
                   debug_sprintf(szDbgMsg, "%s: FAILED to get VAS Data", __FUNCTION__);
                   APP_TRACE(szDbgMsg);

                   rv = FAILURE;
                   break;
            }
            /*
             * We have received Card data and VAS from PreSwipe but not send still back. After sending VAS Data back to POS
             * bVASsentToPOS will be True, so we will use the preswipe carddtls if present, in the next command but
             * after that command ends we will clear the preswipe carddtls, and so for all subsequent request, card dtls won't be there
             *
             */
            if(stVasDataDtls.bVASPresent == PAAS_TRUE && stVasDataDtls.bVASsentToPOS == PAAS_FALSE)
            {
            	bRetainPreSwipeDtls = PAAS_TRUE;
            	debug_sprintf(szDbgMsg, "%s: Retaining VAS Dtls in Session ", __FUNCTION__);
            	APP_TRACE(szDbgMsg);
            }
        }

        // MukeshS3: FRD 3.75.2: If early captured card details are stored in the session, than we are not supposed to flush the card details
        // rather we will use it for very next payment command.
        if(pstSess->iEarlyCardDtlsStatus == DATA_PRESENT)
        {
        	bRetainPreSwipeDtls = PAAS_TRUE;
        }

        if((bRetainPreSwipeDtls == PAAS_FALSE))
        {
    		//flushing the card details
    		memset(&pstSess->stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
    		//Flush the Backed up card details
    		if(resetBkupDtlsInSession() != SUCCESS)
    		{
    			debug_sprintf(szDbgMsg, "%s: FAILED to Reset the Backup Card Details", __FUNCTION__);
    			APP_TRACE(szDbgMsg);
    		}
    		pstSess->iEMVCrdPresence = 0;
    		/* Setting PreSwiped flag to 0 since we flushed pre-swipe card details */
    		pstSess->bPreSwipeDone = PAAS_FALSE;

    		pstSess->iEarlyCardDtlsStatus = DATA_EMPTY; // reset the status of early captured card details, if present

    		/*AjayS2: 22/Jan/2016: USRS00022129 CA 3.0 SCA 4.0 SP7
    		 * If POS got disconnected(sec port cancel etc.) after inserting a different language card, we need to switch back to default
    		 * terminal language, so that on next transaction, def language will come
    		 */
    		setLangId(pstSess->iDefLangBeforeEMV);

/*Akshaya: 25-07-15: Flushing the VAS details only when RetainPreSwipeDtls is FALSE*/
    		if(pstSess->pstVasDataDtls != NULL)
    		{
    			//Flush and free VAS Data
    			free(pstSess->pstVasDataDtls);
    			//memset(&pstSess->stVasDataDtls, 0x00, sizeof(VASDATA_DTLS_STYPE));
    		}
        }
		//flushing the  card token details
		memset(pstSess->stUnsolMsgInfo.szCardToken, 0x00, sizeof(pstSess->stUnsolMsgInfo.szCardToken));
		memset(pstSess->stUnsolMsgInfo.szTokenSource, 0x00, sizeof(pstSess->stUnsolMsgInfo.szTokenSource));
		memset(pstSess->stUnsolMsgInfo.szEmail, 0x00, sizeof(pstSess->stUnsolMsgInfo.szEmail));
		memset(pstSess->stCardDtls.szPayPalPymtCode, 0x00, sizeof(pstSess->stCardDtls.szPayPalPymtCode));


		pstSess->stUnsolMsgInfo.iPreSwiped = 0;
		pstSess->stUnsolMsgInfo.iPayWithPaypal = 0;

		pstSess->stUnsolMsgInfo.iEmailOffer = 0;
		pstSess->stUnsolMsgInfo.iEmailReceipt = 0;
		pstSess->stUnsolMsgInfo.iGiftReceipt = 0;


		/*T_RaghavendranR1:Fix for If Gift Receipt is selected and then a partial tender is completed the Gift Receipt button shows unselected.
		 *(This was an issue in Phase 2 release also).
		 *
		 */
		/* Setting enabled consumer option in the list to 0*/
		//for(iIndex = 0; iIndex < MAX_CONSUMER_OPTS; iIndex++)
		//{
			//rv = setConsOptSelected(iIndex, 0);
		//}

		//Clearing Gift Card Dtls if current Command is Not a Gift Balance Command
		if(isStoreCardDtlsForGiftCloseEnabled())
		{
			getFxnNCmdForSSITran(szTranStkKey, &iFxn, &iCmd);

			if(iCmd != SSI_BAL && pstSess->pstPrevGiftTranCardDtls != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Freeing Gift Balance Stored Card details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				free(pstSess->pstPrevGiftTranCardDtls);
			}
		}

		//Clearing Post Auth Card Dtls If Present
		if(isStoreCardDtlsForPostAuthEnabled())
		{
			getPostAuthCardDtlsIndicatorForPymtTran(szTranStkKey, &bPostAuthCardDtls);

			if(bPostAuthCardDtls ==  PAAS_FALSE && pstSess->pstPostAuthTransCardDtls != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Freeing Post Auth Stored Card details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				free(pstSess->pstPostAuthTransCardDtls);
			}
		}
		if( isDupSwipeDetectEnabled() )
		{
			if(strlen(szTranStkKey) > 0)
			{
				//Check whether duplicate swipe detected, If not flush the previously stored dup card details.
				getDupTranIndicatorForPymtTran( szTranStkKey, &bDupTranDetectedflg);
				if( bDupTranDetectedflg == PAAS_FALSE )
				{
					if(pstSess->pstDupSwipeCardDtls != NULL)
					{
						//flushing the duplicate card details
						//memset(pstSess->pstDupSwipeCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

						free(pstSess->pstDupSwipeCardDtls);
					}
					//Set the payment type to -1.
					if(pstSess->iEarlyCardDtlsStatus !=  DATA_PRESENT)	// need to retain payment type for next command
					{
						pstSess->iPaymentType = -1;
					}

					debug_sprintf(szDbgMsg, "%s: flushing dup card details", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: flushDupSwipeCardDetails
 *
 * Description	: This function flushes the dup-swipe card details
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int flushDupSwipeCardDetails(int* piDupSwipeFlushed)
{
	int					rv					= SUCCESS;
	SESSDTLS_PTYPE		pstSess				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstSessMap == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Session Map Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		/* Get the transaction reference */
		rv = getSession(pstSessMap->szSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//flushing the card details
		//if( (pstSess->pstDupSwipeCardDtls != NULL) && (strlen(pstSess->pstDupSwipeCardDtls->szTrackData) > 0 || strlen(pstSess->stDupSwipeCardDtls->szPAN) > 0))
		if(pstSess->pstDupSwipeCardDtls != NULL)
		{
			free(pstSess->pstDupSwipeCardDtls);
			//memset(&pstSess->stDupSwipeCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
			*piDupSwipeFlushed = PAAS_TRUE;
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: flushPreSwipeCardDetails
 *
 * Description	: This function flushes the pre-swipe card details
 *
 * Input Params	: bRetainPreTapVasData-> It will tell that we have clear only Card
 * 					data or both card and VAS Data
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int flushPreSwipeCardDetails(PAAS_BOOL bRetainPreTapVasData)
{
	int					rv					= SUCCESS;
	SESSDTLS_PTYPE		pstSess				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- Clear VAS Data[%s]", __FUNCTION__, bRetainPreTapVasData?"NO":"YES");
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstSessMap == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Session Map Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		/* Get the transaction reference */
		rv = getSession(pstSessMap->szSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//flushing the card details
		memset(&pstSess->stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

		pstSess->bPreSwipeDone 	 = PAAS_FALSE;
		pstSess->iEMVCrdPresence = PAAS_FALSE;
		// Reset the Early Card details present indicator in session
		pstSess->iEarlyCardDtlsStatus = DATA_EMPTY;
		if(bRetainPreTapVasData == PAAS_FALSE && pstSess->pstVasDataDtls != NULL)
		{
			free(pstSess->pstVasDataDtls);
			//memset(&pstSess->stVasDataDtls, 0x00, sizeof(VASDATA_DTLS_STYPE));
		}
		setEMVFallbackMode(PAAS_FALSE);

		//Flush the Backed up card details
		if(resetBkupDtlsInSession() != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to Reset the Backup Card Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
#if 0
		//flushing the  card token details
		memset(pstSess->stUnsolMsgInfo.szCardToken, 0x00, sizeof(pstSess->stUnsolMsgInfo.szCardToken));
		memset(pstSess->stUnsolMsgInfo.szTokenSource, 0x00, sizeof(pstSess->stUnsolMsgInfo.szTokenSource));
#endif
		/* Setting PreSwiped flag to 0 since we flushed pre-swipe card details */
		pstSess->stUnsolMsgInfo.iPreSwiped = 0;
		pstSess->stUnsolMsgInfo.iPayWithPaypal = 0;

		setLangId(pstSess->iDefLangBeforeEMV);
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isPreSwipeDtlsPresentInSession
 *
 * Description	: To Check Whether the session contains Pre Swipe Data
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isPreSwipeDtlsPresentInSession()
{
	SESSDTLS_PTYPE		pstSess			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return PAAS_FALSE;
		}
	}
	else
	{
		/* No Session */
		return PAAS_FALSE;
	}
	/* If duplicate transaction is detected or pre-swipe is done; then application needs to show
	 * "processing please... wait for cashier" msg to customer.
	 */
	if(strlen(pstSess->stCardDtls.szTrackData) > 0 )
	{
		/* Track data Present in the Session */
		return PAAS_TRUE;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/*
 * ============================================================================
 * Function Name: isDupCardDtlsPresentInSession
 *
 * Description	: To Check Whether the session contains Dup Card Data
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isDupCardDtlsPresentInSession()
{
	SESSDTLS_PTYPE		pstSess			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return PAAS_FALSE;
		}
	}
	else
	{
		/* No Session */
		return PAAS_FALSE;
	}

	if(pstSess->pstDupSwipeCardDtls != NULL)
	{
		/* Duplicate Card data Present in the Session */
		return PAAS_TRUE;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/*
 * ============================================================================
 * Function Name: setPreSwipeDoneFlag
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setPreSwipeDoneFlag(PAAS_BOOL bPreSwipeDone)
{
	SESSDTLS_PTYPE		pstSess			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			 /* There is no session existing.*/
			return;
		}
	}
	else
	{
		 /* No session existing.*/
		return;
	}

	pstSess->bPreSwipeDone = bPreSwipeDone;

	return;
}

/*
 * ============================================================================
 * Function Name: isPreSwipeDone
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isPreSwipeDone()
{
	SESSDTLS_PTYPE		pstSess			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* KranthiK1:
			 * There is no session existing. Assuming that preswipe is done.
			 */
			return PAAS_FALSE;
		}
	}
	else
	{
		/* KranthiK1:
		 * There is no session existing. Assuming that preswipe is done.
		 */
		return PAAS_FALSE;
	}

	debug_sprintf(szDbgMsg, "%s: PreSwipe [%s]", __FUNCTION__,
							 					(pstSess->bPreSwipeDone == PAAS_TRUE) ? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	return(pstSess->bPreSwipeDone);
}

/*
 * ============================================================================
 * Function Name: EMVCrdPresenceOnPreswipe
 *
 * Description	: Used to check the card presence on Preswipe
 *
 * Input Params	:
 *
 * Output Params: int
 * ============================================================================
 */
int EMVCrdPresenceOnPreswipe()
{	SESSDTLS_PTYPE		pstSess			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
	}
	else
	{
		return FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, pstSess->iEMVCrdPresence);
	APP_TRACE(szDbgMsg);

	return(pstSess->iEMVCrdPresence);
}

/*
 * ============================================================================
 * Function Name: isVASDataCaptured
 *
 * Description	: Used to check whether VAS Details Captured or not
 *
 * Input Params	:
 *
 * Output Params: Negative Value if Session Not Present
 * 				  0 = if Session Present But VAS not Captured
 * 				  1 = if Session Present And VAS Captured
 * ============================================================================
 */
PAAS_BOOL isVASDataCaptured()
{
	SESSDTLS_PTYPE		pstSess			= NULL;
	PAAS_BOOL			bisVasPresent	= PAAS_FALSE;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Session Dtls from session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
	}
	else
	{
		return FAILURE;
	}

	if(pstSess->pstVasDataDtls != NULL)
	{
		bisVasPresent = pstSess->pstVasDataDtls->bVASPresent;
	}
	else
	{
		bisVasPresent = PAAS_FALSE;
	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, bisVasPresent);
	APP_TRACE(szDbgMsg);

	return bisVasPresent;
//	return(pstSess->stVasDataDtls.bVASPresent);
}
#if 0
/* ==========================================================================
* Function Name: isVASDataSentToPOS
*
* Description	: Used to check whether VAS sent to POS or not
*
* Input Params	:
*
* Output Params: Negative Value if Session Not Present
* 				  0 = if Session Present But VAS not Sent
* 				  1 = if Session Present And VAS Sent
* ============================================================================
*/
PAAS_BOOL isVASDataSentToPOS()
{
	SESSDTLS_PTYPE		pstSess			= NULL;
	PAAS_BOOL			bisVasSent	= PAAS_FALSE;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Session Dtls from session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
	}
	else
	{
		return FAILURE;
	}

	if(pstSess != NULL)
	{
		bisVasSent = pstSess->bisVASDataSentToPos;
	}
	else
	{
		bisVasSent = PAAS_FALSE;
	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, bisVasSent);
	APP_TRACE(szDbgMsg);

	return bisVasSent;
}
#endif

/*
 * ============================================================================
 * Function Name: isOnWelcomeScreenAfterVAS
 *
 * Description	:	Used to check that VAS is sent to POS and we are waiting
 * 					for next command on Welcome Screen or not
 *
 * Input Params	:
 *
 * Output Params: Negative Value if Session Not Present
 * 				  0 = if Session Present But not On Welcome After VAS
 * 				  1 = if Session Present And On Welcome After VAS
 * ============================================================================
 */
PAAS_BOOL isOnWelcomeScreenAfterVAS()
{
	SESSDTLS_PTYPE		pstSess						= NULL;
	PAAS_BOOL			bisOnWelcomeScreenAfterVAS	= PAAS_FALSE;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Session Dtls from session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
	}
	else
	{
		return FAILURE;
	}

	if(pstSess->pstVasDataDtls != NULL)
	{
		bisOnWelcomeScreenAfterVAS = pstSess->pstVasDataDtls->bOnWelcomeScreenAfterVAS;
	}
	else
	{
		bisOnWelcomeScreenAfterVAS = PAAS_FALSE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, bisOnWelcomeScreenAfterVAS);
	APP_TRACE(szDbgMsg);

	return bisOnWelcomeScreenAfterVAS;
//	return(pstSess->stVasDataDtls.bOnWelcomeScreenAfterVAS);
}

/*
 * ============================================================================
 * Function Name: isOnWelcomeScrnAfterEarlyCardCptr
 *
 * Description	:	Used to check that early card data is captured & sent & waiting for next payment command
 *
 * Input Params	:
 *
 * Output Params: Negative Value if Session Not Present
 * 				  0 = if Session Present But not On Welcome screen
 * 				  1 = if Session Present And On Welcome screen
 * ============================================================================
 */
PAAS_BOOL isOnWelcomeScrnAfterEarlyCardCptr()
{
	SESSDTLS_PTYPE		pstSess								= NULL;
	PAAS_BOOL			bisOnWelcomeScrnAfterEarlyCardCptr	= PAAS_FALSE;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Session Dtls from session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
	}
	else
	{
		return FAILURE;
	}

	if(pstSess->iEarlyCardDtlsStatus == DATA_PRESENT)
	{
		bisOnWelcomeScrnAfterEarlyCardCptr = PAAS_TRUE;
	}
	else
	{
		bisOnWelcomeScrnAfterEarlyCardCptr = PAAS_FALSE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, bisOnWelcomeScrnAfterEarlyCardCptr);
	APP_TRACE(szDbgMsg);

	return bisOnWelcomeScrnAfterEarlyCardCptr;
}

/*
 * ============================================================================
 * Function Name: isProvisionSentDuringPymt
 *
 * Description	: Used to check whether Provision Sent On Payment Or not
 *
 * Input Params	:
 *
 * Output Params: Negative Value if Session Not Present
 * 				  0 = if Session Present But Provision not Sent
 * 				  1 = if Session Present And Provision Sent
 * ============================================================================
 */
PAAS_BOOL isProvisionSentDuringPymt()
{
	SESSDTLS_PTYPE		pstSess				= NULL;
	PAAS_BOOL			bProvisionPassSent	= PAAS_FALSE;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Session Dtls from session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
	}
	else
	{
		return FAILURE;
	}

	if(pstSess->pstVasDataDtls != NULL)
	{
		bProvisionPassSent = pstSess->pstVasDataDtls->bProvisionPassSent;
	}
	else
	{
		bProvisionPassSent = PAAS_FALSE;
	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, bProvisionPassSent);
	APP_TRACE(szDbgMsg);

	return bProvisionPassSent;
}

/*
 * ============================================================================
 * Function Name: setProvisionSentDuringPymt
 *
 * Description	: Used to set Provision Sent On Payment Or not
 *
 * Input Params	: PAAS_BOOL
 *
 * Output Params:
 * ============================================================================
 */
int setProvisionSentDuringPymt(PAAS_BOOL bProvisionPassSent)
{
	SESSDTLS_PTYPE		pstSess				= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Session Dtls from session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
	}
	else
	{
		return FAILURE;
	}

	if(pstSess->pstVasDataDtls == NULL)
	{
		pstSess->pstVasDataDtls = (VASDATA_DTLS_PTYPE) malloc(sizeof(VASDATA_DTLS_STYPE));
		if(pstSess->pstVasDataDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to Allocate Memory For Vas Data", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
		memset(pstSess->pstVasDataDtls, 0x00 , sizeof(VASDATA_DTLS_STYPE));
	}

	pstSess->pstVasDataDtls->bProvisionPassSent = bProvisionPassSent;

	debug_sprintf(szDbgMsg, "%s: bProvisionPassSent Set to %d", __FUNCTION__, bProvisionPassSent);
	APP_TRACE(szDbgMsg);
	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: isInEMVFallbackMode
 *
 * Description	: Used to check whether On Fallback Mode or Not
 *
 * Input Params	:
 *
 * Output Params: Negative Value if Session Not Present
 * 				  0 = if Session Present But not in Fallback
 * 				  1 = if Session Present And in Fallback
 * ============================================================================
 */
PAAS_BOOL isInEMVFallbackMode()
{
	SESSDTLS_PTYPE		pstSess			= NULL;
	PAAS_BOOL			bIsInFallback	= PAAS_FALSE;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	/*
	 * Praveen_P1: 26 August 2016
	 * Modified this function slightly to
	 * fix Coverity CID# 83358
	 */
	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			bIsInFallback = PAAS_FALSE;
		}
		else
		{
			if(pstSess == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get Session Dtls from session", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				bIsInFallback = PAAS_FALSE;
			}
			else
			{
				bIsInFallback = pstSess->bInFallbackMode;
			}
		}
	}
	else
	{
		bIsInFallback = PAAS_FALSE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, bIsInFallback);
	APP_TRACE(szDbgMsg);

	return bIsInFallback;
}

/*
 * ============================================================================
 * Function Name: setEMVFallbackMode
 *
 * Description	: Used to set Fallback Mode
 *
 * Input Params	: BOOL
 *
 * Output Params: Negative Value if Session Not Present
 * 				  0 = Success
 * 				  Other = Failure
 * ============================================================================
 */
int setEMVFallbackMode(PAAS_BOOL bFallbackMode)
{
	SESSDTLS_PTYPE		pstSess				= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Session Dtls from session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
	}
	else
	{
		return FAILURE;
	}

	pstSess->bInFallbackMode = bFallbackMode;

	debug_sprintf(szDbgMsg, "%s: bFallbackMode Set to %d", __FUNCTION__, bFallbackMode);
	APP_TRACE(szDbgMsg);
	return SUCCESS;
}
/*
 * ============================================================================
 * Function Name: getMerchantIndexIfAvaliable
 *
 * Description	:This Function gets the Merchant Index from Session.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getMerchantIndexIfAvaliable(char* szMerchantIndex)
{
	SESSDTLS_PTYPE			pstSess						= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s:Entering", __FUNCTION__);
					APP_TRACE(szDbgMsg);
	// CID-67297: 3-Feb-16: MukeshS3: it is being taken care in the caller function, because we dont have buffer size here to memset
	//memset(szMerchantIndex, 0x00, sizeof(szMerchantIndex));

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Session Dtls from session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
	}
	else
	{
		return FAILURE;
	}

	strcpy(szMerchantIndex, pstSess->szMerchantIndex);

	debug_sprintf(szDbgMsg, "%s: Returning %s", __FUNCTION__, szMerchantIndex);
	APP_TRACE(szDbgMsg);

	return SUCCESS;
}
/*
 * ============================================================================
 * Function Name: getVASTermModeIfAvaliable
 *
 * Description	:This Function gets VAS Terminal Mode from Session
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getVASTermModeIfAvaliable(char* szNFCVASMode)
{
	SESSDTLS_PTYPE			pstSess						= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s:Entering", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Session Dtls from session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
	}
	else
	{
		return FAILURE;
	}

	strcpy(szNFCVASMode, pstSess->szNFCVASMode);

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, szNFCVASMode);
	APP_TRACE(szDbgMsg);

	return SUCCESS;
}
/*
 * ============================================================================
 * Function Name: isCustomerWaitingForCashier
 *
 * Description	: Returns TRUE if customer is waiting for cashier else FALSE.
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isCustomerWaitingForCashier()
{
	SESSDTLS_PTYPE		pstSess			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return PAAS_FALSE;
		}
	}
	else
	{
		return PAAS_FALSE;
	}
	/* If duplicate transaction is detected or pre-swipe is done; then application needs to show
	 * "processing please... wait for cashier" msg to customer.
	 */
	if( ( (pstSess->pstDupSwipeCardDtls != NULL) && (strlen(pstSess->pstDupSwipeCardDtls->szTrackData) > 0 )) ||
		 ( pstSess->bPreSwipeDone == PAAS_TRUE) )
	{
		//Customer is waiting for cashier.
		return PAAS_TRUE;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/*
 * ============================================================================
 * Function Name: storeandSendConsOptInfo
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int storeandSendConsOptInfo(UNSOLMSGINFO_PTYPE pstUnsolMsgInfo, int iOption, char * szAppLogDiag)
{
	int					rv				= SUCCESS;
	SESSDTLS_PTYPE		pstSess			= NULL;
	UNSOLMSGINFO_STYPE 	stLocUnsolMsgInfo;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSessMap == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Session not present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}
	/* Get the transaction reference */
	rv = getSession(pstSessMap->szSessKey, &pstSess);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	/*
	 * Storing the current option and server IP/Port details
	 * to the local struct so that this struct can be used
	 * to frame the response to POS
	 */
	memset(&stLocUnsolMsgInfo, 0x00, sizeof(UNSOLMSGINFO_STYPE));

	memcpy(&stLocUnsolMsgInfo, pstUnsolMsgInfo, sizeof(UNSOLMSGINFO_STYPE));

	strcpy(stLocUnsolMsgInfo.szPOSIP, pstSess->stUnsolMsgInfo.szPOSIP);
	strcpy(stLocUnsolMsgInfo.szPOSPort, pstSess->stUnsolMsgInfo.szPOSPort);

	/*
	 * Storing the info in the main session tran instance
	 */
	switch(iOption)
	{
	case GIFTRECEIPT_OPT:
		pstSess->stUnsolMsgInfo.iGiftReceipt = pstUnsolMsgInfo->iGiftReceipt;
		break;
	case EMAILRECEIPT_OPT:
		pstSess->stUnsolMsgInfo.iEmailReceipt = pstUnsolMsgInfo->iEmailReceipt;
		strcpy(pstSess->stUnsolMsgInfo.szEmail, pstUnsolMsgInfo->szEmail);
		break;
	case EMAILOFFER_OPT:
		pstSess->stUnsolMsgInfo.iEmailOffer = pstUnsolMsgInfo->iEmailOffer;
		strcpy(pstSess->stUnsolMsgInfo.szEmail, pstUnsolMsgInfo->szEmail);
		break;
	case PRIVCARD_OPT:
		break;
	default:
		break;
	}

	rv = sendSCIUnsolMsg(&stLocUnsolMsgInfo, pstSess->szPOSId, pstSess->iEncFlag, szAppLogDiag);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while sending Consumer Option Details!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: udpatePaymentCmd
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int udpatePaymentCmd(char *pszTranKey, int *piUpdatedCmd)
{
	int				rv						= SUCCESS;
	TRAN_PTYPE		pstTran					= NULL;
	char			szTranStkKey[10]		= "";
	FTRANDTLS_STYPE	stFTranDtls;
	int				iCurrentCmd				= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pszTranKey == NULL || piUpdatedCmd == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL param passed!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Get the reference to the transaction placeholder */
		rv = getTran(pszTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get access to new tran", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
		strcpy(szTranStkKey, (char *)pstTran->data); //Getting the SSI stack key

		/* Getting the following details */
		memset(&stFTranDtls, 0x00, sizeof(FTRANDTLS_STYPE));
		rv = getFollowOnDtlsForPymt(szTranStkKey, &stFTranDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failure while getting the Following details from SSI tran instance!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		iCurrentCmd = pstTran->iCmd;

		debug_sprintf(szDbgMsg, "%s: Current Command is %d", __FUNCTION__, iCurrentCmd);
		APP_TRACE(szDbgMsg);

		switch(iCurrentCmd)
		{
		case SCI_AUTH:
			/*
			 * If AUTH CODE is present in the request, it
			 * becomes VOICE AUTH command, if it is not present
			 * then it is PRE AUTH command
			 */
			if(strlen(stFTranDtls.szAuthCode) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Auth code is present, setting as VOICE AUTH", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				*piUpdatedCmd = SCI_VOICEAUTH;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Auth code is NOT present, setting as PRE AUTH", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				*piUpdatedCmd = SCI_PREAUTH;
			}
			break;
		case SCI_CAPTURE:
			/*
			 * If AUTH CODE is present in the request, it
			 * becomes POST AUTH command, if CTroutd is present
			 * then it is COMPLETION command otherwise it is CAPTURE/SALE command
			 */
			if(strlen(stFTranDtls.szAuthCode) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Auth code is present, setting as POST AUTH", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				*piUpdatedCmd = SCI_POSTAUTH;
			}
			else if(strlen(stFTranDtls.szCTroutd) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: CTroutd code is present, setting as COMPLETION", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				*piUpdatedCmd = SCI_COMPLETION;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Auth code is NOT present, setting as CAPTURE", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				*piUpdatedCmd = SCI_CAPTURE;
			}
			break;
		default:
			*piUpdatedCmd = iCurrentCmd;
			break;
		}
		break; //Breaking from the while loop
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: testCounter
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int testCounter(char * szLbl, char * szPOSCtr, char * errMsg)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	ulong	ulReqCtr		= 0L;
	ulong	ulRecCtr		= 0L;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Assuming iLen would be always greater than zero and at max 10 */
		if( ( (iLen = strlen(szPOSCtr)) == 10 ) &&
						( strcmp(szPOSCtr, "4294967295") > 0) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid POS counter [%s] > 429467295",
														__FUNCTION__, szPOSCtr);
			APP_TRACE(szDbgMsg);

			sprintf(errMsg, "Invalid tran counter [%s]", szPOSCtr);
			rv = ERR_INV_FLD;

			break;
		}

		/* Get the values in long */
		ulReqCtr = strtoul(szPOSCtr, NULL, 0);
		getTranCounter(szLbl, &ulRecCtr);

		/* Compare these counter values */
		if(ulReqCtr <= ulRecCtr)
		{
			debug_sprintf(szDbgMsg, "%s: POS ctr [%lu] <= stored ctr [%lu]",
											__FUNCTION__, ulReqCtr, ulRecCtr);
			APP_TRACE(szDbgMsg);

			sprintf(errMsg, "Invalid tran counter [%s]", szPOSCtr);
			rv = ERR_INV_FLD;

			break;
		}

		/* Update the new transaction counter value in the pos table */
		rv = updateTranCounter(szLbl, ulReqCtr);

		break;
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: validatePOSPIN
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int validatePOSPIN(char * pszClrkPIN, char *pszPOSPin, char *pszPOSRSAKey, int iRegCmdVersion)
{
	int	   rv					= SUCCESS;
	char   szEntFourDigts[5] 	= "";
	char   szKeyFourDigts[5]	= "";
	char   szFingerPrint[42]	= "";

#ifdef DEVDEBUG
	char			szDbgMsg[2048]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(iRegCmdVersion == 1)
		{
			/* Compare the two PINs */
			if(strcmp(pszClrkPIN, pszPOSPin) != SUCCESS)
			{
				/* PINs did not match */
	#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s: Both PINs don't MATCH [%s] vs [%s]",
							DEVDEBUGMSG, __FUNCTION__, pszPOSPin, pszClrkPIN);
				APP_TRACE(szDbgMsg);
	#else
				debug_sprintf(szDbgMsg, "%s: Both PINs don't MATCH", __FUNCTION__);
				APP_TRACE(szDbgMsg);
	#endif
				rv = ERR_FLD_MISMATCH;
				break;
			}
		}
		else if(iRegCmdVersion == 2)
		{
			memset(szEntFourDigts, 0x00, sizeof(szEntFourDigts));
			strncpy(szEntFourDigts, pszClrkPIN, 4);

			debug_sprintf(szDbgMsg, "%s: Entered First Four digits entered [%s]", __FUNCTION__, szEntFourDigts);
			APP_TRACE(szDbgMsg);

			rv = calcFingerPrint(pszPOSRSAKey, szFingerPrint);
			if (rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Not able to calculate the finger print", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s:%s: RSA Key: [%s] Finger Print [%s]",
						DEVDEBUGMSG, __FUNCTION__, pszPOSRSAKey, szFingerPrint);
			APP_TRACE(szDbgMsg);
#endif
			memset(szKeyFourDigts, 0x00, sizeof(szKeyFourDigts));
			strncpy(szKeyFourDigts, szFingerPrint, 4);

			debug_sprintf(szDbgMsg, "%s: POS Key Four digits entered [%s]", __FUNCTION__, szKeyFourDigts);
			APP_TRACE(szDbgMsg);

			/* Compare the two PINs */
			if(strcasecmp(szKeyFourDigts, szEntFourDigts) != SUCCESS)
			{
				/* PINs did not match */
	#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s: Both PINs don't MATCH [%s] vs [%s]",
							DEVDEBUGMSG, __FUNCTION__, szKeyFourDigts, szEntFourDigts);
				APP_TRACE(szDbgMsg);
	#else
				debug_sprintf(szDbgMsg, "%s: Both PINs don't MATCH", __FUNCTION__);
				APP_TRACE(szDbgMsg);
	#endif
				rv = ERR_FLD_MISMATCH;
				break;
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: createAESKey
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: void
 * ============================================================================
 */
static char* createAESKey(char *pszAES_Key)
{
	if( (RAND_bytes((uchar *)pszAES_Key, MAC_128_BIT_KEYLEN)) -1 == 0)
		return pszAES_Key;
	else
		return NULL;
}

/*
 * ============================================================================
 * Function Name: promptClerkPIN
 *
 * Description	: This function prompts the clerk to enter PIN. This PIN is
 * 					used to initiate pairing or to break the pairing request
 * 					sent from POS.
 *
 * Input Params	: szPIN	-> the placeholder for PIN to be entered by the user
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int promptClerkPIN(char * szPIN, int iCmdVersion, char * szErrMsg, int iTitle)
{
	int			rv				= SUCCESS;
#ifdef DEVDEBUG
	char			szDbgMsg[512]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: iCmdVersion[%d]", __FUNCTION__, iCmdVersion);
	APP_TRACE(szDbgMsg);

	if(iCmdVersion == 1)
	{
		rv = show4DigitPOSPinEntryScreen(szPIN, iTitle);
	}
	else if(iCmdVersion == 2)
	{
		rv = show8DigitPOSPinEntryScreen(szPIN, ENTER_8PIN_TITLE);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Command version!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	if(rv == SUCCESS)
	{
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: PIN entered by user = [%s]", DEVDEBUGMSG, __FUNCTION__, szPIN);
		APP_TRACE(szDbgMsg);
#endif
	}
	else if(rv == UI_CANCEL_PRESSED)
	{
		debug_sprintf(szDbgMsg, "%s: User cancelled PIN entry", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		sprintf(szErrMsg, "CANCELLED by Customer");
		rv = ERR_USR_CANCELED;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Some error with UI agent", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getLIRunningAmounts
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void getLIRunningAmounts(char* pszTaxAmount, char* pszTotalAmount, char *pszSubTotAmount)
{
	int					rv 			= SUCCESS;
	SESSDTLS_PTYPE 		pstSessDtls = NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSessMap != NULL)
	{
		rv = getSession(pstSessMap->szSessKey, &pstSessDtls);
		if (rv == SUCCESS)
		{
			if(pstSessDtls->pstLIInfo != NULL)
			{
				strcpy(pszTaxAmount,    pstSessDtls->pstLIInfo->runTaxAmt);
				strcpy(pszTotalAmount,  pstSessDtls->pstLIInfo->runTranAmt);
				strcpy(pszSubTotAmount, pstSessDtls->pstLIInfo->runSubTtlAmt);
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No session details!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		/* Should not come here */
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getStatusDtlsFromSession
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getStatusDtlsFromSession(STATUSDTLS_PTYPE pstStatusDtls)
{
	int				rv 			= SUCCESS;
	int				iDuration	= 0;
	int				iHours		= 0;
	int				iMinutes	= 0;
	int 			iSecs		= 0;
	SESSDTLS_PTYPE	pstSessDtls = NULL;
	time_t			curTime;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSessMap != NULL)
	{
		strcpy(pstStatusDtls->szSessMacLbl, pstSessMap->szMACLbl);

		debug_sprintf(szDbgMsg, "%s: Session Mac Label is [%s]", __FUNCTION__, pstStatusDtls->szSessMacLbl);
		APP_TRACE(szDbgMsg);

		rv = getSession(pstSessMap->szSessKey, &pstSessDtls);
		if(rv == SUCCESS)
		{
			strcpy(pstStatusDtls->szInvoice, pstSessDtls->szInvoice);

			debug_sprintf(szDbgMsg, "%s: Session Invoice is [%s]", __FUNCTION__, pstStatusDtls->szInvoice);
			APP_TRACE(szDbgMsg);

			time(&curTime);
			iDuration = difftime(curTime, pstSessDtls->startTime);

			/*Calculating the hours minutes and seconds from the total duration*/
			iSecs 	  = iDuration%60;
			iMinutes  = iDuration/60;
			iHours	  = iMinutes/60;
			iMinutes  = iMinutes%60;

			sprintf(pstStatusDtls->szSessDuration, "%02d:%02d:%02d", iHours, iMinutes, iSecs);

			debug_sprintf(szDbgMsg, "%s: Session Duration is [%s]", __FUNCTION__, pstStatusDtls->szSessDuration);
			APP_TRACE(szDbgMsg);
		}
	}

	getDeviceName(pstStatusDtls->szDeviceName);

	debug_sprintf(szDbgMsg, "%s: Device Name is [%s]", __FUNCTION__, pstStatusDtls->szDeviceName);
	APP_TRACE(szDbgMsg);

	getDevSrlNum(pstStatusDtls->szSerialNum);

	debug_sprintf(szDbgMsg, "%s: Device serial Number is [%s]", __FUNCTION__, pstStatusDtls->szSerialNum);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isCancelRebootAllowed
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isCancelRebootAllowed()
{
	PAAS_BOOL rv = PAAS_FALSE;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(getAppState())
	{
	case NO_SESSION_IDLE:
	case IN_SESSION_IDLE:
	case IN_LINEITEM:
	case CAPTURING_PAYMENT:
	case CAPTURING_VOID:
	case CAPTURING_REFUND:
	case CAPTURING_GIFT:
	case REGISTER:
	case SAF_QUERY:
	case SAF_REMOVAL:
	case CAPTURING_DEV_MAIL:
	case CAPTURING_DEV_CUST_QUESTION:
	case CAPTURING_DEV_CUST_SURVEY:
	case CAPTURING_DEV_LOYALTY:
	case CAPTURING_DEV_SIGNATURE:
	case CAPTURING_DEV_CHARITY:
	case CAPTURING_DEV_VERSION:
	case CAPTURING_DEV_CUST_BUTTON:
	case LANE_CLOSED:
	case IN_SESSION_WELCOME:
	case CAPTURING_CREDIT_APP:
	case CAPTURING_PAYPAL_PAYMENT_CODE:
	case CAPTURING_EBT_BALANCE:
	case CAPTURING_PRIV_BALANCE:
	case PROCESSING_GET_PARM:
	case PROCESSING_PROVISION_PASS:
	case PROCESSING_DISPLAY_MESSAGE:
	case PROCESSING_GET_CARD_DATA:
	case DISPLAYING_QRCODE_SCREEN:
	case IN_LINEITEM_QRCODE:
	case CAPTURING_DEV_CHECKBOX:
		rv = PAAS_TRUE;
		break;

	case POSTING_PAYMENT:
	case POSTING_REPORT:
	case POSTING_VOID:
	case POSTING_REFUND:
	case POSTING_GIFT:
	case POSTING_EBT_BALANCE:
	case POSTING_PRIV_BALANCE:
	case PROCESSING_SET_PARM:
	case PROCESSING_VHQ_UPDATES:
	case TRANSACTION_COMPLETED: // T_RaghavendranR1: PTMX 785 Bug Fix Amdocs Case #151124-8713 : SCA is honoring a secondary port cancel even when a transaction processing is in flight and SSI response is obtained from host.
		rv = PAAS_FALSE;
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__,rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setPaymentTypesDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	: tender list to be set from the function, and the PaymentT types text sent from POS
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
extern int setPaymentTypesDtlsForPymtTran(int * tendersList, char *pszPymtTypes)
{
	int		rv							= SUCCESS;
	int		iCnt						= 0;
	int		iPymtCnt					= 0;
	int		tenderSetting[MAX_TENDERS]	= {0};
	int		iAppLogEnabled				= isAppLogEnabled();
	char	szAppLogData[300]			= "";
	char	szAppLogDiag[300]			= "";
	char *	pszStartPtr		 			= NULL;
	char *	pszCurrPtr		 			= NULL;
	char 	szPymtType[20]				= "";
	char *	szStdPymtTypes[] 			= {"CREDIT", "DEBIT", "GIFT", "GWALLET", "IWALLET", "CASH", "PRIV_LBL",
									"MERCH_CREDIT", "PAYPAL", "EBT", "FSA", "POS_TENDER1", "POS_TENDER2", "POS_TENDER3","others", NULL};
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszPymtTypes == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Parameter is NULL!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	getTendersListFromSettings(tenderSetting);//Gets the Enabled Tenders List From Payment Setting
	for(iCnt = 0; iCnt < MAX_TENDERS; iCnt++)
	{
		/* Initialization of the list */
		tendersList[iCnt] = -1;
	}

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	// Now we should Enable the Tender List only which were present in the PAYMENT_TYPES field we received from POS
	pszStartPtr = pszPymtTypes;
	while(1)
	{
		memset(szPymtType, 0x00, sizeof(szPymtType));
		if(pszStartPtr && *pszStartPtr && ((pszCurrPtr = strchr(pszStartPtr, '|')) != NULL)) //Parse with | separator
		{
			strncpy(szPymtType, pszStartPtr, (pszCurrPtr - pszStartPtr));
		}
		else
		{
			strcpy(szPymtType, pszStartPtr); // If No | in the text, we might have come to END of the text
		}

		stripSpaces(szPymtType, STRIP_LEADING_SPACES|STRIP_TRAILING_SPACES);

		for(iCnt = 0; (iCnt < MAX_TENDERS) && (szStdPymtTypes[iCnt]); iCnt++)
		{
			if(strcmp(szPymtType, szStdPymtTypes[iCnt]) == SUCCESS)
			{
				if(tenderSetting[iCnt] == PAAS_TRUE)
				{
					tendersList[iCnt] = PAAS_TRUE; // Enable in the list only when Enabled in the Payment setting
					iPymtCnt++;
					break;
				}
			}
		}


		// Break if the given PAYMENT TYPES from POS exceeded 6 or if it does not match with the defined Payment Types
		if(iCnt == MAX_TENDERS)
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error, Setting the Payment Types for the Transaction.");
				strcpy(szAppLogDiag, "Please Check the Payment Types Field Given From POS Request.");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}
			rv = ERR_INV_PYMT_TYPES;
			break;
		}

		if(iPymtCnt > 6)
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error, Setting the Payment Types for the Transaction. Total 6 Payment Types Can be Set for a Transaction.");
				strcpy(szAppLogDiag, "Please Do not Provide More than 6 Payment Types From POS Request");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_INV_PYMT_TYPES_CNT;
			break;
		}

		// Move to Next Given Payment Types
		if(pszCurrPtr)
		{
			pszStartPtr = pszCurrPtr + 1;
		}
		else
		{
			break;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: setPOSTendersDtlsToTenderList
 *
 * Description	: This function modifies the tenderList, according to the received POS Tender Types
 *
 * Input Params	: tender list to be edit, and POS Tender Dtls
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
static int setPOSTendersDtlsToTenderList(int *tendersList, POSTENDERDTLS_PTYPE pstPOSTenderDtls)
{
	int				rv							= SUCCESS;
	int				iCnt						= 0;
	int				jCnt						= 0;
	int				iPymtCnt					= 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		for(iCnt = 0; iCnt < MAX_TENDERS; iCnt++)
		{
			switch(iCnt)
			{
			case TEND_CREDIT:
				if(tendersList[iCnt] == PAAS_TRUE)
				{
					iPymtCnt++;
				}
				break;
			case TEND_DEBIT:
				if(tendersList[iCnt] == PAAS_TRUE)
				{
					iPymtCnt++;
				}
				break;
			case TEND_GIFT:
				if(tendersList[iCnt] == PAAS_TRUE)
				{
					iPymtCnt++;
				}
				break;
			case TEND_GOOGLE:
				if(tendersList[iCnt] == PAAS_TRUE)
				{
					iPymtCnt++;
				}
				break;
			case TEND_ISIS:
				if(tendersList[iCnt] == PAAS_TRUE)
				{
					iPymtCnt++;
				}
				break;
			case TEND_BALANCE:
				if(tendersList[iCnt] == PAAS_TRUE)
				{
					iPymtCnt++;
				}
				break;
			case TEND_PRIVATE:
				if(tendersList[iCnt] == PAAS_TRUE)
				{
					iPymtCnt++;
				}
				break;
			case TEND_MERCHCREDIT:
				if(tendersList[iCnt] == PAAS_TRUE)
				{
					iPymtCnt++;
				}
				break;
			case TEND_PAYPAL:
				if(tendersList[iCnt] == PAAS_TRUE)
				{
					iPymtCnt++;
				}
				break;
			case TEND_EBT:
				if(tendersList[iCnt] == PAAS_TRUE)
				{
					iPymtCnt++;
				}
				break;
			case TEND_FSA:
				if(tendersList[iCnt] == PAAS_TRUE)
				{
					iPymtCnt++;
				}
				break;
			case TEND_POS_TENDER1:
			case TEND_POS_TENDER2:
			case TEND_POS_TENDER3:
				if(jCnt < 3 && (strlen(pstPOSTenderDtls->szPOSTender[jCnt])) && (pstPOSTenderDtls->iPOSTenderAction[jCnt] != 0))
				{
					tendersList[iCnt] = PAAS_TRUE;
					iPymtCnt++;
				}
				jCnt++;
				break;

			case TEND_OTHER:
				if(tendersList[iCnt] == PAAS_TRUE)
				{
					iPymtCnt++;
				}
				break;

			//default: CID 67492 (#1 of 1): Dead default in switch (DEADCODE) (#1 of 1): Dead default in switch (DEADCODE) T_RaghavendranR1, Dead Code. Never gets Executed. Commented.
				//debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
				//APP_TRACE(szDbgMsg);
				//rv = FAILURE;
				//break;
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: getEnabledTenders
 *
 * Description	:
 *
 * Input Params	: tender list to be set from the function, the transaction key,
 * 				  cardtypeLst - Used in case of new payment flow where we set the tenders
 * 				  list based on the Card Types that CDT supports for particular range.
 * 				  ptndrCnt - Used to return the number of tenders that needs to be
 * 				  displayed on the tender selection screen,
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
int getEnabledTenders(int * tendersList, char *pszTranKey,int * cardTypeLst, int *ptndrCnt, PAAS_BOOL *bTogglePymtType)
{
	int						rv							= SUCCESS;
	int						tenderSetting[MAX_TENDERS]  = {0};
	int						iCnt						= 0;
	int						jCnt						= 0;
	int						iFxn						= 0;
	int						iCmd						= 0;
	int						iPymtType					= -1;
	int						iAppLogEnabled				= isAppLogEnabled();
	PAAS_BOOL				bManualEntry				= PAAS_FALSE;
	PAAS_BOOL				bBarCodeAcctNumExist		= PAAS_FALSE;
	PAAS_BOOL				bPreSwiped					= PAAS_FALSE;
	PAAS_BOOL				bRegPymtFlow				= PAAS_TRUE;
	char					szAppLogData[300]			= "";
	char					szAppLogDiag[300]			= "";
	char					szPymtTypes[100]			= "";
	POSTENDERDTLS_STYPE		stPOSTenderDtls;
	CARDDTLS_STYPE 			stCardDtls;
	FSADTLS_STYPE			stFSADtls;
	EBTDTLS_STYPE			stEBTDtls;
	FTRANDTLS_STYPE 		stFollowTranDtls;
	SESSDTLS_PTYPE			pstSess						= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	bPreSwiped = isPreSwipeDone();
	/* Initialization of the list */
	for(iCnt = 0; iCnt < MAX_TENDERS; iCnt++)
	{
		tendersList[iCnt] = -1;
	}

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		rv = getSession(pstSessMap->szSessKey, &pstSess);
		if(rv != SUCCESS || pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return rv;
		}
	}
	else // CID 67465 (#1 of 1): Explicit null dereferenced (FORWARD_NULL). T_RaghavendranR1. In No Session Present return FAILURE.
	{
		debug_sprintf(szDbgMsg, "%s: Session is not in progress", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	/* get the func and cmd of the current transaction instance */
	getFxnNCmdForSSITran(pszTranKey, &iFxn, &iCmd);

	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
	getCardDtlsForPymtTran(pszTranKey, &stCardDtls);

	if(stCardDtls.bEmvData == PAAS_TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: Card Data is an EMV Data", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* Adding check of Card source because , if we are in new payment flow, then szPAN will be present when
	 * we move to Tender Selection screen after card swipe screen
	 */
	if((strlen(stCardDtls.szPAN) > 0 || strlen(stCardDtls.szBarCode) > 0) && (stCardDtls.iCardSrc == 0 ))
	{
		bBarCodeAcctNumExist = PAAS_TRUE;
	}

	memset(&stFollowTranDtls, 0x00, sizeof(FTRANDTLS_STYPE));
	rv = getFollowOnDtlsForPymt(pszTranKey, &stFollowTranDtls);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failure while fetching the follow on details!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	/* Get the manual entry flag from the transaction instance */
	getManualFlagForPymtTran(pszTranKey, &bManualEntry);

	/* Get The Payment Types sent in POS Request from PymntTran */
	memset(&szPymtTypes, 0x00, sizeof(szPymtTypes));
	rv = getPaymentTypesDtlsForPymtTran(pszTranKey, szPymtTypes);

	if(strlen(szPymtTypes) > 0)
	{
		rv = setPaymentTypesDtlsForPymtTran(tenderSetting, szPymtTypes);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to set the Payment types given from POS", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return rv;
		}

	}
	else
	{
		rv = getTendersListFromSettings(tenderSetting);
	}

	/* Get The POS Defined Tender Types sent in SCI Request from PymntTran for only CAPTURE command*/
	if((iFxn == SSI_PYMT) && (iCmd == SSI_SALE))
	{
		memset(&stPOSTenderDtls, 0x00, sizeof(POSTENDERDTLS_STYPE));
		rv = getPOSTendersDtlsForPymtTran(pszTranKey, &stPOSTenderDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to Get the POS Tender types given from POS", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return rv;
		}
		else if((strlen(stPOSTenderDtls.szPOSTender[0])) || (strlen(stPOSTenderDtls.szPOSTender[1])) || (strlen(stPOSTenderDtls.szPOSTender[2])))
		{
			rv = setPOSTendersDtlsToTenderList(tenderSetting, &stPOSTenderDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to set the Tender types given from POS", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return rv;
			}
		}
	}

	if( stCardDtls.bEmvData == PAAS_TRUE || pstSess->stCardDtls.bEmvData == PAAS_TRUE )
	{
		if(stCardDtls.bEmvData == PAAS_TRUE)
		{
			getEMVDtlsfromAID(&stCardDtls,&iPymtType);
			*bTogglePymtType = stCardDtls.bTogglePymtType;
		}
		else
		{
			getEMVDtlsfromAID(&pstSess->stCardDtls,&iPymtType);
			*bTogglePymtType = pstSess->stCardDtls.bTogglePymtType;
		}
		getPymtFlowForPymtTran(pszTranKey,&bRegPymtFlow);

		debug_sprintf(szDbgMsg, "%s: Got Pymt from AID as %d", __FUNCTION__, iPymtType);
		APP_TRACE(szDbgMsg);

		if((iCmd == SSI_VOICEAUTH) || (iCmd == SSI_POSTAUTH) || ((iCmd == SSI_PREAUTH)))
		{
			/* Daivik:21/1/2016 : For EMV Transaction when Command is any of the Auth transactions , we need to avoid
			 * debit tender type.
			 */
			debug_sprintf(szDbgMsg, "%s: Debit Tender will not be allowed for Auth Transactions", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			tenderSetting[TEND_DEBIT] = PAAS_FALSE;
		}

		/*
		 * AjayS2: 04 Nov 2016: If payment type in AIDList is set as Toggle(2) for EMV cards, then we are setting Payment Type initially as Debit, by Default.
		 * The final payment type would be based on PIN CVM done or not. If PIN CVM is done, payment type would be Debit only. If PIN CVM not done,
		 * we are toggling payment type to Credit.
		 *
		 * If we received PAYMENT_TYPE or PAYMENT_TYPES as CREDIT from POS, still the default payment type initially  would be Debit. Here the payment type from POS and
		 * Payment type initially selected are not matching, but we should not abort the transaction here, because there is chance that this transaction might be CREDIT.
		 * So we are not throwing Error back here in such a case.
		 */
		if(bRegPymtFlow || (iPymtType == -1 && !bRegPymtFlow))
		{
			if(tenderSetting[TEND_CREDIT] == PAAS_TRUE && (iPymtType == -1 || iPymtType == PYMT_CREDIT))
			{
				tendersList[jCnt] = TEND_CREDIT;
				jCnt++;
			}
			if(tenderSetting[TEND_DEBIT] == PAAS_TRUE && (iPymtType == -1 || iPymtType == PYMT_DEBIT))
			{
				tendersList[jCnt] = TEND_DEBIT;
				jCnt++;
			}
			else if(*bTogglePymtType && iPymtType == PYMT_DEBIT)
			{
				/*AjayS2: 14 Nov 2016: If payment type is set as 2 for the AID, then we have to proceed with Debit even when Debit is not configured in device or POS
				 * doesn't want Debit, because this may be Toggled to Credit. Later, if we found that this transaction can't be Toggled, we will throw error to POS.
				 *
				 * PS: We can merge this code up also, Putting here just for sake of clarity
				 */
				tendersList[jCnt] = TEND_DEBIT;
				jCnt++;
			}
		}
		else if(iPymtType == PYMT_CREDIT)
		{
			if(tenderSetting[TEND_CREDIT] == PAAS_TRUE)
			{
				tendersList[jCnt] = TEND_CREDIT;
				jCnt++;
			}
		}
		else if(iPymtType == PYMT_DEBIT)
		{
			if(tenderSetting[TEND_DEBIT] == PAAS_TRUE || *bTogglePymtType)
			{
				tendersList[jCnt] = TEND_DEBIT;
				jCnt++;
			}
		}
	}
	else
	{
		/* Setting enabled tenders in the list */
		for(iCnt = 0; iCnt < MAX_TENDERS; iCnt++)
		{
			debug_sprintf(szDbgMsg, "%s: cardTypeLst[%d]=%d", __FUNCTION__,iCnt,cardTypeLst[iCnt]);
			APP_TRACE(szDbgMsg);
			if(tenderSetting[iCnt] == PAAS_TRUE)
			{
				switch(iCnt)
				{
				case TEND_CREDIT:
					if((cardTypeLst[TEND_CREDIT] == -1) && (iCnt == TEND_CREDIT))
						break;
					tendersList[jCnt] = iCnt;
					jCnt++;
					break;
				case TEND_PRIVATE:
					if((cardTypeLst[TEND_PRIVATE] == -1) && (iCnt == TEND_PRIVATE))
						break;
					if(!bBarCodeAcctNumExist)
					{
						tendersList[jCnt] = iCnt;
						jCnt++;
					}
					break;
				case TEND_BALANCE:
					switch(iCmd)
					{
					case SSI_VOICEAUTH:
					case SSI_PREAUTH:
					case SSI_POSTAUTH:
					case SSI_CREDIT:
						break;
					default :
						if(!bBarCodeAcctNumExist)
						{
							tendersList[jCnt] = iCnt;
							jCnt++;
						}
						break;
					}
		    		break;
				case TEND_GIFT:
					if((cardTypeLst[TEND_GIFT] == -1) && (iCnt == TEND_GIFT))
						break;
				case TEND_MERCHCREDIT:
					if((cardTypeLst[TEND_MERCHCREDIT] == -1) && (iCnt == TEND_MERCHCREDIT))
						break;
					tendersList[jCnt] = iCnt;
					jCnt++;

					break;
			    case TEND_DEBIT:
					if(cardTypeLst[TEND_DEBIT] == -1)
						break;
					switch(iCmd)
					{
					case SSI_VOICEAUTH:
					case SSI_PREAUTH:
					case SSI_POSTAUTH:

						break;

					case SSI_CREDIT:
					case SSI_SALE:
						if(strlen(stFollowTranDtls.szCardToken) > 0)
						{
							break;
						}
						else if(bManualEntry != PAAS_TRUE && bBarCodeAcctNumExist != PAAS_TRUE)
						{
							tendersList[jCnt] = iCnt;
							jCnt++;
						}
						break;

					default :
						if(bManualEntry != PAAS_TRUE && bBarCodeAcctNumExist != PAAS_TRUE)
						{
							tendersList[jCnt] = iCnt;
							jCnt++;
						}
						break;
					}
		    		break;
			    case TEND_PAYPAL:
			    	if(bPreSwiped == PAAS_FALSE)
			    	{
			    		switch(iCmd)
			    		{
			    		case SSI_VOICEAUTH:
			    		case SSI_PREAUTH:
			    		case SSI_POSTAUTH:
			    		break;

			    		/* Paypal Refund Transaction is Processed only if CTROUTD Field is Present.
			    		 * If CTROUTD is Not Present in the request, we do not show the Payment Type Selection Option in the Screen*/
			    		case SSI_CREDIT:
			    			if(strlen(stFollowTranDtls.szCTroutd) > 0)
			    			{
			    				tendersList[jCnt] = iCnt;
			    				jCnt++;
			    			}
			    			break;
			    			default :
			    			if(bManualEntry != PAAS_TRUE && bBarCodeAcctNumExist != PAAS_TRUE)
			    			{
			    				tendersList[jCnt] = iCnt;
			    				jCnt++;
			    			}
			    			break;
						}
			    	}
					break;
				case TEND_GOOGLE:
				case TEND_ISIS:
					if(bManualEntry != PAAS_TRUE && bBarCodeAcctNumExist != PAAS_TRUE)
					{
						tendersList[jCnt] = iCnt;
						jCnt++;
					}
					break;
				case TEND_EBT:
					if(cardTypeLst[TEND_EBT] == -1)
						break;
					memset(&stEBTDtls, 0x00, sizeof(EBTDTLS_STYPE));
					rv = getEBTDtlsForPymtTran(pszTranKey, &stEBTDtls);
					switch(iCmd)
					{
					case SSI_VOICEAUTH:
					case SSI_PREAUTH:
					case SSI_POSTAUTH:
						break;

		    			/* EBT Refund Transaction is Processed only if EBT Field Food Stamp Field is Present.*/
		    		case SSI_CREDIT:
						if((rv == SUCCESS) && ((strlen(stEBTDtls.szEBTSNAPElig) > 0)
						|| (strlen(stEBTDtls.szEBTType) > 0)))
						{
							tendersList[jCnt] = iCnt;
							jCnt++;
						}
						break;
					default :

						if((rv == SUCCESS) && ((strlen(stEBTDtls.szEBTCashElig) > 0)
								|| (strlen(stEBTDtls.szEBTSNAPElig) > 0) || (strlen(stEBTDtls.szEBTType) > 0)))
						{
							tendersList[jCnt] = iCnt;
							jCnt++;
						}
						break;
					}
					break;

				case TEND_FSA:
					if(cardTypeLst[TEND_FSA] == -1)
						break;
					switch(iCmd)
					{
					case SSI_VOICEAUTH:
					case SSI_PREAUTH:
					case SSI_POSTAUTH:

						break;
					default :
						memset(&stFSADtls, 0x00, sizeof(FSADTLS_STYPE));
						rv = getFSADtlsForPymtTran(pszTranKey, &stFSADtls);
						if((rv == SUCCESS) && (strlen(stFSADtls.szAmtHealthCare) > 0))
						{
							tendersList[jCnt] = iCnt;
							jCnt++;
						}
						break;
					}
					break;

				case TEND_POS_TENDER1:
				case TEND_POS_TENDER2:
				case TEND_POS_TENDER3:
					tendersList[jCnt] = iCnt;
					jCnt++;
					break;
				}
			}
		}
	}

	*ptndrCnt = jCnt;
	if(jCnt == 0)
	{
		debug_sprintf(szDbgMsg, "%s: No Payment Types is Enabled or Set for the Transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error, Setting the Payment Types for the Transaction.");
			strcpy(szAppLogDiag, "Please Check the Payment Type(s) Field Given From POS Request.");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
		}
		rv = ERR_INV_PYMT_TYPES;
	}
	else if(jCnt > 6)
	{
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error, Setting the Payment Types for the Transaction. Total 6 Payment Types Can be Set for a Transaction.");
			strcpy(szAppLogDiag, "Please Do not Provide More than 6 Payment Types From POS Request");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
		}

		rv = ERR_INV_TENDER_CNT;
	}

	debug_sprintf(szDbgMsg, "%s: --- Returning [%d] ---", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: applyVHQUpdates
 *
 * Description	: This API would apply the available updates.
 *
 * Input Params	: Transaction key, Status Msg for Response text.
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int applyVHQUpdates(char * szTranKey, char * szStatMsg)
{
	int						rv						= SUCCESS;
	int						iAppLogEnabled			= isAppLogEnabled();
	int						VHQRcvMsgQId			= 0;
	PAAS_BOOL				bVHQUpdateFlag 			= PAAS_FALSE;
	PAAS_BOOL				bVHQUpdateAvailable		= PAAS_FALSE;
	char					szAppLogData[300]		= "";
	APPLYUPDATES_STYPE	  	stApplyUpdatesDtls;
	POSRESPDATA_VHQ_STYPE		stPOSRespDataForVHQ;

#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		memset(&stApplyUpdatesDtls, 0x00, sizeof(APPLYUPDATES_STYPE));
		memset(&stPOSRespDataForVHQ,   0x00, sizeof(POSRESPDATA_VHQ_STYPE));

		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the time placeholder from the transaction instance */
		rv = getVHQAplyUpdtDtlsForDevTran(szTranKey, &stApplyUpdatesDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Apply Updates Details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		bVHQUpdateFlag 	 	= stApplyUpdatesDtls.bUpdateFlag;
		bVHQUpdateAvailable = isVHQUpdateAvailable();

		if(bVHQUpdateAvailable & bVHQUpdateFlag)	// in case POS approved updates
		{
			CHECK_POS_INITIATED_STATE;

			setAppState(PROCESSING_VHQ_UPDATES);

			showPSGenDispForm(MSG_PROCESSING_VHQ_UPDATES, STANDBY_ICON, 0);

			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "VHQ Updates are In Progress...");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			sprintf(szStatMsg, "Updates Approved");

			stPOSRespDataForVHQ.iRespType = SCA_POS_APPROVED;
			sprintf(stPOSRespDataForVHQ.szText, "APPROVED");

			/* Need to add data to POS-VHQ receive MsgQ to Notify VHQ thread about POS approval */

			VHQRcvMsgQId = getVHQRcvFrmPOSMsgQId();
			rv = msgsnd(VHQRcvMsgQId, (void *)&stPOSRespDataForVHQ, sizeof(POSRESPDATA_VHQ_STYPE), IPC_NOWAIT); //IPC system call
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to add VHQ Agent data to SCA VHQ MSG Queue",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				debug_sprintf(szDbgMsg, "%s: Something went wrong msgsnd(): [%d] [%s]", __FUNCTION__, errno, strerror(errno));
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: POS approval response added to MsgQ",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* Need to Reset the Update unavailable, once it is taken/consumed */
				setVHQUpdtAvailableFlag(PAAS_FALSE);

				/* Setting the update status here as 'Updating' */
				setVHQUpdateStatus(VHQ_UPDATE_PROCESSING);

				setVHQUpdateInProgressFlag(PAAS_TRUE);

			}
		}
		else if(bVHQUpdateAvailable == PAAS_TRUE && bVHQUpdateFlag == PAAS_FALSE)	// in case POS declined updates
		{

			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "VHQ Updates are Rejected From POS");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			sprintf(szStatMsg, "POS DECLINED UPDATES");

			stPOSRespDataForVHQ.iRespType = SCA_POS_REJECT;
			sprintf(stPOSRespDataForVHQ.szText, "REJECTED");

			/* Need to add data to POS-VHQ receive MsgQ to Notify VHQ thread about POS approval */

			VHQRcvMsgQId = getVHQRcvFrmPOSMsgQId();
			rv = msgsnd(VHQRcvMsgQId, (void *)&stPOSRespDataForVHQ, sizeof(POSRESPDATA_VHQ_STYPE), IPC_NOWAIT); //IPC system call
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to add VHQ Agent data to SCA VHQ MSG Queue",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				debug_sprintf(szDbgMsg, "%s: Something went wrong msgsnd(): [%d] [%s]", __FUNCTION__, errno, strerror(errno));
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: POS rejection response added to MsgQ",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* Need to Reset the Update unavailable, once it is taken/consumed */
				setVHQUpdtAvailableFlag(PAAS_FALSE);

				/* Resetting the update status here as 'No Updates' */
				setVHQUpdateStatus(VHQ_NO_UPDATES);
			}
		}
		else if(bVHQUpdateAvailable == PAAS_FALSE)	// in case No Updates are available
		{
			debug_sprintf(szDbgMsg, "%s: No VHQ Updates are available",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "VHQ Updates are Not Available");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

		//	sprintf(szStatMsg, "VHQ Updates are Not Available");
			rv = ERR_NO_UPDATES;
		}

		break;
	}	// end of while loop

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: validateEnabledTenders
 *
 * Description	:
 *
 * Input Params	: Transaction key.
 *
 * Output Params:
 * ============================================================================
 */
int validateEnabledTenders(char *pszTranKey)
{
	int						rv							= SUCCESS;
	int						tenderSetting[MAX_TENDERS]  = {0};
	int						iCnt						= 0;
	int						iAppLogEnabled				= isAppLogEnabled();
	char					szAppLogData[300]			= "";
	char					szAppLogDiag[300]			= "";
	char *					pszStartPtr		 			= NULL;
	char *					pszCurrPtr		 			= NULL;
	char 					szPymtType[20]				= "";
	char					szPymtTypes[100]			= "";
	char *					szStdPymtTypes[] 			= {"CREDIT", "DEBIT", "GIFT", "GWALLET", "IWALLET", "CASH", "PRIV_LBL",
															"MERCH_CREDIT", "PAYPAL", "EBT", "FSA", "POS_TENDER1", "POS_TENDER2", "POS_TENDER3","others", NULL};
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Get The Payment Types sent in POS Request from PymntTran */
	memset(&szPymtTypes, 0x00, sizeof(szPymtTypes));
	rv = getPaymentTypesDtlsForPymtTran(pszTranKey, szPymtTypes);

	if(strlen(szPymtTypes) > 0)
	{
		rv = getTendersListFromSettings(tenderSetting);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to set the Payment types given from POS", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return rv;
		}

		pszStartPtr = szPymtTypes;

		if(pszStartPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Payment Types NOT present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return rv;
		}

		while(1)
		{
			memset(szPymtType, 0x00, sizeof(szPymtType));
			if(pszStartPtr && *pszStartPtr && ((pszCurrPtr = strchr(pszStartPtr, '|')) != NULL)) //Parse with | separator
			{
				strncpy(szPymtType, pszStartPtr, (pszCurrPtr - pszStartPtr));
			}
			else
			{
				strcpy(szPymtType, pszStartPtr); // If No | in the text, we might have come to END of the text
			}

			stripSpaces(szPymtType, STRIP_LEADING_SPACES|STRIP_TRAILING_SPACES);

			for(iCnt = 0; (iCnt < MAX_TENDERS) && (szStdPymtTypes[iCnt]); iCnt++)
			{
				if(strcmp(szPymtType, szStdPymtTypes[iCnt]) == SUCCESS)
				{
					if(tenderSetting[iCnt] == PAAS_TRUE)
					{
						break;
					}
				}
			}

			// Break if the given PAYMENT TYPES from POS does not match with the defined Payment Types
			if(iCnt == MAX_TENDERS)
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error, Setting the Payment Types for the Transaction.");
					strcpy(szAppLogDiag, "Please Check the Payment Types Field Given From POS Request.");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
				}
				rv = ERR_INV_PYMT_TYPES;
				break;
			}

			// Move to Next Given Payment Types
			if(pszCurrPtr)
			{
				pszStartPtr = pszCurrPtr + 1;
			}
			else
			{
				break;
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: PAYMENT_TYPES field is not present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
/*
 * ============================================================================
 * Function Name: resetBkupDtlsInSession
 *
 * Description	: This function is used to free any allocated memory for storing
 * the backup card details. This needs to be called whenever we flush the previous card
 * details . For ex.flushpreswipe/flushcarddetails etc
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int resetBkupDtlsInSession()
{
	int						rv						= SUCCESS;
	int						iCnt					= 0;
	SESSDTLS_PTYPE			pstSess					= NULL;
	BACKUP_CARDDTLS_PTYPE	pstBkupDtls	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSessMap == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Session Map Details", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	/* Get the Session reference */
	rv = getSession(pstSessMap->szSessKey, &pstSess);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	pstBkupDtls = &pstSess->stBkupDtls;

	if(pstBkupDtls->pszTrackDtlsBkup != NULL)
	{
		debug_sprintf(szDbgMsg, "%s Free Backup Track Details", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		for(iCnt = 0; iCnt < 4; iCnt++)
		{
			if(pstBkupDtls->pszTrackDtlsBkup[iCnt] != NULL)
			{
				free(pstBkupDtls->pszTrackDtlsBkup[iCnt]);
			}
		}
		free(pstBkupDtls->pszTrackDtlsBkup);
	}
	if(pstBkupDtls->pszRSATrackDtlsBkup != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Free Backup RSA Details", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		for(iCnt = 0; iCnt < 2; iCnt++)
		{
			if(pstBkupDtls->pszRSATrackDtlsBkup[iCnt] != NULL)
			{
				free(pstBkupDtls->pszRSATrackDtlsBkup[iCnt]);
			}
		}
		free(pstBkupDtls->pszRSATrackDtlsBkup);
	}
	if(pstBkupDtls->pszVSDBkup != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Free Backup VSD Details", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		for(iCnt = 0; iCnt < 4; iCnt++)
		{
			if(pstBkupDtls->pszVSDBkup[iCnt] != NULL)
			{
				free(pstBkupDtls->pszVSDBkup[iCnt]);
			}
		}
		free(pstBkupDtls->pszVSDBkup);
	}

	memset(&pstSess->stBkupDtls, 0x00, sizeof(BACKUP_CARDDTLS_STYPE));

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getBkupDtlsFromSession
 *
 * Description	: To get Backup Card Details stored in the session. This would
 * be called after tender selection to get the backup details and then parse the
 * track data.
 *
 * Input Params	: Pointer to a structure holding the backup card details
 *
 * Output Params:
 * ============================================================================
 */
int getBkupDtlsFromSession(BACKUP_CARDDTLS_PTYPE pstBkDtls)
{
	int					rv						= SUCCESS;
	SESSDTLS_PTYPE		pstSess					= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstBkDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	if(pstSessMap == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Session Map Details", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	/* Get the Session reference */
	rv = getSession(pstSessMap->szSessKey, &pstSess);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	/*Storing the Card Details into the Session */
	memcpy(pstBkDtls, &pstSess->stBkupDtls,sizeof(BACKUP_CARDDTLS_STYPE));

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
/*
 * ============================================================================
 * Function Name: storeBkupDtlsinSession
 *
 * Description	: To store Backup Card Details in the session.
 *
 * Input Params	: Pointer to a structure holding the backup card details
 *
 * Output Params:
 * ============================================================================
 */
int storeBkupDtlsinSession(BACKUP_CARDDTLS_PTYPE pstBkDtls)
{
	int					rv						= SUCCESS;
	SESSDTLS_PTYPE		pstSess					= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstBkDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	if(pstSessMap == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Session Map Details", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	/* Get the Session reference */
	rv = getSession(pstSessMap->szSessKey, &pstSess);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	// Even if the flush of backup details was not done prior to calling this function, we will ensure we cleanup the previous details and
	// then call this function. This would avoid memory leaks.
	rv = resetBkupDtlsInSession();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to Reset Previous Bkup Details", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	/*Storing the Card Details into the Session */
	memcpy(&pstSess->stBkupDtls, pstBkDtls, sizeof(BACKUP_CARDDTLS_STYPE));

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
/*AkshayaM1 - ApplePay :: Added StoreCardDtlsInSession() to store only CardDetails got in Swipe Screen */
		/*When Both VAS and Card data got in the Payment Transaction .VAS Data is Given to POS and Storing the CardDetails in the session  */
/*
 * ============================================================================
 * Function Name: storeCardDtlsinSession
 *
 * Description	: To store Only Card Details in the session
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int storeCardDtlsinSession(CARDDTLS_PTYPE pstCardDtls)
{
	int					rv						= SUCCESS;
	int					iAppLogEnabled			= isAppLogEnabled();
	char				szAppLogData[300]		= "";
	SESSDTLS_PTYPE		pstSess					= NULL;
	PAAS_BOOL			bEmvEnabled;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	bEmvEnabled = isEmvEnabledInDevice();

	if(pstCardDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Params Passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	if(pstSessMap == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Session Map Details", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	/* Get the Session reference */
	rv = getSession(pstSessMap->szSessKey, &pstSess);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "Received Payment Details, Storing it in the Session");
		addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
	}

	memset(&pstSess->stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

	/*Storing the Card Details into the Session */
	memcpy(&pstSess->stCardDtls, pstCardDtls, sizeof(CARDDTLS_STYPE));

	/*Akshaya: 21-07-16 : When PreSwipe details are stored in Session.Updating bPreSwipedone flag in Session to TRUE*/
	//Setting Pre-Swipe flag
	pstSess->bPreSwipeDone = PAAS_TRUE;

	if(bEmvEnabled == PAAS_TRUE && pstCardDtls->bEmvData == PAAS_TRUE && pstCardDtls->iCardSrc == CRD_EMV_CT)
	{
		pstSess->iEMVCrdPresence = 1;
	}
	else
	{
		pstSess->iEMVCrdPresence = 0;
	}
	debug_sprintf(szDbgMsg, "%s: Stored card details", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
/*
 * ============================================================================
 * Function Name: getSessionDtls
 *
 * Description	:
 *
 * Input Params	: Used to obtain Session Info , when session key is not available
 *
 * Output Params:
 * ============================================================================
 */
int getSessionDtls(SESSDTLS_PTYPE * pstSess)
{
	int	rv			= SUCCESS;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Get the Session reference */
	if(pstSessMap)
	{
		rv = getSession(pstSessMap->szSessKey, pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session Map", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
/*Akshaya :21-07-16 . Ignoring the Implementation if having NFCVAS_MODE and MRCHANT_INDEX are sent in SHOW Line Item Command.*/
#if 0
/*
 * ============================================================================
 * Function Name: getLIShowReqDtls
 *
 * Description	: This function retrieve the VAS Terminal Mode and Merchant Index from Show Line Item command.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getLIShowReqDtls(char * szTranKey, SHWLIINFO_PTYPE pstShwLiInfo)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	SHWLIINFO_PTYPE pstLocShowLI;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		pstLocShowLI = (SHWLIINFO_PTYPE) pstTran->data;
		if(pstLocShowLI == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		memcpy(pstShwLiInfo, pstLocShowLI, sizeof(SHWLIINFO_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ========================================================================================
 * Function Name: updateLIShowReqDtlsInSession
 *
 * Description	:This Function Updates the VAS Terminal Mode and Merchant Index to Session
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ========================================================================================
 */
int updateLIShowReqDtlsInSession(char * szSessKey, SHWLIINFO_PTYPE pstShwLiInfo)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	SESSDTLS_PTYPE	pstSess				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szSessKey == NULL) || (pstShwLiInfo == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getSession(szSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memset(pstSess->szNFCVASMode, 0x00, sizeof(pstSess->szNFCVASMode));
		memset(pstSess->szMerchantIndex, 0x00, sizeof(pstSess->szMerchantIndex));

		if(pstShwLiInfo->szNFCVASMode != NULL)
		{
			strcpy(pstSess->szNFCVASMode, pstShwLiInfo->szNFCVASMode);
		}
		if(pstShwLiInfo->szMerchantIndex != NULL)
		{
			strcpy(pstSess->szMerchantIndex, pstShwLiInfo->szMerchantIndex);
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "VAS Terminal Mode and Merchant Index Updated Successfully in the Session");
			addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: flushPreSwipeVASDtls
 *
 * Description	:This Function Flushes the VAS details Captured in Pre-Swipe/ Line Item Screen.
 *
 * Input Params	:Session Key
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int flushPreSwipeVASDtls(char * szSessKey)
{
	int				rv					= SUCCESS;
	SESSDTLS_PTYPE	pstSess				= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif
	debug_sprintf(szDbgMsg, "%s: Entering", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(szSessKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getSession(szSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/*Flush the VAS details*/
		if(pstSess->pstVasDataDtls != NULL)
		{
			free(pstSess->pstVasDataDtls);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returing", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: processDispQRCode
 *
 * Description	: This API is use to process Display QR Code command. It will generate QR image from the
 * 					provided QR code data & display that image on either LI screen or Full QR Code screen, according to current state of application.
 * 					This also checks for any Display text & button label present in the request, than it will be mapped accordingly to the screen
 *
 * Input Params	: transaction key, status msg
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processDispQRCode(char * szTranKey, char * szStatMsg)
{
	int					rv							= SUCCESS;
	int					iAppLogEnabled				= isAppLogEnabled();
	char				szAppLogData[512]			= "";
	UI_FORM_ENUM		iCurForm					= -1;
	DISPQRCODE_DTLS_STYPE	stDispQRCodeDtls;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(&stDispQRCodeDtls, 0x00, sizeof(DISPQRCODE_DTLS_STYPE));
		rv = getDispQRCodeDtlsForDevTran(szTranKey, &stDispQRCodeDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get Display QR Code details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		iCurForm = getCurFormNumber();
		if(iCurForm == LI_FULL_DISP_FRM)
		{
			debug_sprintf(szDbgMsg, "%s: SCA currently at Full LI screen; Not allowing Display QR Code command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "SCA Currently At Full LI Screen; Not Allowing Display QR Code Command");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, NULL);
			}
			rv = ERR_INV_DISP_QRCODE_CMD;
			strcpy(szStatMsg, "This Command is Not Supported At Full Line Item Screen");
			break;
		}

		// First generate the QR Code image from the given data
		rv = generateQRCodeImage(stDispQRCodeDtls.szQRCodeData, szStatMsg);
		if(rv == SUCCESS)
		{
			if(iCurForm == LI_DISP_OPT_FRM || iCurForm == LI_DISP_FRM)
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Displaying the QR Code At Line Item Screen");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
				// Store the QR code details in session, as we need to display these details(QR image, display text etc.) in case of any FORM changes
				// at LI screen due to card read error or EMV card read
				updateQRCodeDtlsInSession(pstSessMap->szSessKey, &stDispQRCodeDtls);

				rv = updateQRCodeAtLIScrn(&stDispQRCodeDtls, szStatMsg);
			}
			else
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Displaying the QR Code On Full Screen");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}

				rv = showQRCodeAtFullScrn(&stDispQRCodeDtls, szStatMsg);
			}
		}
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to display QR Code", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Display QR Code");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
		}
		else
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Successfully Displayed QR Code");
				addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processCancelQRCode
 *
 * Description	: This API is use to clear QR code present on screen. According to the current screen of SCA, it will change the screen to either
 * 					Line item screen or Idle screen.
 *
 * Input Params	: status msg
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processCancelQRCode(char * szStatMsg)
{
	int					rv							= SUCCESS;
	int					iAppLogEnabled				= isAppLogEnabled();
	int					iAppState					= -1;
	char				szAppLogData[512]			= "";
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if(szStatMsg == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		iAppState = getAppState();
		if(iAppState == IN_LINEITEM_QRCODE)
		{
			debug_sprintf(szDbgMsg, "%s: CANCEL_QRCODE command received at Line Item With QR Code Screen", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "CANCEL_QRCODE command received at Line Item With QR Code Screen");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			showLineItemScreen();
		}
		else if(iAppState == DISPLAYING_QRCODE_SCREEN)
		{
			debug_sprintf(szDbgMsg, "%s: CANCEL_QRCODE command received at Full QR Code Screen", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "CANCEL_QRCODE command received at Full QR Code Screen");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			if(isSessionInProgress() && getLastForm() == WELCOME_DISP_FRM)
			{
				showWelcomeScreen(PAAS_FALSE);
			}
			else if(isSessionInProgress() && getLastForm() == WELCOME_DUP_DISP_FRM)
			{
				showWelcomeScreen(PAAS_TRUE);
			}
			else
			{
				showIdleScreen();
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No QR Code Image Present on Screen to Remove", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "No QR Code Image Present on Screen to Remove. Ignoring The CANCEL_QRCODE Command");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			strcpy(szStatMsg, "No QR Code Image Present on Screen to Remove");
			rv = ERR_INV_CANCEL_QRCODE_CMD;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getQRCodeDtlsFromSession
 *
 * Description	: This function gives QR Code Dtls stored in the session.
 *
 * Input Params	: place holder for holding QR Code dtls 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getQRCodeDtlsFromSession(DISPQRCODE_DTLS_PTYPE pstQRCodeDtls)
{
	int				rv				= SUCCESS;
	SESSDTLS_PTYPE	pstSess			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstQRCodeDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getSession(pstSessMap->szSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Session details are not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		memcpy(pstQRCodeDtls, pstSess->pstQRCodeDtls, sizeof(DISPQRCODE_DTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doDDKAdvance
 *
 * Description	: This function will checks whether DDK needs to be advanced,
 * 					and performed Advance DDK if needed
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int doDDKAdvance(char * szTranStkKey, char *pszErrMsg)
{
	int				rv						= SUCCESS;
	int				iTime					= 0;
	int				iStatusMsgDispInterval  = DFLT_STATUS_DISP_TIME;
	int				iAppLogEnabled			= isAppLogEnabled();
	char			szProcessor[20]			= "";
	char 			szAppLogData[300]		= "";
	char 			szAppLogDiag[300]		= "";
	char			szDispMsg[120]			= "";
	char 			szTmp[256]				= "";
	FILE			*fpLastUpdateDate		= NULL;
	time_t 			tCurrentEpochTime  		= 0;
	time_t 			tTmpEpochTime    		= 0;
	time_t 			tDDKFileEpochTime    	= 0;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	/*AjayS2:19 Aug 2016:
	 * Advance DDK is not needed if Host Interface is Elavon
	 */
	memset(szProcessor, 0x00, sizeof(szProcessor));
	getEnvFile(SECTION_DHI, PROCESSOR, szProcessor, sizeof(szProcessor));
	if(isDHIEnabled() && (strcasecmp(szProcessor, "elvn") == SUCCESS))
	{
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Host Interface is Elvaon,");
			strcpy(szAppLogDiag, "Not Performing VSP Advance DDK");
			addAppEventLog(SCA, PAAS_SUCCESS, START_UP, szAppLogData, szAppLogDiag);
		}
		return SUCCESS;
	}
	while(1)
	{
		if(doesFileExist(VSP_LAST_UPDATE_ORIGINAL) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: DDK Advance Days File Does not Exist, Setting current date/time in the file ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Advance Days File Not Present,");
				strcpy(szAppLogDiag, "Setting Current Date/Time as Last DDK Advance Date/Time");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, szAppLogDiag);
			}
			fpLastUpdateDate = fopen(VSP_LAST_UPDATE_ORIGINAL, "w");
			if(fpLastUpdateDate == NULL)	//Should never enter here
			{
				debug_sprintf(szDbgMsg, "%s: ERROR while Creating DDK Advance Days File ", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			tCurrentEpochTime = time(NULL); // Get the Current epoch time
			memset(szTmp, 0x00, sizeof(szTmp));
			sprintf(szTmp, "%ld", tCurrentEpochTime);
			fputs(szTmp, fpLastUpdateDate);
			if(fpLastUpdateDate != NULL)
			{
				fclose(fpLastUpdateDate);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: DDK Advance Days File Exist in Secure Location, Creating Shadow Copy & Checking whether Advance DDK is required or not", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Advance Days File Present in Secure Location");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}
		}

		debug_sprintf(szDbgMsg, "%s: Creating/Updating the Shadow File in Logs Location ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		memset(szTmp, 0x00, sizeof(szTmp));
		sprintf(szTmp, "cp %s %s", VSP_LAST_UPDATE_ORIGINAL ,VSP_LAST_UPDATE_SHADOW);
		local_svcSystem(szTmp);
		//Changing Permisson so that this file can be grabbed by VHQ also and can be sent in usr1 or OS logs.
		memset(szTmp, 0x00, sizeof(szTmp));
		sprintf(szTmp, "chmod %s %s", "666" ,VSP_LAST_UPDATE_SHADOW);
		local_svcSystem(szTmp);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Created a Shadow Copy of Advance Days File in Logs Location");
			addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
		}

		fpLastUpdateDate = fopen(VSP_LAST_UPDATE_ORIGINAL, "r");
		if(fpLastUpdateDate == NULL)	//Should never enter here
		{
			debug_sprintf(szDbgMsg, "%s: ERROR while Opening DDK Advance Days File ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(szTmp, 0x00, sizeof(szTmp));
		if(fgets(szTmp, 20+1, fpLastUpdateDate) == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR while Reading DDK Advance Days File ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		if(fpLastUpdateDate != NULL)
		{
			fclose(fpLastUpdateDate);
			fpLastUpdateDate = NULL;
		}
		tDDKFileEpochTime = atol(szTmp);
		tTmpEpochTime = (getAdvanceVSPDays())*24*60*60;
		tCurrentEpochTime = time(NULL); // Get the Current epoch time

		debug_sprintf(szDbgMsg, "%s: VSPAdvanceEpoch [%ld], CurrentEpoch [%ld] DDKFileEpoch [%ld]", __FUNCTION__, tTmpEpochTime, tCurrentEpochTime, tDDKFileEpochTime);
		APP_TRACE(szDbgMsg);

		if(tCurrentEpochTime >= tDDKFileEpochTime + tTmpEpochTime)
		{
			debug_sprintf(szDbgMsg, "%s: DDK Advance is Required", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "DDK Advance Need to Be Done");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: DDK Advance is Not required", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "DDK Advance is Not Required");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}
			rv = SUCCESS;
			break;
		}
		if(isSAFRecordsPendingInDevice() != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: SAF Records are Present, Skipping Advance DDK for next Restart/Reboot", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "SAF Records are Present, Skipping Advance DDK for next Restart/Reboot");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}
			rv = SUCCESS;
			break;
		}
		if(isHostConnected() == PAAS_FALSE)
		{
			debug_sprintf(szDbgMsg, "%s: Host is Not Available, Skipping Advance DDK for next Restart/Reboot", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Host is Not Available, Skipping Advance DDK for next Restart/Reboot");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}
			rv = SUCCESS;
			break;
		}

		showPSMessageDispForm(szDispMsg, MSG_VSP_DDK_ADV_IN_PROGRESS, STANDBY_ICON, 0);
		//svcWait(3000);

		memset(szTranStkKey, 0x00, strlen(szTranStkKey));
		setCriticalStepInProgressFlag(PAAS_TRUE);
		rv = doVSPReg(NULL, szTranStkKey, PAAS_FALSE, pszErrMsg, ADVANCE_DDK);
		setCriticalStepInProgressFlag(PAAS_FALSE);
		//rv = FAILURE;
		if(rv == SUCCESS)
		{
			rv = VSP_FULLY_SUCCESS;
			break;
		}
		else if(rv == ERR_VCL_DATA_ERR)
		{
			debug_sprintf(szDbgMsg, "%s: Error While getting VCL Data From FA with ADVACNE DDK command, Will Retry on next Reboot/Restart ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "VSP DDK Advanced Failed in Device,");
				strcpy(szAppLogDiag, "We will Retry on next Restart/Reboot");
				addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, szAppLogDiag);
			}
			showPSMessageDispForm(szDispMsg, MSG_VSP_DDK_ADV_FAILED, SUCCESS_ICON, iStatusMsgDispInterval);
			setVSPActivatedStatus(PAAS_FALSE);
			break;
		}
		else if(rv == ERR_VSP_HOST_ERR)
		{
			debug_sprintf(szDbgMsg, "%s: Error In Posting Data to Host, Retrying after 30 seconds", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error [%d] In Posting Data to Host", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Error [%d] in VSP Advance DDK", rv);
				strcpy(szAppLogDiag, "We will Retry on next Restart/Reboot or in Background");
				addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, szAppLogDiag);
			}
			setVSPActivatedStatus(PAAS_FALSE);
			break;
		}

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Advance DDK Failed First Time,");
			strcpy(szAppLogDiag, "We will Retry After 30 seconds Again");
			addAppEventLog(SCA, PAAS_WARNING, START_UP, szAppLogData, szAppLogDiag);
		}

		iTime = 30;
		while(iTime > 0)
		{
			if(iTime % 10 == 0)
			{
				memset(szDispMsg, 0x00, sizeof(szDispMsg));
				memset(szTmp, 0x00, sizeof(szTmp));
				getDisplayMsg(szTmp, MSG_VSP_DDK_ADV_RETRY);
				sprintf(szDispMsg, szTmp, iTime);
				showPSMessageDispForm(szDispMsg, MSG_VSP_DDK_ADV_RETRY, STANDBY_ICON, 0);
			}
			iTime--;
			svcWait(1000);
		}
		showPSMessageDispForm(szDispMsg, MSG_VSP_DDK_ADV_IN_PROGRESS, STANDBY_ICON, 0);
		svcWait(3000);

		memset(szTranStkKey, 0x00, strlen(szTranStkKey));
		setCriticalStepInProgressFlag(PAAS_TRUE);
		rv = doVSPReg(NULL, szTranStkKey, PAAS_FALSE, pszErrMsg, DEVICE_REG);
		setCriticalStepInProgressFlag(PAAS_FALSE);
		//rv = FAILURE;
		if(rv == SUCCESS)
		{
			rv = VSP_FULLY_SUCCESS;
			break;
		}
		else if(rv == ERR_VCL_DATA_ERR || rv == ERR_VSP_HOST_ERR)
		{
			debug_sprintf(szDbgMsg, "%s: Error [%d] In Posting Data to Host", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error [%d] In Posting Data to Host", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Error [%d] in VSP Advance DDK", rv);
				strcpy(szAppLogDiag, "We will Retry in Background");
				addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, szAppLogDiag);
			}
			break;
		}

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Advance DDK Failed Second Time Also,");
			strcpy(szAppLogDiag, "We will Retry After 60 seconds Again");
			addAppEventLog(SCA, PAAS_WARNING, START_UP, szAppLogData, szAppLogDiag);
		}

		iTime = 60;
		while(iTime > 0)
		{
			if(iTime % 10 == 0)
			{
				memset(szDispMsg, 0x00, sizeof(szDispMsg));
				memset(szTmp, 0x00, sizeof(szTmp));
				getDisplayMsg(szTmp, MSG_VSP_DDK_ADV_RETRY);
				sprintf(szDispMsg, szTmp, iTime);
				showPSMessageDispForm(szDispMsg, MSG_VSP_DDK_ADV_RETRY, STANDBY_ICON, 0);
			}
			iTime--;
			svcWait(1000);
		}
		showPSMessageDispForm(szDispMsg, MSG_VSP_DDK_ADV_IN_PROGRESS, STANDBY_ICON, 0);
		//svcWait(3000);

		memset(szTranStkKey, 0x00, strlen(szTranStkKey));
		setCriticalStepInProgressFlag(PAAS_TRUE);
		rv = doVSPReg(NULL, szTranStkKey, PAAS_FALSE, pszErrMsg, DEVICE_REG);
		setCriticalStepInProgressFlag(PAAS_FALSE);
		//rv = SUCCESS;
		if(rv == SUCCESS)
		{
			rv = VSP_FULLY_SUCCESS;
			break;
		}
		else if (rv == ERR_VCL_DATA_ERR || rv == ERR_VSP_HOST_ERR)
		{
			debug_sprintf(szDbgMsg, "%s: Error [%d] In Posting Data to Host", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
			if(rv == ERR_VCL_DATA_ERR)
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "VSP DDK Advanced in Device, But Some Error Occurred in Getting VCL Data");
					strcpy(szAppLogDiag, "We will Retry in Background");
					addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, szAppLogDiag);
				}
			}
			else if(rv == ERR_VSP_HOST_ERR)
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "VSP DDK Advanced Successfully in Device, But Failed to Sync with Host");
					strcpy(szAppLogDiag, "We will Retry in Background");
					addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, szAppLogDiag);
				}
			}
			showPSMessageDispForm(szDispMsg, MSG_VSP_DDK_ADV_DEV_DONE_HOST_FAIL, STANDBY_ICON, iStatusMsgDispInterval);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error [%d] In Posting Data to Host", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Error [%d] in VSP Advance DDK", rv);
				strcpy(szAppLogDiag, "We will Retry in Background");
				addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, szAppLogDiag);
			}
			break;
		}
		break;
	}

	if(rv != SUCCESS && isRedoVSPAfterAdvanceDDKActivated(PAAS_FALSE))
	{
		/*-------- Create the VSPRegPostAdvanceDDK Thread, This thread will keep on trying to do VSP Reg with Host until success ---------- */
		rv = pthread_create(&ptVSPRegPostAdvanceDDK, NULL, VSPRegPostAdvanceDDK, NULL);
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: VSP Registration after Advance DDK, thread created Successfully ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while creating VSP Registration after Advance DDK, thread", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	if(fpLastUpdateDate != NULL)
	{
		fclose(fpLastUpdateDate);
	}

	if(rv == VSP_FULLY_SUCCESS)
	{
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "VSP DDK Advance Success");
			addAppEventLog(SCA, PAAS_SUCCESS, START_UP, szAppLogData, NULL);
		}
		showPSMessageDispForm(szDispMsg, MSG_VSP_DDK_ADV_SUCCESS, SUCCESS_ICON, iStatusMsgDispInterval);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: VSPRegPostAdvanceDDK
 *
 * Description	: Thread used to do VSP Reg after Advance DDK Failed with
 * 				   Host, but Success in Device
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void * VSPRegPostAdvanceDDK(void * arg)
{
	int			iRetVal     		   	= SUCCESS;
	int			iAppLogEnabled			= isAppLogEnabled();
	char		szAppLogData[300]		= "";
	int			iWaitingTime 			= 60;// 1 minute wait time
	char		szTranStkKey[10]		= "";

#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: VSP Registration after Advance DDK, thread begins execution", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		svcWait(iWaitingTime*1000);

		memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
		iRetVal = doVSPReg(NULL, szTranStkKey, PAAS_FALSE, NULL, DEVICE_REG);
		if(iRetVal == SUCCESS)
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "VSP DDK Advance Success in Background");
				addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
			}

			debug_sprintf(szDbgMsg, "%s: VSP DDK Advance Success in Background!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		else
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "VSP DDK Advance Failed, We will Retry After 60 seconds");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, NULL);
			}

			debug_sprintf(szDbgMsg, "%s: VSP DDK Advance Failed, We will Retry After 60 seconds!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: VSP Registration after Advance DDK, thread existing!!!", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: setCardDataInBQueueFlag
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setCardDataInBQueueFlag(PAAS_BOOL bFlag)
{
	SESSDTLS_PTYPE		pstSess			= NULL;

	acquireMutexLock(&gptCardDataInBQueueMutex, "Setting the Card Data in BQueue");

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) == SUCCESS)
		{
			pstSess->bCardDataInBQueue = bFlag;
		}
	}

	releaseMutexLock(&gptCardDataInBQueueMutex, "Setting the Card Data in BQueue");

	return ;
}

/*
 * ============================================================================
 * Function Name: isCardDataInBQueueFlag
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isCardDataInBQueueFlag()
{
	SESSDTLS_PTYPE		pstSess			= NULL;
	PAAS_BOOL			bCardDataInBQueue = PAAS_FALSE;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
	acquireMutexLock(&gptCardDataInBQueueMutex, "Setting the Card Data in BQueue");

	if(pstSessMap != NULL)
	{
		/* Get the transaction reference */
		if(getSession(pstSessMap->szSessKey, &pstSess) == SUCCESS)
		{
			bCardDataInBQueue = pstSess->bCardDataInBQueue;
		}
	}

	debug_sprintf(szDbgMsg, "%s: CardDataInBQueueFlag [%s]", __FUNCTION__,
							 					(bCardDataInBQueue == PAAS_TRUE) ? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	releaseMutexLock(&gptCardDataInBQueueMutex, "Setting the Card Data in BQueue");

	return bCardDataInBQueue;
}

/*
 * ============================================================================
 * Function Name: processCustCheckbox
 *
 * Description	:This Function Captures the Checkbox Options from User.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processCustCheckbox(char * szTranKey, char * szStatMsg)
{
	int					    	rv 					= SUCCESS;
	int							iCnt				= 0;
	int							iTemp				= 0;
	int							iCurAppState		= 0;
	int							iAppLogEnabled		= isAppLogEnabled();
	char						szAppLogData[300]	= "";
	CUSTCHECKBOX_DTLS_STYPE		stCustCheckboxDtls;
#ifdef DEBUG
	char					szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iCurAppState = getAppState();

	setAppState(CAPTURING_DEV_CHECKBOX);
	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Capturing the Customer Checkbox Details From User");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}
		/* get the customer button from the transaction instance */
		memset(&stCustCheckboxDtls, 0x00, sizeof(CUSTCHECKBOX_DTLS_STYPE));
		rv = getCustCheckBoxDtlsForDevTran(szTranKey, &stCustCheckboxDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Customer Checkbox details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Customer Checkbox Details From User");
				addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
			}

			break;
		}
		for(iCnt = 0;iCnt < MAX_CHECKBOX; iCnt++)
		{
			if(strlen(stCustCheckboxDtls.szCheckboxLabels[iCnt]) > 0)
			{
				iTemp++;
			}
		}
		if(iTemp < 2)
		{
			debug_sprintf(szDbgMsg, "%s:Minimum 2 Checkbox Options Required!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			sprintf(szStatMsg, "Minimum 2 Checkbox Options Required");
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Missing Field : Minimum 2 Checkbox Options Required");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, NULL);
			}
			rv = ERR_FLD_REQD;
			return rv;
		}

		/* Capture the cutomer checkbox options by prompting the user on the device screen */
		rv = captureCustCheckbox(&stCustCheckboxDtls);
		if(rv == SUCCESS)
		{
			/* Update the customer Button details in the transaction instance */
			rv = updateCustCheckBoxDtlsForDevTran(szTranKey, &stCustCheckboxDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update customer Checkbox dtls",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Update Customer Checkbox Details");
					addAppEventLog(SCA, PAAS_INFO, DISPLAY_SCREEN, szAppLogData, NULL);
				}

				rv = FAILURE;
				break;
			}
			sprintf(szStatMsg, "Checkbox Option(s) Captured");
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Customer Checkbox Details Successfully Captured From User");
				addAppEventLog(SCA, PAAS_SUCCESS, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}
		else if(rv == UI_CANCEL_PRESSED)
		{
			debug_sprintf(szDbgMsg, "%s: User cancelled Customer Checkbox",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "User Pressed Cancel Button While Capturing the Customer Checkbox Details");
				addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
			}
			sprintf(szStatMsg, "CANCELED by Customer");
			rv = ERR_USR_CANCELED;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to capture customer Checkbox",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Capture Customer Checkbox Details From User");
				addAppEventLog(SCA, PAAS_FAILURE, CAPTURE_DETAILS, szAppLogData, NULL);
			}
		}

		break;
	}

	setAppState(iCurAppState);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * End of file blAPIs.c
 * ============================================================================
 */
