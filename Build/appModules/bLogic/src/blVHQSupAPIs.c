/*
 * =============================================================================
 * Filename    : blVHQSupAPIs.c
 *
 * Application : Mx Point SCA
 *
 * Description : This module is responsible for interacting with VHQ agent using callback functions/message queue through TMS library.
 * 				 It uses message queue IPC mechanism to send data to TMS library and TMS library uses registered
 * 				 function call back to notify/send data to SCA application.
 *
 * Modification History:
 *
 *  Date      Version No     	Programmer		  			Change History
 *  -------   -----------  	 ---------------------		------------------------
 *								ArjunU1, MukeshS3			20th August,15
 															3-March-16
															17-June-16
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved.
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include <signal.h>
#include <pthread.h>
#include <syslog.h>
#include <svc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/timeb.h>
#include "iniparser.h"

#include "common/common.h"
#include "common/utils.h"
#include "common/xmlUtils.h"
#include "common/tranDef.h"
#include "common/cfgData.h"
#include "bLogic/blVHQSupDef.h"
#include "bLogic/blAPIs.h"
#include "uiAgent/uiConstants.h"
#include "uiAgent/uiAPIs.h"
#include "uiAgent/uiCfgDef.h"
#include "appLog/appLogAPIs.h"
#include "ssi/ssiCfgDef.h"
#include "sci/sciConfigDef.h"
#include "db/safDataStore.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

//VHQ Library Send msg queue details. Msg Q is created by TMS Library.
static key_t					gVHQSndMsgQKey;
static int						gVHQSndMsgQId;

//TMS_LIB -> VHQThread Receive Data msg queue details. Msg Q is created by SCA.
static key_t					gVHQRcvFrmLIBMsgQKey;
static int						gVHQRcvFrmLIBMsgQId;

//Blogic -> VHQThread Receive Data msg queue details. Msg Q is created by SCA.
static key_t					gVHQRcvFrmPOSMsgQKey;
static int						gVHQRcvFrmPOSMsgQId;

static pthread_t				gVHQthreadId;
static pthread_mutex_t			gptAllowUpdtAvailableMutex;
static pthread_mutex_t			gptAllowUpdtStatusMutex;
static pthread_mutex_t			gptAllowUpdtDoneMutex;
static pthread_mutex_t			gptCriticalStepInProgressMutex;

//global variable to check SCA Initialization
extern int						gbSCAInitDone;

// global structure variable to store TMS settings
VHQ_CFG_STYPE vhqSettings;

// global variable to determine whether any VHQ updates are available or not
static PAAS_BOOL		gbVHQUpdateAvailable	= PAAS_FALSE;

//global variable to determine the current status of any VHQ update
static int				giVHQUpdateStatus		= VHQ_NO_UPDATES;

static int				gbShowIdleScrn			= PAAS_FALSE;

static PAAS_BOOL		gbVHQUpdateInProgress	= PAAS_FALSE;

static PAAS_BOOL		gbCriticalStepInProgress	= PAAS_FALSE;
//global static array to store App parameters, needed by VHQ agent
static struct tms_AppParameter 	gpstAppParams[MAX_APP_INFO];

/* static functions declarations */
static void VHQEventCallBack(TMS_LIBTOAPP_DATA);
static void *VHQThread(void *);
static int  createVHQRcvDataMsgQForLIB(void);
static int  createVHQRcvDataMsgQForPOS(void);
static int  setDynamicFunctionPtr(char *LibName, char *FuncName, void (**pFunc)(void *));
static int  printVHQAgentInfo(void);
static void flushVHQRcvFrmLIBMsgQ(int);
static void flushVHQRcvFrmPOSMsgQ(int );
static int  loadVHQConfigParams(void);


static int  processVHQMsg(TMS_LIBTOAPP_DATA *);
static int  processAppInfo(TMS_APPTOLIB_MSG *);
static int  processParamInstall(TMS_APPTOLIB_MSG *, TMS_LIBTOAPP_DATA *);
static int  processGetParams(TMS_APPTOLIB_MSG *);
static int  processInstallResult(int);

static int  getVHQSndMsgQueueId(void);
static int  getFileDtls(struct tmsFileDetails *, TMS_LIBTOAPP_DATA **);
static int  getAppInfo(int *);
static int  getApprovalForVHQEvent(int , int *, char *, TMS_LIBTOAPP_DATA*);
static int  checkAppStatus(int , int *, char *, TMS_LIBTOAPP_DATA*);
static int  waitForPOSApproval(int, int *, char *);
static int  waitForPOStoQueryUpdateStatus(int iTimeout);
static int  processParamFileForVHQ(int *, TMS_LIBTOAPP_DATA* );
static int  checkForCriticalParams(int *, GENKEYVAL_PTYPE );
static PAAS_BOOL isCriticalStepInProgress(void);

//static char* getTMStoPOSRespType(void);

static int  setAppResponseForVHQEvent(TMS_APPTOLIB_MSG *, int);
//static int  setAppInfoForVHQEvent(struct tms_AppParameter *, char **);

static int  copyFiletoDest(struct tmsFileDetails *);
static int  sendDatatoMsgQ(TMS_APPTOLIB_MSG *);
static char* intToStrRespMsg(int);
//static int  sendDataOnScndryPort(int *, char *);
//static int sendUnsolicitedMsg(int , char *);

//static void setupFileDebug(void);
//static int  mapParamsinMemory(struct tmsFileDetails *);


/* Function pointers for API's, exposed to SCA Application from TMS Library
 */
static int   (*fnpVHQInit)(TMS_APP_INIT);
static char* (*fnpGetSvcTmsVersion)(void);
static char* (*fnpGetTMSLibVersion)(void);
static int 	 (*fnpForceSetAppState)(int);
int   (*fnpVHQUnRgstrApp)(char *);

extern void saveLogs();

/*
 * ============================================================================
 * Function Name: initVHQSupport
 *
 * Description	: This function would call TMS library TMS_Init API.
 * 				  It will register the SCA Application with VHQ agent.
 * 				  It creates a VHQ Thread to handle all VHQ events.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int initVHQSupport(char *szErrMsg)
{
	int 			rv						= SUCCESS;
	int				iAppLogEnabled			= isAppLogEnabled();
	char			szAppLogData[300]		= "";
	TMS_APP_INIT	stAppInit;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s: ----- Enter ----- ",__FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Initialize VHQ Configuration settings */
		rv = loadVHQConfigParams();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: Failed to load VHQ config parameters",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			return rv;
		}

		if(!isVHQSupEnabled())
		{
			debug_sprintf(szDbgMsg,"%s: VHQ Support is disabled, No need to Initialize it",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "VHQ Support Is Disabled, No Need to Initialize It");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}
			break;
		}

		/*Initializing the TMS Library */

		debug_sprintf(szDbgMsg,"%s: Initializing VHQ Agent",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Initializing VHQ Support");
			addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
		}

		memset(&stAppInit, 0x00, sizeof(TMS_APP_INIT));

		/* Register The Event Callback Signature, Application Name & Event Timeout*/

		//strcpy(stAppInit.szAppName, SCA_APP_NAME);
		strcpy(stAppInit.szAppName, getSCAPkgName());
		stAppInit.pLibCallBack 		 = &VHQEventCallBack;
		stAppInit.iLibWaitTimeoutSec = getVHQEventRespTimeout();

		debug_sprintf(szDbgMsg,"%s: Linking The Function Pointers with TMS Library",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* TMS_Init */
		rv = setDynamicFunctionPtr(TMS_LIB_NAME, TMS_INIT, (void *) &fnpVHQInit);
		if( rv != SUCCESS )
		{
			debug_sprintf(szDbgMsg,"%s: Error while linking 'VHQInit' Function Pointer",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		/* TMS_GetVersion_SVC_Agent */
		rv = setDynamicFunctionPtr(TMS_LIB_NAME, TMS_GET_SVC_VERSION, (void *)&fnpGetSvcTmsVersion);
		if( rv != SUCCESS )
		{
			debug_sprintf(szDbgMsg,"%s: Error while linking 'GetVHQAgentVersion' Function Pointer",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		/* TMS_GetVersion_Lib */
		rv = setDynamicFunctionPtr(TMS_LIB_NAME, TMS_GET_LIB_VERSION, (void *)&fnpGetTMSLibVersion);
		if( rv != SUCCESS )
		{
			debug_sprintf(szDbgMsg,"%s: Error while linking 'GetTMSLibVersion' Function Pointer",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		/* TMS_ForceSetAppState */
		rv = setDynamicFunctionPtr(TMS_LIB_NAME, TMS_SET_APP_STATE, (void *) &fnpForceSetAppState);
		if( rv != SUCCESS )
		{
			debug_sprintf(szDbgMsg,"%s: Error while linking 'ForceSetAppState' Function Pointer",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		/* TMS_Unregister */
		rv = setDynamicFunctionPtr(TMS_LIB_NAME, TMS_UNREGISTER_APP, (void *) &fnpVHQUnRgstrApp);
		if( rv != SUCCESS )
		{
			debug_sprintf(szDbgMsg,"%s: Error while linking 'VHQUnregisterApp' Function Pointer",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		debug_sprintf(szDbgMsg,"%s: Registering Application with VHQ Agent...",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = fnpVHQInit(stAppInit);
		if(rv == LTMS_INIT_SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: Application Registered with VHQ Agent",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else if(rv ==  LTMS_INIT_FAIL)
		{
			debug_sprintf(szDbgMsg,"%s: VHQ Initialization Failure",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: Should not Come here",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Display VHQ Agent Information */
		rv = printVHQAgentInfo();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: Could not print VHQ Agent Information",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			//TODO: Error is required or not
		}

		 /* Get Msg Queue ID for sending the Msgs to VHQ Library */
		rv = getVHQSndMsgQueueId();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get message queue id of VHQ Library created Msg Queue", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Create a MSG Q to receive data from VHQ Agent through TMS library */
		debug_sprintf(szDbgMsg, "%s: Creating Queue to receive data from VHQ Agent through TMS library", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = createVHQRcvDataMsgQForLIB();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create message queue for receiving data from VHQ Library", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		//Its very important to flush the existing data in the message queue.
		flushVHQRcvFrmLIBMsgQ(gVHQRcvFrmLIBMsgQId);

		/* Create a MSG Q to receive data from BLogic thread */
		debug_sprintf(szDbgMsg, "%s: Creating Queue to receive data from Blogic thread", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = createVHQRcvDataMsgQForPOS();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create message queue for receiving data from Blogic thread", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		//Its very important to flush the existing data in the message queue.
		flushVHQRcvFrmPOSMsgQ(gVHQRcvFrmPOSMsgQId);

		/*
		 * This Mutex is created for global variables gbVHQUpdateAvailable and giVHQUpdateStatus
		 * These variables are shared across threads(VHQ and BLogic)
		 */
		if(pthread_mutex_init(&gptAllowUpdtAvailableMutex, NULL))//Initialize the AllowUpdtAvailable Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create Update Available Flag mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		if(pthread_mutex_init(&gptAllowUpdtStatusMutex, NULL))//Initialize the AllowUpdtStatus Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create Update Status mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		if(pthread_mutex_init(&gptAllowUpdtDoneMutex, NULL))//Initialize the AllowUpdtDone Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create Update Done mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		if(pthread_mutex_init(&gptCriticalStepInProgressMutex, NULL))//Initialize the SAFRecordsInProgress Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create Critical Step In Progress mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		/* Create VHQThread to process the Data received from TMS Library and send back the response through MSG Q*/

		rv = pthread_create(&gVHQthreadId, NULL, VHQThread, (void *)gVHQSndMsgQId);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create VHQ agent thread", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Created thread to handle VHQ Agent Events", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "VHQ Support Successfully Initialized");
			addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Initialized VHQ Support",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: VHQEventCallBack
 *
 * Description  : This call back function is called by TMS library whenever it wants to get approval from application
 * 				  or send some data to application.
 *
 * Input Params : TMS_LIBTOAPP_DATA
 *
 * Output Params: None
 * ============================================================================
 */
void VHQEventCallBack(TMS_LIBTOAPP_DATA stVHQCallBackData)
{
	int 				rv 		= SUCCESS;
	TMS_LIBTOAPP_DATA 	stVHQFunCallBckData;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s: ----- Enter ----- ",__FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&stVHQFunCallBckData, 0x00, sizeof(TMS_LIBTOAPP_DATA));
	memcpy(&stVHQFunCallBckData, &stVHQCallBackData, sizeof(TMS_LIBTOAPP_DATA));

	//Add vhq data to the SCA vhq message queue for processing.
	rv = msgsnd(gVHQRcvFrmLIBMsgQId, (void *)&stVHQFunCallBckData, sizeof(TMS_LIBTOAPP_DATA), IPC_NOWAIT); //IPC system call
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add VHQ Agent data to SCA VHQ MSG Queue",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Something went wrong msgsnd(): [%d] [%s]", __FUNCTION__, errno, strerror(errno));
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: VHQ Data Added to SCA-VHQ MsgQ for processing", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return ;
}


/*
 * ============================================================================
 * Function Name: VHQThread
 *
 * Description	: This thread would read data added by TMS library call back function to SCA msg queue. This thread would call the function
 * 				  handler based on the message type and send the response back to TMS Library through MSG Q.
 *
 * Input Params	: none
 *
 * Output Params: NULL
 * ============================================================================
 */
static void * VHQThread(void * arg)
{
//	int 				rv						= SUCCESS;
	int					iAppLogEnabled			= isAppLogEnabled();
	char				szAppLogData[300]		= "";
	TMS_LIBTOAPP_DATA	stVHQFunCallBckData;
#ifdef DEBUG
	char            szDbgMsg[1024]  = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VHQ Receive Data MSG Queue Id is [%d]", __FUNCTION__, gVHQRcvFrmLIBMsgQId);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(&stVHQFunCallBckData, 0x00, sizeof(TMS_LIBTOAPP_DATA));

		//Read data from the message queue added by VHQ library function call
		if( msgrcv(gVHQRcvFrmLIBMsgQId, &stVHQFunCallBckData, sizeof(TMS_LIBTOAPP_DATA), 0, MSG_NOERROR) == -1) // IPC system call
		{
			debug_sprintf(szDbgMsg, "%s: Failed to receive data from VHQ Receive Data MSG Queue ", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Something went wrong msgrcv(): %s", __FUNCTION__, strerror(errno));
			APP_TRACE(szDbgMsg);

			continue;
		}

		debug_sprintf(szDbgMsg, "%s:  Received data from SCA-VHQ MSG Q", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "***** Received VHQ Event *****");
			addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
		}

		debug_sprintf(szDbgMsg, "%s:  Processing the VHQ Data...", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//Process VHQ data received through function call
		processVHQMsg(&stVHQFunCallBckData);

	}
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}


/*
 * ============================================================================
 * Function Name: processVHQMsg
 *
 * Description  : This function check the Request type and calls
 * 				  the corresponding handler function to process it.
 * 				  After processing, it would send the Data to TMS Library through MSG Q.
 *
 * Input Params : TMS_LIBTOAPP_DATA *
 *
 * Output Params: NULL
 * ============================================================================
 */
static int processVHQMsg(TMS_LIBTOAPP_DATA * pstVHQFunCallBckData)
{
	int						rv						= SUCCESS;
	int						iEventType				= -1;
	int						iAppLogEnabled			= isAppLogEnabled();
	char					szAppLogData[300]		= "";
	char					szAppLogDiag[300]		= "";
	TMS_APPTOLIB_MSG		stVHQMsgQSndData;
//	POSRESPDATA_VHQ_STYPE	stPOSRespDataForVHQ;
#ifdef DEBUG
	char szDbgMsg[1024]   = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		if( pstVHQFunCallBckData == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: NULL param passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			return rv;
		}

		iEventType = pstVHQFunCallBckData->iReqType;

		debug_sprintf(szDbgMsg, "%s: Event Type --- [%d]", __FUNCTION__, iEventType);
		APP_TRACE(szDbgMsg);

		memset(&stVHQMsgQSndData, 0x00, sizeof(TMS_APPTOLIB_MSG));

		stVHQMsgQSndData.iReqType = iEventType;
		stVHQMsgQSndData.MsgType  = LTMS_MSGQ_TYPE;
		stVHQMsgQSndData.NotInUse = NULL;

		switch(iEventType)
		{
		case LTMS_MSGTYPE_GET_REBOOT_APPROVE:
				debug_sprintf(szDbgMsg, "%s: LTMS_MSGTYPE_GET_REBOOT_APPROVE", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Processing Device Reboot Approval");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
				rv = setAppResponseForVHQEvent(&stVHQMsgQSndData, iEventType);
				break;

		case LTMS_MSGTYPE_GET_APPRESTART_APPROVE:
				debug_sprintf(szDbgMsg, "%s: LTMS_MSGTYPE_GET_APPRESTART_APPROVE", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Processing Application Restart Approval");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
				rv = setAppResponseForVHQEvent(&stVHQMsgQSndData, iEventType);
				break;

		case LTMS_MSGTYPE_GET_INSTALL_APPROVE:
				debug_sprintf(szDbgMsg, "%s: LTMS_MSGTYPE_GET_INSTALL_APPROVE", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Processing Installation Approval For Content/Software");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
				rv = setAppResponseForVHQEvent(&stVHQMsgQSndData, iEventType);
				break;

				//TODO: Handling for any other unknown events
		case LTMS_MSGTYPE_GET_APP_STATE:
				debug_sprintf(szDbgMsg, "%s: LTMS_MSGTYPE_GET_APP_STATE", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Processing Get App State Event");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
				rv = setAppResponseForVHQEvent(&stVHQMsgQSndData, iEventType);
				break;

		case LTMS_MSGTYPE_GET_CLOCKUPDATE_APPROVE:
				debug_sprintf(szDbgMsg, "%s: LTMS_MSGTYPE_GET_CLOCKUPDATE_APPROVE", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Processing Set Clock Approval");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
				rv = setAppResponseForVHQEvent(&stVHQMsgQSndData, iEventType);
				break;
		case LTMS_MSGTYPE_GET_PARAM_LIST:
				debug_sprintf(szDbgMsg, "%s: LTMS_MSGTYPE_GET_PARAM_LIST", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Processing Parameter List Event");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
				//  The function is not doing anything currently
				rv = processGetParams(&stVHQMsgQSndData);
				break;

		case LTMS_MSGTYPE_GET_APP_INFO:
				debug_sprintf(szDbgMsg, "%s: LTMS_MSGTYPE_GET_APP_INFO", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Processing Heartbeat/Status Notification");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
				rv = processAppInfo(&stVHQMsgQSndData);
				break;


#if 0
		case LTMS_MSGTYPE_APPRESTART_NOTIFY:
				printf("App is Restarting...\n");
				/* do clean up / Backup if needed */
				break;

		case LTMS_MSGTYPE_REBOOT_NOTIFY:
				/* do clean up / Backup if needed */
				printf("Device is Rebooting...\n");
				break;
#endif

		case LTMS_MSGTYPE_INSTALLFILE_FULL:
				debug_sprintf(szDbgMsg, "%s: LTMS_MSGTYPE_INSTALLFILE_FULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Processing Parameter Installation");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
				rv = processParamInstall(&stVHQMsgQSndData, pstVHQFunCallBckData);
				break;

		case LTMS_MSGTYPE_CONTENTINST_SUCCESS_NOTIFY:
				debug_sprintf(szDbgMsg, "%s: LTMS_MSGTYPE_CONTENTINST_SUCCESS_NOTIFY", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Content Is Successfully Installed");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
				rv = processInstallResult(iEventType);
				break;

		case LTMS_MSGTYPE_CONTENTINST_ERROR_NOTIFY:
				debug_sprintf(szDbgMsg, "%s: LTMS_MSGTYPE_CONTENTINST_ERROR_NOTIFY", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Content Installation Failed");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
				rv = processInstallResult(iEventType);
				break;
		case LTMS_INSTALLSUCCESS_NOTIFY:
				debug_sprintf(szDbgMsg, "%s: LTMS_INSTALLSUCCESS_NOTIFY", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Installation Successfull");
					addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
				}
				rv = processInstallResult(iEventType);
				break;
		case LTMS_INSTALLSUCCESS_APPRESTART_NOTIFY:
				debug_sprintf(szDbgMsg, "%s: LTMS_INSTALLSUCCESS_APPRESTART_NOTIFY", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Installation Successfull, Application Is Going To Restart...");
					addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
				}
				rv = processInstallResult(iEventType);
				break;
		case LTMS_INSTALLSUCCESS_REBOOT_NOTIFY:
				debug_sprintf(szDbgMsg, "%s: LTMS_INSTALLSUCCESS_REBOOT_NOTIFY", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Installation Successfull, Device Is Going To Reboot...");
					addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
				}
				rv = processInstallResult(iEventType);
				break;
		case LTMS_INSTALLFAILED_NOTIFY:
				debug_sprintf(szDbgMsg, "%s: LTMS_INSTALLFAILED_NOTIFY", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Installation Failed");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}
				rv = processInstallResult(iEventType);
				break;
		default:
				debug_sprintf(szDbgMsg, "%s: Undefined Event!!! Should not come here", __FUNCTION__);
				APP_TRACE(szDbgMsg);
		}

		/* Need to Send Data to TMS Library through MSG Q */
		if(iEventType == LTMS_MSGTYPE_GET_APP_INFO 					|| 		// Heartbeat
				iEventType == LTMS_MSGTYPE_GET_PARAM_LIST			||		// Parameter list(empty as of now)
				iEventType == LTMS_MSGTYPE_GET_INSTALL_APPROVE		||		// Content/Software install approve
				iEventType == LTMS_MSGTYPE_INSTALLFILE_FULL			||		// Parameter installation result
				iEventType == LTMS_MSGTYPE_GET_APP_STATE			||		// App state response
				iEventType == LTMS_MSGTYPE_GET_REBOOT_APPROVE		||		// Reboot approve
				iEventType == LTMS_MSGTYPE_GET_APPRESTART_APPROVE 	||		// App restart approve
				iEventType == LTMS_MSGTYPE_GET_CLOCKUPDATE_APPROVE )		// Set Clock approve
		{
			rv = sendDatatoMsgQ(&stVHQMsgQSndData);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Post data to Msg Q", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			debug_sprintf(szDbgMsg, "%s: Data[%s] successfully sent to VHQ Agent [%s]", __FUNCTION__,
														intToStrRespMsg(stVHQMsgQSndData.uAppData.stTMSAppStatus.iAppStatus),	// response
														stVHQMsgQSndData.uAppData.stTMSAppStatus.szStatusDescription);		// result description
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Data[%s] Successfully Sent to VHQ Agent [%s]",
												intToStrRespMsg(stVHQMsgQSndData.uAppData.stTMSAppStatus.iAppStatus),	// response
												stVHQMsgQSndData.uAppData.stTMSAppStatus.szStatusDescription);		// result description
				addAppEventLog(SCA, PAAS_INFO, SENT, szAppLogData, NULL);
			}
		}
		// After parameter installation, applcation needs to be restarted to consume the new parameters.
		if((iEventType == LTMS_MSGTYPE_INSTALLFILE_FULL) && (stVHQMsgQSndData.uAppData.stTMSAppStatus.iAppStatus == LTMS_MSGRESP_FILEINSTALL_SUCCESS))
		{
			gbShowIdleScrn = PAAS_FALSE;	// setting to FALSE for next Event

			// wait here to make sure that VHQ agent sends Install result to VHQ server. So, after that we can restart the App.
			svcWait(20000);

			debug_sprintf(szDbgMsg, "%s: Application is going to restart", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			setVHQUpdateStatus(VHQ_UPDATE_SUCCESS_RESTARTING_APP);

			if(gbSCAInitDone == PAAS_TRUE && isSecondaryPortEnabled() && isPOSApprReqdForUpdate())
			{
				/* Before going for restart, application has to to make sure that parameter installation status has been taken by POS from Secondary PORT
				 * that's why we are waiting here
				 */
				showPSGenDispForm(MSG_WAITING_BEFORE_RESTART, STANDBY_ICON, 0);	//display waiting Message here.

				waitForPOStoQueryUpdateStatus((getPOSNotificationTimoutValue() * 60));	// this is blocked call, which will wait until POS queries for UPDATE STATUS
																// command on secondary port or posnotifytimeout timer is running.
			}

			// Waiting here to make sure that any SAF transactions are not getting posted just before going for restart.
			while(isCriticalStepInProgress())
			{
				debug_sprintf(szDbgMsg, "%s: Application is posting SAF records",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				svcWait(10000);
			}

			// If SCA has not come up fully than SCA may not be able to send request to FA for displaying the FORM.
			if(gbSCAInitDone == PAAS_TRUE)
			{
				/* showing Application Restart message after parameter installation */
				showPSGenDispForm(MSG_RESTARTING_AFTER_UPDATE, STANDBY_ICON, DFLT_STATUS_DISP_TIME);

				setAppState(NO_SESSION_IDLE);
			}

			setVHQUpdateInProgressFlag(PAAS_FALSE);

			rv = restartSCA();
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Not able to restart the application",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Restart The Application");
					strcpy(szAppLogDiag, "Please Restart The Application Manually to Consume The Parameters");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
				}
			}
		}
		else if(gbShowIdleScrn == PAAS_TRUE && gbSCAInitDone == PAAS_TRUE)
		{
			showIdleScreen();
			gbShowIdleScrn = PAAS_FALSE;
			setVHQUpdateInProgressFlag(PAAS_FALSE);	// 11-Dec-15: Resetting this flag here after showing the idle screen.
		}
		break;
	}	// END OF WHILE LOOP

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setDynamicFunctionPtr
 *
 * Description	: This function sets the function pointer
 * 				  in TMS library depends on the configuration
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int setDynamicFunctionPtr(char *pszLibName, char *pszFuncName, void (**pFunc)(void *))
{
	int			rv				= SUCCESS;
    void 		*pFunctionLib 	= NULL;   // Handle to shared lib file
    const char 	*pdlError;        // Pointer to error string

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    // Open Dynamic Loadable Libary using LD_LIBRARY path
    pFunctionLib = dlopen(pszLibName, RTLD_LAZY);	// NOTE: Don't attempt to close or free this resource allocated to pFunctionLib, it will unlink the opened library.
    pdlError = dlerror();
    if (pdlError)
    {
    	debug_sprintf(szDbgMsg, "%s - unable to dynamically open %s!, error=[%s]",
                						__FUNCTION__, ((pszLibName) ? pszLibName : "application"), pdlError);
    	APP_TRACE(szDbgMsg);

    	// CID-67295: 29-Jan-16: MukeshS3: Need to free the resource allocated for handle of shared library.
        if(pFunctionLib != NULL)
        {
        	dlclose(pFunctionLib);
        }
        pFunc = NULL;
        rv = FAILURE;
    }
    else
    {
        if (pszLibName)
        {
            debug_sprintf(szDbgMsg, "%s - loaded [%s] module!", __FUNCTION__, pszLibName);
            APP_TRACE(szDbgMsg);
        }

        //CID 68334 (#1 of 1): Dereference before null check (REVERSE_INULL) T_RaghavendranR1, Check for NULL added before passing it on to dlsym function.
        if(pFunctionLib != NULL)
        {
        	// Find requested function
        	*pFunc = dlsym(pFunctionLib, pszFuncName);
        	pdlError = dlerror();
        	if (pdlError)
        	{
        		debug_sprintf(szDbgMsg, "%s - unable to locate function [%s] in %s, error=[%s]",
        				__FUNCTION__, pszFuncName, ((pszLibName) ? pszLibName : "application"), pdlError);
        		APP_TRACE(szDbgMsg);

        		*pFunc = NULL;
        		rv = FAILURE;
        	}
        	else
        	{
        		debug_sprintf(szDbgMsg, "%s - obtained [%s] pointer=%p", __FUNCTION__, pszFuncName, pFunc);
        		APP_TRACE(szDbgMsg);
        	}

    		// CID-67295: 29-Jan-16: MukeshS3, T_RaghavendranR1: Need to free the resource allocated for handle of shared library.
    		//dlclose(pFunctionLib);	Mukesh: Commenting this here. we are not supposed to unlink the library when it is in use,
        	// we can ignore the Coverity issue here, it is safe & intentional.
        }
    }

    debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
    APP_TRACE(szDbgMsg);

    return rv;
}

/*
 * ============================================================================
 * Function Name: printVHQAgentInfo
 *
 * Description  : This function shows the SVC/TMS Version and TMS Library version.
 *
 * Input Params : none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int printVHQAgentInfo()
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	char 			*pszLibVrsn		 	= NULL;
	char 			*pszAgentVrsn 		= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	pszAgentVrsn = fnpGetSvcTmsVersion();
	if(pszAgentVrsn != NULL)
	{
		debug_sprintf(szDbgMsg,"%s: SVC/VHQ Agent Version [%s]",__FUNCTION__, pszAgentVrsn);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "VHQ Agent Version [%s]", pszAgentVrsn);
			addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: SVC/VHQ Agent Version is not available",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}


	pszLibVrsn = fnpGetTMSLibVersion();
	if(pszLibVrsn != NULL)
	{
		debug_sprintf(szDbgMsg,"%s: TMS Library Version [%s]",__FUNCTION__, pszLibVrsn);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "TMS Library Version [%s]", pszLibVrsn);
			addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: TMS Library Version is not available",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if((pszAgentVrsn == NULL) && (pszLibVrsn == NULL))
	{
		rv = FAILURE;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getVHQSndMsgQueueId
 *
 * Description	: This function would get msg queue id of VHQ msg queue created by TMS library to send data from application.
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
static int getVHQSndMsgQueueId()
{
	int				rv					= SUCCESS;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (gVHQSndMsgQKey = ftok(LTMS_MSGQ_PATH, LTMS_MSGQ_ID)) == -1 )
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to generate MSG Queue key for communicating with VHQ Agent", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Something went wrong ftok(): %s, errno=%d", __FUNCTION__,
					strerror(errno), errno);
			APP_TRACE(szDbgMsg);


			rv = FAILURE;
			break;
		}

		if( (gVHQSndMsgQId = msgget(gVHQSndMsgQKey, 0666 )) == -1 ) //Queue should be already existed. VHQ library would have created msg Queue.
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get MSG Queue Id for VHQ Agent", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Something went wrong msgget(): %s, errno=%d", __FUNCTION__,
					strerror(errno), errno);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Got MSG Q to send Data to TMS Library",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Message Queue ID=%d", __FUNCTION__, gVHQSndMsgQId);
		APP_TRACE(szDbgMsg);
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: createVHQRcvDataMsgQForLIB
 *
 * Description	: This function would create msg queue. TMS library call-back function will add
 * 				  data into this msg queue and immediately function returns to the caller function. In SCA application "VHQThread" will
 * 				  read data from this queue and process it.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int createVHQRcvDataMsgQForLIB()
{
	int				rv					= SUCCESS;
	FILE *			fp					= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		//Create tmp file to get unique msg key from it.
		fp = fopen(SCA_VHQ_MSGQ_PATH, "a+");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create/open file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		if( (gVHQRcvFrmLIBMsgQKey = ftok(SCA_VHQ_MSGQ_PATH, SCA_VHQ_MSGQ_ID)) == -1 )
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to generate MSG Queue key for receiving data from VHQ Agent", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Something went wrong ftok(): %s, errno=%d", __FUNCTION__,
					strerror(errno), errno);
			APP_TRACE(szDbgMsg);


			rv = FAILURE;
			break;
		}

		if( (gVHQRcvFrmLIBMsgQId = msgget(gVHQRcvFrmLIBMsgQKey, 0666 | IPC_CREAT )) == -1 )
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Receive MSG Queue Id for VHQ Agent", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Something went wrong msgget(): %s, errno=%d", __FUNCTION__,
					strerror(errno), errno);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Created SCA-MSG Q to process the received data from TMS LIB",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;
	}
	/* Daivik:27/1/2016 - Coverity 67261 - Handling the Closing of the opened file here , irrespective of success/fail of any of above function calls */
	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: createVHQRcvDataMsgQForPOS
 *
 * Description	: This function would create msg queue to listen from BLogic thread.
 * 				  Once POS give any approval for any VHQ event, bLogic thread will add response to this MsgQ.
 * 				  VHQ thread will pick this response and act upon it.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int createVHQRcvDataMsgQForPOS()
{
	int				rv					= SUCCESS;
	FILE *			fp					= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		//Create tmp file to get unique msg key from it.
		fp = fopen(POS_VHQ_MSGQ_PATH, "a+");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create/open file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		if( (gVHQRcvFrmPOSMsgQKey = ftok(POS_VHQ_MSGQ_PATH, POS_VHQ_MSGQ_ID)) == -1 )
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to generate MSG Queue key for receiving data from BLogic", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Something went wrong ftok(): %s, errno=%d", __FUNCTION__,
					strerror(errno), errno);
			APP_TRACE(szDbgMsg);


			rv = FAILURE;
			break;
		}

		if( (gVHQRcvFrmPOSMsgQId = msgget(gVHQRcvFrmPOSMsgQKey, 0666 | IPC_CREAT )) == -1 )
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Receive MSG Queue Id for POS", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Something went wrong msgget(): %s, errno=%d", __FUNCTION__,
					strerror(errno), errno);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Created SCA-MSG Q to receive data from Blogic",__FUNCTION__);
		APP_TRACE(szDbgMsg);
/*	// CID-67447: 22-Jan-16: MukeshS3: moved down after breaking from main while loop, to make sure that we always free this resource.
		if(fp != NULL)
		{
			fclose(fp);
		}
*/
		break;
	}
	// CID-67447: 22-Jan-16: MukeshS3:
	if(fp != NULL)
	{
		fclose(fp);
	}
	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: flushVHQRcvFrmLIBMsgQ
 *
 * Description	: This Function would read the message queue and ignores the data in order to flush the queue first time.
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
static void flushVHQRcvFrmLIBMsgQ(int ivhqRcvMsgQId)
{
	int								rv 				= SUCCESS;
	int 							iCnt				= 0;
	TMS_LIBTOAPP_DATA			 	stVHQFunCallBckData;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	// Flush the queue in case there are remnant messages in it
	debug_sprintf(szDbgMsg, "%s: Message Queue ID=%d", __FUNCTION__, gVHQRcvFrmLIBMsgQId);
	APP_TRACE(szDbgMsg);

	memset(&stVHQFunCallBckData, 0x00, sizeof(TMS_LIBTOAPP_DATA));
	// Read through all messages on queue and discard
	while( (rv = msgrcv(ivhqRcvMsgQId, &stVHQFunCallBckData, sizeof(TMS_LIBTOAPP_DATA), 0, IPC_NOWAIT)) > 0 )
	{
		iCnt++;
		debug_sprintf(szDbgMsg, "%s: Flushing Message Queue %d - msg# %d", __FUNCTION__, ivhqRcvMsgQId, iCnt);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returned", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: flushVHQRcvFrmPOSMsgQ
 *
 * Description	: This Function would read the message queue and ignores the data in order to flush the queue first time.
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
static void flushVHQRcvFrmPOSMsgQ(int ivhqRcvMsgQId)
{
	int								rv 				= SUCCESS;
	int 							iCnt				= 0;
	POSRESPDATA_VHQ_STYPE				stPOSRespDataForVHQ;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	// Flush the queue in case there are remnant messages in it
	debug_sprintf(szDbgMsg, "%s: Message Queue ID=%d", __FUNCTION__, gVHQRcvFrmPOSMsgQId);
	APP_TRACE(szDbgMsg);

	memset(&stPOSRespDataForVHQ, 0x00, sizeof(POSRESPDATA_VHQ_STYPE));
	// Read through all messages on queue and discard
	while( (rv = msgrcv(ivhqRcvMsgQId, &stPOSRespDataForVHQ, sizeof(POSRESPDATA_VHQ_STYPE), 0, IPC_NOWAIT)) > 0 )
	{
		iCnt++;
		debug_sprintf(szDbgMsg, "%s: Flushing Message Queue %d - msg# %d", __FUNCTION__, ivhqRcvMsgQId, iCnt);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returned", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}


/*
 * ============================================================================
 * Function Name: setAppResponseForVHQEvent
 *
 * Description	: This Function sets the Application state for different approvals(Reboot, AppRestart, Installation).
 *
 * Input Params	: TMS_APPTOLIB_MSG*, Event Type
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int setAppResponseForVHQEvent(TMS_APPTOLIB_MSG* pstVHQMsgQSndData, int iEventType)
{
	int 				rv 										= SUCCESS;
	int 				iStateForVHQ							= SCA_APP_BUSY;
	char				szRsltDscrp[LTMS_MAX_MSG_DESCRP + 1]	= "";
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( pstVHQMsgQSndData == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: NULL param passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Need to check the Application status and/or POS permissions before setting the status or giving any approval to VHQ */
		rv = getApprovalForVHQEvent(iEventType, &iStateForVHQ, szRsltDscrp, NULL);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s:Couldn't get the Approval for VHQ Event", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Set the App Status for following Approvals after enquiring into SCA & POS*/

		if(iStateForVHQ == SCA_APP_FREE)
		{
			debug_sprintf(szDbgMsg,"%s: Application is FREE",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			switch(iEventType)
			{
			case LTMS_MSGTYPE_GET_APP_STATE:
					pstVHQMsgQSndData->uAppData.stTMSAppStatus.iAppStatus = LTMS_MSGRESP_FREE;
					strcpy(pstVHQMsgQSndData->uAppData.stTMSAppStatus.szStatusDescription, szRsltDscrp);
					break;

			case LTMS_MSGTYPE_GET_REBOOT_APPROVE:
			case LTMS_MSGTYPE_GET_APPRESTART_APPROVE:
			case LTMS_MSGTYPE_GET_INSTALL_APPROVE:
			case LTMS_MSGTYPE_GET_CLOCKUPDATE_APPROVE:
					pstVHQMsgQSndData->uAppData.stTMSAppStatus.iAppStatus = LTMS_MSGRESP_APPROVE;
					strcpy(pstVHQMsgQSndData->uAppData.stTMSAppStatus.szStatusDescription, szRsltDscrp);

					//setVHQUpdateStatus(VHQ_UPDATE_SUCCESS);	//28-Dec-15: Keeping the Update status in Processing state for Content/Software update.
					//Once application gets notification from TMS library about result of Content/Software, it will be changed: EMS-393

					//setVHQUpdateInProgressFlag(PAAS_FALSE);	// 11-Dec-15: Putting at last after processing each VHQ event.

					//gbShowIdleScrn = PAAS_TRUE;				//28-Dec-15: Keeping the same screen

					break;
			default:
					break;
			}
		}
		else if(iStateForVHQ == SCA_APP_BUSY)
		{
			debug_sprintf(szDbgMsg,"%s: Application is BUSY",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			switch(iEventType)
			{
			case LTMS_MSGTYPE_GET_APP_STATE:
					pstVHQMsgQSndData->uAppData.stTMSAppStatus.iAppStatus = LTMS_MSGRESP_BUSY;
					strcpy(pstVHQMsgQSndData->uAppData.stTMSAppStatus.szStatusDescription, szRsltDscrp);
					break;

			case LTMS_MSGTYPE_GET_REBOOT_APPROVE:
			case LTMS_MSGTYPE_GET_APPRESTART_APPROVE:
			case LTMS_MSGTYPE_GET_INSTALL_APPROVE:
			case LTMS_MSGTYPE_GET_CLOCKUPDATE_APPROVE:
					pstVHQMsgQSndData->uAppData.stTMSAppStatus.iAppStatus = LTMS_MSGRESP_REJECT;
					strcpy(pstVHQMsgQSndData->uAppData.stTMSAppStatus.szStatusDescription, szRsltDscrp);
					break;
			}
		}
		break;
	}

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Something went wrong[%d]; sending busy response", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);
		pstVHQMsgQSndData->uAppData.stTMSAppStatus.iAppStatus = LTMS_MSGRESP_REJECT;
		strcpy(pstVHQMsgQSndData->uAppData.stTMSAppStatus.szStatusDescription, "Failure");

		rv = SUCCESS;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processAppInfo
 *
 * Description	: This Function sets the Application Information(LaneID, StoreID), required by VHQ Agent at each Heartbeat Message.
 *
 * Input Params	: TMS_APPTOLIB_MSG*
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int processAppInfo(TMS_APPTOLIB_MSG* pstVHQMsgQSndData)
{
	int 						rv 							= SUCCESS;
	int 						iTotCnt						= 0;
	int							iCnt						= 0;
//	char 						*pszAppInfo[MAX_APP_INFO]	= {NULL};
//	struct tms_AppParameter 	*pstAppParams 				= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( pstVHQMsgQSndData == NULL )
	{
		debug_sprintf(szDbgMsg, "%s: NULL param passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	/* Initialize the App Info */
	pstVHQMsgQSndData->uAppData.stTMSAppInfo.stparameterList = NULL;
	pstVHQMsgQSndData->uAppData.stTMSAppInfo.iparameterCount = 0;


	while(1)
	{
		if(gbSCAInitDone == PAAS_TRUE)
		{
			rv = getAppInfo(&iTotCnt);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get App Info", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			if(iTotCnt > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Total Parameters[%d]", __FUNCTION__, iTotCnt);
				APP_TRACE(szDbgMsg);

				pstVHQMsgQSndData->uAppData.stTMSAppInfo.stparameterList = gpstAppParams;
				pstVHQMsgQSndData->uAppData.stTMSAppInfo.iparameterCount = iTotCnt;
			}
		}

#if 0
		if(gbSCAInitDone == PAAS_TRUE)
		{
			rv = getAppInfo(pszAppInfo, &iTotCnt);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get App Info", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
		}


		debug_sprintf(szDbgMsg, "%s: Total Parameters[%d]", __FUNCTION__, iTotCnt);
		APP_TRACE(szDbgMsg);

		if(iTotCnt > 0)
		{
			pstAppParams = (struct tms_AppParameter *)malloc(iTotCnt * sizeof(struct tms_AppParameter));
			if(pstAppParams == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				return rv;
			}
			else
			{
				memset(pstAppParams, 0x00, sizeof(iTotCnt * sizeof(struct tms_AppParameter)));
				/* set the App Info into structure */
				rv = setAppInfoForVHQEvent(pstAppParams, pszAppInfo);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to set App Info", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				pstVHQMsgQSndData->uAppData.stTMSAppInfo.parameterList = pstAppParams;
				pstVHQMsgQSndData->uAppData.stTMSAppInfo.parameterCount = iTotCnt;
			}
		}
#endif

		for(iCnt = 0; iCnt < iTotCnt ; iCnt++)
		{
			debug_sprintf(szDbgMsg, "%s: param[%d] [%s] value[%s] Type[%d]", __FUNCTION__, iCnt+1,
																						pstVHQMsgQSndData->uAppData.stTMSAppInfo.stparameterList[iCnt].szparameterName,
																						pstVHQMsgQSndData->uAppData.stTMSAppInfo.stparameterList[iCnt].szparameterValue,
																						pstVHQMsgQSndData->uAppData.stTMSAppInfo.stparameterList[iCnt].iparameterType);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Something went wrong[%d]; Nothing to send", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);
		pstVHQMsgQSndData->uAppData.stTMSAppInfo.stparameterList = NULL;
		pstVHQMsgQSndData->uAppData.stTMSAppInfo.iparameterCount = 0;
		rv = SUCCESS;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processParamInstall
 *
 * Description	: This Function Install the Parameter File in usr1/flash directory.
 *
 * Input Params	: TMS_APPTOLIB_MSG *, TMS_LIBTOAPP_DATA*
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int processParamInstall(TMS_APPTOLIB_MSG *pstVHQMsgQSndData, TMS_LIBTOAPP_DATA* pstVHQFunCallBckData)
{
	int 						rv 										= SUCCESS;
	int							iStateForVHQ							= SCA_APP_BUSY;
	char						szRsltDscrp[LTMS_MAX_MSG_DESCRP + 1]	= "";
	struct tmsFileDetails 		stFileDtls;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((pstVHQMsgQSndData == NULL) || (pstVHQFunCallBckData == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL param passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		rv = getApprovalForVHQEvent(LTMS_MSGTYPE_INSTALLFILE_FULL, &iStateForVHQ, szRsltDscrp, pstVHQFunCallBckData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s:Couldn't get the Approval for VHQ Event", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		if(iStateForVHQ == SCA_APP_FREE)
		{
			memset(&stFileDtls, 0x00, sizeof(stFileDtls));
			rv = getFileDtls(&stFileDtls, &pstVHQFunCallBckData);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Could not get the File details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				memset(szRsltDscrp, 0x00, sizeof(szRsltDscrp));
				strcpy(szRsltDscrp, "Parameter File Not Present");
				break;
			}

			/* Copy Parameter File from source location to Destination location given by library */
			rv = copyFiletoDest(&stFileDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while copying parameter file !!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				memset(szRsltDscrp, 0x00, sizeof(szRsltDscrp));
				strcpy(szRsltDscrp, "Installation Error");
				break;
			}

			pstVHQMsgQSndData->uAppData.stTMSAppStatus.iAppStatus = LTMS_MSGRESP_FILEINSTALL_SUCCESS;
			strcpy(pstVHQMsgQSndData->uAppData.stTMSAppStatus.szStatusDescription, szRsltDscrp);

			debug_sprintf(szDbgMsg, "%s: Parameter Installation SuccessFull", __FUNCTION__);
			APP_TRACE(szDbgMsg);

//			setVHQUpdateStatus(VHQ_UPDATE_SUCCESS_RESTARTING_APP);	//shifted at last of processVHQMsg

#if 0
			setAppState(NO_SESSION_IDLE);

			//gbShowIdleScrn = PAAS_TRUE;

			setVHQUpdateInProgressFlag(PAAS_FALSE);


			if(rv != SUCCESS)
			{
				/* Setting the Update status here for failure*/
				setVHQUpdateStatus(VHQ_UPDATE_FAILED);
			}
			else
			{
				/* Setting the Update status here for Success*/
				setVHQUpdateStatus(VHQ_UPDATE_SUCCESS_RESTARTING_APP);
			}
#endif
		}
		else if(iStateForVHQ == SCA_APP_BUSY)
		{
			debug_sprintf(szDbgMsg, "%s: Application is Busy; Can't process parameter download", __FUNCTION__);
			APP_TRACE(szDbgMsg);

		//	pstVHQMsgQSndData->uAppData.stTMSAppStatus.iAppStatus = LTMS_MSGRESP_FILEINSTALL_FAIL;	//28-Dec-15: Using new POSTPONE status
			pstVHQMsgQSndData->uAppData.stTMSAppStatus.iAppStatus = LTMS_MSGRESP_FILEINSTALL_POSTPONED;
			strcpy(pstVHQMsgQSndData->uAppData.stTMSAppStatus.szStatusDescription, szRsltDscrp);

#if 0
			rv = fnpForceSetAppState(LTMS_MSGRESP_BUSY);
			if(rv == LTMS_CALLSERVER_SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully set the BUSY status to VHQ server", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else if(rv == LTMS_GENERIC_ERROR)
			{
				debug_sprintf(szDbgMsg, "%s: could not connect to VHQ server; Unable to set BUSY status", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
#endif
		}

		break;
	}

	if(rv != SUCCESS)//Not a hard error; Need to send Installation failure response back to the VHQ Agent.
	{
		// CID 67257 (#1 of 1): Dereference after null check (FORWARD_NULL). T_RaghavendranR1. Check for NULL added.
		if(pstVHQMsgQSndData != NULL)
		{
			pstVHQMsgQSndData->uAppData.stTMSAppStatus.iAppStatus = LTMS_MSGRESP_FILEINSTALL_FAIL;
			strcpy(pstVHQMsgQSndData->uAppData.stTMSAppStatus.szStatusDescription, szRsltDscrp);
		}

		debug_sprintf(szDbgMsg, "%s: Parameter Installation Failure", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		setVHQUpdtAvailableFlag(PAAS_FALSE);

		setVHQUpdateStatus(VHQ_UPDATE_FAILED);

		setAppState(NO_SESSION_IDLE);

		gbShowIdleScrn = PAAS_TRUE;

		//setVHQUpdateInProgressFlag(PAAS_FALSE); // 11-Dec-15: Putting at last after sending data to agent & showing IDLE screen.

		rv = SUCCESS;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sendDatatoMsgQ
 *
 * Description	: This Function sends data to TMS library trough MSG Q.
 *
 * Input Params	: TMS_APPTOLIB_MSG*, *arg
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int sendDatatoMsgQ(TMS_APPTOLIB_MSG *pstVHQMsgQSndData)
{
	int			rv 				= SUCCESS;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	while(1)
	{
		if(pstVHQMsgQSndData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL param passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			return rv;
		}

		do // Loop, to ensure we retry in case we get interrupted by a signal
		{
			//Add SCA App data to msg queue for sending it to TMS Library.
			rv = msgsnd(gVHQSndMsgQId, pstVHQMsgQSndData, sizeof(TMS_APPTOLIB_MSG) - sizeof(long), IPC_NOWAIT); //IPC system call
	//		debug_sprintf(szDbgMsg, "%s:Sending data - %d ",__FUNCTION__,rv);
	//		APP_TRACE(szDbgMsg);
		}while ( (rv == -1) && (errno == EINTR));

		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to send application data to VHQ Agent",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Something went wrong msgsnd(): [%d] [%s]", __FUNCTION__, errno, strerror(errno));
			APP_TRACE(szDbgMsg);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: copyFiletoDest
 *
 * Description	: This Function cpoies the parameter file to flash/destination folder.
 *
 * Input Params	: struct tmsFileDetails *
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int copyFiletoDest(struct tmsFileDetails *pstFileDtls)
{
	int 				rv 				= SUCCESS;
	char 				szCommand[100] 	= "";
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	if(strlen(pstFileDtls->szSrcFile) > 0)	//check the File name
	{
		debug_sprintf(szDbgMsg, "%s: Source File [%s]", __FUNCTION__, pstFileDtls->szSrcFile);
		APP_TRACE(szDbgMsg);
		if(doesFileExist(pstFileDtls->szSrcFile) == SUCCESS)	//check the File existance
		{
			sprintf(szCommand,"cp -f %s /home/usr1/flash/xmlCfgFile.xml",pstFileDtls->szSrcFile);

			rv = local_svcSystem(szCommand);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to execute [%s] rv = [%d]", __FUNCTION__, szCommand, rv);
				APP_TRACE(szDbgMsg);
				debug_sprintf(szDbgMsg, "%s:Failed to copy parameter file!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				return rv;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s:File doesn't Exist!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			return rv;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s:Source file is not available; can't copy", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	return rv;
}

/*
 * ============================================================================
 * Function Name: getFileDtls
 *
 * Description	: This Function gets the File details.
 *
 * Input Params	: struct tmsFileDetails *,TMS_LIBTOAPP_DATA **
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getFileDtls(struct tmsFileDetails *pstFileDtls, TMS_LIBTOAPP_DATA **dpstVHQFunCallBckData)
{
	if((pstFileDtls != NULL) && ((*dpstVHQFunCallBckData)->ptrsttmsFileData != NULL))
	{
		memcpy(pstFileDtls, (*dpstVHQFunCallBckData)->ptrsttmsFileData, sizeof(struct tmsFileDetails));
		return SUCCESS;
	}
	else
	{
		return FAILURE;
	}
}

/*
 * ============================================================================
 * Function Name: processGetParams
 *
 * Description	: This Function process the Get-Parameter Msg.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int processGetParams(TMS_APPTOLIB_MSG *pstVHQMsgQSndData)
{
	int 			rv				= SUCCESS;

	//	char 			**dpszFileinfo	= NULL;
	//char 			*szParamFile	= strdup("/home/usr1/flash/xmlCfgFile.xml");

	pstVHQMsgQSndData->uAppData.stTMSParamList.pszFileinfo 		= NULL;
	pstVHQMsgQSndData->uAppData.stTMSParamList.iTotalFileCount  = 0;

	return rv;
}


/*
 * ============================================================================
 * Function Name: processInstallResult
 *
 * Description	: This Function process
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int processInstallResult(int iEventType)
{
	int 			rv				= SUCCESS;
	int				iUpdateStatus	= 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	iUpdateStatus = getVHQUpdateStatus();

	switch(iEventType)
	{
	case LTMS_MSGTYPE_CONTENTINST_ERROR_NOTIFY:
		debug_sprintf(szDbgMsg, "%s: Content Installation Failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(gbSCAInitDone)
		{
			showPSGenDispForm(MSG_INSTALL_FAILURE, FAILURE_ICON, 1000);
		}
		setVHQUpdateStatus(VHQ_UPDATE_FAILED);
		gbShowIdleScrn = PAAS_TRUE;
		break;

	case LTMS_MSGTYPE_CONTENTINST_SUCCESS_NOTIFY:
		debug_sprintf(szDbgMsg, "%s: Content/Media successfully installed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(gbSCAInitDone)
		{
			showPSGenDispForm(MSG_INSTALL_SUCCESS, SUCCESS_ICON, 1000);
		}
		setVHQUpdateStatus(VHQ_UPDATE_SUCCESS);
		gbShowIdleScrn = PAAS_TRUE;
		break;

	case LTMS_INSTALLSUCCESS_NOTIFY:
		debug_sprintf(szDbgMsg, "%s: Installation Successfull", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		// We will get this notification from agent for all three kind of downloads even though application already knows the status of parameter & content(library)
		// So, we should honor these result status only when application is currently processing any updates, otherwise will igonore these notifications
		if(iUpdateStatus == VHQ_UPDATE_PROCESSING)
		{
			if(gbSCAInitDone)
			{
				showPSGenDispForm(MSG_INSTALL_SUCCESS, SUCCESS_ICON, 1000);
			}
			setVHQUpdateStatus(VHQ_UPDATE_SUCCESS);
			gbShowIdleScrn = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Redundant notification from TMS agent for Content/Parameter", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case LTMS_INSTALLFAILED_NOTIFY:
		debug_sprintf(szDbgMsg, "%s: Installation Failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		// We will get this notification from agent for all three kind of downloads even though application already knows the status of parameter & content(library)
		// So, we should honor these result status only when application is currently processing any updates, otherwise will igonore these notifications
		if(iUpdateStatus == VHQ_UPDATE_PROCESSING)
		{
			if(gbSCAInitDone)
			{
				showPSGenDispForm(MSG_INSTALL_FAILURE, FAILURE_ICON, 1000);
			}
			setVHQUpdateStatus(VHQ_UPDATE_FAILED);
			gbShowIdleScrn = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Redundant notification from TMS agent for Content/Parameter", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case LTMS_INSTALLSUCCESS_REBOOT_NOTIFY:
		debug_sprintf(szDbgMsg, "%s: Device is going for Reboot...", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(gbSCAInitDone)
		{
			if(isPOSApprReqdForUpdate() && isSecondaryPortEnabled())
			{
				showPSGenDispForm(MSG_WAITING_BEFORE_REBOOT, STANDBY_ICON, 0);
			}
			else
			{
				showPSGenDispForm(MSG_REBOOTING_AFTER_UPDATE, STANDBY_ICON, 0);
			}
		}
		setVHQUpdateStatus(VHQ_UPDATE_SUCCESS_REBOOTING);
		break;

	case LTMS_INSTALLSUCCESS_APPRESTART_NOTIFY:
		debug_sprintf(szDbgMsg, "%s: Application is going for Restart...", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(gbSCAInitDone)
		{
			if(isPOSApprReqdForUpdate() && isSecondaryPortEnabled())
			{
				showPSGenDispForm(MSG_WAITING_BEFORE_RESTART, STANDBY_ICON, 0);
			}
			else
			{
				showPSGenDispForm(MSG_RESTARTING_AFTER_UPDATE, STANDBY_ICON, 0);
			}
		}
		setVHQUpdateStatus(VHQ_UPDATE_SUCCESS_RESTARTING_APP);
		break;

	default:
		break;
	}
	return rv;
}

#if 0
/*
 * ============================================================================
 * Function Name: setupFileDebug
 *
 * Description	: This Function setup the debug logs in a temp log file.
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
static void setupFileDebug(void)
{
	int fd = -1;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	fd = open("/home/usr1/flash/testAppLog.dat", O_RDWR|O_CREAT,0777);
	if(fd == -1)
	{
		debug_sprintf(szDbgMsg,"%s: Unable to open Log File ",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: File Created ",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(dup2(fd,1) != -1)
		{
			debug_sprintf(szDbgMsg,"%s: File Debug Set ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: File Debug Failure ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

}


/*
 * ============================================================================
 * Function Name: APP_TRACE
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void APP_TRACE(char * pszMsg)
{
	int				iPrefixLen		= 0;
	int				iDbgMsgSize		= 0;
	int				iMsgLen			= 0;
	pid_t			processID		= 0;
	pthread_t		threadID		= 0L;
	char			szTSPrefix[41]	= ""; /* MM/DD/YYYY HH:MM:SS:sss */
	char			szDbgMsg[15+4096+2+1+1] = "";
	char *			pszMsgPrefix	= "PS";
	struct tm *		tm_ptr			= NULL;
	struct timeb	the_time;


	/* Get the starting values */
	iDbgMsgSize = sizeof(szDbgMsg) - 1;
	iMsgLen = strlen(pszMsg);
	processID = getpid();
	threadID = pthread_self();

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
		/* Keep within 3 digits, since sprintf's width.precision specification
		 * does not truncate large values */
		the_time.millitm = 999;
	}

	sprintf(szTSPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d", tm_ptr->tm_mon+1,
				tm_ptr->tm_mday, 1900+tm_ptr->tm_year, tm_ptr->tm_hour,
				tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	iPrefixLen = sprintf(szDbgMsg, "%s %04d %04d %s: ", szTSPrefix, processID,
												(int) threadID, pszMsgPrefix);

	/* If addition of full datasize will exceed szDbgMsg */
	if((iPrefixLen + iMsgLen) >= (iDbgMsgSize - 1))
	{
		/* Chop iMsgLen down; save last 2 slots for '\n' and '\0' */
		iMsgLen = iDbgMsgSize - iPrefixLen - 2;
	}

	memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
	szDbgMsg[iPrefixLen + iMsgLen] = '\n';
	szDbgMsg[iPrefixLen + iMsgLen + 1] = 0;

	printf(szDbgMsg);


	return;
}
#endif

#if 0
/*
 * ============================================================================
 * Function Name: getAppInfo
 *
 * Description	: This API would get the Customer Specific details like (LaneID, StoreID).
 *
 * Input Params	: Array of char pointers to hold App details, int pointer for Total number of parameter
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int  getAppInfo(char **dpszAppInfo, int *piTotCnt)
{
	int 		rv			= SUCCESS;
	int 		iCnt		= 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < MAX_APP_INFO; iCnt++)
	{
		switch(iCnt)
		{
		case SCA_LANE_ID:
			/* Get Store ID */
			dpszAppInfo[iCnt] = getMerchantLaneId();
			if((dpszAppInfo[iCnt] != NULL) && (strlen(dpszAppInfo[iCnt]) > 0))
			{
				debug_sprintf(szDbgMsg, "%s: LaneID[%s]", __FUNCTION__, dpszAppInfo[iCnt]);
				APP_TRACE(szDbgMsg);
				(*piTotCnt)++;
			}
			break;
		case SCA_STORE_ID:
			/* Get LANE ID */
			dpszAppInfo[iCnt] = getMerchantStoreId();
			if((dpszAppInfo[iCnt] != NULL) && (strlen(dpszAppInfo[iCnt]) > 0))
			{
				debug_sprintf(szDbgMsg, "%s: StoreID[%s]", __FUNCTION__, dpszAppInfo[iCnt]);
				APP_TRACE(szDbgMsg);
				(*piTotCnt)++;
			}
			break;
		default:
			rv = FAILURE;
			break;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: getAppInfo
 *
 * Description	: This API would get the Customer Specific details like (LaneID, StoreID).
 *
 * Input Params	: Array of char pointers to hold App details, int pointer for Total number of parameter
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int  getAppInfo(int *piTotCnt)
{
	int 		rv			= SUCCESS;
	int 		iCnt		= 0;
	int			jCnt		= 0;
	char *		pszAppParam	= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(gpstAppParams, 0x00, sizeof(gpstAppParams));

	for(iCnt = 0; iCnt < MAX_APP_INFO; iCnt++)
	{
		switch(iCnt)
		{
		case SCA_LANE_ID:
			/* Get LANE ID */
			pszAppParam = getMerchantLaneId();
			if((pszAppParam != NULL) && (strlen(pszAppParam) > 0))
			{
				debug_sprintf(szDbgMsg, "%s: LaneID[%s]", __FUNCTION__, pszAppParam);
				APP_TRACE(szDbgMsg);
				strcpy(gpstAppParams[jCnt].szparameterName, "LaneID");
				strcpy(gpstAppParams[jCnt].szparameterValue, pszAppParam);
				gpstAppParams[jCnt].iparameterType = LTMS_PARAMETER_TYPE_IDENTIFIER;
				jCnt++;
			}
			pszAppParam = NULL;
			break;

		case SCA_STORE_ID:
			/* Get STORE ID */
			pszAppParam = getMerchantStoreId();
			if((pszAppParam != NULL) && (strlen(pszAppParam) > 0))
			{
				debug_sprintf(szDbgMsg, "%s: StoreID[%s]", __FUNCTION__, pszAppParam);
				APP_TRACE(szDbgMsg);
				strcpy(gpstAppParams[jCnt].szparameterName, "StoreID");
				strcpy(gpstAppParams[jCnt].szparameterValue, pszAppParam);
				gpstAppParams[jCnt].iparameterType = LTMS_PARAMETER_TYPE_IDENTIFIER;
				jCnt++;
			}
			pszAppParam = NULL;
			break;

			// T_RaghavendranR1 CID 67249 (#1 of 1): Dead default in switch (DEADCODE),
			/*default:
			rv = FAILURE;
			break;*/
		}
	}

	*piTotCnt = jCnt;

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

#if 0
/*
 * ============================================================================
 * Function Name: setAppInfoForVHQEvent
 *
 * Description	: This API would set the App details from array of pointers to array of structure of TMS_LIB
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int setAppInfoForVHQEvent(struct tms_AppParameter *pstAppParams, char **dpszAppInfo)
{
	int 		rv			= SUCCESS;
	int 		iCnt		= 0;
	int			jCnt		= 0;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( pstAppParams == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: NULL param passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			return rv;
		}

		for(iCnt = 0; iCnt < MAX_APP_INFO; iCnt++)
		{
			if(dpszAppInfo[iCnt] != NULL && strlen(dpszAppInfo[iCnt]) > 0)
			{
				switch(iCnt)
				{
				case SCA_LANE_ID:
					strcpy(pstAppParams[jCnt].parameterName, "LaneID");
					strcpy(pstAppParams[jCnt].parameterValue, dpszAppInfo[iCnt]);
					pstAppParams[jCnt].parameterType = LTMS_PARAMETER_TYPE_IDENTIFIER;
					jCnt++;
					break;

				case SCA_STORE_ID:
					strcpy(pstAppParams[jCnt].parameterName, "StoreID");
					strcpy(pstAppParams[jCnt].parameterValue, dpszAppInfo[iCnt]);
					pstAppParams[jCnt].parameterType = LTMS_PARAMETER_TYPE_IDENTIFIER;
					jCnt++;
					break;

				default:
					rv = FAILURE;
					break;
				}
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
#endif
	/* 1. No session should be there
	 * 2. Need to check SAF records
	 * 3. Check App state(ref. APPLN_STATE in common.h )
	 * 4. check SCA preamble(For downloading MID & TID)
	 * 5. check the Data-sytem*
	 * 6. home/usr1/flash/BET.DAT
	 * 7. home/usr1/flash/CDT.dat
	 * 8. home/usr1/flash/PCI_BIT.DAT-Bin range
	 * 9. RSAPubKey.DAT
	 * 10.SAFData.db
	 * 11.SAFCompletedRecords.db
	 * 12.Thumbs.db
	 * 13.VSD.INI
	 * 14.WL.DAT
	 * 15.appmsgs.ini
	 * 16.devKey.dat
	 * 17.nfc.ini
	 * 18.posAuth.dat
	 * 19.rfidtags.dat
	 * 20.stb.txt
	 *
	 */

/*
 * ============================================================================
 * Function Name: getApprovalForVHQEvent
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getApprovalForVHQEvent(int iEventType, int *piStateForVHQ, char *pszRsltDscrp, TMS_LIBTOAPP_DATA* pstVHQFunCallBckData)
{
	int 				rv 										= SUCCESS;
	int 				iStateApp	 							= SCA_APP_BUSY;
	int 				iStatePOS 								= SCA_POS_RESP_TIMEDOUT;
	int					iTimeOut								= DFLT_VHQ_EVENT_RESP_TIMEOUT;
	int					iAppLogEnabled							= isAppLogEnabled();
	char				szAppLogData[300]						= "";
	char				szRsltDscrp[LTMS_MAX_MSG_DESCRP + 1]	= "";
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(szRsltDscrp, 0x00, sizeof(szRsltDscrp));
		/* before Start-up of SCA-Application(till creating the SCA thread - sciEvtProcssr), VHQ Thread itself should be able to give approvals for
		 * downloading parameter, Installing software, Restart APP and Reboot Device and need not get any approval from Application and POS(except checking SAF records).
		 * After start-up of SCA, VHQ Thread must ask to Application & POS for each approval or any download.
		 */
		/* Exceptions at start up:
		 * 1. When Event type is parameter installation with MID/TID present in the incoming file, application checks for any SAF records present in device.
		 * 		If, SAF records are present at this stage, it won't allow the parameter installation to happen & postpone it till next window.
		 * 2. When Event type is Reboot/Restart approval, application first check for any critical operations is going on while start up like
		 * 		device registration, EMV initialization, VSP registration, dummy sale transactions. If, there is any critical opeation is going on in device
		 * 		it will postpone the reboot/restart approval till next window.
		 */

		if(gbSCAInitDone == PAAS_FALSE)
		{
			/* Need to check SAF records for parameter download */
			if((iEventType == LTMS_MSGTYPE_INSTALLFILE_FULL) && (isSAFRecordsPendingInDevice() == PAAS_TRUE))
			{
				//SAF Records are present; Need to look for the critical parameters that can affect the SAF Records.
				debug_sprintf(szDbgMsg, "%s: SAF Records are Present; Need to check parameters", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(szRsltDscrp, "SAF Records are Present");
				rv = processParamFileForVHQ(&iStateApp, pstVHQFunCallBckData);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s:Couldn't process the parameter file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					memset(szRsltDscrp, 0x00, LTMS_MAX_MSG_DESCRP);
					strcpy(szRsltDscrp, "XML File Error");
					rv = FAILURE;
				}
				*piStateForVHQ = iStateApp;
				break;
			}
			else if((iEventType == LTMS_MSGTYPE_GET_REBOOT_APPROVE) || (iEventType == LTMS_MSGTYPE_GET_APPRESTART_APPROVE))
			{
				if(isCriticalStepInProgress())
				{
					debug_sprintf(szDbgMsg, "%s: SCA is Performing Some Critical Step. Postponing The Restart/Reboot Approval till Next Window", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(szRsltDscrp, "SCA is Performing Some Critical Step");
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "SCA is Performing Some Critical Step. Postponing The Restart/Reboot Approval till Next Window.");
						addAppEventLog(SCA, PAAS_INFO, DEVICE_BUSY, szAppLogData, NULL);
					}
					*piStateForVHQ = SCA_APP_BUSY;
				}
				else
				{
					*piStateForVHQ = SCA_APP_FREE;
				}
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: SCA Initialization is pending; No need to get any approval from App & POS", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				*piStateForVHQ = SCA_APP_FREE;
				break;
			}
		}
		else	//	SCA Initialization is Done. Need to take App & POS Permissions.
		{
			rv = checkAppStatus(iEventType, &iStateApp, szRsltDscrp, pstVHQFunCallBckData);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s:Couldn't get the App status", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			if(iStateApp == SCA_APP_BUSY)
			{
				*piStateForVHQ = SCA_APP_BUSY;
				break;
			}
			else
			{
				/* Handling Reboot/Restart events seperatly here */
				if((iEventType == LTMS_MSGTYPE_GET_REBOOT_APPROVE) || (iEventType == LTMS_MSGTYPE_GET_APPRESTART_APPROVE))
				{
					// If it is a POST install action, application has to wait for POS to query for update status before going for restart/reboot.
					// otherwise, if it is a standalone reboot/restart, application need not require to wait for anything.
					/* POST Install Action */
					//if((getVHQUpdateStatus() == VHQ_UPDATE_SUCCESS_REBOOTING) || (getVHQUpdateStatus() == VHQ_UPDATE_SUCCESS_RESTARTING_APP))
					if(getAppState() == PROCESSING_VHQ_UPDATES)
					{
						if(isPOSApprReqdForUpdate() && isSecondaryPortEnabled())
						{
							iTimeOut = getVHQEventRespTimeout() - 2;	// adding 2 secs offset to have enough time to send the response back to the library
							// this is non-blocked call, which will wait until POS queries for UPDATE STATUS on secondary port or Event TimeOut occurs.
							iStatePOS = waitForPOStoQueryUpdateStatus(iTimeOut);
							if(iStatePOS == SCA_POS_RESP_TIMEDOUT)
							{
								*piStateForVHQ = SCA_APP_BUSY;
								break;
							}
							else if(iStatePOS == SCA_POS_NOTIFY_SUCCESS)
							{
								*piStateForVHQ = SCA_APP_FREE;
								if(iEventType == LTMS_MSGTYPE_GET_REBOOT_APPROVE)
								{
									showPSGenDispForm(MSG_REBOOTING_AFTER_UPDATE, STANDBY_ICON, 1000);
								}
								else
								{
									showPSGenDispForm(MSG_RESTARTING_AFTER_UPDATE, STANDBY_ICON, 1000);
								}
								break;
							}
						}
						else
						{
							// diplay message(for update status) has already been showed on screen
							*piStateForVHQ = SCA_APP_FREE;
							break;
						}
					}
					else	/* stand-alone reboot/restart. */
					{
						*piStateForVHQ = SCA_APP_FREE;
						setVHQUpdateInProgressFlag(PAAS_TRUE);
						if(iEventType == LTMS_MSGTYPE_GET_REBOOT_APPROVE)
						{
							showPSGenDispForm(MSG_REBOOT, STANDBY_ICON, 1000);
						}
						else
						{
							showPSGenDispForm(MSG_APP_RESTART, STANDBY_ICON, 1000);
						}
						break;
					}
				}
				if( ! isPOSApprReqdForUpdate() )
				{
					debug_sprintf(szDbgMsg, "%s: POS Approval is not required", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iEventType != LTMS_MSGTYPE_GET_APP_STATE && 	// For these event we will not get any update status back from TMS library/agent
						iEventType != LTMS_MSGTYPE_GET_CLOCKUPDATE_APPROVE )
					{
						setVHQUpdateStatus(VHQ_UPDATE_PROCESSING);

						setAppState(PROCESSING_VHQ_UPDATES);

						setVHQUpdateInProgressFlag(PAAS_TRUE);

						/* Showing processing update screen */
						showPSGenDispForm(MSG_PROCESSING_VHQ_UPDATES, STANDBY_ICON, 0);
					}

					*piStateForVHQ = SCA_APP_FREE;
					break;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Waiting for POS Approval...", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					/* This API is used to get approval from POS for Applying any Installation. */
					rv = waitForPOSApproval(iEventType, &iStatePOS, szRsltDscrp);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s:Couldn't get the POS status", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						memset(szRsltDscrp, 0x00, sizeof(szRsltDscrp));
						strcpy(szRsltDscrp, "Application Error");
						rv = FAILURE;
						break;
					}

					if(iStatePOS == SCA_POS_NOTIFY_SUCCESS)	//Not in use Now
					{
						*piStateForVHQ = SCA_APP_FREE;
						debug_sprintf(szDbgMsg, "%s: Notification Succesfully Sent to POS", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					else if(iStatePOS == SCA_POS_APPROVED)
					{
						*piStateForVHQ = SCA_APP_FREE;
						debug_sprintf(szDbgMsg, "%s: POS Approved VHQ Updates", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					else if(iStatePOS == SCA_POS_REJECT)
					{
						debug_sprintf(szDbgMsg, "%s: POS Declined VHQ Updates", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						*piStateForVHQ = SCA_APP_BUSY;
						break;
					}
					else if(iStatePOS == SCA_POS_RESP_TIMEDOUT)
					{
						debug_sprintf(szDbgMsg, "%s: POS Approval Got Time Out; Setting Busy Status", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						memset(szRsltDscrp, 0x00, sizeof(szRsltDscrp));
						strcpy(szRsltDscrp, "Event Timeout! Please Apply Updates");
						*piStateForVHQ = SCA_APP_BUSY;
						break;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Undefined POS State", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						*piStateForVHQ = SCA_APP_BUSY;
						break;
					}
				}
			}
		}
		break;
	}	//end of while loop

	strcpy(pszRsltDscrp, szRsltDscrp);
	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: checkAppStatus
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int checkAppStatus(int iEventType, int *piStateApp, char* pszRsltDscrp, TMS_LIBTOAPP_DATA* pstVHQFunCallBckData)
{
	int 		rv					= SUCCESS;
	int			iCurAppState		= SCA_APP_BUSY;
	int			iIsSAFRecsPres		= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((pszRsltDscrp == NULL) || (piStateApp == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL Params Paased", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* get the App state */
		iCurAppState = getAppState();
		if(iCurAppState != NO_SESSION_IDLE && iCurAppState != LANE_CLOSED && iCurAppState  != PROCESSING_VHQ_UPDATES)	// if Application is Busy in some other state.
		{
			debug_sprintf(szDbgMsg, "%s:Application is Busy in a Transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			*piStateApp = SCA_APP_BUSY;
			strcpy(pszRsltDscrp, "Transaction is going on");
			break;
		}
		else	// Application is Free here; not doing any transaction
		{
			switch(iEventType)
			{
			case LTMS_MSGTYPE_GET_APPRESTART_APPROVE:
			case LTMS_MSGTYPE_GET_REBOOT_APPROVE:

				if(isCriticalStepInProgress() == PAAS_TRUE)
				{
					*piStateApp = SCA_APP_BUSY;
					strcpy(pszRsltDscrp, "Application is Posting SAF Records");
				}
				else
				{
					*piStateApp = SCA_APP_FREE;
					strcpy(pszRsltDscrp, "REBOOT/RESTART Approved");
				}
				break;

			case LTMS_MSGTYPE_GET_INSTALL_APPROVE:
				*piStateApp = SCA_APP_FREE;
				strcpy(pszRsltDscrp, "Install Approved");
				break;

			case LTMS_MSGTYPE_GET_APP_STATE:
				*piStateApp = SCA_APP_FREE;
				strcpy(pszRsltDscrp, "Approved");
				break;
			case LTMS_MSGTYPE_GET_CLOCKUPDATE_APPROVE:
				*piStateApp = SCA_APP_FREE;
				strcpy(pszRsltDscrp, "Set Clock Approved");
				break;

			case LTMS_MSGTYPE_INSTALLFILE_FULL:

				/* Need to Check SAF records */
				iIsSAFRecsPres = isSAFRecordsPendingInDevice();
				if(iIsSAFRecsPres == PAAS_TRUE)	//SAF Records are present; Need to look for the critical parameters that can affect the SAF Records.
				{
					debug_sprintf(szDbgMsg, "%s: SAF Records are Present; Need to check parameters", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(pszRsltDscrp, "SAF Records are Present");
					rv = processParamFileForVHQ(piStateApp, pstVHQFunCallBckData);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s:Couldn't process the parameter file", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						memset(pszRsltDscrp, 0x00, LTMS_MAX_MSG_DESCRP);
						strcpy(pszRsltDscrp, "XML File Error");
						rv = FAILURE;
					}
					break;
				}
				else
				{
					*piStateApp = SCA_APP_FREE;
					strcpy(pszRsltDscrp, "Install Approved");
					break;
				}
				break;

			default:
				rv = FAILURE;
				break;
			}
		}
		break;
	}	//end of while loop

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: waitForPOSApproval
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int waitForPOSApproval(int iEventType, int *piStatePOS, char *pszRsltDscrp)
{
	int						rv										= SUCCESS;
	int						iStatePOS								= SCA_POS_RESP_TIMEDOUT;
	int						iTimeOut								= DFLT_VHQ_EVENT_RESP_TIMEOUT;
	int						iAppLogEnabled							= isAppLogEnabled();
	ullong					endTime									= 0L;
	ullong					curTime									= 0L;
	char					szAppLogData[300]						= "";
	char					szRsltDscrp[LTMS_MAX_MSG_DESCRP + 1]	= "";
	POSRESPDATA_VHQ_STYPE	stPOSRespDataForVHQ;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		if((piStatePOS == NULL) || (pszRsltDscrp == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL Params Paased", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			return rv;
		}

		// setting the update status as 'Update Available'
		if(getVHQUpdateStatus() != VHQ_UPDATE_PROCESSING)
		{
			/* make the updates available for bLogic thread */
			setVHQUpdtAvailableFlag(PAAS_TRUE);
			// This update available Flag will be reset by Blogic thread as soon as it Post the POS response to MsgQ successfully.

			// setting the update status here as 'Update Available'
			setVHQUpdateStatus(VHQ_UPDATE_AVAILABLE);
		}

		switch(iEventType)
		{
		// Reboot & Restart Events are not being used here as of now.
#if 0
		case LTMS_MSGTYPE_GET_REBOOT_APPROVE:
			debug_sprintf(szDbgMsg, "%s: Rebooting the Device", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* Setting the update status here as 'Update Success Rebooting' */
			setVHQUpdateStatus(VHQ_UPDATE_SUCCESS_REBOOTING);
			iStatePOS = SCA_POS_APPROVED;		//TODO: change response here.
			break;

		case LTMS_MSGTYPE_GET_APPRESTART_APPROVE:
			debug_sprintf(szDbgMsg, "%s: Restarting the Application", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Setting the update status here as 'Update Success Restarting the Application' */
			setVHQUpdateStatus(VHQ_UPDATE_SUCCESS_RESTARTING_APP);
			iStatePOS = SCA_POS_APPROVED;		//TODO: change response here.
			break;
#endif
		case LTMS_MSGTYPE_GET_INSTALL_APPROVE:
		case LTMS_MSGTYPE_INSTALLFILE_FULL:
		case LTMS_MSGTYPE_GET_APP_STATE:
		case LTMS_MSGTYPE_GET_CLOCKUPDATE_APPROVE:
			/* Set the timeout value for getting the POS reponse; here we are adding the offset of 2 Secs, to provide sufficient time
			 * for application to get response from POS and send it back to TMS app interface library.
			 * Note: TMS Lib will get Time Out in 5 Secs less than the agent time out value and application will get
			 * time out 2 Secs less than the TMS library time out value.
			 */
			iTimeOut = getVHQEventRespTimeout() - 2;	// adding 2 secs offset to have enoug time to send the response back to the library

			debug_sprintf(szDbgMsg, "%s: Waiting for POS to apply the pending updates...", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Waiting For POS To Apply The Pending Updates Through Primary Port");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			curTime = svcGetSysMillisec();
			endTime = curTime + (iTimeOut * 1000L);

			/* waiting for POS response here; data will be added here by bLogic thread, once POS sends APPLYUPDATES command
			 * or else explicitly come out of this loop once the Event Timeout Occurs.
			 */
			while(endTime > curTime)
			{
				memset(&stPOSRespDataForVHQ, 0x00, sizeof(POSRESPDATA_VHQ_STYPE));

				//Read data from the message queue added by bLogic Thread for Applying the updates
				if( msgrcv(gVHQRcvFrmPOSMsgQId, &stPOSRespDataForVHQ, sizeof(POSRESPDATA_VHQ_STYPE), 0, IPC_NOWAIT) == -1) // IPC system call
				{
					//TODO: need to remove logs later
					//debug_sprintf(szDbgMsg, "%s: msgrcv(): %s", __FUNCTION__, strerror(errno));
					//APP_TRACE(szDbgMsg);

					svcWait(1000);
					curTime = svcGetSysMillisec();
					continue;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Data Received from BLogic ", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(stPOSRespDataForVHQ.iRespType == SCA_POS_APPROVED)
					{
						iStatePOS = SCA_POS_APPROVED;
						/* make the update unavailable, once it is approved for all downloads(manual or automatic) */
						setVHQUpdtAvailableFlag(PAAS_FALSE);

						/*  Now we are setting the update status as 'Update successful' for all manual downloads(Content or Software)
						 * 		 For Parameter download, we would set SUCCESS status, once it successfully get installed.
						 * 		 TODO:Need to change for content and software download also if possible.
						 */
/*						if(iEventType != LTMS_MSGTYPE_INSTALLFILE)		//28-Dec-15: EMS-393 changes
						{
							//setVHQUpdateStatus(VHQ_UPDATE_SUCCESS);	//28-Dec-15: Keeping the Update status in Processing state for Content/Software
							//update, untill application gets any notification from TMS library about result of Content/Software : EMS-393

							//setVHQUpdateInProgressFlag(PAAS_FALSE); // 11-Dec-15: Putting at last after processing each VHQ event.

							//gbShowIdleScrn = PAAS_TRUE;				//28-Dec-15: Keeping the same screen.
						}
*/
						//22-Feb-16: Handling Get App State here, because no update status will come from agent for this event.
						if(iEventType == LTMS_MSGTYPE_GET_APP_STATE || iEventType == LTMS_MSGTYPE_GET_CLOCKUPDATE_APPROVE)
						{
							setVHQUpdateStatus(VHQ_UPDATE_SUCCESS);
							setVHQUpdateInProgressFlag(PAAS_FALSE);
							gbShowIdleScrn = PAAS_TRUE;
						}
					}
					else if(stPOSRespDataForVHQ.iRespType == SCA_POS_REJECT)
					{
						iStatePOS = SCA_POS_REJECT;
						// make the update unavailable here.
						setVHQUpdtAvailableFlag(PAAS_FALSE);
						// setting the update status as 'No Updates'
						setVHQUpdateStatus(VHQ_NO_UPDATES);
					}
					else
					{
						continue;
						//FIXME: doing continue, because same MsgQ is being used for both UPDATE_STAUS(secondary) & APPLYUPDATES(primary) command.
						// Anyway both the msgs cann't come together in this MsgQ at a time.
					}

					break;
				}
			}

			break;

		default:
			rv = FAILURE;
			break;
		}

		// Make the 'update available Flag' true here if POS response got TimeOut - For Content & Software download.
		if((iStatePOS == SCA_POS_RESP_TIMEDOUT) && (getVHQUpdateStatus() != VHQ_UPDATE_PROCESSING) && (iEventType == LTMS_MSGTYPE_GET_INSTALL_APPROVE ||
																									   iEventType == LTMS_MSGTYPE_GET_APP_STATE		  ||
																									   iEventType == LTMS_MSGTYPE_GET_CLOCKUPDATE_APPROVE ))
		{
			// make the update available again.
			setVHQUpdtAvailableFlag(PAAS_TRUE);
			// setting the update status as 'Update Available'
			setVHQUpdateStatus(VHQ_UPDATE_AVAILABLE);
		}
		// Make the 'update available Flag' false here if POS response got TimeOut - For Parameter download.
		else if((iStatePOS == SCA_POS_RESP_TIMEDOUT) && (getVHQUpdateStatus() != VHQ_UPDATE_PROCESSING) && (iEventType == LTMS_MSGTYPE_INSTALLFILE_FULL))
		{
			// make the update available again.
			setVHQUpdtAvailableFlag(PAAS_FALSE);
			// setting the update status as 'Update Available'
			setVHQUpdateStatus(VHQ_NO_UPDATES);
		}

		*piStatePOS = iStatePOS;
		strcpy(pszRsltDscrp, szRsltDscrp);
		break;
	}	//end of while loop

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

#if 0
/*
 * ============================================================================
 * Function Name: sendDataOnScndryPort
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int sendDataOnScndryPort(int *piStatePOS, char *pszRsltDscrp)
{
	int					rv					= SUCCESS;
	int					iStatePOS 			= SCA_POS_REJECT;
	int					iTimeOut			= VHQ_EVENT_TIMEOUT - 1;
	ullong				endTime				= 0L;
	ullong				curTime				= 0L;
	POSRESPDATA_VHQ_STYPE	stPOSRespDataForVHQ;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* make the updates available for bLogic thread */
	gbVHQUpdateAvailable = PAAS_TRUE;


	curTime = svcGetSysMillisec();
	endTime = curTime + (iTimeOut * 1000L);

	/* waiting for POS response here; data will be added here by bLogic thread, once POS sends APPLYUPDATES command
	 * or else explicitly come out of this loop once the Event Timeout Occurs.
	 */
	while(endTime > curTime)
	{
		memset(&stPOSRespDataForVHQ, 0x00, sizeof(POSRESPDATA_VHQ_STYPE));

		//Read data from the message queue added by bLogic Thread for Applying the updates
		if( msgrcv(gVHQRcvFrmPOSMsgQId, &stPOSRespDataForVHQ, sizeof(POSRESPDATA_VHQ_STYPE), 0, IPC_NOWAIT) == -1) // IPC system call
		{
			iStatePOS = SCA_APP_BUSY;
			debug_sprintf(szDbgMsg, "%s: msgrcv(): %s", __FUNCTION__, strerror(errno));
			APP_TRACE(szDbgMsg);

			svcWait(1000);
			curTime = svcGetSysMillisec();
			continue;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data Recieved from BLogic ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//TODO: Check Msg Text for approve or Reject
			/* Need to Reset the Update Status to unavailable, once it is taken */
			setVHQUpdtAvailableFlag(PAAS_FALSE);
			iStatePOS = SCA_POS_APPROVED;
			break;
		}
	}

	*piStatePOS = iStatePOS;

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

#endif
/*
 * ============================================================================
 * Function Name: isVHQUpdateAvailable
 *
 * Description	: Checks if any new Updates are available.
 *
 * Input Params	: none
 *
 * Output Params: TRUE / FALSE
 * ============================================================================
 */
PAAS_BOOL isVHQUpdateAvailable(void)
{
	PAAS_BOOL		bVHQUpdtAvailableFlag;

	acquireMutexLock(&gptAllowUpdtAvailableMutex, "Getting the VHQ Update Available Flag");

	bVHQUpdtAvailableFlag =  gbVHQUpdateAvailable;

	releaseMutexLock(&gptAllowUpdtAvailableMutex, "Getting the VHQ Update Available Flag");

	return bVHQUpdtAvailableFlag;
}

/*
 * ============================================================================
 * Function Name: setVHQUpdtAvailableFlag
 *
 * Description	:
 *
 * Input Params	: Update Status either  PAAS_TRUE  - update still available
 * 									 	PAAS_FALSE - update unavailable/applied
 *
 * Output Params: None
 * ============================================================================
 */
void setVHQUpdtAvailableFlag(PAAS_BOOL bUpdtStatus)
{
	acquireMutexLock(&gptAllowUpdtAvailableMutex, "Setting the VHQ Update Available Flag");

	gbVHQUpdateAvailable = bUpdtStatus;

	releaseMutexLock(&gptAllowUpdtAvailableMutex, "Setting the VHQ Update Available Flag");

	return ;
}

/*
 * ============================================================================
 * Function Name: getVHQUpdateStatus
 *
 * Description	: Checks the Status of current Update.
 *
 * Input Params	: none
 *
 * Output Params:	0="NO_UPDATES"
 * 					1="UPDATE_PROCESSING"
 * 					2=�UPDATE SUCEESS�
 *					3=�UPDATE FAILED�
 *					4=�UPDATE SUCCESS REBOOTING��
 *					5=�UPDATE SUCCESS RESTARTING APPLICATION��
 * ============================================================================
 */
int getVHQUpdateStatus()
{
	int		iCurrUpdtStatus;

	acquireMutexLock(&gptAllowUpdtStatusMutex, "Getting the VHQ Update Available Flag");

	iCurrUpdtStatus = giVHQUpdateStatus;

	releaseMutexLock(&gptAllowUpdtStatusMutex, "Getting the VHQ Update Available Flag");

	return iCurrUpdtStatus;
}

/*
 * ============================================================================
 * Function Name: setVHQUpdateStatus
 *
 * Description	: set the current Update status.
 *
 * Input Params	: integer
 *
 * Output Params: None
 * ============================================================================
 */
void setVHQUpdateStatus(int iCurrUpdtStatus)
{
	acquireMutexLock(&gptAllowUpdtStatusMutex, "Setting the VHQ Update Available Flag");

	giVHQUpdateStatus = iCurrUpdtStatus;

	releaseMutexLock(&gptAllowUpdtStatusMutex, "Setting the VHQ Update Available Flag");

	return ;
}

/*
 * ============================================================================
 * Function Name: setVHQUpdateInProgressFlag
 *
 * Description	: set the current Update Done Flag.
 *
 * Input Params	: BOOLEAN
 *
 * Output Params: None
 * ============================================================================
 */
void setVHQUpdateInProgressFlag(PAAS_BOOL bCurrUpdtProgressFlag)
{
	acquireMutexLock(&gptAllowUpdtDoneMutex, "Setting the VHQ Update In Progress Flag");

	gbVHQUpdateInProgress = bCurrUpdtProgressFlag;

	releaseMutexLock(&gptAllowUpdtDoneMutex, "Setting the VHQ Update in Progress Flag");

	return ;
}

/*
 * ============================================================================
 * Function Name: getVHQUpdateInProgressFlag
 *
 * Description	: set the current Update Done Flag.
 *
 * Input Params	: BOOLEAN
 *
 * Output Params: None
 * ============================================================================
 */
PAAS_BOOL getVHQUpdateInProgressFlag(void)
{
	PAAS_BOOL		bCurrUpdtProgressFlag;

	acquireMutexLock(&gptAllowUpdtDoneMutex, "Getting the VHQ Update In Progress Flag");

	bCurrUpdtProgressFlag = gbVHQUpdateInProgress;

	releaseMutexLock(&gptAllowUpdtDoneMutex, "Getting the VHQ Update In Progress Flag");

	return bCurrUpdtProgressFlag;
}

/*
 * ============================================================================
 * Function Name: setSAFRecordsInProgressFlag
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: None
 * ============================================================================
 */
void setCriticalStepInProgressFlag(PAAS_BOOL bCriticalStepInPrgsFlag)
{
	acquireMutexLock(&gptCriticalStepInProgressMutex, "Setting the Critical Step Progress Flag");

	gbCriticalStepInProgress = bCriticalStepInPrgsFlag;

	releaseMutexLock(&gptCriticalStepInProgressMutex, "Setting the Critical Step Progress Flag");

	return ;
}

/*
 * ============================================================================
 * Function Name: isSAFRecordsInProgress
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: TRUE / FALSE
 * ============================================================================
 */
static PAAS_BOOL isCriticalStepInProgress(void)
{
	PAAS_BOOL	bCriticalStepInPrgsFlag;

	acquireMutexLock(&gptCriticalStepInProgressMutex, "Getting the Critical Step Progress Flag");

	bCriticalStepInPrgsFlag = gbCriticalStepInProgress;

	releaseMutexLock(&gptCriticalStepInProgressMutex, "Getting the Critical Step Progress Flag");

	return bCriticalStepInPrgsFlag;
}

/*
 * ============================================================================
 * Function Name: loadVHQConfigParams
 *
 * Description	: Load configurations parameter related to VHQ
 *
 * Input Params	: none
 *
 * Output Params: TRUE / FALSE
 * ============================================================================
 */
static int loadVHQConfigParams()
{
	int					rv				= SUCCESS;
	int					iLen			= 0;
	int					iTemp			= 0;
	//char				szTemp[30]		= "";	// CID-67379: 25-Jan-16: MukeshS3:
	char				szTemp[50]		= "";	// increasing size to 50 bytes to hold the long paramters name present in VHQ.ini file
	dictionary *		dict			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szTemp);
	memset(&vhqSettings, 0x00, sizeof(VHQ_CFG_STYPE));

	/* ----- Check for TMS/VHQ Settings -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, VHQ_SUP_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Bool value accordingly */
		if( (*szTemp == 'y') || (*szTemp == 'Y') )
		{
			vhqSettings.bVHQSupEnabled = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: VHQ Support ENABLED", __FUNCTION__);
		}
		else
		{
			vhqSettings.bVHQSupEnabled = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: VHQ Support DISABLED", __FUNCTION__);
		}
		rv = SUCCESS;
	}
	else
	{
		/* Value not found.. setting default */
		vhqSettings.bVHQSupEnabled = PAAS_FALSE;
		debug_sprintf(szDbgMsg, "%s: VHQ Support DISABLED by default",__FUNCTION__);
	}
	APP_TRACE(szDbgMsg);

	/* ----- Check SCA package name configured in PERM section  --------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_PERM, SCA_PACKAGE_NAME, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ...*/
		if(strlen(szTemp))
		{
			strncpy(vhqSettings.szSCAPkgName, szTemp, LTMS_MAX_APP_NAME_LEN);
			debug_sprintf(szDbgMsg, "%s: Setting the SCA Package Name as [%s]", __FUNCTION__, vhqSettings.szSCAPkgName);
		}
		else
		{
			strcpy(vhqSettings.szSCAPkgName, "pointMx_SCA-App");
			debug_sprintf(szDbgMsg, "%s: Invalid Value; Setting the SCA Package Name as [%s]", __FUNCTION__, vhqSettings.szSCAPkgName);
		}
		rv = SUCCESS;
	}
	else
	{
		/* Value not found.. setting default */
		strcpy(vhqSettings.szSCAPkgName, "pointMx_SCA-App");
		debug_sprintf(szDbgMsg, "%s: Setting the Default SCA Package Name as [%s]", __FUNCTION__, vhqSettings.szSCAPkgName);
	}
	APP_TRACE(szDbgMsg);
	if(isVHQSupEnabled())
	{
		/* ----- Check Settings for POS Approval required for VHQ  --------- */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DEVICE, POS_APRV_REQD_FOR_UPDATE, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				vhqSettings.bPOSAprvReqdForUpdate = PAAS_TRUE;
				debug_sprintf(szDbgMsg, "%s: POS Approval Required for Updates", __FUNCTION__);
			}
			else
			{
				vhqSettings.bPOSAprvReqdForUpdate = PAAS_FALSE;
				debug_sprintf(szDbgMsg, "%s: POS Approval Not Required", __FUNCTION__);
			}
			rv = SUCCESS;
		}
		else
		{
			/* Value not found.. setting default */
			vhqSettings.bPOSAprvReqdForUpdate = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: POS Approval Not Required by default",__FUNCTION__);
		}
		APP_TRACE(szDbgMsg);

		// Mukesh: 17-June-16
		/* ----- Check Settings for POS Notification Timeout for VHQ  --------- */

		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DEVICE, POS_UPDATE_NOTIFY_TIMEOUT, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Int value accordingly */
			iTemp = atoi(szTemp);
			if((iLen = strlen(szTemp)) && (strspn(szTemp,"0123456789") == iLen) && (iTemp >= MIN_POS_UPDATE_NOTIFY_TIMEOUT) && (iTemp <= MAX_POS_UPDATE_NOTIFY_TIMEOUT))
			{
				vhqSettings.iPOSUpdateNotifyTimeOut = iTemp;
				debug_sprintf(szDbgMsg, "%s: Setting the pos update notify time out as [%d Minutes]", __FUNCTION__, iTemp);
			}
			else
			{
				vhqSettings.iPOSUpdateNotifyTimeOut = DFLT_POS_UPDATE_NOTIFY_TIMEOUT;
				debug_sprintf(szDbgMsg, "%s: Setting the event response time out as [%d Minutes]", __FUNCTION__, DFLT_POS_UPDATE_NOTIFY_TIMEOUT);
			}
			rv = SUCCESS;
		}
		else
		{
			/* Value not found.. setting default */
			vhqSettings.iPOSUpdateNotifyTimeOut = DFLT_POS_UPDATE_NOTIFY_TIMEOUT;
			debug_sprintf(szDbgMsg, "%s: Setting the event response time out as [%d Seconds]", __FUNCTION__, DFLT_POS_UPDATE_NOTIFY_TIMEOUT);
		}
		APP_TRACE(szDbgMsg);

		/* ----- Need to get 'Event Response Timeout' parameter value from VHQ.ini  --------- */

		memset(szTemp, 0x00, iLen);
		/* Check if the VHQ.ini file exits and has some data */
		while(1)
		{
			rv = doesFileExist(VHQ_CONFIG_FILE_NAME);
			if(rv == SUCCESS)
			{
				/* Load the ini file in the parser library */
				dict = iniparser_load(VHQ_CONFIG_FILE_NAME);
				if(dict == NULL)
				{
					/* Unable to load the ini file */
					debug_sprintf(szDbgMsg, "%s: Unable to parse ini file [%s], errno [%d]",
															__FUNCTION__, VHQ_CONFIG_FILE_NAME, errno);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				else
				{
					/* Read and store the event reponse timeout value */
					sprintf(szTemp, "vhq:appifc event response timeout");
					iTemp = iniparser_getint(dict, szTemp, -1);
					if(iTemp == -1)
					{
						debug_sprintf(szDbgMsg, "%s: Missing parameter[%s]",
																	__FUNCTION__, szTemp);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;	// Will take default value
						break;
					}
					else if((iTemp > MAX_VHQ_EVENT_RESP_TIMEOUT) || ((iTemp < MIN_VHQ_EVENT_RESP_TIMEOUT)))
					{
						debug_sprintf(szDbgMsg, "%s: [%s] parameter value is out of range", __FUNCTION__, szTemp);
						APP_TRACE(szDbgMsg);
						rv = FAILURE;	// Will take default value
						break;
					}
					else if((iTemp - MIN_VHQ_EVENT_RESP_TIMEOUT) <= 5)
					{
						debug_sprintf(szDbgMsg, "%s: Setting the event response time out as [%d Seconds]", __FUNCTION__, iTemp);
						APP_TRACE(szDbgMsg);
						vhqSettings.iEventRespTimeOut = iTemp;
					}
					else
					{// Adding offset of 5 secs to provide sufficient time for TMS library(app interface lib)
						// to send response back to agent.
						debug_sprintf(szDbgMsg, "%s: Setting the event response time out as [%d Seconds]", __FUNCTION__, iTemp-5);
						APP_TRACE(szDbgMsg);
						vhqSettings.iEventRespTimeOut = iTemp - 5;
					}
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: File [%s] does not exist", __FUNCTION__,
						VHQ_CONFIG_FILE_NAME);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			break;	//breaking from while loop
		}
		/* Set the default value, if not present in VHQ.ini config file */
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Setting the default TimeOut[%d]", __FUNCTION__, DFLT_VHQ_EVENT_RESP_TIMEOUT);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS;
			vhqSettings.iEventRespTimeOut = DFLT_VHQ_EVENT_RESP_TIMEOUT;
		}
		/* Free the dictionary */
		if(dict != NULL)
		{
			iniparser_freedict(dict);
		}

	}

		/*KranthiK1: Not Required for Now*/
#if 0
		if(isPOSApprReqdForUpdate())
		{
			/* ----- Check Settings for VHQ/TMS Response type to POS -------------- */
			memset(szTemp, 0x00, iLen);
			rv = getEnvFile(SECTION_DEVICE, TMS_POS_RESP_TYPE, szTemp, iLen);
			if(rv > 0)
			{
				/* Value found ... Setting Bool value accordingly */
				if( ! strcasecmp(szTemp, POS_SECONDARY_PORT) )
				{
					strcpy(vhqSettings.szTMStoPOSRespType, POS_SECONDARY_PORT);
					debug_sprintf(szDbgMsg, "%s: TMS-POS Response Type is Secondary Port", __FUNCTION__);
				}
				else if( ! strcasecmp(szTemp, "UNSOLICITED") )
				{
					strcpy(vhqSettings.szTMStoPOSRespType, POS_UNSOLICITED);
					debug_sprintf(szDbgMsg, "%s: TMS-POS Response Type is Unsolicited", __FUNCTION__);
				}
				else
				{
					strcpy(vhqSettings.szTMStoPOSRespType, POS_SECONDARY_PORT);
					debug_sprintf(szDbgMsg, "%s: TMS-POS Response Type is by default Secondary Port", __FUNCTION__);
				}
				rv = SUCCESS;
			}
			else
			{
				/* Value not found.. setting default */
				strcpy(vhqSettings.szTMStoPOSRespType, POS_SECONDARY_PORT);
				debug_sprintf(szDbgMsg, "%s: TMS-POS Response Type is by default Secondary Port", __FUNCTION__);
			}
			APP_TRACE(szDbgMsg);
		}
#endif


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: isVHQSupEnabled
 *
 * Description	: Returns TRUE if TMS is Enabled else FALSE.
 *
 * Input Params	: none
 *
 * Output Params: TRUE / FALSE
 * ============================================================================
 */
PAAS_BOOL isVHQSupEnabled()
{
	return vhqSettings.bVHQSupEnabled;
}

/*
 * ============================================================================
 * Function Name: isPOSApprReqdForUpdate
 *
 * Description	: Returns TRUE if POS Approve is Required else FALSE.
 *
 * Input Params	: none
 *
 * Output Params: TRUE / FALSE
 * ============================================================================
 */
PAAS_BOOL isPOSApprReqdForUpdate()
{
	return vhqSettings.bPOSAprvReqdForUpdate;
}

/*
 * ============================================================================
 * Function Name: getVHQEventRespTimeout
 *
 * Description	: Returns Event Reponse Timeout value
 *
 * Input Params	: none
 *
 * Output Params: Timeout
 * ============================================================================
 */
int getVHQEventRespTimeout(void)
{
	return vhqSettings.iEventRespTimeOut;
}
#if 0
/*
 * ============================================================================
 * Function Name: sendUnsolicitedMsg
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: TRUE / FALSE
 * ============================================================================
 */
static int sendUnsolicitedMsg(int iEventType, char *pszRsltDscrp)
{
	return SUCCESS;
}
#endif

/*
 * ============================================================================
 * Function Name: getVHQRcvMsgQIdForPOS
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: TRUE / FALSE
 * ============================================================================
 */
int getVHQRcvFrmPOSMsgQId(void)
{
	return gVHQRcvFrmPOSMsgQId;
}

/*
 * ============================================================================
 * Function Name: processParamFileForVHQ
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: TRUE / FALSE
 * ============================================================================
 */
static int processParamFileForVHQ(int *piStateApp, TMS_LIBTOAPP_DATA* pstVHQFunCallBckData)
{
	int 					rv					= SUCCESS;
	int						iStateApp			= SCA_APP_BUSY;
	int						iAppLogEnabled		= isAppLogEnabled();
	char					szAppLogData[300]	= "";
	struct tmsFileDetails 	stFileDtls;
	GENKEYVAL_PTYPE			paramValLstPtr		= NULL;
	GENKEYVAL_PTYPE			tmpNodePtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		memset(&stFileDtls, 0x00, sizeof(stFileDtls));
		rv = getFileDtls(&stFileDtls, &pstVHQFunCallBckData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Could not get the File details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		/* Get the param values list from the xml file */
		rv = parseXMLFileForConfigData(stFileDtls.szSrcFile, &paramValLstPtr);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to read the xml file for params"
					, __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Read XML File for Parameters");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, NULL);
			}

			break;
		}

		rv = checkForCriticalParams(&iStateApp, paramValLstPtr);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to check the parsed file for critical params" , __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		/* Free any allocated memory of param value list*/
		if(paramValLstPtr != NULL)
		{
			while(paramValLstPtr != NULL)
			{
				tmpNodePtr = paramValLstPtr;
				paramValLstPtr = paramValLstPtr->next;

				if(tmpNodePtr->key)
				{
					free(tmpNodePtr->key);
				}

				if(tmpNodePtr->value)
				{
					free(tmpNodePtr->value);
				}

				free(tmpNodePtr);
			}
		}
		paramValLstPtr = NULL;

		*piStateApp = iStateApp;
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: checkForCriticalParams
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: TRUE / FALSE
 * ============================================================================
 */
static int checkForCriticalParams(int *piStateApp, GENKEYVAL_PTYPE paramValLstPtr)
{
	int			rv					= SUCCESS;
	int 		iStateApp			= SCA_APP_FREE;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
	char *		cKeyPtr				= NULL;
	char *		cValPtr				= NULL;
#ifdef DEBUG
	char		szDbgMsg[512]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while( (paramValLstPtr != NULL) && (rv == SUCCESS) )
	{
		cKeyPtr = paramValLstPtr->key;
		cValPtr = paramValLstPtr->value;

		if((strcasecmp(cKeyPtr, MERCHANT_ID) == SUCCESS) ||
			(strcasecmp(cKeyPtr, TERMINAL_ID) == SUCCESS))
		{
			debug_sprintf(szDbgMsg, "%s: MID/TID parameter are not allowed to be installed; SAF records are present" , __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "MID/TID Parameter are Not Allowed To be Changed; SAF Records are Present");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, NULL);
			}
			iStateApp = SCA_APP_BUSY;
			break;
		}

		paramValLstPtr = paramValLstPtr->next;
	}

	*piStateApp = iStateApp;
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}


/*
 * ============================================================================
 * Function Name: waitForPOStoQueryUpdateStatus		waitForPOSResponse
 *
 * Description	: This APIwill wait for POS to query the update status before going for Reboot/Restart.
 *
 * Input Params	: TimeOut: If it is passed as zero(0), it will wait for indefinate time untill it receives valid msg in MsgQ,
 * 				  otherwise, it will wait for specified time out period & come out after that.
 *
 * Output Params: NONE
 * ============================================================================
 */
static int waitForPOStoQueryUpdateStatus(int iTimeout)
{
	int 					iMsgFlag						= 0;
	int						iStatePOS						= SCA_POS_RESP_TIMEDOUT;
	int						iAppLogEnabled					= isAppLogEnabled();
	ullong					endTime							= 0L;
	ullong					curTime							= 0L;
	static PAAS_BOOL		bTimerStatus					= PAAS_FALSE;
	static time_t			startTime;
	static time_t			finishTime;
	time_t					tPOSNotifyTimeOut;
	char					szAppLogData[300]				= "";
	POSRESPDATA_VHQ_STYPE	stPOSRespDataForVHQ;
#ifdef DEBUG
	char		szDbgMsg[512]		= "";
#endif

	/* MukeshS3: 17-June-16: MCD Requirement - When POS is not quering(UPDATE_STATUS) for a long time & just sitting idle after sending APPLYUPDATES command
	 * Or there might be possibility that POS has been rebooted after applying the updates, so after reboot POS will not be aware of any last updates
	 * So, POS will never query for UPDATE_STATUS & Device will stuck on the last screen(Waiting for POS to Notify).
	 * We are introducing a new timeout parameter from SCA application side. Application will wait for this much time for POS to send the UPDATE_STATUS
	 * command and take the update status value. If, POS is not quering for UPDATE_STATUS command before the timer expires, SCA applition will
	 * go ahead & approve the next Reboot/Restart request from VHQ Agent. In this case POS will not be getting any update status after reboot/restart.
	 */
	// This is to handle for any post install action after software download
	if(bTimerStatus == PAAS_FALSE)
	{
		// Create the timers
		tPOSNotifyTimeOut = (getPOSNotificationTimoutValue() * 60);
		startTime = time(NULL);
		finishTime = startTime + tPOSNotifyTimeOut;
		bTimerStatus = PAAS_TRUE;	// set the timer status
	}
//	else if(time(NULL) > finishTime)	// if timer has expired
//	{
//		bTimerStatus = PAAS_FALSE;	// reset the timer status
//		debug_sprintf(szDbgMsg, "%s: POS Update Notification Timer has been Expired. Approving Restart/Reboot for Post Install Action.", __FUNCTION__);
//		APP_TRACE(szDbgMsg);
//		if(iAppLogEnabled == 1)
//		{
//			strcpy(szAppLogData, "POS Update Notification Timer has been Expired. Approving Restart/Reboot for Post Install Action.");
//			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
//		}
//		iStatePOS = SCA_POS_NOTIFY_SUCCESS;
//		return iStatePOS;
//	}

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	debug_sprintf(szDbgMsg, "%s: Waiting for POS to query update status on secondary port", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "Waiting For POS To Query Update Status On Secondary Port");
		addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
	}
	if(! iTimeout)
	{
		iMsgFlag = MSG_NOERROR;	//blocked call
	}
	else
	{
		iMsgFlag = IPC_NOWAIT;	// non-blocked call
	}

	curTime = svcGetSysMillisec();
	endTime = curTime + (iTimeout * 1000L);
	debug_sprintf(szDbgMsg, "%s: Timeout[%d] Cur Time[%lld] endTime[%lld]", __FUNCTION__, iTimeout, curTime, endTime);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(bTimerStatus == PAAS_TRUE && (time(NULL) > finishTime))	// if timer has expired
		{
			bTimerStatus = PAAS_FALSE;	// reset the timer status
			debug_sprintf(szDbgMsg, "%s: POS Update Notification Timer has been Expired. Approving Restart/Reboot for Post Install Action.", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "POS Update Notification Timer has been Expired. Approving Restart/Reboot for Post Install Action.");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			iStatePOS = SCA_POS_NOTIFY_SUCCESS;
			return iStatePOS;
		}

		memset(&stPOSRespDataForVHQ, 0x00, sizeof(POSRESPDATA_VHQ_STYPE));

		//Read data from the message queue added by bLogic Thread for Applying the updates;
		if(( msgrcv(gVHQRcvFrmPOSMsgQId, &stPOSRespDataForVHQ, sizeof(POSRESPDATA_VHQ_STYPE), 0, iMsgFlag) == -1))
		{
			//debug_sprintf(szDbgMsg, "%s: msgrcv(): %s", __FUNCTION__, strerror(errno));
			//APP_TRACE(szDbgMsg);
			svcWait(1000);
			curTime = svcGetSysMillisec();
//			debug_sprintf(szDbgMsg, "%s: Cur Time[%lld] endTime[%lld]", __FUNCTION__, curTime, endTime);
//			APP_TRACE(szDbgMsg);
			if(endTime < curTime)
			{
				debug_sprintf(szDbgMsg, "%s: Time Out Occured... ", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			continue;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data Received from BLogic ", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(stPOSRespDataForVHQ.iRespType == SCA_POS_NOTIFY_SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: POS has been Notified of Restarting/Rebooting the application/device ", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "POS Has Been Notified Of Restarting/Rebooting The Application/Device");
					addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
				}
				iStatePOS = SCA_POS_NOTIFY_SUCCESS;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Invalid response type in MsgQ", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				continue;
				//FIXME: doing continue, because same MsgQ is being used for both UPDATE_STAUS(secondary) & APPLYUPDATES(primary) command.
				// Anyway both the msgs cann't come together in this MsgQ at a time.
			}
			break;
		}
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iStatePOS);
	APP_TRACE(szDbgMsg);
	return iStatePOS;
}

/*
 * ============================================================================
 * Function Name: intToStrRespMsg
 *
 * Description	: Converts app response status to string messages
 *
 * Input Params	: Integer
 *
 * Output Params: pointer to a string msg
 * ============================================================================
 */
static char* intToStrRespMsg(int iAppStatus)
{
	switch(iAppStatus)
	{
	case LTMS_MSGRESP_APPROVE:
		return "APPROVED";
	case LTMS_MSGRESP_REJECT:
		return "REJECTED";
	case LTMS_MSGRESP_POSTPONED:
		return "POSTPONED";
	}

	switch(iAppStatus)
	{
	case LTMS_MSGRESP_FREE:
		return "FREE";
	case LTMS_MSGRESP_BUSY:
		return "BUSY";
	}

	switch(iAppStatus)
	{
	case LTMS_MSGRESP_FILEINSTALL_SUCCESS:
		return "SUCCESS";
	case LTMS_MSGRESP_FILEINSTALL_FAIL:
		return "FAILURE";
	case LTMS_MSGRESP_FILEINSTALL_BUSY:
		return "BUSY";
	case LTMS_MSGRESP_FILEINSTALL_POSTPONED:
		return "POSTPONED";
	}

	return "Info";	// for other event types like appinfo or paramlist etc.
}

/*
 * ============================================================================
 * Function Name: getPOSNotificationTimoutValue
 *
 * Description	: get POS Update Notification Timeout value
 *
 * Input Params	: NONE
 *
 * Output Params: Timeout value in mintues
 * ============================================================================
 */
time_t getPOSNotificationTimoutValue()
{
	return (vhqSettings.iPOSUpdateNotifyTimeOut);
}

/*
 * ============================================================================
 * Function Name: getSCAPkgName
 *
 * Description	: get the SCA package name
 *
 * Input Params	: NONE
 *
 * Output Params: char*
 * ============================================================================
 */
char* getSCAPkgName()
{
	return (vhqSettings.szSCAPkgName);
}
