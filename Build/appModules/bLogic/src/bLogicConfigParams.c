/******************************************************************
*                       blogicConfigParams.c                          *
*******************************************************************
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis which loads and reads the 		  *
* 			   config parameters required for the blogic Module       *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                kranthik1                                        *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/


#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>
#include <svc.h>
#include <SemtekHTdes.h>
#include "iniparser.h"

#include "ssi/ssiDef.h"

#include "bLogic/bLogicCfgData.h"
#include "bLogic/bLogicCfgDef.h"
#include "appLog/appLogAPIs.h"

#include "uiAgent/uiMsgFmt.h"
#include "uiAgent/uiAPIs.h"
#include "uiAgent/uiCfgDef.h"
#include "common/utils.h"
#include "common/VCL.h"
#include "appLog/appLogAPIs.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

/*Static variables declaration*/
static BLOGIC_CFG_STYPE bLogicSettings;
static PYMNT_CFG_STYPE	pymntSettings;
static STB_CFG_STYPE	stbSettings;

/*Static Function declarations*/
static int 	loadEncryptionType();
static void initializeBLogicSettings();
static void initializePaymentSettings();
static int  loadPymntSettings();
static int  getCashBackSettings();
static int  getCounterTipSettings();
static int  getSAFSettings();
static int  loadSTBSettings();
static int  loadVSDSettings(char *);
static int  storeVSDSettingsFromINI(VSD_DTLS_PTYPE, char *);
static int  validateVSDSettings(VSD_DTLS_PTYPE);
static void freeVSDtagList(VSD_DTLS_PTYPE);
/*
 * ============================================================================
 * Function Name: loadBLogicConfigParams
 *
 * Description	: This function reads all the config parameters from the
 * 				  config.ur1 file and store them in the static structure.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int	loadBLogicConfigParams(char *pszErrMsg)
{
	int		rv					= 0;
	int		iLen				= 0;
	int		iVal				= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";
	char	szTemp[30]			= "";

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	initializeBLogicSettings();

	iLen = sizeof(szTemp);

	/* ------------- Check for VSP settings ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, VSP_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'N') || (*szTemp == 'n'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting VSP Enabled as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.vspEnabled = PAAS_FALSE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting VSP Enabled as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.vspEnabled = PAAS_TRUE;
		}
	}
	else
	{
		/* No value found for vsp enabled parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for vsp enabled;Set TRUE"
															, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.vspEnabled = PAAS_TRUE;
	}

	/* ------------- Check for VSD settings ------------------- */
	/* check for VSD_BASE64OUT parameter in reg section */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_REG, "vsd_base64out", szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		if(iVal == 1)
		{
			debug_sprintf(szDbgMsg, "%s: Setting VSD B64 Encoding Enabled as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.vsdB64EncodingEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting VSD B64 Encoding Enabled as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.vsdB64EncodingEnabled = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for VSD_BASE64OUT parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for VSD B64 Encoding Enabled;Set FALSE"
															, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.vsdB64EncodingEnabled = PAAS_FALSE;
	}

	/* ----- Check for N/W configuration required  settings -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, NETWORK_CFG_REQD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting N/W cfg req flag as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.networkcfgreqd = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting N/W cfg req flag as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.networkcfgreqd = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for PaaS demo mode parameter */
		debug_sprintf(szDbgMsg,"%s: No val found for N/w cfg req flag;Set FALSE"
															, __FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.networkcfgreqd = PAAS_FALSE;
	}

	/* ------------- Check if VSP reg is required to be redone ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, REDO_VSP_REG, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting redo VSP reg as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.redoVSPReg = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting redo VSP reg as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.redoVSPReg = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for PaaS demo mode parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for vsp re-reg; Set FALSE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.redoVSPReg = PAAS_FALSE;
	}

	/* ------------- Check if Card details are required for Void ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, VOID_CARD_DTLS_REQD, szTemp, iLen); //Praveen_P1: Currently we are not using this config variable
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting void card details required as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.voidCardDtlsreqd = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting void card details required as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.voidCardDtlsreqd = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for PaaS demo mode parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for voidcarddtlsreqd Set FALSE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.voidCardDtlsreqd = PAAS_FALSE;
	}

	/* ------------- Check if Card details are required for Debit Void ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, DEBIT_VOID_CARD_DTLS_REQD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Debit Void card details required as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.debitVoidCardDtlsReqd = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Debit Void card details required as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.debitVoidCardDtlsReqd = PAAS_FALSE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: No value found for debitvoidcarddtlsreqd Set TRUE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.debitVoidCardDtlsReqd = PAAS_TRUE;
	}

	/* ------------- Check if we have to return embossed card num for gift trans ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, RETURN_EMBOSSED_NUM_FOR_GIFT, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Return Embossed Num for Gift Transaction as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.returnEmbossedNumForGift = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Return Embossed Num for Gift Transaction as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.returnEmbossedNumForGift = PAAS_FALSE;
		}
	}
	else
	{

		debug_sprintf(szDbgMsg,"%s: No value found for %s Set FALSE (Default Action)",
																__FUNCTION__, RETURN_EMBOSSED_NUM_FOR_GIFT);
		APP_TRACE(szDbgMsg);

		bLogicSettings.returnEmbossedNumForGift = PAAS_FALSE;
	}
	/* ------------- Check if we have to embossed card number calculation is required for gift trans ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, EMBOSSED_NUM_CALC_REQD, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Bool value accordingly */
		if( (*szTemp == 'y') || (*szTemp == 'Y') )
		{
			bLogicSettings.bEmbossedNumCalReqd = PAAS_TRUE;

			debug_sprintf(szDbgMsg,"%s: Setting Embossed Card Num Calculation is REQUIRED",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			bLogicSettings.bEmbossedNumCalReqd = PAAS_FALSE;

			debug_sprintf(szDbgMsg,"%s: Setting Embossed Card Num Calculation is NOT REQUIRED",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		bLogicSettings.bEmbossedNumCalReqd = PAAS_FALSE;

		debug_sprintf(szDbgMsg, "%s: Setting Embossed Card Num Calculation is NOT REQUIRED (default action)",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* ------------- Check if we have to return embossed card num for Private Label trans ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, RETURN_EMBOSSED_NUM_FOR_PL, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Return Embossed Num for Priv Label Transaction as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.returnEmbossedNumForPL = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Return Embossed Num for Priv Label Transaction as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.returnEmbossedNumForPL = PAAS_FALSE;
		}
	}
	else
	{

		debug_sprintf(szDbgMsg,"%s: No value found for %s Set FALSE (Default Action)",
																__FUNCTION__, RETURN_EMBOSSED_NUM_FOR_PL);
		APP_TRACE(szDbgMsg);

		bLogicSettings.returnEmbossedNumForPL = PAAS_FALSE;
	}

	/* ------------- Check if we have to capture signature for Manual Transactions ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, CAPTURE_SIGN_FOR_MANUAL_TRAN, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Capture Signature For Manual Tran as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.captureSignForManualTran = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Capture Signature For Manual Tran as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.captureSignForManualTran = PAAS_FALSE;
		}
	}
	else
	{

		debug_sprintf(szDbgMsg,"%s: No value found for %s Set TRUE (Default Action)",
																__FUNCTION__, CAPTURE_SIGN_FOR_MANUAL_TRAN);
		APP_TRACE(szDbgMsg);

		bLogicSettings.captureSignForManualTran = PAAS_TRUE;
	}

	/* ------------- Check if we have sending sign dtls to host------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, SEND_SIGN_DTLS_TO_SSI_HOST, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'N') || (*szTemp == 'n'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Send Sign Dtls to SSI Host as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.sendSignDtlsToSSIHost = PAAS_FALSE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Send Sign Dtls to SSI Host as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.sendSignDtlsToSSIHost = PAAS_TRUE;
		}
	}
	else
	{

		debug_sprintf(szDbgMsg,"%s: No value found for %s Set TRUE (Default Action)",
																__FUNCTION__, SEND_SIGN_DTLS_TO_SSI_HOST);
		APP_TRACE(szDbgMsg);

		bLogicSettings.sendSignDtlsToSSIHost = PAAS_TRUE;
	}

	/* ------------- Check if swipe ahead is enabled ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, SWIPE_AHEAD_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting swipe ahead enabled as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.swipeaheadEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting swipe ahead enabled as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.swipeaheadEnabled = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for swipe ahead parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for swipeaheadenabled Set FALSE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.swipeaheadEnabled = PAAS_FALSE;
	}

	/* ------------- Check if Offline CVM needs to be sent as Debit  ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, DEBIT_OFFLINE_PIN_CVM, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Debit Offline PIN CVM as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.debitOffPinCVM = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Debit Offline PIN CVM as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.debitOffPinCVM = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for debit ofline cvm parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for Debit Offline PIN CVM  Set FALSE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.debitOffPinCVM = PAAS_FALSE;
	}

	/* ------------- Check if duplicate transaction detect is enabled ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, DUP_DETECT_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting duplicate swipe detect enabled as TRUE",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.dupSwipeDetectEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting duplicate swipe detect enabled as FALSE",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.dupSwipeDetectEnabled = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for dup detect parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for dupSwipeDetectEnabled Set FALSE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.dupSwipeDetectEnabled = PAAS_FALSE;
	}

	/* ------------- Check if Line Item Display is enabled ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, LINEITEM_DISPLAY_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Line Iem Display enabled as TRUE",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.lineItemDisplayEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Line Item Display enabled as FALSE",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.lineItemDisplayEnabled = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for Line Item Display */
		debug_sprintf(szDbgMsg,"%s: No value found for Line Item Display Set FALSE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.lineItemDisplayEnabled = PAAS_TRUE;
	}

	/* ------------- Check if full Line Item Display is enabled ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, FULL_LINEITEM_DISPLAY, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting full Line Iem Display enabled as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.fullLineItemDisplayEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting full Line Item Display enabled as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.fullLineItemDisplayEnabled = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for full Line Item Display */
		debug_sprintf(szDbgMsg,"%s: No value found for full Line Item Display Set FALSE",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.fullLineItemDisplayEnabled = PAAS_FALSE;
	}

	/* ------------- Check if TOR is enabled ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, TOR_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting TOR enabled as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.torEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting TOR enabled as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.torEnabled = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for swipe ahead parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for torenabled, Setting as  FALSE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.torEnabled = PAAS_FALSE;
	}

	/* ------------- Check for TOR retry count value ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, TOR_RETRY_COUNT, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		if(((iLen = strlen(szTemp)) > 0) && (strspn(szTemp,"0123456789") == iLen) && (iVal > 0) && (iVal <= 5))
		{
			debug_sprintf(szDbgMsg, "%s: Setting TOR retry count as [%d]",
																__FUNCTION__, atoi(szTemp));
			APP_TRACE(szDbgMsg);

			bLogicSettings.iTORretryCount = iVal;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid value; setting TOR retry count as [1]",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.iTORretryCount = 1;
		}
		iLen = sizeof(szTemp);
	}
	else
	{
		/* No value found for torretries parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for torretries, Setting as [1]",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.iTORretryCount = 1;
	}

	/* ------------- Check if device name capture is enabled ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, DEV_NAME_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting device name enabled as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.devNameEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting device name enabled as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.devNameEnabled = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for swipe ahead parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for devNameEnabled Set FALSE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.devNameEnabled = PAAS_FALSE;
	}

	if(bLogicSettings.devNameEnabled) //Look for the device name only if device name is enabled
	{
		/* ------------- Capture the device name ------- */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DEVICE, DEVICE_NAME, szTemp, iLen);
		if(rv > 0)
		{

				debug_sprintf(szDbgMsg, "%s: Device name given is %s",
						__FUNCTION__, szTemp);
				APP_TRACE(szDbgMsg);

				strncpy(bLogicSettings.szDevName, szTemp, sizeof(bLogicSettings.szDevName) - 1);
		}
		else
		{
			/* No value found for swipe ahead parameter */
			debug_sprintf(szDbgMsg,"%s: Device name not found",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}


	/* ------------- Check if capturing of merchant settings is required ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, MCNT_SETTINGS_REQD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Merchant Settings  required as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.mcntSettingsReqd = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Merchant Settings required as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.mcntSettingsReqd = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for merchant settings required parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for Merchant Settings required Set FALSE",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.mcntSettingsReqd = PAAS_FALSE;
	}

	/* ------------- Check if capturing of Signature is Required for Private Label Transactions ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, CAPTURE_SIG_FOR_PRIV_LABEL, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Signature Capture for Private Label Trans set as TRUE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.captureSignForPrivTran = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Signature Capture for Private Label Trans set as FALSE",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.captureSignForPrivTran = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for merchant settings required parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for  Signature Capture for Private Label Trans",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.captureSignForPrivTran = PAAS_FALSE;
	}

	/* ------------- Check if SVS Non Denominated Card Enabled ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, SVS_NON_DENOMINATED_CARD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: SVS Non Denominated Card Enabled",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.svsNonDenominated = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: SVS Non Denominated Card Disabled",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.svsNonDenominated = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for SVS Non Denominated parameter */
		debug_sprintf(szDbgMsg,"%s: No Value Found for SVS Non Denominated Card. Disabled by Default",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.svsNonDenominated = PAAS_FALSE;
	}

	/* ------------- Check if Unsolicitated Msg needs to be sent ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, UNSOL_MSG_DURING_PYMT_TRAN, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting 'Send Unsolicitated Msg during Pymt Tran' as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.sendUnsolMsgDuringPymtTran = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting 'Send Unsolicitated Msg during Pymt Tran' as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.sendUnsolMsgDuringPymtTran = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for swipe ahead parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for Unsolicitated Msg during Pymt Tran, Setting as  FALSE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.sendUnsolMsgDuringPymtTran = PAAS_FALSE;
	}

	/* ----- Check for N/W Obtain Wait Time if Fails to obtain IP and Network Mode -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, WAIT_TO_OBTAIN_NETWORK_IP, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		if(iVal >= 1)
		{
			debug_sprintf(szDbgMsg, "%s: Default Network Obtain Wait Time if Failed is [%d]", __FUNCTION__, iVal);
			APP_TRACE(szDbgMsg);

			bLogicSettings.iWaitToObtainNetworkIP = iVal;
		}
		else
		{
			/* Incorrect value for Network Config Wait Time */
			debug_sprintf(szDbgMsg,
					"%s: Incorrect Value for Network Obtain Wait Time, Setting it to Default [%d]",
											__FUNCTION__, DFLT_WAIT_TO_OBTAIN_NETWORK_IP);
			APP_TRACE(szDbgMsg);

			/*In this case, We will keep the value to DFLT_WAIT_TO_OBTAIN_NETWORK_IP*/
			bLogicSettings.iWaitToObtainNetworkIP = DFLT_WAIT_TO_OBTAIN_NETWORK_IP;
		}
	}
	else
	{
		/* No value found for Network obtain Wait Time */
		debug_sprintf(szDbgMsg,
					"%s: No Value Found for Network Obtain Wait Time, Setting it to Default [%d]",
											__FUNCTION__, DFLT_WAIT_TO_OBTAIN_NETWORK_IP);
		APP_TRACE(szDbgMsg);
		bLogicSettings.iWaitToObtainNetworkIP = DFLT_WAIT_TO_OBTAIN_NETWORK_IP;
	}

	/* ------------- Check if Include TA Flag Enabled ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, INCLUDE_TA_FLAG, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Include TA Flag Enabled",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.includeTAFlag = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: IncludeTA Flag Disabled",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.includeTAFlag = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for include TA Flag */
		debug_sprintf(szDbgMsg,"%s: No Value Found for include TA Flag. Disabled by Default",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.includeTAFlag = PAAS_FALSE;
	}

	/* ----- Check for Display Interval of Terminal Identification Screen -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, TERM_IDEN_DISPLAY_INTERVAL, szTemp, iLen);
	if(rv > 0)
	{
		setPOSACKPort(POS_ACK_PORT);
		iVal = atoi(szTemp);
		if(iVal >= 1 && iVal <= 120)
		{
			debug_sprintf(szDbgMsg, "%s: Terminal Identification Display Interval is [%d]", __FUNCTION__, iVal);
			APP_TRACE(szDbgMsg);

			bLogicSettings.iTermIdenDispInterval = iVal;
		}
		else if(iVal == 0)
		{
			debug_sprintf(szDbgMsg,
						"%s: Value Found for Terminal Identification Display Interval is 0, No need to Initiate Broadcast", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			bLogicSettings.iTermIdenDispInterval = 0;
		}
		else
		{
			/* Incorrect value for Network Config Wait Time */
			debug_sprintf(szDbgMsg,
					"%s: Incorrect Value, Terminal Identification Display Interval set to Defualt Value [%d] secs", __FUNCTION__, DFLT_TERM_IDEN_DISPLAY_INETRVAL);
			APP_TRACE(szDbgMsg);

			/*In this case, We will keep the value to DFLT_TERM_IDEN_DISPLAY_INETRVAL*/
			bLogicSettings.iTermIdenDispInterval = DFLT_TERM_IDEN_DISPLAY_INETRVAL;
			memset(szTemp, 0x00, iLen);
			sprintf(szTemp, "%d", DFLT_TERM_IDEN_DISPLAY_INETRVAL);
			rv = putEnvFile(SECTION_DEVICE, TERM_IDEN_DISPLAY_INTERVAL, szTemp);
		}
	}
	else
	{
		/* No value found for Display Interval In this case we we keep the value to 0 (Do Not Initiate The Terminal Identification to the Network)*/
		debug_sprintf(szDbgMsg,
					"%s: No Value Found for Terminal Identification Display Interval, Setting it to 0, No need to Initiate Broadcast",
											__FUNCTION__);
		APP_TRACE(szDbgMsg);
		bLogicSettings.iTermIdenDispInterval = 0;
	}

	/* ----- Check for Wait Time for POS ACK Interval during terminal Identification  -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, TERM_IDEN_POS_ACK_INTERVAL, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		if(iVal >= 30 && iVal <= 300)
		{
			debug_sprintf(szDbgMsg, "%s: Interval Time to wait for POS ACK during Terminal Identification is  [%d]", __FUNCTION__, iVal);
			APP_TRACE(szDbgMsg);

			bLogicSettings.iTermIdenPOSAckInterval = iVal;
		}
		else
		{
			/* Incorrect value for Interval Time */
			debug_sprintf(szDbgMsg,
					"%s: Incorrect Value for POS ACK Interval Time, Setting it to Default [%d]",
											__FUNCTION__, DFLT_TERM_IDEN_POS_ACK_INTERVAL);
			APP_TRACE(szDbgMsg);

			/*In this case, We will keep the value to DFLT_TERM_IDEN_POS_ACK_INTERVAL*/
			bLogicSettings.iTermIdenPOSAckInterval = DFLT_TERM_IDEN_POS_ACK_INTERVAL;
			memset(szTemp, 0x00, iLen);
			sprintf(szTemp, "%d", DFLT_TERM_IDEN_POS_ACK_INTERVAL);
			rv = putEnvFile(SECTION_DEVICE, TERM_IDEN_POS_ACK_INTERVAL, szTemp);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg,
					"%s: No Value Found for POS ACK Interval Time, Setting it to Default [%d]",
											__FUNCTION__, DFLT_TERM_IDEN_POS_ACK_INTERVAL);
		APP_TRACE(szDbgMsg);
		bLogicSettings.iTermIdenPOSAckInterval = DFLT_TERM_IDEN_POS_ACK_INTERVAL;
		memset(szTemp, 0x00, iLen);
		sprintf(szTemp, "%d", DFLT_TERM_IDEN_POS_ACK_INTERVAL);
		rv = putEnvFile(SECTION_DEVICE, TERM_IDEN_POS_ACK_INTERVAL, szTemp);
	}

	/* ----- Check for Broadcast Port to Initiate terminal Identification  -------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, BROADCAST_PORT, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		if(iVal >= 1 && iVal <= 65535)
		{
			debug_sprintf(szDbgMsg, "%s: Broadcast Port to Initiate Terminal Identification is  [%d]", __FUNCTION__, iVal);
			APP_TRACE(szDbgMsg);

			bLogicSettings.iBroadcastPort = iVal;
			if(getPOSACKPort() == iVal)
			{
				setPOSACKPort(iVal + 10); // Set a Different POS ACKNOWLEDGEMENT Port if both the broadcast and POS AK port are same.
			}
		}
		else
		{
			/* Incorrect value for broadcast port */
			debug_sprintf(szDbgMsg,
					"%s: Incorrect Value for Broadcast Port, Setting it to Default [%d]",
											__FUNCTION__, DFLT_TERM_IDEN_POS_ACK_INTERVAL);
			APP_TRACE(szDbgMsg);

			/*In this case, We will keep the value to DFLT_BROADCAST_PORT*/
			bLogicSettings.iBroadcastPort = DFLT_BROADCAST_PORT;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg,
					"%s: No Value Found for Broadcast Port, Setting it to Default [%d]",
											__FUNCTION__, DFLT_BROADCAST_PORT);
		APP_TRACE(szDbgMsg);
		bLogicSettings.iBroadcastPort = DFLT_BROADCAST_PORT;
	}

	/* ------------- Check if DCC is Enabled ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, DCC_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: DCC is Enabled", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.dccEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: DCC is Disabled",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.dccEnabled = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for dccEnabled  flag*/
		debug_sprintf(szDbgMsg,"%s: No Value Found for DCC Enabled. Disabled by Default",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		bLogicSettings.dccEnabled = PAAS_FALSE;
	}

	rv = loadSTBSettings();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error in loading STB settings", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "Failed to Load the STB Settings [%d]", rv);
			addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
		}
	}

	rv = loadEncryptionType();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error in loading encryption type",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "Failed to Load the Encryption Type [%d]", rv);
			addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
		}
	}
	else
	{
		//CID 67420 (#1 of 1): Unused value (UNUSED_VALUE), T_RaghavendranR1 We are overwriting below. As Coverity fix we  moved initVSP below loadPaymentSettings
		if(bLogicSettings.encryptionType == VSP_ENC)
		{
			rv = InitVSP();

			debug_sprintf(szDbgMsg, "%s: Initialized the VSP and it returned %d", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);

			if(rv <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: VSP initialization failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "VSP Initialization Failed With Return Value [%d]", rv);
					strcpy(szAppLogDiag, "Please Check the VCL Setting");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, szAppLogDiag);
				}

				// Not Sending Hard Error, if VSP Init Fails, if VSPInit Fails VSP Registration will also get Fail
				//rv = FAILURE;
			}
			else
			{
				//rv = SUCCESS; // InitVSP on success return 1, so sending SUCCES on successful VSP Operation.
			}
		}
		/* MukeshS3: 8-March-16: FRD 3.80
		 * SCA is supporting ADE encryption(using VSD format) from now onwards.
		 * If VSD encryption is configured, than we need to load all the setting present in /mnt/flash/userdata/share/VSD.INI, that will be
		 * required while parsing the data coming from XPI/FA in E07 response.
		 */
		else if(bLogicSettings.encryptionType == VSD_ENC)
		{
			rv = loadVSDSettings(pszErrMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: VSD initialization failed[%d]", __FUNCTION__, rv);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				if(strlen(pszErrMsg) == 0)
				{
					getDisplayMsg(pszErrMsg, MSG_VSD_LOAD_ERR);
				}
				return rv;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Initialized VSD encryption successfully[%d]", __FUNCTION__, rv);
				APP_TRACE(szDbgMsg);
			}
		}


		/* Load the payment settings */
		rv = loadPymntSettings();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in loading Payment settings",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;

}

/*
 * ============================================================================
 * Function Name: loadEncryptionType
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
static int loadEncryptionType()
{
	int		rv					= SUCCESS;
	char*	pszConfig[4]		= {"VSPENCRYPTION", "RSA_ENCRYPT", "RSA_KEY_MD5", "CARD_RESPONSE_FORMAT"};
	char*  	pszConfigVal[4]		= {NULL, NULL, NULL, NULL};

#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		rv = getConfigVarWrapper(pszConfig, pszConfigVal, 4);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s:Not able to retrieve the config params", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		if (pszConfigVal[0] != NULL)
		{
			if(atoi(pszConfigVal[0]) == 1)
			{
				bLogicSettings.encryptionType = VSP_ENC;

				debug_sprintf(szDbgMsg, "%s:Encryption type is VSP", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				free(pszConfigVal[0]);
				break;
			}

			/*Freeing the memory which was allocated while parsing the ui resp*/
			free(pszConfigVal[0]);
		}

		if (pszConfigVal[1] != NULL)
		{
			if(atoi(pszConfigVal[1]) == 1)
			{
				bLogicSettings.encryptionType = RSA_ENC;

				debug_sprintf(szDbgMsg, "%s:Encryption type is RSA", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				free(pszConfigVal[1]);
				break;
			}

			/*Freeing the memory which was allocated while parsing the ui resp*/
			free(pszConfigVal[1]);
		}

		if (pszConfigVal[3] != NULL)
		{
			if(atoi(pszConfigVal[3]) == 3)	// For VSD
			{
				bLogicSettings.encryptionType = VSD_ENC;

				debug_sprintf(szDbgMsg, "%s:Encryption type is VSD", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				free(pszConfigVal[3]);
				break;
			}

			/*Freeing the memory which was allocated while parsing the ui resp*/
			free(pszConfigVal[3]);
		}

		if(bLogicSettings.vspEnabled == PAAS_TRUE)
		{
			bLogicSettings.encryptionType = VSP_ENC;

			debug_sprintf(szDbgMsg, "%s: VSP enabled so setting encryption type to VSP", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			bLogicSettings.encryptionType = NO_ENC;
			debug_sprintf(szDbgMsg, "%s:Encryption type is NO_ENC", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	if(pszConfigVal[2] != NULL)
	{
		strcpy(bLogicSettings.szRSAFingerPrint, pszConfigVal[2]);

		/*Freeing the memory which was allocated while parsing the ui resp*/
		free(pszConfigVal[2]);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: initializeBLogicSettings
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
static void initializeBLogicSettings()
{
	initializePaymentSettings();

	memset(&bLogicSettings, 0x00, sizeof(BLOGIC_CFG_STYPE));

	return;
}

/*
 * ============================================================================
 * Function Name: isVSPEnabledInDevice
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isVSPEnabledInDevice()
{
	return bLogicSettings.vspEnabled;
}

/*
 * ============================================================================
 * Function Name: isVSDb64EncodingEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isVSDb64EncodingEnabled()
{
	return bLogicSettings.vsdB64EncodingEnabled;
}

/*
 * ============================================================================
 * Function Name: isCardDtlsRequiedforVoid
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isCardDtlsRequiedforVoid() //Praveen_P1: Currently we are not using this one
{
	return bLogicSettings.voidCardDtlsreqd;
}

/*
 * ============================================================================
 * Function Name: isCardDtlsRequiedforDebitnEBTVoid
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isCardDtlsRequiedforDebitnEBTVoid()
{
	return bLogicSettings.debitVoidCardDtlsReqd;
}

/*
 * ============================================================================
 * Function Name: returnEmbossedNumForGiftType
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL returnEmbossedNumForGiftType()
{
	return bLogicSettings.returnEmbossedNumForGift;
}

/*
 * ============================================================================
 * Function Name: isEmbossedCardNumCalcRequired
 *
 *
 * Description	: This function will check if Embossed Card Number Calculation is required or not
 *
 *
 * Input Params	:
 *
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */

PAAS_BOOL isEmbossedCardNumCalcRequired()
{
	return bLogicSettings.bEmbossedNumCalReqd;
}

/*
 * ============================================================================
 * Function Name: returnEmbossedNumForPrivLblType
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL returnEmbossedNumForPrivLblType()
{
	return bLogicSettings.returnEmbossedNumForPL;
}

/*
 * ============================================================================
 * Function Name: isSigCaptureReqdForManualTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isSigCaptureReqdForManualTran()
{
	return bLogicSettings.captureSignForManualTran;
}

/*
 * ============================================================================
 * Function Name: isSendSignDtlsToSSIHostEnabled
 *
 * Description	: Getter function for the parameter sendsigndtlstossihost
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isSendSignDtlsToSSIHostEnabled()
{
	return bLogicSettings.sendSignDtlsToSSIHost;
}

/*
 * ============================================================================
 * Function Name: isSigCaptureReqdForPrivTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isSigCaptureReqdForPrivTran()
{
	return bLogicSettings.captureSignForPrivTran;
}

/*
 * ============================================================================
 * Function Name: isSVSNonDenominatedEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isSVSNonDenominatedEnabled()
{
	return bLogicSettings.svsNonDenominated;
}

/*
 * ============================================================================
 * Function Name: getWaitTimeToObtainNetworkIP
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getWaitTimeToObtainNetworkIP()
{
	return bLogicSettings.iWaitToObtainNetworkIP;
}

/*
 * ============================================================================
 * Function Name: getTermIdenDisplayInterval
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getTermIdenDisplayInterval()
{
	return bLogicSettings.iTermIdenDispInterval;
}

/*
 * ============================================================================
 * Function Name: getTermIdenPOSACKInterval
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getTermIdenPOSACKInterval()
{
	return bLogicSettings.iTermIdenPOSAckInterval;
}

/*
 * ============================================================================
 * Function Name: getTermIdenBroadcastPort
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getTermIdenBroadcastPort()
{
	return bLogicSettings.iBroadcastPort;
}

/*
 * ============================================================================
 * Function Name: getPOSACKPort
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getPOSACKPort()
{
	return bLogicSettings.iPOSACKPort;
}

/*
 * ============================================================================
 * Function Name: setPOSACKPort
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int setPOSACKPort(int iPort)
{

	bLogicSettings.iPOSACKPort = iPort;

	return 1;
}
/*
 * ============================================================================
 * Function Name: isIncludeTAFlagEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isIncludeTAFlagEnabled()
{
	return bLogicSettings.includeTAFlag;
}

/*
 * ============================================================================
 * Function Name: isDupSwipeDetectEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isDupSwipeDetectEnabled()
{
	return bLogicSettings.dupSwipeDetectEnabled;
}

/*
 * ============================================================================
 * Function Name: isLineItemDisplayEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isLineItemDisplayEnabled()
{
	return bLogicSettings.lineItemDisplayEnabled;
}
/*
 * ============================================================================
 * Function Name: isLineItemDisplayEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isFullLineItemDisplayEnabled()
{
	return bLogicSettings.fullLineItemDisplayEnabled;
}


/*
 * ============================================================================
 * Function Name: isSwipeAheadEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isSwipeAheadEnabled()
{
	return bLogicSettings.swipeaheadEnabled;
}

/*
 * ============================================================================
 * Function Name: setSwipeAheadParamValue
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setSwipeAheadParamValue(PAAS_BOOL bValue)
{
	bLogicSettings.swipeaheadEnabled = bValue;
}

/*
 * ============================================================================
 * Function Name: setNwCfgReqd
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int setNwCfgReqd(PAAS_BOOL status)
{
	int		rv			= SUCCESS;
	char	strVal[2]	= "";

	bLogicSettings.networkcfgreqd = status;

	switch(status)
	{
	case PAAS_TRUE:
		*strVal = 'Y';
		break;

	case PAAS_FALSE:
		*strVal = 'N';
		break;
	}

	rv = putEnvFile(SECTION_DEVICE, NETWORK_CFG_REQD, strVal);
	if(rv < 0)
	{
		rv = FAILURE;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: isNwCfgReqd
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isNwCfgReqd()
{
	PAAS_BOOL	rv	= PAAS_FALSE;

	if(bLogicSettings.networkcfgreqd == PAAS_TRUE)
	{
		rv = PAAS_TRUE;
	}
	else
	{
		rv = PAAS_FALSE;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: isVSPReRegReqd
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isVSPReRegReqd()
{
	PAAS_BOOL	rv	= PAAS_FALSE;

	if(bLogicSettings.redoVSPReg == PAAS_TRUE)
	{
		rv = PAAS_TRUE;
	}
	else
	{
		rv = PAAS_FALSE;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: setVSPReRegReqd
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int setVSPReRegReqd(PAAS_BOOL status)
{
	int		rv				= SUCCESS;
	char	strVal[2]		= "";
	char	szProcessor[20]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	memset(szProcessor, 0x00, sizeof(szProcessor));
	rv = getEnvFile(SECTION_DHI, PROCESSOR, szProcessor, sizeof(szProcessor));
	if(rv <= 0)	//T_RaghavendranR1, CID83338 : Unchecked return value (CHECKED_RETURN)
	{
		debug_sprintf(szDbgMsg, "%s: Processor Field Not Present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Processor [%s]", __FUNCTION__, szProcessor);
	APP_TRACE(szDbgMsg);

	if((strcasecmp(szProcessor, "elvn") != SUCCESS)) // Not required to set the redo VSP for Elavon Host, as VSP will be initiated for Elavon for Fist transaction with different Locator values from POS.
	{
		bLogicSettings.redoVSPReg = status;

		switch(status)
		{
		case PAAS_TRUE:
			*strVal = 'Y';
			break;

		case PAAS_FALSE:
			*strVal = 'N';
			break;
		}

		rv = putEnvFile(SECTION_DEVICE, REDO_VSP_REG, strVal);
		if(rv < 0)
		{
			rv = FAILURE;
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: isVSPActivated
 *
 * Description	: This API would be called in device preamble part to
 * 					check vsp is activated or not.
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isVSPActivated()
{
	int			rv			= 0;
	int			iLen		= 0;
	char		tmpStr[10]	= "";
	PAAS_BOOL	retVal		= PAAS_TRUE;

	iLen = sizeof(tmpStr);

	rv = getEnvFile(SECTION_REG, VSP_ACTIVATED, tmpStr, iLen);
	if(rv > 0)
	{
		if(*tmpStr == '1')
		{
			/* True case scenario */
			retVal = PAAS_TRUE;
		}
		else
		{
			/* False case scenario */
			retVal = PAAS_FALSE;
		}
	}
	else
	{
		/* Cant find the config variable in the config.usr1 */
		/* False case scenario */
		retVal = PAAS_FALSE;
	}

	return retVal;
}

/*
 * ============================================================================
 * Function Name: isRedoVSPAfterAdvanceDDKActivated
 *
 * Description	: This API would be called to check if Advance DDK success
 * 					in Device and Failed with Host
 *
 * Input Params	: If bReadfromConfig is true, config will be read from file,
 * 					otherwise local copy will be shared
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isRedoVSPAfterAdvanceDDKActivated(PAAS_BOOL bReadfromConfig)
{
	int			rv			= 0;
	int			iLen		= 0;
	char		tmpStr[10]	= "";
	PAAS_BOOL	retVal		= PAAS_TRUE;

	if(bReadfromConfig)
	{
		iLen = sizeof(tmpStr);

		rv = getEnvFile(SECTION_DEVICE, REDO_VSP_AFTER_ADVANCE_DDK, tmpStr, iLen);
		if(rv > 0)
		{
			if(*tmpStr == '1')
			{
				/* True case scenario */
				retVal = PAAS_TRUE;
				bLogicSettings.redoVSPAfterDDKAdvanced = PAAS_TRUE;
			}
			else
			{
				/* False case scenario */
				retVal = PAAS_FALSE;
				bLogicSettings.redoVSPAfterDDKAdvanced = PAAS_FALSE;
			}
		}
		else
		{
			/* Cant find the config variable in the config.usr1 */
			/* False case scenario */
			retVal = PAAS_FALSE;
		}
	}
	else
	{
		retVal = bLogicSettings.redoVSPAfterDDKAdvanced;
	}

	return retVal;
}

/*
 * ============================================================================
 * Function Name: initializePaymentSettings
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
static void initializePaymentSettings()
{
	if(pymntSettings.cbCfgPtr != NULL)
	{
		free(pymntSettings.cbCfgPtr);
	}

	if(pymntSettings.ctrTipCfgPtr != NULL)
	{
		free(pymntSettings.ctrTipCfgPtr);
	}

	memset(&pymntSettings, 0x00, sizeof(PYMNT_CFG_STYPE));

	return;
}

static int loadSTBSettings()
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	double	fVal			= 0;
	char	szTemp[30]		= "";

#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&stbSettings, 0x00, sizeof(STB_CFG_STYPE));

	while(1)
	{
		iLen = sizeof(szTemp);
		memset(szTemp, 0x00, sizeof(szTemp));
		/* Check if STB logic is enabled in the configuration file */
		rv = getEnvFile(SECTION_SPINTHEBIN, STB_LOGIC_ENABLED, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				stbSettings.stbLogicEnabled = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: Setting STB Logic Enabled as TRUE",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				stbSettings.stbLogicEnabled = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: Setting STB Logic Enabled as FALSE",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}
		else
		{
			stbSettings.stbLogicEnabled = PAAS_FALSE;

			debug_sprintf(szDbgMsg, "%s: Setting STB Logic Enabled as FALSE (default action)",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		/* Get the STB amount limit from the config file */

		memset(szTemp, 0x00, sizeof(szTemp));

		rv = getEnvFile(SECTION_SPINTHEBIN, STB_SIG_LIMIT, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found in the configuration file */
			fVal = atof(szTemp);
			if(fVal < 0)
			{
				debug_sprintf(szDbgMsg, "%s: Incorrect signature amt limit [%lf]",
														__FUNCTION__, fVal);
				APP_TRACE(szDbgMsg);

				debug_sprintf(szDbgMsg, "%s: Disabling STB logic", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* Disable the STB logic enabled */
				stbSettings.stbLogicEnabled = PAAS_FALSE;
				break;
			}
			else
			{
				/* Store the signature amount limit. */
				stbSettings.fSigLimit = fVal;
			}
		}
		else
		{
			/* Value not found in the configuration file */
			debug_sprintf(szDbgMsg, "%s: signature amt limit for STB/ASR not found",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Disabling STB logic",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Disable the STB logic enabled */
			stbSettings.stbLogicEnabled = PAAS_FALSE;
			break;
		}

		rv = getEnvFile(SECTION_SPINTHEBIN, STB_PIN_LIMIT, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found in the configuration file */
			fVal = atof(szTemp);
			if(fVal < 0)
			{
				debug_sprintf(szDbgMsg, "%s: Incorrect pin amt limit [%lf]",
														__FUNCTION__, fVal);
				APP_TRACE(szDbgMsg);

				debug_sprintf(szDbgMsg, "%s: Disabling STB logic",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* Disable the STB logic enabled */
				stbSettings.stbLogicEnabled = PAAS_FALSE;
				break;
			}
			else
			{
				/* Store the pin amount limit. */
				stbSettings.fPinLimit = fVal;
			}
		}
		else
		{
			/* Value not found in the configuration file */
			debug_sprintf(szDbgMsg, "%s: Pin amt limit for STB/ASR not found",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Disabling STB logic",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Disable the STB logic enabled */
			stbSettings.stbLogicEnabled = PAAS_FALSE;
			break;
		}

		rv = getEnvFile(SECTION_SPINTHEBIN, STB_INTERCHANGE_MGMT_LIMIT, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found in the configuration file */
			fVal = atof(szTemp);
			if(fVal < 0)
			{
				debug_sprintf(szDbgMsg, "%s: Incorrect interchange management amt limit [%lf]",
														__FUNCTION__, fVal);
				APP_TRACE(szDbgMsg);

				debug_sprintf(szDbgMsg, "%s: Disabling STB logic",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* Disable the STB logic enabled */
				stbSettings.stbLogicEnabled = PAAS_FALSE;
				break;
			}
			else
			{
				/* Store the interchange management amount limit. */
				stbSettings.fInterchangeMgmtLimit = fVal;
			}
		}
		else
		{
			/* Value not found in the configuration file */
			debug_sprintf(szDbgMsg, "%s: Interchange Management amt limit for STB/ASR not found",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Disabling STB logic",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Disable the STB logic enabled */
			stbSettings.stbLogicEnabled = PAAS_FALSE;
			break;
		}
		break;
	}
	//Returning always SUCCESS.
	rv = SUCCESS;

	debug_sprintf(szDbgMsg,"%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadPymntSettings
 *
 * Description	: This function is responsible for loading the configuration
 * 					parameters specific to payment business logic
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int loadPymntSettings()
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	int		iCnt			= 0;
	int		iTotTenderCnt	= 0;
	char	szTemp[30]		= "";
	char	szErrMsg[128]	= "";
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Check for cashback */
	if(SUCCESS != getCashBackSettings())
	{
		debug_sprintf(szDbgMsg, "%s: Error while loading cashback settings",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	/* Check for counter tip */
	if(SUCCESS != getCounterTipSettings())
	{
		debug_sprintf(szDbgMsg, "%s: Error while loading counter tip settings",
					   											__FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	iLen = sizeof(szTemp);
	memset(szTemp, 0x00, iLen);

	/* ---------- Check for partial auth ----------- */
	rv = getEnvFile(SECTION_PAYMENT, PARTAUTH_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Bool value accordingly */
		if( (*szTemp == 'y') || (*szTemp == 'Y') )
		{
			pymntSettings.partAuthEnabled = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Partial Auth ENABLED", __FUNCTION__);
		}
		else
		{
			pymntSettings.partAuthEnabled = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: Partial Auth DISABLED", __FUNCTION__);
		}
	}
	else
	{
		/* Value not found.. setting default */
		pymntSettings.partAuthEnabled = PAAS_FALSE;
		debug_sprintf(szDbgMsg, "%s: Partial Auth DISABLED (default action)",
																__FUNCTION__);
	}
	APP_TRACE(szDbgMsg);

	memset(szTemp, 0x00, iLen);
	/* ---------- Check for tip support ----------- */
	rv = getEnvFile(SECTION_PAYMENT, TIP_SUPP_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Bool value accordingly */
		if( (*szTemp == 'y') || (*szTemp == 'Y') )
		{
			pymntSettings.tipSuppEnabled = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Tip Support ENABLED", __FUNCTION__);
		}
		else
		{
			pymntSettings.tipSuppEnabled = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: Tip Support DISABLED", __FUNCTION__);
		}
	}
	else
	{
		/* Value not found.. setting default */
		pymntSettings.tipSuppEnabled = PAAS_FALSE;
		debug_sprintf(szDbgMsg, "%s: Tip Support DISABLED (default action)",
																__FUNCTION__);
	}
	APP_TRACE(szDbgMsg);

	memset(szTemp, 0x00, iLen);
	/* ---------- Check for split tender ----------- */
	rv = getEnvFile(SECTION_PAYMENT, SPLT_TNDR_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Bool value accordingly */
		if( (*szTemp == 'y') || (*szTemp == 'Y') )
		{
			pymntSettings.splitTenderEnabled = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Split Tender ENABLED", __FUNCTION__);
		}
		else
		{
			pymntSettings.splitTenderEnabled = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: Split Tender DISABLED", __FUNCTION__);
		}
	}
	else
	{
		/* Value not found.. setting default */
		pymntSettings.splitTenderEnabled = PAAS_FALSE;
		debug_sprintf(szDbgMsg, "%s: Split Tender DISABLED (default action)",
																__FUNCTION__);
	}
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szTemp);
	memset(szTemp, 0x00, iLen);

	/* --------------- Check for manual entry ---------------- */
	rv = getEnvFile(SECTION_PAYMENT, MANENTRY_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Bool value accordingly */
		if( (*szTemp == 'y') || (*szTemp == 'Y') )
		{
			pymntSettings.manEntryEnabled = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Manual Entry ENABLED", __FUNCTION__);
		}
		else
		{
			pymntSettings.manEntryEnabled = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: Manual Entry DISABLED", __FUNCTION__);
		}
	}
	else
	{
		/* Value not found.. setting default */
		pymntSettings.manEntryEnabled = PAAS_FALSE;
		debug_sprintf(szDbgMsg, "%s: Manual Entry DISABLED (default action)",
																__FUNCTION__);
	}
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szTemp);
	memset(szTemp, 0x00, iLen);

	/* ---------------- Check for loyalty capture -------------- */
	rv = getEnvFile(SECTION_PAYMENT, LYLTYCAP_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Bool value accordingly */
		if( (*szTemp == 'y') || (*szTemp == 'Y') )
		{
			pymntSettings.loyaltyCapEnabled = PAAS_TRUE;
			debug_sprintf(szDbgMsg,"%s: Loyalty Capture ENABLED",__FUNCTION__);
		}
		else
		{
			pymntSettings.loyaltyCapEnabled = PAAS_FALSE;
			debug_sprintf(szDbgMsg,"%s: Loyalty Capture DISABLED",__FUNCTION__);
		}
	}
	else
	{
		/* Value not found.. setting default */
		pymntSettings.loyaltyCapEnabled = PAAS_FALSE;
		debug_sprintf(szDbgMsg, "%s: Loyalty Capture DISABLED (default action)",
																__FUNCTION__);
	}
	APP_TRACE(szDbgMsg);

	memset(szTemp, 0x00, iLen);

	/* ---------------- Check for Gift Refund -------------- */
	rv = getEnvFile(SECTION_PAYMENT, GIFTREFUND_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Bool value accordingly */
		if( (*szTemp == 'y') || (*szTemp == 'Y') )
		{
			pymntSettings.giftRefundEnabled = PAAS_TRUE;
			debug_sprintf(szDbgMsg,"%s: Gift Refund ENABLED",__FUNCTION__);
		}
		else
		{
			pymntSettings.giftRefundEnabled = PAAS_FALSE;
			debug_sprintf(szDbgMsg,"%s: Gift Refund DISABLED",__FUNCTION__);
		}
	}
	else
	{
		/* Value not found.. setting default */
		pymntSettings.giftRefundEnabled = PAAS_TRUE;
		debug_sprintf(szDbgMsg, "%s: Gift Refund ENABLED (default action)",
																__FUNCTION__);
	}
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szTemp);
	memset(szTemp, 0x00, iLen);

	/* --------------- Check for email capture ---------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_PAYMENT, EMAILCAP_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Bool value accordingly */
		if( (*szTemp == 'y') || (*szTemp == 'Y') )
		{
			pymntSettings.emailCapEnabled = PAAS_TRUE;
			debug_sprintf(szDbgMsg,"%s: Email Capture ENABLED",__FUNCTION__);
		}
		else
		{
			pymntSettings.emailCapEnabled = PAAS_FALSE;
			debug_sprintf(szDbgMsg,"%s: Email Capture DISABLED",__FUNCTION__);
		}
	}
	else
	{
		/* Value not found.. setting default */
		pymntSettings.emailCapEnabled = PAAS_FALSE;
		debug_sprintf(szDbgMsg, "%s: Email Capture DISABLED (default action)",
																__FUNCTION__);
	}
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szTemp);
	memset(szTemp, 0x00, iLen);

	/* --------------- Check for contactless ---------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_PAYMENT, CNTCTLSS_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Bool value accordingly */
		if( (*szTemp == 'y') || (*szTemp == 'Y') )
		{
			pymntSettings.cntctLessEnabled = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Contactless Cad processing ENABLED",
																__FUNCTION__);
		}
		else
		{
			pymntSettings.cntctLessEnabled = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: Contactless Cad processing DISABLED",
																__FUNCTION__);
		}
	}
	else
	{
		/* Value not found.. setting default */
		pymntSettings.cntctLessEnabled = PAAS_FALSE;
		debug_sprintf(szDbgMsg,
					"%s: Contactless Cad processing DISABLED (default action)",
																__FUNCTION__);
	}
	APP_TRACE(szDbgMsg);


	/* ------------- Check for Enabled Tender Types ------------ */
	for(iCnt = 0; iCnt < MAX_TENDERS; iCnt++)
	{
		iLen = sizeof(szTemp);
		memset(szTemp, 0x00, iLen);

		switch(iCnt)
		{
		case TEND_CREDIT:
			debug_sprintf(szDbgMsg, "%s: Checking for CREDIT tender",
																__FUNCTION__);
			rv = getEnvFile(SECTION_TENDER, CREDIT_TYPE, szTemp, iLen);

			break;

		case TEND_DEBIT:
			debug_sprintf(szDbgMsg, "%s: Checking for DEBIT tender",
																__FUNCTION__);
			rv = getEnvFile(SECTION_TENDER, DEBIT_TYPE, szTemp, iLen);

			break;

		case TEND_GIFT:
			debug_sprintf(szDbgMsg, "%s: Checking for GIFT CARD tender",
																__FUNCTION__);
			rv = getEnvFile(SECTION_TENDER, GIFTCARD_TYPE, szTemp, iLen);

			break;

		case TEND_GOOGLE:
/*			debug_sprintf(szDbgMsg, "%s: Checking for GOOGLE WALLET tender",
																__FUNCTION__);
			rv = getEnvFile(SECTION_TENDER, GWALLET_TYPE, szTemp, iLen);*/

			break;

		case TEND_ISIS:
/*			debug_sprintf(szDbgMsg, "%s: Checking for ISIS WALLET tender",
																__FUNCTION__);
			rv = getEnvFile(SECTION_TENDER, IWALLET_TYPE, szTemp, iLen);*/

			break;

		case TEND_PRIVATE:
			debug_sprintf(szDbgMsg, "%s: Checking for Private tender",
																__FUNCTION__);
			rv = getEnvFile(SECTION_TENDER, PRIVATE_TYPE, szTemp, iLen);

			break;

		case TEND_PAYPAL:
			debug_sprintf(szDbgMsg, "%s: Checking for PAYPAL tender",
																__FUNCTION__);
			rv = getEnvFile(SECTION_TENDER, PAYPAL_TYPE, szTemp, iLen);

			break;

		case TEND_EBT:
			debug_sprintf(szDbgMsg, "%s: Checking for EBT tender",
																__FUNCTION__);
			rv = getEnvFile(SECTION_TENDER, EBT_TYPE, szTemp, iLen);

			break;

		case TEND_FSA:
			debug_sprintf(szDbgMsg, "%s: Checking for FSA tender",
																__FUNCTION__);
			rv = getEnvFile(SECTION_TENDER, FSA_TYPE, szTemp, iLen);

			break;

		case TEND_MERCHCREDIT:
			debug_sprintf(szDbgMsg, "%s: Checking for Merchandise Credit tender",
																__FUNCTION__);
			rv = getEnvFile(SECTION_TENDER, MERCHCREDIT_TYPE, szTemp, iLen);

			break;

		case TEND_BALANCE:
			debug_sprintf(szDbgMsg, "%s: Checking for PAY BY CASH tender",
																__FUNCTION__);
			rv = getEnvFile(SECTION_TENDER, CASH_TYPE, szTemp, iLen);

			break;

		case TEND_OTHER:

			break;

		default:
			/* Should never come here */
			debug_sprintf(szDbgMsg, "%s: Should never come here", __FUNCTION__);

			break;
		}

		APP_TRACE(szDbgMsg);

		if(rv > 0)
		{
			/* Value found ... Setting bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				pymntSettings.tenderList[iCnt]= PAAS_TRUE;
				debug_sprintf(szDbgMsg, "%s: tender is ENABLED", __FUNCTION__);

				iTotTenderCnt++;/*To keep track of the total enabled tenders*/
			}
			else
			{
				pymntSettings.tenderList[iCnt]= PAAS_FALSE;
				debug_sprintf(szDbgMsg, "%s: tender is DISABLED", __FUNCTION__);
			}
		}
		else
		{
			/* Value not found.. setting default */
			pymntSettings.tenderList[iCnt] = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: tender is DISABLED (default action)",
																__FUNCTION__);
		}

		APP_TRACE(szDbgMsg);
	}

	if (iTotTenderCnt > 6)
	{
		debug_sprintf(szErrMsg, "%s: Total Tender enabled count exceede the limit 6",
															__FUNCTION__);
		APP_TRACE(szErrMsg);

		syslog(LOG_ERR|LOG_USER, szErrMsg);

		return  FAILURE;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Total enabled tenders is %d", __FUNCTION__,
															iTotTenderCnt);
		APP_TRACE(szDbgMsg);
	}


	/* ------------- Check for Enabled Line Item Screen Options ------------ */
	for(iCnt = 0; iCnt < MAX_CONSUMER_OPTS; iCnt++)
	{
		iLen = sizeof(szTemp);
		memset(szTemp, 0x00, iLen);

		debug_sprintf(szDbgMsg, "%s: iCnt [%d]", __FUNCTION__, iCnt);
		APP_TRACE(szDbgMsg);

		switch(iCnt)
		{
		case CONSUMER_OPT1:
			debug_sprintf(szDbgMsg, "%s: Checking for Consumer option 1",
																__FUNCTION__);
			rv = getEnvFile(SECTION_CONSUMEROPTS, CONSUMER_OPTION1, szTemp, iLen);

			break;

		case CONSUMER_OPT2:
			debug_sprintf(szDbgMsg, "%s: Checking for Consumer option 2",
																__FUNCTION__);
			rv = getEnvFile(SECTION_CONSUMEROPTS, CONSUMER_OPTION2, szTemp, iLen);

			break;

		case CONSUMER_OPT3:
			debug_sprintf(szDbgMsg, "%s: Checking for Consumer option 3",
																__FUNCTION__);
			rv = getEnvFile(SECTION_CONSUMEROPTS, CONSUMER_OPTION3, szTemp, iLen);

			break;

		case CONSUMER_OPT4:
			debug_sprintf(szDbgMsg, "%s: Checking for Consumer option 4",
																__FUNCTION__);
			rv = getEnvFile(SECTION_CONSUMEROPTS, CONSUMER_OPTION4, szTemp, iLen);

			break;

		case CONSUMER_OPT5:
			debug_sprintf(szDbgMsg, "%s: Checking for Consumer option 5",
																__FUNCTION__);
			rv = getEnvFile(SECTION_CONSUMEROPTS, CONSUMER_OPTION5, szTemp, iLen);

			break;

		case CONSUMER_OPT6:
			debug_sprintf(szDbgMsg, "%s: Checking for Consumer option 6",
																__FUNCTION__);
			rv = getEnvFile(SECTION_CONSUMEROPTS, CONSUMER_OPTION6, szTemp, iLen);

			break;

		//default:
			/* Should never come here */
			//debug_sprintf(szDbgMsg, "%s: Should never come here", __FUNCTION__); T_RaghavendranR1 CID 67281 (#1 of 1): Dead default in switch (DEADCODE). Dead Default case. So commenting it.

			//break;
		}

		APP_TRACE(szDbgMsg);

		if(rv > 0)
		{
			if(strcasecmp(szTemp, "GiftReceipt") == SUCCESS)
			{
				pymntSettings.consumerOptList[iCnt][0] = GIFTRECEIPT_OPT;
				pymntSettings.consumerOptList[iCnt][1] = 0;
				debug_sprintf(szDbgMsg, "%s: Gift Receipt Option is ENABLED",__FUNCTION__);
			}
			else if(strcasecmp(szTemp, "EmailReceipt") == SUCCESS)
			{
				pymntSettings.consumerOptList[iCnt][0] = EMAILRECEIPT_OPT;
				pymntSettings.consumerOptList[iCnt][1] = 0;
				debug_sprintf(szDbgMsg, "%s: Email Receipt Option is ENABLED",__FUNCTION__);
			}
			else if(strcasecmp(szTemp, "EmailOffer") == SUCCESS)
			{
				pymntSettings.consumerOptList[iCnt][0] = EMAILOFFER_OPT;
				pymntSettings.consumerOptList[iCnt][1] = 0;
				debug_sprintf(szDbgMsg, "%s: Email Offer Option is ENABLED",__FUNCTION__);
			}
			else if(strcasecmp(szTemp, "PrivCard") == SUCCESS)
			{
				//Need to check whether PrivLabel is enabled
				if(isPrivLabelTenderEnabled() == PAAS_TRUE)
				{
					debug_sprintf(szDbgMsg, "%s: PRIVCARD_OPT is set PrivLabel tender is enabled!!!",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					pymntSettings.consumerOptList[iCnt][0] = PRIVCARD_OPT;
					pymntSettings.consumerOptList[iCnt][1] = 0;
					debug_sprintf(szDbgMsg, "%s: Private Card Option is ENABLED",__FUNCTION__);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: PRIVCARD_OPT is set but PrivLabel tender is not enabled!!!",__FUNCTION__);
					APP_TRACE(szDbgMsg);

					pymntSettings.consumerOptList[iCnt][0] = -1;
					pymntSettings.consumerOptList[iCnt][1] = 0;

					debug_sprintf(szDbgMsg, "%s: Not setting PRIVCARD_OPT!!!", __FUNCTION__);
				}
			}
			else
			{
				pymntSettings.consumerOptList[iCnt][0] = -1;
				pymntSettings.consumerOptList[iCnt][1] = 0;
				debug_sprintf(szDbgMsg, "%s: Option is DISABLED (invalid option value)",
																				__FUNCTION__);
			}
		}
		else
		{
			/* Value not found.. setting default */
			pymntSettings.consumerOptList[iCnt][0] = -1;
			pymntSettings.consumerOptList[iCnt][1] = 0;
			debug_sprintf(szDbgMsg, "%s: Option is DISABLED (default action)",
																__FUNCTION__);
		}
		APP_TRACE(szDbgMsg);
	}
	//Load SAF settings.
	if(SUCCESS != getSAFSettings())
	{
		debug_sprintf(szDbgMsg,"%s: Error while loading SAF settings", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	rv = SUCCESS;

	debug_sprintf(szDbgMsg,"%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCashBackSettings
 *
 * Description	: This function is responsible for checking if the cashback is
 * 					enabled and if yes, then for getting the settings for the
 * 					cash back.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getCashBackSettings()
{
	int				rv				= 0;
	int				iLen			= 0;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iVal			= 0;
	char			szTemp[10]		= "";
	char			szParam[20]		= "";
	CBACK_PTYPE		cbCfgPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szTemp);

	while(1)
	{
		/* Check if cashback is enabled in the configuration file */
		rv = getEnvFile(SECTION_PAYMENT, CASHBACK_ENABLED, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				pymntSettings.cashBackEnabled = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: CashBack is ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = SUCCESS;
			}
			else
			{
				pymntSettings.cashBackEnabled = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: CashBack is DISABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			pymntSettings.cashBackEnabled = PAAS_FALSE;

			debug_sprintf(szDbgMsg, "%s: CashBack is DISABLED (default action)",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}

		/* Check if EBT Cashback is enabled in the configuration file */
		rv = getEnvFile(SECTION_PAYMENT, EBTCASHBACK_ENABLED, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				pymntSettings.ebtCashBackEnabled = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: EBT CashBack is ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = SUCCESS;
			}
			else
			{
				pymntSettings.ebtCashBackEnabled = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: EBT CashBack is DISABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			pymntSettings.ebtCashBackEnabled = PAAS_FALSE;

			debug_sprintf(szDbgMsg, "%s: EBT CashBack is DISABLED (default action)",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}

		/* Cashback setting is set to enabled in the config file. */

		/* Get the configuration parameters for cash back */
		cbCfgPtr = (CBACK_PTYPE) malloc(sizeof(CBACK_STYPE));
		if(cbCfgPtr == NULL)
		{
			/* Failure to allocate memory for cash back settings */

			debug_sprintf(szDbgMsg, "%s: Memory allocation failure",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Memory allocated. Initialize the memory */
		memset(cbCfgPtr, 0x00, sizeof(CBACK_STYPE));

		/* Get the cashback amount limit from the config file */
		iLen = sizeof(szTemp);
		memset(szTemp, 0x00, sizeof(szTemp));

		rv = getEnvFile(SECTION_CBACK, CB_AMT_LIMIT, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found in the configuration file */
			iVal = atoi(szTemp);
			if(iVal <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: Incorrect cashback amt limit [%d]",
														__FUNCTION__, iVal);
				APP_TRACE(szDbgMsg);

				/* Deallocate the memory from the heap */
				free(cbCfgPtr);

				debug_sprintf(szDbgMsg, "%s: Disabling cashback setting",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* Disable the cashback setting */
				pymntSettings.cashBackEnabled = PAAS_FALSE;
				pymntSettings.ebtCashBackEnabled = PAAS_FALSE;

				rv = SUCCESS;
				break;
			}
			else
			{
				/* Store the cashback amount limit. */
				cbCfgPtr->maxCBAmt = iVal;
				rv = SUCCESS;
			}
		}
		else
		{
			/* Value not found in the configuration file */
			debug_sprintf(szDbgMsg, "%s: Cashback amt limit not found",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Deallocate the memory from the heap */
			free(cbCfgPtr);

			debug_sprintf(szDbgMsg, "%s: Disabling cashback setting",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Disable the cashback setting */
			pymntSettings.cashBackEnabled = PAAS_FALSE;
			pymntSettings.ebtCashBackEnabled = PAAS_FALSE;

			rv = SUCCESS;
			break;
		}

		/* Cashback amount limit has been read from the config file. Read the
		 * other cashback amount limits; validate them and store them */
		jCnt = 0;
		for(iCnt = 0; iCnt < MAX_CBACK_AMTS; iCnt++)
		{
			iVal	= 0;
			rv		= 0;
			iLen 	= sizeof(szTemp);
			memset(szTemp, 0x00, iLen);

			sprintf(szParam, "%s%d", CFG_CBAMT_PREFIX, iCnt + 1);
			rv = getEnvFile(SECTION_CBACK, szParam, szTemp, iLen);
			if(rv > 0)
			{
				/* Value found in the config file */
				iVal = atoi(szTemp);

				/* The preset cashback should be a positive integer value not
				 * greater than the cashback amount limit already set */
				if((iVal > 0) && (iVal <= cbCfgPtr->maxCBAmt))
				{
					debug_sprintf(szDbgMsg, "%s: [%d]-Cashback Amt = [%d]",
											__FUNCTION__, iCnt + 1, iVal);
					APP_TRACE(szDbgMsg);

					cbCfgPtr->cbAmt[jCnt] = iVal;	// MukeshS3: PTMX-1587
					jCnt++;
				}
				else
				{
					/* Incorrect Value */
					debug_sprintf(szDbgMsg,
						"%s: Ignoring incorrect cashback amt[%d] for preset %d",
													__FUNCTION__, iVal, iCnt);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				/* Value not found in the config file */
				debug_sprintf(szDbgMsg,
								"%s: Cashback amt not found for preset [%d]",
															__FUNCTION__, iCnt);
				APP_TRACE(szDbgMsg);
			}
		}

		/* Validate if the cashback amounts were correctly saved */
		rv = SUCCESS;
#if 0
		if(jCnt == 0)
		{
			/* Not even a single valid cashback preset amount was saved */
			debug_sprintf(szDbgMsg, "%s: No cashback preset amts defined",
								__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Deallocate the memory from the heap */
			free(cbCfgPtr);

			debug_sprintf(szDbgMsg, "%s: Disabling the cashback setting",
								__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Disable the cashback amount setting */
			pymntSettings.cashBackEnabled = PAAS_FALSE;
			pymntSettings.ebtCashBackEnabled = PAAS_FALSE;

			rv = SUCCESS;
			break;
		}
#endif
		/* Set the values */
		cbCfgPtr->iTotCnt = jCnt;
		pymntSettings.cbCfgPtr = cbCfgPtr;

		/* TRACE Of all the values */
		debug_sprintf(szDbgMsg, "%s: CashBack Amt Limit = [%d]", __FUNCTION__,
															cbCfgPtr->maxCBAmt);
		APP_TRACE(szDbgMsg);

		for(iCnt = 0; iCnt < jCnt; iCnt++)
		{
			debug_sprintf(szDbgMsg, "%s: CashBack Preset Amt-%d = [%d]",
								__FUNCTION__, iCnt + 1, cbCfgPtr->cbAmt[iCnt]);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCounterTipSettings
 *
 * Description	: This function is responsible for checking if the counter tip
 * 					is enabled and if yes, then for getting the settings for
 * 					the counter tip
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getCounterTipSettings()
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iLen			= 0;
	int				iVal			= 0;
	char			szTemp[10]		= "";
	char			szParam[20]		= "";
	CTRTIP_PTYPE	ctrTipCfgPtr	= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szTemp);

	while(1)
	{
		/* Check if counter tip is enabled in the configuration file */
		rv = getEnvFile(SECTION_PAYMENT, CTRTIP_ENABLED, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				debug_sprintf(szDbgMsg, "%s: Counter Tip is ENABLED",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				pymntSettings.ctrTipEnabled = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Counter Tip is DISABLED",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				pymntSettings.ctrTipEnabled = PAAS_FALSE;

				rv = SUCCESS;

				break;
			}
		}
		else
		{
			/* Value not found in the configuration file */
			debug_sprintf(szDbgMsg,
				"%s: Counter Tip is DISABLED (default action)", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pymntSettings.ctrTipEnabled = PAAS_FALSE;

			rv = SUCCESS;

			break;
		}

		/* Counter Tip is enabled, get all the preset counter tips */

		ctrTipCfgPtr = (CTRTIP_PTYPE) malloc(sizeof(CTRTIP_STYPE));
		if(ctrTipCfgPtr == NULL)
		{
			/* Failure to allocate memory for counter tip settings */
			debug_sprintf(szDbgMsg, "%s: Memory Allocation Failure",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize the memory area for the counter tips structure */
		memset(ctrTipCfgPtr, 0x00, sizeof(CTRTIP_STYPE));

		for(iCnt = 0; iCnt < MAX_CTRTIPS; iCnt++)
		{
			rv	= 0;
			iLen= sizeof(szTemp);
			memset(szTemp, 0x00, sizeof(szTemp));

			sprintf(szParam, "%s%d", CFG_CTRTIP_PREFIX, iCnt + 1);
			rv = getEnvFile(SECTION_CTRTIP, szParam, szTemp, iLen);
			if(rv > 0)
			{
				/* Value found in the configuration file */

				/* Counter tip is a %age value, it should be positive and
				 * should not be greater than 100 */
				iVal = atoi(szTemp);
				if((iVal > 0) && (iVal <= 100))
				{
					/* Correct Value */
					ctrTipCfgPtr->ctrTipPcnt[iCnt] = iVal;
				}
				else
				{
					/* Incorrect Value */
					debug_sprintf(szDbgMsg,
							"%s: Ignoring incorrect CounterTip preset_%d [%d]",
											__FUNCTION__, iCnt + 1, iVal);
					APP_TRACE(szDbgMsg);
				}
				jCnt++;
			}
			else
			{
				/* Value not found in the configuration file */
				debug_sprintf(szDbgMsg, "%s: Counter Tip preset [%d] not found",
														__FUNCTION__, iCnt + 1);
				APP_TRACE(szDbgMsg);
			}
		}

		/* Reading from the config file is done. Now validations are to be
		 * done */
		rv = SUCCESS;

		if(jCnt == 0)
		{
			debug_sprintf(szDbgMsg, "%s: No Counter Tip preset found",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Deallocate the memory in the heap */
			free(ctrTipCfgPtr);

/*			According to FRD 3.68 we need to ask tip if counter tip is enabled, even if preset tips are not available.
 *          before this requirement, we were disabling the counter tip if no preset values found.
 *
 *    	 	debug_sprintf(szDbgMsg, "%s: Disabling counter tips", __FUNCTION__);
 *			APP_TRACE(szDbgMsg);
 *
 *          Set the counter tips as disabled
 *
 *     	    pymntSettings.ctrTipEnabled = PAAS_FALSE;
*/
			break;
		}

		/* Set the values */
		ctrTipCfgPtr->iTotCnt = jCnt;
		pymntSettings.ctrTipCfgPtr = ctrTipCfgPtr;

		/* Print the log Trace */
		for(iCnt = 1; iCnt <= jCnt; iCnt++)
		{
			debug_sprintf(szDbgMsg, "%s: Counter Tip Preset-%d= %d",
						__FUNCTION__, iCnt, ctrTipCfgPtr->ctrTipPcnt[iCnt - 1]);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSAFSettings
 *
 * Description	: This function is responsible for checking if the SAF is
 * 					enabled and if yes, then for getting the settings for the
 * 					SAF.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getSAFSettings()
{
	int				rv				= 0;
	int				iLen			= 0;
	int				iVal			= 0;
	double			fVal			= 0.0;
	char			szTemp[11]		= "";
	SAF_PTR_TYPE	safCfgPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szTemp);

	while(1)
	{
		/* Check if SAF is enabled in the configuration file */
		rv = getEnvFile(SECTION_PAYMENT, SAF_ENABLED, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				pymntSettings.safEnabled = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: SAF is ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				pymntSettings.safEnabled = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: SAF is DISABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;

				break;
			}
		}
		else
		{
			pymntSettings.safEnabled = PAAS_FALSE;

			debug_sprintf(szDbgMsg, "%s: SAF is DISABLED (default action)",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;

			break;
		}

		/* SAF is enabled in the config file. */

		/* Get the configuration parameters for cash back */
		safCfgPtr = (SAF_PTR_TYPE) malloc(sizeof(SAF_STRUCT_TYPE));

		if(safCfgPtr == NULL)
		{
			/* Failure to allocate memory for saf settings */

			debug_sprintf(szDbgMsg, "%s: Memory allocation failure", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Memory allocated. Initialize the memory */
		memset(safCfgPtr, 0x00, sizeof(SAF_STRUCT_TYPE));

		/* Get the Transaction floor amount limit from the config file */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_TRAN_FLOOR_LIMIT, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found in the configuration file */
			fVal = atof(szTemp);
			if(fVal < 0)
			{
				debug_sprintf(szDbgMsg, "%s: Incorrect SAF transaction amount limit [%lf]", __FUNCTION__, fVal);
				APP_TRACE(szDbgMsg);
				/* Deallocate the memory from the heap */
				free(safCfgPtr);
				debug_sprintf(szDbgMsg, "%s: Disabling SAF setting", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				/* Disable the SAF setting */
				pymntSettings.safEnabled = PAAS_FALSE;
				rv = SUCCESS;
				break;
			}
			else
			{
				/* Store the SAF Transaction amount limit. */
				safCfgPtr->fTranFloorLimit = fVal;
				rv = SUCCESS;
			}
		}
		else
		{
			/* Value not found in the configuration file */
			debug_sprintf(szDbgMsg, "%s: SAF transaction amount limit not found", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* Deallocate the memory from the heap */
			free(safCfgPtr);
			debug_sprintf(szDbgMsg, "%s: Disabling SAF setting", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* Disable the SAF setting */
			pymntSettings.safEnabled = PAAS_FALSE;
			rv = SUCCESS;
			break;
		}

		/* ------------- Check for safemvonlinepintran Enabled ------------------- */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_SAF, SAF_EMV_ONLINE_PIN_TRANS, szTemp, iLen);
		if(rv > 0)
		{
			if( (*szTemp == 'n') || (*szTemp == 'N') )
			{
				safCfgPtr->bSafEMVOnlinePINTrans = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: SAF_EMV_ONLINE_PIN_TRANS is FALSE",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				safCfgPtr->bSafEMVOnlinePINTrans = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: AF_EMV_ONLINE_PIN_TRANS is TRUE",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			/* No value found for SAF_EMV_ONLINE_PIN_TRANS Enabled parameter */
			debug_sprintf(szDbgMsg,"%s: No value found for SAF_EMV_ONLINE_PIN_TRANS enabled; Set as TRUE", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			safCfgPtr->bSafEMVOnlinePINTrans = PAAS_TRUE;
		}

		if(isPrivLabelTenderEnabled()) // PRIV_LBL payment type is enabled.
		{
			/* Get the Private Card Offline floor amount limit from the config file */
			memset(szTemp, 0x00, sizeof(szTemp));
			rv = getEnvFile(SECTION_SAF, SAF_PRIV_LBL_TRAN_FLOOR_LIMIT, szTemp, iLen);
			if(rv > 0)
			{
				/* Value found in the configuration file */
				fVal = atof(szTemp);
				if(fVal < 0)
				{
					debug_sprintf(szDbgMsg, "%s: Incorrect SAF Tran Floor Limit [%lf] for PRIV_LBL payment type", __FUNCTION__, fVal);
					APP_TRACE(szDbgMsg);
					/* Deallocate the memory from the heap */
					free(safCfgPtr);
					debug_sprintf(szDbgMsg, "%s: Disabling SAF setting", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					/* Disable the SAF setting */
					pymntSettings.safEnabled = PAAS_FALSE;
					rv = SUCCESS;
					break;
				}
				else
				{
					/* Store the SAF total amount limit. */
					safCfgPtr->fPrvLblTranFlrLmt = fVal;
					rv = SUCCESS;
				}
			}
			else
			{
				/* Value not found in the configuration file */
				debug_sprintf(szDbgMsg, "%s: SAF tran floor limit not found for PRIV_LBL payment type", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				/* Deallocate the memory from the heap */
				free(safCfgPtr);
				debug_sprintf(szDbgMsg, "%s: Disabling SAF setting", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				/* Disable the SAF setting */
				pymntSettings.safEnabled = PAAS_FALSE;
				rv = SUCCESS;
				break;
			}
		}
		/* Get the Total floor amount limit from the config file */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_TOTAL_FLOOR_LIMIT, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found in the configuration file */
			fVal = atof(szTemp);
			if(fVal <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: Incorrect SAF Total amount limit [%lf]", __FUNCTION__, fVal);
				APP_TRACE(szDbgMsg);
				/* Deallocate the memory from the heap */
				free(safCfgPtr);
				debug_sprintf(szDbgMsg, "%s: Disabling SAF setting", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				/* Disable the SAF setting */
				pymntSettings.safEnabled = PAAS_FALSE;
				rv = SUCCESS;
				break;
			}
			else
			{
				/* Store the SAF total amount limit. */
				safCfgPtr->fTotalFloorLimit = fVal;
				rv = SUCCESS;
			}
		}
		else
		{
			/* Value not found in the configuration file */
			debug_sprintf(szDbgMsg, "%s: SAF Total amount limit not found", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* Deallocate the memory from the heap */
			free(safCfgPtr);
			debug_sprintf(szDbgMsg, "%s: Disabling SAF setting", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* Disable the SAF setting */
			pymntSettings.safEnabled = PAAS_FALSE;
			rv = SUCCESS;
			break;
		}

		/* Get the offline days limit from the config file */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_DAYS_LIMIT, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found in the configuration file */
			iVal = atoi(szTemp);
			if(iVal <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: Incorrect SAF offline days limit [%d]", __FUNCTION__, iVal);
				APP_TRACE(szDbgMsg);
				/* Deallocate the memory from the heap */
				free(safCfgPtr);
				debug_sprintf(szDbgMsg, "%s: Disabling SAF setting", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				/* Disable the SAF setting */
				pymntSettings.safEnabled = PAAS_FALSE;
				rv = SUCCESS;
				break;
			}
			else
			{
				/* Store the SAF total amount limit. */
				safCfgPtr->iOfflineDaysLimit = iVal;
				rv = SUCCESS;
			}
		}
		else
		{
			/* Value not found in the configuration file */
			debug_sprintf(szDbgMsg, "%s: SAF offline days limit not found", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* Deallocate the memory from the heap */
			free(safCfgPtr);
			debug_sprintf(szDbgMsg, "%s: Disabling SAF setting", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* Disable the SAF setting */
			pymntSettings.safEnabled = PAAS_FALSE;
			rv = SUCCESS;
			break;
		}

		/* Get SAF Ping Interval from the config file */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_PING_INTERVAL, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found in the configuration file */
			iVal = atoi(szTemp);
			if(iVal < 1 || iVal > 5)
			{
				debug_sprintf(szDbgMsg, "%s: Incorrect SAF Ping Interval is set, defaulting [%d]", __FUNCTION__, iVal);
				APP_TRACE(szDbgMsg);
				safCfgPtr->iSAFPingInterval = 2;
				rv = SUCCESS;
			}
			else
			{
				/* Store the SAF total amount limit. */
				safCfgPtr->iSAFPingInterval = iVal;
				rv = SUCCESS;
			}
		}
		else
		{
			/* Value not found in the configuration file */
			debug_sprintf(szDbgMsg, "%s: SAF Ping Interval not found, defaulting", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			safCfgPtr->iSAFPingInterval = 2;
			rv = SUCCESS;
		}

		/* Get SAF Purge Days from the config file */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_PURGE_DAYS, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found in the configuration file */
			iVal = atoi(szTemp);
			if(iVal < 1 || iVal > 20)
			{
				debug_sprintf(szDbgMsg, "%s: Incorrect SAF purge days[%d] set, defaulting[10]", __FUNCTION__, iVal);
				APP_TRACE(szDbgMsg);
				safCfgPtr->iSAFPurgeDays = 10;
				rv = SUCCESS;
			}
			else
			{
				/* Store the SAF total amount limit. */
				safCfgPtr->iSAFPurgeDays = iVal;
				rv = SUCCESS;
			}
		}
		else
		{
			/* Value not found in the configuration file */
			debug_sprintf(szDbgMsg, "%s: SAF Purge Days not found, defaulting[10]", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			safCfgPtr->iSAFPurgeDays = 10;
			rv = SUCCESS;
		}

		/* Get SAF Tran Post interval from the config file */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_TRAN_POST_INTERVAL, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found in the configuration file */
			iVal = atoi(szTemp);
			if(iVal < 1 )
			{
				debug_sprintf(szDbgMsg, "%s: Incorrect SAF Post interval[%d] set, defaulting[5Sec]", __FUNCTION__, iVal);
				APP_TRACE(szDbgMsg);

				safCfgPtr->iSAFPostInterval = 5;
				rv = SUCCESS;
			}
			else
			{
				/* Store the SAF Post Interval */
				safCfgPtr->iSAFPostInterval = iVal;
				rv = SUCCESS;
			}
		}
		else
		{
			/* Value not found in the configuration file */
			debug_sprintf(szDbgMsg, "%s: SAF Post Interval not found, defaulting[5Sec]", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			safCfgPtr->iSAFPostInterval = 5;
			rv = SUCCESS;
		}

		/* Get SAF Throttling Interval value from the config file */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_THROT_INTERVAL, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found in the configuration file */
			iVal = atoi(szTemp);
			if(iVal <= 0 )
			{
				debug_sprintf(szDbgMsg, "%s: Incorrect SAF Throttling interval[%d] set, defaulting[300sec]", __FUNCTION__, iVal);
				APP_TRACE(szDbgMsg);
				safCfgPtr->iSAFThrotInterval = 300;
				rv = SUCCESS;
			}
			else
			{
				/* Store the SAF Throttling Interval */
				safCfgPtr->iSAFThrotInterval = iVal;
				rv = SUCCESS;
			}
		}
		else
		{
			/* Value not found in the configuration file */
			debug_sprintf(szDbgMsg, "%s: SAF Throttling interval not found, defaulting[300sec]", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			safCfgPtr->iSAFThrotInterval = 300;
			rv = SUCCESS;
		}

		/* Check if SAF Throttling is enabled in the configuration file */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_THROTLING_ENABLED, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				safCfgPtr->bSAFThrottlingEnable = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: SAF Throttling is ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				safCfgPtr->bSAFThrottlingEnable = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: SAF Throttling is DISABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			safCfgPtr->bSAFThrottlingEnable = PAAS_FALSE;

			debug_sprintf(szDbgMsg, "%s: SAF Throttling is DISABLED (default action)",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}

		/* Check if SAF config variable is enabled to proc offline tran when online transaction are being processed */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_ALLOW_OFFLINE_TRAN_TOGOWITH_ONLINE, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				safCfgPtr->bProcSAFTranWithOnlineTran = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: SAF Allow Offline Tran to go with Online is ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				safCfgPtr->bProcSAFTranWithOnlineTran = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: SAF Allow Offline Tran to go with Online is DISABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			safCfgPtr->bProcSAFTranWithOnlineTran = PAAS_TRUE;

			debug_sprintf(szDbgMsg,"%s: SAF Allow Offline Tran to go with Online is ENABLED(Default Action)",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}

		/* Check FORCE FLAG for SAF transaction */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_FORCE_DUP_TRAN, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				safCfgPtr->bForceSAFDupTran = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: FORCE FLAG for SAF Tran is ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				safCfgPtr->bForceSAFDupTran = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: FORCE FLAG for SAF Tran is DISABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			safCfgPtr->bForceSAFDupTran = PAAS_TRUE;

			debug_sprintf(szDbgMsg,"%s:  FORCE FLAG for SAF Tran is ENABLED(Default Action)",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}

		/* Check Connection required for SAF transaction */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_CHK_CONN_REQD, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				safCfgPtr->bSAFChkConnReqd = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: Check Connection for SAF Tran is ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				safCfgPtr->bSAFChkConnReqd = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: Check Connection for SAF Tran is DISABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			safCfgPtr->bSAFChkConnReqd = PAAS_TRUE;

			debug_sprintf(szDbgMsg,"%s:  Check Connection for SAF Tran is ENABLED(Default Action)",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}

		/* Get the business date if present from the config file*/
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_BUSINESSDATE, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( strlen(szTemp) > 0 )
			{
				strcpy(safCfgPtr->szBusinessDate, szTemp);

				debug_sprintf(szDbgMsg,"%s: Found Business Date[%s] in config file",__FUNCTION__, safCfgPtr->szBusinessDate);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Business Date is not present",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: Business Date is not present",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}

		/* Check SAF Indicator required to include in SSI request */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_INDICATOR_REQUIRED, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				safCfgPtr->bSAFIndicatorReqd = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: SAF Indicator in SSI request is REQUIRED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				safCfgPtr->bSAFIndicatorReqd = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: SAF Indicator in SSI request is NOT-REQUIRED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			safCfgPtr->bSAFIndicatorReqd = PAAS_FALSE;

			debug_sprintf(szDbgMsg,"%s:  SAF Indicator in SSI request is NOT-REQUIRED(Default Action)",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}
#if 0
		/* Check SAF Indicator required to include in SSI request */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_INDICATOR_REQUIRED, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				safCfgPtr->bSAFIndicatorReqd = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: SAF Indicator in SSI request is REQUIRED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				safCfgPtr->bSAFIndicatorReqd = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: SAF Indicator in SSI request is NOT-REQUIRED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			safCfgPtr->bSAFIndicatorReqd = PAAS_FALSE;

			debug_sprintf(szDbgMsg,"%s:  SAF Indicator in SSI request is NOT-REQUIRED(Default Action)",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}
#endif
		/* Check SAF Refund to allow */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_ALLOW_REFUND_TRAN, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				safCfgPtr->bAllowSAFRefund = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: SAF Refund Tran is ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				safCfgPtr->bAllowSAFRefund = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: SAF Refund Tran is NOT ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			safCfgPtr->bAllowSAFRefund = PAAS_TRUE;

			debug_sprintf(szDbgMsg,"%s:  SAF Refund Tran is ENABLED(Default Action)",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}

		/* Check SAF Void to allow */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_ALLOW_VOID_TRAN, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				safCfgPtr->bAllowSAFVoid = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: SAF Void Tran is ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				safCfgPtr->bAllowSAFVoid = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: SAF Void Tran is NOT ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			safCfgPtr->bAllowSAFVoid = PAAS_TRUE;

			debug_sprintf(szDbgMsg,"%s:  SAF Void Tran is ENABLED(Default Action)",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}

		/* Check SAF Gift Activate to allow */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_ALLOW_GIFT_ACTIVATE, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				safCfgPtr->bAllowSAFGiftAtvt = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: SAF Gift Activate Tran is ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				safCfgPtr->bAllowSAFGiftAtvt = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: SAF Gift Activate Tran is NOT ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			safCfgPtr->bAllowSAFGiftAtvt = PAAS_TRUE;

			debug_sprintf(szDbgMsg,"%s:  SAF Gift Activate Tran is ENABLED(Default Action)",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}

		/* Config Parameter to check whether Transaction Floor Limit needs to be checked for REFUND Transactions */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_CHK_FLOOR_LIMIT_TO_SAF_REFUND, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				safCfgPtr->bChkFloorLimitForRefund = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: Transaction Floor Limit Check for SAF refund is ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				safCfgPtr->bChkFloorLimitForRefund = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: Transaction Floor Limit Check for SAF refund is NOT ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			safCfgPtr->bChkFloorLimitForRefund = PAAS_TRUE;

			debug_sprintf(szDbgMsg,"%s:  Transaction Floor Limit Check for SAF refund is ENABLED (Default)",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}

		/* Check SAF Pay Account to allow */
		memset(szTemp, 0x00, sizeof(szTemp));
		rv = getEnvFile(SECTION_SAF, SAF_ALLOW_PAYACCOUNT_TO_SAF, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				safCfgPtr->bAllowSAFPayAccount = PAAS_TRUE;

				debug_sprintf(szDbgMsg,"%s: SAF Pay Account Tran is ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				safCfgPtr->bAllowSAFPayAccount = PAAS_FALSE;

				debug_sprintf(szDbgMsg,"%s: SAF Pay Account Tran is NOT ENABLED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}
		else
		{
			safCfgPtr->bAllowSAFPayAccount = PAAS_FALSE;

			debug_sprintf(szDbgMsg,"%s:  SAF Pay Account Tran is NOT ENABLED(Default Action)",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}

		pymntSettings.safCfgPtr = safCfgPtr; //Setting the SAF pointer

		debug_sprintf(szDbgMsg, "%s: SAF Enabled [%d], SAF Transaction Floor Limit [%lf], SAF Total Floor Limit [%lf]",
						__FUNCTION__, pymntSettings.safEnabled, pymntSettings.safCfgPtr->fTranFloorLimit, pymntSettings.safCfgPtr->fTotalFloorLimit);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Private Card Offline Tran Floor Limit [%lf]", __FUNCTION__, pymntSettings.safCfgPtr->fPrvLblTranFlrLmt);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: SAF Offline Days Limit [%d], SAF Ping Interval [%d], SAF Purge Days[%d]",
						__FUNCTION__, pymntSettings.safCfgPtr->iOfflineDaysLimit, pymntSettings.safCfgPtr->iSAFPingInterval, pymntSettings.safCfgPtr->iSAFPurgeDays);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isLoyaltyCapEnabled
 *
 * Description	: This function informs the caller if the loyalty capture is
 * 					enabled or not for PaaS.
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isLoyaltyCapEnabled()
{
	return pymntSettings.loyaltyCapEnabled;
}

/*
 * ============================================================================
 * Function Name: isEmailCapEnabled
 *
 * Description	: This function informs the caller if the email capture is
 * 					enabled or not for PaaS.
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isEmailCapEnabled()
{
	return pymntSettings.emailCapEnabled;
}

PAAS_BOOL isPartAuthEnabled()
{
	return pymntSettings.partAuthEnabled;
}

/*
 * ============================================================================
 * Function Name: isSplitTenderEnabled
 *
 * Description	: This function informs the caller if the split tender is
 * 					enabled for PaaS transactions.
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isSplitTenderEnabled()
{
	return pymntSettings.splitTenderEnabled;
}

/*
 * ============================================================================
 * Function Name: isCtrTipEnabled
 *
 * Description	: This function informs the caller if the counter tip is
 * 					enabled for PaaS
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isCtrTipEnabled()
{
	PAAS_BOOL	rv				= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	if(pymntSettings.tipSuppEnabled == PAAS_TRUE)
	{
		rv = pymntSettings.ctrTipEnabled;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Tip Support is disabled", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = PAAS_FALSE;
	}

	return rv;
}


/*
 * ============================================================================
 * Function Name: isCashBackEnabled
 *
 * Description	: This function informs the caller if the cash back is enabled
 * 					for PaaS
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isCashBackEnabled()
{
	return pymntSettings.cashBackEnabled;
}

/*
 * ============================================================================
 * Function Name: isEBTCashBackEnabled
 *
 * Description	: This function informs the caller if the cash back is enabled
 * 					for PaaS
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isEBTCashBackEnabled()
{
	return pymntSettings.ebtCashBackEnabled;
}

/*
 * ============================================================================
 * Function Name: isGiftRefundEnabled
 *
 * Description	: This function informs the caller if the gift refund is enabled
 * 					for PaaS
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isGiftRefundEnabled()
{
	return pymntSettings.giftRefundEnabled;
}

/*
 * ============================================================================
 * Function Name: isUnsolMsgDuringPymtTranEnabled
 *
 * Description	: This function informs the caller if the Unsolicitated response
 * need to be sent during the payment transaction
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isUnsolMsgDuringPymtTranEnabled()
{
	return bLogicSettings.sendUnsolMsgDuringPymtTran;
}

/*
 * ============================================================================
 * Function Name: isVSPEparamsEON
 *
 * Description	: This function informs the caller if the Eparams are ON or Not
 * 					 in device
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isVSPEparamsON()
{
	int 			 rv				= SUCCESS;
	int				 iLen			= 0;
	static PAAS_BOOL bFirsttime 	= PAAS_TRUE;
	char			 szTemp[128]	= "";

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif
	if(bFirsttime)
	{
		iLen = sizeof(szTemp);
		/* ------------- Check if Eparams ON Device ------- */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_REG, "vspeparmstatus", szTemp, iLen);
		if(rv > 0)
		{
			if(strcasecmp(szTemp, "ON") == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Eparams ON in Device",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				bLogicSettings.eparamsON = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Eparams OFF in Device",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				bLogicSettings.eparamsON = PAAS_FALSE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Eparams status Not Present, Taking as OFF in Device",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			bLogicSettings.eparamsON = PAAS_FALSE;
		}
		bFirsttime = PAAS_FALSE;
	}
	debug_sprintf(szDbgMsg, "%s: Eparams Status: %d", __FUNCTION__, bLogicSettings.eparamsON);
	APP_TRACE(szDbgMsg);
	return bLogicSettings.eparamsON;
}
/*
 * ============================================================================
 * Function Name: isSAFEnabled
 *
 * Description	: This function informs the caller if the SAF is enabled
 * 					for PaaS
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isSAFEnabled()
{
	return pymntSettings.safEnabled;
}

/*
 * ============================================================================
 * Function Name: getSAFTranFloorAmount
 *
 * Description	: This function gives the config variable value
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
double getSAFTranFloorAmount()
{
	if(pymntSettings.safCfgPtr != NULL)
	{
		return pymntSettings.safCfgPtr->fTranFloorLimit;
	}
	else
	{
		return 0.00;
	}
}

/*
 * ============================================================================
 * Function Name: getSAFTotalFloorAmount
 *
 * Description	: This function gives the config variable value
 *
 * Input Params	: none
 *
 * Output Params: Amount
 * ============================================================================
 */
double getSAFTotalFloorAmount()
{
	if(pymntSettings.safCfgPtr != NULL)
	{
		return pymntSettings.safCfgPtr->fTotalFloorLimit;
	}
	else
	{
		return 0.00;
	}
}

/*
 * ============================================================================
 * Function Name: getSAFTranFloorAmountForPrivLabel
 *
 * Description	: This function gives the config variable value
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
double getSAFTranFloorAmountForPrivLabel()
{
	if(pymntSettings.safCfgPtr != NULL)
	{
		return pymntSettings.safCfgPtr->fPrvLblTranFlrLmt;
	}
	else
	{
		return 0.00;
	}
}

/*
 * ============================================================================
 * Function Name: getSAFOfflineDaysLimit
 *
 * Description	: This function gives the config variable value
 *
 * Input Params	: none
 *
 * Output Params: Value
 * ============================================================================
 */
int getSAFOfflineDaysLimit()
{
	if(pymntSettings.safCfgPtr != NULL)
	{
		return pymntSettings.safCfgPtr->iOfflineDaysLimit;
	}
	else
	{
		return 0;
	}
}

/*
 * ============================================================================
 * Function Name: getSAFPingInterval
 *
 * Description	: This function gives the config variable value of SAF Ping Interval
 *
 * Input Params	: none
 *
 * Output Params: Value
 * ============================================================================
 */
int getSAFPingInterval()
{
	if(pymntSettings.safCfgPtr != NULL)
	{
		return pymntSettings.safCfgPtr->iSAFPingInterval;
	}
	else
	{
		return 0;
	}
}

/*
 * ============================================================================
 * Function Name: getSAFPurgeDays
 *
 * Description	: This function gives the config variable value of SAF Purge Days
 *
 * Input Params	: none
 *
 * Output Params: Value
 * ============================================================================
 */
int getSAFPurgeDays()
{
	if(pymntSettings.safCfgPtr != NULL)
	{
		return pymntSettings.safCfgPtr->iSAFPurgeDays;
	}
	else
	{
		return 0;
	}
}

/*
 * ============================================================================
 * Function Name: getSAFPostInterval
 *
 * Description	: This function gives the config variable value of SAF Post Interval
 *
 * Input Params	: none
 *
 * Output Params: Value
 * ============================================================================
 */
int getSAFPostInterval()
{
	if(pymntSettings.safCfgPtr != NULL)
	{
		return pymntSettings.safCfgPtr->iSAFPostInterval;
	}
	else
	{
		return 0;
	}
}

/*
 * ============================================================================
 * Function Name: getSAFThrottlingInterval
 *
 * Description	: This function gives the config variable value of SAF Throttling Interval
 *
 * Input Params	: none
 *
 * Output Params: Value
 * ============================================================================
 */
int getSAFThrottlingInterval()
{
	if(pymntSettings.safCfgPtr != NULL)
	{
		return pymntSettings.safCfgPtr->iSAFThrotInterval;
	}
	else
	{
		return 0;
	}
}

/*
 * ============================================================================
 * Function Name: isSAFThrottlingEnabled
 *
 * Description	: This function gives the config variable value of SAF Throttling enabled or disabled
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */

PAAS_BOOL isSAFThrottlingEnabled()
{
	if(pymntSettings.safCfgPtr != NULL)
	{
		return pymntSettings.safCfgPtr->bSAFThrottlingEnable;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/*
 * ============================================================================
 * Function Name: bProcSAFTranWithOnlineTran
 *
 * Description	: This function gives the config variable value of SAF to indicate whether Offline tran can be processed or not
 * 				  when online tran are being processed.
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */

PAAS_BOOL bProcSAFTranWithOnlineTran()
{
	if(pymntSettings.safCfgPtr != NULL)
	{
		return pymntSettings.safCfgPtr->bProcSAFTranWithOnlineTran;
	}
	else
	{
		return PAAS_TRUE;
	}
}

/*
 * ============================================================================
 * Function Name: setBusinessDate
 *
 * Description	: This API sets the business date in SAF structure member and also in config.usr1 variable.
 *
 * Input Params	:
 *
 * Output Params: Amount/FAILURE
 * ============================================================================
 */
void setBusinessDate(char *pszBusinessDate)
{
	SAF_PTR_TYPE	safCfgPtr		= NULL;
	static 	int		bFirstTime		= 1;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Setting Business Date to [%s]", __FUNCTION__, pszBusinessDate);
	APP_TRACE(szDbgMsg);

	if( strlen(pszBusinessDate) > 0 )
	{
		if(bFirstTime)
		{
			if(!isSAFEnabled()) //If SAF is not enabled then need to create memory for SAF config ptr
			{
				debug_sprintf(szDbgMsg, "%s: SAF is not enabled, so need to create SAF Config ptr", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				safCfgPtr = (SAF_PTR_TYPE) malloc(sizeof(SAF_STRUCT_TYPE));

				if(safCfgPtr == NULL)
				{
					/* Failure to allocate memory for saf settings */
					debug_sprintf(szDbgMsg, "%s: Memory allocation failure", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					//TODO what to do here??
				}
				else
				{
					/* Memory allocated. Initialize the memory */
					memset(safCfgPtr, 0x00, sizeof(SAF_STRUCT_TYPE));
					pymntSettings.safCfgPtr = safCfgPtr; //Setting the SAF pointer
				}
			}
			bFirstTime = 0;
		}
		memset(pymntSettings.safCfgPtr->szBusinessDate, 0x00, sizeof(pymntSettings.safCfgPtr->szBusinessDate) );
		memcpy(pymntSettings.safCfgPtr->szBusinessDate, pszBusinessDate, strlen(pszBusinessDate));

		//Also store into the config.usr1 file in order to retain the business date even after application restart/reboot.
		putEnvFile(SECTION_SAF, SAF_BUSINESSDATE, pszBusinessDate);
	}
}

/*
 * ============================================================================
 * Function Name: getBusinessDate
 *
 * Description	: This API returns the business date present in the SAF structure.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void getBusinessDate(char *pszBusinessDate)
{
	if(pymntSettings.safCfgPtr != NULL)
	{
		memcpy(pszBusinessDate, pymntSettings.safCfgPtr->szBusinessDate, strlen(pymntSettings.safCfgPtr->szBusinessDate));
	}
}

/*
 * ============================================================================
 * Function Name: isForceFlgSetForSAFTran
 *
 * Description	: This API returns the force flag for SAF Transaction.
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isForceFlgSetForSAFTran()
{
	if(pymntSettings.safCfgPtr != NULL)
	{
		return pymntSettings.safCfgPtr->bForceSAFDupTran;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/*
 * ============================================================================
 * Function Name: isCheckConnReqdForSAFTran
 *
 * Description	: This API returns the check connection required flag
 * 				  for SAF Transaction.
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isCheckConnReqdForSAFTran()
{
	if(pymntSettings.safCfgPtr != NULL)
	{
		return pymntSettings.safCfgPtr->bSAFChkConnReqd;
	}
	else
	{
		return PAAS_TRUE;
	}
}

/*
 * ============================================================================
 * Function Name: isSAFIndReqdForSSIReq
 *
 * Description	: This API returns the check connection required flag
 * 				  for SAF Transaction.
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isSAFIndReqdForSSIReq()
{
	if(pymntSettings.safEnabled)
	{
		return pymntSettings.safCfgPtr->bSAFIndicatorReqd;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/*
 * ============================================================================
 * Function Name: isSAFAllowedForRefundTran
 *
 * Description	: This API returns the SAF is allowed for Refund Transaction
 *
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isSAFAllowedForRefundTran()
{
	if(pymntSettings.safEnabled)
	{
		return pymntSettings.safCfgPtr->bAllowSAFRefund;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/*
 * ============================================================================
 * Function Name: isSafAllowedForEmvOnlinePIN
 *
 * Description	: This API returns the SAF is allowed for EMV Online PIN tran
 *
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isSafAllowedForEmvOnlinePIN()
{
	if(pymntSettings.safEnabled)
	{
		return pymntSettings.safCfgPtr->bSafEMVOnlinePINTrans;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/* ============================================================================
* Function Name: isSAFAllowedForVoidTran
*
* Description	: This API returns the SAF is allowed for Void Transaction
*
*
* Input Params	:
*
* Output Params: PAAS_TRUE/PAAS_FALSE
* ============================================================================
*/
PAAS_BOOL isSAFAllowedForVoidTran()
{
	if(pymntSettings.safEnabled)
	{
		return pymntSettings.safCfgPtr->bAllowSAFVoid;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/* ============================================================================
* Function Name: isSAFAllowedForGiftActivate
*
* Description	: This API returns the whether SAF is allowed for Gift Card Activate Transaction
*
*
* Input Params	:
*
* Output Params: PAAS_TRUE/PAAS_FALSE
* ============================================================================
*/
PAAS_BOOL isSAFAllowedForGiftActivate()
{
	if(pymntSettings.safEnabled)
	{
		return pymntSettings.safCfgPtr->bAllowSAFGiftAtvt;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/* ============================================================================
* Function Name: isTransFloorLimitChkAllowedForSAFRefund
*
* Description	: This API returns the whether Transaction Floor Limit Check is Allowed for SAF Refund
*
*
* Input Params	:
*
* Output Params: PAAS_TRUE/PAAS_FALSE
* ============================================================================
*/
PAAS_BOOL isFloorLimitChkAllowedForSAFRefund()
{
	if(pymntSettings.safEnabled)
	{
		return pymntSettings.safCfgPtr->bChkFloorLimitForRefund;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/* ============================================================================
* Function Name: isSAFAllowedForPayAccount
*
* Description	: This API returns the whether SAF is allowed for Pay Account Transaction
*
*
* Input Params	:
*
* Output Params: PAAS_TRUE/PAAS_FALSE
* ============================================================================
*/
PAAS_BOOL isSAFAllowedForPayAccount()
{
	if(pymntSettings.safEnabled)
	{
		return pymntSettings.safCfgPtr->bAllowSAFPayAccount;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/* ============================================================================
* Function Name: isPrivLabelTenderEnabled
*
* Description	: This API returns the whether the Private Label Tender is Enabled in the Setting/Config
*
*
* Input Params	:
*
* Output Params: PAAS_TRUE/PAAS_FALSE
* ============================================================================
*/
PAAS_BOOL isPrivLabelTenderEnabled()
{
	PAAS_BOOL brv = PAAS_FALSE;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	brv =  pymntSettings.tenderList[TEND_PRIVATE];

	debug_sprintf(szDbgMsg, "%s: PrivLabel Tender Enabled [%s]", __FUNCTION__,
							 					(brv == PAAS_TRUE) ? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	return brv;
}

/*
 * ============================================================================
 * Function Name: getTendersListFromSettings
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
int getTendersListFromSettings(int * tendersList)
{

	int		iCnt	= 0;
	int		rv 		= SUCCESS;

	/* Setting enabled tenders in the list */
	for(iCnt = 0; iCnt < MAX_TENDERS; iCnt++)
	{
		/* Initialization of the list */
		tendersList[iCnt] = -1;
		if(pymntSettings.tenderList[iCnt] == PAAS_TRUE)
		{
			tendersList[iCnt] = PAAS_TRUE;
		}
	}

	return rv;
}
/*
 * ============================================================================
 * Function Name: isPaypalTenderEnabled
 *
 * Description	:returns TRUE if Paypal is configured on terminal
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
PAAS_BOOL isPaypalTenderEnabled()
{
	PAAS_BOOL brv = PAAS_FALSE;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	brv =  pymntSettings.tenderList[TEND_PAYPAL];

	debug_sprintf(szDbgMsg, "%s: Paypal Tender Enabled [%s]", __FUNCTION__,
							 					(brv == PAAS_TRUE) ? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	return brv;
}

/*
 * ============================================================================
 * Function Name: isEBTTenderEnabled
 *
 * Description	:
 *
 * Input Params	: index to set
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
PAAS_BOOL isEBTTenderEnabled()
{
	PAAS_BOOL brv = PAAS_FALSE;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	brv =  pymntSettings.tenderList[TEND_EBT];

	debug_sprintf(szDbgMsg, "%s: EBT Tender Enabled [%s]", __FUNCTION__,
							 					(brv == PAAS_TRUE) ? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	return brv;
}
/*
 * ============================================================================
 * Function Name: isPayBalanceTenderEnabled
 *
 * Description	: Returns PAAS_TRUE if pay by cash is enabled on terminal
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isPayBalanceTenderEnabled()
{
	PAAS_BOOL brv = PAAS_FALSE;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	brv =  pymntSettings.tenderList[TEND_BALANCE];

	debug_sprintf(szDbgMsg, "%s:Pay by Balance Tender Enabled [%s]", __FUNCTION__,
							 					(brv == PAAS_TRUE) ? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	return brv;
}
/*

 * ============================================================================
 * Function Name: isFSATenderEnabled
 *
 * Description	:
 *
 * Input Params	: index to set
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
PAAS_BOOL isFSATenderEnabled()
{
	PAAS_BOOL brv = PAAS_FALSE;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	brv =  pymntSettings.tenderList[TEND_FSA];

	debug_sprintf(szDbgMsg, "%s: FSA Tender Enabled [%s]", __FUNCTION__,
							 					(brv == PAAS_TRUE) ? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	return brv;
}

/*
 * ============================================================================
 * Function Name: resetConsOptSelection
 *
 * Description	:
 *
 * Input Params	: index to set
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int resetConsOptSelection()
{
	int		rv		= SUCCESS;
	int		iCnt	= 0;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Resetting all consumer selection options", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Setting enabled consumer option in the list */
	for(iCnt = 0; iCnt < MAX_CONSUMER_OPTS; iCnt++)
	{
		pymntSettings.consumerOptList[iCnt][1] = 0;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: setConsOptSelected
 *
 * Description	:
 *
 * Input Params	: index to set
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int setConsOptSelected(int iIndex, int iValue)
{
	int		rv		= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	if(iIndex >= MAX_CONSUMER_OPTS)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Index[%d] to set", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	pymntSettings.consumerOptList[iIndex][1] = iValue;

	return rv;
}

/*
 * ============================================================================
 * Function Name: getConsOptions
 *
 * Description	:
 *
 * Input Params	: Option list to be filled
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getConsOptions(int consOptList[][2])
{
	int		rv		= SUCCESS;
	int		iCnt	= 0;
#ifdef DEBUG
	//char	szDbgMsg[128]	= "";
#endif
	/* Initialization of the list */
	for(iCnt = 0; iCnt < MAX_CONSUMER_OPTS; iCnt++)
	{
		consOptList[iCnt][0] = -1;
	}
	for(iCnt = 0; iCnt < MAX_CONSUMER_OPTS; iCnt++)
	{
		consOptList[iCnt][1] = 0;
	}

	/* Setting enabled consumer option in the list */
	for(iCnt = 0; iCnt < MAX_CONSUMER_OPTS; iCnt++)
	{
		consOptList[iCnt][0] = pymntSettings.consumerOptList[iCnt][0];

		//debug_sprintf(szDbgMsg, "%s: consOptList[%d][0] = %d", __FUNCTION__, iCnt, consOptList[iCnt][0]);
		//APP_TRACE(szDbgMsg);
	}

	/* Setting enabled consumer option in the list */
	for(iCnt = 0; iCnt < MAX_CONSUMER_OPTS; iCnt++)
	{
		consOptList[iCnt][1] = pymntSettings.consumerOptList[iCnt][1];

		//debug_sprintf(szDbgMsg, "%s: consOptList[%d][1] = %d", __FUNCTION__, iCnt, consOptList[iCnt][1]);
		//APP_TRACE(szDbgMsg);
	}

	return rv;
}
/*
 * ============================================================================
 * Function Name: isConsumerOptsEnabled
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isConsumerOptsEnabled()
{
	PAAS_BOOL		brv		= PAAS_FALSE;
	int				iCnt	= 0;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	for(iCnt = 0; iCnt < MAX_CONSUMER_OPTS; iCnt++)
	{
		if(pymntSettings.consumerOptList[iCnt][0] != -1)
		{
			brv = PAAS_TRUE;
			break;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__,
						 					(brv == PAAS_TRUE) ? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	return brv;
}


/*
 * ============================================================================
 * Function Name: getPresetCtrTipsInfo
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
int getPresetCtrTipsInfo(CTRTIP_PTYPE pstCtrTips)
{
	int		rv				= SUCCESS;
	int		iSize			= 0;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	if(pstCtrTips == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL parameter passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else if(pymntSettings.ctrTipCfgPtr == NULL)
	{
		debug_sprintf(szDbgMsg,"%s: empty Counter tip cfg params",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		iSize = sizeof(CTRTIP_STYPE);

		memset(pstCtrTips, 0x00, iSize);
		memcpy(pstCtrTips, pymntSettings.ctrTipCfgPtr, iSize);
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPresetCbackAmtsInfo
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getPresetCbackAmtsInfo(CBACK_PTYPE	pstCBAmts)
{
	int		rv				= SUCCESS;
	int		iSize			= 0;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	if(pstCBAmts == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL parameter passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else if(pymntSettings.cbCfgPtr == NULL)
	{
		debug_sprintf(szDbgMsg,"%s: empty Cashbck amt cfg params",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		iSize = sizeof(CBACK_STYPE);

		memset(pstCBAmts, 0x00, iSize);
		memcpy(pstCBAmts, pymntSettings.cbCfgPtr, iSize);
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDevAdminRqdStatus
 *
 * Description	: 0 = Do Nothing, 1 =Do dev admn using existing device key, 2= Fresh dev Admin required.
 *
 * Input Params	: none
 *
 * Output Params: 0 / 1 / 2
 * ============================================================================
 */
int getDevAdminRqdStatus()
{
	int			rv				= 0;
	int			iLen			= 0;
	char		szVal[10]		= "";
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szVal);

	rv = getEnvFile(SECTION_DEVICE, "devadminreqd", szVal, iLen);
	if(rv < 0)
	{
		rv = 0;	//Do Nothing
	}
	else
	{
		switch(szVal[0])
		{
		case '1':
		case 'y':
		case 'Y':
			rv = 1;
			break;

		case '0':
		case 'n':
		case 'N':
			rv = 0;
			break;

		case '2':
			rv = 2;
			break;

		default:
			rv = 0;	//Do nothing
			break;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: setDevAdminRqd
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int setDevAdminRqd(PAAS_BOOL status)
{
	int		rv			= SUCCESS;
	char	strVal[2]	= "";

	switch(status)
	{
	case PAAS_TRUE:
		*strVal = 'Y';
		break;

	case PAAS_FALSE:
		*strVal = 'N';
		break;
	}

	rv = putEnvFile(SECTION_DEVICE, "devadminreqd", strVal);
	if(rv < 0)
	{
		rv = FAILURE;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: isDevEmvSetupRqd
 *
 * Description	: Reads from the config.usr1 if EMV Setup reqd
 * 				  0 - Setup not reqd
 * 				  1 - Setup reqd without connecting to host
 * 				  2 - Setup reqd with communication with host
 *
 * Input Params	: none
 *
 * Output Params: Int
 * ============================================================================
 */
int isDevEmvSetupRqd()
{
	int			rv				= 0;
	int			iLen			= 0;
	char		szVal[10]		= "";
	int			iEmvSetup		= 0;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szVal);

	rv = getEnvFile(SECTION_DEVICE, EMV_SETUP_REQD, szVal, iLen);
	if(rv < 0)
	{
		iEmvSetup = EMV_SETUP_NOT_REQ;
	}
	else
	{
		switch(szVal[0])
		{
		case '0':
			iEmvSetup = EMV_SETUP_NOT_REQ;
			break;

		case '1':
			iEmvSetup = EMV_SETUP_REQ_WITHOUT_HOST;
			break;

		case '2':
			iEmvSetup = EMV_SETUP_REQ_WITH_HOST;
			break;
		default:
			iEmvSetup = EMV_SETUP_NOT_REQ;
			break;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iEmvSetup);
	APP_TRACE(szDbgMsg);

	return iEmvSetup;
}

/*
 * ============================================================================
 * Function Name: setDevEmvSetupRqd
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int setDevEmvSetupRqd(int status)
{
	int		rv			= SUCCESS;
	char	strVal[2]	= "";

	switch(status)
	{
	case EMV_SETUP_NOT_REQ:
		*strVal = '0';
		break;

	case EMV_SETUP_REQ_WITHOUT_HOST:
		*strVal = '1';
		break;

	case EMV_SETUP_REQ_WITH_HOST:
		*strVal = '2';
		break;

	default:
		*strVal = '0';
		break;
	}
	rv = putEnvFile(SECTION_DEVICE, "emvsetupreqd", strVal);
	if(rv < 0)
	{
		rv = FAILURE;
	}

	return rv;
}

/*
 * ============================================================================
 * ------------------------- APIs for Startup Preamble ------------------------
 * ============================================================================
 */

/*
 * ============================================================================
 * Function Name: isPreambleSuccessful
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isPreambleSuccessful()
{
	int			rv			= 0;
	int			iLen		= 0;
	char		tmpStr[10]	= "";
	PAAS_BOOL	retVal		= PAAS_TRUE;

	iLen = sizeof(tmpStr);

	rv = getEnvFile(SECTION_REG, PREAMBLE_SUCCESS, tmpStr, iLen);
	if(rv > 0)
	{
		if(*tmpStr == '1')
		{
			/* True case scenario */
			retVal = PAAS_TRUE;
		}
		else
		{
			/* False case scenario */
			retVal = PAAS_FALSE;
		}
	}
	else
	{
		/* Cant find the config variable in the config.usr1 */
		/* False case scenario */
		retVal = PAAS_FALSE;
	}

	return retVal;
}

/*
 * ============================================================================
 * Function Name: isPreambleSuccessful
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
int setPreambleStatus(PAAS_BOOL status)
{
	int		rv			= SUCCESS;
	char	strVal[2]	= "";

	switch(status)
	{
	case PAAS_TRUE:
		*strVal = '1';
		break;

	case PAAS_FALSE:
		*strVal = '0';
		break;
	}

	rv = putEnvFile(SECTION_REG, PREAMBLE_SUCCESS, strVal);
	if(rv < 0)
	{
		rv = FAILURE;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: setVSPActivatedStatus
 *
 * Description	: This API would be called only in device preamble part to set
 * 					vsp is activated.
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
int setVSPActivatedStatus(PAAS_BOOL status)
{
	int		rv			= SUCCESS;
	char	strVal[2]	= "";

	switch(status)
	{
	case PAAS_TRUE:
		*strVal = '1';
		break;

	case PAAS_FALSE:
		*strVal = '0';
		break;
	}

	rv = putEnvFile(SECTION_REG, VSP_ACTIVATED, strVal);
	if(rv < 0)
	{
		rv = FAILURE;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: setRedoVSPAfterAdvanceDDK
 *
 * Description	: This API would be used to create thread for VSP Reg in background
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
int setRedoVSPAfterAdvanceDDK(PAAS_BOOL status)
{
	int		rv			= SUCCESS;
	char	strVal[2]	= "";

	switch(status)
	{
	case PAAS_TRUE:
		*strVal = '1';
		bLogicSettings.redoVSPAfterDDKAdvanced = PAAS_TRUE;
		break;

	case PAAS_FALSE:
		*strVal = '0';
		bLogicSettings.redoVSPAfterDDKAdvanced = PAAS_FALSE;
		break;
	}

	rv = putEnvFile(SECTION_DEVICE, REDO_VSP_AFTER_ADVANCE_DDK, strVal);
	if(rv < 0)
	{
		rv = FAILURE;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getVSDEncryptionSettings
 *
 * Description	: This API returns the VSD encryption settings loaded at startup.
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
VSD_DTLS_PTYPE getVSDEncryptionSettings()
{
	return bLogicSettings.pstVSDDtls;
}

/*
 * ============================================================================
 * Function Name: getEncryptionType
 *
 * Description	: This API returns the encryption type loaded.
 *
 *
 * Input Params	: none
 *
 * Output Params: ENC_TYPE: VSP_ENC, RSA_ENC, VSD_ENC, NO_ENC
 * ============================================================================
 */
ENC_TYPE getEncryptionType()
{
	return bLogicSettings.encryptionType;
}
/*
 * ============================================================================
 * Function Name: getRSAFingerPrint
 *
 * Description	: This API returns the encryption type loaded.
 *
 *
 * Input Params	: none
 *
 * Output Params: ENC_TYPE: VSP_ENC, RSA_ENC, NO_ENC
 * ============================================================================
 */
char* getRSAFingerPrint()
{
	return bLogicSettings.szRSAFingerPrint;
}
/*
 * ============================================================================
 * Function Name: isDebitOffPinCVMEnabled
 *
 * Description	: This indicates if we need to send offline PIN CVM as debit
 * when AIDList.txt indicates the payment type as '2' (Debit but Credit capable)
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isDebitOffPinCVMEnabled()
{

	return bLogicSettings.debitOffPinCVM;
}
/*
 * ============================================================================
 * Function Name: getDeviceName
 *
 * Description	: This API copies the device name into the buffer given
 *
 *
 * Input Params	: char*
 *
 * Output Params: NONE
 * ============================================================================
 */
void getDeviceName(char* pszDevName)
{
	strcpy(pszDevName, bLogicSettings.szDevName);
}

/*
 * ============================================================================
 * Function Name: updateDeviceName
 *
 * Description	: This API copies updates the device name from the buffer
 *
 *
 * Input Params	: char*
 *
 * Output Params: NONE
 * ============================================================================
 */
void updateDeviceName(char* pszDevName)
{
	strcpy(bLogicSettings.szDevName, pszDevName);

	putEnvFile(SECTION_DEVICE, DEVICE_NAME, bLogicSettings.szDevName);
}

/*
 * ============================================================================
 * Function Name: isDevNameEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isDevNameEnabled()
{
	return bLogicSettings.devNameEnabled;
}

/*
 * ============================================================================
 * Function Name: isTOREnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isTOREnabled()
{
	return bLogicSettings.torEnabled;
}
/*
 * ============================================================================
 * Function Name: getTORretryCount
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getTORretryCount()
{
	return bLogicSettings.iTORretryCount;
}
/*
 * ============================================================================
 * Function Name: isMerchantSettingsReqd
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isMerchantSettingsReqd()
{
	return bLogicSettings.mcntSettingsReqd;
}

/*
 * ============================================================================
 * Function Name: isSTBLogicEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isSTBLogicEnabled()
{
	return stbSettings.stbLogicEnabled;
}

/*
 * ============================================================================
 * Function Name: getSTBPINLimit
 *
 * Description	: This API gives the PIN limit
 *
 *
 * Input Params	: NONE
 *
 * Output Params: PIN LIMIT Value
 * ============================================================================
 */
double getSTBPINLimit()
{
	return stbSettings.fPinLimit;
}

/*
 * ============================================================================
 * Function Name: getSTBSignatureLimit
 *
 * Description	: This API gives the signature amount limit
 *
 *
 * Input Params	: NONE
 *
 * Output Params: Signature LIMIT Value
 * ============================================================================
 */
double getSTBSignatureLimit()
{
	return stbSettings.fSigLimit;
}

/*
 * ============================================================================
 * Function Name: getSTBInterchangeMgmtLimit
 *
 * Description	: This API gives the interchange management amount limit
 *
 *
 * Input Params	: NONE
 *
 * Output Params: Signature LIMIT Value
 * ============================================================================
 */
double getSTBInterchangeMgmtLimit()
{
	return stbSettings.fInterchangeMgmtLimit;
}

/*
 * ============================================================================
 * Function Name: getHashValueOfPreviousPAN
 *
 * Description	: This function generates hash value for the given PAN.
 *
 * Input Params	: Buffer to store previous hash value of the PAN.
 *
 * Output Params: SUCCESS-Hash value of the PAN / FAILURE
 * ============================================================================
 */
int getHashValueOfPreviousPAN(char *pszPrvsPANHashValue)
{
	int		rv				= SUCCESS;
	char	tmpStr[50]		= "";
	FILE *	fp				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( strlen(bLogicSettings.szPANHashValue) > 0 )
		{
#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s:%s: PAN Hash Value from App Memory = [%s]",
												DEVDEBUGMSG, __FUNCTION__, bLogicSettings.szPANHashValue);
			APP_TRACE(szDbgMsg);
#endif

			strcpy(pszPrvsPANHashValue, bLogicSettings.szPANHashValue);
			break;
		}
		/* Get the Previous hash value stored in the PAN_HASHVALUE_FILE */
		rv = doesFileExist(PAN_HASHVALUE_FILE);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: File %s doesn't exist", __FUNCTION__, PAN_HASHVALUE_FILE);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Try to open the file for reading the hash value of the PAN */
		fp = fopen(PAN_HASHVALUE_FILE, "r");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to %s open file", __FUNCTION__, PAN_HASHVALUE_FILE);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Try to read the hash value of the PAN from the file */
		if(NULL == fgets(tmpStr, 41, fp))
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to %s read file", __FUNCTION__, PAN_HASHVALUE_FILE);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
        if (tmpStr[strlen(tmpStr)-1] == '\n')
        {
        	tmpStr[strlen(tmpStr)-1] = 0;/*eliminate the last new line character character*/
        }

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: PAN Hash Value from file = [%s]",
											DEVDEBUGMSG, __FUNCTION__, tmpStr);
		APP_TRACE(szDbgMsg);
#endif

		strcpy(pszPrvsPANHashValue, tmpStr);

		//Also store in the memory
		memset(bLogicSettings.szPANHashValue, 0x00, sizeof(bLogicSettings.szPANHashValue));
		strcpy(bLogicSettings.szPANHashValue, tmpStr);

		break;
	}

	/* Close any open files */
	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeHashValueOfCurPAN
 *
 * Description	: Stores the hash value into the file as well as in application memory.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int storeHashValueOfCurPAN(const char * pszHashValueOfPAN)
{
	int		rv				= SUCCESS;
	FILE *	fp				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/*
		 * Praveen_P1: 08 Jan 2016
		 * As per the CR 45 from McD, we need to store has value if the current
		 * transaction is approved/captured
		 * for declined transactions we will remove the stored hash value
		 * from the caller function sending empty string, thats why commented
		 * the strlen check of the string in the below if condition
		 */
		if( ( pszHashValueOfPAN == NULL ) /*|| ( strlen(pszHashValueOfPAN) <= 0 ) */ )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid parameter", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: Hash Value = [%s]", DEVDEBUGMSG,
														__FUNCTION__, pszHashValueOfPAN);
		APP_TRACE(szDbgMsg);
#endif

		/* Open the temporary file for writing */
		fp = fopen(PAN_HASHVALUE_FILE, "w");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to open %s file for writing",
										__FUNCTION__, PAN_HASHVALUE_FILE);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Write the hash value to the temporary file */
		if(EOF == fputs(pszHashValueOfPAN, fp))
		{
			debug_sprintf(szDbgMsg, "%s: Failed to write hash value of the PAN", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//store hash value of the PAN into application memory.
		memset(bLogicSettings.szPANHashValue, 0x00, sizeof(bLogicSettings.szPANHashValue));
		strcpy(bLogicSettings.szPANHashValue, pszHashValueOfPAN);

		fclose(fp);
		fp = NULL;

		break;
	}

	/* Close any open file pointers */
	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isDCCEnabled
 *
 * Description	: Function returns whether dcc is enabled or not
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isDCCEnabled()
{
	return bLogicSettings.dccEnabled;
}

/*
 * ============================================================================
 * Function Name: loadVSDSettings
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int loadVSDSettings(char *pszErrMsg)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[256]	= "";
	VSD_DTLS_STYPE	stVSDDtls;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(&stVSDDtls, 0x00, sizeof(VSD_DTLS_STYPE));

	while(1)
	{
		/* Store all the VSD setting from VSD.INI*/
		rv = storeVSDSettingsFromINI(&stVSDDtls, pszErrMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to store VSD settings from VSD.INI", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		/* validate the tags configured in the VSD.INI */
		rv = validateVSDSettings(&stVSDDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to validate VSD settings", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		else
		{
			//store in blogic settings
			if(bLogicSettings.pstVSDDtls == NULL)
			{
				bLogicSettings.pstVSDDtls = (VSD_DTLS_PTYPE) malloc(sizeof(VSD_DTLS_STYPE));
				if(bLogicSettings.pstVSDDtls == NULL)
				{
					rv = FAILURE;
					break;
				}
				else
				{
					memset(bLogicSettings.pstVSDDtls, 0x00, sizeof(VSD_DTLS_STYPE));
					memcpy(bLogicSettings.pstVSDDtls, &stVSDDtls, sizeof(VSD_DTLS_STYPE));
				}
			}
		}
		break;
	}

	if(rv != SUCCESS && iAppLogEnabled)
	{
		strcpy(szAppLogData, "Failed To Load VSD Settings From VSD.INI");
		addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}


/*
 * ============================================================================
 * Function Name: storeVSDSettingsFromINI
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeVSDSettingsFromINI(VSD_DTLS_PTYPE pstVSDDtls, char *pszErrMsg)
{
	int					rv							= SUCCESS;
	int					iCnt						= 0;
	int					jCnt						= 0;
	int					kCnt						= 0;
	int					mCnt						= 0;
	int					nCnt						= 0;
	int					iSectionCnt					= 0;
	char				*szCardTypes[]				= {"paym", "gift", "lyl", "noniso"};
	char				szSection[20+1]				= "";
	char				szKey[20+1]					= "";
	char				szTemp[20+1]				= "";
	char				*pszValue					= NULL;
	dictionary 			*dict						= NULL;
	VSD_DTLS_STYPE		stVSDDtls;
	VSD_TAGNODE_PTYPE	pstTmpTagNode 				= NULL;
	VSD_TAGLIST_PTYPE	pstTmpTagList 				= NULL;
	VSD_BLOB_PTYPE		pstTmpVSDBlob 				= NULL;
	char				*szTagMap[MAX_VSD_TAGS][2]  = {	{"cardtype",		NULL},
														{"paytype",			NULL},
														{"pan",				NULL},
														{"expdate",			NULL},
														{"cxx",				NULL},
														{"pan6first",		NULL},
														{"pan4last",		NULL},
														{"servicecode",		NULL},
														{"cardholdername",	NULL},
														{"encmodule",		NULL},
														{"trk1reg",			NULL},
														{"trk2reg",			NULL},
														{"trk3reg",			NULL},
														{"trk1random",		NULL},
														{"trk2random",		NULL},
														{"trk1sha",			NULL},
														{"trk2sha",			NULL},
														{"trk1mask",		NULL},
														{"trk2mask",		NULL},
														{"encblob0",		NULL},
														{"ksn0",			NULL},
														{"iv0",				NULL},
														{"encblob1",		NULL},
														{"ksn1",			NULL},
														{"iv1",				NULL},
														{"emvtag5a",		NULL},
														{"emvtag5f24",		NULL},
														{"emvtag57",		NULL},
														{"emvtag9f1f",		NULL},
														{"emvtag9f20",		NULL},
														{"emvtag5f30",		NULL}};
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(&stVSDDtls, 0x00, sizeof(VSD_DTLS_STYPE));

		/* Check if file exist */
		rv = doesFileExist(VSD_INI_FILE_NAME);
		if(rv != SUCCESS)
		{
			break;
		}

		/* Load the ini file in the parser library */
		dict = iniparser_load(VSD_INI_FILE_NAME);
		if(dict == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to parse ini file [%s], errno [%d]",__FUNCTION__, VSD_INI_FILE_NAME, errno);
			APP_TRACE(szDbgMsg);
			getDisplayMsg(pszErrMsg, MSG_FILE_LOAD_ERR);
			sprintf(pszErrMsg, "%s: %s",pszErrMsg, VSD_INI_FILE_NAME);
			rv = FAILURE;
			break;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Loaded [%s] successfully",__FUNCTION__, VSD_INI_FILE_NAME);
			APP_TRACE(szDbgMsg);
		}

		/* Get all the tag names from the [vsd.ber] section  */
		memset(szSection, 0x00, sizeof(szSection));
		strcpy(szSection, "vsd.ber");

		for(iCnt = 0; iCnt < MAX_VSD_TAGS; iCnt++)
		{
			memset(szTemp, 0x00, sizeof(szTemp));
			sprintf(szTemp,"%s:%s", szSection, szTagMap[iCnt][0]);
			szTagMap[iCnt][1] = iniparser_getstring(dict, szTemp, NULL);
		}

		for(iCnt = 0; iCnt < MAX_VSD_TAGS; iCnt++)
		{
			debug_sprintf(szDbgMsg, "%s: TagID[%s] TagName[%s]",__FUNCTION__, szTagMap[iCnt][0], szTagMap[iCnt][1]);
			APP_TRACE(szDbgMsg);
		}

		strcpy(stVSDDtls.szCardTypeTagName, szTagMap[TAG_CARD_TYPE][1]);
		strcpy(stVSDDtls.szPayTypeTagName, szTagMap[TAG_PAY_TYPE][1]);

		/*  Create the lists of tags configured for each section in VSD.INI */
		for(iCnt = 0, iSectionCnt = 0; iCnt < MAX_CARD_TYPES && rv == SUCCESS ; iCnt++)
		{
			for(jCnt = 1; jCnt <= 3 && rv == SUCCESS; jCnt++, iSectionCnt++)
			{
				memset(szSection, 0x00, sizeof(szSection));
				sprintf(szSection, "vsd.0%d.%s",jCnt, szCardTypes[kCnt]);

				debug_sprintf(szDbgMsg, "%s: ***** Section%d [%s] *****",__FUNCTION__, iSectionCnt, szSection);
				APP_TRACE(szDbgMsg);
				// Get the number of keys present in the section
/*				iNumKeys = iniparser_getsecnkeys(dict, szSection);
				if(iNumKeys <= 0)
				{
					// Section doesn't exist or empty section; No need to parse this section
					stVSDDtls.pstVSDBlob[iSectionCnt] = NULL;
					continue;
				}
*/

				/* Create tag list*/
				for(mCnt = 1; rv == SUCCESS ; mCnt++)
				{
					memset(szKey, 0x00, sizeof(szKey));
					sprintf(szKey, "%s:tlv%d", szSection, mCnt);

					// read key value from the INI & store the tagID & tagName in list
					pszValue = iniparser_getstring (dict, szKey, NULL);
					if(pszValue == NULL)
					{
						//end of parsing for this blob(section)
						debug_sprintf(szDbgMsg, "%s: End of Section%d",__FUNCTION__,iSectionCnt);
						APP_TRACE(szDbgMsg);
						break;
					}

					/* Allocate memory for this section */
					if(pstTmpVSDBlob == NULL)
					{
						pstTmpVSDBlob = (VSD_BLOB_PTYPE) malloc(sizeof(VSD_BLOB_STYPE));
						if(pstTmpVSDBlob == NULL)
						{
							rv = FAILURE;
							break;
						}
						memset(pstTmpVSDBlob, 0x00, sizeof(VSD_BLOB_STYPE));

						pstTmpTagList = (VSD_TAGLIST_PTYPE) malloc(sizeof(VSD_TAGLIST_STYPE));
						if(pstTmpTagList == NULL)
						{
							rv = FAILURE;
							break;
						}
						memset(pstTmpTagList, 0x00, sizeof(VSD_TAGLIST_STYPE));

						// copy the card type & payment type for this blob
						pstTmpVSDBlob->cardType = kCnt;
						pstTmpVSDBlob->payType  = jCnt;
					}

					//Allocate memory for each tag node
					pstTmpTagNode = (VSD_TAGNODE_PTYPE) malloc(sizeof(VSD_TAGNODE_STYPE));
					if(pstTmpTagNode == NULL)
					{
						rv = FAILURE;
						break;
					}
					memset(pstTmpTagNode, 0x00, sizeof(VSD_TAGNODE_STYPE));

					// fill in the node value with corresponding tag ID & Name
					for(nCnt = 0; nCnt < MAX_VSD_TAGS; nCnt++)
					{
						if(! strcmp(szTagMap[nCnt][0], pszValue))
						{
							pstTmpTagNode->tagID = nCnt;
							break;
						}
					}
					if(nCnt == MAX_VSD_TAGS)
					{
						debug_sprintf(szDbgMsg, "%s: Should not come here. Undefined tag value at[%s]",__FUNCTION__, szKey);
						APP_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
					strcpy(pstTmpTagNode->tagName, szTagMap[pstTmpTagNode->tagID][1]);
					pstTmpTagNode->next = NULL;

					debug_sprintf(szDbgMsg, "%s: Tags: key[%s] value[%s] TagName[%s]",__FUNCTION__, szKey, pszValue, szTagMap[pstTmpTagNode->tagID][1]);
					APP_TRACE(szDbgMsg);

					// Add node to the list
					if(pstTmpTagList->start == NULL)
					{
						pstTmpTagList->start = pstTmpTagNode;
						pstTmpTagList->end 	 = pstTmpTagNode;
					}
					else
					{
						pstTmpTagList->end->next = pstTmpTagNode;
						pstTmpTagList->end = pstTmpTagNode;
						pstTmpTagNode = NULL;
					}
					pstTmpTagList->iTotTags++;
				}
				stVSDDtls.pstVSDBlob[iSectionCnt] = pstTmpVSDBlob;
				if(pstTmpVSDBlob != NULL)
				{
					stVSDDtls.pstVSDBlob[iSectionCnt]->pstTagList = pstTmpTagList;
					stVSDDtls.iTotSection++;
					pstTmpVSDBlob = NULL;
					pstTmpTagList = NULL;
				}

			}
			kCnt++;
		}

		// print all tags stored in the list
		debug_sprintf(szDbgMsg, "%s: Total[%d] Valid Sections filled in list", __FUNCTION__, stVSDDtls.iTotSection);
		APP_TRACE(szDbgMsg);
#if 0
		if(stVSDDtls.iTotSection > 0)
		{
			for(iCnt = 0; iCnt < MAX_VSD_SECTION; iCnt++)
			{
				debug_sprintf(szDbgMsg, "%s: Section[%d]", __FUNCTION__, iCnt+1);
				APP_TRACE(szDbgMsg);
				if(stVSDDtls.pstVSDBlob[iCnt] == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Empty Section", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}

				debug_sprintf(szDbgMsg, "%s: Card Type:[%d: %s] PayType [%d]", __FUNCTION__, stVSDDtls.pstVSDBlob[iCnt]->cardType, szCardTypes[stVSDDtls.pstVSDBlob[iCnt]->cardType], stVSDDtls.pstVSDBlob[iCnt]->payType);
				APP_TRACE(szDbgMsg);

				pstTmpTagNode = stVSDDtls.pstVSDBlob[iCnt]->pstTagList->start;
				while(pstTmpTagNode != NULL)
				{
					debug_sprintf(szDbgMsg, "%s: TagID[%d] TagName[%s]", __FUNCTION__, pstTmpTagNode->tagID, pstTmpTagNode->tagName);
					APP_TRACE(szDbgMsg);
					pstTmpTagNode = pstTmpTagNode->next;
				}
			}
		}
#endif
		break;
	}

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Parsing Failed; Freeing any allocated memory", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//free allocated memory properly
		free(pstTmpTagNode);	// free the last temp tag node
		free(pstTmpTagList);	// free the last temp tag list(single node)
		free(pstTmpVSDBlob);	// free the last temp VSD blob(single node)
		freeVSDtagList(&stVSDDtls);// free the Tag list from beginning
	}
	else
	{
		// copy the VSD details to the input param
		memcpy(pstVSDDtls, &stVSDDtls, sizeof(VSD_DTLS_STYPE));
	}

	if(dict != NULL)
	{
		iniparser_freedict(dict);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: validateVSDSettings
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int validateVSDSettings(VSD_DTLS_PTYPE pstVSDDtls)
{
	int					rv					= SUCCESS;
	int 				iCnt 				= 0;
	int					iTagCnt				= 0;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[256]	= "";
	char				szTemp[10]			= "";
	PAAS_BOOL			bEncBlob0Parsed		= PAAS_FALSE;
	PAAS_BOOL			bEncBlob1Parsed		= PAAS_FALSE;
	PAAS_BOOL			bIVTagReqd			= PAAS_TRUE;
	PAAS_BOOL			bIV0TagPresent		= PAAS_FALSE;
	PAAS_BOOL			bIV1TagPresent		= PAAS_FALSE;
	PAAS_BOOL			bKSN0TagPresent		= PAAS_FALSE;
	PAAS_BOOL			bKSN1TagPresent		= PAAS_FALSE;
	VSD_TAGNODE_PTYPE 	pstTmpTagNode 		= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	// read the config parameter ADE_SETTING
	memset(szTemp, 0x00, sizeof(szTemp));
	rv = getEnvFile(SECTION_REG, "ADE_SETTING", szTemp, sizeof(szTemp));
	if(rv > 0)
	{
		if(szTemp[2] == '2')
		{
			debug_sprintf(szDbgMsg, "%s: IV is configured as randomized",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			bIVTagReqd = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: IV Tag is not required",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			bIVTagReqd = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for ADE_SETTING parameter */
		debug_sprintf(szDbgMsg, "%s: IV is by default set as randomized",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		bIVTagReqd = PAAS_TRUE;
	}
	rv = SUCCESS;

	for(iCnt = 0; iCnt < MAX_VSD_SECTION && rv == SUCCESS; iCnt++)
	{
		if(pstVSDDtls->pstVSDBlob[iCnt] == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty Section[%d]", __FUNCTION__, iCnt);
			APP_TRACE(szDbgMsg);
			continue;
		}

		if(pstVSDDtls->pstVSDBlob[iCnt]->pstTagList != NULL)
		{
			pstTmpTagNode = pstVSDDtls->pstVSDBlob[iCnt]->pstTagList->start;
			while(pstTmpTagNode != NULL)
			{
				iTagCnt++;

				/* First two tags of each blob(section in VSD.INI) must be configured for cardtype & paytype tags respectively for all four kind of cards */
				if((iTagCnt == 1 && pstTmpTagNode->tagID != TAG_CARD_TYPE) || (iTagCnt ==2 && pstTmpTagNode->tagID != TAG_PAY_TYPE))
				{
					debug_sprintf(szDbgMsg, "%s: [cardtype] and/or [paytype] tags not configured in correct order in section%d", __FUNCTION__,iCnt);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "[cardtype] AND/OR [paytype] Tags Not Configured In Correct Order In VSD.INI");
						addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
					}
					break;
				}

				// we will set the following two flags as soon as we encounter any encryption blob either 0 or 1
				if(pstTmpTagNode->tagID == TAG_ENCBLOB0)
				{
					bEncBlob0Parsed = PAAS_TRUE;
				}
				else if(pstTmpTagNode->tagID == TAG_ENCBLOB1)
				{
					bEncBlob1Parsed = PAAS_TRUE;
				}

				/*
				 * In each section of VSD.INI, ksn & iv tags must be configured  after the encblob0/encblob1.
				 * Any changes in the order of these tags will results in failure of SCA initialization
				 */
				if((((pstTmpTagNode->tagID == TAG_KSN0) || (pstTmpTagNode->tagID == TAG_IV0)) && (bEncBlob0Parsed == PAAS_FALSE)) ||
						(((pstTmpTagNode->tagID == TAG_KSN1) || (pstTmpTagNode->tagID == TAG_IV1)) && (bEncBlob1Parsed == PAAS_FALSE)))
				{
					debug_sprintf(szDbgMsg, "%s: [ksn] and/or [iv] tags not configured in correct order in section%d", __FUNCTION__, iCnt);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "[ksn] AND/OR [iv] Tags Not Configured In Correct Order In VSD.INI");
						addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
					}
					break;
				}

				/* Check the presence of KSN tag*/
				if((bEncBlob0Parsed == PAAS_TRUE) && (pstTmpTagNode->tagID == TAG_KSN0))
				{
					bKSN0TagPresent = PAAS_TRUE;
				}
				else if((bEncBlob1Parsed == PAAS_TRUE) && (pstTmpTagNode->tagID == TAG_KSN1))
				{
					bKSN1TagPresent = PAAS_TRUE;
				}

				/* Check the presence of IV tag */
				if((bIVTagReqd == PAAS_TRUE) &&
					((pstTmpTagNode->tagID == TAG_IV0) && (bEncBlob0Parsed == PAAS_TRUE)))
				{
					bIV0TagPresent = PAAS_TRUE;
				}
				else if((bIVTagReqd == PAAS_TRUE) &&
					((pstTmpTagNode->tagID == TAG_IV1) && (bEncBlob1Parsed == PAAS_TRUE)))
				{
					bIV1TagPresent = PAAS_TRUE;
				}

				pstTmpTagNode = pstTmpTagNode->next;

			}

			if((bIVTagReqd == PAAS_TRUE) &&
					(((bEncBlob0Parsed == PAAS_TRUE) && (bIV0TagPresent == PAAS_FALSE)) ||
					((bEncBlob1Parsed == PAAS_TRUE) && (bIV1TagPresent == PAAS_FALSE))))
			{
				debug_sprintf(szDbgMsg, "%s: IV tag must be configured under section%d in VSD.INI", __FUNCTION__, iCnt);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Required IV Tag Is Not Being Configured In VSD.INI");
					addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
				}
				break;
			}

			if(((bEncBlob0Parsed == PAAS_TRUE) && (bKSN0TagPresent == PAAS_FALSE)) ||
					((bEncBlob1Parsed == PAAS_TRUE) && (bKSN1TagPresent == PAAS_FALSE)))
			{
				debug_sprintf(szDbgMsg, "%s: KSN tag must be configured under section%d in VSD.INI", __FUNCTION__, iCnt);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Required KSN Tag Is Not Being Configured In VSD.INI");
					addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
				}
				break;
			}
			//reset all the counters/flags for next section
			iTagCnt = 0;
			bEncBlob0Parsed = PAAS_FALSE;
			bEncBlob1Parsed = PAAS_FALSE;
			bIV0TagPresent = PAAS_FALSE;
			bIV1TagPresent = PAAS_FALSE;
			bKSN0TagPresent = PAAS_FALSE;
			bKSN1TagPresent = PAAS_FALSE;
		}
	}

	if(rv != SUCCESS)
	{
		freeVSDtagList(pstVSDDtls);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: freeVSDtagList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void freeVSDtagList(VSD_DTLS_PTYPE pstVSDDtls)
{
	int 				iCnt 				= 0;
	VSD_TAGNODE_PTYPE 	pstTmpTagNode 		= NULL;
	VSD_TAGNODE_PTYPE 	pstTmpNxtTagNode	= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	for(iCnt = 0; iCnt < MAX_VSD_SECTION; iCnt++)
	{
		if(pstVSDDtls->pstVSDBlob[iCnt] == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty Section[%d]", __FUNCTION__, iCnt);
			APP_TRACE(szDbgMsg);
			continue;
		}

		if(pstVSDDtls->pstVSDBlob[iCnt]->pstTagList != NULL)
		{
			// free each node present in the list
			pstTmpTagNode = pstVSDDtls->pstVSDBlob[iCnt]->pstTagList->start;
			while(pstTmpTagNode != NULL)
			{
				pstTmpNxtTagNode = pstTmpTagNode->next;
				free(pstTmpTagNode);
				pstTmpTagNode = pstTmpNxtTagNode;
			}

			// free the tag list node
			free(pstVSDDtls->pstVSDBlob[iCnt]->pstTagList);
		}
		// free the current VSD blob node
		free(pstVSDDtls->pstVSDBlob[iCnt]);
	}

	return ;
}
