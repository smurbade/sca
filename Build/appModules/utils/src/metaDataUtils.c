
/*
 * =============================================================================
 * Filename    : metaDataUtils.c
 *
 * Application : Mx Point SCA
 *
 * Description : 
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common/common.h"
#include "common/metaData.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

/*
 * ============================================================================
 * Function Name: freeMetaData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int freeMetaData(METADATA_PTYPE metaDataPtr)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	void *			dataPtr			= NULL;
	void *			tmpPtr			= NULL;
	VAL_LST_PTYPE	valLstPtr		= NULL;
	VAL_NODE_PTYPE	valNodePtr		= NULL;
	KEYVAL_PTYPE	listPtr			= NULL;
	METADATA_STYPE	stLocMetaData;
#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);
	// CID-67219: 2-Feb-16: MukeshS3: Adding Null params check
	if(metaDataPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(&stLocMetaData, 0x00, METADATA_SIZE);

	listPtr = metaDataPtr->keyValList;

	if(listPtr == NULL)
	{
		//debug_sprintf(szDbgMsg, "%s: No list to free!!!", __FUNCTION__);
		//APP_TRACE(szDbgMsg);
		//memset(metaDataPtr, 0x00, METADATA_SIZE);

		return rv;
	}

	/* Clean all the values */
	for(iCnt = 0; iCnt < metaDataPtr->iTotCnt; iCnt++)
	{
		if(listPtr[iCnt].value == NULL)
		{
			continue;
		}

		switch(listPtr[iCnt].valType)
		{
		case SINGLETON:
		case AMOUNT:
		case NUMERICS:
		case MASK_NUM_STR:
		case BOOLEAN:
		case STRING:
		case LIST:
		case TIME:
		case DATE:
		case NUM_RANGE:
		case CHAR_SET:
		case ATTRIBUTE:
			free(listPtr[iCnt].value);
			listPtr[iCnt].value = NULL;

			break;

		case ATTR_LKDLST: /* VDR: FIX THIS */

			valNodePtr = (VAL_NODE_PTYPE) listPtr[iCnt].value;

			while(valNodePtr)
			{
				stLocMetaData.iTotCnt = valNodePtr->elemCnt;
				stLocMetaData.keyValList = valNodePtr->elemList;

				freeMetaData(&stLocMetaData);

				if(valNodePtr->szVal != NULL)
				{
					free(valNodePtr->szVal);
				}

				dataPtr = valNodePtr;
				valNodePtr = valNodePtr->next;

				free(dataPtr);
			}

			break;

		case MULTILIST:
			dataPtr = listPtr[iCnt].value;
			while(dataPtr)
			{
				tmpPtr = dataPtr;
				dataPtr = ((MLVAL_PTYPE) dataPtr)->next;

				free(tmpPtr);
				tmpPtr = NULL;
			}

			break;

		case VARLIST:
		case RECLIST:
			valLstPtr = (VAL_LST_PTYPE) listPtr[iCnt].value;

			valNodePtr = valLstPtr->start;
			while(valNodePtr)
			{
				stLocMetaData.iTotCnt = valNodePtr->elemCnt;
				stLocMetaData.keyValList = valNodePtr->elemList;

				freeMetaData(&stLocMetaData);

				if(valNodePtr->szVal != NULL)
				{
					free(valNodePtr->szVal);
				}
				/* DAIVIK:14-11-2015- The absence of the below free , can cause a memory leak in cases where the SCI Data consists
				 * of list within a list structure. For Example - while doing SAF Query , we were leaking 460 bytes per SAF record.
				 */
				if(valNodePtr->elemList != NULL)
				{
					free(valNodePtr->elemList);
				}

				dataPtr = valNodePtr;
				valNodePtr = valNodePtr->next;
				free(dataPtr);
			}

			free(valLstPtr);

			break;

		case NULL_ADD:
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: [%s]SHOULD NEVER COME HERE [%d]",
												__FUNCTION__, listPtr[iCnt].key,
												listPtr[iCnt].valType);
			APP_TRACE(szDbgMsg);

			free(listPtr[iCnt].value);
			listPtr[iCnt].value = NULL;

			break;
		}
	}

	memset(metaDataPtr, 0x00, METADATA_SIZE);

	return rv;
}

/*
 * ============================================================================
 * Function Name: freeMetaDataEx
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int freeMetaDataEx(METADATA_PTYPE metaDataPtr)
{
	int				rv		= SUCCESS;
	KEYVAL_PTYPE	listPtr	= NULL;

#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(metaDataPtr != NULL)
	{
		listPtr = metaDataPtr->keyValList;
		freeMetaData(metaDataPtr);
		//CID 67219 (#1 of 1): Dereference after null check (FORWARD_NULL) memset of metaDataPtr is not done if it is NULL. T_RaghavendranR1
		if(listPtr != NULL)
		{
			free(listPtr);
		}

		memset(metaDataPtr, 0x00, METADATA_SIZE);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: freeValListNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int freeValListNode(VAL_LST_PTYPE tmpListPtr)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	VAL_NODE_PTYPE	tmpNodePtr		= NULL;
	VAL_NODE_PTYPE	tmpNxtNodePtr	= NULL;

	if(tmpListPtr != NULL)
	{
		tmpNodePtr = tmpListPtr->start;
		for(iCnt = 0; iCnt < tmpListPtr->valCnt && tmpNodePtr != NULL; iCnt++)
		{
			tmpNxtNodePtr = tmpNodePtr->next;
			rv = freeValNode(tmpNodePtr);
			tmpNodePtr = tmpNxtNodePtr;
		}
		free(tmpListPtr);
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: freeValNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int freeValNode(VAL_NODE_PTYPE tmpNodePtr)
{
	int				rv				= SUCCESS;

	if(tmpNodePtr != NULL)
	{
		if(tmpNodePtr->elemList != NULL)
		{
			rv = freeKeyValDataNode(tmpNodePtr->elemList, tmpNodePtr->elemCnt);
		}
		free(tmpNodePtr);
	}

	return rv;
}
/*
 * ============================================================================
 * Function Name: freeKeyValDataNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int freeKeyValDataNode(KEYVAL_PTYPE tmpListPtr, int iTotCnt)
{
	int				rv				= SUCCESS;
	METADATA_STYPE  stTempMetaData;

	memset(&stTempMetaData, 0x00, sizeof(METADATA_STYPE));
	stTempMetaData.keyValList = tmpListPtr;
	stTempMetaData.iTotCnt = iTotCnt;
	rv = freeMetaDataEx(&stTempMetaData);

	return rv;
}

/*
 * ============================================================================
 * End of file metaDataUtils.c
 * ============================================================================
 */
