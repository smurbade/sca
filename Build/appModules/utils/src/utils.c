/*
 * ============================================================================
 * File Name	: utils.c
 *
 * Description	:
 *
 * Author		:
 * ============================================================================
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <svcsec.h>
#include <sys/timeb.h>	// For struct timeb
#include <sys/stat.h>
#include <ctype.h>		//for toupper()

#include "svc.h"
#include "openssl/bio.h"
#include "openssl/rsa.h"
#include "openssl/aes.h"
#include "openssl/hmac.h"
#include "openssl/buffer.h"
#include "openssl/x509.h"
#include "openssl/evp.h"
#include "appLog/appLogAPIs.h"
#include "common/utils.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);


static int getTimeFormatSpecifier(char *, char *);
static int getDateFormatSpecifier(char *, char *);

/* needed for Base 64 encoding/decoding */
static const char base64Char[] = {
					 /*  0    1    2    3    4    5    6    7    8    9   10 */
						'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
					 /*  11   12  13   14   15   16   17   18   19   20   21 */
						'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
					 /* 22   23   24   25 */
						'W', 'X', 'Y', 'Z',
					 /* 26   27   28   29   30   31   32   33   34   35   36 */
						'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
					 /* 37   38   39   40   41   42   43   44   45   46   47 */
						'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
					 /* 48   49   50   51 */
						'w', 'x', 'y', 'z',
					 /* 52   53   54   55   56   57   58   59   60   61 */
						'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
					 /* 62   63 */
						'+', '/'
					};

/* Static functions declarations */

static RSA * loadRSAKey(const uchar * key, int size);
static int	toUTF(ushort, uchar *);

#ifdef DEBUG

#define PAAS_DEBUG	"PAAS_DEBUG"
#define PAAS_MEM_DEBUG	"PAAS_MEM_DEBUG"

#define MAX_DEBUG_MSG_SIZE	15+4096+2+1
char chDebug 	= NO_DEBUG;
char chMemDebug = NO_DEBUG;

/* *
 * Initialize all debug related parameters.
 * */
/*
 * ============================================================================
 * Function Name: setupDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setupDebug(int *piSaveDebugLogs)
{
	char *	cpDbgFlag	= NULL;

	chDebug = NO_DEBUG;

	if((cpDbgFlag = (char *)getenv(PAAS_DEBUG)) == NULL)
	{
		chDebug = NO_DEBUG; // Debug runtime disabled
	}
	else
	{
		if(cpDbgFlag[0] == '1') // Spew on the console
		{
			chDebug = CONSOLE_DEBUG;
		}
		else if(cpDbgFlag[0] == '2') // Ethernet Debug
		{
			chDebug = ETHERNET_DEBUG;
		}
		else if(cpDbgFlag[0] == '0')
		{
			chDebug = NO_DEBUG; // Debug runtime disabled
		}
	}

	if(chDebug == ETHERNET_DEBUG)
	{
		iOpenDebugSocket();
	}

    char *pszEnvVar = NULL;
    if ((pszEnvVar = getenv("DEBUGOPT")) != NULL)
    {
        if ((strcasecmp(pszEnvVar, "FILE") == 0)        // => File-based logging
                ||
            (strcasecmp(pszEnvVar, "USB") == 0)         // => USB-based logging
                ||
            (strcasecmp(pszEnvVar, "USB_DIRECT") == 0)  // => USB-based logging, direct to USB flash - added 16-Nov-12
                ||
            (strcasecmp(pszEnvVar, "SD") == 0))         // => microSD-based logging - added 16-Nov-12
        {
        	*piSaveDebugLogs = 1;
        }
        else
        {
        	*piSaveDebugLogs = 0;
        }
    }
    else
    {
    	*piSaveDebugLogs = 0;
    }

	return;
}

/* *
 * Initialize all Memory debug related parameters.
 * */
/*
 * ============================================================================
 * Function Name: setupDebug
 *
 * Description	:  Initialize all Memory debug related parameters. This will enable/disable the debug statements of memory allocation/free
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setupMemDebug(int *piSaveDebugLogs)
{
	char *	cpDbgFlag	= NULL;

	chMemDebug = NO_DEBUG;

	if((cpDbgFlag = (char *)getenv(PAAS_MEM_DEBUG)) == NULL)
	{
		chMemDebug = NO_DEBUG; // Debug runtime disabled
	}
	else
	{
		if(cpDbgFlag[0] == '1') // Spew on the console
		{
			chMemDebug = CONSOLE_DEBUG;
		}
		else if(cpDbgFlag[0] == '2') // Ethernet Debug
		{
			chMemDebug = ETHERNET_DEBUG;
		}
		else if(cpDbgFlag[0] == '0')
		{
			chMemDebug = NO_DEBUG; // Debug runtime disabled
		}
	}

	if(chMemDebug == ETHERNET_DEBUG)
	{
		iOpenDebugSocket();
	}

    char *pszEnvVar = NULL;
    if ((pszEnvVar = getenv("DEBUGOPT")) != NULL)
    {
        if ((strcasecmp(pszEnvVar, "FILE") == 0)        // => File-based logging
                ||
            (strcasecmp(pszEnvVar, "USB") == 0)         // => USB-based logging
                ||
            (strcasecmp(pszEnvVar, "USB_DIRECT") == 0)  // => USB-based logging, direct to USB flash - added 16-Nov-12
                ||
            (strcasecmp(pszEnvVar, "SD") == 0))         // => microSD-based logging - added 16-Nov-12
        {
        	*piSaveDebugLogs = 1;
        }
        else
        {
        	*piSaveDebugLogs = 0;
        }
    }
    else
    {
    	*piSaveDebugLogs = 0;
    }

	return;
}

/*
 * ============================================================================
 * Function Name: closeupDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void closeupDebug(void)
{
	int 	rv 					= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(chDebug == ETHERNET_DEBUG)
	{
		rv = iCloseDebugSocket();

		debug_sprintf(szDbgMsg, "%s: Return value of iCloseDebugSocket is %d", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);

	}

	return;
}

/*
 * ============================================================================
 * Function Name: closeupMemDebug
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void closeupMemDebug(void)
{
	int 	rv 					= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(chMemDebug == ETHERNET_DEBUG)
	{
		rv = iCloseDebugSocket();

		debug_sprintf(szDbgMsg, "%s: Return value of iCloseDebugSocket is %d", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);

	}

	return;
}

/*
 * ============================================================================
 * Function Name: setDebugParamsToEnv
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int setDebugParamsToEnv(int * piDbgParamSet)
{
	int 	rv 					= SUCCESS;
	char	szParamValue[20]	= "";
	int		iLen;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szParamValue);

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "PAAS_DEBUG", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting PAAS_DEBUG variable[%s=%s]", __FUNCTION__, "PAAS_DEBUG", szParamValue);
		APP_TRACE(szDbgMsg);

		rv = setenv("PAAS_DEBUG", szParamValue, 1);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting PAAS_DEBUG to env", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "PAAS_MEM_DEBUG", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting PAAS_MEM_DEBUG variable[%s=%s]", __FUNCTION__, "PAAS_MEM_DEBUG", szParamValue);
		APP_TRACE(szDbgMsg);

		rv = setenv("PAAS_MEM_DEBUG", szParamValue, 1);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting PAAS_MEM_DEBUG to env", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DEBUG_TMS", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DEBUG_TMS variable[%s=%s]", __FUNCTION__, "DEBUG_TMS", szParamValue);
		APP_TRACE(szDbgMsg);

		rv = setenv("DEBUG_TMS", szParamValue, 1);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting DEBUG_TMS to env", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DBGIP", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DBGIP variable[%s=%s]", __FUNCTION__, "DBGIP", szParamValue);
		APP_TRACE(szDbgMsg);

		rv = setenv("DBGIP", szParamValue, 1);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting DBGIP to env", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DBGPRT", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DBGPRT variable[%s=%s]", __FUNCTION__, "DBGPRT", szParamValue);
		APP_TRACE(szDbgMsg);

		rv = setenv("DBGPRT", szParamValue, 1);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting DBGPRT to env", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		*piDbgParamSet = 1;
	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DEBUGOPT", szParamValue, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Setting DEBUGOPT variable[%s=%s]", __FUNCTION__, "DEBUGOPT", szParamValue);
		APP_TRACE(szDbgMsg);

		rv = setenv("DEBUGOPT", szParamValue, 1);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting DEBUGOPT to env", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		*piDbgParamSet = 1;
	}

	rv = SUCCESS;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: APP_TRACE
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void APP_TRACE(char * pszMsg)
{
	int				iPrefixLen		= 0;
	int				iDbgMsgSize		= 0;
	int				iMsgLen			= 0;
	pid_t			processID		= 0;
	pthread_t		threadID		= 0L;
	char			szTSPrefix[41]	= ""; /* MM/DD/YYYY HH:MM:SS:sss */
	char			szDbgMsg[MAX_DEBUG_MSG_SIZE+1] = "";
	char *			pszMsgPrefix	= "PS";
	struct tm *		tm_ptr			= NULL;
	struct timeb	the_time;

	if(chDebug == NO_DEBUG)
	{
		return;
	}

	/* Get the starting values */
	iDbgMsgSize = sizeof(szDbgMsg) - 1;
	iMsgLen = strlen(pszMsg);
	processID = getpid();
	threadID = pthread_self();

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
		/* Keep within 3 digits, since sprintf's width.precision specification
		 * does not truncate large values */
		the_time.millitm = 999;
	}

	sprintf(szTSPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d", tm_ptr->tm_mon+1,
				tm_ptr->tm_mday, 1900+tm_ptr->tm_year, tm_ptr->tm_hour,
				tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	iPrefixLen = sprintf(szDbgMsg, "%s %04d %04d %s: ", szTSPrefix, processID,
												(int) threadID, pszMsgPrefix);

	if(chDebug == ETHERNET_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= iDbgMsgSize)
		{
			/* Chop iMsgLen down */
			iMsgLen = iDbgMsgSize - iPrefixLen;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = 0;

		DebugMsg(szDbgMsg);
	}
	else if(chDebug == CONSOLE_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= (iDbgMsgSize - 1))
		{
			/* Chop iMsgLen down; save last 2 slots for '\n' and '\0' */
			iMsgLen = iDbgMsgSize - iPrefixLen - 2;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = '\n';
		szDbgMsg[iPrefixLen + iMsgLen + 1] = 0;

		printf(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: APP_MEM_TRACE
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void APP_MEM_TRACE(char * pszMsg)
{
	int				iPrefixLen		= 0;
	int				iDbgMsgSize		= 0;
	int				iMsgLen			= 0;
	pid_t			processID		= 0;
	pthread_t		threadID		= 0L;
	char			szTSPrefix[41]	= ""; /* MM/DD/YYYY HH:MM:SS:sss */
	char			szDbgMsg[MAX_DEBUG_MSG_SIZE+1] = "";
	char *			pszMsgPrefix	= "PS";
	struct tm *		tm_ptr			= NULL;
	struct timeb	the_time;

	if(chMemDebug == NO_DEBUG)
	{
		return;
	}

	/* Get the starting values */
	iDbgMsgSize = sizeof(szDbgMsg) - 1;
	iMsgLen = strlen(pszMsg);
	processID = getpid();
	threadID = pthread_self();

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
		/* Keep within 3 digits, since sprintf's width.precision specification
		 * does not truncate large values */
		the_time.millitm = 999;
	}

	sprintf(szTSPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d", tm_ptr->tm_mon+1,
				tm_ptr->tm_mday, 1900+tm_ptr->tm_year, tm_ptr->tm_hour,
				tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	iPrefixLen = sprintf(szDbgMsg, "%s %04d %04d %s: ", szTSPrefix, processID,
												(int) threadID, pszMsgPrefix);

	if(chMemDebug == ETHERNET_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= iDbgMsgSize)
		{
			/* Chop iMsgLen down */
			iMsgLen = iDbgMsgSize - iPrefixLen;
		}

//		printf("szMsg to log --- [%s]", pszMsg);
		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = 0;

		DebugMsg(szDbgMsg);
	}
	else if(chMemDebug == CONSOLE_DEBUG)
	{
		/* If addition of full datasize will exceed szDbgMsg */
		if((iPrefixLen + iMsgLen) >= (iDbgMsgSize - 1))
		{
			/* Chop iMsgLen down; save last 2 slots for '\n' and '\0' */
			iMsgLen = iDbgMsgSize - iPrefixLen - 2;
		}

		memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
		szDbgMsg[iPrefixLen + iMsgLen] = '\n';
		szDbgMsg[iPrefixLen + iMsgLen + 1] = 0;

		printf(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: APP_TRACE_EX
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void APP_TRACE_EX(char *pszMessage, int iMsgLen)
{
	char 		 szTimeStampPrefix[40+1]; // MM/DD/YYYY HH:MM:SS:sss
	char 		 szDebugMsg[MAX_DEBUG_MSG_SIZE+1+MAX_DEBUG_MSG_SIZE];
	char 		 *pszMsgPrefix = "PS";
	struct timeb the_time;
	struct tm 	 *tm_ptr;
	pid_t 		 processID = getpid();		// Get current process ID
	pthread_t 	 threadID = pthread_self();	// Get current thread's ID
	int          iPrefixLen;
	int          iDebugMsgSize = sizeof(szDebugMsg)-1;

	if (chDebug == NO_DEBUG)
	{
		return;
	}

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if (the_time.millitm >= 1000)
	{
		the_time.millitm = 999;	// Keep within 3 digits, since sprintf's width.precision specification does not truncate large values
	}
	sprintf(szTimeStampPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d",
						tm_ptr->tm_mon+1, tm_ptr->tm_mday, 1900+tm_ptr->tm_year,
						tm_ptr->tm_hour, tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	if (chDebug == ETHERNET_DEBUG)
	{
		iPrefixLen = sprintf(szDebugMsg, "%s %04d %04d %s: ", szTimeStampPrefix, processID,
							(int)threadID, pszMsgPrefix);
		// 07-Oct-08: make sure we don't exceed szDebugMsg[] capacity
		if ((iPrefixLen+iMsgLen) >= iDebugMsgSize) // If addition of full datasize will exceed szDebugMsg[]
		{
			iMsgLen = iDebugMsgSize - iPrefixLen;  // Chop iMsgLen down
		}

		memcpy(szDebugMsg+iPrefixLen, pszMessage, iMsgLen);
		DebugMsgEx((unsigned char *)szDebugMsg, iPrefixLen+iMsgLen);
	}
}

#endif
#if 0
/*
 * ============================================================================
 * Function Name: encdBase64
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int encdBase64(uchar * inpData, int inpLen, uchar * outData, int * outLen)
{
	int				iCount	= 0;
	int				jCount	= 0;
	int				kCount	= 0;
	unsigned char	iStr[3]	= "";
	unsigned char	oStr[4]	= "";
#ifdef DEBUG
	char			szDbgMsg[128] = "";
#endif

	if(inpData == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input data is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else if(outData == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Output data is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	while(inpLen > 2)
	{
		iStr[0] = inpData[iCount++];
		iStr[1] = inpData[iCount++];
		iStr[2] = inpData[iCount++];

		oStr[0] = (iStr[0] >> 2) & 0xFF;
		oStr[1] = (((iStr[0] & 0x03) << 4) | ((iStr[1] & 0xF0) >> 4)) & 0xFF;
		oStr[2] = (((iStr[1] & 0x0F) << 2) | ((iStr[2] >> 6) & 0x03)) & 0xFF;
		oStr[3] = (iStr[2] & 0x3F) & 0xFF;

		outData[jCount++] = base64Char[oStr[0]];

		debug_sprintf(szDbgMsg, "outData[%d] = [%c] ", jCount, outData[jCount-1]);
		APP_TRACE(szDbgMsg);

		outData[jCount++] = base64Char[oStr[1]];
		debug_sprintf(szDbgMsg, "outData[%d] = [%c] ", jCount, outData[jCount-1]);
		APP_TRACE(szDbgMsg);

		outData[jCount++] = base64Char[oStr[2]];

		debug_sprintf(szDbgMsg, "outData[%d] = [%c] ", jCount, outData[jCount-1]);
		APP_TRACE(szDbgMsg);

		outData[jCount++] = base64Char[oStr[3]];
		debug_sprintf(szDbgMsg, "outData[%d] = [%c] ", jCount, outData[jCount-1]);
		APP_TRACE(szDbgMsg);

		inpLen -= 3;
	}

	memset(iStr, 0x00, sizeof(iStr));

	for(kCount = 0; kCount < inpLen; kCount++)
	{
		iStr[kCount] = inpData[iCount++];
	}

	memset(oStr, 0x00, sizeof(oStr));

	oStr[0] = (iStr[0] >> 2) & 0xFF;
	outData[jCount++] = base64Char[oStr[0]];

	debug_sprintf(szDbgMsg, "outData[%d] = [%c] ", jCount, outData[jCount-1]);
			APP_TRACE(szDbgMsg);

	oStr[1] = (((iStr[0] & 0x03) << 4) | ((iStr[1] & 0xF0) >> 4)) & 0xFF;
	outData[jCount++] = base64Char[oStr[1]];
	debug_sprintf(szDbgMsg, "outData[%d] = [%c] ", jCount, outData[jCount-1]);
			APP_TRACE(szDbgMsg);
	if(inpLen == 2)
	{
		oStr[2] = (((iStr[1] & 0x0F) << 2) | ((iStr[2] >> 6) & 0x03)) & 0xFF;
		outData[jCount++] = base64Char[oStr[2]];
		debug_sprintf(szDbgMsg, "outData[%d] = [%c] ", jCount, outData[jCount-1]);
				APP_TRACE(szDbgMsg);
	}
	else
	{
		outData[jCount++] = '=';
		debug_sprintf(szDbgMsg, "outData[%d] = [%c] ", jCount, outData[jCount-1]);
				APP_TRACE(szDbgMsg);
	}
	outData[jCount++] = '=';
	debug_sprintf(szDbgMsg, "outData[%d] = [%c] ", jCount, outData[jCount-1]);
			APP_TRACE(szDbgMsg);

	*outLen = jCount;

	return SUCCESS;
}
#endif
/** Encode three bytes using base64 (RFC 3548)
 * @param triple three bytes that should be encoded
 * @param result buffer of four characters where the result is stored
 * */
static void _base64_encode_triple(unsigned char triple[3], char result[4])
{
    int iTripleValue, i;
//APP_TRACE("Here");
    iTripleValue = triple[0];
    iTripleValue *= 256;
    iTripleValue += triple[1];
    iTripleValue *= 256;
    iTripleValue += triple[2];

    for (i=0; i<4; i++)
    {
        result[3-i] = base64Char[iTripleValue%64];
        iTripleValue /= 64;
    }
}

/* encode an array of bytes using Base64 (RFC 3548)
* @param source the source buffer
* @param sourcelen the length of the source buffer
* @param target the target buffer
* @return total number of characters on success, 0 otherwise
*/
int encdBase64(uchar * source, int sourcelen, uchar * target, int * outLen)
//static int base64_encode(unsigned char *source, int sourcelen, char *target)
{
    int iRet = 0;

    while (sourcelen >= 3)
    {
        _base64_encode_triple(source, (char *)target);
        sourcelen -= 3;
        source += 3;
        target += 4;
        iRet = iRet + 4;
    }
    /* encode the last one or two characters */
    if (sourcelen > 0)
    {
        unsigned char temp[3];
        memset(temp, 0, sizeof(temp));
        memcpy(temp, source, sourcelen);
        _base64_encode_triple(temp, (char *)target);
        target[3] = '=';
        if (sourcelen == 1)
        target[2] = '=';
        target += 4;
        iRet = iRet + 4;
    }
    /* terminate the string */
    target[0] = 0;

    //debug_sprintf((szDebugMessage, "%s - The Encoded data is : %s", __FUNCTION__, target - iRet));
    //LIB_TRACE(szDebugMessage);

    *outLen = iRet;

    return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: calcFingerPrint
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int calcFingerPrint(char* pubKey, char* fingerPrint)
{
	int			   rv 				  = SUCCESS;
	int			   i				  = 0;
	int			   iLength 			  = 0;
	int			   iDecodedDataLen	  = 0;
	char		   temp[4]			  = "";
	char 		   *pszDecodedData 	  = NULL;
	unsigned char  sha1Value[EVP_MAX_MD_SIZE];

//	EVP_MD_CTX 	   sha1ctx;

#ifdef DEBUG
	char			szDbgMsg[128] = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iDecodedDataLen = ((strlen((char *)pubKey) / 4) * 3 ) + 1;

	pszDecodedData = (char *)malloc(sizeof(char) * iDecodedDataLen);
	if(pszDecodedData == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc failed for decoded data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(pszDecodedData, 0x00, iDecodedDataLen);

	/* Decode the payload data */
	rv = dcdBase64((unsigned char *)pubKey, strlen((char *)pubKey), (unsigned char *)pszDecodedData, &iLength);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: dcdBase64 failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if (pszDecodedData != NULL)
		{
			free(pszDecodedData);
		}

		return FAILURE;
	}

	/* Calling the sha1 hash function for calculating the fingerprint
	 * Calculates the fingerprint and stores the value in sha1value
	 */
	SHA1((unsigned char *)pszDecodedData, iLength, sha1Value);

	for (i = 0; i < 20; i++)
	{
		sprintf(temp, "%02X", sha1Value[i]);
		strcat(fingerPrint, temp);
	}

	debug_sprintf(szDbgMsg, "%s:The sha1 value is: %s",__FUNCTION__, sha1Value);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s:The finger print value is: %s",__FUNCTION__, fingerPrint);
	APP_TRACE(szDbgMsg);

	if (pszDecodedData != NULL)
	{
		free(pszDecodedData);
	}
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: dcdbase64
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int dcdBase64(uchar * inpData, int inpLen, uchar * outData, int * outLen)
{
	int				iCount	= 0;
	int				jCount	= 0;
	int				kCount	= 0;
	int				twoByte	= 0;
	int				oneByte	= 0;
	unsigned char	iStr[4]	= "";
	unsigned char	oStr[3]	= "";
#ifdef DEBUG
//	int				iCnt		= 0;
//	char			szTmp[5]	= "";
	char			szDbgMsg[4096] = "";
#endif

#if 0
#ifdef DEBUG
	sprintf(szDbgMsg, "%s: input string = ", __FUNCTION__);
	for(iCnt = 0; iCnt < inpLen; iCnt++)
	{
		sprintf(szTmp, "%02X ", inpData[iCnt]);
		strcat(szDbgMsg, szTmp);
	}

	APP_TRACE(szDbgMsg);
#endif
#endif

	if((inpLen % 4) != 0)
	{
		/* FAILURE */
		debug_sprintf(szDbgMsg, "%s: base 64 decoding failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	while(iCount < inpLen)
	{
		iStr[0] = inpData[iCount++];
		iStr[1] = inpData[iCount++];
		iStr[2] = inpData[iCount++];
		iStr[3] = inpData[iCount++];

		for(kCount = 0; kCount < 4; kCount++)
		{
			if((iStr[kCount] >= 'A') && (iStr[kCount] <= 'Z'))
			{
				iStr[kCount] = iStr[kCount] - 'A' + 0;
			}
			else if((iStr[kCount] >= 'a') && (iStr[kCount] <= 'z'))
			{
				iStr[kCount] = (iStr[kCount] - 'a') + 26;
			}
			else if((iStr[kCount] >= '0') && (iStr[kCount] <= '9'))
			{
				iStr[kCount] = iStr[kCount] - '0' + 52;
			}
			else if(iStr[kCount] == '+')
			{
				iStr[kCount] = 62;
			}
			else if(iStr[kCount] == '/')
			{
				iStr[kCount] = 63;
			}
			else if(iStr[kCount] == '=')
			{
				if(kCount == 2)
				{
					twoByte = 1;
				}
				else
				{
					oneByte = 1;
				}

				iStr[kCount] = 0x00;
			}
			else
			{
				return FAILURE;
			}
		}

		oStr[0] = (((iStr[0] & 0x3F) << 2) | ((iStr[1] >> 4) & 0x03)) & 0xFF;
		oStr[1] = (((iStr[1] & 0x0F) << 4) | ((iStr[2] >> 2) & 0x0F)) & 0xFF;
		oStr[2] = (((iStr[2] & 0x03) << 6) | ((iStr[3]) & 0x3F)) & 0xFF;

		if(twoByte == 1)
		{
			/* End of input string with two empty bytes */
			outData[jCount++] = oStr[0];
		}
		else if(oneByte == 1)
		{
			/* End of input string with one empty byte */
			outData[jCount++] = oStr[0];
			outData[jCount++] = oStr[1];
		}
		else
		{
			/* Normal string working */
			outData[jCount++] = oStr[0];
			outData[jCount++] = oStr[1];
			outData[jCount++] = oStr[2];
		}
	}

	* outLen = jCount;

#if 0
#ifdef DEBUG
	sprintf(szDbgMsg, "%s: output string = ", __FUNCTION__);
	for(iCnt = 0; iCnt < *outLen; iCnt++)
	{
		sprintf(szTmp, "%02X ", outData[iCnt]);
		strcat(szDbgMsg, szTmp);
	}

	APP_TRACE(szDbgMsg);
#endif
#endif

	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: decryptDataWithAES
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int decryptDataWithAES(char *pszData, char *pszOutputData, char *pszAESKey, int Len)
{
	int			rv					= SUCCESS;
	int			iRequiredLen 		= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
	char		szAppLogDiag[300]	= "";
    AES_KEY 	dec_key;
    unsigned char iv_dec[AES_BLOCK_SIZE];

#ifdef DEBUG
	char		szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	AES_set_decrypt_key((unsigned char *)pszAESKey, 128, &dec_key); //We are using AES128 key

	memset(iv_dec, 0, AES_BLOCK_SIZE); //Taking it as zero vector

	AES_cbc_encrypt((unsigned char *)pszData, (unsigned char *)pszOutputData, Len, &dec_key, iv_dec, AES_DECRYPT);

	/*Daivik 4/Feb/2016 - Coverity - 67638 -  Following is Dead code which cannot execute */
#if 0
    if(rv != SUCCESS)
    {
    	if(iAppLogEnabled == 1)
    	{
    		strcpy(szAppLogData, "AES Decryption Error");
    		addAppEventLog(SCA, PAAS_ERROR, PROCESSING, szAppLogData, NULL);
    	}

    	rv = ERR_AES_DECRYPTION;
    }
#endif
#ifdef DEVDEBUG // Make sure this does not show up in normal debug log!

    if(strlen(pszOutputData) < 4000)
    {
    	debug_sprintf(szDbgMsg, "%s - DEVDEBUG - szDataDecryptionBlock with Padding ::: [%s]", __FUNCTION__, pszOutputData);
    	APP_TRACE(szDbgMsg);
    }
#endif

    /*
     * Need to unpad the data
     */

    debug_sprintf(szDbgMsg, "%s - Last Byte is [%d], so need to remove those many from last", __FUNCTION__, pszOutputData[Len-1]);
    APP_TRACE(szDbgMsg);

    iRequiredLen = Len - pszOutputData[Len-1];

    debug_sprintf(szDbgMsg, "%s - Required Length is %d", __FUNCTION__, iRequiredLen);
    APP_TRACE(szDbgMsg);

    if(iRequiredLen < 0)
    {
    	debug_sprintf(szDbgMsg, "%s - Some error in the incoming packet!!!", __FUNCTION__);
    	APP_TRACE(szDbgMsg);

    	if(iAppLogEnabled == 1)
    	{
    		strcpy(szAppLogData, "AES Decryption Error.");
    		strcpy(szAppLogDiag, "Error in Incoming Packet");
    		addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
    	}

    	rv = ERR_AES_DECRYPTION;
    	return rv;
    }

    memset(&pszOutputData[iRequiredLen], 0x00, pszOutputData[Len-1]);

#ifdef DEVDEBUG // Make sure this does not show up in normal debug log!
    if(strlen(pszOutputData) < 4000)
    {
    	debug_sprintf(szDbgMsg, "%s - DEVDEBUG - szDataDecryptionBlock ::: [%s]", __FUNCTION__, pszOutputData);
    	APP_TRACE(szDbgMsg);
    }
#endif
    debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
    APP_TRACE(szDbgMsg);

    return rv;
}

/*
 * ============================================================================
 * Function Name: encryptDataWithAES
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int encryptDataWithAES(char **pszData, char **pszOutputData, char *pszAESKey, int *piEncLen)
{
	int		rv					= SUCCESS;
    int 	inLen 				= 0;
    int 	iPadDataLen			= 0;
    char	*pszInputPaddedData = NULL;
    AES_KEY enc_key;
    unsigned char iv_enc[AES_BLOCK_SIZE];

#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
    if(strlen(*pszData) < 4000)
    {
    	debug_sprintf(szDbgMsg, "%s - DEVDEBUG - inputBuffer = [%s]", __FUNCTION__, *pszData);
    	APP_TRACE(szDbgMsg);
    }
#endif

    inLen = strlen(*pszData);

    iPadDataLen = 16 - (inLen % 16);

    debug_sprintf(szDbgMsg, "%s: Input Buffer Length[%d] Padded Length[%d]", __FUNCTION__, inLen, iPadDataLen);
    APP_TRACE(szDbgMsg);

    *piEncLen = iPadDataLen + inLen;

    debug_sprintf(szDbgMsg, "%s: Enc buffer size [%d]", __FUNCTION__, *piEncLen);
    APP_TRACE(szDbgMsg);

    pszInputPaddedData = (char *)realloc(*pszData, (*piEncLen + 1) * sizeof(char));
    if(pszInputPaddedData == NULL)
    {
    	debug_sprintf(szDbgMsg, "%s: Error while reallocating", __FUNCTION__);
    	APP_TRACE(szDbgMsg);
    	rv = FAILURE;
    	return rv;
    }

    memset(pszInputPaddedData+inLen, 0x00, (iPadDataLen + 1) * sizeof(char));

    *pszData = pszInputPaddedData;
    pszInputPaddedData = NULL;

    //Pad the inputbuffer using PKCS5 algorithm
    getPKCS5PaddedData(*pszData);

#ifdef DEVDEBUG
    if(strlen(*pszData) < 4000)
    {
    	debug_sprintf(szDbgMsg, "%s - DEVDEBUG - inputBuffer after padding = [%s]", __FUNCTION__, *pszData);
    	APP_TRACE(szDbgMsg);
    }
#endif

    /*
     * Allocate memory for Encrypted data
     * For AES Enc Data size is same as Clear Data size
     */
    *pszOutputData = (char *)malloc((*piEncLen + 1) * sizeof(char));
    if(*pszOutputData == NULL)
    {
    	debug_sprintf(szDbgMsg, "%s: Malloc failed for enc output buffer!!!", __FUNCTION__);
    	APP_TRACE(szDbgMsg);
    	rv = FAILURE;
    	return rv;
    }

    memset(*pszOutputData, 0x00, *piEncLen + 1);

    AES_set_encrypt_key((unsigned char *)pszAESKey, 128, &enc_key);

    memset(iv_enc, 0, AES_BLOCK_SIZE); //Taking it as zero vector

    AES_cbc_encrypt((unsigned char *)*pszData, (unsigned char *)*pszOutputData, *piEncLen, &enc_key, iv_enc, AES_ENCRYPT);

#ifdef DEVDEBUG
#if 0
    debug_sprintf(szDbgMsg, "%s - Printing the encrypted data!!!", __FUNCTION__);
    APP_TRACE(szDbgMsg); //Praveen_P1: Remove this later


	sprintf(szDbgMsg, "%s: Encrypted Data = ", __FUNCTION__);
	for(iCnt = 0; iCnt < iPadDataLen + inLen; iCnt++)
	{
		sprintf(szTmp, "%02X ", (*pszOutputData)[iCnt]);
		strcat(szDbgMsg, szTmp);
	}

	APP_TRACE(szDbgMsg);
#endif
#endif

    debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
    APP_TRACE(szDbgMsg);

    return rv;
}

/*
 * ============================================================================
 * Function Name: doesFileExist
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int doesFileExist(const char *szFullFileName)
{
	int			rv			= SUCCESS;
	struct stat	fileStatus;
#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

/*	debug_sprintf(szDbgMsg, "%s: Enter", __FUNCTION__);
	APP_TRACE(szDbgMsg);*/

	memset(&fileStatus, 0x00, sizeof(fileStatus));

	rv = stat(szFullFileName, &fileStatus);
	if(rv < 0)
	{
		if(errno == EACCES)
		{
			debug_sprintf(szDbgMsg, "%s: Not enough search permissions for %s",
												__FUNCTION__, szFullFileName);
		}
		else if (errno == ENAMETOOLONG)
		{
			debug_sprintf(szDbgMsg, "%s: Name %s is too long", __FUNCTION__,
																szFullFileName);
		}
		else
		{
			/* For ENOENT/ENOTDIR/ELOOP */
			debug_sprintf(szDbgMsg,
						"%s: File - [%s] (Name not correct)/(file doesnt exist)"
												, __FUNCTION__, szFullFileName);
		}
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		/* Check if the file is a regular file */
		if(S_ISREG(fileStatus.st_mode))
		{
			/* Check if the file is not empty */
			if(fileStatus.st_size <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: File [%s] is empty", __FUNCTION__,
																szFullFileName);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error: File [%s] is not a regular file"
												, __FUNCTION__, szFullFileName);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}
	}

/*	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);*/

	return rv;
}

/*
 * ============================================================================
 * Function Name: deleteFile
 *
 * Description	: This API is responsible for deleting the files from the given
 * 					paths (file name along with path to be specified in the
 * 					parameter)
 *
 * Input Params	: szFullFileName -> name of the file to be deleted
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int deleteFile(const char *szFullFileName)
{
	int		rv				= SUCCESS;
	char	szCmdBuf[256]	= "";
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	while(1)
	{
		if(szFullFileName == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty Params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		sprintf(szCmdBuf, "rm -f %s", szFullFileName);

		rv = local_svcSystem(szCmdBuf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to execute command [%s]",
														__FUNCTION__, szCmdBuf);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}

		break;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: findLRC
 *
 * Description	: The purpose of this function is to calculate the LRC of the
 * 					character string data passed as argument.
 *
 * Input Params	: data -> character data for which the LRC is to be calculated
 * 				  dataLen -> length of the data
 *
 * Output Params: LRC value
 * ============================================================================
 */
uchar findLrc(char *data, int dataLen)
{
	unsigned char	lrcChar	= 0;

	while(dataLen--)
	{
		lrcChar	^= *data++;
	}

	return lrcChar;
}

/*
 * ============================================================================
 * Function Name: asciiToHex
 *
 * Description	: This function is responsible for converting an ascii string
 * 					to comparable Hex data string.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int asciiToHex(unsigned char *dest, char *src, int length)
{
	int		iCnt	= 0;
	int		iLen	= 0;
	int		jCnt	= 0;
	int		hChar	= 0x00;
	int		lChar	= 0x00;

	iLen = strlen(src);
	if((length * 2) < iLen)
	{
		APP_TRACE("asciiToHex: Target length is less, cant convert");
		return FAILURE;
	}

	iLen = length * 2;

	for(iCnt = 0; iCnt < iLen; iCnt++)
	{
		/* get the MSB and LSB characters */
		hChar = src[iCnt++];
		lChar = src[iCnt];

		/* Convert the MSB & LSB from ascii to hexadecimal */
		hChar = ((hChar <= '9') && (hChar >= '0'))? (hChar - '0') :
				(((hChar >= 'a') && (hChar <= 'f'))? (hChar - 'a' + 10) :
				(hChar - 'A' + 10) );
		lChar = ((lChar <= '9') && (lChar >= '0'))? (lChar - '0') :
				(((lChar >= 'a') && (lChar <= 'f'))? (lChar - 'a' + 10) :
				(lChar - 'A' + 10) );

		dest[jCnt]	= 0x00;
		dest[jCnt] |= (hChar & 0x0F);
		dest[jCnt]	= dest[jCnt] << 4;
		dest[jCnt] |= (lChar & 0x0F);

		jCnt++;
	}

	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: stripSpaces
 *
 * Description	: This function is responsible for striping trailing and leading
 * 					spaces
 *
 * Input Params	: Buffer, option
 *
 * Output Params: Length / FAILURE
 * ============================================================================
 */
int stripSpaces(char *pszBuffer, int iStripMask)
{
    int  i, iLength;
    char *pchTemp;

    if (iStripMask & STRIP_TRAILING_SPACES)
    {
        // Strip off trailing spaces
        iLength = strlen(pszBuffer);
        for (i = iLength-1; ((*(pszBuffer+i) == ' ') ||(*(pszBuffer+i) == 0x09) ) && (i >= 0); i--)
        {
            ;
        }
        i++;
        *(pszBuffer+i) = 0;
    }

    if (iStripMask & STRIP_LEADING_SPACES)
    {
        // Strip off leading spaces
        for (pchTemp = pszBuffer; *pchTemp == ' ' || *pchTemp == 0x09 ; pchTemp++)
        {
            ;
        }
        strcpy(pszBuffer, pchTemp);
    }

    return (strlen(pszBuffer));
}

/*
 * ============================================================================
 * Function Name: strupper
 *
 * Description	: This function coverts string to uppercase
 *
 *
 * Input Params	: Input buffer
 *
 * Output Params: NONE
 * ============================================================================
 */
void strupper(char *temp)
{
	while(*temp!='\0')
	{
		*temp = toupper(*temp);
		temp++;
	}
}

/*
 * ============================================================================
 * Function Name: hexToAscii
 *
 * Description	: This function is responsible for converting a Hex data string
 * 					to a comparable ascii string
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int hexToAscii(char * dest, unsigned char *src, int length)
{
	int		iCnt	= 0;
	int		iLen	= 0;
	int		jCnt	= 0;
	int		hChar	= 0x00;
	int		lChar	= 0x00;

	if((length % 2) != 0) {
		APP_TRACE("hextoascii: Key length should be even, cant convert");
		return FAILURE;
	}

	iLen = length/2;

	for(iCnt = 0; iCnt < iLen; iCnt++)
	{
		/* Get the MSB & LSB values */
		lChar = (src[iCnt] & 0x0F);
		hChar = ((src[iCnt] >> 4) & 0x0F);

		/* Convert the characters */
		hChar = ((hChar >= 0) && (hChar <= 9)) ? (hChar + '0'):
				(hChar + 'a' - 10);

		lChar = ((lChar >= 0) && (lChar <= 9)) ? (lChar + '0'):
				(lChar + 'a' - 10);

		dest[jCnt++] = hChar;
		dest[jCnt++] = lChar;
	}
	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: calcMAC
 *
 * Description	: This function is responsible for calculating MAC of a data
 * 					using SHA 256 hashing algorithm, provided the key for the
 * 					conversion.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int calcMAC(uchar * key, int keyLen, uchar * data, int dataLen, uchar * mac,
																int * macLen)
{
	int		rv				= SUCCESS;
	uchar *	ptr				= NULL;
#ifdef DEBUG
	char	szDbgMsg[4096]	= "";
#ifdef DEVDEBUG
	char	szTmp[5]		= "";
	int		iCnt			= 0;
#endif
#endif

	if((dataLen > 0) && ((*macLen) > 0))
	{
#ifdef DEVDEBUG
		sprintf(szDbgMsg, "%s:%s: Data = ", DEVDEBUGMSG, __FUNCTION__);
		for(iCnt = 0; iCnt < dataLen; iCnt++)
		{
			sprintf(szTmp, "%02X ", data[iCnt]);
			strcat(szDbgMsg, szTmp);
		}
		APP_TRACE(szDbgMsg);

		sprintf(szDbgMsg, "%s:%s: Key = ", DEVDEBUGMSG, __FUNCTION__);
		for(iCnt = 0; iCnt < keyLen; iCnt++)
		{
			sprintf(szTmp, "%02X ", key[iCnt]);
			strcat(szDbgMsg, szTmp);
		}
		APP_TRACE(szDbgMsg);
#endif
		memset(mac, 0x00, *macLen);
		ptr = HMAC(EVP_sha256(),key,keyLen,data,dataLen,mac,(uint *)macLen);

		if(ptr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: MAC of the data failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}
		else
		{
#ifdef DEVDEBUG
		sprintf(szDbgMsg, "%s:%s: MAC'd Value = ", DEVDEBUGMSG, __FUNCTION__);
		for(iCnt = 0; iCnt < *macLen; iCnt++)
		{
			sprintf(szTmp, "%02X ", mac[iCnt]);
			strcat(szDbgMsg, szTmp);
		}
		APP_TRACE(szDbgMsg);
#endif
		}
	}
	else
	{
		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: rsaEncryptData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int	rsaEncryptData(uchar * rsaPEMStrKey, uchar * data, uchar * encrData,int * len)
{
	int		rv				= SUCCESS;
	int		keyLen			= 0;
	int		dataLen			= 0;
	int		encrLen			= 0;
	RSA *	rsaKey			= NULL;
	uchar	derStr[500]		= "";
	uchar	encStr[4096]	= "";
#ifdef DEBUG
	char	szDbgMsg[8192]	= "";
	char	szTmp[5]		= "";
	int		iCnt			= 0;
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	keyLen = strlen((char *)rsaPEMStrKey);
	dataLen = 16;

	if((keyLen <= 0) || (dataLen <= 0))
	{
		debug_sprintf(szDbgMsg, "%s: KeyLen or data len <= 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
#ifdef DEBUG
		sprintf(szDbgMsg, "%s: RSA PEM key = ", __FUNCTION__);
		for(iCnt = 0; iCnt < strlen((char *)rsaPEMStrKey); iCnt++)
		{
			sprintf(szTmp, "%02X ", rsaPEMStrKey[iCnt]);
			strcat(szDbgMsg, szTmp);
		}
		APP_TRACE(szDbgMsg);
#endif
		/* Decode the rsaPEM key string to rsa DER key string */
		rv = dcdBase64(rsaPEMStrKey, strlen((char *)rsaPEMStrKey), derStr,
																	&keyLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: dcdBase64 failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		else
		{
#ifdef DEBUG

			sprintf(szDbgMsg, "%s: RSA DER key = ", __FUNCTION__);
			for(iCnt = 0; iCnt < keyLen; iCnt++)
			{
				sprintf(szTmp, "%02X ", derStr[iCnt]);
				strcat(szDbgMsg, szTmp);
			}
			APP_TRACE(szDbgMsg);
#endif
		}

		/* Load the RSA key */
		if( (rsaKey = loadRSAKey(derStr, keyLen)) == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: loadRSAKey failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: loadRSAKey success", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(rsaKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: RSA KEY is NULL - BAD", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

#ifdef DEBUG
		sprintf(szDbgMsg, "%s: Data (MAC_Key) = ", __FUNCTION__);
		for(iCnt = 0; iCnt < dataLen; iCnt++)
		{
			sprintf(szTmp, "%02X ", data[iCnt]);
			strcat(szDbgMsg, szTmp);
		}
		APP_TRACE(szDbgMsg);
#endif

		debug_sprintf(szDbgMsg, "%s: Key[%s] that is encrypted", __FUNCTION__, data);
		APP_TRACE(szDbgMsg);

		/* Encrypt the data */
		encrLen = RSA_public_encrypt(dataLen, data, encStr, rsaKey,
															RSA_PKCS1_PADDING);
		debug_sprintf(szDbgMsg, "%s: After RSA_public_encrypt", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: encrLen %d", __FUNCTION__, encrLen);
		APP_TRACE(szDbgMsg);

		if(encrLen > 0)
		{
#ifdef DEBUG
			sprintf(szDbgMsg, "%s: RSA encrypted data = ", __FUNCTION__);
			for(iCnt = 0; iCnt < encrLen; iCnt++)
			{
				sprintf(szTmp, "%02X ", encStr[iCnt]);
				strcat(szDbgMsg, szTmp);
			}
			APP_TRACE(szDbgMsg);
#endif
			/* encode the encrypted data */
			if(SUCCESS != (rv = encdBase64(encStr, encrLen, encrData, len)))
			{
				debug_sprintf(szDbgMsg, "%s: encdBase64 failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
			}
			else
			{
#ifdef DEBUG
				sprintf(szDbgMsg, "%s: Base 64 encoded RSA encrypted data = ",
																__FUNCTION__);
				for(iCnt = 0; iCnt < *len; iCnt++)
				{
					sprintf(szTmp, "%02X ", encrData[iCnt]);
					strcat(szDbgMsg, szTmp);
				}
				APP_TRACE(szDbgMsg);
#endif
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: encrLen < 0, failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadRSAKey
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
RSA * loadRSAKey(const uchar * key, int size)
{
	RSA *	tmpRSAKey		= NULL;
#ifdef DEBUG
	char	szDbgMsg[4096]	= "";
#endif

	tmpRSAKey = (RSA *)d2i_RSA_PUBKEY(&tmpRSAKey, &key, size);

	if(tmpRSAKey == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while loading the RSA key!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: BN_bn2hex(tmpRSAKey->n) = %s", __FUNCTION__,
													BN_bn2hex(tmpRSAKey->n));
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: BN_bn2hex(tmpRSAKey->e) = %s", __FUNCTION__,
													BN_bn2hex(tmpRSAKey->e));
	APP_TRACE(szDbgMsg);

	return tmpRSAKey;
}

/*
 * ============================================================================
 * Function Name: LogData
 *
 * Description	: Kept for compilation to be possible with libtcpip library
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void LogData()
{
	/* Kept for compilation to be possible with libtcpip library */
}

/*
 * ============================================================================
 * Function Name: formatAmount
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void formatAmount(char * szAmount, char * szFmtAmt, AMT_SIGN sign)
{
	char *  pszTmpAmt		= NULL;
	char	szTmpAmt[11]	= ""; /* { 1 + (6 + 1 + 2) } + 1  i.e { '-'XXXXXX.XX }*/

#ifdef DEBUG
	//char	szDbgMsg[4096]	= "";
#endif
	
	memset(szFmtAmt, 0x00, 11);
	
	pszTmpAmt = szAmount;	
	if(pszTmpAmt[0] == '-')
	{
		pszTmpAmt++;

		//debug_sprintf(szDbgMsg, "%s: INSIDE if Amount = %s", __FUNCTION__, pszTmpAmt);
		//APP_TRACE(szDbgMsg);
	}
	else 
	{
		//debug_sprintf(szDbgMsg, "%s: INSIDE else Amount = %s", __FUNCTION__, pszTmpAmt);
		//APP_TRACE(szDbgMsg);
	}	
	switch(sign)
	{
	case AMT_POSITIVE:
		sprintf(szTmpAmt, "%s", pszTmpAmt);
		break;

	case AMT_NEGATIVE:
		sprintf(szTmpAmt, "-%s", pszTmpAmt);
		break;
	}

	sprintf(szFmtAmt, "%9s", szTmpAmt);

	return;
}

#if 0
/*
 * ============================================================================
 * Function Name: validateAmountField
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int validateAmountField(char * szFieldVal)
{
	int		rv				= SUCCESS;
	int		iValLen			= 0;
	int		iTmpLen			= 0;
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iValLen = strlen(szFieldVal);
		if(iValLen <= 0) {
			debug_sprintf(szDbgMsg, "%s: No value present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Check for characters */
		if(strspn(szFieldVal, "1234567890.") != iValLen)
		{
			debug_sprintf(szDbgMsg, "%s: Characters other than 0-9 & . found",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		cCurPtr = strchr(szFieldVal, '.');
		if(cCurPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Decimal character not present",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		cNxtPtr = strchr(cCurPtr + 1, '.');
		if(cNxtPtr != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Multiple instances of dec character",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Validate the characteristic xyz.00 */
		iTmpLen = cCurPtr - szFieldVal;
		if( (iTmpLen < 1) || (iTmpLen > 5) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid length [%d] before decimal",
														__FUNCTION__, iTmpLen);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Validate the mantissa 0.xy */
		iTmpLen = iValLen - iTmpLen - 1;
		if(iTmpLen != 2)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid length [%d] after decimal",
														__FUNCTION__, iTmpLen);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: validateNumericField
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int validateNumericField(char * szFieldVal)
{
	int		rv					= SUCCESS;
	int		iValLen				= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif
//
//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));


	while(1)
	{
		iValLen = strlen(szFieldVal);
		if(iValLen <= 0) {
			debug_sprintf(szDbgMsg, "%s: No value present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Check for characters */
		if(strspn(szFieldVal, "1234567890") != iValLen)
		{
			debug_sprintf(szDbgMsg, "%s: Characters other than 0-9 found",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "XML Field Error, Non Numeric Characters Found.");
				strcpy(szAppLogDiag, "Please Check the XML Field Value");
				addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}

		break;
	}


//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: validateBooleanField
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int validateBooleanField(char * szFieldVal)
{
	int		rv				= SUCCESS;
	int		iValLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	while(1)
	{
		iValLen = strlen(szFieldVal);
		if(iValLen <= 0) {
			debug_sprintf(szDbgMsg, "%s: No value present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Check for characters (1, 0, TRUE, FALSE, T, F, YES, NO) */
		if(strspn(szFieldVal, "10TRUEYSFALNO") != iValLen)
		{
			debug_sprintf(szDbgMsg, "%s: Characters other than 0-9 found",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(strcmp(szFieldVal, "1") == SUCCESS)
		{
			rv = SUCCESS;
		}
		else if(strcmp(szFieldVal, "0") == SUCCESS)
		{
			rv = SUCCESS;
		}
		else if(strcmp(szFieldVal, "T") == SUCCESS)
		{
			rv = SUCCESS;
		}
		else if(strcmp(szFieldVal, "F") == SUCCESS)
		{
			rv = SUCCESS;
		}
		else if(strcmp(szFieldVal, "YES") == SUCCESS)
		{
			rv = SUCCESS;
		}
		else if(strcmp(szFieldVal, "NO") == SUCCESS)
		{
			rv = SUCCESS;
		}
		else if(strcmp(szFieldVal, "TRUE") == SUCCESS)
		{
			rv = SUCCESS;
		}
		else if(strcmp(szFieldVal, "FALSE") == SUCCESS)
		{
			rv = SUCCESS;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Value [%s] is not a valid BOOL value",
													__FUNCTION__, szFieldVal);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}

		break;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: checkForExpiry
 *
 * Description	: This function is used to compare the month and year passed as
 * 					parameters with the local month and year of the device to
 * 					check if the card for which the mon and year were passed is
 * 					expired
 *
 * Input Params	: 	pszMon	-> month value (mm format)
 * 					pszYear	-> year value (yy format)
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int checkForExpiry(char * pszMon, char * pszYear)
{
	int			rv				= SUCCESS;
	int			iExpMon			= 0;
	int			iExpYear		= 0;
	int			iCurMon			= 0;
	int			iCurYear		= 0;
	time_t		rawTime			= 0;
	struct tm *	ptm				= NULL;
#ifdef DEBUG
	char		szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((pszMon == NULL) || (pszYear == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: Card Expiry month = [%s], year = [%s]",
								DEVDEBUGMSG, __FUNCTION__, pszMon, pszYear);
		APP_TRACE(szDbgMsg);
#endif

		if(strspn(pszMon, "0123456789") != strlen(pszMon))
		{
			debug_sprintf(szDbgMsg, "%s: Non-numeric characters in month",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(strspn(pszYear, "0123456789") != strlen(pszYear))
		{
			debug_sprintf(szDbgMsg, "%s: Non-numeric characters in year",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		iExpMon = atoi(pszMon);
		iExpYear= atoi(pszYear);

		if( (iExpMon < 1) || (iExpMon > 12) )
		{
#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s:%s: Invalid Month value = [%02d]",
										DEVDEBUGMSG, __FUNCTION__, iExpMon);
			APP_TRACE(szDbgMsg);
#else
			debug_sprintf(szDbgMsg, "%s: Invalid Month value", __FUNCTION__);
			APP_TRACE(szDbgMsg);
#endif
			rv = FAILURE;
			break;
		}

		/* Get the current date and time */
		time(&rawTime);
		ptm = (struct tm *) localtime(&rawTime);
		iCurMon = ptm->tm_mon + 1;
		iCurYear = (ptm->tm_year + 1900) - 2000;

		debug_sprintf(szDbgMsg, "%s: Current month = [%02d], year = [%02d]",
											__FUNCTION__, iCurMon, iCurYear);
		APP_TRACE(szDbgMsg);

		/* Do the comparison to validate the expiry date */
		if(iCurYear > iExpYear)
		{
			debug_sprintf(szDbgMsg, "%s: Card Expired (year)", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
		else if(iCurYear == iExpYear)
		{
			if(iCurMon > iExpMon)
			{
				debug_sprintf(szDbgMsg,"%s: Card Expired (month)",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: acquireMutexLock
 *
 * Description	: This function is used to acquire a mutex for sychronization
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
void acquireMutexLock(pthread_mutex_t * pMutex, const char * pszName)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	rv = pthread_mutex_lock(pMutex);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to acquire %s mutex, errno = %d",
												__FUNCTION__, pszName, errno);
		APP_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: releaseMutexLock
 *
 * Description	: This function is used to release an already acquired mutex
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
void releaseMutexLock(pthread_mutex_t * pMutex, const char * pszName)
{
	int					rv				= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	rv = pthread_mutex_unlock(pMutex);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to release %s mutex, errno = %d",
												__FUNCTION__, pszName, errno);
		APP_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: releaseAcquiredMutexLock
 *
 * Description	: This function is used to check if Mutext is acquired
 * 				  already, if acquired release the mutex
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
void releaseAcquiredMutexLock(pthread_mutex_t * pMutex, const char * pszName)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	rv = pthread_mutex_trylock(pMutex);
	if(rv == SUCCESS || rv == EDEADLK || rv == EBUSY)
	{
		if(rv == EDEADLK)
		{
			debug_sprintf(szDbgMsg, "%s: The %s mutex is already held by the calling thread, we will unlock it", __FUNCTION__, pszName);
			APP_TRACE(szDbgMsg);
		}
		else if(rv == EBUSY) //Praveen_P1: Not sure of EBUSY we can unlock by this thread, without doing this one still it hangs sometimes when secondary post CANCEL came
		{
			debug_sprintf(szDbgMsg, "%s: The %s mutex is currently locked by another thread", __FUNCTION__, pszName);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: The %s mutex is not held by the calling thread, we will unlock it", __FUNCTION__, pszName);
			APP_TRACE(szDbgMsg);
		}


		rv = pthread_mutex_unlock(pMutex);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to release %s mutex, errno = %d",__FUNCTION__, pszName, errno);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to trylock %s mutex, errno = %d, rv = %d",__FUNCTION__, pszName, errno, rv);
		APP_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: getMaskedPANForBAPI
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void getMaskedPANForBAPI(char *pszClearPAN, char *pszMaskedPAN)
{
	int  iPANLength       		= 0;
	int	 iLastDigitsClear 		= 4;
	int  iDigitsToMask    		= 0;
	int	 iIndex				    = 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iPANLength = strlen(pszClearPAN);

	debug_sprintf(szDbgMsg, "%s: Length of the PAN is %d", __FUNCTION__, iPANLength);
	APP_TRACE(szDbgMsg);

	iDigitsToMask = iPANLength - iLastDigitsClear; //4 from end and excluding the number of clear digits

	debug_sprintf(szDbgMsg, "%s: Number of Digits to Mask is %d", __FUNCTION__, iDigitsToMask);
	APP_TRACE(szDbgMsg);

	strcpy(pszMaskedPAN, pszClearPAN);

	for (iIndex = 0; iIndex < iDigitsToMask; iIndex++)
	{
		pszMaskedPAN[iIndex] = '*';
	}

	debug_sprintf(szDbgMsg, "%s: Masked PAN is [%s]", __FUNCTION__, pszMaskedPAN);
	APP_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: getMaskedPAN
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void getMaskedPAN(char *pszClearPAN, char *pszMaskedPAN)
{
	int  iPANLength       		= 0;
	int  iFirstDigitsInClear    = 6;
	int	 iLastDigitsClear 		= 4;
	int  iDigitsToMask    		= 0;
	int	 iIndex				    = 0;
	int  iTempIndex				= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iPANLength = strlen(pszClearPAN);

	debug_sprintf(szDbgMsg, "%s: Length of the PAN is %d", __FUNCTION__, iPANLength);
	APP_TRACE(szDbgMsg);

	iDigitsToMask = iPANLength - iLastDigitsClear - iFirstDigitsInClear; //4 from end and excluding the number of clear digits

	if(iDigitsToMask < 1)
	{
		debug_sprintf(szDbgMsg, "%s: Adjusting the Digits to Mask", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iFirstDigitsInClear = iFirstDigitsInClear - 1;
		iLastDigitsClear    = iLastDigitsClear - 1;
		iDigitsToMask = iPANLength - iLastDigitsClear - iFirstDigitsInClear;
	}

	debug_sprintf(szDbgMsg, "%s: Number of Digits to Mask is %d", __FUNCTION__, iDigitsToMask);
	APP_TRACE(szDbgMsg);

	strncpy(pszMaskedPAN, pszClearPAN, iPANLength - iDigitsToMask - iLastDigitsClear);

	iIndex = strlen(pszMaskedPAN);

	for (iTempIndex = 1 ; iTempIndex <= iDigitsToMask; iTempIndex++)
	{
		pszMaskedPAN[iIndex] = '*';
		iIndex ++;
	}

	strcat(&pszMaskedPAN[iIndex], pszClearPAN + iFirstDigitsInClear + iDigitsToMask);

	debug_sprintf(szDbgMsg, "%s: Masked PAN is [%s]", __FUNCTION__, pszMaskedPAN);
	APP_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: getMaskedTRACK2
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void getMaskedTRACK2(char *pszClearTrack2, char *pszMaskedTrack2)
{
	int  iTrack2Length     		= 0;
	int  iFirstDigitsInClear    = 6;
	int	 iLastDigitsClear 		= 4;
	int  iDigitsToMask    		= 0;
	int	 iIndex				    = 0;
	int  iTempIndex				= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTrack2Length = strlen(pszClearTrack2);

	debug_sprintf(szDbgMsg, "%s: Length of the Track2 is %d", __FUNCTION__, iTrack2Length);
	APP_TRACE(szDbgMsg);

	iDigitsToMask = iTrack2Length - iLastDigitsClear - iFirstDigitsInClear; //4 from end and excluding the number of clear digits

	if(iDigitsToMask < 1)
	{
		debug_sprintf(szDbgMsg, "%s: Adjusting the Digits to Mask", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iFirstDigitsInClear = iFirstDigitsInClear - 1;
		iLastDigitsClear    = iLastDigitsClear - 1;
		iDigitsToMask = iTrack2Length - iLastDigitsClear - iFirstDigitsInClear;
	}

	debug_sprintf(szDbgMsg, "%s: Number of Digits to Mask is %d", __FUNCTION__, iDigitsToMask);
	APP_TRACE(szDbgMsg);

	strncpy(pszMaskedTrack2, pszClearTrack2, iTrack2Length - iDigitsToMask - iLastDigitsClear);

	iIndex = strlen(pszMaskedTrack2);

	for (iTempIndex = 1 ; iTempIndex <= iDigitsToMask; iTempIndex++)
	{
		pszMaskedTrack2[iIndex] = '*';
		iIndex ++;
	}

	strcat(&pszMaskedTrack2[iIndex], pszClearTrack2 + iFirstDigitsInClear + iDigitsToMask);

	debug_sprintf(szDbgMsg, "%s: Masked Track2 is [%s]", __FUNCTION__, pszMaskedTrack2);
	APP_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: getMaskedCVV
 *
 * Description	:This Function will mask the CVV Field.
 *
 * Input Params	:ClearCVV, MaskedCVV
 *
 * Output Params:
 * ============================================================================
 */
void getMaskedCVV(char *pszClearCVV, char *pszMaskedCVV)
{
	int  iDigitsToMask    		= 0;
	int	 iIndex				    = 0;
	int  iTempIndex				= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iDigitsToMask = strlen(pszClearCVV);

	debug_sprintf(szDbgMsg, "%s: Length of the PAN is %d", __FUNCTION__, iDigitsToMask);
	APP_TRACE(szDbgMsg);

	strncpy(pszMaskedCVV, pszClearCVV, iDigitsToMask);

	for (iTempIndex = 1 ; iTempIndex <= iDigitsToMask; iTempIndex++)
	{
		pszMaskedCVV[iIndex] = '*';
		iIndex ++;
	}

	debug_sprintf(szDbgMsg, "%s: Masked PAN is [%s]", __FUNCTION__, pszMaskedCVV);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: toUTF
 *
 * Description	: just a patch code for the multi-language compatibility...
 * 					TODO Find a proper fix for the whole issue asap
 *
 * Input Params	: 
 *
 * Output Params: 
 * ============================================================================
 */
static int toUTF(ushort utfShort, uchar * pszOut)
{
	int		iLen			= 0;
	uchar	utfArr[4]		= "";	/* 3 + 1 */
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: utfShort = [%04X]", __FUNCTION__, utfShort);
	APP_TRACE(szDbgMsg);

	if((utfShort == 0xFEFF) || (utfShort == 0xFFFE))
	{
		/* ignore BOM character */
		iLen = 0;
	}
	else if (utfShort < 0x80)					// One byte UTF-8
	{
		utfArr[0] = (uchar) utfShort;			// Use number as is

		iLen = 1;
	}
	else if (utfShort < 0x0800)					// Two byte UTF-8
	{
		utfArr[1] = 0x80 + (utfShort & 0x3F);	// Least Significant 6 bits
		utfShort = utfShort >> 6;				// Shift utfShort right 6 bits
		utfArr[0] = 0xC0 + (utfShort & 0x1F);	// Use 5 remaining bits

		debug_sprintf(szDbgMsg, "%s: -- %02X - %02X", __FUNCTION__, utfArr[0],
																utfArr[1]);
		APP_TRACE(szDbgMsg);

		iLen = 2;
	}
	else										// Three byte UTF-8
	{
		utfArr[2] = 0x80 + (utfShort & 0x3F);	// Least Significant 6 bits
		utfShort = utfShort >> 6;				// Shift utfShort right 6 bits
		utfArr[1] = 0x80 + (utfShort & 0x3F);	// Use next 6 bits
		utfShort = utfShort >> 6;				// Shift utfShort right 6 bits
		utfArr[0] = 0xE0 + (utfShort & 0x0F);	// Use 4 remaining bits

		iLen = 3;
	}

	if(iLen > 0)
	{
		memcpy(pszOut, utfArr, iLen);	// Copy UTF-8 bytes
	}

	return iLen;
}

typedef union TwoByteShort
{
	uchar	b1b2[2];
	ushort	shVal;
}
TWOBYTE_SHORT;

/*
 * ============================================================================
 * Function Name: convStrToUTFEscStr
 *
 * Description	: just a patch code for the multi-language compatibility...
 * 					TODO Find a proper fix for the whole issue asap
 *
 * Input Params	: 
 *
 * Output Params: 
 * ============================================================================
 */
void convStrToUTFEscStr(char * szInp, char * szOut)
{
	int				iCnt			= 0;
	int				jCnt			= 0;
	TWOBYTE_SHORT	byte2Short;

	for(iCnt = 0; szInp[iCnt] != '\0'; iCnt++)
	{
		if(szInp[iCnt] > 0x7F)
		{
			byte2Short.b1b2[1] = 0x00;
			byte2Short.b1b2[0] = szInp[iCnt];

			jCnt = toUTF(byte2Short.shVal, (uchar *)szOut);
		}
		else
		{
			*szOut = szInp[iCnt];
			jCnt = 1;
		}

		szOut += jCnt;
	}

	return;
}

/*
 * ============================================================================
 * Function Name: genTimeStamp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int genTimeStamp(char * pszTS, int iTSFmt)
{
	int			rv				= SUCCESS;
	time_t		rawTime			= 0;
	struct tm *	ptm				= NULL;
#ifdef DEBUG
	char		szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	if(pszTS == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: --- NULL Pointer ---", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}

	while(1)
	{
		/* Get the current date and time */
		time(&rawTime);
		ptm = (struct tm *) localtime(&rawTime);

		switch(iTSFmt)
		{
		case 1: /* YYYYMMDDHHMMSS */
			sprintf(pszTS, "%04d%02d%02d%02d%02d%02d", ptm->tm_year + 1900,
							ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour,
							ptm->tm_min, ptm->tm_sec);
			break;

		case 2: /* YYMMDDHHMMSS */
			sprintf(pszTS, "%02d%02d%02d%02d%02d%02d", ptm->tm_year + 1900-2000,
							ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour,
							ptm->tm_min, ptm->tm_sec);
			break;

		case 3: /* DDMMYYYY */
			sprintf(pszTS, "%02d%02d%04d", ptm->tm_mday, ptm->tm_mon + 1,
							ptm->tm_year + 1900);
			break;

		case 4: /* HHMMSS */
			sprintf(pszTS, "%02d%02d%02d",ptm->tm_hour,ptm->tm_min,ptm->tm_sec);
			break;

		case 5: /* YYYY-MM-DDThh:mm:ss */
			sprintf(pszTS, "%04d-%02d-%02dT%02d:%02d:%02d", ptm->tm_year + 1900,
							ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour,
							ptm->tm_min, ptm->tm_sec);
			break;

		case 6: /* HH:MM:SS - 24 hr format*/
			sprintf(pszTS, "%02d:%02d:%02d", ptm->tm_hour, ptm->tm_min,
																ptm->tm_sec);
			break;

		case 7: /* hh:mm:ss - 12 hr format */
			sprintf(pszTS, "%02d:%02d:%02d", ptm->tm_hour, ptm->tm_min,
																ptm->tm_sec);
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

#ifdef DEBUG
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: Time stamp = [%s]",__FUNCTION__,pszTS);
			APP_TRACE(szDbgMsg);
		}
#endif

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: alignString
 *
 * Description	: Adjusts the string to given alignment
 *
 *
 * Input Params	: String buffer, Alignment, Maxlen of the string
 *
 * Output Params: None
 * ============================================================================
 */
void alignString(char* szString, char* align, int iPosition, int iMaxLen)
{
	char *pszTempString = NULL;
	char *pcTemp        = NULL;
	char  *szKey        = NULL;
	char  *szValue      = NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: -----entered-----",__FUNCTION__);
	//APP_TRACE(szDbgMsg);

	if(strcasecmp(align, "left") == SUCCESS)
	{
		alignStringToLeft(szString, iMaxLen);
	}
	else if(strcasecmp(align, "right") == SUCCESS)
	{
		alignStringToRight(szString, iMaxLen);
	}
	else if(strcasecmp(align, "center") == SUCCESS)
	{
		alignStringToCenter(szString, iMaxLen);
	}
	else if(strcasecmp(align, "custom") == SUCCESS)
	{
		pszTempString = (char *)malloc((iMaxLen + 1) * sizeof(char));

		if(pszTempString == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while allocation memory",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			return;
		}

		memset(pszTempString, ' ', iMaxLen); //Filling with buffer with spaces for the max len
		if((strlen(szString) + iPosition) > iMaxLen)
		{
			debug_sprintf(szDbgMsg, "%s: Buffer over shooting...should not happen!!!",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		strcpy(pszTempString + iPosition, szString); //Copying the line value from the specified value

		memset(szString, 0x00, iMaxLen);
		sprintf(szString, "%s", pszTempString);

		free(pszTempString);
	}
	else if(strcasecmp(align, "justify") == SUCCESS)
	{
		pcTemp = strchr(szString, ':');

		if(pcTemp != NULL)
		{
			szKey = (char *)malloc(iMaxLen * sizeof(char));
			szValue = (char *)malloc(iMaxLen * sizeof(char));

			if(szKey == NULL || szValue == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while allocation memory",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(szKey != NULL)
				{
					free(szKey);
				}
				if(szValue != NULL)
				{
					free(szValue);
				}
				return;
			}
			memset(szKey, 0x00, iMaxLen);
			memset(szValue, 0x00, iMaxLen);

			strncpy(szKey, szString, pcTemp - szString + 1);

			strcpy(szValue, pcTemp+1);

			debug_sprintf(szDbgMsg, "%s: Key [%s] Value[%s]",__FUNCTION__, szKey, szValue);
			APP_TRACE(szDbgMsg);

			memset(szString, 0x00, iMaxLen);
			//Making the key as left aligned and value as right aligned
			sprintf(szString, "%-*s%*s", (iMaxLen/2), szKey, (iMaxLen/2), szValue);

			if(szKey != NULL)
			{
				free(szKey);
			}
			if(szValue != NULL)
			{
				free(szValue);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain key and value...omitting this line",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(szString, 0x00, iMaxLen);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Alignment given, defaulting to LEFT",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		alignStringToLeft(szString, iMaxLen);
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%s]",__FUNCTION__, szString);
//	APP_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: alignStringToLeft
 *
 * Description	:Places the given string exterme left with total string of length
 * 			   received as input. Remaining characters are filled with whitespaces
 *
 * Input Params	:String and the output length
 *
 * Output Params:Output will be stored in the string received
 * ============================================================================
 */
void alignStringToLeft (char * szInp, int length)
{
	char* szOutput = (char*)malloc( length*sizeof(char)+1);
	memset ( szOutput, 0x00, length+1);
	sprintf (szOutput, "%-*s", length, szInp);
	strcpy ( szInp, szOutput);
	free(szOutput);
}

/*
 * ============================================================================
 * Function Name: alignStringToRight
 *
 * Description	:Places the given string exterme right with total string of length
 * 			   received as input. Remaining characters are filled with whitespaces
 *
 * Input Params	:String and the output buffer length
 *
 * Output Params:Output will be stored in the string received
 * ============================================================================
 */
void alignStringToRight (char * szInp, int length)
{
	char* szOutput = (char*)malloc( length*sizeof(char)+1);
	memset(szOutput, 0x00, length+1);
	sprintf(szOutput, "%*s", length, szInp);
	strcpy(szInp, szOutput);
	free(szOutput);
}

/*
 * ============================================================================
 * Function Name: alignStringToCenter
 *
 * Description	:Places the given string in middle with total string of length
 * 			   received as input. Remaining characters are filled with whitespaces
 *
 * Input Params	:String and the output length
 *
 * Output Params:Output will be stored in the string received
 * ============================================================================
 */
void alignStringToCenter (char * szInp, int length)
{
	char* szOutput = (char*)malloc( length*sizeof(char)+1);
	memset( szOutput, 0x00, length+1);
	sprintf(szOutput, "%*s", (strlen(szInp)+length)/2, szInp);
	sprintf(szInp, "%-*s", length, szOutput);
	free(szOutput);
}

/*
 * ============================================================================
 * Function Name: getDate
 *
 * Description	: This API gets the date in the specified format
 *
 *
 * Input Params	: Date Format, Buffer to fill the date value
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getDate(char *pszDateFormat, char *pszDate)
{
	int  iRetVal 				   = SUCCESS;
	char szDateFormatSpecifier[30] = "";
	time_t timer;
	struct tm* tm_info;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszDateFormat == NULL || pszDate == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid buffer is passed!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(szDateFormatSpecifier, 0x00, sizeof(szDateFormatSpecifier));

	iRetVal = getDateFormatSpecifier(pszDateFormat, szDateFormatSpecifier);

	if(iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while getting the date format specifier!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	debug_sprintf(szDbgMsg, "%s: Date Specifier is %s", __FUNCTION__, szDateFormatSpecifier);
	APP_TRACE(szDbgMsg);

	time(&timer);
	tm_info = localtime(&timer);

	/*AjayS2 25 Aug 2016 - Coverity - 83353 -  Max Offset(2nd argument below) allowed should not exceed the size of pszDate
	 *  As this fucntion is called from many places, setting the max offset as the minimum size of pszDate from all callers
	 */

	strftime(pszDate, 14, szDateFormatSpecifier, tm_info);

	debug_sprintf(szDbgMsg, "%s: Receipt Date in the required format is %s", __FUNCTION__, pszDate);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getTime
 *
 * Description	: This API gets the date in the specified format
 *
 *
 * Input Params	: Date Format, Buffer to fill the date value
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getTime(char *pszTimeFormat, char *pszTime)
{
	int  iRetVal 				   = SUCCESS;
	char szTimeFormatSpecifier[30] = "";
	time_t timer;
	struct tm* tm_info;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTimeFormat == NULL || pszTime == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid buffer is passed!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(szTimeFormatSpecifier, 0x00, sizeof(szTimeFormatSpecifier));

	iRetVal = getTimeFormatSpecifier(pszTimeFormat, szTimeFormatSpecifier);

	if(iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while getting the date format specifier!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	debug_sprintf(szDbgMsg, "%s: Time Format Specifier is %s", __FUNCTION__, szTimeFormatSpecifier);
	APP_TRACE(szDbgMsg);

	time(&timer);
	tm_info = localtime(&timer);

	/*AjayS2 25 Aug 2016 - Coverity - 83359 -  Max Offset(2nd argument below) allowed should not exceed the size of pszTime
	 *  As this fucntion is called from many places, setting the max offset as the minimum size of pszTime from all callers
	 */
	strftime(pszTime, 14, szTimeFormatSpecifier, tm_info);

	debug_sprintf(szDbgMsg, "%s: Time in the required format is %s", __FUNCTION__, pszTime);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getDateFormatSpecifier
 *
 * Description	: This API gives the format specifier to use in strftime for
 *                the required Date format
 *
 * Input Params	: Date Format, Buffer to fill the format specifier
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getDateFormatSpecifier(char *pszDateFormat, char *pszFormatSpecifier)
{
	int iRetVal = SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strcmp(pszDateFormat, "DD/MM/YY") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%d/%m/%y");
	}
	else if(strcmp(pszDateFormat, "MM/DD/YY") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%m/%d/%y");
	}
	else if(strcmp(pszDateFormat, "DD/MM/YYYY") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%d/%m/%Y");
	}
	else if(strcmp(pszDateFormat, "MM/DD/YYYY") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%m/%d/%Y");
	}
	else if(strcmp(pszDateFormat, "DD/MMM/YY") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%d/%b/%y");
	}
	else if(strcmp(pszDateFormat, "MMM/DD/YY") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%b/%d/%y");
	}
	else if(strcmp(pszDateFormat, "DD/MMM/YYYY") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%d/%b/%Y");
	}
	else if(strcmp(pszDateFormat, "MMM/DD/YYYY") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%b/%d/%Y");
	}
	else if(strcmp(pszDateFormat, "MMDDYYYY") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%m%d%Y");
	}
	else if(strcmp(pszDateFormat, "YYYY.MM.DD") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%Y.%m.%d");
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Date format... Defaulting", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		strcpy(pszFormatSpecifier, "%d/%m/%y");
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, pszFormatSpecifier);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getTimeFormatSpecifier
 *
 * Description	: This API gives the format specifier to use in strftime for
 *                the required Time format
 *
 * Input Params	: Time Format, Buffer to fill the format specifier
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getTimeFormatSpecifier(char *pszTimeFormat, char *pszFormatSpecifier)
{
	int iRetVal = SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

/*	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);*/

	if(strcmp(pszTimeFormat, "HH:MM:SS") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%H:%M:%S");
	}
	if(strcmp(pszTimeFormat, "HHMMSS") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%H%M%S");
	}
	else if(strcmp(pszTimeFormat, "hh:MM:SS") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%I:%M:%S %p"); //%p for AM or PM designation
	}
	else if(strcmp(pszTimeFormat, "HH:MM") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%H:%A");
	}
	else if(strcmp(pszTimeFormat, "hh:MM") == SUCCESS)
	{
		strcpy(pszFormatSpecifier, "%I:%M %p"); //%p for AM or PM designation
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Time format... Defaulting", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		strcpy(pszFormatSpecifier, "%H:%M:%S");
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, pszFormatSpecifier);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getUUID
 *
 * Description	: This API gives generates and gives the unique id number.
 *
 * Input Params	: char*
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getUUID(char* szUUID)
{
#if 0
	int   i 		 = 0;
	int	  iCnt		 = 0;
	int	  iTotCnt	 = 0;
#endif
	//char* szCmd 	 = "cat /proc/sys/kernel/random/uuid > uuid.txt";
	char  szTemp[60] = "";
	FILE* fptr	= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	/* Daivik : 6-Nov-2015 - In order to get the unique number, we used to first redirect uuid to a seperate file
	 * and then read from that file. We were also deleting the uuid temp file. This process takes up upto 800ms
	 * and then adds to overall transaction time, which we can avoid if we directly read from the random/uuid file.
	 * This is part of the performance enhacement changes.
	 */
	//local_svcSystem(szCmd);

	fptr = fopen("/proc/sys/kernel/random/uuid", "r");
	if (fptr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Cannot open file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	// CID-67251: 2-Feb-16: MukeshS3: Checking for return value
	if(fscanf(fptr,"%50s", szTemp) != 1)
	{
		debug_sprintf(szDbgMsg, "%s: Could not read data from file!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	strcpy(szUUID, szTemp);
	fclose(fptr);
	//local_svcSystem("rm uuid.txt");

	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: getDevSrlNum
 *
 * Description	: This API gives generates and gives the unique id number.
 *
 * Input Params	: char*
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getDevSrlNum(char* szDevSrlNum)
{
	char* cPtr = NULL;

	svcInfoSerialNum(szDevSrlNum);

	cPtr = strtok(szDevSrlNum, "-");
	//CID 67308 (#1 of 1): Dereference null return value (NULL_RETURNS). T_RaghavendranR1
	if(cPtr != NULL)
	{
		strcpy(szDevSrlNum, cPtr);
		while(cPtr != NULL)
		{
			cPtr = strtok(NULL, "-");
			if(cPtr != NULL)
			{
				strcat(szDevSrlNum, cPtr);
			}
		}
	}
	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: getPKCS5PaddedData
 *
 * Description	: This API gives pads the input buffer using PKCS5 padding.
 * 				  Make sure buffer that is being passed has enough space
 * 				  to perfom padding
 *
 * Input Params	: char*
 *
 * Output Params: void
 * ============================================================================
 */
void getPKCS5PaddedData(char *pszBuff)
{
	int iLen = 0;
	int	iPad = 0;

#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = strlen(pszBuff);

	/*
	 * PKCS5Padding schema is actually very simple. It follows the following rules:
	 *	The number of bytes to be padded equals to "16 - numberOfBytes(clearText) mod 16".
	 *	 So 1 to 16 bytes will be padded to the clear text data depending on the length of the clear text data.
	 *	All padded bytes have the same value - the number of bytes padded
	 *
	 */

	/*
	 * This function will perform PKCS5Padding schema,
		Example
		If numberOfBytes(clearText) mod 8 == 7, PM = M + 0x01
		If numberOfBytes(clearText) mod 8 == 6, PM = M + 0x0202
		If numberOfBytes(clearText) mod 8 == 5, PM = M + 0x030303
		...
		If numberOfBytes(clearText) mod 8 == 0, PM = M + 0x0808080808080808
	*/

	iPad = 16 - (iLen % 16);

	debug_sprintf(szDbgMsg, "%s: Input Buffer Length[%d] PadLen[%d]", __FUNCTION__, iLen, iPad);
	APP_TRACE(szDbgMsg);

	memset(pszBuff+iLen, iPad, iPad);

#ifdef DEVDEBUG
	if(strlen(pszBuff) < 4000)
	{
		debug_sprintf(szDbgMsg, "%s: DEVDEBUG - Padded Buffer[%s]", __FUNCTION__, pszBuff);
		APP_TRACE(szDbgMsg);
	}
#endif
	return;
}
/*
 * ============================================================================
 * Function Name: strToUpper
 *
 * Description	: This API converts lower case string to upper case string.
 *
 * Input Params	: char*
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
 
void strToUpper(char *s) 
{
    while (*s) 
	{
        if ((*s >= 'a' ) && (*s <= 'z')) 
		*s -= ('a'-'A');
        
		s++;
    }
}

/*
 * ============================================================================
 * Function Name: local_svcSystem
 *
 * Description	: This API is wrapper to svcSystem
 *
 * Input Params	: char*
 *
 * Output Params:
 * ============================================================================
 */
int local_svcSystem(const char *command)
{
	int retval;
	struct sigaction sigact, saveact;

	sigact.sa_handler = SIG_DFL;
	sigemptyset(&sigact.sa_mask);
	sigact.sa_flags = 0;
	//save the existing SIGCHLD handler in saveact and set the handler to SIG_DFL.
	sigaction(SIGCHLD, &sigact, &saveact);

	retval = system(command);

	//Restore the original saved handler.
	sigaction(SIGCHLD, &saveact, NULL);
	return retval;
}

/*
 * ============================================================================
 * Function Name: getFGNAmntWithDecimals
 *
 * Description	: Converts the Foreign Amount with decimal fields
 *
 * Input Params	: char*, int, char*
 *
 * Output Params:
 * ============================================================================
 */
void getFGNAmntWithDecimals(char* pszFGNAmount, int iMinorUnits, char* pszAmntWithDec)
{
	int	iCount				= iMinorUnits;
	int iTotAmount 			= 0;
	int	iDivisible			= 1;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszFGNAmount == NULL || pszAmntWithDec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed Returning", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}
	if(iMinorUnits > 0)
	{
		for(iCount = iMinorUnits; iCount; iCount--)
		{
			iDivisible *= 10;
		}

		iTotAmount = atoi(pszFGNAmount);

		sprintf(pszAmntWithDec, "%d.%0*d", iTotAmount/iDivisible, iMinorUnits, iTotAmount%iDivisible);
	}
	else
	{
		strcpy(pszAmntWithDec, pszFGNAmount);
	}

	debug_sprintf(szDbgMsg, "%s: Returning Amount %s", __FUNCTION__, pszAmntWithDec);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: strReplaceAll
 *
 * Description	: Replaces in the string 'input' all the occurrences of the 'search' string from with the destination string 'format'
 *
 * Input Params	: char*, const char*, const char*
 *
 * Output Params:
 * ============================================================================
 */
int strReplaceAll(char * input,  const char* search,  const char *format)
{
	int	rv		= 0;
	int iSearchLen = 0;
	int iFormatLen = 0;
	int iInputLen = 0;
	int iLen = 0;
	char *startPtr = NULL;
	char *curPtr = NULL;
	char *substr = NULL;
	char *source = input;
#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((input == NULL || search == NULL || format == NULL) ||
				(strlen(input) == 0))
		{
			debug_sprintf(szDbgMsg, "%s: Null params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		if((strlen(search) == 0) ||
				(strlen(search) < strlen(format)))
		{
			debug_sprintf(szDbgMsg, "%s: search length must be greater or equal to format length", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		iSearchLen = strlen(search);
		iFormatLen = strlen(format);
		iInputLen = strlen(input);

		startPtr = (char*)malloc(iInputLen + 1);
		if(startPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(startPtr, 0x00, iInputLen + 1);
		curPtr = startPtr;
		do
		{
			if((substr = strstr(input, search)) != NULL)
			{
				iLen = substr - input;
				memcpy(curPtr, input, iLen);	// copy first segment
				curPtr += iLen;
				memcpy(curPtr, format, iFormatLen);	// copy the format string
				curPtr += iFormatLen;
				input = substr + iSearchLen;
			}
			else
			{
				iLen = iInputLen - (input - source);
				memcpy(curPtr, input, iLen);
			}
		}while(substr != NULL);

		memset(source, 0x00, iInputLen + 1);
		memcpy(source, startPtr, strlen(startPtr));
		free(startPtr);
		startPtr = NULL;
		break;
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
/*
 * ============================================================================
 * End of file utils.c
 * ============================================================================
 */
