/*
 * =============================================================================
 * Filename    : xmlutils.c
 *
 * Application : Mx Point SCA
 *
 * Description : 
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */

#include <string.h>

#include "common/common.h"
#include "common/utils.h"
#include "common/xmlUtils.h"
#include "iniparser.h"
#include "common/cfgData.h"
#include "appLog/appLogAPIs.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

/* Static functions declarations */
static int addDataNode(VAL_LST_PTYPE, int, KEYVAL_PTYPE, int);
static int getCorrectNumVal(char *, char **, STR_DEF_PTYPE);
static int getCorrectNumRange(char * , char ** , NUM_RANGE_DEF_PTYPE );
static int getCorrectCharSetStrVal(char * , char ** , CHAR_SET_DEF_PTYPE );
static int getCorrectMaskedString(char *, char **, MASK_STR_DEF_PTYPE);
static int getCorrectStrVal(char *, char **, STR_DEF_PTYPE);
static int getCorrectAmtVal(char *, char **, AMT_DEF_PTYPE);
static int getCorrectBoolVal(char *, int **);
static int getCorrectDateVal(char *, char **);
static int getCorrectTimeVal(char *, char **);
static int getCorrectEnumValFromList(char *, LIST_INFO_PTYPE, int **);
static int getCorrectStrFromList(char **, LIST_INFO_PTYPE, int);
static int validateAmountField(char *, AMT_DEF_PTYPE);
static int parseXMLForListElem(xmlDoc *, xmlNode *, VARLST_INFO_PTYPE,
															VAL_LST_PTYPE *);
static int parseXMLForMultiListValues(xmlNode *,LIST_INFO_PTYPE,MLVAL_PTYPE *);
static int parseXMLForData(xmlDoc *, xmlNode *, KEYVAL_PTYPE, int);
static void getParamValues(xmlNode *, GENKEYVAL_PTYPE *);
static int parseXMLForAttribLst(xmlDoc *, xmlNode *, VARLST_INFO_PTYPE,
															VAL_NODE_PTYPE *);
static int getAttributes(xmlNode *, KEYVAL_PTYPE, int);
//static void freeParamValList(GENKEYVAL_PTYPE);

/* APIs for building the xml message */
static int buildXMLTreeForVarList(xmlDocPtr, xmlNode *, VAL_LST_PTYPE, VARLST_INFO_PTYPE);
static int buildXMLTreeForRecList(xmlNode *, char *, VAL_LST_PTYPE);
static int buildXMLTreeForMultiList(xmlNode *, MLVAL_PTYPE, LIST_INFO_PTYPE);
static int buildXMLTree(xmlDocPtr, xmlNode *, KEYVAL_PTYPE, int);

/* ADDED_NEW TO be TESTED: VDR */
static int parseXML(xmlDoc *, char *, char *, METADATA_PTYPE);
static int isXmlNodePropReq(char*);

/* External Functions */
extern int storeParamsInLocalCfgIniFile(GENKEYVAL_PTYPE, char *, dictionary *);
extern int getEmvParamKeyForIndex(int, char *, int);
extern int getHostProtocolFormat();
/*
 * ============================================================================
 * Function Name: parseXMLMsg
 *
 * Description	:
 *
 * Input Params	:  docPtr		->
 * 				   metaDataPtr 	->
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int parseXMLMsg(xmlDoc * docPtr, METADATA_PTYPE metaDataPtr)
{
	int			rv					= SUCCESS;
	int			iElemRead			= 0;
	int			iMandCnt			= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
	char		szAppLogDiag[300]	= "";
	xmlNode *	root				= NULL;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	iMandCnt = metaDataPtr->iMandCnt;

	debug_sprintf(szDbgMsg, "%s: Tot Cnt = [%d], Mand Cnt = [%d]",
					__FUNCTION__, metaDataPtr->iTotCnt, iMandCnt);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	/*
	 * Praveen_P1: Added the following check for the Report response
	 * We need not parse the SSI response for the REPORT command
	 */
	if(metaDataPtr->iTotCnt == 0)
	{
		debug_sprintf(szDbgMsg, "%s: No fields to parse the XML data",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		return rv;
	}

	while(1)
	{
		root = xmlDocGetRootElement(docPtr);
		if(root == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML root is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the xml for the required data */
		iElemRead = parseXMLForData(docPtr, root->xmlChildrenNode,
								metaDataPtr->keyValList, metaDataPtr->iTotCnt);
		if((iElemRead >= 0) && (iElemRead < iMandCnt))
		{
			debug_sprintf(szDbgMsg, "%s: All mand fields NOT present %d vs %d",
											__FUNCTION__, iElemRead, iMandCnt);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "All Mandatory Fields Not Filled in XML Request.");
				strcpy(szAppLogDiag, "Please Fill All the Mandatory Fields in the XML Request to Process.");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_FLD_REQD;
			break;
		}
		else if(iElemRead < 0)
		{
			/* Set the error value */
			rv = iElemRead;
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXMLMsg_1
 *
 * Description	:
 *
 * Input Params	: 
 *
 * Output Params:
 * ============================================================================
 */
int parseXMLMsg_1(char * szXMLMsg, char * szRoot, char * szNode, METADATA_PTYPE
																	pstMetaData)
{
	int			rv				= SUCCESS;
	int			iLen			= 0;
	xmlDoc *	docPtr			= NULL;

#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (szXMLMsg == NULL) || (szRoot == NULL) || (pstMetaData == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML message using the libxml API */
		iLen = strlen(szXMLMsg);

		debug_sprintf(szDbgMsg, "%s: The length of the message %d", __FUNCTION__, iLen);
		APP_TRACE(szDbgMsg);

		docPtr = xmlParseMemory(szXMLMsg, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse the xml message",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Parse the xml data retrieved from the message */
		rv = parseXML(docPtr, szRoot, szNode, pstMetaData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse xml", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: getXMLRootName
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getXMLRootName(char * szXMLMsg, char * pszRootName)
{
	int			rv					= SUCCESS;
	int			iLen				= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	xmlDoc *	docPtr				= NULL;
	xmlNode *	rootPtr				= NULL;
	char		szAppLogDiag[300]	= "";
	char		szAppLogData[300]	= "";
#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		if( (szXMLMsg == NULL) || (pszRootName == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML message using the libxml API */
		iLen = strlen(szXMLMsg);

		debug_sprintf(szDbgMsg, "%s: The length of the message %d", __FUNCTION__, iLen);
		APP_TRACE(szDbgMsg);

		docPtr = xmlParseMemory(szXMLMsg, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Parsing the XML Data Failed.");
				strcpy(szAppLogDiag, "XML Data is Empty");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid XML Data.");
				strcpy(szAppLogDiag, "XML Root Node Data is Empty");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_INV_XML_MSG;
			break;
		}
		strcpy(pszRootName, (char *)rootPtr->name);

		debug_sprintf(szDbgMsg, "%s: Root Name [%s]",__FUNCTION__, pszRootName);
		APP_TRACE(szDbgMsg);

		break;
	}
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXMLFile
 *
 * Description	:
 *
 * Input Params	: 
 *
 * Output Params:
 * ============================================================================
 */
int parseXMLFile(char * szXMLFile, char * szRoot, char * szNode, METADATA_PTYPE
																	pstMetaData)
{
	int			rv				= SUCCESS;
	xmlDoc *	docPtr			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (szXMLFile == NULL) || (szRoot == NULL) || (pstMetaData == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML file using the libxml API */
		docPtr = xmlParseFile(szXMLFile);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Parse the xml data retrieved from the file */
		rv = parseXML(docPtr, szRoot, szNode, pstMetaData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse xml", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXML
 *
 * Description	:
 *
 * Input Params	: 
 *
 * Output Params:
 * ============================================================================
 */
static int parseXML(xmlDoc * docPtr, char * szRoot, char * szNode,
													METADATA_PTYPE pstMetaData)
{
	int			rv					= SUCCESS;
	int			iElemRead			= 0;
	int			iMandCnt			= 0;
	char		szAppLogData[300]	= "";
	char		szAppLogDiag[300]	= "";
	int			iAppLogEnabled		= isAppLogEnabled();
	xmlNode *	rootPtr				= NULL;
	xmlNode *	curNode				= NULL;
#ifdef DEBUG
	char		szDbgMsg[128]		= "";
#endif

	iMandCnt = pstMetaData->iMandCnt;

	debug_sprintf(szDbgMsg, "%s: Tot Cnt = [%d], Mand Cnt = [%d]",
					__FUNCTION__, pstMetaData->iTotCnt, iMandCnt);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid XML Data.");
				strcpy(szAppLogDiag, "XML Data is Empty");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_INV_XML_MSG;
			break;
		}
		else if(strcmp((char *)rootPtr->name, szRoot) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Incorrect xml document", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Incorrect XML Data.");
				strcpy(szAppLogDiag, "Incorrect Root Element is Not Present");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Get the current node in the xml data from where the search of
		 * required fields should begin */
		if(szNode != NULL)
		{
			curNode = findNodeInXML(szNode, rootPtr);
			if(curNode == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Cant find [%s] node in xml file",
														__FUNCTION__, szNode);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Cannot Find the Node[%s] in XML Data", szNode);
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}

				rv = ERR_INV_XML_MSG;
				break;
			}
		}
		else
		{
			curNode = rootPtr->xmlChildrenNode;
		}

		/* Parse the xml data starting from the current node */
		iMandCnt = pstMetaData->iMandCnt;
		iElemRead = parseXMLForData(docPtr, curNode, pstMetaData->keyValList,
														pstMetaData->iTotCnt);
		if((iElemRead >= 0) && (iElemRead < iMandCnt))
		{
			debug_sprintf(szDbgMsg, "%s: All mand fields NOT present %d vs %d",
											__FUNCTION__, iElemRead, iMandCnt);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "All Mandatory Fields Not Filled in XML Request.");
				strcpy(szAppLogDiag, "Please Fill All the Mandatory Fields in the XML Request to Process.");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_FLD_REQD;
			break;
		}
		else if(iElemRead < 0)
		{
			/* Set the error value */
			rv = iElemRead;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: buildXMLTreeForMultiList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildXMLTreeForMultiList(xmlNode * curNode, MLVAL_PTYPE valListPtr,
													LIST_INFO_PTYPE listInfoPtr)
{
	int				rv				= SUCCESS;
	int				iVal			= 0;
	char *			szVal			= 0;
	MLVAL_PTYPE		curValPtr		= NULL;
	xmlNodePtr		childNodePtr	= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	curValPtr = valListPtr;

	while( (rv == SUCCESS) && (curValPtr != NULL) )
	{
		iVal = curValPtr->iLstVal;
		szVal = NULL;

		getCorrectStrFromList(&szVal, listInfoPtr, iVal);

		childNodePtr = xmlNewChild(curNode, NULL, BAD_CAST szVal, NULL);

		curValPtr = curValPtr->next;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildXMLTreeForVarList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildXMLTreeForVarList(xmlDocPtr docPtr, xmlNode * curNode, VAL_LST_PTYPE valLstPtr,
												VARLST_INFO_PTYPE metaInfoPtr)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iNodeType		= 0;
	int				iTmpCnt			= 0;
	VAL_NODE_PTYPE	valNodePtr		= NULL;
	xmlNodePtr		childNodePtr	= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while( (metaInfoPtr[iTmpCnt].nodeType) != -1)
	{
		iTmpCnt++;
	}
	valNodePtr = valLstPtr->start;
	while((rv == SUCCESS) && (valNodePtr != NULL))
	{
		while((iNodeType = metaInfoPtr[iCnt].nodeType) != -1)
		{	
			if(iNodeType == valNodePtr->type)
			{
				if(metaInfoPtr[iCnt].szNodeStr != NULL)
				{
					childNodePtr = xmlNewChild(curNode, NULL, BAD_CAST
										metaInfoPtr[iCnt].szNodeStr, NULL);
					if(isXmlNodePropReq(metaInfoPtr[iCnt].szNodeStr) == SUCCESS)
					{
						xmlNewProp(childNodePtr, BAD_CAST "Sequence", BAD_CAST "");
						xmlNewProp(childNodePtr, BAD_CAST "LoadID", BAD_CAST "");
						xmlNewProp(childNodePtr, BAD_CAST "LastModified", BAD_CAST "");
					}

				}
				else
				{
					childNodePtr = curNode;
					iTmpCnt--;
				}					
				rv = buildXMLTree(docPtr, childNodePtr, valNodePtr->elemList,
														valNodePtr->elemCnt);
				if(rv != SUCCESS)
				{
					break;
				}				
			}			
			iCnt++;						
		}
		if(iTmpCnt <= 0 )
		{
			debug_sprintf(szDbgMsg, "%s: Element in varlist are over",
												__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		iCnt = 0;
		valNodePtr = valNodePtr->next;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildXMLTreeForRecList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildXMLTreeForRecList(xmlNode * curNode, char * szName,
													VAL_LST_PTYPE valLstPtr)
{
	int				rv				= SUCCESS;
	VAL_NODE_PTYPE	valNodePtr		= NULL;
	xmlNodePtr		childNodePtr	= NULL;
	xmlNodePtr		textNodePtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	valNodePtr = valLstPtr->start;

	while(valNodePtr != NULL)
	{
		childNodePtr = xmlNewChild(curNode, NULL, BAD_CAST szName, NULL);

		if(valNodePtr->szVal != NULL)
		{
			textNodePtr = xmlNewText(BAD_CAST valNodePtr->szVal);
			xmlAddChild(childNodePtr, textNodePtr);
		}

		valNodePtr = valNodePtr->next;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

static int isXmlNodePropReq(char* key)
{
	if (strcmp(key, "basket") == SUCCESS)
		return SUCCESS;
	else if (strcmp(key, "lineItem") == SUCCESS)
		return SUCCESS;
	else if (strcmp(key, "refundItem") == SUCCESS)
		return SUCCESS;
	else if (strcmp(key, "redemptionItem") == SUCCESS)
		return SUCCESS;

	return FAILURE;
}
/*
 * ============================================================================
 * Function Name: buildXMLTree
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildXMLTree(xmlDocPtr docPtr, xmlNode * curNode, KEYVAL_PTYPE listPtr, int iTotCnt)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iListVal		= 0;
	char *			szListVal		= NULL;
	xmlNodePtr		elemNodePtr		= NULL;
	xmlNodePtr		textNodePtr		= NULL;
	xmlNodePtr		tempNodePtr		= NULL;
	LIST_INFO_PTYPE	listInfoPtr		= NULL;

#ifdef DEVDEBUG
	char			szDbgMsg[5712]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	while((rv == SUCCESS) && (iCnt < iTotCnt))
	{
		if(listPtr[iCnt].value == NULL)
		{
			if(listPtr[iCnt].valType == NULL_ADD)
			{
				elemNodePtr = xmlNewChild(curNode, NULL, BAD_CAST listPtr[iCnt].key, NULL);
				if(elemNodePtr == NULL) // CID 67213 (#1 of 1): Unused value (UNUSED_VALUE) T_RaghavendranR1
				{
					debug_sprintf(szDbgMsg, "%s: Failed to Add New XML Child", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}

			iCnt++;
			continue;
		}

		switch(listPtr[iCnt].valType)
		{
		case SINGLETON:
		case STRING:
		case AMOUNT:
		case DATE:
		case TIME:
		case NUMERICS:
		case MASK_NUM_STR:
		case NULL_ADD:
		case NUM_RANGE:
		case CHAR_SET:

			elemNodePtr = xmlNewChild(curNode, NULL, BAD_CAST listPtr[iCnt].key, NULL);
			if(elemNodePtr == NULL) // CID 67213 (#1 of 1): Unused value (UNUSED_VALUE) T_RaghavendranR1
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Add New XML Child", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			if (strcmp(listPtr[iCnt].key, "PAYLOAD") == SUCCESS)
			{
				/*Using string length for the size of the buffer
				 * Ideally we should use the size given when building the xml.
				 * Needs a revisit here.
				 */
				/*
				 * Now we have PAYLOAD tag for two packets, one for basket
				 * which is the CDATA block and other one for ERESPONSE packet
				 * Having following condition to check whether we need to include
				 * CDATA block for Payload or add it as the test
				 * If the value contains the BasketSubmission as the root name
				 * then considering it as the basket packet and creating the CDATA block
				 * In case of ERESPONSE the data would be encoded text, so getXMLRootName
				 * fails, adding the value as the text to that node
				 */

				/* Looking for the BasketSubmission in the value field so that we will make it CDATA
				 * as required by the bapi host.
				 */

				if(strstr(listPtr[iCnt].value, "BasketSubmission") != NULL)
				{
					textNodePtr = xmlNewCDataBlock( docPtr, listPtr[iCnt].value, strlen(listPtr[iCnt].value));
				}
				else
				{
					textNodePtr = xmlNewText(BAD_CAST listPtr[iCnt].value);
				}
			}
			else
			{
				textNodePtr = xmlNewText(BAD_CAST listPtr[iCnt].value);
			}

			if(textNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Testnode is NULL while adding value for key %s",
														__FUNCTION__, listPtr[iCnt].key);
				APP_TRACE(szDbgMsg);
			}

			tempNodePtr = xmlAddChild(elemNodePtr, textNodePtr);

			if(tempNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: tempNodePtr is NULL while adding value for key %s", 
															__FUNCTION__, listPtr[iCnt].key);
				APP_TRACE(szDbgMsg);
			}

			break;

		case BOOLEAN:
			iListVal = * ((int *)listPtr[iCnt].value);
			if(iListVal == PAAS_TRUE)
			{
				textNodePtr = xmlNewText(BAD_CAST "TRUE");
			}
			else
			{
				textNodePtr = xmlNewText(BAD_CAST "FALSE");
			}

			elemNodePtr = xmlNewChild(curNode, NULL, BAD_CAST
													listPtr[iCnt].key, NULL);
			xmlAddChild(elemNodePtr, textNodePtr);

			break;

		case LIST:
			listInfoPtr = (LIST_INFO_PTYPE) listPtr[iCnt].valMetaInfo;
			iListVal = * ((int *)listPtr[iCnt].value);
			szListVal = NULL;

			getCorrectStrFromList(&szListVal, listInfoPtr, iListVal);

			textNodePtr = xmlNewText(BAD_CAST szListVal);
			elemNodePtr = xmlNewChild(curNode, NULL, BAD_CAST
													listPtr[iCnt].key, NULL);
			xmlAddChild(elemNodePtr, textNodePtr);

			break;

		case VARLIST:
	
			elemNodePtr = xmlNewChild(curNode, NULL, BAD_CAST listPtr[iCnt].key,
																		NULL);
			if(isXmlNodePropReq(listPtr[iCnt].key) == SUCCESS)
			{
				xmlNewProp(elemNodePtr, BAD_CAST "Sequence", BAD_CAST "");
				xmlNewProp(elemNodePtr, BAD_CAST "LoadID", BAD_CAST "");
				xmlNewProp(elemNodePtr, BAD_CAST "LastModified", BAD_CAST "");
			}

			rv = buildXMLTreeForVarList(docPtr, elemNodePtr, listPtr[iCnt].value,
												listPtr[iCnt].valMetaInfo);
			break;

		case RECLIST:
			rv = buildXMLTreeForRecList(curNode, listPtr[iCnt].key,
														listPtr[iCnt].value);
			break;

		case MULTILIST:
			elemNodePtr = xmlNewChild(curNode, NULL, BAD_CAST listPtr[iCnt].key,
																		NULL);
			rv = buildXMLTreeForMultiList(elemNodePtr, listPtr[iCnt].value,
												listPtr[iCnt].valMetaInfo);
			break;

		case ATTRIBUTE:
			xmlNewProp(curNode, BAD_CAST listPtr[iCnt].key, BAD_CAST listPtr[iCnt].value);
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should never come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv == SUCCESS)
		{
			iCnt++;
		}
	}

	//debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	//APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildXMLAdminReqMsgFromMetaData
 *
 * Description	:
 *
 * Input Params	:  docPtr		->
 * 				   metaDataPtr 	->
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int buildXMLAdminReqMsgFromMetaData(char ** szDestPtr, int * iSize, char * pszRootMsg,
														METADATA_PTYPE metaDataPtr)
{
	int				rv				= SUCCESS;
	int				iTotCnt			= 0;
	int				iCnt			= 0;
	int				iHostProtocol	= getHostProtocolFormat();
	xmlDocPtr		docPtr			= NULL;
	xmlNodePtr		elemNodePtr		= NULL;
	xmlNodePtr		childNode		= NULL;
	xmlNodePtr		rootPtr			= NULL;
	xmlAttrPtr 		newAttr			= NULL;
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Create the xml document */
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Create the root node for the xml document */
		rootPtr = xmlNewNode(NULL, BAD_CAST pszRootMsg);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the root element for the doc */
		xmlDocSetRootElement(docPtr, rootPtr);

		if(iHostProtocol == HOST_PROTOCOL_SSI)
		{
			childNode = xmlNewChild(rootPtr, NULL, BAD_CAST "HEADER", NULL);

		}
		else
		{
			childNode = rootPtr;
		}

		listPtr = metaDataPtr->keyValList;
		iTotCnt = metaDataPtr->iTotCnt;

		while (iTotCnt > 0 )
		{
			if(listPtr[iCnt].value != NULL)
			{
				elemNodePtr = xmlNewChild(childNode, NULL, BAD_CAST listPtr[iCnt].key,
						BAD_CAST listPtr[iCnt].value);
				if(elemNodePtr == NULL) // CID 67396 (#1 of 1): Unused value (UNUSED_VALUE) T_RaghavendranR1
				{
					debug_sprintf(szDbgMsg, "%s: Failed to Add New XML Child", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			iCnt++;
			iTotCnt--;
		}

		if(iHostProtocol == HOST_PROTOCOL_SSI)
		{
			elemNodePtr = xmlNewChild(rootPtr, NULL, BAD_CAST "ADMIN", NULL);

			if(elemNodePtr == NULL)// CID 67396 (#1 of 1): Unused value (UNUSED_VALUE) T_RaghavendranR1
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Add New XML Child", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			newAttr = xmlNewProp (elemNodePtr, BAD_CAST "COMMAND",
									BAD_CAST "SETUP_REQUEST_V3");
		}
		break;
	}

	if(rv == SUCCESS)
	{
		/* If the XML message tree has formed correctly, dump this message into
		 * the string provided as parameter */
		xmlDocDumpFormatMemory(docPtr, (xmlChar **) szDestPtr, iSize, 1);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build XML tree", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* Free the memory allocated for the xml document tree */
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildXMLMsgFromMetaData
 *
 * Description	:
 *
 * Input Params	:  docPtr		->
 * 				   metaDataPtr 	->
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int buildXMLMsgFromMetaData(char ** szDestPtr, int * iSize, char * pszRootMsg,
												METADATA_PTYPE metaDataPtr)
{
	int				rv				= SUCCESS;
	int				iTotCnt			= 0;
	xmlDocPtr		docPtr			= NULL;
	xmlNodePtr		rootPtr			= NULL;
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	//char			szDbgMsg[512]	= "";
	char			szDbgMsg[7120]	= ""; //Remove this please
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Create the xml document */
		docPtr = xmlNewDoc(BAD_CAST "1.0");
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create doc",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Create the root node for the xml document */
		rootPtr = xmlNewNode(NULL, BAD_CAST pszRootMsg);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: XML build FAILED;couldnt create root",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Setting XML Schema Version For UGP Request
		if(getHostProtocolFormat() == HOST_PROTOCOL_UGP)
		{
			xmlSetProp(rootPtr, (xmlChar*)"VER", (xmlChar*)(XML_DEFAULT_VERSION));
		}
		/* Set the root element for the doc */
		xmlDocSetRootElement(docPtr, rootPtr);

		listPtr = metaDataPtr->keyValList;
		iTotCnt = metaDataPtr->iTotCnt;

		/* Build the xml tree from the data provided */
		rv = buildXMLTree(docPtr, rootPtr, listPtr, iTotCnt);

		break;
	}

	if(rv == SUCCESS)
	{
		/* If the XML message tree has formed correctly, dump this message into
		 * the string provided as parameter */
		xmlDocDumpFormatMemory(docPtr, (xmlChar **) szDestPtr, iSize, 0);

		debug_sprintf(szDbgMsg, "%s: Total Bytes = [%d]", __FUNCTION__, *iSize);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to build XML tree", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* Free the memory allocated for the xml document tree */
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: findNodeInXML
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
xmlNode * findNodeInXML(char * szNodeId, xmlNode * curNode)
{
	xmlNode *	nxtNode			= NULL;
	xmlNode *	reqdNode		= NULL;

	if((curNode->type == XML_ELEMENT_NODE) &&
		(xmlStrcmp(curNode->name, (const xmlChar *) szNodeId) == SUCCESS) ) {
		/* Element found */
		reqdNode = curNode;
	}
	else {
		/* Search in other places */
		/* Go for the child node */
		nxtNode = curNode->xmlChildrenNode;
		if(nxtNode != NULL) {
			reqdNode = findNodeInXML(szNodeId, nxtNode);
		}

		if(reqdNode == NULL) {
			/* If all the child nodes are exhausted, go for the sibling node */
			nxtNode = curNode->next;
			if(nxtNode != NULL) {
				reqdNode = findNodeInXML(szNodeId, nxtNode);
			}
		}
	}

	return reqdNode;
}

/*
 * ============================================================================
 * Function Name: parseXMLForData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXMLForData(xmlDoc * docPtr, xmlNode * curNode,
										KEYVAL_PTYPE keyValList, int keyCnt)
{
	int				rv				= SUCCESS;
	int				iRetVal			= 0;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iLen			= 0;
	char *			cPtr			= NULL;
	xmlChar *		value			= NULL;
	xmlNode *		nxtNode 		= NULL;
	PAAS_BOOL		bMatched		= PAAS_FALSE;
	PAAS_BOOL		bAlreadyParsed	= PAAS_FALSE;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: ---enter---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	while( (iCnt < keyCnt) && (rv == SUCCESS) && (curNode != NULL))
	{
		if(curNode->type == 3) /* TEXT TYPE NODE */
		{
			/* No need for checking this node */
			break;
		}

		if(curNode->type == XML_ENTITY_REF_NODE)
		{
			debug_sprintf(szDbgMsg, "%s: Node is of type XML_ENTITY_REF_NODE", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		if(!xmlStrcmp(curNode->name, (const xmlChar *)keyValList[iCnt].key))
		{
			switch(keyValList[iCnt].valType)
			{
			case NUMERICS:
				/* Get the correct value according to the allowable types of
				 * strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectNumVal((char *) value,
								(char **) &keyValList[iCnt].value,
								(STR_DEF_PTYPE)keyValList[iCnt].valMetaInfo);
					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
					}
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == PAAS_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case MASK_NUM_STR:
				/* Get the correct value according to the allowable types of
				 * strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectMaskedString((char *) value,
								(char **) &keyValList[iCnt].value,
							(MASK_STR_DEF_PTYPE)keyValList[iCnt].valMetaInfo);
					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
					}
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == PAAS_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case NUM_RANGE:
				/* Get the correct value according to the allowable types of
				 * strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectNumRange((char *) value,
								(char **) &keyValList[iCnt].value,
							(NUM_RANGE_DEF_PTYPE)keyValList[iCnt].valMetaInfo);
					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
					}
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == PAAS_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}
				break;

			case CHAR_SET:
				/* Get the correct value according to the allowable types of
				 * strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectCharSetStrVal((char *) value,
								(char **) &keyValList[iCnt].value,
								(CHAR_SET_DEF_PTYPE)keyValList[iCnt].valMetaInfo);
					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
					}
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == PAAS_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case STRING:
				/* Get the correct value according to the allowable types of
				 * strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectStrVal((char *) value,
								(char **) &keyValList[iCnt].value,
								(STR_DEF_PTYPE)keyValList[iCnt].valMetaInfo);
					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
					}
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == PAAS_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case AMOUNT:
				/* Get the correct value according to the allowable types of
				 * boolean strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectAmtVal((char *) value,
								(char **) &keyValList[iCnt].value,
								(AMT_DEF_PTYPE)keyValList[iCnt].valMetaInfo);
					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
					}
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == PAAS_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case DATE:
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectDateVal((char * )value,
											(char **) &keyValList[iCnt].value);
					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
					}
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == PAAS_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case TIME:
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode,1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectTimeVal((char * )value,
											(char **) &keyValList[iCnt].value);
					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
					}
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == PAAS_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case BOOLEAN:
				/* Get the correct value according to the allowable types of
				 * boolean strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectBoolVal((char * )value,
											(int **) &keyValList[iCnt].value);
					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
					}
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == PAAS_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;

			case SINGLETON:
				/* Get the correct value according to the allowable types of
				 * numerics strings present in the message */
				value = xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					iLen = xmlStrlen(value);
					if(iLen > 0)
					{
						cPtr = (char *) malloc(iLen + 1);
						memset(cPtr, 0x00, iLen + 1);

						memcpy(cPtr, value, iLen);
						keyValList[iCnt].value = cPtr;
						cPtr = NULL;
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
						/* If the value is mandatory and is read, update the
						 * mandatory count */
						if(keyValList[iCnt].isMand == PAAS_TRUE)
						{
							jCnt++;
						}
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break; /* End of SINGLETON switch case */

			case LIST:
				/* Get the correct value according to the allowable types of
				 * LIST strings present in the message */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value == NULL)
				{
					break;
				}

				if(keyValList[iCnt].value == NULL)
				{
					rv = getCorrectEnumValFromList((char *)value,
								(LIST_INFO_PTYPE)keyValList[iCnt].valMetaInfo,
								(int **) &keyValList[iCnt].value);
					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
					}
					if((rv == SUCCESS)&&(keyValList[iCnt].isMand == PAAS_TRUE))
					{
						jCnt++;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,
						"%s: Value [%s] already filled for [%s]", __FUNCTION__,
						(char *) keyValList[iCnt].value, keyValList[iCnt].key);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break; /* End of LIST switch case */

			case MULTILIST:

				if(curNode->xmlChildrenNode == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: No child node", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					rv = parseXMLForMultiListValues(curNode->xmlChildrenNode,
								(LIST_INFO_PTYPE)keyValList[iCnt].valMetaInfo,
								(MLVAL_PTYPE *) &keyValList[iCnt].value);

					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
					}
					if(keyValList[iCnt].isMand == PAAS_TRUE)
					{
						if(rv == SUCCESS)
						{
							jCnt++;
						}
					}
					else
					{
						rv = SUCCESS;
					}
				}

				break; /* End of MULTILIST switch case */

			case VARLIST:

				if(curNode->xmlChildrenNode == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: No child node", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					/* Parse xml and store the variable linked list of data */
					rv = parseXMLForListElem(docPtr, curNode->xmlChildrenNode,
							(VARLST_INFO_PTYPE) keyValList[iCnt].valMetaInfo,
							(VAL_LST_PTYPE *) &keyValList[iCnt].value);
					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
					}
					if(keyValList[iCnt].isMand == PAAS_TRUE)
					{
						if(rv == SUCCESS)
						{
							jCnt++;
						}
					}
					/*AjayS2: If Invalid Any Fields, it should not matter whether current Node is mandatory or not,
					 * We should Throw Error
					 */
#if 0
					else
					{
						rv = SUCCESS;
					}
#endif
					bAlreadyParsed = PAAS_TRUE;
				}

				break; /* End of VARLIST switch case */

			case ATTR_LKDLST:

				if(curNode->xmlChildrenNode == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: No child node", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					/* Parse xml and store the variable linked list of data */
					rv = parseXMLForAttribLst(docPtr, curNode->xmlChildrenNode,
							(VARLST_INFO_PTYPE) keyValList[iCnt].valMetaInfo,
							(VAL_NODE_PTYPE *) &keyValList[iCnt].value);
					if(rv == SUCCESS)
					{
						/* Set this variable to skip parsing remaining xml message*/
						bMatched = PAAS_TRUE;
					}
					if(keyValList[iCnt].isMand == PAAS_TRUE)
					{
						if(rv == SUCCESS)
						{
							jCnt++;
						}
					}
					else
					{
						rv = SUCCESS;
					}
				}

				break; /* End of ATTR_LKDLST switch case */

			default:
				debug_sprintf(szDbgMsg, "%s: Undefined type = [%d]",
										__FUNCTION__, keyValList[iCnt].valType);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break; /* End of default switch case */

			} /* End of SWITCH */
		} /* End of if condition*/
		/* Deallocating any allocated memory not in use now */
		if(value != NULL)
		{
			xmlFree(value);
			value = NULL;
		}
		if(bMatched == PAAS_TRUE)
		{
			/*Given key matched with XML node*/
			bMatched = PAAS_FALSE;	
			break;		
		}
		if(rv == SUCCESS)
		{
			/*Checking Next Key*/
			iCnt++;
		}
	} /* End of WHILE loop */
	if(rv == SUCCESS && (curNode != NULL))
	{
//#if 0
		//AjayS2: We already have parsed the child node for VARLIST type data, we should not parse it again
		/* Go for the child Node */
		nxtNode = curNode->xmlChildrenNode;
		if(nxtNode && bAlreadyParsed == PAAS_FALSE)
		{
			iRetVal = parseXMLForData(docPtr, nxtNode, keyValList, keyCnt);
			if(iRetVal >= 0)
			{
				jCnt += iRetVal;
			}
			else
			{
				jCnt = iRetVal;
			}
		}
//#endif
		if(jCnt >= 0)
		{
			/* go for the sibling node */
			nxtNode = curNode->next;
			if(nxtNode)
			{
				iRetVal = parseXMLForData(docPtr, nxtNode, keyValList, keyCnt);
				if(iRetVal >= 0)
				{
					jCnt += iRetVal;
				}
				else
				{
					jCnt = iRetVal;
				}
			}
		}
	}
	else
	{
		jCnt = rv;
	}

	//debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, jCnt);
	//APP_TRACE(szDbgMsg);
	return jCnt;
}

/*
 * ============================================================================
 * Function Name: parseXMLForMultiListValues
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXMLForMultiListValues(xmlNode * curNode, LIST_INFO_PTYPE refLst,
													MLVAL_PTYPE * valueList)
{
	int			rv				= SUCCESS;
	int			iCnt			= 0;
	int *		iVal			= NULL;
	char *		value			= NULL;
	MLVAL_PTYPE	tmpValPtr		= NULL;
	MLVAL_PTYPE	curValPtr		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while((rv == SUCCESS) && (curNode != NULL))
	{
		if(curNode->type == 3)
		{
			curNode = curNode->next;
			continue;
		}

		value = (char *) curNode->name;

		rv = getCorrectEnumValFromList(value, refLst, &iVal);
		if(rv == SUCCESS)
		{
			tmpValPtr = (MLVAL_PTYPE) malloc(MLVAL_SIZE);
			if(tmpValPtr)
			{
				memset(tmpValPtr, 0x00, MLVAL_SIZE);
				tmpValPtr->iLstVal = *iVal;

				if(curValPtr == NULL)
				{
					curValPtr = tmpValPtr;
					*valueList = tmpValPtr;
				}
				else
				{
					curValPtr->next = tmpValPtr;
					curValPtr = tmpValPtr;
				}

				iCnt++;
			}
		}

		if(iVal != NULL)
		{
			free(iVal);
			iVal = NULL;
		}

		curNode = curNode->next;
	}

	/* If not even a single value was found */
	if(iCnt == 0)
	{
		debug_sprintf(szDbgMsg, "%s: No values found", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_NO_FLD;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getAttributes
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getAttributes(xmlNode * curNode, KEYVAL_PTYPE listPtr, int totCnt)
{
	int			rv				= SUCCESS;
	int			iCnt			= 0;
	int			jCnt			= 0;
	int			iLen			= 0;
	char *		cTmpPtr			= NULL;
	xmlChar *	value			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < totCnt; iCnt++)
	{
		value = xmlGetProp(curNode, (const xmlChar *) listPtr[iCnt].key);
		if(value != NULL)
		{
			iLen = strlen((char *) value);
			if(iLen > 0)
			{
				cTmpPtr = (char *) malloc(iLen + 1);
				if(cTmpPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILURE", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				memset(cTmpPtr, 0x00, iLen + 1);
				memcpy(cTmpPtr, value, iLen);
				listPtr[iCnt].value = cTmpPtr;

				if(listPtr[iCnt].isMand == PAAS_TRUE)
				{
					jCnt++;
				}
			}

			xmlFree(value);
		}
	}

	if(rv == SUCCESS)
	{
		rv = jCnt;
	}
	else
	{
		// CID-67380: 29-Jan-16: MukeshS3:
		freeKeyValDataNode(listPtr, iCnt);
	}

	//debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	//APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXMLForAttribLst
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXMLForAttribLst(xmlDoc * docPtr, xmlNode * curNode,
						VARLST_INFO_PTYPE varLst, VAL_NODE_PTYPE * listBegin)
{
	int				rv					= SUCCESS;
	int				iCnt				= 0;
	int				jCnt				= 0;
	int				iTotCnt				= 0;
	int				iMandCnt			= 0;
	int				iSize				= 0;
	int				iAttribRead			= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";
	char *			cTmpPtr				= NULL;
	xmlChar *		value				= NULL;
	KEYVAL_PTYPE	keyLstPtr			= NULL;
	VAL_NODE_PTYPE	tmpNodePtr			= NULL;
	VAL_NODE_PTYPE	curNodePtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while((rv == SUCCESS) && (curNode != NULL))
	{
		if(curNode->type == 3)
		{
			curNode = curNode->next;
			continue;
		}

		while( (rv == SUCCESS) && (varLst[iCnt].szNodeStr != NULL) )
		{
			keyLstPtr = NULL;
			tmpNodePtr = NULL;
			iMandCnt = 0;
			cTmpPtr = NULL;

			if( !xmlStrcmp(curNode->name,
									(const xmlChar *)varLst[iCnt].szNodeStr) )
			{
				/* Get the value of the current node if present */
				value=xmlNodeListGetString(docPtr, curNode->xmlChildrenNode, 1);
				if(value != NULL)
				{
					iSize = strlen((const char *)value);
					cTmpPtr = (char *) malloc(iSize + 1);
					if(cTmpPtr != NULL)
					{
						memset(cTmpPtr, 0x00, iSize + 1);
						memcpy(cTmpPtr, value, iSize);
					}
				}

				/* Get the elements' count */
				iTotCnt = varLst[iCnt].iElemCnt;
				iSize = iTotCnt * KEYVAL_SIZE;

				/* Allocate memory for the key list */
				keyLstPtr = (KEYVAL_PTYPE) malloc(iSize);
				if(keyLstPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed for key list",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				/* Copy the reference list here */
				memset(keyLstPtr, 0x00, iSize);
				memcpy(keyLstPtr, varLst[iCnt].keyList, iSize);

				/* Get the count of the mandatory fields */
				for(jCnt = 0; jCnt < iTotCnt; jCnt++)
				{
					if(keyLstPtr[jCnt].isMand == PAAS_TRUE)
					{
						/* Increment the mandatory field count */
						iMandCnt++;
					}
				}

				/* Get the attribute values for the list of attributes */
				iAttribRead = getAttributes(curNode, keyLstPtr, iTotCnt);

				if((iAttribRead >= 0) && (iAttribRead < iMandCnt))
				{
					/* VDR: FIXME: Need to free the memory allocated to the
					 * key list */
					debug_sprintf(szDbgMsg,
								"%s: All mand attributes NOT present %d vs %d",
								__FUNCTION__, iAttribRead, iMandCnt);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "All Mandatory Fields Not Filled in XML Request.");
						strcpy(szAppLogDiag, "Please Fill All the Mandatory Fields in the XML Request to Process.");
						addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
					}

					rv = ERR_FLD_REQD;
					break;
				}
				else if(iAttribRead < 0)
				{
					rv = iAttribRead;
					break;
				}

				/* Allocate memory for the data node */
				tmpNodePtr = (VAL_NODE_PTYPE) malloc(sizeof(VAL_NODE_STYPE));
				if(tmpNodePtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc failed for data node",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				memset(tmpNodePtr, 0x00, sizeof(VAL_NODE_STYPE));
				tmpNodePtr->elemCnt = iTotCnt;
				tmpNodePtr->elemList = keyLstPtr;
				tmpNodePtr->type = varLst[iCnt].nodeType;
				tmpNodePtr->szVal = cTmpPtr;

				/* Add the data in the linked list */
				if(curNodePtr == NULL)
				{
					curNodePtr = tmpNodePtr;
					*listBegin = curNodePtr;
				}
				else
				{
					curNodePtr->next = tmpNodePtr;
					curNodePtr = tmpNodePtr;
				}

				break;
			}

			iCnt++;
		}

		if(varLst[iCnt].nodeType == -1)
		{
			/* None of the string matched with the element */
			debug_sprintf(szDbgMsg, "%s: [%s] is invalid node string",
												__FUNCTION__, curNode->name);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
		}

		if(rv == SUCCESS)
		{
			APP_TRACE(szDbgMsg);
			curNode = curNode->next;
		}
		else
		{	// CID-67380: 29-Jan-16: MukeshS3: We must free any allocated memory above, if it is not going to be used anywhere for FAILURE case.
			free(cTmpPtr);

			//CID 68332 (#1 of 1): Double free (USE_AFTER_FREE). We are Double freeing the same memory location. Hence Commenting the below line. Need to test this.
			freeValNode(*listBegin);
			/* In case any malloc fail, these temp nodes would not have added to the list, So freeing here */
			freeKeyValDataNode(keyLstPtr, 1);
			//CID 68332 (#1 of 1): Double free (USE_AFTER_FREE). We are Double freeing the same memory location. Hence Commenting the below line. Need to test this.
			//freeValNode(curNodePtr);
			//freeValNode(tmpNodePtr);
			listBegin = NULL;
		}
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseXMLForListElem
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseXMLForListElem(xmlDoc * docPtr, xmlNode * curNode,
					VARLST_INFO_PTYPE varLstPtr, VAL_LST_PTYPE * valNodesLst)
{
	int				rv					= SUCCESS;
	int				iCnt				= 0;
	int				jCnt				= 0;
	int				iSize				= 0;
	int				iTotCnt				= 0;
	int				iElemRead			= 0;
	int				iMandCnt			= 0;
	int				iTmpCnt				= 0;
	int				iSingleXMLFieldsRead = PAAS_FALSE;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";
	xmlChar *		value				= NULL;
	xmlNode	*		tmpNode				= NULL;
	KEYVAL_PTYPE	keyLstPtr			= NULL;
	VAL_LST_PTYPE	valNodesLstPtr		= NULL;
	METADATA_STYPE	stLocMetaData;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	valNodesLstPtr = (VAL_LST_PTYPE) malloc(VAL_LST_SIZE);
	if(valNodesLstPtr == NULL)
	{
		/* If the memory is not allocated, return ERROR */
		debug_sprintf(szDbgMsg, "%s: Malloc failed for value nodes list ptr",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		/* Initialize the memory if allocated */
		memset(valNodesLstPtr, 0x00, VAL_LST_SIZE);

		*valNodesLst = valNodesLstPtr;
	}
	tmpNode = curNode;
	while( (tmpNode != NULL) )
	{
		if(!strcmp((char *)tmpNode->name, "text"))
		{
			tmpNode = tmpNode->next;
			continue;
		}
		else
		{	
			iTmpCnt++;
			tmpNode = tmpNode->next;			
		}
	}
	while( (rv == SUCCESS) && (curNode != NULL) )
	{
		if(curNode->type == 3)
		{
			curNode = curNode->next;
			continue;
		}

		while( (rv == SUCCESS) && (varLstPtr[iCnt].nodeType != -1) )
		{

			if( (((varLstPtr[iCnt].szNodeStr == NULL) && (curNode->xmlChildrenNode)->next == NULL))||
				(!xmlStrcmp(curNode->name,
							(const xmlChar *)varLstPtr[iCnt].szNodeStr) )  )
			{

				if((varLstPtr[iCnt].szNodeStr == NULL) && (curNode->xmlChildrenNode)->next == NULL && iSingleXMLFieldsRead == PAAS_TRUE)
				{
					iTmpCnt--;

					iCnt++;
					continue;
				}

				/* Get the elements count */
				iTotCnt = varLstPtr[iCnt].iElemCnt;
				iSize = KEYVAL_SIZE * iTotCnt;
				iMandCnt = 0;

				if(iSize > 0)
				{
					/* Allocate memory for the key list */
					keyLstPtr = (KEYVAL_PTYPE) malloc(iSize);
					if(keyLstPtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc failed for key list",
								__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}

					/* Copy the reference list here */
					memset(keyLstPtr, 0x00, iSize);
					memcpy(keyLstPtr, varLstPtr[iCnt].keyList, iSize);

					/* Hard coding the getting of attributes for now*/
					if(!xmlStrcmp(curNode->name, (const xmlChar *)"Application"))
					{
						value = xmlGetProp(curNode, (const xmlChar *) keyLstPtr[0].key);
						if(value != NULL)
						{
							keyLstPtr[0].value = strdup((char*)value);

							xmlFree(value);
						}
					}

					if(!xmlStrcmp(curNode->name, (const xmlChar *)"CapKey"))
					{
						value = xmlGetProp(curNode, (const xmlChar *) keyLstPtr[0].key);
						if(value != NULL)
						{
							keyLstPtr[0].value = strdup((char*)value);

							xmlFree(value);
						}

						value = xmlGetProp(curNode, (const xmlChar *) keyLstPtr[1].key);
						if(value != NULL)
						{
							keyLstPtr[1].value = strdup((char*)value);

							xmlFree(value);
						}
					}

					/* Get the count of mandatory fields */
					for(jCnt = 0; jCnt < iTotCnt; jCnt++)
					{
						if(keyLstPtr[jCnt].isMand == PAAS_TRUE)
						{
							/* Increment the mandatory field count */
							iMandCnt++;
						}
					}

					/* Parse the xml for the required data */
					if(varLstPtr[iCnt].szNodeStr == NULL && (curNode->xmlChildrenNode)->next == NULL)
					{
						iElemRead = parseXMLForData(docPtr, curNode, keyLstPtr,
								iTotCnt);
						iSingleXMLFieldsRead = PAAS_TRUE;
					}
					else
					{
						iElemRead = parseXMLForData(docPtr, curNode->xmlChildrenNode,
								keyLstPtr, iTotCnt);
					}

					if((iElemRead >= 0) && (iElemRead < iMandCnt))
					{
						debug_sprintf(szDbgMsg,
								"%s: All mand fields NOT present %d vs %d",
								__FUNCTION__, iElemRead, iMandCnt);
						APP_TRACE(szDbgMsg);
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "All Mandatory Fields Not Filled in XML Request.");
							strcpy(szAppLogDiag, "Please Fill All the Mandatory Fields in the XML Request to Process.");
							addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
						}

						/* VDR:FIXME : Free the memory which is not required */

						rv = ERR_FLD_REQD;
						break;
					}
					else if(iElemRead < 0)
					{
						rv = iElemRead;
						break;
					}


					/* Add the data in the linked list */
					rv = addDataNode(valNodesLstPtr, varLstPtr[iCnt].nodeType,
							keyLstPtr, iTotCnt);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Addition of data node FAILED",
								__FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
				}
				/* Daivik:3/12/2015 :Once we have successfully added the node to the list we can equate this to NULL so that
				 * the we free the pointer only if node was not added in the list and if there is some error that
				 * has occurred as per the fix which we have made outside the loop.
				 */
				keyLstPtr = NULL;

				//Decrement the counter after adding the node.
				iTmpCnt--;

				break;
			}

			iCnt++;
		}
		/* Daivik:3/12/2015 : Fix to ensure that we free the KeyLstPtr , that was locally created, but not added to the
		 * main meta data list because of some error.
		 */
		if(rv != SUCCESS && keyLstPtr)
		{
			stLocMetaData.iTotCnt = iTotCnt;
			stLocMetaData.keyValList = keyLstPtr;

			freeMetaData(&stLocMetaData);
			free(keyLstPtr);
		}
#if 0 //ArjunU1: Don't check this condition*/
		if(varLstPtr[iCnt].nodeType == -1)
		{
			/* None of the string matched with the element */
			debug_sprintf(szDbgMsg, "%s: [%s] is invalid node string",
					__FUNCTION__, curNode->name);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
		}
#endif
		if(rv == SUCCESS)
		{
			if(iTmpCnt <= 0 )
			{
				debug_sprintf(szDbgMsg, "%s: Element in varlist are over",
													__FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			iCnt = 0; //ArjunU1: We must reset this for next iteration.
			/*Checking next node*/
			curNode = curNode->next;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectStrVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectStrVal(char * xmlStr, char ** dpszStrVal, STR_DEF_PTYPE
																		defPtr)
{
	int		rv					= SUCCESS;
	int		iLen				= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";
	char *	cPtr				= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]		= "";
#endif
//
//	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
//	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iLen = strlen(xmlStr);
	if((iLen < defPtr->minLen) || (iLen > defPtr->maxLen))
	{
		if(iLen < 512)
		{
			debug_sprintf(szDbgMsg, "%s: [%s] is an invalid string value",
					__FUNCTION__, xmlStr);
			APP_TRACE(szDbgMsg);
		}
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "XML Field Error, Invalid String Length for the XML Field.");
			strcpy(szAppLogDiag, "Please Check the XML Field Minimum and Maximum Length");
			addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
		}

		rv = ERR_INV_FLD;
	}
	else
	{
		cPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cPtr != NULL)
		{
			memset(cPtr, 0x00, (iLen + 1) * sizeof(char));
			strcpy(cPtr, xmlStr);
			*dpszStrVal = cPtr;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectMaskedString
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectMaskedString(char * xmlStr, char ** dpszStrVal,
													MASK_STR_DEF_PTYPE defPtr)
{
	int		rv					= SUCCESS;
	int		iLen				= 0;
	int		iMaskedLen			= 0;
	int		iCnt				= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";
	char *	cPtr				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		iLen = strlen(xmlStr);
		if((iLen < defPtr->minLen) || (iLen > defPtr->maxLen))
		{
			debug_sprintf(szDbgMsg, "%s: [%s] is an invalid string value",
														__FUNCTION__, xmlStr);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "XML Field Error, Invalid String Length for the XML Field.");
				strcpy(szAppLogDiag, "Please Check the XML Field Minimum and Maximum Length");
				addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
			}
			rv = ERR_INV_FLD;
			break;
		}

		if(strspn(xmlStr, "0123456789*") != iLen)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid characters in the string [%s]",
														__FUNCTION__, xmlStr);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}

		cPtr = xmlStr;

		for(iCnt = 0; iCnt < defPtr->prefixLen; iCnt++)
		{
			if(*cPtr == '*')
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Masked String [%s]",
														__FUNCTION__, xmlStr);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			cPtr++;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		iMaskedLen = iLen - (defPtr->prefixLen) - (defPtr->suffixLen);

		for(iCnt = 0; iCnt < iMaskedLen; iCnt++)
		{
			if(*cPtr != '*')
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Masked String [%s]",
														__FUNCTION__, xmlStr);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			cPtr++;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		for(iCnt = 0; iCnt < defPtr->suffixLen; iCnt++)
		{
			if(*cPtr == '*')
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Masked String [%s]",
														__FUNCTION__, xmlStr);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			cPtr++;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate the memory for storing the value */
		cPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cPtr != NULL)
		{
			memset(cPtr, 0x00, (iLen + 1) * sizeof(char));
			strcpy(cPtr, xmlStr);
			*dpszStrVal = cPtr;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectCharSetStrVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectCharSetStrVal(char * xmlStr, char ** dpszStrVal, CHAR_SET_DEF_PTYPE defPtr)
{
	int		rv					= SUCCESS;
	int		iLen				= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";
	char *	cPtr				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iLen = strlen(xmlStr);
	if((iLen < defPtr->minLen) || (iLen > defPtr->maxLen))
	{
		debug_sprintf(szDbgMsg, "%s: [%s] is an invalid string value",
														__FUNCTION__, xmlStr);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "XML Field Error, Invalid String Length for the XML Field.");
			strcpy(szAppLogDiag, "Please Check the XML Field Minimum and Maximum Length");
			addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
		}

		rv = ERR_INV_FLD;
	}
	else if(strspn(xmlStr, defPtr->charSet) != iLen)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid characters in the string [%s]",
													__FUNCTION__, xmlStr);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "XML Field Error, Invalid Characters in the XML Field.");
			strcpy(szAppLogDiag, "Please Check the XML Prompt Format Field");
			addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
		}

		rv = ERR_INV_FLD;
	}
	else
	{
		cPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cPtr != NULL)
		{
			memset(cPtr, 0x00, (iLen + 1) * sizeof(char));
			strcpy(cPtr, xmlStr);
			*dpszStrVal = cPtr;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectNumRange
 *
 * Description	:
 *
 * Input Params	: Validates the Maximum and Minimum values given in the Number range
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectNumRange(char * xmlStr, char ** dpszNumVal, NUM_RANGE_DEF_PTYPE defPtr)
{
	int		rv					= SUCCESS;
	int		iLen				= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";
	char *	cPtr				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	/* Validate the field for numeric */
	iLen = strlen(xmlStr);
	if((iLen < defPtr->minLen) || (iLen > defPtr->maxLen))
	{
		debug_sprintf(szDbgMsg, "%s: [%s] is an invalid string value (length)",
														__FUNCTION__, xmlStr);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "XML Field Error, Invalid String Length for the XML Field.");
			strcpy(szAppLogDiag, "Please Check the XML Field Minimum and Maximum Length");
			addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
		}

		rv = ERR_INV_FLD;
	}
	else if((rv = validateNumericField(xmlStr)) != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: [%s] is an invalid numeric value",
														__FUNCTION__, xmlStr);
		APP_TRACE(szDbgMsg);

		rv = ERR_INV_FLD;
	}
	else if((atoi(xmlStr) < defPtr->minValue) || (atoi(xmlStr) > defPtr->maxValue))
	{
		debug_sprintf(szDbgMsg, "%s: [%s] is an invalid string value", __FUNCTION__, xmlStr);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "XML Field Error, Invalid Value for the XML Field.");
			strcpy(szAppLogDiag, "Please Check the XML Field, Minimum or Maximum Value of Prompt Characters Not Within Range");
			addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
		}

		rv = ERR_INV_FLD;
	}
	else
	{
		cPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cPtr != NULL)
		{
			memset(cPtr, 0x00, (iLen + 1) * sizeof(char));
			strcpy(cPtr, xmlStr);
			*dpszNumVal = cPtr;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectNumVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectNumVal(char * xmlStr, char ** dpszNumVal, STR_DEF_PTYPE
																		defPtr)
{
	int		rv					= SUCCESS;
	int		iLen				= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";
	char *	cPtr				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	/* Validate the field for numeric */
	iLen = strlen(xmlStr);
	if((iLen < defPtr->minLen) || (iLen > defPtr->maxLen))
	{
		debug_sprintf(szDbgMsg, "%s: [%s] is an invalid string value (length)",
														__FUNCTION__, xmlStr);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "XML Field Error, Invalid String Length for the XML Field.");
			strcpy(szAppLogDiag, "Please Check the XML Field Minimum and Maximum Length");
			addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
		}

		rv = ERR_INV_FLD;
	}
	else if((rv = validateNumericField(xmlStr)) != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: [%s] is an invalid numeric value",
														__FUNCTION__, xmlStr);
		APP_TRACE(szDbgMsg);

		rv = ERR_INV_FLD;
	}
	else
	{
		cPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cPtr != NULL)
		{
			memset(cPtr, 0x00, (iLen + 1) * sizeof(char));
			strcpy(cPtr, xmlStr);
			*dpszNumVal = cPtr;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectAmtVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectAmtVal(char * xmlStr, char ** dpszAmt,AMT_DEF_PTYPE defPtr)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	char *	cPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	/* Validate the field for amount */
	rv = validateAmountField(xmlStr, defPtr);
	if(rv == SUCCESS)
	{
		/* Amount is valid, save the value */
		iLen = strlen(xmlStr);
		cPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cPtr != NULL)
		{
			memset(cPtr, 0x00, (iLen + 1) * sizeof(char));
			strcpy(cPtr, xmlStr);
			*dpszAmt = cPtr;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: [%s] is an invalid amount", __FUNCTION__,
																		xmlStr);
		APP_TRACE(szDbgMsg);

		rv = ERR_INV_FLD;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectDateVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectDateVal(char * xmlStr, char ** szDatePtr)
{
	int		rv				= SUCCESS;
	int		day				= 0;
	int		maxDayVal		= 0;
	int		month			= 0;
	int		year			= 0;
	int		iLen			= 0;
	char	szTmp[5]		= "";
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	while(1)
	{
		iLen = strlen(xmlStr);

		/*
		 * Praveen_P1: Lately PWC changed the DATE format for DUP_CHECK report from
		 * YYYY.MM.DD to MM/DD/YYYY
		 * Now supporting both the formats in SCA
		 *
		 */

		/* YYYY.MM.DD -> 10 characters */
		/* MM/DD/YYYY -> 10 characters */

		if( (iLen != 10) || (strspn(xmlStr, "0123456789./") != iLen) )
		{
			rv = ERR_INV_FLD;
			break;
		}

		cCurPtr = xmlStr;

		if(strchr(cCurPtr, '.') != NULL)
		{
			//Format : YYYY.MM.DD

			/* Validate the year */
			cNxtPtr = strchr(xmlStr, '.');
			if((cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 4))
			{
				debug_sprintf(szDbgMsg, "%s: Invalid year length", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
			year = atoi(szTmp);

			/* Validate the month */
			cCurPtr = cNxtPtr + 1;
			cNxtPtr = strchr(cCurPtr, '.');
			if( (cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2) )
			{
				debug_sprintf(szDbgMsg, "%s: Invalid month length", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

			month = atoi(szTmp);
			if( (month <= 0) || (month > 12))
			{
				debug_sprintf(szDbgMsg, "%s: Invalid month value %d", __FUNCTION__,
																			month);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			/* Validate the day */
			cCurPtr = cNxtPtr + 1;
			cNxtPtr = strchr(cCurPtr, '.');
			if(cNxtPtr != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid date format [%s]",
															__FUNCTION__, xmlStr);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			cNxtPtr = strchr(cCurPtr, '\0');
			if( (cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2) )
			{
				debug_sprintf(szDbgMsg, "%s: Invalid day len", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

			day = atoi(szTmp);
			switch(month)
			{
			case 1: /* JAN - 31 days */
			case 3: /* MAR - 31 days */
			case 5: /* MAY - 31 days */
			case 7:	/* JUL - 31 days */
			case 8: /* AUG - 31 days */
			case 10:/* OCT - 31 days */
			case 12:/* DEC - 31 days */

				maxDayVal = 31;
				break;

			case 4: /* APR - 30 days */
			case 6: /* JUN - 30 days */
			case 9:	/* SEP - 30 days */
			case 11:/* NOV - 30 days */

				maxDayVal = 30;
				break;

			case 2: /* FEB - 28/29 days */
				if( ((year % 4 == 0) && (year % 100 != 0)) ||
								((year % 100 == 0) && (year % 400 == 0)) )
				{
					maxDayVal = 29;
				}
				else
				{
					maxDayVal = 28;
				}

				break;
			}

			if(day < 1 || day > maxDayVal)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid day value %d", __FUNCTION__,
																			day);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
		}
		else if(strchr(cCurPtr, '/') != NULL)
		{
			//Format : MM/DD/YYYY

			/* Validate the month */
			cNxtPtr = strchr(xmlStr, '/');
			if( (cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2) )
			{
				debug_sprintf(szDbgMsg, "%s: Invalid month length", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

			month = atoi(szTmp);
			if( (month <= 0) || (month > 12))
			{
				debug_sprintf(szDbgMsg, "%s: Invalid month value %d", __FUNCTION__,
																			month);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			/* Validate the day */
			cCurPtr = cNxtPtr + 1;

			cNxtPtr = strchr(cCurPtr, '/');
			if( (cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2) )
			{
				debug_sprintf(szDbgMsg, "%s: Invalid day len", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

			day = atoi(szTmp);

			//CID 67394 (#1 of 1): Logically dead code (DEADCODE). T_RaghavendranR1. Year is not assigned any value at all.
			/* Validate the year */
			cCurPtr = cNxtPtr + 1;

			cNxtPtr = strchr(cCurPtr, '/');
			if(cNxtPtr != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid date format [%s]",
															__FUNCTION__, xmlStr);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			cNxtPtr = strchr(cCurPtr, '\0');
			if( (cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 4) )
			{
				debug_sprintf(szDbgMsg, "%s: Invalid year len", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

			year = atoi(szTmp);
			switch(month)
			{
			case 1: /* JAN - 31 days */
			case 3: /* MAR - 31 days */
			case 5: /* MAY - 31 days */
			case 7:	/* JUL - 31 days */
			case 8: /* AUG - 31 days */
			case 10:/* OCT - 31 days */
			case 12:/* DEC - 31 days */

				maxDayVal = 31;
				break;

			case 4: /* APR - 30 days */
			case 6: /* JUN - 30 days */
			case 9:	/* SEP - 30 days */
			case 11:/* NOV - 30 days */

				maxDayVal = 30;
				break;

			case 2: /* FEB - 28/29 days */
				if( ((year % 4 == 0) && (year % 100 != 0)) ||
								((year % 100 == 0) && (year % 400 == 0)) )
				{
					maxDayVal = 29;
				}
				else
				{
					maxDayVal = 28;
				}

				break;
			}

			if(day < 1 || day > maxDayVal)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid day value %d", __FUNCTION__,
																			day);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			/* Validate the year */
			cCurPtr = cNxtPtr + 1;
			cNxtPtr = strchr(cCurPtr, '/');
			if(cNxtPtr != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid date format [%s]",
															__FUNCTION__, xmlStr);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}

			cNxtPtr = strchr(cCurPtr, '\0');
			if((cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 4))
			{
				debug_sprintf(szDbgMsg, "%s: Invalid year length", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_FLD;
				break;
			}
			memset(szTmp, 0x00, sizeof(szTmp));
			memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);
			year = atoi(szTmp);
		}

		/* All the fields of the date are valid */
		cCurPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cCurPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(cCurPtr, 0x00, sizeof(char) * (iLen + 1));
		memcpy(cCurPtr, xmlStr, iLen);
		*szDatePtr = cCurPtr;

		break;
	}

	if(rv == ERR_INV_FLD)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid date value [%s]", __FUNCTION__,
																		xmlStr);
		APP_TRACE(szDbgMsg);
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectTimeVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectTimeVal(char * xmlStr, char ** szTimePtr)
{
	int		rv				= SUCCESS;
	int		hour			= 0;
	int		min				= 0;
	int		sec				= 0;
	int		iLen			= 0;
	char	szTmp[3]		= "";
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	while(1)
	{
		iLen = strlen(xmlStr);

		/* hh:mm:ss -> 8 characters */
		if( (iLen != 8) || (strspn(xmlStr, "0123456789:") != iLen) )
		{
			rv = ERR_INV_FLD;
			break;
		}

		/* Validate the hour */
		cCurPtr = xmlStr;
		cNxtPtr = strchr(xmlStr, ':');
		if((cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid hour len", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}
		memset(szTmp, 0x00, sizeof(szTmp));
		memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

		hour = atoi(szTmp);
		if( (hour < 0) || (hour >= 24))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid hour value", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}

		/* Validate the minute */
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, ':');
		if( (cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid minute len", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}
		memset(szTmp, 0x00, sizeof(szTmp));
		memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

		min = atoi(szTmp);
		if( (min < 0) || (min >= 60) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid minute value", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}

		/* Validate the second */
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, ':');
		if(cNxtPtr != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid time format [%s]",
														__FUNCTION__, xmlStr);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}

		cNxtPtr = strchr(cCurPtr, '\0');
		if( (cNxtPtr == NULL) || ((cNxtPtr - cCurPtr) != 2) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid second len", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}
		memset(szTmp, 0x00, sizeof(szTmp));
		memcpy(szTmp, cCurPtr, cNxtPtr - cCurPtr);

		sec = atoi(szTmp);
		if( (sec < 0) || (sec >= 60) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid second value", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_FLD;
			break;
		}

		/* All the fields of the time are valid */
		cCurPtr = (char *) malloc((iLen + 1) * sizeof(char));
		if(cCurPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(cCurPtr, 0x00, sizeof(char) * (iLen + 1));
		memcpy(cCurPtr, xmlStr, iLen);
		*szTimePtr = cCurPtr;

		break;
	}

	if(rv == ERR_INV_FLD)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid time value [%s]", __FUNCTION__,
																		xmlStr);
		APP_TRACE(szDbgMsg);
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectBoolVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectBoolVal(char * xmlStr, int ** iBoolValPtr)
{
	int		rv				= SUCCESS;
	int *	iBoolVal		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Value to be searched = [%s]",
														__FUNCTION__, xmlStr);
	APP_TRACE(szDbgMsg);

	if( (strcmp(xmlStr, "TRUE") == SUCCESS)	||
		(strcmp(xmlStr, "YES") == SUCCESS)	||
		(strcmp(xmlStr, "T") == SUCCESS)	||
		(strcmp(xmlStr, "1") == SUCCESS)	)
	{
		/* The Boolean value passed in the xml is a TRUE value */

		iBoolVal = (int *) malloc(sizeof(int));
		if(iBoolVal != NULL)
		{
			*iBoolVal = PAAS_TRUE;
			*iBoolValPtr = iBoolVal;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}
	else if( (strcmp(xmlStr, "FALSE") == SUCCESS)||
			 (strcmp(xmlStr, "NO") == SUCCESS)	 ||
			 (strcmp(xmlStr, "F") == SUCCESS)	 ||
			 (strcmp(xmlStr, "0") == SUCCESS)	 )
	{
		/* The Boolean value passed in the xml is a FALSE value */

		iBoolVal = (int *) malloc(sizeof(int));
		if(iBoolVal != NULL)
		{
			*iBoolVal = PAAS_FALSE;
			*iBoolValPtr = iBoolVal;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}
	else
	{
		/* The value passed in the string doesnt match with any valid BOOLEAN
		 * string */
		debug_sprintf(szDbgMsg, "%s: [%s] is an INVALID Boolean Value",
														__FUNCTION__, xmlStr);
		APP_TRACE(szDbgMsg);

		rv = ERR_INV_FLD_VAL;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectStrFromList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectStrFromList(char ** szVal, LIST_INFO_PTYPE refListPtr,
																	int iVal)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	/* Search for the data in the constant list (meta information) */
	while(refListPtr[iCnt].iListVal != -1)
	{
		if(refListPtr[iCnt].iListVal == iVal)
		{
			*szVal = refListPtr[iCnt].szListVal;

			debug_sprintf(szDbgMsg, "%s: string value = [%s]", __FUNCTION__,
																		*szVal);
			APP_TRACE(szDbgMsg);

			break;
		}

		iCnt++;
	}

	/* Value did not match with any of the list entries */
	if(refListPtr[iCnt].iListVal == -1)
	{
		debug_sprintf(szDbgMsg, "%s: Value [%d] not present in the list",
														__FUNCTION__, iVal);
		APP_TRACE(szDbgMsg);

		rv = ERR_INV_FLD_VAL;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectEnumValFromList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCorrectEnumValFromList(char * xmlStr, LIST_INFO_PTYPE refListPtr,
																int ** value)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

//	debug_sprintf(szDbgMsg, "%s: Value to be searched = [%s]", __FUNCTION__,
//																		xmlStr);
//	APP_TRACE(szDbgMsg);

	/* Search for the data in the constant list (meta information) */
	while(refListPtr[iCnt].iListVal != -1)
	{
		/* Match found with one of the list entries */
		if(strcmp(xmlStr, refListPtr[iCnt].szListVal) == SUCCESS)
		{
			int	*	tmpPtr	= NULL;

			tmpPtr = (int *) malloc(sizeof(int));
			if(tmpPtr != NULL)
			{
				*tmpPtr = refListPtr[iCnt].iListVal;
				*value = tmpPtr;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Memory allocation FAILURE",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}

			break;
		}

		iCnt++;
	}

	/* Value did not match with any of the list entries */
	if(refListPtr[iCnt].iListVal == -1)
	{
		debug_sprintf(szDbgMsg, "%s: Value [%s] not present in the list",
														__FUNCTION__, xmlStr);
		APP_TRACE(szDbgMsg);

		rv = ERR_INV_FLD_VAL;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getParamValues
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static void getParamValues(xmlNode * curXMLNode, GENKEYVAL_PTYPE * paramValList)
{
	xmlChar *			szParam		= NULL;
	xmlChar *			szValue		= NULL;
	xmlChar *			szHostName	= NULL;
	GENKEYVAL_PTYPE		curNodePtr	= NULL;
	GENKEYVAL_PTYPE		tmpNodePtr	= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(curXMLNode != NULL)
	{
		if(curXMLNode->type == 3)
		{
			curXMLNode = curXMLNode->next;
			continue;
		}
		else if(strcmp((char *)curXMLNode->name, "Parameter") != SUCCESS)
		{
			curXMLNode = curXMLNode->next;
			continue;
		}

		szParam = xmlGetProp(curXMLNode, (const xmlChar *) "ParamName");
		if(szParam != NULL)
		{
			szValue = xmlGetProp(curXMLNode, (const xmlChar *) "Value");
			if(szValue != NULL)
			{
				tmpNodePtr = (GENKEYVAL_PTYPE) malloc(GENKEYVAL_SIZE);
				if(tmpNodePtr != NULL)
				{
					memset(tmpNodePtr, 0x00, GENKEYVAL_SIZE);
					tmpNodePtr->key = (char *) szParam;
					tmpNodePtr->value = (char *) szValue;

					/*
					 * Praveen_P1: Adding new attribute to the parameter field in the
					 * config.xml to specify under which host interface this
					 * variable has to go
					 */
					szHostName = xmlGetProp(curXMLNode, (const xmlChar *) "HostName");

					if(szHostName != NULL)
					{
						tmpNodePtr->hostName = (char *)szHostName;
					}
					else
					{
						tmpNodePtr->hostName = NULL;
					}
					tmpNodePtr->next = NULL;

					/* Add the node in the linked list */
					if(curNodePtr == NULL)
					{
						/* Adding the first node */
						curNodePtr = tmpNodePtr;
						*paramValList = curNodePtr;
					}
					else
					{
						/* Adding the nth node */
						curNodePtr->next = tmpNodePtr;
						curNodePtr = tmpNodePtr;
					}
				}
				else
				{
					xmlFree(szParam);
					xmlFree(szValue);
				}
			}
			else
			{
				xmlFree(szParam);
			}
		}

		curXMLNode = curXMLNode->next;
	}

	debug_sprintf(szDbgMsg, "%s: returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: parseXMLFileForConfigData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
int parseXMLFileForConfigData(char * szXMLFile, GENKEYVAL_PTYPE * paramValList)
{
	int			rv					= SUCCESS;
	int			iLen				= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
	char		szAppLogDiag[300]	= "";
	xmlDoc *	docPtr				= NULL;
	xmlNode *	rootPtr				= NULL;
	xmlNode *	curNode				= NULL;
	xmlChar *	szGid				= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		if( (szXMLFile == NULL) || ((iLen = strlen(szXMLFile)) <= 0) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid data passed as paramter",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML file using the libxml API */
		docPtr = xmlParseFile(szXMLFile);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Parsing the XML File [%s] Failed.", szXMLFile);
				strcpy(szAppLogDiag, "Please Check the XML File");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Find the root node */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid XML Data");
				sprintf(szAppLogDiag, "No XML Data is Present in the File [%s], Please Check the XML File", szXMLFile);
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_INV_XML_MSG;
			break;
		}
		else if(strcmp((char *)rootPtr->name, "VeriCentreImportData")!= SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Incorrect xml document", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Incorrect XML Document.");
				strcpy(szAppLogDiag, "VeriCentreImportData Root Name Not Present in the XML data, Please Check the XML File");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Find the node "Group" with GID=1 as attribute */
		curNode = findNodeInXML("Group", rootPtr);
		while(curNode != NULL)
		{
			szGid = xmlGetProp(curNode, (const xmlChar *)"GID");
			if(szGid != NULL)
			{
				if(strcmp((char *)szGid, "1") == SUCCESS)
				{
					xmlFree(szGid);
					break;
				}

				xmlFree(szGid);
			}

			curNode = curNode->next;
		}

		if(curNode == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Group with gid=1 missing in xml",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid XML Data.");
				strcpy(szAppLogDiag, "Group With gid = 1 Missing in the Document, Please Check the XML File");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_INV_XML_GID;
			break;
		}

		/* Get the parameters and values from the file */
		getParamValues(curNode->xmlChildrenNode, paramValList);

		break;
	}

	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: parseXMLFileForConfigIniData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
int parseXMLFileForConfigIniData(char * szXMLFile, GENKEYVAL_PTYPE * paramValList)
{
	int				rv					= SUCCESS;
	int				iLen				= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";
	xmlDoc *		docPtr				= NULL;
	xmlNode *		rootPtr				= NULL;
	xmlNode *		curNode				= NULL;
	xmlChar *		szGid				= NULL;
	dictionary *	dict				= NULL;
	FILE		* 	fp					= NULL;
	GENKEYVAL_PTYPE	locParamValLstPtr	= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		if( (szXMLFile == NULL) || ((iLen = strlen(szXMLFile)) <= 0) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid data passed as paramter",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		/* Load the ini file in the parser library to store EMV params */
		dict = iniparser_load(CFG_INI_FILE_PATH);
		if(dict == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to load ini file [%s]",
													__FUNCTION__, CFG_INI_FILE_PATH);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Unable to Load the INI File[%s]", CFG_INI_FILE_PATH);
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

	   fp = fopen(CFG_INI_FILE_PATH, "w");
	   if (fp == NULL)   // File open failed!
	   {
			debug_sprintf(szDbgMsg, "%s: Cannot open file [%s] for writing file information!",
																__FUNCTION__, CFG_INI_FILE_PATH);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Failed to Open the INI File[%s] For Write Operation", CFG_INI_FILE_PATH);
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
#ifdef DEBUG
			fprintf(stderr, szDbgMsg, CFG_INI_FILE_PATH);
#endif
			iniparser_freedict(dict);
			dict = NULL;
			rv = FAILURE;
			break;
		}

		APP_TRACE("opened file for writing");

		/* Parse the XML file using the libxml API */
		docPtr = xmlParseFile(szXMLFile);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error, Cannot Parse the XML File Failed.");
				strcpy(szAppLogDiag, "Please Check the INI File");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Find the root node */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty xml data",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "XML Data Received is Empty.");
				strcpy(szAppLogDiag, "File Does not Contain XML Root Element Data, Please Check the INI File");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_INV_XML_MSG;
			break;
		}
		else if(strcmp((char *)rootPtr->name, "VeriCentreImportData")!= SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Incorrect xml document", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Incorrect XML Data Received.");
				strcpy(szAppLogDiag, "Received XML Data Does Not Contain the VeriCentreImportData Root Element, Please Check the INI File");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Find the node "Group" with GID=1 as attribute */
		curNode = findNodeInXML("Group", rootPtr);
		while(curNode != NULL)
		{
			szGid = xmlGetProp(curNode, (const xmlChar *)"GID");
			if(szGid != NULL)
			{

				if(strncasecmp((char *)szGid, "A000000003", 10) == SUCCESS) //VISA
				{
					debug_sprintf(szDbgMsg, "%s: Storing the params in ini file for VISA[%s]",
																		__FUNCTION__, (char *)szGid);
					APP_TRACE(szDbgMsg);

					/* Get the parameters and values from the file */
					getParamValues(curNode->xmlChildrenNode, &locParamValLstPtr);

					//Store into ini file
					storeParamsInLocalCfgIniFile(locParamValLstPtr, (char *)szGid, dict);

					/* Free any allocated memory of param value list*/
					freeParamValList(locParamValLstPtr);
					locParamValLstPtr = NULL;

					//xmlFree(szGid);
				}
				else if(strncasecmp((char *)szGid, "A000000004", 10) == SUCCESS) //MASTER CARD
				{
					debug_sprintf(szDbgMsg, "%s: Storing the params in ini file for MASTER CARD[%s]",
																		__FUNCTION__, (char *)szGid);
					APP_TRACE(szDbgMsg);

					/* Get the parameters and values from the file */
					getParamValues(curNode->xmlChildrenNode, &locParamValLstPtr);

					//Store into ini file
					storeParamsInLocalCfgIniFile(locParamValLstPtr, (char *)szGid, dict);

					/* Free any allocated memory of param value list*/
					freeParamValList(locParamValLstPtr);
					locParamValLstPtr = NULL;

					//xmlFree(szGid);
				}
				else if(strncasecmp((char *)szGid, "A000000025", 10) == SUCCESS) //AMEX
				{
					debug_sprintf(szDbgMsg, "%s: Storing the params in ini file for AMEX[%s]",
																		__FUNCTION__, (char *)szGid);
					APP_TRACE(szDbgMsg);

					/* Get the parameters and values from the file */
					getParamValues(curNode->xmlChildrenNode, &locParamValLstPtr);

					//Store into ini file
					storeParamsInLocalCfgIniFile(locParamValLstPtr, (char *)szGid, dict);

					/* Free any allocated memory of param value list*/
					freeParamValList(locParamValLstPtr);
					locParamValLstPtr = NULL;

					//xmlFree(szGid);
				}
				else if(strncasecmp((char *)szGid, "A000000277", 10) == SUCCESS) //INTERAC
				{
					debug_sprintf(szDbgMsg, "%s: Storing the params in ini file for INTERAC[%s]",
																		__FUNCTION__, (char *)szGid);
					APP_TRACE(szDbgMsg);

					/* Get the parameters and values from the file */
					getParamValues(curNode->xmlChildrenNode, &locParamValLstPtr);

					//Store into ini file
					storeParamsInLocalCfgIniFile(locParamValLstPtr, (char *)szGid, dict);

					/* Free any allocated memory of param value list*/
					freeParamValList(locParamValLstPtr);
					locParamValLstPtr = NULL;

					//xmlFree(szGid);
				}
				else if(strncasecmp((char *)szGid, "A000000324", 10) == SUCCESS) //DISCOVER
				{
					debug_sprintf(szDbgMsg, "%s: Storing the params in ini file for DISCOVER[%s]",
																		__FUNCTION__, (char *)szGid);
					APP_TRACE(szDbgMsg);;

					/* Get the parameters and values from the file */
					getParamValues(curNode->xmlChildrenNode, &locParamValLstPtr);

					//Store into ini file
					storeParamsInLocalCfgIniFile(locParamValLstPtr, (char *)szGid, dict);

					/* Free any allocated memory of param value list*/
					freeParamValList(locParamValLstPtr);
					locParamValLstPtr = NULL;

					//xmlFree(szGid);
				}
				else if(strncasecmp((char *)szGid, "A000000065", 10) == SUCCESS) //JCB
				{
					debug_sprintf(szDbgMsg, "%s: Storing the params in ini file for JCB[%s]",
																		__FUNCTION__, (char *)szGid);
					APP_TRACE(szDbgMsg);

					/* Get the parameters and values from the file */
					getParamValues(curNode->xmlChildrenNode, &locParamValLstPtr);

					//Store into ini file
					storeParamsInLocalCfgIniFile(locParamValLstPtr, (char *)szGid, dict);

					/* Free any allocated memory of param value list*/
					freeParamValList(locParamValLstPtr);
					locParamValLstPtr = NULL;

					//xmlFree(szGid);
				}
				//Add RID here to support other types of cards later.
				else;

				xmlFree(szGid);
				szGid = NULL;
			}

			curNode = curNode->next;
		}
#if 0
		if(curNode == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Group with gid=1 missing in xml",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}
#endif
		break;
	}

	if( dict != NULL && rv == SUCCESS)
	{
		iniparser_dump_ini(dict, fp);
		iniparser_freedict(dict);
		dict = NULL;
		fclose(fp);
	}
	if(dict != NULL)
	{
		iniparser_freedict(dict);
		dict = NULL;
	}
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: freeParamValList
 *
 * Description	: Frees the param value list.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */

static void freeParamValList(GENKEYVAL_PTYPE paramValList)
{
	if(paramValList != NULL)
	{
		GENKEYVAL_PTYPE	tmpNodePtr	= NULL;

		while(paramValList != NULL)
		{
			tmpNodePtr = paramValList;
			paramValList = paramValList->next;

			if(tmpNodePtr->key)
			{
				free(tmpNodePtr->key);
			}

			if(tmpNodePtr->value)
			{
				free(tmpNodePtr->value);
			}

			free(tmpNodePtr);
		}
	}
}
#endif

/*
 * ============================================================================
 * Function Name: addDataNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addDataNode(VAL_LST_PTYPE valLstPtr, int iNodeType,
										KEYVAL_PTYPE listPtr, int iElemCnt)
{
	int				rv				= SUCCESS;
	VAL_NODE_PTYPE	tmpNodePtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	tmpNodePtr = (VAL_NODE_PTYPE) malloc(sizeof(VAL_NODE_STYPE));
	if(tmpNodePtr != NULL)
	{
		/* Initialize the allocated memory */
		memset(tmpNodePtr, 0x00, sizeof(VAL_NODE_STYPE));
		tmpNodePtr->elemCnt = iElemCnt;
		tmpNodePtr->elemList = listPtr;
		tmpNodePtr->type = iNodeType;

		/* Add the node to the list */
		if(valLstPtr->end == NULL)
		{
			/* Adding the first element */
			valLstPtr->start = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

//			debug_sprintf(szDbgMsg, "%s: Adding first node", __FUNCTION__);
//			APP_TRACE(szDbgMsg);
		}
		else
		{
			/* Adding the nth element */
			valLstPtr->end->next = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

//			debug_sprintf(szDbgMsg, "%s: Adding some node", __FUNCTION__);
//			APP_TRACE(szDbgMsg);
		}
		/* Increment the node count */
		valLstPtr->valCnt += 1;	
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: validateAmountField
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int validateAmountField(char * szFieldVal, AMT_DEF_PTYPE amtDefPtr)
{
	int		rv					= SUCCESS;
	int		iValLen				= 0;
	int		iTmpLen				= 0;
	int 	iMaxManLen			= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";
	char *	cCurPtr				= NULL;
	char *	cNxtPtr				= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif
//
//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		iValLen = strlen(szFieldVal);
		if(iValLen <= 0) {
			debug_sprintf(szDbgMsg, "%s: No value present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "XML Field Error, No Value Present in the Amount Field.");
				strcpy(szAppLogDiag, "Please Check the Amount Field Entered");
				addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}
		if(amtDefPtr->isNegAmtAllwd == PAAS_TRUE)
		{
			/* Check for characters */
			if(strspn(szFieldVal, "1234567890.-") != iValLen)
			{
				debug_sprintf(szDbgMsg, "%s: Characters other than [0-9], [.] & [-] found",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			//Praveen_P1: Just checking if there is - in other positions other than first position
			if(strspn(&szFieldVal[1], "1234567890.") != (iValLen - 1))
			{
				debug_sprintf(szDbgMsg, "%s: Characters other than [0-9] & [.]found from second position onwards",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		else
		{
			/* Check for characters */
			if(strspn(szFieldVal, "1234567890.") != iValLen)
			{
				debug_sprintf(szDbgMsg, "%s: Characters other than 0-9 & . found",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "XML Field Error, Amount Field Contains Non Numeric Characters.");
					strcpy(szAppLogDiag, "Please Check the Amount Field Entered");
					addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
				}
				rv = FAILURE;
				break;
			}			
		}
		cCurPtr = strchr(szFieldVal, '.');
		if(cCurPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Decimal character not present",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "XML Field Error, Decimal Character Not Present in the Amount Field of the Request.");
				strcpy(szAppLogDiag, "Please Check the Amount Field Entered");
				addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}

		cNxtPtr = strchr(cCurPtr + 1, '.');
		if(cNxtPtr != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Multiple instances of dec character",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "XML Field Error, Multiple Instances of \".\" Noticed in the Amount Field.");
				strcpy(szAppLogDiag, "Please Check the Amount Field Entered");
				addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
			}
			rv = FAILURE;
			break;
		}

		/* Validate the characteristic xyz.00 */
		iTmpLen = cCurPtr - szFieldVal;

		iMaxManLen = amtDefPtr->maxManLen;
		if(amtDefPtr->isNegAmtAllwd)
		{
			if(szFieldVal[0] == '-')
				iMaxManLen += 1;
		}

		if((iTmpLen < amtDefPtr->minManLen) || (iTmpLen > iMaxManLen) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid length [%d] before decimal fieldval:%c min%d max%d",
														__FUNCTION__, iTmpLen,szFieldVal[1],amtDefPtr->minManLen,iMaxManLen);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "XML Field Error, Invalid Length of Amount Field.");
				strcpy(szAppLogDiag, "Please Check the Amount Field Entered");
				addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}

		/* Validate the mantissa 0.xy */
		iTmpLen = iValLen - iTmpLen - 1;
		if((iTmpLen < amtDefPtr->minDecLen) || (iTmpLen > amtDefPtr->maxDecLen))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid length [%d] after decimal",
														__FUNCTION__, iTmpLen);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "XML Field Error, Invalid Length of Decimal in Amount Field.");
				strcpy(szAppLogDiag, "Please Check the Amount Field Entered");
				addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
			}
			rv = FAILURE;
			break;
		}

		break;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * End of file xmlUtils.c
 * ============================================================================
 */
