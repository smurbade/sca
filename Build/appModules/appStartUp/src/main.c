/*
 * =============================================================================
 * Filename    : main.c
 *
 * Application : Mx Point SCA
 *
 * Description : This file contains the code for the begining point of the
 * 					application.
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include <errno.h>
#include <svc.h>

#include "common/common.h"
#include "sci/sciMain.h"
#include "sci/sciDef.h"
#include "ssi/ssiMain.h"
#include "bLogic/bLogic.h"
#include "db/dataSystem.h"
#include "db/tranDS.h"
#include "db/posDataStore.h"
#include "uiAgent/uiManager.h"
#include "uiAgent/uiGeneral.h"
#include "uiAgent/uiConstants.h"
#include "uiAgent/uiAPIs.h"
#include "uiAgent/uiCfgDef.h"
#include "common/utils.h"
#include "appLog/appLogAPIs.h"
#include "bLogic/blVHQSupDef.h"
#include "common/configusr1applib.h"

/* Static functions */
static int	sciEvtProcssr(char *, char *);
static int	procSCIReqData(SCIDATA_PTYPE, BLDATA_PTYPE);
static int  createQueueForBL(char *);

#ifdef DEBUG
static int giSaveDebugLogs = 0;
static int giSaveMemDebugLogs = 0;
static int 	faPid = 0;
static void sigHandler(int);
static void setupSigHandlers();
#endif

/* Extern functions */
extern int loadConfigParams();
extern int mapParamsFromXMLFile();
extern int getListeningPortNum();
extern int getNtwrkModeAndIp(PAAS_BOOL * , char *, char *);
extern int SaveDebugLogs();
extern int cleanCURLHandle(PAAS_BOOL);
extern int (*fnpVHQUnRgstrApp)(char *);

int 	showSystemInfo();

//T_MukeshS3: global variable to check SCA Initialization
int gbSCAInitDone = PAAS_FALSE;

/*
 * ============================================================================
 * Function Name: main
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int main(int argc, char ** argv)
{
	int			rv						= SUCCESS;
	int			iAppLogEnabled			= 0;
	int			iStatusMsgDispInterval  = DFLT_STATUS_DISP_TIME;
	char		szSCIQId[10]			= "";
	char		szBLQId[10]				= "";
	char		szErrMsg[256]  			= "";
	char		szErrDispMsg[256]		= "";
	char		szAppLogData[300]		= "";

#ifdef DEBUG
	int			iDbgParmsSet	= 0;
	char		szDbgMsg[256]	= "";
#endif

	rv = initConfigUsr1AppLib();
	if (rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Init Of Config Usr1 App Lib Failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	/*
	 * ---------------------------------
	 * STEP 1: Initialize the debugger.
	 * ---------------------------------
	 */
#ifdef DEBUG
	setupDebug(&giSaveDebugLogs);
	setupMemDebug(&giSaveMemDebugLogs);
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: App Ver:[%s]", __FUNCTION__, SCA_APP_VERSION);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: App Build Number:[%s]", __FUNCTION__, SCA_BUILD_NUMBER);
	APP_TRACE(szDbgMsg);

#ifdef DEBUG
	 //Setting Signal handlers for debugging purpose
	setupSigHandlers();
#endif

	/*
	 * Praveen_P1: Please see following note: So we call at the starting of the application before any thread gets created
	 * The basic rule for constructing a program that uses libcurl is this:
	 * Call curl_global_init(), with a CURL_GLOBAL_ALL argument,
	 * immediately after the program starts, while it is still only one thread and before it uses libcurl at all.
	 */

	/* Initialize the CURL library */
	curl_global_init(CURL_GLOBAL_ALL);

	/*
	 * ----------------------------------------------
	 * STEP 2: Initialize the Application Logging
	 * ----------------------------------------------
	 */
	rv = initAppLog();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to initiate application Transaction Logging",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to init applocation trans log");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		while(1)
		{
			svcWait(10);
		}
	}

	iAppLogEnabled = isAppLogEnabled();
	debug_sprintf(szDbgMsg, "%s: IsAppLogging Enable [%d]", __FUNCTION__, iAppLogEnabled);
	APP_TRACE(szDbgMsg);

	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, START_INDICATOR);
		addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);

		strcpy(szAppLogData, "Application Event Logging Initiated Successfully");
		addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		sprintf(szAppLogData, "SCA Application Version No [%s], SCA Application Build No [%s]", SCA_APP_VERSION, SCA_BUILD_NUMBER);

		syslog(LOG_INFO|LOG_USER, szAppLogData); //Logging Application version information to syslog

		addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
	}

	/*
	 * ---------------------------------
	 * STEP 3: Initialize the Data System
	 * ---------------------------------
	 */
	/* Note: Data system should be initialized at the beginning */
	rv = initDataSystem();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to initialize data system",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to load Data System");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Failed to Load Data System");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
		}

		while(1)
		{
			svcWait(10);
		}
	}
	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "Data System Initiated Successfully");
		addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
	}

	/*
	 * ---------------------------------
	 * STEP 4: Mapping the config params from the xml file.
	 * ---------------------------------
	 */
	rv = mapParamsFromXMLFile();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to maps params from xml file",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to maps params from xml file");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Failed to Map Parameters From XML File");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
		}

		while(1)
		{
			svcWait(10);
		}
	}

#ifdef DEBUG
	/*
	 * ------------------------------------
	 * STEP 5: Setting the Debug config params to environment.
	 * ------------------------------------
	 */
	rv = setDebugParamsToEnv(&iDbgParmsSet);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to set debug params to environment",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to set debug params to environment");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		while(1)
		{
			svcWait(10);
		}
	}
	else
	{
		if(iDbgParmsSet == 1)
		{
			debug_sprintf(szDbgMsg, "%s: Debug parameters are reset to environment",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/*
			 * Praveen_P1: For testing purpose only
			 */
			char *	cpDbgFlag		= NULL;
			char *	cpMemDbgFlag	= NULL;
			char *pszEnvVar = NULL;

			cpDbgFlag = (char *)getenv("PAAS_DEBUG");
			cpMemDbgFlag = (char *)getenv("PAAS_MEM_DEBUG");

			debug_sprintf(szDbgMsg, "%s: Debug Flag[%s]",__FUNCTION__, cpDbgFlag);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Mem Debug Flag[%s]",__FUNCTION__, cpMemDbgFlag);
			APP_TRACE(szDbgMsg);

			pszEnvVar = getenv("DEBUGOPT");
			debug_sprintf(szDbgMsg, "%s: Debug OPT[%s]",__FUNCTION__, pszEnvVar);
			APP_TRACE(szDbgMsg);

			giSaveMemDebugLogs = 0;
			giSaveDebugLogs = 0; //testing purpose

			/* Close the debug socket before updating with new debug env params */
			closeupDebug();

			/* Close the Memory debug socket before updating with new debug env params */
			closeupMemDebug();

			//svcWait(1000); //Praveen_P1: Please check if it is required

			//Setting debug with new environment variables
			setupDebug(&giSaveDebugLogs);

			//Setting Memory debug with new environment variables
			setupMemDebug(&giSaveMemDebugLogs);

			//svcWait(1000); //Praveen_P1: Please check if it is required

			debug_sprintf(szDbgMsg, "%s: giSaveDebugLogs[%d], giSaveMemDebugLogs[%d]",__FUNCTION__, giSaveDebugLogs, giSaveMemDebugLogs);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Reseting DEBUG params to envinorment is not required",
										__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
#endif

	memset(szBLQId, 0x00, sizeof(szBLQId));
	rv = createQueueForBL(szBLQId);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create queue for BL data", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to create queue for BL data");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Failed to Create Queues for Buisness Logic");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
		}

		while(1)
		{
			svcWait(10);
		}
	}

	/*
	 * ---------------------------------
	 * STEP 6: Initialize the UI Module
	 * ---------------------------------
	 */
	rv = initUIModule(szBLQId, szErrDispMsg); //Passing the QID so that UIManager can send the data to BL incase consumer opt is selected on the line items
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to init UI module", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to load UI Module");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Failed to Initiate UI Module");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
		}
		if(strlen(szErrDispMsg) == 0)
		{
			showPSGenDispForm(MSG_APP_LOAD_ERR, FAILURE_ICON, iStatusMsgDispInterval);
		}
		else
		{
			showMessageBox("Critical Error", szErrDispMsg);
		}
		while(1)
		{
			svcWait(10);
		}

	}
	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "UI Module Initiated Successfully");
		addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
	}

	/*
	 * ---------------------------------
	 * STEP X: Initialize the VHQ/TMS Support
	 * ---------------------------------
	 */
	/* Note: VHQ Support should be initialized at the beginning, before initializing the other modules of application */
	rv = initVHQSupport(szErrDispMsg);
	if( rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to Initialize VHQ Support",__FUNCTION__);
		APP_TRACE(szDbgMsg);

	//	rv = FAILURE; CID 67265:T_POLISETTYG1: Unused value.
		strcpy(szErrMsg, "FAILED to Initialize VHQ Support");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Failed to Initialize VHQ Support");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
		}

		getDisplayMsg(szErrDispMsg, MSG_VHQ_LOAD_ERR);
		showMessageBox("Critical Error", szErrDispMsg);
		while(1)
		{
			svcWait(10);
		}
	}

	//Get the status message display interval
	iStatusMsgDispInterval = getStatusMsgDispInterval();

	/*
	 * ---------------------------------
	 * STEP 7: Initialize the SSI Module
	 * ---------------------------------
	 */
	rv = initSSIModule();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to init SSI module", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to Initialize SSI Module");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		showPSGenDispForm(MSG_APP_LOAD_ERR, FAILURE_ICON, iStatusMsgDispInterval);

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Failed to Initiate SSI Module");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
		}

		while(1)
		{
			svcWait(10);
		}

	}
	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "SSI Module Initiated Successfully");
		addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
	}

	/*
	 * ---------------------------------
	 * STEP 8: Initialize the SCI Module
	 * ---------------------------------
	 */
	rv = initSCIModule(szSCIQId);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to init SCI module", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to Initialize SCI Module");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		showPSGenDispForm(MSG_APP_LOAD_ERR, FAILURE_ICON, iStatusMsgDispInterval);

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Failed to Initiate SCI Module");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
		}
		while(1)
		{
			svcWait(10);
		}
	}

	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "SCI Module Initiated Successfully");
		addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
	}

	/*
	 * --------------------------------------------
	 * STEP 9: Initialize the Business Logic Module
	 * --------------------------------------------
	 */
	rv = initBLogicModule(szBLQId, szErrDispMsg);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to init BL module", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to Initialize Business Logic Module");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(strlen(szErrDispMsg) == 0)
		{
			showPSGenDispForm(MSG_APP_LOAD_ERR, FAILURE_ICON, iStatusMsgDispInterval);
		}
		else
		{
			showMessageBox("Critical Error", szErrDispMsg);
		}
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Failed to Initiate Business Logic Module");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
		}
		while(1)
		{
			svcWait(10);
		}

	}
	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "Business Logic Module Initiated Successfully");
		addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
	}

	/*
	 * --------------------------------------------
	 * STEP 10: Show the System Information Screen
	 * --------------------------------------------
	 */
	rv = showSystemInfo();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to show System Information screen at the startup", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to show system information screen at the start up");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		showPSGenDispForm(MSG_APP_LOAD_ERR, FAILURE_ICON, iStatusMsgDispInterval);

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Failed to Show System Information Screen");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
		}
		while(1)
		{
			svcWait(10);
		}
	}

	/*
	 * --------------------------------------------
	 * STEP 11: Show the Idle Screen
	 * --------------------------------------------
	 */
	rv = showIdleScreen();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to show Idle screen at the startup", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to show Idle screen at the start up");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		showPSGenDispForm(MSG_APP_LOAD_ERR, FAILURE_ICON, iStatusMsgDispInterval);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Failed to Show idle Screen");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
		}
		while(1)
		{
			svcWait(10);
		}
	}

	/*
	 * --------------------------------------------
	 * STEP 12: Start the event processor
	 * --------------------------------------------
	 */

	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "Waiting for POS Request");
		addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
		addAppEventLog(SCA, PAAS_INFO, START_UP, END_INDICATOR, NULL);
	}

	rv = sciEvtProcssr(szSCIQId, szBLQId);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: SaveLogs
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void saveLogs()
{
#ifdef DEBUG
	if(giSaveDebugLogs == 1)
	{
		SaveDebugLogs();
	}
#endif
}

/*
 * ============================================================================
 * Function Name: SaveLogs
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void saveMemLogs()
{
#ifdef DEBUG
	if(giSaveMemDebugLogs == 1)
	{
		SaveDebugLogs();
	}
#endif
}

/*
 * ============================================================================
 * Function Name: sciEvtProcssr
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int sciEvtProcssr(char * szSCIQId, char * szBLQId)
{
	int				rv					= SUCCESS;
	int				iSize				= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	SCIDATA_PTYPE	pstSCIData			= NULL;
	BLDATA_STYPE	stBLData;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: SCI Queue Id [%s], Business Logic Queue Id [%s]", __FUNCTION__, szSCIQId, szBLQId);
	APP_TRACE(szDbgMsg);

	gbSCAInitDone = PAAS_TRUE;	//T_MukeshS3: variable to check whether SCA is successfully initialized or not.

	while(1)
	{
		/* Look for any SCI events in the SCI data queue */
		pstSCIData = (SCIDATA_PTYPE) getDataFrmQ(szSCIQId, &iSize);
		if(pstSCIData == NULL)
		{
			svcWait(10); //Praveen_P1: Reducing svcWait to minimum value to increase the turn around time[Request-Response time]
			continue;
		}

		/* -------- Some SCI event found in the queue ------- */

		memset(&stBLData, 0x00, sizeof(BLDATA_STYPE));
		memset(szAppLogData, 0x00, sizeof(szAppLogData));

		if(pstSCIData->iEvtType == DISCONN)
		{
			/* In case of POS disconnection */
			strcpy(stBLData.szConnInfo, pstSCIData->szConnInfo);
			stBLData.iEvtType = DISCONN;

			rv = SUCCESS;
		}
		else if(pstSCIData->iEvtType == DATA)
		{
			/* In case data was received from POS */

			if(iAppLogEnabled == 1)
			{
				addAppEventLog(SCA, PAAS_INFO, START, START_INDICATOR, NULL);
				strcpy(szAppLogData, "Request Received From POS");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}

			rv = procSCIReqData(pstSCIData, &stBLData);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get Business Logic data",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid event type", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received Invalid Request From POS, Please Send a Valid Request");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}

			rv = FAILURE;
		}

		if(rv == SUCCESS)
		{
			/* Queue the Business Logic data node for the business logic process
			 * to look into and subsequently process */
			rv = addDataToQ(&stBLData, sizeof(stBLData), szBLQId);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to add BL tran to queue",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				deleteTran(stBLData.szTranKey);
			}
		}

		/* Flush out data to get back the allocated memory after the use is
		 * complete */
		if(pstSCIData->szMsg != NULL)
		{
			//free(pstSCIData->szMsg);
			scaFree((void **)&(pstSCIData->szMsg), __LINE__, (char *)__FUNCTION__);
		}

		//free(pstSCIData);
		scaFree((void **)&(pstSCIData), __LINE__, (char *)__FUNCTION__);
		pstSCIData = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Retunrning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: procSCIReqData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int procSCIReqData(SCIDATA_PTYPE pstSCIData, BLDATA_PTYPE pstBLData)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	tranKey[10]			= "";
	char	macLbl[10]			= "";
	char	szErrMsg[100]		= "";
	char	szAppLogData[300]	= "";
	int		iFxn				= 0;
	int		iCmd				= 0;
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	while(1)
	{
		/* Create a transaction place holder */
		rv = createTran(tranKey);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create a new transaction",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		
		/* Pass the transaction data for processing */
		rv = processSCIReq((char **) &pstSCIData->szMsg, pstSCIData->iSize, tranKey,
												macLbl, szErrMsg, pstSCIData->iPortType);
		if(rv != SUCCESS)
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Process the POS Request");
				addAppEventLog(SCA, PAAS_FAILURE, RECEIVE, szAppLogData, NULL);
			}

			debug_sprintf(szDbgMsg, "%s: FAILED to process SCI request",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		rv = getFxnNCmdForTran(tranKey, &iFxn, &iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get function and command",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(iFxn == SCI_SCND && iCmd == SCI_CANCEL)
		{
			debug_sprintf(szDbgMsg, "%s: Setting Event Type to CANCEL for BL", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received CANCEL Command Through Secondary Port");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}

			pstBLData->iEvtType = CANCEL;
		}
		else if(iFxn == SCI_SCND && iCmd == SCI_REBOOT)
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received REBOOT Command Through Secondary Port");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}
			pstBLData->iEvtType = REBOOT;
		}
		else if(iFxn == SCI_SCND && iCmd == SCI_STATUS)
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received STATUS Command Through Secondary Port");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}
			pstBLData->iEvtType = STATUS;
		}
		else if(iFxn == SCI_SCND && iCmd == SCI_ANY_UPDATES)
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received ANY_UPDATES Command Through Secondary Port");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}
			pstBLData->iEvtType = UPDATE_QUERY;
		}
		else if(iFxn == SCI_SCND && iCmd == SCI_UPDATE_STATUS)
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received UPDATE_STATUS Command Through Secondary Port");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}
			pstBLData->iEvtType = UPDATE_STATUS;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Event Type to DATA for BL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstBLData->iEvtType = DATA;
		}
		strcpy(pstBLData->szMacLbl, macLbl);
		strcpy(pstBLData->szTranKey, tranKey);
		strcpy(pstBLData->szConnInfo, pstSCIData->szConnInfo);

		break;
	}

	if((rv != SUCCESS) && (strlen(tranKey) > 0))
	{
		/* Set the error status for the transaction */
		setStatusInTran(tranKey, rv);

		/* Set the response details for the transaction */
		setGenRespDtlsinTran(tranKey, szErrMsg);

		/* Send the response to the POS, using the SCI module */
		sendSCIResp(pstSCIData->szConnInfo, tranKey, macLbl);

		/* Delete the transaction placeholder */
		deleteTran(tranKey);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: showSystemInfo
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int showSystemInfo()
{
	int			rv						= SUCCESS;
	PAAS_BOOL	bDHCPMode				= PAAS_FALSE;
	int			listnPortNum			= 0;
	char		szNetworkIp[16]			= "";
	char		szNwkMACAddr[25 + 1]	= "";
	char		szErrMsg[256]			= "";
#ifdef DEBUG
	char	szDbgMsg[256]				= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Get the network mode and IP to be displayed on the sys information
		 * screen */
		memset(szNwkMACAddr, 0x00, sizeof(szNwkMACAddr));
		rv = getNtwrkModeAndIp(&bDHCPMode, szNetworkIp, szNwkMACAddr);
		if(rv != SUCCESS)
		{
			sprintf(szErrMsg, "%s: FAILED to get N/W mode and IP", __FUNCTION__);
			APP_TRACE(szErrMsg);
			syslog(LOG_ERR|LOG_USER, szErrMsg);
			break;
		}

		/* Get the listening port number for the purpose of displaying it on the
		 * system information screen */
		listnPortNum = getListeningPortNum();

		/* Fetch the system information and display it on the device screen */
		rv = showSystemInformation(bDHCPMode, szNetworkIp, listnPortNum);
		if(rv != SUCCESS)
		{
			sprintf(szErrMsg, "%s: FAILED to show SysInfo", __FUNCTION__);
			APP_TRACE(szErrMsg);
			syslog(LOG_ERR|LOG_USER, szErrMsg);
			break;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Retunrning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: createQueueForBL
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int createQueueForBL(char *pszQID)
{
	int			rv					= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Create queue for reading the business logic data */
	rv = createQueue(pszQID);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create rcv Q", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	debug_sprintf(szDbgMsg, "%s: Queue Id created for BL data[%s]", __FUNCTION__, pszQID);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Retunrning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: scaMalloc
 *
 * Description	: Wrapper for malloc, will print num of bytes and starting
 * 					address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void *scaMalloc(unsigned int iSize, int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalAllocMem		= 0;
	void *		tmpPtr				= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	tmpPtr = malloc(iSize);

	if(tmpPtr != NULL)

	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] StartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				__FUNCTION__, tmpPtr, iSize, pszCallerFuncName, iLineNum);
		APP_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", __FUNCTION__);
		APP_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: scaReAlloc
 *
 * Description	: Wrapper for realloc, will print num of bytes and starting
 * 					and original address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void *scaReAlloc(void *ptr, unsigned int iSize, int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalReallocMem= 0;
	void *		tmpPtr				= NULL;
	char		szOrigAddr[16]		= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//Storing Original Address
	memset(szOrigAddr, 0x00, sizeof(szOrigAddr));
	sprintf(szOrigAddr, "%p", ptr);

	tmpPtr = realloc(ptr, iSize);

	if(tmpPtr != NULL)
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] OrigStartAddr[%s] NewStartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				__FUNCTION__, szOrigAddr, tmpPtr, iSize, pszCallerFuncName, iLineNum);
		APP_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", __FUNCTION__);
		APP_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: scaFree
 *
 * Description	: Wrapper for free, will print going to be free
 * 					address also and make the freed pointer points to NULL
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void scaFree(void **pptr, int iLineNum, char * pszCallerFuncName)
{
	//size_t *sizePtr					= ((size_t *)ptr) - 1;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(*pptr != NULL)
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] FreedAddr[%p] called by [%s] from Line[%d]",
				__FUNCTION__, *pptr, pszCallerFuncName, iLineNum);
		APP_MEM_TRACE(szDbgMsg);
		free(*pptr);
		*pptr = NULL;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: [MEMDEBUG] NULL params Passed ", __FUNCTION__);
		APP_MEM_TRACE(szDbgMsg);
	}
}

#ifdef DEBUG
#define BACKTRACE 0
#ifdef  BACKTRACE // TESTTING purposes only 
#include <execinfo.h>

static void threadBckTrace()
{
	int j = 0, nptrs;
#define SIZE 100
	void *buffer[SIZE];
	char **strings;
#ifdef DEBUG
	char	szDbgMsg[4096]	= "";
#endif

	nptrs = backtrace(buffer, SIZE);
	debug_sprintf(szDbgMsg, "%s: backtrace() returned %d addresses", __FUNCTION__, nptrs);
	APP_TRACE(szDbgMsg);

	// NOTE:
	// backtrace_symbols() allocates the strings on the heap, while backtrace_symbols_fd() writes the strings
	// to a file descriptor that you can read
	strings = backtrace_symbols(buffer, nptrs);
	if (strings == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: backtrace_symbols() error!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		for (j = 0; j < nptrs; j++)
		{
			debug_sprintf(szDbgMsg, "%s: %s", __FUNCTION__, strings[j]);
			APP_TRACE(szDbgMsg);
		}

	// Free the string pointers
		free(strings);
	}
}
#endif // BACKTRACE

static void catch_stop()
{
}

/*
 * ============================================================================
 * Function Name: setupSigHandlers
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void setupSigHandlers()
{
	char	szDbgMsg[256]	= "";
	char	szCommand[64]  	= "";
	FILE* 	fptr			= NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Set up signal handlers */
	if(SIG_ERR == signal(SIGABRT, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGABRT",
						__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGSEGV, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGSEGV",
						__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGILL, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGILL",
						__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGKILL, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGKILL",
						__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGSTOP, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGSTOP",
						__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if(SIG_ERR == signal(SIGTERM, sigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGSTOP",
						__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}


	sprintf(szCommand,"pidof frmAgent.exe > ./flash/faPid.txt");
	if(local_svcSystem(szCommand) != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Could not execute the command %s",
												__FUNCTION__, szCommand);
		APP_TRACE(szDbgMsg);
	}


	/* Reading the pid from the file inorder to kill the FA
	 * Stored the pid during the setting up of the signal handlers
	 */
	fptr = fopen("./flash/faPid.txt", "r");
	if(fptr != NULL)
	{
		if(fscanf(fptr,"%d", &faPid) != 1)
		{
			debug_sprintf(szDbgMsg, "%s: Could not read data from file!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		fclose(fptr);

		local_svcSystem("rm ./flash/faPid.txt");
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Cannot open the file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: sigHandler
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void sigHandler(int iSigNo)
{
	char	szDbgMsg[256]	= "";
	int		rv				= SUCCESS;
	struct 	sigaction 		setup_action;
	sigset_t block_mask;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pthread_t self = pthread_self();	

	debug_sprintf(szDbgMsg, "%s: threadId=%lu", __FUNCTION__, (unsigned long)self);
	APP_TRACE(szDbgMsg);
	
	sigemptyset(&block_mask);

	sigaddset(&block_mask, SIGABRT);
	sigaddset(&block_mask, SIGSEGV);
	sigaddset(&block_mask, SIGILL);
	sigaddset(&block_mask, SIGPIPE);

	setup_action.sa_handler = catch_stop;
	setup_action.sa_mask = block_mask;
	setup_action.sa_flags = 0;
	sigaction(iSigNo, &setup_action, NULL);

	switch(iSigNo)
	{
	case SIGABRT:
		sprintf(szDbgMsg, "%s: SIGABRT signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		savePosInfoRecords(); /* Save the POS current details list in the persistent memory */
		cleanCURLHandle(PAAS_TRUE);

		saveLogs();
		saveMemLogs();

		/* Unregister the SCA from VHQ Agent */
		rv = fnpVHQUnRgstrApp(SCA_APP_NAME);
		if(rv == LTMS_UNREG_SUCCESS)
		{
			sprintf(szDbgMsg, "%s: Successfully Unregistered from VHQ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* Killing the FA since SCA is dying*/
		rv = kill(faPid, SIGTERM);
#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGSEGV:
		sprintf(szDbgMsg, "%s: SIGSEGV signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		savePosInfoRecords(); /* Save the POS current details list in the persistent memory */
		cleanCURLHandle(PAAS_TRUE);

		saveLogs();
		saveMemLogs();

		/* Unregister the SCA from VHQ Agent */
		rv = fnpVHQUnRgstrApp(SCA_APP_NAME);
		if(rv == LTMS_UNREG_SUCCESS)
		{
			sprintf(szDbgMsg, "%s: Successfully Unregistered from VHQ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		/* Killing the FA since SCA is dying*/
		rv = kill(faPid, SIGTERM);
#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGILL:
		sprintf(szDbgMsg, "%s: SIGILL signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		savePosInfoRecords(); /* Save the POS current details list in the persistent memory */
		cleanCURLHandle(PAAS_TRUE);

		saveLogs();
		saveMemLogs();

		/* Unregister the SCA from VHQ Agent */
		rv = fnpVHQUnRgstrApp(SCA_APP_NAME);
		if(rv == LTMS_UNREG_SUCCESS)
		{
			sprintf(szDbgMsg, "%s: Successfully Unregistered from VHQ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		/* Killing the FA since SCA is dying*/
		rv = kill(faPid, SIGTERM);
#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGKILL:
		sprintf(szDbgMsg, "%s: SIGKILL signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		savePosInfoRecords(); /* Save the POS current details list in the persistent memory */
		cleanCURLHandle(PAAS_TRUE);

		saveLogs();
		saveMemLogs();

		/* Unregister the SCA from VHQ Agent */
		rv = fnpVHQUnRgstrApp(SCA_APP_NAME);
		if(rv == LTMS_UNREG_SUCCESS)
		{
			sprintf(szDbgMsg, "%s: Successfully Unregistered from VHQ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		/* Killing the FA since SCA is dying*/
		rv = kill(faPid, SIGTERM);
#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGSTOP:
		sprintf(szDbgMsg, "%s: SIGSTOP signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		savePosInfoRecords(); /* Save the POS current details list in the persistent memory */
		cleanCURLHandle(PAAS_TRUE);

		saveLogs();
		saveMemLogs();

		/* Unregister the SCA from VHQ Agent */
		rv = fnpVHQUnRgstrApp(SCA_APP_NAME);
		if(rv == LTMS_UNREG_SUCCESS)
		{
			sprintf(szDbgMsg, "%s: Successfully Unregistered from VHQ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		/* Killing the FA since SCA is dying*/
		rv = kill(faPid, SIGTERM);
#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGTERM:
		sprintf(szDbgMsg, "%s: SIGTERM signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);

		savePosInfoRecords(); /* Save the POS current details list in the persistent memory */
		cleanCURLHandle(PAAS_TRUE);

		saveLogs();
		saveMemLogs();

		/* Unregister the SCA from VHQ Agent */
		rv = fnpVHQUnRgstrApp(SCA_APP_NAME);
		if(rv == LTMS_UNREG_SUCCESS)
		{
			sprintf(szDbgMsg, "%s: Successfully Unregistered from VHQ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		/* Killing the FA since SCA is dying*/
		rv = kill(faPid, SIGTERM);
#ifdef BACKTRACE
		threadBckTrace();
#endif
		exit(-1);
		break;

	case SIGPIPE:
		sprintf(szDbgMsg, "%s: SIGPIPE signal received", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);
		break;

	default:
		sprintf(szDbgMsg, "%s: Signal no = [%d]", __FUNCTION__, iSigNo);
		APP_TRACE(szDbgMsg);
		syslog(LOG_ERR|LOG_USER, szDbgMsg);
	}

	return;
}
#endif

/*
 * ============================================================================
 * End of file main.c
 * ============================================================================
 */
