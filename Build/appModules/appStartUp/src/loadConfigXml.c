/*
 * ============================================================================
 * File Name	: ldCfgParams.c
 *
 * Description	: This file contains the definition of the functions used for
 * 					loading all the parameters needed for the PaaS application.
 *
 * Author		: Vikram Datt Rana
 * ============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <libsecins.h>
#include "common/cfgData.h"
#include "iniparser.h"
#include "svc.h"
#include "common/utils.h"
#include "common/xmlUtils.h"
#include "db/cdtExtern.h"
#include "sci/sciConfigDef.h"
#include "uiAgent/uiConstants.h"
#include "appLog/appLogCfgDef.h"
#include "appLog/appLogAPIs.h"
#include "bLogic/blVHQSupDef.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

/* Static functions's declarations - END */
static int getLangNameForThisId(int, char *);
static int storeCBackParamsInCfgFile(char *, char *);
static int storeCtrTipInCfgFile(char *, char *);
static int checkForCDTParams(char *, char *);
static int updateBoolValInCfg(char *, char *, char *);
static int storeDefLangValinCfg(char *, char *, char *);
static int storeRcptParamsInCfgFile(char *, char *, int);
static int storeParamsInLocalCfgFile(GENKEYVAL_PTYPE);
static void freeParamValList(GENKEYVAL_PTYPE);
static int restartApp(char *);
static int getHostNameFromConfig(char *, int);
//External function
extern int getEmvParamKeyForIndex(int, char *, int);

static PAAS_BOOL gbDHIRestart 		= PAAS_FALSE;
static PAAS_BOOL gbRedoAppLogInit	= PAAS_FALSE;

/*
 * ============================================================================
 * Function Name: getLangNameForThisId
 *
 * Description	: This function is responsible for parsing the display prompts
 * 					ini file and get the lang name for the given language id
 *
 * Input Params	: Language Id, Buffer to store the language name
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getLangNameForThisId(int iLangId, char * pszLanVal)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				langCnt			= 0;
	char *			langName		= NULL;
	dictionary *	dict			= NULL;
	char			szKey[40]		= "";
	char *			value			= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Load the ini file in the parser library */
		dict = iniparser_load(PROMPTS_FILE_NAME);
		if(dict == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to load ini file [%s]",
					__FUNCTION__, PROMPTS_FILE_NAME);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the number of language definitons stored in the ini file */
		langCnt = iniparser_getnsec(dict);
		if(langCnt <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No sections present in ini file [%s]",
					__FUNCTION__, PROMPTS_FILE_NAME);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Number of sections present = [%d]",
				__FUNCTION__, langCnt);
		APP_TRACE(szDbgMsg);

		/* Get the language definitions */
		for(iCnt = 0; iCnt < langCnt; iCnt++)
		{
			langName = iniparser_getsecname(dict, iCnt);

			debug_sprintf(szDbgMsg, "%s: iCnt[%d], langName[%s]",__FUNCTION__, iCnt, langName);
			APP_TRACE(szDbgMsg);

			sprintf(szKey, "%s:%s", langName, "id");

			debug_sprintf(szDbgMsg, "%s: Key is %s",__FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);

			value = iniparser_getstring(dict, szKey, NULL);
			if(value == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Language Id is missing for %s",__FUNCTION__, szKey);
				APP_TRACE(szDbgMsg);

				continue; //Is it fine?? Please check
			}

			debug_sprintf(szDbgMsg, "%s: Key[%s] = Value[%s]",__FUNCTION__, szKey, value);
			APP_TRACE(szDbgMsg);

			if(atoi(value) == iLangId)
			{
				debug_sprintf(szDbgMsg, "%s: Language Is matches!!!",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(pszLanVal, langName);
				break; //Got the corresponding language id, no need to check other languages
			}
		}
		if(strlen(pszLanVal) == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Did not find the corresponding Language id in the display prompts file, defaulting to English",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			sprintf(pszLanVal, "%s", "ENGLISH"); //Default language is English
		}

		break; //Breaking from the while loop
	}


	/* Free the dictionary */
	if(dict != NULL)
	{
		iniparser_freedict(dict);
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: storeCBackParamsInCfgFile
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeCBackParamsInCfgFile(char * pszKey, char * pszVal)
{
	int		rv				= SUCCESS;
	int		iIndex			= 0;
	char	szParam[20]		= "";
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strcasecmp(pszKey, "Lmt") == SUCCESS)
	{
		sprintf(szParam, "%s", CB_AMT_LIMIT);
	}
	else
	{
		iIndex = atoi(pszKey);

		if( (iIndex < 1) || (iIndex > 5) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid index %d for cback preset amt",
					__FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
		else
		{
			sprintf(szParam, "%s%d", CFG_CBAMT_PREFIX, iIndex);
		}
	}

	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Updating %s in section [%s] with value %s"
				, __FUNCTION__, szParam, SECTION_CBACK, pszVal);
		APP_TRACE(szDbgMsg);

		putEnvFile(SECTION_CBACK, szParam, pszVal);
	}

	rv = SUCCESS;

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeCtrTipInCfgFile
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeCtrTipInCfgFile(char * pszKey, char * pszVal)
{
	int		rv				= SUCCESS;
	int		iIndex			= 0;
	char	szParam[20]		= "";
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iIndex = atoi(pszKey);

	if( (iIndex < 1) || (iIndex > 4) )
	{
		debug_sprintf(szDbgMsg, "%s: Invalid index %d for counter tip preset",
				__FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		sprintf(szParam, "%s%d", CFG_CTRTIP_PREFIX, iIndex);
	}

	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Updating %s in section [%s] with value %s"
				, __FUNCTION__, szParam, SECTION_CTRTIP,pszVal);
		APP_TRACE(szDbgMsg);

		putEnvFile(SECTION_CTRTIP, szParam, pszVal);
	}

	rv = SUCCESS;

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeConsumerParamsInCfgFile
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeConsumerParamsInCfgFile(char * pszKey, char * pszVal)
{
	int		rv				= SUCCESS;
	int		iIndex			= 0;
	int		iVal			= -1;
	char	szParam[20]		= "";
	char	szVal[20]		= "";
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iIndex = atoi(pszKey);

	if( (iIndex < 1) || (iIndex > 6) )
	{
		debug_sprintf(szDbgMsg, "%s: Invalid index %d for consumer option",
				__FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		sprintf(szParam, "%s%d", CONSUMER_OPTION_PREFIX, iIndex);
	}

	if(rv == SUCCESS)
	{
		iVal = atoi(pszVal);
		switch(iVal)
		{
		case 0:
			/* Need not set with a value for 0*/
			break;

		case 1:
			sprintf(szVal, "GiftReceipt");
			break;

		case 2:
			sprintf(szVal, "PrivCard");
			break;

		case 3:
			sprintf(szVal, "EmailOffer");
			break;

		case 4:
			sprintf(szVal, "EmailReceipt");
			break;

		default:
			rv = FAILURE;
			debug_sprintf(szDbgMsg, "%s: Wrong Option given", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(rv == SUCCESS)
		{
			putEnvFile(SECTION_CONSUMEROPTS, szParam, szVal);
			debug_sprintf(szDbgMsg, "%s: Updating %s in section [%s] with value %s"
					, __FUNCTION__, szParam, SECTION_CONSUMEROPTS, szVal);
			APP_TRACE(szDbgMsg);
		}
	}

	rv = SUCCESS;

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateBoolValInCfg
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int updateBoolValInCfg(char * pszSection, char * pszKey, char * pszVal)
{
	int		rv				= SUCCESS;
	char *	cTmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(*pszVal == '1')
	{
		cTmpPtr = "Y";
	}
	else if(*pszVal == '0')
	{
		cTmpPtr = "N";
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Incorrect parameter bool value %s",
				__FUNCTION__, pszVal);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Updating %s in section [%s] with val %s",
				__FUNCTION__, pszKey, pszSection, cTmpPtr);
		APP_TRACE(szDbgMsg);

		putEnvFile(pszSection, pszKey, cTmpPtr);
	}

	rv = SUCCESS;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeDefLangValinCfg
 *
 * Description	: Stores the value of the Defaul Language config var from the
 * 				  value comes from the xml file
 *
 * Input Params	: Section name, Key and its value
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeDefLangValinCfg(char * pszSection, char * pszKey, char * pszVal)
{
	int		rv				= SUCCESS;
	char	szLanVal[20]	= "";
	int		iLangId			= -1;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLangId = atoi(pszVal);

	debug_sprintf(szDbgMsg, "%s: Language ID is %d", __FUNCTION__, iLangId);
	APP_TRACE(szDbgMsg);

	rv = getLangNameForThisId(iLangId, szLanVal);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while getting Language name for this ID", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return rv;
	}

	debug_sprintf(szDbgMsg, "%s: Language Value is %s", __FUNCTION__, szLanVal);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Updating %s in section [%s] with val %s",
			__FUNCTION__, pszKey, pszSection, szLanVal);
	APP_TRACE(szDbgMsg);

	putEnvFile(pszSection, pszKey, szLanVal);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeRcptParamsInCfgFile
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeRcptParamsInCfgFile(char * pszKey, char * pszVal, int opt)
{
	int		rv				= SUCCESS;
	int		iIndex			= 0;
	char	szParam[16]		= "";
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//iIndex = atoi(pszKey);
	iIndex	= pszKey[0] - 48;
	if( (iIndex < 1) || (iIndex > 4) || (pszKey[1] != 0 &&  pszKey[1] != '1'))
	{
		debug_sprintf(szDbgMsg, "%s: Invalid parameter key index [%s]",
				__FUNCTION__, pszKey);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		switch(opt)
		{
		case 1:
			/* Header */
			sprintf(szParam, "%s%d", RCT_HEADER_PREFIX, iIndex);
			break;

		case 2:
			/* Footer */
			sprintf(szParam, "%s%d", RCT_FOOTER_PREFIX, iIndex);
			break;

		case 3:
			/* Disclaimer */
			sprintf(szParam, "%s%d", RCT_DCLAIM_PREFIX, iIndex);
			break;

		case 4:
			/* DCC Disclaimer */
			sprintf(szParam, "%s%d", RCT_DCC_DCLAIM_PREFIX, iIndex);
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
	}

	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Updating %s in section [%s] with val %s",
				__FUNCTION__, szParam, SECTION_RECEIPT, pszVal);
		APP_TRACE(szDbgMsg);

		putEnvFile(SECTION_RECEIPT, szParam, pszVal);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeParamsInLocalCfgFile
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int storeParamsInLocalCfgFile(GENKEYVAL_PTYPE paramValLstPtr)
{
	int			rv					= SUCCESS;
	int			iLen				= 0;
	int			iVal				= -1;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[256]	= "";
	char		szSectionName[10]	= "";
	char		szHostName[30]		= "";
	char *		cKeyPtr				= NULL;
	char *		cValPtr				= NULL;
	char *		cHostNamePtr		= NULL;
#ifdef DEBUG
	char		szDbgMsg[512]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "Storing the XML Data to the Local Configuration File");
		addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
	}

	while( (paramValLstPtr != NULL) && (rv == SUCCESS) )
	{
		cKeyPtr = paramValLstPtr->key;
		cValPtr = paramValLstPtr->value;
		cHostNamePtr = paramValLstPtr->hostName;

		if(strncasecmp(cKeyPtr, "Cashbk",(iLen=(strlen("Cashbk")))) == SUCCESS)
		{
			if(strlen(cKeyPtr) == iLen)
			{
				/* Set if cashback is enabled/disabled */
				updateBoolValInCfg(SECTION_PAYMENT, CASHBACK_ENABLED, cValPtr);
			}
			else
			{
				storeCBackParamsInCfgFile(cKeyPtr + iLen, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, EBTCASHBACK_ENABLED) == SUCCESS)
		{
			/* Set if ebt cashback is enabled/disabled */
			updateBoolValInCfg(SECTION_PAYMENT, EBTCASHBACK_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, XML_CFGFILE_PATH) == SUCCESS)
		{
			/* Set the path for the xml config file */
			putEnvFile(SECTION_REG, XML_CFGFILE_PATH, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, PREAMBLE_SUCCESS) == SUCCESS)
		{
			/* Set if preamble is success */
			putEnvFile(SECTION_REG, PREAMBLE_SUCCESS, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "Preamblepingreqd") == SUCCESS)
		{
			/* Set if preamble ping is required */
			updateBoolValInCfg(SECTION_DEVICE, "preamblepingreqd", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, HOST_PING_REQD) == SUCCESS)
		{
			/* Set if host ping is required */
			updateBoolValInCfg(SECTION_DEVICE, HOST_PING_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, GIFTREFUND_ENABLED) == SUCCESS)
		{
			/* Set if gift refund is enabled */
			updateBoolValInCfg(SECTION_PAYMENT, GIFTREFUND_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "IPPROMPT") == SUCCESS)
		{
			/* Set if network configuration is required */
			updateBoolValInCfg(SECTION_DEVICE, NETWORK_CFG_REQD, cValPtr);
		}
		else if((strcasecmp(cKeyPtr, "SplitTndr") == SUCCESS) ||
				(strcasecmp(cKeyPtr, "SPLIT") == SUCCESS))
		{
			/* Set if split tender is enabled/disabled */
			updateBoolValInCfg(SECTION_PAYMENT,SPLT_TNDR_ENABLED,cValPtr);
		}
		else if((strcasecmp(cKeyPtr, "CptrLylty") == SUCCESS) ||
				(strcasecmp(cKeyPtr, "LOYALTY") == SUCCESS))
		{
			/* Set if loyalty capture is enabled/disabled */
			updateBoolValInCfg(SECTION_PAYMENT, LYLTYCAP_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "CptrEmail") == SUCCESS)
		{
			/* Set if email capture is enabled/disabled */
			updateBoolValInCfg(SECTION_PAYMENT, EMAILCAP_ENABLED, cValPtr);
		}
		else if((strcasecmp(cKeyPtr, "PartlAuth") == SUCCESS) ||
				(strcasecmp(cKeyPtr, "PARTATH1") == SUCCESS))
		{
			/* Set if partial authorization is enabled/disabled */
			updateBoolValInCfg(SECTION_PAYMENT, PARTAUTH_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "SigCaptr") == SUCCESS)
		{
			/* Set if signature capture is enabled/disabled */
			updateBoolValInCfg(SECTION_PAYMENT, SIGCAP_ENABLED, cValPtr);
		}
		else if(strncasecmp(cKeyPtr, "GRAT", 4) == SUCCESS)
		{
			if(strcasecmp(cKeyPtr + 4, "ON1") == SUCCESS)
			{
				/* We are not implementing GRATON1 param name in this release
				 * so ignore this value */
			}
			else
			{
				/* Store the preset counter tip value */
				rv = storeCtrTipInCfgFile(cKeyPtr + 4, cValPtr);
			}
		}
		else if((strcasecmp(cKeyPtr, "PRTIP1") == SUCCESS) ||
				(strcasecmp(cKeyPtr, "PRTIP") == SUCCESS))
		{
			/* Support for TIP - PRTIP1 param name */
			updateBoolValInCfg(SECTION_PAYMENT, TIP_SUPP_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "CPTIP") == SUCCESS)
		{
			updateBoolValInCfg(SECTION_PAYMENT, CTRTIP_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "LANGX") == SUCCESS)
		{
			storeDefLangValinCfg(SECTION_DEVICE, DFLT_LANG, cValPtr);
		}
		else if(strncasecmp(cKeyPtr, "HDR", 3) == SUCCESS)
		{
			/* Store headers for the receipt */
			storeRcptParamsInCfgFile(cKeyPtr + 3, cValPtr, 1);
		}
		else if(strncasecmp(cKeyPtr, "FOOT", 4) == SUCCESS)
		{
			/* Store footers for the reciept */
			storeRcptParamsInCfgFile(cKeyPtr + 4, cValPtr, 2);
		}
		else if(strncasecmp(cKeyPtr, "DISCL", 5) == SUCCESS)
		{
			/* Store disclaimers for the reciept */
			storeRcptParamsInCfgFile(cKeyPtr + 5, cValPtr, 3);
		}
		else if(strncasecmp(cKeyPtr, "DCCDISCLAIMER_", 14) == SUCCESS)
		{
			/* Store disclaimers for the reciept */
			storeRcptParamsInCfgFile(cKeyPtr + 14, cValPtr, 4);
		}
		else if((strcasecmp(cKeyPtr, "SAFFAIL1") == SUCCESS) ||
				(strcasecmp(cKeyPtr, "BAUTH") == SUCCESS))
		{
			/* Set if SAF processing is enabled/disabled */
			updateBoolValInCfg(SECTION_PAYMENT, SAF_ENABLED, cValPtr);
		}
		else if (strcasecmp(cKeyPtr, "GENRECEIPTXML") == SUCCESS)
		{
			updateBoolValInCfg(SECTION_PAYMENT, RECEIPT_ENABLED, cValPtr);
		}
		else if (strcasecmp(cKeyPtr, CREDIT_TYPE) == SUCCESS)
		{
			updateBoolValInCfg(SECTION_TENDER, CREDIT_TYPE, cValPtr);
		}
		else if (strcasecmp(cKeyPtr, PRIVATE_TYPE) == SUCCESS)
		{
			updateBoolValInCfg(SECTION_TENDER, PRIVATE_TYPE, cValPtr);
		}
		else if (strcasecmp(cKeyPtr, MERCHCREDIT_TYPE) == SUCCESS)
		{
			updateBoolValInCfg(SECTION_TENDER, MERCHCREDIT_TYPE, cValPtr);
		}
		else if (strcasecmp(cKeyPtr, PAYPAL_TYPE) == SUCCESS)
		{
			updateBoolValInCfg(SECTION_TENDER, PAYPAL_TYPE, cValPtr);
		}
		else if (strcasecmp(cKeyPtr, EBT_TYPE) == SUCCESS)
		{
			updateBoolValInCfg(SECTION_TENDER, EBT_TYPE, cValPtr);
		}
		else if (strcasecmp(cKeyPtr, FSA_TYPE) == SUCCESS)
		{
			updateBoolValInCfg(SECTION_TENDER, FSA_TYPE, cValPtr);
		}
		else if (strcasecmp(cKeyPtr, DCC_ENABLED) == SUCCESS)
		{
			updateBoolValInCfg(SECTION_DEVICE, DCC_ENABLED, cValPtr);
		}
		else if((strcasecmp(cKeyPtr, "DEBIT1") == SUCCESS) ||
				(strcasecmp(cKeyPtr, "DEBIT") == SUCCESS))
		{
			/* Set if DEBIT processing is enabled/disabled */
			updateBoolValInCfg(SECTION_TENDER, DEBIT_TYPE, cValPtr);
		}
		else if((strcasecmp(cKeyPtr, "GIFT1") == SUCCESS) ||
				(strcasecmp(cKeyPtr, "GIFT") == SUCCESS))
		{
			/* Set if GIFT processing is enabled/disabled */
			updateBoolValInCfg(SECTION_TENDER, GIFTCARD_TYPE, cValPtr);
		}
/*		KranthiK1: Removing them as not used by any customer
 * 		else if (strcasecmp(cKeyPtr, GWALLET_TYPE) == SUCCESS)
		{
			 Set if Google Wallet is enable/disabled
			updateBoolValInCfg(SECTION_TENDER, GWALLET_TYPE, cValPtr);
		}
		else if (strcasecmp(cKeyPtr, IWALLET_TYPE) == SUCCESS)
		{
			 Set if Google isis is enable/disabled
			updateBoolValInCfg(SECTION_TENDER, IWALLET_TYPE, cValPtr);
		}*/
		else if (strcasecmp(cKeyPtr, CASH_TYPE) == SUCCESS)
		{
			/* Set if pay by cash is enable/disabled*/
			updateBoolValInCfg(SECTION_TENDER, CASH_TYPE, cValPtr);
		}
		else if((strcasecmp(cKeyPtr, "IPPAH1") == SUCCESS) ||
				(strcasecmp(cKeyPtr, "primurl") == SUCCESS))
		{
			/* Set the PWC primary URL */
			putEnvFile(SECTION_HOST, PRIM_HOST_URL, cValPtr);
		}
		else if((strcasecmp(cKeyPtr, "IPPAP1") == SUCCESS) ||
				(strcasecmp(cKeyPtr, "primport") == SUCCESS))
		{
			/* Set the PWC primary URL's port no */
			putEnvFile(SECTION_HOST, PRIM_HOST_PORT, cValPtr);
		}
		else if((strcasecmp(cKeyPtr, "IPSAH1") == SUCCESS) ||
				(strcasecmp(cKeyPtr, "scndurl") == SUCCESS))
		{
			/* Set the PWC secondary URL */
			putEnvFile(SECTION_HOST, SCND_HOST_URL, cValPtr);
		}
		else if((strcasecmp(cKeyPtr, "IPSAP1") == SUCCESS)  ||
				(strcasecmp(cKeyPtr, "scndport") == SUCCESS))
		{
			/* Set the PWC secondary URL's port no */
			putEnvFile(SECTION_HOST, SCND_HOST_PORT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, PRIM_CONN_TO) == SUCCESS)
		{
			/* Set the primary connection time out */
			putEnvFile(SECTION_HOST, PRIM_CONN_TO, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SCND_CONN_TO) == SUCCESS)
		{
			/* Set the secondary connection time out */
			putEnvFile(SECTION_HOST, SCND_CONN_TO, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, PRIM_RESP_TO) == SUCCESS)
		{
			/* Set the primary response time out */
			putEnvFile(SECTION_HOST, PRIM_RESP_TO, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SCND_RESP_TO) == SUCCESS)
		{
			/* Set the secondary response time out */
			putEnvFile(SECTION_HOST, SCND_RESP_TO, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, BAPI_PRIM_URL) == SUCCESS)
		{
			/* Set the BAPI primary URL */
			putEnvFile(SECTION_HOST, BAPI_PRIM_URL, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, BAPI_SCND_URL) == SUCCESS)
		{
			/* Set the BAPI secondary URL */
			putEnvFile(SECTION_HOST, BAPI_SCND_URL, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, BAPI_PRIM_PORT) == SUCCESS)
		{
			/* Set the BAPI primary port */
			putEnvFile(SECTION_HOST, BAPI_PRIM_PORT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, BAPI_SCND_PORT) == SUCCESS)
		{
			/* Set the BAPI secondary port */
			putEnvFile(SECTION_HOST, BAPI_SCND_PORT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, BAPI_PRIM_CONN_TO) == SUCCESS)
		{
			/* Set the BAPI primary connection time out */
			putEnvFile(SECTION_HOST, BAPI_PRIM_CONN_TO, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, BAPI_SCND_CONN_TO) == SUCCESS)
		{
			/* Set the BAPI secondary connection time out */
			putEnvFile(SECTION_HOST, BAPI_SCND_CONN_TO, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, BAPI_PRIM_RESP_TO) == SUCCESS)
		{
			/* Set the BAPI primary response timeout */
			putEnvFile(SECTION_HOST, BAPI_PRIM_RESP_TO, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, BAPI_SCND_RESP_TO) == SUCCESS)
		{
			/* Set the BAPI secondary response timeout */
			putEnvFile(SECTION_HOST, BAPI_SCND_RESP_TO, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, BAPI_DEST_ID) == SUCCESS)
		{
			/* Set the BAPI destination id */
			putEnvFile(SECTION_HOST, BAPI_DEST_ID, cValPtr);
		}
		else if((strcasecmp(cKeyPtr, "ATYPE1") == SUCCESS) ||
				(strcasecmp(cKeyPtr, "ATYPE") == SUCCESS))
		{
			/* Set the application type */
			putEnvFile(SECTION_DEVICE, APPLICATION_TYPE, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, PIN_ENCR_TYPE) == SUCCESS)
		{
			/* Set the PIN encryption type - ENCRPT1 */
			putEnvFile(SECTION_DEVICE, PIN_ENCR_TYPE, cValPtr);
		}
		else if((strcasecmp(cKeyPtr, "DEMO1") == SUCCESS) ||
				(strcasecmp(cKeyPtr, "DEMO") == SUCCESS))
		{
			/* Set the demo type for the application */
			updateBoolValInCfg(SECTION_DEVICE, DEMO_MODE, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, VSP_ENABLED) == SUCCESS)
		{
			/* Set the VSP ENABLED value for the application */
			updateBoolValInCfg(SECTION_DEVICE, VSP_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, VHQ_SUP_ENABLED) == SUCCESS)
		{
			/* Set the VHQ ENABLED value for the application */
			updateBoolValInCfg(SECTION_DEVICE, VHQ_SUP_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SWIPE_AHEAD_ENABLED) == SUCCESS)
		{
			/* Set the swipeahead ENABLED value for the application */
			updateBoolValInCfg(SECTION_DEVICE, SWIPE_AHEAD_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, BAPI_ENABLED) == SUCCESS)
		{
			/* Set the BAPI ENABLED value for the application */
			updateBoolValInCfg(SECTION_DEVICE, BAPI_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, DHI_ENABLED) == SUCCESS)
		{
			/* Set the DHI ENABLED value for the application */
			updateBoolValInCfg(SECTION_DEVICE, DHI_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SCND_PORT_ENABLED) == SUCCESS)
		{
			/* Set the SCND value for the application */
			updateBoolValInCfg(SECTION_DEVICE, SCND_PORT_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, DUP_DETECT_ENABLED) == SUCCESS)
		{
			/* Set the dupswipedetectenabled value for the application */
			updateBoolValInCfg(SECTION_DEVICE, DUP_DETECT_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, DEBIT_VOID_CARD_DTLS_REQD) == SUCCESS)
		{
			/* Set the debitvoidcarddtlsreqd value for the application */
			updateBoolValInCfg(SECTION_DEVICE, DEBIT_VOID_CARD_DTLS_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, RETURN_EMBOSSED_NUM_FOR_GIFT) == SUCCESS)
		{
			/* Set the returnembossednumforgift value for the application */
			updateBoolValInCfg(SECTION_DEVICE, RETURN_EMBOSSED_NUM_FOR_GIFT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, RETURN_EMBOSSED_NUM_FOR_PL) == SUCCESS)
		{
			/* Set the returnembossednumforprivlbl value for the application */
			updateBoolValInCfg(SECTION_DEVICE, RETURN_EMBOSSED_NUM_FOR_PL, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, CAPTURE_SIGN_FOR_MANUAL_TRAN) == SUCCESS)
		{
			/* Set the capturesignformanualtran value for the application */
			updateBoolValInCfg(SECTION_DEVICE, CAPTURE_SIGN_FOR_MANUAL_TRAN, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SEND_SIGN_DTLS_TO_SSI_HOST) == SUCCESS)
		{
			/* Set the sendsigndtlstossihost value for the application */
			updateBoolValInCfg(SECTION_DEVICE, SEND_SIGN_DTLS_TO_SSI_HOST, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, APP_LOG_ENABLED) == SUCCESS)
		{
			/* Set the transaction logging value for the application */
			updateBoolValInCfg(SECTION_DEVICE, APP_LOG_ENABLED, cValPtr);

			//Set redo app log init parameter for application to re-initialization.
			gbRedoAppLogInit = PAAS_TRUE;
		}
		else if(strcasecmp(cKeyPtr, MAX_BCKUP_LOG_FILES) == SUCCESS)
		{
			/*	Set the maximum number of logg files value */
			putEnvFile(SECTION_DEVICE, MAX_BCKUP_LOG_FILES, cValPtr);

			//Set redo app log init parameter for application to re-initialization.
			gbRedoAppLogInit = PAAS_TRUE;
		}
		else if(strcasecmp(cKeyPtr, MAX_LOG_FILE_SIZE) == SUCCESS)
		{
			/*	Set the max size of each log file */
			putEnvFile(SECTION_DEVICE, MAX_LOG_FILE_SIZE, cValPtr);

			//Set redo app log init parameter for application to re-initialization.
			gbRedoAppLogInit = PAAS_TRUE;
		}
		else if(strcasecmp(cKeyPtr, LOG_FILE_LOCATION) == SUCCESS)
		{
			/*	Set the Log file location */
			putEnvFile(SECTION_DEVICE, LOG_FILE_LOCATION, cValPtr);

			//Set redo app log init parameter for application to re-initialization.
			gbRedoAppLogInit = PAAS_TRUE;
		}
		else if(strcasecmp(cKeyPtr, VSP_ACTIVATED) == SUCCESS)
		{
			/* Set the vsp activated status for application */
			//updateBoolValInCfg(SECTION_REG, VSP_ACTIVATED, cValPtr);
			putEnvFile(SECTION_REG, VSP_ACTIVATED, cValPtr);
		}
		else if((strcasecmp(cKeyPtr, DEVTYPE_SUFFIX) == SUCCESS) ||
				(strcasecmp(cKeyPtr, "TMTYPE_SUF") == SUCCESS))
		{
			/* Set the device type */
			putEnvFile(SECTION_DEVICE, DEVTYPE_SUFFIX, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, LISTEN_PORT) == SUCCESS)
		{
			/*	Set the listening port*/
			putEnvFile(SECTION_DEVICE, LISTEN_PORT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, LISTEN_SCND_PORT) == SUCCESS)
		{
			/*	Set the listening port*/
			putEnvFile(SECTION_DEVICE, LISTEN_SCND_PORT, cValPtr);
		}
		else if((strcasecmp(cKeyPtr, "DOADMREQ") == SUCCESS) ||
				(strcasecmp(cKeyPtr, "DOADMINREQD") == SUCCESS))
		{
			/*	Set if device admin required*/
			putEnvFile(SECTION_DEVICE, DEV_ADMIN_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, PREAMBLE_SELECT_WAIT_TIME) == SUCCESS)
		{
			/*	Set the ip disp interval*/
			putEnvFile(SECTION_DEVICE, PREAMBLE_SELECT_WAIT_TIME, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, IP_DISP_INTRVL) == SUCCESS)
		{
			/*	Set the ip disp interval*/
			putEnvFile(SECTION_DEVICE, IP_DISP_INTRVL, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, DEVNAME_DISP_INTRVL) == SUCCESS)
		{
			/*	Set the dev name disp interval*/
			putEnvFile(SECTION_DEVICE, DEVNAME_DISP_INTRVL, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SYS_INFO_DISP_INTRVL) == SUCCESS)
		{
			/*	Set the sys info interval*/
			putEnvFile(SECTION_DEVICE, SYS_INFO_DISP_INTRVL, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, PRIV_LAB_CVV_REQD) == SUCCESS)
		{
			/*	Set if CVV is required for private card*/
			putEnvFile(SECTION_DEVICE, PRIV_LAB_CVV_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, PRIV_LAB_EXP_REQD) == SUCCESS)
		{
			/*	Set if exp date is required for private card*/
			putEnvFile(SECTION_DEVICE, PRIV_LAB_EXP_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, GIFT_CVV_REQD) == SUCCESS)
		{
			/*	Set if CVV is required for Gift*/
			putEnvFile(SECTION_DEVICE, GIFT_CVV_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, GIFT_EXP_REQD) == SUCCESS)
		{
			/*	Set if exp date is required for Gift*/
			putEnvFile(SECTION_DEVICE, GIFT_EXP_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, GIFT_PINCODE_REQD) == SUCCESS)
		{
			/*	Set if pin code is required for Gift*/
			putEnvFile(SECTION_DEVICE, GIFT_PINCODE_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SIG_OPT_ON_PINENTRY_REQD) == SUCCESS)
		{
			/*	Set if signature option is required on the pin entry screen*/
			putEnvFile(SECTION_DEVICE, SIG_OPT_ON_PINENTRY_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, HOST_PROTOCOL_FORMAT) == SUCCESS)
		{
			/*	Set the host protocol format whether it is UGP or SSI*/
			putEnvFile(SECTION_DEVICE, HOST_PROTOCOL_FORMAT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, PA_PYMT_TYPE_FORMAT) == SUCCESS)
		{
			/*	Set the payaccount payment type format whether it is UGP or SSI*/
			putEnvFile(SECTION_DEVICE, PA_PYMT_TYPE_FORMAT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, DEBIT_OFFLINE_PIN_CVM) == SUCCESS)
		{
			/*	Set the device name feature enabled param*/
			updateBoolValInCfg(SECTION_DEVICE, DEBIT_OFFLINE_PIN_CVM, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, DEV_NAME_ENABLED) == SUCCESS)
		{
			/*	Set the device name feature enabled param*/
			updateBoolValInCfg(SECTION_DEVICE, DEV_NAME_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, TOR_ENABLED) == SUCCESS)
		{
			/*	Set the TOR enabled param*/
			updateBoolValInCfg(SECTION_DEVICE, TOR_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, TOR_RETRY_COUNT) == SUCCESS)
		{
			/*	Set the TOR retries param*/
			putEnvFile(SECTION_DEVICE, TOR_RETRY_COUNT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, STB_LOGIC_ENABLED) == SUCCESS)
		{
			/*	Set the stb logic enabled param*/
			updateBoolValInCfg(SECTION_SPINTHEBIN, STB_LOGIC_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, STB_SIG_LIMIT) == SUCCESS)
		{
			/*	Set the signature limit param*/
			putEnvFile(SECTION_SPINTHEBIN, STB_SIG_LIMIT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, STB_PIN_LIMIT) == SUCCESS)
		{
			/*	Set the pin limit  param*/
			putEnvFile(SECTION_SPINTHEBIN, STB_PIN_LIMIT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, STB_INTERCHANGE_MGMT_LIMIT) == SUCCESS)
		{
			/*	Set the interchange management limit  param*/
			putEnvFile(SECTION_SPINTHEBIN, STB_INTERCHANGE_MGMT_LIMIT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_TRAN_FLOOR_LIMIT) == SUCCESS)
		{
			/*	Set the Transaction SAF floor limit  param*/
			putEnvFile(SECTION_SAF, SAF_TRAN_FLOOR_LIMIT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, DEVICE_NAME) == SUCCESS)
		{
			/*	Set the device name  param*/
			putEnvFile(SECTION_DEVICE, DEVICE_NAME, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, STATUS_MSG_DISP_INTRVL) == SUCCESS)
		{
			/*	Set the staus message disp interval*/
			putEnvFile(SECTION_DEVICE, STATUS_MSG_DISP_INTRVL, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, ADVANCE_VSP_IN_X_DAYS) == SUCCESS)
		{
			/*	Set the VSP Advance Days*/
			putEnvFile(SECTION_DEVICE, ADVANCE_VSP_IN_X_DAYS, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_PING_INTERVAL) == SUCCESS)
		{
			/*	Set the saf ping interval*/
			putEnvFile(SECTION_SAF, SAF_PING_INTERVAL, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_DAYS_LIMIT) == SUCCESS)
		{
			/*	Set the saf days limit*/
			putEnvFile(SECTION_SAF, SAF_DAYS_LIMIT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_PURGE_DAYS) == SUCCESS)
		{
			/*	Set the saf purge days*/
			putEnvFile(SECTION_SAF, SAF_PURGE_DAYS, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_TRAN_POST_INTERVAL) == SUCCESS)
		{
			/*	Set the post interval*/
			putEnvFile(SECTION_SAF, SAF_TRAN_POST_INTERVAL, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_ALLOW_OFFLINE_TRAN_TOGOWITH_ONLINE) == SUCCESS)
		{
			/*	Set the SAF allow offline tran to with online transaction*/
			putEnvFile(SECTION_SAF, SAF_ALLOW_OFFLINE_TRAN_TOGOWITH_ONLINE, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_THROTLING_ENABLED) == SUCCESS)
		{
			/*	Set the saf throttling */
			updateBoolValInCfg(SECTION_SAF, SAF_THROTLING_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_FORCE_DUP_TRAN) == SUCCESS)
		{
			/*	Set the saf force flag */
			updateBoolValInCfg(SECTION_SAF, SAF_FORCE_DUP_TRAN, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_CHK_CONN_REQD) == SUCCESS)
		{
			/*	Set the saf check connection required */
			updateBoolValInCfg(SECTION_SAF, SAF_CHK_CONN_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_INDICATOR_REQUIRED) == SUCCESS)
		{
			/*	Set the SAF Indicator in SSI request required */
			updateBoolValInCfg(SECTION_SAF, SAF_INDICATOR_REQUIRED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_ALLOW_REFUND_TRAN) == SUCCESS)
		{
			/*	Set the saf allow refund required */
			updateBoolValInCfg(SECTION_SAF, SAF_ALLOW_REFUND_TRAN, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_ALLOW_VOID_TRAN) == SUCCESS)
		{
			/*	Set the saf allow void required */
			updateBoolValInCfg(SECTION_SAF, SAF_ALLOW_VOID_TRAN, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_ALLOW_GIFT_ACTIVATE) == SUCCESS)
		{
			/*	Set the saf allow gift activate required */
			updateBoolValInCfg(SECTION_SAF, SAF_ALLOW_GIFT_ACTIVATE, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SIGN_IMAGE_TYPE) == SUCCESS)
		{
			/*	Set the sign image type*/
			putEnvFile(SECTION_SIGN, SIGN_IMAGE_TYPE, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, IDLE_IMG_LIST_FILE) == SUCCESS)
		{
			/*	Set the idle screen image list file*/
			putEnvFile(SECTION_MEDIA, IDLE_IMG_LIST_FILE, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, VIDEO_LIST_FILE) == SUCCESS)
		{
			/*	Set the video list file*/
			putEnvFile(SECTION_MEDIA, VIDEO_LIST_FILE, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, IDLE_SCREEN_MODE_FILE) == SUCCESS)
		{
			/*	Set the Idle screen Image Mode file*/
			putEnvFile(SECTION_MEDIA, IDLE_SCREEN_MODE_FILE, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, LI_IMG_LIST_FILE) == SUCCESS)
		{
			/*	Set the idle screen image list file*/
			putEnvFile(SECTION_MEDIA, LI_IMG_LIST_FILE, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, PS_GC_MANUAL_ENTRY) == SUCCESS)
		{
			/* Set the Gift Card Manual Entry Type*/
			putEnvFile(SECTION_MRCHNT, PS_GC_MANUAL_ENTRY, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "clientid") == SUCCESS)
		{
			/* Set the client ID*/
			putEnvFile(SECTION_MRCHNT, "clientid", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "supportedwallets") == SUCCESS)
		{
			/* Set the supportedwallets*/
			putEnvFile(SECTION_DEVICE, "supportedwallets", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "ADVANCEVSPKEYINXDAYS") == SUCCESS)
		{
			/* Set the ADVANCEVSPKEYINXDAYS*/
			putEnvFile(SECTION_DEVICE, "ADVANCEVSPKEYINXDAYS", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_TOTAL_FLOOR_LIMIT) == SUCCESS)
		{
			/* Set the Total SAF floor limit*/
			putEnvFile(SECTION_SAF, SAF_TOTAL_FLOOR_LIMIT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_PRIV_LBL_TRAN_FLOOR_LIMIT) == SUCCESS)
		{
			/* Set the SAF floor limit for private payment type */
			putEnvFile(SECTION_SAF, SAF_PRIV_LBL_TRAN_FLOOR_LIMIT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, EMV_ENABLED) == SUCCESS)
		{
			/* Set if Contact EMV is enabled */
			updateBoolValInCfg(SECTION_DEVICE, EMV_ENABLED, cValPtr);
			gbDHIRestart = PAAS_TRUE;
		}
		else if(strcasecmp(cKeyPtr, CTLS_EMV_ENABLED) == SUCCESS)
		{
			/* Set if Contactless EMV is enabled */
			updateBoolValInCfg(SECTION_DEVICE, CTLS_EMV_ENABLED, cValPtr);
			gbDHIRestart = PAAS_TRUE;
		}
		else if(strcasecmp(cKeyPtr, CTLS_ENABLED) == SUCCESS)
		{
			/* Set if Contactless is enabled */
			updateBoolValInCfg(SECTION_DEVICE, CTLS_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, CARD_READ_SCREEN_FIRST) == SUCCESS)
		{
			/* Set if cardreadscreenfirst is enabled */
			updateBoolValInCfg(SECTION_DEVICE, CARD_READ_SCREEN_FIRST, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, EMV_KERNEL_SWITCHING_ALLOWED) == SUCCESS)
		{
			/* Set if emvkernelswitchallowed is enabled */
			updateBoolValInCfg(SECTION_DEVICE, EMV_KERNEL_SWITCHING_ALLOWED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_EMV_ONLINE_PIN_TRANS) == SUCCESS)
		{
			/* Set if safemvonlinepintran is enabled */
			updateBoolValInCfg(SECTION_SAF, SAF_EMV_ONLINE_PIN_TRANS, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SERVICE_CODE_CHECK_IN_FALLBACK) == SUCCESS)
		{
			/* Set if servicecodechkforfallback is enabled */
			updateBoolValInCfg(SECTION_DEVICE, SERVICE_CODE_CHECK_IN_FALLBACK, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, EMPTY_CANDID_AS_FALLBACK) == SUCCESS)
		{
			/* Set if emptycandidasfallback is enabled */
			updateBoolValInCfg(SECTION_DEVICE, EMPTY_CANDID_AS_FALLBACK, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, TAGS_REQ_VOID_CARD_PRESENT) == SUCCESS)
		{
			/* Set if tagsreqforcardpresentvoid is enabled */
			updateBoolValInCfg(SECTION_DEVICE, TAGS_REQ_VOID_CARD_PRESENT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, STORE_CARD_DTLS_POST_AUTH) == SUCCESS)
		{
			/* Set if storecarddtlsforpostauth is enabled */
			updateBoolValInCfg(SECTION_DEVICE, STORE_CARD_DTLS_POST_AUTH, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, STORE_PREV_TRANS_CARD_DTLS_GIFT_CLOSE) == SUCCESS)
		{
			/* Set if storecarddtlsforgiftclose is enabled */
			updateBoolValInCfg(SECTION_DEVICE, STORE_PREV_TRANS_CARD_DTLS_GIFT_CLOSE, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, PARTIAL_EMV_ALLOWED) == SUCCESS)
		{
			/* Set if partialemvallowed is enabled */
			updateBoolValInCfg(SECTION_DEVICE, PARTIAL_EMV_ALLOWED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, CLEAR_EXPIRY_TO_EMV_HOST) == SUCCESS)
		{
			/* Set if clearexpiryin5F24tag is enabled */
			updateBoolValInCfg(SECTION_DEVICE, CLEAR_EXPIRY_TO_EMV_HOST, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, CUSTOM_TAG_REQD) == SUCCESS)
		{
			/* Set if custom tags is enabled */
			updateBoolValInCfg(SECTION_DEVICE, CUSTOM_TAG_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, VERBOSERECPT_ENABLED) == SUCCESS)
		{
			/* Set if verboseRecptEnabled is enabled/disabled */
			updateBoolValInCfg(SECTION_DEVICE, VERBOSERECPT_ENABLED, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, DESCRIPTIVE_ENTRY_MODE) == SUCCESS)
		{
			/* Set if descriptiveentrymode is enabled/disabled */
			updateBoolValInCfg(SECTION_DEVICE, DESCRIPTIVE_ENTRY_MODE, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, CHECK_HOST_CONN_STATUS) == SUCCESS)
		{
			/* Set if CheckHostConnectionStatus is enabled/disabled */
			updateBoolValInCfg(SECTION_DEVICE, CHECK_HOST_CONN_STATUS, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, EMV_SETUP_REQD) == SUCCESS)
		{
			/*	Set the post interval*/
			putEnvFile(SECTION_DEVICE, EMV_SETUP_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, ANAIMATION_UPDATE_INTV) == SUCCESS)
		{
			/*	Set the animation frame delay interval*/
			putEnvFile(SECTION_MEDIA, ANAIMATION_UPDATE_INTV, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, POS_APRV_REQD_FOR_UPDATE) == SUCCESS)
		{
			/*	Set the post interval*/
			putEnvFile(SECTION_DEVICE, POS_APRV_REQD_FOR_UPDATE, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, POS_UPDATE_NOTIFY_TIMEOUT) == SUCCESS)
		{
			/*	Set the post interval*/
			putEnvFile(SECTION_DEVICE, POS_UPDATE_NOTIFY_TIMEOUT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SCA_PACKAGE_NAME) == SUCCESS)
		{
			/*	Set the post interval*/
			putEnvFile(SECTION_PERM, SCA_PACKAGE_NAME, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_ON_RESP_TIMEOUT) == SUCCESS)
		{
			/*	Set the saf on resptimeout value*/
			putEnvFile(SECTION_DEVICE, SAF_ON_RESP_TIMEOUT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SWITCH_TO_PRIMURL_AFTER_X_TRAN) == SUCCESS)
		{
			/*	Set the switch on prim url after x tran value*/
			putEnvFile(SECTION_DEVICE, SWITCH_TO_PRIMURL_AFTER_X_TRAN, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, TOGGLE_URL_AFTER_X_RESP_TIMEOUT) == SUCCESS)
		{
			/*	Set the switch url on x resptimeout value*/
			putEnvFile(SECTION_DEVICE, TOGGLE_URL_AFTER_X_RESP_TIMEOUT, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, UNSOL_MSG_DURING_PYMT_TRAN) == SUCCESS)
		{
			/*	Update the Unsolicited msg during payment tran value*/
			updateBoolValInCfg(SECTION_DEVICE, UNSOL_MSG_DURING_PYMT_TRAN, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, PROCESSOR) == SUCCESS)
		{
			/*	Set the Processor field*/
			putEnvFile(SECTION_DHI, PROCESSOR, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, MERCHANT_ID) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL)
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
			}
			else
			{
				strcpy(szSectionName, SECTION_DHI);
			}

			debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
			APP_TRACE(szDbgMsg);

			/*Setting MID*/
			putEnvFile(szSectionName, MERCHANT_ID, cValPtr);
			gbDHIRestart = PAAS_TRUE;
		}
		else if(strcasecmp(cKeyPtr, TERMINAL_ID) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL)
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
			}
			else
			{
				strcpy(szSectionName, SECTION_DHI);
			}

			debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
			APP_TRACE(szDbgMsg);

			/*Setting TID*/
			putEnvFile(szSectionName, TERMINAL_ID, cValPtr);
			gbDHIRestart = PAAS_TRUE;
		}
		else if(strcasecmp(cKeyPtr, LANE_ID) == SUCCESS)
		{
			/*Setting LANEID*/
			putEnvFile(SECTION_DHI, LANE_ID, cValPtr);
			gbDHIRestart = PAAS_TRUE;
		}
		else if(strcasecmp(cKeyPtr, "BANKID") == SUCCESS)
		{
			/*Setting BANKID*/
			putEnvFile(SECTION_DHI, "BANKID", cValPtr);
			gbDHIRestart = PAAS_TRUE;
		}
		else if(strcasecmp(cKeyPtr, "MIDLENGTH") == SUCCESS)
		{
			/*Setting MIDLENGTH*/
			putEnvFile(SECTION_DHI, "MIDLENGTH", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "TIDLENGTH") == SUCCESS)
		{
			/*Setting TIDLENGTH*/
			putEnvFile(SECTION_DHI, "TIDLENGTH", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "LANEIDLENGTH") == SUCCESS)
		{
			/*Setting LANEIDLENGTH*/
			putEnvFile(SECTION_DHI, "LANEIDLENGTH", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "ALTMERCHIDLENGTH") == SUCCESS)
		{
			/*Setting ALTMERCHIDLENGTH*/
			putEnvFile(SECTION_DHI, "ALTMERCHIDLENGTH", cValPtr);
		}

		/* From here on all config variables related to Rapid Connect Host Interafce*/
		else if(strcasecmp(cKeyPtr, TPP_ID) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the TPP ID for the Rapid connect Host */
				putEnvFile(szSectionName, TPP_ID, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, TPP_ID, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}

		}
#if 0
		else if(strcasecmp(cKeyPtr, RC_TERMINAL_ID) == SUCCESS)
		{
			/* Set the TID for the Rapid connect Host */
			putEnvFile(SECTION_RCHI, TERMINAL_ID, cValPtr);
			gbDHIRestart = PAAS_TRUE;
		}
		else if(strcasecmp(cKeyPtr, RC_MERCHANT_ID) == SUCCESS)
		{
			/* Set the MID for the Rapid connect Host */
			putEnvFile(SECTION_RCHI, MERCHANT_ID, cValPtr);
			gbDHIRestart = PAAS_TRUE;
		}
#endif
		else if(strcasecmp(cKeyPtr, MERCHANT_CAT_CODE) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the TPP ID for the Rapid connect Host */
				putEnvFile(szSectionName, MERCHANT_CAT_CODE, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, MERCHANT_CAT_CODE, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, TRANSACTION_CURRENCY) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Transaction Currency for the Rapid connect Host */
				putEnvFile(szSectionName, TRANSACTION_CURRENCY, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, TRANSACTION_CURRENCY, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, GROUP_ID) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Group ID for the Rapid connect Host */
				putEnvFile(szSectionName, GROUP_ID, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, GROUP_ID, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
#if 0
		else if(strcasecmp(cKeyPtr, RC_LANE_ID) == SUCCESS)
		{
			/* Set the Lane ID for the Rapid connect Host */
			putEnvFile(SECTION_RCHI, LANE_ID, cValPtr);
			gbDHIRestart = PAAS_TRUE;
		}
#endif
		else if(strcasecmp(cKeyPtr, ADDTL_AMT_CURRENCY) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Additional Amount Currency Type for the Rapid connect Host */
				putEnvFile(szSectionName, ADDTL_AMT_CURRENCY, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, ADDTL_AMT_CURRENCY, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, ALTERNATE_MERCH_ID) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Alternate Merchant ID for the Rapid connect Host */
				putEnvFile(szSectionName, ALTERNATE_MERCH_ID, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, ALTERNATE_MERCH_ID, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, DOMAIN) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Domain for the Rapid connect Host */
				putEnvFile(szSectionName, DOMAIN, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, DOMAIN, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, BRAND) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Brand for the Rapid connect Host */
				putEnvFile(szSectionName, BRAND, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, BRAND, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, FD_SERVICE_ID) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Service ID for the Rapid connect Host */
				putEnvFile(szSectionName, FD_SERVICE_ID, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, FD_SERVICE_ID, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, FD_APP_ID) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the App ID for the Rapid connect Host */
				putEnvFile(szSectionName, FD_APP_ID, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, FD_APP_ID, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, REG_HOST_URL) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Registration URL for the Rapid connect Host */
				putEnvFile(szSectionName, REG_HOST_URL, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, REG_HOST_URL, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, REG_HOST_PORT) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Registration URL Port for the Rapid connect Host */
				putEnvFile(szSectionName, REG_HOST_PORT, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, REG_HOST_URL, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, REG_CONN_TO) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Registration URL Connection Timeout for the Rapid connect Host */
				putEnvFile(szSectionName, REG_CONN_TO, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, REG_CONN_TO, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, REG_RESP_TO) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Registration URL Response Timeout for the Rapid connect Host */
				putEnvFile(szSectionName, REG_RESP_TO, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, REG_RESP_TO, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, PRIM_HOST_URL) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Primary URL for the Rapid connect Host */
				putEnvFile(szSectionName, PRIM_HOST_URL, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, PRIM_HOST_PORT) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Primary URL Port for the Rapid connect Host */
				putEnvFile(szSectionName, PRIM_HOST_PORT, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, SCND_HOST_URL) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Secondary URL for the Rapid connect Host */
				putEnvFile(szSectionName, SCND_HOST_URL, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, SCND_HOST_URL, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, SCND_HOST_PORT) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Secondary URL Port for the Rapid connect Host */
				putEnvFile(szSectionName, SCND_HOST_PORT, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, SCND_HOST_PORT, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, PRIM_CONN_TO) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Primary URL Connection Timeout for the Rapid connect Host */
				putEnvFile(szSectionName, PRIM_CONN_TO, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, PRIM_CONN_TO, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, SCND_CONN_TO) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Secondary URL Connection Timeout for the Rapid connect Host */
				putEnvFile(szSectionName, SCND_CONN_TO, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, PRIM_RESP_TO) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Primary URL Response Timeout for the Rapid connect Host */
				putEnvFile(szSectionName, PRIM_RESP_TO, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp(cKeyPtr, SCND_RESP_TO) == SUCCESS)
		{
			memset(szSectionName, 0x00, sizeof(szSectionName));
			if(cHostNamePtr != NULL) //Host name attribute value is required to map this param
			{
				sprintf(szSectionName, "%sHI", cHostNamePtr);

				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				/* Set the Secondary URL Response Timeout for the Rapid connect Host */
				putEnvFile(szSectionName, SCND_RESP_TO, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else if((getHostNameFromConfig(szHostName, sizeof(szHostName)) != 0) && (strlen(szHostName) > 0))
			{
				sprintf(szSectionName, "%sHI", szHostName);
				memset(szHostName, 0x00, sizeof(szHostName));
				debug_sprintf(szDbgMsg, "%s: %s param is setting under %s section", __FUNCTION__, cKeyPtr, szSectionName);
				APP_TRACE(szDbgMsg);

				putEnvFile(szSectionName, SCND_RESP_TO, cValPtr);
				gbDHIRestart = PAAS_TRUE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host Name is not specified to map %s parameter", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);
			}
		}
		/* Till Above all config variables related to Rapid Connect Host Interafce*/
		else if(strcasecmp(cKeyPtr, TIMEZONE) == SUCCESS)
		{
			/*Setting the timezone for the device*/
			putEnvFile(SECTION_REG, TIMEZONE, cValPtr);
			gbDHIRestart = PAAS_TRUE; //This variable would be used by RCHI thats why
		}
		else if(strcasecmp(cKeyPtr, "DBGIP") == SUCCESS)
		{
			/*Setting the ip for DEBUG logs*/
			putEnvFile(SECTION_REG, "DBGIP", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "DBGPRT") == SUCCESS)
		{
			/*Setting the port  logs*/
			putEnvFile(SECTION_REG, "DBGPRT", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "DBGSCA") == SUCCESS)
		{
			/*Setting the PAAS_DEBUG for logs*/
			putEnvFile(SECTION_REG, "PAAS_DEBUG", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "MEMDBGSCA") == SUCCESS)
		{
			/*Setting the PAAS_MEM_DEBUG for Memory debugging logs*/
			putEnvFile(SECTION_REG, "PAAS_MEM_DEBUG", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "DEBUG_TMS") == SUCCESS)
		{
			/*Setting the DEBUG_TMS for Memory debugging logs*/
			putEnvFile(SECTION_REG, "DEBUG_TMS", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "DBGFA") == SUCCESS)
		{
			/*Setting the param for FA logs*/
			putEnvFile(SECTION_REG, "DEBUG", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "DBGOPT") == SUCCESS)
		{
			iVal = atoi(cValPtr);
			switch(iVal)
			{
			case 0:
				/*Set it with empty string DEBUGOPT is given 0*/
				putEnvFile(SECTION_REG, "DEBUGOPT", "");
				break;
			case 1:
				/*Setting the ip for DEBUG logs*/
				putEnvFile(SECTION_REG, "DEBUGOPT", "FILE");
				break;
			case 2:
				/*Setting the ip for DEBUG logs*/
				putEnvFile(SECTION_REG, "DEBUGOPT", "USB");
				break;
			case 3:
				/*Setting the ip for DEBUG logs*/
				putEnvFile(SECTION_REG, "DEBUGOPT", "SD");
				break;
			case 4:
				/*Setting the ip for DEBUG logs*/
				putEnvFile(SECTION_REG, "DEBUGOPT", "USB_DIRECT");
				break;
			default:
				debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strncasecmp(cKeyPtr, "CONSUMER_OPTION", 15) == SUCCESS)
		{
			/* Store footers for the reciept */
			storeConsumerParamsInCfgFile(cKeyPtr + 15, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "KEEPALIVE") == SUCCESS)
		{
			/*Setting the keep alive parameter*/
			putEnvFile(SECTION_HOST, "KEEPALIVE", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "KEEPALIVEINTERVAL") == SUCCESS)
		{
			/*Setting the keepaliveinterval period*/
			putEnvFile(SECTION_HOST, "KEEPALIVEINTERVAL", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, "LINEITEM_DISPLAY") == SUCCESS)
		{
			/*Setting the lineitem display*/
			putEnvFile(SECTION_DEVICE, "LINEITEM_DISPLAY", cValPtr);
		}
		else if(strcasecmp(cKeyPtr, FULL_LINEITEM_DISPLAY) == SUCCESS)
		{
			/*Setting the lineitem display*/
			putEnvFile(SECTION_DEVICE, FULL_LINEITEM_DISPLAY, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, MAX_POS_CONNECTIONS) == SUCCESS)
		{
			/*Setting the Max POS Connections display*/
			putEnvFile(SECTION_DEVICE, MAX_POS_CONNECTIONS, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, LANE_CLOSED_FONT_COL) == SUCCESS)
		{
			/*Setting the Lane Closed Font Color*/
			putEnvFile(SECTION_DEVICE, LANE_CLOSED_FONT_COL, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, LANE_CLOSED_FONT_SIZE) == SUCCESS)
		{
			/*Setting the Lane Closed Font Size*/
			putEnvFile(SECTION_DEVICE, LANE_CLOSED_FONT_SIZE, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_CHK_FLOOR_LIMIT_TO_SAF_REFUND) == SUCCESS)
		{
			/* Set if SAF_CHK_FLOOR_LIMIT_TO_SAF_REFUND is enabled/disabled */
			updateBoolValInCfg(SECTION_SAF, SAF_CHK_FLOOR_LIMIT_TO_SAF_REFUND, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, CAPTURE_SIG_FOR_PRIV_LABEL) == SUCCESS)
		{
			/* Set if CAPTURE_SIG_FOR_PRIV_LABEL is enabled/disabled */
			updateBoolValInCfg(SECTION_DEVICE, CAPTURE_SIG_FOR_PRIV_LABEL, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SAF_ALLOW_PAYACCOUNT_TO_SAF) == SUCCESS)
		{
			/* Set if SAF_ALLOW_PAYACCOUNT_TO_SAF is enabled/disabled */
			updateBoolValInCfg(SECTION_SAF, SAF_ALLOW_PAYACCOUNT_TO_SAF, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, SVS_NON_DENOMINATED_CARD) == SUCCESS)
		{
			/* Set if SAF_ALLOW_PAYACCOUNT_TO_SAF is enabled/disabled */
			updateBoolValInCfg(SECTION_DEVICE, SVS_NON_DENOMINATED_CARD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, EBT_CVV_REQD) == SUCCESS)
		{
			/* Set if EBT CVV Reqd is enabled/disabled */
			updateBoolValInCfg(SECTION_DEVICE, EBT_CVV_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, EBT_EXP_REQD) == SUCCESS)
		{
			/* Set if EBT Expiry is enabled/disabled */
			updateBoolValInCfg(SECTION_DEVICE, EBT_EXP_REQD, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, INCLUDE_TA_FLAG) == SUCCESS)
		{
			/* Set if Include TA Flag is enabled/disabled */
			updateBoolValInCfg(SECTION_DEVICE, INCLUDE_TA_FLAG, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, TERM_IDEN_DISPLAY_INTERVAL) == SUCCESS)
		{
			/*Setting the Terminal Identification Display Interval*/
			putEnvFile(SECTION_DEVICE, TERM_IDEN_DISPLAY_INTERVAL, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, TERM_IDEN_POS_ACK_INTERVAL) == SUCCESS)
		{
			/*Setting the Max POS ACK Wait Time Interval*/
			putEnvFile(SECTION_DEVICE, TERM_IDEN_POS_ACK_INTERVAL, cValPtr);
		}
		else if(strcasecmp(cKeyPtr, BROADCAST_PORT) == SUCCESS)
		{
			/*Setting the Broadcast Port for Terminal Identification*/
			putEnvFile(SECTION_DEVICE, BROADCAST_PORT, cValPtr);
		}
		else
		{
			/* Check for the CDT table related fields */
			rv = checkForCDTParams(cKeyPtr, cValPtr);
			if(rv == NO_REC_PRESENT)
			{
				debug_sprintf(szDbgMsg, "%s: Cant match %s with any known param", __FUNCTION__, cKeyPtr);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
		}

		paramValLstPtr = paramValLstPtr->next;
	}

	/* Commit the changes done to the CDT */
	commitCDTChanges();

	if(gbDHIRestart)
	{
		debug_sprintf(szDbgMsg, "%s: DHI parameters are set, so need to restart DHI", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: storeParamsInLocalCfgIniFile
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int storeParamsInLocalCfgIniFile(GENKEYVAL_PTYPE paramValLstPtr, char *pszGID, dictionary *dict)
{
	int				rv					= SUCCESS;
	char			szTmpKey[100]		= "";
	char			szRID[100]		= "";
	char			szTmpVal[100]		= "";
	char			szKey[100]			= "";
	char *			cKeyPtr				= NULL;
	char *			cValPtr				= NULL;
	char *			value				= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//If length is greater than 10. Then its AID. Need to map this value to ini config variable(SUPPORTED).
	if( strlen(pszGID) > 10)
	{
		memset(szKey, 0x00, sizeof(szKey));
		memset(szRID, 0x00, sizeof(szRID));

		//Get the previous supported AID list from ini file.
		memcpy(szRID, pszGID, 10);
		sprintf(szKey, "%s:%s_%s", szRID, szRID, "SUPPORTED");
		value = iniparser_getstring(dict, szKey, NULL);
		if(value != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Current Supported AIDs[%s] from ini file",__FUNCTION__, value);
			APP_TRACE(szDbgMsg);

			//Check whether supported AID already exist in the ini file before storing.
			if( (NULL== strstr(value, pszGID)) )	//Supported AID doesn't exist in ini file.
			{
				//Append a new supported AID to the existing supported AID list in the ini file.
				memset(szTmpVal, 0x00, sizeof(szTmpVal));
				sprintf(szTmpVal, "%s,%s", value, pszGID);

				debug_sprintf(szDbgMsg, "%s: Storing Supported AIDs[%s] into ini file",__FUNCTION__, szTmpVal);
				APP_TRACE(szDbgMsg);

				//Store the final supported AID config variable in the ini file.
				rv = iniparser_set(dict, szKey, szTmpVal);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: No need store supported AID[%s] into ini file. Already exist",
						__FUNCTION__, pszGID);
				APP_TRACE(szDbgMsg);
			}

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Supported AID is missing for %s",__FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);
		}
	}
	while( (paramValLstPtr != NULL) && (rv == SUCCESS) )
	{
		cKeyPtr = paramValLstPtr->key;
		cValPtr = paramValLstPtr->value;

		memset(szTmpKey, 0x00, sizeof(szTmpKey));
		memset(szKey, 0x00, sizeof(szKey));

		if(strcasecmp(cKeyPtr, "RECNo") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(RECNO, szTmpKey, 13); //13= D13 cmd
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "RID") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(RID, szTmpKey, 15);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "TRMDataPresent") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(TRM_DATA_PRESENT, szTmpKey, 14); //14= D14 cmd
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "MaxTPforBRS") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(MAX_TARGET_RS_PERCENT, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "TPforBRS") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(TARGET_RS_PERCENT, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "DefaultDDOL") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(DEFAULT_DDOL, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "DefaultTDOL") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(DEFAULT_TDOL, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "DefaultTAC") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(TAC_DEFAULT, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "DenialTAC") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(TAC_DENIAL, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "OnlineTAC") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(TAC_ONLINE, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CtlsDefaultTAC") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CTLS_TAC_DEFAULT, szTmpKey, 19);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if (strcasecmp(cKeyPtr, "CtlsDenialTAC") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CTLS_TAC_DENIAL, szTmpKey, 19);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CtlsOnlineTAC") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CTLS_TAC_ONLINE, szTmpKey, 19);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "TermCatCode") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(TERMINAL_CAT_CODE, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "MerchForceOnline") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(MERCH_FORCE_ONLINE_FLG, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "BlackListCardSupport") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(BLACK_LIST_CARD_SUPPORT_FLG, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "NextRecord") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(NEXT_RECORD, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "AppAutoSelect") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(AUTO_SELECT_APPL, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "EMVCounter") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(EMV_COUNTER, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "TermCAPKIndex") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CAPK_INDEX, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "PINBypass") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(PIN_BYPASS_FLG, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "PINTimeOut") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(EMV_PIN_TIMEOUT, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "PINformat") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(PIN_FORMAT, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "PINScriptNo") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(PIN_SCRIPT_NUM, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "PINMacroNo") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(PIN_MACRO_NUMBER, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "PINDerivKey") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(DEVRI_KEY_FLG, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "PINDerivMacro") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(PIN_MACRO_NUMBER, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CardStatusDisplay") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CARD_STAT_DISPLAY_FLG, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "IssAcq") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(ISS_ACQ_FLG, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "NoDispSupport") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(NO_DISPLAY_SUPPORT_FLG, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "ModCandList") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(MODIFY_CAND_LIST_FLG, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "PartNameMatch") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(PARTIAL_NAME_FLG, szTmpKey, 13);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "TermAVN") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(TERM_AVN, szTmpKey, 13);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "2ndTermAVN") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(SCND_TERM_AVN, szTmpKey, 13);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "RecomAIDName") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(RID, szTmpKey, 13);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CardName") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(RECMD_AID_NAME, szTmpKey, 13); //13= D13 cmd
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "InNumofTransaction") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(LNNUMOFTRANSACTION, szTmpKey, 15);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "SchemeLabel") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(SCHM_LABEL, szTmpKey, 15);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CSNList") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CSN_LIST, szTmpKey, 15);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "InEMVTablRec") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(EMV_TABLE_RECORD, szTmpKey, 15);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CSNListFile") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CSN_LIST_FILE, szTmpKey, 15);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "TransCurrCode") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CURRENCY_CODE, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "TransCurrExp") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(TERM_CUR_EXP, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "AcquireID") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(ACQUIRER_ID, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "MerchCatCode") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(MERCH_CAT_CODE, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "MerchID") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(MERCH_ID, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "TermCounCode") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(COUNTRY_CODE, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "TermID") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(TERM_ID, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "TermCap") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(TERM_CAPACITY, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "AddTermCap") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(ADD_CAPACITY, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "TermType") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(TERM_TYPE, szTmpKey, 14);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "VisaTTQ") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(VISATTQ, szTmpKey, 19);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CTLSMaxAIDLen") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(MAX_AID_LEN, szTmpKey, 17);
			if(rv == SUCCESS)
			{
				memset(szRID, 0x00, sizeof(szRID));

				//Get the previous supported AID list from ini file.
				memcpy(szRID, pszGID, 10);

				sprintf(szKey, "%s:%s_%s", szRID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "EMVTRecN0") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(EMV_RECORD, szTmpKey, 17);
			if(rv == SUCCESS)
			{
				memset(szRID, 0x00, sizeof(szRID));

				//Get the previous supported AID list from ini file.
				memcpy(szRID, pszGID, 10);

				sprintf(szKey, "%s:%s_%s", szRID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CTLSAppFlow") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(APP_FLOW, szTmpKey, 17);
			if(rv == SUCCESS)
			{
				memset(szRID, 0x00, sizeof(szRID));

				//Get the previous supported AID list from ini file.
				memcpy(szRID, pszGID, 10);

				sprintf(szKey, "%s:%s_%s", szRID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CTLSPartNameMatch") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(PARTIAL_NAME, szTmpKey, 17);
			if(rv == SUCCESS)
			{
				memset(szRID, 0x00, sizeof(szRID));

				//Get the previous supported AID list from ini file.
				memcpy(szRID, pszGID, 10);

				sprintf(szKey, "%s:%s_%s", szRID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CTLSAccSelec") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(ACCOUNT_SEL_FLG, szTmpKey, 17);
			if(rv == SUCCESS)
			{
				memset(szRID, 0x00, sizeof(szRID));

				//Get the previous supported AID list from ini file.
				memcpy(szRID, pszGID, 10);

				sprintf(szKey, "%s:%s_%s", szRID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CTLSTransScheme") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(TRANS_SCHEME, szTmpKey, 17);
			if(rv == SUCCESS)
			{
				memset(szRID, 0x00, sizeof(szRID));

				//Get the previous supported AID list from ini file.
				memcpy(szRID, pszGID, 10);

				sprintf(szKey, "%s:%s_%s", szRID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if (strcasecmp(cKeyPtr, "CTLSEnable") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CTLS_ENABLE_FLG, szTmpKey, 17);
			if(rv == SUCCESS)
			{
				memset(szRID, 0x00, sizeof(szRID));

				//Get the previous supported AID list from ini file.
				memcpy(szRID, pszGID, 10);

				sprintf(szKey, "%s:%s_%s", szRID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "BelowFTC") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(BELLOW_FLR_LIMIT_TERM_CAPABILITIES, szTmpKey, 18);
			if(rv == SUCCESS)
			{
				//Prepare key(secName:key)
				sprintf(szKey, "%s:%s", "D18", szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "AboveFTC") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(ABOVE_FLR_LIMIT_TERM_CAPABILITIES, szTmpKey, 18);
			if(rv == SUCCESS)
			{
				//Prepare key(secName:key)
				sprintf(szKey, "%s:%s", "D18", szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CTLSModFloorlimit") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CTLS_FLR_LIMIT, szTmpKey, 19);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CTLSCVMLimit") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CTLS_CVM_LIMIT, szTmpKey, 19);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}

			//Get Application param name.
			/*Note: Floor limit for D18 Same value as in the D19 - CTLSCVMLimit.
			  	  	For more info Refer D18 command in XPI doc.*/
			memset(szTmpKey, 0x00, sizeof(szTmpKey));
			memset(szKey, 0x00, sizeof(szKey));
			rv = getEmvParamKeyForIndex(CTLSCVM_FLR_LIMIT, szTmpKey, 18);
			if(rv == SUCCESS)
			{
				//Prepare key(secName:key)
				sprintf(szKey, "%s:%s", "D18", szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}

		}
		else if(strcasecmp(cKeyPtr, "CTLSTRansLimit") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CTLS_TRN_LIMIT, szTmpKey, 19);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CTLSTermCap") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(TERMINAL_CAPABILITIES, szTmpKey, 19);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CTLSAddTermCap") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(ADDITIONAL_CAPABILITIES, szTmpKey, 19);
			if(rv == SUCCESS)
			{
				sprintf(szKey, "%s:%s_%s", pszGID, pszGID, szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "VisaDebit") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(VISA_DEBIT, szTmpKey, 25);
			if(rv == SUCCESS)
			{
				//Prepare key(secName:key)
				sprintf(szKey, "%s:%s", "D25", szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "CtlsMode") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CONTACTLESS_MODE, szTmpKey, 25);
			if(rv == SUCCESS)
			{
				//Prepare key(secName:key)
				sprintf(szKey, "%s:%s", "D25", szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "PDOLsupport") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(PDOL_SUPPORT, szTmpKey, 25);
			if(rv == SUCCESS)
			{
				//Prepare key(secName:key)
				sprintf(szKey, "%s:%s", "D25", szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "FlashMerchType") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CTLS_MERCHANT_TYPE, szTmpKey, 20);
			if(rv == SUCCESS)
			{
				//Prepare key(secName:key)
				sprintf(szKey, "%s:%s", "D20", szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "FlashTransInfo") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CTLS_TERM_TRAN_INFO, szTmpKey, 20);
			if(rv == SUCCESS)
			{
				//Prepare key(secName:key)
				sprintf(szKey, "%s:%s", "D20", szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "FlashTermTransType") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CTLS_TERM_TRAN_TYPE, szTmpKey, 20);
			if(rv == SUCCESS)
			{
				//Prepare key(secName:key)
				sprintf(szKey, "%s:%s", "D20", szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "FlashReqReceiptLimit") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CTLS_REQ_RECIEPT_LIMIT, szTmpKey, 20);
			if(rv == SUCCESS)
			{
				//Prepare key(secName:key)
				sprintf(szKey, "%s:%s", "D20", szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "FlashOptStat") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CTLS_OPTION_STATUS, szTmpKey, 20);
			if(rv == SUCCESS)
			{
				//Prepare key(secName:key)
				sprintf(szKey, "%s:%s", "D20", szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}
		else if(strcasecmp(cKeyPtr, "FlashReadFloorlimit") == SUCCESS)
		{
			//Get Application param name.
			rv = getEmvParamKeyForIndex(CTLS_READER_FLR_LIMIT, szTmpKey, 20);
			if(rv == SUCCESS)
			{
				//Prepare key(secName:key)
				sprintf(szKey, "%s:%s", "D20", szTmpKey);

				rv = iniparser_set(dict, szKey, cValPtr);
			}
		}

		debug_sprintf(szDbgMsg, "%s: For Key [%s] iniparser_set returned [%d]", __FUNCTION__,
				cKeyPtr, rv);
		APP_TRACE(szDbgMsg);

		paramValLstPtr = paramValLstPtr->next;

	}


	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: checkForCDTParams
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int checkForCDTParams(char * szKey, char * szVal)
{
	int		rv				= SUCCESS;
	int		locIdx			= 0;
	char	szLocKey[10]	= "";
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(strncasecmp(szKey, "CDT", 3) == SUCCESS)
		{
			/* VDR: Not sure about this field. It is currently not implemented
			 * in the CDT table for Point Mx application */

			/* Param No 1 */
			memcpy(szLocKey, szKey, 3);
			locIdx = atoi(szKey + 3);
		}
		else if(strncasecmp(szKey, "BAUTH", 5) == SUCCESS)
		{
			/* VDR: Not sure about this field. It is currently not implemented
			 * in the CDT table for Point Mx application */

			/* Param No 2 */
			memcpy(szLocKey, szKey, 5);
			locIdx = atoi(szKey + 5);
		}
		else if(strncasecmp(szKey, "BALIMT", 6) == SUCCESS)
		{
			/* Param No 3 */
			memcpy(szLocKey, szKey, 6);
			locIdx = atoi(szKey + 6);
		}
		else if(strncasecmp(szKey, "SLIMIT", 6) == SUCCESS)
		{
			/* Param No 4 */
			memcpy(szLocKey, szKey, 6);
			locIdx = atoi(szKey + 6);
		}
		else if(strncasecmp(szKey, "CDTYP", 5) == SUCCESS)
		{
			/* Param No 5 */
			memcpy(szLocKey, szKey, 5);
			locIdx = atoi(szKey + 5);
		}
		else if(strncasecmp(szKey, "PANLO", 5) == SUCCESS)
		{
			/* Param No 6 */
			memcpy(szLocKey, szKey, 5);
			locIdx = atoi(szKey + 5);
		}
		else if(strncasecmp(szKey, "PANHI", 5) == SUCCESS)
		{
			/* Param No 7 */
			memcpy(szLocKey, szKey, 5);
			locIdx = atoi(szKey + 5);
		}
		else if(strncasecmp(szKey, "MNPNDG", 6) == SUCCESS)
		{
			/* Param No 8 */
			memcpy(szLocKey, szKey, 6);
			locIdx = atoi(szKey + 6);
		}
		else if(strncasecmp(szKey, "MXPNDG", 6) == SUCCESS)
		{
			/* Param No 9 */
			memcpy(szLocKey, szKey, 6);
			locIdx = atoi(szKey + 6);
		}
		else if(strncasecmp(szKey, "TIPDIS", 6) == SUCCESS)
		{
			/* Param No 10 */
			memcpy(szLocKey, szKey, 6);
			locIdx = atoi(szKey + 6);
		}
		else if(strncasecmp(szKey, "CVV_II", 6) == SUCCESS)
		{
			/* Param No 11 */
			memcpy(szLocKey, szKey, 3);
			locIdx = atoi(szKey + 3);
		}
		else if(strncasecmp(szKey, "CDABB", 5) == SUCCESS)
		{
			/* Param No 12 */
			memcpy(szLocKey, szKey, 5);
			locIdx = atoi(szKey + 5);
		}
		else if(strncasecmp(szKey, "CDLBL", 5) == SUCCESS)
		{
			/* Param No 13 */
			memcpy(szLocKey, szKey, 5);
			locIdx = atoi(szKey + 5);
		}
		else if(strncasecmp(szKey, "IPCLBL", 6) == SUCCESS)
		{
			/* Param No 14 */
			memcpy(szLocKey, szKey, 6);
			locIdx = atoi(szKey + 6);
		}
		else if(strncasecmp(szKey, "TRKREQ", 6) == SUCCESS)
		{
			/* Param No 15 */
			memcpy(szLocKey, szKey, 6);
			locIdx = atoi(szKey + 6);
		}
		else if(strncasecmp(szKey, "GFTMIN", 6) == SUCCESS)
		{
			/* Param No 16 */
			memcpy(szLocKey, szKey, 6);
			locIdx = atoi(szKey + 6);
		}
		else if(strncasecmp(szKey, "GFTMAX", 6) == SUCCESS)
		{
			/* Param No 17 */
			memcpy(szLocKey, szKey, 6);
			locIdx = atoi(szKey + 6);
		}
		else if(strncasecmp(szKey, "RNGOFF", 6) == SUCCESS)
		{
			/* Param No 18 */
			memcpy(szLocKey, szKey, 6);
			locIdx = atoi(szKey + 6);
		}
		else if(strncasecmp(szKey, "CHKLHN", 6) == SUCCESS)
		{
			/* Param No 19 */
			memcpy(szLocKey, szKey, 6);
			locIdx = atoi(szKey + 6);
		}
		else if(strncasecmp(szKey, "EXPDT", 5) == SUCCESS)
		{
			/* Param No 20 */
			memcpy(szLocKey, szKey, 5);
			locIdx = atoi(szKey + 5);
		}
		else if(strncasecmp(szKey, "MANL", 4) == SUCCESS)
		{
			/* Param No 21 */
			memcpy(szLocKey, szKey, 4);
			locIdx = atoi(szKey + 4);
		}
		else if(strncasecmp(szKey, "SIGNLIN", 7) == SUCCESS)
		{
			/* Param No 22 */
			memcpy(szLocKey, szKey, 7);
			locIdx = atoi(szKey + 7);
		}
		else if(strncasecmp(szKey, "PPREQ", 5) == SUCCESS)
		{
			/* Param No 23 */
			memcpy(szLocKey, szKey, 5);
			locIdx = atoi(szKey + 5);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Key doesnt match with any CDT keys",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = NO_REC_PRESENT;

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Key = [%s], index = [%d]", __FUNCTION__,
				szLocKey, locIdx);
		APP_TRACE(szDbgMsg);

		/* Proceed with the updation of the CDT table */
		rv = modifyCDT(locIdx, szLocKey, szVal);
		if(rv == CDT_RECORD_NOT_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: Can't find CDT record for [%s] at [%d]"
					, __FUNCTION__, szLocKey, locIdx);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}
		else if(rv == FAILURE)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to set CDT for %s with %s at %d"
					, __FUNCTION__, szLocKey, szVal, locIdx);
			APP_TRACE(szDbgMsg);

			/* VDR: TODO: set the syslog for this error */
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: mapParamsFromXMLFile
 *
 * Description	: Place holder for the function which would be responsible for
 * 					checking the common XML config file and updating our
 * 					config.usr1 file accordingly.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int mapParamsFromXMLFile()
{
	int				rv					= SUCCESS;
	int				iLen				= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szFilePath[100]		= "";
	char 			szErrMsg[50]		= "";
	char			szAppLogData[256]	= "";
	GENKEYVAL_PTYPE	paramValLstPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[512]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Get the path of the xml configuration file (supposed to be
		 * downloaded from VHQ) */
		iLen = sizeof(szFilePath);
		rv = getEnvFile(SECTION_REG, XML_CFGFILE_PATH, szFilePath, iLen);
		if(rv <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: Couldn't find xml file path in config"
					, __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Could Not Find XML File Path in Configuration File");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		/* Got the path for the xml file, validate the path and the file exists
		 * there */
		if(SUCCESS != doesFileExist(szFilePath))
		{
			debug_sprintf(szDbgMsg, "%s: %s File not present, no need of mapping",
					__FUNCTION__, szFilePath);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "XML File for Configuration Not Present, So No Need to Map");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

			rv = SUCCESS;
			break;
		}

		/* Get the param values list from the xml file */
		rv = parseXMLFileForConfigData(szFilePath, &paramValLstPtr);

		if(rv == ERR_INV_XML_GID)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid GID value Noticed in xml file for params", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid GID value Noticed in XML file for Parameters Mapping");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}
			/* Daivik:27/1/2016 Coverity 67242 - Free the allocated linked list */
			freeParamValList(paramValLstPtr);
			paramValLstPtr = NULL;
			rv = SUCCESS;
			break;
		}
		else if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to read the xml file for params"
					, __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Read XML File for Parameters");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}
			/* Daivik:27/1/2016 Coverity 67242 - Free the allocated linked list */
			freeParamValList(paramValLstPtr);
			paramValLstPtr = NULL;
			break;
		}

		/* Map the param values to the ones we use in config.usr1 */
		rv = storeParamsInLocalCfgFile(paramValLstPtr);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to store the params in config",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Store the XML File Parameters in Configuration File");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}
			/* Daivik:27/1/2016 Coverity 67242 - Free the allocated linked list */
			freeParamValList(paramValLstPtr);
			paramValLstPtr = NULL;
			break;
		}
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "XML File Parameters Stored Successfully in the Configuration File");
			addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
		}

		/* Free any allocated memory of param value list*/
		freeParamValList(paramValLstPtr);
		paramValLstPtr = NULL;

/*		if(SUCCESS != doesFileExist(CFG_INI_FILE_PATH))
		{
			debug_sprintf(szDbgMsg, "%s: %s File not present, no need of mapping",
					__FUNCTION__, CFG_INI_FILE_PATH);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "INI File Not Present, No Need to Map INI File");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

			rv = SUCCESS;
		}
		else
		{
			 Get the EMV param values list from the xml file
			rv = parseXMLFileForConfigIniData(szFilePath, &paramValLstPtr);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Unable to read the ini file for params"
						, __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Read INI File for Configuring INI Data");
					addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
				}

				break;
			}
		}*/

		if( rv == SUCCESS ) //Mapped all config xml parameters successfully, Delete the file
		{
			/* Delete the file from the directory */
			if( SUCCESS != deleteFile(szFilePath) )
			{
				debug_sprintf(szDbgMsg, "%s: Failed to delete [%s]", __FUNCTION__,
						szFilePath);
				APP_TRACE(szDbgMsg);
			}

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Mapping Parameters From XML File Done Successfully");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}
		}

		if(gbDHIRestart) //DHI config parameters changed, Restart the DHI application
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "DHI Parameters are set, restarting DHI application");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

			rv = restartDHI();

			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while restarting the DHI/VHI application!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error While Restarting DHI Application");
					addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
				}
			}

			gbDHIRestart = PAAS_FALSE;
		}

		if(gbRedoAppLogInit) //App log config parameter changed, re-initialize the app log module.
		{
			debug_sprintf(szDbgMsg, "%s: Re-initializing the application transaction logging", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//If the app log module is already initialized, We need to first close the app log module in order to re-initialization it.
			closeAppLog();

			rv = initAppLog();
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to initiate application Transaction Logging",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				strcpy(szErrMsg, "FAILED to Init Applocation Trans Logging");
				syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

				break;
			}
			debug_sprintf(szDbgMsg, "%s: Successfully re-initialized the application transaction logging", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			gbRedoAppLogInit = PAAS_FALSE;

		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: freeParamValList
 *
 * Description	: Frees the param value list.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */

static void freeParamValList(GENKEYVAL_PTYPE paramValList)
{
	if(paramValList != NULL)
	{
		GENKEYVAL_PTYPE	tmpNodePtr	= NULL;

		while(paramValList != NULL)
		{
			tmpNodePtr = paramValList;
			paramValList = paramValList->next;

			if(tmpNodePtr->key)
			{
				free(tmpNodePtr->key);
			}

			if(tmpNodePtr->value)
			{
				free(tmpNodePtr->value);
			}

			free(tmpNodePtr);
		}
	}
}

/*
 * ============================================================================
 * Function Name: restartDHI
 *
 * Description	: Frees the param value list.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int restartDHI()
{
	int 		iRetVal				= SUCCESS;
	char 		szErrMsg[256]		= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* ----Restart VHI Application if its running---- */
#if 0
	iLen = sizeof(szTemp);
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_PERM, VHI_APP, szTemp, iLen);
	if(rv > 0)
	{
		if(strcasecmp(szTemp, "vhiD") == SUCCESS)
		{
			iRetVal = restartApp("VHIAppD.exe");
		}
		else
		{
			iRetVal = restartApp("VHIApp.exe");
		}
	}
	else
	{
		iRetVal = restartApp("VHIAppD.exe");
	}
#endif

	iRetVal = restartApp("VHI");
	if(iRetVal == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: VHI Application Restarting!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		memset(szErrMsg, 0x00, sizeof(szErrMsg));
		sprintf(szErrMsg, "%s: VHI Application Restarting!!", __FUNCTION__);
		syslog(LOG_INFO|LOG_USER, szErrMsg);
	}

#if 0
	iLen = sizeof(szTemp);
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_PERM, DHI_APP, szTemp, iLen);
	if(rv > 0)
	{
		if(strcasecmp(szTemp, "dhiD") == SUCCESS)
		{
			/* ----Restart DHI Application if its running---- */
			iRetVal = restartApp("DHIAppD.exe");
		}
		else
		{
			iRetVal = restartApp("DHIApp.exe");
		}
	}
	else
	{
		iRetVal = restartApp("DHIAppD.exe");
	}
#endif

	iRetVal = restartApp("DHI");
	if(iRetVal == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: DHI Application Restarting!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		memset(szErrMsg, 0x00, sizeof(szErrMsg));
		sprintf(szErrMsg, "%s: DHI Application Restarting!!", __FUNCTION__);
	

		syslog(LOG_INFO|LOG_USER, szErrMsg);
	}

	iRetVal = SUCCESS;
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: restartApp
 *
 * Description	: Checks whether given application is running or not. If Yes, it will kill and restarts the application.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int restartApp(char *pszAppName)
{
	int 		iRetVal				= SUCCESS;
	char 		szFilePath[50]		="";
	char		szCommand[70]  		= "";

#ifdef DEBUG
	char			szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		sprintf(szCommand, "killall %s*", pszAppName);
		if(local_svcSystem(szCommand) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Could not execute the command %s",
					__FUNCTION__, szCommand);
			APP_TRACE(szDbgMsg);

			iRetVal = FAILURE;
			break;
		}

		memset(szCommand, 0x00, sizeof(szCommand));
		if(! strncasecmp(pszAppName, "DHI", 3))
		{
			strcpy(szCommand, "startdhi.sh");
		}
		else if(! strncasecmp(pszAppName, "VHI", 3))
		{
			strcpy(szCommand, "startVHI.sh");
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid App name passed [%s]",
					__FUNCTION__, pszAppName);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		memset(szFilePath, 0x00, sizeof(szFilePath));
		sprintf(szFilePath, "/home/usr1/%s", szCommand);

		iRetVal = Secins_start_app(szFilePath);


		break;
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getHostNameFromConfig
 *
 * Description	: This API would get the Name of current Host available, updated by DHI in config.usr1.
 *
 * Input Params	: cHostNamePtr
 *
 * Output Params: None
 * ============================================================================
 */
static int getHostNameFromConfig(char *cHostNamePtr, int iLen)
{
	int			rv				= 0;

	rv = getEnvFile(SECTION_DHI, PROCESSOR, cHostNamePtr, iLen);
	if(rv > 0)
	{
		if((cHostNamePtr == NULL) || (strlen(cHostNamePtr) == 0))
		{
			cHostNamePtr = NULL;
		}
	}
	else
	{
		cHostNamePtr = NULL;
	}
	return rv;
}
/*
 * ============================================================================
 * End of file ldCfgParams.c
 * ============================================================================
 */
