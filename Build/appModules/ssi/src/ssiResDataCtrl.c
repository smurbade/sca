#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <SemtekHTdes.h>

#include "common/metaData.h"
#include "common/tranDef.h"
#include "common/VCL.h"
#include "common/utils.h"
#include "sci/sciConfigDef.h"
#include "ssi/ssiDef.h"
#include "ssi/ssiHostCfg.h"
#include "ssi/ssiCfgDef.h"
#include "ssi/ssiMain.h"
#include "bLogic/bLogicCfgDef.h"
#include "db/dataSystem.h"

#include "ssi/ssiRespLists.inc"

#include "uiAgent/emv.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

typedef METADATA_STYPE	META_STYPE;
typedef METADATA_PTYPE	META_PTYPE;

/* Extern functions declarations */
extern int storeDeviceKey(char *);
extern int storeClientId(char *);
extern int setAdminPacketRequestStatus(PAAS_BOOL);
extern int storeProcessorId(char *, char *);
extern int setDevAdminRqd(PAAS_BOOL);
extern PAAS_BOOL isEmvEnabledInDevice();
extern PAAS_BOOL checkBinInclusion(char *);
//AJAYS2
extern int parseAndStoreHostEMVResponse(EMV_RESP_DTLS_PTYPE, char *);

/* Static functions declarations */
static int checkDevResyncKey(KEYVAL_PTYPE);

static int getRespKeysLstForAdmin(META_PTYPE, int);
static int getRespKeysLstForPymt(META_PTYPE, int);
static int getRespKeysLstForBatch(META_PTYPE, int);
static int getRespKeysLstForRpt(META_PTYPE, int);
static int getRespKeysLstForDev(META_PTYPE, int);

static int storeAdminParams(KEYVAL_PTYPE);
static int storeAdminResp(META_PTYPE, TRAN_PTYPE);
static int storePymtResp(META_PTYPE, TRAN_PTYPE);
static int storeBatchResp(META_PTYPE, TRAN_PTYPE);
static int storeDevResp(META_PTYPE, TRAN_PTYPE);
static int storeRptResp(META_PTYPE, TRAN_PTYPE, char *);
static int storeGenRespDtls(KEYVAL_PTYPE, RESPDTLS_PTYPE);
static int storeVSPRespDtls(KEYVAL_PTYPE, VSPDTLS_PTYPE *);
static int storeAmtDtls(KEYVAL_PTYPE, AMTDTLS_PTYPE *, int);
static int storeCardDtls(KEYVAL_PTYPE, CARDDTLS_PTYPE *, int);
static int storeEmvKeyLoadDtls(KEYVAL_PTYPE, EMVKEYLOAD_INFO_PTYPE *);
static int storeCAPKDtls(KEYVAL_PTYPE, EMVKEYLOAD_INFO_PTYPE);
static int storeMVTDtls(KEYVAL_PTYPE, EMVKEYLOAD_INFO_PTYPE);
static int storeEmvRespDtls(KEYVAL_PTYPE, EMV_RESP_DTLS_PTYPE *);
static int storeCurTranDtls(KEYVAL_PTYPE, CTRANDTLS_PTYPE *);
static int storeDupFieldDtls(KEYVAL_PTYPE , DUPFIELDDTLS_PTYPE * );
static int storeSuppFxnInfo(VAL_NODE_PTYPE );
static int storeSuppCardInfo(VAL_NODE_PTYPE );
static int storeDisclaimers(VAL_NODE_PTYPE );
static int storeFooters(VAL_NODE_PTYPE );
static int storeHeaders(VAL_NODE_PTYPE);
static int storeDHIVersionParam(KEYVAL_PTYPE);
static int storePassThrgResFields(KEYVAL_PTYPE, PYMTTRAN_PTYPE *);
static int storeDCCResFields(KEYVAL_PTYPE listPtr, DCCDTLS_PTYPE *);

//ArjunU1: EMV Testing
static int addEmvMVTResp(EMVKEYLOAD_INFO_PTYPE, VAL_LST_PTYPE);
static int addEmvCAPKResp(EMVKEYLOAD_INFO_PTYPE, VAL_LST_PTYPE);
static int savePublicKeys(EMVKEYLOAD_NODE_PTYPE,VAL_NODE_PTYPE);
static int saveFallBckIndicator(EMVKEYLOAD_NODE_PTYPE,VAL_NODE_PTYPE); 
static int saveOffLineFlrLmt(EMVKEYLOAD_NODE_PTYPE,VAL_NODE_PTYPE);

/*
 * ============================================================================
 * Function Name: procParsedResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int procParsedResp(TRAN_PTYPE pstTran, META_PTYPE pstMeta, char *szMsg)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((pstMeta == NULL) || (pstTran == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		switch(pstTran->iFxn)
		{
		case SSI_ADMIN:
			rv = storeAdminResp(pstMeta, pstTran);
			break;

		case SSI_PYMT:
			rv = storePymtResp(pstMeta, pstTran);
			break;

		case SSI_BATCH:
			rv = storeBatchResp(pstMeta, pstTran);
			break;

		case SSI_RPT:
			rv = storeRptResp(pstMeta, pstTran, szMsg);
			break;

		case SSI_DEV:
			rv = storeDevResp(pstMeta, pstTran);
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaDataForSSIResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getMetaDataForSSIResp(META_PTYPE pstMeta, int iFxn, int iCmd)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstMeta == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		switch(iFxn)
		{
		case SSI_ADMIN:
			rv = getRespKeysLstForAdmin(pstMeta, iCmd);
			break;

		case SSI_PYMT:
			rv = getRespKeysLstForPymt(pstMeta, iCmd);
			break;

		case SSI_BATCH:
			rv = getRespKeysLstForBatch(pstMeta, iCmd);
			break;

		case SSI_RPT:
			rv = getRespKeysLstForRpt(pstMeta, iCmd);
			break;

		case SSI_DEV:
			rv = getRespKeysLstForDev(pstMeta, iCmd);
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

#ifdef DEBUG
		if(rv == SUCCESS)
		{
#ifdef DEVDEBUG
//			int		iCnt	= 0;
#endif
			debug_sprintf(szDbgMsg, "%s: Mandatory Fields = [%d]", __FUNCTION__,
													pstMeta->iMandCnt);
			APP_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: Total Fields = [%d]", __FUNCTION__,
													pstMeta->iTotCnt);
			APP_TRACE(szDbgMsg);
/*
#ifdef DEVDEBUG
			for(iCnt = 0; iCnt < pstMeta->iTotCnt; iCnt++)
			{
				debug_sprintf(szDbgMsg, "%s: KEY = [%s]", __FUNCTION__,
											pstMeta->keyValList[iCnt].key);
				APP_TRACE(szDbgMsg);
			}
#endif
*/
		}
#endif

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaDataForBAPIResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getMetaDataForBAPIResp(META_PTYPE pstMeta)
{
	int				rv		 = SUCCESS;
	int 			iCnt	 = 0;
	int				iTotCnt	 = 0;
	int 			iMandCnt = 0;
	KEYVAL_PTYPE	listPtr	 = NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	 = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstMeta == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		iTotCnt = sizeof(genRespLst) / KEYVAL_SIZE;

		/* Allocate the memory */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize and populate the memory assigned for the key list */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);

		iCnt = sizeof(genRespLst) / KEYVAL_SIZE;
		memcpy(listPtr, genRespLst, iCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		memset(pstMeta, 0x00, METADATA_SIZE);
		pstMeta->iMandCnt	 = iMandCnt;
		pstMeta->iTotCnt 	 = iTotCnt;
		pstMeta->keyValList	 = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeAdminResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeAdminResp(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		listPtr = pstMeta->keyValList;

		/* Store the general response data */
		iCnt = storeGenRespDtls(listPtr, &(pstTran->stRespDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to store gen resp",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		listPtr += iCnt;

		switch(pstTran->iCmd)
		{
		case SSI_SETUP:
			iCnt = storeAdminParams(listPtr);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to store device admin params",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			break;

		case SSI_VERSION:

			iCnt = storeDHIVersionParam(listPtr);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to store DHI Version param",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			break;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storePymtResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storePymtResp(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int						rv					= SUCCESS;
	int						iCnt				= 0;
	char 					szBIN[6+1]			= "";
	char *					pszProcessorId		= NULL;
	ENC_TYPE				iEncType			= getEncryptionType();
	KEYVAL_PTYPE			listPtr				= NULL;
	PYMTTRAN_PTYPE			pstPymtTran			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		listPtr = pstMeta->keyValList;

		pstPymtTran = (PYMTTRAN_PTYPE)pstTran->data;
		if(pstPymtTran == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No payment tran present",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			
			rv = FAILURE;
			break;
		}

		/* Check and process for the resync key if present in the response
		 * data, the host can send it any time in the response to any
		 * transaction. While building the key list for parsing the SSI
		 * response, we need to keep the first key for parsing the RESYNC_KEY
		 * field value, and this first field only would be checked if populated
		 * with any value which needs to be stored for the device */
		rv = checkDevResyncKey(listPtr);
		if(rv != SUCCESS )
		{
			debug_sprintf(szDbgMsg, "%s: Resync check FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		listPtr += 1; //1 for the resync key

		/* Store the general response data */
		//For testing purpose..remove them		
		iCnt = storeGenRespDtls(listPtr, &(pstTran->stRespDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to store gen resp",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		listPtr += iCnt;		

		if(pstTran->iCmd == SSI_SIGN)
		{
			debug_sprintf(szDbgMsg,"%s: SSI SIGN commands contain only till general resp details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* No more values to store..so breaking */
			break;
		}

		/* Store the VSP response */
		iCnt = storeVSPRespDtls(listPtr, &(pstPymtTran->pstVSPDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to store VSP resp",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		listPtr += iCnt;

		/* Store the card details */
		iCnt = storeCardDtls(listPtr, &(pstPymtTran->pstCardDtls), pstPymtTran->iPymtType);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to store Carddtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		listPtr += iCnt;

		/* Store the amount details */
		iCnt = storeAmtDtls(listPtr, &(pstPymtTran->pstAmtDtls), pstTran->iCmd);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to store AMT dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		listPtr += iCnt;
		
		/* Store the current tran details */
		iCnt = storeCurTranDtls(listPtr, &(pstPymtTran->pstCTranDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to store trandtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		listPtr += iCnt;
		/*
		 * Praveen_P1: Adding only if EMV is enabled
		 */
		if(isEmvEnabledInDevice() == PAAS_TRUE)
		{
			/* Store the EMV Key Load Details */
			iCnt = storeEmvKeyLoadDtls(listPtr, &(pstPymtTran->pstEmvKeyLoadInfo));
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to store EMV Key Load Dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			listPtr += iCnt;

			/* TODO: Store the EMV Response Details */
			iCnt = storeEmvRespDtls(listPtr, &(pstPymtTran->pstEmvRespDtls));
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to store EMV Dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			listPtr += iCnt;
		}

		/* Store the Duplicate field details */
		iCnt = storeDupFieldDtls(listPtr, &(pstPymtTran->pstDupDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to store trandtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		listPtr += iCnt;

		// MukeshS3: 15-April-16: These fields will be passed along to the SCI as it is.
		/* save pass through response fields details into paymt transaction */
		iCnt = storePassThrgResFields(listPtr, &pstPymtTran);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save pass through fields details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		listPtr += iCnt;

		iCnt = storeDCCResFields(listPtr, &(pstPymtTran->pstDCCDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to store DCC Fields", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		listPtr += iCnt;

		/*
		 * Certain merchants need the POS to have the full account number for gift card or private label card transactions.
		 * While this is primarily being encountered in Vantiv direct merchants it also surfaces in other host processing models.
		 * The merchant may need this for their gift card liability reconciliation or to evaluate merchandise credit issued gift cards from normal gift cards
		 * SCA is enhanced to return the clear PAN on gift and/or private label cards
		 */
		if(pstPymtTran->iPymtType == PYMT_GIFT || pstPymtTran->iPymtType == PYMT_PRIVATE)
		{
			/*
			 * We will be filling the EMBOSSED_ACCT_NUM field with the clear PAN
			 * we will fill this field if we dont receive Embossed card num fields
			 * in the SSI response
			 */
			if(pstPymtTran->pstCardDtls != NULL)
			{
				if(strlen(pstPymtTran->pstCardDtls->szEmbossedPAN) <= 0)
				{
					//Embossed Card Num is not present in the SSI response, will update here
					if((pstPymtTran->iPymtType == PYMT_GIFT && returnEmbossedNumForGiftType() &&
						isEmbossedCardNumCalcRequired() && (strlen(pstPymtTran->pstCardDtls->szPAN) > 0) &&
						(strcasecmp("SVDOT", (char *)getMerchantProcessor(PROCESSOR_GIFT)) == 0)))
					{
						/*Calculate the Embossed Card Number for Gift Transactions with ValueLink Processors as per the logic
						we already implemented in RCHI*/
						if(iEncType == VSP_ENC)
						{
							getEmbossedPANFromTrackData(pstPymtTran->pstCardDtls->szPAN, pstPymtTran->pstCardDtls->szEmbossedPAN, pstPymtTran->pstCardDtls->bManEntry);
						}
					}
					else
					{
						if( ((returnEmbossedNumForGiftType() && pstPymtTran->iPymtType == PYMT_GIFT) ||
								(returnEmbossedNumForPrivLblType() && pstPymtTran->iPymtType == PYMT_PRIVATE)) && (strlen(pstPymtTran->pstCardDtls->szPAN) > 0) )
						{
							memset(szBIN, 0x00, sizeof(szBIN));
							strncpy(szBIN, pstPymtTran->pstCardDtls->szPAN, 6);


							if(checkBinInclusion(szBIN) == PAAS_FALSE || (strlen(pstPymtTran->pstCardDtls->szPAN) <= 12))
							{
								debug_sprintf(szDbgMsg, "%s: Card is NOT Included in PCI Card Range, updating Embossed Card number", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								if(iEncType == VSP_ENC)
								{
									strcpy(pstPymtTran->pstCardDtls->szEmbossedPAN, pstPymtTran->pstCardDtls->szPAN);
									getDataFromVCL(pstPymtTran->pstCardDtls->szEmbossedPAN, NULL, NULL, NULL, pstPymtTran->pstCardDtls->bManEntry);

									/*
									 * Praveen_P1: Putting extra check here so that we will not send pci clear range
									 */
									if(strlen(pstPymtTran->pstCardDtls->szEmbossedPAN) > 0)
									{
										memset(szBIN, 0x00, sizeof(szBIN));
										strncpy(szBIN, pstPymtTran->pstCardDtls->szEmbossedPAN, 6);
										if(checkBinInclusion(szBIN) == PAAS_TRUE && (strlen(pstPymtTran->pstCardDtls->szPAN) > 12))
										{
											debug_sprintf(szDbgMsg, "%s: PAN is Included in PCI Card Range, not sending Embossed Card number", __FUNCTION__);
											APP_TRACE(szDbgMsg);
											memset(pstPymtTran->pstCardDtls->szEmbossedPAN, 0x00, sizeof(pstPymtTran->pstCardDtls->szEmbossedPAN));
										}
									}
								}
								else if(iEncType == VSD_ENC)
								{
									strcpy(pstPymtTran->pstCardDtls->szEmbossedPAN, pstPymtTran->pstCardDtls->szClrPAN);
								}
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Card is Included in PCI Card Range, not updating Embossed Card number", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}
						}
					}
				}
				else
				{
					debug_sprintf(szDbgMsg,"%s: Embossed Card number is present in the SSI response, not updating it here",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Card Details are not present, will not update embossed account number",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		/* T_VinayS3: Copying PAYMENT_MEDIA into BANK_USERDATA only if
		 * Credit processor is RCNASH (as per PWC-RC Requirement)
		 */
		pszProcessorId = getMerchantProcessor(PYMT_CREDIT);
		if(!strcasecmp(pszProcessorId, RCNASH_PROCESSOR))
		{
			if(pstPymtTran->pstCTranDtls != NULL && pstPymtTran->pstCardDtls != NULL )
			{
				strcpy(pstPymtTran->pstCTranDtls->szBankUserData ,pstPymtTran->pstCardDtls->szPymtMedia);
			}
		}

		/* 5-Jan-16: MukeshS3: WSI issue:- For Token Query command, SCA will internally send Payment Type as ADMIN in the SSI request. Host(either SSI or DHI)
		 * may return Payment Type(as ADMIN) field in the response & subsequently SCA will send this field as it is to the POS(in SCI response).
		 * So, From Now on, SCA will supress(do not send to POS back in SCI response) Payment Type field(if it comes as ADMIN)
		 * even if it comes in host response,for TOKEN_QUERY command.
		 */
		if(pstTran->iCmd == SSI_TOKENQUERY)
		{
			if(pstPymtTran != NULL && pstPymtTran->pstCTranDtls != NULL && !strcasecmp(pstPymtTran->pstCTranDtls->szPymntType, "ADMIN"))
			{
				memset(&(pstPymtTran->pstCTranDtls->szPymntType), 0x00, sizeof(pstPymtTran->pstCTranDtls->szPymntType));
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeBatchResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeBatchResp(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		listPtr = pstMeta->keyValList;

		/* Check and process for the resync key if present in the response
		 * data, the host can send it any time in the response to any
		 * transaction. While building the key list for parsing the SSI
		 * response, we need to keep the first key for parsing the RESYNC_KEY
		 * field value, and this first field only would be checked if populated
		 * with any value which needs to be stored for the device */
		rv = checkDevResyncKey(listPtr);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Resync check FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		listPtr += 1; //1 for the resync key

		/* Store the general response data */
		iCnt = storeGenRespDtls(listPtr, &(pstTran->stRespDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to store gen resp",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: Stored gen resp to Tran key",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeRptResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeRptResp(META_PTYPE pstMeta, TRAN_PTYPE pstTran, char * szMsg)
{
	int				rv				= SUCCESS;
#if 0
	int				iCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	PYMTTRAN_PTYPE	pstPymtTran		= NULL;
#endif
	PASSTHRU_PTYPE	pstPassThru		= NULL;
#ifdef DEVDEBUG
	char			szDbgMsg[4096]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
#if 0
		if(pstTran->iCmd == SSI_LPTQUERY)
		{
			listPtr = pstMeta->keyValList;

			pstPymtTran = (PYMTTRAN_PTYPE)pstTran->data;
			if(pstPymtTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: No place holder for payment data", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			rv = checkDevResyncKey(listPtr);
			if(rv != SUCCESS )
			{
				debug_sprintf(szDbgMsg, "%s: Resync check FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			listPtr += 1; //1 for the resync key

			/* Store the general response data */
			iCnt = storeGenRespDtls(listPtr, &(pstTran->stRespDtls));
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to store gen resp",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			listPtr += iCnt;

			/* Store the current tran details */
			iCnt = storeCurTranDtls(listPtr, &(pstPymtTran->pstCTranDtls));
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to store trandtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			listPtr += iCnt;
		}
#endif
		pstPassThru = (PASSTHRU_PTYPE) pstTran->data;
		if(pstPassThru == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No place holder for pass thru data",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		  /*
		  * szMsg is the response from the Host, this pointer is getting deallocated
		  * in the doSSICommunication func after parsing the SSI response
		  * thats why allocating memory to copy the response from host
		  * to send back to SCI
		  */
		if(szMsg != NULL)
		{
		  pstPassThru->szRespMsg = (char *)malloc(sizeof(char) * (strlen(szMsg) + 1));
		  if(pstPassThru->szRespMsg == NULL)
		  {
				 debug_sprintf(szDbgMsg,"%s: FAILED to allocate mem for passthru struct",__FUNCTION__);
				 APP_TRACE(szDbgMsg);

				 rv = FAILURE;
				 break;
		  }
		  memset(pstPassThru->szRespMsg, 0x00, strlen(szMsg) + 1);
		  memcpy(pstPassThru->szRespMsg, szMsg, strlen(szMsg));
		}
#ifdef DEVDEBUG
		if(strlen(pstPassThru->szRespMsg) < 4000)
		{
			debug_sprintf(szDbgMsg, "%s:%s: passthru Resp Msg = [%s]",DEVDEBUGMSG,__FUNCTION__, pstPassThru->szRespMsg);
			APP_TRACE(szDbgMsg);
		}
#endif

		
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeGenRespDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeGenRespDtls(KEYVAL_PTYPE listPtr, RESPDTLS_PTYPE pstGenResp)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iHostProtocol	= getHostProtocolFormat();

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(pstGenResp, 0x00, sizeof(RESPDTLS_STYPE));

		/*
		 * Praveen_P1: Setting default value of host result code
		 * as -2, Please make sure Host does not give -2
		 * as the result code
		 */
		strcpy(pstGenResp->szHostRsltCode, "-2");
		strcpy(pstGenResp->szRespCode, "-2");

		iTotCnt = sizeof(genRespLst) / KEYVAL_SIZE;

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "TERMINATION_STATUS") == SUCCESS)
			{
				strncpy(pstGenResp->szTermStat, listPtr[iCnt].value, sizeof(pstGenResp->szTermStat) - 1);
			}
			else if(strcmp(listPtr[iCnt].key, "RESULT_CODE") == SUCCESS)
			{
				strncpy(pstGenResp->szRsltCode, listPtr[iCnt].value, sizeof(pstGenResp->szRsltCode) - 1);
			}
			else if(strcmp(listPtr[iCnt].key, "RESULT") == SUCCESS)
			{
				strncpy(pstGenResp->szRslt, listPtr[iCnt].value, sizeof(pstGenResp->szRslt) - 1);
			}
			else if(strcmp(listPtr[iCnt].key, "RESPONSE_TEXT") == SUCCESS)
			{
				strncpy(pstGenResp->szRespTxt, listPtr[iCnt].value, sizeof(pstGenResp->szRespTxt) - 1);
			}
			else if(strcmp(listPtr[iCnt].key, "RESPONSE_CODE") == SUCCESS)
			{
				memset(pstGenResp->szRespCode, 0x00, sizeof(pstGenResp->szRespCode));
				strncpy(pstGenResp->szRespCode, listPtr[iCnt].value, sizeof(pstGenResp->szRespCode) - 1);
			}
			else if(iHostProtocol == HOST_PROTOCOL_SSI)
			{
				if(strcmp(listPtr[iCnt].key, "HOST_RESPCODE") == SUCCESS)
				{
					memset(pstGenResp->szHostRsltCode, 0x00, sizeof(pstGenResp->szHostRsltCode));
					strncpy(pstGenResp->szHostRsltCode, listPtr[iCnt].value, sizeof(pstGenResp->szHostRsltCode) - 1);
				}
			}
		}
		/* Daivik:15/6/2016 If Host Result Code was not present , then copy whatever we got in the RESPONSE_CODE field */
		if(strcmp(pstGenResp->szHostRsltCode, "-2") == 0)
		{
			memset(pstGenResp->szHostRsltCode, 0x00, sizeof(pstGenResp->szHostRsltCode));
			strncpy(pstGenResp->szHostRsltCode, pstGenResp->szRespCode, sizeof(pstGenResp->szHostRsltCode) - 1);
		}
		rv = iTotCnt;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeCreditAppRespDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeCreditAppRespDtls(KEYVAL_PTYPE listPtr, CREDITAPPDTLS_PTYPE pstCreditAppDtls)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		iTotCnt			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt = sizeof(creditAppRespList) / KEYVAL_SIZE;

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "PROXY_REF") == SUCCESS)
			{
				strncpy(pstCreditAppDtls->szProxyRef, listPtr[iCnt].value, sizeof(pstCreditAppDtls->szProxyRef) - 1);
			}
			else if(strcmp(listPtr[iCnt].key, "PROXY_RESULT") == SUCCESS)
			{
				strncpy(pstCreditAppDtls->szProxyResult, listPtr[iCnt].value, sizeof(pstCreditAppDtls->szProxyResult) - 1);
			}
			else if(strcmp(listPtr[iCnt].key, "CREDITAPP_PAYLOAD") == SUCCESS)
			{
				strncpy(pstCreditAppDtls->szCreditAppPayLoad, listPtr[iCnt].value, sizeof(pstCreditAppDtls->szCreditAppPayLoad) - 1);
			}
		}

		rv = iTotCnt;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeDevResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeDevResp(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int				rv				= SUCCESS;
	KEYVAL_PTYPE	listPtr			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);


		listPtr = pstMeta->keyValList;

		pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		if(pstDevTran == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No dev tran present",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}

		switch(pstTran->iCmd)
		{
		case SSI_CREDITAPP:
			storeCreditAppRespDtls(listPtr, &(pstDevTran->stCreditAppDtls));
			break;

		default:
			debug_sprintf(szDbgMsg,"%s: Should not come here",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addEmvMVTResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addEmvMVTResp(EMVKEYLOAD_INFO_PTYPE pstEmvKeyLoadInfo, VAL_LST_PTYPE pstValLst)
{
	int				rv								= SUCCESS;
	EMVKEYLOAD_NODE_PTYPE	pstEmvKeyLoadNode		= NULL;
	VAL_NODE_PTYPE	nodePtr							= NULL;
	EMVKEYLOAD_NODE_STYPE	stEmvKeyLoadNode;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	nodePtr = pstValLst->start;

	while(nodePtr != NULL)
	{
		memset(&stEmvKeyLoadNode, 0x00, sizeof(EMVKEYLOAD_NODE_STYPE));
		if((nodePtr->type == FALLBACK_INDICATOR_0) || (nodePtr->type == FALLBACK_INDICATOR_1) ||
		   (nodePtr->type == FALLBACK_INDICATOR_2) || (nodePtr->type == FALLBACK_INDICATOR_3) ||
		   (nodePtr->type == FALLBACK_INDICATOR_4) || (nodePtr->type == FALLBACK_INDICATOR_5))
		{
			rv = saveFallBckIndicator(&stEmvKeyLoadNode, nodePtr);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to store FALLBACK_INDICATOR",
								__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else if((nodePtr->type == OFFLINE_FLOOR_LIMIT_0) || (nodePtr->type == OFFLINE_FLOOR_LIMIT_1) ||
				(nodePtr->type == OFFLINE_FLOOR_LIMIT_2) || (nodePtr->type == OFFLINE_FLOOR_LIMIT_3))				

		{
			rv = saveOffLineFlrLmt(&stEmvKeyLoadNode, nodePtr);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to store offline floor limit",
								__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid EMV download tag type %d",
										__FUNCTION__, nodePtr->type);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		pstEmvKeyLoadNode = (EMVKEYLOAD_NODE_PTYPE) malloc(sizeof(EMVKEYLOAD_NODE_STYPE));
		if(pstEmvKeyLoadNode == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memcpy(pstEmvKeyLoadNode, &stEmvKeyLoadNode, sizeof(EMVKEYLOAD_NODE_STYPE));

		if(pstEmvKeyLoadInfo->mainLstHead == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Adding First Node",__FUNCTION__);
			APP_TRACE(szDbgMsg);		
			pstEmvKeyLoadInfo->mainLstHead = pstEmvKeyLoadNode;
			pstEmvKeyLoadInfo->mainLstTail = pstEmvKeyLoadNode;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Adding Node to existing linked list",__FUNCTION__);
			APP_TRACE(szDbgMsg);		
			pstEmvKeyLoadInfo->mainLstTail->nextEmvNode = pstEmvKeyLoadNode;
			pstEmvKeyLoadInfo->mainLstTail = pstEmvKeyLoadNode;
		}

		pstEmvKeyLoadNode = NULL;
		nodePtr = nodePtr->next;	
	}

	if(rv != SUCCESS)
	{
		EMVKEYLOAD_NODE_PTYPE	pstTmpNode	= NULL;

		pstEmvKeyLoadNode = pstEmvKeyLoadInfo->mainLstHead;
		while(pstEmvKeyLoadNode != NULL)
		{
			if(pstEmvKeyLoadNode->emvData != NULL)
			{
				free(pstEmvKeyLoadNode->emvData);
			}

			pstTmpNode = pstEmvKeyLoadNode;
			pstEmvKeyLoadNode  = pstEmvKeyLoadNode->nextEmvNode;
			free(pstTmpNode);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addEmvCAPKResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addEmvCAPKResp(EMVKEYLOAD_INFO_PTYPE pstEmvKeyLoadInfo, VAL_LST_PTYPE pstValLst)
{
	int				rv								= SUCCESS;
	EMVKEYLOAD_NODE_PTYPE	pstEmvKeyLoadNode		= NULL;
	VAL_NODE_PTYPE	nodePtr							= NULL;
	EMVKEYLOAD_NODE_STYPE	stEmvKeyLoadNode;
#ifdef DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	nodePtr = pstValLst->start;

	while(nodePtr != NULL)
	{
		memset(&stEmvKeyLoadNode, 0x00, sizeof(EMVKEYLOAD_NODE_STYPE));
		if((nodePtr->type == PUBLIC_KEY_0) || (nodePtr->type == PUBLIC_KEY_1) ||
		   (nodePtr->type == PUBLIC_KEY_2) || (nodePtr->type == PUBLIC_KEY_3) ||
		   (nodePtr->type == PUBLIC_KEY_4) || (nodePtr->type == PUBLIC_KEY_5))
		{
			rv = savePublicKeys(&stEmvKeyLoadNode, nodePtr);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to store PUBLIC_KEY",
								__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid EMV PUBLIC_KEY tag type %d",
										__FUNCTION__, nodePtr->type);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		pstEmvKeyLoadNode = (EMVKEYLOAD_NODE_PTYPE) malloc(sizeof(EMVKEYLOAD_NODE_STYPE));
		if(pstEmvKeyLoadNode == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memcpy(pstEmvKeyLoadNode, &stEmvKeyLoadNode, sizeof(EMVKEYLOAD_NODE_STYPE));

		if(pstEmvKeyLoadInfo->mainLstHead == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Adding First Node",__FUNCTION__);
			APP_TRACE(szDbgMsg);		
			pstEmvKeyLoadInfo->mainLstHead = pstEmvKeyLoadNode;
			pstEmvKeyLoadInfo->mainLstTail = pstEmvKeyLoadNode;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Adding Node to existing linked list",__FUNCTION__);
			APP_TRACE(szDbgMsg);		
			pstEmvKeyLoadInfo->mainLstTail->nextEmvNode = pstEmvKeyLoadNode;
			pstEmvKeyLoadInfo->mainLstTail = pstEmvKeyLoadNode;
		}

		pstEmvKeyLoadNode = NULL;
		nodePtr = nodePtr->next;	
	}

	if(rv != SUCCESS)
	{
		EMVKEYLOAD_NODE_PTYPE	pstTmpNode	= NULL;

		pstEmvKeyLoadNode = pstEmvKeyLoadInfo->mainLstHead;
		while(pstEmvKeyLoadNode != NULL)
		{
			if(pstEmvKeyLoadNode->emvData != NULL)
			{
				free(pstEmvKeyLoadNode->emvData);
			}

			pstTmpNode = pstEmvKeyLoadNode;
			pstEmvKeyLoadNode  = pstEmvKeyLoadNode->nextEmvNode;
			free(pstTmpNode);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveFallBckIndicator
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveFallBckIndicator(EMVKEYLOAD_NODE_PTYPE pstEmvKeyLoadNode,VAL_NODE_PTYPE pstValNode)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	FALLBACK_STYPE	stFallBckDtls;
#ifdef DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		listPtr = pstValNode->elemList;
		iTotCnt = pstValNode->elemCnt;
		memset(&stFallBckDtls, 0x00, sizeof(FALLBACK_STYPE));
		debug_sprintf(szDbgMsg, "%s: iTotCnt[%d]", __FUNCTION__, iTotCnt);
		APP_TRACE(szDbgMsg);
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			debug_sprintf(szDbgMsg, "%s: --- Inside for[%d], [%s]", __FUNCTION__, iCnt, (char *)listPtr[iCnt].key);
			APP_TRACE(szDbgMsg);		
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}	
			else if(strcmp(listPtr[iCnt].key, "FALLBACK_VALUE") == SUCCESS)
			{	
				strncpy(stFallBckDtls.szFallBackValue, listPtr[iCnt].value, sizeof(stFallBckDtls.szFallBackValue) - 1);
				debug_sprintf(szDbgMsg, "%s: FallBackValue[%s]", __FUNCTION__, 
												stFallBckDtls.szFallBackValue);
				APP_TRACE(szDbgMsg);				
			}
			else if(strcmp(listPtr[iCnt].key, "APP_ID") == SUCCESS)
			{
				strncpy(stFallBckDtls.szAppID, listPtr[iCnt].value, sizeof(stFallBckDtls.szAppID) - 1);
				debug_sprintf(szDbgMsg, "%s: AppID[%s]", __FUNCTION__, 
												 stFallBckDtls.szAppID);
				APP_TRACE(szDbgMsg);				
			}
			else if(strcmp(listPtr[iCnt].key, "FALLBACK_IND") == SUCCESS)
			{
				strncpy(stFallBckDtls.szFallBackInd, listPtr[iCnt].value, sizeof(stFallBckDtls.szFallBackInd) - 1);
				debug_sprintf(szDbgMsg, "%s: FallBackInd[%s]", __FUNCTION__, 
													stFallBckDtls.szFallBackInd);
				APP_TRACE(szDbgMsg);				
			}
		}
		pstEmvKeyLoadNode->emvData = (FALLBACK_PTYPE) malloc(sizeof(FALLBACK_STYPE));
		if(pstEmvKeyLoadNode->emvData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memcpy(pstEmvKeyLoadNode->emvData, &stFallBckDtls, sizeof(FALLBACK_STYPE));
		pstEmvKeyLoadNode->emvKeyLoadType = FALLBACK_INDICATOR;
		
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: savePublicKeys
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int savePublicKeys(EMVKEYLOAD_NODE_PTYPE pstEmvKeyLoadNode,VAL_NODE_PTYPE pstValNode)
{
	int							rv				= SUCCESS;
	int							iCnt			= 0;
	int							iTotCnt			= 0;
	KEYVAL_PTYPE				listPtr			= NULL;
	EMV_CAPK_INFO_STYPE			stCAPKDtls;
#ifdef DEBUG
	char			szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		listPtr = pstValNode->elemList;
		iTotCnt = pstValNode->elemCnt;
		memset(&stCAPKDtls, 0x00, sizeof(EMV_CAPK_INFO_STYPE));

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "APP_ID") == SUCCESS)
			{
				strncpy(stCAPKDtls.szRID, listPtr[iCnt].value, sizeof(stCAPKDtls.szRID) - 1);
				/* Assign RID length */
				stCAPKDtls.iRIDLen = strlen(stCAPKDtls.szRID);
				
				debug_sprintf(szDbgMsg, "%s: AppID[%s]", __FUNCTION__, stCAPKDtls.szRID);
				APP_TRACE(szDbgMsg);				
			}			
			else if(strcmp(listPtr[iCnt].key, "PUBLIC_KEY_IND") == SUCCESS)
			{	
				strncpy(stCAPKDtls.szPKInd, listPtr[iCnt].value, sizeof(stCAPKDtls.szPKInd) - 1);

				debug_sprintf(szDbgMsg, "%s: PK Ind[%s]", __FUNCTION__, stCAPKDtls.szPKInd);
				APP_TRACE(szDbgMsg);				
			}			
			else if(strcmp(listPtr[iCnt].key, "PUBLIC_KEY_CHECKSUM") == SUCCESS)
			{	
				#if 0
				if(stCAPKDtls.pszCheckSum == NULL)
				{
					stCAPKDtls.pszCheckSum = malloc(strlen(listPtr[iCnt].value)+1);
				}	
				memset(stCAPKDtls.pszCheckSum, 0x00, strlen(listPtr[iCnt].value)+1);
				#endif

				strncpy(stCAPKDtls.szCheckSum, listPtr[iCnt].value, sizeof(stCAPKDtls.szCheckSum) - 1);

				debug_sprintf(szDbgMsg, "%s: PK checksum[%s]", __FUNCTION__, stCAPKDtls.szCheckSum);
				APP_TRACE(szDbgMsg);				
			}
			else if(strcmp(listPtr[iCnt].key, "PUBLIC_KEY_CHECKSUM_LEN") == SUCCESS)
			{	
				/* Assign PK Checksum Length */
				stCAPKDtls.iCheckSumLen = atoi(listPtr[iCnt].value);
				
				debug_sprintf(szDbgMsg, "%s: PK checksum len[%d]", __FUNCTION__, stCAPKDtls.iCheckSumLen);
				APP_TRACE(szDbgMsg);					
			}				
			else if(strcmp(listPtr[iCnt].key, "PUBLIC_KEY_INDEX") == SUCCESS)
			{	
				#if 0
				if(stCAPKDtls.pszPKIndex == NULL)
				{
					stCAPKDtls.pszPKIndex = malloc(strlen(listPtr[iCnt].value)+1);
				}				
				memset(stCAPKDtls.pszPKIndex, 0x00, strlen(listPtr[iCnt].value)+1);
				#endif	

				strncpy(stCAPKDtls.szPKIndex, listPtr[iCnt].value, sizeof(stCAPKDtls.szPKIndex) - 1);

				/* Assign PKIndex length */
				stCAPKDtls.iPKIndexLen = strlen(stCAPKDtls.szPKIndex);
				
				debug_sprintf(szDbgMsg, "%s: PK Index[%s]", __FUNCTION__, stCAPKDtls.szPKIndex);
				APP_TRACE(szDbgMsg);				
			}			
			else if(strcmp(listPtr[iCnt].key, "PUBLIC_KEY_MOD") == SUCCESS)
			{	
				#if 0
				if(stCAPKDtls.pszModulus == NULL)
				{
					stCAPKDtls.pszModulus = malloc(strlen(listPtr[iCnt].value)+1);
				}						
				memset(stCAPKDtls.pszModulus, 0x00, strlen(listPtr[iCnt].value)+1);
				#endif		

				strncpy(stCAPKDtls.szModulus, listPtr[iCnt].value, sizeof(stCAPKDtls.szModulus) - 1);

				debug_sprintf(szDbgMsg, "%s: PK Mod[%s]", __FUNCTION__, stCAPKDtls.szModulus);
				APP_TRACE(szDbgMsg);				
			}
			else if(strcmp(listPtr[iCnt].key, "PUBLIC_KEY_MOD_LEN") == SUCCESS)
			{	
				/* Assign Modulus Length */
				stCAPKDtls.iModLen = atoi(listPtr[iCnt].value);
				
				debug_sprintf(szDbgMsg, "%s: PK mod len[%d]", __FUNCTION__, stCAPKDtls.iModLen);
				APP_TRACE(szDbgMsg);				
			}			
			else if(strcmp(listPtr[iCnt].key, "PUBLIC_KEY_EXP") == SUCCESS)
			{
				#if 0
				if(stCAPKDtls.pszPKIndex == NULL)
				{
					stCAPKDtls.pszExponent = malloc(strlen(listPtr[iCnt].value)+1);
				}						
				memset(stCAPKDtls.pszExponent, 0x00, strlen(listPtr[iCnt].value)+1);
				#endif	

				strncpy(stCAPKDtls.szExponent, listPtr[iCnt].value, sizeof(stCAPKDtls.szExponent) - 1);

				debug_sprintf(szDbgMsg, "%s: PK Exponent[%s]", __FUNCTION__, stCAPKDtls.szExponent);
				APP_TRACE(szDbgMsg);				
			}
			else if(strcmp(listPtr[iCnt].key, "PUBLIC_KEY_EXP_LEN") == SUCCESS)
			{	
				/* Assign PK exp Length */
				stCAPKDtls.iExpLen = atoi(listPtr[iCnt].value);
				
				debug_sprintf(szDbgMsg, "%s: PK Exponent len[%d]", __FUNCTION__, stCAPKDtls.iExpLen);
				APP_TRACE(szDbgMsg);				
			}
			else if(strcmp(listPtr[iCnt].key, "PUBLIC_KEY_EXP_DATE") == SUCCESS)
			{
				/* Copy Public Key Expiry Date in DDMMYY format*/
				strncpy(stCAPKDtls.szExpDate, listPtr[iCnt].value, sizeof(stCAPKDtls.szExpDate) - 1);

				debug_sprintf(szDbgMsg, "%s: PK Expiry date [%s]", __FUNCTION__, stCAPKDtls.szExpDate);
				APP_TRACE(szDbgMsg);
			}
		}

		pstEmvKeyLoadNode->emvData = (EMV_CAPK_INFO_PTYPE) malloc(sizeof(EMV_CAPK_INFO_STYPE));
		if(pstEmvKeyLoadNode->emvData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memcpy(pstEmvKeyLoadNode->emvData, &stCAPKDtls, sizeof(EMV_CAPK_INFO_STYPE));
		pstEmvKeyLoadNode->emvKeyLoadType = PUBLIC_KEY;
		
		break;
	}
	#if 0
	if(rv != SUCCESS)
	{
		if(stCAPKDtls.pszCheckSum != NULL)
		{
			free(stCAPKDtls.pszCheckSum);
		}
		if(stCAPKDtls.pszPKIndex != NULL)
		{
			free(stCAPKDtls.pszPKIndex);
		}
		if(stCAPKDtls.pszModulus != NULL)
		{
			free(stCAPKDtls.pszModulus);
		}
		if(stCAPKDtls.pszExponent != NULL)
		{
			free(stCAPKDtls.pszExponent);
		}		
	}
	#endif
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveOffLineFlrLmt
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveOffLineFlrLmt(EMVKEYLOAD_NODE_PTYPE pstEmvKeyLoadNode,VAL_NODE_PTYPE pstValNode)
{
	int							rv				= SUCCESS;
	int							iCnt			= 0;
	int							iTotCnt			= 0;
	KEYVAL_PTYPE				listPtr			= NULL;
	OFFLINE_FLR_LIMIT_STYPE		stOfflineFlrLmtDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		listPtr = pstValNode->elemList;
		iTotCnt = pstValNode->elemCnt;
		memset(&stOfflineFlrLmtDtls, 0x00, sizeof(OFFLINE_FLR_LIMIT_STYPE));

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "OFFLINE_FLOOR_LIMIT_VALUE") == SUCCESS)
			{
				strncpy(stOfflineFlrLmtDtls.szOffFlrLmt, listPtr[iCnt].value, sizeof(stOfflineFlrLmtDtls.szOffFlrLmt) - 1);

				debug_sprintf(szDbgMsg, "%s: szOffFlrLmt[%s]", __FUNCTION__,
											stOfflineFlrLmtDtls.szOffFlrLmt);
				APP_TRACE(szDbgMsg);				
			}
			else if(strcmp(listPtr[iCnt].key, "APP_ID") == SUCCESS)
			{
				strncpy(stOfflineFlrLmtDtls.szAppID, listPtr[iCnt].value, sizeof(stOfflineFlrLmtDtls.szAppID) - 1);

				debug_sprintf(szDbgMsg, "%s: szAppID[%s]", __FUNCTION__, 
											stOfflineFlrLmtDtls.szAppID);
				APP_TRACE(szDbgMsg);					
			}
			else if(strcmp(listPtr[iCnt].key, "OFFLINE_FLOOR_LIMIT_IND") == SUCCESS)
			{
				strncpy(stOfflineFlrLmtDtls.szOffFlrLmtInd, listPtr[iCnt].value, sizeof(stOfflineFlrLmtDtls.szOffFlrLmtInd) - 1);

				debug_sprintf(szDbgMsg, "%s: szOffFlrLmtInd[%s]", __FUNCTION__,
											stOfflineFlrLmtDtls.szOffFlrLmtInd);
				APP_TRACE(szDbgMsg);				
			}
			else if(strcmp(listPtr[iCnt].key, "FLOOR_LIMIT_THRESHOLD") == SUCCESS)
			{
				strncpy(stOfflineFlrLmtDtls.szOffFlrLmtThrshld, listPtr[iCnt].value, sizeof(stOfflineFlrLmtDtls.szOffFlrLmtThrshld) - 1);

				debug_sprintf(szDbgMsg, "%s: szOffFlrLmtThrshld[%s]", __FUNCTION__,
												stOfflineFlrLmtDtls.szOffFlrLmtThrshld);
				APP_TRACE(szDbgMsg);					
			}			
		}

		pstEmvKeyLoadNode->emvData = 
				(OFFLINE_FLR_LIMIT_PTYPE) malloc(sizeof(OFFLINE_FLR_LIMIT_STYPE));
		if(pstEmvKeyLoadNode->emvData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memcpy(pstEmvKeyLoadNode->emvData, 
						&stOfflineFlrLmtDtls, sizeof(OFFLINE_FLR_LIMIT_STYPE));
		pstEmvKeyLoadNode->emvKeyLoadType = OFFLINE_FLOOR_LIMIT;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeAdminParams
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeDHIVersionParam(KEYVAL_PTYPE listPtr)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		iTotCnt			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt += sizeof(versionRespList) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		if(listPtr[iCnt].value == NULL)
		{
			continue;
		}
		else if(strcmp(listPtr[iCnt].key, "VERSION_INFO") == SUCCESS)
		{
			storeDHIVersionInfo((char*) listPtr[iCnt].value);
		}
	}

	if(rv == SUCCESS)
	{
		rv = iTotCnt;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeAdminParams
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeAdminParams(KEYVAL_PTYPE listPtr)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		iTotCnt			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt += sizeof(adminRespLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		if(listPtr[iCnt].value == NULL)
		{
			continue;
		}
		else if(strcmp(listPtr[iCnt].key, "DEVICEKEY") == SUCCESS)
		{
			rv = storeDeviceKey((char *)listPtr[iCnt].value);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to store device key",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else if(strcmp(listPtr[iCnt].key, "CLIENT_ID") == SUCCESS)
		{
			rv = storeClientId((char *)listPtr[iCnt].value);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to store client id",
															__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else if(strcmp(listPtr[iCnt].key, "HEADERS") == SUCCESS)
		{
			storeHeaders((VAL_NODE_PTYPE) listPtr[iCnt].value);
		}
		else if(strcmp(listPtr[iCnt].key, "FOOTERS") == SUCCESS)
		{
			storeFooters((VAL_NODE_PTYPE) listPtr[iCnt].value);
		}
		else if(strcmp(listPtr[iCnt].key, "DISCLAIMERS") == SUCCESS)
		{
			storeDisclaimers((VAL_NODE_PTYPE) listPtr[iCnt].value);
		}
		else if(strcmp(listPtr[iCnt].key, "CARD_TYPES") == SUCCESS)
		{
			storeSuppCardInfo((VAL_NODE_PTYPE) listPtr[iCnt].value);
		}
		else if(strcmp(listPtr[iCnt].key, "SUPPORTED_FUNCTIONS") == SUCCESS)
		{
			storeSuppFxnInfo((VAL_NODE_PTYPE) listPtr[iCnt].value);
		}
	}

	if(rv == SUCCESS)
	{
		rv = iTotCnt;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeHeaders
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int storeHeaders(VAL_NODE_PTYPE valNodePtr)
{
	int				rv				= SUCCESS;
	KEYVAL_PTYPE	tmpLstPtr		= NULL;
	int				iTmpTotCnt		= 0;
	int				iCount			= 0;
	int				iIndex			= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(valNodePtr != NULL)
	{
		tmpLstPtr = valNodePtr->elemList;
		iTmpTotCnt = valNodePtr->elemCnt;

		for(iCount = 0; iCount < iTmpTotCnt; iCount++)
		{
			if(tmpLstPtr[iCount].value == NULL)
			{
				continue;
			}

			if(strcmp(tmpLstPtr[iCount].key, "NO") == SUCCESS)
			{
				iIndex = atoi((char *) tmpLstPtr[iCount].value);

				if(valNodePtr->szVal != NULL)
				{
					setReceiptHeader(iIndex, valNodePtr->szVal);
				}
			}
		}
		valNodePtr = valNodePtr->next;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeFooters
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int storeFooters(VAL_NODE_PTYPE valNodePtr)
{
	int				rv				= SUCCESS;
	KEYVAL_PTYPE	tmpLstPtr		= NULL;
	int				iTmpTotCnt		= 0;
	int				iCount			= 0;
	int				iIndex			= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	while(valNodePtr != NULL)
	{
		tmpLstPtr = valNodePtr->elemList;
		iTmpTotCnt = valNodePtr->elemCnt;

		for(iCount = 0; iCount < iTmpTotCnt; iCount++)
		{
			if(tmpLstPtr[iCount].value == NULL)
			{
				continue;
			}

			if(strcmp(tmpLstPtr[iCount].key, "NO") == SUCCESS)
			{
				iIndex = atoi((char *) tmpLstPtr[iCount].value);

				if(valNodePtr->szVal != NULL)
				{
					setReceiptFooter(iIndex, valNodePtr->szVal);
				}
			}
		}

		valNodePtr = valNodePtr->next;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeDisclaimers
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int storeDisclaimers(VAL_NODE_PTYPE valNodePtr)
{
	int				rv				= SUCCESS;
	KEYVAL_PTYPE	tmpLstPtr		= NULL;
	int				iTmpTotCnt		= 0;
	int				iCount			= 0;
	int				iIndex			= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(valNodePtr != NULL)
	{
		tmpLstPtr = valNodePtr->elemList;
		iTmpTotCnt = valNodePtr->elemCnt;


		for(iCount = 0; iCount < iTmpTotCnt; iCount++)
		{
			if(tmpLstPtr[iCount].value == NULL)
			{
				continue;
			}

			if(strcmp(tmpLstPtr[iCount].key, "NO") == SUCCESS)
			{
				iIndex = atoi((char *) tmpLstPtr[iCount].value);

				if(valNodePtr->szVal != NULL)
				{
					setReceiptDisclaimer(iIndex, valNodePtr->szVal);
				}
			}
		}

		valNodePtr = valNodePtr->next;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: storeSuppCardInfo
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int storeSuppCardInfo(VAL_NODE_PTYPE valNodePtr)
{
	int				rv				= SUCCESS;
	KEYVAL_PTYPE	tmpLstPtr		= NULL;
	int				iTmpTotCnt		= 0;
	int				iCount			= 0;
	char *			cTmpPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(valNodePtr != NULL)
	{
		tmpLstPtr = valNodePtr->elemList;
		iTmpTotCnt = valNodePtr->elemCnt;

		if(valNodePtr->szVal != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: IPC lbl = [%s]", __FUNCTION__, valNodePtr->szVal);
			APP_TRACE(szDbgMsg);
		}

		for(iCount = 0; iCount < iTmpTotCnt; iCount++)
		{
			if(tmpLstPtr[iCount].value == NULL)
			{
				continue;
			}

			if(strcmp(tmpLstPtr[iCount].key, "ENABLED") == SUCCESS)
			{
				cTmpPtr = (char *) tmpLstPtr[iCount].value;

				debug_sprintf(szDbgMsg, "%s: enabled = [%s]", __FUNCTION__, cTmpPtr);
				APP_TRACE(szDbgMsg);

				/* Update the CDT records in the memory */
				updateCardRangeField(cTmpPtr, valNodePtr->szVal);
			}
		}

		valNodePtr = valNodePtr->next;
	}

	/* Update the CDT records in the file */
	commitCDTChanges();

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: storeSuppFxnInfo
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int storeSuppFxnInfo(VAL_NODE_PTYPE valNodePtr)
{
	int				rv				= SUCCESS;
	char *			pszProcessorId	= NULL;
	char *			pszPaymentId		= NULL;
	KEYVAL_PTYPE	tmpLstPtr		= NULL;
	int				iTmpTotCnt		= 0;
	int				iCount			= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(valNodePtr != NULL)
	{
		tmpLstPtr = valNodePtr->elemList;
		iTmpTotCnt = valNodePtr->elemCnt;
		pszProcessorId = NULL;
		pszPaymentId = NULL;

		for(iCount = 0; iCount < iTmpTotCnt; iCount++)
		{
			if(tmpLstPtr[iCount].value == NULL)
			{
				continue;
			}

			if(strcmp(tmpLstPtr[iCount].key, "PROCESSOR_ID") == SUCCESS)
			{
				pszProcessorId = (char *)tmpLstPtr[iCount].value;
			}
			else if(strcmp(tmpLstPtr[iCount].key, "ID") == SUCCESS)
			{
				pszPaymentId = (char *)tmpLstPtr[iCount].value;
			}
		}

		if((pszProcessorId != NULL) && (pszPaymentId != NULL))
		{
			rv = storeProcessorId(pszPaymentId, pszProcessorId);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg,
						"%s: Failed to store proc id for [%s]", __FUNCTION__, pszPaymentId);
				APP_TRACE(szDbgMsg);
			}
		}
		valNodePtr = valNodePtr->next;
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeVSPRespDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeVSPRespDtls(KEYVAL_PTYPE listPtr, VSPDTLS_PTYPE * dpstVSP)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iSize			= 0;
	int				iTotCnt			= 0;
	VSPDTLS_STYPE	stVSP;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(VSPDTLS_STYPE);

	while(1)
	{
		/* If the VSP details structure is already present in the transaction
		 * structure then do not flush the already existing details */
		if(*dpstVSP != NULL)
		{
			memcpy(&stVSP, *dpstVSP, iSize);
		}
		else
		{
			memset(&stVSP, 0x00, iSize);
		}

		iTotCnt = sizeof(vspRespLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "VSP_CODE") == SUCCESS)
			{
				strncpy(stVSP.szCode, listPtr[iCnt].value, sizeof(stVSP.szCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "VSP_TRXID") == SUCCESS)
			{
				strncpy(stVSP.szTrxId, listPtr[iCnt].value, sizeof(stVSP.szTrxId) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "VSP_RESULTDESC") == SUCCESS)
			{
				strncpy(stVSP.szResultDesc, listPtr[iCnt].value, sizeof(stVSP.szResultDesc) - 1);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstVSP == NULL)
			{
				/* Allocate memory to store the VSP response details for the
				 * transaction */
				*dpstVSP = (VSPDTLS_PTYPE) malloc(iSize);
				if(*dpstVSP == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstVSP, 0x00, iSize);
			memcpy(*dpstVSP, &stVSP, iSize);
		}

		rv = iTotCnt;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeAmtDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeAmtDtls(KEYVAL_PTYPE listPtr, AMTDTLS_PTYPE * dpstAmt, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iSize			= 0;
	int				iTotCnt			= 0;
	int				iStringSize		= 0;
	PAAS_BOOL		bRecvApprovedAmt= PAAS_FALSE;
	AMTDTLS_STYPE	stAmtDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize 		= sizeof(AMTDTLS_STYPE);
	iStringSize = sizeof(stAmtDtls.apprvdAmt) - 1;
	while(1)
	{
		/* If the amount details is already present in the transaction
		 * structure then do not flush the already existing details */
		if(*dpstAmt != NULL)
		{
			memset(&stAmtDtls, 0x00, iSize);
			memcpy(&stAmtDtls, *dpstAmt, iSize);
		}
		else
		{
			memset(&stAmtDtls, 0x00, iSize);
		}

		iTotCnt = sizeof(amtLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "TRANS_AMOUNT") == SUCCESS)
			{
				if(strlen(stAmtDtls.tranAmt) <= 0)
				{
					strncpy(stAmtDtls.tranAmt, listPtr[iCnt].value, iStringSize);
				}
				/*
				 * Praveen_P1: Some of the processors like TSYS, FD NORTH
				 * are not sending APPROVED_AMOUNT tag, TRANS_AMOUNT will be
				 * updated in that case, so copying this amount to approved amount
				 * variable
				 */
				strncpy(stAmtDtls.apprvdAmt, listPtr[iCnt].value, iStringSize);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "APPROVED_AMOUNT") == SUCCESS)
			{
				bRecvApprovedAmt = PAAS_TRUE;
				strncpy(stAmtDtls.apprvdAmt, listPtr[iCnt].value, iStringSize);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PREVIOUS_BALANCE") == SUCCESS)
			{
				strncpy(stAmtDtls.previousBal, listPtr[iCnt].value, iStringSize);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AUTH_AMOUNT") == SUCCESS)
			{
				strncpy(stAmtDtls.apprvdAmt, listPtr[iCnt].value, iStringSize);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AVAIL_BALANCE") == SUCCESS)
			{
				strncpy(stAmtDtls.availBal, listPtr[iCnt].value, iStringSize);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AMOUNT_BALANCE") == SUCCESS)
			{
				strncpy(stAmtDtls.availBal, listPtr[iCnt].value, iStringSize);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DIFF_AMOUNT_DUE") == SUCCESS)
			{
				strncpy(stAmtDtls.dueamount, listPtr[iCnt].value, iStringSize);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DIFF_DUE_AMOUNT") == SUCCESS)
			{
				strncpy(stAmtDtls.dueamount, listPtr[iCnt].value, iStringSize);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "ORIG_TRANS_AMOUNT") == SUCCESS)
			{
				strncpy(stAmtDtls.tranAmt, listPtr[iCnt].value, iStringSize);
				strncpy(stAmtDtls.origTranAmtFrmHost, listPtr[iCnt].value, iStringSize);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DIFF_AUTH_AMOUNT") == SUCCESS)
			{
				strncpy(stAmtDtls.diffAuthAmt, listPtr[iCnt].value, iStringSize);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "FS_AVAIL_BALANCE") == SUCCESS)
			{
				strncpy(stAmtDtls.EBTFoodSNAPBal, listPtr[iCnt].value, iStringSize);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CB_AVAIL_BALANCE") == SUCCESS)
			{
				strncpy(stAmtDtls.EBTCashBenefitsBal, listPtr[iCnt].value, iStringSize);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			// PTMX-675: For Gift Close, we Send the Previous Balance Field as a Approved amount, as some Processors do not send Approved amount for Gift Close Transactions.
			if(iCmd == SSI_GIFTCLOSE && (bRecvApprovedAmt == PAAS_FALSE) && (strlen(stAmtDtls.previousBal) > 0))
			{
				strcpy(stAmtDtls.apprvdAmt, stAmtDtls.previousBal);
			}

			if(*dpstAmt == NULL)
			{
				/* Allocate memory to store the VSP response details for the
				 * transaction */
				*dpstAmt = (AMTDTLS_PTYPE) malloc(iSize);
				if(*dpstAmt == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

			}

			memset(*dpstAmt, 0x00, iSize);
			memcpy(*dpstAmt, &stAmtDtls, iSize);
		}

		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeCardDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeCardDtls(KEYVAL_PTYPE listPtr, CARDDTLS_PTYPE * dpstCardDtls, int iPymtType)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iSize			= 0;
	int				iTotCnt			= 0;
	int				iHostProtocol	= getHostProtocolFormat();
	CARDDTLS_STYPE	stCardDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(CARDDTLS_STYPE);

	while(1)
	{
		/* If the card details is already present in the transaction
		 * structure then do not flush the already existing details */
		if(*dpstCardDtls != NULL)
		{
			memcpy(&stCardDtls, *dpstCardDtls, iSize);
		}
		else
		{
			memset(&stCardDtls, 0x00, iSize);
		}

		iTotCnt = sizeof(cardRespLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "PAYMENT_MEDIA") == SUCCESS)
			{
				strncpy(stCardDtls.szPymtMedia, listPtr[iCnt].value, sizeof(stCardDtls.szPymtMedia) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CARDHOLDER") == SUCCESS)
			{
				strncpy(stCardDtls.szName, listPtr[iCnt].value, sizeof(stCardDtls.szName) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CVV2_CODE") == SUCCESS)
			{
				strncpy(stCardDtls.szCVVCode, listPtr[iCnt].value, sizeof(stCardDtls.szCVVCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "ACCT_NUM") == SUCCESS)
			{
				/* T_RaghavendranR1: Commented storing the ACCT_NUM if received from SSI_Response.
				 * It is noticed with Manual Entry for ADS Private Label Transaction we receive ACCT_NUM with asteriks(masked)
				 * So we are sending the masked EMBOSSED_ACCT_NUM to POS. Hence commented the below part*/
				if( (returnEmbossedNumForGiftType() && iPymtType == PYMT_GIFT) ||
						(returnEmbossedNumForPrivLblType() && iPymtType == PYMT_PRIVATE) )
				{
					/* Do not honor the ACCT_NUM Obtained From SSI_Response */
				}
				else
				{
					strncpy(stCardDtls.szPAN, listPtr[iCnt].value, sizeof(stCardDtls.szPAN) - 1);
				}
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "EMBOSSED_ACCT_NUM") == SUCCESS)
			{
				strncpy(stCardDtls.szEmbossedPAN, listPtr[iCnt].value, sizeof(stCardDtls.szEmbossedPAN) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PINLESSDEBIT") == SUCCESS)
			{
				memset(stCardDtls.szPINlessDebit, 0x00, sizeof(stCardDtls.szPINlessDebit));
				strncpy(stCardDtls.szPINlessDebit, listPtr[iCnt].value, sizeof(stCardDtls.szPINlessDebit) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CARD_ABBRV") == SUCCESS)
			{
				memset(stCardDtls.szCardAbbrv, 0x00, sizeof(stCardDtls.szCardAbbrv));
				strncpy(stCardDtls.szCardAbbrv, listPtr[iCnt].value, sizeof(stCardDtls.szCardAbbrv) - 1);
				jCnt++;

				debug_sprintf(szDbgMsg, "%s: Card Abbrvation from SSI Response [%s]", __FUNCTION__, stCardDtls.szCardAbbrv);
				APP_TRACE(szDbgMsg);

			}
			else if(iHostProtocol == HOST_PROTOCOL_UGP)
			{
				if(strcmp(listPtr[iCnt].key, "TKN_CNAME") == SUCCESS)
				{
					strncpy(stCardDtls.szName, listPtr[iCnt].value, sizeof(stCardDtls.szName) - 1);
					jCnt++;
				}
				else if(strcmp(listPtr[iCnt].key, "TKN_EXPDATE") == SUCCESS)
				{
					/* To Be Changed after verifying the ExpDate format of the UGP Response*/
					strncpy(stCardDtls.szExpMon, listPtr[iCnt].value, sizeof(stCardDtls.szExpMon));
					strncpy(stCardDtls.szExpYear, listPtr[iCnt].value, sizeof(stCardDtls.szExpYear));
					jCnt++;
				}
			}
		}

		if(jCnt > 0)
		{
			if(*dpstCardDtls == NULL)
			{
				/* Allocate memory to store the VSP response details for the
				 * transaction */
				*dpstCardDtls = (CARDDTLS_PTYPE) malloc(iSize);
				if(*dpstCardDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

			}

			memset(*dpstCardDtls, 0x00, iSize);
			memcpy(*dpstCardDtls, &stCardDtls, iSize);
		}

		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeDCCResFields
 *
 * Description	: Store the DCC response fields in the payment instancxe
 *
 * Input Params	: KEYVAL_PTYPE DCCDTLS_PTYPE *
 *
 * Output Params:
 * ============================================================================
 */
static int storeDCCResFields(KEYVAL_PTYPE listPtr, DCCDTLS_PTYPE * dpstDccDtls)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iSize			= 0;
	int				iTotCnt			= 0;
	DCCDTLS_STYPE	stDccDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DCCDTLS_STYPE);

	while(1)
	{
		/* If the card details is already present in the transaction
		 * structure then do not flush the already existing details */
		memset(&stDccDtls, 0x00, sizeof(DCCDTLS_STYPE));

		iTotCnt = sizeof(dccRespLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "EXCHANGE_RATE") == SUCCESS)
			{
				strncpy(stDccDtls.szExchngRate, listPtr[iCnt].value, sizeof(stDccDtls.szExchngRate) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "FGN_CURR_CODE") == SUCCESS)
			{
				strncpy(stDccDtls.szFgnCurCode, listPtr[iCnt].value, sizeof(stDccDtls.szFgnCurCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "FGN_AMOUNT") == SUCCESS)
			{
				strncpy(stDccDtls.szFgnAmount, listPtr[iCnt].value, sizeof(stDccDtls.szFgnAmount) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DCC_OFFERED") == SUCCESS)
			{
				stDccDtls.isDccEligible = (*((int *)listPtr[iCnt].value));
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "MARGINRATE_PERCENTAGE") == SUCCESS)
			{
				strncpy(stDccDtls.szMarginRatePercentage, listPtr[iCnt].value, sizeof(stDccDtls.szMarginRatePercentage) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "EXCHANGE_RATE_SOURCENAME") == SUCCESS)
			{
				strncpy(stDccDtls.szExchangeRateSouceName, listPtr[iCnt].value, sizeof(stDccDtls.szExchangeRateSouceName) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "COMMISSION_PERCENTAGE") == SUCCESS)
			{
				strncpy(stDccDtls.szCommissionPercentage, listPtr[iCnt].value, sizeof(stDccDtls.szCommissionPercentage) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "MINOR_UNITS") == SUCCESS)
			{
				stDccDtls.iMinorUnits = atoi((char *) listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "ALPHA_CURRENCY_CODE") == SUCCESS)
			{
				strncpy(stDccDtls.szAlphaCurrCode, listPtr[iCnt].value, sizeof(stDccDtls.szAlphaCurrCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CURRENCY_SYMBOL") == SUCCESS)
			{
				strncpy(stDccDtls.szCurrencySymbol, listPtr[iCnt].value, sizeof(stDccDtls.szCurrencySymbol) - 1);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDccDtls == NULL)
			{
				/* Allocate memory to store the VSP response details for the
				 * transaction */
				*dpstDccDtls = (DCCDTLS_PTYPE) malloc(iSize);
				if(*dpstDccDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

			}

			memset(*dpstDccDtls, 0x00, iSize);
			memcpy(*dpstDccDtls, &stDccDtls, iSize);
		}

		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeEmvKeyLoadDtls
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeEmvKeyLoadDtls(KEYVAL_PTYPE listPtr, EMVKEYLOAD_INFO_PTYPE * dpstEmvKeyLoadInfo)
{

	int						rv					= SUCCESS;
	int						iCnt				= 0;
	int						jCnt				= 0;
	int						iSize				= 0;	
	int						iTotCnt				= 0;
	EMVKEYLOAD_INFO_STYPE	stEmvKeyLoadInfo;	
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(EMVKEYLOAD_INFO_STYPE);	
	while(1)
	{
		/* If the EMV details is already present in the transaction
		 * structure then do not flush the already existing details */
		if(*dpstEmvKeyLoadInfo != NULL)
		{
			memcpy(&stEmvKeyLoadInfo, *dpstEmvKeyLoadInfo, iSize);
		}
		else
		{
			memset(&stEmvKeyLoadInfo, 0x00, iSize);
		}

		/* Store the EMV CAPK File Details */
		iCnt = storeCAPKDtls(listPtr, &stEmvKeyLoadInfo);
		iTotCnt += iCnt;
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to store EMV CAPK File Dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		listPtr += iCnt;

		/* Store the EMV MVT table Details */
		iCnt = storeMVTDtls(listPtr, &stEmvKeyLoadInfo);
		iTotCnt += iCnt;
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to store EMV MVT Table Dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		listPtr += iCnt;
		
		iCnt = sizeof(emvKeyLoadGenRespList) / KEYVAL_SIZE;
		iTotCnt += iCnt;
		
		int temp = iCnt;
		/* Populate the values */
		for(iCnt = 0; iCnt < temp; iCnt++)
		{	
			debug_sprintf(szDbgMsg,"%s: key[%s], value[%s]",
					__FUNCTION__, (char *)listPtr[iCnt].key, (char *)listPtr[iCnt].value);
			APP_TRACE(szDbgMsg);

			if(listPtr[iCnt].value == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Value is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);			
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "DOWNLOAD_STATUS") == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Storing DOWNLOAD_STATUS value", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				strncpy(stEmvKeyLoadInfo.szKeyLoadStatus, listPtr[iCnt].value, sizeof(stEmvKeyLoadInfo.szKeyLoadStatus) - 1);
				jCnt++;
			}
//9/8/16 - We are storing this in the Current Transaction Details instead of stEmvKeyLoadInfo.
#if 0
			else if(strcmp(listPtr[iCnt].key, "REFERENCE") == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Storing REFERENCE value", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				strncpy(stEmvKeyLoadInfo.szReference, listPtr[iCnt].value, sizeof(stEmvKeyLoadInfo.szReference) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TROUTD") == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Storing TROUTD value", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				strncpy(stEmvKeyLoadInfo.szTROUTD, listPtr[iCnt].value, sizeof(stEmvKeyLoadInfo.szTROUTD) - 1);
				jCnt++;
			}
#endif
		}

		if(jCnt > 0)
		{
			if(*dpstEmvKeyLoadInfo == NULL)
			{
				/* Allocate memory to store the EMV response details for the
				 * transaction */
				*dpstEmvKeyLoadInfo = (EMVKEYLOAD_INFO_PTYPE) malloc(iSize);
				if(*dpstEmvKeyLoadInfo == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

			}

			memset(*dpstEmvKeyLoadInfo, 0x00, iSize);
			memcpy(*dpstEmvKeyLoadInfo, &stEmvKeyLoadInfo, iSize);
		}
		
		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: storeCAPKDtls
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeCAPKDtls(KEYVAL_PTYPE listPtr, EMVKEYLOAD_INFO_PTYPE pstEmvKeyLoadInfo)
{

	int						rv					= SUCCESS;
	int						iCnt				= 0;
	int						jCnt				= 0;
	int						iSize				= 0;	
	int						iTotCnt				= 0;
	EMVKEYLOAD_INFO_STYPE	stEmvKeyLoadInfo;	
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(EMVKEYLOAD_INFO_STYPE);	
	while(1)
	{
		/* If the EMV details is already present in the transaction
		 * structure then do not flush the already existing details */
		if(pstEmvKeyLoadInfo != NULL)
		{
			memcpy(&stEmvKeyLoadInfo, pstEmvKeyLoadInfo, iSize);
		}
		else
		{
			memset(&stEmvKeyLoadInfo, 0x00, iSize);
		}
	
		iTotCnt = sizeof(emvCAPKRespList) / KEYVAL_SIZE;
		
		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{	
			if(listPtr[iCnt].value == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Value is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);			
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "PUBLIC_KEY") == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Storing PUBLIC_KEY tag values", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				jCnt += addEmvCAPKResp(&stEmvKeyLoadInfo, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(pstEmvKeyLoadInfo == NULL)
			{
				/* Allocate memory to store the EMV response details for the
				 * transaction */
				pstEmvKeyLoadInfo = (EMVKEYLOAD_INFO_PTYPE) malloc(iSize);
				if(pstEmvKeyLoadInfo == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

			}

			memset(pstEmvKeyLoadInfo, 0x00, iSize);
			memcpy(pstEmvKeyLoadInfo, &stEmvKeyLoadInfo, iSize);
		}
		
		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeMVTDtls
 *
 * Description	: Yet to be completed
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeMVTDtls(KEYVAL_PTYPE listPtr, EMVKEYLOAD_INFO_PTYPE pstEmvKeyLoadInfo)
{

	int						rv					= SUCCESS;
	int						iCnt				= 0;
	int						jCnt				= 0;
	int						iSize				= 0;	
	int						iTotCnt				= 0;
	EMVKEYLOAD_INFO_STYPE	stEmvKeyLoadInfo;	
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(EMVKEYLOAD_INFO_STYPE);	
	while(1)
	{
		/* If the EMV details is already present in the transaction
		 * structure then do not flush the already existing details */
		if(pstEmvKeyLoadInfo != NULL)
		{
			memcpy(&stEmvKeyLoadInfo, pstEmvKeyLoadInfo, iSize);
		}
		else
		{
			memset(&stEmvKeyLoadInfo, 0x00, iSize);
		}
	
		iTotCnt = sizeof(emvMVTRespList) / KEYVAL_SIZE;
		
		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{	
			if(listPtr[iCnt].value == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Value is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);			
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "FALLBACK_INDICATOR") == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Storing FALLBACK_INDICATOR value", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				jCnt += addEmvMVTResp(&stEmvKeyLoadInfo, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "OFFLINE_FLOOR_LIMIT") == SUCCESS)	//TODO: Check node
			{
				debug_sprintf(szDbgMsg, "%s: Storing OFFLINE_FLOOR_LIMIT value", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				jCnt += addEmvMVTResp(&stEmvKeyLoadInfo, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(pstEmvKeyLoadInfo == NULL)
			{
				/* Allocate memory to store the EMV response details for the
				 * transaction */
				pstEmvKeyLoadInfo = (EMVKEYLOAD_INFO_PTYPE) malloc(iSize);
				if(pstEmvKeyLoadInfo == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

			}

			memset(pstEmvKeyLoadInfo, 0x00, iSize);
			memcpy(pstEmvKeyLoadInfo, &stEmvKeyLoadInfo, iSize);
		}
		
		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeEmvRespDtls
 *
 * Description	: Yet to be completed
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeEmvRespDtls(KEYVAL_PTYPE listPtr, EMV_RESP_DTLS_PTYPE * dpstEmvRespDtls)
{
	int						rv				= SUCCESS;
	int						iCnt			= 0;
	int						jCnt			= 0;
	int						iSize			= 0;
	int						iTotCnt			= 0;
	char					szEmvResp[2048]	= "";
	char					szEmvUpdateReq[2]	= "";
	EMV_RESP_DTLS_STYPE		stEmvRespDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(EMV_RESP_DTLS_STYPE);

	while(1)
	{
	
		/* If the EMV details is already present in the transaction
		 * structure then do not flush the already existing details */
		if(*dpstEmvRespDtls != NULL)
		{
			memset(&stEmvRespDtls, 0x00, iSize);
			memcpy(&stEmvRespDtls, *dpstEmvRespDtls, iSize);
		}
		else
		{
			memset(&stEmvRespDtls, 0x00, iSize);
		}

//		iTotCnt += sizeof(emvGenRespList) / KEYVAL_SIZE;
		iTotCnt += sizeof(emvRespList) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s: KEY = [%s]", __FUNCTION__, listPtr[iCnt].key);
			APP_TRACE(szDbgMsg);
#endif
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "EMV_TAGS_RESPONSE") == SUCCESS)
			{
				memset(szEmvResp, 0x00, 2048);
				strcpy(szEmvResp, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "EMV_UPDATE") == SUCCESS)
			{
				memset(szEmvUpdateReq, 0x00, 2);
				strncpy(szEmvUpdateReq, listPtr[iCnt].value, 1);
				jCnt++;
			}
#if 0
			else if(strcmp(listPtr[iCnt].key, "TRANS_TIME") == SUCCESS)
			{
				strncpy(stEmvRespDtls.stEmvGenRespDtls.szTranTime, listPtr[iCnt].value, sizeof(stEmvRespDtls.stEmvGenRespDtls.szTranTime) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TRANS_DATE") == SUCCESS)
			{
				strncpy(stEmvRespDtls.stEmvGenRespDtls.szTranDate, listPtr[iCnt].value, sizeof(stEmvRespDtls.stEmvGenRespDtls.szTranDate) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PY_RESP_CODE") == SUCCESS)
			{
				strncpy(stEmvRespDtls.stEmvGenRespDtls.szPysRespCode, listPtr[iCnt].value, sizeof(stEmvRespDtls.stEmvGenRespDtls.szPysRespCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "RESP_SEQ_NUM") == SUCCESS)
			{
				strncpy(stEmvRespDtls.stEmvGenRespDtls.szRespSeqNum, listPtr[iCnt].value, sizeof(stEmvRespDtls.stEmvGenRespDtls.szRespSeqNum) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "BATCH_NUM") == SUCCESS)
			{
				strncpy(stEmvRespDtls.stEmvGenRespDtls.szBatchNum, listPtr[iCnt].value, sizeof(stEmvRespDtls.stEmvGenRespDtls.szBatchNum) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TRACE_CODE") == SUCCESS)
			{
				strncpy(stEmvRespDtls.stEmvGenRespDtls.szTraceCode, listPtr[iCnt].value, sizeof(stEmvRespDtls.stEmvGenRespDtls.szTraceCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "RESPONSE_CODE") == SUCCESS)
			{
				strncpy(stEmvRespDtls.stEmvGenRespDtls.szRespCode, listPtr[iCnt].value, sizeof(stEmvRespDtls.stEmvGenRespDtls.szRespCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "ISSUER_SCRIPT_IDENTIFIER") == SUCCESS)
			{
				strncpy(stEmvRespDtls.szIssuerScrptID, listPtr[iCnt].value, sizeof(stEmvRespDtls.szIssuerScrptID) - 1);
				jCnt++;
			}			
			else if(strcmp(listPtr[iCnt].key, "EMV_ISSUER_SCRIPT_DATA") == SUCCESS)
			{
				strncpy(stEmvRespDtls.szIssuerScrptData, listPtr[iCnt].value, sizeof(stEmvRespDtls.szIssuerScrptData) - 1);
				jCnt++;
				/*TODO: Since PAYware Transact server is not sending script id. 
						Temporarly copying from script data.First two bytes will be scrpt id.*/
				memset(stEmvRespDtls.szIssuerScrptID, 0x00, sizeof(stEmvRespDtls.szIssuerScrptID));
				memcpy(stEmvRespDtls.szIssuerScrptID, stEmvRespDtls.szIssuerScrptData, 2);
			}
			else if(strcmp(listPtr[iCnt].key, "AUTHORIZATION_RESPONSE_CODE") == SUCCESS)
			{					
				strncpy(stEmvRespDtls.szAuthRespCode, listPtr[iCnt].value, sizeof(stEmvRespDtls.szAuthRespCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "ISSUER_AUTHENTICATION_DATA") == SUCCESS)
			{
				strncpy(stEmvRespDtls.szIssuerAuthData, listPtr[iCnt].value, sizeof(stEmvRespDtls.szIssuerAuthData) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "ISSUER_SCRIPT_COMMAND") == SUCCESS)
			{
				strncpy(stEmvRespDtls.szIssuerScrptCmd, listPtr[iCnt].value, sizeof(stEmvRespDtls.szIssuerScrptCmd) - 1);
				jCnt++;
			}
			/* Add missing emv tags here later if any */
#endif
		}

		if(jCnt > 0)
		{
			if(*dpstEmvRespDtls == NULL)
			{
				/* Allocate memory to store the EMV response details for the
				 * transaction */
				*dpstEmvRespDtls = (EMV_RESP_DTLS_PTYPE) malloc(iSize);
				if(*dpstEmvRespDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

			}
			if(strlen(szEmvResp) > 0)
			{
				rv = parseAndStoreHostEMVResponse(&stEmvRespDtls, szEmvResp);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Unable to Parse Host EMV Response Tags", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}
			if(strlen(szEmvUpdateReq) > 0)
			{
				if(strcmp(szEmvUpdateReq, "Y") == 0 || strcmp(szEmvUpdateReq, "y") == 0)
				{
					stEmvRespDtls.bEmvUpdateReq = PAAS_TRUE;
				}
				else
				{
					stEmvRespDtls.bEmvUpdateReq = PAAS_FALSE;
				}
			}
			memset(*dpstEmvRespDtls, 0x00, iSize);
			memcpy(*dpstEmvRespDtls, &stEmvRespDtls, iSize);
		}

		rv = iTotCnt;
		break;
	}

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s: Got EMVResp %s len %d and value: %s", __FUNCTION__, "AllTags", stEmvRespDtls.iEMVRespTagsLen, stEmvRespDtls.szEMVRespTags);
	APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: Got EMVResp %s len %d and value: %s", __FUNCTION__, "szAuthRespCode", stEmvRespDtls.iAuthRespCodeLen, stEmvRespDtls.szAuthRespCode);
	APP_TRACE(szDbgMsg);
#endif
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeDupFieldDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeDupFieldDtls(KEYVAL_PTYPE listPtr, DUPFIELDDTLS_PTYPE * dpstDupFieldDtls)
{
	int					rv				= SUCCESS;
	int					iCnt			= 0;
	int					jCnt			= 0;
	int					iSize			= 0;
	int					iTotCnt			= 0;
	int					iHostProtocol	= getHostProtocolFormat();
	DUPFIELDDTLS_STYPE	stDupFieldDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DUPFIELDDTLS_STYPE);

	while(1)
	{
		/* If the amount details is already present in the transaction
		 * structure then do not flush the already existing details */
		if(*dpstDupFieldDtls != NULL)
		{
			memcpy(&stDupFieldDtls, *dpstDupFieldDtls, iSize);
		}
		else
		{
			memset(&stDupFieldDtls, 0x00, iSize);
		}

		iTotCnt = sizeof(dupRespLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{

			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "DUP_CTROUTD") == SUCCESS)
			{
				strncpy(stDupFieldDtls.szDupCTroutd, listPtr[iCnt].value, sizeof(stDupFieldDtls.szDupCTroutd) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DUP_AUTH_CODE") == SUCCESS)
			{
				strncpy(stDupFieldDtls.szDupAuthCode, listPtr[iCnt].value, sizeof(stDupFieldDtls.szDupAuthCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DUP_ACCT_NUM") == SUCCESS)
			{
				strncpy(stDupFieldDtls.szDupAccNum, listPtr[iCnt].value, sizeof(stDupFieldDtls.szDupAccNum) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DUP_PAYMENT_MEDIA") == SUCCESS)
			{
				strncpy(stDupFieldDtls.szDupPymtMedia, listPtr[iCnt].value, sizeof(stDupFieldDtls.szDupPymtMedia) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DUP_TROUTD") == SUCCESS)
			{
				strncpy(stDupFieldDtls.szDupTroutd, listPtr[iCnt].value, sizeof(stDupFieldDtls.szDupTroutd) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DUP_INVOICE") == SUCCESS)
			{
				strncpy(stDupFieldDtls.szDupInvoice, listPtr[iCnt].value, sizeof(stDupFieldDtls.szDupInvoice) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DUP_TRANS_AMOUNT") == SUCCESS)
			{
				strncpy(stDupFieldDtls.szDupTranAmount, listPtr[iCnt].value, sizeof(stDupFieldDtls.szDupTranAmount) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DUP_AVS_CODE") == SUCCESS)
			{
				strncpy(stDupFieldDtls.szDupAVSCode, listPtr[iCnt].value, sizeof(stDupFieldDtls.szDupAVSCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DUP_CVV2_CODE") == SUCCESS)
			{
				strncpy(stDupFieldDtls.szDupCVV2, listPtr[iCnt].value, sizeof(stDupFieldDtls.szDupCVV2) - 1);
				jCnt++;
			}
			else if(iHostProtocol == HOST_PROTOCOL_UGP)
			{
				if(strcmp(listPtr[iCnt].key, "DUP_TRANS_DATETIME") == SUCCESS)
				{
					/* KranthiK!:
					 * Format of the DUP_TRANS_DATETIME received from the host is MM/DD/YYYY HH:MM:SS
					 * So first copying the date to the target and then copying
					 */
					strncpy(stDupFieldDtls.szDupTransDate, listPtr[iCnt].value, strlen("MM/DD/YYYY"));
					strcpy(stDupFieldDtls.szDupTransTime, listPtr[iCnt].value + strlen("MM/DD/YYYY") + 1);
					jCnt++;
				}
			}
			else if(iHostProtocol == HOST_PROTOCOL_SSI)
			{
				if(strcmp(listPtr[iCnt].key, "DUP_TRANS_DATE") == SUCCESS)
				{
					strncpy(stDupFieldDtls.szDupTransDate, listPtr[iCnt].value, sizeof(stDupFieldDtls.szDupTransDate) - 1);
					jCnt++;
				}
				else if(strcmp(listPtr[iCnt].key, "DUP_TRANS_TIME") == SUCCESS)
				{
					strncpy(stDupFieldDtls.szDupTransTime, listPtr[iCnt].value, sizeof(stDupFieldDtls.szDupTransTime) - 1);
					jCnt++;
				}
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDupFieldDtls == NULL)
			{
				/* Allocate memory to store the current tran details for the
				 * transaction */
				*dpstDupFieldDtls = (DUPFIELDDTLS_PTYPE) malloc(iSize);
				if(*dpstDupFieldDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDupFieldDtls, 0x00, iSize);
			memcpy(*dpstDupFieldDtls, &stDupFieldDtls, iSize);
		}

		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: storeCurTranDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeCurTranDtls(KEYVAL_PTYPE listPtr, CTRANDTLS_PTYPE * dpstCTranDtl)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iSize			= 0;
	int				iTotCnt			= 0;
	int				iHostProtocol	= getHostProtocolFormat();
	CTRANDTLS_STYPE	stCTranDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(CTRANDTLS_STYPE);

	while(1)
	{
		/* If the amount details is already present in the transaction
		 * structure then do not flush the already existing details */
		if(*dpstCTranDtl != NULL)
		{
			memcpy(&stCTranDtls, *dpstCTranDtl, iSize);
		}
		else
		{
			memset(&stCTranDtls, 0x00, iSize);
		}

		iTotCnt = sizeof(pymtRespLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "TRANS_SEQ_NUM") == SUCCESS)
			{
				strncpy(stCTranDtls.szTranSeq, listPtr[iCnt].value, sizeof(stCTranDtls.szTranSeq) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "INTRN_SEQ_NUM") == SUCCESS)
			{
				strncpy(stCTranDtls.szTranId, listPtr[iCnt].value, sizeof(stCTranDtls.szTranId) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TROUTD") == SUCCESS)
			{
				strncpy(stCTranDtls.szTroutd, listPtr[iCnt].value, sizeof(stCTranDtls.szTroutd) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CTROUTD") == SUCCESS)
			{
				strncpy(stCTranDtls.szCTroutd, listPtr[iCnt].value, sizeof(stCTranDtls.szCTroutd) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AUTH_CODE") == SUCCESS)
			{
				strncpy(stCTranDtls.szAuthCode, listPtr[iCnt].value, sizeof(stCTranDtls.szAuthCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PAYMENT_TYPE") == SUCCESS)
			{
				strncpy(stCTranDtls.szPymntType, listPtr[iCnt].value, sizeof(stCTranDtls.szPymntType) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TRACE_NUM") == SUCCESS)
			{
				strncpy(stCTranDtls.szTraceNum, listPtr[iCnt].value, sizeof(stCTranDtls.szTraceNum) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "RETURN_CHECK_FEE") == SUCCESS)
			{
				strncpy(stCTranDtls.szRtnCheckFee, listPtr[iCnt].value, sizeof(stCTranDtls.szRtnCheckFee) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "RETURN_CHECK_NOTE") == SUCCESS)
			{
				strncpy(stCTranDtls.szRtnCheckNote, listPtr[iCnt].value, sizeof(stCTranDtls.szRtnCheckNote) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "ACK_REQUIRED") == SUCCESS)
			{
				stCTranDtls.iAckRequired = atoi(listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CHECK_NUM") == SUCCESS)
			{
				strncpy(stCTranDtls.szCheckNum, listPtr[iCnt].value, sizeof(stCTranDtls.szCheckNum) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "BANK_USERDATA") == SUCCESS)
			{
				/*if(!strcmp(pszProcessorId, "RCNASH"))
				{
					strcpy(stCTranDtls.szBankUserData, pstSAFRec->szPymtMedia);
				}*/
				strncpy(stCTranDtls.szBankUserData, listPtr[iCnt].value, sizeof(stCTranDtls.szBankUserData) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AUTH_RESP_CODE") == SUCCESS)
			{
				strncpy(stCTranDtls.szAuthRespCode, listPtr[iCnt].value, sizeof(stCTranDtls.szAuthRespCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TXN_POSENTRYMODE") == SUCCESS)
			{
				strncpy(stCTranDtls.szTxnPosEntryMode, listPtr[iCnt].value, sizeof(stCTranDtls.szTxnPosEntryMode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "MERCHID") == SUCCESS)
			{
				strncpy(stCTranDtls.szMerchId, listPtr[iCnt].value, sizeof(stCTranDtls.szMerchId) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TERMID") == SUCCESS)
			{
				strncpy(stCTranDtls.szTermId, listPtr[iCnt].value, sizeof(stCTranDtls.szTermId) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "LANE") == SUCCESS)
			{
				strncpy(stCTranDtls.szLaneId, listPtr[iCnt].value, sizeof(stCTranDtls.szLaneId) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "STORE_NUM") == SUCCESS)
			{
				strncpy(stCTranDtls.szStoreId, listPtr[iCnt].value, sizeof(stCTranDtls.szStoreId) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "BATCH_TRACE_ID") == SUCCESS)
			{
				strncpy(stCTranDtls.szBatchTraceId, listPtr[iCnt].value, sizeof(stCTranDtls.szBatchTraceId) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PAYPAL_CHECKID_KEY") == SUCCESS)
			{
				strncpy(stCTranDtls.szPaypalCheckIDKey, listPtr[iCnt].value, sizeof(stCTranDtls.szPaypalCheckIDKey) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AVS_CODE") == SUCCESS)
			{
				strncpy(stCTranDtls.szAvsCode, listPtr[iCnt].value, sizeof(stCTranDtls.szAvsCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PPCV") == SUCCESS)
			{
				strncpy(stCTranDtls.szPPCV, listPtr[iCnt].value, sizeof(stCTranDtls.szPPCV) - 1); //Not sure of length of this field, currently putting 20
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CREDIT_PLAN_NBR") == SUCCESS)
			{
				strncpy(stCTranDtls.szCreditPlanNbr, listPtr[iCnt].value, sizeof(stCTranDtls.szCreditPlanNbr) - 1); //Not sure of length of this field, currently putting 10
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "APR_TYPE") == SUCCESS)
			{
				strncpy(stCTranDtls.szAprType, listPtr[iCnt].value, sizeof(stCTranDtls.szAprType) - 1); 		//Added to PAYMENT = PRIVATE LABEL
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PURCHASE_APR") == SUCCESS)
			{
				strncpy(stCTranDtls.szPurchaseApr, listPtr[iCnt].value, sizeof(stCTranDtls.szPurchaseApr) - 1); 	//Added to PAYMENT = PRIVATE LABEL
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "SVC_PHONE") == SUCCESS)
			{
				strncpy(stCTranDtls.szSvcPhone, listPtr[iCnt].value, sizeof(stCTranDtls.szSvcPhone) - 1); 	//Added to PAYMENT = PRIVATE LABEL
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "RECEIPT_TEXT") == SUCCESS)
			{
				strncpy(stCTranDtls.szReceiptText, listPtr[iCnt].value, sizeof(stCTranDtls.szReceiptText) - 1); 	//Added to PAYMENT = PRIVATE LABEL
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "ACTION_CODE") == SUCCESS)
			{
				strncpy(stCTranDtls.szActionCode, listPtr[iCnt].value, sizeof(stCTranDtls.szActionCode) - 1); 	//Added to PAYMENT = PRIVATE LABEL
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "STATUS_FLAG") == SUCCESS)
			{
				strncpy(stCTranDtls.szStatusFlag, listPtr[iCnt].value, sizeof(stCTranDtls.szStatusFlag) - 1); 	//Added to PAYMENT = PRIVATE LABEL
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "REF_NUMBER") == SUCCESS)
			{
				strncpy(stCTranDtls.szReferenceNum, listPtr[iCnt].value, sizeof(stCTranDtls.szReferenceNum) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "VALIDATION_CODE") == SUCCESS)
			{
				strncpy(stCTranDtls.szValidtionCode, listPtr[iCnt].value, sizeof(stCTranDtls.szValidtionCode) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "VISA_IDENTIFIER") == SUCCESS)
			{
				strncpy(stCTranDtls.szVisaIdentifier, listPtr[iCnt].value, sizeof(stCTranDtls.szVisaIdentifier) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DENIAL_REC_NUM") == SUCCESS)
			{
				strncpy(stCTranDtls.szDenialRecNum, listPtr[iCnt].value, sizeof(stCTranDtls.szDenialRecNum) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "SIGNATUREREF") == SUCCESS)
			{
				strncpy(stCTranDtls.szSignatureRef, listPtr[iCnt].value, sizeof(stCTranDtls.szSignatureRef) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AUTHNWID") == SUCCESS)
			{
				strncpy(stCTranDtls.szAthNtwID, listPtr[iCnt].value, sizeof(stCTranDtls.szAthNtwID) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PROCESSOR") == SUCCESS)
			{
				strncpy(stCTranDtls.szProcessor, listPtr[iCnt].value, sizeof(stCTranDtls.szProcessor) - 1);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "REFERENCE") == SUCCESS)
			{
				strncpy(stCTranDtls.szReference, listPtr[iCnt].value, sizeof(stCTranDtls.szReference) - 1);
				jCnt++;
			}
			else if(iHostProtocol == HOST_PROTOCOL_UGP)
			{
				if(strcmp(listPtr[iCnt].key, "TRANS_DATETIME") == SUCCESS)
				{
					/* KranthiK!:
					 * Format of the TRANS_DATETIME received from the host is MM/DD/YYYY HH:MM:SS
					 * So first copying the date to the target and then copying
					 */
					strncpy(stCTranDtls.szTransDate, listPtr[iCnt].value, strlen("MM/DD/YYYY"));
					strcpy(stCTranDtls.szTransTime, listPtr[iCnt].value + strlen("MM/DD/YYYY") + 1);
					jCnt++;
				}
				else if(strcmp(listPtr[iCnt].key, "CMRCL_FLAG") == SUCCESS)
				{
					strncpy(stCTranDtls.szCmrclFlag, listPtr[iCnt].value, sizeof(stCTranDtls.szCmrclFlag) - 1);
					jCnt++;
				}
				else if(strcmp(listPtr[iCnt].key, "TKN_PAYMENT") == SUCCESS)
				{
					strncpy(stCTranDtls.szCardToken, listPtr[iCnt].value, sizeof(stCTranDtls.szCardToken) - 1);
					jCnt++;
				}
				else if(strcmp(listPtr[iCnt].key, "TKN_MATCHING") == SUCCESS)
				{
					/* Daivik : 25/1/2016 Coverity test 67192 Buffer Over Flow */
					strncpy(stCTranDtls.szLPToken, listPtr[iCnt].value, sizeof(stCTranDtls.szLPToken) - 1);
					jCnt++;
				}
			}
			else if (iHostProtocol == HOST_PROTOCOL_SSI)
			{
				if(strcmp(listPtr[iCnt].key, "TRANS_DATE") == SUCCESS)
				{
					strncpy(stCTranDtls.szTransDate, listPtr[iCnt].value, sizeof(stCTranDtls.szTransDate) - 1);
					jCnt++;
				}
				else if(strcmp(listPtr[iCnt].key, "TRANS_TIME") == SUCCESS)
				{
					strncpy(stCTranDtls.szTransTime, listPtr[iCnt].value, sizeof(stCTranDtls.szTransTime) - 1);
					jCnt++;
				}
				else if(strcmp(listPtr[iCnt].key, "CARD_TOKEN") == SUCCESS)
				{
					strncpy(stCTranDtls.szCardToken, listPtr[iCnt].value, sizeof(stCTranDtls.szCardToken) - 1);
					jCnt++;
				}
				else if(strcmp(listPtr[iCnt].key, "TA_TOKEN") == SUCCESS)
				{
					strncpy(stCTranDtls.szCardToken, listPtr[iCnt].value, sizeof(stCTranDtls.szCardToken) - 1);
					jCnt++;
				}
				else if(strcmp(listPtr[iCnt].key, "LPTOKEN") == SUCCESS)
				{
					if(strlen(stCTranDtls.szLPToken) == 0)
					{
						strncpy(stCTranDtls.szLPToken, listPtr[iCnt].value, sizeof(stCTranDtls.szLPToken) - 1);
					}
					jCnt++;
				}
				else if(strcmp(listPtr[iCnt].key, "TKN_MATCHING") == SUCCESS)
				{
					strncpy(stCTranDtls.szLPToken, listPtr[iCnt].value, sizeof(stCTranDtls.szLPToken) - 1);
					jCnt++;
				}
			}
		}

		if(jCnt > 0)
		{
			if(*dpstCTranDtl == NULL)
			{
				/* Allocate memory to store the current tran details for the
				 * transaction */
				*dpstCTranDtl = (CTRANDTLS_PTYPE) malloc(iSize);
				if(*dpstCTranDtl == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstCTranDtl, 0x00, iSize);
			memcpy(*dpstCTranDtl, &stCTranDtls, iSize);
		}

		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: checkDevResyncKey
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int checkDevResyncKey(KEYVAL_PTYPE listPtr)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (listPtr[0].value != NULL) &&
						(strcmp(listPtr[0].key, "RESYNC_KEY") == SUCCESS) )
		{
			debug_sprintf(szDbgMsg, "%s: Re-sync key present!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Store the device key in the configuration */
			rv = storeDeviceKey((char *)listPtr[0].value);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to store the resync devkey"
																, __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			/* Set the status of Admin request packet */
			setAdminPacketRequestStatus(PAAS_TRUE);

			/* Set the flag for device resync request in the
			* config.usr1 */
			setDevAdminRqd(PAAS_TRUE);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getRespKeysLstForAdmin
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getRespKeysLstForAdmin(META_PTYPE pstMeta, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iMandCnt		= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(genRespLst) / KEYVAL_SIZE;
		switch(iCmd)
		{
		case SSI_SETUP:

			iTotCnt += sizeof(adminRespLst) / KEYVAL_SIZE;

			break;

		case SSI_VERSION:

			iTotCnt += sizeof(versionRespList) / KEYVAL_SIZE;

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate the memory */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize and populate the memory assigned for the key list */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);

		iCnt = sizeof(genRespLst) / KEYVAL_SIZE;
		memcpy(listPtr, genRespLst, iCnt * KEYVAL_SIZE);

		switch(iCmd)
		{
		case SSI_SETUP:
			memcpy(listPtr + iCnt, adminRespLst, (iTotCnt - iCnt) * KEYVAL_SIZE);
			break;

		case SSI_VERSION:
			memcpy(listPtr + iCnt, versionRespList, (iTotCnt - iCnt) * KEYVAL_SIZE);
			break;
		}

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		/* Set the metadata */
		memset(pstMeta, 0x00, METADATA_SIZE);
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getRespKeysLstForPymt
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getRespKeysLstForPymt(META_PTYPE pstMeta, int iCmd)
{
	int						rv				= SUCCESS;
	int						iCnt			= 0;
	int						iMandCnt		= 0;
	int						iTotCnt			= 0;
	KEYVAL_PTYPE			listPtr			= NULL;
	KEYVAL_PTYPE			curPtr			= NULL;
	PASSTHRG_FIELDS_PTYPE	pstPassThrghDtls 	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/* MukeshS3: 15-April-16:
	 * Adding support for parsing of pass through fields dynamically.
	 * The list of pass through fields is being configured in a .INI file present at flash location.
	 * Need to add all those pass through response fields to the listPtr before parsing.
	 * First we will get the details of pass through fields
	 */
	pstPassThrghDtls = getPassThrgFieldsDtls();

	while(1)
	{
		iTotCnt = sizeof(resyncKeyList) / KEYVAL_SIZE;

		switch(iCmd)
		{
		case SSI_PREAUTH:
		case SSI_VOICEAUTH:
		case SSI_POSTAUTH:
		case SSI_SALE:
		case SSI_CREDIT:
		case SSI_REVERSAL:
		case SSI_VOID:
		case SSI_COMPLETION:
		case SSI_COMMERCIAL:
		case SSI_ACTIVATE:
		case SSI_ADDVAL:
		case SSI_BAL:
		case SSI_REG:
		case SSI_GIFTCLOSE:
		case SSI_DEACTIVATE:
		case SSI_REACTIVATE:
		case SSI_REMVAL:
		case SSI_SIGN:
		case SSI_EMVADMIN:
		case SSI_TOKENQUERY:
		case SSI_VERIFY:
		case SSI_ADDTIP:
		case SSI_RESETTIP:
		case SSI_ACK:
		case SSI_CHECKIN:
		case SSI_CHECKOUT:
		case SSI_TOR:
		case SSI_PAYACCOUNT:
		case SSI_QUERY:
		case SSI_CARDRATE:
			iTotCnt += sizeof(genRespLst) / KEYVAL_SIZE;

			if(iCmd != SSI_SIGN)
			{
				iTotCnt += sizeof(vspRespLst) / KEYVAL_SIZE;
				iTotCnt += sizeof(cardRespLst) / KEYVAL_SIZE;
				iTotCnt += sizeof(amtLst) / KEYVAL_SIZE;
				iTotCnt += sizeof(pymtRespLst) / KEYVAL_SIZE;
				/*
				* Praveen_P1: Adding only if EMV is enabled
				*/
				if(isEmvEnabledInDevice() == PAAS_TRUE)
				{
					iTotCnt += sizeof(emvCAPKRespList) / KEYVAL_SIZE;
					iTotCnt += sizeof(emvMVTRespList) / KEYVAL_SIZE;
					iTotCnt += sizeof(emvKeyLoadGenRespList) / KEYVAL_SIZE;
//					iTotCnt += sizeof(emvGenRespList) / KEYVAL_SIZE;
					iTotCnt += sizeof(emvRespList) / KEYVAL_SIZE;
				}
				iTotCnt += sizeof(dupRespLst)  / KEYVAL_SIZE;

				//DCC and pass through inside non-sig command
				iTotCnt += pstPassThrghDtls->iResTagsCnt;
				iTotCnt += sizeof(dccRespLst) / KEYVAL_SIZE;
			}
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Allocate memory for the key list */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize and set the key list */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		curPtr = listPtr;

		iCnt = sizeof(resyncKeyList) / KEYVAL_SIZE;
		memcpy(curPtr, resyncKeyList, iCnt * KEYVAL_SIZE);
		curPtr += iCnt;

		iCnt = sizeof(genRespLst) / KEYVAL_SIZE;
		memcpy(curPtr, genRespLst, iCnt * KEYVAL_SIZE);
		curPtr += iCnt;
		
		if(iCmd != SSI_SIGN)
		{
			iCnt = sizeof(vspRespLst) / KEYVAL_SIZE;
			memcpy(curPtr, vspRespLst, iCnt * KEYVAL_SIZE);
			curPtr += iCnt;

			iCnt = sizeof(cardRespLst) / KEYVAL_SIZE;
			memcpy(curPtr, cardRespLst, iCnt * KEYVAL_SIZE);
			curPtr += iCnt;

			iCnt = sizeof(amtLst) / KEYVAL_SIZE;
			memcpy(curPtr, amtLst, iCnt * KEYVAL_SIZE);
			curPtr += iCnt;

			iCnt = sizeof(pymtRespLst) / KEYVAL_SIZE;
			memcpy(curPtr, pymtRespLst, iCnt * KEYVAL_SIZE);
			curPtr += iCnt;

			/*
			 * Praveen_P1: Adding only if EMV is enabled
			 */
			if(isEmvEnabledInDevice() == PAAS_TRUE)
			{
				iCnt = sizeof(emvCAPKRespList) / KEYVAL_SIZE;
				memcpy(curPtr, emvCAPKRespList, iCnt * KEYVAL_SIZE);
				curPtr += iCnt;

				iCnt = sizeof(emvMVTRespList) / KEYVAL_SIZE;
				memcpy(curPtr, emvMVTRespList, iCnt * KEYVAL_SIZE);
				curPtr += iCnt;

				iCnt = sizeof(emvKeyLoadGenRespList) / KEYVAL_SIZE;
				memcpy(curPtr, emvKeyLoadGenRespList, iCnt * KEYVAL_SIZE);
				curPtr += iCnt;
/*
				iCnt = sizeof(emvGenRespList)/ KEYVAL_SIZE;
				memcpy(curPtr, emvGenRespList, iCnt * KEYVAL_SIZE);
				curPtr += iCnt;
*/
				iCnt = sizeof(emvRespList) / KEYVAL_SIZE;
				memcpy(curPtr, emvRespList, iCnt * KEYVAL_SIZE);
				curPtr += iCnt;
			}

			iCnt = sizeof(dupRespLst) / KEYVAL_SIZE;
			memcpy(curPtr, dupRespLst, iCnt * KEYVAL_SIZE);
			curPtr += iCnt;

			/* MukeshS3: 15-April-16:
			 * We have used the same key value pair structure KEYVAL_STYPE for pass through fields, which we uses for other XML tags.
			 * AjayS2: 17 May 2016 Moving here
			 */
			if(pstPassThrghDtls->iResTagsCnt > 0)
			{
				iCnt = pstPassThrghDtls->iResTagsCnt;
				memcpy(curPtr, pstPassThrghDtls->pstResTagList, iCnt * KEYVAL_SIZE);
				curPtr += iCnt;
			}

			iCnt = sizeof(dccRespLst) / KEYVAL_SIZE;
			memcpy(curPtr, dccRespLst, iCnt * KEYVAL_SIZE);
			curPtr += iCnt;
		}


		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		/* Set the meta data */
		memset(pstMeta, 0x00, METADATA_SIZE);
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getRespKeysLstForBatch
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getRespKeysLstForBatch(META_PTYPE pstMeta, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iMandCnt		= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	KEYVAL_PTYPE	curPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt = sizeof(resyncKeyList) / KEYVAL_SIZE;

		switch(iCmd)
		{
		case SSI_SETTLE:
			iTotCnt += sizeof(genRespLst) / KEYVAL_SIZE;

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate the memory */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize and populate the memory assigned for the key list */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		curPtr = listPtr;

		iCnt = sizeof(resyncKeyList) / KEYVAL_SIZE;
		memcpy(curPtr, resyncKeyList, iCnt * KEYVAL_SIZE);
		curPtr += iCnt;

		iCnt = sizeof(genRespLst) / KEYVAL_SIZE;
		memcpy(curPtr, genRespLst, iCnt * KEYVAL_SIZE);
		curPtr += iCnt;

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		/* Set the metadata */
		memset(pstMeta, 0x00, METADATA_SIZE);
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getRespKeysLstForRpt
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getRespKeysLstForRpt(META_PTYPE pstMeta, int iCmd)
{
	int				rv				= SUCCESS;
	int				iMandCnt		= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/*
		 * Praveen_P1: we need not parse the SSI response for the REPORT
		 * 			   command...so not adding any list to parse
		 */
#if 0
		if(iCmd == SSI_LPTQUERY)
		{
			iTotCnt = sizeof(resyncKeyList) / KEYVAL_SIZE;

			iTotCnt += sizeof(genRespLst) / KEYVAL_SIZE;

			iTotCnt += sizeof(pymtRespLst) / KEYVAL_SIZE;

			/* Allocate the memory */
			listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
			if(listPtr == NULL)
			{
				debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			/* Initialize and populate the memory assigned for the key list */
			memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
			curPtr = listPtr;

			iCnt = sizeof(resyncKeyList) / KEYVAL_SIZE;
			memcpy(curPtr, resyncKeyList, iCnt * KEYVAL_SIZE);
			curPtr += iCnt;

			iCnt = sizeof(genRespLst) / KEYVAL_SIZE;
			memcpy(curPtr, genRespLst, iCnt * KEYVAL_SIZE);

			iCnt = sizeof(pymtRespLst) / KEYVAL_SIZE;
			memcpy(curPtr, pymtRespLst, iCnt * KEYVAL_SIZE);

		}
#endif

#if 0
		iTotCnt = sizeof(resyncKeyList) / KEYVAL_SIZE;

		switch(iCmd)
		{
		case SSI_DAYSUMM:
		case SSI_PRESTL:
		case SSI_STLERR:
		case SSI_TRANSEARCH:
		case SSI_STLSUMM:
		case SSI_LASTTRAN:
		case SSI_DUPCHK:
			iTotCnt += sizeof(genRespLst) / KEYVAL_SIZE;
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate the memory */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize and populate the memory assigned for the key list */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		curPtr = listPtr;

		iCnt = sizeof(resyncKeyList) / KEYVAL_SIZE;
		memcpy(curPtr, resyncKeyList, iCnt * KEYVAL_SIZE);
		curPtr += iCnt;
		
		iCnt = sizeof(genRespLst) / KEYVAL_SIZE;
		memcpy(curPtr, genRespLst, iCnt * KEYVAL_SIZE);
#endif
		/* Set the metadata */
		memset(pstMeta, 0x00, METADATA_SIZE);
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getRespKeysLstForDEV
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getRespKeysLstForDev(META_PTYPE pstMeta, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iMandCnt		= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	KEYVAL_PTYPE	curPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		switch(iCmd)
		{
		case SSI_CREDITAPP:

			iTotCnt += sizeof(creditAppRespList) / KEYVAL_SIZE;

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Allocate memory for the key list */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize and set the key list */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		curPtr = listPtr;

		iCnt = sizeof(creditAppRespList) / KEYVAL_SIZE;
		memcpy(curPtr, creditAppRespList, iCnt * KEYVAL_SIZE);
		curPtr += iCnt;

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		/* Set the meta data */
		memset(pstMeta, 0x00, METADATA_SIZE);
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
* ============================================================================
* Function Name: getEmbossedPANFromTrackData
*
* Description       :
*
* Input Params      :
*
* Output Params:
* ============================================================================
*/
void getEmbossedPANFromTrackData(char *pszPAN, char *pszEmbossedPAN, PAAS_BOOL bManEntry)
{
	int			iLen							= 0;
	int			rvTemp							= SUCCESS;
	char*		cCurPtr                         = NULL;
	char* 		cNxtPtr                         = NULL;
	char		szPartA[5]						= "";
	char		szPartB[10]						= "";
	char		szPartC[5]						= "";
	char		szClearData[1024+1]				= "";
	char		szErrorBuf[256+1]					= "";
	char		szTrack2[100]					= "";
	PAAS_BOOL	bTrk2							= PAAS_FALSE;

#ifdef DEBUG
	char   szDbgMsg[256] = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * Please refer to UMF document about how to extract Embossed card number
	 * from the track2
	 * To construct the embossed number from Track 2 data
	 * take part (A) i.e. Partial serial number dropping the second digit
	 * Concatenate parts (B) i.e. Partial Account Number and (C) i.e. Complete checksum
	 */

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s:%s: PAN [%s]", DEVDEBUGMSG, __FUNCTION__, pszPAN);
	APP_TRACE(szDbgMsg);
#endif


	if(bManEntry)
	{
		debug_sprintf(szDbgMsg, "%s: Manual entry is set", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rvTemp = ProcessVSPCommand(VSP_MSG_PAN_CLEAR_REQUEST, pszPAN, szErrorBuf);
		strcpy(pszEmbossedPAN, pszPAN);

		debug_sprintf(szDbgMsg, "%s: ProcessVSPCommand Returning [%d]", __FUNCTION__, rvTemp);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		rvTemp = ProcessVSPCommand(VSP_MSG_CLEAR_REQUEST, szClearData, szErrorBuf);

		debug_sprintf(szDbgMsg, "%s: ProcessVSPCommand Returning [%d]", __FUNCTION__, rvTemp);
		APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s:%s: ClearData from VCL [%s]", DEVDEBUGMSG, __FUNCTION__, szClearData);
	APP_TRACE(szDbgMsg);
#endif

		if(strlen(szClearData) > 0)
		{
			cCurPtr = szClearData;

			/* Get the first track information */
			cNxtPtr = strchr(cCurPtr, FS);
			iLen = cNxtPtr - cCurPtr;
			if(iLen > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Track1 is present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Track1 is NOT present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			/* get the second track data */
			cCurPtr = cNxtPtr + 1;
			cNxtPtr = strchr(cCurPtr, FS);
			iLen = cNxtPtr - cCurPtr;
			if(iLen > 0)
			{
				memcpy(szTrack2, cCurPtr, iLen);

				bTrk2 = PAAS_TRUE;

				debug_sprintf(szDbgMsg, "%s: Track2 is present", __FUNCTION__);
				APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s: TRACK 2 DATA = [%s]", DEVDEBUGMSG,
						__FUNCTION__, szTrack2);
				APP_TRACE(szDbgMsg);
#endif
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Track2 is NOT present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			if(bTrk2)
			{
				cCurPtr = szTrack2+1;
				cNxtPtr = strchr(cCurPtr, '=');
				if(cNxtPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: No Separator in Track2 data", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					return;
				}

				if(strlen(cNxtPtr) >= 20)
				{
					/* store the partial account number */
					memset(szPartB, 0x00, sizeof(szPartB));

					cCurPtr = cCurPtr + 6; //It will be pointing to first byte of partial account data

					memcpy(szPartB, cCurPtr, 9); //partial account number would be 9 bytes

	#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: PartB [%s]", DEVDEBUGMSG, __FUNCTION__, szPartB);
	#else
					debug_sprintf(szDbgMsg,"%s: Retrieved PartB from the Track Data",__FUNCTION__);
	#endif
					APP_TRACE(szDbgMsg);

					/* store the Partial serial number first four bytes of discretionary data */
					memset(szPartA, 0x00, sizeof(szPartA));

					cCurPtr = cNxtPtr + 13; //It will be pointing to first byte of discretionary data

					memcpy(szPartA, cCurPtr, 4); //partial serial number would be 4 bytes

	#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: PartA [%s]", DEVDEBUGMSG, __FUNCTION__, szPartA);
	#else
					debug_sprintf(szDbgMsg,"%s: Retrieved PartA from the Track Data",__FUNCTION__);
	#endif
					APP_TRACE(szDbgMsg);

					cCurPtr = cCurPtr + 4;

					memset(szPartC, 0x00, sizeof(szPartC));

					memcpy(szPartC, cCurPtr, 4); //check sum would be 4 bytes

	#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: PartC [%s]", DEVDEBUGMSG, __FUNCTION__, szPartC);
	#else
					debug_sprintf(szDbgMsg,"%s: Retrieved PartC from the Track Data",__FUNCTION__);
	#endif
					APP_TRACE(szDbgMsg);

					sprintf(pszEmbossedPAN, "%c%c%c%s%s", szPartA[0], szPartA[2], szPartA[3], szPartB, szPartC);

	#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: Embossed Account Number [%s]", DEVDEBUGMSG, __FUNCTION__, pszEmbossedPAN);
	#else
					debug_sprintf(szDbgMsg,"%s: Retrieved Embossed from the Track Data", __FUNCTION__);
	#endif
					APP_TRACE(szDbgMsg);
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No Data", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

	}

	return;
}

/*
 * ============================================================================
 * Function Name: getDataFromVCL
 *
 * Description	: This function retrieves the clear pan from the VCL
 *
 * Input Params	: encrypted pan and the manual entry flag.
 *
 * Output Params:
 * ============================================================================
 */
int getDataFromVCL(char* szClearPAN, char *pszClearTrack1, char *pszClearTrack2,char *pszClearTrack3, PAAS_BOOL bManEntry)
{
	int			rv 					= SUCCESS;
	int			rvTemp				= SUCCESS;
	int			iLen				= 0;
	char		szClearData[1024+1]	= "";
	char		szErrorBuf[256+1]		= "";
	char		szTrack1[100]		= "";
	char		szTrack2[100]		= "";
	char		szTrack3[100]		= "";
	char		pszPAN[256]			= "";
	char*		nxtPtr				= NULL;
	char* 		curPtr				= NULL;
	char*		cCurPtr				= NULL;
	char*		cNxtPtr				= NULL;
	PAAS_BOOL 	bTrk1				= PAAS_FALSE;
	PAAS_BOOL	bTrk2				= PAAS_FALSE;
	PAAS_BOOL	bTrk3				= PAAS_FALSE;

#ifdef DEVDEBUG
	char		szDbgMsg[4096]		= "";
#elif DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Data Retrieving", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(bManEntry)
	{
		debug_sprintf(szDbgMsg, "%s: Manual entry is set", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rvTemp = ProcessVSPCommand(VSP_MSG_PAN_CLEAR_REQUEST, szClearPAN, szErrorBuf);

		debug_sprintf(szDbgMsg, "%s: ProcessVSPCommand Returning [%d]", __FUNCTION__, rvTemp);
		APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: Clear PAN: %s",  DEVDEBUGMSG, __FUNCTION__, szClearPAN);
		APP_TRACE(szDbgMsg);
#endif
	}
	else
	{
		rvTemp = ProcessVSPCommand(VSP_MSG_CLEAR_REQUEST, szClearData, szErrorBuf);

		debug_sprintf(szDbgMsg, "%s: ProcessVSPCommand Returning [%d]", __FUNCTION__, rvTemp);
		APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: Clear Data: %s",  DEVDEBUGMSG, __FUNCTION__, szClearData);
		APP_TRACE(szDbgMsg);
#endif
		if(strlen(szClearData) > 0)
		{
			curPtr = szClearData;

			/* Get the first track information */
			nxtPtr = strchr(curPtr, FS);
			iLen = nxtPtr - curPtr;
			if(iLen > 0)
			{
				//Copying after Removing End Sentinel. We were doing this towards the end, but moving it here to ensure we copy correct PAN
				memcpy(szTrack1, curPtr, iLen-1);
				bTrk1 = PAAS_TRUE;

				debug_sprintf(szDbgMsg, "%s: Track1 is present", __FUNCTION__);
				APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s: TRACK 1 DATA = [%s]", DEVDEBUGMSG,
						__FUNCTION__, szTrack1);
				APP_TRACE(szDbgMsg);
#endif
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Track1 is NOT present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			/* get the second track data */
			curPtr = nxtPtr + 1;
			nxtPtr = strchr(curPtr, FS);
			iLen = nxtPtr - curPtr;
			if(iLen > 0)
			{
				//Copying after Removing End Sentinel. We were doing this towards the end, but moving it here to ensure we copy correct PAN
				memcpy(szTrack2, curPtr, iLen-1);
				bTrk2 = PAAS_TRUE;

				debug_sprintf(szDbgMsg, "%s: Track2 is present", __FUNCTION__);
				APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s: TRACK 2 DATA = [%s]", DEVDEBUGMSG,
						__FUNCTION__, szTrack2);
				APP_TRACE(szDbgMsg);
#endif
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Track2 is NOT present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			/* get the third track data */
			curPtr = nxtPtr + 1;
			nxtPtr = strchr(curPtr, '?');
			iLen = nxtPtr - curPtr;
			if(iLen > 0)
			{
				//Copying after Removing End Sentinel. We were doing this towards the end, but moving it here to ensure we copy correct PAN
				memcpy(szTrack3, curPtr, iLen-1);
				bTrk3 = PAAS_TRUE;
				debug_sprintf(szDbgMsg, "%s: Track3 is present", __FUNCTION__);
				APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s: TRACK 3 DATA = [%s]", DEVDEBUGMSG,
						__FUNCTION__, szTrack3);
				APP_TRACE(szDbgMsg);
#endif
			}

			if(bTrk2) //Track 2 data
			{
				debug_sprintf(szDbgMsg, "%s: Retrieving PAN from Track2", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				cCurPtr = szTrack2+1;

				cNxtPtr = strchr(cCurPtr, '=');
				if(cNxtPtr == NULL)
				{
					/* FIXME: Currently done under assumption that some gift cards dont
					 * have the '=' sentinel in their track 2 */
					strncpy(pszPAN, cCurPtr, (sizeof(pszPAN) - 1));
				}
				else
				{
					memcpy(pszPAN, cCurPtr, cNxtPtr - cCurPtr);
				}
			}
			else if(bTrk1) //Track1 data
			{
				debug_sprintf(szDbgMsg, "%s: Retrieving PAN from Track1", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				cCurPtr = szTrack1+2;

				cNxtPtr = strchr(cCurPtr, '^');
				if(cNxtPtr == NULL)
				{
					/* FIXME: Currently done under assumption that some gift cards dont
					 * have the '^' sentinel in their track 1 information */
					strcpy(pszPAN, cCurPtr);
				}
				else
				{
					memcpy(pszPAN, cCurPtr, cNxtPtr - cCurPtr);
				}
			}
			else if(bTrk3) //Track3 data
			{
				debug_sprintf(szDbgMsg, "%s: Retrieving PAN from Track3", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				cCurPtr = szTrack3+3;

				cNxtPtr = strchr(cCurPtr, '=');
				if(cNxtPtr == NULL)
				{
					/* FIXME: Currently done under assumption that some gift cards dont
					 * have the '=' sentinel in their track 3 information */
					strncpy(pszPAN, cCurPtr, (sizeof(pszPAN) - 1));
				}
				else
				{
					memcpy(pszPAN, cCurPtr, cNxtPtr - cCurPtr);
				}
			}

			if(strlen(pszPAN) > 0)
			{
				strcpy(szClearPAN, pszPAN);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No Data", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		// MukeshS3: sending out all three clear tracks & let caller function decide which one to pick up
#if 0
		if((strlen(szTrack2) > 0) && (pszClearTrack != NULL))
		{
			strncpy(pszClearTrack, szTrack2+1, strlen(szTrack2) - 2);
		}
		else if((strlen(szTrack1) > 0) && (pszClearTrack != NULL))
		{
			/* if Card have only Track1 data, then we are sending Trakc1 data back in input buffer */
			strncpy(pszClearTrack, szTrack1+1, strlen(szTrack1) - 2);
		}
#endif
		if((strlen(szTrack1) > 0) && (pszClearTrack1 != NULL))
		{
			strncpy(pszClearTrack1, szTrack1+1, strlen(szTrack1) - 1);
		}
		if((strlen(szTrack2) > 0) && (pszClearTrack2 != NULL))
		{
			strncpy(pszClearTrack2, szTrack2+1, strlen(szTrack2) - 1);
		}
		if((strlen(szTrack3) > 0) && (pszClearTrack3 != NULL))
		{
			strncpy(pszClearTrack3, szTrack3+1, strlen(szTrack3) - 1);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: savePassThrgResFields
 *
 * Description	: This function is responsible for updating pass through response fields in data system
 *
 * Input Params	: KEYVAL_PTYPE
 *
 * Output Params: Number of total response fields
 * ============================================================================
 */
static int storePassThrgResFields(KEYVAL_PTYPE listPtr, PYMTTRAN_PTYPE *dpstPymtTran)
{
	int			iCnt 			= 0;
	int			iLen 			= 0;
	int			iTotCnt			= 0;
	void		*pTemp		= NULL;
	PASSTHRG_FIELDS_PTYPE	pstLocPassThrghDtls	 = NULL;
	PASSTHRG_FIELDS_PTYPE	pstTmpPassThrghDtls	 = NULL;
	KEYVAL_PTYPE			pstLocKeyValPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		pstLocPassThrghDtls = getPassThrgFieldsDtls();
		if(pstLocPassThrghDtls->pstResTagList == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pass Through fields has not been configured", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return SUCCESS;
		}

		iTotCnt = pstLocPassThrghDtls->iResTagsCnt;
		pstLocKeyValPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstLocKeyValPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iCnt = FAILURE;
			break;
		}
		memset(pstLocKeyValPtr, 0x00, iTotCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			pstLocKeyValPtr[iCnt].key = strdup(pstLocPassThrghDtls->pstResTagList[iCnt].key);
			pstLocKeyValPtr[iCnt].isMand = pstLocPassThrghDtls->pstResTagList[iCnt].isMand;
			pstLocKeyValPtr[iCnt].valType = pstLocPassThrghDtls->pstResTagList[iCnt].valType;

			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, pstLocKeyValPtr[iCnt].key) == SUCCESS)
			{
				iLen = strlen((char*)listPtr[iCnt].value);
				if(iLen > 0)
				{
					pTemp = (char*)malloc(iLen + 1);
					if(pTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						iCnt = FAILURE;
						break;
					}
					memset(pTemp, 0x00, iLen + 1);
					memcpy(pTemp, (char*)listPtr[iCnt].value, iLen);
				}
				pstLocKeyValPtr[iCnt].value = pTemp;
				pTemp = NULL;
			}
		}

#if 0
		for(iCnt = 0; iCnt < stPassThrgDtls.iResTagsCnt; iCnt++)
		{
			debug_sprintf(szDbgMsg, "%s: key[%s] val[%s] valType[%d] mandFlag[%d]", __FUNCTION__,
											(stPassThrgDtls.pstResTagList+iCnt)->key,
											(char*)(stPassThrgDtls.pstResTagList+iCnt)->value,
											(stPassThrgDtls.pstResTagList+iCnt)->valType,
											(stPassThrgDtls.pstResTagList+iCnt)->isMand
											);
			APP_TRACE(szDbgMsg);
		}
#endif
		break;
	}

	if(iCnt > 0)		// SUCCESS
	{
		if((*dpstPymtTran)->pstPassThrghDtls == NULL)
		{
			// Create a place holder for Pass through fields in payment transaction
			pstTmpPassThrghDtls = (PASSTHRG_FIELDS_PTYPE) malloc(sizeof(PASSTHRG_FIELDS_STYPE));
			if(pstTmpPassThrghDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(pstLocKeyValPtr != NULL) // T_raghavendranR1 CID 84335 (#1 of 1): Resource leak (RESOURCE_LEAK)
				{
					while(iCnt)
					{
						free(pstLocKeyValPtr[iCnt - 1].key);
						free(pstLocKeyValPtr[iCnt - 1].value);
						iCnt--;
					}
					free(pstLocKeyValPtr);
				}
				iCnt = FAILURE;
			}
			else
			{
				/*AjayS2 25 Aug 2016 - Coverity - 83377 -  If malloc failed, pstTmpPassThrghDtls will be NULL so we cannot memset and assign any data
				 * Moving this part in else case.
				 */
				memset(pstTmpPassThrghDtls, 0x00, sizeof(PASSTHRG_FIELDS_STYPE));
				pstTmpPassThrghDtls->iResTagsCnt = iTotCnt;
				pstTmpPassThrghDtls->pstResTagList = pstLocKeyValPtr;
			}
			(*dpstPymtTran)->pstPassThrghDtls = pstTmpPassThrghDtls;
		}
		else
		{
			(*dpstPymtTran)->pstPassThrghDtls->iResTagsCnt = iTotCnt;
			(*dpstPymtTran)->pstPassThrghDtls->pstResTagList = pstLocKeyValPtr;
		}
	}
	else
	{
		if(pstLocKeyValPtr != NULL) // T_raghavendranR1 CID 83354 (#1 of 1): Dereference after null check (FORWARD_NULL)
		{
			while(iCnt)
			{
				free(pstLocKeyValPtr[iCnt - 1].key);
				free(pstLocKeyValPtr[iCnt - 1].value);
				iCnt--;
			}
			free(pstLocKeyValPtr);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);
	return iCnt;
}

#if 0
/*
 * ============================================================================
 * Function Name: getTrackDataFromVCL
 *
 * Description	: This function retrieves the clear track data from the VCL
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getTrackDataFromVCL(char* pszClearTrack1, char *pszClearTrack2, char *pszClearTrack3)
{
	int			rv 					= SUCCESS;
	int			rvTemp				= SUCCESS;
	int			iLen				= 0;
	char		szErrorBuf[256+1]	= "";
	char		szClearData[1024+1]	= "";
	char		szTrack1[100]		= "";
	char		szTrack2[100]		= "";
	char		szTrack3[100]		= "";
	char*		nxtPtr				= NULL;
	char* 		curPtr				= NULL;

#ifdef DEVDEBUG
	char		szDbgMsg[4096]		= "";
#elif DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rvTemp = ProcessVSPCommand(VSP_MSG_CLEAR_REQUEST, szClearData, szErrorBuf);

	debug_sprintf(szDbgMsg, "%s: ProcessVSPCommand Returning [%d]", __FUNCTION__, rvTemp);
	APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s:%s: Clear Data: %s",  DEVDEBUGMSG, __FUNCTION__, szClearData);
	APP_TRACE(szDbgMsg);
#endif
	if(strlen(szClearData) > 0)
	{
		curPtr = szClearData;

		/* Get the first track information */
		nxtPtr = strchr(curPtr, FS);
		iLen = nxtPtr - curPtr;
		if(iLen > 0)
		{
			memcpy(szTrack1, curPtr, iLen);

			debug_sprintf(szDbgMsg, "%s: Track1 is present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s:%s: TRACK 1 DATA = [%s]", DEVDEBUGMSG,
					__FUNCTION__, szTrack1);
			APP_TRACE(szDbgMsg);
#endif
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Track1 is NOT present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* get the second track data */
		curPtr = nxtPtr + 1;
		nxtPtr = strchr(curPtr, FS);
		iLen = nxtPtr - curPtr;
		if(iLen > 0)
		{
			memcpy(szTrack2, curPtr, iLen);

			debug_sprintf(szDbgMsg, "%s: Track2 is present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s:%s: TRACK 2 DATA = [%s]", DEVDEBUGMSG,
					__FUNCTION__, szTrack2);
			APP_TRACE(szDbgMsg);
#endif
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Track2 is NOT present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* get the third track data */
		curPtr = nxtPtr + 1;
		nxtPtr = strchr(curPtr, FS);
		iLen = nxtPtr - curPtr;
		if(iLen > 0)
		{
			memcpy(szTrack3, curPtr, iLen);

			debug_sprintf(szDbgMsg, "%s: Track3 is present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s:%s: TRACK 3 DATA = [%s]", DEVDEBUGMSG,
					__FUNCTION__, szTrack3);
			APP_TRACE(szDbgMsg);
#endif
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No Data", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if((strlen(szTrack1) > 0) && (pszClearTrack1 != NULL))
	{
		strncpy(pszClearTrack1, szTrack1+1, strlen(szTrack1) - 2);
	}
	if((strlen(szTrack2) > 0) && (pszClearTrack2 != NULL))
	{
		strncpy(pszClearTrack2, szTrack2+1, strlen(szTrack2) - 2);
	}
	if((strlen(szTrack3) > 0) && (pszClearTrack3 != NULL))
	{
		strncpy(pszClearTrack3, szTrack3+1, strlen(szTrack3) - 2);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * End of file ssiResDataCtrl.c
 * ============================================================================
 */
