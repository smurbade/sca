
/******************************************************************
*                     ssiConfigParams.c                           *
*******************************************************************
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis which loads and reads the 		  *
* 			   config parameters required for the SSI Module       *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                kranthik1                                        *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <svc.h>
#include <sys/stat.h>

#include "common/utils.h"
#include "ssi/ssiHostCfg.h"
#include "ssi/ssiCfgDef.h"
#include "uiAgent/uiApis.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

#define putEnvFile(dest, param, paramVal)		scaPutEnvFile(dest, param, paramVal);
#define getEnvFile(dest, param, paramVal, iLen)	scaGetEnvFile(dest, param, paramVal, iLen);

extern PAAS_BOOL isEmvEnabledInDevice();

static SSI_CFG_STYPE ssiSettings;

extern int scaGetEnvFile(char* , char* , char* , int );
extern int scaPutEnvFile(char* , char* , char*);
static void initializeSSISettings();
static int 	loadMerchantSettings();
static int  getHostDefinition();
static int  patchCompatibilityCode();
static int  getBapiHostDefinition();
void setDemoModeParamValue(PAAS_BOOL);

/*
 * ============================================================================
 * Function Name: loadSSIConfigParams
 *
 * Description	: This function reads all the config parameters from the
 * 				  config.ur1 file and store them in the static structure.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int	loadSSIConfigParams()
{
	int		rv				= 0;
	int		iLen			= 0;
	int		iVal			= 0;
	int		iValLen			= 0;
	char	szTemp[20]		= "";
	char    szVal[20]		= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	initializeSSISettings();

	iLen = sizeof(szTemp);
	iValLen = sizeof(szVal);

	ssiSettings.devModel = svcInfoPlatform(7);

	/* ------------ Set the device serial number ----------- */

	getDevSrlNum(ssiSettings.szDevSrlNo);


	/* ------------ Set the device type --------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, DEVTYPE_SUFFIX, szTemp, iLen);
	if(rv > 0)
	{
		if(strlen(szTemp) > 2)
		{
			szTemp[2] = '\0';
		}

		sprintf(ssiSettings.szDevType, "Mx%d%s", ssiSettings.devModel,
																	szTemp);
	}
	else
	{
		sprintf(ssiSettings.szDevType, "Mx%d", ssiSettings.devModel);
	}

	debug_sprintf(szDbgMsg, "%s: Device Type = %s", __FUNCTION__,
													ssiSettings.szDevType);
	APP_TRACE(szDbgMsg);

	/* ------------- Check for demo mode settings ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, DEMO_MODE, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Paas Demo mode as TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			ssiSettings.demoMode = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting Paas Demo mode as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			ssiSettings.demoMode = PAAS_FALSE;
		}
	}
	else
	{
		/* No value found for PaaS demo mode parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for demo;Setting FALSE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		ssiSettings.demoMode = PAAS_FALSE;
	}

	/* ------------ Check for the Host Format UGP or SSI --------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, HOST_PROTOCOL_FORMAT, szTemp, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Host Support [%s] Protocol Format", __FUNCTION__, szTemp);
		APP_TRACE(szDbgMsg);

		if(strcasecmp(szTemp, "UGP") == SUCCESS)
		{
			ssiSettings.ihostProtocol = HOST_PROTOCOL_UGP;
		}
		else
		{
			ssiSettings.ihostProtocol = HOST_PROTOCOL_SSI;
		}
		rv = SUCCESS;
	}
	else
	{
		/* Set the default Host Protocol as SSI */
		ssiSettings.ihostProtocol = HOST_PROTOCOL_SSI;
		/* Update the config.usr1 */
		putEnvFile(SECTION_DEVICE, HOST_PROTOCOL_FORMAT, "SSI");
		debug_sprintf(szDbgMsg, "%s: Default Host Protocol not present, setting SSI as default", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = SUCCESS;
	}

	/* ------------ Check for the Host Format UGP or SSI --------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, PA_PYMT_TYPE_FORMAT, szTemp, iLen);
	if(rv > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Host Support [%s] Protocol Format for PAYACCOUNT", __FUNCTION__, szTemp);
		APP_TRACE(szDbgMsg);

		if(strcasecmp(szTemp, "UGP") == SUCCESS)
		{
			ssiSettings.iPAPymtProtocol = HOST_PROTOCOL_UGP;
		}
		else if(strcasecmp(szTemp, "SSI") == SUCCESS)
		{
			ssiSettings.iPAPymtProtocol = HOST_PROTOCOL_SSI;
		}
		else
		{
			/* Update the config.usr1 */
			putEnvFile(SECTION_DEVICE, PA_PYMT_TYPE_FORMAT, "SSI");

			debug_sprintf(szDbgMsg, "%s: Invalid Format is set, setting SSI as default", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			ssiSettings.iPAPymtProtocol = HOST_PROTOCOL_SSI;
		}
		rv = SUCCESS;
	}
	else
	{
		/* Set the default Host Protocol as SSI */
		ssiSettings.iPAPymtProtocol = HOST_PROTOCOL_SSI;
		/* Update the config.usr1 */
		putEnvFile(SECTION_DEVICE, PA_PYMT_TYPE_FORMAT, "SSI");
		debug_sprintf(szDbgMsg, "%s: Default PAYACCOUNT Protocol not present, setting SSI as default", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = SUCCESS;
	}

	/* ------------- Check for keep Alive settings ------------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_HOST, KEEP_ALIVE, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		ssiSettings.iKeepAlive = KEEP_ALIVE_DISABLED;
		ssiSettings.iKeepAliveInt = 0;
		if((iVal >= KEEP_ALIVE_DISABLED) && (iVal <= KEEP_ALIVE_DEFINED_TIME))
		{
			ssiSettings.iKeepAlive = iVal;
		}
		else
		{
			/* keep Alive parameter value not in range*/
			debug_sprintf(szDbgMsg,"%s: No value found for Keep Alive parameter, Setting Default - Disabled", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			ssiSettings.iKeepAlive = KEEP_ALIVE_DISABLED;
			memset(szVal, 0x00, iValLen);
			sprintf(szVal, "%d", KEEP_ALIVE_DISABLED);
			putEnvFile(SECTION_HOST, KEEP_ALIVE, szVal);
			rv = SUCCESS;
		}

		if(ssiSettings.iKeepAlive == 2)
		{
			memset(szTemp, 0x00, iLen);
			rv = getEnvFile(SECTION_HOST, KEEP_ALIVE_INTERVAL, szTemp, iLen);
			if(rv > 0)
			{
				iVal = atoi(szTemp);
				if( (iVal >= 1) && (iVal <= 60))
				{
					ssiSettings.iKeepAliveInt = iVal;
				}
				else
				{
					/* No value found for keep Alive Interval parameter */
					debug_sprintf(szDbgMsg,"%s: Value found for Keep Alive Interval parameter is not in range ;Setting to Default", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					ssiSettings.iKeepAliveInt = DFLT_KEEP_ALIVE_INTERVAL;
					memset(szVal, 0x00, iValLen);
					sprintf(szVal, "%d", DFLT_KEEP_ALIVE_INTERVAL);
					putEnvFile(SECTION_HOST, KEEP_ALIVE_INTERVAL, szVal);
				}
			}
			else
			{
				/* No value found for keep Alive Interval parameter */
				debug_sprintf(szDbgMsg,"%s: No value found for Keep Alive Interval parameter, Setting to Default Value", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				ssiSettings.iKeepAliveInt = DFLT_KEEP_ALIVE_INTERVAL;
				memset(szVal, 0x00, iValLen);
				sprintf(szVal, "%d", DFLT_KEEP_ALIVE_INTERVAL);
				putEnvFile(SECTION_HOST, KEEP_ALIVE_INTERVAL, szVal);
			}
		}
		rv = SUCCESS;
	}
	else
	{
		/* No value found for keep Alive parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for Keep Alive parameter, Setting to Default - Disabled", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		ssiSettings.iKeepAlive = KEEP_ALIVE_DISABLED;
		memset(szVal, 0x00, iValLen);
		sprintf(szVal, "%d", KEEP_ALIVE_DISABLED);
		putEnvFile(SECTION_HOST, KEEP_ALIVE, szVal);
		rv = SUCCESS;
	}

	/* Check if host ping is required before the transaction data is sent to
	 * the PWC host. Part of temporary fix for Issue: 1659 */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, "hostpingreqd", szTemp, iLen);
	if(rv > 0)
	{
		switch(szTemp[0])
		{
		case '0':
		case 'N':
		case 'n':
			ssiSettings.hostPingReqd = PAAS_FALSE;
			break;

		case '1':
		case 'Y':
		case 'y':
			ssiSettings.hostPingReqd = PAAS_TRUE;
			break;

		default:
			/* Keeping default as FALSE */
			ssiSettings.hostPingReqd = PAAS_FALSE;
			putEnvFile(SECTION_DEVICE, "hostpingreqd", "N");
			break;
		}

		rv = SUCCESS;
	}
	else
	{
		/* Keeping default as FALSE for now */
		ssiSettings.hostPingReqd = PAAS_FALSE;
		putEnvFile(SECTION_DEVICE, "hostpingreqd", "N");
		rv = SUCCESS;
	}

	/* Check if ping test is required during the preamble to check
	 * the network settings are configured correctly */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, "preamblepingreqd", szTemp, iLen);
	if(rv > 0)
	{
		switch(szTemp[0])
		{
		case '0':
		case 'N':
		case 'n':
			ssiSettings.preamblePingReqd = PAAS_FALSE;
			break;

		case '1':
		case 'Y':
		case 'y':
			ssiSettings.preamblePingReqd = PAAS_TRUE;
			break;

		default:
			/* Keeping default as TRUE */
			ssiSettings.preamblePingReqd = PAAS_TRUE;
			putEnvFile(SECTION_DEVICE, "preamblepingreqd", "Y");
			break;
		}

		rv = SUCCESS;
	}
	else
	{
		/* Keeping default as TRUE*/
		ssiSettings.preamblePingReqd = PAAS_TRUE;
		putEnvFile(SECTION_DEVICE, "preamblepingreqd", "Y");
		rv = SUCCESS;
	}

	/* ----------------- Check if DHI is enabled ----------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, DHI_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting DHI Enabled as  TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			ssiSettings.dhiEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting dhienabled as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			ssiSettings.dhiEnabled = PAAS_FALSE;
		}
		rv = SUCCESS;
	}
	else
	{
		/* No value found for DHI enabled parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for dhienabled; Set FALSE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		ssiSettings.dhiEnabled = PAAS_FALSE;
	}

	/* ----------------- Check if CheckHostConnStatus is enabled ----------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, CHECK_HOST_CONN_STATUS, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting CHECK_HOST_CONN_STATUS  as  TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			ssiSettings.chkHostConnStatus = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting CHECK_HOST_CONN_STATUS as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			ssiSettings.chkHostConnStatus = PAAS_FALSE;
		}
		rv = SUCCESS;
	}
	else
	{
		/* No value found for DHI enabled parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for checkhostconnstatus; Set FALSE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		ssiSettings.chkHostConnStatus = PAAS_FALSE;
	}
	/* ----------------- Check if SAF On response Timeout Configuration is enabled ----------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, SAF_ON_RESP_TIMEOUT, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		if(iVal < 0 || iVal > 10)
		{
			/* Incorrect value found for SAF On Response Timeout parameter */
			debug_sprintf(szDbgMsg,"%s: Incorrect Value for Saf On Response Timeout, set Default 0",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			ssiSettings.iSAFOnRespTimeout = 0;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: Value for SAF on Response Timeout :%d",__FUNCTION__,iVal);
			APP_TRACE(szDbgMsg);
			ssiSettings.iSAFOnRespTimeout = iVal;
		}
		rv = SUCCESS;
	}
	else
	{
		/* No value found for SAF On Response Timeout parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for Saf On Response Timeout, set Default 0",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		ssiSettings.iSAFOnRespTimeout = 0;
	}

	/* ----------------- Check if 'Switch On Primary URL after X tranx' Configuration is enabled ----------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, SWITCH_TO_PRIMURL_AFTER_X_TRAN, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		if(iVal < 0 || iVal > 100)
		{
			/* Incorrect value found for 'Switch On Primary URL after X tranx' parameter */
			debug_sprintf(szDbgMsg,"%s: Incorrect Value for 'Switch On Primary URL after X tranx', set Default 0",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			ssiSettings.iSwitchOnPrimUrlAfterXTran = 0;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: Value for 'Switch On Primary URL after X tranx' :%d",__FUNCTION__,iVal);
			APP_TRACE(szDbgMsg);
			ssiSettings.iSwitchOnPrimUrlAfterXTran = iVal;
		}
		rv = SUCCESS;
	}
	else
	{
		/* No value found for 'Switch On Primary URL after X tranx' parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for 'Switch On Primary URL after X tranx', set Default 0",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		ssiSettings.iSwitchOnPrimUrlAfterXTran = 0;
	}

	/* ----------------- Check if 'Switch URL after X response timeout' Configuration is enabled ----------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, TOGGLE_URL_AFTER_X_RESP_TIMEOUT, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		if(iVal < 0 || iVal > 20)
		{
			/* Incorrect value found for 'Switch URL after X response timeout' parameter */
			debug_sprintf(szDbgMsg,"%s: Incorrect Value for 'Switch URL after X response timeout', set Default 0",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			ssiSettings.iSwitchUrlAfterXRespTimeout = 0;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: Value for 'Switch URL after X response timeout' :%d",__FUNCTION__,iVal);
			APP_TRACE(szDbgMsg);
			ssiSettings.iSwitchUrlAfterXRespTimeout = iVal;
		}
		rv = SUCCESS;
	}
	else
	{
		/* No value found for 'Switch URL after X response timeout' parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for 'Switch URL after X response timeout', set Default 0",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		ssiSettings.iSwitchUrlAfterXRespTimeout = 0;
	}

	/* ----------------- Check if Bapi is enabled ----------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, BAPI_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'Y') || (*szTemp == 'y'))
		{
			debug_sprintf(szDbgMsg, "%s: Setting Bapi Enabled as  TRUE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			ssiSettings.bapiEnabled = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Setting bapienabled as FALSE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			ssiSettings.bapiEnabled = PAAS_FALSE;
		}
		rv = SUCCESS;
	}
	else
	{
		/* No value found for PaaS bapi enabled parameter */
		debug_sprintf(szDbgMsg,"%s: No value found for bapienabled; Set FALSE",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		ssiSettings.bapiEnabled = PAAS_FALSE;
	}

	/* ----------------- Getting the bapi destination id ----------------- */
	if(isBapiEnabled())
	{
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_HOST, BAPI_DEST_ID, szTemp, iLen);
		if(rv > 0)
		{
			ssiSettings.bapiDestId = atoi(szTemp);
			rv = SUCCESS;
		}
		else
		{
			/* No value found for PaaS bapi enabled parameter */
			debug_sprintf(szDbgMsg,"%s: No value found for bapi destination id; Set to 1",
									__FUNCTION__);
			APP_TRACE(szDbgMsg);

			ssiSettings.bapiDestId = 1;
			putEnvFile(SECTION_HOST, BAPI_DEST_ID, BAPI_DEST_ID_DEF);
		}
	}

	if((rv == SUCCESS))
	{
		/* Get PWC host urls and other communication parameters */
		rv = getHostDefinition();
		/* If the device is running in demo mode, Host Definition Parameters are not Mandatory
		 * So No need to Fail From here. */
		if(rv == SUCCESS || isDemoModeEnabled())
		{
			rv = loadMerchantSettings();
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error in loading Merchant settings"
																, __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error in loading PWC Host settings",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}
	/* By Default the Number of Hosts available will be 2. But incase
	 * both the URL's are the same, then we set iNumUniqueHosts = 1.
	 * During Posting the data, if both URL's are same then we will
	 * avoid unnecessarily posting to same URL Twice.
	 */
	ssiSettings.iNumUniqueHosts = 2;
	if(ssiSettings.hostDef[0].url && ssiSettings.hostDef[1].url )
	{
		if(!strcmp(ssiSettings.hostDef[0].url,ssiSettings.hostDef[1].url))
		{
			debug_sprintf(szDbgMsg, "%s: Both URL's match %s , So 1 Unique Host",__FUNCTION__,ssiSettings.hostDef[0].url);
			APP_TRACE(szDbgMsg);
			ssiSettings.iNumUniqueHosts = 1;
		}
	}
	debug_sprintf(szDbgMsg, "%s: SSI Num Hosts:%d",__FUNCTION__,ssiSettings.iNumUniqueHosts);
	APP_TRACE(szDbgMsg);
	/*Reading and setting the bapi urls*/

	if (rv == SUCCESS && isBapiEnabled())
	{
		/* Get Bapi host urls and other communication parameters */
		rv = getBapiHostDefinition();
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in getting the bapi host definition",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: initializeSSISettings
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
static void initializeSSISettings()
{
	if(ssiSettings.stMcntCfgPtr != NULL)
	{
		free(ssiSettings.stMcntCfgPtr);
	}

	memset(&ssiSettings, 0x00, sizeof(SSI_CFG_STYPE));

	return;
}

/*
 * ============================================================================
 * Function Name: isHostPingReqd
 *
 * Description	: This function would get the display prompt demanded by the
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
PAAS_BOOL isHostPingReqd()
{
	if(ssiSettings.hostPingReqd == PAAS_TRUE)
	{
		return PAAS_TRUE;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/*
 * ============================================================================
 * Function Name: getBAPIHostDetails
 *
 * Description	: This function would get the display prompt demanded by the
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================*/

int getBAPIHostDetails(HOST_URL_ENUM hostType, HOSTDEF_PTR_TYPE hostDefPtr)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(hostDefPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No placeholder provided for host details",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		memset(hostDefPtr, 0x00, sizeof(HOSTDEF_STRUCT_TYPE));

		switch(hostType)
		{
		case PRIMARY_URL:
			debug_sprintf(szDbgMsg, "%s: Getting PRIMARY BAPI Host details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memcpy(hostDefPtr, &(ssiSettings.bapiHostDef[0]),
												sizeof(HOSTDEF_STRUCT_TYPE));
			break;

		case SECONDARY_URL:
			debug_sprintf(szDbgMsg, "%s: Getting SECONDARY BAPI Host details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memcpy(hostDefPtr, &(ssiSettings.bapiHostDef[1]),
												sizeof(HOSTDEF_STRUCT_TYPE));
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Incorrect Host Type given",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;

			break;
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSSIHostDetails
 *
 * Description	: This function would get the display prompt demanded by the
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================*/

int getSSIHostDetails(HOST_URL_ENUM hostType, HOSTDEF_PTR_TYPE hostDefPtr)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(hostDefPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No placeholder provided for host details",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
	}
	else
	{
		memset(hostDefPtr, 0x00, sizeof(HOSTDEF_STRUCT_TYPE));

		switch(hostType)
		{
		case PRIMARY_URL:
			debug_sprintf(szDbgMsg, "%s: Getting PRIMARY SSI Host details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memcpy(hostDefPtr, &(ssiSettings.hostDef[0]),
												sizeof(HOSTDEF_STRUCT_TYPE));
			break;

		case SECONDARY_URL:
			debug_sprintf(szDbgMsg, "%s: Getting SECONDARY SSI Host details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			memcpy(hostDefPtr, &(ssiSettings.hostDef[1]),
												sizeof(HOSTDEF_STRUCT_TYPE));
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Incorrect Host Type given",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;

			break;
		}
	}

	return rv;
}
/*
 * ============================================================================
 * Function Name: getNumUniqueHosts
 *
 *
 * Description	: This function will return number of unique hosts
 *
 *
 * Input Params	: None
 *
 *
 * Output Params: Number of Hosts configured
 * ============================================================================
 */
int getNumUniqueHosts()
{
	return ssiSettings.iNumUniqueHosts;
}
/*
 * ============================================================================
 * Function Name: getNumOfTimedOutTranToSAF
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: Returns the number of timed out transactions after which we need to start
 * SAF'ing the transactions.
 * ============================================================================
 */
int getNumOfTimedOutTranToSAF()
{
	return ssiSettings.iSAFOnRespTimeout;
}

/*
 * ============================================================================
 * Function Name: getNumOfMaxTranOnScndURL
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: Returns the number of maximum transactions which can be done on secondary URL, after that it will auto switch to primary URL
 * ============================================================================
 */
int getNumOfMaxTranOnScndURL()
{
	return ssiSettings.iSwitchOnPrimUrlAfterXTran;
}

/*
 * ============================================================================
 * Function Name: getNumOfMaxTimeoutTranToSwitchURL
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: Returns the number of maximum timeout transactions after that URL has to be switched to the alternative one
 * ============================================================================
 */
int getNumOfMaxTimeoutTranToSwitchURL()
{
	return ssiSettings.iSwitchUrlAfterXRespTimeout;
}

/*
 * ================================================================================
 * Function Name: isPreamblePingReqd
 *
 * Description	: This function tells whether PING test is required during preamble
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * =================================================================================
 */
PAAS_BOOL isPreamblePingReqd()
{
	if(ssiSettings.preamblePingReqd == PAAS_TRUE)
	{
		return PAAS_TRUE;
	}
	else
	{
		return PAAS_FALSE;
	}
}

/*
 * ============================================================================
 * Function Name: isDemoModeEnabled
 *
 * Description	: This function informs the caller if the demo mode is enabled
 * 					or not for PaaS.
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isDemoModeEnabled()
{
	return ssiSettings.demoMode;
}

/*
 * ============================================================================
 * Function Name: getDemoModeText
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
char* getDemoModeText()
{
	return ssiSettings.szDemoModeText;
}

/*
 * ============================================================================
 * Function Name: setDemoModeParamValue
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void setDemoModeParamValue(PAAS_BOOL bValue)
{
	ssiSettings.demoMode = bValue;
}

/*
 * ============================================================================
 * Function Name: getHostProtocolFormat
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getHostProtocolFormat()
{
	return ssiSettings.ihostProtocol;
}

/*
 * ============================================================================
 * Function Name: getPAPymtTypeFormat
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getPAPymtTypeFormat()
{
	return ssiSettings.iPAPymtProtocol;
}

/*
 * ============================================================================
 * Function Name: getBapiHostDefinition
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getBapiHostDefinition()
{
	int		rv				= 0;
	int		len				= 0;
	int		iCnt			= 0;
	int		jCnt			= 0;
	int		iVal			= 0;
	int		maxLen			= 0;
	char *	tempPtr			= NULL;
	char	szTemp[100]		= "";
	long	lVal			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while((rv == SUCCESS) && (iCnt < 2))
	{
		switch(iCnt)
		{
		case PRIMARY_URL:

			maxLen = sizeof(szTemp);
			memset(szTemp, 0x00, maxLen);

			/* Get Host URL */
			rv = getEnvFile(SECTION_HOST, BAPI_PRIM_URL, szTemp, maxLen);
			if(rv > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Bapi Primary PWC Host URL = [%s]",
														__FUNCTION__, szTemp);
				APP_TRACE(szDbgMsg);

				/* NOTE: validation of url remaining */
				len = (rv + 1) * sizeof(char);
				tempPtr = (char *) malloc(len);
				if(tempPtr)
				{
					memset(tempPtr, 0x00, len);
					memcpy(tempPtr, szTemp, rv);
					ssiSettings.bapiHostDef[iCnt].url = tempPtr;
					tempPtr = NULL;
					len = 0;

					rv = SUCCESS;
				}
				else
				{
					/* Memory allocation failed for storing primary URL */
					debug_sprintf(szDbgMsg,
							"%s - memory allocation failed for Primary PWC URL",
							__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}
			}
			else
			{
				/* URL not found */
				debug_sprintf(szDbgMsg, "%s - Primary PWC URL not found",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}

			if(rv == SUCCESS)
			{
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get the PWC host port number ---- */
				rv = getEnvFile(SECTION_HOST, BAPI_PRIM_PORT, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal > 0) && (iVal <= 65535) )
					{
						ssiSettings.bapiHostDef[iCnt].portNum = iVal;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid prim BAPI port [%d]"
										, __FUNCTION__, iVal);
						ssiSettings.bapiHostDef[iCnt].portNum = 0;
					}
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg, "%s: Bapi Primary host port not found",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					ssiSettings.bapiHostDef[iCnt].portNum = 0;
				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Connection Timeout ---- */
				rv = getEnvFile(SECTION_HOST, BAPI_PRIM_CONN_TO, szTemp, maxLen);
				if(rv > 0)
				{
					lVal = atol(szTemp);
					ssiSettings.bapiHostDef[iCnt].conTimeOut = lVal;

					debug_sprintf(szDbgMsg, "%s: Bapi Primary Conn TimeOut = [%ld]",
															__FUNCTION__, lVal);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg,
						"%s: Bapi Primary conn timeout not found, setting default",
						__FUNCTION__);
					APP_TRACE(szDbgMsg);

					ssiSettings.bapiHostDef[iCnt].conTimeOut =
														DFLT_HOSTCONN_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTCONN_TIMEOUT);
					putEnvFile(SECTION_HOST, BAPI_PRIM_CONN_TO, szTemp);

				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Response Timeout ---- */
				rv = getEnvFile(SECTION_HOST, BAPI_PRIM_RESP_TO, szTemp, maxLen);
				if(rv > 0)
				{
					lVal = atol(szTemp);
					ssiSettings.bapiHostDef[iCnt].respTimeOut = lVal;

					debug_sprintf(szDbgMsg, "%s: Primary Resp TimeOut = [%ld]",
															__FUNCTION__, lVal);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg,
						"%s: Bapi Primary resp timeout not found, setting default",
						__FUNCTION__);
					APP_TRACE(szDbgMsg);

					ssiSettings.bapiHostDef[iCnt].respTimeOut =
														DFLT_HOSTRESP_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTRESP_TIMEOUT);
					putEnvFile(SECTION_HOST, BAPI_PRIM_RESP_TO, szTemp);
				}

				rv = SUCCESS;
			}

			break;

		case SECONDARY_URL:

			maxLen = sizeof(szTemp);
			memset(szTemp, 0x00, maxLen);

			/* Get Host URL */
			rv = getEnvFile(SECTION_HOST, BAPI_SCND_URL, szTemp, maxLen);
			if(rv > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Secondary Bapi Host URL = [%s]",
														__FUNCTION__, szTemp);
				APP_TRACE(szDbgMsg);

				/* NOTE: validation of url remaining */
				len = (rv + 1) * sizeof(char);
				tempPtr = (char *) malloc(len);
				if(tempPtr)
				{
					memset(tempPtr, 0x00, len);
					memcpy(tempPtr, szTemp, rv);
					ssiSettings.bapiHostDef[iCnt].url = tempPtr;
					tempPtr = NULL;
					len = 0;

					rv = SUCCESS;
				}
				else
				{
					/* Memory allocation failed for storing secondary URL */
					debug_sprintf(szDbgMsg,
						"%s - memory allocation failed for Secondary PWC URL",
						__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}
			}
			else
			{
				/* URL not found */
				debug_sprintf(szDbgMsg, "%s - Secondary PWC URL not found",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}

			/* ---- Get Connection Timeout ---- */
			if(rv == SUCCESS)
			{
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get the PWC host port number ---- */
				rv = getEnvFile(SECTION_HOST, BAPI_SCND_PORT, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal > 0) && (iVal <= 65535) )
					{
						ssiSettings.bapiHostDef[iCnt].portNum = iVal;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid scnd PWC port [%d]"
										, __FUNCTION__, iVal);
						ssiSettings.bapiHostDef[iCnt].portNum = 0;
					}
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg, "%s: Secondary host port not found",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					ssiSettings.bapiHostDef[iCnt].portNum = 0;
				}
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				rv = getEnvFile(SECTION_HOST, BAPI_SCND_CONN_TO, szTemp, maxLen);
				if(rv > 0)
				{
					lVal = atol(szTemp);
					ssiSettings.bapiHostDef[iCnt].conTimeOut = lVal;

					debug_sprintf(szDbgMsg, "%s: Bapi Secondary Conn TimeOut = [%ld]",
															__FUNCTION__, lVal);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					/* Set Default Value */
					debug_sprintf(szDbgMsg,
						"%s -Bapi  Secndary conn timeout not found, setting default",
						__FUNCTION__);
					APP_TRACE(szDbgMsg);

					ssiSettings.bapiHostDef[iCnt].conTimeOut =
														DFLT_HOSTCONN_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTCONN_TIMEOUT);
					putEnvFile(SECTION_HOST, BAPI_SCND_CONN_TO, szTemp);
				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Response Timeout ---- */
				rv = getEnvFile(SECTION_HOST, BAPI_SCND_RESP_TO, szTemp, maxLen);
				if(rv > 0)
				{
					lVal = atol(szTemp);
					ssiSettings.bapiHostDef[iCnt].respTimeOut = lVal;

					debug_sprintf(szDbgMsg, "%s: Secondary Resp TimeOut = [%ld]",
															__FUNCTION__, lVal);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					/* Set Default Value */
					debug_sprintf(szDbgMsg,
						"%s - Secndary resp timeout not found, setting default",
						__FUNCTION__);
					APP_TRACE(szDbgMsg);

					ssiSettings.bapiHostDef[iCnt].respTimeOut =
														DFLT_HOSTRESP_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTRESP_TIMEOUT);
					putEnvFile(SECTION_HOST, BAPI_SCND_RESP_TO, szTemp);
				}

				rv = SUCCESS;
			}

			break;

		//default: CID 67344 (#1 of 1): Dead default in switch (DEADCODE). T_RaghavendranR1. Default Case Never Gets Executed.
			//break;
		}

		iCnt++;
	}

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Bapi Host definitions load failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Free any allocated resources */
		for(jCnt = 0; jCnt <= iCnt; jCnt++)
		{
			free(ssiSettings.bapiHostDef[jCnt].url);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getHostDefinition
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getHostDefinition()
{
	int		rv				= 0;
	int		len				= 0;
	int		iCnt			= 0;
	int		jCnt			= 0;
	int		iVal			= 0;
	int		maxLen			= 0;
	char *	tempPtr			= NULL;
	char	szTemp[100]		= "";
	long	lVal			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while((rv == SUCCESS) && (iCnt < 2))
	{
		switch(iCnt)
		{
		case PRIMARY_URL:

			maxLen = sizeof(szTemp);
			memset(szTemp, 0x00, maxLen);

			/* Get Host URL */
			rv = getEnvFile(SECTION_HOST, PRIM_HOST_URL, szTemp, maxLen);
			if(rv > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Primary PWC Host URL = [%s]",
														__FUNCTION__, szTemp);
				APP_TRACE(szDbgMsg);

				/* NOTE: validation of url remaining */
				len = (rv + 1) * sizeof(char);
				tempPtr = (char *) malloc(len);
				if(tempPtr)
				{
					memset(tempPtr, 0x00, len);
					memcpy(tempPtr, szTemp, rv);
					ssiSettings.hostDef[iCnt].url = tempPtr;
					tempPtr = NULL;
					len = 0;

					rv = SUCCESS;
				}
				else
				{
					/* Memory allocation failed for storing primary URL */
					debug_sprintf(szDbgMsg,
							"%s - memory allocation failed for Primary PWC URL",
							__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}
			}
			else
			{
				/* URL not found */
				debug_sprintf(szDbgMsg, "%s - Primary PWC URL not found",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}

			if(rv == SUCCESS)
			{
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get the PWC host port number ---- */
				rv = getEnvFile(SECTION_HOST, PRIM_HOST_PORT, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal > 0) && (iVal <= 65535) )
					{
						ssiSettings.hostDef[iCnt].portNum = iVal;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid prim PWC port [%d]"
										, __FUNCTION__, iVal);
						ssiSettings.hostDef[iCnt].portNum = 0;
					}
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg, "%s: Primary host port not found",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					ssiSettings.hostDef[iCnt].portNum = 0;
				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Connection Timeout ---- */
				rv = getEnvFile(SECTION_HOST, PRIM_CONN_TO, szTemp, maxLen);
				if(rv > 0)
				{
					lVal = atol(szTemp);
					ssiSettings.hostDef[iCnt].conTimeOut = lVal;

					debug_sprintf(szDbgMsg, "%s: Primary Conn TimeOut = [%ld]",
															__FUNCTION__, lVal);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg,
						"%s: Primary conn timeout not found, setting default",
						__FUNCTION__);
					APP_TRACE(szDbgMsg);

					ssiSettings.hostDef[iCnt].conTimeOut =
														DFLT_HOSTCONN_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTCONN_TIMEOUT);
					putEnvFile(SECTION_HOST, PRIM_CONN_TO, szTemp);

				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Response Timeout ---- */
				rv = getEnvFile(SECTION_HOST, PRIM_RESP_TO, szTemp, maxLen);
				if(rv > 0)
				{
					lVal = atol(szTemp);
					ssiSettings.hostDef[iCnt].respTimeOut = lVal;

					debug_sprintf(szDbgMsg, "%s: Primary Resp TimeOut = [%ld]",
															__FUNCTION__, lVal);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg,
						"%s: Primary resp timeout not found, setting default",
						__FUNCTION__);
					APP_TRACE(szDbgMsg);

					ssiSettings.hostDef[iCnt].respTimeOut =
														DFLT_HOSTRESP_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTRESP_TIMEOUT);
					putEnvFile(SECTION_HOST, PRIM_RESP_TO, szTemp);
				}

				rv = SUCCESS;
			}

			break;

		case SECONDARY_URL:

			maxLen = sizeof(szTemp);
			memset(szTemp, 0x00, maxLen);

			/* Get Host URL */
			rv = getEnvFile(SECTION_HOST, SCND_HOST_URL, szTemp, maxLen);
			if(rv > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Secondary PWC Host URL = [%s]",
														__FUNCTION__, szTemp);
				APP_TRACE(szDbgMsg);

				/* NOTE: validation of url remaining */
				len = (rv + 1) * sizeof(char);
				tempPtr = (char *) malloc(len);
				if(tempPtr)
				{
					memset(tempPtr, 0x00, len);
					memcpy(tempPtr, szTemp, rv);
					ssiSettings.hostDef[iCnt].url = tempPtr;
					tempPtr = NULL;
					len = 0;

					rv = SUCCESS;
				}
				else
				{
					/* Memory allocation failed for storing secondary URL */
					debug_sprintf(szDbgMsg,
						"%s - memory allocation failed for Secondary PWC URL",
						__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}
			}
			else
			{
				/* URL not found */
				debug_sprintf(szDbgMsg, "%s - Secondary PWC URL not found",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}

			/* ---- Get Connection Timeout ---- */
			if(rv == SUCCESS)
			{
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get the PWC host port number ---- */
				rv = getEnvFile(SECTION_HOST, SCND_HOST_PORT, szTemp, maxLen);
				if(rv > 0)
				{
					iVal = atoi(szTemp);
					if( (iVal > 0) && (iVal <= 65535) )
					{
						ssiSettings.hostDef[iCnt].portNum = iVal;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Invalid scnd PWC port [%d]"
										, __FUNCTION__, iVal);
						ssiSettings.hostDef[iCnt].portNum = 0;
					}
				}
				else
				{
					/* Value not found */
					debug_sprintf(szDbgMsg, "%s: Secondary host port not found",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					ssiSettings.hostDef[iCnt].portNum = 0;
				}
				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				rv = getEnvFile(SECTION_HOST, SCND_CONN_TO, szTemp, maxLen);
				if(rv > 0)
				{
					lVal = atol(szTemp);
					ssiSettings.hostDef[iCnt].conTimeOut = lVal;

					debug_sprintf(szDbgMsg, "%s: Secondary Conn TimeOut = [%ld]",
															__FUNCTION__, lVal);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					/* Set Default Value */
					debug_sprintf(szDbgMsg,
						"%s - Secndary conn timeout not found, setting default",
						__FUNCTION__);
					APP_TRACE(szDbgMsg);

					ssiSettings.hostDef[iCnt].conTimeOut =
														DFLT_HOSTCONN_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTCONN_TIMEOUT);
					putEnvFile(SECTION_HOST, SCND_CONN_TO, szTemp);
				}

				maxLen = sizeof(szTemp);
				memset(szTemp, 0x00, maxLen);

				/* ---- Get Response Timeout ---- */
				rv = getEnvFile(SECTION_HOST, SCND_RESP_TO, szTemp, maxLen);
				if(rv > 0)
				{
					lVal = atol(szTemp);
					ssiSettings.hostDef[iCnt].respTimeOut = lVal;

					debug_sprintf(szDbgMsg, "%s: Secondary Resp TimeOut = [%ld]",
															__FUNCTION__, lVal);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					/* Value not found */
					/* Set Default Value */
					debug_sprintf(szDbgMsg,
						"%s - Secndary resp timeout not found, setting default",
						__FUNCTION__);
					APP_TRACE(szDbgMsg);

					ssiSettings.hostDef[iCnt].respTimeOut =
														DFLT_HOSTRESP_TIMEOUT;
					/* Set the value in config.usr1 */
					sprintf(szTemp, "%d", DFLT_HOSTRESP_TIMEOUT);
					putEnvFile(SECTION_HOST, SCND_RESP_TO, szTemp);
				}

				rv = SUCCESS;
			}

			break;

		//default:
			//break; CID 67479 (#1 of 1): Dead default in switch (DEADCODE) T_RaghavendranR1 No Need for default case as it is has no effect.
		}

		iCnt++;
	}

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: PWC Host definitions load failed",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Free any allocated resources */
		for(jCnt = 0; jCnt <= iCnt; jCnt++)
		{
			free(ssiSettings.hostDef[jCnt].url);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadMerchantSettings
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int loadMerchantSettings()
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iRetVal			= 0;
	int				iLen			= 0;
	char *			cParamPtr		= NULL;
	char			szTemp[100]		= "";
	char		szDevKey[65]		= "";	
	MCNT_DTLS_PTYPE	tmpDtlsPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szTemp);

	while(1)
	{
		tmpDtlsPtr = (MCNT_DTLS_PTYPE) malloc(sizeof(MCNT_DTLS_STYPE));
		if(tmpDtlsPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation error",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(tmpDtlsPtr, 0x00, sizeof(MCNT_DTLS_STYPE));

		/* Get the client id */
		iRetVal = getEnvFile(SECTION_MRCHNT, CLIENT_ID, szTemp, iLen);
		if(iRetVal > 0)
		{
			memcpy(tmpDtlsPtr->szClientId, szTemp, iRetVal);

			debug_sprintf(szDbgMsg, "%s: Client Id = [%s]", __FUNCTION__,
														tmpDtlsPtr->szClientId);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Client ID not found", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* New Admin command SETUP_REQUEST_V3 should be made
			to get client id and device key from host*/
			setDevAdminRqd(PAAS_TRUE);
		}

		memset(szDevKey, 0x00, sizeof(szDevKey));
		/* Get the device key stored in devKey.dat file */
		iRetVal = getDeviceKey(szDevKey);
		if(iRetVal == SUCCESS)
		{
			memset(tmpDtlsPtr->szDevKey, 0x00, sizeof(tmpDtlsPtr->szDevKey));
			memcpy(tmpDtlsPtr->szDevKey, szDevKey, strlen(szDevKey));

			debug_sprintf(szDbgMsg, "%s: Device Key = [%s]", __FUNCTION__,
														tmpDtlsPtr->szDevKey);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Device key not found", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* New Admin command SETUP_REQUEST_V3 should be made
			to get device key from host*/
			setDevAdminRqd(PAAS_TRUE);
		}
		
		for(iCnt = 0; iCnt < 4; iCnt++)
		{
			switch(iCnt)
			{
			case PROCESSOR_CREDIT:
				cParamPtr = CREDIT_PRCSR_ID;
				break;

			case PROCESSOR_DEBIT:
				cParamPtr = DEBIT_PRCSR_ID;
				break;

			case PROCESSOR_GIFT:
				cParamPtr = GIFT_CRD_PRCSR_ID;
				break;

			case PROCESSOR_CHECK:
				cParamPtr = CHECK_PRCSR_ID;
				break;

			//default: T_RaghavendranR1 CID 67272 (#1 of 1): Dead default in switch (DEADCODE). Dead Default case. So commenting it.
				//debug_sprintf(szDbgMsg,"%s: Should not come here",__FUNCTION__);
				//APP_TRACE(szDbgMsg);

				//break;
			}

			memset(szTemp, 0x00, sizeof(szTemp));
			iRetVal = getEnvFile(SECTION_MRCHNT, cParamPtr, szTemp, iLen);
			if(iRetVal > 0)
			{
				memcpy(tmpDtlsPtr->szPymntPrcsrId[iCnt], szTemp, iRetVal);
			}
		}

		/* Get the gift card processor ID */
		memset(szTemp, 0x00, sizeof(szTemp));
		iRetVal = getEnvFile(SECTION_MRCHNT, GIFT_CRD_PRCSR_ID, szTemp, iLen);
		if(iRetVal > 0)
		{
			memcpy(tmpDtlsPtr->szGiftCrdPrcsrId, szTemp, iRetVal);

			debug_sprintf(szDbgMsg, "%s: Gift card processor Id = [%s]",
									__FUNCTION__, tmpDtlsPtr->szGiftCrdPrcsrId);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: processor id for gift card not found",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* Get the GC Manual Entry Parameter */
		memset(szTemp, 0x00, sizeof(szTemp));
		iRetVal = getEnvFile(SECTION_MRCHNT, PS_GC_MANUAL_ENTRY, szTemp, iLen);
		if(iRetVal > 0)
		{
			tmpDtlsPtr->iPointGCManualEntry = atoi(szTemp);
			if(tmpDtlsPtr->iPointGCManualEntry != 0 && tmpDtlsPtr->iPointGCManualEntry != 1)
			{
				debug_sprintf(szDbgMsg, "%s: Gift Card Manual entry is set to invalid value, defaulting...",
																				__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			debug_sprintf(szDbgMsg, "%s: Gift Card Manual entry = [%d]",
									__FUNCTION__, tmpDtlsPtr->iPointGCManualEntry);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Gift Card Manual entry is not set, defaulting...",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			tmpDtlsPtr->iPointGCManualEntry = 0;
		}

		/* Get the store id */
		memset(szTemp, 0x00, sizeof(szTemp));
		iRetVal = getEnvFile(SECTION_MRCHNT, MCNT_STORE_ID, szTemp, iLen);
		if(iRetVal > 0)
		{
			memcpy(tmpDtlsPtr->szStoreId, szTemp, iRetVal);

			debug_sprintf(szDbgMsg, "%s: Store Id = [%s]", __FUNCTION__,
														tmpDtlsPtr->szStoreId);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Store ID not found", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* Get the Lane id */
		memset(szTemp, 0x00, sizeof(szTemp));
		iRetVal = getEnvFile(SECTION_DHI, MCNT_LANE_ID, szTemp, iLen); //Lane Id is present in DHI section. Thats why reading from DHI section.
		if(iRetVal > 0)
		{
			memcpy(tmpDtlsPtr->szLaneId, szTemp, iRetVal);

			debug_sprintf(szDbgMsg, "%s: Lane Id = [%s]", __FUNCTION__,
														tmpDtlsPtr->szLaneId);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Lane Id not found", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(rv == SUCCESS)
		{
			/* Assign the values to the data system */

			debug_sprintf(szDbgMsg, "%s: got the merchant settings",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			ssiSettings.stMcntCfgPtr = tmpDtlsPtr;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Flushing the data", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* Deallocate the memory */
			free(tmpDtlsPtr);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getGenDevPymtDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getGenDevPymtDtls(char ** szDevDtls, int * iDevModel)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Pointing to client id */
	szDevDtls[0] = ssiSettings.stMcntCfgPtr->szClientId;

	/* Pointing to device key */
	szDevDtls[1] = ssiSettings.stMcntCfgPtr->szDevKey;

	/* Pointing to device serial number */
	szDevDtls[2] = ssiSettings.szDevSrlNo;

	szDevDtls[3] = ssiSettings.szDevType;
	
	*iDevModel 	 = ssiSettings.devModel;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMerchantProcessor
 *
 * Description	: getter call for payment processor id.
 *
 * Input Params	:
 *
 * Output Params: char*
 * ============================================================================
 */
char * getMerchantProcessor(int pymtType)
{
	return ssiSettings.stMcntCfgPtr->szPymntPrcsrId[pymtType];
}

/*
 * ============================================================================
 * Function Name: getMerchantStoreId
 *
 * Description	: Function returns store id of the merchant
 *
 * Input Params	:
 *
 * Output Params: char*
 * ============================================================================
 */
char * getMerchantStoreId()
{
	return ssiSettings.stMcntCfgPtr->szStoreId;
}

/*
 * ============================================================================
 * Function Name: getMerchantStoreId
 *
 * Description	: Function returns lane id of the merchant
 *
 * Input Params	:
 *
 * Output Params: char*
 * ============================================================================
 */
char * getMerchantLaneId()
{
	return ssiSettings.stMcntCfgPtr->szLaneId;
}

/*
 * ============================================================================
 * Function Name: getGCManualEntryParam
 *
 * Description	: This function returns the Gift card Manual Entry option
 *
 * Input Params	: none
 *
 * Output Params: 0/ 1
 * ============================================================================
 */
int getGCManualEntryParam()
{
	if(ssiSettings.stMcntCfgPtr != NULL)
	{
		return ssiSettings.stMcntCfgPtr->iPointGCManualEntry;
	}
	return 0; //returning default value
}

/*
 * ============================================================================
 * Function Name: getURLFrmCnfgFile
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
char * getURLFrmCnfgFile()
{
	return ssiSettings.hostDef[0].url;
}

/*
 * ============================================================================
 * Function Name: storeProcessorId
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int storeProcessorId(char * szPymntId, char * szProcId)
{
	int		rv		= SUCCESS;

	if(strcmp(szPymntId, "CREDIT") == SUCCESS)
	{
		/* For now do nothing */
		strcpy(ssiSettings.stMcntCfgPtr->szPymntPrcsrId[PROCESSOR_CREDIT],
																szProcId);
		/* Update the confi.usr1 file also */
		rv = putEnvFile(SECTION_MRCHNT, CREDIT_PRCSR_ID, szProcId);
		if(rv < 0)
		{
			rv = FAILURE;
		}
	}
	else if(strcmp(szPymntId, "DEBIT") == SUCCESS)
	{
		strcpy(ssiSettings.stMcntCfgPtr->szPymntPrcsrId[PROCESSOR_DEBIT],
																szProcId);
		/* Update the confi.usr1 file also */
		rv = putEnvFile(SECTION_MRCHNT, DEBIT_PRCSR_ID, szProcId);
		if(rv < 0)
		{
			rv = FAILURE;
		}
	}
	else if(strcmp(szPymntId, "GIFT") == SUCCESS)
	{
		/* VDR: TODO: deprecate this immediate line of code */
		strcpy(ssiSettings.stMcntCfgPtr->szGiftCrdPrcsrId, szProcId);

		strcpy(ssiSettings.stMcntCfgPtr->szPymntPrcsrId[PROCESSOR_GIFT],
																szProcId);

		/* Update the confi.usr1 file also */
		rv = putEnvFile(SECTION_MRCHNT, GIFT_CRD_PRCSR_ID, szProcId);
		if(rv < 0)
		{
			rv = FAILURE;
		}
	}
	else if(strcmp(szPymntId, "CHECK") == SUCCESS)
	{

		strcpy(ssiSettings.stMcntCfgPtr->szPymntPrcsrId[PROCESSOR_CHECK],
																szProcId);

		/* Update the confi.usr1 file also */
		rv = putEnvFile(SECTION_MRCHNT, CHECK_PRCSR_ID, szProcId);
		if(rv < 0)
		{
			rv = FAILURE;
		}
	}
	return rv;
}

/*
 * ============================================================================
 * Function Name: getClientId
 *
 * Description	:
 *
 * Input Params	: Buffer to store value
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getClientId(char * pszClientID)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszClientID == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid buffer is passed!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(isDemoModeEnabled() == PAAS_TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: Demo Mode is ENABLED", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(pszClientID, "DEMO");
	}
	else
	{
		iLen = strlen(ssiSettings.stMcntCfgPtr->szClientId);

		if(iLen > 0)
		{
			strcpy(pszClientID, ssiSettings.stMcntCfgPtr->szClientId);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No value for clientid", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeClientId
 *
 * Description	:
 *
 * Input Params	: client ID
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int storeClientId(char * szClientId)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szClientId == NULL) || ((iLen = strlen(szClientId)) <= 0) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid paramter", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: client Id = [%s]", DEVDEBUGMSG,
													__FUNCTION__, szClientId);
		APP_TRACE(szDbgMsg);
#endif
		strncpy(ssiSettings.stMcntCfgPtr->szClientId, szClientId, sizeof(ssiSettings.stMcntCfgPtr->szClientId) - 1);

		rv = putEnvFile(SECTION_MRCHNT, CLIENT_ID, szClientId);
		if(rv < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to store in client id ",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeDHIVersionInfo
 *
 * Description	:
 *
 * Input Params	: client ID
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int storeDHIVersionInfo(char * szDHIVersion)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	char*	nxtPtr			= NULL;
	char*	currPtr			= NULL;
	char*	tempPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szDHIVersion == NULL) || ((iLen = strlen(szDHIVersion)) <= 0) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid paramter", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: DHI Version = [%s]", DEVDEBUGMSG,
													__FUNCTION__, szDHIVersion);
		APP_TRACE(szDbgMsg);
#endif

		memset(ssiSettings.szDHIVersion, 0x00, sizeof(ssiSettings.szDHIVersion));
		nxtPtr = strchr(szDHIVersion, PIPE);
		if (nxtPtr != NULL)
		{
			memcpy(ssiSettings.szDHIVersion, szDHIVersion, nxtPtr - szDHIVersion);

			nxtPtr = nxtPtr + 1;
			ssiSettings.iDHIBuild = atoi(nxtPtr);
		}
		else
		{
			strcpy(ssiSettings.szDHIVersion, szDHIVersion);
		}

		memset(ssiSettings.szMID, 0x00, sizeof(ssiSettings.szMID));

		nxtPtr = strstr(szDHIVersion, "MID");
		if(nxtPtr != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Contains MID", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			tempPtr = nxtPtr + 4; //Starting of the MID value

			nxtPtr = strchr(tempPtr, PIPE);

			if (nxtPtr != NULL)
			{
				memcpy(ssiSettings.szMID, tempPtr, nxtPtr - tempPtr);
			}
		}

		memset(ssiSettings.szTID, 0x00, sizeof(ssiSettings.szTID));

		nxtPtr = strstr(szDHIVersion, "TID");
		if(nxtPtr != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Contains TID", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			tempPtr = nxtPtr + 4; //Starting of the TID value

			nxtPtr = strchr(tempPtr, PIPE);
			if (nxtPtr != NULL)
			{
				memcpy(ssiSettings.szTID, tempPtr, nxtPtr - tempPtr);
			}
			else
			{
				strcpy(ssiSettings.szTID, tempPtr);
			}

			currPtr = tempPtr;

		}

		if(currPtr != NULL)
		{
			memset(ssiSettings.szDHIProc, 0x00, sizeof(ssiSettings.szDHIProc));
			nxtPtr = strchr(currPtr, PIPE);
			if (nxtPtr != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Contains DHI Processor Field", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				nxtPtr = nxtPtr + 1;
				strcpy(ssiSettings.szDHIProc, nxtPtr);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Does Not Contain DHI Processor Field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveMcntStoreId
 *
 * Description	:
 *
 * Input Params	: Store ID
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int saveMcntStoreId(char * pszStoreId)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pszStoreId == NULL) || (strlen(pszStoreId) <= 0) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid paramter", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: Store Id = [%s]", DEVDEBUGMSG,
													__FUNCTION__, pszStoreId);
		APP_TRACE(szDbgMsg);
#endif
		if(ssiSettings.stMcntCfgPtr != NULL)
		{
			strcpy(ssiSettings.stMcntCfgPtr->szStoreId, pszStoreId);

			rv = putEnvFile(SECTION_MRCHNT, MCNT_STORE_ID, pszStoreId);
			if(rv < 0)
			{
				debug_sprintf(szDbgMsg, "%s: Unable to store in Store Id ", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		rv = SUCCESS;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveMcntLaneId
 *
 * Description	:
 *
 * Input Params	: Lane ID
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int saveMcntLaneId(char * pszLaneId)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pszLaneId == NULL) || (strlen(pszLaneId) <= 0) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid paramter", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: Lane Id = [%s]", DEVDEBUGMSG,
													__FUNCTION__, pszLaneId);
		APP_TRACE(szDbgMsg);
#endif
		if(ssiSettings.stMcntCfgPtr != NULL)
		{
			//Store it memory
			strcpy(ssiSettings.stMcntCfgPtr->szLaneId, pszLaneId);

			rv = putEnvFile(SECTION_DHI, MCNT_LANE_ID, pszLaneId);
			if(rv < 0)
			{
				debug_sprintf(szDbgMsg, "%s: Unable to store in Lane Id ", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		rv = SUCCESS;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDHUBuildNum
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getDHIBuildNum()
{
	return ssiSettings.iDHIBuild;
}

/*
 * ============================================================================
 * Function Name: getDHIVersionInfo
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
char* getDHIVersionInfo()
{
	return ssiSettings.szDHIVersion;
}

/*
 * ============================================================================
 * Function Name: getDHIMID
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
char* getDHIMID()
{
	return ssiSettings.szMID;
}

/*
 * ============================================================================
 * Function Name: getDHITID
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
char* getDHITID()
{
	return ssiSettings.szTID;
}

/*
 * ============================================================================
 * Function Name: getDHIProcessor
 *
 * Description	: Returns the Processor Library used by the DHI
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
char* getDHIProcessor()
{
	return ssiSettings.szDHIProc;
}

/*
 * ============================================================================
 * Function Name: getDeviceKey
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getDeviceKey(char * szDevKey)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	char	tmpStr[100]		= "";
	FILE *	fp				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		patchCompatibilityCode();

		if(ssiSettings.stMcntCfgPtr == NULL)
		{
			MCNT_DTLS_PTYPE		tmpPtr	= NULL;

			tmpPtr = (MCNT_DTLS_PTYPE) malloc(sizeof(MCNT_DTLS_STYPE));
			if(tmpPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory allocation FAILED",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(tmpPtr, 0x00, sizeof(MCNT_DTLS_STYPE));
			ssiSettings.stMcntCfgPtr = tmpPtr;
		}
		else
		{
			/* First check if already stored in the data system */
			iLen = strlen(ssiSettings.stMcntCfgPtr->szDevKey);
			if(iLen > 0)
			{
				strcpy(szDevKey, ssiSettings.stMcntCfgPtr->szDevKey);
#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s: Device key found = [%s]",
										DEVDEBUGMSG, __FUNCTION__, szDevKey);
				APP_TRACE(szDbgMsg);
#endif
				break;
			}
		}

		/* If the memory to the merchant details structure is allocated just
		 * now, or if the device key doesnt exist in the structure, then try
		 * looking for the device key in the DEVKEY_FILE */
		rv = doesFileExist(DEVKEY_FILE);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: File doesnt exist", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Try to open the file for reading the device key */
		fp = fopen(DEVKEY_FILE, "r");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to open file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		if(chmod(DEVKEY_FILE, 0666) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* Try to read the device key from the file */
		memset(tmpStr, 0x00, sizeof(tmpStr));
		if(NULL == fgets(tmpStr, 65, fp))
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to read file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: Device Key from file = [%s]",
											DEVDEBUGMSG, __FUNCTION__, tmpStr);
		APP_TRACE(szDbgMsg);
#endif
		memset(ssiSettings.stMcntCfgPtr->szDevKey, 0x00, sizeof(ssiSettings.stMcntCfgPtr->szDevKey));
		strcpy(ssiSettings.stMcntCfgPtr->szDevKey, tmpStr);
		strcpy(szDevKey, tmpStr);

		break;
	}

	/* Close any open files */
	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeDeviceKey
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
int storeDeviceKey(char * szDevKey)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	char	szCmd[256]		= "";
	FILE *	fp				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szDevKey == NULL) || ((iLen = strlen(szDevKey)) <= 0) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid paramter", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: Device Key = [%s]", DEVDEBUGMSG,
														__FUNCTION__, szDevKey);
		APP_TRACE(szDbgMsg);
#endif
		/* Saving the device key in the config.usr1 can be a security threat to
		 * the application as any party can see this device key and use the
		 * same to communicate with the PWC. The other three mandatory fields
		 * (DEV_SRL_NO, CLIENT_ID, DEV_TYPE) can be easily obtained, so this
		 * should be stored securely without providing access to any third
		 * party user or application */

		/* Open the temporary file for writing */
		fp = fopen(TEMP_FILE, "w");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Unable to open file for writing",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Write the device key to the temporary file */
		if(EOF == fputs(szDevKey, fp))
		{
			debug_sprintf(szDbgMsg, "%s: Failed to write device key",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		fclose(fp);
		fp = NULL;

		/* Move the temporary file to the original location, overwriting the
		 * original file, if present */
		sprintf(szCmd, "mv %s %s", TEMP_FILE, DEVKEY_FILE);
		rv = local_svcSystem(szCmd);
		if(rv != SUCCESS)
		{
#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s:%s: FAILED to execute [%s] rv = [%d]",
										DEVDEBUGMSG, __FUNCTION__, szCmd, rv);
#else
			debug_sprintf(szDbgMsg,"%s: FAILED to move temp file",__FUNCTION__);
#endif
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		if(chmod(DEVKEY_FILE, 0666) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		strcpy(ssiSettings.stMcntCfgPtr->szDevKey, szDevKey);

		break;
	}

	/* Close any open file pointers */
	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: patchCompatibilityCode
 *
 * Description	: This function is being added to provide a compatibility of
 * 					the deviceKey storing/retrieving logic changes done in 2.17
 * 					with the version 2.16
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int patchCompatibilityCode()
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	char	tmpStr[100]		= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(SUCCESS == doesFileExist(DEVKEY_FILE))
		{
			/* The patch code has already executed in the previous execution of
			 * the application. The steps below should not be repeated for
			 * every reboot of the application. */

			/* Remove the device key from the config.usr1 file, if added
			 * maliciously or accidently by the user */
			putEnvFile(SECTION_MRCHNT, DEVICE_KEY, "");

			break;
		}

		iLen = sizeof(tmpStr);
		rv = getEnvFile(SECTION_MRCHNT, DEVICE_KEY, tmpStr, iLen);
		if(rv <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: Couldnt find Device Key in config.usr1"
																, __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Device Key found in config.usr1 file, remove it from there and store
		 * the device key in the new file */
		rv = storeDeviceKey(tmpStr);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to store device key in new file"
																, __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Remove the device key from the config.usr1 file */
		putEnvFile(SECTION_MRCHNT, DEVICE_KEY, "");

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: isDeviceKeyPresent
 *
 * Description	:  @__deprecated : VDR: should use getDeviceKey instead..
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isDeviceKeyPresent()
{
	int			rv			= 0;
	int			iLen		= 0;
	char		tmpStr[30]	= "";
	PAAS_BOOL	retVal		= PAAS_TRUE;

	iLen = sizeof(tmpStr);

	rv = getEnvFile(SECTION_DEVICE, DEVICE_KEY, tmpStr, iLen);
	if(rv > 0)
	{
		if(strlen(tmpStr) > 0)
		{
			/* True case scenario */
			retVal = PAAS_TRUE;
		}
		else
		{
			/* False case scenario */
			retVal = PAAS_FALSE;
		}
	}
	else
	{
		/* Cant find the config variable in the config.usr1 */
		/* False case scenario */
		retVal = PAAS_FALSE;
	}

	return retVal;
}
#endif
/*
 * ============================================================================
 * Function Name: getBAPIDestId
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getBAPIDestId()
{
	return ssiSettings.bapiDestId;
}

/*
 * ============================================================================
 * Function Name: isBapiEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
PAAS_BOOL isBapiEnabled()
{
	return ssiSettings.bapiEnabled;
}

/*
 * ============================================================================
 * Function Name: getKeepAliveParameter
 *
 * Description	: This function returns the keep Alive parameter value
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getKeepAliveParameter()
{
	return ssiSettings.iKeepAlive;
}

/*
 * ============================================================================
 * Function Name: getKeepAliveInterval
 *
 * Description	: This function returns the keep Alive parameter value
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getKeepAliveInterval()
{
	return ssiSettings.iKeepAliveInt;
}

/*
 * ============================================================================
 * Function Name: isDHIEnabled
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
PAAS_BOOL isDHIEnabled()
{
	return ssiSettings.dhiEnabled;
}

/*
 * ============================================================================
 * Function Name: isCheckHostConnStatusEnable
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: TRUE / FALSE
 * ============================================================================
 */
PAAS_BOOL isCheckHostConnStatusEnable()
{
	return ssiSettings.chkHostConnStatus;
}
