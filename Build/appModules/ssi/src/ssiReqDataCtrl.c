#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common/metaData.h"
#include "common/tranDef.h"
#include "common/xmlUtils.h"
#include "common/utils.h"
#include "db/dataSystem.h"
#include "ssi/ssiDef.h"
#include "ssi/ssiMain.h"

#include "bLogic/bLogicCfgDef.h"
//#include "ssi/ssiCfgDef.h"

#include "ssi/ssiReqLists.inc"


typedef METADATA_STYPE	META_STYPE;
typedef METADATA_PTYPE	META_PTYPE;

#define BASKLINE_SIZE 		40
#define BASKET_RESPONSE "BasketSubmission"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

static char * szSSIFxn[SSI_MAX_FXN] =
{
	"ADMIN",
	"PAYMENT",
	"BATCH",
	"REPORT",
	"DEVICE"
};

static char * szSSICmd[SSI_MAX_CMD] =
{
	"SETUP_REQUEST_V4",
	"PRE_AUTH",
	"VOICE_AUTH",
	"POST_AUTH",
	"SALE",
	"CREDIT",
	"VOID",
	"COMPLETION",
	"COMMERCIAL",
	"ACTIVATE",
	"ADD_VALUE",
	"BALANCE",
	"REGISTER",
	"GIFT_CLOSE",
	"DEACTIVATE",
	"REACTIVATE",
	"REMOVE_VALUE",
	"SIGNATURE",
	"SETTLE",
	"DAYSUMMARY",
	"PRESETTLEMENT",
	"SETTLEERROR",
	"TRANSEARCH",
	"SETTLESUMMARY",
	"LAST_TRAN",
	"DUPCHECK",
	"EMVADMIN",
	"TOKEN_QUERY",    	/* Token Query */
	"VERIFY",    		/*CHECK Verify */
	"REVERSAL",  		/*CHECK REFUND */
	"ADD_TIP",
	"RESET_TIP",
	"MESSAGE_ACK",      /* Message ACK to SSI */
	"VERSION",			/*Version For DHI*/
	"CUTOVER",
	"SITETOTALS",
	"CREDIT_APP",
	"TIMEOUT_REVERSAL",
	"CHECKIN",
	"CHECKOUT",
	"ACCT_PAY",			/*KranthiK1: Changed it to ACCT_PAY from PAY_ACCOUNT Need to revert it back when UGP is enabled*/
	"REFUND",
	"QUERY",
	"REGISTER",
	"CARD_RATE"
};

//static char * szPymtType[MAX_TENDERS] =
static char * szPymtType[MAX_PYMT] =
{
	"CREDIT",
	"DEBIT",
	"GIFT",
	"ISIS",
	"GOOGLE",
	"BALANCE",
	"PRIV_LBL",
	"MERCH_CREDIT",
	"PAYPAL",
	"EBT",
	"FSA",
	"POS_TENDER1",		/* POS Defined Tender Types */
	"POS_TENDER2",		/* Note: These 3 Payment Type names would never be used in SSI, because we won't encounter these 3 Payment Types in SSI*/
	"POS_TENDER3",		/* Adding here, just to maintain the size of szPymtType array with the corresponding enums */
	"PAYACCOUNT",
	"TOKEN",
	"CHECK",
	"CHECK",
	"CHECK",
	"OTHER",
	"ADMIN"
};

static char * szPymtSubType[MAX_SUBPYMT] =
{
	"INTERNAL",
	"EXTERNAL",
	"GIFTMALL",
	"INCOMM",
	"BLACKHAWK"
};

static char * szCheckType[MAX_CHKTYPE] =
{
	"0",  //Personal
	"0", //Consumer/Business
	"1", //Payroll
	"2", //Government
};

extern int	getGenDevPymtDtls(char **, int *);
extern int	getClientId(char *);
extern int	getBAPIDestId();
extern int	doBAPICommunication(char* , int );
extern int	cleanupPaymentItems(PYMNTINFO_PTYPE);
extern char* getMerchantProcessor(int);
extern void cleanupLineItems(LIINFO_PTYPE);
extern PAAS_BOOL isDHIEnabled();
extern int getHostProtocolFormat();
extern int getPAPymtTypeFormat();

/* Static functions declarations */
static int getAdminReqMetaData(META_PTYPE, int);
static int getPymtReqMetaData(META_PTYPE, PYMTTRAN_PTYPE, int);
static int getBatchReqMetaData(META_PTYPE, int);
static int getRptReqMetaData(META_PTYPE, PASSTHRU_PTYPE, int);
static int getDevReqMetaData(META_PTYPE, DEVTRAN_PTYPE, int);

static int fillAdminReqData(KEYVAL_PTYPE);
static int fillGenReqData(KEYVAL_PTYPE, int, int);
static int fillSessionDtls(KEYVAL_PTYPE, SESSDTLS_PTYPE);
static int fillAmtDtls(KEYVAL_PTYPE, AMTDTLS_PTYPE);
static int fillCardDtls(KEYVAL_PTYPE, CARDDTLS_PTYPE);
static int fillManCardData(KEYVAL_PTYPE, CARDDTLS_PTYPE);
static int fillSwipedCardData(KEYVAL_PTYPE, CARDDTLS_PTYPE);
static int fillEmvKeyLoadDtls(KEYVAL_PTYPE, EMVKEYLOAD_INFO_PTYPE);
static int fillEmvDtls(KEYVAL_PTYPE, CARDDTLS_PTYPE);
static int fillPINDtls(KEYVAL_PTYPE, PINDTLS_PTYPE);
static int fillSignDtls(KEYVAL_PTYPE, SIGDTLS_PTYPE);
static int fillLVL2Dtls(KEYVAL_PTYPE, LVL2_PTYPE);
static int fillFollowOnDtls(KEYVAL_PTYPE , FTRANDTLS_PTYPE , int , int );
static int fillGenPymtDtls(KEYVAL_PTYPE, PYMTTRAN_PTYPE, int );
static int fillGenRptFlds(KEYVAL_PTYPE);
static int fillChkDtls(KEYVAL_PTYPE , CHKDTLS_PTYPE );
static int fillCustInfoDtls(KEYVAL_PTYPE , CUSTINFODTLS_PTYPE );
static int fillOrigTranDtls(KEYVAL_PTYPE , PYMTTRAN_PTYPE );
static int fillCreditAppReqData(KEYVAL_PTYPE, CREDITAPPDTLS_PTYPE, int, int);
static int fillEBTTranDtls(KEYVAL_PTYPE , EBTDTLS_PTYPE );
static int fillFSATranDtls(KEYVAL_PTYPE , FSADTLS_PTYPE );

static int addKeyValDataNode(VAL_LST_PTYPE, void*, int);
static int getMetaDataForBasket(META_PTYPE, SESSDTLS_PTYPE);
static int fillBasketData(VAL_LST_PTYPE, SESSDTLS_PTYPE, char*);
static int fillLineItemsInBasket(KEYVAL_PTYPE, LIINFO_PTYPE, char*);
static int fillOffersInBasket(KEYVAL_PTYPE, LIINFO_PTYPE, char*);
static int fillRefundsInBasket(KEYVAL_PTYPE, PYMNTINFO_PTYPE, char*);
static int fillPaymentsInBasket(KEYVAL_PTYPE, PYMNTINFO_PTYPE, char*);
static int fillBasketGenReqDtls(KEYVAL_PTYPE);
static int fillBasketReqDtls(KEYVAL_PTYPE, char*, int);
static int fillPassThrgReqDtls(KEYVAL_PTYPE , PASSTHRG_FIELDS_PTYPE);
static int fillDCCDtls(KEYVAL_PTYPE, DCCDTLS_PTYPE);
static VAL_LST_PTYPE fillVarListPassThroughDetails(VAL_LST_PTYPE pstVarLstPtr);
static int addVarListNode(VAL_LST_PTYPE , VAL_NODE_PTYPE );

/*
 * ============================================================================
 * Function Name: getMetaDataForSSIReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getMetaDataForSSIReq(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((pstMeta == NULL) || (pstTran == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize the meta data */
		memset(pstMeta, 0x00, METADATA_SIZE);

		switch(pstTran->iFxn)
		{
		case SSI_ADMIN:
			rv = getAdminReqMetaData(pstMeta, pstTran->iCmd);
			break;

		case SSI_PYMT:
			rv = getPymtReqMetaData(pstMeta, pstTran->data, pstTran->iCmd);
			break;

		case SSI_BATCH:
			rv = getBatchReqMetaData(pstMeta, pstTran->iCmd);
			break;

		case SSI_RPT:
			rv = getRptReqMetaData(pstMeta, pstTran->data, pstTran->iCmd);
			break;

		case SSI_DEV:
			rv = getDevReqMetaData(pstMeta, pstTran->data, pstTran->iCmd);
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaDataForBAPIReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getMetaDataForBAPIReq(META_PTYPE pstMeta, char* pszBasket, int iBaskSize)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	KEYVAL_PTYPE	curPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(genReqLst)/KEYVAL_SIZE;
		iTotCnt += sizeof(basketReqLst)/KEYVAL_SIZE;

		/* Subtracting -1 from total because there is func type
		 * in basket req details
		 */
		listPtr  = (KEYVAL_PTYPE)malloc((iTotCnt-1)*KEYVAL_SIZE);
		// CID 67301 (#2-1 of 2): Dereference before null check (REVERSE_INULL). T_RaghavendranR1
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(listPtr, 0x00, (iTotCnt-1)*KEYVAL_SIZE);
		curPtr 	 = listPtr;

		iCnt = fillBasketGenReqDtls(curPtr);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill general req details",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillBasketReqDtls(curPtr, pszBasket, iBaskSize);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill basket req details",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		/* Update the meta data with the list information */
		pstMeta->iTotCnt = iTotCnt - 1;
		pstMeta->keyValList = listPtr;
		break;
	}

	if( (rv != SUCCESS) && (listPtr != NULL) )
	{
		free(listPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: getAdminReqMetaData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getAdminReqMetaData(META_PTYPE pstMeta, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iHostProtocol	= getHostProtocolFormat();
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		switch(iCmd)
		{
		//case TMP_SETUP: Praveen_P1: Just doing it..not sure whether it is correct :(
		case SSI_SETUP:

			if(iHostProtocol == HOST_PROTOCOL_SSI)
			{
				iTotCnt += sizeof(authReqLst) / KEYVAL_SIZE;
			}
			else if (iHostProtocol == HOST_PROTOCOL_UGP)
			{
				iTotCnt += sizeof(genReqLst) / KEYVAL_SIZE;
			}

			/* Allocate memory for the list */
			listPtr = (KEYVAL_PTYPE) malloc(KEYVAL_SIZE * iTotCnt);
			if(listPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory allocation FAILED",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			/* Build the list */
			memset(listPtr, 0x00, KEYVAL_SIZE * iTotCnt);

			if(iHostProtocol == HOST_PROTOCOL_SSI)
			{
				/* Fill the list */
				iCnt = fillAdminReqData(listPtr);
			}
			else if (iHostProtocol == HOST_PROTOCOL_UGP)
			{
				/* Fill the list */
				iCnt = fillGenReqData(listPtr, SSI_ADMIN, iCmd);
				if(iCnt < 0)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to fill general req details",
																		__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}

				break;
			}

			break;

		case SSI_VERSION:

			
			iTotCnt += sizeof(genReqLst) / KEYVAL_SIZE;

			/* Allocate memory for the list */
			listPtr = (KEYVAL_PTYPE) malloc(KEYVAL_SIZE * iTotCnt);
			if(listPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory allocation FAILED",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			/* Build the list */
			memset(listPtr, 0x00, KEYVAL_SIZE * iTotCnt);

			/* Fill the list */
			iCnt = fillGenReqData(listPtr, SSI_ADMIN, iCmd);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill general req details",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Element Count = [%d]", __FUNCTION__, iCnt);
		APP_TRACE(szDbgMsg);

		/* Update the meta data with the list information */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	if( (rv != SUCCESS) && (listPtr != NULL) )
	{
		free(listPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPymtReqMetaData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getPymtReqMetaData(META_PTYPE pstMeta, PYMTTRAN_PTYPE pstTran, int cmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	PAAS_BOOL		iRCNashProcessor= PAAS_FALSE;
	char * 			pszProcessorId	= NULL;
	KEYVAL_PTYPE	listPtr			= NULL;
	KEYVAL_PTYPE	curPtr			= NULL;
	int				iFxn            = 0;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pszProcessorId = getMerchantProcessor(PYMT_CREDIT);
	if(!strcasecmp(pszProcessorId, RCNASH_PROCESSOR))
	{
		iRCNashProcessor = PAAS_TRUE;
	}
	while(1)
	{
		switch(cmd)
		{
		case SSI_PREAUTH:
		case SSI_VOICEAUTH:
		case SSI_POSTAUTH:
		case SSI_SALE:
		case SSI_CREDIT:
		case SSI_REVERSAL:
		case SSI_VOID:
		case SSI_COMPLETION:
		case SSI_COMMERCIAL:
		case SSI_ACTIVATE:
		case SSI_ADDVAL:
		case SSI_BAL:
		case SSI_REG:
		case SSI_GIFTCLOSE:
		case SSI_DEACTIVATE:
		case SSI_REACTIVATE:
		case SSI_REMVAL:
		case SSI_SIGN:
		case SSI_EMVADMIN:
		case SSI_VERIFY:   //Check verify command
		case SSI_ADDTIP:
		case SSI_RESETTIP:
		case SSI_TOKENQUERY:
		case SSI_ACK:      //MESSAGE ACK command
		case SSI_CHECKIN:
		case SSI_CHECKOUT:
		case SSI_TOR:
		case SSI_PAYACCOUNT:
		case SSI_QUERY:
		case SSI_CARDRATE:

			/* For Valid payment commands */

			iTotCnt += sizeof(genReqLst) / KEYVAL_SIZE;
			iTotCnt += sizeof(pymtGenReqLst) / KEYVAL_SIZE;

			/* Count for session Details */
			if(pstTran->pstSess != NULL)
			{
				iTotCnt += sizeof(sessDtlsLst) / KEYVAL_SIZE;
			}

			/* Count for amount Details */
			if(pstTran->pstAmtDtls != NULL)
			{
				iTotCnt += sizeof(stAmtLst) / KEYVAL_SIZE;
			}

			/* Count for card Details */
			if(pstTran->pstCardDtls != NULL)
			{
				iTotCnt += sizeof(cardDtlsLst) / KEYVAL_SIZE;
			}

			/* Count for EMV Details */
			if(pstTran->pstCardDtls && (pstTran->pstCardDtls->bEmvData == PAAS_TRUE || pstTran->pstCardDtls->iCardSrc ==  CRD_EMV_CT || pstTran->pstCardDtls->iCardSrc == CRD_EMV_CTLS))
			{
				iTotCnt += sizeof(stEmvReqLst) / KEYVAL_SIZE;
			}

			/* Count for EMV Key load Details */
			if(pstTran->pstEmvKeyLoadInfo != NULL)
			{
				iTotCnt += sizeof(stEmvKeyLoadLst) / KEYVAL_SIZE;
			}
			
			/* Count for PIN Details */
			if(pstTran->pstPINDtls != NULL)
			{
				iTotCnt += sizeof(pinDtlsLst) / KEYVAL_SIZE;
			}

			/* Count for signature Details */
			if(pstTran->pstSigDtls != NULL)
			{
				iTotCnt += sizeof(sigDtlsLst) / KEYVAL_SIZE;
			}

			/* Count for tax (level 2) Details */
			if(pstTran->pstLevel2Dtls != NULL)
			{
				iTotCnt += sizeof(lvl2DtlsLst) / KEYVAL_SIZE;
			}

			/* Count for follow on tran Details */
			if(pstTran->pstFTranDtls != NULL)
			{
				iTotCnt += sizeof(followOnDtlsLst) / KEYVAL_SIZE;
			}

			/* Count for Check processing Details */
			if(pstTran->pstChkDtls != NULL)
			{
				iTotCnt += sizeof(chkDtlsLst) / KEYVAL_SIZE;
			}

			/* Count for Customer Info Details */
			if(pstTran->pstCustInfoDtls != NULL)
			{
				iTotCnt += sizeof(custInfoDtlsLst) / KEYVAL_SIZE;
			}

			/* Count for Original Transaction Date/Time*/
			if((strlen(pstTran->szOrigTranDt) > 0) && (strlen(pstTran->szOrigTranTm) > 0))
			{
				iTotCnt += sizeof(pymtOrigTranDateLst) / KEYVAL_SIZE;
			}

			if(pstTran->pstEBTDtls != NULL)
			{
				iTotCnt += sizeof(ebtTransReqLst) / KEYVAL_SIZE;
			}

			if((pstTran->pstFSADtls != NULL) && ((cmd == SSI_SALE) || (cmd == SSI_CREDIT)))
			{
				iTotCnt += sizeof(FSATransReqLst) / KEYVAL_SIZE;
			}

			if(pstTran->pstPassThrghDtls != NULL && pstTran->pstPassThrghDtls->iReqTagsCnt > 0)
			{
				iTotCnt += pstTran->pstPassThrghDtls->iReqTagsCnt;
			}

			/* Count for card Details */
			if(pstTran->pstDCCDtls != NULL)
			{
				iTotCnt += sizeof(dccDtlsLst) / KEYVAL_SIZE;
			}
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}


		if(iRCNashProcessor == PAAS_TRUE)
		{
			/* For a token based transaction in PWC-RC setup, the BANK_USERDATA
			 * obtained from the SCI Request should be sent as PAYMENT_MEDIA, so
			 * BANK_USERDATA is copied as PAYMENT_MEDIA and BANK_USERDATA is cleared
			 */
			if(pstTran->pstFTranDtls != NULL && pstTran->pstCardDtls != NULL)
			{
				if(strlen(pstTran->pstFTranDtls->szBankUserData) > 0)
				{
					strcpy(pstTran->pstCardDtls->szPymtMedia, pstTran->pstFTranDtls->szBankUserData);
					memset(pstTran->pstFTranDtls->szBankUserData, 0x00, sizeof(pstTran->pstFTranDtls->szBankUserData));
				}
			}
#if 0			/* For a normal transaction PAYMENT_MEDIA field need not be sent in the request,
			 * hence clearing the PAYMENT_MEDIA buffer
			 */
			if(pstTran->pstFTranDtls == NULL && pstTran->pstCardDtls != NULL)
			{
				memset(pstTran->pstCardDtls->szPymtMedia, 0x00, sizeof(pstTran->pstCardDtls->szPymtMedia));
			}
#endif
		}
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		curPtr = listPtr;

		/* Fill the general request details */
		iFxn = SSI_PYMT;
		iCnt = fillGenReqData(curPtr, iFxn, cmd);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill general req details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* Fill the general payment details */
		iCnt = fillGenPymtDtls(curPtr, pstTran, cmd);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill general payment details"
														, __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* Fill the session details */
		if(pstTran->pstSess != NULL)
		{
			iCnt = fillSessionDtls(curPtr, pstTran->pstSess);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill session details",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill the amount details */
		if(pstTran->pstAmtDtls != NULL)
		{
			iCnt = fillAmtDtls(curPtr, pstTran->pstAmtDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill amount details",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill the card details */
		if(pstTran->pstCardDtls != NULL)
		{
			iCnt = fillCardDtls(curPtr, pstTran->pstCardDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill card details",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		
		/* Fill the EMV key load details */
		if(pstTran->pstEmvKeyLoadInfo != NULL)
		{
			iCnt = fillEmvKeyLoadDtls(curPtr, pstTran->pstEmvKeyLoadInfo);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill EMV Key Load Info details",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}		

		/* Fill the EMV details */
		if(pstTran->pstCardDtls != NULL && (pstTran->pstCardDtls->bEmvData == PAAS_TRUE || pstTran->pstCardDtls->iCardSrc ==  CRD_EMV_CT || pstTran->pstCardDtls->iCardSrc == CRD_EMV_CTLS) )
		{
			iCnt = fillEmvDtls(curPtr, pstTran->pstCardDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill EMV details",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill the PIN details */
		if(pstTran->pstPINDtls != NULL)
		{
			iCnt = fillPINDtls(curPtr, pstTran->pstPINDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill PIN details",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill the signature details */
		if(pstTran->pstSigDtls != NULL)
		{
			iCnt = fillSignDtls(curPtr, pstTran->pstSigDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill signature details",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill the level 2 (tax) details */
		if(pstTran->pstLevel2Dtls != NULL)
		{
			iCnt = fillLVL2Dtls(curPtr, pstTran->pstLevel2Dtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill level 2 details",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill the follow on details */
		if(pstTran->pstFTranDtls != NULL)
		{
			/*
			 * For DEBIT REFUND, we should not add CTROUTD even though it is
			 * present in the SCI request.
			 * Thats why we are sending command and payment type to the following
			 * function to check and add the CTROUTD value accordingly
			 */
			iCnt = fillFollowOnDtls(curPtr, pstTran->pstFTranDtls, cmd, pstTran->iPymtType);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill tax details",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill the Check processing details */
		if(pstTran->pstChkDtls != NULL)
		{
			iCnt = fillChkDtls(curPtr, pstTran->pstChkDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill check details",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill the Customer Info Details */
		if(pstTran->pstCustInfoDtls != NULL)
		{
			iCnt = fillCustInfoDtls(curPtr, pstTran->pstCustInfoDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill cust info details",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill the Original Transaction Date/Time */
		if((strlen(pstTran->szOrigTranDt) > 0) && (strlen(pstTran->szOrigTranTm) > 0))
		{
			iCnt = fillOrigTranDtls(curPtr, pstTran);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill Orig Tran Date/Time details"
															, __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill the EBT Transaction Details */
		if((pstTran->pstEBTDtls != NULL) && (pstTran->iPymtType == PYMT_EBT))
		{
			iCnt = fillEBTTranDtls(curPtr, pstTran->pstEBTDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill EBT details" , __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill the FSA Transaction Details */
		if((pstTran->pstFSADtls != NULL) && (strlen(pstTran->pstFSADtls->szAmtHealthCare) > 0) && ((cmd == SSI_SALE) || (cmd == SSI_CREDIT)))
		{
			iCnt = fillFSATranDtls(curPtr, pstTran->pstFSADtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill FSA Trans details" , __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* MukeshS3: 14-April-16:
		 * We have used the same key value pair structure KEYVAL_STYPE for pass through fields, which we uses for other XML tags.
		 */
		if(pstTran->pstPassThrghDtls != NULL && pstTran->pstPassThrghDtls->pstReqTagList != NULL)
		{
			iCnt = fillPassThrgReqDtls(curPtr, pstTran->pstPassThrghDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill Pass through details" , __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		if(pstTran->pstDCCDtls != NULL)
		{
			iCnt = fillDCCDtls(curPtr, pstTran->pstDCCDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill card details",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}


		debug_sprintf(szDbgMsg, "%s: Element Count = [%d]", __FUNCTION__, iCnt);
		APP_TRACE(szDbgMsg);

		/* Update the meta data with the list information */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	if( (rv != SUCCESS) && (listPtr != NULL) )
	{
		/* FIXME: Might not be correct way of de-allocation */
		free(listPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getBatchReqMetaData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getBatchReqMetaData(META_PTYPE pstMeta, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	int				iFxn            = 0;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		switch(iCmd)
		{
		case SSI_SETTLE:
			/* For Valid batch commands */

			iTotCnt += sizeof(genReqLst) / KEYVAL_SIZE;
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);

		/* Fill the general request details */
		iFxn = SSI_BATCH;
		iCnt = fillGenReqData(listPtr, iFxn, iCmd);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill general req details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Element Count = [%d]", __FUNCTION__, iCnt);
		APP_TRACE(szDbgMsg);

		/* Update the meta data with the list information */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	if( (rv != SUCCESS) && (listPtr != NULL) )
	{
		free(listPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getRptReqMetaData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getRptReqMetaData(META_PTYPE pstMeta, PASSTHRU_PTYPE pstData,int cmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iFxn            = 0;
	PAAS_BOOL		bAddFlds		= PAAS_FALSE;
	KEYVAL_PTYPE	listPtr			= NULL;
	KEYVAL_PTYPE	curPtr			= NULL;
	META_PTYPE		pstRptMeta		= NULL;
	PASSTHRU_PTYPE 	pstPTData       = NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		switch(cmd)
		{
		case SSI_DAYSUMM:
		case SSI_PRESTL:
		case SSI_STLERR:
		case SSI_TRANSEARCH:
		case SSI_STLSUMM:
			pstPTData = (PASSTHRU_PTYPE)pstData;
			pstRptMeta = (META_PTYPE) (pstPTData->pstMetaData);
			iTotCnt = pstRptMeta->iTotCnt;

			iTotCnt += sizeof(genReqLst) / KEYVAL_SIZE;
			iTotCnt += sizeof(genRptReqLst) / KEYVAL_SIZE;

			bAddFlds = PAAS_TRUE;
			break;

		case SSI_LASTTRAN:
			pstPTData = (PASSTHRU_PTYPE)pstData;
			pstRptMeta = (META_PTYPE) (pstPTData->pstMetaData);
			iTotCnt = pstRptMeta->iTotCnt;

			iTotCnt += sizeof(genReqLst) / KEYVAL_SIZE;

			bAddFlds = PAAS_TRUE;

			break;
		case SSI_DUPCHK:
			pstPTData = (PASSTHRU_PTYPE)pstData;
			pstRptMeta = (META_PTYPE) (pstPTData->pstMetaData);
			iTotCnt = pstRptMeta->iTotCnt;

			iTotCnt += sizeof(genReqLst) / KEYVAL_SIZE;
			bAddFlds = PAAS_TRUE;
			break;
		case SSI_CUTOVER:
		case SSI_SITETOTALS:
			pstPTData = (PASSTHRU_PTYPE)pstData;
			pstRptMeta = (META_PTYPE) (pstPTData->pstMetaData);
			iTotCnt = pstRptMeta->iTotCnt;

			iTotCnt += sizeof(genReqLst) / KEYVAL_SIZE;

			bAddFlds = PAAS_TRUE;
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		curPtr = listPtr;

		/* Fill the general request details */
		iFxn = SSI_RPT;
		iCnt = fillGenReqData(curPtr, iFxn, cmd);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill general req details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		//CID 67385 (#1 of 1): Logically dead code (DEADCODE). T_RaghavendranR1. Below if statement not required. Commenting it.
		/*if(bAddLPFlds == PAAS_TRUE)
		{
			// Fill the general payment details

			iTempTotCnt = sizeof(pymtGenReqLst) / KEYVAL_SIZE;

			// Start the population of data for this key list
			for(iCnt = 0; iCnt < iTempTotCnt; iCnt++)
			{
				memcpy(&curPtr[iCnt], &pymtGenReqLst[iCnt], KEYVAL_SIZE);

				if(strcmp(curPtr[iCnt].key, "PAYMENT_TYPE") == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Payment Type = %d", __FUNCTION__, pstPTData->iPymtType);
					APP_TRACE(szDbgMsg);

					if( (pstPTData->iPymtType >= 0) && (pstPTData->iPymtType < MAX_PYMT) )
					{
						iLen = strlen(szPymtType[pstPTData->iPymtType]);
						if(iLen > 0)
						{
							tmpPtr = (char *) malloc(iLen + 1);
							if(tmpPtr != NULL)
							{
								memset(tmpPtr, 0x00, iLen + 1);
								strcpy(tmpPtr, szPymtType[pstPTData->iPymtType]);
								debug_sprintf(szDbgMsg, "%s: Payment Type = %s", __FUNCTION__, szPymtType[pstPTData->iPymtType]);
								APP_TRACE(szDbgMsg);
							}
						}
					}
				}
				curPtr[iCnt].value = tmpPtr;
				tmpPtr = NULL;
			}

			curPtr += iCnt;

			// Fill the card details
			iCnt = fillCardDtls(curPtr, pstPTData->pstCardDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill card details",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}*/

		/* Fill the additional report fields in the pass thru request */
		if(bAddFlds == PAAS_TRUE)
		{
			if((cmd != SSI_DUPCHK) && (cmd != SSI_CUTOVER) && (cmd != SSI_SITETOTALS) && (cmd != SSI_LASTTRAN))
			{
				iCnt = fillGenRptFlds(curPtr);
				if(iCnt < 0)
				{
					debug_sprintf(szDbgMsg, "%s: Failed to add additional rpt flds"
																	, __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				curPtr += iCnt;
			}

			if(pstRptMeta->keyValList != NULL)
			{
				iCnt = pstRptMeta->iTotCnt;

				memcpy(curPtr, pstRptMeta->keyValList, iCnt * KEYVAL_SIZE);

				/* Free any meta data information from this local block */
				free(pstRptMeta->keyValList);
				memset(pstRptMeta, 0x00, METADATA_SIZE);
			}
		}

		debug_sprintf(szDbgMsg, "%s: Element Count = [%d]", __FUNCTION__, iCnt);
		APP_TRACE(szDbgMsg);

		/* Update the meta data with the list information */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	if( (rv != SUCCESS) && (listPtr != NULL) )
	{
		free(listPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDevReqMetaData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getDevReqMetaData(META_PTYPE pstMeta, DEVTRAN_PTYPE pstData, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		switch(iCmd)
		{

		case SSI_CREDITAPP:

			iTotCnt += sizeof(creditAppnReqLst) / KEYVAL_SIZE;

			/* Allocate memory for the list */
			listPtr = (KEYVAL_PTYPE) malloc(KEYVAL_SIZE * iTotCnt);
			if(listPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Memory allocation FAILED",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			/* Build the list */
			memset(listPtr, 0x00, KEYVAL_SIZE * iTotCnt);

			/* Fill the list */
			iCnt = fillCreditAppReqData(listPtr, &pstData->stCreditAppDtls, SSI_DEV, iCmd);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill general req details",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Element Count = [%d]", __FUNCTION__, iCnt);
		APP_TRACE(szDbgMsg);

		/* Update the meta data with the list information */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	if( (rv != SUCCESS) && (listPtr != NULL) )
	{
		free(listPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillAdminReqData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillAdminReqData(KEYVAL_PTYPE curPtr)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	int		iDevModel		= 0;
	char *	tmpPtr			= NULL;
	char *	szCfgVal[4]		= { NULL, NULL, NULL, NULL };
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Get the generic device and merchant related values, to be populated in
	 * the message */
	getGenDevPymtDtls(szCfgVal, &iDevModel);

	iTotCnt = sizeof(authReqLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &authReqLst[iCnt], KEYVAL_SIZE);
		tmpPtr = NULL;

		if(strcmp(curPtr[iCnt].key, "DEVICEKEY") == SUCCESS)
		{
			iLen = strlen(szCfgVal[1]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szCfgVal[1]);
				}
				curPtr[iCnt].valType = SINGLETON;
			}
		}
		else if( (strcmp(curPtr[iCnt].key, "TERMINAL") == SUCCESS) ||
				 (strcmp(curPtr[iCnt].key, "DEVTYPE") == SUCCESS) )
		{
			iLen = strlen(szCfgVal[3]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szCfgVal[3]);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SERIALNUM") == SUCCESS)
		{
			iLen = strlen(szCfgVal[2]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szCfgVal[2]);
				}
			}
		}
		
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillGenReqData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillBasketReqDtls(KEYVAL_PTYPE curPtr, char* szBaskMsg, int iBaskSize)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	char *	tmpPtr			= NULL;
	char	plType[5]		= "XML";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);


	iTotCnt = sizeof(basketReqLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &basketReqLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "PLTYPE") == SUCCESS)
		{
			tmpPtr = (char *) malloc(sizeof(plType));
			if (tmpPtr!= NULL)
			{
				memset(tmpPtr, 0x00, sizeof(plType));
				strcpy(tmpPtr, plType);
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DESTINATION") == SUCCESS)
		{
			tmpPtr = (char *) malloc(2);
			if(tmpPtr != NULL)
			{
				memset(tmpPtr, 0x00, 2);
				sprintf(tmpPtr, "%d", getBAPIDestId());
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PAYLOAD") == SUCCESS)
		{
			tmpPtr = (char *) malloc(iBaskSize + 20);
			if(tmpPtr != NULL)
			{
				memset(tmpPtr, 0x00, iBaskSize + 20);
				strcpy(tmpPtr, szBaskMsg);
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillBasketGenReqDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillBasketGenReqDtls(KEYVAL_PTYPE curPtr)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	int		iDevModel		= 0;
	char *	szCfgVal[4]		= { NULL, NULL, NULL, NULL };
	char *	tmpPtr			= NULL;
	char	command[20]		= "PassThrough";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Get the generic deive and merchant related values, to be populated in
	 * the message */
	getGenDevPymtDtls(szCfgVal, &iDevModel);

	iTotCnt = sizeof(genReqLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 1; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt-1], &genReqLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt-1].key, "COMMAND") == SUCCESS)
		{
			tmpPtr = (char *) malloc(sizeof(command) + 1);
			memset(tmpPtr, 0x00, sizeof(command) + 1);
			strcpy(tmpPtr, command);
		}
		else if(strcmp(curPtr[iCnt-1].key, "CLIENT_ID") == SUCCESS)
		{
			iLen = strlen(szCfgVal[0]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szCfgVal[0]);
				}
			}
		}
		else if(strcmp(curPtr[iCnt - 1].key, "DEVICEKEY") == SUCCESS)
		{
			iLen = strlen(szCfgVal[1]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szCfgVal[1]);
				}
			}
		}
		else if(strcmp(curPtr[iCnt - 1].key, "SERIAL_NUM") == SUCCESS)
		{
			iLen = strlen(szCfgVal[2]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szCfgVal[2]);
				}
			}
		}
		else if(strcmp(curPtr[iCnt - 1].key, "DEVTYPE") == SUCCESS)
		{
			iLen = strlen(szCfgVal[3]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szCfgVal[3]);
				}
			}
		}

		curPtr[iCnt - 1].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt - 1);
	APP_TRACE(szDbgMsg);

	return iCnt - 1;
}

/*
 * ============================================================================
 * Function Name: fillGenReqData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillGenReqData(KEYVAL_PTYPE curPtr, int fxn, int cmd)
{
	int					iCnt			= 0;
	int					iTotCnt			= 0;
	int					iLen			= 0;
	int					iDevModel		= 0;
	char *				szCfgVal[4]		= { NULL, NULL, NULL, NULL };
	char *				tmpPtr			= NULL;
	static	char		szAcc[8]		= "";
	static	char		szSite[5]		= "";
	static	char		szTerm[5]		= "";
	static PAAS_BOOL	bClientIDParsed	= PAAS_FALSE;
	static PAAS_BOOL	bFirstTime		= PAAS_TRUE;
	int					iHostProtocol	= getHostProtocolFormat();
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Get the generic deive and merchant related values, to be populated in
	 * the message */
	getGenDevPymtDtls(szCfgVal, &iDevModel);

	iTotCnt = sizeof(genReqLst) / KEYVAL_SIZE;

	if(iHostProtocol == HOST_PROTOCOL_UGP && bClientIDParsed == PAAS_FALSE && cmd != SSI_SETUP)
	{
		/* Client ID is comprised of An ACCOUNT number, a SITE number, and a TERMINAL number respectively.
		 * The SITE and TERMINAL numbers are padded with leading zeros, if those numbers are fewer than four digits in length.
		 * For e.g., 5400110001 is CLIENT ID.
		 * Account is 	54 (No padding needed, It is in the starting. Should be atleast 1 char long)
		 * Site is 		11 (Should be always of 4 chars, if not, left padded by zeros)
		 * Terminal 	1  (Should be always of 4 chars, if not, left padded by zeros)
		 */
		//CLIENT ID is stored in szCfgVal[0]
		iLen = strlen(szCfgVal[0]);

		if(iLen < 9)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR:: ClientID is only %d long, Need to be atleast 9", __FUNCTION__, iLen);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		memset(szAcc, 0x00, 8);
		memset(szSite, 0x00, 5);
		memset(szTerm, 0x00, 5);

		//Copying Acc No. from Starting to 8th from last as last 8 are for TERM and SITE fixed
		strncpy(szAcc, szCfgVal[0], iLen - 8);
		strncpy(szSite, szCfgVal[0] + iLen - 8, 4);
		strncpy(szTerm, szCfgVal[0] + iLen - 4, 4);

		debug_sprintf(szDbgMsg, "%s: UGP ClientID[%s] Acc[%s] Site[%s] Term[%s]", __FUNCTION__, szCfgVal[0], szAcc, szSite, szTerm);
		APP_TRACE(szDbgMsg);

		bClientIDParsed = PAAS_TRUE;
	}

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &genReqLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "FUNCTION_TYPE") == SUCCESS)
		{
			iLen = strlen(szSSIFxn[fxn]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szSSIFxn[fxn]);
					debug_sprintf(szDbgMsg, "%s: Function Type = %s", __FUNCTION__, szSSIFxn[fxn]);
					APP_TRACE(szDbgMsg);

				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "COMMAND") == SUCCESS)
		{
			if(iHostProtocol == HOST_PROTOCOL_UGP && cmd == SSI_CREDIT)
			{
				cmd = SSI_REFUND;
			}

			if(getPAPymtTypeFormat() == HOST_PROTOCOL_SSI && bFirstTime)
			{
				szSSICmd[SSI_PAYACCOUNT] = "PAYACCOUNT";
				bFirstTime = PAAS_FALSE;
			}

			/* Set the function and command as PAYEMNT and Gift command respectively for the
			 * SSI Gift transaction */
			if(cmd == SSI_ACTIVATE && isSVSNonDenominatedEnabled() == PAAS_TRUE)
			{
				cmd = SSI_REGISTER;
			}

			iLen = strlen(szSSICmd[cmd]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szSSICmd[cmd]);
					debug_sprintf(szDbgMsg, "%s: Command Type = %s", __FUNCTION__, szSSICmd[cmd]);
					APP_TRACE(szDbgMsg);
				}
			}

		}
		else if(strcmp(curPtr[iCnt].key, "DEVICEKEY") == SUCCESS)
		{
			iLen = strlen(szCfgVal[1]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szCfgVal[1]);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SERIAL_NUM") == SUCCESS)
		{
			iLen = strlen(szCfgVal[2]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szCfgVal[2]);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DEVTYPE") == SUCCESS)
		{
			iLen = strlen(szCfgVal[3]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szCfgVal[3]);
				}
			}
		}
		else if(iHostProtocol == HOST_PROTOCOL_SSI)
		{
			if(strcmp(curPtr[iCnt].key, "CLIENT_ID") == SUCCESS)
			{
				iLen = strlen(szCfgVal[0]);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, szCfgVal[0]);
					}
				}
			}
		}
		else if(iHostProtocol == HOST_PROTOCOL_UGP)
		{
			if(strcmp(curPtr[iCnt].key, "ACCOUNT") == SUCCESS)
			{
				iLen = strlen(szAcc);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, szAcc);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "SITE") == SUCCESS)
			{
				iLen = strlen(szSite);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, szSite);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "TERM") == SUCCESS)
			{
				iLen = strlen(szTerm);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, szTerm);
					}
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillSessionDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillSessionDtls(KEYVAL_PTYPE curPtr, SESSDTLS_PTYPE pstSess)
{
	int			iCnt			= 0;
	int			iTotCnt			= 0;
	int			iLen			= 0;
	char *		tmpPtr			= NULL;
	int			iHostProtocol	= getHostProtocolFormat();
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(sessDtlsLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &sessDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "INVOICE") == SUCCESS)
		{
			iLen = strlen(pstSess->szInvoice);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSess->szInvoice);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "LANE") == SUCCESS)
		{
			iLen = strlen(pstSess->szLaneNo);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSess->szLaneNo);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PURCHASE_ID") == SUCCESS)
		{
			iLen = strlen(pstSess->szPurchaseId);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSess->szPurchaseId);
				}
			}
		}
		else if(iHostProtocol == HOST_PROTOCOL_SSI)
		{
			if(strcmp(curPtr[iCnt].key, "STORE_NUM") == SUCCESS)
			{
				iLen = strlen(pstSess->szStoreNo);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstSess->szStoreNo);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "CASHIER_NUM") == SUCCESS)
			{
				iLen = strlen(pstSess->szCashierId);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstSess->szCashierId);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "SERVER_ID") == SUCCESS)
			{
				iLen = strlen(pstSess->szServerId);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstSess->szServerId);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "SHIFT_ID") == SUCCESS)
			{
				iLen = strlen(pstSess->szShiftId);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstSess->szShiftId);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "TABLE_NUM") == SUCCESS)
			{
				iLen = strlen(pstSess->szTableNo);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstSess->szTableNo);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "BUSINESSDATE") == SUCCESS)
			{
				iLen = strlen(pstSess->szBusinessDate);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstSess->szBusinessDate);
					}
				}
			}
		}
		else if(iHostProtocol == HOST_PROTOCOL_UGP)
		{
			if(strcmp(curPtr[iCnt].key, "CLERK_ID") == SUCCESS)
			{
				iLen = strlen(pstSess->szCashierId);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstSess->szCashierId);
					}
				}
			}
		}


		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillAmtDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillAmtDtls(KEYVAL_PTYPE curPtr, AMTDTLS_PTYPE pstAmtDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stAmtLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stAmtLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "TRANS_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstAmtDtls->tranAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->tranAmt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TIP_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstAmtDtls->tipAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->tipAmt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CASHBACK_AMNT") == SUCCESS)
		{
			iLen = strlen(pstAmtDtls->cashBackAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->cashBackAmt);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillDCCDtls
 *
 * Description	: Filling the required details for dcc
 *
 * Input Params	: Card Details structure and current ptr
 *
 * Output Params:
 * ============================================================================
 */
static int fillDCCDtls(KEYVAL_PTYPE curPtr, DCCDTLS_PTYPE pstDccDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	char	szTemp[5]		= "";
	char *	tmpPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(dccDtlsLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &dccDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "CARD_ID") == SUCCESS)
		{
			if(strlen(pstDccDtls->szCardID))
			{
				tmpPtr = strdup(pstDccDtls->szCardID);
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TRANS_AMOUNT") == SUCCESS)
		{
			if(strlen(pstDccDtls->szTranAmt))
			{
				tmpPtr = strdup(pstDccDtls->szTranAmt);
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DCC_IND") == SUCCESS)
		{
			if(pstDccDtls->iDCCInd)
			{
				sprintf(szTemp, "%d", pstDccDtls->iDCCInd);
				tmpPtr = strdup(szTemp);
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DCC_TRANS_AMOUNT") == SUCCESS)
		{
			if(strlen(pstDccDtls->szFgnAmount))
			{
				tmpPtr = strdup(pstDccDtls->szFgnAmount);
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DCC_CONV_RATE") == SUCCESS)
		{
			if(strlen(pstDccDtls->szExchngRate))
			{
				tmpPtr = strdup(pstDccDtls->szExchngRate);
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DCC_CURR_CODE") == SUCCESS)
		{
			if(strlen(pstDccDtls->szFgnCurCode))
			{
				tmpPtr = strdup(pstDccDtls->szFgnCurCode);
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillCardDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillCardDtls(KEYVAL_PTYPE curPtr, CARDDTLS_PTYPE pstCardDtls)
{
	int		iCnt			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCardDtls->bManEntry == PAAS_TRUE)
	{
		iCnt = fillManCardData(curPtr, pstCardDtls);
	}
	else
	{
		iCnt = fillSwipedCardData(curPtr, pstCardDtls);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillManCardData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillManCardData(KEYVAL_PTYPE curPtr, CARDDTLS_PTYPE pstCardDtls)
{
	int			iLen				= 0;
	int			iEncType			= 0;
	int			iTotCnt				= 0;
	int			iCnt				= 0;
	char *		tmpPtr				= NULL;
	char *		szProcessorId		= NULL;
	PAAS_BOOL	iRCNashProcessor	= PAAS_FALSE;
	int			iHostProtocol		= getHostProtocolFormat();
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCardDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Parameter is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iCnt;
	}

	/* Check for RCNASH Credit Processor, since we have to TA_FLAG = 1
	 * field for all the requests of this processor
	 */
	szProcessorId = getMerchantProcessor(PYMT_CREDIT);
	if(!strcasecmp(szProcessorId, RCNASH_PROCESSOR))
	{
		iRCNashProcessor = PAAS_TRUE;
	}
	/* Getting the encryption type*/
	//if(pstCardDtls != NULL && (strlen(pstCardDtls->szPAN) <= 0) )	// CID-67250,67297: 2-Feb-16: MukeshS3: Removing NULL check for pstCardDtls here
	if((strlen(pstCardDtls->szPAN) <= 0) )							// It is being done at beginning.
	{
		iEncType = 0;
	}
	else
	{
		iEncType = getEncryptionType();
	}

	iTotCnt = sizeof(cardDtlsLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &cardDtlsLst[iCnt], KEYVAL_SIZE);

		if((strcmp(curPtr[iCnt].key, "PRESENT_FLAG") == SUCCESS))
		{
			/* PRESENT FLAG is sent as 2 for manual card entry */
			iLen = strlen("2");
			tmpPtr = (char *) malloc(iLen + 1);
			if(tmpPtr != NULL)
			{
				strcpy(tmpPtr, "2");
			}
		}
		else if((strcmp(curPtr[iCnt].key, "CARDHOLDER") == SUCCESS))
		{
			iLen = strlen(pstCardDtls->szName);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCardDtls->szName);
				}
			}
		}
		else if((strcmp(curPtr[iCnt].key, "ACCT_NUM") == SUCCESS))
		{
			iLen= strlen(pstCardDtls->szEncBlob);
			if( (iEncType == RSA_ENC || iEncType == VSD_ENC) && (iLen > 0))		// encrypted Acct Number
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCardDtls->szEncBlob);
				}
			}
			else if(strlen(pstCardDtls->szClrPAN) > 0 && iEncType == VSD_ENC)	// clear PAN for only VSD encryption in case encrypted is not available
			{
				iLen= strlen(pstCardDtls->szClrPAN);
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCardDtls->szClrPAN);
				}
			}
			else
			{
				iLen = strlen(pstCardDtls->szPAN);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstCardDtls->szPAN);
					}
				}
			}
		}
		else if((strcmp(curPtr[iCnt].key, "CVV2") == SUCCESS))
		{
			iLen = strlen(pstCardDtls->szCVV);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCardDtls->szCVV);
				}
			}
		}
		else if((strcmp(curPtr[iCnt].key, "PAYMENT_MEDIA") == SUCCESS) &&
				(( isDHIEnabled() != PAAS_FALSE) ||  iRCNashProcessor != PAAS_FALSE) )
		{
			iLen = strlen(pstCardDtls->szPymtMedia);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCardDtls->szPymtMedia);
				}
			}
		}

		else if(iHostProtocol == HOST_PROTOCOL_SSI)
		{
			/* MukeshS3: 12-Sep-16: With ref. to PTMX-1580. Making Encryption Type field in SSI request independent of card based/non card based transactions
			 * Means, When a non card based transaction(mostly Completion) is performed(like using CARD TOKEN, CTROUD, VOID, REF_TROUD),
			 * will send 'ENCRYPTION_TYPE' field with 0(NONE) value.
			 * And, when a card based transaction is performed, will send ENCRYPTION_TYPE field with configured encryption scheme in terminal.
			 * This Tag ENCRYPTION_TYPE has been added to general payment request list which will be framed every time for each request
			 */
#if 0
			if((strcmp(curPtr[iCnt].key, "ENCRYPTION_TYPE") == SUCCESS))
			{
				iLen = 1;
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					switch(iEncType)
					{
					case RSA_ENC:
						strcpy(tmpPtr, "3");
						break;

					case VSP_ENC:
						strcpy(tmpPtr, "5");
						break;

					case VSD_ENC:
						strcpy(tmpPtr, "1");
						break;
					default:
						strcpy(tmpPtr, "0");
						debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
				}
			}
#endif
			if((strcmp(curPtr[iCnt].key, "EXP_MONTH") == SUCCESS))
			{
				iLen = strlen(pstCardDtls->szExpMon);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstCardDtls->szExpMon);
					}
				}
			}
			else if((strcmp(curPtr[iCnt].key, "EXP_YEAR") == SUCCESS))
			{
				iLen = strlen(pstCardDtls->szExpYear);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstCardDtls->szExpYear);
					}
				}
			}
			else if((strcmp(curPtr[iCnt].key, "ENCRYPTION_PAYLOAD") == SUCCESS))
			{
				iLen = strlen(pstCardDtls->szEncPayLoad);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstCardDtls->szEncPayLoad);
					}
				}
			}
			else if((strcmp(curPtr[iCnt].key, "INIT_VECTOR") == SUCCESS))
			{
				iLen = strlen(pstCardDtls->szVSDInitVector);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstCardDtls->szVSDInitVector);
					}
				}

			}
			else if((strcmp(curPtr[iCnt].key, "PINLESSDEBIT") == SUCCESS))
			{
				iLen = strlen(pstCardDtls->szPINlessDebit);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstCardDtls->szPINlessDebit);
					}
				}
			}
			else if((strcmp(curPtr[iCnt].key, "BARCODE") == SUCCESS))
			{
				iLen = strlen(pstCardDtls->szBarCode);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstCardDtls->szBarCode);
					}
				}
			}
			else if((strcmp(curPtr[iCnt].key, "PIN_CODE") == SUCCESS))
			{
				iLen = strlen(pstCardDtls->szPINCode);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstCardDtls->szPINCode);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "PAYMENT_CODE") == SUCCESS)
			{
				iLen = strlen(pstCardDtls->szPayPalPymtCode);
	#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s: pstCardDtls->szPayPalPymtCode [%s]", __FUNCTION__, pstCardDtls->szPayPalPymtCode);
				APP_TRACE(szDbgMsg);
	#endif
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					{
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, pstCardDtls->szPayPalPymtCode);
						}
					}
				}
			}
			else if((strcmp(curPtr[iCnt].key, "TA_FLAG") == SUCCESS))
			{
				if(iRCNashProcessor == PAAS_TRUE &&
					isIncludeTAFlagEnabled() == PAAS_TRUE &&
				((strlen(pstCardDtls->szPAN) > 0) || (strlen(pstCardDtls->szEMVEncBlob) > 0) || (strlen(pstCardDtls->szEncPayLoad) > 0) || (strlen(pstCardDtls->szEncBlob) > 0)))
				{
					iLen = 1;
					tmpPtr = (char *) malloc(iLen + 1);
					{
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, "1");
						}
					}
				}
			}
		}
		else if(iHostProtocol == HOST_PROTOCOL_UGP)
		{
			if((strcmp(curPtr[iCnt].key, "EXP_DATE") == SUCCESS))
			{
				iLen = strlen(pstCardDtls->szExpMon) + strlen(pstCardDtls->szExpYear);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstCardDtls->szExpMon);
						strcat(tmpPtr, pstCardDtls->szExpYear);
					}
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillSwipedCardData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillSwipedCardData(KEYVAL_PTYPE curPtr, CARDDTLS_PTYPE pstCardDtls)
{
	int			iLen				= 0;
//	int			iEnc				= 0;
	int			iTrkNo				= -1;
	int			iTotCnt				= 0;
	int			iCnt				= 0;
	PAAS_BOOL	iRCNashProcessor	= PAAS_FALSE;
	char *		tmpPtr				= NULL;
	char *      szProcessorId		= NULL;
	int			iHostProtocol		= getHostProtocolFormat();
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCardDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Parameter is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iCnt;
	}

	iTotCnt = sizeof(cardDtlsLst) / KEYVAL_SIZE;

	/* Check for RCNASH Credit Processor, since we have to TA_FLAG = 1
	 * field for all the requests of this processor
	 */
	szProcessorId = getMerchantProcessor(PYMT_CREDIT);
	if(!strcasecmp(szProcessorId, RCNASH_PROCESSOR))
	{
		iRCNashProcessor = PAAS_TRUE;
	}

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &cardDtlsLst[iCnt], KEYVAL_SIZE);

		if((strcmp(curPtr[iCnt].key, "PRESENT_FLAG") == SUCCESS))
		{
			/* PRESENT FLAG would be sent as 3 for MSR cards and as 4 for RFID
			 * or NFC cards */
			switch(pstCardDtls->iCardSrc)
			{
			case CRD_MSR:
				iLen = strlen("3");
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "3");
					}
				}

				break;

			case CRD_NFC:
			case CRD_RFID:
			case CRD_EMV_MSD_CTLS:
				iLen = strlen("4");
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "4");
					}
				}

				break;
			case CRD_EMV_CT:
				iLen = strlen("5");
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "5");
					}
				}

				break;
			case CRD_EMV_CTLS:
				iLen = strlen("6");
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "6");						
					}
				}

				break;
			case CRD_EMV_FALLBACK_MSR:
				iLen = strlen("7");
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "7");
					}
				}

				break;				
			default:
				debug_sprintf(szDbgMsg,"%s: Unknown card source[%d]",__FUNCTION__, pstCardDtls->iCardSrc);
				APP_TRACE(szDbgMsg);
				debug_sprintf(szDbgMsg,"%s: Should not come here",__FUNCTION__);
				APP_TRACE(szDbgMsg);


				break;
			}
		}
		else if((strcmp(curPtr[iCnt].key, "CARDHOLDER") == SUCCESS))
		{
			iLen = strlen(pstCardDtls->szName);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCardDtls->szName);
				}
			}
		}
		else if((strcmp(curPtr[iCnt].key, "TRACK_DATA") == SUCCESS) && strlen(pstCardDtls->szEMVEncBlob) == 0)
		{
			iLen = strlen(pstCardDtls->szTrackData);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCardDtls->szTrackData);
				}
			}
		}
		else if((strcmp(curPtr[iCnt].key, "PAYMENT_MEDIA") == SUCCESS) &&
				(( isDHIEnabled() != PAAS_FALSE) ||  iRCNashProcessor != PAAS_FALSE) )
		{
			iLen = strlen(pstCardDtls->szPymtMedia);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCardDtls->szPymtMedia);
				}
			}
		}

		else if(iHostProtocol == HOST_PROTOCOL_SSI)
		{
			/* MukeshS3: 12-Sep-16: With ref. to PTMX-1580. Making Encryption Type field in SSI request independent of card based/non card based transactions
			 * Means, When a non card based transaction(mostly Completion) is performed(like using CARD TOKEN, CTROUD, VOID, REF_TROUD),
			 * will send 'ENCRYPTION_TYPE' field with 0(NONE) value.
			 * And, when a card based transaction is performed, will send ENCRYPTION_TYPE field with configured encryption scheme in terminal.
			 * This Tag ENCRYPTION_TYPE has been added to general payment request list which will be framed every time for each request
			 */
#if 0
			debug_sprintf(szDbgMsg, "%s: Enc type %d", __FUNCTION__, pstCardDtls->iEncType);
			APP_TRACE(szDbgMsg);
			if((strcmp(curPtr[iCnt].key, "ENCRYPTION_TYPE") == SUCCESS) && pstCardDtls->iEncType != -1)
			{
				iLen = 1;
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					//iEnc = getEncryptionType();
					iEnc = pstCardDtls->iEncType;
					switch(iEnc)
					{
					case RSA_ENC:
						strcpy(tmpPtr, "3");
						break;

					case VSP_ENC:
						strcpy(tmpPtr, "5");
						break;
					case VSD_ENC:
						strcpy(tmpPtr, "1");
						break;
					default:
						strcpy(tmpPtr, "0");
						debug_sprintf(szDbgMsg, "%s: Should not come here Enc type", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
				}
			}
#endif
			if((strcmp(curPtr[iCnt].key, "PINLESSDEBIT") == SUCCESS))
			{
				iLen = strlen(pstCardDtls->szPINlessDebit);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstCardDtls->szPINlessDebit);
					}
				}
			}
			else if( (strcmp(curPtr[iCnt].key, "TRACK_INDICATOR") == SUCCESS ) &&
					 ( isDHIEnabled() != PAAS_FALSE ) )
			{
				/* If DHI(Direct Host Interface) is enabled, This field is included in the request.
				 * If It's SAF transaction then its value will be 1 or else 0.
				 * */
				iTrkNo = pstCardDtls->iTrkNo;
				if(iTrkNo != -1)
				{
					iLen = 1;
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						switch(iTrkNo)
						{
						case TRACK1_INDICATOR:
							strcpy(tmpPtr, "1");
							break;

						case TRACK2_INDICATOR:
							strcpy(tmpPtr, "2");
							break;

						case TRACK3_INDICATOR:
							strcpy(tmpPtr, "3");
							break;

						case NOTRACK_INDICATOR:
							if(pstCardDtls != NULL && ((pstCardDtls->iCardSrc == CRD_EMV_CT) || (pstCardDtls->iCardSrc == CRD_EMV_CTLS )))
							{
								strcpy(tmpPtr, "2");
							}
							else
							{
								free(tmpPtr);
								tmpPtr = NULL;
							}
							break;

						default:
							free(tmpPtr);
							tmpPtr = NULL;
							debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							break;
						}
					}
				}
			}
			else if((strcmp(curPtr[iCnt].key, "ENCRYPTION_PAYLOAD") == SUCCESS))
			{
				iLen = strlen(pstCardDtls->szEncPayLoad);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstCardDtls->szEncPayLoad);
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Enc Payload is not present", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

			}
			else if((strcmp(curPtr[iCnt].key, "INIT_VECTOR") == SUCCESS))
			{
				iLen = strlen(pstCardDtls->szVSDInitVector);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstCardDtls->szVSDInitVector);
					}
				}
			}
			else if((strcmp(curPtr[iCnt].key, "EXP_MONTH") == SUCCESS))
			{
				/*
				 * Include Exp Month if track data is not present
				 * this is pass the value received from POS
				 */
				if(strlen(pstCardDtls->szTrackData) == 0)
				{
					iLen = strlen(pstCardDtls->szExpMon);
					if(iLen > 0)
					{
						tmpPtr = (char *) malloc(iLen + 1);
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, pstCardDtls->szExpMon);
						}
					}
				}
			}
			else if((strcmp(curPtr[iCnt].key, "EXP_YEAR") == SUCCESS))
			{
				/*
				 * Include Exp Year if track data is not present
				 * this is pass the value received from POS
				 */
				if(strlen(pstCardDtls->szTrackData) == 0 && (strlen(pstCardDtls->szEMVEncBlob) == 0) )
				{
					iLen = strlen(pstCardDtls->szExpYear);
					if(iLen > 0)
					{
						tmpPtr = (char *) malloc(iLen + 1);
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, pstCardDtls->szExpYear);
						}
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "PAYMENT_CODE") == SUCCESS)
			{
				iLen = strlen(pstCardDtls->szPayPalPymtCode);
#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s: pstCardDtls->szPayPalPymtCode [%s]", __FUNCTION__, pstCardDtls->szPayPalPymtCode);
				APP_TRACE(szDbgMsg);
#endif
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					{
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, pstCardDtls->szPayPalPymtCode);
						}
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "EMV_ENCRYPTED_BLOB") == SUCCESS)
			{
				iLen = strlen(pstCardDtls->szEMVEncBlob);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					{
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, pstCardDtls->szEMVEncBlob);
						}
					}
				}
			}
			else if((strcmp(curPtr[iCnt].key, "TA_FLAG") == SUCCESS))
			{
				if(iRCNashProcessor == PAAS_TRUE &&
					isIncludeTAFlagEnabled() == PAAS_TRUE &&
				((strlen(pstCardDtls->szTrackData) > 0) || (strlen(pstCardDtls->szEMVEncBlob) > 0) || (strlen(pstCardDtls->szEncPayLoad) > 0) || (strlen(pstCardDtls->szEncBlob) > 0)))
				{
					iLen = 2;
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, "1");
					}

				}
			}
			else if(strcmp(curPtr[iCnt].key, "PAYPASS_TYPE") == SUCCESS)
			{
				iLen = strlen(pstCardDtls->szPayPassType);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					{
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, pstCardDtls->szPayPassType);
						}
					}
				}
			}
		}
		else if (iHostProtocol == HOST_PROTOCOL_UGP)
		{
			if((strcmp(curPtr[iCnt].key, "EXP_DATE") == SUCCESS))
			{
				/*
				 * Include Exp date if track data is not present
				 * this is pass the value received from POS
				 */
				if(strlen(pstCardDtls->szTrackData) == 0)
				{
					iLen = strlen(pstCardDtls->szExpMon) + strlen(pstCardDtls->szExpYear);
					if(iLen > 0)
					{
						tmpPtr = (char *) malloc(iLen + 1);
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, pstCardDtls->szExpMon);
							strcat(tmpPtr, pstCardDtls->szExpYear);
						}
					}
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillEmvKeyLoadDtls
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillEmvKeyLoadDtls(KEYVAL_PTYPE curPtr, EMVKEYLOAD_INFO_PTYPE pstEmvKeyLoadInfo)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stEmvKeyLoadLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stEmvKeyLoadLst[iCnt], KEYVAL_SIZE);

		if((strcmp(curPtr[iCnt].key, "DOWNLOAD_STATUS") == SUCCESS))
		{
			iLen = strlen(pstEmvKeyLoadInfo->szKeyLoadStatus);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstEmvKeyLoadInfo->szKeyLoadStatus);
				}
			}
		}
		else if((strcmp(curPtr[iCnt].key, "REFERENCE") == SUCCESS))
		{
			iLen = strlen(pstEmvKeyLoadInfo->szReference);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstEmvKeyLoadInfo->szReference);
				}
			}
		}
		else if((strcmp(curPtr[iCnt].key, "TROUTD") == SUCCESS))
		{
			iLen = strlen(pstEmvKeyLoadInfo->szTROUTD);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstEmvKeyLoadInfo->szTROUTD);
				}
			}
		}		
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillEmvDtls
 *
 * Description	: To be done
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillEmvDtls(KEYVAL_PTYPE curPtr, CARDDTLS_PTYPE pstCardDtls)
{
	int			iCnt			= 0;
	int			iTotCnt			= 0;
	int			iLen			= 0;
	char *		tmpPtr			= NULL;
	int			iHostProtocol	= getHostProtocolFormat();
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stEmvReqLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stEmvReqLst[iCnt], KEYVAL_SIZE);
		if(strcmp(curPtr[iCnt].key, "EMV_TAGS") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->szEMVTags);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCardDtls->szEMVTags);
				}
			}
		}
		if(iHostProtocol == HOST_PROTOCOL_SSI)
		{
			if(strcmp(curPtr[iCnt].key, "EMV_REVERSAL_TYPE") == SUCCESS)
			{
				iLen = strlen(pstCardDtls->szEmvReversalType);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstCardDtls->szEmvReversalType);
					}
				}
			}
		}
		else if(iHostProtocol == HOST_PROTOCOL_SSI)
		{

		}


		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}



/*
 * ============================================================================
 * Function Name: fillPINDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillPINDtls(KEYVAL_PTYPE curPtr, PINDTLS_PTYPE pstPINDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(pinDtlsLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &pinDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "PIN_BLOCK") == SUCCESS)
		{
			iLen = strlen(pstPINDtls->szPIN);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstPINDtls->szPIN);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "KEY_SERIAL_NUMBER") == SUCCESS)
		{
			iLen = strlen(pstPINDtls->szKSN);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstPINDtls->szKSN);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillSignDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillSignDtls(KEYVAL_PTYPE curPtr, SIGDTLS_PTYPE pstSignDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//CID 67196 (#1 of 1): Dereference after null check (FORWARD_NULL) T_RaghavendranR1
	if(pstSignDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Signature struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(sigDtlsLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &sigDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "MIMETYPE") == SUCCESS)
		{

			if((iLen = strlen(pstSignDtls->szMime)) > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSignDtls->szMime);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SIGNATUREDATA") == SUCCESS)
		{
			if(	(pstSignDtls->szSign != NULL) &&
				(iLen = strlen(pstSignDtls->szSign)) > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					memcpy(tmpPtr, pstSignDtls->szSign, iLen);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillLVL2Dtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillLVL2Dtls(KEYVAL_PTYPE curPtr, LVL2_PTYPE pstLvl2Dtls)
{
	int			iCnt			= 0;
	int			iTotCnt			= 0;
	int			iLen			= 0;
	char *		tmpPtr			= NULL;
	int			iHostProtocol	= getHostProtocolFormat();
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(lvl2DtlsLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &lvl2DtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "TAX_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstLvl2Dtls->taxAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLvl2Dtls->taxAmt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TAX_IND") == SUCCESS)
		{
			if(pstLvl2Dtls->taxInd != -1) //-1 is the default value, if it is other than -1 then this variable is set
			{
				tmpPtr = (char *) malloc(2);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, 2);
					sprintf(tmpPtr, "%d", pstLvl2Dtls->taxInd);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DISCOUNT_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstLvl2Dtls->discAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLvl2Dtls->discAmt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "FREIGHT_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstLvl2Dtls->freightAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLvl2Dtls->freightAmt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SHIP_FROM_ZIP_CODE") == SUCCESS)
		{
			iLen = strlen(pstLvl2Dtls->shipPostalcode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLvl2Dtls->shipPostalcode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "ALT_TAX_ID") == SUCCESS)
		{
			iLen = strlen(pstLvl2Dtls->alternateTaxId);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLvl2Dtls->alternateTaxId);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CUSTOMER_CODE") == SUCCESS)
		{
			iLen = strlen(pstLvl2Dtls->customerCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLvl2Dtls->customerCode);
				}
			}
		}
		else if(iHostProtocol == HOST_PROTOCOL_SSI)
		{
			if(strcmp(curPtr[iCnt].key, "CMRCL_TYPE") == SUCCESS)
			{
				tmpPtr = (char *) malloc(2);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, 2);
					sprintf(tmpPtr, "%c", pstLvl2Dtls->cmrclFlag);
					/*
					 * Praveen_P1: Following check is to fix JIRA- PTMX-7
					 * PTMX-7: CMRCL_TYPE tag present in all the request sent to SSI with empty value
					 */
					if(strlen(tmpPtr) == 0)
					{
						free(tmpPtr);
						tmpPtr = NULL;
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "DUTY_AMOUNT") == SUCCESS)
			{
				iLen = strlen(pstLvl2Dtls->dutyAmt);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstLvl2Dtls->dutyAmt);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "RETAIL_ITEM_DESC_1") == SUCCESS)
			{
				iLen = strlen(pstLvl2Dtls->prodDesc);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstLvl2Dtls->prodDesc);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "DEST_POSTAL_CODE") == SUCCESS)
			{
				iLen = strlen(pstLvl2Dtls->destPostalcode);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstLvl2Dtls->destPostalcode);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "DEST_COUNTRY_CODE") == SUCCESS)
			{
				iLen = strlen(pstLvl2Dtls->destCountryCode);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstLvl2Dtls->destCountryCode);
					}
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}
/*
 * ============================================================================
 * Function Name: fillChkDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillChkDtls(KEYVAL_PTYPE curPtr, CHKDTLS_PTYPE pstChkDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(chkDtlsLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &chkDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "MICR") == SUCCESS)
		{
			iLen = strlen(pstChkDtls->szMICR);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstChkDtls->szMICR);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "ABA_NUM") == SUCCESS)
		{
			iLen = strlen(pstChkDtls->szABANum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstChkDtls->szABANum);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "ACCT_NUM") == SUCCESS)
		{
			iLen = strlen(pstChkDtls->szAcctNum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstChkDtls->szAcctNum);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CHECK_NUM") == SUCCESS)
		{
			iLen = strlen(pstChkDtls->szChkNum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstChkDtls->szChkNum);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DL_STATE") == SUCCESS)
		{
			iLen = strlen(pstChkDtls->szDLState);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstChkDtls->szDLState);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DL_NUMBER") == SUCCESS)
		{
			iLen = strlen(pstChkDtls->szDLNumber);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstChkDtls->szDLNumber);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CHECK_TYPE") == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Check Type = %d", __FUNCTION__, pstChkDtls->iChkType);
			APP_TRACE(szDbgMsg);

			iLen = strlen(szCheckType[pstChkDtls->iChkType]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szCheckType[pstChkDtls->iChkType]);
					debug_sprintf(szDbgMsg, "%s: Check Type = %s", __FUNCTION__, szCheckType[pstChkDtls->iChkType]);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillCustInfoDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillCustInfoDtls(KEYVAL_PTYPE curPtr, CUSTINFODTLS_PTYPE pstCustInfoDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(custInfoDtlsLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &custInfoDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "CUSTOMER_STREET") == SUCCESS)
		{
			iLen = strlen(pstCustInfoDtls->szCustStreetName);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCustInfoDtls->szCustStreetName);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CUSTOMER_ZIP") == SUCCESS)
		{
			iLen = strlen(pstCustInfoDtls->szCustZipNum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCustInfoDtls->szCustZipNum);
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillFollowOnDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillFollowOnDtls(KEYVAL_PTYPE curPtr, FTRANDTLS_PTYPE pstFTranDtls, int iCommand, int iPaymentType)
{
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iLen			= 0;
	int				iExceptClientId	= 0;
	int				iHostProtocol	= getHostProtocolFormat();
	char *			tmpPtr			= NULL;
	char *			pszProcName		= NULL;
	PAAS_BOOL		iRCNashProcessor= PAAS_FALSE;
	VAL_LST_PTYPE 	pstLineItemptr	= NULL;
	KEYVAL_PTYPE	lineItemPtr		= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(followOnDtlsLst) / KEYVAL_SIZE;
	/* T_VinayS3: Checking for RCNASH Processor for PWC-RC Setup,
	 * in case we need to send additional fields such as TA_TOKEN
	 */
	pszProcName = getMerchantProcessor(PYMT_CREDIT);
	if(!strcasecmp(pszProcName, RCNASH_PROCESSOR))
	{
		iRCNashProcessor = PAAS_TRUE;
	}

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &followOnDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "CTROUTD") == SUCCESS)
		{
			/*
			 * For the DEBIT REFUND, we should not send the
			 * CTROUTD if it is present in the SCI request
			 */

			/*
			 * Need to Send the CTROUTD for debit refund for
			 * the processors TSYS(5411200010001)
			 * and HeartLand (10093100010001)
			 * Checking if they are these
			 */
			pszProcName = getMerchantProcessor(PROCESSOR_DEBIT);

			if ( (strcasecmp(pszProcName, "VISA") == SUCCESS) || (strcasecmp(pszProcName, "HPS") == SUCCESS))
			{
				debug_sprintf(szDbgMsg, "%s: Filling Ctroutd for debit refund proc Id is %s",
														__FUNCTION__, pszProcName);
				APP_TRACE(szDbgMsg);
				iExceptClientId = 1;
			}

			if( (iCommand != SSI_CREDIT) || (iPaymentType != PYMT_DEBIT) || iExceptClientId)
			{
				iLen = strlen(pstFTranDtls->szCTroutd);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstFTranDtls->szCTroutd);
					}
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "AUTH_CODE") == SUCCESS)
		{
			iLen = strlen(pstFTranDtls->szAuthCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstFTranDtls->szAuthCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "REFERENCE") == SUCCESS)
		{
			iLen = strlen(pstFTranDtls->szReference);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstFTranDtls->szReference);
				}
			}
		}


#if 0
		else if(strcmp(curPtr[iCnt].key, "TOKEN_SOURCE") == SUCCESS)
		{
			iLen = strlen(pstFTranDtls->szTokenSrc);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstFTranDtls->szTokenSrc);
				}
			}
		}
#endif
		else if(iHostProtocol == HOST_PROTOCOL_SSI)
		{
			if(strcmp(curPtr[iCnt].key, "REF_TROUTD") == SUCCESS)
			{
				iLen = strlen(pstFTranDtls->szTroutd);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstFTranDtls->szTroutd);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "CARD_TOKEN") == SUCCESS)
			{
				/* Check for RCNASH Processors which requires TA_TOKEN
				 * to be sent in the SSI Request instead of CARD_TOKEN
				 */
				if(iRCNashProcessor == PAAS_FALSE)
				{
					iLen = strlen(pstFTranDtls->szCardToken);
					if(iLen > 0)
					{
						tmpPtr = (char *) malloc(iLen + 1);
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, pstFTranDtls->szCardToken);
						}
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "TA_TOKEN") == SUCCESS)
			{
				if(iRCNashProcessor == PAAS_TRUE)
				{
					iLen = strlen(pstFTranDtls->szCardToken);
					if(iLen > 0)
					{
						tmpPtr = (char *) malloc(iLen + 1);
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, pstFTranDtls->szCardToken);
						}
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "BANK_USERDATA") == SUCCESS)
			{
				iLen = strlen(pstFTranDtls->szBankUserData);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstFTranDtls->szBankUserData);
					}
				}
			}
			else if( (strcmp(curPtr[iCnt].key, "SAF_APPROVALCODE") == SUCCESS) &&
					 (isDHIEnabled() == PAAS_TRUE ) )
			{
				iLen = strlen(pstFTranDtls->szSAFApprovalCode);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstFTranDtls->szSAFApprovalCode);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "RETAIL_ITEM_DESC_1") == SUCCESS)
			{
				iLen = strlen(pstFTranDtls->szReference);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstFTranDtls->szReference);
					}
				}
			}
		}
		else if(iHostProtocol == HOST_PROTOCOL_UGP)
		{
			if(strcmp(curPtr[iCnt].key, "REF_CTROUTD") == SUCCESS)
			{
				iLen = strlen(pstFTranDtls->szTroutd);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstFTranDtls->szTroutd);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "TKN_PAYMENT") == SUCCESS)
			{
				iLen = strlen(pstFTranDtls->szCardToken);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstFTranDtls->szCardToken);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "TKN_PROCESS") == SUCCESS)
			{
				/* Currently We default the TKN_PROCESS to 3, so that we receive Both Matching and Payment Token from UGP */
				iLen = strlen("3");
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, "3");
					}
				}
			}
			/*
			 * Praveen_P1: As per the requirement, we need to map REFERENCE to
			 * ITEM_DESC_1, currently we are doing this, once we support
			 * UGP fully, it has to go under UGP
			 */
			else if(strcmp(curPtr[iCnt].key, "LINE_ITEM") == SUCCESS &&
					iPaymentType == PYMT_PRIVATE)
			{
				iLen = strlen(pstFTranDtls->szReference);
				if(iLen > 0)
				{
					pstLineItemptr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
					if (pstLineItemptr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						return FAILURE;
					}
					memset(pstLineItemptr, 0X00, sizeof(VAL_LST_STYPE));

					lineItemPtr = (KEYVAL_PTYPE)malloc(iTotCnt * KEYVAL_SIZE);
					if (lineItemPtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						// CID-67375: 25-Jan-16: MukeshS3: free the memory allocated for pstLineItemptr before returning
						if(pstLineItemptr != NULL)
						{
							free(pstLineItemptr);
						}
						return FAILURE;
					}

					memcpy(&lineItemPtr[0], &lineItemDesc[0], KEYVAL_SIZE);

					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstFTranDtls->szReference);
					}

					lineItemPtr[0].value = tmpPtr;

					addKeyValDataNode(pstLineItemptr, lineItemPtr, 1);
					curPtr[iCnt].value = pstLineItemptr;
					tmpPtr = NULL;
				}
			}
		}

		// CID-67423: 1-Feb-16: MukeshS3, T_RaghavendranR1: tmpPtr is being assigned to the cur node for all the key value except LINE_ITEM.
		// For LINE_ITEM we assign it inside, where we come across LINE_ITEM key. This is done to fix CID 67423 coverity issue.
		if(tmpPtr != NULL)
		{
			curPtr[iCnt].value = tmpPtr;
			tmpPtr = NULL;
		}

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillOrigTranDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillOrigTranDtls(KEYVAL_PTYPE curPtr, PYMTTRAN_PTYPE pstPymtDtls)
{
	int			iCnt			= 0;
	int			iTotCnt			= 0;
	int			iLen			= 0;
	char *		tmpPtr			= NULL;
	int			iHostProtocol	= getHostProtocolFormat();
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(pymtOrigTranDateLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &pymtOrigTranDateLst[iCnt], KEYVAL_SIZE);

		if(iHostProtocol == HOST_PROTOCOL_SSI)
		{
			if(strcmp(curPtr[iCnt].key, "ORIG_TRANS_DATE") == SUCCESS)
			{
				iLen = strlen(pstPymtDtls->szOrigTranDt);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstPymtDtls->szOrigTranDt);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "ORIG_TRANS_TIME") == SUCCESS)
			{
				iLen = strlen(pstPymtDtls->szOrigTranTm);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstPymtDtls->szOrigTranTm);
					}
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillEBTTranDtls
 *
 * Description	: Fills the EBT Related fields to be sent to Host
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillEBTTranDtls(KEYVAL_PTYPE curPtr, EBTDTLS_PTYPE pstEBTDtls)
{
	int			iCnt			= 0;
	int			iTotCnt			= 0;
	int			iLen			= 0;
	char *		tmpPtr			= NULL;
	int			iHostProtocol	= getHostProtocolFormat();
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(ebtTransReqLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &ebtTransReqLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "EBT_TYPE") == SUCCESS)
		{
			iLen = strlen(pstEBTDtls->szEBTType);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstEBTDtls->szEBTType);
				}
			}
		}
		if(iHostProtocol == HOST_PROTOCOL_SSI)
		{
			if(strcmp(curPtr[iCnt].key, "EBTCASH_ELIGIBLE") == SUCCESS)
			{
				iLen = strlen(pstEBTDtls->szEBTCashElig);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstEBTDtls->szEBTCashElig);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "EBTSNAP_ELIGIBLE") == SUCCESS)
			{
				iLen = strlen(pstEBTDtls->szEBTSNAPElig);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstEBTDtls->szEBTSNAPElig);
					}
				}
			}
		}
		else if(iHostProtocol == HOST_PROTOCOL_UGP)
		{
			if(strcmp(curPtr[iCnt].key, "CASHBACK_AMNT") == SUCCESS)
			{
				iLen = strlen(pstEBTDtls->szEBTCashElig);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstEBTDtls->szEBTCashElig);
					}
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillFSATranDtls
 *
 * Description	: Fills the FSA Related fields to be sent to SSI Host
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillFSATranDtls(KEYVAL_PTYPE curPtr, FSADTLS_PTYPE pstFSADtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(FSATransReqLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &FSATransReqLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "AMOUNT_HEALTHCARE") == SUCCESS)
		{
			iLen = strlen(pstFSADtls->szAmtHealthCare);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstFSADtls->szAmtHealthCare);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "AMOUNT_PRESCRIPTION") == SUCCESS)
		{
			iLen = strlen(pstFSADtls->szAmtPres);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstFSADtls->szAmtPres);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "AMOUNT_VISION") == SUCCESS)
		{
			iLen = strlen(pstFSADtls->szAmtVision);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstFSADtls->szAmtVision);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "AMOUNT_CLINIC") == SUCCESS)
		{
			iLen = strlen(pstFSADtls->szAmtClinic);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstFSADtls->szAmtClinic);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "AMOUNT_DENTAL") == SUCCESS)
		{
			iLen = strlen(pstFSADtls->szAmtDental);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstFSADtls->szAmtDental);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillGenPymtDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillGenPymtDtls(KEYVAL_PTYPE curPtr, PYMTTRAN_PTYPE pstPymtDtls, int iCommand)
{
	int			iCnt					= 0;
	int			iTotCnt					= 0;
	int			iLen					= 0;
	int			iEnc					= 0;
	char *		tmpPtr					= NULL;
	char *		szTmp					= NULL;
	char		szDate[50]				= "";
	char		szTime[50]				= "";
	int			iHostProtocol	= getHostProtocolFormat();
	//char	szBatchTraceId[50]			= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(pymtGenReqLst) / KEYVAL_SIZE;

	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &pymtGenReqLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "PAYMENT_TYPE") == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Payment Type = %d", __FUNCTION__, pstPymtDtls->iPymtType);
			APP_TRACE(szDbgMsg);

			if( (pstPymtDtls->iPymtType >= 0) && (pstPymtDtls->iPymtType < MAX_PYMT) )
			{
				iLen = strlen(szPymtType[pstPymtDtls->iPymtType]);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);

					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, szPymtType[pstPymtDtls->iPymtType]);
						debug_sprintf(szDbgMsg, "%s: Payment Type = %s", __FUNCTION__, szPymtType[pstPymtDtls->iPymtType]);
						APP_TRACE(szDbgMsg);
					}
				}
			}
		}
		else if( (strcmp(curPtr[iCnt].key, "FORCE_FLAG") == SUCCESS) &&
				 (pstPymtDtls->iPymtType != PYMT_OTHER) )
		{
			if(pstPymtDtls->bForce == PAAS_TRUE)
			{
				szTmp = "TRUE";
			}
			else
			{
				szTmp = "FALSE";
			}

			if(iCommand == SSI_ACK) //We need not send this field for MESSAGE_ACK
			{
				szTmp = NULL;
			}

			if(szTmp != NULL)
			{
				iLen = strlen(szTmp);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, szTmp);
					}
				}
			}
		}
		else if( (strcmp(curPtr[iCnt].key, "BILLPAY") == SUCCESS) )
		{
			if(pstPymtDtls->bBillPay == PAAS_TRUE)
			{
				szTmp = "TRUE";
			}
			else
			{
				szTmp = NULL;
			}

			if(szTmp != NULL)
			{
				iLen = strlen(szTmp);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, szTmp);
					}
				}
			}
		}
		else if( (strcmp(curPtr[iCnt].key, "PARTIAL_AUTH") == SUCCESS) &&
				 (pstPymtDtls->iPymtType != PYMT_OTHER) )
		{		
			/* PARTIAL_AUTH flag should be set only if partialauth variable is set */
			/*
			 * Valid for CREDIT and DEBIT payment types only as mentioned in
			 * the SSI specifications
			 */
			if((isPartAuthEnabled() == PAAS_TRUE) && (iCommand == SSI_SALE || iCommand == SSI_PREAUTH) &&
			  ((pstPymtDtls->iPymtType == PYMT_CREDIT) || (pstPymtDtls->iPymtType == PYMT_DEBIT) || (pstPymtDtls->iPymtType == PYMT_EBT) ||
			  (pstPymtDtls->iPymtType == PYMT_GIFT && (strcasecmp("PYMT", (char *)getMerchantProcessor(PROCESSOR_GIFT)) != 0))))
			{
				szTmp = "1";
			}
			else
			{
				if( pstPymtDtls->iPymtType != PYMT_PRIVATE &&
						((iCommand == SSI_SALE || iCommand == SSI_PREAUTH)) )
				{
					szTmp = "0";
				}
				else //We need not send this field for other commands
				{
					szTmp = NULL;
				}
			}

			if(szTmp != NULL)
			{
				iLen = strlen(szTmp);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, szTmp);
					}
				}
			}
		}
		else if( (strcmp(curPtr[iCnt].key, "MODIFIER") == SUCCESS) &&
						 (pstPymtDtls->iPymtType == PYMT_GIFT) )
		{
			/*
			 * Praveen_P1: Fix for PTMX-193: MODIFIER element has to be set to NONSF
			 * to enable the partial auth with Valuelink processor
			 * if partial auth is set and payment type is Gift and processor is
			 * SVDOT then setting this field
			 *
			 */
			if((isPartAuthEnabled() == PAAS_TRUE) &&
				strcasecmp("SVDOT", (char *)getMerchantProcessor(PROCESSOR_GIFT)) == 0)
			{
				szTmp = "NONSF";
			}
			else
			{
				szTmp = NULL;
			}

			if(szTmp != NULL)
			{
				iLen = strlen(szTmp);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, szTmp);
					}
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PROMO_CODE") == SUCCESS &&
			   ((pstPymtDtls->iPymtType == PYMT_GIFT) || (pstPymtDtls->iPymtType == PYMT_PRIVATE) )) //Payment Type: Gift, Private Label and protocol is UGP
		{
			iLen = strlen(pstPymtDtls->szPromoCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				{
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstPymtDtls->szPromoCode);
					}
				}
			}
			else
			{
				if( pstPymtDtls->iPymtType == PYMT_PRIVATE &&
						getPAPymtTypeFormat() == HOST_PROTOCOL_UGP && //Including this default value only when protocol is set to UGP
						!(iCommand == SSI_BAL || iCommand == SSI_PAYACCOUNT || iCommand == SSI_VOID) ) //We should not send this field for these two commands
				{
					/*
					 * According to Requirement, need to default if not received form SCI
					 *Default to 000001 if not passed from POS when sending to UGP Gateway
					 *Currently defaulting for private label transactions only
					 */
					debug_sprintf(szDbgMsg, "%s: Promo Code is not present, defaulting to 000001", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					iLen = strlen("000001");
					tmpPtr = (char *) malloc(iLen + 1);
					{
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, "000001");
						}
					}
				}

			}
		}
		else if(strcmp(curPtr[iCnt].key, "CREDIT_PLAN_NBR") == SUCCESS &&
				(pstPymtDtls->iPymtType == PYMT_PRIVATE))					//Payment Type: Private Label
		{
			iLen = strlen(pstPymtDtls->szCreditPlanNBR);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				{
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstPymtDtls->szCreditPlanNBR);
					}
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "ORDER_DATETIME") == SUCCESS)
		{
			iLen = strlen(pstPymtDtls->szOrderDtTm);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				{
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstPymtDtls->szOrderDtTm);
					}
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PURCHASE_APR") == SUCCESS &&
			(pstPymtDtls->iPymtType == PYMT_PRIVATE)) //Payment Type: Private Label
		{
			iLen = strlen(pstPymtDtls->szPurchaseApr);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				{
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstPymtDtls->szPurchaseApr);
					}
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "APR_TYPE") == SUCCESS &&
			(pstPymtDtls->iPymtType == PYMT_PRIVATE))		// Payment Type:Private Label
		{
			iLen = strlen(pstPymtDtls->szAPRType);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				{
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstPymtDtls->szAPRType);
					}
				}
			}
		}
		else if(iHostProtocol == HOST_PROTOCOL_SSI)
		{
			if(strcmp(curPtr[iCnt].key, "PAYMENT_SUBTYPE") == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Payment Sub Type = %d", __FUNCTION__, pstPymtDtls->iPymtSubType);
				APP_TRACE(szDbgMsg);

				if( (pstPymtDtls->iPymtSubType >= 0) && (pstPymtDtls->iPymtSubType < MAX_SUBPYMT)
						&&   pstPymtDtls->iPymtType == PYMT_GIFT ) //Send this only if payment type is gift
				{
					iLen = strlen(szPymtSubType[pstPymtDtls->iPymtSubType]);
					if(iLen > 0)
					{
						tmpPtr = (char *) malloc(iLen + 1);
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, szPymtSubType[pstPymtDtls->iPymtSubType]);
							debug_sprintf(szDbgMsg, "%s: Payment Sub Type = %s", __FUNCTION__, szPymtSubType[pstPymtDtls->iPymtSubType]);
							APP_TRACE(szDbgMsg);
						}
					}
				}
			}
			else if( (strcmp(curPtr[iCnt].key, "PARTIAL_REDEMPTION_FLAG") == SUCCESS) &&
					 (pstPymtDtls->iPymtType != PYMT_OTHER) )
			{
				/* PARTIAL_REDEMPTION_FLAG flag should be set only if partialauth variable is set */
				/* Valid for PAYMENTTECH GIFT payment type only as mentioned in the SSI
				 * specifications */
				if((isPartAuthEnabled() == PAAS_TRUE) &&
				  (pstPymtDtls->iPymtType == PYMT_GIFT) && (strcasecmp("PYMT", (char *)getMerchantProcessor(PROCESSOR_GIFT)) == 0))
				{
					szTmp = "TRUE";
				}
				else
				{
					if(strcasecmp("PYMT", (char *)getMerchantProcessor(PROCESSOR_GIFT)) == 0)
					{
						szTmp = "FALSE";
					}
					else //Not Including this flag for other processors
					{
						szTmp = NULL;
					}
				}

				if(iCommand == SSI_ACK) //We need not send this field for MESSAGE_ACK
				{
					szTmp = NULL;
				}

				if(szTmp != NULL)
				{
					iLen = strlen(szTmp);
					if(iLen > 0)
					{
						tmpPtr = (char *) malloc(iLen + 1);
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, szTmp);
						}
					}
				}
			}
			else if( ( strcmp(curPtr[iCnt].key, "SAF_TRAN") == SUCCESS) &&
					 ( ( isDHIEnabled() == PAAS_TRUE ) || (isSAFIndReqdForSSIReq() == PAAS_TRUE) ) )
			{
				/* If DHI(Direct Host Interface) is enabled, This field is included in the request.
				 * If It's SAF transaction then its value will be 1 or else 0.
				 * */
				if( pstPymtDtls->bSAFTran == PAAS_TRUE )
				{
					szTmp = "1";
				}
				else
				{
					szTmp = "0";
				}

				iLen = strlen(szTmp);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, szTmp);
					}
				}
			}
			else if( ( strcmp(curPtr[iCnt].key, "VSP_REG") == SUCCESS) &&
					 ( isDHIEnabled() == PAAS_TRUE ) )
			{
				/* If DHI(Direct Host Interface) is enabled and VSP registration transaction, then
				 * this field is included in the request.
				 */
				if( pstPymtDtls->bVSPTran == PAAS_TRUE )
				{
					szTmp = "1";
				}
				else
				{
					szTmp = ""; //Don't add this field.
				}

				iLen = strlen(szTmp);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, szTmp);
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "BATCH_TRACE_ID") == SUCCESS)
			{
				iLen = strlen(pstPymtDtls->szBatchTraceId);
				if(iLen <= 0)
				{
					getUUID(pstPymtDtls->szBatchTraceId);
					iLen = strlen(pstPymtDtls->szBatchTraceId);
				}
				debug_sprintf(szDbgMsg, "%s: szBatchTraceId [%s]", __FUNCTION__, pstPymtDtls->szBatchTraceId);
				APP_TRACE(szDbgMsg); //Remove this later
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					{
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, pstPymtDtls->szBatchTraceId);
							debug_sprintf(szDbgMsg, "%s: BatchTraceId for current transaction = %s", __FUNCTION__, pstPymtDtls->szBatchTraceId);
							APP_TRACE(szDbgMsg);//Remove this later
						}
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "DATE") == SUCCESS)
			{
				if( !((getPAPymtTypeFormat() == HOST_PROTOCOL_UGP) && (pstPymtDtls->iPymtType == PYMT_PRIVATE)) )//Praveen_P1: For Private label transactions, PWC is not accepting this field
				{
					if(strlen(pstPymtDtls->szTranDt) > 0)
					{
						strcpy(szDate, pstPymtDtls->szTranDt);
					}
					else
					{
						getDate("MMDDYYYY", szDate);
					}
					iLen = strlen(szDate);
					if(iLen > 0)
					{
						strcpy(pstPymtDtls->szTranDt, szDate);
						tmpPtr = (char *) malloc(iLen + 1);
						{
							if(tmpPtr != NULL)
							{
								memset(tmpPtr, 0x00, iLen + 1);
								strcpy(tmpPtr, szDate);
								debug_sprintf(szDbgMsg, "%s: szDate for Current Transactions = %s", __FUNCTION__, szDate);
								APP_TRACE(szDbgMsg);
							}
						}
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "TIME") == SUCCESS)
			{
				if( !((getPAPymtTypeFormat() == HOST_PROTOCOL_UGP) && (pstPymtDtls->iPymtType == PYMT_PRIVATE)) )//Praveen_P1: For Private label transactions, PWC is not accepting this field
				{
					if(strlen(pstPymtDtls->szTranTm) > 0)
					{
						strcpy(szTime, pstPymtDtls->szTranTm);
					}
					else
					{
						getTime("HHMMSS", szTime);
					}
					iLen = strlen(szTime);
					if(iLen > 0)
					{
						strcpy(pstPymtDtls->szTranTm, szTime);
						tmpPtr = (char *) malloc(iLen + 1);
						{
							if(tmpPtr != NULL)
							{
								memset(tmpPtr, 0x00, iLen + 1);
								strcpy(tmpPtr, szTime);
								debug_sprintf(szDbgMsg, "%s: szTime for current Transactions = %s", __FUNCTION__, szTime);
								APP_TRACE(szDbgMsg);
							}
						}
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "PAYPAL_CHECKID_KEY") == SUCCESS)
			{
				iLen = strlen(pstPymtDtls->szCheckIdKey);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					{
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, pstPymtDtls->szCheckIdKey);
							debug_sprintf(szDbgMsg, "%s: Paypal CheckId key for current transaction = [%s]", __FUNCTION__, pstPymtDtls->szCheckIdKey);
							APP_TRACE(szDbgMsg);
						}
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "ENCRYPTION_TYPE") == SUCCESS && (iCommand != SSI_ACK) && (iCommand != SSI_TOR)
					&& (pstPymtDtls->iPymtType != PYMT_CHECK) && (pstPymtDtls->iPymtType != PYMT_CHKSALE) && (pstPymtDtls->iPymtType != PYMT_CHKVERIFY))
			{
				iLen = 1;
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					if(pstPymtDtls->pstCardDtls == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Card not present", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						iEnc = NO_ENC;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Card present [%d]", __FUNCTION__, pstPymtDtls->pstCardDtls->iEncType);
						APP_TRACE(szDbgMsg);
						iEnc = pstPymtDtls->pstCardDtls->iEncType;
					}
					switch(iEnc)
					{
					case NO_ENC:
						strcpy(tmpPtr, "0");
						break;

					case VSD_ENC:
						strcpy(tmpPtr, "1");
						break;

					case RSA_ENC:
						strcpy(tmpPtr, "3");
						break;

					case VSP_ENC:
						strcpy(tmpPtr, "5");
						break;

					default:
						strcpy(tmpPtr, "0");
						debug_sprintf(szDbgMsg, "%s: Should not come here for Enc type", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
				}
			}
		}
		else if(iHostProtocol == HOST_PROTOCOL_UGP)
		{
			if( ( strcmp(curPtr[iCnt].key, "SAF_FLAG") == SUCCESS) &&
					 ( ( isDHIEnabled() == PAAS_TRUE ) || (isSAFIndReqdForSSIReq() == PAAS_TRUE) ) )
			{
				/* If DHI(Direct Host Interface) is enabled, This field is included in the request.
				 * If It's SAF transaction then its value will be 1 or else 0.
				 * */
				if( pstPymtDtls->bSAFTran == PAAS_TRUE )
				{
					szTmp = "1";
				}
				else
				{
					szTmp = "0";
				}

				iLen = strlen(szTmp);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, szTmp);
					}
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillGenRptFlds
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillGenRptFlds(KEYVAL_PTYPE curPtr)
{
	int			iCnt			= 0;
	int			iLen			= 0;
	int			iTotCnt			= 0;
	char *		tmpPtr			= NULL;
	int			iHostProtocol	= getHostProtocolFormat();
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(genRptReqLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &genRptReqLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "FORMAT") == SUCCESS)
		{
			iLen = strlen("xml");
			tmpPtr = (char *) malloc(iLen + 1);
			if(tmpPtr != NULL)
			{
				strcpy(tmpPtr, "xml");
			}
		}
		if(iHostProtocol == HOST_PROTOCOL_SSI)
		{
			if(strcmp(curPtr[iCnt].key, "DELIMITER") == SUCCESS)
			{
				iLen = strlen("|");
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, "|");
				}
			}
			else if(strcmp(curPtr[iCnt].key, "RETURN_FLD_HDRS") == SUCCESS)
			{
				iLen = strlen("0");
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, "0");
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: sendBasketData
 *
 * Description	: This function is responsible for creating and sending the basket
 * 				  data to the url
 *
 * Input Params	: szSessKey
 *
 * Output Params: success or failure.
 * ============================================================================
 */
void* sendBasketData(void* arg)
{
	int				rv			= SUCCESS;
	int 			iLen		= 0;
	char*			szMsg		= NULL;
	META_STYPE		stMeta;
	SESSDTLS_PTYPE 	pstSessDtls = NULL;

#ifdef DEVDEBUG
	char			szDbgMsg[4096]	= "";
#elif  DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		pstSessDtls = (SESSDTLS_PTYPE)arg;
		/*
		 * Checking if atleast one line items or payment details is available
		 * If not then not sending the bapi packet
		 */
		/*
		 * Daivik  - Feb/4/2016 - Code change as part of Coverity fix 67336.
		 * Perform the NULL check before dereferencing this pointer
		 */
		if(pstSessDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Session details available",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS;
			return NULL;
		}
		if (pstSessDtls->pstPYMNTInfo == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No Payment details. Not sending bapi data",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS;
			break;
		}

		/* Got the session details. Use this data to create the metadata for
		 * basket building */
		memset(&stMeta, 0x00, METADATA_SIZE);

		rv = getMetaDataForBasket(&stMeta, pstSessDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get meta data for the basket",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildXMLMsgFromMetaData(&szMsg, &iLen, BASKET_RESPONSE, &stMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build xml message from the metadata",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Built the xml message from metadata Total Bytes [%d]",
				__FUNCTION__, iLen);
		APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
		if(strlen(szMsg) < 4000)
		{
			debug_sprintf(szDbgMsg, "%s: %s",__FUNCTION__,szMsg);
			APP_TRACE(szDbgMsg);
		}
#endif

		rv = doBAPICommunication(szMsg, iLen);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s:BAPI Communication failed",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/*	Free the meta data*/
		freeMetaDataEx(&stMeta);

		if (szMsg != NULL)
		{
			free(szMsg);
			szMsg = NULL;
		}
		break;
	}

	if(pstSessDtls->pstLIInfo != NULL)
	{
		cleanupPaymentItems(pstSessDtls->pstPYMNTInfo);
	}

	if(pstSessDtls->pstLIInfo != NULL)
	{
		cleanupLineItems(pstSessDtls->pstLIInfo);
	}

	free(arg);
	arg = NULL;


	debug_sprintf(szDbgMsg,"%s: Returning",__FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: getMetaDataForBasket
 *
 * Description	: This function forms the metadata for the building and sendint
 * 				  the basket data to the url
 *
 * Input Params	: szSessKey
 *
 * Output Params: success or failure.
 * ============================================================================
 */
static int getMetaDataForBasket(META_PTYPE pstMeta, SESSDTLS_PTYPE pstSessDtls)
{
	int				rv					 	= SUCCESS;
	int				iCnt					= 0;
	int				iTotCnt					= 0;
	char 			basketId[BASKLINE_SIZE]	= "";
	VAL_LST_PTYPE	tmpPtr					= NULL;
	KEYVAL_PTYPE	pstList					= NULL;
	KEYVAL_PTYPE	curPtr					= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( (pstMeta == NULL) || (pstSessDtls == NULL) )
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}
	iTotCnt = sizeof(basketSubm)/KEYVAL_SIZE;

	/* Allocate the memory for the key value pairs */
	pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
	if(pstList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		/* Daivik : 4/Feb/2016 - Coverity Fix 67332 - If we dont return here , then we may be dereferencing the pointer
		 * ahead.
		 */
		rv = FAILURE;
		return rv;
	}

	/* Initialize the variables */
	memset(pstList, 0x00, iTotCnt * KEYVAL_SIZE);
	curPtr = pstList;
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &basketSubm[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "basket") == SUCCESS)
		{
			tmpPtr 	= (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
			if(tmpPtr != NULL)
			{
				memset(tmpPtr, 0x00, sizeof(VAL_LST_STYPE));
				rv 	= fillBasketData (tmpPtr, pstSessDtls, basketId);
			}

			if (rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill the basekt data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				// CID-67478: 29-Jan-16: MukeshS3:
				// Freeing memory for only list node, because it would be already de-allocated for KeyVal & Val node inside fillBasketData
				freeValListNode(tmpPtr);
			}
			else
			{
				curPtr[iCnt].value = tmpPtr;
			}
		}
		else if (strcmp(curPtr[iCnt].key, "lineItems") == SUCCESS)
		{
			rv = fillLineItemsInBasket (&curPtr[iCnt], pstSessDtls->pstLIInfo, basketId);
			if (rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill the basekt data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else if (strcmp(curPtr[iCnt].key, "payments") == SUCCESS)
		{
			rv = fillPaymentsInBasket (&curPtr[iCnt], pstSessDtls->pstPYMNTInfo, basketId);
			if (rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill the basekt data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else if (strcmp(curPtr[iCnt].key, "redeemedOffers") == SUCCESS)
		{
			rv = fillOffersInBasket (&curPtr[iCnt], pstSessDtls->pstLIInfo, basketId);
			if (rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill the basekt data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else if (strcmp(curPtr[iCnt].key, "refunds") == SUCCESS)
		{
			rv = fillRefundsInBasket (&curPtr[iCnt], pstSessDtls->pstPYMNTInfo, basketId);
			if (rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to fill the basekt data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
	}
	if (rv == SUCCESS)
	{
		/* Set the metadata */
		memset(pstMeta, 0x00, METADATA_SIZE);
		pstMeta->iTotCnt 	= iTotCnt;
		pstMeta->keyValList	= pstList;

	}
	else
	{
		// CID-67381,67478: 22-Jan-16: MukeshS3: We must free any allocated memory above, if it is not going to be used anywhere for FAILURE case.
		// This function will free memory allocated for each value in the list from 0 till current location & than KEYVAL_PTYPE pointer itself.
		freeKeyValDataNode(pstList, iTotCnt);
		/* Set the metadata */
		memset(pstMeta, 0x00, METADATA_SIZE);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillPaymentsInBasket
 *
 * Description	: This function forms the metadata for the building line items
 * 				  in the basket
 *
 *
 * Input Params	: szSessKey
 *
 * Output Params: success or failure.
 * ============================================================================
 */
static int fillPaymentsInBasket(KEYVAL_PTYPE pszBasket, PYMNTINFO_PTYPE pstPymntInfo, char* basketId)
{
	int 			 rv			 	= SUCCESS;
	int 			 iCnt		 	= 0;
	int 			 iTotCnt		 	= 0;
	char*			 szTemp		 	= NULL;
	VAL_LST_PTYPE 	 pstBasketPymntptr	= NULL;
	PYMNT_NODE_PTYPE curPtr			= NULL;
	KEYVAL_PTYPE	 paymentPtr		= NULL;


#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if ( (pszBasket == NULL) || (pstPymntInfo == NULL) )
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		// CID-67274: 2-Feb-16: MukeshS3: Commenting this as it is a NULL pointer.
		if(pszBasket != NULL)
		{
			pszBasket->valType = NULL_ADD;
		}
		return SUCCESS;
	}

	pstBasketPymntptr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstBasketPymntptr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(pstBasketPymntptr, 0X00, sizeof(VAL_LST_STYPE));

	curPtr	 = pstPymntInfo->mainLstHead;
	iTotCnt  = sizeof(payment)/KEYVAL_SIZE;

	while (curPtr != NULL)
	{
		if (curPtr->type == PYMNT)
		{
			paymentPtr = (KEYVAL_PTYPE)malloc(iTotCnt*KEYVAL_SIZE);
			if (paymentPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//return FAILURE;	// CID-67334: 29-Jan-16: MukeshS3:
				rv = FAILURE;		// Need to de-allocate any unused memory
				break;
			}
			for (iCnt = 0; iCnt < iTotCnt; iCnt++)
			{
				memcpy(&paymentPtr[iCnt], &payment[iCnt], KEYVAL_SIZE);
				if (strcmp(paymentPtr[iCnt].key, "id") == SUCCESS)
				{
					szTemp = (char*) malloc (BASKLINE_SIZE);
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67334, 67476: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, BASKLINE_SIZE);

					getUUID(szTemp);
					if (rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to get UUID", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}

					paymentPtr[iCnt].value = szTemp;
				}
				else if (strcmp(paymentPtr[iCnt].key, "payLineNumber") == SUCCESS)
				{
/*					szTemp = (char*) malloc (BASKLINE_SIZE);
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						return FAILURE;
					}
					memset(szTemp, 0x00, BASKLINE_SIZE);
					strcpy(szTemp,basketId);
					memset(szTemp, 0x00, BASKLINE_SIZE);
					paymentPtr[iCnt].value = szTemp;*/
					paymentPtr[iCnt].valType = NULL_ADD;
				}
				else if (strcmp(paymentPtr[iCnt].key, "basketId") == SUCCESS)
				{
					szTemp = (char*) malloc (BASKLINE_SIZE);
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67334, 67476: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, BASKLINE_SIZE);
					strcpy(szTemp,basketId);
					paymentPtr[iCnt].value = szTemp;
				}
				else if (strcmp(paymentPtr[iCnt].key, "mop") == SUCCESS)
				{
					if (strlen(curPtr->szPymtMedia) > 0)
					{
						szTemp = (char*)malloc(sizeof(curPtr->szPymtMedia));
						if(szTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//return FAILURE;	// CID-67334, 67476: 29-Jan-16: MukeshS3:
							rv = FAILURE;		// Need to de-allocate any unused memory
							break;
						}
						memset(szTemp, 0x00, sizeof(curPtr->szPymtMedia));
						strcpy(szTemp, curPtr->szPymtMedia);
						paymentPtr[iCnt].value = szTemp;
					}
					else
					{
						paymentPtr[iCnt].valType = NULL_ADD;
					}
				}
				else if (strcmp(paymentPtr[iCnt].key, "cardNum") == SUCCESS)
				{
					if (strlen(curPtr->szPAN) > 0)
					{
						szTemp = (char*)malloc(sizeof(curPtr->szPAN));
						if(szTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//return FAILURE;	// CID-67334, 67476: 29-Jan-16: MukeshS3:
							rv = FAILURE;		// Need to de-allocate any unused memory
							break;
						}
						memset(szTemp, 0x00, sizeof(curPtr->szPAN));
						getMaskedPANForBAPI(curPtr->szPAN, szTemp);
						paymentPtr[iCnt].value = szTemp;
					}
					else
					{
						paymentPtr[iCnt].valType = NULL_ADD;
					}
				}
				else if (strcmp(paymentPtr[iCnt].key, "amount") == SUCCESS)
				{
					if (strlen(curPtr->apprvdAmt) > 0)
					{
						szTemp = (char*)malloc(sizeof(curPtr->apprvdAmt));
						if(szTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//return FAILURE;	// CID-67334, 67476: 29-Jan-16: MukeshS3:
							rv = FAILURE;		// Need to de-allocate any unused memory
							break;
						}
						memset(szTemp, 0x00, sizeof(curPtr->apprvdAmt));
						strcpy(szTemp, curPtr->apprvdAmt);
						paymentPtr[iCnt].value = szTemp;
					}
					else
					{
						paymentPtr[iCnt].valType = NULL_ADD;
					}
				}
				else if (strcmp(paymentPtr[iCnt].key, "cardAuth") == SUCCESS)
				{
					if (strlen(curPtr->szAuthCode) > 0)
					{
						szTemp = (char*)malloc(sizeof(curPtr->szAuthCode));
						if(szTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//return FAILURE;	// CID-67334, 67476: 29-Jan-16: MukeshS3:
							rv = FAILURE;		// Need to de-allocate any unused memory
							break;
						}
						memset(szTemp, 0x00, sizeof(curPtr->szAuthCode));
						strcpy(szTemp, curPtr->szAuthCode);
						paymentPtr[iCnt].value = szTemp;
					}
					else
					{
						paymentPtr[iCnt].valType = NULL_ADD;
					}
				}
				else if (strcmp(paymentPtr[iCnt].key, "cardName") == SUCCESS)
				{
					if (strlen(curPtr->szName) > 0)
					{
						szTemp = (char*)malloc(sizeof(curPtr->szName));
						if(szTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//return FAILURE;	// CID-67334, 67476: 29-Jan-16: MukeshS3:
							rv = FAILURE;		// Need to de-allocate any unused memory
							break;
						}
						memset(szTemp, 0x00, sizeof(curPtr->szName));
						strcpy(szTemp, curPtr->szName);
						paymentPtr[iCnt].value = szTemp;
					}
					else
					{
						paymentPtr[iCnt].valType = NULL_ADD;
					}
				}
				else if (strcmp(paymentPtr[iCnt].key, "ctroutd") == SUCCESS)
				{
					if (strlen(curPtr->szName) > 0)
					{
						szTemp = (char*)malloc(sizeof(curPtr->szCTroutd));
						if(szTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//return FAILURE;	// CID-67334, 67476,: 29-Jan-16: MukeshS3:
							rv = FAILURE;		// Need to de-allocate any unused memory
							break;
						}
						memset(szTemp, 0x00, sizeof(curPtr->szCTroutd));
						strcpy(szTemp, curPtr->szCTroutd);
						paymentPtr[iCnt].value = szTemp;
					}
					else
					{
						paymentPtr[iCnt].valType = NULL_ADD;
					}
				}
				else if (strcmp(paymentPtr[iCnt].key, "lptoken") == SUCCESS)
				{
					if (strlen(curPtr->szLPToken) > 0)
					{
						szTemp = (char*)malloc(sizeof(curPtr->szLPToken));
						if(szTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//return FAILURE;	// CID-67334: 29-Jan-16: MukeshS3:
							rv = FAILURE;		// Need to de-allocate any unused memory
							break;
						}
						memset(szTemp, 0x00, sizeof(curPtr->szLPToken));
						strcpy(szTemp, curPtr->szLPToken);
						paymentPtr[iCnt].value = szTemp;
					}
					else
					{
						paymentPtr[iCnt].valType = NULL_ADD;
					}
				}
			}
			addKeyValDataNode(pstBasketPymntptr, paymentPtr, iTotCnt);
		}
		curPtr = curPtr->next;
	}
	if (rv == SUCCESS)
	{
		pszBasket->value = pstBasketPymntptr;
	}
	else
	{	// CID-67334, 67476, 67235: 29-Jan-16: MukeshS3:Freeing this temporary allocated memory, for failure case
		freeKeyValDataNode(paymentPtr, iCnt);
		freeValListNode(pstBasketPymntptr);
		pszBasket->valType = NULL_ADD;
		pszBasket->value = NULL;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillRefundsInBasket
 *
 * Description	: This function forms the metadata for the building offers
 * 				  in the basket
 *
 *
 * Input Params	: szSessKey
 *
 * Output Params: success or failure.
 * ============================================================================
 */
static int fillRefundsInBasket(KEYVAL_PTYPE pszBasket, PYMNTINFO_PTYPE pstPymntInfo, char* basketId)
{
	int 			 rv			 	= SUCCESS;
	int 			 iCnt		 	= 0;
	int 			 iTotCnt		 	= 0;
	char*			 szTemp		 	= NULL;
	VAL_LST_PTYPE 	 pstBasketPymntptr	= NULL;
	PYMNT_NODE_PTYPE curPtr			= NULL;
	KEYVAL_PTYPE	 refundPtr		= NULL;


#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if ( (pszBasket == NULL) || (pstPymntInfo == NULL) )
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(pszBasket != NULL)
		{
			pszBasket->valType = NULL_ADD;
		}
		return SUCCESS;
	}

	pstBasketPymntptr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstBasketPymntptr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(pstBasketPymntptr, 0X00, sizeof(VAL_LST_STYPE));

	curPtr	 = pstPymntInfo->mainLstHead;
	iTotCnt  = sizeof(refund)/KEYVAL_SIZE;

	while (curPtr != NULL)
	{
		if (curPtr->type == REFUND)
		{
			refundPtr = (KEYVAL_PTYPE)malloc(iTotCnt*KEYVAL_SIZE);
			if (refundPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//return FAILURE;	// CID-67456: 22-Jan-16: MukeshS3:
				rv = FAILURE;	// breaking from here, instead of directly returning FAILURE
				break;			// need to de-allocate any previously allocated memory/resource
			}
			for (iCnt = 0; iCnt < iTotCnt; iCnt++)
			{
				memcpy(&refundPtr[iCnt], &refund[iCnt], KEYVAL_SIZE);
				if (strcmp(refundPtr[iCnt].key, "id") == SUCCESS)
				{
					szTemp = (char*) malloc (BASKLINE_SIZE);
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67456: 22-Jan-16: MukeshS3:
						rv = FAILURE;	// breaking from here, instead of directly returning FAILURE
						break;			// need to de-allocate any previously allocated memory/resource
					}
					memset(szTemp, 0x00, BASKLINE_SIZE);

					getUUID(szTemp);
					if (rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to get UUID", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}

					refundPtr[iCnt].value = szTemp;
				}
				else if (strcmp(refundPtr[iCnt].key, "basketId") == SUCCESS)
				{
					szTemp = (char*) malloc (BASKLINE_SIZE);
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67456: 22-Jan-16: MukeshS3:
						rv = FAILURE;	// breaking from here, instead of directly returning FAILURE
						break;			// need to de-allocate any previously allocated memory/resource
					}
					memset(szTemp, 0x00, BASKLINE_SIZE);
					strcpy(szTemp,basketId);
					refundPtr[iCnt].value = szTemp;
				}
				else if (strcmp(refundPtr[iCnt].key, "clientId") == SUCCESS)
				{
					szTemp = (char*) malloc (BASKLINE_SIZE);
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}
					memset(szTemp, 0x00, BASKLINE_SIZE);
					getClientId(szTemp);
					refundPtr[iCnt].value = szTemp;
				}
				else if (strcmp(refundPtr[iCnt].key, "ctroutd") == SUCCESS)
				{
					if (strlen(curPtr->szPAN)>0)
					{
						szTemp = (char*)malloc(sizeof(curPtr->szCTroutd));
						if(szTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//return FAILURE;	// CID-67456: 22-Jan-16: MukeshS3:
							rv = FAILURE;	// breaking from here, instead of directly returning FAILURE
							break;			// need to de-allocate any previously allocated memory/resource
						}
						memset(szTemp, 0x00, sizeof(curPtr->szCTroutd));
						strcpy(szTemp, curPtr->szCTroutd);
						refundPtr[iCnt].value = szTemp;
					}
					else
					{
						refundPtr[iCnt].valType = NULL_ADD;
					}
				}
				else if (strcmp(refundPtr[iCnt].key, "cardNum") == SUCCESS)
				{
					if (strlen(curPtr->szPAN)>0)
					{
						szTemp = (char*)malloc(sizeof(curPtr->szPAN));
						if(szTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//return FAILURE;	// CID-67456: 22-Jan-16: MukeshS3:
							rv = FAILURE;	// breaking from here, instead of directly returning FAILURE
							break;			// need to de-allocate any previously allocated memory/resource
						}
						memset(szTemp, 0x00, sizeof(curPtr->szPAN));
						getMaskedPANForBAPI(curPtr->szPAN, szTemp);
						refundPtr[iCnt].value = szTemp;
					}
					else
					{
						refundPtr[iCnt].valType = NULL_ADD;
					}
				}
				else if (strcmp(refundPtr[iCnt].key, "amount") == SUCCESS)
				{
					if (strlen(curPtr->apprvdAmt)>0)
					{
						szTemp = (char*)malloc(sizeof(curPtr->apprvdAmt));
						if(szTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//return FAILURE;	// CID-67456: 22-Jan-16: MukeshS3:
							rv = FAILURE;	// breaking from here, instead of directly returning FAILURE
							break;			// need to de-allocate any previously allocated memory/resource
						}
						memset(szTemp, 0x00, sizeof(curPtr->apprvdAmt));
						strcpy(szTemp, curPtr->apprvdAmt);
						refundPtr[iCnt].value = szTemp;
					}
					else
					{
						refundPtr[iCnt].valType = NULL_ADD;
					}
				}
				else if (strcmp(refundPtr[iCnt].key, "cardAuth") == SUCCESS)
				{
					if (strlen(curPtr->szAuthCode)>0)
					{
						szTemp = (char*)malloc(sizeof(curPtr->szAuthCode));
						if(szTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//return FAILURE;	// CID-67456: 22-Jan-16: MukeshS3:
							rv = FAILURE;	// breaking from here, instead of directly returning FAILURE
							break;			// need to de-allocate any previously allocated memory/resource
						}
						memset(szTemp, 0x00, sizeof(curPtr->szAuthCode));
						strcpy(szTemp, curPtr->szAuthCode);
						refundPtr[iCnt].value = szTemp;
					}
					else
					{
						refundPtr[iCnt].valType = NULL_ADD;
					}
				}
			}
			addKeyValDataNode(pstBasketPymntptr, refundPtr, iTotCnt);
		}
		curPtr = curPtr->next;
	}
	if (rv == SUCCESS)
	{
		pszBasket->value = pstBasketPymntptr;
	}
	else
	{
		pszBasket->valType = NULL_ADD;
		// CID-67456, 67253: 22-Jan-16: MukeshS3: Need to free any unused allocated memory before leaving, for FAILURE case.
		freeKeyValDataNode(refundPtr, iCnt);
		freeValListNode(pstBasketPymntptr);
		pszBasket->value = NULL;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillOffersInBasket
 *
 * Description	: This function forms the metadata for the building offers
 * 				  in the basket
 *
 *
 * Input Params	: szSessKey
 *
 * Output Params: success or failure.
 * ============================================================================
 */
static int fillOffersInBasket(KEYVAL_PTYPE pszBasket, LIINFO_PTYPE pstLIInfo, char* basketId)
{
	int 			rv			 	= SUCCESS;
	int 			iCnt		 	= 0;
	int 			iTotCnt		 	= 0;
	char*			szTemp		 	= NULL;
	VAL_LST_PTYPE 	pstBasketLIptr	= NULL;
	LI_NODE_PTYPE	curPtr			= NULL;
	OFF_LI_PTYPE  	offerDtlsPtr	= NULL;
	KEYVAL_PTYPE	lineItemPtr		= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if ( (pszBasket == NULL) || (pstLIInfo == NULL) )
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//CID 67205 (#1 of 1): Dereference after null check (FORWARD_NULL). Now Check for NULL and dereference. T_RaghavendranR1
		if(pszBasket != NULL)
		{
			pszBasket->valType = NULL_ADD;
		}

		return SUCCESS;
	}

	pstBasketLIptr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstBasketLIptr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(pstBasketLIptr, 0X00, sizeof(VAL_LST_STYPE));

	curPtr	 = pstLIInfo->mainLstHead;
	//iTotCnt  = sizeof(lineItem)/KEYVAL_SIZE;	// CID-67397, 67371: 1-Feb-16: MukeshS3:
	iTotCnt  = sizeof(redemption)/KEYVAL_SIZE;	// Correcting it to redemption list while calculating iTotal count value.

	while (curPtr != NULL)
	{
		if (curPtr->liType == OFFER)
		{
			offerDtlsPtr = (OFF_LI_PTYPE) curPtr->liData;
			lineItemPtr = (KEYVAL_PTYPE)malloc(iTotCnt*KEYVAL_SIZE);
			if (lineItemPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//return FAILURE;	// CID-67350, 67371: 29-Jan-16: MukeshS3:
				rv = FAILURE;		// Need to de-allocate any unused memory
				break;
			}
			for (iCnt = 0; iCnt < iTotCnt; iCnt++)
			{
				memcpy(&lineItemPtr[iCnt], &redemption[iCnt], KEYVAL_SIZE);
				if (strcmp(lineItemPtr[iCnt].key, "id") == SUCCESS)
				{
					szTemp = (char*) malloc (BASKLINE_SIZE);
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67350, 67371: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, BASKLINE_SIZE);

					getUUID(szTemp);
					if (rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to get UUID", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}

					lineItemPtr[iCnt].value = szTemp;
				}
				else if (strcmp(lineItemPtr[iCnt].key, "basketId") == SUCCESS)
				{
					szTemp = (char*) malloc (BASKLINE_SIZE);
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67350, 67371: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, BASKLINE_SIZE);
					strcpy(szTemp, basketId);
					lineItemPtr[iCnt].value = szTemp;
				}
				else if (strcmp(lineItemPtr[iCnt].key, "offerLineNumber") == SUCCESS)
				{
					szTemp = (char*)malloc(sizeof(curPtr->szLiNo));
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67350, 67371: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, sizeof(curPtr->szLiNo));
					strcpy(szTemp,curPtr->szLiNo);
					lineItemPtr[iCnt].value = szTemp;
				}
				else if (strcmp(lineItemPtr[iCnt].key, "appliedLineNumber") == SUCCESS)
				{
					szTemp = (char*)malloc(sizeof(offerDtlsPtr->szOfferLI));
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67350, 67371: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, sizeof(offerDtlsPtr->szOfferLI));
					strcpy(szTemp,offerDtlsPtr->szOfferLI);
					lineItemPtr[iCnt].value = szTemp;
				}
				else if (strcmp(lineItemPtr[iCnt].key, "description") == SUCCESS)
				{
					szTemp = (char*)malloc(sizeof(curPtr->szDesc));
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67350, 67371: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, sizeof(curPtr->szDesc));
					strcpy(szTemp,curPtr->szDesc);
					lineItemPtr[iCnt].value = szTemp;
				}
				else if (strcmp(lineItemPtr[iCnt].key, "amount") == SUCCESS)
				{
					szTemp = (char*)malloc(sizeof(offerDtlsPtr->szOfferAmt));
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67350, 67371: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, sizeof(offerDtlsPtr->szOfferAmt));
					strcpy(szTemp,offerDtlsPtr->szOfferAmt);
					lineItemPtr[iCnt].value = szTemp;
				}
			}
			if(rv == SUCCESS)
			{
				addKeyValDataNode(pstBasketLIptr, lineItemPtr, iTotCnt);
			}
			else // CID 67208 (#1 of 1): Resource leak (RESOURCE_LEAK). T_RaghavendranR1. Breaking from the while loop, if rv is FAILURE in previous case while adding.
			{
				break;
			}
		}
		curPtr = curPtr->nextLI;
	}
	if (rv == SUCCESS)
	{
		pszBasket->value = pstBasketLIptr;
	}
	else
	{
		pszBasket->valType = NULL_ADD;
		// CID-67350: 29-Jan-16: MukeshS3: Freeing this temporary allocated memory, for failure case
		freeKeyValDataNode(lineItemPtr, iCnt);
		freeValListNode(pstBasketLIptr);
		pszBasket->value = NULL;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillLineItemsInBasket
 *
 * Description	: This function forms the metadata for the building line items
 * 				  in the basket
 *
 *
 * Input Params	: szSessKey
 *
 * Output Params: success or failure.
 * ============================================================================
 */
static int fillLineItemsInBasket(KEYVAL_PTYPE pszBasket, LIINFO_PTYPE pstLIInfo, char* basketId)
{
	int 			rv			 	= SUCCESS;
	int 			iCnt		 	= 0;
	int 			iTotCnt		 	= 0;
	char*			szTemp		 	= NULL;
	VAL_LST_PTYPE 	pstBasketLIptr	= NULL;
	LI_NODE_PTYPE	curPtr			= NULL;
	MCNT_LI_PTYPE 	mcntDataPtr	 	= NULL;
	KEYVAL_PTYPE	lineItemPtr		= NULL;


#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if ( (pszBasket == NULL) || (pstLIInfo == NULL) )
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//CID 67206 (#1 of 1): Dereference after null check (FORWARD_NULL). Now Check for NULL and dereference. T_RaghavendranR1
		if(pszBasket != NULL)
		{
			pszBasket->valType = NULL_ADD;
		}
		return SUCCESS;
	}

	pstBasketLIptr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
	if (pstBasketLIptr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(pstBasketLIptr, 0X00, sizeof(VAL_LST_STYPE));

	curPtr	 = pstLIInfo->mainLstHead;
	iTotCnt  = sizeof(lineItem)/KEYVAL_SIZE;

	while (curPtr != NULL)
	{
		if (curPtr->liType == MERCHANDISE)
		{
			mcntDataPtr = (MCNT_LI_PTYPE) curPtr->liData;
			lineItemPtr = (KEYVAL_PTYPE)malloc(iTotCnt*KEYVAL_SIZE);
			if (lineItemPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//return FAILURE;	// CID-67330: 29-Jan-16: MukeshS3:
				rv = FAILURE;		// Need to de-allocate any unused memory
				break;
			}
			for (iCnt = 0; iCnt < iTotCnt; iCnt++)
			{
				memcpy(&lineItemPtr[iCnt], &lineItem[iCnt], KEYVAL_SIZE);
				if (strcmp(lineItemPtr[iCnt].key, "id") == SUCCESS)
				{
					szTemp = (char*) malloc (BASKLINE_SIZE);
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67330: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, BASKLINE_SIZE);

					getUUID(szTemp);
					if (rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to get UUID", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}

					lineItemPtr[iCnt].value = szTemp;
				}
				else if (strcmp(lineItemPtr[iCnt].key, "basketId") == SUCCESS)
				{
					szTemp = (char*) malloc (BASKLINE_SIZE);
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67330: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, BASKLINE_SIZE);
					strcpy(szTemp,basketId);
					lineItemPtr[iCnt].value = szTemp;
				}
				else if (strcmp(lineItemPtr[iCnt].key, "itemId") == SUCCESS)
				{
					szTemp = (char*)malloc(sizeof(curPtr->szSKU));
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67330: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, sizeof(curPtr->szSKU));
					strcpy(szTemp, curPtr->szSKU);
					lineItemPtr[iCnt].value = szTemp;
				}
				else if (strcmp(lineItemPtr[iCnt].key, "lineNumber") == SUCCESS)
				{
					szTemp = (char*)malloc(sizeof(curPtr->szLiNo));
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67330: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, sizeof(curPtr->szLiNo));
					strcpy(szTemp, curPtr->szLiNo);
					lineItemPtr[iCnt].value = szTemp;
				}
				else if (strcmp(lineItemPtr[iCnt].key, "code") == SUCCESS)
				{
					szTemp = (char*)malloc(sizeof(mcntDataPtr->szUPC));
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67330: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, sizeof(mcntDataPtr->szUPC));
					strcpy(szTemp, mcntDataPtr->szUPC);
					lineItemPtr[iCnt].value = szTemp;
				}
				else if (strcmp(lineItemPtr[iCnt].key, "quantity") == SUCCESS)
				{
					szTemp = (char*)malloc(sizeof(mcntDataPtr->szQty));
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67330: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, sizeof(mcntDataPtr->szQty));
					strcpy(szTemp, mcntDataPtr->szQty);
					lineItemPtr[iCnt].value = szTemp;
				}
				else if (strcmp(lineItemPtr[iCnt].key, "description") == SUCCESS)
				{
					szTemp = (char*)malloc(sizeof(curPtr->szDesc));
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67330: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, sizeof(curPtr->szDesc));
					strcpy(szTemp, curPtr->szDesc);
					lineItemPtr[iCnt].value = szTemp;
				}
				else if (strcmp(lineItemPtr[iCnt].key, "price") == SUCCESS)
				{
					szTemp = (char*)malloc(sizeof(mcntDataPtr->szUnitPrice));
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//return FAILURE;	// CID-67330: 29-Jan-16: MukeshS3:
						rv = FAILURE;		// Need to de-allocate any unused memory
						break;
					}
					memset(szTemp, 0x00, sizeof(mcntDataPtr->szUnitPrice));
					strcpy(szTemp, mcntDataPtr->szUnitPrice);
					lineItemPtr[iCnt].value = szTemp;
				}
			}
			if(rv == SUCCESS)
			{
				addKeyValDataNode(pstBasketLIptr, lineItemPtr, iTotCnt);
			}
			else // CID 67221 (#1 of 1): Resource leak (RESOURCE_LEAK). T_RaghavendranR1. Breaking from the while loop, if rv is FAILURE in previous case while adding.
			{
				break;
			}
		}
		curPtr = curPtr->nextLI;
	}
	if (rv == SUCCESS)
	{
		pszBasket->value = pstBasketLIptr;
	}
	else
	{
		pszBasket->valType = NULL_ADD;
		// CID-67330, 67221: 29-Jan-16: MukeshS3: Freeing this temporary allocated memory, for failure case
		freeKeyValDataNode(lineItemPtr, iCnt);
		freeValListNode(pstBasketLIptr);
		pszBasket->value = NULL;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillBasketData
 *
 * Description	: This function forms the metadata for the building basket
 *
 *
 * Input Params	: szSessKey
 *
 * Output Params: success or failure.
 * ============================================================================
 */
static int fillBasketData(VAL_LST_PTYPE pstValLst, SESSDTLS_PTYPE pstSessDtls, char* basketId)
{
	int 			rv			= SUCCESS;
	int 			iCnt		= 0;
	int 			iTotCnt		= 0;
	int				iDuration	= 0;
	char*			szTemp		= NULL;
	KEYVAL_PTYPE	pstList		= NULL;
	KEYVAL_PTYPE	curPtr		= NULL;
	time_t			endTime;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( (pstValLst == NULL) || (pstSessDtls == NULL) )
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}
	endTime = time(&endTime);
	iTotCnt = sizeof(basket)/KEYVAL_SIZE;

	/* Allocate the memory for the key value pairs */
	pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
	if(pstList == NULL)
	{

		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}
	/* Initialize the variables */
	memset(pstList, 0x00, iTotCnt * KEYVAL_SIZE);
	curPtr = pstList;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &basket[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "id") == SUCCESS)
		{
			szTemp = (char*) malloc (BASKLINE_SIZE);
			if(szTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(szTemp, 0x00, BASKLINE_SIZE);

			rv = getUUID(szTemp);
			if (rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get UUID", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			curPtr[iCnt].value = szTemp;
			strcpy (basketId, szTemp);
		}

		else if(strcmp(curPtr[iCnt].key, "dayId") == SUCCESS)
		{
			szTemp = (char*) malloc (BASKLINE_SIZE);
			if(szTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(szTemp, 0x00, BASKLINE_SIZE);
			genTimeStamp(szTemp, 3);
			curPtr[iCnt].value = szTemp;
		}
		else if(strcmp(curPtr[iCnt].key, "timeId") == SUCCESS)
		{
			szTemp = (char*) malloc (BASKLINE_SIZE);
			if(szTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(szTemp, 0x00, BASKLINE_SIZE);
			getTime("HH:MM:SS", szTemp);
			curPtr[iCnt].value = szTemp;
		}

		else if(strcmp(curPtr[iCnt].key, "playerId") == SUCCESS)
		{
			szTemp = (char*) malloc (BASKLINE_SIZE);
			if(szTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(szTemp, 0x00, BASKLINE_SIZE);
			getDevSrlNum(szTemp);
			curPtr[iCnt].value = szTemp;
		}

		else if(strcmp(curPtr[iCnt].key, "cashierId") == SUCCESS)
		{
			if (strlen(pstSessDtls->szCashierId) > 0)
			{
				szTemp = (char*) malloc (BASKLINE_SIZE);
				if(szTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(szTemp, 0x00, BASKLINE_SIZE);
				strcpy(szTemp, pstSessDtls->szCashierId);
				curPtr[iCnt].value = szTemp;
			}
			else
			{
				curPtr[iCnt].valType = NULL_ADD;
			}
		}

		else if(strcmp(curPtr[iCnt].key, "store") == SUCCESS)
		{
			if (strlen(pstSessDtls->szCashierId) > 0)
			{
				szTemp = (char*) malloc (BASKLINE_SIZE);
				if(szTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(szTemp, 0x00, BASKLINE_SIZE);
				strcpy(szTemp, pstSessDtls->szStoreNo);
				curPtr[iCnt].value = szTemp;
			}
			else
			{
				curPtr[iCnt].valType = NULL_ADD;
			}
		}

		else if(strcmp(curPtr[iCnt].key, "lane") == SUCCESS)
		{
			if (strlen(pstSessDtls->szLaneNo) > 0)
			{
				szTemp = (char*) malloc (BASKLINE_SIZE);
				if(szTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(szTemp, 0x00, BASKLINE_SIZE);
				strcpy(szTemp, pstSessDtls->szLaneNo);
				curPtr[iCnt].value = szTemp;
			}
			else
			{
				curPtr[iCnt].valType = NULL_ADD;
			}
		}

		else if(strcmp(curPtr[iCnt].key, "cashierCode") == SUCCESS)
		{
			if (strlen(pstSessDtls->szCashierId) > 0)
			{
				szTemp = (char*) malloc (BASKLINE_SIZE);
				if(szTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(szTemp, 0x00, BASKLINE_SIZE);
				strcpy(szTemp, pstSessDtls->szCashierId);
				curPtr[iCnt].value = szTemp;
			}
			else
			{
				curPtr[iCnt].valType = NULL_ADD;
			}
		}

		else if(strcmp(curPtr[iCnt].key, "startTime") == SUCCESS)
		{
			if (strlen(pstSessDtls->szStartTS) > 0)
			{
				szTemp = (char*) malloc (BASKLINE_SIZE);
				if(szTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(szTemp, 0x00, BASKLINE_SIZE);
				strcpy(szTemp, pstSessDtls->szStartTS);
				curPtr[iCnt].value = szTemp;
			}
			else
			{
				curPtr[iCnt].valType = NULL_ADD;
			}
		}

		else if(strcmp(curPtr[iCnt].key, "duration") == SUCCESS)
		{
			szTemp = (char*) malloc (BASKLINE_SIZE);
			if(szTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(szTemp, 0x00, BASKLINE_SIZE);
			iDuration = difftime(endTime, pstSessDtls->startTime);
			sprintf(szTemp, "%02d:%02d", iDuration/60, iDuration%60);
			curPtr[iCnt].value = szTemp;
		}
		else if(strcmp(curPtr[iCnt].key, "lineCount") == SUCCESS)
		{
			if (pstSessDtls->pstLIInfo != NULL)
			{
				szTemp = (char*) malloc (BASKLINE_SIZE);
				if(szTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(szTemp, 0x00, BASKLINE_SIZE);
				sprintf(szTemp,"%d", pstSessDtls->pstLIInfo->iItemCnt);
				curPtr[iCnt].value = szTemp;
			}
			else
			{
				curPtr[iCnt].valType = NULL_ADD;
			}
		}

		else if(strcmp(curPtr[iCnt].key, "offerCount") == SUCCESS)
		{
			if (pstSessDtls->pstLIInfo != NULL)
			{
				szTemp = (char*) malloc (BASKLINE_SIZE);
				if(szTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(szTemp, 0x00, BASKLINE_SIZE);
				sprintf(szTemp, "%d", pstSessDtls->pstLIInfo->iOfferCnt);
				curPtr[iCnt].value = szTemp;
			}
			else
			{
				curPtr[iCnt].valType = NULL_ADD;
			}
		}

		else if(strcmp(curPtr[iCnt].key, "receiptTime") == SUCCESS)
		{
			szTemp = (char*) malloc (BASKLINE_SIZE);
			if(szTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(szTemp, 0x00, BASKLINE_SIZE);
			getTime("HH:MM:SS", szTemp);
			curPtr[iCnt].value = szTemp;
		}

		else if(strcmp(curPtr[iCnt].key, "txNum") == SUCCESS)
		{
			if (strlen(pstSessDtls->szInvoice) > 0)
			{
				szTemp = (char*) malloc (BASKLINE_SIZE);
				if(szTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(szTemp, 0x00, BASKLINE_SIZE);
				strcpy(szTemp, pstSessDtls->szInvoice);
				curPtr[iCnt].value = szTemp;
			}
			else
			{
				curPtr[iCnt].valType = NULL_ADD;
			}
		}

		else if(strcmp(curPtr[iCnt].key, "subtotal") == SUCCESS)
		{
			if (pstSessDtls->pstLIInfo != NULL)
			{
				szTemp = (char*) malloc (sizeof(pstSessDtls->pstLIInfo->runTaxAmt));
				if(szTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(szTemp, 0x00, sizeof(pstSessDtls->pstLIInfo->runTaxAmt));
				sprintf(szTemp,"%.2lf",atof(pstSessDtls->pstLIInfo->runTranAmt)-atof(pstSessDtls->pstLIInfo->runTaxAmt));
				curPtr[iCnt].value = szTemp;
			}
			else
			{
				curPtr[iCnt].valType = NULL_ADD;
			}
		}

		else if(strcmp(curPtr[iCnt].key, "tax") == SUCCESS)
		{
			if (pstSessDtls->pstLIInfo != NULL)
			{
				szTemp = (char*) malloc (sizeof(pstSessDtls->pstLIInfo->runTaxAmt));
				if(szTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(szTemp, 0x00, sizeof(pstSessDtls->pstLIInfo->runTaxAmt));
				strcpy(szTemp, pstSessDtls->pstLIInfo->runTaxAmt);
				curPtr[iCnt].value = szTemp;
			}
			else
			{
				curPtr[iCnt].valType = NULL_ADD;
			}
		}

		else if(strcmp(curPtr[iCnt].key, "total") == SUCCESS)
		{
			if (pstSessDtls->pstLIInfo != NULL)
			{
				szTemp = (char*) malloc (sizeof(pstSessDtls->pstLIInfo->runTaxAmt));
				if(szTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(szTemp, 0x00, sizeof(pstSessDtls->pstLIInfo->runTranAmt));
				strcpy(szTemp, pstSessDtls->pstLIInfo->runTranAmt);
				curPtr[iCnt].value = szTemp;
			}
			else
			{
				curPtr[iCnt].valType = NULL_ADD;
			}
		}

		else if(strcmp(curPtr[iCnt].key, "clientId") == SUCCESS)
		{
			szTemp = (char*) malloc (BASKLINE_SIZE);
			if(szTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(szTemp, 0x00, BASKLINE_SIZE);
			getClientId(szTemp);
			curPtr[iCnt].value = szTemp;
		}

		else if(strcmp(curPtr[iCnt].key, "shopperToken") == SUCCESS)
		{
			if (pstSessDtls->pstPYMNTInfo != NULL)
			{
				if (strlen(pstSessDtls->pstPYMNTInfo->szShopToken)>0)
				{
					szTemp = (char*) malloc (sizeof(pstSessDtls->pstPYMNTInfo->szShopToken));
					if(szTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}
					memset(szTemp, 0x00, sizeof(pstSessDtls->pstPYMNTInfo->szShopToken));
					strcpy(szTemp, pstSessDtls->pstPYMNTInfo->szShopToken);
					curPtr[iCnt].value = szTemp;
				}
				else
				{
					curPtr[iCnt].valType = NULL_ADD;
				}
			}
			else
			{
				curPtr[iCnt].valType = NULL_ADD;
			}
		}
	}
	if (rv == SUCCESS)
	{
		addKeyValDataNode(pstValLst, pstList, iTotCnt);
	}
	else
	{
		// CID-67482: 22-Jan-16: MukeshS3: We must free any allocated memory above, if it is not going to be used anywhere for FAILURE case.
		// This function will free memory allocated for each value in the list from current location till 0 & than KEYVAL_PTYPE pointer itself.
		freeKeyValDataNode(pstList, iCnt);
	}

	debug_sprintf(szDbgMsg, "%s: Returning value [%d]",__FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: fillCreditAppReqData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillCreditAppReqData(KEYVAL_PTYPE curPtr, CREDITAPPDTLS_PTYPE pstCreditAppDtls, int fxn, int cmd)
{
	int			iCnt			= 0;
	int			iIndex			= 0;
	int			iTotCnt			= 0;
	int			iLen			= 0;
	char *		tmpPtr			= NULL;
	int			iHostProtocol	= getHostProtocolFormat();
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(creditAppnReqLst) / KEYVAL_SIZE;

	if(iHostProtocol == HOST_PROTOCOL_UGP)
	{
		debug_sprintf(szDbgMsg, "%s: CREDIT_APP not supported for UGP currently Returning[%d]", __FUNCTION__, iCnt);
		APP_TRACE(szDbgMsg);

		return iCnt;
	}
	/* Start the population of data for this key list */
	for(iCnt = 0; iCnt < 3; iCnt++)
	{
		memcpy(&curPtr[iCnt], &creditAppnReqLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "FUNCTION_TYPE") == SUCCESS)
		{
			iLen = strlen(szSSIFxn[fxn]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szSSIFxn[fxn]);
					debug_sprintf(szDbgMsg, "%s: Function Type = %s", __FUNCTION__, szSSIFxn[fxn]);
					APP_TRACE(szDbgMsg);

				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "COMMAND") == SUCCESS)
		{
			iLen = strlen(szSSICmd[cmd]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szSSICmd[cmd]);
					debug_sprintf(szDbgMsg, "%s: Command Type = %s", __FUNCTION__, szSSICmd[cmd]);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PROXY_REF") == SUCCESS)
		{
			iLen = strlen(pstCreditAppDtls->szProxyRef);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCreditAppDtls->szProxyRef);
					debug_sprintf(szDbgMsg, "%s: Command Type = %s", __FUNCTION__, szSSICmd[cmd]);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	for(; iCnt < iTotCnt; iCnt++)
	{
		/* Calculating the index for the array
		 * Text and the data will be under the same index
		 */
		iIndex = (iCnt - 3)/2;

		memcpy(&curPtr[iCnt], &creditAppnReqLst[iCnt], KEYVAL_SIZE);

		iLen = strlen(pstCreditAppDtls->stPromptDtls[iIndex].szPromptText);
		if(iLen > 0 )
		{
			tmpPtr = strdup(pstCreditAppDtls->stPromptDtls[iIndex].szPromptText);
		}

		curPtr[iCnt].value = tmpPtr;
		iCnt++;

		memcpy(&curPtr[iCnt], &creditAppnReqLst[iCnt], KEYVAL_SIZE);

		tmpPtr = NULL;
		iLen = strlen(pstCreditAppDtls->stPromptDtls[iIndex].szPromptDataWithLiterals);
		if(iLen > 0 )
		{
			tmpPtr = strdup(pstCreditAppDtls->stPromptDtls[iIndex].szPromptDataWithLiterals);
		}
		curPtr[iCnt].value = tmpPtr;

		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: addKeyValDataNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addKeyValDataNode(VAL_LST_PTYPE valLstPtr, void* elemList, int iCnt)
{
	int				iRetVal				= SUCCESS;
	VAL_NODE_PTYPE	tmpNodePtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	tmpNodePtr = (VAL_NODE_PTYPE) malloc(sizeof(VAL_NODE_STYPE));
	if(tmpNodePtr != NULL)
	{
		/* Initialize the allocated memory */
		memset(tmpNodePtr, 0x00, sizeof(VAL_NODE_STYPE));
		tmpNodePtr->elemList = elemList;
		tmpNodePtr->elemCnt	 = iCnt;
		/* Add the node to the list */
		if(valLstPtr->end == NULL)
		{
			/* Adding the first element */
			valLstPtr->start = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

			debug_sprintf(szDbgMsg, "%s: Adding first node", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			/* Adding the nth element */
			valLstPtr->end->next = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

			//debug_sprintf(szDbgMsg, "%s: Adding nth node", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}

		/* Increment the node count */
		valLstPtr->valCnt += 1;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		iRetVal = FAILURE;
	}

	//debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	//APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: fillPassThrgReqDtls
 *
 * Description	: Fills the Pass Through fields to be sent to SSI Host
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillPassThrgReqDtls(KEYVAL_PTYPE curPtr, PASSTHRG_FIELDS_PTYPE pstPassThrghDtls)
{
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iLen			= 0;
	void *			tmpPtr			= NULL;
	VAL_LST_PTYPE 	pstPassThrgVarLst = NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	if(pstPassThrghDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	iTotCnt = pstPassThrghDtls->iReqTagsCnt;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		curPtr[iCnt].key = pstPassThrghDtls->pstReqTagList[iCnt].key;	// just assigning the key location, because we never free the key memory from curPtr
		curPtr[iCnt].isMand = pstPassThrghDtls->pstReqTagList[iCnt].isMand;
		curPtr[iCnt].valType = pstPassThrghDtls->pstReqTagList[iCnt].valType;

		debug_sprintf(szDbgMsg, "%s: pstPassThrghDtls->pstReqTagList[iCnt].valType [%d], pstPassThrghDtls->pstReqTagList[iCnt].value [%p]", __FUNCTION__, pstPassThrghDtls->pstReqTagList[iCnt].valType, pstPassThrghDtls->pstReqTagList[iCnt].value);
		APP_TRACE(szDbgMsg);

		if(pstPassThrghDtls->pstReqTagList[iCnt].value == NULL)
		{
			continue;
		}

		if(pstPassThrghDtls->pstReqTagList[iCnt].valType == BOOLEAN)
		{
			iLen = sizeof(int);
			tmpPtr = (int *) malloc(iLen);
			if(tmpPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iCnt = FAILURE;
				break;
			}
			memset(tmpPtr, 0x00, iLen);
			memcpy(tmpPtr, (int*)pstPassThrghDtls->pstReqTagList[iCnt].value, iLen);
			curPtr[iCnt].value = tmpPtr;
			tmpPtr = NULL;
		}
		else if(pstPassThrghDtls->pstReqTagList[iCnt].valType == VARLIST)
		{
			pstPassThrgVarLst = fillVarListPassThroughDetails((VAL_LST_PTYPE)pstPassThrghDtls->pstReqTagList[iCnt].value);
			if(pstPassThrgVarLst == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Fill the VarList Pass Through Details for [%s] in SSI request", __FUNCTION__, pstPassThrghDtls->pstReqTagList[iCnt].key);
				APP_TRACE(szDbgMsg);
				iCnt = FAILURE;
				break;
			}
			curPtr[iCnt].value = pstPassThrgVarLst;
			curPtr[iCnt].valMetaInfo = pstPassThrghDtls->pstReqTagList[iCnt].valMetaInfo;
			tmpPtr = NULL;
		}
		else
		{
			iLen = strlen((char*)pstPassThrghDtls->pstReqTagList[iCnt].value);
			if(iLen > 0)
			{
				tmpPtr = (char*) malloc(iLen + 1);
				if(tmpPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iCnt = FAILURE;
					break;
				}
				memset(tmpPtr, 0x00, iLen + 1);
				memcpy(tmpPtr, (char*)pstPassThrghDtls->pstReqTagList[iCnt].value, iLen);
				curPtr[iCnt].value = tmpPtr;
				tmpPtr = NULL;
			}
		}
	}

#if 0
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		debug_sprintf(szDbgMsg, "%s: key[%s] value[%s]", __FUNCTION__, curPtr[iCnt].key, (char*)(curPtr[iCnt].value));
		APP_TRACE(szDbgMsg);
	}
#endif
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);
	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillVarListPassThroughDetails
 *
 * Description	: This function forms the metadata for the building VarList Details of SCI - SSI Passthrough
 *
 *
 * Input Params	: szSessKey
 *
 * Output Params: success or failure.
 * ============================================================================
 */
static VAL_LST_PTYPE fillVarListPassThroughDetails(VAL_LST_PTYPE pstVarLstPtr)
{
	int					iLen						= 0;
	int					iTotCnt						= 0;
	int					iCnt						= 0;
	void *				pTemp						= NULL;
	VAL_NODE_PTYPE		nodePtr						= NULL;
	VAL_NODE_PTYPE		currNodePtr 				= NULL;
	KEYVAL_PTYPE 		listPtr						= NULL;
	VAL_LST_PTYPE 		pstPassThrgVarLst 			= NULL;
	VAL_LST_PTYPE 		pstPassThrgVarLstInsideLst	= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: pstVarLstPtr [%p]", __FUNCTION__, pstVarLstPtr);
	APP_TRACE(szDbgMsg);

	pstPassThrgVarLst = (VAL_LST_PTYPE) malloc(sizeof(VAL_LST_STYPE));
	if(pstPassThrgVarLst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return pstPassThrgVarLst;
	}
	memset(pstPassThrgVarLst, 0x00, sizeof(VAL_LST_STYPE));

	pstPassThrgVarLst->valCnt = pstVarLstPtr->valCnt;
	nodePtr = pstVarLstPtr->start;

	debug_sprintf(szDbgMsg, "%s: --- ValCnt [%d] --- nodePtr [%p]", __FUNCTION__, pstPassThrgVarLst->valCnt, nodePtr);
	APP_TRACE(szDbgMsg);

	while(nodePtr != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: nodePtr->type [%d]", __FUNCTION__, nodePtr->type);
		APP_TRACE(szDbgMsg);

		if(nodePtr->type != -1)
		{
			currNodePtr = (VAL_NODE_PTYPE) malloc (sizeof(VAL_NODE_STYPE));
			memset(currNodePtr, 0x00, sizeof(VAL_NODE_STYPE));
			currNodePtr->elemCnt = nodePtr->elemCnt;
			if(nodePtr->szVal != NULL && strlen(nodePtr->szVal) > 0)
			{
				currNodePtr->szVal = (char *) malloc(strlen(nodePtr->szVal) + 1);
				if(currNodePtr->szVal == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					free(pstPassThrgVarLst);
					pstPassThrgVarLst = NULL;

					break;
				}
				strcpy(currNodePtr->szVal, nodePtr->szVal);
			}

			currNodePtr->type = nodePtr->type;
			currNodePtr->elemList = (KEYVAL_PTYPE) malloc(nodePtr->elemCnt * sizeof(KEYVAL_STYPE));
			if(currNodePtr->elemList == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				free(pstPassThrgVarLst);
				pstPassThrgVarLst = NULL;

				break;
			}
			memset(currNodePtr->elemList, 0x00, nodePtr->elemCnt * sizeof(KEYVAL_STYPE));

			iTotCnt = nodePtr->elemCnt;
			listPtr = nodePtr->elemList;

			for(iCnt = 0; iCnt < iTotCnt; iCnt++)
			{
				if(listPtr[iCnt].value == NULL)
				{
					continue;
				}

				debug_sprintf(szDbgMsg, "%s: Key  [%s], Value  [%s]", __FUNCTION__, (char *) (nodePtr->elemList[iCnt]).key, (char *)(nodePtr->elemList[iCnt]).value);
				APP_TRACE(szDbgMsg);

				debug_sprintf(szDbgMsg, "%s: ValType [%d]", __FUNCTION__, (nodePtr->elemList[iCnt]).valType);
				APP_TRACE(szDbgMsg);

				currNodePtr->elemList[iCnt].key = strdup(nodePtr->elemList[iCnt].key);
				currNodePtr->elemList[iCnt].isMand = nodePtr->elemList[iCnt].isMand;
				currNodePtr->elemList[iCnt].valType = nodePtr->elemList[iCnt].valType;

				if(nodePtr->elemList[iCnt].valType == BOOLEAN)		//save BOOl value, if present
				{
					iLen = sizeof(int);
					pTemp = (int *) malloc(iLen);
					if(pTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						free(pstPassThrgVarLst);
						pstPassThrgVarLst = NULL;

						break;
					}
					memset(pTemp, 0x00, iLen);
					memcpy(pTemp, (int*)nodePtr->elemList[iCnt].value, iLen);
					currNodePtr->elemList[iCnt].value = pTemp;

					pTemp = NULL;
				}
				else if(nodePtr->elemList[iCnt].valType == VARLIST)
				{
					debug_sprintf(szDbgMsg, "%s: Value Cnt of VARLIST inside VARLIST [%d]", __FUNCTION__, ((VAL_LST_PTYPE) (nodePtr->elemList[iCnt].value))->valCnt);
					APP_TRACE(szDbgMsg);

					pstPassThrgVarLstInsideLst = fillVarListPassThroughDetails( (VAL_LST_PTYPE) (nodePtr->elemList[iCnt].value));
					if(pstPassThrgVarLstInsideLst == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to Fill the VarList Pass Through Details for [%s] in SSI request", __FUNCTION__, nodePtr->elemList[iCnt].key);
						APP_TRACE(szDbgMsg);
						iCnt = FAILURE;
						break;
					}
					currNodePtr->elemList[iCnt].value = pstPassThrgVarLstInsideLst;
					currNodePtr->elemList[iCnt].valMetaInfo = ((VAL_LST_PTYPE) (nodePtr->elemList[iCnt].valMetaInfo));
				}
				else
				{
					iLen = strlen((char*)nodePtr->elemList[iCnt].value);
					if(iLen > 0)
					{
						pTemp = (char*)malloc(iLen + 1);
						if(pTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							free(pstPassThrgVarLst);
							pstPassThrgVarLst = NULL;

							break;
						}
						memset(pTemp, 0x00, iLen + 1);
						memcpy(pTemp, (char*)nodePtr->elemList[iCnt].value, iLen);
					}
					currNodePtr->elemList[iCnt].value = pTemp;

					debug_sprintf(szDbgMsg, "%s: pTemp for String and Numerics [%p] [%s]", __FUNCTION__, pTemp, (char *) pTemp);
					APP_TRACE(szDbgMsg);

					pTemp = NULL;
				}

			}

			addVarListNode(pstPassThrgVarLst, currNodePtr);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Varist type %d", __FUNCTION__, nodePtr->type);
			APP_TRACE(szDbgMsg);

			break;
		}

		nodePtr = nodePtr->next;
	}


	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pstPassThrgVarLst;
}

/*
 * ============================================================================
 * Function Name: addDataNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addVarListNode(VAL_LST_PTYPE valLstPtr, VAL_NODE_PTYPE currNodePtr)
{
	int				rv				= SUCCESS;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(currNodePtr != NULL)
	{
		/* Add the node to the list */
		if(valLstPtr->end == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Adding the First Node", __FUNCTION__);
			APP_TRACE(szDbgMsg);


			/* Adding the first element */
			valLstPtr->start = currNodePtr;
			valLstPtr->end = currNodePtr;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Adding the next Nodes", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Adding the nth element */
			valLstPtr->end->next = currNodePtr;
			valLstPtr->end = currNodePtr;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * End of file ssiMsgDataCtrl.c
 * ============================================================================
 */
