/*
 * ============================================================================
 * File Name	: demoAuthorizer.c
 *
 * Description	:
 *
 * Author		: Vikram Datt Rana
 * ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <svc.h>

#include "common/common.h"
#include "common/metaData.h"
#include "common/tranDef.h"
#include "sci/sciConfigDef.h"
#include "ssi/ssiDef.h"
#include "db/dataSystem.h"
#include "appLog/appLogAPIs.h"
#include "bLogic/bLogicCfgDef.h"
#include "ssi/ssiMain.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

#ifdef DEBUG
	static char	szDbgMsg[1024] = "";
#endif

/* static functions declarations */
static int getInteger(char *, int);
static int tranAmtCheck(char *);
static int processCaptureCommand(TRAN_PTYPE );
static int processRefunds(TRAN_PTYPE );
static int processVoidCommand(TRAN_PTYPE );
static int processAuthCommand(TRAN_PTYPE );
static int processCompletionCommand(TRAN_PTYPE );
static int saveFailureResponse(int , TRAN_PTYPE);
static int saveSuccessResponse(int , TRAN_PTYPE);
static int getNextTranSeqNo();
static int getNextInternalSeqNo();
static int getNextAuthCode(char *);
static int getNextTrout(char *);
static int getNextCTrout(char *);
static int saveAdditionalResponse(int , TRAN_PTYPE);
static int processGiftCard(int , TRAN_PTYPE );
static int saveEmbossedCardNumber(TRAN_PTYPE pstTran);

extern PAAS_BOOL checkBinInclusion(char *);
extern char * getMerchantProcessor(int);


/*
 * ============================================================================
 * Function Name: demoPWCProcessTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int demoSSIProcessTran(char *szTranKey, char *szStatMsg)
{
	int			rv					= SUCCESS;
	int			iFxn				= 0;
	int			iCmd				= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	TRAN_PTYPE	pstTran				= NULL;
	char		szAppLogData[300]	= "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Processing Demo SSI Transaction Details");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		/* Try fetching the SSI transaction instance for which the
		 * communication needs to be performed */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get tran data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* setting the status message which will be sent as response text to POS*/
			strcpy(szStatMsg, "Device Application Internal Error");
			break;
		}

		iFxn = pstTran->iFxn;
		iCmd = pstTran->iCmd;

		if(iFxn != SSI_PYMT)
		{
			debug_sprintf(szDbgMsg, "%s: Incorrect function for demo processing",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szStatMsg, "Function/Command Not Supported in Demo Mode");
				strcpy(szAppLogData, "Unsupported Command For the Demo Mode");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}


			rv = ERR_UNSUPP_CMD;
		}

		switch(iCmd)
		{
		case SSI_PREAUTH :
		case SSI_POSTAUTH:
		case SSI_VOICEAUTH:
			debug_sprintf(szDbgMsg, "%s: AUTH cmd processed ", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = processAuthCommand(pstTran);

			break;

		case SSI_SALE:
			debug_sprintf(szDbgMsg, "%s: CAPTURE cmd processed ", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = processCaptureCommand(pstTran);

			break;

		case SSI_CREDIT: /* For refunds */

			debug_sprintf(szDbgMsg, "%s: CREDIT cmd processed ", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = processRefunds(pstTran);

			break;

		case SSI_VOID:

			debug_sprintf(szDbgMsg, "%s: VOID cmd processed ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = processVoidCommand(pstTran);
			break;

		case SSI_COMPLETION:

			debug_sprintf(szDbgMsg, "%s: COMPLETION cmd processed ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = processCompletionCommand(pstTran);
			break;
		case SSI_COMMERCIAL:

			debug_sprintf(szDbgMsg, "%s: COMMERCIAL cmd processed ", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;

		case SSI_ACTIVATE:
		case SSI_REG:
		case SSI_ADDVAL:
		case SSI_BAL:
		case SSI_GIFTCLOSE:
		case SSI_DEACTIVATE:
		case SSI_REACTIVATE:
		case SSI_REMVAL:

			debug_sprintf(szDbgMsg, "%s: GIFT CARD processed ", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = processGiftCard(iCmd, pstTran);

			break;

		/* Dont worry about these fields.. transaction will not reach till the demo
		 * authorizer for these fields. */
		case SSI_ADDTIP:
		case SSI_RESETTIP:
		//case OPEN_TAB:
		//case CLOSE_TAB:
		//case DELETE_TAB:

			debug_sprintf(szDbgMsg,"%s: Command not supported in Demo",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szStatMsg, "Command Not Supported in Demo Mode");
				strcpy(szAppLogData, "Unsupported Command For the Demo Mode");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = ERR_UNSUPP_CMD;
			break;

		case SSI_SETTLE:
		case SSI_DAYSUMM:
		case SSI_PRESTL:
		case SSI_STLERR:
		case SSI_TRANSEARCH:
		case SSI_STLSUMM:
		case SSI_LASTTRAN:
		case SSI_DUPCHK:
		case SSI_CUTOVER:
		case SSI_SITETOTALS:

			debug_sprintf(szDbgMsg,"%s: Command not supported in Demo mode",
																	__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szStatMsg, "Command Not Supported in Demo Mode");
				strcpy(szAppLogData, "Unsupported Command For the Demo Mode");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = ERR_UNSUPP_CMD;
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should never come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		svcWait(3000);

		break;

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processGiftCard
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int processGiftCard(int iCmd, TRAN_PTYPE pstTran)
{
	int		rv	 		= SUCCESS;
	char	tranAmt[20]	= "";
	PYMTTRAN_PTYPE	pstPymtTran		= NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstPymtTran = (PYMTTRAN_PTYPE)pstTran->data;
	if(pstPymtTran == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No payment tran present",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	switch(iCmd)
	{
		case SSI_ACTIVATE:
		case SSI_REG:
		case SSI_ADDVAL:
		case SSI_REMVAL:
		/* Transaction amount would be present in these cases. It can be
		 * validated */

		if(pstPymtTran->pstAmtDtls != NULL)
		{
			memset(tranAmt, 0x00, sizeof(tranAmt));
			strcpy(tranAmt, pstPymtTran->pstAmtDtls->tranAmt);

			debug_sprintf(szDbgMsg, "%s: Transaction Amount is [%s]",__FUNCTION__, tranAmt);
			APP_TRACE(szDbgMsg);

			/* Validate the transaction amount */
			rv = tranAmtCheck(tranAmt);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Transaction Amt check PASSED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = saveSuccessResponse(iCmd, pstTran);
				if(rv == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: SUCCESS response saved", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Couldnt save SUCCESS response",__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Transaction Amt check FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = saveFailureResponse(iCmd, pstTran);
				if(rv == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Failure response saved", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Couldnt save failure response", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the transaction amount", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}

		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Transaction amount not required",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = saveSuccessResponse(iCmd, pstTran);
		if(rv == SUCCESS) {
			debug_sprintf(szDbgMsg, "%s: SUCCESS response saved", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else {
			debug_sprintf(szDbgMsg, "%s: Couldnt save SUCCESS response",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processRefunds
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int processRefunds(TRAN_PTYPE pstTran)
{
	int				rv	 			= SUCCESS;
	char			tranAmt[20]		= "";
	PYMTTRAN_PTYPE	pstPymtTran		= NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstPymtTran = (PYMTTRAN_PTYPE)pstTran->data;
	if(pstPymtTran == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No payment tran present",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	if(pstPymtTran->pstAmtDtls != NULL)
	{
		memset(tranAmt, 0x00, sizeof(tranAmt));
		strcpy(tranAmt, pstPymtTran->pstAmtDtls->tranAmt);

		debug_sprintf(szDbgMsg, "%s: Transaction Amount is [%s]",__FUNCTION__, tranAmt);
		APP_TRACE(szDbgMsg);

		rv = tranAmtCheck(tranAmt);
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Transaction Amt check passed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = saveSuccessResponse(SSI_CREDIT, pstTran);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: SUCCESS response saved",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Couldnt save SUCCESS response",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Transaction Amt check failed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = saveFailureResponse(SSI_CREDIT, pstTran);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure response saved",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Couldnt save failure response",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}

//			rv = FAILURE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to get the transaction amount",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: processCompletionCommand
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int processCompletionCommand(TRAN_PTYPE pstTran)
{
	int				rv	 			= SUCCESS;
	char			tranAmt[20]		= "";
	PYMTTRAN_PTYPE	pstPymtTran		= NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstPymtTran = (PYMTTRAN_PTYPE)pstTran->data;
	if(pstPymtTran == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No payment tran present",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	if(pstPymtTran->pstAmtDtls != NULL)
	{
		memset(tranAmt, 0x00, sizeof(tranAmt));
		strcpy(tranAmt, pstPymtTran->pstAmtDtls->tranAmt);

		debug_sprintf(szDbgMsg, "%s: Transaction Amount is [%s]",__FUNCTION__, tranAmt);
		APP_TRACE(szDbgMsg);

		rv = tranAmtCheck(tranAmt);
		if(rv == SUCCESS)
		{
		    rv = saveSuccessResponse(SSI_COMPLETION, pstTran);
		    if(rv == SUCCESS)
		    {
		        debug_sprintf(szDbgMsg, "%s: SUCCESS response saved",
		                                                        __FUNCTION__);
		        APP_TRACE(szDbgMsg);
		    }
		    else
		    {
				debug_sprintf(szDbgMsg, "%s: Couldnt save SUCCESS response",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
		    }
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Transaction Amt check failed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

	        rv = saveFailureResponse(SSI_COMPLETION, pstTran);
	        if(rv == SUCCESS)
	        {
	            debug_sprintf(szDbgMsg, "%s: Failure response saved",
	                                                            __FUNCTION__);
	            APP_TRACE(szDbgMsg);
	        }
	        else
	        {
	            debug_sprintf(szDbgMsg, "%s: Couldnt save failure response",
	                                                            __FUNCTION__);
	            APP_TRACE(szDbgMsg);

	            rv = FAILURE;
	        }
		}
	}



    debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: processVoidCommand
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int processVoidCommand(TRAN_PTYPE pstTran)
{
	int		rv	 		= SUCCESS;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    rv = saveSuccessResponse(SSI_VOID, pstTran);
    if(rv == SUCCESS)
    {
        debug_sprintf(szDbgMsg, "%s: SUCCESS response saved",
                                                        __FUNCTION__);
        APP_TRACE(szDbgMsg);
    }
    else
    {
        rv = saveFailureResponse(SSI_VOID, pstTran);
        if(rv == SUCCESS)
        {
            debug_sprintf(szDbgMsg, "%s: Failure response saved",
                                                            __FUNCTION__);
            APP_TRACE(szDbgMsg);
        }
        else
        {
            debug_sprintf(szDbgMsg, "%s: Couldnt save failure response",
                                                            __FUNCTION__);
            APP_TRACE(szDbgMsg);

            rv = FAILURE;
        }
    }

    debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: processCaptureCommand
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int processCaptureCommand(TRAN_PTYPE pstTran)
{
	int				rv	 			= SUCCESS;
	char			tranAmt[20]		= "";
	PYMTTRAN_PTYPE	pstPymtTran		= NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstPymtTran = (PYMTTRAN_PTYPE)pstTran->data;
	if(pstPymtTran == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No payment tran present",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	if(pstPymtTran->pstAmtDtls != NULL)
	{
		memset(tranAmt, 0x00, sizeof(tranAmt));
		strcpy(tranAmt, pstPymtTran->pstAmtDtls->tranAmt);

		debug_sprintf(szDbgMsg, "%s: Transaction Amount is [%s]",__FUNCTION__, tranAmt);
		APP_TRACE(szDbgMsg);

		rv = tranAmtCheck(tranAmt);
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Transaction Amt check passed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = saveSuccessResponse(SSI_SALE, pstTran);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: SUCCESS response saved",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Couldnt save SUCCESS response",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}
		}
		else {
			debug_sprintf(szDbgMsg, "%s: Transaction Amt check failed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = saveFailureResponse(SSI_SALE, pstTran);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure response saved",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Couldnt save failure response",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to get the transaction amount",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: processAuthCommand
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int processAuthCommand(TRAN_PTYPE pstTran)
{
	int		rv	 		= SUCCESS;
	char	tranAmt[20]	= "";

	PYMTTRAN_PTYPE	pstPymtTran		= NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstPymtTran = (PYMTTRAN_PTYPE)pstTran->data;
	if(pstPymtTran == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No payment tran present",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	if(pstPymtTran->pstAmtDtls != NULL)
	{
		memset(tranAmt, 0x00, sizeof(tranAmt));
		strcpy(tranAmt, pstPymtTran->pstAmtDtls->tranAmt);

		debug_sprintf(szDbgMsg, "%s: Transaction Amount is [%s]",__FUNCTION__, tranAmt);
		APP_TRACE(szDbgMsg);

		rv = tranAmtCheck(tranAmt);
		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Transaction Amt check passed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = saveSuccessResponse(SSI_PREAUTH, pstTran);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: SUCCESS response saved",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Couldnt save SUCCESS response",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Transaction Amt check failed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = saveFailureResponse(SSI_PREAUTH, pstTran);
			if(rv == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failure response saved",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Couldnt save failure response",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failed to get the transaction amount",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: saveSuccessResponse
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveSuccessResponse(int cmd, TRAN_PTYPE pstTran)
{
	int						rv			= SUCCESS;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	strcpy(pstTran->stRespDtls.szTermStat, "SUCCESS");          	//TERMINATION_STATUS

	if(cmd == SSI_VOID)
	{
		strcpy(pstTran->stRespDtls.szRslt, "VOIDED");      			//RESULT
	}
	else if(cmd == SSI_CREDIT)
	{
		strcpy(pstTran->stRespDtls.szRslt, "REFUNDED");      		//RESULT
	}
	else
	{
		strcpy(pstTran->stRespDtls.szRslt, "APPROVED/CAPTURED");    //RESULT
	}

	if(cmd == SSI_VOID)
	{
		strcpy(pstTran->stRespDtls.szRsltCode, "7");                //RESULT_CODE
	}
	else
	{
		strcpy(pstTran->stRespDtls.szRsltCode, "4");                //RESULT_CODE
	}
	strcpy(pstTran->stRespDtls.szRespTxt, "Transaction SUCCESS"); 	//RESPONSE_TEXT

	rv = saveAdditionalResponse(cmd, pstTran);

	rv = saveEmbossedCardNumber(pstTran);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveFailureResponse
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveFailureResponse(int cmd, TRAN_PTYPE pstTran)
{
	int						rv			= SUCCESS;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	strcpy(pstTran->stRespDtls.szTermStat, "SUCCESS");          			//TERMINATION_STATUS
	strcpy(pstTran->stRespDtls.szRslt, "DECLINED");      					//RESULT
	strcpy(pstTran->stRespDtls.szRsltCode, "6");                			//RESULT_CODE
	strcpy(pstTran->stRespDtls.szRespTxt, "Invalid transaction amount"); 	//RESPONSE_TEXT

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	rv = saveEmbossedCardNumber(pstTran);

	return rv;
}

/*
 * ============================================================================
 * Function Name: tranAmtCheck
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int tranAmtCheck(char * tranAmt)
{
	int		rv			= SUCCESS;
	int		iLen		= 0;
	int		iDollars	= 0;
	int		iCents		= 0;
	char *	cCurPtr		= NULL;
	char *	cNxtPtr		= NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = strlen(tranAmt);

	/* Should be only one decimal separator */
	cCurPtr = strchr(tranAmt, '.');
	if(cCurPtr != NULL) {
		cNxtPtr = strchr(cCurPtr + 1, '.');
		if(cNxtPtr != NULL) {
			debug_sprintf(szDbgMsg, "%s: Amount contains two separators invalid"
																, __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
		else {
			/* Get dollars and cents */
			iDollars = getInteger(tranAmt, cCurPtr - tranAmt);
			iCents	 = getInteger(cCurPtr + 1, iLen - (cCurPtr - tranAmt) - 1);

			debug_sprintf(szDbgMsg, "%s: Dollars = [%d] and cents = [%d]",
												__FUNCTION__, iDollars, iCents);
			APP_TRACE(szDbgMsg);

			/* No need to do this validation now, added new logic below */
#if 0			/* If the cents value is 1 then the amount is invalid */
			if(iCents == 1) {
				debug_sprintf(szDbgMsg, "%s: Invalid Amount - [%d] cents",
														__FUNCTION__, iCents);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}
#endif
			if((iCents % 2) != 0)				/* Declining the transaction in demo mode, if cents are Odd (Just for Demo Purpose)*/
			{
				debug_sprintf(szDbgMsg, "%s: Odd Cents - [%d] cents", __FUNCTION__, iCents);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}
		}
	}
	else {
		iDollars = getInteger(tranAmt, iLen);

		debug_sprintf(szDbgMsg, "%s: Dollars = [%d]", __FUNCTION__, iDollars);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}



/*
 * ============================================================================
 * Function Name: getInteger
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getInteger(char *szVal, int iLen)
{
	int     rv      = SUCCESS;
	int     val     = 0;
	int     iCnt    = 0;

	if(strlen(szVal) >= iLen) {

		for(iCnt = 0; iCnt < iLen; iCnt++)
		{
			val = ( (val * 10) + (szVal[iCnt] - '0') );
		}

		rv = val;
	}
	else {
		rv = FAILURE;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getNextTranSeqNo
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getNextTranSeqNo(char * szInp)
{
	static int	tranSeqNo = 500;

	if(tranSeqNo + 1 > 5000) {
		tranSeqNo = 0;
	}
	else {
		tranSeqNo ++;
	}

	sprintf(szInp, "%d", tranSeqNo);

	return 0;
}

/*
 * ============================================================================
 * Function Name: getNextInternalSeqNo
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getNextInternalSeqNo(char * szInp)
{
	static int	intrnSeqNo = 326;

	if(intrnSeqNo + 1 > 7000) {
		intrnSeqNo = 0;
	}
	else {
		intrnSeqNo ++;
	}

	sprintf(szInp, "%d", intrnSeqNo);

	return 0;
}

/*
 * ============================================================================
 * Function Name: getNextTrout
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getNextTrout(char * szInp)
{
	static int	no = 3261;

	if(no + 1 > 7000) {
		no = 0;
	}
	else {
		no ++;
	}

	//sprintf(szInp, "SCA_DEMO_%d", no);
	sprintf(szInp, "%d", no);

	return 0;
}

/*
 * ============================================================================
 * Function Name: getNextTrout
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getNextCTrout(char * szInp)
{
	static int	no = 6241;

	if(no + 1 > 7000) {
		no = 0;
	}
	else {
		no ++;
	}

	//sprintf(szInp, "SCA_DEMO_%d", no);
	sprintf(szInp, "%d", no);

	return 0;
}

/*
 * ============================================================================
 * Function Name: getNextAuthCode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getNextAuthCode(char * szInp)
{
	static int	no = 1516;

	if(no + 1 > 7000) {
		no = 0;
	}
	else {
		no ++;
	}

	sprintf(szInp, "%d", no);

	return 0;
}

/*
 * ============================================================================
 * Function Name: saveEmbossedCardNumber
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveEmbossedCardNumber(TRAN_PTYPE pstTran)
{
	int						rv			= SUCCESS;
	char 					szBIN[6+1]	= "";
	ENC_TYPE				iEncType	= getEncryptionType();
	PYMTTRAN_PTYPE			pstPymtTran		= NULL;

	pstPymtTran = (PYMTTRAN_PTYPE)pstTran->data;
	if(pstPymtTran == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No payment tran present",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	if(pstPymtTran->iPymtType == PYMT_GIFT || pstPymtTran->iPymtType == PYMT_PRIVATE)
	{
		/*
		 * We will be filling the EMBOSSED_ACCT_NUM field with the clear PAN
		 * we will fill this field if we dont receive Embossed card num fields
		 * in the SSI response
		 */
		if(pstPymtTran->pstCardDtls != NULL)
		{
			if(strlen(pstPymtTran->pstCardDtls->szEmbossedPAN) <= 0)
			{
				//Embossed Card Num is not present in the SSI response, will update here
				if((pstPymtTran->iPymtType == PYMT_GIFT && returnEmbossedNumForGiftType() &&
						isEmbossedCardNumCalcRequired() && (strlen(pstPymtTran->pstCardDtls->szPAN) > 0) &&
						(strcasecmp("SVDOT", (char *)getMerchantProcessor(PROCESSOR_GIFT)) == 0)))
				{
					/*Calculate the Embossed Card Number for Gift Transactions with ValueLink Processors as per the logic
					we already implemented in RCHI*/
					if(iEncType == VSP_ENC)
					{
						getEmbossedPANFromTrackData(pstPymtTran->pstCardDtls->szPAN, pstPymtTran->pstCardDtls->szEmbossedPAN, pstPymtTran->pstCardDtls->bManEntry);
					}
				}
				else
				{
					if( ((returnEmbossedNumForGiftType() && pstPymtTran->iPymtType == PYMT_GIFT) ||
							(returnEmbossedNumForPrivLblType() && pstPymtTran->iPymtType == PYMT_PRIVATE)) && (strlen(pstPymtTran->pstCardDtls->szPAN) > 0) )
					{
						memset(szBIN, 0x00, sizeof(szBIN));
						strncpy(szBIN, pstPymtTran->pstCardDtls->szPAN, 6);

						if(checkBinInclusion(szBIN) == PAAS_FALSE || (strlen(pstPymtTran->pstCardDtls->szPAN) <= 12))
						{
							debug_sprintf(szDbgMsg, "%s: Card is NOT Included in PCI Card Range, updating Embossed Card number", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							if(iEncType == VSP_ENC)
							{
								strcpy(pstPymtTran->pstCardDtls->szEmbossedPAN, pstPymtTran->pstCardDtls->szPAN);
								getDataFromVCL(pstPymtTran->pstCardDtls->szEmbossedPAN, NULL, NULL, NULL, pstPymtTran->pstCardDtls->bManEntry);

								/*
								 * Praveen_P1: Putting extra check here so that we will not send pci clear range
								 */
								if(strlen(pstPymtTran->pstCardDtls->szEmbossedPAN) > 0)
								{
									memset(szBIN, 0x00, sizeof(szBIN));
									strncpy(szBIN, pstPymtTran->pstCardDtls->szEmbossedPAN, 6);
									if(checkBinInclusion(szBIN) == PAAS_TRUE && (strlen(pstPymtTran->pstCardDtls->szPAN) > 12))
									{
										debug_sprintf(szDbgMsg, "%s: PAN is Included in PCI Card Range, not sending Embossed Card number", __FUNCTION__);
										APP_TRACE(szDbgMsg);
										memset(pstPymtTran->pstCardDtls->szEmbossedPAN, 0x00, sizeof(pstPymtTran->pstCardDtls->szEmbossedPAN));
									}
								}
							}
							else if(iEncType == VSD_ENC)
							{
								strcpy(pstPymtTran->pstCardDtls->szEmbossedPAN, pstPymtTran->pstCardDtls->szClrPAN);
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Card is Included in PCI Card Range, not updating Embossed Card number", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
					}
				}
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Embossed Card number is present in the SSI response, not updating it here",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: Card Details are not present, will not update embossed account number",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: Payment Type is not GIFT or Private",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveAdditionalResponse
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveAdditionalResponse(int cmd, TRAN_PTYPE pstTran)
{
	int						rv			= SUCCESS;
	char					szTmp[20]	= "";
	char					tranAmt[20]	= "";
	char					balAmt[20]	= "450.00";
	PYMTTRAN_PTYPE			pstPymtTran		= NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		pstPymtTran = (PYMTTRAN_PTYPE)pstTran->data;
		if(pstPymtTran == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No payment tran present",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Process the amounts here */
		switch(cmd)
		{
		//case AUTH:
		case SSI_PREAUTH :
		case SSI_VOICEAUTH:
		case SSI_POSTAUTH:
		//case CAPTURE:
		case SSI_SALE:
		case SSI_CREDIT:
		case SSI_VOID:
		case SSI_COMPLETION:
		case SSI_COMMERCIAL:
		case SSI_ACTIVATE:
		case SSI_ADDVAL:
		case SSI_REG:
		case SSI_REMVAL:
		//case GIFT_SALE:
		//case GIFT_CREDIT:

			if(pstPymtTran->pstAmtDtls != NULL)
			{
				memset(tranAmt, 0x00, sizeof(tranAmt));
				strcpy(tranAmt, pstPymtTran->pstAmtDtls->tranAmt);

				debug_sprintf(szDbgMsg, "%s: Transaction Amount is [%s]",__FUNCTION__, tranAmt);
				APP_TRACE(szDbgMsg);
			}

			break;

		case SSI_DEACTIVATE:
		case SSI_BAL:
		case SSI_GIFTCLOSE:
		case SSI_REACTIVATE:
			/* There was no tran amount in the request */
			break;
		}

		if(pstPymtTran->pstCTranDtls == NULL) //Need to allocate memory
		{
			pstPymtTran->pstCTranDtls = (CTRANDTLS_PTYPE)malloc(sizeof(CTRANDTLS_STYPE));
			if(pstPymtTran->pstCTranDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtTran->pstCTranDtls, 0x00, sizeof(CTRANDTLS_STYPE));
		}

		if(pstPymtTran->pstAmtDtls == NULL) //Need to allocate memory
		{
			pstPymtTran->pstAmtDtls = (AMTDTLS_PTYPE)malloc(sizeof(AMTDTLS_STYPE));
			if(pstPymtTran->pstAmtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtTran->pstAmtDtls, 0x00, sizeof(AMTDTLS_STYPE));

		}

		memset(szTmp, 0x00, sizeof(szTmp));
		getNextTranSeqNo(szTmp);
		strcpy(pstPymtTran->pstCTranDtls->szTranSeq, szTmp);   //TRANS_SEQ_NUM

		memset(szTmp, 0x00, sizeof(szTmp));
		getNextInternalSeqNo(szTmp);
		strcpy(pstPymtTran->pstCTranDtls->szTranId, szTmp);    //INTRN_SEQ_NUM

		memset(szTmp, 0x00, sizeof(szTmp));
		getNextTrout(szTmp);
		strcpy(pstPymtTran->pstCTranDtls->szTroutd, szTmp);      //TROUTD

		memset(szTmp, 0x00, sizeof(szTmp));
		getNextCTrout(szTmp);
		strcpy(pstPymtTran->pstCTranDtls->szCTroutd, szTmp);      //CTROUTD

		memset(szTmp, 0x00, sizeof(szTmp));
		getNextAuthCode(szTmp);
		strcpy(pstPymtTran->pstCTranDtls->szAuthCode, szTmp);     //AUTH_CODE

		strcpy(pstPymtTran->pstCTranDtls->szLPToken, "7654321");  //LPTOKEN

		if(cmd == SSI_BAL || cmd == SSI_ADDVAL)
		{
			strcpy(pstPymtTran->pstAmtDtls->availBal, balAmt);     //AVAILABLE_BALANCE
		}

		strcpy(pstPymtTran->pstAmtDtls->apprvdAmt, tranAmt);    //APPROVED_AMOUNT

		switch(pstPymtTran->iPymtType)
		{
			case PYMT_CREDIT:
				strcpy(pstPymtTran->pstCTranDtls->szPymntType, "CREDIT");  //PAYMENT_TYPE
				break;
			case PYMT_DEBIT:
				strcpy(pstPymtTran->pstCTranDtls->szPymntType, "DEBIT");  //PAYMENT_TYPE
				break;
			case PYMT_GIFT:
				strcpy(pstPymtTran->pstCTranDtls->szPymntType, "GIFT");  //PAYMENT_TYPE
				break;
			default:
				break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}



/*
 * ============================================================================
 * End of file demoAuthorizer.c
 * ============================================================================
 */
