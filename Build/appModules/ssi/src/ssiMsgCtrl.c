
#include <stdio.h>
#include <string.h>
#include <libxml/tree.h>
#include <libxml/parser.h>

#include "common/xmlUtils.h"
//#include "sessDataStore.h"
#include "common/tranDef.h"
#include "db/dataSystem.h"

#define SSIREQ_STR	"TRANSACTION"
#define SSIRES_STR	"RESPONSE"
#define SSIADM_STR	"MSH"

typedef METADATA_STYPE	META_STYPE;
typedef METADATA_PTYPE	META_PTYPE;

/* Extern functions */
extern int getMetaDataForSSIReq(META_PTYPE, TRAN_PTYPE);
extern int procParsedResp(TRAN_PTYPE, META_PTYPE, char *);
extern int setAdminPacketRequestStatus(PAAS_BOOL);
extern int getMetaDataForSSIResp(META_PTYPE, int, int);
extern int setDevAdminRqd(PAAS_BOOL);

/* Static functions declarations */
static int parseSSIResp(META_PTYPE, char *, int, int);
static void replaceUTF8WithUTF16(char *);

/*
 * ============================================================================
 * Function Name: buildSSIReq
 *
 * Description	: This function is responsible for building the XML request
 * 					according to the SSI specifications, filling the request
 * 					with the relevant transaction details, depending upon the
 * 					function and the command of the current transaction.
 *
 * Input Params	:	szTranKey	-> identifier for the transaction data
 * 					pszBuf		-> placeholder for the xml request
 * 					iLen		-> placeholder for the xml request length
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int buildSSIReqMsg(char * szTranKey, char ** pszBuf, int * iLen)
{
	int			rv				= SUCCESS;
	TRAN_PTYPE	pstTran			= NULL;
	META_STYPE	stMetaData;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Daivik:27/1/2016 : Coverity 67256 - Need to initialize stMetaData*/
	memset(&stMetaData, 0x00, METADATA_SIZE);

	while(1)
	{
		/* Get the SSI transaction from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get tran data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Got the transaction data. Use this to get the message */
		rv = getMetaDataForSSIReq(&stMetaData, pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Call the xml utility api to build the xml message from the metadata
		 * just built from the transaction data */
		rv = buildXMLMsgFromMetaData(pszBuf, iLen, SSIREQ_STR, &stMetaData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI REQ XML built",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	/* Free the metaData */
	freeMetaDataEx(&stMetaData);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processSSIResp
 *
 * Description	: This function is responsible to process the XML response
 * 					receieved from the payment host, after the transcation
 * 					request was posted.
 *
 * Input Params	:	szTranKey-> identifier of the transaction data
 * 					szMsg	 -> XML response recieved from the payment host
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int processSSIResp(char * szTranKey, char * szMsg)
{
	int			rv				= SUCCESS;
	TRAN_PTYPE	pstTran			= NULL;
	META_STYPE	stMetaData;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Get the transaction data from the stack. */
		/* Get the SSI transaction from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get tran data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Parse the xml response */
		rv = parseSSIResp(&stMetaData, szMsg, pstTran->iFxn, pstTran->iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to parse XML resp",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Process the parsed XML response */
		rv = procParsedResp(pstTran, &stMetaData, szMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to process parsed response",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseSSIResp
 *
 * Description	: This function is responsible for parsing the XML response got
 * 					from the payment host. The parsing is done by looking for
 * 					specific information in the xml message based on SSI spec
 * 					for the current function and command of the transaction.
 *
 * Input Params	:	pstMeta	-> placeholder for parsed information
 * 					szXML	-> xml response got from the server
 * 					iFxn	-> current transaction's function
 * 					iCmd	-> current transaction's command
 *
 * Output Params: SUCCESS / FAILURE/ ERR_INV_XML_MSG
 * ============================================================================
 */
static int parseSSIResp(META_PTYPE pstMeta, char * szXML, int iFxn, int iCmd)
{
	int			rv				= SUCCESS;
	int			iLen			= 0;
	xmlDoc *	docPtr			= NULL;
	xmlNode *	rootPtr			= NULL;
	xmlChar *	pszVal			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check the parameters passed */
		if((szXML == NULL) || ((iLen = strlen(szXML)) == 0) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* 
		 * -------------------------- VIKRAM DATT RANA -----------------------
		 * FIXME: This is not a good idea, but has been done to prevent an
		 * error being given by libcurl while parsing the xml response coming
		 * from PWC. The PWC guys are not sending correct form of xml response,
		 * causing the following error
		 * -------------------------------------------------------------------
		 * Entity: line 1: parser error : Document labelled UTF-16 but has
		 * UTF-8 content
		 * <?xml version="1.0" encoding="utf-16"?><MSH
		 * TYPE="21"><HEADER><DEVTYPE>Mx915P</DEVTYPE>
		 * -------------------------------------------------------------------
		 */
		replaceUTF8WithUTF16(szXML);

		iLen = strlen(szXML);

		debug_sprintf(szDbgMsg,"%s: Length of PWC resp = %d",__FUNCTION__,iLen);
		APP_TRACE(szDbgMsg);

		/* Make the libxml parse the xml message */
		docPtr = xmlParseMemory(szXML, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: libxml FAILED to parse the XML data",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Get the root of the xml message */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to find root of the xml data",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Check for the root string */
		if( (strcasecmp((char *) (rootPtr->name), SSIRES_STR) != SUCCESS) &&
				(strcasecmp((char *) (rootPtr->name), SSIADM_STR) != SUCCESS) )
		{
			debug_sprintf(szDbgMsg, "%s: Unrecognized xml response format",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Check the RSA FLAG for the root element of the xml response, this
		 * check is needed to ensure if the device resync is needed or not */
		pszVal = xmlGetProp(rootPtr, (const xmlChar *)"RSA");
		if( (pszVal != NULL) && (*pszVal == '1') )
		{
			if(*pszVal == '1')
			{
				/* Device resync is needed */
				setAdminPacketRequestStatus(PAAS_TRUE);

				/* Set the flag for device resync request in the
				* config.usr1 */
				setDevAdminRqd(PAAS_TRUE);
			}

			xmlFree(pszVal);
		}

		/* Get the meta data ready for parsing the response from PWC */
		rv = getMetaDataForSSIResp(pstMeta, iFxn, iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get key list", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Parse the xml message based on the meta data */
		rv = parseXMLMsg(docPtr, pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to parse PWC resp",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	/* Free the xml document */
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: replaceUTF8WithUTF16
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void replaceUTF8WithUTF16(char * szXML)
{
	char *	cPtr1			= NULL;
	char *	cPtr2			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	cPtr1 = strstr(szXML, "encoding=\"utf-16\"");
	if(cPtr1 != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Replacing utf-16 by utf-8", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		cPtr2 = strstr(cPtr1, "utf-16");
		if(cPtr2 != NULL)
		{
			memcpy(cPtr2, "utf-8\" ", strlen("utf-8\" "));
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * End of file ssiMsgCtrl.c
 * ============================================================================
 */
