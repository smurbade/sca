/*
 * =============================================================================
 * Filename    : ssiComm.c
 *
 * Application : Mx Point SCA
 *
 * Description : This module initializes the curl handler and posts transaction 
 * 				 to host over HTTPS sever using curl library interfaces. It is
 * 				 also responsible for host host connection availability check.
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include <openssl/crypto.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <svc.h>

#include <svc_net.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <syslog.h>
#include <setjmp.h>
#include <arpa/nameser.h>
#include <resolv.h>

#include "bLogic/bLogicCfgDef.h"
#include "common/common.h"
#include "ssi/ssiHostCfg.h"
#include "ssi/ssiCfgDef.h"
#include "ssi/ssiComm.h"
#include "ssi/ssiDef.h"
#include "common/utils.h"
#include "common/tranDef.h"
#include "appLog/appLogAPIs.h"
#include "db/safDataStore.h"

#define CA_CERT_FILE	"ca-bundle.crt"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

/*
 * ----------------------------------------------------------------------------
 * Some certificate information:
 *
 * PEM Format: .pem, .crt, .cer and .key
 *
 * DER Format:
 * ----------------------------------------------------------------------------
 */
//static struct curl_slist *	headers			= NULL;

static PAAS_BOOL		gbCheckingHostConn;
static pthread_t		ptKeepAliveIntId;
static pthread_t		ptChkHostConnStatus;
static CURL             *gptSSICurlHandle = NULL;
static pthread_mutex_t  gptSSICurlHandleMutex;
static sigjmp_buf		keepAliveJmpBuf;
static sigjmp_buf		hostDownJmpBuf;

/* Static functions' declarations */
static int		chkConn(CURL *);
//static int	doPINGTest(HOST_URL_ENUM);
//static int	getURLToPing(char *, HOST_URL_ENUM);
int 			getCURLHandle(HOST_URL_ENUM);
int 			cleanCURLHandle(PAAS_BOOL);
static void * 	keepAliveIntervalTimer(void *);
static void * 	checkHostConnectionStatus(void *);
static int		initSSICURLHandle(CURL **, HOST_URL_ENUM);
static int  	initBAPICURLHandle(CURL ** , HOST_URL_ENUM );
static int		initializeRespData(RESP_DATA_PTYPE stRespPtr);
static int		sendDataToHost(CURL *, char *, int, char **, int *, int);
static void 	commonInit(CURL *, PAAS_BOOL);
static size_t 	saveResponse(void *, size_t, size_t, void *);
static void 	keepAliveTimeSigHandler(int);
static void 	hostDownSigHandler(int );
static int      initializeOpenSSLLocks();

static int sendDataToHost_1( char * , int ,
												char ** , int * , HOST_URL_ENUM );

// extern functions declarations
extern int checkAndSwitchURL(int iCmd, int hostResult, PAAS_BOOL bForceFlag, PAAS_BOOL bCleanCurlHandle);

#ifdef DEBUG
/* Static functions' declarations */
static int 		curlDbgFunc(CURL *, curl_infotype, char *, size_t, void *);
#endif

/* we have this global to let the callback get easy access to it */
static pthread_mutex_t *lockarray;

static pthread_mutex_t  gptSSIHostConStatusMutex;

static pthread_mutex_t  gptHostTypeMutex;

static int	host				= PRIMARY_URL;

static PAAS_BOOL gbHostConnectionStatus = PAAS_TRUE;

/*
 * ============================================================================
 * Function Name: initSsiIFace
 *
 * Description	:
 *
 * Input Params	: Nothing
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int initSsiIFace()
{
	int				rv				= SUCCESS;
	int				iKeepAlive		= -1;
	char *			pszCURLVer		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iKeepAlive = getKeepAliveParameter();

#if 0
	debug_sprintf(szDbgMsg, "%s: [CURLCALL]  curl_global_init called in [%s] from Line[%d]",
						__FUNCTION__, __FUNCTION__, __LINE__);
	APP_TRACE(szDbgMsg);

	/* Initialize the CURL library */
	curl_global_init(CURL_GLOBAL_ALL);
#endif

	/*
	 * Praveen_P1: We have started getting problem with the SAF and Online
	 * transactions together. To make it thread safe, we have followed the
	 * following link
	 * http://curl.haxx.se/libcurl/c/threaded-ssl.html
	 *
	 */
	rv = initializeOpenSSLLocks();

	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while initializing OpenSSL Lock Callbacks", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return rv;
	}

	/* Get the CURL library version and print it in the logs */
	pszCURLVer = curl_version();

	/* VDR: TODO: Add a syslog statement */
	debug_sprintf(szDbgMsg, "%s: CURL Ver: %s", __FUNCTION__, pszCURLVer);
	APP_TRACE(szDbgMsg);

	/* Add data to the headers */
	//headers = curl_slist_append(headers, "Content-Type:text/xml");

	if(pthread_mutex_init(&gptSSICurlHandleMutex, NULL))//Initialize the SSI curl Handle Mutex
	{
		debug_sprintf(szDbgMsg, "%s - Failed to create SSI Curl Handle Mutex!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(pthread_mutex_init(&gptSSIHostConStatusMutex, NULL))//Initialize the SSI Host Connection Status Mutex
	{
		debug_sprintf(szDbgMsg, "%s - Failed to create SSI Host Connection Status Mutex!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(pthread_mutex_init(&gptHostTypeMutex, NULL))//Initialize the Host Type Mutex
	{
		debug_sprintf(szDbgMsg, "%s - Failed to create Host Type Mutex!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(iKeepAlive == KEEP_ALIVE_DEFINED_TIME)
	{
		//Open the keep Alive Interval Thread
		debug_sprintf(szDbgMsg, "%s - Starting the keepAliveIntervalThread ...", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pthread_create(&ptKeepAliveIntId, NULL, keepAliveIntervalTimer, NULL);
	}

	if(isCheckHostConnStatusEnable() == PAAS_TRUE)
	{
		//Open the keep Alive Interval Thread
		debug_sprintf(szDbgMsg, "%s - Starting the Host Connection Status Tracking Thread ...", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pthread_create(&ptChkHostConnStatus, NULL, checkHostConnectionStatus, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
		
/*
 * ============================================================================
 * Function Name: closeSsiIFace
 *
 * Description	: This function is responsible for closing the SSI interface
 *
 * Input Params	: Nothing
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int closeSsiIFace()
{
	int		rv				= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	curl_global_cleanup();

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: cleanCURLHandle
 *
 * Description	: This function cleans/closes the opened CURL Handle
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int cleanCURLHandle(PAAS_BOOL iForceClose)
{
	int				rv				= SUCCESS;
	static 	int		iKeepAlive		= -1;
#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	if(iKeepAlive == -1)
	{
		iKeepAlive = getKeepAliveParameter();
	}

	acquireMutexLock(&gptSSICurlHandleMutex, "SSI Curl Handle Mutex");

	//debug_sprintf(szDbgMsg, "%s: Enter, gptSSICurlHandle [%p]", __FUNCTION__, gptSSICurlHandle);
	//APP_TRACE(szDbgMsg);

	if((iForceClose == PAAS_TRUE) || (iKeepAlive == KEEP_ALIVE_DISABLED))
	{
		if(gptSSICurlHandle != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Cleaning up the CURL handle from if cond", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			curl_easy_cleanup(gptSSICurlHandle);
			gptSSICurlHandle = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No need to clean CURL Handle", __FUNCTION__);
		APP_TRACE(szDbgMsg);

	}

	releaseMutexLock(&gptSSICurlHandleMutex, "SSI Curl Handle Mutex");

	return rv;
}

/*
 * ============================================================================
 * Function Name: releaseSSICurlHandleLock
 *
 * Description	: This function will release the mutex on Curl handler
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
void releaseSSICurlHandleLock()
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Releasing SSI Curl Handler", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	releaseMutexLock(&gptSSICurlHandleMutex, "SSI Curl Handle Mutex");

	debug_sprintf(szDbgMsg, "%s:Retuning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getHostConnectionStatus
 *
 * Description	: This function gives the current status of Host connection
 *
 * Input Params	:
 *
 * Output Params: TRUE / FALSE
 * ============================================================================
 */
PAAS_BOOL getHostConnectionStatus()
{
	PAAS_BOOL brv;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	acquireMutexLock(&gptSSICurlHandleMutex, "SSI Curl Handle Mutex");

	brv = gbHostConnectionStatus;

	releaseMutexLock(&gptSSICurlHandleMutex, "SSI Curl Handle Mutex");

	debug_sprintf(szDbgMsg, "%s: Connection Status %d", __FUNCTION__, brv);
	APP_TRACE(szDbgMsg);

	return brv;
}

/*
 * ============================================================================
 * Function Name: updateHostConnectionStatus
 *
 * Description	: This function updates the status of Host connection
 *
 * Input Params	:
 *
 * Output Params: NONE
 * ============================================================================
 */
void updateHostConnectionStatus(PAAS_BOOL bCurrentStatus)
{
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	acquireMutexLock(&gptSSICurlHandleMutex, "SSI Curl Handle Mutex");

	gbHostConnectionStatus = bCurrentStatus;

	releaseMutexLock(&gptSSICurlHandleMutex, "SSI Curl Handle Mutex");

	debug_sprintf(szDbgMsg, "%s: Connection Status %d", __FUNCTION__, gbHostConnectionStatus);
	APP_TRACE(szDbgMsg);

	return ;
}



/*
 * ============================================================================
 * Function Name: static void * checkHostConnectionStatus(void * arg)
 *
 * Description	: Thread to run the keep Alive Interval timer
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void * checkHostConnectionStatus(void * arg)
{
	int			iRetVal							= 0;
	int			iAppLogEnabled					= isAppLogEnabled();
	int			iVal							= 0;
	char		szAppLogData[300]				= "";
	char		szAppLogDiag[300]				= "";
	PAAS_BOOL 	bCurrentHostStatus				= PAAS_FALSE;
	PAAS_BOOL 	bIsHostWentOffline 				= PAAS_TRUE;

#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	/* Add signal handler for SIG_HOST_DOWN signal */
	if(SIG_ERR == signal(SIG_HOST_DOWN, hostDownSigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIG_HOST_DOWN",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Check Host Connection Status Thread Started", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iVal = sigsetjmp(hostDownJmpBuf, 1);
	if(iVal == 1)
	{
		debug_sprintf(szDbgMsg, "%s: Received Host Down Information", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			//strcpy(szAppLogData, "Resetting Keep Alive Interval Timer");
			//addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}
	}

	while(1)
	{
		bCurrentHostStatus = getHostConnectionStatus();

		if(bCurrentHostStatus == PAAS_FALSE)
		{
			gbCheckingHostConn = PAAS_TRUE;

			/*-------Step2: Check whether connection is available with the SSI Host-------- */
			iRetVal = waitForHostConnection(&bIsHostWentOffline, 2); //29 Sep 2016: Praveen_P1: Passing additional parameter to print differnt app log statement
			if(iRetVal == FAILURE)
			{
				debug_sprintf(szDbgMsg, "%s: Error, while waiting for PWC!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				svcWait(1000);
				continue;
			}

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Host Connection is Available");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			gbCheckingHostConn = PAAS_FALSE;

			updateHostConnectionStatus(PAAS_TRUE); //Updating the host status
		}

		svcWait(720000); // keep the thread to run permanently by waiting
	}

	return NULL;
}

/*
 * ============================================================================
 * Function Name: hostDownSigHandler
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static void hostDownSigHandler(int iSigNo)
{
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(iSigNo == SIG_HOST_DOWN)
	{
		debug_sprintf(szDbgMsg, "%s: Signal SIG_HOST_DOWN delivered", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		siglongjmp(hostDownJmpBuf, 1);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Unknown signal", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	return;
}

/*
 * ===================================================================================================================
 * Function Name: resetKAIntervalUpdationTimer
 *
 * Description	: Sends the signal to CheckHostConnectionStatus thread to check the Host current status
 *
 * Input Params	:
 *
 * Output Params: none
 * =====================================================================================================================
 */
int sendHostDownSignal()
{
	int		iRetVal = SUCCESS;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(isCheckHostConnStatusEnable() == PAAS_TRUE ) //If this is enabled only send the signal
	{
		if(gbCheckingHostConn == PAAS_FALSE)
		{
			debug_sprintf(szDbgMsg, "%s: CheckHostConnectionStatus is enabled, sending signal", __FUNCTION__);
			APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s: Check Host Connection Status thread id: %d", __FUNCTION__, (int)ptChkHostConnStatus);
			APP_TRACE(szDbgMsg);
#endif
			//Check for the thread before killing.
			if(ptChkHostConnStatus != 0)
			{
				iRetVal = pthread_kill(ptChkHostConnStatus, SIG_HOST_DOWN);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Already checking for the host connection. Not sending signal again", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: CheckHostConnectionStatus is not enabled, not sending signal", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: static void * keepAliveIntervalTimer(void * arg)
 *
 * Description	: Thread to run the keep Alive Interval timer
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void * keepAliveIntervalTimer(void * arg)
{
	int			iKeepAliveInterval				= 0;
	int			iAppLogEnabled					= isAppLogEnabled();
	int			iVal							= 0;
	ullong		curTime							= 0;
	ullong		endTime							= 0;
	char		szAppLogData[300]				= "";
	char		szAppLogDiag[300]				= "";

#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	/* Add signal handler for SIG_KA_TIME SETTIME */
	if(SIG_ERR == signal(SIG_KA_TIME, keepAliveTimeSigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIG_KA_TIME",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Keep Alive Interval Thread Started", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iKeepAliveInterval = getKeepAliveInterval();
	iVal = sigsetjmp(keepAliveJmpBuf, 1);
	if(iVal == 1)
	{
		debug_sprintf(szDbgMsg, "%s: Resetting the Keep Alive Interval Timer",
								__FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Resetting Keep Alive Interval Timer");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}
	}

	curTime = svcGetSysMillisec();
	endTime = curTime + (iKeepAliveInterval * 60 * 1000L);

	while(1)
	{
		while(endTime >= curTime)
		{
			svcWait(100); // Sleep little by little in the loop for KeepAliveInterval Duration
			curTime = svcGetSysMillisec();
		}

		if(gptSSICurlHandle != NULL)
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Keep Alive Timer Elapsed, Cleaning Curl Handler");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			cleanCURLHandle(PAAS_TRUE);
		}
		svcWait(360000); // keep the thread to run permanently by waiting
	}

	return NULL;
}

/*
 * ============================================================================
 * Function Name: keepAliveTimeSigHandler
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static void keepAliveTimeSigHandler(int iSigNo)
{
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(iSigNo == SIG_KA_TIME)
	{
		debug_sprintf(szDbgMsg, "%s: Signal SIG_KA_TIME delivered", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		siglongjmp(keepAliveJmpBuf, 1);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Unknown signal", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	return;
}

/*
 * ===================================================================================================================
 * Function Name: resetKAIntervalUpdationTimer
 *
 * Description	: Sends the signal to Keep Alive Interval thread to reset the timer once set time command is received
 *
 * Input Params	:
 *
 * Output Params: none
 * =====================================================================================================================
 */
int resetKAIntervalUpdationTimer()
{
	int		iRetVal = SUCCESS;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Keep Alive Interval Timer thread id: %d", __FUNCTION__, (int)ptKeepAliveIntId);
	APP_TRACE(szDbgMsg);

	//Check for the thread before killing.
	if(ptKeepAliveIntId != 0)
	{
		iRetVal = pthread_kill(ptKeepAliveIntId, SIG_KA_TIME);
	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getCURLHandle
 *
 * Description	: This function returns the curl Handle according to keep Alive configuration parameter
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getCURLHandle(HOST_URL_ENUM host)
{
	int				rv 						= SUCCESS;
	char			szAppLogData[300]		= "";
	char			szAppLogDiag[300]		= "";
	int				iAppLogEnabled			= isAppLogEnabled();
	static 	int		iKeepAlive				= -1;

#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	if(iKeepAlive == -1)
	{
		iKeepAlive = getKeepAliveParameter();
	}

	acquireMutexLock(&gptSSICurlHandleMutex, "SSI Curl Handle Mutex");

	debug_sprintf(szDbgMsg, "%s: gptSSICurlHandle ---- [%p]", __FUNCTION__, gptSSICurlHandle);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(gptSSICurlHandle == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: CURL Handle Not Available, Create CURL Handle", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Curl Handler Not Available, Creating New One");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			rv = initSSICURLHandle(&gptSSICurlHandle, host);
		}
		else
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Using Existing Curl Handler for Posting Data");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
		}

		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: CURL Handle Init Failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Initialization of Curl Handler Failed.");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
			}

			break;
		}

		if(iKeepAlive == KEEP_ALIVE_DEFINED_TIME && (ptKeepAliveIntId != 0))
		{
			rv = pthread_kill(ptKeepAliveIntId, SIG_KA_TIME);
		}

		break;
	}

	releaseMutexLock(&gptSSICurlHandleMutex, "SSI Curl Handle Mutex");

	return rv;
}

#if 0

/*
 * ============================================================================
 * Function Name: postDataToSSIHost
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int postDataToSSIHost(char * req, int reqSize, char ** resp, int * respSize)
{
	int					rv				= SUCCESS;
	int					iCnt			= 0;
	static int			host			= PRIMARY_URL;
	CURL *				curlHandle		= NULL;
	PAAS_BOOL			certValdReq 	= PAAS_FALSE;
	char				szTmpURL[100]	= "";
	HOSTDEF_STRUCT_TYPE	hostDef;
	struct curl_slist 		*headers = NULL;
	char			szCurlErrBuf[4096] = "";
	RESP_DATA_STYPE	stRespData;

#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	if(strlen(req) < 4500)
	{
		debug_sprintf(szDbgMsg, "%s:%s: HOST REQ = [%s]",DEVDEBUGMSG,__FUNCTION__, req);
		APP_TRACE(szDbgMsg);
	}
#endif



	while(iCnt < 2)
	{
		//rv = getCURLHandle(host);
		//rv = initSSICURLHandle(&curlHandle, host);

		memset(&hostDef, 0x00, sizeof(hostDef));

		rv = getSSIHostDetails(host, &hostDef);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the host details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if (strncmp(hostDef.url, "https", 5) == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Url: %s",__FUNCTION__, hostDef.url);
			APP_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: Url starts with https. Cert Validation required",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			certValdReq = PAAS_TRUE;
		}

		headers = curl_slist_append(headers, "Content-Type:text/xml");

		/* Initialize the CURL library */
		curl_global_init(CURL_GLOBAL_DEFAULT);

		/* Initialize the handle using the CURL library */
		curlHandle = curl_easy_init();
		if(curlHandle == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Curl initialization for handle failed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the URL */
		if(hostDef.portNum != 0)
		{
			sprintf(szTmpURL, "%s:%d", hostDef.url, hostDef.portNum);
			curl_easy_setopt(curlHandle, CURLOPT_URL, szTmpURL);

			debug_sprintf(szDbgMsg, "%s: Setting PWC URL = [%s]", __FUNCTION__,
																	szTmpURL);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			curl_easy_setopt(curlHandle, CURLOPT_URL, hostDef.url);

			debug_sprintf(szDbgMsg, "%s: Setting PWC URL = [%s]", __FUNCTION__,
																hostDef.url);
			APP_TRACE(szDbgMsg);
		}

		/* Set the connection timeout */
		curl_easy_setopt(curlHandle, CURLOPT_CONNECTTIMEOUT, hostDef.conTimeOut);

		/* Set the total timeout */
		if(hostDef.respTimeOut > 0)
		{
			curl_easy_setopt(curlHandle, CURLOPT_TIMEOUT,
							hostDef.conTimeOut + hostDef.respTimeOut);
		}


		/* Set the other common features */
		//commonInit(curlHandle, certValdReq);

		/* Set the NO SIGNAL option, This option is very important to set in case
		 * of multi-threaded applications like ours, because otherwise the libcurl
		 * uses signal handling for the communication and that causes the threads
		 * to go crazy and even crash. Fix done for issue 1659 (Church of LDS AmDocs
		 * Case# 130826-3983) */
		curl_easy_setopt(curlHandle, CURLOPT_NOSIGNAL, 1L);

		/* Set the HTTP post option */
		curl_easy_setopt(curlHandle, CURLOPT_POST, 1L);

		/* Add the headers */
		curl_easy_setopt(curlHandle, CURLOPT_HTTPHEADER, headers);

	#ifdef DEBUG
		/* Add the debug function */
		curl_easy_setopt(curlHandle, CURLOPT_DEBUGFUNCTION, curlDbgFunc);
	#endif

		if (certValdReq == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Url starts with https. Doing certificate validation",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYPEER, 1L);
			curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYHOST, 2L);
		}

		/* Set the CA certificate */
		curl_easy_setopt(curlHandle, CURLOPT_CAINFO, CA_CERT_FILE);

		/* Set the write function */
		curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, saveResponse);

		/* Set the detection as TRUE for HTTP errors */
		curl_easy_setopt(curlHandle, CURLOPT_FAILONERROR, PAAS_TRUE);

		/* To request using TLS for the transfer */
		curl_easy_setopt(curlHandle, CURLOPT_USE_SSL, CURLUSESSL_NONE);

		/* To set preferred TLS/SSL version */
		curl_easy_setopt(curlHandle, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);

		if(rv == SUCCESS)
		{
			/*
			 * CURL Handler was initialized successfully
			 */
			//acquireMutexLock(&gptSSICurlHandleMutex, "SSI Curl Handle Mutex");
			//rv = sendDataToHost(curlHandle, req, reqSize, resp, respSize);
			//releaseMutexLock(&gptSSICurlHandleMutex, "SSI Curl Handle Mutex");

			while(1)
			{
				/* Initialize the response data */
				rv = initializeRespData(&stRespData);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Initialization of response data FAILED"
																		, __FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}

				/* Set some curl library options before sending the data */
				curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &stRespData);
				curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, (void *) req);
				curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDSIZE, reqSize);
				curl_easy_setopt(curlHandle, CURLOPT_ERRORBUFFER, szCurlErrBuf);

				debug_sprintf(szDbgMsg, "%s: Posting the data to the server", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* post the data to the server */
				rv = curl_easy_perform(curlHandle);

				debug_sprintf(szDbgMsg, "%s: curl_easy_perform done, rv = %d", __FUNCTION__, rv);
				APP_TRACE(szDbgMsg);

				if(rv == CURLE_OK)
				{
					*resp = stRespData.szMsg;
					*respSize = stRespData.iWritten;

					debug_sprintf(szDbgMsg, "%s: Response Len = [%d]", __FUNCTION__,
																stRespData.iWritten);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					switch(rv)
					{
					case CURLE_UNSUPPORTED_PROTOCOL: /* 1 */
						/*
						 * The URL you passed to libcurl used a protocol that this libcurl does not support.
						 * The support might be a compile-time option that you didn't use,
						 * it can be a misspelled protocol string or just a protocol libcurl has no code for.
						 */
						debug_sprintf(szDbgMsg, "%s: protocol not supported by library",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;

					case CURLE_URL_MALFORMAT: /* 3 */
						/*
						 * The URL was not properly formatted.
						 */
						debug_sprintf(szDbgMsg, "%s: URL not properly formatted",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CFG_PARAMS;
						break;

					case CURLE_COULDNT_RESOLVE_HOST: /* 6 */
						/*
						 * Couldn't resolve host. The given remote host was not resolved.
						 */
						debug_sprintf(szDbgMsg, "%s: Couldnt resolve server's address",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_COULDNT_CONNECT: /* 7 */
						/*
						 * Failed to connect() to host or proxy.
						 */
						debug_sprintf(szDbgMsg, "%s: Failed to connect server",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_HTTP_RETURNED_ERROR: /* 22 */
						/*
						 * This is returned if CURLOPT_FAILONERROR is set TRUE
						 * and the HTTP server returns an error code that is >= 400.
						 */
						debug_sprintf(szDbgMsg, "%s: Got HTTP error while posting",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_WRITE_ERROR: /* 23 */
						/*
						 * An error occurred when writing received data to a local file,
						 * or an error was returned to libcurl from a write callback.
						 */
						debug_sprintf(szDbgMsg, "%s: Failed to save the response data",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;

					case CURLE_OUT_OF_MEMORY: /* 27 */
						/*
						 * A memory allocation request failed.
						 * This is serious badness and things are severely screwed up if this ever occurs.
						 */
						debug_sprintf(szDbgMsg, "%s: Facing memory shortage ",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;

					case CURLE_OPERATION_TIMEDOUT: /* 28 */
						/*
						 * Operation timeout.
						 * The specified time-out period was reached according to the conditions.
						 */
						debug_sprintf(szDbgMsg, "%s: Timeout happened while receiving",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_RESP_TO;
						break;

					case CURLE_SSL_CONNECT_ERROR: /* 35 */
						/*
						 * A problem occurred somewhere in the SSL/TLS handshake.
						 *
						 */
						debug_sprintf(szDbgMsg, "%s: SSL/TLS Handshake FAILED",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_PEER_FAILED_VERIFICATION: /* 51 */
						/*
						 * The remote server's SSL certificate or SSH md5 fingerprint was deemed not OK.
						 */
						debug_sprintf(szDbgMsg,
									"%s: Server's SSL certificate verification FAILED",
									__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_SEND_ERROR: /* 55 */
						/*
						 * Failed sending network data.
						 */
						debug_sprintf(szDbgMsg, "%s: Failed to post the request data",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_RECV_ERROR: /* 56 */
						/*
						 * Failure with receiving network data.
						 */
						debug_sprintf(szDbgMsg, "%s: Failed to receive the response",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_SSL_CACERT: /* 60 */
						/*
						 * Peer certificate cannot be authenticated with known CA certificates.
						 */
						debug_sprintf(szDbgMsg,
										"%s: Couldnt authenticate peer certificate",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_SSL_CACERT_BADFILE: /* 77 */
						/*
						 * Problem with reading the SSL CA cert (path? access rights?)
						 */
						debug_sprintf(szDbgMsg, "%s: Cant find SSL CA certificate [%s]",
														__FUNCTION__, CA_CERT_FILE);
						APP_TRACE(szDbgMsg);

						rv = ERR_CFG_PARAMS;
						break;

					default:
						debug_sprintf(szDbgMsg, "%s: Error [%d] occured while posting",
																	__FUNCTION__, rv);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;
					}

					/* Deallocate the allocated memory */
					if(stRespData.szMsg != NULL)
					{
						free(stRespData.szMsg);
						stRespData.szMsg = NULL;
					}
		#ifdef DEVDEBUG
					if(strlen(szDbgMsg) < 5000)
					{
						debug_sprintf(szDbgMsg, "%s: Lib Curl Error [%s]",__FUNCTION__, szCurlErrBuf);
						APP_TRACE(szDbgMsg);
					}
		#endif

				}

				break;
			}

			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post data to server",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

#if 0
		if(rv == SUCCESS)
		{
			cleanCURLHandle(PAAS_FALSE);
			break;
		}

		cleanCURLHandle(PAAS_TRUE); //Closing the current curl handler before trying the next host url
#endif

		if(curlHandle != NULL)
		{
			/* always cleanup */
			curl_easy_cleanup(curlHandle);
			curl_global_cleanup();
		}

		if(rv == SUCCESS)
		{
			break;
		}
		/* If the first host is not working then try the next host */
		host ^= 1;
		iCnt++;

		debug_sprintf(szDbgMsg, "%s: Trying again..", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: postSAFDataToSSIHost
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int postSAFDataToSSIHost(char * req, int reqSize, char ** resp, int * respSize)
{
	int					rv				= SUCCESS;
	int					iCnt			= 0;
	static int			host			= PRIMARY_URL;
	CURL *				curlHandle		= NULL;
	char				szTmpURL[100]	= "";
	HOSTDEF_STRUCT_TYPE	hostDef;
	PAAS_BOOL			certValdReq 	= PAAS_FALSE;
	struct curl_slist 		*headers = NULL;
	char			szCurlErrBuf[4096] = "";
	RESP_DATA_STYPE	stRespData;

#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	if(strlen(req) < 4500)
	{
		debug_sprintf(szDbgMsg, "%s:%s: HOST REQ = [%s]",DEVDEBUGMSG,__FUNCTION__, req);
		APP_TRACE(szDbgMsg);
	}
#endif

	while(iCnt < 2)
	{
		//rv = initSSICURLHandle(&curlHandle, host);

		memset(&hostDef, 0x00, sizeof(hostDef));

		rv = getSSIHostDetails(host, &hostDef);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the host details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if (strncmp(hostDef.url, "https", 5) == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Url: %s",__FUNCTION__, hostDef.url);
			APP_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: Url starts with https. Cert Validation required",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			certValdReq = PAAS_TRUE;
		}

		/* Initialize the CURL library */
		curl_global_init(CURL_GLOBAL_DEFAULT);

		/* Initialize the handle using the CURL library */
		curlHandle = curl_easy_init();
		if(curlHandle == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Curl initialization for handle failed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the URL */
		if(hostDef.portNum != 0)
		{
			sprintf(szTmpURL, "%s:%d", hostDef.url, hostDef.portNum);
			curl_easy_setopt(curlHandle, CURLOPT_URL, szTmpURL);

			debug_sprintf(szDbgMsg, "%s: Setting PWC URL = [%s]", __FUNCTION__,
																	szTmpURL);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			curl_easy_setopt(curlHandle, CURLOPT_URL, hostDef.url);

			debug_sprintf(szDbgMsg, "%s: Setting PWC URL = [%s]", __FUNCTION__,
																hostDef.url);
			APP_TRACE(szDbgMsg);
		}

		/* Set the connection timeout */
		curl_easy_setopt(curlHandle, CURLOPT_CONNECTTIMEOUT, hostDef.conTimeOut);

		/* Set the total timeout */
		if(hostDef.respTimeOut > 0)
		{
			curl_easy_setopt(curlHandle, CURLOPT_TIMEOUT,
							hostDef.conTimeOut + hostDef.respTimeOut);
		}

		/* Set the other common features */
		//commonInit(curlHandle, certValdReq);

		/* Set the NO SIGNAL option, This option is very important to set in case
		 * of multi-threaded applications like ours, because otherwise the libcurl
		 * uses signal handling for the communication and that causes the threads
		 * to go crazy and even crash. Fix done for issue 1659 (Church of LDS AmDocs
		 * Case# 130826-3983) */
		curl_easy_setopt(curlHandle, CURLOPT_NOSIGNAL, 1L);

		/* Set the HTTP post option */
		curl_easy_setopt(curlHandle, CURLOPT_POST, 1L);

		/* Add the headers */
		curl_easy_setopt(curlHandle, CURLOPT_HTTPHEADER, headers);

	#ifdef DEBUG
		/* Add the debug function */
		curl_easy_setopt(curlHandle, CURLOPT_DEBUGFUNCTION, curlDbgFunc);
	#endif

		if (certValdReq == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Url starts with https. Doing certificate validation",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYPEER, 1L);
			curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYHOST, 2L);
		}

		/* Set the CA certificate */
		curl_easy_setopt(curlHandle, CURLOPT_CAINFO, CA_CERT_FILE);

		/* Set the write function */
		curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, saveResponse);

		/* Set the detection as TRUE for HTTP errors */
		curl_easy_setopt(curlHandle, CURLOPT_FAILONERROR, PAAS_TRUE);

		/* To request using TLS for the transfer */
		curl_easy_setopt(curlHandle, CURLOPT_USE_SSL, CURLUSESSL_NONE);

		/* To set preferred TLS/SSL version */
		curl_easy_setopt(curlHandle, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);

		if(rv == SUCCESS)
		{
			/*
			 * CURL Handler was initialized successfully
			 */
			//rv = sendDataToHost(curlHandle, req, reqSize, resp, respSize);

			while(1)
			{
				/* Initialize the response data */
				rv = initializeRespData(&stRespData);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Initialization of response data FAILED"
																		, __FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}

				/* Set some curl library options before sending the data */
				curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &stRespData);
				curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, (void *) req);
				curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDSIZE, reqSize);
				curl_easy_setopt(curlHandle, CURLOPT_ERRORBUFFER, szCurlErrBuf);

				debug_sprintf(szDbgMsg, "%s: Posting the data to the server", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* post the data to the server */
				rv = curl_easy_perform(curlHandle);

				debug_sprintf(szDbgMsg, "%s: curl_easy_perform done, rv = %d", __FUNCTION__, rv);
				APP_TRACE(szDbgMsg);

				if(rv == CURLE_OK)
				{
					*resp = stRespData.szMsg;
					*respSize = stRespData.iWritten;

					debug_sprintf(szDbgMsg, "%s: Response Len = [%d]", __FUNCTION__,
																stRespData.iWritten);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					switch(rv)
					{
					case CURLE_UNSUPPORTED_PROTOCOL: /* 1 */
						/*
						 * The URL you passed to libcurl used a protocol that this libcurl does not support.
						 * The support might be a compile-time option that you didn't use,
						 * it can be a misspelled protocol string or just a protocol libcurl has no code for.
						 */
						debug_sprintf(szDbgMsg, "%s: protocol not supported by library",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;

					case CURLE_URL_MALFORMAT: /* 3 */
						/*
						 * The URL was not properly formatted.
						 */
						debug_sprintf(szDbgMsg, "%s: URL not properly formatted",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CFG_PARAMS;
						break;

					case CURLE_COULDNT_RESOLVE_HOST: /* 6 */
						/*
						 * Couldn't resolve host. The given remote host was not resolved.
						 */
						debug_sprintf(szDbgMsg, "%s: Couldnt resolve server's address",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_COULDNT_CONNECT: /* 7 */
						/*
						 * Failed to connect() to host or proxy.
						 */
						debug_sprintf(szDbgMsg, "%s: Failed to connect server",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_HTTP_RETURNED_ERROR: /* 22 */
						/*
						 * This is returned if CURLOPT_FAILONERROR is set TRUE
						 * and the HTTP server returns an error code that is >= 400.
						 */
						debug_sprintf(szDbgMsg, "%s: Got HTTP error while posting",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_WRITE_ERROR: /* 23 */
						/*
						 * An error occurred when writing received data to a local file,
						 * or an error was returned to libcurl from a write callback.
						 */
						debug_sprintf(szDbgMsg, "%s: Failed to save the response data",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;

					case CURLE_OUT_OF_MEMORY: /* 27 */
						/*
						 * A memory allocation request failed.
						 * This is serious badness and things are severely screwed up if this ever occurs.
						 */
						debug_sprintf(szDbgMsg, "%s: Facing memory shortage ",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;

					case CURLE_OPERATION_TIMEDOUT: /* 28 */
						/*
						 * Operation timeout.
						 * The specified time-out period was reached according to the conditions.
						 */
						debug_sprintf(szDbgMsg, "%s: Timeout happened while receiving",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_RESP_TO;
						break;

					case CURLE_SSL_CONNECT_ERROR: /* 35 */
						/*
						 * A problem occurred somewhere in the SSL/TLS handshake.
						 *
						 */
						debug_sprintf(szDbgMsg, "%s: SSL/TLS Handshake FAILED",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_PEER_FAILED_VERIFICATION: /* 51 */
						/*
						 * The remote server's SSL certificate or SSH md5 fingerprint was deemed not OK.
						 */
						debug_sprintf(szDbgMsg,
									"%s: Server's SSL certificate verification FAILED",
									__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_SEND_ERROR: /* 55 */
						/*
						 * Failed sending network data.
						 */
						debug_sprintf(szDbgMsg, "%s: Failed to post the request data",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_RECV_ERROR: /* 56 */
						/*
						 * Failure with receiving network data.
						 */
						debug_sprintf(szDbgMsg, "%s: Failed to receive the response",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_SSL_CACERT: /* 60 */
						/*
						 * Peer certificate cannot be authenticated with known CA certificates.
						 */
						debug_sprintf(szDbgMsg,
										"%s: Couldnt authenticate peer certificate",
																		__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;

					case CURLE_SSL_CACERT_BADFILE: /* 77 */
						/*
						 * Problem with reading the SSL CA cert (path? access rights?)
						 */
						debug_sprintf(szDbgMsg, "%s: Cant find SSL CA certificate [%s]",
														__FUNCTION__, CA_CERT_FILE);
						APP_TRACE(szDbgMsg);

						rv = ERR_CFG_PARAMS;
						break;

					default:
						debug_sprintf(szDbgMsg, "%s: Error [%d] occured while posting",
																	__FUNCTION__, rv);
						APP_TRACE(szDbgMsg);

						rv = ERR_CONN_FAIL;
						break;
					}

					/* Deallocate the allocated memory */
					if(stRespData.szMsg != NULL)
					{
						free(stRespData.szMsg);
						stRespData.szMsg = NULL;
					}
		#ifdef DEVDEBUG
					if(strlen(szDbgMsg) < 5000)
					{
						debug_sprintf(szDbgMsg, "%s: Lib Curl Error [%s]",__FUNCTION__, szCurlErrBuf);
						APP_TRACE(szDbgMsg);
					}
		#endif

				}

				break;
			}

			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post data to server",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		if(curlHandle != NULL)
		{
			/* always cleanup */
			curl_easy_cleanup(curlHandle);
			curl_global_cleanup();
		}

		if(rv == SUCCESS)
		{
			break;
		}

		/* If the first host is not working then try the next host */
		host ^= 1;
		iCnt++;

		debug_sprintf(szDbgMsg, "%s: Trying again..", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: postDataToSSIHost
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int postDataToSSIHost(char * req, int reqSize, char ** resp, int * respSize)
{
	int			rv					= SUCCESS;
	int			iCnt				= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
//	static int	host				= PRIMARY_URL;//MukeshS3: 3-Aug-16: Making it as global variable, which will shared between online as well as SAF thread
	char		szAppLogData[300]	= "";

	//CURL *		curlHandle		= NULL;
#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

#ifdef DEVDEBUG
	if(strlen(req) < 4500)
	{
		debug_sprintf(szDbgMsg, "%s:%s: HOST REQ = [%s]",DEVDEBUGMSG,__FUNCTION__, req);
		APP_TRACE(szDbgMsg);
	}
#endif

	/* Daivik:5/5/2016 - If both the URL are same, then we post to a single URL only.
	 *
	 */
	do
	{
		rv = getCURLHandle(host);

		if(rv == SUCCESS)
		{
			/*
		 	 * CURL Handler was initialized successfully
		 	 */
			acquireMutexLock(&gptSSICurlHandleMutex, "SSI Curl Handle Mutex");
			rv = sendDataToHost(gptSSICurlHandle, req, reqSize, resp, respSize, host);
			releaseMutexLock(&gptSSICurlHandleMutex, "SSI Curl Handle Mutex");

			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post data to server",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(rv == ERR_RESP_TO) //Incase of response timeout we should not try with the next url
				{
					debug_sprintf(szDbgMsg, "%s: Response Time out from the host",
											__FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
			}

			if((rv == SUCCESS) && (*resp))
			{
				//cleanCURLHandle(PAAS_FALSE);	// Mukesh: 11-8-16: Curl has to be cleaned always(without considering rv value) depending upon keepalive parameter. So, moved at end of function.
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Request Successfully Posted and Received Response Successfully From SSI Host");
					addAppEventLog(SCA, PAAS_SUCCESS, RECEIVE, szAppLogData, NULL);

					addAppEventLog(SCA, PAAS_INFO, SSI_RESPONSE, *resp, NULL);
				}
				break;
			}

			//Mukesh: moved inside of checkAndSwitchURLIfReqd function
			//cleanCURLHandle(PAAS_TRUE); //Closing the current curl handler before trying the next host url
		}
		else
		{
			rv = FAILURE;
		}

		/* If the first host is not working then try the next host */
		//host ^= 1;
		iCnt++;

		debug_sprintf(szDbgMsg, "%s: Trial :%d", __FUNCTION__,iCnt);
		APP_TRACE(szDbgMsg);
	}while(iCnt < getNumUniqueHosts() && checkAndSwitchURL(-1, -1, PAAS_TRUE, PAAS_TRUE) == SUCCESS);

	cleanCURLHandle(PAAS_FALSE);
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: postDataToProxyUrl
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int postDataToProxyUrl(char * req, int reqSize, char ** resp, int * respSize, CREDITAPPDTLS_PTYPE pstCreditAppDtls)
{
	int			rv					= SUCCESS;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
	CURL *		curlHandle			= NULL;
	PAAS_BOOL	certValdReq			= PAAS_FALSE;	// CID-67378: 25-Jan-16: MukeshS3: Initialize it with FALSE value, if url starts with https,
													// it will be changed to TRUE value.

#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

#ifdef DEVDEBUG
	if(strlen(req) < 4500)
	{
		debug_sprintf(szDbgMsg, "%s:%s: PROXY REQ = [%s]",DEVDEBUGMSG,__FUNCTION__, req);
		APP_TRACE(szDbgMsg);
	}
#endif

	if (strncmp(pstCreditAppDtls->szForwardProxy, "https", 5) == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Url: %s",__FUNCTION__, pstCreditAppDtls->szForwardProxy);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Url starts with https. Cert Validation required",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);
		certValdReq = PAAS_TRUE;
	}

#if 0
	debug_sprintf(szDbgMsg, "%s: [CURLCALL]  curl_global_init called in [%s] from Line[%d]",
							__FUNCTION__, __FUNCTION__, __LINE__);
	APP_TRACE(szDbgMsg);

	/* Initialize the CURL library */
	curl_global_init(CURL_GLOBAL_DEFAULT);
#endif


	/* Initialize the handle using the CURL library */
	curlHandle = curl_easy_init();
	if(curlHandle == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Curl initialization for handle failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	/* Set the URL */
	curl_easy_setopt(curlHandle, CURLOPT_URL, pstCreditAppDtls->szForwardProxy);

	/* Set the connection timeout */
	curl_easy_setopt(curlHandle, CURLOPT_CONNECTTIMEOUT, 10);

	/* Set the total timeout */
	if(pstCreditAppDtls->iProxyTimeout > 0)
	{
		curl_easy_setopt(curlHandle, CURLOPT_TIMEOUT, pstCreditAppDtls->iProxyTimeout);
	}
	else
	{
		/* Setting the default timeout value as 30*/
		curl_easy_setopt(curlHandle, CURLOPT_TIMEOUT, 30);
	}

	/* Set the other common features */
	commonInit(curlHandle, certValdReq);

	debug_sprintf(szDbgMsg, "%s: curlHandle [%p]", __FUNCTION__, curlHandle);
	APP_TRACE(szDbgMsg);

	if(rv == SUCCESS)
	{
		rv = sendDataToHost(curlHandle, req, reqSize, resp, respSize, AVOID_HOST_CONN_CHECK);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to post data to server",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
//	CID:67412 :T_POLISETTYG1: Logically dead code, This Condition will never Get executed
//	else
//	{
//		rv = FAILURE;
//	}

	if((rv == SUCCESS) && (*resp))
	{
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Request Successfully Posted and Received Response Successfully From SSI Host");
			addAppEventLog(SCA, PAAS_SUCCESS, RECEIVE, szAppLogData, NULL);

			addAppEventLog(SCA, PAAS_INFO, SSI_RESPONSE, *resp, NULL);
		}
	}

	curl_easy_cleanup(curlHandle);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: postSAFDataToSSIHost
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int postSAFDataToSSIHost(char * req, int reqSize, char ** resp, int * respSize)
{
	int			rv					= SUCCESS;
	int			iCnt				= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
//	static int	host				= PRIMARY_URL;//MukeshS3: 3-Aug-16: Making it as global variable, which will shared between online as well as SAF thread
	char		szAppLogData[300]	= "";
	//CURL *		curlHandle		= NULL;
#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

#ifdef DEVDEBUG
	if(strlen(req) < 4500)
	{
		debug_sprintf(szDbgMsg, "%s:%s: HOST REQ = [%s]",DEVDEBUGMSG,__FUNCTION__, req);
		APP_TRACE(szDbgMsg);
	}
#endif

	/* Daivik:5/5/2016 - If both the URL are same, then we post to a single URL only.
	 *
	 */
	do
	{
		rv = sendDataToHost_1(req, reqSize, resp, respSize, host);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to post data to server",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(rv == ERR_RESP_TO) //MukeshS3: PTMX-1592: 16-Sept-16: Incase of response timeout we should not try with the next url
			{
				debug_sprintf(szDbgMsg, "%s: Response Time out from the host",
										__FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}

		//curl_easy_cleanup(curlHandle);

		if((rv == SUCCESS) && (*resp))
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Request Successfully Posted and Received Response Successfully From SSI Host");
				addAppEventLog(SCA, PAAS_SUCCESS, RECEIVE, szAppLogData, NULL);

				addAppEventLog(SCA, PAAS_INFO, SSI_RESPONSE, *resp, NULL);
			}
			break;
		}
		/* If the first host is not working then try the next host */
		//host ^= 1;
		iCnt++;
		debug_sprintf(szDbgMsg, "%s: Trial :%d", __FUNCTION__,iCnt);
		APP_TRACE(szDbgMsg);
	}while(iCnt < getNumUniqueHosts() && checkAndSwitchURL(-1, -1, PAAS_TRUE, PAAS_FALSE) == SUCCESS);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: checkHostConn
 *
 * Description	: This Function is used to check the host connection. it will
 * 					create a new local curl handler every time using the global primary & secondary URLs alternatively
 *
 * Input Params	: Curl Handler
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int checkHostConn(int hostType)
{
	int			rv				= SUCCESS;
	int			iCnt			= 0;
//	static int	host			= PRIMARY_URL;
	CURL *		localCurlHandle		= NULL;
#ifdef DEBUG
	char		szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Daivik:5/5/2016 - If both the URL are same, then we post to a single URL only.
	 *
	 */
	do
	{
		/* Daivik:6/6/2016- This logic was added because we are now checking host connection
		 * before we post the data to host and we need to ensure we check for host connection only
		 * for the required host. When hosttype is -1 , we need to use the default logic of checkHostConn and use
		 * the static variable host from this function. If hosttype is AVOID_HOST_CONN , then we do not perform
		 * the host connectivity check. Apart from this hosttype will have values 0/1 for PRIM/SECONDARY URL.
		 */
		if(hostType == USE_DEFAULT_URL)
		{
			rv = initSSICURLHandle(&localCurlHandle, host);	// global host
		}
		else
		{
			rv = initSSICURLHandle(&localCurlHandle, hostType);	// passed from caller
		}

		debug_sprintf(szDbgMsg, "%s: curlHandle [%p]", __FUNCTION__, localCurlHandle);
		APP_TRACE(szDbgMsg);

		if(rv == SUCCESS)
		{
			/* Curl handler initialized successfully
			 * Checking connection */
			rv = chkConn(localCurlHandle);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to check Host connection",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Host connection available", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		if(localCurlHandle != NULL)
		{
			curl_easy_cleanup(localCurlHandle);
		}

		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Breaking from the loop", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		if(hostType != USE_DEFAULT_URL)
		{
			break;
		}
		/* If the first host is not working then try the next host */
		//host ^= 1;
		iCnt++;
	}while(iCnt < getNumUniqueHosts() && checkAndSwitchURL(-1, -1, PAAS_TRUE, PAAS_FALSE) == SUCCESS);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: sendDataToHost_1
 *
 * Description	: This function sends the XML data to the PWC server, using the
 * 					appropriate CURL handle.
 *
 * Input Params	:
 * 				PWC_URL_TYPE_ENUM urlType -> constant telling which handle to
 * 												use
 * 				unsigned char *	  data	  -> the XML data that needs to be sent
 * 				int				  size	  -> size of the data
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int sendDataToHost_1( char * req, int reqSize,
												char ** resp, int * respSize, HOST_URL_ENUM hostType)
{
	int						rv				   		= SUCCESS;
	int						iRetVal					= SUCCESS;
	int						iAppLogEnabled			= isAppLogEnabled();
	char					szCurlErrBuf[4096] 		= "";
	char					szTmpURL[100]			= "";
	char * 					pszCURLVer				= NULL;
	char					szAppLogData[300]		= "";
	char					szAppLogDiag[300]		= "";
	PAAS_BOOL				certValdReq				= PAAS_FALSE;
	CURL * 					loccurlHandle			= NULL;
	RESP_DATA_STYPE			stRespData;
	RESP_DATA_PTYPE			pstRespData				= NULL;
	HOSTDEF_STRUCT_TYPE		hostDef;
	struct curl_slist 		*headers = NULL;
#ifdef DEVDEBUG
	char			szDbgMsg[4096+256]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		memset(&hostDef, 0x00, sizeof(hostDef));

		rv = getSSIHostDetails(hostType, &hostDef);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the host details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if (strncmp(hostDef.url, "https", 5) == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Url: %s",__FUNCTION__, hostDef.url);
			APP_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: Url starts with https. Cert Validation required",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			certValdReq = PAAS_TRUE;
		}

		headers = curl_slist_append(headers, "User-Agent:SCA 2.19.X");
		headers = curl_slist_append(headers, "Content-Type:text/xml");
        /*T_DAIVIKP1 Expect:100- continue was causing a latency in the communication between SSI and DHI.
         * This is not really necessary and hence removing this from the header.
         * http://www.w3.org/Protocols/rfc2616/rfc2616-sec8.html
         */
		if(isDHIEnabled())
		{
			debug_sprintf(szDbgMsg, "%s: DHI enabled - remove expect from header",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			headers = curl_slist_append(headers, "Expect:");
		}

#if 0
		debug_sprintf(szDbgMsg, "%s: [CURLCALL]  curl_global_init called in [%s] from Line[%d]",
													__FUNCTION__, __FUNCTION__, __LINE__);
		APP_TRACE(szDbgMsg);

		/* Initialize the CURL library */
		curl_global_init(CURL_GLOBAL_DEFAULT);
#endif

		/* Initialize the handle using the CURL library */
		loccurlHandle = curl_easy_init();
		if(loccurlHandle == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Curl initialization for handle failed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the URL */
		if(hostDef.portNum != 0)
		{
			sprintf(szTmpURL, "%s:%d", hostDef.url, hostDef.portNum);
			curl_easy_setopt(loccurlHandle, CURLOPT_URL, szTmpURL);

			debug_sprintf(szDbgMsg, "%s: Setting SSI URL = [%s]", __FUNCTION__,
																	szTmpURL);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			curl_easy_setopt(loccurlHandle, CURLOPT_URL, hostDef.url);

			debug_sprintf(szDbgMsg, "%s: Setting SSI URL = [%s]", __FUNCTION__,
																hostDef.url);
			APP_TRACE(szDbgMsg);
		}

		/* Set the connection timeout */
		curl_easy_setopt(loccurlHandle, CURLOPT_CONNECTTIMEOUT, hostDef.conTimeOut);

		/* Set the total timeout */
		if(hostDef.respTimeOut > 0)
		{
			curl_easy_setopt(loccurlHandle, CURLOPT_TIMEOUT,
							hostDef.conTimeOut + hostDef.respTimeOut);
		}

		/* Initialize the response data */
		rv = initializeRespData(&stRespData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Initialization of response data FAILED"
																, __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Get the CURL library version and print it in the logs */
		pszCURLVer = curl_version();

		/* TODO: Add a syslog statement */
		debug_sprintf(szDbgMsg, "%s: cURL Version: %s", __FUNCTION__, pszCURLVer);
		APP_TRACE(szDbgMsg);

		curl_easy_setopt(loccurlHandle, CURLOPT_NOSIGNAL, 1L);

		/* Set the HTTP post option */
		curl_easy_setopt(loccurlHandle, CURLOPT_POST, 1L);

		/* Add the headers */
		curl_easy_setopt(loccurlHandle, CURLOPT_HTTPHEADER, headers);

	#ifdef DEBUG
		/* Add the debug function */
		curl_easy_setopt(loccurlHandle, CURLOPT_DEBUGFUNCTION, curlDbgFunc);
	#endif

		if (certValdReq == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Url starts with https. Doing certificate validation",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			curl_easy_setopt(loccurlHandle, CURLOPT_SSL_VERIFYPEER, 1L);
			curl_easy_setopt(loccurlHandle, CURLOPT_SSL_VERIFYHOST, 2L);
		}

		/* Set the CA certificate */
		curl_easy_setopt(loccurlHandle, CURLOPT_CAINFO, CA_CERT_FILE);

		/* Set the write function */
		curl_easy_setopt(loccurlHandle, CURLOPT_WRITEFUNCTION, saveResponse);

		/* Set the detection as TRUE for HTTP errors */
		curl_easy_setopt(loccurlHandle, CURLOPT_FAILONERROR, PAAS_TRUE);

		/* To request using TLS for the transfer */
		curl_easy_setopt(loccurlHandle, CURLOPT_USE_SSL, CURLUSESSL_NONE);

		/* To set preferred TLS/SSL version */
		//curl_easy_setopt(loccurlHandle, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
		//Test ends

		pstRespData = &stRespData;
		/* Set some curl library options before sending the data */
		curl_easy_setopt(loccurlHandle, CURLOPT_WRITEDATA, (void *) pstRespData); // CID 67188 (#1 of 1): Incorrect sizeof expression (BAD_SIZEOF) T_RaghavendranR1 Need to discuss, Keep it in Pending.
		curl_easy_setopt(loccurlHandle, CURLOPT_POSTFIELDS, (void *) req);
		curl_easy_setopt(loccurlHandle, CURLOPT_POSTFIELDSIZE, reqSize);
		curl_easy_setopt(loccurlHandle, CURLOPT_ERRORBUFFER, szCurlErrBuf);

		debug_sprintf(szDbgMsg, "%s: Posting the data to the server", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* KranthiK1: 30-May-2016
		 * Making it as a two step process to avoid CURL RESP TO error
		 * */
		/* Daivik:10/6/2016 - Currently we do not support the connect_only based host connection check in DHI
		 * avoid sending this host connectivity message
		 */
		if(isDHIEnabled())
		{
			/* post the data to the server */
			rv = curl_easy_perform(loccurlHandle);
		}
		else
		{
			if(checkHostConn(hostType) == SUCCESS)
			{
				/* post the data to the server */
				rv = curl_easy_perform(loccurlHandle);
			}
			else
			{
				rv = CURLE_COULDNT_CONNECT;
			}
		}
		debug_sprintf(szDbgMsg, "%s: curl_easy_perform done, rv = %d", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);

		if(rv == CURLE_OK)
		{
			*resp = stRespData.szMsg;
			*respSize = stRespData.iWritten;

			debug_sprintf(szDbgMsg, "%s: Response Len = [%d]", __FUNCTION__,
														stRespData.iWritten);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			iRetVal = res_init();

			debug_sprintf(szDbgMsg, "%s: res_init returned [%d]", __FUNCTION__, iRetVal);
			APP_TRACE(szDbgMsg);

			switch(rv)
			{
			case CURLE_UNSUPPORTED_PROTOCOL: /* 1 */
				/*
				 * The URL you passed to libcurl used a protocol that this libcurl does not support.
				 * The support might be a compile-time option that you didn't use,
				 * it can be a misspelled protocol string or just a protocol libcurl has no code for.
				 */
				debug_sprintf(szDbgMsg, "%s: protocol not supported by library",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_UNSUPPORTED_PROTOCOL].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = FAILURE;
				break;

			case CURLE_URL_MALFORMAT: /* 3 */
				/*
				 * The URL was not properly formatted.
				 */
				debug_sprintf(szDbgMsg, "%s: URL not properly formatted",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_URL_MALFORMAT].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CFG_PARAMS;
				break;

			case CURLE_COULDNT_RESOLVE_HOST: /* 6 */
				/*
				 * Couldn't resolve host. The given remote host was not resolved.
				 */
				debug_sprintf(szDbgMsg, "%s: Couldnt resolve server's address",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_COULDNT_RESOLVE_HOST].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_COULDNT_CONNECT: /* 7 */
				/*
				 * Failed to connect() to host or proxy.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to connect server",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_COULDNT_CONNECT].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_HTTP_RETURNED_ERROR: /* 22 */
				/*
				 * This is returned if CURLOPT_FAILONERROR is set TRUE
				 * and the HTTP server returns an error code that is >= 400.
				 */
				debug_sprintf(szDbgMsg, "%s: Got HTTP error while posting",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_HTTP_RETURNED_ERROR].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_WRITE_ERROR: /* 23 */
				/*
				 * An error occurred when writing received data to a local file,
				 * or an error was returned to libcurl from a write callback.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to save the response data",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data and Save Response From SSI Host, Error[CURLE_WRITE_ERROR].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = FAILURE;
				break;

			case CURLE_OUT_OF_MEMORY: /* 27 */
				/*
				 * A memory allocation request failed.
				 * This is serious badness and things are severely screwed up if this ever occurs.
				 */
				debug_sprintf(szDbgMsg, "%s: Facing memory shortage ",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_OUT_OF_MEMORY].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = FAILURE;
				break;

			case CURLE_OPERATION_TIMEDOUT: /* 28 */
				/*
				 * Operation timeout.
				 * The specified time-out period was reached according to the conditions.
				 */
				debug_sprintf(szDbgMsg, "%s: Timeout happened while receiving",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_OPERATION_TIMEDOUT].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_RESP_TO;
				break;

			case CURLE_SSL_CONNECT_ERROR: /* 35 */
				/*
				 * A problem occurred somewhere in the SSL/TLS handshake.
				 *
				 */
				debug_sprintf(szDbgMsg, "%s: SSL/TLS Handshake FAILED",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_SSL_CONNECT_ERROR].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_PEER_FAILED_VERIFICATION: /* 51 */
				/*
				 * The remote server's SSL certificate or SSH md5 fingerprint was deemed not OK.
				 */
				debug_sprintf(szDbgMsg,
							"%s: Server's SSL certificate verification FAILED",
							__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_PEER_FAILED_VERIFICATION].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_SEND_ERROR: /* 55 */
				/*
				 * Failed sending network data.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to post the request data",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_SEND_ERROR].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_RECV_ERROR: /* 56 */
				/*
				 * Failure with receiving network data.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to receive the response",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Receive Response From SSI Host, Error[CURLE_RECV_ERROR].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_SSL_CACERT: /* 60 */
				/*
				 * Peer certificate cannot be authenticated with known CA certificates.
				 */
				debug_sprintf(szDbgMsg,
								"%s: Couldnt authenticate peer certificate",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Could Not Authenticate Peer Certificate, Error[CURLE_SSL_CACERT].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_SSL_CACERT_BADFILE: /* 77 */
				/*
				 * Problem with reading the SSL CA cert (path? access rights?)
				 */
				debug_sprintf(szDbgMsg, "%s: Cant find SSL CA certificate [%s]",
												__FUNCTION__, CA_CERT_FILE);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Cannot Find SSL CA Certificate, Error[CURLE_SSL_CACERT_BADFILE].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CFG_PARAMS;
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Error [%d] occured while posting",
															__FUNCTION__, rv);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, [CURLE Error].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;
			}

			/* always cleanup */
			/* Doing cleanup below - outside the else condition ( Irrespective of status of posting packet ) */
			//curl_easy_cleanup(loccurlHandle);
			//curl_global_cleanup();

			/* Deallocate the allocated memory */
			if(stRespData.szMsg != NULL)
			{
				free(stRespData.szMsg);
				stRespData.szMsg = NULL;
			}
#ifdef DEVDEBUG
			if(strlen(szDbgMsg) < 5000)
			{
				debug_sprintf(szDbgMsg, "%s: Lib Curl Error [%s]",__FUNCTION__, szCurlErrBuf);
				APP_TRACE(szDbgMsg);
			}
#endif

		}
		/* 3-11-2015: DAIVIK : Perform a cleanup whenever the curl_easy_init is called. CURL does not de-allocate the resources.
		 * It expects curl_easy_cleanup to be called for cleaning up the resources.Global cleanup needs to be done only when program
		 * is terminating. So not doing the global_cleanup.
		 * curl_easy_cleanup is a must. curl_slist_free_all , is required because we have created slist called headers
		 */
		curl_easy_cleanup(loccurlHandle);
		curl_slist_free_all(headers);
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: postDataToBAPIHost
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int postDataToBAPIHost(char * req, int reqSize, char ** resp, int * respSize)
{
	int			rv				= SUCCESS;
	int			iCnt			= 0;
	static int	host			= PRIMARY_URL;
	CURL *		curlHandle		= NULL;
#ifdef DEVDEBUG
	char			szDbgMsg[4608]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
	if(strlen(req) < 4500)
	{
		debug_sprintf(szDbgMsg, "%s:%s: BAPI HOST REQ = [%s]",DEVDEBUGMSG,__FUNCTION__, req);
		APP_TRACE(szDbgMsg);
	}
#endif

	while(iCnt < 2)
	{
		rv = initBAPICURLHandle(&curlHandle, host);

		if(rv == SUCCESS)
		{
			/*
			 * CURL Handler was initialized successfully
			 */
			rv = sendDataToHost(curlHandle, req, reqSize, resp, respSize,AVOID_HOST_CONN_CHECK);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to post data to server",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		curl_easy_cleanup(curlHandle);

		if(rv == SUCCESS)
		{
			break;
		}

		/* If the first host is not working then try the next host */
		host ^= 1;
		iCnt++;

		debug_sprintf(szDbgMsg, "%s: Trying again..", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: initSSICURLHandle
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int initSSICURLHandle(CURL ** handle, HOST_URL_ENUM hostType)
{
	int					rv				= SUCCESS;
	char				szTmpURL[100]	= "";
	CURL *				locHandle		= NULL;
	PAAS_BOOL			certValdReq 	= PAAS_FALSE;
	HOSTDEF_STRUCT_TYPE	hostDef;
#ifdef DEBUG
	char				szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(&hostDef, 0x00, sizeof(hostDef));

		rv = getSSIHostDetails(hostType, &hostDef);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the host details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if (strncmp(hostDef.url, "https", 5) == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Url: %s",__FUNCTION__, hostDef.url);
			APP_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: Url starts with https. Cert Validation required",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			certValdReq = PAAS_TRUE;
		}
#if 0
		debug_sprintf(szDbgMsg, "%s: [CURLCALL]  curl_global_init called in [%s] from Line[%d]",
																				__FUNCTION__, __FUNCTION__, __LINE__);
		APP_TRACE(szDbgMsg);

		/* Initialize the CURL library */
		curl_global_init(CURL_GLOBAL_DEFAULT);
#endif
		/* Initialize the handle using the CURL library */
		locHandle = curl_easy_init();
		if(locHandle == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Curl initialization for handle failed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the URL */
		if(hostDef.portNum != 0)
		{
			sprintf(szTmpURL, "%s:%d", hostDef.url, hostDef.portNum);
			curl_easy_setopt(locHandle, CURLOPT_URL, szTmpURL);

			debug_sprintf(szDbgMsg, "%s: Setting PWC URL = [%s]", __FUNCTION__,
																	szTmpURL);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			curl_easy_setopt(locHandle, CURLOPT_URL, hostDef.url);

			debug_sprintf(szDbgMsg, "%s: Setting PWC URL = [%s]", __FUNCTION__,
																hostDef.url);
			APP_TRACE(szDbgMsg);
		}

		/* Set the connection timeout */
		curl_easy_setopt(locHandle, CURLOPT_CONNECTTIMEOUT, hostDef.conTimeOut);

		/* Set the total timeout */
		if(hostDef.respTimeOut > 0)
		{
			curl_easy_setopt(locHandle, CURLOPT_TIMEOUT,
										hostDef.respTimeOut);
		}

		/* Set the other common features */
		commonInit(locHandle, certValdReq);

		debug_sprintf(szDbgMsg, "%s: locHandle [%p]", __FUNCTION__, locHandle);
		APP_TRACE(szDbgMsg);

		*handle = locHandle;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: initBAPICURLHandle
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int initBAPICURLHandle(CURL ** handle, HOST_URL_ENUM hostType)
{
	int					rv				= SUCCESS;
	char				szTmpURL[100]	= "";
	CURL *				locHandle		= NULL;
	PAAS_BOOL			certValdReq		= PAAS_FALSE;
	HOSTDEF_STRUCT_TYPE	hostDef;
#ifdef DEBUG
	char				szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(&hostDef, 0x00, sizeof(hostDef));

		rv = getBAPIHostDetails(hostType, &hostDef);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the host details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		if (strncmp(hostDef.url, "https", 5) == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Url starts with https. Cert Validation required",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			certValdReq = PAAS_TRUE;
		}

		/* Initialize the handle using the CURL library */
		locHandle = curl_easy_init();
		if(locHandle == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Curl initialization for handle failed",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the URL */
		if(hostDef.portNum != 0)
		{
			sprintf(szTmpURL, "%s:%d", hostDef.url, hostDef.portNum);
			curl_easy_setopt(locHandle, CURLOPT_URL, szTmpURL);

			debug_sprintf(szDbgMsg, "%s: Setting PWC URL = [%s]", __FUNCTION__,
																	szTmpURL);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			curl_easy_setopt(locHandle, CURLOPT_URL, hostDef.url);

			debug_sprintf(szDbgMsg, "%s: Setting PWC URL = [%s]", __FUNCTION__,
																hostDef.url);
			APP_TRACE(szDbgMsg);
		}

		/* Set the connection timeout */
		curl_easy_setopt(locHandle, CURLOPT_CONNECTTIMEOUT, hostDef.conTimeOut);

		/* Set the total timeout */
		if(hostDef.respTimeOut > 0)
		{
			curl_easy_setopt(locHandle, CURLOPT_TIMEOUT,
							hostDef.conTimeOut + hostDef.respTimeOut);
		}

		/* Set the other common features */
		commonInit(locHandle, certValdReq);
		*handle = locHandle;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveResponse
 *
 * Description	: This function performs the job of saving the XML response
 * 					coming from the server into a buffer provided as parameter.
 * 					This function is passed to the CURL library, and the
 * 					library would call this function, not us.
 *
 * Input Params	:
 * 					void * ptr -> pointer to the data that needs to be copied
 * 					size_t size-> size of one element of data
 * 					size_t cnt -> count of data members
 * 					void * des -> our buffer where the response is saved
 *
 * Output Params: totLen -> length of data saved
 * ============================================================================
 */
static size_t saveResponse(void * ptr, size_t size, size_t cnt, void * des)
{
	int				rv				= SUCCESS;
	int				iSizeLeft		= 0;
	int				totLen			= 0;
	int				iReqdLen		= 0;
	char *			cTmpPtr			= NULL;
	RESP_DATA_PTYPE	respPtr			= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{

		respPtr = (RESP_DATA_PTYPE) des;
		totLen = size * cnt;
		iSizeLeft = respPtr->iTotSize - respPtr->iWritten;

		if(totLen > iSizeLeft)
		{
			iReqdLen = (respPtr->iTotSize + totLen - iSizeLeft) + 1/* one extra buffer for NULL at the end for safety purpose*/;

			cTmpPtr = (char *) realloc(respPtr->szMsg, iReqdLen);
			if(cTmpPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Reallocation of memory FAILED",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(cTmpPtr + respPtr->iWritten, 0x00, iReqdLen - respPtr->iWritten);

			respPtr->iTotSize = iReqdLen;
			respPtr->szMsg = cTmpPtr;
		}

		memcpy(respPtr->szMsg + respPtr->iWritten, ptr, totLen);
		respPtr->iWritten += totLen;

		rv = totLen;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: initializeRespData
 *
 * Description	: This function is used for the initial setting of the response
 * 					data structure which would then be used for storing XML
 * 					response retrieved from the server being contacted.
 *
 * Input Params	: RESP_DATA_PTYPE stRespPtr -> pointer to response structure
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int initializeRespData(RESP_DATA_PTYPE stRespPtr)
{
	int		rv				= SUCCESS;
	char *	cTmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	cTmpPtr = (char *) malloc(4096 * sizeof(char));
	if(cTmpPtr != NULL)
	{
		/* Initialize the memory */
		memset(cTmpPtr, 0x00, 4096 * sizeof(char));
		memset(stRespPtr, 0x00, sizeof(RESP_DATA_STYPE));

		/* Assign the data */
		stRespPtr->iTotSize = 4096;
		stRespPtr->iWritten = 0;
		stRespPtr->szMsg	= cTmpPtr;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: commonInit
 *
 * Description	: This function contains the code for common initialization of
 * 					all the three handles needed for communication with the PWC
 *
 * Input Params	: CURL * handle	-> curl handle that needs to be configured
 *
 * Output Params: void
 * ============================================================================
 */
static void commonInit(CURL * handle, PAAS_BOOL certValdReq)
{
	static struct curl_slist 		*headers = NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* DAIVIK:16-11-2015 curl_slist_append we will do only for the first time because this remains same throught.
	 * If we are not doing this then, we have an option of calling curl_slist_free_all, in order to free the memory.
	 * Since this function is called from multiple points and we need to probably keep the "headers" untill we post data to the
	 * host, we are making this static and will reuse the same each time we need to add the headers.
	 */
	if(headers == NULL)
	{
		headers = curl_slist_append(headers, "User-Agent:SCA v2.19.X");

		debug_sprintf(szDbgMsg, "%s: [CURLCALL]  curl_slist_append called in [%s] from Line[%d]",__FUNCTION__, __FUNCTION__, __LINE__);
		APP_TRACE(szDbgMsg);
		headers = curl_slist_append(headers, "Content-Type:text/xml");
		/* T_DAIVIKP1 Expect:100- continue was causing a latency in the communication between SSI and DHI.
		 * This is not really necessary and hence removing this from the header.
		 * http://www.w3.org/Protocols/rfc2616/rfc2616-sec8.html
		 */
		if(isDHIEnabled())
		{
			debug_sprintf(szDbgMsg, "%s: DHI enabled - remove expect",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			headers = curl_slist_append(headers, "Expect:");
		}
	}

	/* Set the NO SIGNAL option, This option is very important to set in case
	 * of multi-threaded applications like ours, because otherwise the libcurl
	 * uses signal handling for the communication and that causes the threads
	 * to go crazy and even crash. Fix done for issue 1659 (Church of LDS AmDocs
	 * Case# 130826-3983) */
	curl_easy_setopt(handle, CURLOPT_NOSIGNAL, 1L);

	/* Set the HTTP post option */
	curl_easy_setopt(handle, CURLOPT_POST, 1L);

	/* Add the headers */
	curl_easy_setopt(handle, CURLOPT_HTTPHEADER, headers);

#ifdef DEBUG
	/* Add the debug function */
	curl_easy_setopt(handle, CURLOPT_DEBUGFUNCTION, curlDbgFunc);
#endif

	if (certValdReq == PAAS_TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: Url starts with https. Doing certificate validation",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);
		curl_easy_setopt(handle, CURLOPT_SSL_VERIFYPEER, 1L);
		curl_easy_setopt(handle, CURLOPT_SSL_VERIFYHOST, 2L);
	}

	/* Set the CA certificate */
	curl_easy_setopt(handle, CURLOPT_CAINFO, CA_CERT_FILE);

	/* Set the write function */
	curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, saveResponse);

	/* Set the detection as TRUE for HTTP errors */
	curl_easy_setopt(handle, CURLOPT_FAILONERROR, PAAS_TRUE);

	/* To request using TLS for the transfer */
	curl_easy_setopt(handle, CURLOPT_USE_SSL, CURLUSESSL_NONE);

	/* To set preferred TLS/SSL version */
	//curl_easy_setopt(handle, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);

	debug_sprintf(szDbgMsg, "%s: --- returning ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	return;
}

/*
 * ============================================================================
 * Function Name: chkConn
 *
 * Description	:
 *
 * Input Params	:
 *
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int chkConn(CURL * curlHandle)
{
	int				rv				= SUCCESS;
	int				iRetVal			= SUCCESS;
	long			lCode			= 0L;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(curlHandle == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	while(1)
	{
#if 0
		if(isDHIEnabled() == PAAS_FALSE)
		{
			curl_easy_setopt(curlHandle, CURLOPT_CONNECT_ONLY, 1L);

			rv = curl_easy_perform(curlHandle);
			if(rv == CURLE_OK)
			{
				debug_sprintf(szDbgMsg, "%s: Perform SUCCESS [%d]", __FUNCTION__, rv);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Return Value from PERFORM = [%d]",
						__FUNCTION__, rv);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;


				iRetVal = res_init();

				debug_sprintf(szDbgMsg, "%s: res_init returned [%d]", __FUNCTION__, iRetVal);
				APP_TRACE(szDbgMsg);

				debug_sprintf(szDbgMsg, "%s: Didnt get the good resp code from host", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			/* Unsetting the connect only option */
			curl_easy_setopt(curlHandle, CURLOPT_CONNECT_ONLY, 0L);
		}
		else
		{

			curl_easy_setopt(curlHandle, CURLOPT_POST, 0L);
			curl_easy_setopt(curlHandle, CURLOPT_NOBODY, 1);
			curl_easy_setopt(curlHandle, CURLOPT_FOLLOWLOCATION, 1);
			rv = curl_easy_perform(curlHandle);
			if(rv == CURLE_OK)
			{
				debug_sprintf(szDbgMsg, "%s: Perform SUCCESS", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = SUCCESS;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Return Value from PERFORM = [%d]", __FUNCTION__, rv);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
			}

			rv = curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &lCode);

			debug_sprintf(szDbgMsg, "%s: Return Value RESPONSE CODE = [%d], [%ld]",
					__FUNCTION__, rv, lCode);
			APP_TRACE(szDbgMsg);

			if(rv == 0 && lCode == 200)
			{
				debug_sprintf(szDbgMsg, "%s: Got Successful respcode from host", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				iRetVal = res_init();

				debug_sprintf(szDbgMsg, "%s: res_init returned [%d]", __FUNCTION__, iRetVal);
				APP_TRACE(szDbgMsg);

				debug_sprintf(szDbgMsg, "%s: Didnt get the good resp code from host", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}
		}
		break;
#endif


		if(isDHIEnabled())
		{
			curl_easy_setopt(curlHandle, CURLOPT_POST, 0L);
			curl_easy_setopt(curlHandle, CURLOPT_NOBODY, 1);
			curl_easy_setopt(curlHandle, CURLOPT_FOLLOWLOCATION, 1);
		}
		else
		{
			curl_easy_setopt(curlHandle, CURLOPT_CONNECT_ONLY, 1L);
		}

		rv = curl_easy_perform(curlHandle);
		if(rv == CURLE_OK)
		{
			debug_sprintf(szDbgMsg, "%s: Perform SUCCESS", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Return Value from PERFORM = [%d]",
					__FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}
		if(!isDHIEnabled())
		{
			if(rv == FAILURE)
			{
				iRetVal = res_init();

				debug_sprintf(szDbgMsg, "%s: res_init returned [%d]", __FUNCTION__, iRetVal);
				APP_TRACE(szDbgMsg);

				debug_sprintf(szDbgMsg, "%s: Didnt get the good resp code from host", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}
		rv = curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &lCode);

		debug_sprintf(szDbgMsg, "%s: Return Value RESPONSE CODE = [%d], [%ld]",
				__FUNCTION__, rv, lCode);
		APP_TRACE(szDbgMsg);

		if(rv == 0 && lCode == 200)
		{
			debug_sprintf(szDbgMsg, "%s: Got Successful respcode from host",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = SUCCESS;
		}
		else
		{
			iRetVal = res_init();

			debug_sprintf(szDbgMsg, "%s: res_init returned [%d]", __FUNCTION__, iRetVal);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Didnt get the good resp code from host"
					, __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}

		break;
	}
#if 0
	if(curlHandle != NULL)
	{
		curl_easy_cleanup(curlHandle);
	}
#endif

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sendDataToHost
 *
 * Description	: This function sends the XML data to the PWC server, using the
 * 					appropriate CURL handle.
 *
 * Input Params	:
 * 				PWC_URL_TYPE_ENUM urlType -> constant telling which handle to
 * 												use
 * 				unsigned char *	  data	  -> the XML data that needs to be sent
 * 				int				  size	  -> size of the data
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int sendDataToHost(CURL * curlHandle, char * req, int reqSize,
												char ** resp, int * respSize, int hostType)
{
	int				rv				   		= SUCCESS;
	int				iRetVal					= SUCCESS;
	int				iAppLogEnabled			= isAppLogEnabled();
	char			szCurlErrBuf[4096] 		= "";
	char			szAppLogData[300]		= "";
	char			szAppLogDiag[300]		= "";
	RESP_DATA_STYPE	stRespData;
	RESP_DATA_PTYPE	pstRespData				= NULL;

#ifdef DEVDEBUG
	char			szDbgMsg[4096+256]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Initialize the response data */
		rv = initializeRespData(&stRespData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Initialization of response data FAILED"
																, __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		pstRespData = &stRespData;

		/* Set some curl library options before sending the data */
		curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, (void *) pstRespData); // CID 67347 (#1 of 1): Incorrect sizeof expression (BAD_SIZEOF) T_RaghavendranR1 Need to discuss, Keep it in Pending.
		curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, (void *) req);
		curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDSIZE, reqSize);
		curl_easy_setopt(curlHandle, CURLOPT_ERRORBUFFER, szCurlErrBuf);

		debug_sprintf(szDbgMsg, "%s: Posting the data to the server", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* KranthiK1: 30-May-2016
		 * Making it as a two step process to avoid CURL RESP TO error
		 * */
		if(isDHIEnabled())
		{
			/* post the data to the server */
			rv = curl_easy_perform(curlHandle);
		}
		else
		{
			if(hostType == AVOID_HOST_CONN_CHECK)
			{
				/* post the data to the server */
				rv = curl_easy_perform(curlHandle);
			}
			else if(checkHostConn(hostType) == SUCCESS)
			{
				/* post the data to the server */
				rv = curl_easy_perform(curlHandle);
			}
			else
			{
				rv = CURLE_COULDNT_CONNECT;
			}

		}
		debug_sprintf(szDbgMsg, "%s: curl_easy_perform done, rv = %d", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);

		if(rv == CURLE_OK)
		{
			*resp = stRespData.szMsg;
			*respSize = stRespData.iWritten;

			debug_sprintf(szDbgMsg, "%s: Response Len = [%d]", __FUNCTION__,
														stRespData.iWritten);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			iRetVal = res_init();

			debug_sprintf(szDbgMsg, "%s: res_init returned [%d]", __FUNCTION__, iRetVal);
			APP_TRACE(szDbgMsg);

			switch(rv)
			{
			case CURLE_UNSUPPORTED_PROTOCOL: /* 1 */
				/*
				 * The URL you passed to libcurl used a protocol that this libcurl does not support.
				 * The support might be a compile-time option that you didn't use,
				 * it can be a misspelled protocol string or just a protocol libcurl has no code for.
				 */
				debug_sprintf(szDbgMsg, "%s: protocol not supported by library",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_UNSUPPORTED_PROTOCOL].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = FAILURE;
				break;

			case CURLE_URL_MALFORMAT: /* 3 */
				/*
				 * The URL was not properly formatted.
				 */
				debug_sprintf(szDbgMsg, "%s: URL not properly formatted",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_URL_MALFORMAT].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CFG_PARAMS;
				break;

			case CURLE_COULDNT_RESOLVE_HOST: /* 6 */
				/*
				 * Couldn't resolve host. The given remote host was not resolved.
				 */
				debug_sprintf(szDbgMsg, "%s: Couldnt resolve server's address",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_COULDNT_RESOLVE_HOST].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_COULDNT_CONNECT: /* 7 */
				/*
				 * Failed to connect() to host or proxy.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to connect server",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_COULDNT_CONNECT].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_HTTP_RETURNED_ERROR: /* 22 */
				/*
				 * This is returned if CURLOPT_FAILONERROR is set TRUE
				 * and the HTTP server returns an error code that is >= 400.
				 */
				debug_sprintf(szDbgMsg, "%s: Got HTTP error while posting",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_HTTP_RETURNED_ERROR].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_WRITE_ERROR: /* 23 */
				/*
				 * An error occurred when writing received data to a local file,
				 * or an error was returned to libcurl from a write callback.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to save the response data",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_WRITE_ERROR].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = FAILURE;
				break;

			case CURLE_OUT_OF_MEMORY: /* 27 */
				/*
				 * A memory allocation request failed.
				 * This is serious badness and things are severely screwed up if this ever occurs.
				 */
				debug_sprintf(szDbgMsg, "%s: Facing memory shortage ",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_OUT_OF_MEMORY].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = FAILURE;
				break;

			case CURLE_OPERATION_TIMEDOUT: /* 28 */
				/*
				 * Operation timeout.
				 * The specified time-out period was reached according to the conditions.
				 */
				debug_sprintf(szDbgMsg, "%s: Timeout happened while receiving",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_OPERATION_TIMEDOUT].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_RESP_TO;
				break;

			case CURLE_SSL_CONNECT_ERROR: /* 35 */
				/*
				 * A problem occurred somewhere in the SSL/TLS handshake.
				 *
				 */
				debug_sprintf(szDbgMsg, "%s: SSL/TLS Handshake FAILED",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_SSL_CONNECT_ERROR].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_PEER_FAILED_VERIFICATION: /* 51 */
				/*
				 * The remote server's SSL certificate or SSH md5 fingerprint was deemed not OK.
				 */
				debug_sprintf(szDbgMsg,
							"%s: Server's SSL certificate verification FAILED",
							__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_PEER_FAILED_VERIFICATION].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_SEND_ERROR: /* 55 */
				/*
				 * Failed sending network data.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to post the request data",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_SEND_ERROR].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_RECV_ERROR: /* 56 */
				/*
				 * Failure with receiving network data.
				 */
				debug_sprintf(szDbgMsg, "%s: Failed to receive the response",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_RECV_ERROR].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_SSL_CACERT: /* 60 */
				/*
				 * Peer certificate cannot be authenticated with known CA certificates.
				 */
				debug_sprintf(szDbgMsg,
								"%s: Couldnt authenticate peer certificate",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_SSL_CACERT].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CONN_FAIL;
				break;

			case CURLE_SSL_CACERT_BADFILE: /* 77 */
				/*
				 * Problem with reading the SSL CA cert (path? access rights?)
				 */
				debug_sprintf(szDbgMsg, "%s: Cant find SSL CA certificate [%s]",
												__FUNCTION__, CA_CERT_FILE);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Failed To Post Data to SSI Host, Error[CURLE_SSL_CACERT_BADFILE].");
					strcpy(szAppLogDiag, "Please Check The SSI Host URLs, Otherwise Contact Verifone");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSING, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CFG_PARAMS;
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Error [%d] occured while posting",
															__FUNCTION__, rv);
				APP_TRACE(szDbgMsg);

				rv = ERR_CONN_FAIL;
				break;
			}

			/* Deallocate the allocated memory */
			if(stRespData.szMsg != NULL)
			{
				free(stRespData.szMsg);
				stRespData.szMsg = NULL;
			}
#ifdef DEVDEBUG
			if(strlen(szDbgMsg) < 5000)
			{
				debug_sprintf(szDbgMsg, "%s: Lib Curl Error [%s]",__FUNCTION__, szCurlErrBuf);
				APP_TRACE(szDbgMsg);
			}
#endif

		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

#if 0
/*
 * ============================================================================
 * Function Name: doPINGTest
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int doPINGTest(HOST_URL_ENUM iHost)
{
	int					rv				= SUCCESS;
	char				szURL[200]		= "";
	char				szIPAddr[16]	= "";
	struct addrinfo *	result			= NULL;
	struct addrinfo *	res				= NULL;
	struct sockaddr_in*	ipv4			= NULL;
	struct netPingInfo	stNetPing;
#ifdef DEBUG
	char				szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Dont do PING test if not specified in the config.usr1 file */
		if(PAAS_TRUE != isHostPingReqd())
		{
			debug_sprintf(szDbgMsg, "%s: Ping Test not required", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Get the URL */
		rv = getURLToPing(szURL, iHost);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get the URL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Get the ip address from the URL */
		rv = getaddrinfo(szURL, NULL, NULL, &result);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid URL; FAILED to derive IP - %d",
														__FUNCTION__, errno);
			APP_TRACE(szDbgMsg);

			rv = ERR_CONN_FAIL;
			break;
		}

		/* Loop over all returned results and do inverse lookup */
		for(res = result; res != NULL; res = res->ai_next)
		{
			ipv4 = (struct sockaddr_in *) res->ai_addr;

			sprintf(szIPAddr, "%s", inet_ntoa(ipv4->sin_addr));

			debug_sprintf(szDbgMsg, "%s: AF_INET (IPv4) = [%s]", __FUNCTION__,
																	szIPAddr);
			APP_TRACE(szDbgMsg);
		}

		debug_sprintf(szDbgMsg, "%s: Pinging to [%s]", __FUNCTION__, szIPAddr);
		APP_TRACE(szDbgMsg);

		memset(&stNetPing, 0x00, sizeof(stNetPing));

		stNetPing = net_ping(szIPAddr, 3);
		if(errno != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: PING failed, errno = [%d]",
															__FUNCTION__, rv);
			APP_TRACE(szDbgMsg);

			rv = ERR_CONN_FAIL;
		}

		break;
	}

	/* De-allocate any allocated memory */
	if(result != NULL)
	{
		freeaddrinfo(result);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getURLToPing
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getURLToPing(char *ptrURL, HOST_URL_ENUM host)
{
	int		rv					= SUCCESS;
	char *	cur					= NULL;
	char *	next				= NULL;
	char	szTmpURLBuf[100]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif
	HOSTDEF_STRUCT_TYPE	hostDef;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		rv = getSSIHostDetails(host, &hostDef);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the host details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Start the parsing of the URL */
		cur = hostDef.url;

		if(ptrURL == NULL && cur == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed URL not present",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		next = strchr(cur, '/');	// skip 'https:'
		cur = next + 2;				// skip "//"
		next = strchr(cur, '/');
		if(next != NULL)
		{
			memcpy(szTmpURLBuf, cur, next - cur);

			/* Copy formatted IP */
			sprintf(ptrURL, "%s", szTmpURLBuf);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid URL - [%s]", __FUNCTION__,
																hostDef.url);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: URL to PING = [%s]", __FUNCTION__, ptrURL);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: lock_callback
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static void lock_callback(int mode, int type, char *file, int line)
{
#ifdef DEBUG
	//char	szDbgMsg[512]		= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

  (void)file;
  (void)line;
  if (mode & CRYPTO_LOCK)
  {
	  //debug_sprintf(szDbgMsg, "%s: SSI Mutex Locking", __FUNCTION__);
	  //APP_TRACE(szDbgMsg);
	  pthread_mutex_lock(&(lockarray[type]));
  }
  else
  {
	  //debug_sprintf(szDbgMsg, "%s: SSI Mutex Un-Locking", __FUNCTION__);
	  //APP_TRACE(szDbgMsg);

	  pthread_mutex_unlock(&(lockarray[type]));
  }

  //debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
  //APP_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: thread_id
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static unsigned long thread_id(void)
{
  unsigned long ret;

#ifdef DEBUG
	//char	szDbgMsg[512]		= "";
#endif

  ret=(unsigned long)pthread_self();

//  debug_sprintf(szDbgMsg, "%s: Returning [%ld]", __FUNCTION__, ret);
//  APP_TRACE(szDbgMsg);

  return(ret);
}

/*
 * ============================================================================
 * Function Name: initializeOpenSSLLocks
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static int initializeOpenSSLLocks()
{
	int		rv					= SUCCESS;
	int		iIndex				= 0;

#ifdef DEBUG
	char	szDbgMsg[512]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	lockarray=(pthread_mutex_t *)OPENSSL_malloc(CRYPTO_num_locks() *
	                                            sizeof(pthread_mutex_t));

	 for (iIndex =0; iIndex < CRYPTO_num_locks(); iIndex++)
	 {
	    pthread_mutex_init(&(lockarray[iIndex]),NULL);
	 }

	 CRYPTO_set_id_callback((unsigned long (*)())thread_id);
	 CRYPTO_set_locking_callback((void (*)())lock_callback);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

#ifdef DEBUG
/*
 * ============================================================================
 * Function Name: curlDbgFunc
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: 0
 * ============================================================================
 */
static int curlDbgFunc(CURL * handle, curl_infotype infoType, char *msg,
														size_t size, void * arg)
{
	char	szDbgMsg[4096]	= "";
	switch(infoType)
	{
	case CURLINFO_TEXT:
		sprintf(szDbgMsg, "%s: info-text: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_HEADER_IN:
		sprintf(szDbgMsg, "%s: info-header in: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_HEADER_OUT:
		sprintf(szDbgMsg, "%s: info-header out: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_DATA_IN:
		sprintf(szDbgMsg, "%s: info-data in: %s", __FUNCTION__, msg);
		break;

	case CURLINFO_DATA_OUT:
		sprintf(szDbgMsg, "%s: info-data out: %s", __FUNCTION__, msg);
		break;

	default:
		sprintf(szDbgMsg, "%s: info-default: %s", __FUNCTION__, msg);
		break;
	}

	APP_TRACE(szDbgMsg);

	return 0;
}
#endif

/*
 * ============================================================================
 * Function Name: checkAndSwitchURL
 *
 * Description	: This function is used to check the current status of the counters running for 1) consecutive host response timeout recevied on any one URL
 * 					2) Number of transactions posted to host using secondary URL.
 * 					If number of consecutive host response timeout reaches the maximum limit, than it will swtich the URL to the alternative one & clean the
 * 					current CURL handler If required
 * 					If Number of transactions posted to host using secondary URL reaches the maxmimum limit, than it will swtich the URL to the primary
 * 					 & clean the current CURL handler If required
 *
 * Input Params	:	Current payment command, Host result (rv) If it is -1 it means host result is not yet available,
 * 					Force flag to forcefully switch the URL to the alternative one, Clean Curl Reqd flag to decide whether curl has to be cleaned or not
 *
 * Output Params:	SUCCESS/FAILURE
 * ============================================================================
 */
int checkAndSwitchURL(int iCmd, int hostResult, PAAS_BOOL bForceFlag, PAAS_BOOL bCleanCurlHandle)
{
	int					rv							= SUCCESS;
	int					iAppLogEnabled				= isAppLogEnabled();
static int				iNumOfCurTranOnScndURL		= 0;
static int				iNumOfCurTimedOutTran		= 0;
	char				szAppLogData[300]			= "";
	PAAS_BOOL			bURLSwitched				= PAAS_FALSE;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	/* MukeshS3: 3-Aug-16: FRD 3.70.1
	 * In this enhancement, After getting response timeouts for "switchurlonxresptimeout" transactions, the current URL has to be switched to the
	 * alternative one (If on the primary the transition is to the secondary, if on the secondary the transition is back to the primary).
	 */

	if(getNumUniqueHosts() == 1)
	{
		debug_sprintf(szDbgMsg, "%s: Both primary & secondary URL are same", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SUCCESS;
	}

	acquireMutexLock(&gptHostTypeMutex, "Host Type Mutex");

	if(bForceFlag)
	{
		host ^= 1;
		bURLSwitched = PAAS_TRUE;
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "Switching From [%s] URL to The [%s] URL",
					host&1 ? "Primary" : "Secondary", host&1 ? "Secondary" : "Primary");
			addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
		}
	}
	else if(iCmd != SSI_TOR)
	{
		if(getNumOfMaxTimeoutTranToSwitchURL())
		{
			if(hostResult == -1)	// this has to be checked before posting to the host
			{
				debug_sprintf(szDbgMsg, "%s: counter %d max = %d", __FUNCTION__, iNumOfCurTimedOutTran, getNumOfMaxTimeoutTranToSwitchURL());
				APP_TRACE(szDbgMsg);
				if(iNumOfCurTimedOutTran >= getNumOfMaxTimeoutTranToSwitchURL())
				{
					if(iAppLogEnabled == 1)
					{
						sprintf(szAppLogData, "Number of Consecutive Host Response Timeouts On [%s] URL Reached Threshold Limit[%d]. Switching to [%s] URL",
								host&1 ? "Secondary" : "Primary", iNumOfCurTimedOutTran, host&1 ? "Primary" : "Secondary");
						addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
					}
					debug_sprintf(szDbgMsg, "%s: Number of Consecutive Host Response Timeouts On [%s] URL Reached Threshold Limit[%d]. Switching to [%s] URL", __FUNCTION__,
							host&1 ? "Secondary" : "Primary", iNumOfCurTimedOutTran, host&1 ? "Primary" : "Secondary");
					APP_TRACE(szDbgMsg);
					host ^= 1;
					bURLSwitched = PAAS_TRUE;
				}
			}
			else if(hostResult == ERR_RESP_TO)	// this has to be checked after posting to the host
			{
				iNumOfCurTimedOutTran++;
				debug_sprintf(szDbgMsg, "%s: Increamenting the timeout tran counter %d", __FUNCTION__, iNumOfCurTimedOutTran);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				iNumOfCurTimedOutTran = 0;
			}
		}

		/* MukeshS3: 3-August-16: FRD 3.70.2
		 * According to this FRD, when host is connected online to the secondary URL for more than "switchonprimurlafterxtran" transactions
		 * than the URL must be switched back to the primary URL from the next transaction onwards as it is not desirable to stay on secondary URL
		 * for long time even if it is connected. After Itegrating with this enhancement, If we already tried "switchonprimurlafterxtran" transactions
		 * on seconadry URL, than we will switch the URL to the primary URL. So, from next transaction, primary URL will be used
		 */
		if(! bURLSwitched && getNumOfMaxTranOnScndURL())
		{
			if(host == SECONDARY_URL)
			{
				if(hostResult == -1)	// this has to be checked before posting to the host
				{
					if(iNumOfCurTranOnScndURL >= getNumOfMaxTranOnScndURL())
					{
						if(iAppLogEnabled == 1)
						{
							sprintf(szAppLogData, "Number of Transactions On Secondary URL Reached Threshold Limit[%d]. Switching Back to Primary URL", iNumOfCurTranOnScndURL);
							addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
						}
						debug_sprintf(szDbgMsg, "%s: Number of Transactions On Secondary URL Reached Threshold Limit[%d]. Switching Back to Primary URL", __FUNCTION__, iNumOfCurTranOnScndURL);
						APP_TRACE(szDbgMsg);
						host ^= 1;
						bURLSwitched = PAAS_TRUE;
					}
				}
				else
				{
					iNumOfCurTranOnScndURL++;
					debug_sprintf(szDbgMsg, "%s: Increamenting the secondary URL counter %d", __FUNCTION__, iNumOfCurTranOnScndURL);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				iNumOfCurTranOnScndURL = 0;
			}
		}
	}

	// reset all the global counters if URL has been switched successfully
	if(bURLSwitched)
	{
		debug_sprintf(szDbgMsg, "%s: Successfully switched from the %s URL to the %s URL", __FUNCTION__,
					host&1 ? "Primary" : "Secondary", host&1 ? "Secondary" : "Primary");
		APP_TRACE(szDbgMsg);
		iNumOfCurTranOnScndURL = 0;
		iNumOfCurTimedOutTran  = 0;
		if(bCleanCurlHandle)
		{
			cleanCURLHandle(PAAS_TRUE); //Closing the current curl handler before trying the next host url
		}
	}

	releaseMutexLock(&gptHostTypeMutex, "Host Type Mutex");

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * End of file ssiComm.c
 * ============================================================================
 */
