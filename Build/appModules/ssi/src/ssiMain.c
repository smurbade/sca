
#include <stdio.h>
#include <string.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <syslog.h>

#include "common/utils.h"
#include "common/xmlUtils.h"
//#include "sessDataStore.h"
#include "common/tranDef.h"
#include "db/dataSystem.h"
#include "db/tranDS.h"
#include "ssi/ssiDef.h"
#include "appLog/appLogAPIs.h"

#define SSIREQ_STR	"TRANSACTION"
#define UGPREQ_STR	"UGPREQUEST"
#define SSIRES_STR	"RESPONSE"
#define UGPRES_STR	"UGPRESPONSE"
#define SSIADM_STR	"MSH"
#define BAPIREQ_STR	"REQUEST"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

typedef METADATA_STYPE	META_STYPE;
typedef METADATA_PTYPE	META_PTYPE;

/* Extern functions */
extern int 	getMetaDataForSSIReq(META_PTYPE, TRAN_PTYPE);
extern int 	getMetaDataForBAPIReq(META_PTYPE , char* , int );
extern int  getSAFTranIndicatorForPymtTran(char *, PAAS_BOOL *);
extern int 	procParsedResp(TRAN_PTYPE, META_PTYPE, char *);
extern int 	setAdminPacketRequestStatus(PAAS_BOOL);
extern int 	getMetaDataForSSIResp(META_PTYPE, int, int);
extern int  getMetaDataForBAPIResp(META_PTYPE pstMeta);
extern int 	initSsiIFace();
extern int 	closeSsiIFace();
extern int 	postDataToSSIHost(char * , int , char ** , int * );
extern int 	postSAFDataToSSIHost(char * , int , char ** , int * );
extern int 	postDataToPAYwareTranHost(char * , int , char ** , int * );
extern int	postDataToBAPIHost(char * , int , char ** , int * );
extern int 	postDataToProxyUrl(char *, int , char **, int *, CREDITAPPDTLS_PTYPE);
extern int	checkHostConn();
extern int 	loadSSIConfigParams();
extern int setDevAdminRqd(PAAS_BOOL);
extern PAAS_BOOL isSAFEnabled();
extern PAAS_BOOL getHostConnectionStatus();
extern PAAS_BOOL isEmvHostEnabled(); //TODO: Added for EMV testing. Remove it later.
extern PAAS_BOOL isCheckHostConnStatusEnable();
extern int getHostProtocolFormat();
extern void updateHostConnectionStatus(PAAS_BOOL );
extern int 	sendHostDownSignal();
extern PAAS_BOOL isDHIEnabled();
extern int getNumOfTimedOutTranToSAF();
extern int checkAndSwitchURL(int iCmd, int hostResult, PAAS_BOOL bForceFlag, PAAS_BOOL bCleanCurlHandle);

/* Static functions declarations */
static int	buildSSIReq(TRAN_PTYPE, char **, int *);
static int	processSSIResp(TRAN_PTYPE, char *);
static int	parseSSIResp(META_PTYPE, char *, int, int);
static void replaceUTF8WithUTF16(char *);
static int 	buildBAPIReq(char **, int *, char*, int);
static int 	parseBAPIResp(META_PTYPE, char *);

int giRespToTran = 0; //Global Counter used to maintain the number of transactions that have timedout
int giNumOfTransToStartSAF = 0; //Global Value holding the number of timed out transactions after which we need to Start SAF

/*
 * ============================================================================
 * Function Name: initSSIModule
 *
 * Description	: This function is responsible for initializing the SSI module 
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int initSSIModule()
{
	int		rv				= SUCCESS;
	int	    iAppLogEnabled	= isAppLogEnabled();
	char	szErrMsg[256]	= "";
	char	szAppLogData[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	while(1)
	{
		/* Load the configuration parameters into the application memory */
		rv	= loadSSIConfigParams();
		if(rv != SUCCESS)
		{
			/* Replacing debug_sprintf with sprintf so that syslogs get printed
			 * even with the execution of release version of the application. */
			sprintf(szErrMsg,"%s: FAILED to load SSILOG_ERR config params",__FUNCTION__);
			APP_TRACE(szErrMsg);
			syslog(LOG_ERR|LOG_USER, szErrMsg);
			break;
		}

		/* Initialize the SSI interface */
		rv = initSsiIFace();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to initialize the SSI iface",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		/* Daivik:4/5/2016 - If Check Host Connection Status is not enabled, then we can consider the "SAF on Timed Out Response" feature a
		 * disabled.
		 */
		if(isCheckHostConnStatusEnable())
		{
			giNumOfTransToStartSAF = getNumOfTimedOutTranToSAF();
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s:Disable SAF On response Timeout, since Check Host connection is disabled", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Disable SAF On Response Timeout Since Host Connection Check is Disabled");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			giNumOfTransToStartSAF = 0;
		}

		break;
	}

	giRespToTran = 0;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: closeSSIModule
 *
 * Description	: This function is responsible for closing the SSI module 
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int closeSSIModule()
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		rv = closeSsiIFace();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to close the SSI iface",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doBAPICommunication
 *
 * Description	: This function is responsible for the Bapi communication with
 * 					the host. It will take care of forming of the Bapi request
 * 					packet, posting the same to the host, parsing and saving
 * 					the response received from the host.
 *
 * Input Params	:	szMsg	-> Bapi basket
 * 					size	-> Size of the basket
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doBAPICommunication(char* szMsg, int msgSize)
{
	int			rv				= SUCCESS;
	int			iReqLen			= 0;
	int			iRespLen		= 0;
	char *		pszBAPIReq		= NULL;
	char *		pszBAPIResp		= NULL;
	META_STYPE	stMetaData;

#ifdef DEVDEBUG
	char		szDbgMsg[4096] = "";
#elif DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&stMetaData, 0x00, METADATA_SIZE);
	while(1)
	{
		if(szMsg == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		rv = buildBAPIReq(&pszBAPIReq, &iReqLen, szMsg, msgSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build the BAPI request", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		/* Send the data to the host and try getting the response*/
		rv = postDataToBAPIHost(pszBAPIReq, iReqLen, &pszBAPIResp, &iRespLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to post data to the host", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}


		rv = parseBAPIResp(&stMetaData, pszBAPIResp);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to parse XML resp",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		break;
	}

	freeMetaDataEx(&stMetaData);
	/* De-allocate any allocated memory which is no longer required */
	if(pszBAPIReq != NULL)
	{
		free(pszBAPIReq);
	}

	if(pszBAPIResp != NULL)
	{
		free(pszBAPIResp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doProxyCommunication
 *
 * Description	: This function is responsible for the SSI communication with
 * 					the proxy. It will take care of forming of the SSI request
 * 					packet, posting the same to the proxy, parsing and saving
 * 					the response received from the proxy.
 *
 * Input Params	:	szTranKey	-> identifier for the transaction data
 * 					szStatMsg	-> placeholder for custom response text
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doProxyCommunication(char * szTranKey, char * szStatMsg)
{
	int			rv					= SUCCESS;
	int			iReqLen				= 0;
	int			iRespLen			= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	char *		pszSSIReq			= NULL;
	char *		pszSSIResp			= NULL;
	char		szAppLogData[300]	= "";
	TRAN_PTYPE	pstTran				= NULL;

#ifdef DEBUG
	char		szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Validate the parameters */
		if((szTranKey == NULL) || (szStatMsg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Try fetching the SSI transaction instance for which the
		 * communication needs to be performed */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get tran data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* setting the status message which will be sent as response text to POS*/
			strcpy(szStatMsg, "Device Application Internal Error");
			break;
		}

		/* Build the SSI request for the transaction */
		rv = buildSSIReq(pstTran, &pszSSIReq, &iReqLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build the SSI request",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* setting the status message which will be sent as response text to POS*/
			strcpy(szStatMsg, "Device Application Internal Error");
			break;
		}

		rv = postDataToProxyUrl(pszSSIReq, iReqLen, &pszSSIResp, &iRespLen, &((DEVTRAN_PTYPE)pstTran->data)->stCreditAppDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to post data to the host",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Post the Data to Proxy URL");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			break;
		}

		/* process the SSI response coming from the proxy */
		rv = processSSIResp(pstTran, pszSSIResp);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to process SSI response",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Process SSI Response Obtained From the Proxy");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			/* setting the status message which will be sent as response text to POS*/
			strcpy(szStatMsg, "Device Application Internal Error");

			break;
		}

		break;
	}

	/* De-allocate any allocated memory which is no longer required */
	if(pszSSIReq != NULL)
	{
		free(pszSSIReq);
	}

	if(pszSSIResp != NULL)
	{
		free(pszSSIResp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doSSICommunication
 *
 * Description	: This function is responsible for the SSI communication with
 * 					the host. It will take care of forming of the SSI request
 * 					packet, posting the same to the host, parsing and saving
 * 					the response received from the host.
 *
 * Input Params	:	szTranKey	-> identifier for the transaction data
 * 					szStatMsg	-> placeholder for custom response text
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int doSSICommunication(char * szTranKey, char * szStatMsg)
{
	int			rv					= SUCCESS;
	int 		rvTemp 				= 0;
	int			iReqLen				= 0;
	int			iRespLen			= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	char *		pszSSIReq			= NULL;
	char *		pszSSIResp			= NULL;
	char		szAppLogData[300]	= "";
	TRAN_PTYPE	pstTran				= NULL;
	PAAS_BOOL	bIsSAFTran			= PAAS_FALSE;
	PAAS_BOOL	bHostAvailable		= PAAS_TRUE;
	RESPDTLS_STYPE	stRespDtlsTmp;

#ifdef DEBUG
	char		szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(&stRespDtlsTmp, 0x00, sizeof(RESPDTLS_STYPE));

	/*AjayS2 25 Aug 2016 - Coverity - 83376 -  If any of below is NULL we should return back. This code was inside while loop with break.
	 * After while we are doing some operations on these parameters.
	 */
	/* Validate the parameters */
	if((szTranKey == NULL) || (szStatMsg == NULL) )
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	while(1)
	{
		/*
		 * Praveen_P1: In the field we see that each SAF transaction is taking long
		 * time to approve it locally. when network is down, it tries to resolve the
		 * DNS by name look up which is taking longer time. we are not able to find
		 * any setting to be done on CURL library. To solve this, we are
		 * keeping track of host connectivity, if the host connectivity is down, we
		 * will not try to post the request itself.
		 * If SAF is enabled, we keep trying the posting of SAF records on the background
		 * so if we are succesful, at that time this status will get changed
		 */
		/* Try fetching the SSI transaction instance for which the
		 * communication needs to be performed */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get tran data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* setting the status message which will be sent as response text to POS*/
			strcpy(szStatMsg, "Device Application Internal Error");
			break;
		}

		/* Daivik:4/5/2016 - If we are sending a TOR Transaction and if host has been marked as Down ( happens
		 * only with saf on response timeout feature) then we were not sending the TOR Transaction for the last transaction,
		 * hence putting this condition below , to check for transactions apart from TOR.
		 */
		if((isCheckHostConnStatusEnable() && getHostConnectionStatus() == PAAS_FALSE) && (pstTran->iCmd != SSI_TOR))
		{
			debug_sprintf(szDbgMsg, "%s: Host Connection Status is Down, not trying to post this request", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = ERR_CONN_TO;
			bHostAvailable = PAAS_FALSE;
			break;
		}

		/* Build the SSI request for the transaction */
		rv = buildSSIReq(pstTran, &pszSSIReq, &iReqLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build the SSI request",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* setting the status message which will be sent as response text to POS*/
			strcpy(szStatMsg, "Device Application Internal Error");
			break;
		}

		rv = getSAFTranIndicatorForPymtTran(szTranKey, &bIsSAFTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SAF transaction indicator", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		/* MukeshS3: 2-August-16:
		 * FRD 3.70.1
		 * In this enhancement, After getting response timeouts for "switchurlonxresptimeout" transactions, the current URL has to be switched to the
		 * alternative one (If on the primary the transition is to the secondary, if on the secondary the transition is back to the primary).
		 * FRD 3.70.2
		 * According to this FRD, when host is connected online to the secondary URL for more than "switchonprimurlafterxtran" transactions
		 * than the URL must be switched back to the primary URL from the next transaction onwards as it is not desirable to stay on secondary URL
		 * for long time even if it is connected. After Itegrating with this enhancement, If we already tried "switchonprimurlafterxtran" transactions
		 * on seconadry URL, than we will switch the URL to the primary URL. So, from next transaction, primary URL will be used
		 */
		rv = checkAndSwitchURL(pstTran->iCmd, -1, PAAS_FALSE, ! bIsSAFTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to Check and Switch URL", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		if( bIsSAFTran != PAAS_TRUE )
		{
			debug_sprintf(szDbgMsg, "%s: Posting Online transaction Data to the host", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				addAppEventLog(SCA, PAAS_INFO, SSI_REQUEST, pszSSIReq, NULL);
			}

			/* Send the data to the host and try getting the response */
			rv = postDataToSSIHost(pszSSIReq, iReqLen, &pszSSIResp, &iRespLen);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to post data to the host",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Posting Offline(SAF) transaction data to the host", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Posting Offline SAF Request to Host");
				addAppEventLog(SCA, PAAS_INFO, SENT, szAppLogData, NULL);

				addAppEventLog(SCA, PAAS_INFO, SSI_REQUEST, pszSSIReq, NULL);
			}

			/* Send the data to the host and try getting the response */
			//rv = postDataToSSIHost(pszSSIReq, iReqLen, &pszSSIResp, &iRespLen);
			rv = postSAFDataToSSIHost(pszSSIReq, iReqLen, &pszSSIResp, &iRespLen);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to post data to the host",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Post the SAF Request to Host");
					addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
				}

				/* setting the status message which will be sent as response text to POS*/
				//strcpy(szStatMsg, "Communication Error With Host"); //Check if we need to fill this here
				break;
			}
		}


		/* process the SSI response coming from the host */
		rv = processSSIResp(pstTran, pszSSIResp);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to process SSI response",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Process SSI Response Obtained From the Host");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			/* setting the status message which will be sent as response text to POS*/
			strcpy(szStatMsg, "Device Application Internal Error");

			break;
		}

		break;
	}
	rvTemp = getGenRespDtlsForSSITran(szTranKey, &stRespDtlsTmp);
	if(rvTemp != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get General Response Details",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	// MukeshS3: 2-Aug-16; FRD 3.70.1, 3.70.2
	if(bHostAvailable)
	{
		if(rv == ERR_RESP_TO || atoi(stRespDtlsTmp.szRsltCode) == ERR_HOST_TRAN_TIMEOUT)
		{
			rvTemp = checkAndSwitchURL(pstTran->iCmd, ERR_RESP_TO, PAAS_FALSE, ! bIsSAFTran);
		}
		else
		{
			rvTemp = checkAndSwitchURL(pstTran->iCmd, rv, PAAS_FALSE, ! bIsSAFTran);
		}
		if(rvTemp != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to Check and Switch URL", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	/* Daivik:5/5/16 -Only if SAF Response Timeout is enabled , we will need to Check for Timed out transactions
	 * and send the host down signal once we see that giNumOfTransToStartSAF transactions have timedout.
	 */
	while(giNumOfTransToStartSAF > 0 && pstTran->iCmd != SSI_CARDRATE)
	{
		if((rv == ERR_RESP_TO) || ((atoi(stRespDtlsTmp.szRsltCode) == ERR_HOST_TRAN_TIMEOUT) && isDHIEnabled()))
		{
			if(pstTran->iCmd != SSI_TOR)
			{
				giRespToTran++;
				debug_sprintf(szDbgMsg, "%s: No. Of Transactions Consecutively Timedout:%d",__FUNCTION__,giRespToTran);
				APP_TRACE(szDbgMsg);
				if(giRespToTran == giNumOfTransToStartSAF)
				{
					debug_sprintf(szDbgMsg, "%s: Need to mark host down,since :%d transactions timedout",__FUNCTION__,giRespToTran);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Considering Host As Down Since Timedout transaction limit is hit");
						addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
					}
					updateHostConnectionStatus(PAAS_FALSE); //Updating the host status
					sendHostDownSignal();
					giRespToTran = 0;
				}
			}
		}
		else
		{
			giRespToTran = 0;
		}
		break;
	}
	/* De-allocate any allocated memory which is no longer required */
	if(pszSSIReq != NULL)
	{
		free(pszSSIReq);
	}

	if(pszSSIResp != NULL)
	{
		free(pszSSIResp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isHostConnected
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isHostConnected()
{
	PAAS_BOOL	bRv				= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(SUCCESS == checkHostConn(-1))
	{
		bRv = PAAS_TRUE;
	}
	else
	{
		bRv = PAAS_FALSE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__,
					(bRv == PAAS_TRUE)? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	return  bRv;
}

/*
 * ============================================================================
 * Function Name: buildBAPIReq
 *
 * Description	: This function is responsible for building the XML request
 * 					according to the BAPI specifications, filling the request
 * 					with the relevant basket details, depending upon the
 * 					current session
 *
 * Input Params	:	szTranKey	-> identifier for the transaction data
 * 					pszBuf		-> placeholder for the xml request
 * 					iLen		-> placeholder for the xml request length
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildBAPIReq(char ** pszBuf, int * iLen, char* pszBasket, int iBaskSize)
{
	int			rv				= SUCCESS;
	META_STYPE	stMetaData;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Got the transaction data. Use this to get the message */
		memset(&stMetaData, 0x00, METADATA_SIZE);
		rv = getMetaDataForBAPIReq(&stMetaData, pszBasket, iBaskSize);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}


		/* Call the xml utility api to build the xml message from the metadata
		 * just built from the transaction data */
		rv = buildXMLMsgFromMetaData(pszBuf, iLen, BAPIREQ_STR, &stMetaData);

		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI REQ XML built",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Total bytes returned [%d]",
														__FUNCTION__, *iLen);
		APP_TRACE(szDbgMsg);

		break;
	}

	/* Free the metaData */
	freeMetaDataEx(&stMetaData);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildSSIReq
 *
 * Description	: This function is responsible for building the XML request
 * 					according to the SSI specifications, filling the request
 * 					with the relevant transaction details, depending upon the
 * 					function and the command of the current transaction.
 *
 * Input Params	:	szTranKey	-> identifier for the transaction data
 * 					pszBuf		-> placeholder for the xml request
 * 					iLen		-> placeholder for the xml request length
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int buildSSIReq(TRAN_PTYPE pstTran, char ** pszBuf, int * iLen)
{
	int			rv				= SUCCESS;
	META_STYPE	stMetaData;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Got the transaction data. Use this to get the message */
		memset(&stMetaData, 0x00, METADATA_SIZE);
		rv = getMetaDataForSSIReq(&stMetaData, pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get meta data", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/*
		 * Praveen_P1: Added the following condition to
		 * frame the different Admin packet for registration
		 * TO_DO : Revist this one!!!
		 */
		if(pstTran->iFxn == SSI_ADMIN && pstTran->iCmd == SSI_SETUP)
		{
			if(getHostProtocolFormat() == HOST_PROTOCOL_UGP)
			{
				rv = buildXMLAdminReqMsgFromMetaData(pszBuf, iLen, UGPREQ_STR, &stMetaData);
			}
			else
			{
				rv = buildXMLAdminReqMsgFromMetaData(pszBuf, iLen, SSIADM_STR, &stMetaData);
			}
		}
		else
		{
			/* Call the xml utility api to build the xml message from the metadata
			 * just built from the transaction data */
			if(getHostProtocolFormat() == HOST_PROTOCOL_UGP)
			{
				rv = buildXMLMsgFromMetaData(pszBuf, iLen, UGPREQ_STR, &stMetaData);
			}
			else
			{
				rv = buildXMLMsgFromMetaData(pszBuf, iLen, SSIREQ_STR, &stMetaData);
			}
		}
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI REQ XML built",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}



		break;
	}

	/* Free the metaData */
	freeMetaDataEx(&stMetaData);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processSSIResp
 *
 * Description	: This function is responsible to process the XML response
 * 					receieved from the payment host, after the transcation
 * 					request was posted.
 *
 * Input Params	:	pstTran -> SSI transaction instance in the stack
 * 					szMsg	-> XML response recieved from the payment host
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int processSSIResp(TRAN_PTYPE pstTran, char * szMsg)
{
	int			rv					= SUCCESS;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
	META_STYPE	stMetaData;

#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(&stMetaData, 0x00, METADATA_SIZE);
	while(1)
	{
		/* Parse the xml response */
		rv = parseSSIResp(&stMetaData, szMsg, pstTran->iFxn, pstTran->iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to parse XML resp",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Processing the SSI Response");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		/* Process the parsed XML response */
		rv = procParsedResp(pstTran, &stMetaData, szMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to process parsed response",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Process SSI Response Obtained From Host");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			break;
		}

		break;
	}

	/* Free the metaData */
	freeMetaDataEx(&stMetaData);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseBAPIResp
 *
 * Description	: This function is responsible for parsing the XML response got
 * 					from the BAPI host. The parsing is done by looking for
 * 					specific information in the xml message based on bapi spec
 * 					for the session
 *
 * Input Params	:	pstMeta	-> placeholder for parsed information
 * 					szXML	-> xml response got from the server
 * 					iFxn	-> current transaction's function
 * 					iCmd	-> current transaction's command
 *
 * Output Params: SUCCESS / FAILURE/ ERR_INV_XML_MSG
 * ============================================================================
 */
static int parseBAPIResp(META_PTYPE pstMeta, char * szXML)
{
	int			rv				= SUCCESS;
	int			iLen			= 0;
	int			iCnt			= 0;
	int			result			= 0;
	xmlDoc *	docPtr			= NULL;
	xmlNode *	rootPtr			= NULL;

#ifdef DEVDEBUG
	char			szDbgMsg[4096+512]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check the parameters passed */
		if((szXML == NULL) || ((iLen = strlen(szXML)) == 0) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

#ifdef DEVDEBUG// CID 67193 (#1 of 1): Dereference before null check (REVERSE_INULL) T_RaghavendranR1 Moved Below the NULL check.
	if(strlen(szXML) < 5000)
	{
		debug_sprintf(szDbgMsg, "%s:%s: HOST BAPI RESP = [%s]",DEVDEBUGMSG,__FUNCTION__, szXML);
		APP_TRACE(szDbgMsg);
	}
#endif

		iLen = strlen(szXML);

		debug_sprintf(szDbgMsg,"%s: Length of host resp = %d",__FUNCTION__, iLen);
		APP_TRACE(szDbgMsg);

		/* Make the libxml parse the xml message */
		docPtr = xmlParseMemory(szXML, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: libxml FAILED to parse the XML data",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Get the root of the xml message */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to find root of the xml data",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Check for the root string */
		if( strcasecmp((char *) (rootPtr->name), SSIRES_STR) != SUCCESS )
		{
			debug_sprintf(szDbgMsg, "%s: Unrecognized xml response format",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}


		/* Get the meta data ready for parsing the response from PWC */
		rv = getMetaDataForBAPIResp(pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get key list", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Parse the xml message based on the meta data */
		rv = parseXMLMsg(docPtr, pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to parse PWC resp",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		for(iCnt = 0; iCnt < pstMeta->iTotCnt; iCnt++)
		{
			if(pstMeta->keyValList[iCnt].value != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: KEY = [%s] Value = [%s]", __FUNCTION__,
						pstMeta->keyValList[iCnt].key, (char*)pstMeta->keyValList[iCnt].value);
				APP_TRACE(szDbgMsg);
				if(strcmp(pstMeta->keyValList[iCnt].key, "RESULT_CODE") == SUCCESS)
				{
					result = atoi((char*)pstMeta->keyValList[iCnt].value);
				}
			}
		}
		switch(result)
		{
		case 1:
			debug_sprintf(szDbgMsg,"%s: Response Text = [%s]",__FUNCTION__,
					"SUCCESS");
			break;
		case 5004:
			debug_sprintf(szDbgMsg,"%s: Response Text = [%s]",__FUNCTION__,
					"reserved");
			break;
		case 5005:
			debug_sprintf(szDbgMsg,"%s: Response Text = [%s]",__FUNCTION__,
					"Missing Payload");
			break;
		case 5009:
			debug_sprintf(szDbgMsg,"%s: Response Text = [%s]",__FUNCTION__,
					"Missing Required Tag");
			break;
		case 5010:
			debug_sprintf(szDbgMsg,"%s: Response Text = [%s]",__FUNCTION__,
					"Malformed Packet: + extended detail if available.");
			break;
		case 5011:
			debug_sprintf(szDbgMsg,"%s: Response Text = [%s]",__FUNCTION__,
					"Invalid Command:  + The passed in Bad Command");
			break;
		case 5012:
			debug_sprintf(szDbgMsg,"%s: Response Text = [%s]",__FUNCTION__,
					"Error sending payload type Xml");
			break;
		case 5013:
			debug_sprintf(szDbgMsg,"%s: Response Text = [%s]",__FUNCTION__,
					"Error sending payload type JSON");
			break;
		case 5020:
			debug_sprintf(szDbgMsg,"%s: Response Text = [%s]",__FUNCTION__,
					"BAPI Host Unavailable: + extended detail if available.");
			break;
		case 5021:
			debug_sprintf(szDbgMsg,"%s: Response Text = [%s]",__FUNCTION__,
					"AWS Queue Error");
			break;
		case 5099:
			debug_sprintf(szDbgMsg,"%s: Response Text = [%s]",__FUNCTION__,
					"Not Authorized");
			break;
		default:
			debug_sprintf(szDbgMsg,"%s: Should not come here. Improper result code",__FUNCTION__);
			break;
		}
		APP_TRACE(szDbgMsg);
		break;
	}

	/* Free the xml document */
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseSSIResp
 *
 * Description	: This function is responsible for parsing the XML response got
 * 					from the payment host. The parsing is done by looking for
 * 					specific information in the xml message based on SSI spec
 * 					for the current function and command of the transaction.
 *
 * Input Params	:	pstMeta	-> placeholder for parsed information
 * 					szXML	-> xml response got from the server
 * 					iFxn	-> current transaction's function
 * 					iCmd	-> current transaction's command
 *
 * Output Params: SUCCESS / FAILURE/ ERR_INV_XML_MSG
 * ============================================================================
 */
static int parseSSIResp(META_PTYPE pstMeta, char * szXML, int iFxn, int iCmd)
{
	int			rv				= SUCCESS;
	int			iLen			= 0;
	xmlDoc *	docPtr			= NULL;
	xmlNode *	rootPtr			= NULL;
	xmlChar *	pszVal			= NULL;
//	char 		szTmpXML[4096]  = "";
#ifdef DEVDEBUG
	char			szDbgMsg[4096+512]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		// CID-67227: 2-Feb-16: MukeshS3: Adding NULL checking for input parameters at beginning.
		/* Check the parameters passed */
		if((szXML == NULL) || ((iLen = strlen(szXML)) == 0) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = ERR_INV_XML_MSG;
			break;
		}

#ifdef DEVDEBUG
		if(strlen(szXML) < 5000)
		{
			debug_sprintf(szDbgMsg, "%s:%s: HOST RESP = [%s]",DEVDEBUGMSG,__FUNCTION__, szXML);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: The length of the SSI response = [%d]",__FUNCTION__, strlen(szXML));
			APP_TRACE(szDbgMsg);
		}
#endif
#if 0
		printf ("%s\n",szXML);
#endif
		/* 
		 * -------------------------- VIKRAM DATT RANA -----------------------
		 * FIXME: This is not a good idea, but has been done to prevent an
		 * error being given by libcurl while parsing the xml response coming
		 * from PWC. The PWC guys are not sending correct form of xml response,
		 * causing the following error
		 * -------------------------------------------------------------------
		 * Entity: line 1: parser error : Document labelled UTF-16 but has
		 * UTF-8 content
		 * <?xml version="1.0" encoding="utf-16"?><MSH
		 * TYPE="21"><HEADER><DEVTYPE>Mx915P</DEVTYPE>
		 * -------------------------------------------------------------------
		 */
		replaceUTF8WithUTF16(szXML);

		iLen = strlen(szXML);

		debug_sprintf(szDbgMsg,"%s: Length of host resp = %d",__FUNCTION__,iLen);
		APP_TRACE(szDbgMsg);

		/* Make the libxml parse the xml message */
		docPtr = xmlParseMemory(szXML, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: libxml FAILED to parse the XML data",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Get the root of the xml message */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to find root of the xml data",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Check for the root string */
		if(getHostProtocolFormat() == HOST_PROTOCOL_UGP)
		{
			if( (strcasecmp((char *) (rootPtr->name), UGPRES_STR) != SUCCESS) &&
					(strcasecmp((char *) (rootPtr->name), SSIADM_STR) != SUCCESS) )
			{
				debug_sprintf(szDbgMsg, "%s: Unrecognized xml response format",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_XML_MSG;
				break;
			}
		}
		else
		{
			if( (strcasecmp((char *) (rootPtr->name), SSIRES_STR) != SUCCESS) &&
					(strcasecmp((char *) (rootPtr->name), SSIADM_STR) != SUCCESS) )
			{
				debug_sprintf(szDbgMsg, "%s: Unrecognized xml response format",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_XML_MSG;
				break;
			}
		}


		/* Check the RSA FLAG for the root element of the xml response, this
		 * check is needed to ensure if the device resync is needed or not */
		pszVal = xmlGetProp(rootPtr, (const xmlChar *)"RSA");
		if( (pszVal != NULL) && (*pszVal == '1') )
		{
			if(*pszVal == '1')
			{
				/* Device resync is needed */
				setAdminPacketRequestStatus(PAAS_TRUE);

				/* Set the flag for device resync request in the
				* config.usr1 */
				setDevAdminRqd(PAAS_TRUE);
			}

			xmlFree(pszVal);
		}

		/* Get the meta data ready for parsing the response from PWC */
		rv = getMetaDataForSSIResp(pstMeta, iFxn, iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get key list", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Parse the xml message based on the meta data */
		rv = parseXMLMsg(docPtr, pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to parse PWC resp",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	/* Free the xml document */
	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: replaceUTF8WithUTF16
 *
 * Description	: just a temporary work around to fix an issue which was
 * 					happening, when PWC was trying to send normal xml message
 * 					as response with encoding specified as UTF16, whereas the
 * 					xml message contained only UTF8 characters. The xml parsing
 * 					was failing at the libcurl side.
 *
 * Input Params	: szXML	-> xml message being parsed
 *
 * Output Params: none
 * ============================================================================
 */
static void replaceUTF8WithUTF16(char * szXML)
{
	char *	cPtr1			= NULL;
	char *	cPtr2			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	cPtr1 = strstr(szXML, "encoding=\"utf-16\"");
	if(cPtr1 != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Replacing utf-16 by utf-8", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		cPtr2 = strstr(cPtr1, "utf-16");
		if(cPtr2 != NULL)
		{
			memcpy(cPtr2, "utf-8\" ", strlen("utf-8\" "));
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * End of file ssiMsgCtrl.c
 * ============================================================================
 */
