/******************************************************************
*                       amount.c                                  *
*******************************************************************
* Application: Point Solutions                                    *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include "stdio.h"
#include "string.h"
#include "stdlib.h"

#include "psAmtExt.h"
#include "common.h"


/* Static functions declarations - BEGIN */
static int sumAmts(AMT_PTYPE, AMT_PTYPE, AMT_PTYPE);
static int diffAmts(AMT_PTYPE, AMT_PTYPE, AMT_PTYPE);
static int calcDiff(AMT_PTYPE, AMT_PTYPE, AMT_PTYPE);
static int compareAmts(AMT_PTYPE, AMT_PTYPE);
/* Static functions declarations - END */

/*
 * ============================================================================
 * Function Name: getAmtFormattedStr
 *
 * Description	: This function is responsible for getting a formatted amount
 * 					string, with the amount provided as parameter to the
 * 					function.
 *
 * Input Params	:	pstAmt -> amount
 * 					szDest -> placeholder for formatted amount string
 * 					iSize  -> size of the placeholder
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getFormattedAmtStr(AMT_PTYPE pstAmt, char * szDest, int iSize)
{
	int			rv				= SUCCESS;
	int			iLen			= 0;
	int			iCnt			= 0;
	char		szTmpAmt[20]	= "";
	CRNCY_PTYPE	pstCrncy		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstAmt == NULL) || (szDest == NULL) || (iSize == 0))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid parameters", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the currency details for the amount */
		pstCrncy = pstAmt->pstCrncy;
		if(pstCrncy == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: The amount passed has no currency",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the span string of the basic amount */
		sprintf(szTmpAmt, "%d%c%.*d", pstAmt->iChar, pstCrncy->cDecSep,
												pstCrncy->iPrec, pstAmt->iMan);

		/* VDR:TODO Remove this later */
		debug_sprintf(szDbgMsg, "%s: Temporary Amount String = [%s]",
														__FUNCTION__, szDest);
		APP_TRACE(szDbgMsg);

		/* Get the probable length of the amount string */
		iLen = strlen(szTmpAmt);
		iLen += 1; /* For the amount sign */
		iLen += strlen(pstCrncy->szSym); /* currency symbol */

		if(iLen < iSize)
		{
			debug_sprintf(szDbgMsg, "%s: Size of string is less, need [%d]",
															__FUNCTION__, iLen);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Size of the destination string is enough for the amount formatting
		 * to be done safely, proceed with printing the formatted amount string
		 * using the amount characteristics */
		memset(szDest, ' ', iSize);
		iCnt = sprintf(szDest, "%c%s", (pstAmt->eSign == NEG_AMT) ? '-': ' ',
						pstCrncy->szSym);
		iLen = iSize - iCnt;
		sprintf(szDest + iCnt, "%*.*s", iLen, iLen, szTmpAmt);

		/* VDR:TODO Remove this later */
		debug_sprintf(szDbgMsg, "%s: Formatted Amount String = [%s]",
														__FUNCTION__, szDest);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSimpleAmtStr
 *
 * Description	: This function is responsible for getting an amount string
 * 					without the sign or the currency information, with the
 * 					amount being provided in the parameters to this function
 *
 * Input Params	:	pstAmt -> amount
 * 					szDest -> placeholder for formatted amount string
 * 					iSize  -> size of the placeholder
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getSimpleAmtStr(AMT_PTYPE pstAmt, char * szDest, int iSize)
{
	int			rv				= SUCCESS;
	int			iLen			= 0;
	char		szTmpAmt[20]	= "";
	CRNCY_PTYPE	pstCrncy		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstAmt == NULL) || (szDest == NULL) || (iSize == 0))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid parameters", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the currency details for the amount */
		pstCrncy = pstAmt->pstCrncy;
		if(pstCrncy == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: The amount passed has no currency",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the string of the basic amount */
		sprintf(szTmpAmt, "%d%c%.*d", pstAmt->iChar, pstCrncy->cDecSep,
												pstCrncy->iPrec, pstAmt->iMan);

		/* Get the probable length of the amount string */
		iLen = strlen(szTmpAmt);
		if(iLen < iSize)
		{
			debug_sprintf(szDbgMsg, "%s: Size of string is less, need [%d]",
															__FUNCTION__, iLen);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Size of the destination string is enough for the amount formatting
		 * to be done safely, proceed with printing the formatted amount string
		 * using the amount characteristics */
		strcpy(szDest, szTmpAmt);

		/* VDR:TODO Remove this later */
		debug_sprintf(szDbgMsg, "%s: Amt String = [%s]", __FUNCTION__, szDest);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getAmtFrmStr
 *
 * Description	: This function is responsible for creating amount from the
 * 					string provided as the parameter. The currency details
 * 					should be mandatorily provided to this function. Advice the
 * 					caller to create currency details and then use the same for
 * 					forming amounts.
 *
 * Input Params	:	pstAmt  -> placeholder for amount details 
 * 					pstCrncy-> currency details (to be used for amount) 
 * 					szAmt   -> amount string 
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getAmtFrmStr(AMT_PTYPE pstAmt, CRNCY_PTYPE pstCrncy, char * szAmt)
{
	int		rv				= SUCCESS;
	int		iChar			= 0;
	int		iMan			= 0;
	char	szTmp[25]		= "";
	char	szTmpAmt[50]	= "";
	char *	cTmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstAmt == NULL) || (pstCrncy == NULL) || (szAmt == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid parameters", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get a stripped version of the amount string, with the currency
		 * characters and the thousands separator, if present, stripped off.
		 * The string received after this operation would be containing only
		 * digits and the decimal separator character */
		strcpy(szTmpAmt, szAmt);
		rv = stripAmtStrForCrncy(szTmpAmt, pstCrncy);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to strip amt str",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* VDR:TODO Remove this later */
		debug_sprintf(szDbgMsg, "%s: Stripped amount string = [%s]",
														__FUNCTION__, szTmpAmt);
		APP_TRACE(szDbgMsg);

		/* Get the chracteristic and mantissa from the amount string */
		cTmpPtr = strchr(szTmpAmt, pstCrncy->cDecSep);
		if(cTmpPtr != NULL)
		{
			memcpy(szTmp, szTmpAmt, cTmpPtr - szTmpAmt);

			iChar = atoi(szTmp);
			iMan = atoi(cTmpPtr + 1);
		}
		else
		{
			iChar = atoi(szTmpAmt);
		}

		/* VDR:TODO Remove this later */
		debug_sprintf(szDbgMsg, "%s: characteristic = [%d] & mantissa = [%d]",
													__FUNCTION__, iChar, iMan);
		APP_TRACE(szDbgMsg);

		/* Set the amount details in the placeholder */
		memset(pstAmt, 0x00, AMT_SIZE);
		pstAmt->pstCrncy = pstCrncy;
		pstAmt->iChar = iChar;
		pstAmt->iMan = iMan;
		pstAmt->eSign = POS_AMT;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addAmts
 *
 * Description	: This function is responsible for adding the first two amounts
 * 					passed as parameters and store the result in the third
 * 					amount param
 *
 * Input Params	:	pstAmt1 -> first amount details
 * 					pstAmt2 -> second amount details
 * 					pstAmt3 -> placeholder for the resulting amount
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int addAmts(AMT_PTYPE pstAmt1, AMT_PTYPE pstAmt2, AMT_PTYPE pstAmt3)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstAmt1 == NULL) || (pstAmt2 == NULL) || (pstAmt3 == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid parameters", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Only the amounts of the same currencies can be added */
		rv = compareCrncys(pstAmt1->pstCrncy, pstAmt2->pstCrncy);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Currencies aren't same", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstAmt1->eSign == pstAmt2->eSign)
		{
			/* Get the sum of the amounts irrespective of their sign */
			sumAmts(pstAmt1, pstAmt2, pstAmt3);

			/* Assign the sign here as the sign can be positive or negative */
			pstAmt3->eSign = pstAmt1->eSign;
		}
		else if(pstAmt2->eSign == NEG_AMT)
		{
			/* Get the difference between amount 1 and amount 2, and keep the
			 * same sign of the resultant amount */
			diffAmts(pstAmt1, pstAmt2, pstAmt3);
		}
		else if(pstAmt1->eSign == NEG_AMT)
		{
			/* Get the difference between amount 2 and amount 1, and keep the
			 * same sign of the resultant amount */
			diffAmts(pstAmt2, pstAmt1, pstAmt3);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: subtractAmts
 *
 * Description	: This function is responsible for subtracting the two amounts
 * 					(second from first) and storing the result in the third
 * 					amount passed as paramter
 *
 * Input Params	:	pstAmt1 -> first amount details
 * 					pstAmt2 -> second amount details
 * 					pstAmt3 -> placeholder for the resulting amount
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int subtractAmts(AMT_PTYPE pstAmt1, AMT_PTYPE pstAmt2, AMT_PTYPE pstAmt3)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstAmt1 == NULL) || (pstAmt2 == NULL) || (pstAmt3 == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid parameters", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Only the amounts of the same currencies can be added */
		rv = compareCrncys(pstAmt1->pstCrncy, pstAmt2->pstCrncy);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Currencies aren't same", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Check for the sign of the first amount */
		switch(pstAmt1->eSign)
		{
		case POS_AMT:

			/* Check for the sign of the second amount */
			switch(pstAmt2->eSign)
			{
			case POS_AMT:
				/* +a - (+b) => a - b
				 * Get the difference between amount 1 and amount 2, and keep
				 * the same sign of the resultant amount */
				diffAmts(pstAmt1, pstAmt2, pstAmt3);

				break;

			case NEG_AMT:
				/* +a - (-b) => a + b
				 * Calculate the sum of both the amounts and keep the sign of
				 * the result amount as positive */
				sumAmts(pstAmt1, pstAmt2, pstAmt3);
				pstAmt3->eSign = POS_AMT;

				break;
			}

			break;

		case NEG_AMT:

			/* Check for the sign of the second amount */
			switch(pstAmt2->eSign)
			{
			case POS_AMT:
				/* -a - (+b) => -(a + b)
				 * Calculate the sum of both the amounts and keep the sign of
				 * the result amount as negative */
				sumAmts(pstAmt1, pstAmt2, pstAmt3);
				pstAmt3->eSign = NEG_AMT;

				break;

			case NEG_AMT:
				/* -a - (-b) => b - a
				 * Get the difference between amount 2 and amount 1, and keep
				 * the same sign of the resultant amount */
				diffAmts(pstAmt2, pstAmt1, pstAmt3);

				break;
			}

			break;
		}

		break; /* For the while loop */
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: mulScalarWithAmt
 *
 * Description	: This function is responsible for multiplying an integer
 * 					scalar value to the amount passed as parameter, and store
 * 					the result in the result amount passed as parameter
 *
 * Input Params	:	iVal      -> scalar value
 * 					pstAmt    -> amount which would be multiplied with iVal
 * 					pstRsltAmt-> result amount details placeholder
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int mulScalarWithAmt(int iVal, AMT_PTYPE pstAmt, AMT_PTYPE pstRsltAmt)
{
	int		rv				= SUCCESS;
	int		iMan			= 0;
	int		iChar			= 0;
	int		iPrec			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstAmt == NULL) || (pstRsltAmt == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid parameters", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		iPrec = pstAmt->pstCrncy->iPrec;

		iChar = pstAmt->iChar * iVal;
		iMan = pstAmt->iMan * iVal;
		if(iMan > (10 ^ iPrec))
		{
			iChar += iMan / (10 ^ iPrec);
			iMan = iMan % (10 ^ iPrec);
		}

		/* Populate the details in the resultant amount */
		memset(pstRsltAmt, 0x00, AMT_SIZE);
		pstRsltAmt->pstCrncy = pstAmt->pstCrncy;
		pstRsltAmt->iChar = iChar;
		pstRsltAmt->iMan = iMan;
		pstRsltAmt->eSign = pstAmt->eSign;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: divideAmtByScalar
 *
 * Description	: This function is responsible for dividing an amount by an
 * 					integer scalar value and store result in the third
 * 					parameter which is a placeholder for resultant amount.
 *
 * Input Params	:	iVal      -> scalar value
 * 					pstAmt    -> amount which would be divided by iVal
 * 					pstRsltAmt-> result amount details placeholder
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int divideAmtByScalar(int iVal, AMT_PTYPE pstAmt, AMT_PTYPE pstRsltAmt)
{
	int		rv				= SUCCESS;
	int		iTmp			= 0;
	int		iMan			= 0;
	int		iChar			= 0;
	int		iPrec			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstAmt == NULL) || (pstRsltAmt == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid parameters", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		iPrec = pstAmt->pstCrncy->iPrec;

		iMan = pstAmt->iMan;
		iChar = pstAmt->iChar / iVal;
		iMan += (pstAmt->iChar % iVal) * (10 ^ iPrec);

		/* Calculations done for rounding off the amount decimal part to the
		 * correct precision */
		iTmp = iMan / iVal;
		if( ((iMan % iVal) * 10) > (iVal * 5) )
		{
			iMan = iTmp + 1;
		}
		else
		{
			iMan = iTmp;
		}

		/* Populate the details in the resultant amount */
		memset(pstRsltAmt, 0x00, AMT_SIZE);
		pstRsltAmt->pstCrncy = pstAmt->pstCrncy;
		pstRsltAmt->iChar = iChar;
		pstRsltAmt->iMan = iMan;
		pstRsltAmt->eSign = pstAmt->eSign;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isAmtGt
 *
 * Description	: This function is responsible for checking if the first amount
 * 					is strictly greater than the second amount, when both
 * 					amounts are being passed as params
 *
 * Input Params	:	pstAmt1 -> first amount
 * 					pstAmt2 -> second amount
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int isAmtGt(AMT_PTYPE pstAmt1, AMT_PTYPE pstAmt2)
{
	int		rv				= SUCCESS;
	int		iRetVal			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstAmt1 == NULL) || (pstAmt2 == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid parameters", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstAmt1->eSign == pstAmt2->eSign)
		{
			iRetVal = compareAmts(pstAmt1, pstAmt2);
			switch(iRetVal)
			{
			case LESSER_THAN:
			case EQUAL_TO:
				if(pstAmt1->eSign == POS_AMT)
				{
					rv = FAILURE;
				}
				else
				{
					rv = SUCCESS;
				}
				break;

			case GREATER_THAN:
				if(pstAmt1->eSign == POS_AMT)
				{
					rv = SUCCESS;
				}
				else
				{
					rv = FAILURE;
				}
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Shouldnt come here", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		else if(pstAmt1->eSign == NEG_AMT)
		{
			rv = FAILURE;
		}
		else if(pstAmt2->eSign == NEG_AMT)
		{
			rv = SUCCESS;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isAmtGte
 *
 * Description	: This function is responsible for checking if the first amount
 * 					is greater than equal to the second amount, when both
 * 					amounts are being passed as params
 *
 * Input Params	:	pstAmt1 -> first amount
 * 					pstAmt2 -> second amount
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int isAmtGte(AMT_PTYPE pstAmt1, AMT_PTYPE pstAmt2)
{
	int		rv				= SUCCESS;
	int		iRetVal			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstAmt1 == NULL) || (pstAmt2 == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid parameters", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstAmt1->eSign == pstAmt2->eSign)
		{
			iRetVal = compareAmts(pstAmt1, pstAmt2);
			switch(iRetVal)
			{
			case LESSER_THAN:
				if(pstAmt1->eSign == POS_AMT)
				{
					rv = FAILURE;
				}
				else
				{
					rv = SUCCESS;
				}
				break;

			case GREATER_THAN:
			case EQUAL_TO:
				if(pstAmt1->eSign == POS_AMT)
				{
					rv = SUCCESS;
				}
				else
				{
					rv = FAILURE;
				}
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Shouldnt come here", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		else if(pstAmt1->eSign == NEG_AMT)
		{
			rv = FAILURE;
		}
		else if(pstAmt2->eSign == NEG_AMT)
		{
			rv = SUCCESS;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isAmtLt
 *
 * Description	: This function is responsible for checking if the first amount
 * 					is strictly smaller than the second amount, when both
 * 					amounts are being passed as params
 *
 * Input Params	:	pstAmt1 -> first amount
 * 					pstAmt2 -> second amount
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int isAmtLt(AMT_PTYPE pstAmt1, AMT_PTYPE pstAmt2)
{
	int		rv				= SUCCESS;
	int		iRetVal			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstAmt1 == NULL) || (pstAmt2 == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid parameters", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstAmt1->eSign == pstAmt2->eSign)
		{
			iRetVal = compareAmts(pstAmt1, pstAmt2);
			switch(iRetVal)
			{
			case LESSER_THAN:
				if(pstAmt1->eSign == POS_AMT)
				{
					rv = SUCCESS;
				}
				else
				{
					rv = FAILURE;
				}
				break;

			case GREATER_THAN:
			case EQUAL_TO:
				if(pstAmt1->eSign == POS_AMT)
				{
					rv = FAILURE;
				}
				else
				{
					rv = SUCCESS;
				}
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Shouldnt come here", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		else if(pstAmt1->eSign == NEG_AMT)
		{
			rv = SUCCESS;
		}
		else if(pstAmt2->eSign == NEG_AMT)
		{
			rv = FAILURE;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isAmtLte
 *
 * Description	: This function is responsible for checking if the first amount
 * 					is smaller than equal to the second amount, when both
 * 					amounts are being passed as params
 *
 * Input Params	:	pstAmt1 -> first amount
 * 					pstAmt2 -> second amount
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int isAmtLte(AMT_PTYPE pstAmt1, AMT_PTYPE pstAmt2)
{
	int		rv				= SUCCESS;
	int		iRetVal			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstAmt1 == NULL) || (pstAmt2 == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid parameters", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstAmt1->eSign == pstAmt2->eSign)
		{
			iRetVal = compareAmts(pstAmt1, pstAmt2);
			switch(iRetVal)
			{
			case LESSER_THAN:
			case EQUAL_TO:
				if(pstAmt1->eSign == POS_AMT)
				{
					rv = SUCCESS;
				}
				else
				{
					rv = FAILURE;
				}
				break;

			case GREATER_THAN:
				if(pstAmt1->eSign == POS_AMT)
				{
					rv = FAILURE;
				}
				else
				{
					rv = SUCCESS;
				}
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Shouldnt come here", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		else if(pstAmt1->eSign == NEG_AMT)
		{
			rv = SUCCESS;
		}
		else if(pstAmt2->eSign == NEG_AMT)
		{
			rv = FAILURE;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: compareAmts
 *
 * Description	: This function is responsible for comparing two amounts passed
 * 					as parameters and tell whether the first is
 * 					lesser/equal/greater than the second.
 *
 * Input Params	:	pstAmt1 -> first amount details
 * 					pstAmt2 -> second amount details
 *
 * Output Params: LESSER_THAN / EQUAL_TO / GREATER_THAN
 * ============================================================================
 */
static int compareAmts(AMT_PTYPE amt1, AMT_PTYPE amt2)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(amt1->iChar > amt2->iChar)
	{
		rv = GREATER_THAN;
	}
	else if(amt1->iChar == amt2->iChar)
	{
		if(amt1->iMan > amt2->iMan)
		{
			rv = GREATER_THAN;
		}
		else if(amt1->iMan == amt2->iMan)
		{
			rv = EQUAL_TO;
		}
		else
		{
			rv = LESSER_THAN;
		}
	}
	else
	{
		rv = LESSER_THAN;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: diffAmts
 *
 * Description	: This function is responsible for subtracting the second
 * 					amount from the first amount and storing the result in the
 * 					third amount, ignoring the signs of the first two amounts
 *
 * Input Params	:	pstAmt1 -> first amount details
 * 					pstAmt2 -> second amount details
 * 					pstAmt3 -> placeholder for the resulting amount
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int diffAmts(AMT_PTYPE pstAmt1, AMT_PTYPE pstAmt2, AMT_PTYPE pstAmt3)
{
	int		rv				= SUCCESS;
	int		iCmpVal			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Check which amount is bigger, the difference calculation and the sign of
	 * the result would depend upon that */
	iCmpVal = compareAmts(pstAmt1, pstAmt2);
	switch(iCmpVal)
	{
	case LESSER_THAN:
		calcDiff(pstAmt2, pstAmt1, pstAmt3);
		pstAmt3->eSign = NEG_AMT;
		break;

	case EQUAL_TO:
	case GREATER_THAN:
		calcDiff(pstAmt1, pstAmt2, pstAmt3);
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: calcDiff
 *
 * Description	: This function calculates the difference between the first
 * 					amount and the second amount, assuming that first amount is
 * 					always greater than the second amount (irrespective of the
 * 					sign of the amounts)
 *
 * Input Params	:	pstAmt1 -> first amount details
 * 					pstAmt2 -> second amount details
 * 					pstAmt3 -> placeholder for the resulting amount
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int calcDiff(AMT_PTYPE pstAmt1, AMT_PTYPE pstAmt2, AMT_PTYPE pstAmt3)
{
	int		rv				= SUCCESS;
	int		iMan			= 0;
	int		iChar			= 0;
	int		iPrec			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Get the precision from any of the first two amounts as the currencies
	 * for both are same */
	iPrec = pstAmt1->pstCrncy->iPrec;

	/* Calculate the difference in characteristics */
	iChar = pstAmt1->iChar - pstAmt2->iChar;

	/* Calculate the difference in mantissas */
	if(pstAmt1->iMan > pstAmt2->iMan)
	{
		iMan = pstAmt1->iMan - pstAmt2->iMan;
	}
	else
	{
		/* Carry forward principle used in normal subtraction */
		iChar -= 1;
		iMan = (pstAmt1->iMan + (10 ^ iPrec)) - pstAmt2->iMan;
	}

	/* Populate the details in the resultant amount */
	memset(pstAmt3, 0x00, AMT_SIZE);
	pstAmt3->pstCrncy = pstAmt1->pstCrncy;
	pstAmt3->iChar = iChar;
	pstAmt3->iMan = iMan;
	pstAmt3->eSign = POS_AMT;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sumAmts
 *
 * Description	: This function is responsible for adding the first two amounts
 * 					and storing the result in the third amount, ignoring the
 * 					signs of the first two amounts.
 *
 * Input Params	:	pstAmt1 -> first amount details
 * 					pstAmt2 -> second amount details
 * 					pstAmt3 -> placeholder for the resulting amount
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int sumAmts(AMT_PTYPE pstAmt1, AMT_PTYPE pstAmt2, AMT_PTYPE pstAmt3)
{
	int		rv				= SUCCESS;
	int		iMan			= 0;
	int		iChar			= 0;
	int		iPrec			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Get the precision from any of the first two amounts as the currencies
	 * for both are same */
	iPrec = pstAmt1->pstCrncy->iPrec;

	/* Calculate the characteristic total */
	iChar = pstAmt1->iChar;
	iChar += pstAmt2->iChar;

	/* Calculate the mantissa total */
	iMan = pstAmt1->iMan;
	iMan += pstAmt2->iMan;

	if(iMan > (10 ^ iPrec))
	{
		iChar += iMan / (10 ^ iPrec);
		iMan = iMan % (10 ^ iPrec);
	}

	/* Populate the details in the resultant amount */
	memset(pstAmt3, 0x00, AMT_SIZE);
	pstAmt3->pstCrncy = pstAmt1->pstCrncy;
	pstAmt3->iChar = iChar;
	pstAmt3->iMan = iMan;
	pstAmt3->eSign = POS_AMT;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name:
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */

/*
 * ============================================================================
 * End of file amount.c
 * ============================================================================
 */
