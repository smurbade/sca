/******************************************************************
*                       config.c                                  *
*******************************************************************
* Application: Point Solutions                                    *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>
#include "svc.h"
#include "common.h"
#include "utils.h"
#include "xmlUtils.h"
#include "cfgData.h"
#include "sessDataStore.h"
#include "amt/psAmtDef.h"
#include "psAmtExt.h"

#define	CCY_TBL_FILE	"./flash/ccyTbl.dat"
#define	CCY_DEF_FILE	"./flash/ISO4217CcyCodes.xml"
#define CCY_MAP_FILE	"./flash/ccySymMap.dat"
#define DFLT_CCY_CODE	"USD"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

/* Definition of a currency record to be stored in the binary tree */
typedef struct __stCcyRec
{
	int     			iCcyNo;
	short   			sExp;
	char    			szCcy[4];
	char    			szSym[5];
	struct __stCcyRec *	left;
	struct __stCcyRec *	right;
}
CCYREC_STYPE, * CCYREC_PTYPE;

/* Head node for the currency records binary tree */
typedef struct __stHeadRec
{
	int             iTotRec;
	CCYREC_PTYPE    pstRoot;
}
CCYTBL_STYPE, * CCYTBL_PTYPE;

typedef enum
{
	POINT_AS_DECIMAL = 1,
	COMMA_AS_DECIMAL
}
AMT_FMT_ENUM;

#define DFLT_AMT_FORMAT	POINT_AS_DECIMAL

/* Static data */
/* TODO: Write code logic for populating these values */
static CRNCY_STYPE	stDfltCcy;
static AMT_FMT_ENUM	eAmtFmt;

/* Static functions declarations - BEGIN */
static int loadCcyDefs(char *);
static int loadCcyTbl(char *);
static int loadCcySymbols(char *, CCYTBL_PTYPE);
static int storeCcyDefs(CCYTBL_PTYPE);
static int createCcyTbl(VAL_LST_PTYPE, CCYTBL_PTYPE);
static int populateCcyRec(int, KEYVAL_PTYPE, CCYREC_PTYPE);
static int addCcyRecToTbl(CCYREC_PTYPE, CCYTBL_PTYPE);
static int getNxtCodeNSymbolPair(FILE *, char *, char *);
static int updateCcySymbol(CCYREC_PTYPE, char *, char *);
static int getDefaultCcy(CRNCY_PTYPE);

static void storeDataInOrder(CCYREC_PTYPE, FILE *);
static void addRecToBinTree(CCYREC_PTYPE, CCYREC_PTYPE);
static void flushCcyBinTree(CCYREC_PTYPE);
#ifdef DEBUG
static void printCcyTbl(CCYREC_PTYPE);
static void printCcyRecDtls(CCYREC_PTYPE);
#endif
/* Static functions declarations - END */

/*
 * ============================================================================
 * Function Name: runAmtCcyCfg
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int runAmtCcyCfg()
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	int		iSize			= 0;
	char	szTmp[10]		= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(doesFileExist(CCY_DEF_FILE) == SUCCESS)
		{
			/* Load the currency definitions from the xml file and store them
			 * in the application internal dat file */
			rv = loadCcyDefs(CCY_DEF_FILE);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: ", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else if(doesFileExist(CCY_TBL_FILE) == SUCCESS)
		{
			/* Load the currency table from the application internal file */
			rv = loadCcyTbl(CCY_TBL_FILE);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: ", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else
		{
			/* No information/file exists on the terminal regarding the
			 * currency records. This is an erroneous case and the application
			 * should not proceed any further */

			debug_sprintf(szDbgMsg, "%s: No information exists regarding CCY",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the application currency code */
		iSize = sizeof(szTmp);
		memset(&stDfltCcy, 0x00, CRNCY_SIZE);

		rv = getEnvFile(SECTION_CCY, CCY_CODE, szTmp, iSize);
		if( (rv < 0) || ( (iLen = strlen(szTmp)) != 3 ) || 
			(strspn(szTmp, "ABCDEFGHIJKLMNOPQRSTUVWXYZ") != iLen) )
		{
			/* The currency code was either not given in config.usr1, or its
			 * length was incorrect (not equal to 3) or was not alphabetic */
			rv = getDefaultCcy(&stDfltCcy);
			if(rv != SUCCESS)
			{
				break;
			}
		}
		else
		{
			/* The currency code seems to be a valid string. */
			rv = getCcyFrmTbl(szTmp, &stDfltCcy);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: No ccy found for [%s]; trying dflt"
														, __FUNCTION__, szTmp);
				APP_TRACE(szDbgMsg);

				/* Currency definition record not found for the currency code
				 * supplied in config.usr1. Try with the application default
				 * currency code */
				rv = getDefaultCcy(&stDfltCcy);
				if(rv != SUCCESS)
				{
					break;
				}
			}
		}

		/* Get the application amount format */
		memset(szTmp, 0x00, iSize);
		rv = getEnvFile(SECTION_CCY, AMT_FMT, szTmp, iSize);
		if( (rv < 0) || ( (iLen = strlen(szTmp)) != 1 ) || 
			(strspn(szTmp, "12") != iLen) )
		{
			/* Invalid/Missing config value for the application amount format.
			 * Will set the default amount format, which is to use point as a
			 * decimal separator */
			eAmtFmt = POINT_AS_DECIMAL;
			putEnvFile(SECTION_CCY, AMT_FMT, "1");
		}
		else if(szTmp[0] == '1')
		{
			eAmtFmt = POINT_AS_DECIMAL;
		}
		else
		{
			eAmtFmt = COMMA_AS_DECIMAL;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDefaultCcy
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getDefaultCcy(CRNCY_PTYPE pstCcy)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	rv = getCcyFrmTbl(DFLT_CCY_CODE, &stDfltCcy);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: No ccy found for default [%s]",
												__FUNCTION__, DFLT_CCY_CODE);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		putEnvFile(SECTION_CCY, CB_AMT_LIMIT, DFLT_CCY_CODE);
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCcyFrmTbl
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getCcyFrmTbl(char * szCcyCode, CRNCY_PTYPE pstCcy)
{
	int				rv				= SUCCESS;
	FILE *			fp				= NULL;
	PAAS_BOOL		bCcyFound		= PAAS_TRUE;
	CCYREC_STYPE	stTmpRec;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(pstCcy, 0x00, CRNCY_SIZE);

		/* Currency needs to be read from the currency table file */
		fp = fopen(CCY_TBL_FILE, "rb");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to open file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Find the currency record corresponding to the currency code supplied
		 * to the function as parameter */
		while(!feof(fp))
		{
			memset(&stTmpRec, 0x00, sizeof(stTmpRec));
			rv = fread(&stTmpRec, sizeof(stTmpRec), 1, fp);
			if(rv == 1)
			{
				rv = SUCCESS;

				if(strcmp(stTmpRec.szCcy, szCcyCode) == SUCCESS)
				{
					/* Currency record found in the file. Get the currency
					 * details for the transaction accordingly */

					pstCcy->iPrec = stTmpRec.sExp;

					if(strlen(stTmpRec.szSym) != 0)
					{
						strcpy(pstCcy->szSym, stTmpRec.szSym);
					}
					else
					{
						strcpy(pstCcy->szSym, stTmpRec.szCcy);
					}

					/* Format: either ONE or TWO */
					if(eAmtFmt == COMMA_AS_DECIMAL)
					{
						pstCcy->cDecSep = ',';
						pstCcy->cThSep = '.';
					}
					else
					{
						pstCcy->cDecSep = '.';
						pstCcy->cThSep = ',';
					}

					bCcyFound = PAAS_TRUE;
				}
			}
			else if(ferror(fp))
			{
				debug_sprintf(szDbgMsg, "%s: Error while reading from file",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		if((rv == SUCCESS) && (bCcyFound != PAAS_TRUE))
		{
			debug_sprintf(szDbgMsg, "%s: No currency found for [%s]",
													__FUNCTION__, szCcyCode);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}

		break;
	}

	/* Close any open file descriptors */
	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDfltCcy
 *
 * Description	: This function is responsible for getting the default currency
 * 					that needs to be used for processing a transaction, in case
 * 					no currency code was supplied by POS
 *
 * Input Params	: pstCcy -> place holder for the currency details
 *
 * Output Params: none
 * ============================================================================
 */
void getDfltCcy(CRNCY_PTYPE pstCcy)
{
	memset(pstCcy, 0x00, CRNCY_SIZE);
	memcpy(pstCcy, &stDfltCcy, CRNCY_SIZE);
}

/*
 * ============================================================================
 * Function Name: loadCcyDefs
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int loadCcyDefs(char * szCcyDefFile)
{
	int				rv				= SUCCESS;
	PAAS_BOOL		bMapFileExists	= PAAS_FALSE;
	CCYTBL_STYPE	stCcyTbl;
	METADATA_STYPE	stMetaData;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	KEYVAL_STYPE	ccyFldLst[]		=
	{
		{ "Ccy"       , NULL, PAAS_TRUE, SINGLETON, NULL },
		{ "CcyNbr"    , NULL, PAAS_TRUE, SINGLETON, NULL },
		{ "CcyMnrUnts", NULL, PAAS_TRUE, SINGLETON, NULL }
	};

	VARLST_INFO_STYPE	stCcyNodes[]= 
	{
		{0 , "CcyNtry", sizeof(ccyFldLst) / KEYVAL_SIZE, ccyFldLst },
		{-1, NULL     , 0                              , NULL      }
	};

	KEYVAL_STYPE	stCcyDefLst[]	= 
	{
		{ "CcyTbl", NULL, PAAS_TRUE, VARLIST, stCcyNodes }
	};

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(&stMetaData, 0x00, METADATA_SIZE);
		stMetaData.keyValList = stCcyDefLst;
		stMetaData.iMandCnt = 1;
		stMetaData.iTotCnt = 1;

		rv = parseXMLFile(szCcyDefFile, "ISO_4217", "CcyTbl", &stMetaData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse ISO4217 XML file",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* The records in the xml file can be in any order, but while storing
		 * them in application internal file, they need to be in a sequence
		 * according to their alphabetic currency code. Thus creating a binary
		 * tree of the records in order to sort them out and ignore any
		 * duplicate records, which might be there in the xml file */
		rv = createCcyTbl(stCcyDefLst[0].value, &stCcyTbl);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create CCY record list",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Check for any currency code-symbol mapping file */
		if(doesFileExist(CCY_MAP_FILE) == SUCCESS)
		{
			/* A currency mapping file exists in the system. Get the currency
			 * symbols and store them in the binary tree which will be then
			 * stored in the internal file of the application */
			rv = loadCcySymbols(CCY_MAP_FILE, &stCcyTbl);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to map ccy symbols",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			bMapFileExists = PAAS_TRUE;
		}

		/* Store the latest currency definitions into the internal file */
		rv = storeCcyDefs(&stCcyTbl);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to store ccy definitions",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Delete the files which are not required now */
		deleteFile(szCcyDefFile);
		if(bMapFileExists == PAAS_TRUE)
		{
			deleteFile(CCY_MAP_FILE);
		}

		break;
	}

	/* Free any allocated memory in meta data */
	freeMetaData(&stMetaData);
	flushCcyBinTree(stCcyTbl.pstRoot);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadCcyTbl
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int loadCcyTbl(char * szFileName)
{
	int				rv				= SUCCESS;
	FILE *			fp				= NULL;
	PAAS_BOOL		bMapFileExists	= PAAS_FALSE;
	CCYTBL_STYPE	stCcyTbl;
	CCYREC_STYPE	stTmpRec;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Open the file for reading */
		fp = fopen(CCY_TBL_FILE, "rb");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to open file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(&stCcyTbl, 0x00, sizeof(CCYTBL_STYPE));

		/* Read the records one by one */
		while(!feof(fp))
		{
			memset(&stTmpRec, 0x00, sizeof(stTmpRec));

			rv = fread(&stTmpRec, sizeof(stTmpRec), 1, fp);
			if(rv == 1)
			{
				rv = SUCCESS;

				rv = addCcyRecToTbl(&stTmpRec, &stCcyTbl);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to add rec to table",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}
			}
			else if(ferror(fp))
			{
				debug_sprintf(szDbgMsg, "%s: Error while reading from file",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		if(rv != SUCCESS)
		{
			/* Do not proceed further if met with some fatal error */
			break;
		}

		/* Check for any currency code-symbol mapping file */
		if(doesFileExist(CCY_MAP_FILE) == SUCCESS)
		{
			/* A currency mapping file exists in the system. Get the currency
			 * symbols and store them in the binary tree which will be then
			 * stored in the internal file of the application */
			rv = loadCcySymbols(CCY_MAP_FILE, &stCcyTbl);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to map ccy symbols",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			bMapFileExists = PAAS_TRUE;
		}

		/* Store the latest currency definitions into the internal file */
		rv = storeCcyDefs(&stCcyTbl);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to store ccy definitions",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Delete the files which are not required now */
		if(bMapFileExists == PAAS_TRUE)
		{
			deleteFile(CCY_MAP_FILE);
		}

		break;
	}

	if(fp != NULL)
	{
		fclose(fp);
	}

	flushCcyBinTree(stCcyTbl.pstRoot);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadCcySymbols
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int loadCcySymbols(char * szMapFileName, CCYTBL_PTYPE pstCcyTbl)
{
	int		rv				= SUCCESS;
	char	szCode[10]		= "";
	char	szSym[10]		= "";
	FILE *	fp				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		fp = fopen(szMapFileName, "r");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to open file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* File has been opened */
		while(!feof(fp))
		{
			rv = getNxtCodeNSymbolPair(fp, szCode, szSym);
			if(rv == SUCCESS)
			{
				rv = updateCcySymbol(pstCcyTbl->pstRoot, szCode, szSym);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to update for [%s:%s]",
												__FUNCTION__, szCode, szSym);
					APP_TRACE(szDbgMsg);

					break;
				}
			}
			else if(ferror(fp))
			{
				debug_sprintf(szDbgMsg, "%s: Error while reading from file",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		break;
	}

	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getNxtCodeNSymbolPair
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getNxtCodeNSymbolPair(FILE * fp, char * szCode, char * szSym)
{
	int		rv				= SUCCESS;
	char *	cPtr			= NULL;
	char *	cNxtPtr			= NULL;
	char	szTmp[25]		= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(NULL != fgets(szTmp, sizeof(szTmp) - 1, fp))
	{
		cPtr = strrchr(szTmp, '\n');
		if(cPtr != NULL)
		{
			cPtr = '\0';
		}

		cPtr = strrchr(szTmp, '\r');
		if(cPtr != NULL)
		{
			cPtr = '\0';
		}

		cPtr = strchr(szTmp, '=');
		memcpy(szCode, szTmp, cPtr - szTmp);

		cNxtPtr = strchr(cPtr, '\0');
		memcpy(szSym, cPtr + 1, cNxtPtr - cPtr - 1);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to read ahead", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateCcySymbol
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int updateCcySymbol(CCYREC_PTYPE pstCurNode, char * szCode, char * szSym)
{
	int		rv				= SUCCESS;
	int		iVal			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iVal = strcmp(pstCurNode->szCcy, szCode);
	if(iVal == 0)
	{
		/* Node found in the binary tree, update the node with the currency
		 * symbol provided */
		strcpy(pstCurNode->szSym, szSym);
	}
	else if(iVal > 0)
	{
		/* The currency code is lesser than that of current node, Check the left
		 * sub-tree */
		if(pstCurNode->left == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No CCY rec for [%s]", __FUNCTION__,
																		szCode);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
		else
		{
			rv = updateCcySymbol(pstCurNode->left, szCode, szSym);
		}
	}
	else if(iVal < 0)
	{
		/* The currency code is greater than that of current node, Check the
		 * right sub-tree */
		if(pstCurNode->right == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No CCY rec for [%s]", __FUNCTION__,
																		szCode);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
		else
		{
			rv = updateCcySymbol(pstCurNode->right, szCode, szSym);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeCcyDefs
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeCcyDefs(CCYTBL_PTYPE pstCcyTbl)
{
	int		rv				= SUCCESS;
	char	szCmd[100]		= "";
	FILE *	fp				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Open a temporary file for writing */
		fp = fopen("/tmp/CCYDEF_TMP.dat", "wb");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to open temp file for writing",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Store the data in increasing order */
		storeDataInOrder(pstCcyTbl->pstRoot, fp);

		fclose(fp);

		sprintf(szCmd, "mv /tmp/CCYDEF_TMP.dat %s", CCY_TBL_FILE);
		svcSystem(szCmd);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeDataInOrder
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void storeDataInOrder(CCYREC_PTYPE pstRec, FILE * fp)
{
	/* Store the left sub-tree */
	if(pstRec->left != NULL)
	{
		storeDataInOrder(pstRec->left, fp);
	}

	/* Store the current node */
	fwrite(pstRec, sizeof(CCYREC_STYPE), 1, fp);

	/* Store the right sub-tree */
	if(pstRec->right != NULL)
	{
		storeDataInOrder(pstRec->right, fp);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: createCcyTbl
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int createCcyTbl(VAL_LST_PTYPE ccyList, CCYTBL_PTYPE pstCcyTbl)
{
	int				rv				= SUCCESS;
	VAL_NODE_PTYPE	pstNode			= NULL;
	CCYREC_STYPE	stCcyRec;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(pstCcyTbl, 0x00, sizeof(CCYTBL_STYPE));
	
		pstNode = ccyList->start;
		if(pstNode == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No ccy def recs found", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		while(pstNode != NULL)
		{
			/* Create a CCY definition record with the definition details */
			memset(&stCcyRec, 0x00, sizeof(stCcyRec));
			rv = populateCcyRec(pstNode->elemCnt, pstNode->elemList, &stCcyRec);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to create ccy def record",
											__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			/* Add the currency definition record to the records list/table */
			rv = addCcyRecToTbl(&stCcyRec, pstCcyTbl);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to add rec to table",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}

		if(rv != SUCCESS)
		{
			/* Flush the ccy records table (binary tree) */
			flushCcyBinTree(pstCcyTbl->pstRoot);
			memset(pstCcyTbl, 0x00, sizeof(CCYTBL_STYPE));
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: populateCcyRec
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int populateCcyRec(int iTot, KEYVAL_PTYPE defLst, CCYREC_PTYPE pstRec)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	void *			val				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < iTot; iCnt++)
	{
		val = defLst[iCnt].value;

		if(val == NULL)
		{
			continue;
		}
		else if(strcmp(defLst[iCnt].key, "Ccy") == SUCCESS)
		{
			/* Alphabetic currency code */
			memcpy(pstRec->szCcy, val, strlen(val));
		}
		else if(strcmp(defLst[iCnt].key, "CcyNbr") == SUCCESS)
		{
			/* Numeric currency code */
			pstRec->iCcyNo = atoi((char *) val);
		}
		else if(strcmp(defLst[iCnt].key, "CcyMnrUnts") == SUCCESS)
		{
			/* Currency precision/exponent */
			pstRec->sExp = atoi((char *) val);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addCcyRecToTbl
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addCcyRecToTbl(CCYREC_PTYPE pstRec, CCYTBL_PTYPE pstTree)
{
	int				rv				= SUCCESS;
	CCYREC_PTYPE	pstTmp			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		pstTmp = (CCYREC_PTYPE) malloc(sizeof(CCYREC_STYPE));
		if(pstTmp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pstTmp, 0x00, sizeof(CCYREC_STYPE));
		memcpy(pstTmp, pstRec, sizeof(CCYREC_STYPE));

		/* Add the data in the binary tree */
		if(pstTree->pstRoot == NULL)
		{
			/* Create the tree starting with the root node */
			pstTree->pstRoot = pstTmp;
		}
		else
		{
			/* Tree created, add the record as a leaf node to the binary tree */
			addRecToBinTree(pstTree->pstRoot, pstTmp);
		}

		pstTree->iTotRec++;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addRecToBinTree
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void addRecToBinTree(CCYREC_PTYPE pstCurNode, CCYREC_PTYPE pstLeafNode)
{
	int		iVal 	= 0;

	iVal = strcmp(pstCurNode->szCcy, pstLeafNode->szCcy);
	if(iVal == 0)
	{
		/* Record already present, Ignore it (de-allocate the memory of leaf) */
		free(pstLeafNode);
	}
	else if(iVal > 0)
	{
		/* Add to the left sub-tree */
		if(pstCurNode->left == NULL)
		{
			pstCurNode->left = pstLeafNode;
		}
		else
		{
			addRecToBinTree(pstCurNode->left, pstLeafNode);
		}
	}
	else if(iVal < 0)
	{
		/* Add to the right sub-tree */
		if(pstCurNode->right == NULL)
		{
			pstCurNode->right = pstLeafNode;
		}
		else
		{
			addRecToBinTree(pstCurNode->right, pstLeafNode);
		}
	}

	return;
}

/*
 * ============================================================================
 * Function Name: flushCcyBinTree
 *
 * Description	: This function is responsible for flushing out the currency
 * 					list in the memory (list is implemented as a binary tree)
 *
 * Input Params	: pstRec -> the current node of the binary sub-tree
 *
 * Output Params: none
 * ============================================================================
 */
static void flushCcyBinTree(CCYREC_PTYPE pstRec)
{
	/* Flush the left sub-tree */
	if(pstRec->left != NULL)
	{
		flushCcyBinTree(pstRec->left);
	}

	/* Flush the right sub-tree */
	if(pstRec->right != NULL)
	{
		flushCcyBinTree(pstRec->right);
	}

	/* De-allocate the current node */
	memset(pstRec, 0x00, sizeof(CCYREC_STYPE));
	free(pstRec);

	return;
}

#ifdef DEBUG
/*
 * ============================================================================
 * Function Name: printCcyTbl
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void printCcyTbl(CCYREC_PTYPE pstCcyRec)
{
	if(pstCcyRec->left != NULL)
	{
		printCcyTbl(pstCcyRec->left);
	}

	printCcyRecDtls(pstCcyRec);

	if(pstCcyRec->right != NULL)
	{
		printCcyTbl(pstCcyRec->right);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: printCcyRecDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void printCcyRecDtls(CCYREC_PTYPE pstCcyRec)
{
	char	szDbgMsg[256]	= "";

	APP_TRACE("-------------------------------------");

	debug_sprintf(szDbgMsg, "%s: Currency Code (Alpha) - [%s]", __FUNCTION__,
															pstCcyRec->szCcy);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Currency Code (Numer) - [%d]", __FUNCTION__,
															pstCcyRec->iCcyNo);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Currency Symbol       - [%s]", __FUNCTION__,
															pstCcyRec->szSym);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Currency Exponent     - [%d]", __FUNCTION__,
															pstCcyRec->sExp);
	APP_TRACE(szDbgMsg);

	APP_TRACE("-------------------------------------");

	return;
}
#endif

/*
 * ============================================================================
 * Function Name: 
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */

/*
 * ============================================================================
 * End of file config.c
 * ============================================================================
 */
