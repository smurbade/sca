/******************************************************************
*                       currency.c                                *
*******************************************************************
* Application: Point Solutions                                    *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "psAmtExt.h"
#include "common.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

/* Static functions declarations - BEGIN */
static int validateAmtStr(char *, CRNCY_PTYPE);
/* Static functions declarations - END */


/* This function is to be called only when the xml file is present */

/*
 * ============================================================================
 * Function Name: stripAmtStrForCrncy
 *
 * Description	: This function is responsible for stripping off an amount
 * 					string from any currency characters or characters like
 * 					thousands separator etc.
 *
 * Input Params	:	szAmt    -> amount string to be stripped
 * 					pstCrncy -> currency to be used as a reference
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int stripAmtStrForCrncy(char * szAmt, CRNCY_PTYPE pstCrncy)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	int		iCnt			= 0;
	int		jCnt			= 0;
	char *	cTmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for the params */
		if((szAmt == NULL) || ((iLen=strlen(szAmt)) == 0) || (pstCrncy == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Validate the amount string against the currency */
		rv = validateAmtStr(szAmt, pstCrncy);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to validate amt", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Create a temporary place holder for the modified string */
		cTmpPtr = (char *) malloc(iLen + 1);
		if(cTmpPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(cTmpPtr, 0x00, iLen + 1);

		/* Start the Stripping process for the amount string */
		for(iCnt = 0; iCnt < iLen; iCnt++)
		{
			switch(szAmt[iCnt])
			{
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				/* A numeric character, must be part of the amount */
				cTmpPtr[jCnt++] = szAmt[iCnt];
				break;

			default:
				/* Not a numeric character, if the character is a decimal
				 * separator, then also copy it in the result amount string */
				if(szAmt[iCnt] == pstCrncy->cDecSep)
				{
					cTmpPtr[jCnt++] = szAmt[iCnt];
				}

				break;
			}
		}

		memset(szAmt, 0x00, iLen);
		strcpy(szAmt, cTmpPtr);

		break;
	}

	/* De-allocating any allocated memory */
	if(cTmpPtr != NULL)
	{
		free(cTmpPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: compareCrncys
 *
 * Description	: This function is used to compare the currencies of two
 * 					amounts, the currencies being passed as parameters for the
 * 					same.
 *
 * Input Params	: 	pstCrncy1 -> first currency details
 * 					pstCrncy2 -> second currency details
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int compareCrncys(CRNCY_PTYPE pstCrncy1, CRNCY_PTYPE pstCrncy2)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if((pstCrncy1 == pstCrncy2) ||
					(memcmp(pstCrncy1, pstCrncy2, CRNCY_SIZE) == SUCCESS) )
	{
		debug_sprintf(szDbgMsg, "%s: Currencies are same", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Currencies are different", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: validateAmtStr
 *
 * Description	: This function is responsible for validation the amount string
 * 					assuming no null or empty params are being passed
 *
 * Input Params	:	szAmt    -> amount string to be validated
 * 					pstCrncy -> currency to be used as a reference
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int validateAmtStr(char * szAmt, CRNCY_PTYPE pstCrncy)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	char	szSpan[20]		= "";
	char *	cCurPtr			= NULL;
	char *	cNxtPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iLen = strlen(szAmt);

		/* Check for the characters that the amount string can have to be valid
		 * (before being stripped off) */
		sprintf(szSpan, "0123456789%c%c%s", pstCrncy->cDecSep, pstCrncy->cThSep,
															pstCrncy->szSym);

		/* VDR: TODO - Remove this later */
		debug_sprintf(szDbgMsg, "%s: Span String = [%s]", __FUNCTION__, szSpan);
		APP_TRACE(szDbgMsg);

		if(strspn(szAmt, szSpan) != iLen)
		{
			debug_sprintf(szDbgMsg, "%s: Extra characters in amount string [%s]"
														, __FUNCTION__, szAmt);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Validate that the amount string has only one decimal separator */
		cCurPtr = strchr(szAmt, pstCrncy->cDecSep);
		if(cCurPtr != NULL)
		{
			if(cCurPtr + 1 == '\0')
			{
				debug_sprintf(szDbgMsg, "%s: Invalid amount string = [%s]",
														__FUNCTION__, szAmt);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			cNxtPtr = strchr(cCurPtr + 1, pstCrncy->cDecSep);
			if(cNxtPtr != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Extra decimal char in amt string",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			/* Check for the precision */
			cNxtPtr = strchr(cCurPtr, '\0');
			if((cNxtPtr - cCurPtr) != pstCrncy->iPrec)
			{
				debug_sprintf(szDbgMsg, "%s: Incorrect Precision of amt string",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		/* Validate for the currency symbol */
		cCurPtr = strstr(szAmt, pstCrncy->szSym);
		if(cCurPtr != NULL)
		{
			if(cCurPtr != szAmt)
			{
				debug_sprintf(szDbgMsg,"%s: Amt str not begining with crncy sym"
																, __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			cNxtPtr = strstr(cCurPtr + strlen(pstCrncy->szSym),pstCrncy->szSym);
			if(cNxtPtr != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Extra currency symbols in amt str",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * End of file currency.c
 * ============================================================================
 */
