/*
 * =============================================================================
 * Filename    : tranDS2.c
 *
 * Application : Mx Point SCA
 *
 * Description : This file contains APIs to update data int the transaction
 * 					instances
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common/common.h"
#include "db/dataSystem.h"
#include "db/tranDS.h"
#include "common/utils.h"
#include "appLog/appLogAPIs.h"
#include "uiAgent/emvAPIs.h"


//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);


/* Static functions declarations */
static int fillRespDtls(char *, int, RESPDTLS_PTYPE);

/*
 * ============================================================================
 * Function Name: setFxnNCmdForTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int setFxnNCmdForTran(char * szTranKey, int iFxn, int iCmd)
{
	int			rv				= SUCCESS;
	TRAN_PTYPE	pstTran			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		pstTran->iFxn = iFxn;
		pstTran->iCmd = iCmd;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setStatusInTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int setStatusInTran(char * szTranKey, int iStatus)
{
	int			rv				= SUCCESS;
	TRAN_PTYPE	pstTran			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		pstTran->iStatus = iStatus;

		debug_sprintf(szDbgMsg, "%s: Status set to %d", __FUNCTION__, iStatus);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setGenRespDtlsinTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int setGenRespDtlsinTran(char * szTranKey, char * szMsg)
{
	int			rv					= SUCCESS;
	//char		szSSITranKey[10]	= "";
	TRAN_PTYPE	pstTran				= NULL;
	//TRAN_PTYPE	pstSTran            = NULL;
#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		/* Fill the response details */
		fillRespDtls(szMsg, pstTran->iStatus, &(pstTran->stRespDtls));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateGenSessDtls
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int updateGenSessDtls(char * szSessKey, SESSDTLS_PTYPE pstSessDtls)
{
	int				rv				= SUCCESS;
	SESSDTLS_PTYPE	pstSess			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (szSessKey == NULL) || (pstSessDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getSession(szSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/*Copying the initialized data
		 *
		 */
		memcpy(pstSessDtls->szStartTS, pstSess->szStartTS, sizeof(pstSess->szStartTS));
		memcpy(&(pstSessDtls->startTime), &(pstSess->startTime), sizeof(time_t));
		memcpy(pstSess, pstSessDtls, sizeof(SESSDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillPymntDtlsInSession
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
static int fillPymntDtlsInSession(char* szTranKey, PYMNTINFO_PTYPE pstPYMNTInfo, int type)
{
	int			 		rv			= SUCCESS;
	CARDDTLS_STYPE		stCardDtls;
	AMTDTLS_STYPE 		stAmtDtls;
	CTRANDTLS_STYPE		stCTranDtls;
	LTYDTLS_STYPE		stLtyDtls;
	PYMNT_NODE_PTYPE	pstPymtNode = NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstPymtNode = (PYMNT_NODE_PTYPE)malloc(sizeof(PYMNT_NODE_STYPE));
	if(pstPymtNode == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv; //CID 67392, 67200 (#1 of 1): Dereference after null check (FORWARD_NULL). T_RaghavendranR1
	}
	memset(pstPymtNode, 0x00, sizeof(PYMNT_NODE_STYPE));
	pstPymtNode->type = type;

	rv = getCardDtlsForPymtTran(szTranKey, &stCardDtls);
	if (rv == SUCCESS)
	{
		strcpy(pstPymtNode->szPymtMedia, stCardDtls.szPymtMedia);
		strcpy(pstPymtNode->szPAN, stCardDtls.szPAN);
		strcpy(pstPymtNode->szName, stCardDtls.szName);
	}

	rv = getAmtDtlsForPymtTran(szTranKey, &stAmtDtls);
	if (rv == SUCCESS)
	{
		strcpy(pstPymtNode->apprvdAmt, stAmtDtls.tranAmt);
	}

	rv = getCurTranDtlsForPymt(szTranKey, &stCTranDtls);
	if (rv == SUCCESS)
	{
		strcpy(pstPymtNode->szAuthCode, stCTranDtls.szAuthCode);
		strcpy(pstPymtNode->szLPToken, stCTranDtls.szLPToken);
		strcpy(pstPymtNode->szCTroutd, stCTranDtls.szCTroutd);
	}

	if (pstPYMNTInfo->mainLstTail == NULL)
	{
		pstPYMNTInfo->mainLstHead = pstPymtNode;
		pstPYMNTInfo->mainLstTail = pstPymtNode;
	}
	else
	{
		pstPYMNTInfo->mainLstTail->next = pstPymtNode;
		pstPYMNTInfo->mainLstTail		= pstPymtNode;
	}
	pstPYMNTInfo->count++;

	if (strlen(pstPYMNTInfo->szShopToken) == 0)
	{
		getLtyDtlsForPymtTran(szTranKey, &stLtyDtls);

		if(strlen(stLtyDtls.szPAN) > 0)
		{
			/*
			* If less than 15 digits send it as is,
			* if more than 15 digits asterisk mask all but last four
			*/
			if(strlen(stLtyDtls.szPAN) < 15)
			{
				strcpy(pstPYMNTInfo->szShopToken, stLtyDtls.szPAN);
			}
			else
			{
				getMaskedPANForBAPI(stLtyDtls.szPAN, pstPYMNTInfo->szShopToken);
			}
		}
		else if (strlen(stLtyDtls.szPh) > 0)
		{
			strcpy(pstPYMNTInfo->szShopToken, stLtyDtls.szPh);
		}
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updatePymntDtlsForBAPI
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updatePymntDtlsForBAPI(char* szSessKey, char* szTranKey, int type)
{
	int				 rv					= SUCCESS;
	char			 szTranStkKey[10]	= "";
	TRAN_PTYPE		 pstTran			= NULL;
	SESSDTLS_PTYPE	 pstSess			= NULL;
#ifdef DEBUG
	char			 szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szSessKey == NULL) || (szTranKey == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getSession(szSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv  = FAILURE;
		}
		pstSess->isBAPIReq = PAAS_TRUE;

		if(pstSess->pstPYMNTInfo == NULL)
		{
			pstSess->pstPYMNTInfo = (PYMNTINFO_PTYPE) malloc(sizeof(PYMNTINFO_STYPE));
			if(pstSess->pstPYMNTInfo == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstSess->pstPYMNTInfo, 0x00, sizeof(PYMNTINFO_STYPE));
		}


		/* Get the reference to the transaction placeholder */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get access to new tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
		strcpy(szTranStkKey, (char *)pstTran->data); //Getting the SSI stack key

		rv = fillPymntDtlsInSession(szTranStkKey, pstSess->pstPYMNTInfo, type);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill the payment dtls in session",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateLIDtlsInSession
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateLIDtlsInSession(char * szSessKey, LIINFO_PTYPE pstLIInfo)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	SESSDTLS_PTYPE	pstSess				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szSessKey == NULL) || (pstLIInfo == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getSession(szSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstSess->pstLIInfo == NULL)
		{
			pstSess->pstLIInfo = (LIINFO_PTYPE) malloc(sizeof(LIINFO_STYPE));
			if(pstSess->pstLIInfo == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		memcpy(pstSess->pstLIInfo, pstLIInfo, sizeof(LIINFO_STYPE));
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Line Item Details Updated Successfully in the Session");
			addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setFxnNCmdForSSITran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int setFxnNCmdForSSITran(char * szTranKey, int iFxn, int iCmd)
{
	int			rv				= SUCCESS;
	TRAN_PTYPE	pstTran			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Setting Funtion = %d, Command = %d", __FUNCTION__, iFxn, iCmd);
		APP_TRACE(szDbgMsg);

		/* get the function and command */
		pstTran->iFxn = iFxn;
		pstTran->iCmd = iCmd;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setFxnForSSITran
 *
 * Description	: This Function Sets the Function Type for the SSI Transaction Alone. This is Required to set the Payment Function Type, so that even if
 * validation of the Payment Transaction fails, we free the stored Payment Structures.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int setFxnForSSITran(char * szTranKey, int iFxn)
{
	int			rv					= SUCCESS;
	char		szTranStkKey[10]	= "";
	TRAN_PTYPE	pstTran				= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the reference to the transaction placeholder */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get access to new tran",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
		strcpy(szTranStkKey, (char *)pstTran->data); //Getting the SSI stack key

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranStkKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Setting Funtion = %d", __FUNCTION__, iFxn);
		APP_TRACE(szDbgMsg);

		/* set the function*/
		pstTran->iFxn = iFxn;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: updatePOSSecDtlsForTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updatePOSSecDtlsForTran(char * szTranKey, POSSEC_PTYPE pstPOSSec)
{
	int			rv				= SUCCESS;
	TRAN_PTYPE	pstTran			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstPOSSec == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data == NULL)
		{
			pstTran->data = (POSSEC_PTYPE) malloc(sizeof(POSSEC_STYPE));
			if(pstTran->data == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstTran->data, 0x00, sizeof(POSSEC_STYPE));
		}

		memcpy(pstTran->data, pstPOSSec, sizeof(POSSEC_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateDevNameDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateDevNameDtlsForDevTran(char * szTranKey, DEVICENAME_PTYPE pstDevNameDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstDevNameDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stDevNameDtls), pstDevNameDtls, sizeof(DEVICENAME_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateDispMsgDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateDispMsgDtlsForDevTran(char * pszTranKey, 	DISPMSG_PTYPE pstDispMsgDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((pszTranKey == NULL) || (pstDispMsgDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(pszTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stDispMsgDtls), pstDispMsgDtls, sizeof(DISPMSG_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: updateGetParmDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateGetParmDtlsForDevTran(char * pszTranKey, 	GETPARM_PTYPE pstGetParmDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((pszTranKey == NULL) || (pstGetParmDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(pszTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stGetParmDtls), pstGetParmDtls, sizeof(GETPARM_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: updateGetPymtTypesDtlsForDevTran
 *
 * Description	: This Function updates the enabled Payment Types in configuration to
 * Device Transaction structure
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateGetPymtTypesDtlsForDevTran(char * szTranKey, PAYMENTTYPES_PTYPE pstPymtTypesDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstPymtTypesDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stPymtTypeDtls), pstPymtTypesDtls, sizeof(PAYMENTTYPES_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*KranthiK1: TODO: Remove it Later*/
#if 0
/*
 * ============================================================================
 * Function Name: updateCounterDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateCounterDtlsForDevTran(char * szTranKey, COUNTERDTLS_PTYPE pstCounterDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstCounterDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stCounterDtls), pstCounterDtls, sizeof(COUNTERDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: updateTimeDtlsForDevTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateTimeDtlsForDevTran(char * szTranKey, TIMEDTLS_PTYPE pstTimeDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstTimeDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stTimeDtls), pstTimeDtls, sizeof(TIMEDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateSigDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateSigDtlsForDevTran(char * szTranKey, SIGDTLS_PTYPE pstSigDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstSigDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stSigDtls), pstSigDtls, sizeof(SIGDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateLtyDtlsForDevTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateLtyDtlsForDevTran(char * szTranKey, LTYDTLS_PTYPE pstLty)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstLty == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stLtyDtls), pstLty, sizeof(LTYDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateVASDtlsForDevTran
 *
 * Description	: This Function will Upadate The VAS details On to the Device Transaction 
 * 				  by using the transation key
 *
 * Input Params	: Transaction key,structure containing the VAS Details 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateVASDtlsForDevTran(char * szTranKey, VASDATA_DTLS_PTYPE pstVasDataDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstVasDataDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stVasDataDtls), pstVasDataDtls, sizeof(VASDATA_DTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateLtyDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateVerDtlsForDevTran(char * szTranKey, VERDTLS_PTYPE pstVer)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstVer == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stVerDtls), pstVer, sizeof(VERDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateSurveyDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateSurveyDtlsForDevTran(char * szTranKey, SURVEYDTLS_PTYPE pstSurvey)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstSurvey == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stSurveyDtls), pstSurvey, sizeof(SURVEYDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateEmpIDDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateEmpIDDtlsForDevTran(char * szTranKey, EMPID_PTYPE pstEmpIDDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstEmpIDDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stEmpIDDtls), pstEmpIDDtls, sizeof(EMPID_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: updateCharityDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateCharityDtlsForDevTran(char * szTranKey, CHARITYDTLS_PTYPE pstCharity)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstCharity == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stCharityDtls), pstCharity, sizeof(CHARITYDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ===================================================================================
 * Function Name: updateGetCardDataDtlsForDevTran
 *
 * Description	:This function will be used to update the GET_CARDDATA_PTYPE structure
 *
 * Input Params	:Transaction Key, GET_CARDDATA_PTYPE structure.
 *
 * Output Params: SUCCESS/ FAILURE
 * ===================================================================================
 */
int updateGetCardDataDtlsForDevTran(char * szTranKey, GET_CARDDATA_PTYPE pstGetCardData)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstGetCardData == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stGetCardDataDtls), pstGetCardData, sizeof(GET_CARDDATA_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateCreditAppPromptDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateCreditAppPromptDtlsForDevTran(char * szTranKey, CREDITAPPDTLS_PTYPE pstCreditAppDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstCreditAppDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stCreditAppDtls), pstCreditAppDtls, sizeof(CREDITAPPDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateCustButtonDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateCustButtonDtlsForDevTran(char * szTranKey, CUSTBUTTONDTLS_PTYPE pstCustButton)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstCustButton == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stCustButtonDtls), pstCustButton, sizeof(CUSTBUTTONDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateSAFDtlsForDevTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateSAFDtlsForDevTran(char * szTranKey, SAFDTLS_PTYPE pstSAFDtls)
{
	int				rv				= SUCCESS;
	SAFDTLS_PTYPE	pstLocSAF		= NULL;
	TRAN_PTYPE		pstTran			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstSAFDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		pstLocSAF = (SAFDTLS_PTYPE) pstTran->data;
		if(pstLocSAF == NULL)
		{
			pstLocSAF = (SAFDTLS_PTYPE) malloc(sizeof(SAFDTLS_STYPE));
			if(pstLocSAF == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstLocSAF, 0x00, sizeof(SAFDTLS_STYPE));
			pstTran->data = pstLocSAF;
		}

		memcpy(pstLocSAF, pstSAFDtls, sizeof(SAFDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updatePymtTypeForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updatePymtTypeForPymtTran(char * szTranKey, int iPymtType)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstPymtDtls->iPymtType = iPymtType;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updatePymtSubTypeForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updatePymtSubTypeForPymtTran(char * szTranKey, int iPymtSubType)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstPymtDtls->iPymtSubType = iPymtSubType;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updatePrivCrdSAFTranIndForPymtTran
 *
 * Description	: This function sets/resets the SAF transaction indicator flag for private card.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updatePrivCrdSAFTranIndForPymtTran(char * szTranKey, PAAS_BOOL bPrvCrdSAFAllwd)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstPymtDtls->bPrvCrdSAFAllwd = bPrvCrdSAFAllwd;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateSafTranIndicatorForPymtTran
 *
 * Description	: This function sets/resets the SAF transaction indicator flag for transaction.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateSAFTranIndicatorForPymtTran(char * szTranKey, PAAS_BOOL bSAFTranFlg)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstPymtDtls->bSAFTran = bSAFTranFlg;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updatePaypalCheckIdKeyForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updatePaypalCheckIdKeyForPymtTran(char * pszTranKey, char * pszCheckIdKey)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((pszTranKey == NULL) || (pszCheckIdKey == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(pszTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		if(strlen(pszCheckIdKey))
		{
			memset(pstPymtDtls->szCheckIdKey, 0x00, sizeof(pstPymtDtls->szCheckIdKey));
			strcpy(pstPymtDtls->szCheckIdKey, pszCheckIdKey);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updatePaypalPymtCodeForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updatePaypalPymtCodeForPymtTran(char * pszTranKey, char * pszPaymentCode)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	CARDDTLS_PTYPE	pstLocCardDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((pszTranKey == NULL) || (pszPaymentCode == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(pszTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocCardDtls = pstPymtDtls->pstCardDtls;
		if(pstLocCardDtls == NULL)
		{
			pstLocCardDtls = (CARDDTLS_PTYPE) malloc(sizeof(CARDDTLS_STYPE));
			if(pstLocCardDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstLocCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
			pstPymtDtls->pstCardDtls = pstLocCardDtls;
		}

		if(strlen(pszPaymentCode))
		{
			memset(pstPymtDtls->pstCardDtls->szPayPalPymtCode, 0x00, sizeof(pstPymtDtls->pstCardDtls->szPayPalPymtCode));
			strcpy(pstPymtDtls->pstCardDtls->szPayPalPymtCode, pszPaymentCode);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updatePaymntFlowIndicForPymtTran
 *
 * Description	: This function sets/resets payment flow chosen for this transaction.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updatePaymntFlowIndicForPymtTran(char * szTranKey, PAAS_BOOL bRegPymtFlow)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstPymtDtls->bRegPymtFlow = bRegPymtFlow;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateVSPTranIndicatorForPymtTran
 *
 * Description	: This function sets/resets the VSP transaction indicator flag for transaction.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateVSPTranIndicatorForPymtTran(char * szTranKey, PAAS_BOOL bVSPTranFlg)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstPymtDtls->bVSPTran = bVSPTranFlg;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateCardTypeForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateCardTypeForPymtTran(char * szTranKey, char * szCardType)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (szCardType == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		memset(pstPymtDtls->szCardType, 0x00, sizeof(pstPymtDtls->szCardType));
		strcpy(pstPymtDtls->szCardType, szCardType);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateTranDateForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateTranDateForPymtTran(char * szTranKey, char * szDate)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (szDate == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		memset(pstPymtDtls->szTranDt, 0x00, sizeof(pstPymtDtls->szTranDt));
		strcpy(pstPymtDtls->szTranDt, szDate);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateTranTimeForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateTranTimeForPymtTran(char * szTranKey, char * szTime)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (szTime == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		memset(pstPymtDtls->szTranTm, 0x00, sizeof(pstPymtDtls->szTranTm));
		strcpy(pstPymtDtls->szTranTm, szTime);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateForceFlagForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateForceFlagForPymtTran(char * szTranKey, PAAS_BOOL bForce)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstPymtDtls->bForce = bForce;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateBillPayForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateBillPayForPymtTran(char * szTranKey, PAAS_BOOL bBillPay)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstPymtDtls->bBillPay = bBillPay;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateBatchTraceIdForPymtTran
 *
 * Description	: This function updates the Batch Trace Id for Payment Instance
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateBatchTraceIdForPymtTran(char * szTranKey, char * pszBatchTraceID)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL || pszBatchTraceID == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		strcpy(pstPymtDtls->szBatchTraceId, pszBatchTraceID);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateFSADtlsForPymtTran
 *
 * Description	: This function updates the FSA Details for Payment Instance
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateFSADtlsForPymtTran(char * szTranKey, FSADTLS_PTYPE pstFSADtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	FSADTLS_PTYPE	pstLocFSADtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL || pstFSADtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocFSADtls = pstPymtDtls->pstFSADtls;
		if(pstLocFSADtls == NULL)
		{
			pstLocFSADtls = (FSADTLS_PTYPE) malloc(sizeof(FSADTLS_STYPE));
			if(pstLocFSADtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstLocFSADtls, 0x00, sizeof(FSADTLS_STYPE));
			pstPymtDtls->pstFSADtls = pstLocFSADtls;
		}

		memcpy(pstLocFSADtls, pstFSADtls, sizeof(FSADTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateEBTDtlsForPymtTran
 *
 * Description	: This function updates the EBT Details for Payment Instance
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateEBTDtlsForPymtTran(char * szTranKey, EBTDTLS_PTYPE pstEBTDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	EBTDTLS_PTYPE	pstLocEBTDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL || pstEBTDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocEBTDtls = pstPymtDtls->pstEBTDtls;
		if(pstLocEBTDtls == NULL)
		{
			pstLocEBTDtls = (EBTDTLS_PTYPE) malloc(sizeof(EBTDTLS_STYPE));
			if(pstLocEBTDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstLocEBTDtls, 0x00, sizeof(EBTDTLS_STYPE));
			pstPymtDtls->pstEBTDtls = pstLocEBTDtls;
		}

		memcpy(pstLocEBTDtls, pstEBTDtls, sizeof(EBTDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateManualFlagForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateManualFlagForPymtTran(char * szTranKey, PAAS_BOOL bMan)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstPymtDtls->bManEntry = bMan;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateSplitTndrFlagForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateSplitTndrFlagForPymtTran(char * szTranKey, PAAS_BOOL bSplit)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstPymtDtls->bSplitTender = bSplit;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addSessionToPymtTran
 *
 * Description	:
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int addSessionToPymtTran(char * szSessKey, char * szTranKey)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";
	TRAN_PTYPE		pstTran				= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
	SESSDTLS_PTYPE	pstSess				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (szSessKey == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		debug_sprintf(szDbgMsg, "%s: Got the payment details", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = getSession(szSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Session not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Session Not Present to Process the Transaction.");
				strcpy(szAppLogDiag, "Please Create the Session to Process the Transaction");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Got the session details", __FUNCTION__);
		APP_TRACE(szDbgMsg);


		if(pstPymtDtls->pstSess == NULL)
		{
			pstPymtDtls->pstSess = (SESSDTLS_PTYPE)malloc(sizeof(SESSDTLS_STYPE));
			if(pstPymtDtls->pstSess == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc failed for sess details in SSI tran instance", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls->pstSess, 0X00, sizeof(SESSDTLS_STYPE));
		}
		memcpy(pstPymtDtls->pstSess, pstSess, sizeof(SESSDTLS_STYPE));

		debug_sprintf(szDbgMsg, "%s: Copied session details to payment tran instance", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: updateSessDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateSessDtlsForPymtTran(char * szTranKey, SESSDTLS_PTYPE pstSessionpDtls)
{
	int				rv					= SUCCESS;
	TRAN_PTYPE		pstTran				= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
	SESSDTLS_PTYPE  pstLocSessionDtls   = NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstSessionpDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}
		pstLocSessionDtls = pstPymtDtls->pstSess;
		if(pstLocSessionDtls == NULL)
		{
			pstLocSessionDtls = (SESSDTLS_PTYPE) malloc(sizeof(SESSDTLS_STYPE));
			if(pstLocSessionDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstSess = pstLocSessionDtls;
		}

		memcpy(pstLocSessionDtls, pstSessionpDtls, sizeof(SESSDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: updateGenRespDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateGenRespDtlsForPymtTran(char * szTranKey, RESPDTLS_PTYPE pstGenRespDtls)
{
	int				rv					= SUCCESS;
	TRAN_PTYPE		pstTran				= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstGenRespDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		memset(&(pstTran->stRespDtls), 0x00, sizeof(RESPDTLS_STYPE));
		memcpy(&(pstTran->stRespDtls), pstGenRespDtls, sizeof(RESPDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateAmtDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateAmtDtlsForPymtTran(char * szTranKey, AMTDTLS_PTYPE pstAmtDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	AMTDTLS_PTYPE	pstLocAmtDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstAmtDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocAmtDtls = pstPymtDtls->pstAmtDtls;
		if(pstLocAmtDtls == NULL)
		{
			pstLocAmtDtls = (AMTDTLS_PTYPE) malloc(sizeof(AMTDTLS_STYPE));
			if(pstLocAmtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: MAlloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstAmtDtls = pstLocAmtDtls;
		}

		memcpy(pstLocAmtDtls, pstAmtDtls, sizeof(AMTDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: clearCashbackDtlsForUserCancel
 *
 * Description	: When Moving back to Tender Selection , it is essential to
 * 				  remove any previous Cashback Amount that is stored.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

int clearCashbackDtlsForUserCancel(char *szTranKey)
{
	int					rv					= SUCCESS;
	char				szTranStkKey[10]	= "";
	AMTDTLS_STYPE   	stAmountDtls;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Validate the parameters */
		if( szTranKey == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(szTranStkKey, 0x00, sizeof(szTranStkKey));
		strcpy(szTranStkKey, szTranKey); //Getting the SSI stack key
		memset(&stAmountDtls, 0x00, sizeof(AMTDTLS_STYPE));
		rv = getAmtDtlsForPymtTran(szTranStkKey, &stAmountDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get amount details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		/* If the User presses Cancel on the PIN entry Screen/Split Tender,
		 * then we need to flush the cashback related amount from the
		 * Amount Details.
		 */

		memset(stAmountDtls.cashBackAmt, 0x00, sizeof(stAmountDtls.cashBackAmt));
		if(SUCCESS != updateAmtDtlsForPymtTran(szTranStkKey, &stAmountDtls))
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to reset the amount dtls", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;

			break;
		}

		break;
	} //end of while

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: updateCardDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateCardDtlsForPymtTran(char * szTranKey, CARDDTLS_PTYPE pstCardDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	CARDDTLS_PTYPE	pstLocCardDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstCardDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present, allocating", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocCardDtls = pstPymtDtls->pstCardDtls;
		if(pstLocCardDtls == NULL)
		{
			pstLocCardDtls = (CARDDTLS_PTYPE) malloc(sizeof(CARDDTLS_STYPE));
			if(pstLocCardDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstCardDtls = pstLocCardDtls;
		}

		memcpy(pstLocCardDtls, pstCardDtls, sizeof(CARDDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateDccDtlsForPymtTran
 *
 * Description	: Adding the DCC Dtls to the pymnt instance
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateDccDtlsForPymtTran(char * szTranKey, DCCDTLS_PTYPE pstDccDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	DCCDTLS_PTYPE	pstLocDccDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstDccDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present, allocating", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocDccDtls = pstPymtDtls->pstDCCDtls;
		if(pstLocDccDtls == NULL)
		{
			pstLocDccDtls = (DCCDTLS_PTYPE) malloc(sizeof(DCCDTLS_STYPE));
			if(pstLocDccDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstDCCDtls = pstLocDccDtls;
		}

		memcpy(pstLocDccDtls, pstDccDtls, sizeof(DCCDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updatePassThrghDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updatePassThrghDtlsForPymtTran(char * szTranKey, PASSTHRG_FIELDS_PTYPE pstPassThrghDtls)
{
	int						rv					= SUCCESS;
	TRAN_PTYPE				pstTran				= NULL;
	PYMTTRAN_PTYPE			pstPymtDtls			= NULL;
	PASSTHRG_FIELDS_PTYPE	pstLocPassThrghDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstPassThrghDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present, allocating", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocPassThrghDtls = pstPymtDtls->pstPassThrghDtls;
		if(pstLocPassThrghDtls == NULL)
		{
			pstLocPassThrghDtls = (PASSTHRG_FIELDS_PTYPE) malloc(sizeof(PASSTHRG_FIELDS_STYPE));
			if(pstLocPassThrghDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstPassThrghDtls = pstLocPassThrghDtls;
		}

		memcpy(pstLocPassThrghDtls, pstPassThrghDtls, sizeof(PASSTHRG_FIELDS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: copyAndUpdatePassThrghDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int copyAndUpdatePassThrghDtlsForPymtTran(char * pszTranStkKey, PASSTHRG_FIELDS_PTYPE pstPassThrghDtls)
{
	int				rv						= SUCCESS;
	int				iCnt					= 0;
	int				iTotCnt					= 0;
	KEYVAL_PTYPE	pstLocKeyValPtr			= NULL;
	KEYVAL_PTYPE	pstRespLocKeyValPtr		= NULL;
	PASSTHRG_FIELDS_STYPE	stLocPassThrgDtls;
	#ifdef DEBUG
		char			szDbgMsg[256]	= "";
	#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	while(1)
	{
		if(pstPassThrghDtls->pstReqTagList == NULL || pstPassThrghDtls->iReqTagsCnt == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Pass through Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		memset(&stLocPassThrgDtls, 0x00, sizeof(PASSTHRG_FIELDS_STYPE));

		iTotCnt = pstPassThrghDtls->iReqTagsCnt;
		// allocate memory for this
		pstLocKeyValPtr = (KEYVAL_PTYPE)malloc(iTotCnt * KEYVAL_SIZE);
		if(pstLocKeyValPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		memset(pstLocKeyValPtr, 0x00, iTotCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(pstPassThrghDtls->pstReqTagList[iCnt].key != NULL)
			{
				pstLocKeyValPtr[iCnt].key = strdup(pstPassThrghDtls->pstReqTagList[iCnt].key);
			}
			if(pstPassThrghDtls->pstReqTagList[iCnt].value != NULL)
			{
				pstLocKeyValPtr[iCnt].value = strdup((char*)pstPassThrghDtls->pstReqTagList[iCnt].value);
			}
			if(pstPassThrghDtls->pstReqTagList[iCnt].valMetaInfo != NULL)
			{
				pstLocKeyValPtr[iCnt].valMetaInfo = pstPassThrghDtls->pstReqTagList[iCnt].valMetaInfo;
			}
			pstLocKeyValPtr[iCnt].valType = pstPassThrghDtls->pstReqTagList[iCnt].valType;
			pstLocKeyValPtr[iCnt].isMand = pstPassThrghDtls->pstReqTagList[iCnt].isMand;
		}

		stLocPassThrgDtls.iReqTagsCnt = iTotCnt;
		stLocPassThrgDtls.pstReqTagList = pstLocKeyValPtr;

		iTotCnt = pstPassThrghDtls->iResTagsCnt;
		// allocate memory for this
		pstRespLocKeyValPtr = (KEYVAL_PTYPE)malloc(iTotCnt * KEYVAL_SIZE);
		if(pstRespLocKeyValPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		memset(pstRespLocKeyValPtr, 0x00, iTotCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(pstPassThrghDtls->pstResTagList[iCnt].key != NULL)
			{
				pstRespLocKeyValPtr[iCnt].key = strdup(pstPassThrghDtls->pstResTagList[iCnt].key);
			}
			if(pstPassThrghDtls->pstResTagList[iCnt].value != NULL)
			{
				pstRespLocKeyValPtr[iCnt].value = strdup((char*)pstPassThrghDtls->pstResTagList[iCnt].value);
			}
			if(pstPassThrghDtls->pstResTagList[iCnt].valMetaInfo != NULL)
			{
				pstRespLocKeyValPtr[iCnt].valMetaInfo = pstPassThrghDtls->pstResTagList[iCnt].valMetaInfo;
			}
			pstRespLocKeyValPtr[iCnt].valType = pstPassThrghDtls->pstResTagList[iCnt].valType;
			pstRespLocKeyValPtr[iCnt].isMand = pstPassThrghDtls->pstResTagList[iCnt].isMand;
		}

		stLocPassThrgDtls.iResTagsCnt = iTotCnt;
		stLocPassThrgDtls.pstResTagList = pstRespLocKeyValPtr;

		updatePassThrghDtlsForPymtTran(pszTranStkKey, &stLocPassThrgDtls);

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: updatePOSTenderDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updatePOSTenderDtlsForPymtTran(char * szTranKey, POSTENDERDTLS_PTYPE pstPOSTenderDtls)
{
	int						rv					= SUCCESS;
	TRAN_PTYPE				pstTran				= NULL;
	PYMTTRAN_PTYPE			pstPymtDtls			= NULL;
	POSTENDERDTLS_PTYPE		pstLocPOSTndrDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstPOSTenderDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present, allocating", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocPOSTndrDtls = pstPymtDtls->pstPOSTenderDtls;
		if(pstLocPOSTndrDtls == NULL)
		{
			pstLocPOSTndrDtls = (POSTENDERDTLS_PTYPE) malloc(sizeof(POSTENDERDTLS_STYPE));
			if(pstLocPOSTndrDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstPOSTenderDtls = pstLocPOSTndrDtls;
		}

		memcpy(pstLocPOSTndrDtls, pstPOSTenderDtls, sizeof(POSTENDERDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: updateDevTranDtls
 *
 * Description	: This function helps in adding the dev tran details to the
 *                 ssi stack. 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateDevTranDtls(char * szTranKey, DEVTRAN_PTYPE pstDevTran)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevDtls		= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstDevTran == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the Device Details placeholder */
		pstDevDtls = (DEVTRAN_PTYPE) (pstTran->data);
		if(pstDevDtls == NULL)
		{
			pstDevDtls = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevDtls, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevDtls;

		}
		else
		{
			pstDevDtls = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(pstDevDtls, pstDevTran, sizeof(DEVTRAN_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateEmvKeyLoadDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateEmvKeyLoadDtlsForPymtTran(char * szTranKey, EMVKEYLOAD_INFO_PTYPE pstEmvKeyLoadInfo)
{
	int							rv						= SUCCESS;
	TRAN_PTYPE					pstTran					= NULL;
	PYMTTRAN_PTYPE				pstPymtDtls				= NULL;
	EMVKEYLOAD_INFO_PTYPE		pstLocEmvKeyLoadInfo	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstEmvKeyLoadInfo == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocEmvKeyLoadInfo = pstPymtDtls->pstEmvKeyLoadInfo;
		if(pstLocEmvKeyLoadInfo == NULL)
		{
			pstLocEmvKeyLoadInfo = (EMVKEYLOAD_INFO_PTYPE) malloc(sizeof(EMVKEYLOAD_INFO_STYPE));
			if(pstLocEmvKeyLoadInfo == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstEmvKeyLoadInfo = pstLocEmvKeyLoadInfo;
		}

		memcpy(pstLocEmvKeyLoadInfo, pstEmvKeyLoadInfo, sizeof(EMVKEYLOAD_INFO_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateEmvDtlsinCardDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateEmvDtlsinCardDtlsForPymtTran(char * szTranKey, EMVDTLS_PTYPE pstEmvDtls)
{
	int							rv						= SUCCESS;
	CARDDTLS_STYPE				stCardDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstEmvDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
		rv = getCardDtlsForPymtTran(szTranKey, &stCardDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Card Dtls from stack", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		storeEmvCardData(pstEmvDtls, &stCardDtls);

		rv = updateCardDtlsForPymtTran(szTranKey, &stCardDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to update card details in the stack", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updatePINDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updatePINDtlsForPymtTran(char * szTranKey, PINDTLS_PTYPE pstPINDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	PINDTLS_PTYPE	pstLocPINDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstPINDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocPINDtls = pstPymtDtls->pstPINDtls;
		if(pstLocPINDtls == NULL)
		{
			pstLocPINDtls = (PINDTLS_PTYPE) malloc(sizeof(PINDTLS_STYPE));
			if(pstLocPINDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstPINDtls = pstLocPINDtls;
		}

		memcpy(pstLocPINDtls, pstPINDtls, sizeof(PINDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateSigDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateSigDtlsForPymtTran(char * szTranKey, SIGDTLS_PTYPE pstSigDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	SIGDTLS_PTYPE	pstLocSigDtls	= NULL;
#ifdef DEVDEBUG
	char			szDbgMsg[4028]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstSigDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocSigDtls = pstPymtDtls->pstSigDtls;

		if(pstLocSigDtls == NULL)
		{
			pstLocSigDtls = (SIGDTLS_PTYPE) malloc(sizeof(SIGDTLS_STYPE));
			if(pstLocSigDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstSigDtls = pstLocSigDtls;
		}
		else
		{
			if(pstLocSigDtls->szSign != NULL)
			{
				free(pstLocSigDtls->szSign);
			}
		}

		memcpy(pstLocSigDtls, pstSigDtls, sizeof(SIGDTLS_STYPE));
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setDupTranIndicatorForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int setDupTranIndicatorForPymtTran(char * szTranKey, PAAS_BOOL bDupTranDetectedflg)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if( szTranKey == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transaction from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the Duplicate transaction indicator */
		pstPymtDtls->bDupTranDetected = bDupTranDetectedflg;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setPostAuthCardDtlsIndicatorForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int setPostAuthCardDtlsIndicatorForPymtTran(char * szTranKey, PAAS_BOOL bPostAuthCardDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if( szTranKey == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transaction from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the Duplicate transaction indicator */
		pstPymtDtls->bPostAuthCardDtls = bPostAuthCardDtls;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateTaxDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateTaxDtlsForPymtTran(char * szTranKey, LVL2_PTYPE pstLevel2Dtls)
{
	int				rv					= SUCCESS;
	TRAN_PTYPE		pstTran				= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
	LVL2_PTYPE		pstLocLevel2Dtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstLevel2Dtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocLevel2Dtls = pstPymtDtls->pstLevel2Dtls;
		if(pstLocLevel2Dtls == NULL)
		{
			pstLocLevel2Dtls = (LVL2_PTYPE) malloc(sizeof(LVL2_STYPE));
			if(pstLocLevel2Dtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstLocLevel2Dtls, 0x00, sizeof(LVL2_STYPE));
			pstPymtDtls->pstLevel2Dtls = pstLocLevel2Dtls;
		}

		memcpy(pstLocLevel2Dtls, pstLevel2Dtls, sizeof(LVL2_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateLtyDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateLtyDtlsForPymtTran(char * szTranKey, LTYDTLS_PTYPE pstLtyDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	LTYDTLS_PTYPE	pstLocLtyDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstLtyDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocLtyDtls = pstPymtDtls->pstLtyDtls;
		if(pstLocLtyDtls == NULL)
		{
			pstLocLtyDtls = (LTYDTLS_PTYPE) malloc(sizeof(LTYDTLS_STYPE));
			if(pstLocLtyDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstLtyDtls = pstLocLtyDtls;
		}

		memcpy(pstLocLtyDtls, pstLtyDtls, sizeof(LTYDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateFollowOnDtlsForPymt
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateFollowOnDtlsForPymt(char * szTranKey, FTRANDTLS_PTYPE pstFTranDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	FTRANDTLS_PTYPE	pstLocFTranDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstFTranDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present, allocating...", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocFTranDtls = pstPymtDtls->pstFTranDtls;
		if(pstLocFTranDtls == NULL)
		{
			pstLocFTranDtls = (FTRANDTLS_PTYPE) malloc(sizeof(FTRANDTLS_STYPE));
			if(pstLocFTranDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstFTranDtls = pstLocFTranDtls;
		}

		memcpy(pstLocFTranDtls, pstFTranDtls, sizeof(FTRANDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateCurTranDtlsForPymt
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateCurTranDtlsForPymt(char * szTranKey, CTRANDTLS_PTYPE pstCTranDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	CTRANDTLS_PTYPE	pstLocCTranDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstCTranDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocCTranDtls = pstPymtDtls->pstCTranDtls;
		if(pstLocCTranDtls == NULL)
		{
			pstLocCTranDtls = (CTRANDTLS_PTYPE) malloc(sizeof(CTRANDTLS_STYPE));
			if(pstLocCTranDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstCTranDtls = pstLocCTranDtls;
		}

		memcpy(pstLocCTranDtls, pstCTranDtls, sizeof(CTRANDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateVSPDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateVSPDtlsForPymtTran(char * szTranKey, VSPDTLS_PTYPE pstVSPDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	VSPDTLS_PTYPE	pstLocVSPDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstVSPDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocVSPDtls = pstPymtDtls->pstVSPDtls;
		if(pstLocVSPDtls == NULL)
		{
			pstLocVSPDtls = (VSPDTLS_PTYPE) malloc(sizeof(VSPDTLS_STYPE));
			if(pstLocVSPDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstVSPDtls = pstLocVSPDtls;
		}

		memcpy(pstLocVSPDtls, pstVSPDtls, sizeof(VSPDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateCardDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateCustInfoDtlsForPymtTran(char * szTranKey, CUSTINFODTLS_PTYPE pstCustInfoDtls)
{
	int					rv						= SUCCESS;
	TRAN_PTYPE			pstTran					= NULL;
	PYMTTRAN_PTYPE		pstPymtDtls				= NULL;
	CUSTINFODTLS_PTYPE 	pstlocCustInfoDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstCustInfoDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present, allocating", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstlocCustInfoDtls = pstPymtDtls->pstCustInfoDtls;
		if(pstlocCustInfoDtls == NULL)
		{
			pstlocCustInfoDtls = (CUSTINFODTLS_PTYPE) malloc(sizeof(CUSTINFODTLS_STYPE));
			if(pstlocCustInfoDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstPymtDtls->pstCustInfoDtls = pstlocCustInfoDtls;
		}

		memcpy(pstlocCustInfoDtls, pstCustInfoDtls, sizeof(CUSTINFODTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateVASDtlsForPymtTran
 *
 * Description	: This Function will Upadate The VAS details On to the Payment transaction Transaction 
 * 				  by using the transation key
 *
 * Input Params	: Transaction key,structure containing the VAS Details 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateVASDtlsForPymtTran(char * szTranKey, VASDATA_DTLS_PTYPE pstVasDataDtls)
{
	int					rv						= SUCCESS;
	TRAN_PTYPE			pstTran					= NULL;
	PYMTTRAN_PTYPE		pstPymtDtls				= NULL;
	VASDATA_DTLS_PTYPE	pstLocVasDataDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstVasDataDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present, allocating", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocVasDataDtls = pstPymtDtls->pstVasDataDtls;
		if(pstLocVasDataDtls == NULL)
		{
			pstLocVasDataDtls = (VASDATA_DTLS_PTYPE) malloc(sizeof(VASDATA_DTLS_STYPE));
			if(pstLocVasDataDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstLocVasDataDtls, 0x00, sizeof(VASDATA_DTLS_STYPE));
			pstPymtDtls->pstVasDataDtls = pstLocVasDataDtls;
		}

		memcpy(pstLocVasDataDtls, pstVasDataDtls, sizeof(VASDATA_DTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateRawCardDtlsForPymtTrans
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateRawCardDtlsForPymtTrans(char * szTranKey, CARDDTLS_PTYPE pstCardDtls, char **pszStdPymtTypes)
{
	int					rv					= SUCCESS;
	TRAN_PTYPE			pstTran				= NULL;
	PYMTTRAN_PTYPE		pstPymtDtls			= NULL;
	RAW_CARDDTLS_PTYPE	pstLocRawCardDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstCardDtls == NULL) || (pszStdPymtTypes == NULL) || (*pszStdPymtTypes == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstLocRawCardDtls = pstPymtDtls->pstRawCardDtls;
		if(pstLocRawCardDtls == NULL)
		{
			pstLocRawCardDtls = (RAW_CARDDTLS_PTYPE) malloc(sizeof(RAW_CARDDTLS_STYPE));
			if(pstLocRawCardDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			pstPymtDtls->pstRawCardDtls = pstLocRawCardDtls;
		}

		rv = getRawCardDtls(&pstLocRawCardDtls, pstCardDtls, PAAS_TRUE);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to Update Card dtls", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Need to copy payment type separatly from Current transaction details */
		if(pstPymtDtls->iPymtType >= 0)
		{
			strcpy(pstLocRawCardDtls->szPymtType, pszStdPymtTypes[pstPymtDtls->iPymtType]);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ==============================================================================
 * Function Name: updateQueryNfcIniDtlsForDevTran
 *
 * Description	:This Function updates the Details of nfc.ini file to Device Tran
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ===============================================================================
 */
int updateQueryNfcIniDtlsForDevTran(char * szTranKey, NFCINI_CONTENT_INFO_PTYPE pstNfcDataDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstNfcDataDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stNfcIniContentInfoDtls), pstNfcDataDtls, sizeof(NFCINI_CONTENT_INFO_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: updateVASmodeForPymtTran
 *
 * Description	: This Function will Upadate The VAS details On to the Payment transaction Transaction
 * 				  by using the transation key
 *
 * Input Params	: Transaction key,structure containing the VAS Details
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateVASmodeForPymtTran(char * szTranKey, PAAS_BOOL bVASMode)
{
	int					rv						= SUCCESS;
	TRAN_PTYPE			pstTran					= NULL;
	PYMTTRAN_PTYPE		pstPymtDtls				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPymtDtls = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
			if(pstPymtDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED for Payment Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstPymtDtls, 0x00, sizeof(PYMTTRAN_STYPE));
			pstTran->data = pstPymtDtls;
		}

		pstPymtDtls->bCheckVASMode = bVASMode;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: resetGenRespDtlsForSSITran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int resetGenRespDtlsForSSITran(char *szTranKey)
{
	int			rv				= SUCCESS;
	TRAN_PTYPE	pstTran			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/*  flush the response details */
		memset(pstTran->stRespDtls.szRespTxt,  0x00, sizeof(pstTran->stRespDtls.szRespTxt));
		memset(pstTran->stRespDtls.szRslt,     0x00, sizeof(pstTran->stRespDtls.szRslt));
		memset(pstTran->stRespDtls.szRsltCode, 0x00, sizeof(pstTran->stRespDtls.szRsltCode));
		memset(pstTran->stRespDtls.szTermStat, 0x00, sizeof(pstTran->stRespDtls.szTermStat));

		break; //Breaking from the while loop
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillRespDtls
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
static int fillRespDtls(char * szMsg, int iStatus, RESPDTLS_PTYPE pstRespDtls)
{
	int		rv						= SUCCESS;
	char *	pszTermStat				= NULL;
	char *	pszRsltCode				= NULL;
	char *	pszRslt					= NULL;
	char *	pszRspTxt				= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Status = %d", __FUNCTION__, iStatus);
	APP_TRACE(szDbgMsg);

	switch(iStatus)
	{
	case SUCCESS:
		pszTermStat = "SUCCESS";
		pszRsltCode = "-1";
		pszRslt		= "OK";
		pszRspTxt	= "Operation SUCCESSFUL";
		break;

	case ERR_INV_XML_MSG:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59027";
		pszRslt		= "ERROR";
		pszRspTxt	= "XML Error";
		break;

	case ERR_USR_CANCELED:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59001";
		pszRslt		= "CANCELLED";
		pszRspTxt	= "Cancelled by CUSTOMER";
		break;

	case POS_CANCELLED:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59049";
		pszRslt		= "CANCELLED";
		pszRspTxt	= "Cancelled by POS/CASHIER";
		break;

	case ERR_USR_TIMEOUT:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59001";
		pszRslt		= "CANCELLED";
		pszRspTxt	= "Cancelled by TIMEOUT";
		break;

	case ERR_DEVICE_BUSY:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59002";
		pszRslt		= "BUSY";
		pszRspTxt	= "DEVICE IS BUSY";
		break;

	case ERR_IN_SESSION:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59003";
		pszRslt		= "BUSY";
		pszRspTxt	= "SESSION in progress";
		break;

	case ERR_NO_SESSION:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59004";
		pszRslt		= "ERROR";
		pszRspTxt	= "NO SESSION";
		break;

	case ERR_UNSUPP_OPR:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59005";
		pszRslt		= "ERROR";
		pszRspTxt	= "Unsupported operation";
		break;

	case ERR_UNSUPP_CMD:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59006";
		pszRslt		= "ERROR";
		pszRspTxt	= "Command Not Supported by Device";
		break;

	case ERR_BAD_CARD:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59007";
		pszRslt		= "ERROR";
		pszRspTxt	= "Bad Card Read";
		break;

	case ERR_SYSTEM:
	case FAILURE:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59020";
		pszRslt		= "INTERNAL_ERROR";
		pszRspTxt	= "System Error";
		break;

	case ERR_VERISHIELD:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59021";
		pszRslt		= "INTERNAL_ERROR";
		pszRspTxt	= "VSP library error";
		break;

	case ERR_DEVICE_APP:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59022";
		pszRslt		= "INTERNAL_ERROR";
		pszRspTxt	= "Device Application Error FA";
		break;

	case ERR_NO_CONN:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59023";
		pszRslt		= "COMM_ERROR";
		pszRspTxt	= "Unable to connect";
		break;

	case ERR_CONN_TO:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59023";
		pszRslt		= "COMM_ERROR";
		pszRspTxt	= "Connection timeout";
		break;

	case ERR_CONN_FAIL:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59025";
		pszRslt		= "COMM_ERROR";
		pszRspTxt	= "Connection disrupted";
		break;

	case ERR_RESP_TO:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59026";
		pszRslt		= "COMM_ERROR";
		pszRspTxt	= "Response timeout";
		break;

	case ERR_CFG_PARAMS:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59028";
		pszRslt		= "ERROR";
		pszRspTxt	= "Configuration Error";
		break;

	case ERR_FLD_MISMATCH:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59040";
		pszRslt		= "FIELD_ERROR";
		pszRspTxt	= "Field Mismatch";
		break;

	case ERR_INV_COMBI:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59041";
		pszRslt		= "FIELD_ERROR";
		pszRspTxt	= "Invalid Combination";
		break;

	case ERR_FLD_REQD:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59042";
		pszRslt		= "FIELD_ERROR";
		pszRspTxt	= "Field is required";
		break;

	case ERR_INV_FLD:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59043";
		pszRslt		= "FIELD_ERROR";
		pszRspTxt	= "Field is invalid";
		break;

	case ERR_DUP_FLD:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59044";
		pszRslt		= "FIELD_ERROR";
		pszRspTxt	= "Field already exists";
		break;

	case ERR_NO_FLD:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59045";
		pszRslt		= "FIELD_ERROR";
		pszRspTxt	= "Field does not exist";
		break;

	case ERR_INV_FLD_VAL:
	case ERR_INV_PYMT_TYPES_CNT:
	case ERR_INV_PYMT_TYPES:
	case ERR_INV_TENDER_CNT:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59046";
		pszRslt		= "FIELD_ERROR";
		pszRspTxt	= "Value not valid for field";
		break;

	case ERR_FLD_LTZ:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59047";
		pszRslt		= "FIELD_ERROR";
		pszRspTxt	= "Field cannot be negative";
		break;

	case ERR_FLD_LTEZ:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59047";
		pszRslt		= "FIELD_ERROR";
		pszRspTxt	= "Value must be greater than zero";
		break;

	case ERR_OFFLINE_TRAN_AMT_EXCEEDED:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59024";
		pszRslt		= "FAILED";
		pszRspTxt	= "Offline Tran amt exceeded; call for voice approval";
		break;

	case ERR_OFFLINE_TOTAL_AMT_EXCEEDED:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59024";
		pszRslt		= "FAILED";
		pszRspTxt	= "Offline total amt exceeded";
		break;

	case ERR_OFFLINE_DAYS_EXCEEDED:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59024";
		pszRslt		= "FAILED";
		pszRspTxt	= "Offline days count exceeded; cant process tran offline";
		break;

	case ERR_OFFLINE_UN_SUPPORTIVE_PYMT_TYPE:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59024";
		pszRslt		= "FAILED";
		pszRspTxt	= "Communication error; cant process tran offline for this payment type";
		break;

	case ERR_SAF_NOT_ALLOWED_CURRENTLY:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59024";
		pszRslt		= "FAILED";
		pszRspTxt	= "Communication error; cant process current tran offline";
		break;

	case ERR_CARD_NOT_IN_INCLUSIONRANGE:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59008";
		pszRslt		= "ERROR";
		pszRspTxt	= "Card Not Valid For Merchant";
		break;

	case ERR_INVALID_CARD_DATA:
	case ERR_BAD_DATA_IN_CARD:
		//We are currently seeing Default Case Error for various commands. Return Card Data Not Valid
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59009";
		pszRslt		= "ERROR";
		pszRspTxt	= "Card Data Not Valid";
		break;

	case ERR_MANUAL_NOTALLWD_FOR_CARD:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59010";
		pszRslt		= "ERROR";
		pszRspTxt	= "Manual Entry Not Allowed For This Card";
		break;

	case ERR_ENCODE:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "40001";
		pszRslt		= "ERROR";
		pszRspTxt	= "Base64 Encode Error";
		break;

	case ERR_AES_ENCRYPTION:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "40002";
		pszRslt		= "ERROR";
		pszRspTxt	= "Encryption Error";
		break;

	case ERR_AES_DECRYPTION:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "40006";
		pszRslt		= "ERROR";
		pszRspTxt	= "Decryption Error";
		break;

	case ERR_ENTRY_CODE_MISMATCH:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "40009";
		pszRslt		= "ERROR";
		pszRspTxt	= "ENTRY_CODE mismatch";
		break;

	case ERR_DEV_REBOOTING:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59050";
		pszRslt		= "REBOOTED";
		pszRspTxt	= "POS Initiated Reboot";
		break;

	case ERR_INVOICE_MISMATCH:
		pszTermStat	= "SUCCESS";
		pszRsltCode	= "56023";
		pszRslt		= "NOT PROCESSED";
		pszRspTxt	= "Invoice Mismatch; Can't Process Duplicate Tran";
		break;

	case ERR_POS_REG_LIMIT_EXCEEDED:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59051";
		pszRslt		= "NOT PROCESSED";
		pszRspTxt	= "Maximum POS Connections Exceeded";
		break;

	case ERR_CHIP_ERROR:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59020";
		pszRslt		= "CHIP_ERROR";
		pszRspTxt	= "Chip Error";
		break;

	case ERR_NO_UPDATES:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59052";
		pszRslt		= "ERROR";
		pszRspTxt	= "No Updates Available on Terminal to Apply";
		break;

	case ERR_NO_MATCHING_PROV_PASS:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59053";
		pszRslt		= "ERROR";
		pszRspTxt	= "Merchant ID or URL Not Present in Device";
		break;

	case ERR_NO_LINE_ITEMS:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59054";
		pszRslt		= "ERROR";
		pszRspTxt	= "No Line Item(s) Present To Remove";
		break;

	case ERR_NOT_ACCEPTED_CONFIG:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59055";
		pszRslt		= "ERROR";
		pszRspTxt	= "Not Accepted Configuration";
		break;

	case ERR_AMOUNT_EXCEEDS_LIMIT:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59056";
		pszRslt		= "ERROR";
		pszRspTxt	= "Amount Exceeds Limit";
		break;

	case ERR_QRCODE_GENE_FAILED:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59057";
		pszRslt		= "ERROR";
		pszRspTxt	= "Failed to Generate QR Code Image For the Given Data";
		break;

	case ERR_INV_DISP_QRCODE_CMD:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59058";
		pszRslt		= "ERROR";
		pszRspTxt	= "This Command is Not Supported For Current Screen";
		break;

	case ERR_INV_CANCEL_QRCODE_CMD:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59059";
		pszRslt		= "ERROR";
		pszRspTxt	= "No QR Code Image Present On Screen to Remove";
		break;

	case ERR_INI_PARSING_FAIL:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59060";
		pszRslt		= "ERROR";
		pszRspTxt	= "Parsing Failed";
		break;

	case ERR_FILE_NOT_FOUND:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59061";
		pszRslt		= "ERROR";
		pszRspTxt	= "File Not Found";
		break;

	case ERR_PIN_REQD_EMV_CASHBACK:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59062";
		pszRslt		= "ERROR";
		pszRspTxt	= "Transaction Cancelled:Pin Mandatory for EMV Cashback Transactions";
		break;

#if 0
	/* Should not come here as the card data is being validated in the online
	 * mode also */
	case ERR_OFFLINE_INVALID_CARD_DATA:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59024";
		pszRslt		= "FAILED";
		pszRspTxt	= "Card data is not valid; can't process tran offline";
		break;
#endif

	default:
		pszTermStat	= "FAILURE";
		pszRsltCode	= "59022";
		pszRslt		= "DEVICE";
		pszRspTxt	= "Default Case Error";
		break;
	}

	/* Populate the response details */

	/*	If the response details are filled already with the
	 * 	response details from SSI host then we need not fill them here
	 *  TO DO: Please revist it again to make it better
	 *
	 */
	if(strlen(pstRespDtls->szTermStat) == 0 && strlen(pstRespDtls->szRsltCode) == 0 && strlen(pstRespDtls->szRslt) == 0 && strlen(pstRespDtls->szRespTxt) == 0)
	{
		debug_sprintf(szDbgMsg, "%s: Filling Gen Response Details", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		memset(pstRespDtls, 0x00, sizeof(RESPDTLS_STYPE));
		strcpy(pstRespDtls->szTermStat, pszTermStat);
		strcpy(pstRespDtls->szRsltCode, pszRsltCode);
		strcpy(pstRespDtls->szRslt, pszRslt);
		if((szMsg != NULL) && (strlen(szMsg) > 0))
		{
			strcpy(pstRespDtls->szRespTxt, szMsg);
		}
		else
		{
			strcpy(pstRespDtls->szRespTxt, pszRspTxt);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Response details are present already...not filling with generic values", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateQRCodeDtlsInSession
 *
 * Description	: This API is used to update the QR code details in session
 *
 * Input Params	: session key, QR code details
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int updateQRCodeDtlsInSession(char* pszSessKey, DISPQRCODE_DTLS_PTYPE pstQRCodeDtls)
{
	int					rv						= SUCCESS;
	SESSDTLS_PTYPE		pstSess					= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((pszSessKey == NULL) || (pstQRCodeDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getSession(pszSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Session details are not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}


		if(pstSess->pstQRCodeDtls == NULL)
		{
			// allocate memory here
			pstSess->pstQRCodeDtls = (DISPQRCODE_DTLS_PTYPE) malloc(sizeof(DISPQRCODE_DTLS_STYPE));
			if(pstSess->pstQRCodeDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
		}
		// reset the details
		memset(pstSess->pstQRCodeDtls, 0x00, sizeof(DISPQRCODE_DTLS_STYPE));
		memcpy(pstSess->pstQRCodeDtls, pstQRCodeDtls, sizeof(DISPQRCODE_DTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateCustCheckBoxDtlsForDevTran
 *
 * Description	:This Function used to update the data captured in Customer Checkbox command.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int updateCustCheckBoxDtlsForDevTran(char * szTranKey, CUSTCHECKBOX_DTLS_PTYPE pstCustCheckBox)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstCustCheckBox == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if((pstTran->data) == NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) malloc(sizeof(DEVTRAN_STYPE));
			if(pstDevTran == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));
			pstTran->data = pstDevTran;
		}
		else
		{
			pstDevTran = (DEVTRAN_PTYPE)pstTran->data;
		}

		memcpy(&(pstDevTran->stCustCheckBoxDtls), pstCustCheckBox, sizeof(CUSTCHECKBOX_DTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * End of file tranDS2.c
 * ============================================================================
 */
