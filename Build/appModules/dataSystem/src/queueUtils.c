/*
 * ============================================================================
 * File Name	: queueUtils.c
 *
 * Description	:
 *
 * Author		:
 * ============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>

#include "common/utils.h"

#define QUEUE_VER "1.0.00"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

/* Data definitions related to Queue implementation */
typedef struct __node
{
	int				dataLen;
	void *			data;
	struct __node *	next;
}
QNODE_STYPE, * QNODE_PTYPE;

typedef struct __meta
{
	char			szKey[10];
	pthread_mutex_t	qMutex;
	QNODE_PTYPE		qHead;
	QNODE_PTYPE		qTail;
	struct __meta *	next;
}
QMETA_STYPE, * QMETA_PTYPE;

/* Static data declarations */
static QMETA_PTYPE		pstQListBeg	= NULL;
static pthread_mutex_t	listMutex;

/* Static functions declarations */
static void addNewQueue(QMETA_PTYPE);
static void getUniqueQId(char *, int);
static QMETA_PTYPE fetchQForId(char *);
static void removeAllQRecs(QMETA_PTYPE);
static int addNewRecToQ(QMETA_PTYPE, void *, int);
static int addNewRecToStack(QMETA_PTYPE, void *, int);
static void * getNextRecFrmQ(QMETA_PTYPE, int *);
static void * peekNextRecFrmQ(QMETA_PTYPE, int *);

/*
 * ============================================================================
 * Function Name: initQueueModule
 *
 * Description	: This function is responsible for initializing the queueing
 * 					module (part of utils library). This function should be
 * 					called only once in the begining of the application
 * 					execution.
 *
 * Input Params	: None
 *
 * Output Params: None
 * ============================================================================
 */
void initQueueModule()
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: Using version [%s]", __FUNCTION__, QUEUE_VER);
	APP_TRACE(szDbgMsg);

	/* Initialize the mutex responsible for managing concurrent accesses to 
	 * multiple queues */
	pthread_mutex_init(&listMutex, NULL);

	return;
}

/*
 * ============================================================================
 * Function Name: createQueue
 *
 * Description	: This function is responsible for creating a new queue
 *
 * Input Params	: szKey -> place holder for unique Queue ID
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int createQueue(char * szKey)
{
	int			rv				= SUCCESS;
	QMETA_PTYPE	pstNewQueue		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Allocate memory for the new queue meta node */
		pstNewQueue = (QMETA_PTYPE) malloc(sizeof(QMETA_STYPE));
		if(pstNewQueue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize the memory for the queue meta node */
		memset(pstNewQueue, 0x00, sizeof(QMETA_STYPE));
		pthread_mutex_init(&(pstNewQueue->qMutex), NULL);

		/* Add this new queue to already existing list of queues */
		addNewQueue(pstNewQueue);

		/* Return the Queue ID assigned to the node when the node got added in
		 * the list. This Queue ID would be useful in accessing this node in
		 * future */
		strcpy(szKey, pstNewQueue->szKey);

		debug_sprintf(szDbgMsg, "%s: Created Queue key [%s]", __FUNCTION__, szKey);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: createStack
 *
 * Description	: This function is responsible for creting a new stack
 *
 * Input Params	: szKey -> place holder for unique Queue ID
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int createStack(char * szKey)
{
	return (createQueue(szKey));
}

/*
 * ============================================================================
 * Function Name: deleteQueue
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int deleteQueue(char * szKey)
{
	int			rv				= SUCCESS;
	QMETA_PTYPE	pstCurNode		= NULL;
	QMETA_PTYPE	pstTmpNode		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Deleting Queue key [%s]", __FUNCTION__, szKey);
	APP_TRACE(szDbgMsg);

	/* Acquire the lock on the queue list before making any changes */
	acquireMutexLock(&listMutex, __FUNCTION__);

	pstCurNode = fetchQForId(szKey);
	if(pstCurNode != NULL)
	{
		pstTmpNode = pstQListBeg;
		while(pstTmpNode->next != pstCurNode)
		{
			pstTmpNode = pstTmpNode->next;
		}

		pstTmpNode->next = pstCurNode->next;

		if(pstCurNode == pstCurNode->next)
		{
			/* If there is only one queue in the list */
			pstQListBeg = NULL;
		}
		else if(pstCurNode == pstQListBeg)
		{
			pstQListBeg = pstCurNode->next;
		}
	}
	else
	{
		rv = FAILURE;
	}

	/* Release the lock on the queue list after making the changes */
	releaseMutexLock(&listMutex, __FUNCTION__);

	//if(pstTmpNode != NULL)
	if(pstCurNode != NULL)
	{
		/* Acquire the lock on the queue */
		acquireMutexLock(&(pstCurNode->qMutex), __FUNCTION__);

		//removeAllQRecs(pstTmpNode);
		removeAllQRecs(pstCurNode);

		/* Release the lock on the queue */
		releaseMutexLock(&(pstCurNode->qMutex), __FUNCTION__);

		/* WARNING: Some Loop Hole here */
		pthread_mutex_destroy(&(pstCurNode->qMutex));
		free(pstCurNode);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: deleteStack
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int deleteStack(char * szKey)
{
	return (deleteQueue(szKey));
}

/*
 * ============================================================================
 * Function Name: flushQueue
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int flushQueue(char * szKey)
{
	int			rv				= SUCCESS;
	QMETA_PTYPE	pstCurNode		= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Acquire the lock on the queue list before making any changes */
	acquireMutexLock(&listMutex, __FUNCTION__);

	pstCurNode = fetchQForId(szKey);
	if(pstCurNode != NULL)
	{
		/* Acquire the lock on the queue */
		acquireMutexLock(&(pstCurNode->qMutex), __FUNCTION__);
	}

	/* Release the lock on the queue list after making the changes */
	releaseMutexLock(&listMutex, __FUNCTION__);

	if(pstCurNode != NULL)
	{
		removeAllQRecs(pstCurNode);

		/* Release the lock on the queue */
		releaseMutexLock(&(pstCurNode->qMutex), __FUNCTION__);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No Queue to flush", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: flushStack
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int flushStack(char * szKey)
{
	return (flushQueue(szKey));
}

/*
 * ============================================================================
 * Function Name: addDataToQ
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int addDataToQ(void * data, int len, char * szKey)
{
	int			rv				= SUCCESS;
	QMETA_PTYPE	pstCurNode		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	/* Acquire the lock on the queue list before making any changes */
	acquireMutexLock(&listMutex, __FUNCTION__);

	pstCurNode = fetchQForId(szKey);
	if(pstCurNode != NULL)
	{
		/* Acquire the lock on the queue */
		acquireMutexLock(&(pstCurNode->qMutex), __FUNCTION__);
	}

	/* Release the lock on the queue list after making the changes */
	releaseMutexLock(&listMutex, __FUNCTION__);

	if(pstCurNode != NULL)
	{
		rv = addNewRecToQ(pstCurNode, data, len);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to add data", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* Release the lock on the queue */
		releaseMutexLock(&(pstCurNode->qMutex), __FUNCTION__);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No Queue to adding data", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addDataToStack
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int addDataToStack(void * data, int len, char * szKey)
{
	int			rv				= SUCCESS;
	QMETA_PTYPE	pstCurNode		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Acquire the lock on the queue list before making any changes */
	acquireMutexLock(&listMutex, __FUNCTION__);

	pstCurNode = fetchQForId(szKey);
	if(pstCurNode != NULL)
	{
		/* Acquire the lock on the queue */
		acquireMutexLock(&(pstCurNode->qMutex), __FUNCTION__);
	}

	/* Release the lock on the queue list after making the changes */
	releaseMutexLock(&listMutex, __FUNCTION__);

	if(pstCurNode != NULL)
	{
		rv = addNewRecToStack(pstCurNode, data, len);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to add data", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* Release the lock on the queue */
		releaseMutexLock(&(pstCurNode->qMutex), __FUNCTION__);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No Queue to adding data", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDataFrmQ
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void * getDataFrmQ(char * szKey, int * size)
{
	void *		data			= NULL;
	QMETA_PTYPE	pstCurNode		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	/* Acquire the lock on the queue list before making any changes */
	acquireMutexLock(&listMutex, __FUNCTION__);

	pstCurNode = fetchQForId(szKey);
	if(pstCurNode != NULL)
	{
		/* Acquire the lock on the queue */
		acquireMutexLock(&(pstCurNode->qMutex), __FUNCTION__);
	}

	/* Release the lock on the queue list after making the changes */
	releaseMutexLock(&listMutex, __FUNCTION__);

	if(pstCurNode != NULL)
	{
		data = getNextRecFrmQ(pstCurNode, size);

		/* Release the lock on the queue */
		releaseMutexLock(&(pstCurNode->qMutex), __FUNCTION__);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No Queue to adding data", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	return data;
}

/*
 * ============================================================================
 * Function Name: peekDataFrmQ
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void * peekDataFrmQ(char * szKey, int * size)
{
	void *		data			= NULL;
	QMETA_PTYPE	pstCurNode		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	/* Acquire the lock on the queue list before making any changes */
	acquireMutexLock(&listMutex, __FUNCTION__);

	pstCurNode = fetchQForId(szKey);
	if(pstCurNode != NULL)
	{
		/* Acquire the lock on the queue */
		acquireMutexLock(&(pstCurNode->qMutex), __FUNCTION__);
	}

	/* Release the lock on the queue list after making the changes */
	releaseMutexLock(&listMutex, __FUNCTION__);

	if(pstCurNode != NULL)
	{
		data = peekNextRecFrmQ(pstCurNode, size);

		/* Release the lock on the queue */
		releaseMutexLock(&(pstCurNode->qMutex), __FUNCTION__);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No Queue to adding data", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	return data;
}



/*
 * ============================================================================
 * Function Name: getDataFrmStack
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void * getDataFrmStack(char * szKey, int * size)
{
	return (peekDataFrmQ(szKey, size));
}

/*
 * ============================================================================
 * Function Name: releaseListQueueMutexLocks
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void releaseListQueueMutexLocks(char * szKey)
{
	QMETA_PTYPE	pstCurNode		= NULL;

#ifdef DEBUG
	//char		szDbgMsg[256]	= "";
#endif

	releaseAcquiredMutexLock(&(listMutex), "List Mutex");

	pstCurNode = fetchQForId(szKey);
	if(pstCurNode != NULL)
	{
		/* Releasing the lock on the queue if it is acquired*/
		releaseAcquiredMutexLock(&(pstCurNode->qMutex), "Current Node Mutex");
	}
}

/*
 * ============================================================================
 * Function Name: popDataFrmStack
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void * popDataFrmStack(char * szKey, int * size)
{
	return (getDataFrmQ(szKey, size));
}

/*
 * ============================================================================
 * Function Name: addNewQueue
 *
 * Description	: This function adds a new queue to the list
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void addNewQueue(QMETA_PTYPE pstNewQ)
{
	QMETA_PTYPE		pstCurNode	= NULL;

	/* Acquire the lock on the queue list before making any changes */
	acquireMutexLock(&listMutex, __FUNCTION__);

	/* Search for a unique name to be assigned to the new Queue as ID */
	getUniqueQId(pstNewQ->szKey, sizeof(pstNewQ->szKey) - 1);

	pstCurNode = pstQListBeg;
	if(pstCurNode == NULL)
	{
		pstQListBeg = pstNewQ;
	}
	else
	{
		while(pstCurNode->next != pstQListBeg)
		{
			pstCurNode = pstCurNode->next;
		}

		pstCurNode->next = pstNewQ;
	}

	pstNewQ->next = pstQListBeg;

	/* Release the lock on the queue list after making the changes */
	releaseMutexLock(&listMutex, __FUNCTION__);

	return;
}

/*
 * ============================================================================
 * Function Name: getUniqueQid
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void getUniqueQId(char * szKey, int iSize)
{
	int		iCnt			= 0;
	int		iLen			= 0;
	char	szAsciiTbl[]	= "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	iLen = strlen(szAsciiTbl);

	while(1)
	{
		memset(szKey, 0x00, iSize + 1);
		for(iCnt = 0; iCnt < iSize; iCnt++)
		{
			szKey[iCnt] = szAsciiTbl[(rand() % iLen)];
		}

		debug_sprintf(szDbgMsg, "%s: Key = [%s]", __FUNCTION__, szKey);
		APP_TRACE(szKey);

		if(NULL != fetchQForId(szKey))
		{
			debug_sprintf(szDbgMsg, "%s: Key [%s] not unique", __FUNCTION__,
																		szKey);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			break;
		}
	}

	return;
}

/*
 * ============================================================================
 * Function Name: fetchQForId
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static QMETA_PTYPE fetchQForId(char * szKey)
{
	PAAS_BOOL	bQFound			= PAAS_FALSE;
	QMETA_PTYPE	pstCurNode		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	pstCurNode = pstQListBeg;
	if(pstCurNode != NULL)
	{
		do
		{
			if(strcmp(pstCurNode->szKey, szKey) == SUCCESS)
			{
				bQFound = PAAS_TRUE;
				break;
			}

			pstCurNode = pstCurNode->next;
		}
		while(pstCurNode != pstQListBeg);

		if(bQFound != PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: No queue found for Key [%s]",
														__FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);

			pstCurNode = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No queue present till now", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	return pstCurNode;
}

/*
 * ============================================================================
 * Function Name: removeAllQRecs
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void removeAllQRecs(QMETA_PTYPE pstQueue)
{
	QNODE_PTYPE		pstCurRec	= NULL;
	QNODE_PTYPE		pstTmpRec	= NULL;

	pstCurRec = pstQueue->qHead;

	while(pstCurRec != NULL)
	{
		pstTmpRec = pstCurRec;
		pstCurRec = pstCurRec->next;

		free(pstTmpRec->data);
		free(pstTmpRec);
	}

	pstQueue->qHead = pstQueue->qTail = NULL;

	return;
}

/*
 * ============================================================================
 * Function Name: addNewRecToQ
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addNewRecToQ(QMETA_PTYPE pstQueue, void * data, int iSize)
{
	int			rv				= SUCCESS;
	QNODE_PTYPE	pstRec			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	while(1)
	{
		pstRec = (QNODE_PTYPE) malloc(sizeof(QNODE_STYPE));
		if(pstRec == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Malloc FAILED for record",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pstRec, 0x00, sizeof(QNODE_STYPE));

		pstRec->data = malloc(iSize);
		if(pstRec->data == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Malloc FAILED for data", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			free(pstRec);

			rv = FAILURE;
			break;
		}

		memset(pstRec->data, 0x00, iSize);
		memcpy(pstRec->data, data, iSize);
		pstRec->dataLen = iSize;

		if(pstQueue->qTail == NULL)
		{
			pstQueue->qHead = pstRec;
			pstQueue->qTail = pstRec;
		}
		else
		{
			pstQueue->qTail->next = pstRec;
			pstQueue->qTail = pstRec;
		}

		break;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addNewRecToStack
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addNewRecToStack(QMETA_PTYPE pstStack, void * data, int iSize)
{
	int			rv				= SUCCESS;
	QNODE_PTYPE	pstRec			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		pstRec = (QNODE_PTYPE) malloc(sizeof(QNODE_STYPE));
		if(pstRec == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Malloc FAILED for record",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pstRec, 0x00, sizeof(QNODE_STYPE));

		pstRec->data = malloc(iSize);
		if(pstRec->data == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Malloc FAILED for data", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			free(pstRec);

			rv = FAILURE;
			break;
		}

		memset(pstRec->data, 0x00, iSize);
		memcpy(pstRec->data, data, iSize);
		pstRec->dataLen = iSize;

		if(pstStack->qHead == NULL)
		{
			pstStack->qHead = pstRec;
			pstStack->qTail = pstRec;
		}
		else
		{
			pstRec->next = pstStack->qHead;
			pstStack->qHead = pstRec;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getNextRecFrmQ
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void * getNextRecFrmQ(QMETA_PTYPE queue, int * size)
{
	void *		data	= NULL;
	QNODE_PTYPE	pstRec	= NULL;

	if(queue->qHead != NULL)
	{
		pstRec = queue->qHead;
		data = pstRec->data;

		if(queue->qHead == queue->qTail)
		{
			queue->qHead = queue->qTail = NULL;
		}
		else
		{
			queue->qHead = pstRec->next;
		}

		*size = pstRec->dataLen;

		free(pstRec);
	}

	return data;
}

/*
 * ============================================================================
 * Function Name: peekNextRecFrmQ
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void * peekNextRecFrmQ(QMETA_PTYPE queue, int * size)
{
	void *		data	= NULL;
	QNODE_PTYPE	pstRec	= NULL;

	if(queue->qHead != NULL)
	{
		pstRec = queue->qHead;
		data = pstRec->data;
		*size = pstRec->dataLen;
	}

	return data;
}

/*
 * ============================================================================
 * End of file queueUtils.c
 * ============================================================================
 */
