/*
 * ============================================================================
 * File Name	: dataSystem.c
 *
 * Description	:
 *
 * Author		: Vikram Datt Rana
 * ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "iniparser.h"
#include "common/utils.h"
#include "db/baseDataLayer.h"
#include "db/dataSystem.h"
#include "db/posDataStore.h"
#include "sci/sciDef.h"
#include "ssi/ssiDef.h"
#include "ssi/ssiMain.h"
#include "common/metaData.h"
#include "bLogic/bLogicCfgDef.h"
#include "uiAgent/emv.h"
#include "appLog/appLogAPIs.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

/* Extern functions */
extern void initQueueModule();
extern int	initCDT();
extern int  initSTB();
extern int  loadBITFile();
extern PAAS_BOOL isBapiEnabled();

/* Static functions declarations */
static void initializeTranData(void *);
static void cleanupTranData(void *);
static void cleanUpReqLI(LIINFO_PTYPE , int, char *);
//static void cleanUpReqLI(void* , int, char *);
static void cleanupSAFDtls(SAFDTLS_PTYPE);
static void cleanupDevTranDtls(DEVTRAN_PTYPE, int);
static void cleanupPymtTranDlts(PYMTTRAN_PTYPE);
static void cleanupRptDtls(PASSTHRU_PTYPE);
static void cleanupSigDtls(SIGDTLS_PTYPE);
static void cleanupSSITranDtls(TRAN_PTYPE);
static void cleanUpPassThrghDtls();
static void initSession(void *);
static void cleanupSession(void *);
static int loadPassThrghFieldsDtls();
static int loadPassThroughVarlist(dictionary * , KEYVAL_PTYPE, char * );
static int loadKeyListDetails(dictionary * , VARLST_INFO_PTYPE, char * );
static int getValueTypeofVarListElement(char * , int *);
static int addVarListSingleElements(char *, VARLST_INFO_PTYPE ) ;
void cleanupLineItems(LIINFO_PTYPE);
void cleanupPaymentItems(PYMNTINFO_PTYPE);

/* Static variables */
static PASSTHRG_FIELDS_STYPE stPassThrgDtls;

/*
 * ============================================================================
 * Function Name: initDataSystem
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int initDataSystem()
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Initialize the base layer of the data system */
		rv = initBaseDataLayer();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to initialize base layer",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Failed to Initialize the Base Data Layer");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}

			break;
		}

		/* Load the POS auth information records */
		rv = loadPosInfoRecords();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to load pos auth records",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Failed Loading POS Authentication Information Records");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}
			break;
		}

		/* Load the CDT records */
		rv = initCDT();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to load Card Data Table",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Failed to Load the Card Data Table");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}

			break;
		}
		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Card Data Table Loaded Successfully");
			addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
		}

		/* Load the STB Decision records */
		rv = initSTB();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to load STB Decision Table",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Failed to Load the STB Decision Table");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}

			break;
		}

		/* Load the BIT records */
		rv = loadBITFile();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to load BIT Table",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Failed to Load the BIT Records");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}

			break;
		}

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "BIT(Bin Inclusion Table) Records Loaded Successfully");
			addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
		}

		/* Load SCA Pass through fields into memory*/
		rv = loadPassThrghFieldsDtls();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in loading Pass through fields",
									__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Load the Pass Through Fields From the File");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}

			break;
		}

		/* Initialize the queue/stack utilities */
		initQueueModule();

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: closeDataSystem
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int closeDataSystem()
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Flush the base data layer */
		rv = flushBaseDataLayer();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to flush base data layer",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: createTran
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int createTran(char * szTranKey)
{
	int				rv				= SUCCESS;
	DATAINFO_STYPE	stDataInfo;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&stDataInfo, 0x00, sizeof(DATAINFO_STYPE));
	stDataInfo.iDataSize = sizeof(TRAN_STYPE);
	stDataInfo.initData = initializeTranData;
	stDataInfo.cleanupData = cleanupTranData;

	rv = addData(&stDataInfo, szTranKey);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add tran data", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTran
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getTran(char * szTranKey, TRAN_PTYPE * dpstTran)
{
	int			rv				= SUCCESS;
	TRAN_PTYPE	pstTran			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (szTranKey == NULL) || (dpstTran == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		pstTran = (TRAN_PTYPE) getData(szTranKey);
		if(pstTran == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get data from base layer",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		*dpstTran = pstTran;

		break;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: deleteTran
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int deleteTran(char * szTranKey)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
	while(1)
	{
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
	
		rv = flushDataForKey(szTranKey, 1);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to flush trandata",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: pushNewSSITran
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int pushNewSSITran(char * szTranKey)
{
	int			rv				= SUCCESS;
	TRAN_STYPE	stSSITran;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(&stSSITran, 0x00, sizeof(TRAN_STYPE));

		if(strlen(szTranKey) <= 0)
		{
			/* Since no stack exists, we need to create a stack first, which
			 * would then be used for pushing the SSI transactions */
			rv = createStack(szTranKey);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to create SSI tran stack",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}

		/* A stack exists for the transaction to be pushed into. Push a new SSI
		 * transaction on the stack */
		rv = addDataToStack(&stSSITran, sizeof(TRAN_STYPE), szTranKey);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to push a new SSI transaction",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTopSSITran
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getTopSSITran(char * szTranKey, TRAN_PTYPE * dpstTran)
{
	int			rv				= SUCCESS;
	int			iSize			= 0;
	TRAN_PTYPE	pstTran			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (szTranKey == NULL) || (dpstTran == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		pstTran = (TRAN_PTYPE) getDataFrmStack(szTranKey, &iSize);
		if(pstTran == NULL)
		{
			//debug_sprintf(szDbgMsg, "%s: FAILED to fetch tran from stack",__FUNCTION__);
			//APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		*dpstTran = pstTran;

		break;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: popSSITran
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int popSSITran(char * szTranKey)
{
	int			rv				= SUCCESS;
	int			iSize			= 0;
	TRAN_PTYPE	pstTran			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		pstTran = (TRAN_PTYPE) popDataFrmStack(szTranKey, &iSize);
		if(pstTran == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fetch tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Cleanup the transaction variables and de-allocate the transaction's
		 * memory */
		cleanupSSITranDtls(pstTran);
		free(pstTran);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: openNewSession
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int openNewSession(char * szSessKey)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	DATAINFO_STYPE	stDataInfo;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		if(szSessKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(&stDataInfo, 0x00, sizeof(DATAINFO_STYPE));
		stDataInfo.iDataSize = sizeof(SESSDTLS_STYPE);
		stDataInfo.initData = initSession;
		stDataInfo.cleanupData = cleanupSession;

		rv = addData(&stDataInfo, szSessKey);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to add session", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Add Session Key to Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSession
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getSession(char * szSessKey, SESSDTLS_PTYPE * dpstSess)
{
	int				rv				= SUCCESS;
	SESSDTLS_PTYPE	pstSess			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (szSessKey == NULL) || (dpstSess == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		pstSess = (SESSDTLS_PTYPE) getData(szSessKey);
		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get data from base layer",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		*dpstSess = pstSess;

		break;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: closeSession
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int closeSession(char * szSessKey)
{
	int				rv					= SUCCESS;
	int				iClean				= 1;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	SESSDTLS_PTYPE	pstTmpSessDtls		= NULL;
	SESSDTLS_PTYPE	pstSessDtls			= NULL;
	pthread_t		bapiThreadId;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		if(szSessKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = getSession(szSessKey, &pstTmpSessDtls);
		if (rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get session details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Session Details From the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			break;
		}

		/* KranthiK1:
		 * If Bapi is enabled then the we need to send the bapi packet.
		 * To reduce the amount of time it takes we take the whole session details
		 * and take a copy of the whole session details and then pass it to a new thread.
		 * The thread in turn builds the message and sends the packet to the bapi
		 * The freeing of the session details copied will be done in the thread.
		 */
		if (isBapiEnabled())
		{
			debug_sprintf(szDbgMsg, "%s: Bapi is enabled", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iClean = 0;

			pstSessDtls = (SESSDTLS_PTYPE)malloc(sizeof(SESSDTLS_STYPE));
			if(pstSessDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			memcpy(pstSessDtls, pstTmpSessDtls, sizeof(SESSDTLS_STYPE));

			rv = pthread_create(&bapiThreadId, NULL, sendBasketData, (void *)pstSessDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to start BAPI thread",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break; //Daivik - 8/Feb/2016- Coverity 67222: rv is unused and overwritten if we dont break here.
			}

		}


		/* KranthiK1:
		 * iClean is set to 1 if it is not bapi enabled
		 * If bapi is enabled then it will be set to 0.
		 * When bapi is enabled the cleaning of the memory for the
		 * payment and lineitem related items is done by the bapi thread
		 */
		rv = flushDataForKey(szSessKey, iClean);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to flush session",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Flush the Session Details From the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: cleanupSSITranDtls
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void cleanupSSITranDtls(TRAN_PTYPE pstTran)
{
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	
	if(pstTran == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	switch(pstTran->iFxn)
	{
	case SSI_ADMIN:
	case SSI_BATCH:
	case SSI_DEV:
		/* No extra data present here */
		break;

	case SSI_PYMT:
		cleanupPymtTranDlts(pstTran->data);
		break;

	case SSI_RPT:
		cleanupRptDtls(pstTran->data);
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Shoud not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;
	}

	if(pstTran->data != NULL)
	{
		free(pstTran->data);
	}

	memset(pstTran, 0x00, sizeof(TRAN_STYPE));

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: initializeTranData
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void initializeTranData(void * arg)
{
	TRAN_PTYPE	pstTran			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstTran = (TRAN_PTYPE) arg;

	memset(pstTran, 0x00, sizeof(TRAN_STYPE));

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: cleanupTranData
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void cleanupTranData(void * arg)
{
	TRAN_PTYPE	pstTran			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstTran = (TRAN_PTYPE) arg;

	if(pstTran->data != NULL)
	{
		switch(pstTran->iFxn)
		{
		case SCI_SEC:
			memset((pstTran->data), 0x00, sizeof(POSSEC_STYPE));
			break;

		case SCI_SESS:
			cleanupSession(pstTran->data);
			break;

		case SCI_LI:
			cleanUpReqLI(pstTran->data, pstTran->iCmd, pstTran->stRespDtls.szRsltCode);
			break;

		case SCI_SAF:
			cleanupSAFDtls(pstTran->data);
			break;

		case SCI_DEV:
			/*
			 * We have a expection for the LP Token Query command
			 * Since it requires online transaction, we have created
			 * the SSI tran instance instead of dev tran type
			 */
			if(pstTran->iCmd == SCI_LPTOKEN)
			{
				popSSITran(pstTran->data);
			}
			else
			{
				cleanupDevTranDtls(pstTran->data, pstTran->iCmd);
			}
			break;

		case SCI_PYMT:
		case SCI_BATCH:
		case SCI_RPT:
			popSSITran(pstTran->data);
			deleteStack(pstTran->data);

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Shoud not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		free(pstTran->data);
	}

	memset(pstTran, 0x00, sizeof(TRAN_STYPE));

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}
#if 0
/*
 * ============================================================================
 * Function Name: cleanupReqLI
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void cleanUpReqLI(void* pstData, int iCmd, char *szResultCode)
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	LI_NODE_PTYPE 	ptmpLiNode 			= NULL;
	LIINFO_PTYPE 	pstLIInfo 			= NULL;
	SHWLIINFO_PTYPE	pstShowLiInfo 		= NULL;

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

/* DAIVIK:14-11-2015 - Not Freeing this causes a leak of data that was allocated in addReqLineItems
 * This used to cause leak of upto 120 bytes per Line Item
 */
	if(iCmd == SCI_SHOW)
	{
		pstShowLiInfo = (SHWLIINFO_PTYPE)pstData;
	}
	else
	{
		pstLIInfo = (LIINFO_PTYPE)pstData;
	}

	if(pstLIInfo)
	{
		/* DAIVIK: 26-11-2015 : Please NOTE: We need to parse through the entire list of the Line Items and free each one.
		 * Earlier to the fix we used to free only the first line item and this used to cause a leak of 120 bytes for each
		 * of the remaining line items.
		 */
		//if(pstLIInfo->mainLstHead)
		while(pstLIInfo->mainLstHead)
		{
			if(pstLIInfo->mainLstHead->liData)
			{
			  /*We still have this Data , which will be freed when the close session happens
				free(pstLIInfo->mainLstHead->liData);
				For REMOVE and OVERRIDE commands we need to free this data since we are going to update what is there in session
				T_POLISETTYG1: 03/02/2016: For ADD Command if addition of line items failed then we need to free as they won't be added to session list.*/
				if(iCmd == SCI_REMOVE || iCmd == SCI_OVERRIDE || (iCmd == SCI_ADD && strcmp(szResultCode, "-1") != 0))
				{
					free(pstLIInfo->mainLstHead->liData);
				}
			}
			ptmpLiNode = pstLIInfo->mainLstHead->nextLI;
			free(pstLIInfo->mainLstHead);
			pstLIInfo->mainLstHead = ptmpLiNode;
		}
	}
//	if(pstShowLiInfo)
//	{
//		free(pstShowLiInfo);
//	}
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}
#endif

/*
 * ============================================================================
 * Function Name: cleanupReqLI
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void cleanUpReqLI(LIINFO_PTYPE pstLIInfo, int iCmd, char *szResultCode)
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
	LI_NODE_PTYPE ptmpLiNode = NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/* DAIVIK:14-11-2015 - Not Freeing this causes a leak of data that was allocated in addReqLineItems
	 * This used to cause leak of upto 120 bytes per Line Item
	 */
	if(pstLIInfo)
	{
		/* DAIVIK: 26-11-2015 : Please NOTE: We need to parse through the entire list of the Line Items and free each one.
		 * Earlier to the fix we used to free only the first line item and this used to cause a leak of 120 bytes for each
		 * of the remaining line items.
		 */
		//if(pstLIInfo->mainLstHead)
		while(pstLIInfo->mainLstHead)
		{
			if(pstLIInfo->mainLstHead->liData)
			{
				/*We still have this Data , which will be freed when the close session happens
				free(pstLIInfo->mainLstHead->liData);
				For REMOVE and OVERRIDE commands we need to free this data since we are going to update what is there in session
				T_POLISETTYG1: 03/02/2016: For ADD Command if addition of line items failed then we need to free as they won't be added to session list.*/
				if(iCmd == SCI_REMOVE || iCmd == SCI_OVERRIDE || (iCmd == SCI_ADD && strcmp(szResultCode, "-1") != 0))
				{
					free(pstLIInfo->mainLstHead->liData);
				}
			}
			ptmpLiNode = pstLIInfo->mainLstHead->nextLI;
			free(pstLIInfo->mainLstHead);
			pstLIInfo->mainLstHead = ptmpLiNode;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: cleanupSAFDtls
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void cleanupSAFDtls(SAFDTLS_PTYPE pstSAFDtls)
{
	SAFREC_PTYPE	curRecNode = NULL;

	SAFREC_PTYPE	tempRecNode = NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls->recList != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Need to Clean up SAF Record List", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		curRecNode = pstSAFDtls->recList;

		while(curRecNode != NULL)
		{
			tempRecNode = curRecNode;
			curRecNode = curRecNode->next;
			if(tempRecNode->pstPassThrghDtls != NULL)
			{
				cleanUpPassThrghDtls(tempRecNode->pstPassThrghDtls);
			}
			free(tempRecNode);
			tempRecNode = NULL;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}
		
/*
 * ============================================================================
 * Function Name: cleanupDevTranDtls
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void cleanupDevTranDtls(DEVTRAN_PTYPE pstDevTran, int iCmd)
{
	NFC_APPLE_SECT_DTLS_PTYPE 	pstNfcAppleSections;
	NFC_APPLE_SECT_DTLS_PTYPE 	pstNfcAppleSectionsTemp;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(iCmd)
	{
	case SCI_SIGN:
	case SCI_SIGN_EX:
		cleanupSigDtls(&(pstDevTran->stSigDtls));
		break;

	case SCI_EMAIL:
	case SCI_LTY:
		memset(&(pstDevTran->stLtyDtls), 0x00, sizeof(LTYDTLS_STYPE));
		break;
	case SCI_CREDIT_APP:
		memset(&(pstDevTran->stCreditAppDtls), 0x00, sizeof(CREDITAPPDTLS_STYPE));
		break;
	case SCI_QUERY_NFCINI:
		pstNfcAppleSections = pstDevTran->stNfcIniContentInfoDtls.pstApplePaySectionList;
		while(pstNfcAppleSections != NULL)
		{
			pstNfcAppleSectionsTemp = pstNfcAppleSections;
			pstNfcAppleSections = pstNfcAppleSections->next;
			free(pstNfcAppleSectionsTemp);
		}
		memset(&(pstDevTran->stNfcIniContentInfoDtls), 0x00, sizeof(NFCINI_CONTENT_INFO_STYPE));
		break;
	default:
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;
	}

	memset(pstDevTran, 0x00, sizeof(DEVTRAN_STYPE));

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: cleanupPymtTranDtls
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void cleanupPymtTranDlts(PYMTTRAN_PTYPE pstPymtTran)
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstPymtTran->pstSess != NULL)
	{
		free(pstPymtTran->pstSess);
	}

	if (pstPymtTran->pstAmtDtls != NULL)
	{
		free(pstPymtTran->pstAmtDtls);
	}

	if (pstPymtTran->pstCardDtls != NULL)
	{
		free(pstPymtTran->pstCardDtls);
	}
	if (pstPymtTran->pstEmvKeyLoadInfo != NULL)
	{
		/*Note: Don't free here inside members of pstEmvKeyLoadInfo here. 
				Its freed in procEmvDwnldInit function */				
		//Free allocated memory for storing EMV download parameters*/
		free(pstPymtTran->pstEmvKeyLoadInfo);
		pstPymtTran->pstEmvKeyLoadInfo = NULL;
	}

	if(pstPymtTran->pstEmvRespDtls != NULL)
	{
		free(pstPymtTran->pstEmvRespDtls);
	}

	if (pstPymtTran->pstPINDtls != NULL)
	{
		free(pstPymtTran->pstPINDtls);
	}

	if(pstPymtTran->pstLevel2Dtls != NULL)
	{
		free(pstPymtTran->pstLevel2Dtls);
	}

	if(pstPymtTran->pstLtyDtls != NULL)
	{
		free(pstPymtTran->pstLtyDtls);
	}

	if(pstPymtTran->pstFTranDtls != NULL)
	{
		free(pstPymtTran->pstFTranDtls);
	}

	if(pstPymtTran->pstCTranDtls != NULL)
	{
		free(pstPymtTran->pstCTranDtls);
	}

	if(pstPymtTran->pstVSPDtls != NULL)
	{
		free(pstPymtTran->pstVSPDtls);
	}

	if(pstPymtTran->pstSigDtls != NULL)
	{
		cleanupSigDtls(pstPymtTran->pstSigDtls);
		/* DAIVIK 14-11-2015- This was causing a leak of 5000 bytes per signature. 5000 bytes correspond to the Display Text memory.
		 * In case of Payment Tran we maintain pointers and the corresponding memory of signature details must be freed.
		 * We are currently freeing only the sign variable which is dynamically allocated.
		 * We are keeping the below free out of cleanupSigDtls because cleanupSigDtls is called by Device Transaction also.
		 */
		free(pstPymtTran->pstSigDtls);
	}

	if(pstPymtTran->pstEBTDtls != NULL)
	{
		free(pstPymtTran->pstEBTDtls);
	}

	if(pstPymtTran->pstFSADtls != NULL)
	{
		free(pstPymtTran->pstFSADtls);
	}

	if(pstPymtTran->pstPOSTenderDtls != NULL)
	{
		free(pstPymtTran->pstPOSTenderDtls);
	}

	if(pstPymtTran->pstVasDataDtls != NULL)
	{
		free(pstPymtTran->pstVasDataDtls)
	}

	if(pstPymtTran->pstDupDtls != NULL)
	{
		free(pstPymtTran->pstDupDtls);
	}

	if(pstPymtTran->pstCustInfoDtls != NULL)
	{
		free(pstPymtTran->pstCustInfoDtls);
	}
	if(pstPymtTran->pstRawCardDtls != NULL)
	{
		free(pstPymtTran->pstRawCardDtls);
	}
	if(pstPymtTran->pstPassThrghDtls != NULL)
	{
		cleanUpPassThrghDtls(pstPymtTran->pstPassThrghDtls);
	}

	memset(pstPymtTran, 0x00, sizeof(PYMTTRAN_STYPE));

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: cleanupRptDtls
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void cleanupRptDtls(PASSTHRU_PTYPE pstPassThru)
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstPassThru->pstMetaData != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Freeing Metadata of Passthru struct", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		freeMetaDataEx(pstPassThru->pstMetaData);
		
		free(pstPassThru->pstMetaData); //Praveen_P1: We need to free this in case of report commands since we are allocating this pointer
	}

	if(pstPassThru->szRespMsg != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Freeing Resp Msg of Passthru struct", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		free(pstPassThru->szRespMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Memseting Passthru struct", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(pstPassThru, 0x00, sizeof(PASSTHRU_STYPE));

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}


/*
 * ============================================================================
 * Function Name: cleanupSigDtls
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static void cleanupSigDtls(SIGDTLS_PTYPE pstSigDtls)
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSigDtls->szSign != NULL)
	{
		free(pstSigDtls->szSign);
	}

	memset(pstSigDtls, 0x00, sizeof(SIGDTLS_STYPE));

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}


/*
 * ============================================================================
 * Function Name: initSession
 *
 * Description	: This function is responsible for initializing the session
 * 					variables. It is a callback function being called by the
 * 					base data layer.
 *
 * Input Params	: pstSess -> reference to the session data
 *
 * Output Params: none
 * ============================================================================
 */
static void initSession(void * arg)
{
	SESSDTLS_PTYPE	pstSess			= NULL;
	time_t			now;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstSess = (SESSDTLS_PTYPE) arg;
	//CID 67224 (#1 of 1): Wrong sizeof argument (SIZEOF_MISMATCH). T_RaghavendranR1
	memset(pstSess, 0x00, sizeof(SESSDTLS_STYPE));

	/* Get the session start time stamp */
	getTime("HH:MM:SS", pstSess->szStartTS);
//	genTimeStamp(pstSess->szStartTS, 4);
	time(&now);
	pstSess->startTime = now;
	debug_sprintf(szDbgMsg, "%s:start time is %s", __FUNCTION__, pstSess->szStartTS);
	APP_TRACE(szDbgMsg);
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: cleanupSession
 *
 * Description	: This function cleans up the session data from the sytem
 * 					memory. It is a callback function being called by the base
 * 					data layer for proper cleanup of the data.
 *
 * Input Params	: pstSess -> reference to the session data
 *
 * Output Params: none
 * ============================================================================
 */
static void cleanupSession(void * arg)
{
	SESSDTLS_PTYPE	pstSess			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstSess = (SESSDTLS_PTYPE) arg;

	if(pstSess->pstLIInfo != NULL)
	{
		cleanupLineItems(pstSess->pstLIInfo);
		/* DAIVIK:14-11-2015 - Not freeing the below pointer was causing a memory leak of around 52 bytes of data that was
		 * that was allocated in updateLIDtlsInSession.
		 */
		free(pstSess->pstLIInfo);
	}

	if(pstSess->pstPYMNTInfo != NULL)
	{
		cleanupPaymentItems(pstSess->pstPYMNTInfo);
	}

	memset(pstSess, 0x00, sizeof(SESSDTLS_STYPE)); //Praveen_P1: changed sizeof to SESSDTLS_STYPE, since SESSDTLS_PTYPE looks suspicious

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: cleanupLineItems
 *
 * Description	: Cleans up the line items by de-allocating the memory
 * 					allocated for them
 *
 * Input Params	: pstLIInfo -> meta information about the line item list
 *
 * Output Params: none
 * ============================================================================
 */
void cleanupLineItems(LIINFO_PTYPE pstLIInfo)
{
	LI_NODE_PTYPE	pstCurNode		= NULL;
	LI_NODE_PTYPE	pstTmpNode		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstCurNode = pstLIInfo->mainLstHead;

	while(pstCurNode != NULL)
	{
		if(pstCurNode->liData != NULL)
		{
			free(pstCurNode->liData);
		}

		pstTmpNode = pstCurNode;
		pstCurNode = pstCurNode->nextLI;
		free(pstTmpNode);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: cleanupPaymentItems
 *
 * Description	: Cleans up the Payment items by de-allocating the memory
 * 					allocated for them
 *
 * Input Params	: pstPYMNTInfo -> meta information about the line item list
 *
 * Output Params: none
 * ============================================================================
 */
void cleanupPaymentItems(PYMNTINFO_PTYPE pstPYMNTInfo)
{
	PYMNT_NODE_PTYPE tmpPtr1	 = NULL;
	PYMNT_NODE_PTYPE tmpPtr2	 = NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);


	if (pstPYMNTInfo != NULL)
	{
		tmpPtr1 = pstPYMNTInfo->mainLstHead;
		while (tmpPtr1 != NULL)
		{
			tmpPtr2 = tmpPtr1;
			tmpPtr1 = tmpPtr1->next;
			free(tmpPtr2);
		}
		free(pstPYMNTInfo);
		pstPYMNTInfo = NULL;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s:No payment details to clean", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * Function Name: loadPassThrghFields
 *
 * Description	: This function is responsible for loading the pass through fields from the file & store all the necessary details into memory
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int loadPassThrghFieldsDtls()
{
	int				rv					= SUCCESS;
	int				iCnt				= 1;
	int				iStatus				= 0;
	int				iReqTagCnt			= 0;
	int				iResTagCnt			= 0;
	int				iMaxLen				= 0;
	int				iMinLen				= 0;
	int				iLen				= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";
	char			szKey[256]			= "";
	char			*pszVal				= NULL;
	char			*cPtr				= NULL;
	char			*cTemp				= NULL;
	dictionary 		*dict				= NULL;
	KEYVAL_STYPE	stLocKeyVal;
	STR_DEF_PTYPE	pstLocStrDef;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(&stPassThrgDtls, 0x00, sizeof(PASSTHRG_FIELDS_STYPE));
	memset(&stLocKeyVal, 0x00, sizeof(KEYVAL_STYPE));
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		/* Check if file exist */
		rv = doesFileExist(SCA_PASSTHRGH_FIELDS_FILE_NAME);
		if(rv != SUCCESS)
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "SCA Pass Through File Is Not Present.");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}
			rv = SUCCESS;
			break;
		}

		/* Load the ini file in the parser library */
		dict = iniparser_load(SCA_PASSTHRGH_FIELDS_FILE_NAME);
		if(dict == NULL)
		{
			/* Unable to load the ini file */
			debug_sprintf(szDbgMsg, "%s: Unable to parse ini file [%s], errno [%d]",__FUNCTION__, SCA_PASSTHRGH_FIELDS_FILE_NAME, errno);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed To Load SCA Pass Through INI File.");
				strcpy(szAppLogDiag, "Please Check The Format Of SCA Pass Through INI File.");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, szAppLogDiag);
			}
			rv = FAILURE;
			break;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Loaded [%s] successfully",__FUNCTION__, SCA_PASSTHRGH_FIELDS_FILE_NAME);
			APP_TRACE(szDbgMsg);
		}

		/* Get the total number of tags present in REQUEST section */
		while(1)
		{
			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "REQUEST:tag%d", iCnt);
			iStatus = iniparser_find_entry(dict, szKey);
			if(iStatus)
			{
				iReqTagCnt++;
				iCnt++;
			}
			else
			{
				iCnt = 1;
				break;
			}
		}
		/* Get the total number of tags present in RESPONSE section */
		while(1)
		{
			memset(szKey, 0x00, sizeof(szKey));
			sprintf(szKey, "RESPONSE:tag%d", iCnt);
			iStatus = iniparser_find_entry(dict, szKey);
			if(iStatus)
			{
				iResTagCnt++;
				iCnt++;
			}
			else
			{
				iCnt = 1;
				break;
			}
		}

		stPassThrgDtls.iReqTagsCnt = iReqTagCnt;
		stPassThrgDtls.iResTagsCnt = iResTagCnt;
		debug_sprintf(szDbgMsg, "%s: Total [%d] Request fields and [%d] Response fields are configured for pass through",
						__FUNCTION__, iReqTagCnt, iResTagCnt);
		APP_TRACE(szDbgMsg);

		if(iReqTagCnt > 0)
		{
			/* Allocate memory for request tag list*/
			stPassThrgDtls.pstReqTagList = (KEYVAL_PTYPE) malloc(iReqTagCnt * sizeof(KEYVAL_STYPE));
			if(stPassThrgDtls.pstReqTagList == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: malloc failed",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			// Initialize the memory
			memset(stPassThrgDtls.pstReqTagList, 0x00, iReqTagCnt * sizeof(KEYVAL_STYPE));

			/* Get all the XML tag details from REQUEST sections which needs to be passed along from SCI to SSI */
			while(1)
			{
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "REQUEST:tag%d", iCnt);
				pszVal = iniparser_getstring(dict, szKey, NULL);
				if(pszVal == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: End of REQUEST Section",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					iCnt = 1;
					break;
				}
				debug_sprintf(szDbgMsg, "%s: Key[%s] Value[%s]",__FUNCTION__, szKey, pszVal);
				APP_TRACE(szDbgMsg);
				memset(&stLocKeyVal, 0x00, sizeof(KEYVAL_STYPE));

				// parse the tag related details like name,value type etc & store them in memory
				// Format for value part of key tag<n> is:
				// <TAG_NAME>,<MANDATORY_FLAG>,<VALUE_TYPE>,<MIN_LEN>,<MAX_LEN>

				if((cPtr = strtok(pszVal, ",")) == NULL)		//TAG_NAME
				{
					rv = FAILURE;
					break;
				}
				iLen = strlen(cPtr);
				cTemp = (char*) malloc(iLen + 1);
				if(cTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: malloc failed",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}
				memset(cTemp, 0x00, iLen + 1);
				strcpy(cTemp, cPtr);
				stLocKeyVal.key = cTemp;
				if((cPtr = strtok(NULL, ",")) == NULL)		//MANDATORY_FLAG
				{
					rv = FAILURE;
					break;
				}
				if(! strcmp(cPtr, "TRUE"))
				{
					stLocKeyVal.isMand = PAAS_TRUE;
				}
				else
				{
					stLocKeyVal.isMand = PAAS_FALSE;
				}
				if((cPtr = strtok(NULL, ",")) == NULL)		//VALUE_TYPE
				{
					rv = FAILURE;
					break;
				}
				stLocKeyVal.valType = atoi(cPtr);

				if((cPtr = strtok(NULL, ",")) == NULL)		//MIN_LEN
				{
					rv = FAILURE;
					break;
				}
				iMinLen = atoi(cPtr);

				if((cPtr = strtok(NULL, ",")) == NULL)		//MAX_LEN
				{
					rv = FAILURE;
					break;
				}
				iMaxLen = atoi(cPtr);

				if(stLocKeyVal.valType == VARLIST)
				{
					if((cPtr = strtok(NULL, ",")) == NULL)		//Var List Info Details
					{
						rv = FAILURE;
						break;
					}

					rv = loadPassThroughVarlist(dict, &stLocKeyVal, cPtr);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to Load VarList Details for [%s]",__FUNCTION__, stLocKeyVal.key);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled == 1)
						{
							sprintf(szAppLogData, "Failed To Parse VarList Info Details INI File for Key [%s].", cPtr);
							strcpy(szAppLogDiag, "Please Check The Format Of VarList Details in SCA Pass Through INI File.");
							addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, szAppLogDiag);
						}

						break;
					}
				}
				else if((iMinLen == 0 && iMaxLen == 0) || (stLocKeyVal.valType == SINGLETON) || (stLocKeyVal.valType == BOOLEAN))
				{
					stLocKeyVal.valMetaInfo = NULL;
				}
				else
				{
					pstLocStrDef = (STR_DEF_PTYPE) malloc(sizeof(STR_DEF_STYPE));
					if(pstLocStrDef == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: malloc failed",__FUNCTION__);
						APP_TRACE(szDbgMsg);
						rv = FAILURE;
						break;
					}
					memset(pstLocStrDef, 0x00, sizeof(STR_DEF_STYPE));
					pstLocStrDef->minLen = iMinLen;
					pstLocStrDef->maxLen = iMaxLen;
					stLocKeyVal.valMetaInfo = pstLocStrDef;
				}
				// copy the details into memory for this XML tag
				memcpy(stPassThrgDtls.pstReqTagList + iCnt - 1, &stLocKeyVal, sizeof(KEYVAL_STYPE));
				iCnt++;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Request Section is not present",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}


		if(rv != SUCCESS)
		{
			// INI file is not in correct format
			debug_sprintf(szDbgMsg, "%s: Request Section is not in correct format",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed To Parse SCA Pass Through INI File.");
				strcpy(szAppLogDiag, "Please Check The Format Of SCA Pass Through INI File.");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, szAppLogDiag);
			}
			break;
		}

		iCnt = 1;
		if(iResTagCnt > 0)
		{
			/* Allocate memory for response tag list*/
			stPassThrgDtls.pstResTagList = (KEYVAL_PTYPE) malloc(iResTagCnt * sizeof(KEYVAL_STYPE));
			if(stPassThrgDtls.pstResTagList == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: malloc failed",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			// Initialize the memory
			memset(stPassThrgDtls.pstResTagList, 0x00, iResTagCnt * sizeof(KEYVAL_STYPE));

			/* Get all the XML tag details from RESPONSE sections which needs to be passed along from SSI to SCI */
			while(1)
			{
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "RESPONSE:tag%d", iCnt);
				pszVal = iniparser_getstring(dict, szKey, NULL);
				if(pszVal == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: End of RESPONSE Section",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
				memset(&stLocKeyVal, 0x00, sizeof(KEYVAL_STYPE));
				// parse the tag name &  store them in memory
				iLen = strlen(pszVal);
				cTemp = (char*) malloc(iLen + 1);
				if(cTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: malloc failed",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					rv = FAILURE;
					break;
				}

				memset(cTemp, 0x00, iLen + 1);
				strcpy(cTemp, pszVal);
				stLocKeyVal.key = cTemp;
				stLocKeyVal.isMand = PAAS_FALSE;
				stLocKeyVal.valType = SINGLETON;
				stLocKeyVal.valMetaInfo = NULL;
				stLocKeyVal.value = NULL;

				// copy the details into memory for this XML tag
				memcpy(stPassThrgDtls.pstResTagList + iCnt - 1, &stLocKeyVal, sizeof(KEYVAL_STYPE));
				debug_sprintf(szDbgMsg, "%s: Key[%s] Value[%s]",__FUNCTION__, szKey, pszVal);
				APP_TRACE(szDbgMsg);
				iCnt++;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s:Response Section is not present",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

#if 0
		for(iCnt = 0; iCnt < iReqTagCnt; iCnt++)
		{
			pstLocStrDef = (STR_DEF_PTYPE)(stPassThrgDtls.pstReqTagList + iCnt)->valMetaInfo;
			debug_sprintf(szDbgMsg, "%s: key[%s] valType[%d] mandFlag[%d]", __FUNCTION__,
											(stPassThrgDtls.pstReqTagList+iCnt)->key,
											(stPassThrgDtls.pstReqTagList+iCnt)->valType,
											(stPassThrgDtls.pstReqTagList+iCnt)->isMand
											);
			APP_TRACE(szDbgMsg);
			if(pstLocStrDef != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: minLen[%d] maxLen[%d]", __FUNCTION__,
															pstLocStrDef->minLen,
															pstLocStrDef->maxLen);
				APP_TRACE(szDbgMsg);
			}

		}

		for(iCnt = 0; iCnt < iResTagCnt; iCnt++)
		{
			debug_sprintf(szDbgMsg, "%s: key[%s] val[%s] valType[%d] mandFlag[%d]", __FUNCTION__,
											(stPassThrgDtls.pstResTagList+iCnt)->key,
											(char*)(stPassThrgDtls.pstResTagList+iCnt)->value,
											(stPassThrgDtls.pstResTagList+iCnt)->valType,
											(stPassThrgDtls.pstResTagList+iCnt)->isMand);
			APP_TRACE(szDbgMsg);
		}
#endif
		break;
	}
	if(dict != NULL)
	{
		iniparser_freedict(dict);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: loadPassThroughVarlist
 *
 * Description	: This function gets the details of Var List type in PassThrough File
 *
 * Input Params	: none
 *
 * Output Params: int rv SUCCESS/FAILURE
 * ============================================================================
 */
static int loadPassThroughVarlist(dictionary * dict, KEYVAL_PTYPE	pstLocKeyVal, char * pszVarListName)
{
	int					rv 					= SUCCESS;
	int					iLen				= 0;
	int					iCnt				= 1;
	int					iListInfoCnt		= 0;
	int					iValType			= -1;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[300]	= "";
	char				szAppLogDiag[300]	= "";
	char				szKey[256]			= "";
	char				*pszValTemp			= NULL;
	char				*pszVal				= NULL;
	char				*cPtr				= NULL;
	char				*cTemp				= NULL;
	VARLST_INFO_PTYPE	pstVarLstInfoLoc	= NULL;
	VARLST_INFO_PTYPE	pstVarLstInfoTemp	= NULL;
	VARLST_INFO_STYPE	stVarLstInfoLoc;
	VARLST_INFO_STYPE	stVarLstInfoSingleElem;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	/* Allocate memory for VARLIST INFO for Single Elements in the VARLIST request
	 * Even if Single Elements are not present in the VARLIST, the value will be by default set to NULL*/
	iListInfoCnt = 1;
	pstVarLstInfoLoc = (VARLST_INFO_PTYPE) malloc(sizeof(VARLST_INFO_STYPE));
	if(pstVarLstInfoLoc == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(pstVarLstInfoLoc, 0x00, sizeof(VARLST_INFO_STYPE)); // Memset the allocated Varlist Info,
	memset(&stVarLstInfoSingleElem, 0x00, sizeof(VARLST_INFO_STYPE));

	while(1)
	{
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:tag%d", pszVarListName, iCnt);
		pszVal = iniparser_getstring(dict, szKey, NULL);
		if(pszVal == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: End of [%s] Section", __FUNCTION__, pszVarListName);
			APP_TRACE(szDbgMsg);

			break;
		}
		debug_sprintf(szDbgMsg, "%s: Key[%s] Value[%s]",__FUNCTION__, szKey, pszVal);
		APP_TRACE(szDbgMsg);

		pszValTemp = strdup(pszVal);
		rv = getValueTypeofVarListElement(pszValTemp, &iValType);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to get the Value Type of Varlist Element. Please Check the VarList Passthrough Format",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(pstVarLstInfoLoc != NULL)
			{
				free(pstVarLstInfoLoc);
			}

			break;
		}
		free(pszValTemp);
		pszValTemp = NULL;

		memset(&stVarLstInfoLoc, 0x00, sizeof(VARLST_INFO_STYPE));

		if(iValType == VARLIST)
		{
			iListInfoCnt++;

			debug_sprintf(szDbgMsg, "%s: VARLIST Noticed inside VARLIST",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstVarLstInfoTemp = (VARLST_INFO_PTYPE) realloc(pstVarLstInfoLoc, sizeof(VARLST_INFO_STYPE) * iListInfoCnt);
			if(pstVarLstInfoTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			memset(pstVarLstInfoTemp + iListInfoCnt - 1, 0x00, sizeof(VARLST_INFO_STYPE)); // Memset the re-allocated Varlist Info, keeping the original already copied data as it is
			pstVarLstInfoLoc = pstVarLstInfoTemp;

			if((cPtr = strtok(pszVal, ",")) == NULL)		//VarList TAG_NAME
			{
				rv = FAILURE;
				break;
			}
			iLen = strlen(cPtr);

			debug_sprintf(szDbgMsg, "%s: VARList Node Name [%s]",__FUNCTION__, cPtr);
			APP_TRACE(szDbgMsg);

			cTemp = (char*) malloc(iLen + 1);
			if(cTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			memset(cTemp, 0x00, iLen + 1);
			strcpy(cTemp, cPtr);

			stVarLstInfoLoc.nodeType = iListInfoCnt - 1;
			stVarLstInfoLoc.szNodeStr = cTemp;

			debug_sprintf(szDbgMsg, "%s: pszVal [%s]",__FUNCTION__, pszVal);
			APP_TRACE(szDbgMsg);

			if((cPtr = strtok(NULL, ",")) == NULL)		//Mandatory Flag
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get Mandatory Flag [%s]",__FUNCTION__, cPtr);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			if((cPtr = strtok(NULL, ",")) == NULL)		//Value type
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get Value type [%s]",__FUNCTION__, cPtr);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			if((cPtr = strtok(NULL, ",")) == NULL)		//Minimum Length
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get Minimum length [%s]",__FUNCTION__, cPtr);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			if((cPtr = strtok(NULL, ",")) == NULL)		//Max Length
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get Maximum length [%s]",__FUNCTION__, cPtr);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			if((cPtr = strtok(NULL, ",")) == NULL)		//KeyList Section Name
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get keylist section name [%s]",__FUNCTION__, cPtr);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			debug_sprintf(szDbgMsg, "%s: VarLst Info keyList Name [%s]",__FUNCTION__, cPtr);
			APP_TRACE(szDbgMsg);

			rv = loadKeyListDetails(dict, &stVarLstInfoLoc, cPtr);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Load KeyList Details for Node [%s]",__FUNCTION__, stVarLstInfoLoc.szNodeStr);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Failed To Parse KeyValue List Details for VarList [%s] in INI File.", stVarLstInfoLoc.szNodeStr);
					strcpy(szAppLogDiag, "Please Check The Format Of VarList Details in SCA Pass Through INI File.");
					addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, szAppLogDiag);
				}

				break;
			}

			// copy the details into memory for this XML tag
			memcpy((pstVarLstInfoLoc + iListInfoCnt - 1), &stVarLstInfoLoc, sizeof(VARLST_INFO_STYPE));
		}
		else if(iValType != -1)
		{
			debug_sprintf(szDbgMsg, "%s: Single Elements in VARLIST Noticed. We will gather all the Signgle Elements in this VARLIST and copy to the main VARInfo List at the End",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			stVarLstInfoSingleElem.nodeType = 0;
			stVarLstInfoSingleElem.szNodeStr = NULL;

			rv = addVarListSingleElements(pszVal, &stVarLstInfoSingleElem);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to add KeyList Details for Single Elements in VARLIST [%s]",__FUNCTION__, pszVarListName);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Failed To Parse KeyValue List Details for Single Elements in VarList [%s]", pszVarListName);
					strcpy(szAppLogDiag, "Please Check The Format Of VarList Details in SCA Pass Through INI File.");
					addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, szAppLogDiag);
				}

				break;
			}
		}

		iCnt++;
	}

	if(rv != FAILURE)
	{
		debug_sprintf(szDbgMsg, "%s:iCnt [%d]",__FUNCTION__, iCnt);
		APP_TRACE(szDbgMsg);


		if(stVarLstInfoSingleElem.keyList != NULL)
		{
			// copy the Single Elements VarList info details into memory. This will be copied into the first index of the VarList Info.
			//Memory is already allocated at the start of the function.
			memcpy(pstVarLstInfoLoc, &stVarLstInfoSingleElem, sizeof(VARLST_INFO_STYPE));
		}

		//After loading the VarList Info of the passthrough Request, Terminate the Info details with NULL and -1.
		iListInfoCnt++;
		memset(&stVarLstInfoLoc, 0x00, sizeof(VARLST_INFO_STYPE));
		stVarLstInfoLoc.nodeType = -1;
		stVarLstInfoLoc.iElemCnt = 0;
		stVarLstInfoLoc.szNodeStr = NULL;
		stVarLstInfoLoc.keyList = NULL;
		pstVarLstInfoTemp = (VARLST_INFO_PTYPE) realloc(pstVarLstInfoLoc, sizeof(VARLST_INFO_STYPE) * iListInfoCnt);
		if(pstVarLstInfoTemp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		memset(pstVarLstInfoTemp + iListInfoCnt - 1, 0x00, sizeof(VARLST_INFO_STYPE)); // Memset the re-allocated Varlist Info, keeping the original already copied data as it is
		memcpy((pstVarLstInfoTemp + iListInfoCnt - 1), &stVarLstInfoLoc, sizeof(VARLST_INFO_STYPE));
		pstVarLstInfoLoc = pstVarLstInfoTemp;

		//Finally Assign the value meta Info to the original Passthrough request.
		pstLocKeyVal->valMetaInfo = pstVarLstInfoLoc;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadKeyListDetails
 *
 * Description	: This function gets the details of Var List type in PassThrough File
 *
 * Input Params	: none
 *
 * Output Params: int rv SUCCESS/FAILURE
 * ============================================================================
 */
static int loadKeyListDetails(dictionary * dict, VARLST_INFO_PTYPE pstVarListInfo, char * pszKeyListName)
{
	int					rv 					= SUCCESS;
	int					iLen				= 0;
	int					iCnt				= 1;
	int					iKeyListCnt			= 0;
	int					iStatus				= 0;
	int					iMinLen				= 0;
	int					iMaxLen				= 0;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szAppLogData[300]	= "";
	char				szAppLogDiag[300]	= "";
	char				szKey[256]			= "";
	char				*pszVal				= NULL;
	char				*cPtr				= NULL;
	char				*cTemp				= NULL;
	KEYVAL_STYPE		stLocKeyVal;
	STR_DEF_PTYPE		pstLocStrDef		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	/* Get the total number of tags present in keyList Details section */
	while(1)
	{
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:tag%d", pszKeyListName, iCnt);
		iStatus = iniparser_find_entry(dict, szKey);
		if(iStatus)
		{
			iKeyListCnt++;
			iCnt++;
		}
		else
		{
			iCnt = 1;
			break;
		}
	}

	pstVarListInfo->iElemCnt = iKeyListCnt;
	pstVarListInfo->keyList = (KEYVAL_PTYPE) malloc(sizeof(KEYVAL_STYPE) * iKeyListCnt);
	if(pstVarListInfo->keyList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;

		return rv;
	}
	memset(pstVarListInfo->keyList, 0x00, sizeof(sizeof(KEYVAL_STYPE) * iKeyListCnt));

	while(1)
	{
		memset(szKey, 0x00, sizeof(szKey));
		sprintf(szKey, "%s:tag%d", pszKeyListName, iCnt);
		pszVal = iniparser_getstring(dict, szKey, NULL);
		if(pszVal == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: End of [%s] Section", __FUNCTION__, pszKeyListName);
			APP_TRACE(szDbgMsg);

			break;
		}
		debug_sprintf(szDbgMsg, "%s: Key[%s] Value[%s] iKeyListCnt [%d]",__FUNCTION__, szKey, pszVal, iKeyListCnt);
		APP_TRACE(szDbgMsg);

		memset(&stLocKeyVal, 0x00, sizeof(KEYVAL_STYPE));

		// parse the tag related details like name,value type etc & store them in memory
		// Format for value part of key tag<n> is:
		// <TAG_NAME>,<MANDATORY_FLAG>,<VALUE_TYPE>,<MIN_LEN>,<MAX_LEN>

		if((cPtr = strtok(pszVal, ",")) == NULL)		//TAG_NAME
		{
			rv = FAILURE;
			break;
		}
		iLen = strlen(cPtr);
		cTemp = (char*) malloc(iLen + 1);
		if(cTemp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(cTemp, 0x00, iLen + 1);
		strcpy(cTemp, cPtr);
		stLocKeyVal.key = cTemp;
		if((cPtr = strtok(NULL, ",")) == NULL)		//MANDATORY_FLAG
		{
			rv = FAILURE;
			break;
		}
		if(! strcmp(cPtr, "TRUE"))
		{
			stLocKeyVal.isMand = PAAS_TRUE;
		}
		else
		{
			stLocKeyVal.isMand = PAAS_FALSE;
		}
		if((cPtr = strtok(NULL, ",")) == NULL)		//VALUE_TYPE
		{
			rv = FAILURE;
			break;
		}
		stLocKeyVal.valType = atoi(cPtr);

		if((cPtr = strtok(NULL, ",")) == NULL)		//MIN_LEN
		{
			rv = FAILURE;
			break;
		}
		iMinLen = atoi(cPtr);

		if((cPtr = strtok(NULL, ",")) == NULL)		//MAX_LEN
		{
			rv = FAILURE;
			break;
		}
		iMaxLen = atoi(cPtr);

		if(stLocKeyVal.valType == VARLIST)
		{
			if((cPtr = strtok(NULL, ",")) == NULL)		//Var List Info Details
			{
				rv = FAILURE;
				break;
			}

			debug_sprintf(szDbgMsg, "%s: VarList Info Section [%s]",__FUNCTION__, cPtr);
			APP_TRACE(szDbgMsg);

			rv = loadPassThroughVarlist(dict, &stLocKeyVal, cPtr);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Load VarList Details for [%s]",__FUNCTION__, stLocKeyVal.key);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					sprintf(szAppLogData, "Failed To Parse VarList Info Details INI File for Key [%s].", cPtr);
					strcpy(szAppLogDiag, "Please Check The Format Of VarList Details in SCA Pass Through INI File.");
					addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, szAppLogDiag);
				}

				break;
			}
		}
		else if((iMinLen == 0 && iMaxLen == 0) || (stLocKeyVal.valType == SINGLETON) || (stLocKeyVal.valType == BOOLEAN))
		{
			stLocKeyVal.valMetaInfo = NULL;
		}
		else
		{
			pstLocStrDef = (STR_DEF_PTYPE) malloc(sizeof(STR_DEF_STYPE));
			if(pstLocStrDef == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			memset(pstLocStrDef, 0x00, sizeof(STR_DEF_STYPE));
			pstLocStrDef->minLen = iMinLen;
			pstLocStrDef->maxLen = iMaxLen;
			stLocKeyVal.valMetaInfo = pstLocStrDef;
		}
		// copy the details into memory for this XML tag
		memcpy(pstVarListInfo->keyList + iCnt - 1, &stLocKeyVal, sizeof(KEYVAL_STYPE));
		iCnt++;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addVarListSingleElements
 *
 * Description	: This function adds the single Elements of VarList PassThrough request and updates the keylist of Single Element KeyList structure in VARLIST
 *
 * Input Params	: none
 *
 * Output Params: int rv SUCCESS/FAILURE
 * ============================================================================
 */
static int addVarListSingleElements(char * pszVal, VARLST_INFO_PTYPE pstVarListSingleElem)
{
	int					rv 					= SUCCESS;
	int					iLen				= 0;
	int					iCnt				= 1;
	int					iMinLen				= 0;
	int					iMaxLen				= 0;
	char				*cPtr				= NULL;
	char				*cTemp				= NULL;
	KEYVAL_PTYPE		pstLocKeyVal;
	KEYVAL_STYPE		stLocKeyVal;
	STR_DEF_PTYPE		pstLocStrDef		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Enter [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	pstVarListSingleElem->iElemCnt++;
	iCnt = pstVarListSingleElem->iElemCnt;
	pstLocKeyVal = (KEYVAL_PTYPE) realloc(pstVarListSingleElem->keyList, sizeof(KEYVAL_STYPE) * iCnt);
	if(pstLocKeyVal == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;

		return rv;
	}
	pstVarListSingleElem->keyList = pstLocKeyVal;
	memset(pstVarListSingleElem->keyList + iCnt -1, 0x00, sizeof(KEYVAL_STYPE));

	while(1)
	{
		debug_sprintf(szDbgMsg, "%s: Value[%s]",__FUNCTION__, pszVal);
		APP_TRACE(szDbgMsg);

		memset(&stLocKeyVal, 0x00, sizeof(KEYVAL_STYPE));

		// parse the tag related details like name,value type etc & store them in memory
		// Format for value part of key tag<n> is:
		// <TAG_NAME>,<MANDATORY_FLAG>,<VALUE_TYPE>,<MIN_LEN>,<MAX_LEN>

		if((cPtr = strtok(pszVal, ",")) == NULL)		//TAG_NAME
		{
			rv = FAILURE;
			break;
		}
		iLen = strlen(cPtr);
		cTemp = (char*) malloc(iLen + 1);
		if(cTemp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(cTemp, 0x00, iLen + 1);
		strcpy(cTemp, cPtr);
		stLocKeyVal.key = cTemp;
		if((cPtr = strtok(NULL, ",")) == NULL)		//MANDATORY_FLAG
		{
			rv = FAILURE;
			break;
		}

		if(! strcmp(cPtr, "TRUE"))
		{
			stLocKeyVal.isMand = PAAS_TRUE;
		}
		else
		{
			stLocKeyVal.isMand = PAAS_FALSE;
		}
		if((cPtr = strtok(NULL, ",")) == NULL)		//VALUE_TYPE
		{
			rv = FAILURE;
			break;
		}
		stLocKeyVal.valType = atoi(cPtr);

		if((cPtr = strtok(NULL, ",")) == NULL)		//MIN_LEN
		{
			rv = FAILURE;
			break;
		}
		iMinLen = atoi(cPtr);

		if((cPtr = strtok(NULL, ",")) == NULL)		//MAX_LEN
		{
			rv = FAILURE;
			break;
		}
		iMaxLen = atoi(cPtr);

		if((iMinLen == 0 && iMaxLen == 0) || (stLocKeyVal.valType == SINGLETON) || (stLocKeyVal.valType == BOOLEAN))
		{
			stLocKeyVal.valMetaInfo = NULL;
		}
		else
		{
			pstLocStrDef = (STR_DEF_PTYPE) malloc(sizeof(STR_DEF_STYPE));
			if(pstLocStrDef == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc Failed",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			memset(pstLocStrDef, 0x00, sizeof(STR_DEF_STYPE));
			pstLocStrDef->minLen = iMinLen;
			pstLocStrDef->maxLen = iMaxLen;
			stLocKeyVal.valMetaInfo = pstLocStrDef;
		}
		// copy the details into memory for this XML tag
		memcpy(pstVarListSingleElem->keyList + iCnt - 1, &stLocKeyVal, sizeof(KEYVAL_STYPE));
		break;

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getValueTypeofVarListElement
 *
 * Description	: This function will determine what is the value type for the tag inside the VARLIST Passthrough Element.
 * There is a possibility that the Element can be VARLIST or SINGLETON, NUMERIC or STRING
 *
 * Input Params	: Passthrough tag string and Value Type pointer
 *
 * Output Params: SUCCESS/FAILUE. The Value Type of the tag is filled.
 * ============================================================================
 */
static int getValueTypeofVarListElement(char * pszVal, int *piValType)
{
	int		rv 		= SUCCESS;
	char *	cPtr	= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	while(1)
	{
		if(pszVal == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input string is NULL.",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}


		if((cPtr = strtok(pszVal, ",")) == NULL)		//VarList TAG_NAME
		{
			rv = FAILURE;
			break;
		}

		if((cPtr = strtok(NULL, ",")) == NULL)		//Mandatory Flag
		{
			rv = FAILURE;
			break;
		}

		if((cPtr = strtok(NULL, ",")) == NULL)		//Value type
		{
			rv = FAILURE;
			break;
		}

		*piValType = atoi(cPtr);
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Value Type Returned [%d]", __FUNCTION__, *piValType);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPassThrgFieldsDtls
 *
 * Description	: This function gets the details of pass through fields
 *
 * Input Params	: none
 *
 * Output Params: PASSTHRG_FIELDS_PTYPE: Pointer to Pass through details structure
 * ============================================================================
 */
PASSTHRG_FIELDS_PTYPE getPassThrgFieldsDtls(void)
{
	return (&stPassThrgDtls);
}

/*
 * ============================================================================
 * Function Name: cleanUpPassThrghDtls
 *
 * Description	: This function clears the pass through details.
 *
 * Input Params	: PASSTHRG_FIELDS_PTYPE
 *
 * Output Params: NONE
 * ============================================================================
 */
static void cleanUpPassThrghDtls(PASSTHRG_FIELDS_PTYPE pstPassThrghDtls)
{
	int				iCnt			= 0;
	VAL_LST_PTYPE	valLstPtr		= NULL;
	VAL_NODE_PTYPE	valNodePtr		= NULL;
	VAL_NODE_PTYPE	dataPtr			= NULL;
	METADATA_STYPE	stLocMetaData;
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&stLocMetaData, 0x00, METADATA_SIZE);

	if(pstPassThrghDtls->pstReqTagList != NULL)
	{
		for(iCnt = 0; iCnt < pstPassThrghDtls->iReqTagsCnt; iCnt++)
		{
			if((pstPassThrghDtls->pstReqTagList + iCnt)->key != NULL)
			{
				free((pstPassThrghDtls->pstReqTagList + iCnt)->key);
			}

			if((pstPassThrghDtls->pstReqTagList + iCnt)->valType == VARLIST)
			{
				valLstPtr = (VAL_LST_PTYPE) ((pstPassThrghDtls->pstReqTagList + iCnt)->value);

				if(valLstPtr != NULL)
				{
					valNodePtr = valLstPtr->start;

					while(valNodePtr != NULL)
					{
						stLocMetaData.iTotCnt = valNodePtr->elemCnt;
						stLocMetaData.keyValList = valNodePtr->elemList;

						freeMetaData(&stLocMetaData);

						if(valNodePtr->szVal != NULL)
						{
							free(valNodePtr->szVal);
						}
						/* DAIVIK:14-11-2015- The absence of the below free , can cause a memory leak in cases where the SCI Data consists
						 * of list within a list structure.
						 */
						if(valNodePtr->elemList != NULL)
						{
							free(valNodePtr->elemList);
						}

						dataPtr = valNodePtr;
						valNodePtr = valNodePtr->next;
						free(dataPtr);
					}

					free(valLstPtr);
				}
			}
			else
			{
				if((pstPassThrghDtls->pstReqTagList + iCnt)->value != NULL)
				{
					free((pstPassThrghDtls->pstReqTagList + iCnt)->value);
				}
			}

		}
	}

	if(pstPassThrghDtls->pstResTagList != NULL)
	{
		for(iCnt = 0; iCnt < pstPassThrghDtls->iResTagsCnt; iCnt++)
		{
			if((pstPassThrghDtls->pstResTagList + iCnt)->key != NULL)
			{
				free((pstPassThrghDtls->pstResTagList + iCnt)->key);
			}
			if((pstPassThrghDtls->pstResTagList + iCnt)->value != NULL)
			{
				free((pstPassThrghDtls->pstResTagList + iCnt)->value);
			}
			if((pstPassThrghDtls->pstResTagList + iCnt)->valMetaInfo != NULL)
			{
				free((pstPassThrghDtls->pstResTagList + iCnt)->valMetaInfo);
			}
		}
	}

	if(pstPassThrghDtls->pstReqTagList != NULL)
	{
		free(pstPassThrghDtls->pstReqTagList);
	}
	if(pstPassThrghDtls->pstResTagList != NULL)
	{
		free(pstPassThrghDtls->pstResTagList);
	}
	free(pstPassThrghDtls);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}
/*
 * ============================================================================
 * End of file dataSytem.c
 * ============================================================================
 */
