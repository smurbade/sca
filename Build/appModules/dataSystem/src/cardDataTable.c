/******************************************************************
*                       cardDataTable.c                           *
*******************************************************************
* Application: Point Solutions                                    *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <syslog.h>

#include "common/common.h"
#include "db/cardDataTable.h"
#include "db/cdtExtern.h"
#include "common/utils.h"
#include "bLogic/bLogicCfgDef.h"
#include "appLog/appLogAPIs.h"

#define MAX_TRACK2_LEN		39
#define MAX_TRACK1_LEN		78

#define CDT_DEFAULT_FILE	"./flash/cdt.txt"
#define CDT_REC_OLD_FILE	"/mnt/flash/userdata/share/CDT.dat"
#define CDT_REC_NEW_FILE	"./flash/CDT.dat"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

/* Static variables */
CDT_STYPE		 stCDT;
BETRECORD_PTYPE	 pstBET;


/* static functions declarations */
static int	getLabelConstant(char *, int *);
static int	getLabelName(char *, char *);
static int	loadCDTFromDfltFile(CDT_PTYPE);
static int	loadCDTFromDat(CDT_PTYPE);
static int	addRecToCDT(CDT_PTYPE, CDTREC_PTYPE);
static int	getRecCntForCDT(int *);
static int	parseCDTLine(char *, CDT_PTYPE);
static int	createCDTRecs(CDT_PTYPE);
static int	fillCDTRecs(CDT_PTYPE);
static int	saveCDTRecs(CDT_PTYPE);
static int	enableDisableCR(CDTREC_PTYPE, char *, PAAS_BOOL);
static int	addFldValToCDTRecs(char *, int, CDT_PTYPE);
static int	modifyCDTRec(CDTREC_DTLS_PTYPE, int, char *);
static int	patchCode();
static int	readCDTRecsFrmOldDat(CDT_PTYPE);
static void flushCDT(CDT_PTYPE);
static void formatFieldValue(char *);
//static void parseForFieldsinSTBRecord(char *);

/* For parsing and validation of cards */
static int parseTrack1Data(char *, char**, char**, CARDDTLS_PTYPE);
static int parseTrack2Data(char *, char**, char**, CARDDTLS_PTYPE);
static int parseNSaveCardInfo(CARDDTLS_PTYPE, char **, char**, char**);

int parseNSaveSTBInfo(CARDDTLS_PTYPE , char *);

//static int getCDTBinRec(CDTREC_PTYPE, CARDDTLS_PTYPE);
int validateCardData(CDTREC_DTLS_PTYPE, CARDDTLS_PTYPE);
int validateCardData_1(CARDDTLS_PTYPE);
static int performLUHNCheck(char *);
static int validatePAN(char *, CDTREC_DTLS_PTYPE);
static int validateCVV(char *);

int getCDTBinRec(CDTREC_PTYPE, CARDDTLS_PTYPE);
#if 0
static int validateAndLoadBETFile(char *);
static int validateBETRecord(char *, int);
static int addBETRecord(long, long);
#endif

#ifdef DEBUG
static void printCDT(CDT_PTYPE);
static void printCDTRecDtls(CDTREC_DTLS_PTYPE);
#endif

extern int validateTrackData(int, char *);
extern int determineCardType(CARDDTLS_PTYPE , char * ); //TODO add it to some header file

/*
 * ============================================================================
 * Function Name: initCDT
 *
 * Description	: This API is responsible for initializing the CDT after
 * 					reading from the CDT.txt or CDT.dat file 
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int initCDT()
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Initialize the place holder for CDT */
		memset(&stCDT, 0x00, sizeof(stCDT));

		/* Run the patch code */
		rv = patchCode();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Patch code FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Check if the default CDT file (txt file) is present. */
		if(doesFileExist(CDT_DEFAULT_FILE) == SUCCESS)
		{
			/* This would be the case when the application is being installed
			 * for the first time in the terminal. The default file has to
			 * crunched and the records hence read have to be stored in our own
			 * format for further processing. The default file would be deleted
			 * after that. From next reboot onwards, the other file would be
			 * read by the application for accessing the CDT records */
			rv = loadCDTFromDfltFile(&stCDT);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to load CDT from default",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Load Card Data Table File");
					addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
				}

				break;
			}

			/* Store the CDT table in the persistent memory in own format */
			rv = saveCDTRecs(&stCDT);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to update CDT dat file",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Update Card Data Table File");
					addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
				}
				break;
			}

			/* Delete the txt file after the successfull loading of CDT from
			 * the file */
			if(SUCCESS != deleteFile(CDT_DEFAULT_FILE))
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to remove default CDT file",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Remove Default Card Data Table File");
					addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
				}

			}
		}
		else if(doesFileExist(CDT_REC_NEW_FILE) == SUCCESS)
		{
			/* The other file exists. Read the records from the file and store
			 * records in the sytem memory */
			rv = loadCDTFromDat(&stCDT);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to load recs from CDT.dat",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Failed to Load Records From Card Data Table File");
					addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
				}


				break;
			}
		}
		else
		{
			/* No information/file exists on the terminal regarding the CDT
			 * records. This is an erroneous case and the application should not
			 * proceed any further */

			debug_sprintf(szDbgMsg, "%s: No information exists regarding CDT",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "No Information Available Regarding Card Data Table File");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}


			rv = FAILURE;
		}

		break;
	}

	if(rv == SUCCESS)
	{
#ifdef DEBUG
		/* For testing print the contents of the CDT in the logs */
		printCDT(&stCDT);
#endif
	}
	else
	{
		/* Flush out any allocated memory to prevent memory leakage */
		flushCDT(&stCDT);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseNSaveSTBInfo
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int parseNSaveSTBInfo(CARDDTLS_PTYPE pstCardDtls, char *szSTBData)
{
	int		rv					= SUCCESS;
	int		iIndex				= 0;
	int		iRecIndex			= 0;
	int		iCurrentIndex		= 0;
	int		i					= 0;
	int		iTempIndex			= 0;
	int		iLength				= 0;
	char *	pszRecord 			= NULL;
	char *	pszTemp				= NULL;
	char *	pszBuffer1			= NULL;
	char * 	pszBuffer2			= NULL;
	char	szRSStr[2]			= "";
	char	szGSStr[2]			= "";
	char	szRecType[2]		= "";
	char	szTemp[30]			= "";
	char	szSTBRecords[3][200];


#ifdef DEBUG
	char			szDbgMsg[2048]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(&pstCardDtls->stBinDtls[B_TYPE_REC_INDEX], 0x00, sizeof(BINDTLS_STYPE));
		memset(&pstCardDtls->stBinDtls[C_TYPE_REC_INDEX], 0x00, sizeof(BINDTLS_STYPE));
		memset(&pstCardDtls->stBinDtls[D_TYPE_REC_INDEX], 0x00, sizeof(BINDTLS_STYPE));

		if(szSTBData == NULL || strlen(szSTBData) == 0)
		{
			debug_sprintf(szDbgMsg, "%s: No STB Data, then Zero records found for this card in the STB file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_NOT_IN_BINRANGE;

			break;
		}

		/* Data is present, need to parse for it */

		if(strlen(szSTBData) < 2000)
		{
			debug_sprintf(szDbgMsg, "%s: STB Data [%s]", __FUNCTION__, szSTBData);
			APP_TRACE(szDbgMsg);
		}

		/*
		 * BIN_DTL_TYPE=C<GS>BIN_DTL_BIN_LEN=1<GS>BIN_DTL_BIN_LOW=4000000000000000<GS>BIN_DTL_BIN_HIGH=4999999999999999<GS>BIN_DTL_CARD_PROD=V<GS>BIN_DTL_PAN_LEN=16<GS>BIN_DTL_REMAINING_REC=16<RS>
		 * BIN_DTL_TYPE=B<GS>BIN_DTL_BIN_LEN=1<GS>BIN_DTL_BIN_LOW=4000000000000000<GS>BIN_DTL_BIN_HIGH=4999999999999999<GS>BIN_DTL_CARD_PROD=V<GS>BIN_DTL_PAN_LEN=16<GS>BIN_DTL_REMAINING_REC=16<RS>
		 * BIN_DTL_TYPE=D<GS>BIN_DTL_BIN_LEN=1<GS>
		 * Each Record will be separated by <RS>
		 * Each Field with in the record separated by <GS>
		 */

		memset(szRSStr, 0x00, sizeof(szRSStr));
		memset(szGSStr, 0x00, sizeof(szGSStr));

		szGSStr[0] = 0x1D;
		szRSStr[0] = 0x1E;

		pszRecord = strtok(szSTBData, szRSStr);
		while(pszRecord != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Record [%s]", __FUNCTION__, pszRecord);
			APP_TRACE(szDbgMsg);

			if(iIndex == 3)
			{
				debug_sprintf(szDbgMsg, "%s: Expecting only three STB records, not considering other records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			memset(szSTBRecords[iIndex], 0x00, sizeof(szSTBRecords[iIndex]));

			strcpy(szSTBRecords[iIndex], pszRecord);

			debug_sprintf(szDbgMsg, "%s: Stored Record [%s]", __FUNCTION__, szSTBRecords[iIndex]);
			APP_TRACE(szDbgMsg);

			iIndex++;

			pszRecord = strtok(NULL, szRSStr);
		}

		for(i = 0; i < iIndex; i++)
		{
			/* Get the Type of the Record */
			pszTemp = NULL;
			pszTemp = strstr(szSTBRecords[i], "BIN_DTL_TYPE=");
			if(pszTemp != NULL)
			{
				pszTemp = pszTemp + strlen("BIN_DTL_TYPE="); //pszTemp will point to Type

				memset(szRecType, 0x00, sizeof(szRecType));
				strncpy(szRecType, pszTemp, 1);

				debug_sprintf(szDbgMsg, "%s: Record Type [%s]", __FUNCTION__, szRecType);
				APP_TRACE(szDbgMsg);

				if(strcmp(szRecType, "B") == 0)
				{
					iRecIndex = B_TYPE_REC_INDEX;
					pstCardDtls->stBinDtls[iRecIndex].iTypePresent = 1;
				}
				else if(strcmp(szRecType, "C") == 0)
				{
					iRecIndex = C_TYPE_REC_INDEX;
					pstCardDtls->stBinDtls[iRecIndex].iTypePresent = 1;
				}
				else if(strcmp(szRecType, "D") == 0)
				{
					iRecIndex = D_TYPE_REC_INDEX;
					pstCardDtls->stBinDtls[iRecIndex].iTypePresent = 1;
				}

				debug_sprintf(szDbgMsg, "%s: Record Index [%d]", __FUNCTION__, iRecIndex);
				APP_TRACE(szDbgMsg);
			}


			if((pszTemp = strstr(szSTBRecords[i], "BIN_DTL_TYPE=")) != NULL)
			{
				pstCardDtls->stBinDtls[iRecIndex].cType = pszTemp[strlen("BIN_DTL_TYPE=")];

				debug_sprintf(szDbgMsg, "%s: Card Type [%c]", __FUNCTION__, pstCardDtls->stBinDtls[iRecIndex].cType);
				APP_TRACE(szDbgMsg);
			}

			if((pszBuffer1 = strstr(szSTBRecords[i], "BIN_DTL_BIN_LEN=")) != NULL)
			{
				pszBuffer1 	= pszBuffer1 + strlen("BIN_DTL_BIN_LEN=");
				pszBuffer2	= strchr(pszBuffer1, szGSStr[0]);
				iLength		= pszBuffer2 - pszBuffer1;

				memset(szTemp, 0x00, sizeof(szTemp));
				strncpy(szTemp, pszBuffer1, iLength);

				pstCardDtls->stBinDtls[iRecIndex].iBinLen = atoi(szTemp);

				debug_sprintf(szDbgMsg, "%s: BIN Length [%d]", __FUNCTION__, pstCardDtls->stBinDtls[iRecIndex].iBinLen);
				APP_TRACE(szDbgMsg);
			}

			if((pszBuffer1 = strstr(szSTBRecords[i], "BIN_DTL_BIN_LOW=")) != NULL)
			{
				pszBuffer1 	= pszBuffer1 + strlen("BIN_DTL_BIN_LOW=");
				pszBuffer2	= strchr(pszBuffer1, szGSStr[0]);
				iLength		= pszBuffer2 - pszBuffer1;

				memset(szTemp, 0x00, sizeof(szTemp));
				strncpy(szTemp, pszBuffer1, iLength);

				pstCardDtls->stBinDtls[iRecIndex].lBinLow = atoll(szTemp);

				debug_sprintf(szDbgMsg, "%s: BIN Low [%lld]", __FUNCTION__, pstCardDtls->stBinDtls[iRecIndex].lBinLow);
				APP_TRACE(szDbgMsg);
			}

			if((pszBuffer1 = strstr(szSTBRecords[i], "BIN_DTL_BIN_HIGH=")) != NULL)
			{
				pszBuffer1 	= pszBuffer1 + strlen("BIN_DTL_BIN_HIGH=");

				pszBuffer2	= strchr(pszBuffer1, szGSStr[0]);

				iLength		= pszBuffer2 - pszBuffer1;

				memset(szTemp, 0x00, sizeof(szTemp));
				strncpy(szTemp, pszBuffer1, iLength);

				pstCardDtls->stBinDtls[iRecIndex].lBinHigh  = atoll(szTemp);

				debug_sprintf(szDbgMsg, "%s: BIN High [%lld]", __FUNCTION__, pstCardDtls->stBinDtls[iRecIndex].lBinHigh);
				APP_TRACE(szDbgMsg);

			}

			if((pszTemp = strstr(szSTBRecords[i], "BIN_DTL_CARD_PROD=")) != NULL)
			{
				pstCardDtls->stBinDtls[iRecIndex].cProd = pszTemp[strlen("BIN_DTL_CARD_PROD=")];

				debug_sprintf(szDbgMsg, "%s: Prod Type [%c]", __FUNCTION__, pstCardDtls->stBinDtls[iRecIndex].cProd);
				APP_TRACE(szDbgMsg);
			}

			if((pszBuffer1 = strstr(szSTBRecords[i], "BIN_DTL_PAN_LEN=")) != NULL)
			{
				pszBuffer1 	= pszBuffer1 + strlen("BIN_DTL_PAN_LEN=");
				pszBuffer2	= strchr(pszBuffer1, szGSStr[0]);
				iLength		= pszBuffer2 - pszBuffer1;

				memset(szTemp, 0x00, sizeof(szTemp));
				strncpy(szTemp, pszBuffer1, iLength);

				pstCardDtls->stBinDtls[iRecIndex].iPanLen  = atoi(szTemp);

				debug_sprintf(szDbgMsg, "%s: PAN Length [%d]", __FUNCTION__, pstCardDtls->stBinDtls[iRecIndex].iPanLen);
				APP_TRACE(szDbgMsg);
			}

			if((pszBuffer1 = strstr(szSTBRecords[i], "BIN_DTL_REMAINING_REC=")) != NULL)
			{
				/*
				 * For C Type : BIN_DTL_REMAINING_REC=<Max PAN Length>
				 * For B Type : BIN_DTL_REMAINING_REC=<BIN-DETL-CARD-IND>(Space if not present)<BIN-DETL-FSA-IND>(space if not present)
				 * 									  <BIN-DTL-PREPAID-IND>
				 *                                    <num_entries>(Entries db_nw_id and pinless_ind recur up to 20 times)
				 *                                    <nw array.db nw id><nw array.pinless ind>
				 */
				if(iRecIndex == C_TYPE_REC_INDEX)
				{
					pszBuffer1 	= pszBuffer1 + strlen("BIN_DTL_REMAINING_REC=");
					pszBuffer2	= strchr(pszBuffer1, szGSStr[0]);

					memset(szTemp, 0x00, sizeof(szTemp));
					if(pszBuffer2 != NULL)
					{
						iLength		= pszBuffer2 - pszBuffer1;

						strncpy(szTemp, pszBuffer1, iLength);
					}
					else
					{
						strcpy(szTemp, pszBuffer1);
					}

					pstCardDtls->stBinDtls[iRecIndex].iMaxPanLen = atoi(szTemp);

					debug_sprintf(szDbgMsg, "%s: Max PAN Length [%d]", __FUNCTION__, pstCardDtls->stBinDtls[iRecIndex].iMaxPanLen);
					APP_TRACE(szDbgMsg);
				}
				else if(iRecIndex == B_TYPE_REC_INDEX)
				{
					iCurrentIndex = strlen("BIN_DTL_REMAINING_REC=");

					pstCardDtls->stBinDtls[iRecIndex].cCardInd = pszBuffer1[iCurrentIndex];

					debug_sprintf(szDbgMsg, "%s: Card Indicator [%c]", __FUNCTION__, pstCardDtls->stBinDtls[iRecIndex].cCardInd);
					APP_TRACE(szDbgMsg);

					iCurrentIndex++;

					pstCardDtls->stBinDtls[iRecIndex].cFSAInd = pszBuffer1[iCurrentIndex];

					debug_sprintf(szDbgMsg, "%s: FSA Indicator [%c]", __FUNCTION__, pstCardDtls->stBinDtls[iRecIndex].cFSAInd);
					APP_TRACE(szDbgMsg);

					iCurrentIndex++;

					pstCardDtls->stBinDtls[iRecIndex].cPrePaidInd = pszBuffer1[iCurrentIndex];

					debug_sprintf(szDbgMsg, "%s: Prepaid Indicator [%c]", __FUNCTION__, pstCardDtls->stBinDtls[iRecIndex].cPrePaidInd);
					APP_TRACE(szDbgMsg);

					iCurrentIndex++;

					memset(szTemp, 0x00, sizeof(szTemp));
					strncpy(szTemp, pszBuffer1+iCurrentIndex, 2); //Copying the Num entries Field

					pstCardDtls->stBinDtls[iRecIndex].iNumEntries = atoi(szTemp);

					debug_sprintf(szDbgMsg, "%s: Num Entries [%d]", __FUNCTION__, pstCardDtls->stBinDtls[iRecIndex].iNumEntries);
					APP_TRACE(szDbgMsg);

					iCurrentIndex = iCurrentIndex + 2; //Since num entries is 2 byte size

					for(iTempIndex = 0; iTempIndex < pstCardDtls->stBinDtls[iRecIndex].iNumEntries; iTempIndex++)
					{
						memset(szTemp, 0x00, sizeof(szTemp));
						strncpy(szTemp, pszBuffer1+iCurrentIndex, 2); //Copying the db nw id

						pstCardDtls->stBinDtls[iRecIndex].stNewtDtls[iTempIndex].iNetworkIndicator = atoi(szTemp);

						debug_sprintf(szDbgMsg, "%s: Network Indicator [%d]", __FUNCTION__, pstCardDtls->stBinDtls[iRecIndex].stNewtDtls[iTempIndex].iNetworkIndicator);
						APP_TRACE(szDbgMsg);

						iCurrentIndex = iCurrentIndex + 2; //Since network indicator is 2 byte size

						pstCardDtls->stBinDtls[iRecIndex].stNewtDtls[iTempIndex].cPinlessInd = pszBuffer1[iCurrentIndex];

						debug_sprintf(szDbgMsg, "%s: Card Type [%c]", __FUNCTION__, pstCardDtls->stBinDtls[iRecIndex].stNewtDtls[iTempIndex].cPinlessInd);
						APP_TRACE(szDbgMsg);

						iCurrentIndex++;
					}
				}
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: parseForFieldsinSTBRecord
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void parseForFieldsinSTBRecord(char *pszRecord)
{
	char *	pszField 			= NULL;
	char 	pszTempRecord[200]	= "";
	char	szGSStr[2]			= "";
#ifdef DEBUG
	char			szDbgMsg[2048]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szGSStr, 0x00, sizeof(szGSStr));
	szGSStr[0] = 0x1D;

	memset(pszTempRecord, 0x00, sizeof(pszTempRecord));
	strcpy(pszTempRecord, pszRecord);


	debug_sprintf(szDbgMsg, "%s: TempRecord [%s]", __FUNCTION__, pszTempRecord);
	APP_TRACE(szDbgMsg);


	pszField = strtok(pszTempRecord, szGSStr);
	while(pszField != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Field [%s]", __FUNCTION__, pszField);
		APP_TRACE(szDbgMsg);

		pszField = strtok(NULL, szGSStr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return;
}
#endif
/*
 * ============================================================================
 * Function Name: getPANFromTrack
 *
 * Description	: Used to extract PAN from Track Data. We pass the pointers for Track 1
 * and Track 2 Data.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getPANFromTrack(char ** szTrks,char *szTrkPAN,int len)
{
	int			rv					= SUCCESS;
	char 		*szTrack2 			= NULL;
	char 		*szTrack1 			= NULL;
	char 		*cCurPtr			= NULL;
	char 		*cNxtPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(szTrks != NULL)
	{
		szTrack2 	= szTrks[1];
		szTrack1 	= szTrks[0];
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s:Track Data Not Obtained...", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = FAILURE;
		return rv;
	}
	if(szTrack2 != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Track 2 Present, get PAN", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(szTrack2[0] == ';') //Start Sentinel
		{
			cCurPtr = &szTrack2[1];
		}
		else
		{
			cCurPtr = szTrack2;
		}
		/*
		 * --------------
		 * Store the PAN
		 * --------------
		 */
		cNxtPtr = strchr(cCurPtr, '=');
		if(cNxtPtr == NULL)
		{
			/* FIXME: Currently done under assumption that some gift cards dont
			 * have the '=' sentinel in their track 2 */
			strncpy(szTrkPAN, cCurPtr,len);
		}
		else
		{
			if((cNxtPtr - cCurPtr) > len)
			{
				memcpy(szTrkPAN, cCurPtr, len);
			}
			else
			{
				memcpy(szTrkPAN, cCurPtr, cNxtPtr - cCurPtr);
			}
		}
		return rv;
	}
	cCurPtr = NULL;
	cNxtPtr = NULL;
	if(szTrack1 != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Track 2 Present, get PAN", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(szTrack1[0] == '%') //Start sentinel for Track1
		{
			cCurPtr = &szTrack1[1];
		}
		else
		{
			cCurPtr = szTrack1;
		}
		/* Skip the format character */
		/*
		 * Need to check if the first char is Format code or not
		 */
		//cCurPtr = szTrack1;
		if(!(cCurPtr[0] >= '0' && cCurPtr[0] <= '9')) //If it is not digit then skip that character
		{
			cCurPtr = cCurPtr + 1;
		}
		/*
		 * -----------
		 * Store PAN
		 * -----------
		 */
		cNxtPtr = strchr(cCurPtr, '^');
		if(cNxtPtr == NULL)
		{
			strncpy(szTrkPAN, cCurPtr,len);
		}
		else
		{
			if((cNxtPtr - cCurPtr) > len)
			{
				memcpy(szTrkPAN, cCurPtr, len);
			}
			else
			{
				memcpy(szTrkPAN, cCurPtr, cNxtPtr - cCurPtr);
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	return rv;
}
/*
 * ============================================================================
 * Function Name: getCardDtls
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int getCardDtls(CARDDTLS_PTYPE pstCardDtls, char ** szTrks, char** szRSATracks, char ** szVsdEncData, char * szTranAmt,
															PAAS_BOOL bValidate, char *szSTBData)
{
	int				rv					= SUCCESS;
	int				iLen				= 0;
	char*			pszRSAFingerPrint	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/* Assuming when this API is called, the Card Data Table is formed and
	 * filled (CDT has to be filled as part of startup process) */

	/* Also assuming the pstCardDtls structure being passed to this function
	 * would already have some details already filled in. */

	while(1)
	{
		if((pstCardDtls == NULL) || (szTranAmt == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstCardDtls->bManEntry != PAAS_TRUE)
		{
			/* Assuming that in case of manual entry, the card details like
			 * PAN, expiry date (encr and clear) and CVV would be already
			 * populated in pstCardDtls. In case of SWIPE/TAP, these details
			 * need to be filled from the track information */

			if(szTrks == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Tracks cant be NULL",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			rv = parseNSaveCardInfo(pstCardDtls, szTrks, szRSATracks, szVsdEncData);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to parse card data",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}


			/* Copying the eparms data*/
			if (szTrks[3] != NULL)
			{
				iLen = strlen(szTrks[3]);
				if (iLen > 0)
				{
					memcpy(pstCardDtls->szEncPayLoad, szTrks[3], iLen);
				}
			}
		}
		else
		{	// For manual transactions
			if((getEncryptionType() == RSA_ENC) && (strlen(pstCardDtls->szEncBlob) > 0))
			{
				pszRSAFingerPrint = getRSAFingerPrint();
				strcpy(pstCardDtls->szEncPayLoad, pszRSAFingerPrint);
			}
			//MukeshS3: For VSD encryption, No need to do anything here as we already copied encryption payload while parsing for manual entry
		}

		/* If the clear date is not populated then perhaps the card data
		 * (manual or swipe) was not encrypted */
		iLen = strlen(pstCardDtls->szClrYear);
		if(iLen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: Expiry Details are not populated",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			strcpy(pstCardDtls->szClrYear, pstCardDtls->szExpYear);
			strcpy(pstCardDtls->szClrMon, pstCardDtls->szExpMon);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Expiry Details are populated",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

#ifdef DEVDEBUG //Praveen_P1: not logging clear expiry details
		debug_sprintf(szDbgMsg, "%s: Clear Year[%s], Clear Month[%s]",__FUNCTION__, pstCardDtls->szClrYear, pstCardDtls->szClrMon);
		APP_TRACE(szDbgMsg);
#endif

		/*
		 * If the Spin The Bin is enabled
		 * we get the STB Data from the FA which we need to parse
		 */
		if( isSTBLogicEnabled() )
		{
			if(bValidate == PAAS_FALSE && szSTBData == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Validation is not required and no STB data",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			rv = parseNSaveSTBInfo(pstCardDtls, szSTBData);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting STB details for given card", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			rv = determineCardType(pstCardDtls, szTranAmt);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while determining card type based on STB details for given card", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			/* Validate card data */
			rv = validateCardData_1(pstCardDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Card data FAILED validation",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else //Following will be executed when STB is not enabled
		{
			/* Stop further execution if the validation of card data is not
			 * required */
			if(bValidate != PAAS_TRUE)
			{
				debug_sprintf(szDbgMsg, "%s: Validation not required",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			/* Do a bin range check and get the CDT record with matching bin range
			 * for the current PAN in the card details structure */

			rv = validateAndUpdateCardDtls(pstCardDtls,szTranAmt);

		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: isKohlsChargeCard
 *
 * Description	: This function is used to check whether this card is a Kohls Card
 *
 * Input Params	:
 *
 * Output Params: TRUE/FALSE
 * ============================================================================
 */
int isKohlsChargeCard(CARDDTLS_PTYPE pstCardDtls)
{
	int				rvTemp  = 0;
	PAAS_BOOL		rv		= PAAS_FALSE;
	CDTREC_STYPE	stCDTRec;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCardDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	memset(&stCDTRec, 0x00, sizeof(CDTREC_SIZE));
	rvTemp = getCDTBinRec(&stCDTRec, pstCardDtls);
	if(rvTemp != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to locate satisfying CDT rec",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}
	debug_sprintf(szDbgMsg, "%s: Card Label for this CDT range is [%s]", __FUNCTION__, stCDTRec.recDtls.szCardLbl);
	APP_TRACE(szDbgMsg);

	if(strcmp(stCDTRec.recDtls.szCardLbl, "KOHLS") == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Its KOHLS card, setting KohlsChargecard to TRUE", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = PAAS_TRUE;
	}
	else
	{
		rv = PAAS_FALSE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/* Validation used to happen at the end of card capture, but since we did not have payment type at that point, the card type would not be set
 * , hence validation of card details needs to happen after obtaining the payment type. Also validateCardData function ensures that we dont
 * do the validation for EMV Transaction.
 * Also, the SAF floor limits and SAF allowed fields needs to be set according to the CDT file , irrespective of whether it is EMV or non EMV.
 * Payment Media is not required to be set from the CDT, because AIDList.txt sets the Payment Media.
 */
int validateAndUpdateCardDtls(CARDDTLS_PTYPE pstCardDtls, char * szTranAmt)
{

	int				rv					= SUCCESS;
	double			fAmt				= 0.0F;
	double			fTranAmt			= 0.0F;
	CDTREC_STYPE	stCDTRec;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(&stCDTRec, 0x00, sizeof(CDTREC_SIZE));
	rv = getCDTBinRec(&stCDTRec, pstCardDtls);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to locate satisfying CDT rec",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}
	debug_sprintf(szDbgMsg, "%s: Card Label for this CDT range is [%s]", __FUNCTION__, stCDTRec.recDtls.szCardLbl);
	APP_TRACE(szDbgMsg);

	//Praveen_P1: Adding to store the payment media for this card which is Card Label from the CDT
	/* Storing the payment media from the CDT */

	/* We will be using the Card Label from CDT for Payment Media , only if it is non EMV Transaction */
	if(!pstCardDtls->bEmvData)
	{
		strcpy(pstCardDtls->szPymtMedia, stCDTRec.recDtls.szCardLbl);
		//Fix for PTMX-1554 - Buffer Overflow caused incorrect value to be returned to POS in CARD_ABBRV field
		strncpy(pstCardDtls->szCardAbbrv, stCDTRec.recDtls.szCardAbbr,sizeof(pstCardDtls->szCardAbbrv) - 1);
	}

	debug_sprintf(szDbgMsg, "%s: Card Label for this card is %s, Card Abbrv [%s]", __FUNCTION__, pstCardDtls->szPymtMedia, pstCardDtls->szCardAbbrv);
	APP_TRACE(szDbgMsg);

	if(strcmp(pstCardDtls->szPymtMedia, "KOHLS") == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Its KOHLS card, setting KohlsChargecard to TRUE", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pstCardDtls->bKohlsChargeCard = PAAS_TRUE;
	}
	else
	{
		//debug_sprintf(szDbgMsg, "%s: Its NON-KOHLS card", __FUNCTION__);
		//APP_TRACE(szDbgMsg); //Praveen_P1: We need not print that its non-Kohls card since every card we will be logging this statement

		pstCardDtls->bKohlsChargeCard = PAAS_FALSE;
	}

	/* Validate card data */
	rv = validateCardData(&(stCDTRec.recDtls), pstCardDtls);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Card data FAILED validation",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

	/* Populate card data (some more details) */
	fTranAmt = atof(szTranAmt);

	/* If SAF is allowed for this transaction */
	fAmt = atof(stCDTRec.recDtls.szSAFFlrLim);

	/*Storing the saf flor limit for future check for the saf during split tender*/
	pstCardDtls->fSafFlrLimit = fAmt;

	debug_sprintf(szDbgMsg, "%s: Saf Floor Lim[%lf], Tran amount [%lf]",__FUNCTION__, fAmt, fTranAmt);
	APP_TRACE(szDbgMsg);

	if(fTranAmt < fAmt)
	{
		pstCardDtls->bSAFAllwd = PAAS_TRUE;

		debug_sprintf(szDbgMsg, "%s: CDT allows SAF for this transaction",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		pstCardDtls->bSAFAllwd = PAAS_FALSE;

		debug_sprintf(szDbgMsg, "%s: CDT doesnt allow SAF for this tran",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/* If signature capture is required for this transaction */
	fAmt = atof(stCDTRec.recDtls.szSigFlrLim);

	/*Storing the sig flor limit for future check for the signature req during split tender*/
	pstCardDtls->fSigFlrLimit = fAmt;

	debug_sprintf(szDbgMsg, "%s: Sig Floor Lim[%lf], Tran amount [%lf]",__FUNCTION__, fAmt, fTranAmt);
	APP_TRACE(szDbgMsg);

	if(fTranAmt >= fAmt)
	{
		pstCardDtls->bSignReqd = PAAS_TRUE;

		debug_sprintf(szDbgMsg, "%s: Sign is required", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		pstCardDtls->bSignReqd = PAAS_FALSE;

		debug_sprintf(szDbgMsg, "%s: Sign not required", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: modifyCDT
 *
 * Description	: This API provides functionality to modify the records of CDT
 *
 * Input Params	: iRecNo -> index of record to be modified
 * 				  pszField -> name of the field to be modified
 * 				  pszVal -> value to be updated for the field
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int modifyCDT(int iRecNo, char * pszField, char * pszVal)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iLblNo			= 0;
	CDTREC_PTYPE	curRecPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Assuming when this API is called, the Card Data Table is formed and
	 * filled (CDT has to be filled as part of startup process) */

	while(1)
	{
		/* Check if the record number is correct */
		if( (iRecNo < 1) || (iRecNo > stCDT.iRecCnt) )
		{
			debug_sprintf(szDbgMsg, "%s: Invalid rec no [%d] (valid 1 - %d)",
										__FUNCTION__, iRecNo, stCDT.iRecCnt);
			APP_TRACE(szDbgMsg);

			rv = CDT_RECORD_NOT_FOUND;
			break;
		}

		rv = getLabelConstant(pszField, &iLblNo);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to validate label",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(iLblNo == NOT_IN_USE)
		{
			debug_sprintf(szDbgMsg, "%s: Unknown Label; not updating record",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Updating label no [%d] for record no [%d]",
												__FUNCTION__, iRecNo, iLblNo);
		APP_TRACE(szDbgMsg);
 
		/* Update the CDT record */
		curRecPtr = stCDT.cdtRecsHead;
		for(iCnt = 1; iCnt < iRecNo; iCnt++)
		{
			curRecPtr = curRecPtr->nextRec;
		}

		rv = modifyCDTRec(&(curRecPtr->recDtls), iLblNo, pszVal);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to modify CDT record",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateCardRangeField
 *
 * Description	: This API provides functionality to enable disable card range
 *
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int updateCardRangeField(char * pszVal, char * pszLbl)
{
	int			rv				= SUCCESS;
	int			iVal			= 0;
	PAAS_BOOL	bDisableCR		= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	// CID-67189: 2-Feb-16: MukeshS3: pszLbl can be empty
	if(pszVal == NULL || pszLbl == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: --- NULL params passed ---", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SUCCESS;
	}

	while(1)
	{
		iVal = atoi(pszVal);
		if(iVal == 0)
		{
			/* Disable the card range */
			bDisableCR = PAAS_TRUE;
		}
		else
		{
			/* Enable the card range */
			bDisableCR = PAAS_FALSE;
		}

		rv = enableDisableCR(stCDT.cdtRecsHead, pszLbl, bDisableCR);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: commitCDTChanges
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int commitCDTChanges()
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	rv = saveCDTRecs(&stCDT);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: patchCode
 *
 * Description	: This function is to be used for moving the already existing
 * 					CDT records in persistent memory (old format) to a new
 * 					location in memory (new format). It will provide
 * 					compatibility with the ver 2.18. 
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int patchCode()
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( (doesFileExist(CDT_REC_OLD_FILE) == SUCCESS) &&
							(doesFileExist(CDT_REC_NEW_FILE) != SUCCESS) )
	{
		/* Read the records from the old file (with different format), convert
		 * them to the new format and then write all these formatted records
		 * into the new file */
		rv = readCDTRecsFrmOldDat(&stCDT);
		if(rv == SUCCESS)
		{
			rv = saveCDTRecs(&stCDT);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to update new CDT dat file",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to read CDT from old dat file",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
		}
	}

	if(rv == SUCCESS)
	{
		/* Delet the old file once the patching is successfully completed */
		deleteFile(CDT_REC_OLD_FILE);
	}

	/* CDT has to be loaded again anyway in the main code, so flush it for now
	 * in the patch code else there will be double the records in CDT */
	flushCDT(&stCDT);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: readCDTRecsFrmOldDat
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int readCDTRecsFrmOldDat(CDT_PTYPE pstCDT)
{
	int					rv				= SUCCESS;
	int					iCnt			= 0;
	FILE *				fp				= NULL;
	OLD_CDTREC_STYPE	stOldRec;
	CDTREC_STYPE		stCDTRec;
#ifdef DEBUG
	char				szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	fp = fopen(CDT_REC_OLD_FILE, "rb");
	if(fp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to open [%s]", __FUNCTION__,
															CDT_REC_OLD_FILE);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		/* Read the record (in old format) one by one from the old CDT dat file
		 * and convert it to the new format before storing it in the CDT (RAM)*/
		while(!feof(fp))
		{
			memset(&stOldRec, 0x00, OLD_CDTREC_SIZE);

			rv = fread(&stOldRec, OLD_CDTREC_SIZE, 1, fp);
			if(rv == 1)
			{
				memset(&stCDTRec, 0x00, CDTREC_SIZE);

				/* Populate the new record's values from the old one */
				stCDTRec.recDtls.iCardType = stOldRec.iCardType;
				stCDTRec.recDtls.iMinPANLen = stOldRec.iPANMinLength;
				stCDTRec.recDtls.iMaxPANLen = stOldRec.iPANMaxLength;
				stCDTRec.recDtls.iAVSCode = stOldRec.iAVSCode;
				stCDTRec.recDtls.iTipDscnt = stOldRec.iTipDiscount;
				stCDTRec.recDtls.iCVV2 = stOldRec.iCVV2Code;

				strcpy(stCDTRec.recDtls.szPANLo, stOldRec.szPANLow);
				strcpy(stCDTRec.recDtls.szPANHi, stOldRec.szPANHigh);
				strcpy(stCDTRec.recDtls.szCardAbbr, stOldRec.szCardAbbrevation);
				strcpy(stCDTRec.recDtls.szCardLbl, stOldRec.szCardLabel);
				strcpy(stCDTRec.recDtls.szIPCLbl, stOldRec.szIPCLabel);
				strcpy(stCDTRec.recDtls.szIPCProcId, stOldRec.szIPCProcessor);
				strcpy(stCDTRec.recDtls.szTrkRqd, stOldRec.szTrackRequired);
				strcpy(stCDTRec.recDtls.szSAFFlrLim, stOldRec.szSAFFloorLimit);
				strcpy(stCDTRec.recDtls.szSigFlrLim, stOldRec.szSigFloorLimit);
				strcpy(stCDTRec.recDtls.szGiftMinAmt, stOldRec.szGiftMinAmount);
				strcpy(stCDTRec.recDtls.szGiftMaxAmt, stOldRec.szGiftMaxAmount);

				if(stOldRec.iLUHNCheckRequired == 1)
				{
					stCDTRec.recDtls.bLUHNChkRqd = PAAS_TRUE;
				}
				else
				{
					stCDTRec.recDtls.bLUHNChkRqd = PAAS_FALSE;
				}

				if(stOldRec.iExpiryDateRequired == 1)
				{
					stCDTRec.recDtls.bExpDateChkRqd = PAAS_TRUE;
				}
				else
				{
					stCDTRec.recDtls.bExpDateChkRqd = PAAS_FALSE;
				}

				if(stOldRec.iManualEntryAllowed == 1)
				{
					stCDTRec.recDtls.bManEntryAllwd = PAAS_TRUE;
				}
				else
				{
					stCDTRec.recDtls.bManEntryAllwd = PAAS_FALSE;
				}

				if(stOldRec.iSigLineRequired == 1)
				{
					stCDTRec.recDtls.bSigLineRqd = PAAS_TRUE;
				}
				else
				{
					stCDTRec.recDtls.bSigLineRqd = PAAS_FALSE;
				}

				if(stOldRec.iPINRequired == 1)
				{
					stCDTRec.recDtls.bPINRqd = PAAS_TRUE;
				}
				else
				{
					stCDTRec.recDtls.bPINRqd = PAAS_FALSE;
				}

				if(stOldRec.iCardDisabled == 1)
				{
					stCDTRec.recDtls.bDisableCR = PAAS_TRUE;
				}
				else
				{
					stCDTRec.recDtls.bDisableCR = PAAS_FALSE;
				}

				/* Add the new formatted record in the CDT (RAM) */
				rv = addRecToCDT(pstCDT, &stCDTRec);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to add record to CDT",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}

				iCnt++;
			}
			else if(ferror(fp))
			{
				debug_sprintf(szDbgMsg, "%s: Error while reading from [%s]",
												__FUNCTION__, CDT_REC_NEW_FILE);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
	}

	if(rv == SUCCESS)
	{
		pstCDT->iRecCnt = iCnt;
	}

	/* Close any file if opened */
	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: enableDisableCR
 *
 * Description	:
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int enableDisableCR(CDTREC_PTYPE recPtr, char * pszLbl, PAAS_BOOL bVal)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(recPtr != NULL)
	{
		if(strcasecmp(recPtr->recDtls.szIPCLbl, pszLbl) == SUCCESS)
		{
			recPtr->recDtls.bDisableCR = bVal;
			iCnt++;
		}

		recPtr = recPtr->nextRec;
	}

	if(iCnt == 0)
	{
		debug_sprintf(szDbgMsg, "%s: No record found in CDT for label [%s]",
														__FUNCTION__, pszLbl);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveCDTRecords
 *
 * Description	: 
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int saveCDTRecs(CDT_PTYPE pstCDT)
{
	int				rv					= SUCCESS;
	int				iIndex				= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	FILE *			fp					= NULL;
	CDTREC_PTYPE	recPtr				= NULL;;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	fp = fopen(CDT_REC_NEW_FILE, "wb");
	if(fp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to open [%s] for writing",
											__FUNCTION__, CDT_REC_NEW_FILE);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Failed to Open the CDT File for Writing");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		rv = FAILURE;
	}
	else
	{
		recPtr = pstCDT->cdtRecsHead;

		/* Write the data to the file */
		while(recPtr != NULL)
		{
			rv = fwrite(&(recPtr->recDtls), CDTREC_DTLS_SIZE, 1, fp);
			if(rv == 1)
			{
				/* Successfully wrote the record in the file */
				debug_sprintf(szDbgMsg, "%s: Record [%d] written",
													__FUNCTION__, iIndex + 1);
				APP_TRACE(szDbgMsg);

				rv = SUCCESS;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to write record [%d]",
													__FUNCTION__, iIndex + 1);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			recPtr = recPtr->nextRec;
			iIndex++;
		}
	}

	/* Close any opened files */
	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadCDTFromDfltFile
 *
 * Description	: This API reads the default CDT file and write to the internal
 * 					CDT.dat file
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int loadCDTFromDfltFile(CDT_PTYPE pstCDT)
{
	int		rv					= SUCCESS;
	int		iRecCnt				= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* read the first line of the file to know the number of records to be
		 * present in the resulting CDT */
		rv = getRecCntForCDT(&iRecCnt);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get rec count from CDT file",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error While Getting the Record Counts From CDT File");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		pstCDT->iRecCnt = iRecCnt;
		rv = createCDTRecs(pstCDT);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to make CDT recs",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Create the CDT Records");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		rv = fillCDTRecs(pstCDT);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill CDT from txt file",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Fill CDT From Text File");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillCDTRecs
 *
 * Description	: This API parses the CDT default file and fills the list of CDT
 * 					records
 *
 * Input Params	: int iRecordCount -> total count of CDT records to be created
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int fillCDTRecs(CDT_PTYPE pstCDT)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";
	char	szLine[1024]		= "";
	FILE *	fp					= NULL;
#ifdef DEBUG
	char	szDbgMsg[1200]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		/* Open the file */
		fp = fopen(CDT_DEFAULT_FILE, "r");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to open [%s]", __FUNCTION__,
															CDT_DEFAULT_FILE);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error While Opening the CDT File.");
				strcpy(szAppLogDiag, "Please Check the CDT File");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}

			rv = FAILURE;
			break;
		}

		/* File opened succcessfully, start populating the CDT records with the
		 * values provided in the file */
		while(!feof(fp))
		{
			memset(szLine, 0x00, sizeof(szLine));
			if(fgets(szLine, sizeof(szLine), fp) != NULL)
			{
				debug_sprintf(szDbgMsg,"%s: Line = [%s]",__FUNCTION__,szLine);
				APP_TRACE(szDbgMsg);

				/* ignore any comment lines in the txt file, the comments in
				 * the txt file are similar to the comments in C language
				 * program */
				if( (strncmp(szLine, "//", 2) == 0) ||
											(strncmp(szLine, "/*", 2) == 0) )
				{
					continue;
				}

				/* Parse the line for storing the particular value in the CDT
				 * record */
				rv = parseCDTLine(szLine, pstCDT);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to parse CDT line",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}
			else if(ferror(fp))
			{
				debug_sprintf(szDbgMsg, "%s: Error while reading from [%s]",
											__FUNCTION__, CDT_DEFAULT_FILE);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error While Reading the CDT File.");
					strcpy(szAppLogDiag, "Please Check the CDT File");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
				}

				rv = FAILURE;
				break;
			}
		}

		break;
	}

	/* Close the open file descriptors */
	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: createCDTRecs
 *
 * Description	: This API will allocate memory for the required number of CDT
 * 					records. This function doesnt take care of flushing the
 * 					allocated memory in case of error. For the flushing call
 * 					the corresponding API.
 *
 * Input Params	: CDT_PTYPE pstCDT
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int createCDTRecs(CDT_PTYPE pstCDT)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iReqdCnt		= 0;
	CDTREC_PTYPE	tmpRecPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iReqdCnt = pstCDT->iRecCnt;

	debug_sprintf(szDbgMsg, "%s: Record count = [%d]", __FUNCTION__, iReqdCnt);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < iReqdCnt; iCnt++)
	{
		/* Allocate memory */
		tmpRecPtr = (CDTREC_PTYPE) malloc(CDTREC_SIZE);
		if(tmpRecPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize the memory */
		memset(tmpRecPtr, 0x00, CDTREC_SIZE);

		/* Add the record to the CDT table */
		if(pstCDT->cdtRecsTail == NULL)
		{
			pstCDT->cdtRecsHead = tmpRecPtr;
			pstCDT->cdtRecsTail = tmpRecPtr;
		}
		else
		{
			pstCDT->cdtRecsTail->nextRec = tmpRecPtr;
			pstCDT->cdtRecsTail = tmpRecPtr;
		}

		tmpRecPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getRecCntForCDT
 *
 * Description	: This API reads the first valid line and counts the number of
 * 					records
 *
 * Input Params	: Buffer to hold the Record Count
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getRecCntForCDT(int *piRecordCount)
{
	int		rv					= SUCCESS;
	int		iCount				= 0;
	char	szLine[2048]		= "";
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";
	FILE *	fp					= NULL;
	char *	pchFirst			= NULL;
	char *	pchNext				= NULL;
#ifdef DEBUG
	char	szDbgMsg[2048]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));


	fp = fopen(CDT_DEFAULT_FILE, "r+");

	if(fp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening the default CDT file[%s]..returning", __FUNCTION__, CDT_DEFAULT_FILE);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error While Opening the CDT File.");
			strcpy(szAppLogDiag, "Please Check the CDT File");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
		}
		return FAILURE;
	}

	while(!feof(fp))
	{
		memset(szLine, 0x00, sizeof(szLine));
		if(fgets(szLine, sizeof(szLine), fp) != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Read Line [%s]", __FUNCTION__, szLine);
			APP_TRACE(szDbgMsg);

			if(strncmp(szLine, "//", 2) == 0 || strncmp(szLine, "/*", 2) == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Its a comment line, ignoring it", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				continue; //goes to read next line
			}

			debug_sprintf(szDbgMsg, "%s: Need to parse the line to get the number of records", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pchNext = strchr(szLine, ' '); //pChr pointing to space
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain space in the line, invalid format!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				// CID-67406: 25-Jan-16: MukeshS3: closing the file before returning
				if(fp != NULL)
				{
					fclose(fp);
				}
				return FAILURE; //Invalid format
			}

			pchFirst = pchNext+1; //Pointing to value of the first record, from here on we need to count number of ','

			//debug_sprintf(szDbgMsg, "%s: After skipping Label Name [%s]", __FUNCTION__, pchFirst);
			//APP_TRACE(szDbgMsg);

			while(1)
			{
				pchNext = strchr(pchFirst, ',');
				if(pchNext == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Does not contain ',', looking for <CR>", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					pchNext = strchr(pchFirst, CR);
					if(pchNext == NULL)
					{
						// CID-67406: 25-Jan-16: MukeshS3: closing the file before returning
						if(fp != NULL)
						{
							fclose(fp);
						}
						return FAILURE; //Invalid format
					}
					else
					{
						iCount = iCount + 1;
					}
					break;
				}
				else
				{
					iCount = iCount + 1;
				}
				//debug_sprintf(szDbgMsg, "%s: Current iCount is  [%d]", __FUNCTION__, iCount);
				//APP_TRACE(szDbgMsg);

				pchFirst = pchNext+1;

			}

			debug_sprintf(szDbgMsg, "%s: iCount is %d", __FUNCTION__, iCount);
			APP_TRACE(szDbgMsg);

			*piRecordCount = iCount;

			break;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while reading the CDT line!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error While Reading the CDT File.");
				strcpy(szAppLogDiag, "Please Check the CDT File");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}
			rv = FAILURE;
			break;
		}
	}

	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d], Record Count [%d]",
					__FUNCTION__, rv, *piRecordCount);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseCDTLine
 *
 * Description	: This API parses the given CDT line
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int parseCDTLine(char * pszLine, CDT_PTYPE pstCDT)
{
	int		rv				= SUCCESS;
	int		iLblNo			= 0;
	char	szLabel[50]		= "";
	char *	pchFirst		= NULL;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* 
		 * --------------------------------------------------------------------
		 * CDT line format looks like this 
		 * Type 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 1
		 * ie <Label><space> record values (comma separated) 
		 * We need to get the label, and check for the record values and keep
		 * adding them to the CDT records
		 * --------------------------------------------------------------------
		 */

		/* Get the label for the current line in the CDT.txt file */
		rv = getLabelName(pszLine, szLabel);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Label", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Reach for the first record value (just after the label) */
		pchFirst = strchr(pszLine, ' ') + 1;

		/* Check if the label for this CDT.txt line is still valid for the
		 * application and if yes then get its corresponding constant value (to
		 * help to identify which field of the record has to be updated) */
		rv = getLabelConstant(szLabel, &iLblNo);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to validate Label",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* If the current record label is not being used currently by the
		 * application, then just skip parsing the line */
		if(iLblNo == NOT_IN_USE)
		{
			debug_sprintf(szDbgMsg, "%s: Label not in use; skip parsing line",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Fill the CDT table with the values for the particular field in all
		 * the records */
		rv = addFldValToCDTRecs(pchFirst, iLblNo, pstCDT);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill CDT records",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getLabelConstant
 *
 * Description	: This API gets the corresponding label number after comparing
 * 					with label names
 *
 * Input Params	: Label Name, Label Number
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getLabelConstant(char * pszLbl, int * piLblNo)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[2048]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( (strcmp(pszLbl, "Type") == SUCCESS) ||
									(strcasecmp(pszLbl, "CDTYP") == SUCCESS) )
	{
		*piLblNo = CARD_TYPE;
	}
	else if(strcasecmp(pszLbl, "PANLo") == SUCCESS)
	{
		*piLblNo = PAN_LOW;
	}
	else if(strcasecmp(pszLbl, "PANHi") == SUCCESS)
	{
		*piLblNo = PAN_HIGH;
	}
	else if( (strcmp(pszLbl, "MinPANDigit") == SUCCESS) ||
									(strcasecmp(pszLbl, "MNPNDG") == SUCCESS) )
	{
		*piLblNo = PAN_MIN_LEN;
	}
	else if( (strcmp(pszLbl, "MaxPANDigit") == SUCCESS) ||
									(strcasecmp(pszLbl, "MXPNDG") == SUCCESS) )
	{
		*piLblNo = PAN_MAX_LEN;
	}
	else if(strcasecmp(pszLbl, "AVS") == SUCCESS)
	{
		*piLblNo = AVS;
	}
	else if( (strcmp(pszLbl, "TipDiscount") == SUCCESS) ||
									(strcasecmp(pszLbl, "TIPDIS") == SUCCESS) )
	{
		*piLblNo = TIP_DISCOUNT;
	}
	else if(strcasecmp(pszLbl, "CVV_II") == SUCCESS)
	{
		*piLblNo = CVV_CODE;
	}
	else if( (strcmp(pszLbl, "CardAbbrev") == SUCCESS) ||
									(strcasecmp(pszLbl, "CDABB") == SUCCESS) )
	{
		*piLblNo = CARD_ABB;
	}
	else if( (strcmp(pszLbl, "CardLabel") == SUCCESS) ||
									(strcasecmp( pszLbl, "CDLBL") == SUCCESS) )
	{
		*piLblNo = CARD_LABEL;
	}
	else if((strcmp( pszLbl, "IPCLabel") == SUCCESS) ||
									(strcasecmp( pszLbl, "IPCLBL") == SUCCESS) )
	{
		*piLblNo = IPC_LABEL;
	}
	else if( strcmp(pszLbl, "IPCProcessor") == SUCCESS )
	{
		*piLblNo = IPC_PROCESSOR;
	}
	else if( (strcmp(pszLbl, "TracksRequired") == SUCCESS) ||
									(strcasecmp(pszLbl, "TRKREQ") == SUCCESS) )
	{
		*piLblNo = TRACK_REQ;
	}
	else if( (strcmp(pszLbl, "BatchAuthFloorLimitAmt") == SUCCESS) ||
									(strcasecmp(pszLbl, "BALIMT") == SUCCESS) )
	{
		*piLblNo = SAF_LIMIT;
	}
	else if( (strcmp(pszLbl, "SignLimitAmount") == SUCCESS) ||
									(strcasecmp(pszLbl, "SLIMIT") == SUCCESS) )
	{
		*piLblNo = SIG_LIMIT;
	}
	else if( (strcmp(pszLbl, "GiftAmtMin") == SUCCESS) ||
									(strcasecmp(pszLbl, "GFTMIN") == SUCCESS) )
	{
		*piLblNo = GIFT_MIN_AMOUNT;
	}
	else if( (strcmp(pszLbl, "GiftAmtMax") == SUCCESS) ||
									(strcasecmp(pszLbl, "GFTMAX") == SUCCESS) )
	{
		*piLblNo = GIFT_MAX_AMOUNT;
	}
	else if( (strcmp(pszLbl, "DisableCardRange") == SUCCESS) ||
									(strcasecmp(pszLbl, "RNGOFF") == SUCCESS) )
	{
		*piLblNo = CARD_DISABLED;
	}
	else if( (strcmp(pszLbl, "ChkLuhn") == SUCCESS) ||
									(strcasecmp(pszLbl, "CHKLHN") == SUCCESS) )
	{
		*piLblNo = LUHN_REQ;
	}
	else if( (strcmp(pszLbl, "ExpDtReqd") == SUCCESS) ||
									(strcasecmp(pszLbl, "EXPDT") == SUCCESS) )
	{
		*piLblNo = EXPIRY_REQ;
	}
	else if( (strcmp(pszLbl, "ManEntry") == SUCCESS) ||
									(strcasecmp(pszLbl, "MANL") == SUCCESS) )
	{
		*piLblNo = MANUAL_ALLOWED;
	}
	else if( (strcmp(pszLbl, "SignLine") == SUCCESS) ||
									(strcasecmp(pszLbl, "SIGNLIN") == SUCCESS) )
	{
		*piLblNo = SIG_LINE_REQ;
	}
	else if( (strcmp(pszLbl, "PinpadRequired") == SUCCESS) ||
									(strcasecmp(pszLbl, "PPREQ") == SUCCESS) )
	{
		*piLblNo = PIN_REQ;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Unknown Label [%s]", __FUNCTION__, pszLbl);
		APP_TRACE(szDbgMsg);

		*piLblNo = NOT_IN_USE;
	}

	debug_sprintf(szDbgMsg, "%s: Label = [%s], label Number = [%d]",
												__FUNCTION__, pszLbl, *piLblNo);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addFldValToCDTRecs
 *
 * Description	: This API gets the corresponding label values from the line and stores
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int addFldValToCDTRecs(char * pszLine, int iLblNo, CDT_PTYPE pstCDT)
{
	int				rv			= SUCCESS;
	int				iCnt		= 0;
	char			szVal[50]	= "";
	char *			cCurPtr		= NULL;
	char *			cNxtPtr		= NULL;
	CDTREC_PTYPE	pstRec		= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 1
	 */

	/* Assign the starting position of the CDT line */
	cCurPtr = pszLine;
	pstRec = pstCDT->cdtRecsHead;

	for(iCnt = 1; iCnt <= pstCDT->iRecCnt; iCnt++)
	{
		cNxtPtr = strchr(cCurPtr, ',');
		if(cNxtPtr == NULL)
		{
			if(iCnt == pstCDT->iRecCnt)
			{
				debug_sprintf(szDbgMsg, "%s: May be last record", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				cNxtPtr = strchr(cCurPtr, CR);
				if(cNxtPtr == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: no <CR> - Invalid format",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				memset(szVal, 0x00, sizeof(szVal));
				strncpy(szVal, cCurPtr, cNxtPtr - cCurPtr);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: no <,> - Invalid format",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		else
		{
			memset(szVal, 0x00, sizeof(szVal));
			strncpy(szVal, cCurPtr, cNxtPtr - cCurPtr);
		}

		debug_sprintf(szDbgMsg, "%s: Value of the %d label for %d record is %s",
									__FUNCTION__, iLblNo, iCnt, szVal);
		APP_TRACE(szDbgMsg);

		formatFieldValue(szVal);

		rv = modifyCDTRec(&(pstRec->recDtls), iLblNo, szVal);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to modify CDT record",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		cCurPtr = cNxtPtr + 1;
		pstRec = pstRec->nextRec;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: modifyCDTRec
 *
 * Description	:
 *
 * Input Params	: Label Number, Record Number, FieldValue
 *
 * Output Params:
 * ============================================================================
 */
static int modifyCDTRec(CDTREC_DTLS_PTYPE pstRecDtls, int iLblNo, char * pszVal)
{
	int		rv				= SUCCESS;
	int		iVal			= 0;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if((pstRecDtls == NULL) || (pszVal == NULL))
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		switch(iLblNo)
		{
		case CARD_TYPE:
			pstRecDtls->iCardType = atoi(pszVal);
			break;

		case PAN_MIN_LEN:
			pstRecDtls->iMinPANLen = atoi(pszVal);
			break;

		case PAN_MAX_LEN:
			pstRecDtls->iMaxPANLen = atoi(pszVal);
			break;

		case AVS:
			pstRecDtls->iAVSCode = atoi(pszVal);
			break;

		case TIP_DISCOUNT:
			pstRecDtls->iTipDscnt = atoi(pszVal);
			break;

		case CVV_CODE:
			pstRecDtls->iCVV2 = atoi(pszVal);
			break;

		case PAN_LOW:
			strcpy(pstRecDtls->szPANLo, pszVal);
			break;

		case PAN_HIGH:
			strcpy(pstRecDtls->szPANHi, pszVal);
			break;

		case CARD_ABB:
			strcpy(pstRecDtls->szCardAbbr, pszVal);
			break;

		case CARD_LABEL:
			strcpy(pstRecDtls->szCardLbl, pszVal);
			break;

		case IPC_LABEL:
			strcpy(pstRecDtls->szIPCLbl, pszVal);
			break;

		case IPC_PROCESSOR:
			strcpy(pstRecDtls->szIPCProcId, pszVal);
			break;

		case TRACK_REQ:
			strcpy(pstRecDtls->szTrkRqd, pszVal);
			break;

		case SAF_LIMIT:
			strcpy(pstRecDtls->szSAFFlrLim, pszVal);
			break;

		case SIG_LIMIT:
			strcpy(pstRecDtls->szSigFlrLim, pszVal);
			break;

		case GIFT_MIN_AMOUNT:
			strcpy(pstRecDtls->szGiftMinAmt, pszVal);
			break;

		case GIFT_MAX_AMOUNT:
			strcpy(pstRecDtls->szGiftMaxAmt, pszVal);
			break;

		case LUHN_REQ:
			iVal = atoi(pszVal);
			if(iVal == 0)
			{
				pstRecDtls->bLUHNChkRqd = PAAS_FALSE;
			}
			else
			{
				pstRecDtls->bLUHNChkRqd = PAAS_TRUE;
			}

			break;

		case EXPIRY_REQ:
			iVal = atoi(pszVal);
			if(iVal == 0)
			{
				pstRecDtls->bExpDateChkRqd = PAAS_FALSE;
			}
			else
			{
				pstRecDtls->bExpDateChkRqd = PAAS_TRUE;
			}

			break;

		case MANUAL_ALLOWED:
			iVal = atoi(pszVal);
			if(iVal == 0)
			{
				pstRecDtls->bManEntryAllwd = PAAS_FALSE;
			}
			else
			{
				pstRecDtls->bManEntryAllwd = PAAS_TRUE;
			}

			break;

		case SIG_LINE_REQ:
			iVal = atoi(pszVal);
			if(iVal == 0)
			{
				pstRecDtls->bSigLineRqd = PAAS_FALSE;
			}
			else
			{
				pstRecDtls->bSigLineRqd = PAAS_TRUE;
			}

			break;

		case PIN_REQ:
			iVal = atoi(pszVal);
			if(iVal == 0)
			{
				pstRecDtls->bPINRqd = PAAS_FALSE;
			}
			else
			{
				pstRecDtls->bPINRqd = PAAS_TRUE;
			}

			break;

		case CARD_DISABLED:
			iVal = atoi(pszVal);
			if(iVal == 0)
			{
				pstRecDtls->bDisableCR = PAAS_FALSE;
			}
			else
			{
				pstRecDtls->bDisableCR = PAAS_TRUE;
			}

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: formatFieldValue
 *
 * Description	: This API formats the field value
 *
 * Input Params	: Buffer which contains the Field Value
 *
 * Output Params:
 * ============================================================================
 */
static void formatFieldValue(char * pszFldVal)
{
	char	szTmp[50]		= "";
	char *	pchFirst		= NULL;
	char *	pchLast			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Old Value [%s]", __FUNCTION__, pszFldVal);
	APP_TRACE(szDbgMsg);

	/* The format of the line in the cdt.txt file can have values in the
	 * following two formats
	 * 1) Label 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 1
	 * 2) Label "400000", "340000", "370000", "2014", "2140", "000000"
	 * We need to get the proper values like 12 or 340000 etc from the strings
	 * given.
	 */

	/* Copying the incoming string into a temporary buffer used for making the
	 * changes. */
	strcpy(szTmp, pszFldVal);

	/* Strip any leading and trailing spaces if present in the field value */
	stripSpaces(szTmp, STRIP_LEADING_SPACES|STRIP_TRAILING_SPACES);

	memset(pszFldVal, 0x00, strlen(pszFldVal));

	pchFirst = strchr(szTmp, '"');
	if(pchFirst != NULL)
	{
		pchLast = strrchr(szTmp, '"');
		if(pchLast != NULL)
		{
			memcpy(pszFldVal, pchFirst+1, pchLast - pchFirst - 1);
		}
		else
		{
			strcpy(pszFldVal, szTmp);
		}
	}
	else
	{
		strcpy(pszFldVal, szTmp);
	}

	debug_sprintf(szDbgMsg, "%s: New Value [%s]", __FUNCTION__, pszFldVal);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: getLabelName
 *
 * Description	: This API gets the label name from the given line
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getLabelName(char * pszLine, char *pszLabelName)
{
	int		rv				= SUCCESS;
	char *	cPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	cPtr = strchr(pszLine, ' ');
	if(cPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid line format: no space",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		strncpy(pszLabelName, pszLine, cPtr - pszLine);

		debug_sprintf(szDbgMsg, "%s: Label = [%s]", __FUNCTION__, pszLabelName);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: loadCDTFromDat
 *
 * Description	: This API reads the card records from the CDT.dat file (having
 * 					our own custom format) and stores them into a linked list
 * 					in the system memory
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int loadCDTFromDat(CDT_PTYPE pstCDT)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	FILE *			fp				= NULL;
	CDTREC_STYPE	stCDTRec;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Open the binary file (having application's own fomrat) for loading the
	 * CDT records into the RAM */
	fp = fopen(CDT_REC_NEW_FILE, "rb");
	if(fp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to open [%s]", __FUNCTION__,
															CDT_REC_NEW_FILE);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		/* File has been opened. Parse the file and store the records in CDT */
		while(!feof(fp))
		{
			memset(&stCDTRec, 0x00, CDTREC_SIZE);

			rv = fread(&(stCDTRec.recDtls), CDTREC_DTLS_SIZE, 1, fp);
			if(rv == 1)
			{
				/* Record successfully read from the file */
				rv = addRecToCDT(pstCDT, &stCDTRec);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to add record to CDT",
																__FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}

				iCnt++;
			}
			else if(ferror(fp))
			{
				debug_sprintf(szDbgMsg, "%s: Error while reading from [%s]",
											__FUNCTION__, CDT_REC_NEW_FILE);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

		}
	}

	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Total records = [%d]", __FUNCTION__, iCnt);
		APP_TRACE(szDbgMsg);

		pstCDT->iRecCnt = iCnt;
	}

	/* Close the file if opened */
	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addRecToCDT
 *
 * Description	: This API adds the record to the list
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int addRecToCDT(CDT_PTYPE pstCDT, CDTREC_PTYPE pstCDTRec)
{
	int				rv				= SUCCESS;
	CDTREC_PTYPE	tmpPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[128]	= "";
#endif

	tmpPtr = (CDTREC_PTYPE) malloc(CDTREC_SIZE);
	if(tmpPtr != NULL)
	{
		memset(tmpPtr, 0x00, CDTREC_SIZE);
		memcpy(tmpPtr, pstCDTRec, CDTREC_SIZE);

		if(pstCDT->cdtRecsTail == NULL)
		{
			pstCDT->cdtRecsHead = tmpPtr;
		}
		else
		{
			pstCDT->cdtRecsTail->nextRec = tmpPtr;
		}

		pstCDT->cdtRecsTail = tmpPtr;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: flushCDT
 *
 * Description	: This API is responsible for flushing the CDT record list
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
static void flushCDT(CDT_PTYPE pstCDT)
{
	CDTREC_PTYPE	curRecPtr	= NULL;
	CDTREC_PTYPE	prevRecPtr	= NULL;

	curRecPtr = pstCDT->cdtRecsHead;

	while(curRecPtr != NULL)
	{
		prevRecPtr = curRecPtr;
		curRecPtr = curRecPtr->nextRec;
		free(prevRecPtr);
	}

	memset(pstCDT, 0x00, sizeof(CDT_STYPE));

	return;
}
int getCrdTypeByCDT(int * crdTypeList, CARDDTLS_PTYPE pstCardDtls, PAAS_BOOL bExtractCardLbl)
{

		int					rv					= SUCCESS;
		int					iCnt				= 0;
		int					iLen1				= 0;
		int					iLen2				= 0;
		int					iMatchedCardType	= -1;
		int					iNumTndrs			= 0;
		int					iNumValidTndrs		= 0;
		int					iAppLogEnabled		= isAppLogEnabled();
		int					tenderSetting[MAX_TENDERS]	= {0};
		char				szTmp[21]			= "";
		char *				pszPAN				= NULL;
		char				szAppLogData[300]	= "";
		char				szAppLogDiag[300]	= "";
		char				szCardLbl[21]		= "";
		unsigned long long	lCurPAN				= 0ULL;
		unsigned long long	lPANLo				= 0ULL;
		unsigned long long  lPANHi				= 0ULL;
		PAAS_BOOL			bManEntry			= PAAS_FALSE;
		CDTREC_PTYPE		tmpRecPtr			= NULL;
	#ifdef DEBUG
		char			szDbgMsg[256]		= "";
	#endif

		debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

		while(1)
		{

			/* Daivik 14/1/2016 : We are checking only for card details because we enter the function only for the new payment flow and by this point we would have
			 * obtained the card details. Even if Card was pre-inserted , we would have passed the CAPTURE_CARDDETAILS state and hence card details structure
			 * will have the required data, we dont need to check in the session card details.
			 * For EMV Cards we want to avoid checking CDT in all cases. Hence this check. The SAF Allowed flag is also updated based on the floor limit in config.usr1.
			 * For the payment types supported we use only the details present in the AIDList.txt
			 */
			if(pstCardDtls->bEmvData == PAAS_TRUE)
			{
				rv = SUCCESS;
				debug_sprintf(szDbgMsg, "%s: EMV Chip Card is used", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			if(bExtractCardLbl)
			{
				/* Initialization of the list */
				for(iCnt = 0; iCnt < MAX_TENDERS; iCnt++)
				{
					tenderSetting[iCnt] = -1;
				}
				// get the enabled tender in config.usr1
				rv = getTendersListFromSettings(tenderSetting);
			}

			bManEntry = pstCardDtls->bManEntry;
			pszPAN = pstCardDtls->szPAN;
			iLen1 = strlen(pszPAN);

			tmpRecPtr = stCDT.cdtRecsHead;

			while(tmpRecPtr != NULL)
			{
				if(tmpRecPtr->recDtls.bDisableCR == PAAS_TRUE)
				{
					/* The card range should be enabled for the PAN bin range check
					 * to be done */
					debug_sprintf(szDbgMsg, "%s: Card range [%s]-[%s] DISABLED",
									__FUNCTION__, tmpRecPtr->recDtls.szPANLo,
									tmpRecPtr->recDtls.szPANHi);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					iLen2 = strlen(tmpRecPtr->recDtls.szPANHi);

					/* If the length of PAN is less than that of PANHi, take the
					 * original length of PAN for comparison, else take the first n
					 * numbers of PAN (n being length of PANHi) */
					memset(szTmp, 0x00, sizeof(szTmp));
					memcpy(szTmp, pszPAN, (iLen1 < iLen2) ? iLen1: iLen2);

					/*lCurPAN = atol(szTmp);
					lPANLo = atol(tmpRecPtr->recDtls.szPANLo);
					lPANHi = atol(tmpRecPtr->recDtls.szPANHi);*/

					/* KranthiK1: Added the below conversion to support first
					 * 11 digit PAN range check
					 * */
					lCurPAN = strtoull(szTmp, NULL, 10);
					lPANLo  = strtoull(tmpRecPtr->recDtls.szPANLo, NULL, 10);
					lPANHi  = strtoull(tmpRecPtr->recDtls.szPANHi, NULL, 10);

	#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: Cur=[%llu];LO=[%llu];HI=[%llu]",
									DEVDEBUGMSG,__FUNCTION__, lCurPAN, lPANLo, lPANHi);
					APP_TRACE(szDbgMsg);
	#endif

					/*
					 * Praveen_P1: Align with the BIN range, we are adding PAN lenth also to the
					 * criteria to decide whether given PAN falling into the any range
					 * in the CDT table
					 */
					if( ((lCurPAN >= lPANLo) && (lCurPAN <= lPANHi))  &&
						 ((iLen1/*iLen1 is length of the Current PAN */ >= tmpRecPtr->recDtls.iMinPANLen) && (iLen1 <= tmpRecPtr->recDtls.iMaxPANLen)) )
					{
						debug_sprintf(szDbgMsg, "%s: Range %llu-%llu for %d satisfied"
										, __FUNCTION__, lPANLo, lPANHi, tmpRecPtr->recDtls.iCardType);
						APP_TRACE(szDbgMsg);

						if( (bManEntry == PAAS_TRUE) &&
								(tmpRecPtr->recDtls.bManEntryAllwd != PAAS_TRUE) )
						{
							debug_sprintf(szDbgMsg, "%s: Man Entry is DISABLED",
																	__FUNCTION__);
							APP_TRACE(szDbgMsg);

						}
						else
						{

							switch(tmpRecPtr->recDtls.iCardType)
							{

							case CREDIT_CARD_TYPE:
								crdTypeList[TEND_CREDIT] = 1;
								if(tenderSetting[TEND_CREDIT] == 1)
								{
									iNumValidTndrs++;
								}
								iNumTndrs++;
								break;

							case DEBIT_CARD_TYPE:
								crdTypeList[TEND_DEBIT] = 1;
								if(tenderSetting[TEND_DEBIT] == 1)
								{
									iNumValidTndrs++;
								}
								iNumTndrs++;
								break;

							case PL_CARD_TYPE:
								crdTypeList[TEND_PRIVATE] = 1;
								if(tenderSetting[TEND_PRIVATE] == 1)
								{
									iNumValidTndrs++;
								}
								iNumTndrs++;
								break;

							case MERCH_CREDIT_CARD_TYPE:
								crdTypeList[TEND_MERCHCREDIT] = 1;
								if(tenderSetting[TEND_MERCHCREDIT] == 1)
								{
									iNumValidTndrs++;
								}
								iNumTndrs++;
								break;

							case FSA_CARD_TYPE:
								crdTypeList[TEND_FSA] = 1;
								if(tenderSetting[TEND_FSA] == 1)
								{
									iNumValidTndrs++;
								}
								iNumTndrs++;
								break;

							case EBT_CARD_TYPE:
								crdTypeList[TEND_EBT] = 1;
								if(tenderSetting[TEND_EBT] == 1)
								{
									iNumValidTndrs++;
								}
								iNumTndrs++;
								break;

							case GIFT_CARD_TYPE:
								crdTypeList[TEND_GIFT] = 1;
								if(tenderSetting[TEND_GIFT] == 1)
								{
									iNumValidTndrs++;
								}
								iNumTndrs++;
								break;

							default:
								debug_sprintf(szDbgMsg, "%s: No Tender Type for this card type:%d",__FUNCTION__,tmpRecPtr->recDtls.iCardType);
								APP_TRACE(szDbgMsg);
								break;
							}

							if(bExtractCardLbl && (iNumValidTndrs == 1) && (strlen(szCardLbl) == 0))
							{
								memset(szCardLbl, 0x00, sizeof(szCardLbl));
								strcpy(szCardLbl, tmpRecPtr->recDtls.szCardLbl);
								iMatchedCardType = tmpRecPtr->recDtls.iCardType;
							}
						}


					}
				}

				/* Go to the next record in the CDT */
				tmpRecPtr = tmpRecPtr->nextRec;
			}

			if((tmpRecPtr == NULL && (iNumTndrs == 0)) || (bExtractCardLbl && (iNumValidTndrs < 1)))
			{
				debug_sprintf(szDbgMsg, "%s: No record of CDT matches with PAN",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Card Data Error.");
					strcpy(szAppLogDiag, "No Record in CDT matches With Card PAN");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}
				rv = ERR_NOT_IN_BINRANGE;

				break;
			}

			if(bExtractCardLbl && (iNumValidTndrs ==  1))
			{
				strcpy(pstCardDtls->szPymtMedia, szCardLbl);
				pstCardDtls->iCardType = iMatchedCardType;
			}

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);

		return rv;


}

/*
 * ============================================================================
 * Function Name: getPymtMediafromCDT
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / ERR_NOT_IN_BINRANGE
 * ============================================================================
 */
int getPymtMediafromCDT(CARDDTLS_PTYPE pstCardDtls, int iPymtType)
{

		int					rv					= SUCCESS;
		int					iLen1				= 0;
		int					iLen2				= 0;
		int					iCardType			= -1;
		int					iNumTndrs			= 0;
		int					iAppLogEnabled		= isAppLogEnabled();
		char				szTmp[21]			= "";
		char *				pszPAN				= NULL;
		char				szAppLogData[300]	= "";
		char				szAppLogDiag[300]	= "";
		char				szCardLbl[21]		= "";		/* 20-letter card label */
		unsigned long long	lCurPAN				= 0ULL;
		unsigned long long	lPANLo				= 0ULL;
		unsigned long long  lPANHi				= 0ULL;
		PAAS_BOOL			bManEntry			= PAAS_FALSE;
		CDTREC_PTYPE		tmpRecPtr			= NULL;
	#ifdef DEBUG
		char			szDbgMsg[256]		= "";
	#endif

		debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

		while(1)
		{
			// Mukesh: 1-March-16: Keeping it as it is.
			/* Daivik 14/1/2016 : We are checking only for card details because we enter the function only for the new payment flow and by this point we would have
			 * obtained the card details. Even if Card was pre-inserted , we would have passed the CAPTURE_CARDDETAILS state and hence card details structure
			 * will have the required data, we dont need to check in the session card details.
			 * For EMV Cards we want to avoid checking CDT in all cases. Hence this check. The SAF Allowed flag is also updated based on the floor limit in config.usr1.
			 * For the payment types supported we use only the details present in the AIDList.txt
			 */
			if(pstCardDtls->bEmvData == PAAS_TRUE)
			{
				rv = SUCCESS;
				debug_sprintf(szDbgMsg, "%s: EMV Chip Card is used", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			switch(iPymtType)
			{
				case TEND_CREDIT:
					iCardType = CREDIT_CARD_TYPE;
					break;
				case TEND_DEBIT:
					iCardType = DEBIT_CARD_TYPE;
					break;
				case TEND_GIFT:
					iCardType = GIFT_CARD_TYPE;
					break;
				case TEND_PRIVATE:
					iCardType = PL_CARD_TYPE;
					break;
				case TEND_MERCHCREDIT:
					iCardType = MERCH_CREDIT_CARD_TYPE;
					break;
				case TEND_EBT:
					iCardType = EBT_CARD_TYPE;
					break;
				case TEND_FSA:
					iCardType = FSA_CARD_TYPE;
					break;

				case TEND_PAYPAL:
				case TEND_BALANCE:
				case TEND_POS_TENDER1:
				case TEND_POS_TENDER2:
				case TEND_POS_TENDER3:
				case TEND_GOOGLE:
				case TEND_ISIS:
				case TEND_OTHER:
				default:
					break;
			}

			if(iCardType == -1)
			{
				debug_sprintf(szDbgMsg, "%s: Not determining Payment media for this payment type", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			//iCardType = pstCardDtls->iCardType;
			bManEntry = pstCardDtls->bManEntry;
			pszPAN = pstCardDtls->szPAN;
			iLen1 = strlen(pszPAN);

			tmpRecPtr = stCDT.cdtRecsHead;

			while(tmpRecPtr != NULL)
			{

				debug_sprintf(szDbgMsg, "%s: Card Type[%d]", __FUNCTION__, tmpRecPtr->recDtls.iCardType);
				APP_TRACE(szDbgMsg);
				if(iCardType == tmpRecPtr->recDtls.iCardType)
				{
					if(tmpRecPtr->recDtls.bDisableCR == PAAS_TRUE)
					{
						/* The card range should be enabled for the PAN bin range check
						 * to be done */
						debug_sprintf(szDbgMsg, "%s: Card range [%s]-[%s] DISABLED",
										__FUNCTION__, tmpRecPtr->recDtls.szPANLo,
										tmpRecPtr->recDtls.szPANHi);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						iLen2 = strlen(tmpRecPtr->recDtls.szPANHi);

						/* If the length of PAN is less than that of PANHi, take the
						 * original length of PAN for comparison, else take the first n
						 * numbers of PAN (n being length of PANHi) */
						memset(szTmp, 0x00, sizeof(szTmp));
						memcpy(szTmp, pszPAN, (iLen1 < iLen2) ? iLen1: iLen2);

						/*lCurPAN = atol(szTmp);
						lPANLo = atol(tmpRecPtr->recDtls.szPANLo);
						lPANHi = atol(tmpRecPtr->recDtls.szPANHi);*/

						/* KranthiK1: Added the below conversion to support first
						 * 11 digit PAN range check
						 * */
						lCurPAN = strtoull(szTmp, NULL, 10);
						lPANLo  = strtoull(tmpRecPtr->recDtls.szPANLo, NULL, 10);
						lPANHi  = strtoull(tmpRecPtr->recDtls.szPANHi, NULL, 10);

	#ifdef DEVDEBUG
						debug_sprintf(szDbgMsg, "%s:%s: Cur=[%llu];LO=[%llu];HI=[%llu]",
										DEVDEBUGMSG,__FUNCTION__, lCurPAN, lPANLo, lPANHi);
						APP_TRACE(szDbgMsg);
	#endif

						/*
						 * Praveen_P1: Align with the BIN range, we are adding PAN lenth also to the
						 * criteria to decide whether given PAN falling into the any range
						 * in the CDT table
						 */
						if( ((lCurPAN >= lPANLo) && (lCurPAN <= lPANHi))  &&
							 ((iLen1/*iLen1 is length of the Current PAN */ >= tmpRecPtr->recDtls.iMinPANLen) && (iLen1 <= tmpRecPtr->recDtls.iMaxPANLen)) )
						{
							debug_sprintf(szDbgMsg, "%s: Range %llu-%llu for %d satisfied"
											, __FUNCTION__, lPANLo, lPANHi, iCardType);
							APP_TRACE(szDbgMsg);

							if( (bManEntry == PAAS_TRUE) &&
									(tmpRecPtr->recDtls.bManEntryAllwd != PAAS_TRUE) )
							{
								debug_sprintf(szDbgMsg, "%s: Man Entry is DISABLED",
																		__FUNCTION__);
								APP_TRACE(szDbgMsg);

							}
							else
							{
								iNumTndrs++;
								strcpy(szCardLbl, tmpRecPtr->recDtls.szCardLbl);
								//TODO:
								break;
							}
						}
					}
				}

				/* Go to the next record in the CDT */
				tmpRecPtr = tmpRecPtr->nextRec;
			}

			if(tmpRecPtr == NULL && (iNumTndrs == 0))
			{
				debug_sprintf(szDbgMsg, "%s: No record of CDT matches with PAN",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Card Data Error.");
					strcpy(szAppLogDiag, "No Record in CDT matches With Card PAN");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}
				rv = ERR_NOT_IN_BINRANGE;

				break;
			}

			if(iNumTndrs == 1 && (strlen(szCardLbl) > 0))
			{
				strcpy(pstCardDtls->szPymtMedia, szCardLbl);
			}

			break;
		}

		debug_sprintf(szDbgMsg, "%s: iNumTndrs[%d], szPymtMedia[%s]", __FUNCTION__, iNumTndrs,szCardLbl);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);

		return rv;


}

/*
 * ============================================================================
 * Function Name: getPymtMediafromCDTandEnabledTndr
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS / ERR_NOT_IN_BINRANGE
 * ============================================================================
 */
int getPymtMediafromCDTandEnabledTndr(CARDDTLS_PTYPE pstCardDtls)
{
		int					rv					= SUCCESS;
		int					iCnt				= 0;
		int					crdTypeLst[MAX_TENDERS]	= {0};
	#ifdef DEBUG
		char			szDbgMsg[256]		= "";
	#endif

		debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		while(1)
		{
			/* Initialization of the list */
			for(iCnt = 0; iCnt < MAX_TENDERS; iCnt++)
			{
				crdTypeLst[iCnt] = -1;
			}

			// Flushing the payment media & card type first, if was filled for private lable tender
			memset(pstCardDtls->szPymtMedia, 0x00, sizeof(pstCardDtls->szPymtMedia));
			pstCardDtls->iCardType = -1;

			rv = getCrdTypeByCDT(crdTypeLst, pstCardDtls, PAAS_TRUE);
			if(rv == ERR_NOT_IN_BINRANGE)
			{
				break;
			}

			if(strlen(pstCardDtls->szPymtMedia) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Payment Media[%s]", __FUNCTION__, pstCardDtls->szPymtMedia);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Swiped card falls in multiple BIN ranges; Can't determine payment media here", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
		APP_TRACE(szDbgMsg);

		return rv;
}


/*
 * ============================================================================
 * Function Name: getCDTBinRec
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS / ERR_NOT_IN_BINRANGE
 * ============================================================================
 */
int getCDTBinRec(CDTREC_PTYPE pstCDTRec, CARDDTLS_PTYPE pstCardDtls)
{
	int					rv					= SUCCESS;
	int					iLen1				= 0;
	int					iLen2				= 0;
	int					iCardType			= 0;
	int					iAppLogEnabled		= isAppLogEnabled();
	char				szTmp[21]			= "";
	char *				pszPAN				= NULL;
	char				szAppLogData[300]	= "";
	char				szAppLogDiag[300]	= "";
	unsigned long long	lCurPAN				= 0ULL;
	unsigned long long	lPANLo				= 0ULL;
	unsigned long long  lPANHi				= 0ULL;
	PAAS_BOOL			bManEntry			= PAAS_FALSE;
	CDTREC_PTYPE		tmpRecPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
/* We will be using the CDT Record for EMV Transactions as well , when we need to check whether
 * a particular transaction is below the SAF floor limit.
 */
//		if(pstCardDtls->bEmvData == PAAS_TRUE)
//		{
//			rv = SUCCESS;
//			//TODO:AJAYS2:Check is it fine or not
//			debug_sprintf(szDbgMsg, "%s: EMV Chip Card is used", __FUNCTION__);
//						APP_TRACE(szDbgMsg);
//			break;
//		}

		iCardType = pstCardDtls->iCardType;
		bManEntry = pstCardDtls->bManEntry;
		pszPAN = pstCardDtls->szPAN;
		iLen1 = strlen(pszPAN);

		tmpRecPtr = stCDT.cdtRecsHead;

		while(tmpRecPtr != NULL)
		{
			if(tmpRecPtr->recDtls.bDisableCR == PAAS_TRUE)
			{
				/* The card range should be enabled for the PAN bin range check
				 * to be done */
				debug_sprintf(szDbgMsg, "%s: Card range [%s]-[%s] DISABLED",
								__FUNCTION__, tmpRecPtr->recDtls.szPANLo,
								tmpRecPtr->recDtls.szPANHi);
				APP_TRACE(szDbgMsg);
			}
			else if(iCardType != tmpRecPtr->recDtls.iCardType)
			{
				/* The card range should be of same card type for the PAN bin
				 * range check to be done */
				debug_sprintf(szDbgMsg, "%s: Card type mismatch for [%s]-[%s]",
								__FUNCTION__, tmpRecPtr->recDtls.szPANLo,
								tmpRecPtr->recDtls.szPANHi);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				iLen2 = strlen(tmpRecPtr->recDtls.szPANHi);

				/* If the length of PAN is less than that of PANHi, take the
				 * original length of PAN for comparison, else take the first n
				 * numbers of PAN (n being length of PANHi) */
				memset(szTmp, 0x00, sizeof(szTmp));
				memcpy(szTmp, pszPAN, (iLen1 < iLen2) ? iLen1: iLen2);

				/*lCurPAN = atol(szTmp);
				lPANLo = atol(tmpRecPtr->recDtls.szPANLo);
				lPANHi = atol(tmpRecPtr->recDtls.szPANHi);*/

				/* KranthiK1: Added the below conversion to support first
				 * 11 digit PAN range check
				 * */
				lCurPAN = strtoull(szTmp, NULL, 10);
				lPANLo  = strtoull(tmpRecPtr->recDtls.szPANLo, NULL, 10);
				lPANHi  = strtoull(tmpRecPtr->recDtls.szPANHi, NULL, 10);

#ifdef DEVDEBUG
				debug_sprintf(szDbgMsg, "%s:%s: Cur=[%llu];LO=[%llu];HI=[%llu]",
								DEVDEBUGMSG,__FUNCTION__, lCurPAN, lPANLo, lPANHi);
				APP_TRACE(szDbgMsg);
#endif

				/*
				 * Praveen_P1: Align with the BIN range, we are adding PAN lenth also to the
				 * criteria to decide whether given PAN falling into the any range
				 * in the CDT table
				 */
				if( ((lCurPAN >= lPANLo) && (lCurPAN <= lPANHi))  &&
					 ((iLen1/*iLen1 is length of the Current PAN */ >= tmpRecPtr->recDtls.iMinPANLen) && (iLen1 <= tmpRecPtr->recDtls.iMaxPANLen)) )
				{
					debug_sprintf(szDbgMsg, "%s: Range %llu-%llu for %d satisfied"
									, __FUNCTION__, lPANLo, lPANHi, iCardType);
					APP_TRACE(szDbgMsg);

					if( (bManEntry == PAAS_TRUE) &&
							(tmpRecPtr->recDtls.bManEntryAllwd != PAAS_TRUE) )
					{
						debug_sprintf(szDbgMsg, "%s: Man Entry is DISABLED",
																__FUNCTION__);
						APP_TRACE(szDbgMsg);
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "Card Data Error.");
							strcpy(szAppLogDiag, "No Manual Entry is Allowed For this Card");
							addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
						}

						rv = ERR_MANUAL_NOTALLWD;
					}

					break;
				}
			}

			/* Go to the next record in the CDT */
			tmpRecPtr = tmpRecPtr->nextRec;
		}

		if(tmpRecPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No record of CDT matches with PAN",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Card Data Error.");
				strcpy(szAppLogDiag, "No Record in CDT matches With Card PAN");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}
			rv = ERR_NOT_IN_BINRANGE;

			break;
		}

		memcpy(pstCDTRec, tmpRecPtr, CDTREC_SIZE);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: validateCardData_1
 *
 * Description	: This API validates the card data
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
int validateCardData_1(CARDDTLS_PTYPE pstCardDtls)
{
	int		rv					= SUCCESS;
	int		iLen				= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		/* Validate PAN */

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: PAN = [%s]", DEVDEBUGMSG, __FUNCTION__, pstCardDtls->szPAN);
		APP_TRACE(szDbgMsg);
#endif
		iLen = strlen(pstCardDtls->szPAN);

		/* PAN should contain numeric characters */
		if(strspn(pstCardDtls->szPAN, "0123456789") != iLen)
		{
			debug_sprintf(szDbgMsg, "%s: Non numeric PAN", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Card Validation Failed.");
				strcpy(szAppLogDiag, "Invalid PAN Details in Card");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}

			rv = ERR_CARD_INVALID;
			break;
		}

		/* Perform LUHN check on the non-gift card types
		  */
		if((pstCardDtls->iCardType != GIFT_CARD_TYPE) && (pstCardDtls->iCardType != POS_TENDER_TYPE))
		{
			rv = performLUHNCheck(pstCardDtls->szPAN);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: LUHN check FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "LUHN Check Failed for this Card.");
					strcpy(szAppLogDiag, "Invalid PAN Details in Card ");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CARD_INVALID;
				break;
			}
		}

		/* Perform All Zero PAN Check
		  */
		if(iLen == strspn(pstCardDtls->szPAN, "0"))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid PAN: Contains All 0",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid PAN Data");
				strcpy(szAppLogDiag, "Contains All Zeros");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}

			rv = ERR_CARD_INVALID;
			break;
		}

		/* CVV related check */
		if(strlen(pstCardDtls->szCVV) > 0)
		{
			rv = validateCVV(pstCardDtls->szCVV);
			if(rv != SUCCESS)
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Card Validation failed.");
					strcpy(szAppLogDiag, "Invalid CVV Details in card");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				debug_sprintf(szDbgMsg, "%s: Invalid CVV", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: CVV not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* Perform Expiry Date check on the non-gift card types
		*/
		if((pstCardDtls->iCardType != GIFT_CARD_TYPE) && (pstCardDtls->iCardType != POS_TENDER_TYPE) && pstCardDtls->bEmvData == PAAS_FALSE)
		{
			if((strlen(pstCardDtls->szClrMon) == 0) || (strlen(pstCardDtls->szClrYear) == 0))
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Card Validation Failed.");
					strcpy(szAppLogDiag, "Expiry Date Not Present in Card");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				debug_sprintf(szDbgMsg, "%s: Expiry Date is required but not present", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_CARD_INVALID;
				break;
			}
			/* Expiry Date Check */
			rv = checkForExpiry(pstCardDtls->szClrMon, pstCardDtls->szClrYear);
			if(rv != SUCCESS)
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Card Validation Failed.");
					strcpy(szAppLogDiag, "Invalid Expiry Details in Card");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				debug_sprintf(szDbgMsg, "%s: Invalid Expiry Date", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_CARD_INVALID;
				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Expiry Check not required", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: validateCardData
 *
 * Description	: This API This API validates the card data based on CDT record
 * 				   for this card
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
int validateCardData(CDTREC_DTLS_PTYPE pstRecDtls, CARDDTLS_PTYPE pstCardDtls)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));


	while(1)
	{
		if(pstCardDtls->bEmvData == PAAS_TRUE)
		{
			rv = SUCCESS;
			//TODO:AJAYS2:Check is it fine or not
			debug_sprintf(szDbgMsg, "%s: EMV Chip Card is used No need to Validate now", __FUNCTION__);
						APP_TRACE(szDbgMsg);
			break;
		}
		/* Validate PAN */
		rv = validatePAN(pstCardDtls->szPAN, pstRecDtls);
		if(rv != SUCCESS)
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Card Validation Failed.");
				strcpy(szAppLogDiag, "Invalid PAN Details in Card");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}

			debug_sprintf(szDbgMsg, "%s: Invalid PAN", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* CVV related check */
		if(strlen(pstCardDtls->szCVV) > 0)
		{
			rv = validateCVV(pstCardDtls->szCVV);
			if(rv != SUCCESS)
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Card Validation failed.");
					strcpy(szAppLogDiag, "Invalid CVV Details in card");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				debug_sprintf(szDbgMsg, "%s: Invalid CVV", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: CVV not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* Perform Expiry Date check on the card data only if it is required by the merchant
		* (would be specified in the CDT for that particular card range) */

		if(pstRecDtls->bExpDateChkRqd == PAAS_TRUE)
		{
			if((strlen(pstCardDtls->szClrMon) == 0) || (strlen(pstCardDtls->szClrYear) == 0))
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Card Validation Failed.");
					strcpy(szAppLogDiag, "Expiry Date Not Present in Card");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				debug_sprintf(szDbgMsg, "%s: Expiry Date is required but not present", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_CARD_INVALID;
				break;
			}
			/* Expiry Date Check */
			rv = checkForExpiry(pstCardDtls->szClrMon, pstCardDtls->szClrYear);
			if(rv != SUCCESS)
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Card Validation Failed.");
					strcpy(szAppLogDiag, "Invalid Expiry Details in Card");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				debug_sprintf(szDbgMsg, "%s: Invalid Expiry Date", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_CARD_INVALID;
				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Expiry Check not required", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: validateCVV
 *
 * Description	: 
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / ERR_CARD_INVALID
 * ============================================================================
 */
static int validateCVV(char *pszCVV)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = strlen(pszCVV);

	if(strspn(pszCVV, "0123456789") != iLen)
	{
		debug_sprintf(szDbgMsg, "%s: Non numeric CVV", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_CARD_INVALID;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: validatePAN
 *
 * Description	: 
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / ERR_CARD_INVALID
 * ============================================================================
 */
static int validatePAN(char * pszPAN, CDTREC_DTLS_PTYPE pstRecDtls)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((pszPAN == NULL) || (pstRecDtls == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_CARD_INVALID;
			break;
		}

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: PAN = [%s]", DEVDEBUGMSG, __FUNCTION__, 
																		pszPAN);
		APP_TRACE(szDbgMsg);
#endif

		/* PAN length should satisfy the length boundaries specified for that
		 * particular card range */
		iLen = strlen(pszPAN);
		if((iLen < pstRecDtls->iMinPANLen) || (iLen > pstRecDtls->iMaxPANLen) )
		{
			debug_sprintf(szDbgMsg, "%s: PAN length [%d] not in range [%d - %d]"
							,__FUNCTION__, iLen, pstRecDtls->iMinPANLen,
							pstRecDtls->iMaxPANLen);
			APP_TRACE(szDbgMsg);

			rv = ERR_CARD_INVALID;
			break;
		}

		/* PAN should contain numeric characters */
		if(strspn(pszPAN, "0123456789") != iLen)
		{
			debug_sprintf(szDbgMsg, "%s: Non numeric PAN", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_CARD_INVALID;
			break;
		}

		/* Perform LUHN check on the PAN only if it is required by the merchant
		 * (would be specified in the CDT for that particular card range) */
		if(pstRecDtls->bLUHNChkRqd == PAAS_TRUE)
		{
			rv = performLUHNCheck(pszPAN);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: LUHN check FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_CARD_INVALID;
				break;
			}
		}

		/* Perform All Zero PAN Check
		  */
		if(iLen == strspn(pszPAN, "0"))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid PAN: Contains All 0", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_CARD_INVALID;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: performLUHNCheck
 *
 * Description	: 
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / ERR_CARD_INVALID
 * ============================================================================
 */
static int performLUHNCheck(char * pszPAN)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	int		iResult			= 0;
	int		iProduct		= 0;
	int		iMultiplier		= 0;
	int		iCnt			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = strlen(pszPAN);
	iMultiplier = 1;

	for(iCnt = iLen - 1; iCnt >= 0; iCnt--)
	{
		iProduct = (pszPAN[iCnt] - '0') * iMultiplier;
		iResult += (iProduct % 10) + (iProduct / 10);
		iMultiplier ^= 0x0003;
	}

	if((iResult % 10) != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Result Not a multiple of 10",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_CARD_INVALID;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: validateTrackData
 *
 * Description	: 
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
extern int validateTrackData(int trackNo, char * pszTrack)
{
	int		rv					= SUCCESS;
	int		iLen				= 0;
	int	  	iAppLogEnabled 		= isAppLogEnabled();
	char *	cPtr1				= NULL;
	char *	cPtr2				= NULL;
	char	szTrack[120]		= "";
	char  	szAppLogData[300]	= "";
	char  	szAppLogDiag[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		if(pszTrack == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_CARD_INVALID;
			break;
		}

		iLen = strlen(pszTrack);
		if(iLen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: Empty Track data", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}


#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: TRACK = [%s], Length [%d]", DEVDEBUGMSG,__FUNCTION__, pszTrack, iLen);
		APP_TRACE(szDbgMsg);
#endif

		switch(trackNo)
		{
		case 1:

			/*
			 * Checking if Track data contains start and end sentinels
			 * if they present, omit them for track validation purpose
			 */
			if(pszTrack[0] == '%') //Start sentinel for Track1
			{
				debug_sprintf(szDbgMsg, "%s: Track1 contains Start sentinel", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(pszTrack[iLen - 2] == '?') //Last char could be LRC, so checking last but char
				{
					debug_sprintf(szDbgMsg, "%s: Track1 contains End sentinel and LRC", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					memset(szTrack, 0x00, sizeof(szTrack));
					strncpy(szTrack, &pszTrack[1], iLen-3); //-3 : one for Start, another one for end sentinel and other one for LRC

				#ifdef DEVDEBUG
						debug_sprintf(szDbgMsg, "%s:%s: Before removing LRC TRACK = [%s]", DEVDEBUGMSG,__FUNCTION__, pszTrack);
						APP_TRACE(szDbgMsg);
				#endif
						debug_sprintf(szDbgMsg, "%s: Removing LRC from the track data", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						/*
						 * Praveen_P1:
						 * On PWC side LRC has never been approved to be sent after the sentinels.
						 * In some cases our LRC that got generated does not conform to XML standards i.e. '<', '>'
						 * at those times VCL registration is failing
						 * so modifying our application to work with PWC by not sending the LRC in the track data
						 *
						 */
						pszTrack[iLen - 1] = 0x00;

				#ifdef DEVDEBUG
						debug_sprintf(szDbgMsg, "%s:%s: After removing LRC TRACK = [%s]", DEVDEBUGMSG,__FUNCTION__, pszTrack);
						APP_TRACE(szDbgMsg);
				#endif
				}
				else if(pszTrack[iLen - 1] == '?')
				{
					debug_sprintf(szDbgMsg, "%s: Track1 contains End sentinel", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					memset(szTrack, 0x00, sizeof(szTrack));
					strncpy(szTrack, &pszTrack[1], iLen-2); //-2 : one for Start and another one for end sentinel
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Track1 does not contain sentinels", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				memset(szTrack, 0x00, sizeof(szTrack));
				strncpy(szTrack, pszTrack, iLen);
			}

			iLen = strlen(szTrack);
			if(iLen <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: Empty Track data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: TRACK = [%s], Length [%d]", DEVDEBUGMSG,__FUNCTION__, szTrack, iLen);
					APP_TRACE(szDbgMsg);
			#endif

			/* Validate track 1 */

			if(iLen > MAX_TRACK1_LEN)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Track 1 Length [%d]",
															__FUNCTION__, iLen);
				APP_TRACE(szDbgMsg);

				rv = ERR_CARD_INVALID;
				break;
			}

			cPtr1 = strchr(szTrack, '^');
			if(cPtr1 != NULL)
			{
				cPtr2 = strrchr(szTrack, '^');
				if(cPtr2 == cPtr1)
				{
					debug_sprintf(szDbgMsg, "%s: Invalid Track1: only one \'^\'"
																, __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = ERR_CARD_INVALID;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Track1: missing \'^\'",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_CARD_INVALID;
			}

			if(iLen == strspn(szTrack, "0"))
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Track1: Contains All 0",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Invalid Track 1 Data");
					strcpy(szAppLogDiag, "Contains All Zeros");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CARD_INVALID;
				break;
			}

			break;

		case 2:

			if(pszTrack[0] == ';') //Start sentinel for Track2
			{
				debug_sprintf(szDbgMsg, "%s: Track2 contains Start sentinel", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(pszTrack[iLen - 2] == '?') //Last char could be LRC, so checking last but char
				{
					debug_sprintf(szDbgMsg, "%s: Track2 contains End sentinel and LRC", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					memset(szTrack, 0x00, sizeof(szTrack));
					strncpy(szTrack, &pszTrack[1], iLen-3); //-3 : one for Start, another one for end sentinel and other one for LRC

					#ifdef DEVDEBUG
						debug_sprintf(szDbgMsg, "%s:%s: Before removing LRC TRACK = [%s]", DEVDEBUGMSG,__FUNCTION__, pszTrack);
						APP_TRACE(szDbgMsg);
					#endif
						debug_sprintf(szDbgMsg, "%s: Removing LRC from the track data", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						/*
						 * Praveen_P1:
						 * On PWC side LRC has never been approved to be sent after the sentinels.
						 * In some cases our LRC that got generated does not conform to XML standards i.e. '<', '>'
						 * at those times VCL registration is failing
						 * so modifying our application to work with PWC by not sending the LRC in the track data
						 *
						 */
						pszTrack[iLen - 1] = 0x00;

				#ifdef DEVDEBUG
						debug_sprintf(szDbgMsg, "%s:%s: After removing LRC TRACK = [%s]", DEVDEBUGMSG,__FUNCTION__, pszTrack);
						APP_TRACE(szDbgMsg);
				#endif
				}
				else if(pszTrack[iLen - 1] == '?')
				{
					debug_sprintf(szDbgMsg, "%s: Track2 contains End sentinel", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					memset(szTrack, 0x00, sizeof(szTrack));
					strncpy(szTrack, &pszTrack[1], iLen-2); //-2 : one for Start and another one for end sentinel
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Track2 does not contain sentinels", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				memset(szTrack, 0x00, sizeof(szTrack));
				strncpy(szTrack, pszTrack, iLen);
			}

			iLen = strlen(szTrack);
			if(iLen <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: Empty Track data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: TRACK = [%s], Length [%d]", DEVDEBUGMSG,__FUNCTION__, szTrack, iLen);
					APP_TRACE(szDbgMsg);
			#endif

			/* Validate track 2 */

			if(iLen > MAX_TRACK2_LEN)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Track 2 Length [%d]",
															__FUNCTION__, iLen);
				APP_TRACE(szDbgMsg);

				rv = ERR_CARD_INVALID;
				break;
			}

			if(iLen != strspn(szTrack, "1234567890="))
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Track2: extra characters",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_CARD_INVALID;
				break;
			}

			if(iLen == strspn(szTrack, "0"))
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Track2: Contains All 0",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Invalid Track 2 Data");
					strcpy(szAppLogDiag, "Contains All Zeros");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				rv = ERR_CARD_INVALID;
				break;
			}

			cPtr1 = strchr(szTrack, '=');
			if(cPtr1 == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Track2: missing \'=\'",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_CARD_INVALID;
			}

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseTrack1Data
 *
 * Description	: This function is supposed to parse the track 1 data provided
 * 					as parameter
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int parseTrack1Data(char * szTrack1, char** szRSATracks, char** szVsdEncData, CARDDTLS_PTYPE pstCardDtls)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szName[30]			= "";
	char	szPAN[216]			= "";
	char	szExpDate[5]		= "";
	char *	cCurPtr				= NULL;
	char *	cNxtPtr				= NULL;
	char *	pszRSAFingerPrint	= NULL;
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(szTrack1 == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_CARD_INVALID;
			break;
		}

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: TRACK 1 = [%s]", DEVDEBUGMSG,
														__FUNCTION__, szTrack1);
		APP_TRACE(szDbgMsg);
#endif

		/* Validate track 1 data before parsing; do not perform validation for
		 * GIFT cards */
		if((pstCardDtls->iCardType != GIFT_CARD_TYPE) && (pstCardDtls->iCardType != POS_TENDER_TYPE))
		{
			rv = validateTrackData(1, szTrack1);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Track 1 not valid", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Card Validation Failed.");
					strcpy(szAppLogDiag, "Track1 Data is Not Valid");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				break;
			}
		}

		if(szTrack1[0] == '%') //Start sentinel for Track1
		{
			cCurPtr = &szTrack1[1];
		}
		else
		{
			cCurPtr = szTrack1;
		}

		/* Skip the format character */
		/*
		 * Need to check if the first char is Format code or not
		 */
		//cCurPtr = szTrack1;
		if(!(cCurPtr[0] >= '0' && cCurPtr[0] <= '9')) //If it is not digit then skip that character
		{
			cCurPtr = cCurPtr + 1;
		}

		/* 
		 * -----------
		 * Store PAN
		 * -----------
		 */
		cNxtPtr = strchr(cCurPtr, '^');
		if(cNxtPtr == NULL)
		{
			if((pstCardDtls->iCardType == GIFT_CARD_TYPE) || (pstCardDtls->iCardType == POS_TENDER_TYPE))
			{
				/* FIXME: Currently done under assumption that some gift cards dont
				* have the '^' sentinel in their track 1 information */

				if(strlen(cCurPtr) > (sizeof(szPAN) - 1))
				{
					strncpy(szPAN, cCurPtr, sizeof(szPAN) - 1);
				}
				else
				{
					strcpy(szPAN, cCurPtr);
				}
				break;
			}

			debug_sprintf(szDbgMsg, "%s: Invalid track; no PAN", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_CARD_INVALID;
			break;
		}

		memcpy(szPAN, cCurPtr, cNxtPtr - cCurPtr);

		/*
		 * -----------
		 * Store Name
		 * -----------
		 */
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, '^');
		if(cNxtPtr == NULL)
		{
			if(pstCardDtls->iCardType == POS_TENDER_TYPE)
			{
				debug_sprintf(szDbgMsg, "%s: For POS_TENDER Type Card, No Need to check for Card Holder Name", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
			debug_sprintf(szDbgMsg, "%s: Invalid track; no Name", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_CARD_INVALID;
			break;
		}

		memcpy(szName, cCurPtr, cNxtPtr - cCurPtr);

		/*
		 * ----------------------
		 * Store Expiration Date
		 * ----------------------
		 */
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cCurPtr, '\0');
		if((cNxtPtr - cCurPtr) < 4)
		{
			if(pstCardDtls->iCardType == POS_TENDER_TYPE)
			{
				debug_sprintf(szDbgMsg, "%s: For POS_TENDER Type Card, No Need to check for Expiry Date", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
			debug_sprintf(szDbgMsg,"%s: Invalid track; no exp dt",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_CARD_INVALID;
			break;
		}

		memcpy(szExpDate, cCurPtr, 4);

		/*
		 * ------------------
		 * Store Service Code
		 * ------------------
		 */
		cNxtPtr = cCurPtr + 4;
 		memcpy(pstCardDtls->szServiceCode, cNxtPtr, 3); 
		
		break;
	}

	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Storing the data retrieved from trk 1",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Copy the data in the structure, only if not present already,
		 * dont overwrite the value */

		/* Track Information */
		if(strlen(pstCardDtls->szTrackData) == 0)
		{
			if((getEncryptionType() == RSA_ENC) && (szRSATracks != NULL) && (szRSATracks[0] != NULL))
			{
				strcpy(pstCardDtls->szTrackData, szRSATracks[0]);

				pszRSAFingerPrint = getRSAFingerPrint();
				strcpy(pstCardDtls->szEncPayLoad, pszRSAFingerPrint);
			}
			else if((getEncryptionType() == VSD_ENC) && (szVsdEncData != NULL))
			{
				// This will get executed when the used card falls in BIN inclusion range & we get encrypted 
				// data back from FA/XPI
				if((szVsdEncData[0] != NULL) && (szVsdEncData[1] != NULL))	// First is encblob data & second is ksn; IV is optional data
				{
					strcpy(pstCardDtls->szTrackData, szVsdEncData[0]);
					strcpy(pstCardDtls->szEncPayLoad, szVsdEncData[1]);
					if(szVsdEncData[2] != NULL)	// if IV is present
					{
						strcpy(pstCardDtls->szVSDInitVector, szVsdEncData[2]);
					}
				}
				// This will get executed when the used card falls in BIN exclusion range
				// in that case, we will send the clear data to the host
				else if(szVsdEncData[3] != NULL)	// clear track data
				{
					strcpy(pstCardDtls->szTrackData, szVsdEncData[3]);
				}
				if(szVsdEncData[3] != NULL)	// copy clear data in a separate buffer, so we can use it later at other places also
				{
#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: CLEAR TRACK = [%s]", DEVDEBUGMSG,
														__FUNCTION__, szVsdEncData[3]);
					APP_TRACE(szDbgMsg);
#endif
					strcpy(pstCardDtls->szClrTrkData, szVsdEncData[3]);
					// parse the clear PAN from clear track1 data.
					cCurPtr = szVsdEncData[3];
					if((cNxtPtr = strchr(cCurPtr, '^')) != NULL)
					{
						cCurPtr++;
						memset(pstCardDtls->szClrPAN,0x00,sizeof(pstCardDtls->szClrPAN));
						strncpy(pstCardDtls->szClrPAN, cCurPtr, cNxtPtr - cCurPtr);
					}
					else
					{
						//This is required for ensuring the PAN for gift cards is copied
						strncpy(pstCardDtls->szClrPAN, cCurPtr, sizeof(pstCardDtls->szClrPAN)-1);
					}
	#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: CLEAR PAN = [%s]", DEVDEBUGMSG,
														__FUNCTION__, pstCardDtls->szClrPAN);
					APP_TRACE(szDbgMsg);
	#endif
				}

			}
			else
			{
				strcpy(pstCardDtls->szTrackData, szTrack1);
			}
			/*
			 * Praveen_P1: Some of the direct host interfaces need track indicator in the request
			 * storing the track number in the data structure
			 */
			pstCardDtls->iTrkNo = TRACK1_INDICATOR;
		}

		/* PAN */
		if(strlen(pstCardDtls->szPAN) == 0)
		{
			int		iCnt	= 0;
			int		iLen	= 0;

			iLen = strlen(szPAN);
			cCurPtr = pstCardDtls->szPAN;

			for(iCnt = 0; (iCnt < iLen) && (iCnt < sizeof(pstCardDtls->szPAN)); iCnt++)
			{
				/* Copy only the non space characters */
				if(szPAN[iCnt] != ' ')
				{
					*cCurPtr = szPAN[iCnt];
					cCurPtr++;
				}
			}
		}

		/* Name */
		if(strlen(pstCardDtls->szName) == 0)
		{
			strcpy(pstCardDtls->szName, szName);
		}

		/* Expiry Date */
		if(strlen(pstCardDtls->szExpYear) == 0)
		{
			strncpy(pstCardDtls->szExpYear, szExpDate, 2);
			//Daivik:19/7:Copy only as much as Month can hold. Crash was observed in 2.19.23. Porting from 2.19.23B13
			strncpy(pstCardDtls->szExpMon, szExpDate + 2,2);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseTrack2Data
 *
 * Description	: This function is supposed to parse the track 2 data provided
 * 					as parameter
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ERR_CARD_INVALID
 * ============================================================================
 */
static int parseTrack2Data(char * szTrack2, char** szRSATracks, char ** szVsdEncData, CARDDTLS_PTYPE pstCardDtls)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szPAN[21]			= "";
	char	szExpDate[5]		= "";
	char *	cCurPtr				= NULL;
	char *	cNxtPtr				= NULL;
	char*	pszRSAFingerPrint	= NULL;
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(szTrack2 == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_CARD_INVALID;
			break;
		}

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: TRACK 2 = [%s]", DEVDEBUGMSG,
														__FUNCTION__, szTrack2);
		APP_TRACE(szDbgMsg);
#endif
		/* Validate track 2 data before parsing; do not perform validation for
		 * GIFT cards */
		if((pstCardDtls->iCardType != GIFT_CARD_TYPE) && (pstCardDtls->iCardType != POS_TENDER_TYPE))
		{
			rv = validateTrackData(2, szTrack2);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Track 2 not valid", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Card Validation Failed.");
					strcpy(szAppLogDiag, "Track 2 Data is Invalid");
					addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
				}

				break;
			}
		}

		if(szTrack2[0] == ';') //Start Sentinel
		{
			cCurPtr = &szTrack2[1];
		}
		else
		{
			cCurPtr = szTrack2;
		}

		/* 
		 * --------------
		 * Store the PAN 
		 * --------------
		 */
		cNxtPtr = strchr(cCurPtr, '=');
		if(cNxtPtr == NULL)
		{
			/* FIXME: Currently done under assumption that some gift cards dont
			 * have the '=' sentinel in their track 2 */

			if(strlen(cCurPtr) > (sizeof(szPAN) - 1))
			{
				strncpy(szPAN, cCurPtr, sizeof(szPAN) - 1);
			}
			else
			{
				strcpy(szPAN, cCurPtr);
			}

			break;
		}
		else
		{
			memcpy(szPAN, cCurPtr, cNxtPtr - cCurPtr);
		}

		/*
		 * ------------------
		 * Store expiry date
		 * ------------------
		 */
		cCurPtr = cNxtPtr + 1;
		cNxtPtr = strchr(cNxtPtr, '\0');
		if(cNxtPtr - cCurPtr < 4)
		{
			if(pstCardDtls->iCardType == POS_TENDER_TYPE)
			{
				debug_sprintf(szDbgMsg, "%s: For POS_TENDER Type Card, No Need to check for Expiry Date", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			debug_sprintf(szDbgMsg, "%s: Invalid track, exp date missing",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_CARD_INVALID;
			break;
		}

		memcpy(szExpDate, cCurPtr, 4);

		/*
		 * ------------------
		 * Store Service Code
		 * ------------------
		 */
		cNxtPtr = cCurPtr + 4;
 		memcpy(pstCardDtls->szServiceCode, cNxtPtr, 3);    	
		
		break;
	}

	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Storing the data retrieved from trk 2",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Copy the data in the structure, only if not present already,
		 * dont overwrite the value */

		/* Track Information */
		if(strlen(pstCardDtls->szTrackData) == 0)
		{
			if((getEncryptionType() == RSA_ENC) && (szRSATracks != NULL) && (szRSATracks[1] != NULL))
			{
				strcpy(pstCardDtls->szTrackData, szRSATracks[1]);

				pszRSAFingerPrint = getRSAFingerPrint();
				strcpy(pstCardDtls->szEncPayLoad, pszRSAFingerPrint);
			}
			else if((getEncryptionType() == VSD_ENC) && (szVsdEncData != NULL))
			{
				// This will get executed when the used card falls in BIN inclusion range & we get encrypted 
				// data back from FA/XPI
				if((szVsdEncData[0] != NULL) && (szVsdEncData[1] != NULL))	// First is encblob data & second is ksn; IV is optional data
				{
					strcpy(pstCardDtls->szTrackData, szVsdEncData[0]);
					strcpy(pstCardDtls->szEncPayLoad, szVsdEncData[1]);
					if(szVsdEncData[2] != NULL)		// if IV is present
					{
						strcpy(pstCardDtls->szVSDInitVector, szVsdEncData[2]);
					}
				}
				// This will get executed when the used card falls in BIN exclusion range
				// in that case, we will send the clear data to the host
				else if(szVsdEncData[3] != NULL)	// clear track data
				{
					strcpy(pstCardDtls->szTrackData, szVsdEncData[3]);
				}
				if(szVsdEncData[3] != NULL)	// copy clear data in a separate buffer, so we can use later it at other places also
				{
#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: CLEAR TRACK = [%s]", DEVDEBUGMSG,
														__FUNCTION__, szVsdEncData[3]);
					APP_TRACE(szDbgMsg);
#endif
					strcpy(pstCardDtls->szClrTrkData, szVsdEncData[3]);
					// parse the clear PAN from clear track data if present.
					cCurPtr = szVsdEncData[3];
					if((cNxtPtr = strchr(cCurPtr, '=')) != NULL)
					{
						strncpy(pstCardDtls->szClrPAN, cCurPtr, cNxtPtr - cCurPtr);
					}
					else
					{
						//This is required for ensuring the PAN for gift cards is copied
						strncpy(pstCardDtls->szClrPAN, cCurPtr, sizeof(pstCardDtls->szClrPAN)-1);
					}
#ifdef DEVDEBUG
					debug_sprintf(szDbgMsg, "%s:%s: CLEAR PAN = [%s]", DEVDEBUGMSG,
														__FUNCTION__, pstCardDtls->szClrPAN);
					APP_TRACE(szDbgMsg);
#endif
				}
			}
			else
			{
				strcpy(pstCardDtls->szTrackData, szTrack2);
			}
			/*
			 * Praveen_P1: Some of the direct host interfaces need track indicator in the request
			 * storing the track number in the data structure
			 */
			pstCardDtls->iTrkNo = TRACK2_INDICATOR;
		}

		/* PAN */
		if(strlen(pstCardDtls->szPAN) == 0)
		{
			strcpy(pstCardDtls->szPAN, szPAN);
		}

		/* Expiry Date */
		if(strlen(pstCardDtls->szExpYear) == 0)
		{
			strncpy(pstCardDtls->szExpYear, szExpDate, 2);
			//Daivik:19/7:Copy only as much as Month can hold. Crash was observed in 2.19.23. Porting from 2.19.23B13
			strncpy(pstCardDtls->szExpMon, szExpDate + 2, 2);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseNSaveCardInfo
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int parseNSaveCardInfo(CARDDTLS_PTYPE pstCardDtls, char ** szTracks, char** szRSATracks, char ** szVsdEncData)
{
	int			rv					= SUCCESS;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
	char		szAppLogDiag[300]	= "";
	PAAS_BOOL	bParsedTrk1			= PAAS_TRUE;
	PAAS_BOOL	bParsedTrk2			= PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[1024]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Parse the track 2 data first */
	if(szTracks[1] != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Track2 is present, parsing it", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = parseTrack2Data(szTracks[1], szRSATracks, szVsdEncData, pstCardDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse track 2", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Card Validation Failed.");
				strcpy(szAppLogDiag, "Failed to Parse Track2 Data");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}

			bParsedTrk1 = PAAS_FALSE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Track2 is NOT present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Track2 Data is Not Present");
			addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, NULL);
		}
		bParsedTrk1 = PAAS_FALSE;
	}

	/* Parse the track 1 data next */
	if(szTracks[0] != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Track1 is present, parsing it", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = parseTrack1Data(szTracks[0], szRSATracks, szVsdEncData, pstCardDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse track 1", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Card Validation Failed.");
				strcpy(szAppLogDiag, "Failed to Parse Track1 Data");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}

			bParsedTrk2 = PAAS_FALSE;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Track1 is NOT present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Track1 Data is Not Present");
			addAppEventLog(SCA, PAAS_INFO, CAPTURE_DETAILS, szAppLogData, NULL);
		}
		bParsedTrk2 = PAAS_FALSE;
	}

	if( (bParsedTrk1 == PAAS_TRUE) || (bParsedTrk2 == PAAS_TRUE) )
	{
		/* If any one of the tracks was parsed successfully, we should return
		 * success */

		rv = SUCCESS;

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: PAN     = [%s]", DEVDEBUGMSG,
											__FUNCTION__, pstCardDtls->szPAN);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s:%s: NAME    = [%s]", DEVDEBUGMSG,
											__FUNCTION__, pstCardDtls->szName);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s:%s: EXPDATE = [%s/%s]", DEVDEBUGMSG,
				__FUNCTION__, pstCardDtls->szExpMon, pstCardDtls->szExpYear);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s:%s: TRACK   = [%s]", DEVDEBUGMSG,
										__FUNCTION__, pstCardDtls->szTrackData);
		APP_TRACE(szDbgMsg);
#endif
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Both Tracks(1&2) not present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = ERR_BAD_CARD; //Both tracks are not there, so considering as bad card
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

#ifdef DEBUG
/*
 * ============================================================================
 * Function Name: printCDT
 *
 * Description	: This API prints the list of records for testing purpose
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
static void printCDT(CDT_PTYPE pstCDT)
{
	int				iIndex			= 0;
	char			szDbgMsg[256]	= "";
	CDTREC_PTYPE	curRecPtr		= NULL;

	curRecPtr = pstCDT->cdtRecsHead;

	while(curRecPtr != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: CDT Record Number [%d]", __FUNCTION__,
																	iIndex + 1);
		APP_TRACE(szDbgMsg);

		printCDTRecDtls(&(curRecPtr->recDtls));

		iIndex++;
		curRecPtr = curRecPtr->nextRec;
	}

	return;
}

/*
 * ============================================================================
 * Function Name: printCDTRecDtls
 *
 * Description	: This API prints the list of records for testing purpose
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
static void printCDTRecDtls(CDTREC_DTLS_PTYPE pstRecDtls)
{
	char	szDbgMsg[256]	= "";

	APP_TRACE("-------------------------------------------------------------");
	debug_sprintf(szDbgMsg, "%s: Card Type     [%d]", __FUNCTION__,
														pstRecDtls->iCardType);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: PAN Min Len   [%d]", __FUNCTION__,
														pstRecDtls->iMinPANLen);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: PAN Max Len   [%d]", __FUNCTION__,
														pstRecDtls->iMaxPANLen);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: AVS code      [%d]", __FUNCTION__,
														pstRecDtls->iAVSCode);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: TIP Discount  [%d]", __FUNCTION__,
														pstRecDtls->iTipDscnt);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: CVV Code      [%d]", __FUNCTION__,
															pstRecDtls->iCVV2);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: PAN Low       [%s]", __FUNCTION__,
														pstRecDtls->szPANLo);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: PAN High      [%s]", __FUNCTION__,
														pstRecDtls->szPANHi);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Card Abb      [%s]", __FUNCTION__,
														pstRecDtls->szCardAbbr);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Card Label    [%s]", __FUNCTION__,
														pstRecDtls->szCardLbl);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: IPC Label     [%s]", __FUNCTION__,
														pstRecDtls->szIPCLbl);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: IPC Processor [%s]", __FUNCTION__,
													pstRecDtls->szIPCProcId);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Track Req     [%s]", __FUNCTION__,
														pstRecDtls->szTrkRqd);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: SAF Floor     [%s]", __FUNCTION__,
													pstRecDtls->szSAFFlrLim);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Sign Floor    [%s]", __FUNCTION__,
													pstRecDtls->szSigFlrLim);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Gift Min      [%s]", __FUNCTION__,
													pstRecDtls->szGiftMinAmt);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Gift Max      [%s]", __FUNCTION__,
													pstRecDtls->szGiftMaxAmt);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Luhn Check    [%s]", __FUNCTION__,
					(pstRecDtls->bLUHNChkRqd == PAAS_TRUE)? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Exp Check     [%s]", __FUNCTION__,
					(pstRecDtls->bExpDateChkRqd == PAAS_TRUE)?"TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Manual entry  [%s]", __FUNCTION__,
					(pstRecDtls->bManEntryAllwd == PAAS_TRUE)?"TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Sig Line      [%s]", __FUNCTION__,
					(pstRecDtls->bSigLineRqd == PAAS_TRUE) ? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: PIN Req       [%s]", __FUNCTION__,
						(pstRecDtls->bPINRqd == PAAS_TRUE) ? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Card Disabled [%s]", __FUNCTION__,
					(pstRecDtls->bDisableCR == PAAS_TRUE)? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);
	APP_TRACE("-------------------------------------------------------------");

	return;
}
#endif

#if 0
/*
 * This function will load BET file
 * @Param NONE.
 * @Param Output: RSA_NO_ERROR/ FAILURE.
 * */
int loadBETFile(void)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);


    rv = validateAndLoadBETFile(BET_FILENAME);
    if (rv == SUCCESS)
    {
        debug_sprintf(szDbgMsg, "%s - %s is validated and loaded", __FUNCTION__, BET_FILENAME);
        APP_TRACE(szDbgMsg); // BET.DAT found in home directory is validated and loaded
    }

    debug_sprintf(szDbgMsg, "%s - Returning %d", __FUNCTION__, rv);
    APP_TRACE(szDbgMsg);

    return rv;
}

/* *
 *  This function validate the BET file.
 * @Param The absolute path of BET File. If it is Null, the function will check for the BET file in user home directory
 * @Returns TRUE for a valid BET file, else respective error code.
 * */
static int validateAndLoadBETFile(char *pszAbsFilePath)
{
    int		 iRetVal 		= SUCCESS;
    char	 szBuffer[32]	= "";
    FILE 	*fpBET			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    fpBET = fopen(pszAbsFilePath, "r");
    if(fpBET == NULL)
    {
    	debug_sprintf(szDbgMsg, "%s: BET File not found", __FUNCTION__);
    	APP_TRACE(szDbgMsg);
        return FAILURE;
    }

    while(!feof(fpBET))
    {
        memset(szBuffer, 0x00, sizeof(szBuffer));
        if(fgets(szBuffer, sizeof(szBuffer), fpBET)==NULL)
        {
            break;
        }
        /*if the first character is new line or # -> for comment, then ignore it and continue*/
        if ( szBuffer[0] == '\n' || szBuffer[0] == '#' || szBuffer[0] == '\r')
        {
            while(szBuffer[strlen(szBuffer)-1] != '\n')
            {
                memset(szBuffer, 0x00, sizeof(szBuffer));
                if(fgets(szBuffer, sizeof(szBuffer), fpBET)==NULL)
                {
                    break;
                }
            }
            continue;
        }

        if (szBuffer[strlen(szBuffer)-1] == '\n')
        {
            szBuffer[strlen(szBuffer)-1] = 0;/*eliminate the last new line character character*/
        }
        if (szBuffer[strlen(szBuffer)-1] == '\r')
        {
            szBuffer[strlen(szBuffer)-1] = 0;/*eliminate the last new line character character*/
        }

        /*Validate the BET range*/
        if(validateBETRecord(szBuffer, strlen(szBuffer)) != SUCCESS)
        {
            fclose(fpBET);

            debug_sprintf(szDbgMsg, "%s: BET Validation failed : %s ",__FUNCTION__, szBuffer);
        	APP_TRACE(szDbgMsg);

            syslog(LOG_INFO|LOG_USER, "BET Validation failed");

            iRetVal = FAILURE;
            break;
        }
    }

    if(pstBET == NULL)
    {
    	syslog(LOG_WARNING, "NO entries in BET file..");
    }
    fclose(fpBET);

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

    return iRetVal;
}

/*
 * Validate the BET Record and returns the status.
 * @Param char array containing BET Record.
 * @Param Lenght of BET Record.
 * @Returns TRUE for a Valid BET Range, else FALSE.
 * */
static int validateBETRecord(char *szData, int size)
{
    int 			iSepFlag = 0;
    int				j 		 = 0;
    char 			lData[20]= "";
    char			rData[20]= "";
    long 			lLower;
    long			lUpper;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    while(j < size)
    {
        if(szData[j] == '-')
        {
            iSepFlag = j;
        }
        else if(j == size-1)
        {
            if(szData[j] != ';')// Last character should be semi colon
            {
                debug_sprintf(szDbgMsg, "%s: Last Character for BET Record : %s is not semicolon ", __FUNCTION__, szData);
            	APP_TRACE(szDbgMsg);
                return FAILURE;
            }
        }
        else if(szData[j] < 48 || szData[j] > 57) // Check for non numeric value
        {
            debug_sprintf(szDbgMsg, "%s: BET Record : %s contains invalid data", __FUNCTION__, szData);
        	APP_TRACE(szDbgMsg);
            return FAILURE;
        }
        j++;
    }

    if(iSepFlag == 0){//Check for field seperator '-'
        debug_sprintf(szDbgMsg, "%s: Field seperator is missing for BET Record : %s", __FUNCTION__, szData);
    	APP_TRACE(szDbgMsg);
        return FAILURE;
    }
    memcpy(&lData, &szData[0], iSepFlag);
    memcpy(&rData, &szData[iSepFlag+1], size-(iSepFlag+2));

    if((strlen(lData) != 6) || (strlen(rData) != 6))  // Length of LHS and RHS of each BET record should be 6
    {
        debug_sprintf(szDbgMsg, "%s: Length of BET Record : %s is not valid ", __FUNCTION__, szData);
    	APP_TRACE(szDbgMsg);
        return FAILURE;
    }

    lLower = lUpper = 0;
    lLower = strtol(lData, NULL, 10);
    lUpper = strtol(rData, NULL, 10);
    if(lLower > lUpper)
    {
        debug_sprintf(szDbgMsg, "%s: BET Lower limit is greater than upper limit : %s ", __FUNCTION__, szData);
    	APP_TRACE(szDbgMsg);
        return FAILURE;
    }

    return addBETRecord(lLower, lUpper);
}

/*
 * Add input BET Range to Global Linked list.
 * @Param Lower range of the BET.
 * @Param Upper Range of the BET.
 * */
static int addBETRecord(long lLower, long lUpper)
{
    BETRECORD_PTYPE pszTemp		= NULL;
    BETRECORD_PTYPE pszTemp2	= NULL;

    pszTemp = (BETRECORD_PTYPE) malloc(sizeof(BETRECORD_STYPE));
    pszTemp->next = NULL;

    pszTemp->lower = lLower;
    pszTemp->upper = lUpper;


    if(pszTemp == NULL)
    {
        pstBET = pszTemp;
    }
    else
    {
    	pszTemp2 = pstBET;
        while(pszTemp2->next != NULL)
        {
        	pszTemp2 = pszTemp2->next;
        }
        pszTemp2->next = pszTemp;
    }

    return SUCCESS;
}

/*
 * This function will check whether the input bin falls in any of the BIN Exclusion range.
 * @Param char array containing Bin to check.
 * @Return TRUE if Bin belongs to any of the Exclusion Range, Else FALSE.
 * */
PAAS_BOOL checkBinExclusion(char *pszBin)
{
	PAAS_BOOL		rv = PAAS_FALSE;
    long		 	lbin;
    BETRECORD_PTYPE pszTemp;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

#ifdef DEVDEBUG
    debug_sprintf(szDbgMsg, "DEVDEBUG - BIN in checkBINExclusion is: %s", pszBin);
    APP_TRACE(szDbgMsg);
#endif

    lbin 	= strtol(pszBin, NULL, 10);
    pszTemp = pstBET;

    while(pszTemp != NULL)
    {

#ifdef DEVDEBUG
    	debug_sprintf(szDbgMsg, "%s: DEVDEBUG - Lower BIN: %ld, Upper BIN: %ld", __FUNCTION__, pszTemp->lower, pszTemp->upper);
    	APP_TRACE(szDbgMsg);
#endif

        if((lbin >= pszTemp->lower) && (lbin <= pszTemp->upper))
        {
        	debug_sprintf(szDbgMsg, "%s - BIN Excluded !", __FUNCTION__);
        	APP_TRACE(szDbgMsg);
            rv = PAAS_TRUE;
        }

        pszTemp = pszTemp->next;
    }

	debug_sprintf(szDbgMsg, "%s - BIN Not Excluded !", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: --- Returning --- %d",__FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

    return rv;
}
#endif

/*
 * ============================================================================
 * End of file cardDataTable.c
 * ============================================================================
 */
