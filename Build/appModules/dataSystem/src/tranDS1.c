/*
 * =============================================================================
 * Filename    : tranDS1.c
 *
 * Application : Mx Point SCA
 *
 * Description : This file contains APIs to fetch data from the transaction
 * 					instances
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "uiAgent/emvAPIs.h"
#include "common/common.h"
#include "common/utils.h"
#include "db/dataSystem.h"
#include "db/tranDS.h"
#include "appLog/appLogAPIs.h"

extern PAAS_BOOL checkBinInclusion(char *);
extern void getEmbossedPANFromTrackData(char *, char *, PAAS_BOOL );
extern int getDataFromVCL(char*, char *, char*, char*, PAAS_BOOL);
extern char * getMerchantProcessor(int);
extern void getMaskedPAN(char *, char *);
extern int getCardEntryMode(CARDDTLS_PTYPE, char *);

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);


/*TODO: Move this static variable into proper place or into structre later*/
static int adminPcktReq = 0; //FALSE.
/*
 * ============================================================================
 * Function Name: getFxnNCmdForTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getFxnNCmdForTran(char * szTranKey, int * piFxn, int * piCmd)
{
	int			rv				= SUCCESS;
	TRAN_PTYPE	pstTran			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	while(1)
	{
		if((szTranKey == NULL) || (piFxn == NULL) || (piCmd == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		*piFxn = pstTran->iFxn;
		*piCmd = pstTran->iCmd;

		break;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPOSSecDtlsForTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getPOSSecDtlsForTran(char * szTranKey, POSSEC_PTYPE pstPOSSec)
{
	int			rv				= SUCCESS;
	TRAN_PTYPE	pstTran			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstPOSSec == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Missing sec dtls in tran",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memcpy(pstPOSSec, pstTran->data, sizeof(POSSEC_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSetParmDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getSetParmDtlsForDevTran(char * szTranKey, SETPARM_PTYPE pstSetParmDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstSetParmDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstSetParmDtls, &(pstDevTran->stSetParmDtls), sizeof(SETPARM_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getGetParmDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getGetParmDtlsForDevTran(char * szTranKey, GETPARM_PTYPE pstGetParmDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstGetParmDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstGetParmDtls, &(pstDevTran->stGetParmDtls), sizeof(GETPARM_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: getDispMsgDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getDispMsgDtlsForDevTran(char * szTranKey, DISPMSG_PTYPE pstDispMsgDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstDispMsgDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstDispMsgDtls, &(pstDevTran->stDispMsgDtls), sizeof(DISPMSG_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTimeDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getTimeDtlsForDevTran(char * szTranKey, TIMEDTLS_PTYPE pstTimeDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstTimeDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstTimeDtls, &(pstDevTran->stTimeDtls), sizeof(TIMEDTLS_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCounterDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getCounterDtlsForDevTran(char * szTranKey, COUNTERDTLS_PTYPE pstCounterDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstCounterDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstCounterDtls, &(pstDevTran->stCounterDtls), sizeof(COUNTERDTLS_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDevNameDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getDevNameDtlsForDevTran(char * szTranKey, DEVICENAME_PTYPE pstDevNameDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstDevNameDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstDevNameDtls, &(pstDevTran->stDevNameDtls), sizeof(DEVICENAME_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSigDtlsForDevTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getSigDtlsForDevTran(char * szTranKey, SIGDTLS_PTYPE pstSigDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstSigDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstSigDtls, &(pstDevTran->stSigDtls), sizeof(SIGDTLS_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getVerDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getVerDtlsForDevTran(char * szTranKey, VERDTLS_PTYPE pstVer)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstVer == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstVer, &(pstDevTran->stVerDtls), sizeof(VERDTLS_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getLtydtlsForDevTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getLtyDtlsForDevTran(char * szTranKey, LTYDTLS_PTYPE pstLty)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstLty == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstLty, &(pstDevTran->stLtyDtls), sizeof(LTYDTLS_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}


		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getProvisionPassDtlsForDevTran
 *
 * Description	: This function will load the Provision Pass details from device
 * 				  transaction instance to given structure(PROVISION_PASS_PTYPE)
 *
 * Input Params	: Transaction key , structure to fill provision Pass details
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getProvisionPassDtlsForDevTran(char * szTranKey, PROVISION_PASS_PTYPE pstProvPassDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstProvPassDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstProvPassDtls, &(pstDevTran->stProvPassDtls), sizeof(PROVISION_PASS_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}


		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCharityDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getCharityDtlsForDevTran(char * szTranKey, CHARITYDTLS_PTYPE pstCharity)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstCharity == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstCharity, &(pstDevTran->stCharityDtls), sizeof(CHARITYDTLS_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}


		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}



/*
 * ============================================================================
 * Function Name: getCardDataDtlsForDevTran
 *
 * Description	:This Function will be used for copying the Details of Card data
 * 				 from DEVTRAN_PTYPE Tran data to GET_CARDDATA_PTYPE structure.
 *
 * Input Params	:Transaction Key, GET_CARDDATA_PTYPE structure
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getCardDataDtlsForDevTran(char * szTranKey, GET_CARDDATA_PTYPE pstGetCardData)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstGetCardData == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstGetCardData, &(pstDevTran->stGetCardDataDtls), sizeof(GET_CARDDATA_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/* ============================================================================
* Function Name: getCreditAppDtlsForDevTran
*
* Description	:
*
* Input Params	:
*
* Output Params: SUCCESS/ FAILURE
* ============================================================================
*/
int getCreditAppDtlsForDevTran(char * szTranKey, CREDITAPPDTLS_PTYPE pstCreditApp)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstCreditApp == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstCreditApp, &(pstDevTran->stCreditAppDtls), sizeof(CREDITAPPDTLS_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is Not Present in Transaction Instance",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}


		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/* ============================================================================
* Function Name: getCustButtonDtlsForDevTran
*
* Description	:
*
* Input Params	:
*
* Output Params: SUCCESS/ FAILURE
* ============================================================================
*/
int getCustButtonDtlsForDevTran(char * szTranKey, CUSTBUTTONDTLS_PTYPE pstCustButton)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstCustButton == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstCustButton, &(pstDevTran->stCustButtonDtls), sizeof(CUSTBUTTONDTLS_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}


		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/* ============================================================================
* Function Name: getLaneClosedDtlsForDevTran
*
* Description	:
*
* Input Params	:
*
* Output Params: SUCCESS/ FAILURE
* ============================================================================
*/
int getLaneClosedDtlsForDevTran(char *szTranKey, LANECLOSED_PTYPE pstLaneDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstLaneDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstLaneDtls, &(pstDevTran->stLaneClosed), sizeof(LANECLOSED_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Display Data is not present currently",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSurveyDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getSurveyDtlsForDevTran(char * szTranKey, SURVEYDTLS_PTYPE pstSurvey)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstSurvey == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstSurvey, &(pstDevTran->stSurveyDtls), sizeof(SURVEYDTLS_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}


		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSAFDtlsForDevTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getSAFDtlsForDevTran(char * szTranKey, SAFDTLS_PTYPE pstSAFDtls)
{
	int				rv				= SUCCESS;
	SAFDTLS_PTYPE	pstLocSAF		= NULL;
	TRAN_PTYPE		pstTran			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstSAFDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		pstLocSAF = (SAFDTLS_PTYPE) pstTran->data;
		if(pstLocSAF == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		memcpy(pstSAFDtls, pstLocSAF, sizeof(SAFDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSessDtlsFrmPOSReq
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getSessDtlsFrmPOSReq(char * szTranKey, SESSDTLS_PTYPE pstSess)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	SESSDTLS_PTYPE	pstLocSess		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstSess == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		pstLocSess = (SESSDTLS_PTYPE) pstTran->data;
		if(pstLocSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		memcpy(pstSess, pstLocSess, sizeof(SESSDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getLIReqDtlsForTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getLIReqDtlsForTran(char * szTranKey, LIINFO_PTYPE pstLIInfo)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	LIINFO_PTYPE	pstLocLIInfo	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstLIInfo == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		pstLocLIInfo = (LIINFO_PTYPE) pstTran->data;
		if(pstLocLIInfo == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}


		memcpy(pstLIInfo, pstLocLIInfo, sizeof(LIINFO_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getLIDtlsFromSession
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getLIDtlsFromSession(char * szSessKey, LIINFO_PTYPE pstLIInfo)
{
	int				rv				= SUCCESS;
	SESSDTLS_PTYPE	pstSess			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szSessKey == NULL) || (pstLIInfo == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getSession(szSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstSess->pstLIInfo == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No LI dtls in SESSION", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		memcpy(pstLIInfo, pstSess->pstLIInfo, sizeof(LIINFO_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPymntTypeDtlsFromSession
 *
 * Description	: This function gives payment type stored in the session dtls.
 *
 * Input Params	: Pointer to store payment type.
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getPymntTypeDtlsFromSession(char * szSessKey, int *piPymntType)
{
	int				rv				= SUCCESS;
	SESSDTLS_PTYPE	pstSess			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szSessKey == NULL) || (piPymntType == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getSession(szSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Session details are not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			*piPymntType = -1; //Payment type not found.
			break;
		}

		*piPymntType = pstSess->iPaymentType;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getFxnNCmdForSSITran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getFxnNCmdForSSITran(char * szTranKey, int * piFxn, int * piCmd)
{
	int			rv				= SUCCESS;
	TRAN_PTYPE	pstTran			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (piFxn == NULL) || (piCmd == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the function and command */
		*piFxn = pstTran->iFxn;
		*piCmd = pstTran->iCmd;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getGenRespDtlsForSSITran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getGenRespDtlsForSSITran(char *szTranKey, RESPDTLS_PTYPE pstRespDtls)
{
	int			rv					= SUCCESS;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[512]	= "";
	TRAN_PTYPE	pstTran				= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstRespDtls == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to get the General Response Details of the SSI Transaction");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			break;
		}

		/*  Copying the elements from one structure to another */
		strcpy(pstRespDtls->szRespTxt, pstTran->stRespDtls.szRespTxt);
		strcpy(pstRespDtls->szRslt, pstTran->stRespDtls.szRslt);
		strcpy(pstRespDtls->szRsltCode, pstTran->stRespDtls.szRsltCode);
		strcpy(pstRespDtls->szTermStat, pstTran->stRespDtls.szTermStat);
		strcpy(pstRespDtls->szHostRsltCode, pstTran->stRespDtls.szHostRsltCode);
		strcpy(pstRespDtls->szRespCode, pstTran->stRespDtls.szRespCode);

		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "Result Code[%s] Response Text [%s]", pstTran->stRespDtls.szRsltCode, pstTran->stRespDtls.szRespTxt);
			addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
		}

		break; //Breaking from the while loop
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getOtherGenRespDtlsForSSITran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getOtherGenRespDtlsForSSITran(char *szTranKey, OTHER_RESPDTLS_PTYPE pstOtherRespDtls)
{
	int				rv					= SUCCESS;
	int     		iPymtType    		= -1;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[512]	= "";
	char 			szBIN[6+1]			= "";
	ENC_TYPE		iEncType			= getEncryptionType();
	TRAN_PTYPE		pstTran				= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstOtherRespDtls == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to get the General Response Details of the SSI Transaction");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Payment dtls not found in the SSI tran from stack", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		//Fill transaction amount details
		if(pstPymtDtls->pstAmtDtls != NULL)
		{
			strcpy(pstOtherRespDtls->szTranAmt, pstPymtDtls->pstAmtDtls->tranAmt);
		}

		/* Get the payment type of the current transaction */
		rv = getPymtTypeForPymtTran(szTranKey, &iPymtType);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get pymt type", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Payment Type From the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			break; // breaking from here, no more states!
		}

		switch(iPymtType)
		{
		case PYMT_DEBIT:
			strcpy(pstOtherRespDtls->szPymntType, "DEBIT");
			break;
		case PYMT_GIFT:
			strcpy(pstOtherRespDtls->szPymntType, "GIFT");
			break;
		case PYMT_MERCHCREDIT:
			strcpy(pstOtherRespDtls->szPymntType, "MERCH_CREDIT");
			break;

		case PYMT_CREDIT:
		case PYMT_FSA:
		case PYMT_GOOGLE:
		case PYMT_ISIS:
			strcpy(pstOtherRespDtls->szPymntType, "CREDIT");
			break;
		case PYMT_PRIVATE:
			strcpy(pstOtherRespDtls->szPymntType, "PRIV_LBL");
			break;
		case PYMT_PAYPAL:
			strcpy(pstOtherRespDtls->szPymntType, "PAYPAL");
			break;

		case PYMT_CASH:
			strcpy(pstOtherRespDtls->szPymntType, "CASH");
			break;

		case PYMT_EBT:
			strcpy(pstOtherRespDtls->szPymntType, "EBT");
			break;

		case PYMT_PAYACCOUNT:
			strcpy(pstOtherRespDtls->szPymntType, "PAYACCOUNT");
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Invalid payment type, should never come here!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		/*Copying the ebt card type inorder to fill the card details*/
		if(pstPymtDtls->pstEBTDtls != NULL)
		{
			strcpy(pstOtherRespDtls->szEBTType, pstPymtDtls->pstEBTDtls->szEBTType);
		}

		if(pstPymtDtls->pstCardDtls != NULL)
		{
			if(strlen(pstOtherRespDtls->szEmbossedPAN) <= 0)
			{
				//Embossed Card Num is not present in the Card Details, will update here
				if((pstPymtDtls->iPymtType == PYMT_GIFT && returnEmbossedNumForGiftType() &&
					isEmbossedCardNumCalcRequired() && (strlen(pstPymtDtls->pstCardDtls->szPAN) > 0) &&
					(strcasecmp("SVDOT", (char *)getMerchantProcessor(PROCESSOR_GIFT)) == 0)))
				{
					/*Calculate the Embossed Card Number for Gift Transactions with ValueLink Processors as per the logic
					we already implemented in RCHI*/
					if(iEncType == VSP_ENC)
					{
						getEmbossedPANFromTrackData(pstPymtDtls->pstCardDtls->szPAN, pstPymtDtls->pstCardDtls->szEmbossedPAN, pstPymtDtls->pstCardDtls->bManEntry);
						strcpy(pstOtherRespDtls->szEmbossedPAN, pstPymtDtls->pstCardDtls->szEmbossedPAN);
					}
				}
				else
				{
					if( ((returnEmbossedNumForGiftType() && pstPymtDtls->iPymtType == PYMT_GIFT) ||
							(returnEmbossedNumForPrivLblType() && pstPymtDtls->iPymtType == PYMT_PRIVATE)) && (strlen(pstPymtDtls->pstCardDtls->szPAN) > 0) )
					{
						memset(szBIN, 0x00, sizeof(szBIN));
						strncpy(szBIN, pstPymtDtls->pstCardDtls->szPAN, 6);

						if(checkBinInclusion(szBIN) == PAAS_FALSE || (strlen(pstPymtDtls->pstCardDtls->szPAN) <= 12))
						{
							debug_sprintf(szDbgMsg, "%s: Card is NOT Included in PCI Card Range, updating Embossed Card number", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							if(iEncType == VSP_ENC)
							{
								strcpy(pstPymtDtls->pstCardDtls->szEmbossedPAN, pstPymtDtls->pstCardDtls->szPAN);
								getDataFromVCL(pstPymtDtls->pstCardDtls->szEmbossedPAN, NULL, NULL, NULL, pstPymtDtls->pstCardDtls->bManEntry);
								strcpy(pstOtherRespDtls->szEmbossedPAN, pstPymtDtls->pstCardDtls->szEmbossedPAN);

								/*
								 * Praveen_P1: Putting extra check here so that we will not send pci clear range
								 */
								if(strlen(pstOtherRespDtls->szEmbossedPAN) > 0)
								{
									memset(szBIN, 0x00, sizeof(szBIN));
									strncpy(szBIN, pstOtherRespDtls->szEmbossedPAN, 6);
									if(checkBinInclusion(szBIN) == PAAS_TRUE && (strlen(pstPymtDtls->pstCardDtls->szPAN) > 12))
									{
										debug_sprintf(szDbgMsg, "%s: PAN is Included in PCI Card Range, not sending Embossed Card number", __FUNCTION__);
										APP_TRACE(szDbgMsg);
										memset(pstOtherRespDtls->szEmbossedPAN, 0x00, sizeof(pstOtherRespDtls->szEmbossedPAN));
									}
								}

							}
							else if(iEncType == VSD_ENC)
							{
								strcpy(pstPymtDtls->pstCardDtls->szEmbossedPAN, pstPymtDtls->pstCardDtls->szClrPAN);
								strcpy(pstOtherRespDtls->szEmbossedPAN, pstPymtDtls->pstCardDtls->szEmbossedPAN);
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Card is Included in PCI Card Range, not updating Embossed Card number", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
					}
				}
			}

			strcpy(pstOtherRespDtls->szPaymentMedia, pstPymtDtls->pstCardDtls->szPymtMedia);
			strcpy(pstOtherRespDtls->szCardAbbrv, pstPymtDtls->pstCardDtls->szCardAbbrv);
			strcpy(pstOtherRespDtls->szCardHolder, pstPymtDtls->pstCardDtls->szName);
			/*T_RaghavendranR1 : PTMX 773 Fix, Add masked ACCT_NUM to POS on Token Query Failures*/
			getMaskedPAN(pstPymtDtls->pstCardDtls->szPAN, pstOtherRespDtls->szPAN); //Masking the PAN before sending
			strcpy(pstOtherRespDtls->szClrMon, pstPymtDtls->pstCardDtls->szClrMon);
			strcpy(pstOtherRespDtls->szClrYear, pstPymtDtls->pstCardDtls->szClrYear);
			getCardEntryMode(pstPymtDtls->pstCardDtls, pstOtherRespDtls->szCardEntryMode);
		}


		break; //Breaking from the while loop
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getPymtFlowForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getPymtFlowForPymtTran(char * szTranKey, PAAS_BOOL * bRegPymtFlow)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	TRAN_PTYPE		pstTran				= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (bRegPymtFlow == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Payment Details Not Present in the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		/* Get the payment type */
		*bRegPymtFlow = pstPymtDtls->bRegPymtFlow;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getPymtTypeForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getPymtTypeForPymtTran(char * szTranKey, int * piPymtType)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	TRAN_PTYPE		pstTran				= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (piPymtType == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Payment Details Not Present in the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		/* Get the payment type */
		*piPymtType = pstPymtDtls->iPymtType;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: getVASmodeForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getVASmodeForPymtTran(char * szTranKey, PAAS_BOOL *bVASMode)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	TRAN_PTYPE		pstTran				= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (bVASMode == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Payment Details Not Present in the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		/* Get the payment type */
		*bVASMode = pstPymtDtls->bCheckVASMode;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif
/*
 * ============================================================================
 * Function Name: getPymtSubTypeForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getPymtSubTypeForPymtTran(char * szTranKey, int * piPymtSubType)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	TRAN_PTYPE		pstTran				= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (piPymtSubType == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Payment Details Not Present in the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		/* Get the payment Sub type */
		*piPymtSubType = pstPymtDtls->iPymtSubType;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCustInfoDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getCustInfoDtlsForPymtTran(char * szTranKey, CUSTINFODTLS_PTYPE	pstCustInfoDtls)
{
	int						rv					= SUCCESS;
	int						iAppLogEnabled		= isAppLogEnabled();
	char					szAppLogData[300]	= "";
	TRAN_PTYPE				pstTran				= NULL;
	PYMTTRAN_PTYPE			pstPymtDtls			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Check for Parameters */
		if( (szTranKey == NULL) || (pstCustInfoDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Payment Details Not Present in the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}
		if(pstPymtDtls->pstCustInfoDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Customer Info Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstCustInfoDtls, pstPymtDtls->pstCustInfoDtls, sizeof(CUSTINFODTLS_STYPE));
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPrivCrdSAFTranIndForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getPrivCrdSAFTranIndForPymtTran(char * szTranKey, PAAS_BOOL * bPrvCrdSAFAllwd)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (bPrvCrdSAFAllwd == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the Private Card SAF Allow Tran Indicator */
		*bPrvCrdSAFAllwd = pstPymtDtls->bPrvCrdSAFAllwd;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSAFTranIndicatorForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getSAFTranIndicatorForPymtTran(char * szTranKey, PAAS_BOOL * bSAFTranFlg)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (bSAFTranFlg == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SAF Tran Indicator */
		*bSAFTranFlg = pstPymtDtls->bSAFTran;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPaypalPaymentCodeForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getPaypalPaymentCodeForPymtTran(char * szTranKey, char * pszPymtCode)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pszPymtCode == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstCardDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Card Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Get the Paypal Payment Code */
		strcpy(pszPymtCode, pstPymtDtls->pstCardDtls->szPayPalPymtCode);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getHashVaueOfCurPANForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getHashVaueOfCurPANForPymtTran(char * szTranKey, char * pszCurPANHashValue)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pszCurPANHashValue == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the Current Hash Value of the PAN */
		strcpy(pszCurPANHashValue, pstPymtDtls->szPANHashValue);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCardTypeForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getCardTypeForPymtTran(char * szTranKey, char * pszCardType)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pszCardType == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the card type */
		strcpy(pszCardType, pstPymtDtls->szCardType);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTranDateForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getTranDateForPymtTran(char * szTranKey, char * pszDate)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pszDate == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction date */
		strcpy(pszDate, pstPymtDtls->szTranDt);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTranTimeForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getTranTimeForPymtTran(char * szTranKey, char * pszTime)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pszTime == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction time */
		strcpy(pszTime, pstPymtDtls->szTranTm);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getForceFlagForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getForceFlagForPymtTran(char * szTranKey, PAAS_BOOL * pbForce)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pbForce == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* get the force flag */
		*pbForce = pstPymtDtls->bForce;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getBillPayDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getBillPayDtlsForPymtTran(char * szTranKey, PAAS_BOOL * pbBillPay)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pbBillPay == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* get the Bill Pay flag */
		*pbBillPay = pstPymtDtls->bBillPay;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setHashVaueOfCurPANForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int setHashVaueOfCurPANForPymtTran(char * szTranKey, char * pszCurPANHashValue)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pszCurPANHashValue == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the Current Hash Value of the PAN */
		strcpy(pstPymtDtls->szPANHashValue, pszCurPANHashValue);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDupTranIndicatorForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getDupTranIndicatorForPymtTran(char * szTranKey, PAAS_BOOL *pbDupTranDetectedflg)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if( szTranKey == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transaction from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the Duplicate transaction indicator */
		*pbDupTranDetectedflg = pstPymtDtls->bDupTranDetected;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPostAuthCardDtlsIndicatorForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getPostAuthCardDtlsIndicatorForPymtTran(char * szTranKey, PAAS_BOOL *bPostAuthCardDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if( szTranKey == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transaction from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the Duplicate transaction indicator */
		*bPostAuthCardDtls = pstPymtDtls->bPostAuthCardDtls;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getManualFlagForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getManualFlagForPymtTran(char * szTranKey, PAAS_BOOL * pbMan)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pbMan == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* get the manual flag */
		*pbMan = pstPymtDtls->bManEntry;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDCCNotAllowedFlagForPymtTran
 *
 * Description	: getting the flag which received from POS dcc not allowed
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getDCCNotAllowedFlagForPymtTran(char * szTranKey, PAAS_BOOL * pbDCCNotAllowed)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pbDCCNotAllowed == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* get the manual flag */
		*pbDCCNotAllowed = pstPymtDtls->bDCCNotAllowed;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getManPromptOptionsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getManPromptOptionsForPymtTran(char * szTranKey, int * piManPrmptOptns)
{
	int				rv					= SUCCESS;
	char			szManPrmptOptns[50]	= "";
	char*			ptrStr				= NULL;
	TRAN_PTYPE		pstTran				= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (piManPrmptOptns == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the manual prompt options */
		memset(szManPrmptOptns, 0x00, sizeof(szManPrmptOptns));
		strcpy(szManPrmptOptns, pstPymtDtls->szManPrmptOptns);

		if( (ptrStr = strstr(szManPrmptOptns, "PAN")) != NULL )
		{
			*piManPrmptOptns |= ( 1 << MANUAL_PAN_PROMPT );
		}
		if( (ptrStr = strstr(szManPrmptOptns, "EXP")) != NULL )
		{
			*piManPrmptOptns |= ( 1 << MANUAL_EXP_PROMPT );
		}
		if( (ptrStr = strstr(szManPrmptOptns, "CVV")) != NULL )
		{
			*piManPrmptOptns |= ( 1 << MANUAL_CVV_PROMPT );
		}
		if( (ptrStr = strstr(szManPrmptOptns, "ZIP")) != NULL )
		{
			*piManPrmptOptns |= ( 1 << MANUAL_ZIP_PROMPT );
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDupTranDetectionDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getDupTranDetectionDtlsForPymtTran(char * szTranKey, char *pszAllowDupTran, PAAS_BOOL * pbDupOverrideFlag)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if( (szTranKey == NULL) || (pszAllowDupTran == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transaction from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if( strlen(pstPymtDtls->szAllowDupTran) > 0)
		{
			strcpy(pszAllowDupTran, pstPymtDtls->szAllowDupTran);

			/* Set the Dup Tran Override flag as Enabled */
			*pbDupOverrideFlag = PAAS_TRUE;
		}
		else
		{
			/* Set the Dup Tran Override flag as disabled */
			*pbDupOverrideFlag = PAAS_FALSE;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getBatchTraceIdDtlsForPymtTran
 *
 * Description	: This function gets the batch trace id that is stored in the
 * 				  Payment instance
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getBatchTraceIdDtlsForPymtTran(char * szTranKey, char *pszBatchTraceId)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if( (szTranKey == NULL) || (pszBatchTraceId == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transaction from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if( strlen(pstPymtDtls->szBatchTraceId) > 0)
		{
			strcpy(pszBatchTraceId, pstPymtDtls->szBatchTraceId);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSplitTndrFlagForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getSplitTndrFlagForPymtTran(char * szTranKey, PAAS_BOOL * pbSplit)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pbSplit == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get teh spli tender flag */
		*pbSplit = pstPymtDtls->bSplitTender;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSessDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getSessDtlsForPymtTran(char * szTranKey, SESSDTLS_PTYPE pstSess)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstSess == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Session Details missing", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstSess, pstPymtDtls->pstSess, sizeof(SESSDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getChkDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getChkDtlsForPymtTran(char * szTranKey, CHKDTLS_PTYPE pstChkDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstChkDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstChkDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Check Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstChkDtls, pstPymtDtls->pstChkDtls, sizeof(CHKDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getAmtDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getAmtDtlsForPymtTran(char * szTranKey, AMTDTLS_PTYPE pstAmtDtls)
{
	int				rv					= SUCCESS;
	char			szAppLogData[300]	= "";
	TRAN_PTYPE		pstTran				= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstAmtDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstAmtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Amount Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstAmtDtls, pstPymtDtls->pstAmtDtls, sizeof(AMTDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCardDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getCardDtlsForPymtTran(char * szTranKey, CARDDTLS_PTYPE pstCardDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstCardDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstCardDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Card Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstCardDtls, pstPymtDtls->pstCardDtls, sizeof(CARDDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDccDtlsForPymtTran
 *
 * Description	: Function copies the DCC details from payment instance to the
 *					given structure
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getDccDtlsForPymtTran(char * szTranKey, DCCDTLS_PTYPE pstDccDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstDccDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstDCCDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Card Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstDccDtls, pstPymtDtls->pstDCCDtls, sizeof(DCCDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getFSADtlsForPymtTran
 *
 * Description	: Returns the FSA Transactions Details filled in the structure
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getFSADtlsForPymtTran(char * szTranKey, FSADTLS_PTYPE pstFSADtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstFSADtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstFSADtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing FSA Card Details in Payment Details Structure", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstFSADtls, pstPymtDtls->pstFSADtls, sizeof(FSADTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getEBTDtlsForPymtTran
 *
 * Description	: Returns the EBT Transactions Details filled in the structure
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getEBTDtlsForPymtTran(char * szTranKey, EBTDTLS_PTYPE pstEBTDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstEBTDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstEBTDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing EBT Details in Payment Details Structure", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstEBTDtls, pstPymtDtls->pstEBTDtls, sizeof(EBTDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCardDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getCardDtlsForRptTran(char * szTranKey, CARDDTLS_PTYPE pstCardDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PASSTHRU_PTYPE	pstPassThru     = NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstCardDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPassThru = (PASSTHRU_PTYPE) (pstTran->data);
		if(pstPassThru == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Report Passthru dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPassThru->pstCardDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Card Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstCardDtls, pstPassThru->pstCardDtls, sizeof(CARDDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDupFieldDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getDupFieldDtlsForPymtTran(char * szTranKey, DUPFIELDDTLS_PTYPE pstDupFieldDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstDupFieldDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstDupDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Duplicate field Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstDupFieldDtls, pstPymtDtls->pstDupDtls, sizeof(DUPFIELDDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getEmvDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getEmvDtlsForPymtTran(char * szTranKey, EMVDTLS_PTYPE pstEmvDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	CARDDTLS_PTYPE	pstCardDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstEmvDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstCardDtls->bEmvData == PAAS_FALSE)
		{
			debug_sprintf(szDbgMsg, "%s: Missing EMV Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
	
			break;
		}

		pstCardDtls = (CARDDTLS_PTYPE)malloc(sizeof(CARDDTLS_STYPE));
		if(pstCardDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(pstCardDtls,0x00,sizeof(CARDDTLS_STYPE));

		memcpy(pstCardDtls, pstPymtDtls->pstCardDtls, sizeof(CARDDTLS_STYPE));

		fillEMVData(pstEmvDtls, pstCardDtls);

		free(pstCardDtls);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getEmvRespDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getEmvRespDtlsForPymtTran(char * szTranKey, EMV_RESP_DTLS_PTYPE pstEmvRespDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstEmvRespDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstEmvRespDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing EMV Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstEmvRespDtls, pstPymtDtls->pstEmvRespDtls, sizeof(EMV_RESP_DTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getEmvKeyLoadDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getEmvKeyLoadDtlsForPymtTran(char * szTranKey, EMVKEYLOAD_INFO_PTYPE pstEmvKeyLoadInfo)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstEmvKeyLoadInfo == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstEmvKeyLoadInfo == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing EMV Key Load info Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		//Copy TROUTD.
		if(pstPymtDtls->pstCTranDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Current tran info details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//This field not necessary for EMVADMIN
			//break;
		}
		else
		{
			strcpy(pstPymtDtls->pstEmvKeyLoadInfo->szTROUTD, pstPymtDtls->pstCTranDtls->szTroutd);
			//Daivik-9/8/16 - We now pickup the Reference parameter in the Current Tran Details, so copying it from current tran to EMVstructure
			strcpy(pstPymtDtls->pstEmvKeyLoadInfo->szReference, pstPymtDtls->pstCTranDtls->szReference);
		}

		
		memcpy(pstEmvKeyLoadInfo, pstPymtDtls->pstEmvKeyLoadInfo, sizeof(EMVKEYLOAD_INFO_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPINDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getPINDtlsForPymtTran(char * szTranKey, PINDTLS_PTYPE pstPINDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstPINDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstPINDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing PIN details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstPINDtls, pstPymtDtls->pstPINDtls, sizeof(PINDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSigDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getSigDtlsForPymtTran(char * szTranKey, SIGDTLS_PTYPE pstSigDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
	int				iSignLen        = 0;
#ifdef DEVDEBUG
	char			szDbgMsg[4028]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstSigDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstSigDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Signature Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/*
		 * Allocating memory for the sign data
		 * Please make sure that you free this memory
		 * after the use
		 */
		if(pstSigDtls->szSign == NULL)
		{
			if(pstPymtDtls->pstSigDtls->szSign != NULL)
			{
				iSignLen = strlen(pstPymtDtls->pstSigDtls->szSign);
				if(pstPymtDtls->pstSigDtls->szSign != NULL && (iSignLen > 0))
				{
					pstSigDtls->szSign = (char *)malloc(iSignLen + 1);
					if(pstSigDtls->szSign == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc failed for sign data", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}
					memset(pstSigDtls->szSign, 0x00, iSignLen + 1);

					/* Copying the signature data */
					memcpy(pstSigDtls->szSign, pstPymtDtls->pstSigDtls->szSign, iSignLen);
				}
			}
		}

		memcpy(pstSigDtls->szMime, pstPymtDtls->pstSigDtls->szMime, strlen(pstPymtDtls->pstSigDtls->szMime));
		memcpy(pstSigDtls->szDispTxt, pstPymtDtls->pstSigDtls->szDispTxt, strlen(pstPymtDtls->pstSigDtls->szDispTxt));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getLevel2DtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getLevel2DtlsForPymtTran(char * szTranKey, LVL2_PTYPE pstLevel2Dtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstLevel2Dtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstLevel2Dtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Tax Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstLevel2Dtls, pstPymtDtls->pstLevel2Dtls, sizeof(LVL2_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCmdTypeForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getCmdTypeForPymtTran(char * szTranKey, int * piCmd)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if(szTranKey == NULL )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Get the command from transaction instance */
		*piCmd = pstTran->iCmd;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getLtyDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getLtyDtlsForPymtTran(char * szTranKey, LTYDTLS_PTYPE pstLtyDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstLtyDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstLtyDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Loyalty Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstLtyDtls, pstPymtDtls->pstLtyDtls, sizeof(LTYDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getFollowOnDtlsForPymt
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getFollowOnDtlsForPymt(char * szTranKey, FTRANDTLS_PTYPE pstFTranDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstFTranDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstFTranDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Follow on Tran Dtls missing",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstFTranDtls, pstPymtDtls->pstFTranDtls, sizeof(FTRANDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getCurTranDtlsForPymt
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getCurTranDtlsForPymt(char * szTranKey, CTRANDTLS_PTYPE pstCTranDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstCTranDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstCTranDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing current tran dtls",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstCTranDtls, pstPymtDtls->pstCTranDtls, sizeof(CTRANDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getVSPDtlsForPymtTran
 *
 * Description	: 
 *
 * Input Params	: 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getVSPDtlsForPymtTran(char * szTranKey, VSPDTLS_PTYPE pstVSPDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstVSPDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstVSPDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing VSP details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstVSPDtls, pstPymtDtls->pstVSPDtls, sizeof(VSPDTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getVASDtlsForPymtTran
 *
 * Description	: This function helps in the getting the VAS details
* 				  from the Payment transaction
 *
 * Input Params	: transaction key, structure need to be filled with VAS details 
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getVASDtlsForPymtTran(char * szTranKey, VASDATA_DTLS_PTYPE pstVasDataDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstVasDataDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstVasDataDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing VAS Data details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstVasDataDtls, pstPymtDtls->pstVasDataDtls, sizeof(VASDATA_DTLS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: setAdminPacketRequestStatus
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
/* Praveen_P1: TO DO  Not sure where this function should be*/
int setAdminPacketRequestStatus(PAAS_BOOL bAdminPcktstatus)
{
	int rv = SUCCESS;

	if( bAdminPcktstatus == PAAS_TRUE)
	{
		adminPcktReq = 1;
	}
	else
	{
		adminPcktReq = 0;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: isAdminPacketRequestRequired
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isAdminPacketRequestRequired()
{
	PAAS_BOOL bAdminPcktReq;

	if(adminPcktReq == 1)
	{
		bAdminPcktReq = PAAS_TRUE;
	}
	else
	{
		bAdminPcktReq = PAAS_FALSE;
	}

	return bAdminPcktReq;
}

/* ============================================================================
* Function Name: getDevTranDtls
*
* Description	: This function helps in the getting the device transaction 
* 					details from the transaction key
*
* Input Params	: transaction key and the devdtls pointer
*
* Output Params: SUCCESS/ FAILURE
* ============================================================================
*/
int getDevTranDtls(char * szTranKey, DEVTRAN_PTYPE* pstDevDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is Not Present in Transaction Instance",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		*pstDevDtls = pstDevTran;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPaymentTypesDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getPaymentTypesDtlsForPymtTran(char * szTranKey, char * pszPymtTypes)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	TRAN_PTYPE		pstTran				= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pszPymtTypes == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Payment Details Not Present in the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		/* Get the Payment types sent from POS */
		strcpy(pszPymtTypes, pstPymtDtls->szPaymentTypes);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPOSTendersDtlsForPymtTran
 *
 * Description	: Function to get POS defined tender details present in the Payment transaction
 *
 * Input Params	: Transaction key, pointer to the structure of POS Tenders
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getPOSTendersDtlsForPymtTran(char * pszTranKey, POSTENDERDTLS_PTYPE pstPOSTenderDtls)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	TRAN_PTYPE		pstTran				= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Check for Parameters */
		if((pszTranKey == NULL) || (pstPOSTenderDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(pszTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Payment Details Not Present in the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstPOSTenderDtls != NULL)
		{
			memcpy(pstPOSTenderDtls, pstPymtDtls->pstPOSTenderDtls, sizeof(POSTENDERDTLS_STYPE));
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getVHQAplyUpdtDtlsForDevTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getVHQAplyUpdtDtlsForDevTran(char * szTranKey, APPLYUPDATES_PTYPE pstApplyUpdatesDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstApplyUpdatesDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstApplyUpdatesDtls, &(pstDevTran->stApplyUpdatesDtls), sizeof(APPLYUPDATES_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
							__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getEarlyRtrnCrdFlagForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getEarlyRtrnCrdFlagForPymtTran(char * szTranKey, PAAS_BOOL *bEarlyRtrnCardFlag)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	TRAN_PTYPE		pstTran				= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (bEarlyRtrnCardFlag == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Payment Details Not Present in the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}

			rv = FAILURE;
			break;
		}

		/* Get the payment type */
		*bEarlyRtrnCardFlag = pstPymtDtls->bEarlyRtrnCardFlag;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getEarlyCrdPrsntFlagFromSession
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getEarlyCrdPrsntFlagFromSession(char * szSessKey, int *iEarlyCardDtlsStatus)
{
	int				rv				= SUCCESS;
	SESSDTLS_PTYPE	pstSess			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szSessKey == NULL) || (iEarlyCardDtlsStatus == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getSession(szSessKey, &pstSess);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to session",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Session details are not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			*iEarlyCardDtlsStatus = DATA_EMPTY;
			break;
		}

		*iEarlyCardDtlsStatus = pstSess->iEarlyCardDtlsStatus;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getRawCardDtls
 *
 * Description	:
 *
 * Input Params	:bFillAll will decide whether to fill all details or only card entry mode
 *
 * Output Params:
 * ============================================================================
 */
int getRawCardDtls(RAW_CARDDTLS_PTYPE *dpstRawCrdDtls, CARDDTLS_PTYPE pstCardDtls, PAAS_BOOL bFillAll)
{
	int			rv							= SUCCESS;
	char 		szBIN[6+1]					= "";
	char		szClearPAN[30]				= "";
	char		szMaskedPAN[30]				= "";
	char		szClearTrack1[100+1]		= "";
	char		szClearTrack2[100+1]		= "";
	char		szClearTrack3[100+1]		= "";
	ENC_TYPE	iEncType					= getEncryptionType();

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/* Intializations */
	memset(szBIN, 0x00, sizeof(szBIN));
	memset(szClearPAN, 0x00, sizeof(szClearPAN));
	memset(szMaskedPAN, 0x00, sizeof(szMaskedPAN));
	memset(szClearTrack1, 0x00, sizeof(szClearTrack1));
	memset(szClearTrack2, 0x00, sizeof(szClearTrack2));
	memset(szClearTrack3, 0x00, sizeof(szClearTrack3));

	while(1)
	{
		if(*dpstRawCrdDtls == NULL)
		{
			*dpstRawCrdDtls = (RAW_CARDDTLS_PTYPE) malloc(sizeof(RAW_CARDDTLS_STYPE));
			if(*dpstRawCrdDtls ==  NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
		}
		memset(*dpstRawCrdDtls, 0x00, sizeof(RAW_CARDDTLS_STYPE));

		if(pstCardDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL card dtls passed, Not filling Card entry Mode or any Other Parameters", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = SUCCESS;
			return rv;
		}

		if(bFillAll)
		{

			strncpy(szBIN, pstCardDtls->szPAN, 6);

			if(checkBinInclusion(szBIN))
			{
				debug_sprintf(szDbgMsg, "%s: Card is Included in PCI Card Range", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//Masking the PAN data
				getMaskedPAN(pstCardDtls->szPAN, szMaskedPAN);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Card is NOT Included in PCI Card Range", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iEncType == VSP_ENC)
				{
					// copy the encrypted PAN first, VCL needs it when clear PAN is request for manual entry transaction.
					strcpy(szClearPAN, pstCardDtls->szPAN);
					/* Getting the clear PAN data from VCL */
					rv = getDataFromVCL(szClearPAN, szClearTrack1, szClearTrack2, szClearTrack3, pstCardDtls->bManEntry);
					if(rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to get data from VCL", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;;
					}
				}
				else if(iEncType == VSD_ENC)
				{
					if(strchr(pstCardDtls->szClrTrkData, '=') != NULL)		// fill in TRACK2 field
					{
						strcpy(szClearTrack2, pstCardDtls->szClrTrkData);	// fill in TRACK1 field
					}
					else if(strchr(pstCardDtls->szClrTrkData, '^') != NULL)
					{
						strcpy(szClearTrack1, pstCardDtls->szClrTrkData);
					}
					else
					{
						//Daivik:21/7/2016 - The above conditions must satisfy , but in case they dont(like giftcards) , then use the track no. that would be populated.
						if(pstCardDtls->iTrkNo == TRACK2_INDICATOR)
						{
							strcpy(szClearTrack2, pstCardDtls->szClrTrkData);
						}
						else if(pstCardDtls->iTrkNo == TRACK1_INDICATOR)
						{
							strcpy(szClearTrack1, pstCardDtls->szClrTrkData);
						}
					}
					strcpy(szClearPAN, pstCardDtls->szClrPAN);
				}
				else
				{
					// will send PAN data which was parsed in S20/C30 response. it could be randomized or clear PAN according to BIN range configuration
					strcpy(szClearPAN, pstCardDtls->szPAN);
				}
			}

			if(strlen(szMaskedPAN))		// copy masked PAN if available
			{
				strcpy((*dpstRawCrdDtls)->szPAN,szMaskedPAN);
			}
			else if(strlen(szClearPAN))
			{
				strcpy((*dpstRawCrdDtls)->szPAN,szClearPAN);
			}
			if(strlen(szClearTrack1))	// copy track1 data if available
			{
				strcpy((*dpstRawCrdDtls)->szTrack1Data,szClearTrack1);
			}
			if(strlen(szClearTrack2))	// copy track2 data if available
			{
				strcpy((*dpstRawCrdDtls)->szTrack2Data,szClearTrack2);
			}
			if(strlen(szClearTrack3))	// copy track3 data if available
			{
				strcpy((*dpstRawCrdDtls)->szTrack3Data,szClearTrack3);
			}
			if(strlen(pstCardDtls->szClrMon))	// copy clear expiry month
			{
				strcpy((*dpstRawCrdDtls)->szClrMon,pstCardDtls->szClrMon);
			}
			if(strlen(pstCardDtls->szClrYear))	// copy clear expiry year
			{
				strcpy((*dpstRawCrdDtls)->szClrYear,pstCardDtls->szClrYear);
			}
			if(strlen(pstCardDtls->szName))		// copy cardholder name
			{
				strcpy((*dpstRawCrdDtls)->szName,pstCardDtls->szName);
			}
			if(strlen(pstCardDtls->szPymtMedia))		// copy payment media
			{
				strcpy((*dpstRawCrdDtls)->szPymtMedia,pstCardDtls->szPymtMedia);

				// copy payment type only if payment media is present
				switch(pstCardDtls->iCardType)
				{
				case CREDIT_CARD_TYPE:
					strcpy((*dpstRawCrdDtls)->szPymtType, "CREDIT");
					break;
				case DEBIT_CARD_TYPE:
					strcpy((*dpstRawCrdDtls)->szPymtType, "DEBIT");
					break;
				case GIFT_CARD_TYPE:
					strcpy((*dpstRawCrdDtls)->szPymtType, "GIFT");
					break;
				case FSA_CARD_TYPE:
					strcpy((*dpstRawCrdDtls)->szPymtType, "FSA");
					break;
				case EBT_CARD_TYPE:
					strcpy((*dpstRawCrdDtls)->szPymtType, "EBT");
					break;
				case PL_CARD_TYPE:
					strcpy((*dpstRawCrdDtls)->szPymtType, "PRIV_LBL");
					break;
				case MERCH_CREDIT_CARD_TYPE:
					strcpy((*dpstRawCrdDtls)->szPymtType, "MERCH_CREDIT");
					break;
					//case LOYALTY_CARD_TYPE:
				case PURCHASE_CARD_TYPE:
				case COMMERCIAL_CARD_TYPE:
				case BUSINESS_CARD_TYPE:
				case PAYACCOUNT_CARD_TYPE:
				case POS_TENDER_TYPE:
					break;
				default:
					debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
			}
		}
		if(pstCardDtls->bManEntry == PAAS_TRUE)
		{
			strcpy((*dpstRawCrdDtls)->szCardEntryMode, "MANUAL");
		}
		else
		{
			switch (pstCardDtls->iCardSrc)
			{
			case CRD_MSR:
			case CRD_EMV_FALLBACK_MSR:
				strcpy((*dpstRawCrdDtls)->szCardEntryMode, "SWIPED");
				break;


			case CRD_NFC:
			case CRD_RFID:
			case CRD_EMV_CTLS:
			case CRD_EMV_MSD_CTLS:
				strcpy((*dpstRawCrdDtls)->szCardEntryMode, "TAPPED");
				break;

			case CRD_EMV_CT:
				strcpy((*dpstRawCrdDtls)->szCardEntryMode, "INSERTED");
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Card source not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		break;
	}	// break from while loop
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}
/*
 * ============================================================================
 * Function Name: getPassThrghDtlsForPymtTran
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getPassThrghDtlsForPymtTran(char * szTranKey, PASSTHRG_FIELDS_PTYPE pstPassThrghDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for Parameters */
		if((szTranKey == NULL) || (pstPassThrghDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the SSI transcation from the stack */
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstPymtDtls->pstPassThrghDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Pass through Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memcpy(pstPassThrghDtls, pstPymtDtls->pstPassThrghDtls, sizeof(PASSTHRG_FIELDS_STYPE));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: getDispQRCodeDtlsForDevTran
 *
 * Description	:	Get the reference to QR Code details from dev transaction structure
 *
 * Input Params	:	Transaction key, place holder for QR code details
 *
 * Output Params: SUCCESS/ FAILURE
 * ============================================================================
 */
int getDispQRCodeDtlsForDevTran(char * szTranKey, DISPQRCODE_DTLS_PTYPE pstDispQRCodeDtls)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstDispQRCodeDtls == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstDispQRCodeDtls, &(pstDevTran->stDispQRCodeDtls), sizeof(DISPQRCODE_DTLS_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/* ============================================================================
* Function Name: getCustCheckBoxDtlsForDevTran
*
* Description	:The Function gets the Values from Customer Checkbox Structure
* 				of Device Transaction.
*
* Input Params	:
*
* Output Params: SUCCESS/ FAILURE
* ============================================================================
*/
int getCustCheckBoxDtlsForDevTran(char * szTranKey, CUSTCHECKBOX_DTLS_PTYPE pstCustCheckBox)
{
	int				rv				= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	DEVTRAN_PTYPE	pstDevTran		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((szTranKey == NULL) || (pstCustCheckBox == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the transaction reference */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get reference to tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->data != NULL)
		{
			pstDevTran = (DEVTRAN_PTYPE) pstTran->data;
			memcpy(pstCustCheckBox, &(pstDevTran->stCustCheckBoxDtls), sizeof(CUSTCHECKBOX_DTLS_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Data is not present currently",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}


		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * End of file tranDS1.c
 * ============================================================================
 */
