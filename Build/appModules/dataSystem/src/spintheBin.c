/******************************************************************
*                      spintheBin.c                               *
*******************************************************************
* Application: Point Solutions                                    *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis corresponds to Spin The Bin      *
* 			   BIN File Management logic                          *
*                                                                 *
* Created on: Dec 26, 2014                                        *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                 VFI                                             *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <syslog.h>
#include <svc.h>

#include "common/common.h"
#include "db/cardDataTable.h"
#include "db/cdtExtern.h"
#include "common/utils.h"
#include "bLogic/bLogicCfgDef.h"
#include "appLog/appLogAPIs.h"

#define STB_FILE_NAME "./flash/stb.txt"

#define HASHSIZE 101

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

typedef struct __stHash /* table entry: */
{
	struct __stHash * next;     		/* Next entry in chain */
    char 		    *pszSetName;       	/* Defined Tag Name */
    char  			szDecisionVal[5];    	/* Index Value of the Tag */
}
HASHLST_STYPE, * HASHLST_PTYPE;

static HASHLST_PTYPE hashtab[HASHSIZE];  // pointer table

static int 				loadSTBDtls();
static int 				parseSTBLine(char *, STBFILERECDTLS_PTYPE );
static int 				addFieldstoHashTable(STBFILERECDTLS_STYPE);
static unsigned int 	generateHashVal(char *);
static HASHLST_PTYPE 	lookupHashTable(char *);
static HASHLST_PTYPE 	addEntryToHashTable(char *, char *);
static int 				getPinPromptType(char *, char *, int *);
static char 			getPINLessInd(CARDDTLS_PTYPE , int );

/*
 * ============================================================================
 * Function Name: initSTB
 *
 * Description	: This API is responsible for initializing the STB after
 * 					reading from the STB file
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int initSTB()
{
	int			rv					= SUCCESS;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
	char		szAppLogDiag[300]	= "";
	PAAS_BOOL 	bSTBEnaled			= PAAS_FALSE;
	int			iLen				= 0;
	char		szTemp[10]			= "";
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iLen = sizeof(szTemp);

	while(1)
	{
		/* This will be loaded in Business Logic
		 * datasystem is initialized before BL, thats why
		 * checking the config param here
		 */
		rv = getEnvFile("STB", "stblogicenabled", szTemp, iLen);
		if(rv > 0)
		{
			if((*szTemp == 'Y') || (*szTemp == 'y'))
			{
				bSTBEnaled = PAAS_TRUE;
			}
			else
			{
				bSTBEnaled = PAAS_FALSE;
			}
		}

		rv = SUCCESS;

		/* check if STB is enabled */
		if(!bSTBEnaled)
		{
			debug_sprintf(szDbgMsg, "%s: STB Logic is not enabled, Nothing to do here", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		if(doesFileExist(STB_FILE_NAME) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: STB Decision file [%s]is not present", __FUNCTION__, STB_FILE_NAME);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "STB Decision File [%s] is Not Present", STB_FILE_NAME);
				sprintf(szAppLogDiag, "Please download the file %s", STB_FILE_NAME);
				addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, szAppLogDiag);
			}

			break;
		}

		debug_sprintf(szDbgMsg, "%s: STB file is present, parsing", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = loadSTBDtls();
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to load STB Decision file [%s]", __FUNCTION__, STB_FILE_NAME);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed to load STB Decision file [%s]", STB_FILE_NAME);
				sprintf(szAppLogDiag, "Please Check the Format of the file %s, Otherwise Contact Verifone", STB_FILE_NAME);
				addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, szAppLogDiag);
			}

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Successfully loaded STB details from file [%s]", __FUNCTION__, STB_FILE_NAME);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: determineCardType
 *
 * Description	: This API determines the card type based on the STB records
 * 				  for this card
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int determineCardType(CARDDTLS_PTYPE pstCardDtls, char * pszTranAmt)
{
	int				rv					= SUCCESS;
	int				iRecordIndex		= -1;
	int				iPINPromptType		= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			cCardProd;
	char			szLookup[4]			= "";
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";
	double			fCurTranAmount		= 0.00;
	double			fSAFTranLimit		= 0.00;
	HASHLST_PTYPE 	pstHashLst			= NULL;


#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		/* Storing the payment media from the STB */
		if(pstCardDtls->stBinDtls[B_TYPE_REC_INDEX].iTypePresent)
		{
			cCardProd = pstCardDtls->stBinDtls[B_TYPE_REC_INDEX].cProd;
			iRecordIndex = B_TYPE_REC_INDEX;
		}
		else if(pstCardDtls->stBinDtls[C_TYPE_REC_INDEX].iTypePresent)
		{
			cCardProd = pstCardDtls->stBinDtls[C_TYPE_REC_INDEX].cProd;
			iRecordIndex = C_TYPE_REC_INDEX;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Either B type or C type not found for this card", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_NOT_IN_BINRANGE;

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid Card, No File Type Found for this PAN");
				strcpy(szAppLogDiag, "Please Add the Record to the BIN file if this is a Valid Card");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}

			//TODO set the corresponding return value here
			break;
		}

		switch(cCardProd)
		{
		case 'V':
			strcpy(pstCardDtls->szPymtMedia, "VISA");
			break;
		case 'M':
			strcpy(pstCardDtls->szPymtMedia, "MASTERCARD");
			break;
		case 'D':
			strcpy(pstCardDtls->szPymtMedia, "DISCOVER");
			break;
		case 'A':
			strcpy(pstCardDtls->szPymtMedia, "AMEX");
			break;
		case 'N':
			strcpy(pstCardDtls->szPymtMedia, "DEBIT");
			break;
		case 'G':
			strcpy(pstCardDtls->szPymtMedia, "GIFT");
			break;
		default:
			debug_sprintf(szDbgMsg, "%s: Invalid Card prod, should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Payment Media [%s]", __FUNCTION__, pstCardDtls->szPymtMedia);
		APP_TRACE(szDbgMsg);

		pstCardDtls->iPINReqd = -1; //Filling with the default value

		if(cCardProd == 'G')
		{
			debug_sprintf(szDbgMsg, "%s: Its Arch card type, no need to check further details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstCardDtls->iCardType = GIFT_CARD_TYPE;

			pstCardDtls->iPINReqd = 0; //Pin is not required

			pstCardDtls->bSignReqd = PAAS_FALSE; //Currently for gift cards signature is not allowing

			pstCardDtls->bSAFAllwd = PAAS_FALSE; //currently not allowing SAF for Gift cards

			//TODO Assign remaining things before breaking from here
			break;
		}

		/*
		 * the look up contains <Card Prod><Detail IND><PINLess-IND>
		 * We will use this as the key to look up the hash table to
		 * get the decision value
		 */
		memset(szLookup, 0x00, sizeof(szLookup));

		if(pstCardDtls->stBinDtls[iRecordIndex].cProd != 0x00)
		{
			szLookup[0] = pstCardDtls->stBinDtls[iRecordIndex].cProd;
		}
		else
		{
			szLookup[0] = '*'; //While creating the hastable, if that value is not present we are keeping *
		}

		if(pstCardDtls->stBinDtls[iRecordIndex].cCardInd != 0x00)
		{
			szLookup[1] = pstCardDtls->stBinDtls[iRecordIndex].cCardInd;
		}
		else
		{
			szLookup[1] = '*'; //While creating the hastable, if that value is not present we are keeping *
		}

		szLookup[2] = getPINLessInd(pstCardDtls, iRecordIndex);

		debug_sprintf(szDbgMsg, "%s: Lookup Key Name [%s]", __FUNCTION__, szLookup);
		APP_TRACE(szDbgMsg);

		/* Do the Hash look up for this key Name */
		pstHashLst = lookupHashTable(szLookup);
		if(pstHashLst == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Did not find entry for this Tag[%s] in the Hash Table!!!", __FUNCTION__, szLookup);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Fetch Decision Rule for the Card.");
				strcpy(szAppLogDiag, "Please Check Decision File(stb.txt), or Contact Verifone");
				addAppEventLog(SCA, PAAS_ERROR, CAPTURE_DETAILS, szAppLogData, szAppLogDiag);
			}

			rv = ERR_CARD_INVALID; //Sending invalid card since we did not find the decision rule for this card
			//rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Decision Value [%s]", __FUNCTION__, pstHashLst->szDecisionVal);
		APP_TRACE(szDbgMsg);

		if(pszTranAmt != NULL)
		{
			fCurTranAmount = atof(pszTranAmt);
		}

		if(fCurTranAmount <= 0)
		{
			if(pstCardDtls->stBinDtls[iRecordIndex].cCardInd == 'E')
			{
				pstCardDtls->iCardType = EBT_CARD_TYPE;
				pstCardDtls->bSignReqd = PAAS_FALSE; //Not allowing signature to capture for ebt card types
			}

			debug_sprintf(szDbgMsg, "%s: Transaction amount is not present, must be pre-swipe case not determining the prompt type now", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		rv = getPinPromptType(pstHashLst->szDecisionVal, pszTranAmt, &iPINPromptType);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while getting the PIN Prompt type", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: The Prompt Type %d", __FUNCTION__, iPINPromptType);
		APP_TRACE(szDbgMsg);

		if(pstCardDtls->bManEntry) //Its Manual transaction
		{
			/*
			 * For Manual transactions, for optional pin capable transactions we
			 * we will set no pin required and credit
			 * if it is pure debit, we should not allow the transaction
			 * since Manual debit is not allowed
			 */
			if(pstCardDtls->stBinDtls[iRecordIndex].cCardInd == 'E')
			{
				pstCardDtls->iCardType = EBT_CARD_TYPE;
				pstCardDtls->bSignReqd = PAAS_FALSE; //Not allowing signature to capture for ebt card types
			}

			if((iPINPromptType == 3) && (pstCardDtls->iCardType != EBT_CARD_TYPE))
			{
				debug_sprintf(szDbgMsg, "%s: Manual entry is not allowed for this card since Pin is required", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_MANUAL_NOTALLWD;
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Its Manual transaction and card determined as optional pin, changing it to no pin", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iPINPromptType = 0; //no pin prompting
			}
		}

		pstCardDtls->iPINReqd = iPINPromptType;

		switch(iPINPromptType)
		{
		case 0: //No PIN Prompting
			pstCardDtls->iCardType = CREDIT_CARD_TYPE;
			pstCardDtls->bSignReqd = PAAS_TRUE; //Allowing signature to capture for credit card types
			break;
		case 1: //Prompt for Optional PIN for CREDIT
		case 2: //Prompt for Optional PIN for DEBIT
		case 3: //Prompt for PIN or Cancel
			pstCardDtls->iCardType = DEBIT_CARD_TYPE;
			pstCardDtls->bSignReqd = PAAS_FALSE; //Not allowing signature to capture for debit card types
			break;
		default:
			break;
		}

		/*
		 * FSA bins will be identified with an �F� in the BIN-DETL-FSA-IND field
		 * EBT bins can be identified in the compressed bin file using the card indicator field,
		 * which is also in the global bin file.  A value of �E� indicates EBT
		 */
		if(pstCardDtls->stBinDtls[iRecordIndex].cCardInd == 'E')
		{
			pstCardDtls->iCardType = EBT_CARD_TYPE;
			pstCardDtls->bSignReqd = PAAS_FALSE; //Not allowing signature to capture for ebt card types
		}

		if(pstCardDtls->stBinDtls[iRecordIndex].cFSAInd == 'F')
		{
			pstCardDtls->iCardType = FSA_CARD_TYPE;
		}

		/* Check if SAF is allowed for this card */

		if(pstCardDtls->stBinDtls[D_TYPE_REC_INDEX].iTypePresent)
		{
			debug_sprintf(szDbgMsg, "%s: D type is present for this card, so SAF is not allowed for this", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstCardDtls->bSAFAllwd = PAAS_FALSE;
		}
		else
		{
			if(isSAFEnabled())
			{
				fSAFTranLimit = getSAFTranFloorAmount();

				debug_sprintf(szDbgMsg, "%s: SAF floor limit [%lf], Transaction Amount [%lf]", __FUNCTION__, fSAFTranLimit, fCurTranAmount);
				APP_TRACE(szDbgMsg);

				if(fCurTranAmount < fSAFTranLimit)
				{
					pstCardDtls->bSAFAllwd = PAAS_TRUE;

					debug_sprintf(szDbgMsg, "%s: SAF Tran floor limit allows SAF for this transaction", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					pstCardDtls->bSAFAllwd = PAAS_FALSE;

					debug_sprintf(szDbgMsg, "%s: SAF Tran floor limit doesnt allow SAF for this tran", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: SAF is disabled, no SAF for this tran", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				pstCardDtls->bSAFAllwd = PAAS_FALSE;
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPINLessInd
 *
 * Description	: This API determines the PIN Less Indicator from the set of
 * 				  indicators for this card
 *
 * Input Params	: Card Details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static char getPINLessInd(CARDDTLS_PTYPE pstCardDtls, int iRecordIndex)
{
	char 	cPINLessInd 	= '*';
	int		iNetworkIndex 	= 0;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * For each card: Entries db_nw_id and pinless_ind recur up to 20 times
	 * we need to pick up PIN Less Indicator from this set
	 * First Preference would be PINLESS which are A,C,D,E (these dont require PIN)
	 * if the current set contains any of these indicators(A,C,D,E) then we have
	 * to return with one of them
	 * Otherwise we have to return from P,L,B set
	 */

	for(iNetworkIndex = 0; iNetworkIndex < 20; iNetworkIndex++) //Maximum 20 network indicators would be present
	{
		if(pstCardDtls->stBinDtls[iRecordIndex].stNewtDtls[iNetworkIndex].cPinlessInd != 0x00)
		{
			if(pstCardDtls->stBinDtls[iRecordIndex].stNewtDtls[iNetworkIndex].cPinlessInd == 'A' ||
			   pstCardDtls->stBinDtls[iRecordIndex].stNewtDtls[iNetworkIndex].cPinlessInd == 'C' ||
			   pstCardDtls->stBinDtls[iRecordIndex].stNewtDtls[iNetworkIndex].cPinlessInd == 'D' ||
			   pstCardDtls->stBinDtls[iRecordIndex].stNewtDtls[iNetworkIndex].cPinlessInd == 'E')
			{
				debug_sprintf(szDbgMsg, "%s: It is one of the A/C/D/E Indicators", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				cPINLessInd = pstCardDtls->stBinDtls[iRecordIndex].stNewtDtls[iNetworkIndex].cPinlessInd;
				break;
			}
		}
		else//no more indicators
		{
			break;
		}
		cPINLessInd = pstCardDtls->stBinDtls[iRecordIndex].stNewtDtls[iNetworkIndex].cPinlessInd;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%c]", __FUNCTION__, cPINLessInd);
	APP_TRACE(szDbgMsg);

	return cPINLessInd;
}

/*
 * ============================================================================
 * Function Name: getPinPromptType
 *
 * Description	: This API determines the PIN Prompt type based on the limits
 * 				  and the decision rules
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getPinPromptType(char *pszDecisionRule, char *pszTranAmt, int *pszPinPromptType)
{
	int				rv						= SUCCESS;
	static int		iGetDtlsOnce			= 0;
	static double	fPINLimit				= 0;
	static double	fSigLimit				= 0;
	static double	fInterchangeMgmtLimit	= 0;
	double			fCurTranAmount			= 0;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(iGetDtlsOnce == 0)
	{
		fPINLimit = getSTBPINLimit();
		fSigLimit = getSTBSignatureLimit();
		fInterchangeMgmtLimit = getSTBInterchangeMgmtLimit();
		iGetDtlsOnce = 1;
	}

	debug_sprintf(szDbgMsg, "%s: PIN Limit [%lf], Signature Limit[%lf], Interchange Management Limit[%lf]", __FUNCTION__, fPINLimit, fSigLimit, fInterchangeMgmtLimit);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pszDecisionRule == NULL || pszTranAmt == NULL || pszPinPromptType == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input param is NULL!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;		}

		fCurTranAmount = atof(pszTranAmt);

		if(fCurTranAmount > fInterchangeMgmtLimit)
		{
			/* Do whatever Amt > Interchange_Mgmt_Limit says */
			*pszPinPromptType = pszDecisionRule[3] - '0';
		}
		else
		{
			if(fCurTranAmount > fPINLimit)
			{
				/* Do whatever Amt >PIN Limit says */
				*pszPinPromptType = pszDecisionRule[2] - '0';
			}
			else
			{
				if(fCurTranAmount > fSigLimit)
				{
					/* Do whatever Amt>Sig Limit says */
					*pszPinPromptType = pszDecisionRule[1] - '0';
				}
				else
				{
					/* do Amt <= Sig Limit says */
					*pszPinPromptType = pszDecisionRule[0] - '0';
				}
			}
		}

		//CID 67230 (#1 of 1): Dereference after null check (FORWARD_NULL) T_RaghavendranR1
		debug_sprintf(szDbgMsg, "%s: Pin Prompt Type [%d]", __FUNCTION__, *pszPinPromptType);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
	return rv;
}

/*
 * ============================================================================
 * Function Name: loadSTBDtls
 *
 * Description	: This API loads the STB decision details from the corresponding
 * 				  STB file
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int loadSTBDtls()
{
	int						rv					= SUCCESS;
	int						iAppLogEnabled		= isAppLogEnabled();
	char					szAppLogData[300]	= "";
	char					szAppLogDiag[300]	= "";
	char					szLine[1024]		= ""; //TO_DO Make sure that each line does not contain more than 1024 bytes
	FILE *					fp					= NULL;
	STBFILERECDTLS_STYPE 	stSTBFileRecDtls;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		/* Open the file */
		fp = fopen(STB_FILE_NAME, "r");
		if(fp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to open [%s]", __FUNCTION__,STB_FILE_NAME);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error While Opening the STB Decision File");
				sprintf(szAppLogDiag, "Please Download the STB Decision File [%s]", STB_FILE_NAME);
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
			}
			rv = FAILURE;
			break;
		}

		/* File opened succcessfully, start populating the STB records with the
		 * values provided in the file */
		while(!feof(fp))
		{
			memset(szLine, 0x00, sizeof(szLine));
			if(fgets(szLine, sizeof(szLine), fp) != NULL)
			{
				debug_sprintf(szDbgMsg,"%s: Line = [%s]",__FUNCTION__,szLine);
				APP_TRACE(szDbgMsg);

				memset(&stSTBFileRecDtls, 0x00, sizeof(STBFILERECDTLS_STYPE));

				/* Parse the line for storing the particular Decision set  */
				rv = parseSTBLine(szLine, &stSTBFileRecDtls);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to parse STB line", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				/* Need to add combination of sets to hash table */
				rv = addFieldstoHashTable(stSTBFileRecDtls);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to add Fields to Hash Table", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}
			else if(ferror(fp))
			{
				debug_sprintf(szDbgMsg, "%s: Error while reading from [%s]", __FUNCTION__, STB_FILE_NAME);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error While Reading the STB Decision File");
					sprintf(szAppLogDiag, "Please Check the STB Decision File [%s]", STB_FILE_NAME);
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
				}

				rv = FAILURE;
				break;
			}
		}

		if(fp != NULL)
		{
			fclose(fp);
		}

		break;

	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseSTBLine
 *
 * Description	: This API parses the given line from the
 * 				  STB file
 *
 * Input Params	: Input Line
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int parseSTBLine(char *pszInput, STBFILERECDTLS_PTYPE pstSTBFileRecDtls)
{
	int		rv					= SUCCESS;
	char *	pszCurPtr			= NULL;
	char *  pszNextPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszInput == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Input to Parse", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return rv;
	}

	/*
	 * Format: <CARD_PROD_SET>|<DETAIL_IND_SET>|<PIN_LESS_IND>|<DECISION_VALUE_SET>
	 * Ex: MVDA|HUVKN|PBL|000001
	 */

	pszCurPtr = pszInput;

	/* Parsing for CARD_PROD_SET */
	pszNextPtr = strchr(pszCurPtr, '|');
	if(pszNextPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Format Error", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(pstSTBFileRecDtls->szProdSet, 0x00, sizeof(pstSTBFileRecDtls->szProdSet));
	strncpy(pstSTBFileRecDtls->szProdSet, pszCurPtr, pszNextPtr - pszCurPtr);

	debug_sprintf(szDbgMsg, "%s: Prod Set [%s]", __FUNCTION__, pstSTBFileRecDtls->szProdSet);
	APP_TRACE(szDbgMsg);

	pszCurPtr = pszNextPtr + 1;

	/* Parsing for DETAIL_INDICATOR_SET */
	pszNextPtr = strchr(pszCurPtr, '|');
	if(pszNextPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Format Error", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(pstSTBFileRecDtls->szDetailIndSet, 0x00, sizeof(pstSTBFileRecDtls->szDetailIndSet));
	strncpy(pstSTBFileRecDtls->szDetailIndSet, pszCurPtr, pszNextPtr - pszCurPtr);

	debug_sprintf(szDbgMsg, "%s: Detail Indicator Set [%s]", __FUNCTION__, pstSTBFileRecDtls->szDetailIndSet);
	APP_TRACE(szDbgMsg);

	pszCurPtr = pszNextPtr + 1;

	/* Parsing for PIN_LESS_IND SET */
	pszNextPtr = strchr(pszCurPtr, '|');
	if(pszNextPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Format Error", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(pstSTBFileRecDtls->szPinLessIndSet, 0x00, sizeof(pstSTBFileRecDtls->szPinLessIndSet));
	strncpy(pstSTBFileRecDtls->szPinLessIndSet, pszCurPtr, pszNextPtr - pszCurPtr);

	debug_sprintf(szDbgMsg, "%s: Pinless Indicator Set [%s]", __FUNCTION__, pstSTBFileRecDtls->szPinLessIndSet);
	APP_TRACE(szDbgMsg);

	pszCurPtr = pszNextPtr + 1;

	/* Parsing for DECISION SET */
	memset(pstSTBFileRecDtls->szDecisionSet, 0x00, sizeof(pstSTBFileRecDtls->szDecisionSet));
	strncpy(pstSTBFileRecDtls->szDecisionSet, pszCurPtr, (strlen(pszCurPtr) -1));

	debug_sprintf(szDbgMsg, "%s: Decision Set [%s]", __FUNCTION__, pstSTBFileRecDtls->szDecisionSet);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addFieldstoHashTable
 *
 * Description	: This API frames the combination of PROD VALUE, DETL IND and PINLESS
 * 				   Ind from corresponding sets and add it to the hash table
 * 				   with the decision value
 *
 *
 * Input Params	: STB Line Details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int addFieldstoHashTable(STBFILERECDTLS_STYPE 	stSTBFileRecDtls)
{
	int				rv					= SUCCESS;
	int				iProdSetIndex		= 0;
	int				iDtlIndSetIndex		= 0;
	int				iPinlessIndIndex	= 0;
	char			szKey[4]			= "";
	HASHLST_PTYPE 	pstHashLst			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	/*
	 * If Set is empty we are putting default value as * so that
	 * inserting will be uniform
	 */
	if(strlen(stSTBFileRecDtls.szProdSet) == 0)
	{
		memset(stSTBFileRecDtls.szProdSet, 0x00, sizeof(stSTBFileRecDtls.szProdSet));
		strcpy(stSTBFileRecDtls.szProdSet, "*");
	}

	if(strlen(stSTBFileRecDtls.szDetailIndSet) == 0)
	{
		memset(stSTBFileRecDtls.szDetailIndSet, 0x00, sizeof(stSTBFileRecDtls.szDetailIndSet));
		strcpy(stSTBFileRecDtls.szDetailIndSet, "*");
	}

	if(strlen(stSTBFileRecDtls.szPinLessIndSet) == 0)
	{
		memset(stSTBFileRecDtls.szPinLessIndSet, 0x00, sizeof(stSTBFileRecDtls.szPinLessIndSet));
		strcpy(stSTBFileRecDtls.szPinLessIndSet, "*");
	}

	for(iProdSetIndex = 0; iProdSetIndex < strlen(stSTBFileRecDtls.szProdSet); iProdSetIndex++)
	{
		for(iDtlIndSetIndex = 0; iDtlIndSetIndex < strlen(stSTBFileRecDtls.szDetailIndSet); iDtlIndSetIndex++)
		{
			for(iPinlessIndIndex = 0; iPinlessIndIndex < strlen(stSTBFileRecDtls.szPinLessIndSet); iPinlessIndIndex++)
			{
				memset(szKey, 0x00, sizeof(szKey));
				sprintf(szKey, "%c%c%c", stSTBFileRecDtls.szProdSet[iProdSetIndex], stSTBFileRecDtls.szDetailIndSet[iDtlIndSetIndex], stSTBFileRecDtls.szPinLessIndSet[iPinlessIndIndex]);

				pstHashLst = addEntryToHashTable(szKey, stSTBFileRecDtls.szDecisionSet);
				if(pstHashLst != NULL)
				{
					//debug_sprintf(szDbgMsg,"%s: Added [%s] key with [%s] Value to the Hash Table",__FUNCTION__, pstHashLst->pszSetName, pstHashLst->szDecisionVal);
					//APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg,"%s: Error while adding to the Hash Table!!!",__FUNCTION__);
					APP_TRACE(szDbgMsg);
#if 0
					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "Error While Adding [%s] key [%d] Value to the Hash Table", pstHashLst->pszSetName, pstHashLst->szDecisionVal);
						strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
						addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
					}
#endif
					//TO_DO: What to do here!!!
				}
				pstHashLst = NULL; //Derefence it before assigning
			}
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: generateHashVal
 *
 * Description	: This function is used to calculate the hash value for the given
 * 				  input string
 * 				  This adds each character value in the string to a scrambled
 * 				  combination of the previous ones and returns the remainder
 * 				  modulo the array size. This is not the best possible hash function,
 * 				  but it is short and effective
 *
 * Input Params	: Input String to Calculate Hash
 *
 * Output Params: Hash Value
 * ============================================================================
 */
static unsigned int generateHashVal(char *pszInput)
{
    unsigned int iHashVal = -1;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	if(pszInput == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Input to calculate hash!!! ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iHashVal;
	}

    for (iHashVal = 0; *pszInput != '\0'; pszInput++)
    {
    	iHashVal = *pszInput + (31 * iHashVal);
    }

    //debug_sprintf(szDbgMsg, "%s: iHashVal [%d] ", __FUNCTION__, iHashVal);
    //APP_TRACE(szDbgMsg);

    return iHashVal % HASHSIZE;
}

/*
 * ============================================================================
 * Function Name: lookupHashTable
 *
 * Description	: This function is used to lookup hash table for the given
 * 				  input string
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The search is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
static HASHLST_PTYPE lookupHashTable(char *pszInPut)
{
	HASHLST_PTYPE pstHashLst;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

    for (pstHashLst = hashtab[generateHashVal(pszInPut)]; pstHashLst != NULL; pstHashLst = pstHashLst->next)
    {
        if (strcmp(pszInPut, pstHashLst->pszSetName) == 0)
        {
        	debug_sprintf(szDbgMsg, "%s: Found Mapping for %s in hash table", __FUNCTION__, pszInPut);
        	APP_TRACE(szDbgMsg);

            return pstHashLst;     // Found
        }
    }

    //debug_sprintf(szDbgMsg, "%s: Did not find Mapping for %s in hash table", __FUNCTION__, pszInPut);
    //APP_TRACE(szDbgMsg);

    return NULL;           // NOT found
}

/*
 * ============================================================================
 * Function Name: addEntryToHashTable
 *
 * Description	: This function is used to add entry to the hash table
 *
 * 				  The hashing process produces a starting index in the array hashtab;
 * 				  if the string is to be found anywhere,
 * 				  it will be in the list of blocks beginning there.
 * 				  The search is performed by lookup().If lookup() finds the
 * 				  entry already present, it returns a pointer to it; if not, it returns NULL
 *
 * Input Params	: Input String to lookup Hash Table
 *
 * Output Params: Hash List
 * ============================================================================
 */
static HASHLST_PTYPE addEntryToHashTable(char *pszSetName, char *pszValue)
{
	HASHLST_PTYPE pstHashLst;
    unsigned int hashval;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

    if ((pstHashLst = lookupHashTable(pszSetName)) == NULL)        // NOT found
    {
        pstHashLst = (HASHLST_PTYPE)malloc(sizeof(HASHLST_STYPE));

        if ((pstHashLst == NULL) || (pstHashLst->pszSetName = strdup(pszSetName)) == NULL)
        {
        	debug_sprintf(szDbgMsg, "%s: Error while creating memory for Hash list ", __FUNCTION__);
        	APP_TRACE(szDbgMsg);
        	/* Daivik:27/1/2016 - Coverity 67243 : Free the allocated resource */
        	free(pstHashLst);
            return NULL;
        }

        //debug_sprintf((szDbgMsg, "%s - TagName=[%s], allocated %d bytes, pointer=%p",
                        //__FUNCTION__, pszTagName, sizeof(HASHLST_STYPE), pstHashLst));
        //APP_TRACE(szDbgMsg);

        hashval = generateHashVal(pszSetName); //Generating the Hash Value

/*        debug_sprintf(szDbgMsg, "%s: Generated Hash Value for key [%s] is [%d], Key Value is [%s] ", __FUNCTION__, pszSetName, hashval, pszValue);
        APP_TRACE(szDbgMsg);*/

        pstHashLst->next = hashtab[hashval];

        hashtab[hashval] = pstHashLst;

    }
    else // Already there
    {
    	debug_sprintf(szDbgMsg, "%s: Already present, need not add it again ", __FUNCTION__);
    	APP_TRACE(szDbgMsg);
    }

    memset(pstHashLst->szDecisionVal, 0x00, sizeof(pstHashLst->szDecisionVal));
    strcpy(pstHashLst->szDecisionVal, pszValue);

    return pstHashLst;
}
