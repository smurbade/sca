/******************************************************************
*                       binInclusionTable.c                       *
*******************************************************************
* Application: Point Solutions                                    *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
* Created on: Feb 9, 2015                                                                *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <syslog.h>
#include <svc.h>
#include <svcsec.h>

#include "common/common.h"
#include "common/utils.h"
#include "appLog/appLogAPIs.h"

#define PCI_BIT_FILE	"./flash/PCI_BIT.DAT"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

struct BITRange{
    long int lower;
    long int upper;
    struct BITRange *next;
};
typedef struct BITRange BIT_RECORD;

BIT_RECORD * gpBIT;

static int ValidateBTTRecord(char *,int );
static int addBITRecord(long int , long int );
static int AuthenticateBITFile();

/*
 * ============================================================================
 * Function Name: loadAndValidateBITFile
 *
 * Description	: This API would load the PCI BIN file and validate the each record
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int loadBITFile()
{
    int 	rv 				= SUCCESS;
    char 	szBuffer[32]	= "";
    FILE *	fpBET;
    int		iAppLogEnabled		= isAppLogEnabled();
    char	szAppLogData[300]	= "";
    char	szAppLogDiag[300]	 = "";

	char	szDbgMsg[256]		= "";


	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	rv = AuthenticateBITFile();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Could not Authenticate BIT File", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return rv;
	}

    fpBET = fopen(PCI_BIT_FILE,"r");
    if(!fpBET)
    {
    	debug_sprintf(szDbgMsg, "%s: BIT file not found", __FUNCTION__);
    	APP_TRACE(szDbgMsg);

    	if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "BIT File Not Found, ");
			strcpy(szAppLogDiag, "Please Download BIT File");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, szAppLogDiag);
		}

        return FAILURE; //Return proper response code
    }

    while(!feof(fpBET))
    {
        memset(szBuffer,0x00,sizeof(szBuffer));
        if(fgets(szBuffer,sizeof(szBuffer),fpBET)==NULL)
        {
            break;
        }

        /*if the first character is new line or # -> for comment, then ignore it and continue*/
        if ( szBuffer[0] == '\n' || szBuffer[0] == '#' || szBuffer[0] == '\r')
        {
            while(szBuffer[strlen(szBuffer)-1] != '\n')
            {
                memset(szBuffer,0x00,sizeof(szBuffer));
                if(fgets(szBuffer,sizeof(szBuffer),fpBET)==NULL)
                {
                    break;
                }
            }
            continue;
        }
        if (szBuffer[strlen(szBuffer)-1] == '\n')
        {
            szBuffer[strlen(szBuffer)-1] = 0;/*eliminate the last new line character character*/
        }
        if (szBuffer[strlen(szBuffer)-1] == '\r')
        {
            szBuffer[strlen(szBuffer)-1] = 0;/*eliminate the last new line character character*/
        }

        /*Validate the BET range*/
        if(ValidateBTTRecord(szBuffer, strlen(szBuffer)) != SUCCESS)
        {
            debug_sprintf(szDbgMsg, "%s: BIT file Validation Failed", __FUNCTION__);
            APP_TRACE(szDbgMsg);

            sprintf(szDbgMsg, "BIT Validation failed : %s ", szBuffer);

            syslog(LOG_INFO|LOG_USER, szDbgMsg);

            if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "BIT File Validation Failed. ");
				strcpy(szAppLogDiag, "Please Check BIT File Format");
				addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, szAppLogDiag);
			}

            rv = FAILURE;
            break;
        }
    }

    if(gpBIT == NULL)
    {
    	 debug_sprintf(szDbgMsg, "%s: No Records in BIT file", __FUNCTION__);
    	 APP_TRACE(szDbgMsg);

    	if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "No Records in BIT File. ");
			strcpy(szAppLogDiag, "Please Download BIT File With Records");
			addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, szAppLogDiag);
		}
    	rv = FAILURE;
    }


    fclose(fpBET);

    debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
    APP_TRACE(szDbgMsg);

    return rv;
}

/*
 * ============================================================================
 * Function Name: checkBinInclusion
 *
 * Description	: This API checks whether the input bin falls in any of the
 * 				  BIN Inclusion range
 *
 * Input Params	: char array containing Bin to check
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL checkBinInclusion(char *bin)
{
    long int 	lbin;
    int			iAppLogEnabled		= isAppLogEnabled();
    char		szAppLogData[300]	= "";
    BIT_RECORD *Temp;

    #ifdef DEBUG
    	char	szDbgMsg[256]		= "";
    #endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(bin == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No BIN to check!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return PAAS_FALSE;
	}

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: Incoming BIN  [%s]", DEVDEBUGMSG, __FUNCTION__, bin);
		APP_TRACE(szDbgMsg);
#endif


#ifdef DEVDEBUG
    debug_sprintf(szDbgMsg, "DEVDEBUG - BIN in checkBINExclusion is: %s", bin);
    APP_TRACE(szDbgMsg);
#endif

    lbin = strtol(bin, NULL, 10);
    Temp = gpBIT;
    while(Temp != NULL)
    {

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: Lower BIN: %ld, Upper BIN: %ld", DEVDEBUGMSG, __FUNCTION__, Temp->lower, Temp->upper);
		APP_TRACE(szDbgMsg);
#endif
        if((lbin >= Temp->lower) && (lbin <= Temp->upper))
        {
        	debug_sprintf(szDbgMsg, "%s - BIN Included !", __FUNCTION__);
        	APP_TRACE(szDbgMsg);

        	 if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "BIN Included in PCI BIN File");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

            return PAAS_TRUE;
        }

        Temp = Temp->next;
    }

    if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "BIN Excluded in PCI BIN File");
		addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
	}

    debug_sprintf(szDbgMsg, "%s - BIN Not Included !", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    return PAAS_FALSE;
}

/*
 * ============================================================================
 * Function Name: ValidateBTTRecord
 *
 * Description	: This API Validate the BET Record and returns the status
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int AuthenticateBITFile()
{
	int 	rv 					= SUCCESS;
	 int	iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	if( (doesFileExist(PCI_BIT_FILE) == SUCCESS) &&
		((doesFileExist(PCI_BIT_FILE".P7S") == SUCCESS) || (doesFileExist(PCI_BIT_FILE".p7s") == SUCCESS)) )
    {
        debug_sprintf(szDbgMsg, "%s - %s and its signature file found", __FUNCTION__, PCI_BIT_FILE);
        APP_TRACE(szDbgMsg); // BET.DAT found in home directory

        if (authFile(PCI_BIT_FILE) == 1)
        {
            debug_sprintf(szDbgMsg, "%s - BIT File is authenticated", __FUNCTION__);
            APP_TRACE(szDbgMsg); // BIT.DAT found in home directory is authenticated

            if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Authentication of BIT File Succeeded");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

            rv = SUCCESS;
        }
        else
        {
        	 debug_sprintf(szDbgMsg, "%s - BIT File is not authenticated", __FUNCTION__);
        	 APP_TRACE(szDbgMsg); // BIT.DAT found in home directory is authenticated

        	if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Authentication of BIT File Failed. ");
				strcpy(szAppLogDiag, "Please Sign and Downlaod BIT File");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, szAppLogDiag);
			}


        	rv = FAILURE;
        }
    }
    else
    {
    	debug_sprintf(szDbgMsg, "%s - BIT File is not Found", __FUNCTION__);
    	APP_TRACE(szDbgMsg); // BIT.DAT found in home directory is authenticated

    	if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "BIT File or Its Signature File Missing. ");
			strcpy(szAppLogDiag, "Please Downlaod BIT File and Its Signature File");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, szAppLogDiag);
		}

    	rv = FAILURE;
    }

    debug_sprintf(szDbgMsg, "%s - returning %d", __FUNCTION__, rv);
    APP_TRACE(szDbgMsg);

    return rv;
}


/*
 * ============================================================================
 * Function Name: ValidateBTTRecord
 *
 * Description	: This API Validate the BET Record and returns the status
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int ValidateBTTRecord(char *szData,int size)
{
    int 		iSepFlag 			= 0;
    int			j 					= 0;
    int			iAppLogEnabled		= isAppLogEnabled();
    char 		lData[20] 			= "";
    char		rData[20]			= "";
    long int 	lLower				= 0;
    long int 	lUpper				= 0;;
    char		szAppLogData[300]	= "";
    char		szAppLogDiag[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	memset(lData, 0x00, sizeof (lData));
	memset(rData, 0x00, sizeof (rData));

    while(j < size)
    {
        if(szData[j] == '-')
        {
            iSepFlag = j;
        }
		else if(j == size-1)
        {
            if(szData[j] != ';')// Last character should be semi colon
            {

                debug_sprintf(szDbgMsg, "%s:Last Character for BET Record : %s is not semicolon", __FUNCTION__, szData);
                APP_TRACE(szDbgMsg);

                if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Last Character for BET Record is Not Semi-Colon ");
					addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
				}

                return FAILURE;
            }
        }
		else if(szData[j] < 48 || szData[j] > 57) // Check for non numeric value
        {
            debug_sprintf(szDbgMsg, "%s:BIT Record : %s contains invalid data", __FUNCTION__, szData);
            APP_TRACE(szDbgMsg);
            if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "BIT File Record Contains Invalid Data");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

            return FAILURE;
        }
        j++;
    }

    if(iSepFlag == 0) //Check for field separator '-'
	{
        debug_sprintf(szDbgMsg, "%s- Field separator is missing for BIT Record : %s ", __FUNCTION__, szData);
        APP_TRACE(szDbgMsg);

        if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Field Separator is Missing in BIT File Record");
			addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
		}

        return FAILURE;
    }
    memcpy(&lData, &szData[0], iSepFlag);
    memcpy(&rData, &szData[iSepFlag+1], size-(iSepFlag+2));

    if((strlen(lData) != 6) || (strlen(rData) != 6))  // Length of LHS and RHS of each BET record should be 6
    {
        debug_sprintf(szDbgMsg, "%s- Length of BIT Record : %s is not valid ", __FUNCTION__, szData);
        APP_TRACE(szDbgMsg);

        if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Length of BIT File Record Range is Not Valid");
			addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
		}

        return FAILURE;
    }

    lLower = lUpper = 0;
    lLower = strtol(lData, NULL, 10);
    lUpper = strtol(rData, NULL, 10);
    if(lLower > lUpper)
    {
        debug_sprintf(szDbgMsg, "%s- BET Lower limit is greater than upper limit : %s ", __FUNCTION__, szData);
        APP_TRACE(szDbgMsg);

        if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "In BIT File Record, Lower Limit is Greater Than Upper Limit");
			addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
		}

        return FAILURE;
    }

    debug_sprintf(szDbgMsg, "%s - Valid BIT Record, Adding to the data system", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    return addBITRecord(lLower, lUpper);

}

/*
 * ============================================================================
 * Function Name: addBITRecord
 *
 * Description	: This API adds the BIT Record to the Data system
 *
 * Input Params	: Lower & Upper Range of BIT Record
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */

static int addBITRecord(long int lLower, long int lUpper)
{
    BIT_RECORD *Temp = NULL, *Temp2;

    Temp = (BIT_RECORD *) malloc(sizeof(BIT_RECORD));
    Temp->next = NULL;

    Temp->lower = lLower;
    Temp->upper = lUpper;

    if(gpBIT == NULL)
    {
    	gpBIT = Temp;
    }
    else
    {
        Temp2 = gpBIT;
        while(Temp2->next != NULL)
        {
            Temp2 = Temp2->next;
        }
        Temp2->next = Temp;
    }

    return SUCCESS;
}
