/*
 * ============================================================================
 * File Name	: baseDataSystem.c
 *
 * Description	:
 *
 * Author		: Vikram Datt Rana
 * ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "db/baseDataSystem.h"
#include "common/utils.h"
#include "appLog/appLogAPIs.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

typedef struct __llNode
{
	char				szKey[5];
	void *				data;
	DATAINFO_STYPE		stDataInfo;
	struct __llNode *	next;
}
LLNODE_STYPE, * LLNODE_PTYPE;

typedef struct __lkdlist
{
	int				iElemCnt;
	LLNODE_PTYPE	firstNode;
	LLNODE_PTYPE	lastNode;
	pthread_mutex_t	llLock;
}
LKDLST_STYPE, * LKDLST_PTYPE;

/* Static variables declarations */
static LKDLST_STYPE		stLkdList;
static PAAS_BOOL		iDSInitDone = PAAS_FALSE;

/* Static functions declarations */
static int	addNodeToLkdList(void *, char *, DATAINFO_PTYPE);
static void generateUniqueKey(char *);
static void flushData(LLNODE_PTYPE);
static PAAS_BOOL isKeyUnique();

/*
 * ============================================================================
 * Function Name: initBaseDataLayer
 *
 * Description	: This API is used for initializing the base data system. It
 * 					should be called in the begining of the program.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int	initBaseDataLayer()
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(iDSInitDone == PAAS_TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: Base Data System is already initialized",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Base data System is Already Initialized");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
		}

		rv = FAILURE;
	}
	else
	{
		memset(&stLkdList, 0x00, sizeof(LKDLST_STYPE));

		/* Initialize the mutex for the linked list */
		pthread_mutex_init(&(stLkdList.llLock), NULL);

		srand(1596);

		iDSInitDone = PAAS_TRUE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: flushBaseDataLayer
 *
 * Description	: This API is responsible for flushing out the complete data
 * 					system, should be used sparingly and cautiously
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int	flushBaseDataLayer()
{
	int				rv				= SUCCESS;
	LLNODE_PTYPE	curNodePtr		= NULL;
	LLNODE_PTYPE	prevNodePtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Acquire the lock before changing the contents of linked list */
	acquireMutexLock(&(stLkdList.llLock), __FUNCTION__);

	curNodePtr = stLkdList.firstNode;
	while(curNodePtr != NULL)
	{
		/* Flush out the data stored in the node */
		flushData(curNodePtr);

		/* Flush the current node and get to the next one */
		prevNodePtr = curNodePtr;
		curNodePtr = curNodePtr->next;
		free(prevNodePtr);
	}

	/* Reset the linked list meta data */
	stLkdList.iElemCnt = 0;
	stLkdList.firstNode = NULL;
	stLkdList.lastNode = NULL;

	/* Release the lock after changing the contents of linked list */
	releaseMutexLock(&(stLkdList.llLock), __FUNCTION__);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: flushDataForKey
 *
 * Description	: This API is responsible for flushing out the data from the
 * 					linkedlist
 *
 * Input Params	: char * szKey -> key for locating the data in the linked list
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int	flushDataForKey(char * szKey, int iClean)
{
	int				rv				= SUCCESS;
	LLNODE_PTYPE	curNodePtr		= NULL;
	LLNODE_PTYPE	prevNodePtr		= NULL;
	PAAS_BOOL		bkeyFound    	= PAAS_FALSE;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	curNodePtr = stLkdList.firstNode;
	while(curNodePtr != NULL)
	{
		if(strcmp(szKey, curNodePtr->szKey) == SUCCESS)
		{
			/* Acquire the lock before changing the contents of linked list */
			acquireMutexLock(&(stLkdList.llLock), __FUNCTION__);

			if(iClean)
			{
				/* Flush out the data stored in the node */
				flushData(curNodePtr);
			}
			if(stLkdList.iElemCnt == 1)
			{
				/* Only one node is present in the list */
				stLkdList.firstNode = NULL;
				stLkdList.lastNode = NULL;
			}
			else if(stLkdList.firstNode == curNodePtr)
			{
				/* First node in the list matches the criteria */
				stLkdList.firstNode = curNodePtr->next;
			}
			else if(stLkdList.lastNode == curNodePtr)
			{
				/* Last node in the list matches the criteria */
				prevNodePtr->next = NULL;
				stLkdList.lastNode = prevNodePtr;
			}
			else
			{
				/* Any middle node in the list matches the criteria */
				prevNodePtr->next = curNodePtr->next;
			}

			free(curNodePtr);
			stLkdList.iElemCnt--;

			/* Release the lock after changing the contents of linked list */
			releaseMutexLock(&(stLkdList.llLock), __FUNCTION__);
			bkeyFound = PAAS_TRUE;
			break;
		}

		prevNodePtr = curNodePtr;
		curNodePtr = curNodePtr->next;
	}

	if((curNodePtr == NULL) && (!bkeyFound))
	{
		debug_sprintf(szDbgMsg, "%s: No record exists with key [%s]",
														__FUNCTION__, szKey);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addData
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int addData(DATAINFO_PTYPE pstDataInfo, char * szKey)
{
	int		rv				= SUCCESS;
	int		iSize			= 0;
	void *	data			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstDataInfo == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if((iSize = pstDataInfo->iDataSize) <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid data size [%d]", __FUNCTION__,
																		iSize);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		data = malloc(iSize);
		if(data == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(data, 0x00, iSize);

		/* Generate a unique key for the data */
		generateUniqueKey(szKey);
		debug_sprintf(szDbgMsg, "%s: Generated Unique Key",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Add the data in the linked list */
		rv = addNodeToLkdList(data, szKey, pstDataInfo);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to add data to the list",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getData
 *
 * Description	: This API is responsible for finding out the data
 *					corresponding to a given key passed as parameter
 *
 * Input Params	: szKey -> unique identifier for the data
 *
 * Output Params: NULL / dataPtr (void *)
 * ============================================================================
 */
void * getData(char * szKey)
{
	void *			dataPtr			= NULL;
	LLNODE_PTYPE	pstCurNode		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	while(1)
	{
		if( (szKey == NULL) || (strlen(szKey) <= 0) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		pstCurNode = stLkdList.firstNode;
		while(pstCurNode != NULL)
		{
			if(strcmp(pstCurNode->szKey, szKey) == SUCCESS)
			{
				dataPtr = pstCurNode->data;
				break;
			}

			pstCurNode = pstCurNode->next;
		}

		if(pstCurNode == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No record present in with key [%s]",
								__FUNCTION__, szKey);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	return dataPtr;
}

/*
 * ============================================================================
 * Function Name: flushData
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void flushData(LLNODE_PTYPE pstLLNode)
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Perform any cleanup on the data if a cleanup function is provided by the
	 * thread which was using this data */
	if(pstLLNode->stDataInfo.cleanupData != NULL)
	{
		pstLLNode->stDataInfo.cleanupData(pstLLNode->data);
	}

	/* Free the allocated memory for the data */
	free(pstLLNode->data);

	return;
}

/*
 * ============================================================================
 * Function Name: addNodeToLkdList
 *
 * Description	: This static function is responsible for creating a linked
 * 					list node and adding it to the liked list, populating the
 * 					details coming as part of parameters to the function
 *
 * Input Params	: 	data	-> data for the linked list node
 * 					szKey	-> unique identifier for the node
 * 					pstInfo -> meta information about the data
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int addNodeToLkdList(void * data, char * szKey, DATAINFO_PTYPE pstInfo)
{
	int				rv				= SUCCESS;
	LLNODE_PTYPE	pstLLNode		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Allocate memory for the node */
		pstLLNode = (LLNODE_PTYPE) malloc(sizeof(LLNODE_STYPE));
		if(pstLLNode == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the details in the linked list node */
		memset(pstLLNode, 0x00, sizeof(LLNODE_STYPE));
		pstLLNode->data = data;
		strcpy(pstLLNode->szKey, szKey);
		memcpy(&(pstLLNode->stDataInfo), pstInfo, sizeof(DATAINFO_STYPE));

		/* ------- Add the node to the linked list -------- */

		/* Acquire the lock before changing the contents of linked list */
		debug_sprintf(szDbgMsg, "%s: Acquiring the List Mutex", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		acquireMutexLock(&(stLkdList.llLock), __FUNCTION__);
		debug_sprintf(szDbgMsg, "%s: Acquired the List Mutex", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(stLkdList.lastNode == NULL)
		{ 
			/* Adding the first node in the linked list */
			stLkdList.firstNode = stLkdList.lastNode = pstLLNode;
		}
		else
		{
			/* Adding the nth node in the linked list */
			stLkdList.lastNode->next = pstLLNode;
			stLkdList.lastNode = pstLLNode;
		}

		stLkdList.iElemCnt++;

		/* Initialize the data element being added, using the custom function
		 * if provided by the caller */
		if(pstInfo->initData != NULL)
		{
			pstInfo->initData(data);
		}

		/* Release the lock after changing the contents of linked list */
		releaseMutexLock(&(stLkdList.llLock), __FUNCTION__);

		break;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: generateUniqueKey
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
static void generateUniqueKey(char * szKey)
{
	int		iCtr		= 0;
	char	szLbl[5]	= "";

	iCtr = rand() % 1000;

	do
	{
		iCtr++;
		sprintf(szLbl, "V%03d", iCtr);
	}
	while(isKeyUnique(szLbl) != PAAS_TRUE);

	strcpy(szKey, szLbl);

	return;
}

/*
 * ============================================================================
 * Function Name: isKeyUnique
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
static PAAS_BOOL isKeyUnique(char * szKey)
{
	PAAS_BOOL		bRetVal			= PAAS_TRUE;
	LLNODE_PTYPE	pstCurNode		= NULL;

	pstCurNode = stLkdList.firstNode;
	while(pstCurNode != NULL)
	{
		if(strcmp(szKey, pstCurNode->szKey) == SUCCESS)
		{
			bRetVal = PAAS_FALSE;
			break;
		}

		pstCurNode = pstCurNode->next;
	}

	return bRetVal;
}

/*
 * ============================================================================
 * End of file baseDataSytem.c
 * ============================================================================
 */
