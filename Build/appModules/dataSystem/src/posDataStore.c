/****************************************************************

    Filename    :   posDataStore.c

    Application :   Mx Point SCA Application
    Description :   This module will provide the mechanism used to access and update POS authentication
					records in the persistent memory.

   Modification History:

     Date     Version No   	 Programmer    		Change History
    -------   -----------  	 ----------- 	  ------------------------
						   	Vikram Datt Rana		
******************************************************************
   Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved.

No part of this software may be used, stored, compiled, reproduced,
modified, transcribed, translated, transmitted, or transferred, in
any form or by any means  whether electronic, mechanical,  magnetic, optical, or otherwise, without the express prior written permission of VeriFone, Inc.
*******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "svc.h"
#include "common/utils.h"
#include "db/posDataStore.h"
#include "appLog/appLogAPIs.h"

#define POSAUTH_TABLE_SIZE	10

#define TEMP_FILE   "/var/tmp/tmpPosAuth.dat"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

int				giTotPOSConn = 0;
POSID_PTR_TYPE	posAuthTable[POSAUTH_TABLE_SIZE];

/* Static functions declarations - BEGIN */

static int getLFURecordIndex();
static PAAS_BOOL isLabelUnique(char *);

/* Static functions declarations - END */

int savePosInfoRecords();

/*
 * ============================================================================
 * Function Name: loadPosInfoRecords
 *
 * Description	: This function has the responsibility to load the POS
 * 					authentication records from the flat file stored in the
 * 					persistent memory
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int loadPosInfoRecords()
{
	int				rv					= SUCCESS;
	int				iIndex				= 0;
	int				iCnt				= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	FILE *			fp					= NULL;
	char *			cPtr				= NULL;
	char			szTemp[128]			= "";
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";
	unsigned char	aesKey[16]			= "";
	POSID_PTR_TYPE	tempRecPtr			= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	/* Initialize the POS Auth table */
	for(iCnt = 0; iCnt < POSAUTH_TABLE_SIZE; iCnt++)
	{
		if(posAuthTable[iCnt] != NULL) {
			/* Deallocate the memory */
			free(posAuthTable[iCnt]);
			posAuthTable[iCnt] = NULL;
		}
	}

	/* Check for the flat file */
	if( SUCCESS == doesFileExist(POS_AUTH_FILE) )
	{
		debug_sprintf(szDbgMsg, "%s: Loading the records from file [%s]",
												__FUNCTION__, POS_AUTH_FILE);
		APP_TRACE(szDbgMsg);

		/* Open the file in read only mode */
		fp = fopen(POS_AUTH_FILE, "r");
		if(fp == NULL)
		{
			/* Unable to open the file */
			debug_sprintf(szDbgMsg, "%s: Unable to open file [%s]",__FUNCTION__
															, POS_AUTH_FILE);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Unable to Open POS Authentication File");
				addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
			}

			rv = FAILURE;
		}
		else
		{
			/* File opened ... load the records in the memory */
			for(iCnt = 0; iCnt < POSAUTH_TABLE_SIZE; iCnt++)
			{
				memset(szTemp, 0x00, sizeof(szTemp));
				if(NULL == fgets(szTemp, 128, fp))
				{
					/* Either we reached end of file or some error occured
					 * while reading the file. In both the cases, stop further
					 * reading and return back to the caller. */
					if(ferror(fp))
					{
						debug_sprintf(szDbgMsg, "%s: Error while reading [%s]",
												__FUNCTION__, POS_AUTH_FILE);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						if(iAppLogEnabled == 1)
						{
							strcpy(szAppLogData, "Error While Reading POS Authentication File.");
							strcpy(szAppLogDiag, "Reached the End of File, Please Check the File");
							addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, szAppLogDiag);
						}
					}

					break;
				}
				else
				{
					/* Store the record data into the memory */
					tempRecPtr = NULL;
					tempRecPtr = (POSID_PTR_TYPE)
											malloc(sizeof(POSID_STRUCT_TYPE));

					if(tempRecPtr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Memory allocation error",
																__FUNCTION__);
						APP_TRACE(szDbgMsg);

						rv = FAILURE;
						break;
					}

					memset(tempRecPtr, 0x00, sizeof(POSID_STRUCT_TYPE));

					/* Some initializations */
					iIndex	= 0;
					cPtr	= NULL;

					/* Assign the values */
					cPtr = strtok(szTemp, "|");
					while (cPtr != NULL)
					{
						if(iIndex == 0)
						{
							/* MAC Label */
							memcpy(tempRecPtr->MACLabel, cPtr, strlen(cPtr));
						}
						else if(iIndex == 1)
						{
							/* MAC Key */
							memset(aesKey, 0x00, 16);
							asciiToHex(aesKey, cPtr, 16);
							memcpy(tempRecPtr->MACKey, aesKey, 16);
						}
						else if(iIndex == 2)
						{
							tempRecPtr->tranCtr = strtoul(cPtr, NULL, 0);
						}
						else if(iIndex == 3)
						{
							/* Time stamp */
							memcpy(tempRecPtr->szTStamp, cPtr, 14);
						}
						cPtr = strtok(NULL, "|");
						iIndex++;
					}

					if(iIndex >= 2 )
					{
						posAuthTable[iCnt] = tempRecPtr;

						debug_sprintf(szDbgMsg,
						"%s: [%d] - Lbl [%s], Ctr [%lu], TS [%s]",
									__FUNCTION__, iCnt, tempRecPtr->MACLabel,
									tempRecPtr->tranCtr, tempRecPtr->szTStamp);
						APP_TRACE(szDbgMsg);
					}
					// CID-67405: 25-Jan-16: MukeshS3:
					// If this file is having data in invalid format/corrupted data, it wont parse & store it in memory rather it was leaking memory(tempRecPtr).
					else
					{
						if(tempRecPtr != NULL)
						{
							free(tempRecPtr);
						}
						posAuthTable[iCnt] = NULL;
					}
					giTotPOSConn++;
				}
			} /* End of for Loop */
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "POS Authentication Information Records Loaded Successfully");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}
		}
	}
	else
	{
		/* If the file does not exist or is empty, we can assume that no POS
		 * authentication record exists. */
		debug_sprintf(szDbgMsg, "%s: [%s] File is empty or does not exist",
												__FUNCTION__, POS_AUTH_FILE);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "POS Authentication File is Empty or File Does not Exist.");
			addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
		}
	}

	/* Close the file pointer for the file */
	if(fp != NULL)
	{
		fclose(fp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getLFURecordIndex
 *
 * Description	: This function will return back the index of the POS record
 * 					with the oldest time stamp. We are assuming that the POS
 * 					record with the oldest time stamp will be the least
 * 					frequently used POS record and thus can be purged from the
 * 					table.
 *
 * Input Params	: none
 *
 * Output Params: iIndex -> index of the record with oldest time stamp
 * ============================================================================
 */
static int getLFURecordIndex()
{
	int		iIndex		= 0;
	int		iCnt		= 0;
	char *	szTSPtr		= NULL;

	iIndex = 0;
	szTSPtr = posAuthTable[0]->szTStamp;

	for(iCnt = 1; iCnt < POSAUTH_TABLE_SIZE; iCnt++)
	{
		/* If the current record's time stamp is less than the reference time
		 * stamp, set the reference time stamp to current record's time stamp
		 * and save the index of this record. */
		if(strcmp(szTSPtr, posAuthTable[iCnt]->szTStamp) > 0)
		{
			szTSPtr = posAuthTable[iCnt]->szTStamp;
			iIndex = iCnt;
		}
	}

	/* Return the index of the record having the oldest value of time stamp */
	return iIndex;
}

/*
 * ============================================================================
 * Function Name: addPosInfoRecord
 *
 * Description	: This function is responsible for adding POS authentication
 * 					record in the table
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int addPosInfoRecord(uchar * aesKey, char * label)
{
	int				rv				= SUCCESS;
	int				iIndex			= 0;
	POSID_PTR_TYPE	tempRecPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	tempRecPtr = (POSID_PTR_TYPE) malloc(sizeof(POSID_STRUCT_TYPE));
	if(tempRecPtr)
	{
		/* Assign the values */
		memset(tempRecPtr, 0x00, sizeof(POSID_STRUCT_TYPE));
		tempRecPtr->tranCtr = 0L;
		memcpy(tempRecPtr->MACLabel, label, strlen(label));
		memcpy(tempRecPtr->MACKey, aesKey, 16);

		genTimeStamp(tempRecPtr->szTStamp, 1);

		/* Search for the position in the table where the record is to be
		 * inserted */
		for(iIndex = 0; iIndex < POSAUTH_TABLE_SIZE; iIndex++)
		{
			if(posAuthTable[iIndex] == NULL)
			{
				break;
			}
		}

		if(iIndex == POSAUTH_TABLE_SIZE)
		{
			/* If all the slots are filled up in the table, find the record
			 * which was least frequently used, in this case, having the oldest
			 * time stamp of access; and overwrite that record with the new
			 * record */
			iIndex = getLFURecordIndex();
			free(posAuthTable[iIndex]);

			debug_sprintf(szDbgMsg, "%s: Overwriting POS record at index [%d]",
														__FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);
		}

		posAuthTable[iIndex] = tempRecPtr;

		/* Save the list in the persistent memory */
		rv = savePosInfoRecords();
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation failure", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMACLabel
 *
 * Description	: This function is responsible for generating a unique label,
 * 					required for storing a new POS auth info record.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
void getMACLabel(char * label)
{
	static PAAS_BOOL	firstTime	= PAAS_TRUE;
	int					iCounter	= 0;
	char				szLabel[6]	= "";

	if(firstTime == PAAS_TRUE)
	{
		firstTime = PAAS_FALSE;
		srand(time(NULL));
	}

	/* Generate a random number between 0 to 1000 */
	iCounter = rand() % 1000;

	do
	{
		/* Keep the counter within 3 digits */
		iCounter = (iCounter + 1) % 1000;
		sprintf(szLabel, "P_%03d", iCounter);
	}
	while(isLabelUnique(szLabel) == PAAS_FALSE);

	memcpy(label, szLabel, strlen(szLabel));

	return;
}

/*
 * ============================================================================
 * Function Name: isLabelUnique
 *
 * Description	: This function is responsible for generating a unique label,
 *
 * Input Params	: label -> the string label to be checked for uniqueness
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
static PAAS_BOOL isLabelUnique(char * label)
{
	int			iCnt		= 0;
	PAAS_BOOL	bRetVal		= PAAS_TRUE;

	for(iCnt = 0; iCnt < POSAUTH_TABLE_SIZE; iCnt++)
	{
		if(posAuthTable[iCnt] == NULL)
		{
			continue;
		}

		if(strcmp(posAuthTable[iCnt]->MACLabel, label) == SUCCESS)
		{
			bRetVal = PAAS_FALSE;
			break;
		}
	}

	return bRetVal;
}

/*
 * ============================================================================
 * Function Name: getAESKey
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getAESKey(char * label, uchar * key, char * szErrMsg)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	for(iCnt = 0; iCnt < POSAUTH_TABLE_SIZE; iCnt++)
	{
		if(posAuthTable[iCnt] == NULL)
		{
			continue;
		}
		else if(strcmp(posAuthTable[iCnt]->MACLabel, label) == SUCCESS)
		{
			break;
		}
	}

	if(iCnt == POSAUTH_TABLE_SIZE)
	{
		debug_sprintf(szDbgMsg, "%s: No record exists for MAC label [%s]",
														__FUNCTION__, label);
		APP_TRACE(szDbgMsg);

		sprintf(szErrMsg, "MAC label [%s] does not exist", label);
		rv = ERR_NO_FLD;
	}
	else
	{
		memcpy(key, posAuthTable[iCnt]->MACKey, 16);

		rv = SUCCESS;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getTranCounter
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getTranCounter(char * label, ulong * tranCtr)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	for(iCnt = 0; iCnt < POSAUTH_TABLE_SIZE; iCnt++)
	{
		if(posAuthTable[iCnt] == NULL)
		{
			continue;
		}
		else if(strcmp(posAuthTable[iCnt]->MACLabel, label) == SUCCESS)
		{
			break;
		}
	}

	if(iCnt == POSAUTH_TABLE_SIZE)
	{
		debug_sprintf(szDbgMsg, "%s: No record exists for MAC label [%s]",
														__FUNCTION__, label);
		APP_TRACE(szDbgMsg);

		rv = ERR_NO_FLD;
	}
	else
	{
		*tranCtr = posAuthTable[iCnt]->tranCtr;

		rv = SUCCESS;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateTranCounter
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int	updateTranCounter(char * label, ulong value)
{
	int			rv				= SUCCESS;
	int			iCnt			= 0;
	static int	changes			= 0;

#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	for(iCnt = 0; iCnt < POSAUTH_TABLE_SIZE; iCnt++)
	{
		if(posAuthTable[iCnt] == NULL)
		{
			continue;
		}
		else if(strcmp(posAuthTable[iCnt]->MACLabel, label) == SUCCESS)
		{
			break;
		}
	}

	if(iCnt == POSAUTH_TABLE_SIZE)
	{
		debug_sprintf(szDbgMsg, "%s: No record exists for MAC label [%s]",
														__FUNCTION__, label);
		APP_TRACE(szDbgMsg);

		rv = ERR_NO_FLD;
	}
	else
	{
		posAuthTable[iCnt]->tranCtr = value;

		/* Update the time stamp for the latest modification of the pos
		 * authentication record */
		genTimeStamp(posAuthTable[iCnt]->szTStamp, 1);

		changes++;

		/* If more than 10 changes done, update the latest list in the
		 * persistent memory */
		if(changes >= 10)
		{
			debug_sprintf(szDbgMsg, "%s: Backing up the POS records in FILE",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = savePosInfoRecords();
			changes= 0;;//After storing to file, updating the variable so that we enter this condition after 10 times again
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: isPOSRegistered
 *
 * Description	: This function would check whether requested POS label matches with the any one label present in
 * 				  in the POS authentication table.
 *
 * Input Params	: label -> MAC Label of the record which is to be checked
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isPOSRegistered(char * label)
{
	int			iCnt			= 0;
	PAAS_BOOL	bPOSRegistered	= PAAS_FALSE;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < POSAUTH_TABLE_SIZE; iCnt++)
	{
		if(posAuthTable[iCnt] == NULL)
		{
			continue;
		}
		else if(strcmp(posAuthTable[iCnt]->MACLabel, label) == SUCCESS)
		{
			bPOSRegistered = PAAS_TRUE;
			break;
		}
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__,
			(bPOSRegistered == PAAS_TRUE)? "TRUE": "FALSE");
	APP_TRACE(szDbgMsg);

	return bPOSRegistered;
}

/*
 * ============================================================================
 * Function Name: deletePosInfoRecord
 *
 * Description	: This function is responsible for deleting a POS
 * 					authentication record from the memory, given the label.
 *
 * Input Params	: label -> MAC Label of the record which is to be deleted
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int deletePosInfoRecord(char * label)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		jCnt			= 0;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < POSAUTH_TABLE_SIZE; iCnt++)
	{
		if(posAuthTable[iCnt] == NULL)
		{
			continue;
		}
		else if(strcmp(posAuthTable[iCnt]->MACLabel, label) == SUCCESS)
		{
			break;
		}
	}

	if(iCnt == POSAUTH_TABLE_SIZE)
	{
		debug_sprintf(szDbgMsg, "%s: Couldnt find the record for label [%s]",
						__FUNCTION__, label);
		APP_TRACE(szDbgMsg);

		rv = ERR_NO_FLD;
	}
	else
	{
		free(posAuthTable[iCnt]);
		posAuthTable[iCnt] = NULL;

		debug_sprintf(szDbgMsg, "%s: Deleted the record for label [%s]",
														__FUNCTION__, label);
		APP_TRACE(szDbgMsg);

		for(jCnt = iCnt; jCnt < POSAUTH_TABLE_SIZE - 1; jCnt++)
		{
			posAuthTable[jCnt] = posAuthTable[jCnt + 1];
		}

		if(jCnt == (POSAUTH_TABLE_SIZE - 1))
		{
			posAuthTable[jCnt] = NULL;
		}
		giTotPOSConn--;

		debug_sprintf(szDbgMsg, "%s: Total Connected POS Count [%d]", __FUNCTION__, giTotPOSConn);
		APP_TRACE(szDbgMsg);

		rv = savePosInfoRecords();

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: deleteAllPosInfoRecord
 *
 * Description	: This function is responsible for deleting all POS records
 * 				  from the memory if requested POS label matches with the any one label present in
 * 				  in the POS authentication table.
 *
 * Input Params	: label -> MAC Label of the record which is to be deleted
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int deleteAllPosInfoRecord()
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Deleting all records ",
													__FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iCnt = 0; iCnt < POSAUTH_TABLE_SIZE - 1; iCnt++)
	{
		if(posAuthTable[iCnt] != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Deleting records for label [%s]", __FUNCTION__,
					posAuthTable[iCnt]->MACLabel);
			APP_TRACE(szDbgMsg);

			free(posAuthTable[iCnt]);
			posAuthTable[iCnt] = NULL;
		}
	}
	if(iCnt == (POSAUTH_TABLE_SIZE - 1))
	{
		debug_sprintf(szDbgMsg, "%s: Deleted all records",
														__FUNCTION__);
		APP_TRACE(szDbgMsg);

		posAuthTable[iCnt] = NULL;
	}

	giTotPOSConn = 0;
	debug_sprintf(szDbgMsg, "%s: Total Connected POS Count [%d]", __FUNCTION__, giTotPOSConn);
	APP_TRACE(szDbgMsg);

	rv = savePosInfoRecords();

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: savePosInfoRecords
 *
 * Description	: This function is responsible for writing the POS
 * 					authentication records into persistent memory
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int savePosInfoRecords()
{

	int		rv				= SUCCESS;
	int		iCnt			= 0;
	FILE *	fp				= NULL;
	//char	szCmd[256]		= "";
	char	aesKey[33]		= "";
	char	szLine[128]		= "";

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	///* Open a temporary file and write the list content in the file */
	//fp = fopen(TEMP_FILE, "a+");

	/* Open a posAuth.dat List File and write the list content in the file */
	fp = fopen(POS_AUTH_FILE, "w+");

	if(fp != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: [%s] File Opened For Update",
													__FUNCTION__, POS_AUTH_FILE);
		APP_TRACE(szDbgMsg);

		for(iCnt = 0; iCnt < POSAUTH_TABLE_SIZE; iCnt++)
		{
			if(posAuthTable[iCnt] == NULL)
			{
				continue;
			}

			hexToAscii(aesKey, posAuthTable[iCnt]->MACKey, sizeof(aesKey) - 1);

			/* Label - Key - Transaction Counter */
			sprintf(szLine, "%s|%s|%lu|%s\n", posAuthTable[iCnt]->MACLabel,
										aesKey, posAuthTable[iCnt]->tranCtr,
										posAuthTable[iCnt]->szTStamp);
#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s: Data Record to be stored = [%s]",
														__FUNCTION__, szLine);
			APP_TRACE(szDbgMsg);
#endif

			fputs(szLine, fp);
		}

		fclose(fp);

		/* We can directly update the posAuth.dat instead of temp file., Updating the temp file and moving it to original location
		 * is causing few issues like deleting the posAuth.file itself*/
		/* Move the file to the flash overwriting existing file */
		//debug_sprintf(szDbgMsg, "%s: Overwriting file [%s] with file [%s]",
									//__FUNCTION__, POS_AUTH_FILE, TEMP_FILE);
		//APP_TRACE(szDbgMsg);

		//sprintf(szCmd, "mv %s %s", TEMP_FILE, POS_AUTH_FILE);
		//local_svcSystem(szCmd);
	}
	else
	{
		/* Unable to open file for writing */
		debug_sprintf(szDbgMsg, "%s: Unable to open file [%s] for writing",
													__FUNCTION__, POS_AUTH_FILE);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getDevMacLabels
 *
 * Description	: This function is responsible for returning all the mac labels
 * 					currently the device is mapped to 
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int getDevMacLabels(char* pszMacLabels)
{
	int		rv 		= SUCCESS;
	int		iIndex;

#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	for(iIndex = 0; iIndex < POSAUTH_TABLE_SIZE; iIndex++)
	{
		if(posAuthTable[iIndex] == NULL)
		{
			continue;
		}
		else
		{
			strcat(pszMacLabels, posAuthTable[iIndex]->MACLabel);
			strcat(pszMacLabels, "|");
		}
	}
	/*Removing the last printed | symbol from the string*/
	pszMacLabels[strlen(pszMacLabels) - 1] = '\0';

	debug_sprintf(szDbgMsg, "%s: MAC Labels existing in device are %s", __FUNCTION__, pszMacLabels);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * End of file posDataStore.c
 * ============================================================================
 */
