/******************************************************************
*                       safDataStore.c                            *
*******************************************************************
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <svc.h>
#include <svcsec.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/stat.h>
#include <setjmp.h>
#include <errno.h>

#include "common/tranDef.h"
#include "db/safDataStore.h"
#include "db/tranDS.h"
#include "common/utils.h"
#include "ssi/ssiDef.h"
#include "ssi/ssiAPIs.h"
#include "ssi/ssiMain.h"

#include "bLogic/bLogicCfgDef.h"
#include "db/dataSystem.h"
#include "appLog/appLogAPIs.h"
#include "bLogic/blVHQSupDef.h"

#define SALE_CMD      1
#define SIGNATURE_CMD 2
#define VOID_CMD      3
#define POST_AUTH_CMD 4

#define OLD_SAF_ELIGIBLE_RECS_FILE			"/mnt/flash/userdata/share/SAFRecords.db"
#define SAF_ELIGIBLE_RECORDS_FILE_NAME		"./flash/SAFRecords.db"
#define SAFRECORD_COUNT_FILE				"/var/tmp/saf_rec_count.txt"

#define OLD_SAF_COMPLETED_RECS_FILE			"/mnt/flash/userdata/share/SAFCompletedRecords.db"
#define SAF_COMPLETED_RECORDS_FILE_NAME		"./flash/SAFCompletedRecords.db"

#define SAF_RECORD_FILE_NAME_TEMP			"/var/tmp/SAFTempRecords.db"

#define OLD_SAF_DATA_FILE_PERMANENT 		"/mnt/flash/userdata/share/SAFData.db"
#define SAF_DATA_FILE_NAME_PERMANENT		"./flash/SAFData.db"

#define SAF_ERR_CODES_FILE					"./flash/SAF_ERR_CODES.DAT"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

static pthread_mutex_t gptSAFDetailsFileMutex;
static pthread_mutex_t gptSAFRecordFileMutex;

static pthread_mutex_t gptSAFTranKeyMutex;

static pthread_mutex_t gptSAFRecordInProgressMutex;

static pthread_t	ptOfflineTranProcId	= 0;
static pthread_t	ptSAFPurgingId		= 0;

static sigjmp_buf	safJmpBuf;

static char szSSITranKey[15];

//Host Resp Details for SAF.
static HOSTRESPDTLS_STYPE	stHostRespDtls;

//Global Variables for SAF
static int 					giSAFRecordSerialNum			= 0;
static unsigned int 		giCurrentSAFRecordNumber 		= 0;
static int 					giTotalSAFRecords        		= 0;
static int					giTotalEligibleSAFRecords 		= 0;
static time_t				gtLastOfflineEpochTime	 		= 0;
static double 				gfTotalSAFAmount         		= 0.0;
static unsigned int			giCurrentSAFIntrnSeqNum  		= 1000;
static unsigned int			giCurrentSAFTranSeqNum   		= 0;
static char					gszLastTimeStamp[25]	 		= "";
static SAF_INPROGRESS_STATUS	iSAFInProgressStatus		= SAF_IN_IDLE;
//Global Variables for TOR SAF
static unsigned int 		giCurrentSAFTORRecordNumber		= 0;
static int 					giTotalSAFTORRecords        	= 0;
static int					giTotalPendingSAFTORRecords 	= 0;

static char  *gpszSafErrCodes = NULL; //Pointer to store the SAF Error Codes

/* static functions declarations */
static int parseSAFDataLine(char *);
static int parseSAFDataLineForTOR(char *);
static int getRecordLength(FILE *);
static int getSAFRecordStatus(char *);
static int getSAFRecordNumber(char *, char * );
static int getSAFRecordStatusAndNumber(char *, char *);
static int parseSAFRecordLine(char *);
static int filterSAFRecsByStatus(SAFDTLS_PTYPE );
static int initSAFMutex();
static int createSAFTranStack();
//static int deleteSAFTranKey();
static int resetSAFDataFile();
static int getAllSAFRecs(SAFDTLS_PTYPE );
static int filterSAFRecsByRecNumber(char* , SAFDTLS_PTYPE );
static int filterSAFRecsByRecRange(SAFDTLS_PTYPE );
static int filterSAFRecsByStatusAndRecNum(SAFDTLS_PTYPE , char *);
static int filterSAFRecsByStatusAndRecRange(SAFDTLS_PTYPE );
static int filterSAFTORRecsByRecRange(SAFDTLS_PTYPE );
static int filterSAFTORRecsByRecNumber(char *, SAFDTLS_PTYPE);
static int filterSAFTORRecsByStatusAndRecNum(SAFDTLS_PTYPE , char *);
static int filterSAFTORRecsByStatusAndRecRange(SAFDTLS_PTYPE);
static char * updateSAFRecSignDtls(char *);
static char * updateSAFRecPymtType(char *);
static char * updateSAFRecSessionDtls(char *);
static char * updateSAFRecSessStoreIdDtls(char *);
static char * updateSAFRecSessLaneIdDtls(char *);
static char * updateSAFRecCustInfoDtls(char *);
static char * updateSAFRecTaxAmount(char *);
static char * updateSAFRecTaxIndicator(char *);
static char * updateSAFRecCommand(char *);
static char * updateSAFRecCardTrkIndicator(char *);
static char * updateSAFRecAmountDtls(char *);
static char * updateSAFRecForceFlag(char *);
static char * updateSAFRecCardDtls(char *);
static char * updateSAFRecAuthCode(char *);
static char * updateSAFRecLevel2Dtls(char *);
static char * updateSAFRecFSADtls(char *);
static char * updateSAFRecEMVDtls(char *);
static char * updateSAFRecPymtSubTypeDtls(char *);
static char * updateSAFRecCTroutd(char *);
static char * updateSAFRecBankUserData(char *);
static char * updateSAFRecAddPymtDtls(char *);
static int 	  updateSAFLocalApprovalCodeDtls(char *);
static char * updateSAFRecCVV2Dtls(char *);
static char * updateSAFRecReferenceDtls(char *);
static char * updateSAFRecCardHolderDtls(char *);
static char * updateSAFRecBillPayDtls(char *);
static char * updateSAFRecTransDatenTimeDtls(char *);
static char * updateSAFRecCashBackDtlsToSAFRec(char *);
static char * updateSAFRecBarCodeDtlsToSAFRec(char *);
static char * updateSAFRecPinCodeDtlsToSAFRec(char *);
static char * updateSAFAprTypeDtlsToSAFRec(char *);
static char * updateVsdEncDtlsToSAFRec(char *);
static char * updatePassThrghReqsDtlsToSAFRec(char *);
static int    updatePassThrghRespDtlstoSAFRec(char *);
static char * updateBatchTraceIDReqsDtlsToSAFRec(char *);

static int    addPymtTypeToSAFRec(char *, char *);
static int	  addSignDtlsToSAFRec(SIGDTLS_PTYPE , char *);
static int	  addSessionDtlsToSAFRec(char *, char *);
static int    addAdditionalPymtDtlsToSAFRec(char * , char *);
static int	  addSessStoreIdDtlsToSAFRec(char *, char *);
static int	  addSessLaneIdDtlsToSAFRec(char *, char *);
static int	  addTaxAmountToSAFRec(char *, char *);
static int	  addCustInfoDtlsToSAFRec(char *, char *);
static int	  addAmountDtlsToSAFRec(char *, char *);
static int	  addForceFlagToSAFRec(char *, char *);
static int	  addCardDtlsToSAFRec(char *, char *);
static int 	  addTrackIndicatorToSAFRec(char *, char *);
static int	  addEncryptionDtlsToSAFRec(char *, char *);
static int	  addCardTokenDtlsToSAFRec(char *, char *);
static int 	  addPINlessDebitCardDtlsToSAFRec(char * , char *);
static int 	  addTaxIndicatorToSAFRec(char * , char *);
static int 	  addCmdTypeToSAFRec(char * , char *);
static int 	  addAuthCodeToSAFRec(char *, char *);
static int 	  addLevel2DtlsToSAFRec(char * , char *);
static int	  addFSADtlsToSAFRec(char *, char *);
static int    addCTroutdToSAFRec(char * , char *);
static int	  addBankUserDataToSAFRec(char * , char *);
static int	  addEMVFieldsToSAFRec(char *, char *);
static int	  addPymtSubTypeDtlsToSAFRec(char *, char *);
static int	  addCVV2DtlsToSAFRec(char *, char *);
static int	  addRefernceDtlsToSAFRec(char *, char *);
static int	  addCardHolderDtlsToSAFRec(char *, char *);
static int	  addBillPayDtlsToSAFRec(char *, char *);
static int	  addTransDatenTimeDtlsToSAFRec(char *, char *);
static int	  addCashBackDtlsToSAFRec(char *, char *);
static int	  addBarCodeDtlsToSAFRec(char *, char *);
static int	  addPinCodeDtlsToSAFRec(char *, char *);
static int	  addAprtypeDtlsToSAFRec(char *, char *);
static int	  addVsdEncDtlsToSAFRec(char *, char *);
static int	  addPassThrghFieldsToSAFRec(char*, char *);
static int	  addBatchTraceIdToSAFRec(char*, char *);

static int getAllSAFTORRecords(SAFDTLS_PTYPE);
static int filterSAFRecsByTORStatus(SAFDTLS_PTYPE);
//static int clearNonTORSAFRecsFromFile(SAFDTLS_PTYPE, char*);
//static int clearNonTORSAFRecords(SAFDTLS_PTYPE);

static PAAS_BOOL	isSAFRecordsAllowedToProcNow(PAAS_BOOL *);
static void * SAFPurgeRecords(void * );
static int getRemainingTimeofDayinSecs(int *);
static char * prepareSAFRecord(int , char *, char *, char *);
//static PAAS_BOOL isSAFRecordPresent();
static int updateSAFRecord(char *, char *, int );
static int updateSAFRecordStatus(char *, int );
static int clearSAFRecsByRecNumber(char *, char *, SAFDTLS_PTYPE);
static int clearSAFRecsByRecRange(SAFDTLS_PTYPE , char *);
static int getCurrentTimeStamp(char *);
static int parseTimeStamp(char *, int *, int *, int *);
static int calcDiffbetweenDates(char *, char *);
static int getDiffbetweenDates(char *);
static int getNumberofDays(int );
static double getTransactionAmount(char *);
static int addNodeToSAFList(SAFREC_PTYPE , SAFDTLS_PTYPE );
static void * SAFProcessor(void * );
static PAAS_BOOL wasDuplicateTran(char * );
static int updateTranWithSAFDetails(char *);
static int updateSAFSSIRespDetails(char *pszTranKey, int);
static void  safTimeSigHandler(int);
static int getPassThrghRespLen();
static char* fillPassThrghRespDtlsToSAFRec(char *, PASSTHRG_FIELDS_PTYPE*);
static int getSAFErrorCodes();
static int AuthenticateSAFErrCodesFile();
static void getCorrectedReceiptText(char *);

extern int addTrantoCurrentTran(char * , int , int *);
extern int procSSIRequest(char *, char *);
extern int sendTORRequesttoSSI(char *, char *);
extern PAAS_BOOL isDHIEnabled();
extern PAAS_BOOL isVSPRegReqdAgain();
extern PAAS_BOOL isSessionInProgress();
extern PAAS_BOOL checkBinInclusion(char *);
extern void     updateHostConnectionStatus(PAAS_BOOL );
extern char* 	getDHIMID();
extern char* 	getDHITID();
extern char * 	getMerchantProcessor(int);

/*
 * ============================================================================
 * Function Name: initSAF
 *
 * Description	: This API
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int initSAF()
{
	int 	rv 			= SUCCESS;
	int		iRetVal 	= 0;
	FILE	*Fh         = NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	safPatchCode();

	/*------Update/Read the SAF data ---------*/
	iRetVal = initSAFMutex();
	if(iRetVal == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Successfully Initialized the SAF File Mutexes", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Error occurred while Initializing the SAF File Mutexes", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	// Set the Permissions of the 3 SAF Files, at startup to 0666.
	Fh = fopen(SAF_DATA_FILE_NAME_PERMANENT, "a+");
	if(Fh!=NULL)
	{
		if(chmod(SAF_DATA_FILE_NAME_PERMANENT, 0666) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		fclose(Fh);
	}

	Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "a+");
	if(Fh!=NULL)
	{
		if(chmod(SAF_COMPLETED_RECORDS_FILE_NAME, 0666) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		fclose(Fh);
	}

	Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "a+");
	if(Fh!=NULL)
	{
		if(chmod(SAF_ELIGIBLE_RECORDS_FILE_NAME, 0666) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		fclose(Fh);
	}

	/*------Update/Read the SAF data ---------*/
	iRetVal = readSAFData();

	if(iRetVal == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Successfully retrieved the SAF data", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Error occurred while retrieving the SAF data", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	/*------Create the stack for SAF transactions ---------*/
	iRetVal = createSAFTranStack();
	if(iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s:  Could not create the new SAF SSI Transaction key", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	/*------Update/Read the SAF record---------*/
	if(getSAFTotalRecords() > 0)
	{
		debug_sprintf(szDbgMsg, "%s: SAF records present..reading the first SAF record", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = readQueuedSAFRecord();

		if(rv == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Updated/Read the SAF records from the file to the data system", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else if(rv == ERR_FILE_NOT_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: SAF Record file not found", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = resetSAFDataFile();
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully reset the SAF details file to default values", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while resetting the SAF details file to default values", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			return rv;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while Updating/Reading the SAF records from the file to the data system", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return rv;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF records NOT present..", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/*-------- Create the SAF Purging Thread ---------- */
	rv = pthread_create(&ptSAFPurgingId, NULL, SAFPurgeRecords, NULL);
	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Purging thread created Successfully ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Error while creating thread for SAF purging", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/*-------- Create the Offline Transaction Processor Thread ---------- */
	rv = pthread_create(&ptOfflineTranProcId, NULL, SAFProcessor, NULL);
	if(rv == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Processor thread created Successfully ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Error while creating thread for SAF processing", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/*-------- Get Error Codes to Consider Host Not Available ---------- */
	rv = getSAFErrorCodes();
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while getting SAF Error Codes", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: safPatchCode
 *
 * Description	: This function has been added to provide backward
 * 					compatibility with the v 2.16 code. Any SAF files created
 * 					by the v 2.16 code would be moved to the new location
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int safPatchCode()
{
	int		rv				= SUCCESS;
	char	szCmd[256]		= "";
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( (SUCCESS != doesFileExist(SAF_ELIGIBLE_RECORDS_FILE_NAME)) &&
			(SUCCESS == doesFileExist(OLD_SAF_ELIGIBLE_RECS_FILE)) )
	{
		sprintf(szCmd, "mv %s %s", OLD_SAF_ELIGIBLE_RECS_FILE,
											SAF_ELIGIBLE_RECORDS_FILE_NAME);
		local_svcSystem(szCmd);
		if(chmod(SAF_ELIGIBLE_RECORDS_FILE_NAME, 0666) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	if( (SUCCESS != doesFileExist(SAF_COMPLETED_RECORDS_FILE_NAME)) &&
			(SUCCESS == doesFileExist(OLD_SAF_COMPLETED_RECS_FILE)) )
	{
		memset(szCmd, 0x00, 256);
		sprintf(szCmd, "mv %s %s", OLD_SAF_COMPLETED_RECS_FILE,
											SAF_COMPLETED_RECORDS_FILE_NAME);
		local_svcSystem(szCmd);
		if(chmod(SAF_COMPLETED_RECORDS_FILE_NAME, 0666) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	if( (SUCCESS != doesFileExist(SAF_DATA_FILE_NAME_PERMANENT)) &&
			(SUCCESS == doesFileExist(OLD_SAF_DATA_FILE_PERMANENT)) )
	{
		memset(szCmd, 0x00, 256);
		sprintf(szCmd, "mv %s %s", OLD_SAF_DATA_FILE_PERMANENT,
												SAF_DATA_FILE_NAME_PERMANENT);
		local_svcSystem(szCmd);
		if(chmod(SAF_DATA_FILE_NAME_PERMANENT, 0666) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getSAFTotalAmount
 *
 * Description	: This API access the SAF data system and returns the Total
 * 					amount in all the SAF pending records
 *
 * Input Params	: none
 *
 * Output Params: Amount/FAILURE
 * ============================================================================
 */
double getSAFTotalAmount()
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Current SAF Total Amount is %lf", __FUNCTION__, gfTotalSAFAmount);
	APP_TRACE(szDbgMsg);

	return gfTotalSAFAmount;
}

/*
 * ============================================================================
 * Function Name: getSAFTotalRecords
 *
 * Description	: This API access the SAF data system and returns the Total SAF
 *                records
 *
 * Input Params	:
 *
 * Output Params: Amount/FAILURE
 * ============================================================================
 */
int getSAFTotalRecords()
{
#ifdef DEBUG
	//char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);
	if(giTotalSAFRecords + giTotalSAFTORRecords > 0) //Log only if SAF records are present
	{
		//debug_sprintf(szDbgMsg, "%s: SAF Total Records  %d", __FUNCTION__, giTotalSAFRecords);
		//APP_TRACE(szDbgMsg);
	}

	return (giTotalSAFRecords + giTotalSAFTORRecords);
}

/*
 * ============================================================================
 * Function Name: isSAFRecordsPendingInDevice
 *
 * Description	: This API parses the SAF Records File for its size, Returns SUCCESS
 * 					if size is zero, else size of file
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
extern int isSAFRecordsPendingInDevice()
{
	int				iRetVal			= SUCCESS;
	struct stat		fileStatus;


#ifdef DEBUG
	char szDbgMsg[512]	= "";
#endif

	while(1)
	{
		iRetVal = stat(SAF_ELIGIBLE_RECORDS_FILE_NAME, &fileStatus);

		if(iRetVal != 0)
		{
			if(errno == EACCES)
			{
				debug_sprintf(szDbgMsg, "%s: Not enough search permissions for %s", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
			}
			else if (errno == ENAMETOOLONG)
			{
				debug_sprintf(szDbgMsg, "%s: Name %s is too long", __FUNCTION__,SAF_ELIGIBLE_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				/* For ENOENT/ENOTDIR/ELOOP */
				debug_sprintf(szDbgMsg,"%s: File - [%s] (Name not correct)/(file doesnt exist)", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
			}

			iRetVal = SUCCESS;
		}
		else
		{
			/* Check if the file is a regular file */
			if(S_ISREG(fileStatus.st_mode))
			{
				/* Check if the file is not empty */
				if(fileStatus.st_size <= 0)
				{
					iRetVal = SUCCESS;
					break;
				}

				iRetVal = fileStatus.st_size;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error: File [%s] is not a regular file", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);

				iRetVal = SUCCESS;
			}
		}
		break;
	}

	return iRetVal;
}

#if 0
/*
 * ============================================================================
 * Function Name: getCurrentSAFRecordNumber
 *
 * Description	: This API access the SAF data system and returns the Total
 * 					amount in all the SAF pending records
 *
 * Input Params	:
 *
 * Output Params: Amount/FAILURE
 * ============================================================================
 */
int getCurrentSAFRecordNumber()
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Current SAF Record Number is %d", __FUNCTION__, giCurrentSAFRecordNumber);
	APP_TRACE(szDbgMsg);

	return giCurrentSAFRecordNumber;
}
#endif
/*
 * ============================================================================
 * Function Name: resetSAFLastOfflineEpochTime
 *
 * Description	: This API resets the last offline time and updates the file
 *
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int resetSAFLastOfflineEpochTime()
{
	int	iRetVal  = SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(gtLastOfflineEpochTime != 0)
	{
		gtLastOfflineEpochTime = 0;
		//iRetVal = updateSAFDataToFile();
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Updated the SAF Details file after resetting SAF Last Offline Epoch Time", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//Praveen_P1: what to do here!!!!
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Value is already ZERO, no need to update", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;

}

/*
 * ============================================================================
 * Function Name: getDeviceOfflineTimeInSeconds
 *
 * Description	: This API returns the total number of Offline Seconds,
 * 				Time in seconds from the last Offline to current.
 *
 * Input Params	:
 *
 * Output Params: No of Days/FAILURE
 * ============================================================================
 */
time_t getDeviceOfflineTimeInSeconds()
{
	time_t tCurrentEpochTime  = 0;
	time_t tDiffSeconds       = 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	tCurrentEpochTime = time(NULL); // Get the Current epoch time

	debug_sprintf(szDbgMsg, "%s: Current Seconds is %ld", __FUNCTION__, tCurrentEpochTime);
	APP_TRACE(szDbgMsg);

	if(gtLastOfflineEpochTime == 0)
	{
		debug_sprintf(szDbgMsg, "%s: Last Offline time is not available, need to set current time as the lastoffline time", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		gtLastOfflineEpochTime = tCurrentEpochTime;
#if 0
		//AjayS2: 2 June 2016: redundant Code
		//iRetVal = updateSAFDataToFile();
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Updated the SAF Details file with the SAF Last Offline Epoch Time", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//Praveen_P1: what to do here!!!!
		}
#endif
		//tDiffSeconds = tCurrentEpochTime - gtLastOfflineEpochTime;
		tDiffSeconds = 0;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Last Offline time is available, need to calculate the offline time in seconds", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		tDiffSeconds = tCurrentEpochTime - gtLastOfflineEpochTime;
	}

	debug_sprintf(szDbgMsg, "%s: Returning with [%ld]", __FUNCTION__, tDiffSeconds);
	APP_TRACE(szDbgMsg);

	return tDiffSeconds;
}

/*
 * ============================================================================
 * Function Name: processTransactionOffline
 *
 * Description	: This API process the current transaction in the offline mode
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE or other error values
 * ============================================================================
 */
int processTransactionOffline(char *pszTranKey, char * pszStatMsg)
{
	int				iRetVal					= SUCCESS;
	int     		iCmd    				= -1;
	int				iFxn					= -1;
	char			szSAFRecordNumber[10]	= "";
	double			fCurrentTranAmount		= 0.0;
	char			*pszSAFRecordData       = NULL;
	static int		iBasicSize 				= 0;
	AMTDTLS_STYPE 	stAmountDtls;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pszTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input Key is NULL!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		if(iBasicSize == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Need to get the size of the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iBasicSize = getSAFRecordSize();

			debug_sprintf(szDbgMsg, "%s: Size of the SAF record is %d", __FUNCTION__, iBasicSize);
			APP_TRACE(szDbgMsg);
		}


		/*----------Step1: get the Transaction Details ----------------- */
		memset(&stAmountDtls, 0x00, sizeof(stAmountDtls));

		iRetVal = getAmtDtlsForPymtTran(pszTranKey, &stAmountDtls);

		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while getting transaction amount!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		/* Getting the current Transaction Amount*/
		fCurrentTranAmount = atof(stAmountDtls.tranAmt);

		/* get the func and cmd of the current transaction instance */
		getFxnNCmdForSSITran(pszTranKey, &iFxn, &iCmd);
		iRetVal = updateSAFDetails(pszTranKey, fCurrentTranAmount, szSAFRecordNumber);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Successfully updated the SAF Details", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iCmd == SSI_TOR)
		{
			pszSAFRecordData = prepareSAFRecord(iBasicSize, "PENDING", szSAFRecordNumber, pszTranKey);
		}
		else
		{
			pszSAFRecordData = prepareSAFRecord(iBasicSize, "ELIGIBLE", szSAFRecordNumber, pszTranKey);
		}

		if(pszSAFRecordData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while framing the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;//Praveen_P1: FIXME: check what to send from here
			break;
		}

		debug_sprintf(szDbgMsg, "%s: SAF Record is framed successfully", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/*----------Step2: Update the SAF record file ----------------------*/
		iRetVal = addSAFRecordToFile(pszSAFRecordData, iCmd);

		if(iRetVal != SUCCESS)
		{
			if(iRetVal == ERR_SAF_FILE_OPEN)
			{
				debug_sprintf(szDbgMsg, "%s: Error while opening the SAF file to add the record", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iRetVal = FAILURE; //Praveen_P1: FIXME: check what to send from here
			}
			else if(iRetVal == ERR_SAF_FILE_WRITE)
			{
				debug_sprintf(szDbgMsg, "%s: Error while writing the SAF record to the SAF file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iRetVal = FAILURE; //Praveen_P1: FIXME: check what to send from here
			}
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Successfully added SAF record to the file", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/*--------------Step3: Update the SAF data -----------------------*/

		iRetVal = updateSAFDataToFile();
		if(iRetVal != SUCCESS)
		{
			if(iRetVal == ERR_SAF_FILE_OPEN)
			{
				debug_sprintf(szDbgMsg, "%s: Error while opening the SAF Data file to add the details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//FIXME: In this case what to do for the SAFRecord file
				iRetVal = FAILURE; //Praveen_P1: FIXME: check what to send from here
			}
			else if(iRetVal == ERR_SAF_FILE_WRITE)
			{
				debug_sprintf(szDbgMsg, "%s: Error while writing the SAF Details to the SAF data file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iRetVal = FAILURE; //Praveen_P1: FIXME: check what to send from here
			}
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Successfully updated the SAF Data", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/*--------------Step4: Save the SAF related fields in payment Response-----------------------*/
		iRetVal = updateTranWithSAFDetails(pszTranKey);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully saved the SAF response in Payment data system", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while saving the SAF response in Payment data system", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//FIXME: What to do here??
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: checkResultCodeinSAFErrCodeList
 *
 * Description	: This API compares the result code with the configured SAF error codes
 * 					and returns TRUE if matching is found, ottherwise false
 *
 * Input Params	: Result Code
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ==================================================================================
 */
PAAS_BOOL checkResultCodeinSAFErrCodeList(int iResultCode)
{
	PAAS_BOOL 	rv 					= PAAS_FALSE;
	int			iAppLogEnabled		= isAppLogEnabled();
	char	 	szAppLogData[300]	= "";
	char		szResultCode[30]	= "";

#ifdef DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	/*
	 * Praveen_P1: 11 July 2016
	 * As per the FRD 3.99, Need to consider some of the RESULT CODE
	 * in SSI response as Host Offline Situation
	 * Checking if result code matches with any of the configured
	 * SAF Error Codes
	 *
	 */
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(gpszSafErrCodes == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Error codes are NOT Configured", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return rv;
	}

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szResultCode, 0x00, sizeof(szResultCode));

	debug_sprintf(szDbgMsg, "%s: Need to find if Result Code %d matches with any of the SAF Error Codes", __FUNCTION__, iResultCode);
	APP_TRACE(szDbgMsg);

	sprintf(szResultCode, "|%d|", iResultCode);

	debug_sprintf(szDbgMsg, "%s: Result Code String Look up %s", __FUNCTION__, szResultCode);
	APP_TRACE(szDbgMsg);

	if(strstr(gpszSafErrCodes, szResultCode) != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Found the Matching for Current Result Code in SAF Error Codes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Result Code Matches with Configured SAF Error Codes, Considering it as Host Offline Situation");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		rv = PAAS_TRUE;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Matching is NOT Found for Current Result Code in SAF Error Codes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = PAAS_FALSE;
	}


	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * =================================================================================
 * Function Name: addAuthCodeToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 					Auth Code details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addAuthCodeToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	FTRANDTLS_STYPE stFollowTranDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	memset(&stFollowTranDtls, 0x00, sizeof(FTRANDTLS_STYPE));

	while(1)
	{
		/*-------- Getting AUTH CODE ------------ */
		iRetVal = getFollowOnDtlsForPymt(pszTranKey, &stFollowTranDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully fetched the Authcode from the Transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while fetching the Authcode!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		/*-------- Adding AUTH CODE ------------ */
		if(strlen(stFollowTranDtls.szAuthCode) > 0)//AUTH code is available
		{
			strcat(pszSAFRecord, stFollowTranDtls.szAuthCode);
		}

		strcat(pszSAFRecord, "|");
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addCardDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 					Card details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addCardDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	CARDDTLS_STYPE  stCardDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}
	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

	while(1)
	{
		iRetVal = getCardDtlsForPymtTran(pszTranKey, &stCardDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully fetched Card Details of transaction",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the card details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		/*-------- Adding PAN Number ------------ */
		if(strlen(stCardDtls.szPAN) > 0)//PAN is available
		{
			strcat(pszSAFRecord, stCardDtls.szPAN);
		}

		/*-------- Adding Encryption Blob (if exists) ------------ */
		if(strlen(stCardDtls.szEncBlob) > 0)
		{
			/*Adding this separator / to pick up only enc blob*/
			strcat(pszSAFRecord, "#");
			strcat(pszSAFRecord, stCardDtls.szEncBlob);
		}
		strcat(pszSAFRecord, "|");

		/*-------- Adding Track2 ------------ */
		if(strlen(stCardDtls.szTrackData) > 0)//Track2 is available
		{
			strcat(pszSAFRecord, stCardDtls.szTrackData);
		}
		strcat(pszSAFRecord, "|");

		/*-------- Adding Expiry Month ------------ */
		if(strlen(stCardDtls.szExpMon) > 0)//Expiry Month is available
		{
			strcat(pszSAFRecord, stCardDtls.szExpMon);
		}
		strcat(pszSAFRecord, "|");

		/*-------- Adding Expiry Year ------------ */
		if(strlen(stCardDtls.szExpYear) > 0)//Expiry Year is available
		{
			strcat(pszSAFRecord, stCardDtls.szExpYear);
		}
		strcat(pszSAFRecord, "|");

		/*-------- Adding Payment Media ------------ */
		if(strlen(stCardDtls.szPymtMedia) > 0)//Payment Media is available
		{
			strcat(pszSAFRecord, stCardDtls.szPymtMedia);
		}
		strcat(pszSAFRecord, "|");

		/*-------- Adding Card Source ------------ */
		if(stCardDtls.iCardSrc == 77)//Card Source  is available, Ascii value for M is 77
		{
			strcat(pszSAFRecord, "M");
		}
		else if(stCardDtls.iCardSrc == 82)//Ascii value for R is 82
		{
			strcat(pszSAFRecord, "R");
		}
		else if(stCardDtls.iCardSrc == 5)//EMV Contact
		{
			strcat(pszSAFRecord, "5");
		}
		else if(stCardDtls.iCardSrc == 6)//EMV Contactless
		{
			strcat(pszSAFRecord, "6");
		}
		else if(stCardDtls.iCardSrc == 7)//EMV Fallback to Swipe
		{
			strcat(pszSAFRecord, "7");
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Card Source is %d",__FUNCTION__, stCardDtls.iCardSrc);
			APP_TRACE(szDbgMsg);

			/*
			 * TO DO Need to add for the manual entry and other sources if any
			 */
		}

		strcat(pszSAFRecord, "|");

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addEncryptionDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 					encryption details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addEncryptionDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	int     		iCmd    		  = -1;
	int				iFxn		      = -1;
	CARDDTLS_STYPE  stCardDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}
	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

	while(1)
	{
		iRetVal = getCardDtlsForPymtTran(pszTranKey, &stCardDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the card details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = FAILURE;
			break;
		}

		/* get the func and cmd of the current transaction instance */
		getFxnNCmdForSSITran(pszTranKey, &iFxn, &iCmd);


		/*-------- Adding Encryption Type ------------ */
		
		switch(stCardDtls.iEncType)
		{
		case RSA_ENC:
			strcat(pszSAFRecord, "3");
			break;

		case VSP_ENC:
			strcat(pszSAFRecord, "5");
			break;
		case VSD_ENC:
			strcat(pszSAFRecord, "1");
			break;

		default:
			if(iCmd == SSI_TOR)
			{
				strcat(pszSAFRecord, "-1");
				debug_sprintf(szDbgMsg, "%s: TOR SAFing, Updating ENC type as -1, so that we will not send ENC Type field to Host", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				strcat(pszSAFRecord, "0");
			}
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		
		strcat(pszSAFRecord, "|");

		/*-------- Adding Encryption Payload ------------ */
		if(strlen(stCardDtls.szEncPayLoad) > 0)//Encryption payload is available
		{
			strcat(pszSAFRecord, stCardDtls.szEncPayLoad);
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addCardTokenDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 					card token details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addCardTokenDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int					iRetVal	= SUCCESS;
	char*				curPtr	= NULL;
	char* 				nxtPtr	= NULL;
	FTRANDTLS_STYPE		stFTranDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}
	memset(&stFTranDtls, 0x00, sizeof(FTRANDTLS_STYPE));

	while(1)
	{
		/* Getting the following details */
		memset(&stFTranDtls, 0x00, sizeof(FTRANDTLS_STYPE));
		iRetVal = getFollowOnDtlsForPymt(pszTranKey, &stFTranDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failure while getting the follow on details from SSI tran instance!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		strcat(pszSAFRecord, "|");

		/*-------- Adding Card Token If Present------------ */
		if(strlen(stFTranDtls.szCardToken) > 0)//Card Token is available
		{
			curPtr = stFTranDtls.szCardToken;
			if( (nxtPtr = strchr(curPtr, '|')) != NULL ) //Card token doesn't contain PIPE separator between card source and id
			{
				//Replace the PIPE character from card token data by "=" separator.
				*nxtPtr = '/';
			}
			else	//PIPE character not found in the card token data.
			{
				//Do nothing.
			}
			strcat(pszSAFRecord, stFTranDtls.szCardToken);

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Card Token is not present",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addCVV2DtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the Card CVV2 details if Captured.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addCVV2DtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int					iRetVal	= SUCCESS;
	CARDDTLS_STYPE		stCardDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(1)
	{
		/* Getting the following details */
		memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
		iRetVal = getCardDtlsForPymtTran(pszTranKey, &stCardDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failure while getting the Card details from SSI tran instance!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		strcat(pszSAFRecord, "|");

		/*-------- Adding PINless Debit ------------ */
		if(strlen(stCardDtls.szCVV) > 0)//PINless debit is available
		{
			strcat(pszSAFRecord, stCardDtls.szCVV);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: CVV is not present",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addPINlessDebitCardDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 					PINless Debit Card details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addPINlessDebitCardDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	CARDDTLS_STYPE  stCardDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}
	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

	while(1)
	{
		iRetVal = getCardDtlsForPymtTran(pszTranKey, &stCardDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully fetched Card Details of transaction",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the card details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		strcat(pszSAFRecord, "|");

		/*-------- Adding PINless Debit ------------ */
		if(strlen(stCardDtls.szPINlessDebit) > 0)//PINless debit is available
		{
			strcat(pszSAFRecord, stCardDtls.szPINlessDebit);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: PIN less Debit is not present %s",__FUNCTION__, stCardDtls.szPINlessDebit);
			APP_TRACE(szDbgMsg);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addTaxIndicatorToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 				  Tax indicator details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addTaxIndicatorToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	char			szTaxInd[5]		  = "";
	LVL2_STYPE   	stLevel2Dtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	memset(&stLevel2Dtls, 0x00, sizeof(LVL2_STYPE));

	stLevel2Dtls.taxInd = -1;

	while(1)
	{
		/*-------- Getting Level2 Tax Amount Details ------------ */
		iRetVal = getLevel2DtlsForPymtTran(pszTranKey, &stLevel2Dtls);
		if( iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! while fetching the Tax Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}
		strcat(pszSAFRecord, "|");

		if( stLevel2Dtls.taxInd >= 0)
		{
			sprintf(szTaxInd, "%d", stLevel2Dtls.taxInd);
			strcat(pszSAFRecord, szTaxInd);
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addCmdTypeToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the ssi
 * 				  command name.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addCmdTypeToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	int				iCmd			  = SSI_SALE; //Default SSI command
	char 			szCmdType[21]	  = "";

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(1)
	{
		/*-------- Getting Tax Amount Details ------------ */
		iRetVal = getCmdTypeForPymtTran(pszTranKey, &iCmd);
		if( iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! while fetching the SSI command",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}
		strcat(pszSAFRecord, "|");

		sprintf(szCmdType, "%d", iCmd);
		strcat(pszSAFRecord, szCmdType);

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addTrackIndicatorToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 				  track indicator of the used card.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addTrackIndicatorToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	char 			szTrackInd[5]	  = "";
	CARDDTLS_STYPE  stCardDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}
	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

	while(1)
	{
		iRetVal = getCardDtlsForPymtTran(pszTranKey, &stCardDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully fetched Card Details of transaction",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the card details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		strcat(pszSAFRecord, "|");

		/*-------- Adding Track Indicator ------------ */
		sprintf(szTrackInd, "%d", stCardDtls.iTrkNo);
		strcat(pszSAFRecord, szTrackInd);

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addForceFlagToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 					Force Flag
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addForceFlagToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	PAAS_BOOL 		bForceFlag		  = PAAS_TRUE;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(1)
	{
		iRetVal = getForceFlagForPymtTran(pszTranKey, &bForceFlag);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully fetched Force flag from transaction instance",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Force flag ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		/*----------Adding Force Flag -----------*/
		if(bForceFlag)
		{
			strcat(pszSAFRecord, "1");
		}
		else
		{
			strcat(pszSAFRecord, "0");
		}
		strcat(pszSAFRecord, "|");

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addAmountDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 					Amount details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addAmountDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	AMTDTLS_STYPE   stAmountDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	memset(&stAmountDtls, 0x00, sizeof(AMTDTLS_STYPE));

	while(1)
	{
		/*-------- Getting Tran Amount Details ------------ */
		iRetVal = getAmtDtlsForPymtTran(pszTranKey, &stAmountDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Amount Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Amount Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		/*-------- Adding Transaction Amount ------------ */
		if(strlen(stAmountDtls.tranAmt) > 0)//Transaction Amount is available
		{
			strcat(pszSAFRecord, stAmountDtls.tranAmt);
		}
		strcat(pszSAFRecord, "|");

		//-------- Adding Tip Amount ------------
		if(strlen(stAmountDtls.tipAmt) > 0)//Tip Amount is available
		{
			strcat(pszSAFRecord, stAmountDtls.tipAmt);
		}
		strcat(pszSAFRecord, "|");

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addTaxAmountToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 					Tax amount details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addTaxAmountToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	LVL2_STYPE   	stLevel2Dtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	memset(&stLevel2Dtls, 0x00, sizeof(LVL2_STYPE));

	while(1)
	{
		/*-------- Getting Level2 Tax Amount Details ------------ */
		iRetVal = getLevel2DtlsForPymtTran(pszTranKey, &stLevel2Dtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully fetched the Tax Amount Details of Tran",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Tax amount Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		if(strlen(stLevel2Dtls.taxAmt) > 0)
		{
			strcat(pszSAFRecord, stLevel2Dtls.taxAmt);
		}
		strcat(pszSAFRecord, "|");
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addFSADtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 				  FSA details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int	addFSADtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int					iRetVal			  = SUCCESS;
	FSADTLS_STYPE   	stFSADtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	memset(&stFSADtls, 0x00, sizeof(FSADTLS_STYPE));

	while(1)
	{
		/*-------- Getting Level2 Details ------------ */
		iRetVal = getFSADtlsForPymtTran(pszTranKey, &stFSADtls);
		if( iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! while fetching the FSA Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stFSADtls.szAmtHealthCare) > 0)
		{
			strcat(pszSAFRecord, stFSADtls.szAmtHealthCare);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stFSADtls.szAmtPres) > 0)
		{
			strcat(pszSAFRecord, stFSADtls.szAmtPres);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stFSADtls.szAmtVision) > 0)
		{
			strcat(pszSAFRecord, stFSADtls.szAmtVision);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stFSADtls.szAmtClinic) > 0)
		{
			strcat(pszSAFRecord, stFSADtls.szAmtClinic);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stFSADtls.szAmtDental) > 0)
		{
			strcat(pszSAFRecord, stFSADtls.szAmtDental);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addLevel2DtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 				  level2 details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addEMVFieldsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	CARDDTLS_STYPE 	stCardDtls;
	PINDTLS_STYPE 	stPINDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
	memset(&stPINDtls, 0x00, sizeof(PINDTLS_STYPE));

	while(1)
	{
		/*-------- Getting Card Details ------------ */
		iRetVal = getCardDtlsForPymtTran(pszTranKey, &stCardDtls);
		if( iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! while fetching the card Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		iRetVal = getPINDtlsForPymtTran(pszTranKey, &stPINDtls);
		if( iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! while fetching the PIN Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stCardDtls.szEMVTags) > 0)
		{
			strcat(pszSAFRecord, stCardDtls.szEMVTags);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stCardDtls.szEMVEncBlob) > 0)
		{
			strcat(pszSAFRecord, stCardDtls.szEMVEncBlob);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stCardDtls.szEmvReversalType) > 0)
		{
			strcat(pszSAFRecord, stCardDtls.szEmvReversalType);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stPINDtls.szPIN) > 0)
		{
			strcat(pszSAFRecord, stPINDtls.szPIN);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stPINDtls.szKSN) > 0)
		{
			strcat(pszSAFRecord, stPINDtls.szKSN);
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}
/*
 * =================================================================================
 * Function Name: addLevel2DtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 				  level2 details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addLevel2DtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	LVL2_STYPE   	stLevel2Dtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	memset(&stLevel2Dtls, 0x00, sizeof(LVL2_STYPE));

	while(1)
	{
		/*-------- Getting Level2 Details ------------ */
		iRetVal = getLevel2DtlsForPymtTran(pszTranKey, &stLevel2Dtls);
		if( iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! while fetching the level2 Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stLevel2Dtls.discAmt) > 0)
		{
			strcat(pszSAFRecord, stLevel2Dtls.discAmt);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stLevel2Dtls.dutyAmt) > 0)
		{
			strcat(pszSAFRecord, stLevel2Dtls.dutyAmt);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stLevel2Dtls.freightAmt) > 0)
		{
			strcat(pszSAFRecord, stLevel2Dtls.freightAmt);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stLevel2Dtls.destCountryCode) > 0)
		{
			strcat(pszSAFRecord, stLevel2Dtls.destCountryCode);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stLevel2Dtls.destPostalcode) > 0)
		{
			strcat(pszSAFRecord, stLevel2Dtls.destPostalcode);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stLevel2Dtls.shipPostalcode) > 0)
		{
			strcat(pszSAFRecord, stLevel2Dtls.shipPostalcode);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stLevel2Dtls.prodDesc) > 0)
		{
			strcat(pszSAFRecord, stLevel2Dtls.prodDesc);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stLevel2Dtls.alternateTaxId) > 0)
		{
			strcat(pszSAFRecord, stLevel2Dtls.alternateTaxId);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stLevel2Dtls.customerCode) > 0)
		{
			strcat(pszSAFRecord, stLevel2Dtls.customerCode);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addCustInfoDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 				  customer info details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addCustInfoDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int						iRetVal			  = SUCCESS;
	CUSTINFODTLS_STYPE		stCustInfoDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	memset(&stCustInfoDtls, 0x00, sizeof(CUSTINFODTLS_STYPE));

	while(1)
	{
		/*-------- Getting Customer Info Details ------------ */
		iRetVal = getCustInfoDtlsForPymtTran(pszTranKey, &stCustInfoDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully fetched the Cust Info Details of Tran",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Cust Info Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stCustInfoDtls.szCustZipNum) > 0)
		{
			strcat(pszSAFRecord, stCustInfoDtls.szCustZipNum);
		}

		strcat(pszSAFRecord, "|");
		if(strlen(stCustInfoDtls.szCustStreetName) > 0)
		{
			strcat(pszSAFRecord, stCustInfoDtls.szCustStreetName);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addPymtTypeToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 					Payment Type.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addPymtTypeToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			 	= SUCCESS;
	int				iPaymentType    	= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}
	while(1)
	{
		/*-------- Getting Payment Type Details ------------ */
		iRetVal = getPymtTypeForPymtTran(pszTranKey, &iPaymentType);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully fetched the payment Type Detail of Transaction",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Payment Type Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Payment Type From the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}
		/*-------- Adding Payment Type ------------ */
		if(iPaymentType == PYMT_CREDIT )//Payment Type is available
		{
			strcat(pszSAFRecord, "CREDIT");
		}
		else if(iPaymentType == PYMT_PRIVATE )//Payment Type is available
		{
			strcat(pszSAFRecord, "PRIV_LBL");
		}
		else if(iPaymentType == PYMT_DEBIT)
		{
			strcat(pszSAFRecord, "DEBIT");
		}
		else if(iPaymentType == PYMT_MERCHCREDIT)
		{
			strcat(pszSAFRecord, "MERCH_CREDIT");
		}
		else if(iPaymentType == PYMT_GIFT)
		{
			strcat(pszSAFRecord, "GIFT");
		}
		else if(iPaymentType == PYMT_PAYPAL)
		{
			strcat(pszSAFRecord, "PAYPAL");
		}
		else if(iPaymentType == PYMT_EBT)
		{
			strcat(pszSAFRecord, "EBT");
		}
		else if(iPaymentType == PYMT_PAYACCOUNT)
		{
			strcat(pszSAFRecord, "PAYACCOUNT");
		}
		else if(iPaymentType == PYMT_CHECK)
		{
			strcat(pszSAFRecord, "CHECK");
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Payment type is not allowed for SAF, SAF is not allowed",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE; //TODO return appropriate value
			break;
		}
		strcat(pszSAFRecord, "|");
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addPymtSubTypeDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with Gift Payment Sub Type Details
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addPymtSubTypeDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			= SUCCESS;
	int				iPaymentSubType	= -1;
	CARDDTLS_STYPE  stCardDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}
	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

	while(1)
	{
		iRetVal = getPymtSubTypeForPymtTran(pszTranKey, &iPaymentSubType);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the card details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = FAILURE;
			break;
		}

		strcat(pszSAFRecord, "|");

		/*-------- Adding Payment Type ------------ */
		if(iPaymentSubType == PYMT_SUBTYPE_INTERNAL )//Payment Type is available
		{
			strcat(pszSAFRecord, "INTERNAL");
		}
		else if(iPaymentSubType == PYMT_SUBTYPE_EXTERNAL )//Payment Type is available
		{
			strcat(pszSAFRecord, "EXTERNAL");
		}
		else if(iPaymentSubType == PYMT_SUBTYPE_GIFTMALL)
		{
			strcat(pszSAFRecord, "GIFTMALL");
		}
		else if(iPaymentSubType == PYMT_SUBTYPE_INCOMM)
		{
			strcat(pszSAFRecord, "INCOMM");
		}
		else if(iPaymentSubType == PYMT_SUBTYPE_BLACKHAWK)
		{
			strcat(pszSAFRecord, "BLACKHAWK");
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Payment Sub Type is not present in the Transaction, So Not Storing Anything in SAF Record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addBankUserDataToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 					Bank User Data.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addBankUserDataToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			 	= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	FTRANDTLS_STYPE	stFollowOnDtls;
	char			szAppLogData[300]	= "";
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}
	while(1)
	{
		memset(&stFollowOnDtls, 0x00, sizeof(FTRANDTLS_STYPE));

		/*-------- Getting Follow-On Details ------------ */
		iRetVal = getFollowOnDtlsForPymt(pszTranKey, &stFollowOnDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully fetched the Follow on Details of Transaction",__FUNCTION__);
			APP_TRACE(szDbgMsg);

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Follow-On Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Follow-On Details From the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		strcat(pszSAFRecord, "|");

		if(strlen(stFollowOnDtls.szBankUserData) > 0)
		{
			strcat(pszSAFRecord, stFollowOnDtls.szBankUserData);
		}


		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addCTroutdToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 					ctroutd.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addCTroutdToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			 	= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	FTRANDTLS_STYPE	stFollowOnDtls;
	char			szAppLogData[300]	= "";
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}
	while(1)
	{
		memset(&stFollowOnDtls, 0x00, sizeof(FTRANDTLS_STYPE));

		/*-------- Getting Follow-On Details ------------ */
		iRetVal = getFollowOnDtlsForPymt(pszTranKey, &stFollowOnDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully fetched the Follow on Details of Transaction",__FUNCTION__);
			APP_TRACE(szDbgMsg);

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Follow-On Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Follow-On Details From the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		if(strlen(stFollowOnDtls.szCTroutd) > 0)
		{
			strcat(pszSAFRecord, stFollowOnDtls.szCTroutd);
		}

		strcat(pszSAFRecord, "|");
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addAdditionalPymtDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the addtional payment details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addAdditionalPymtDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	PYMTTRAN_PTYPE	pstPymtDtls			= NULL;
	TRAN_PTYPE		pstTran				= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(1)
	{
		/* Get the SSI transcation from the stack */
		iRetVal = getTopSSITran(pszTranKey, &pstTran);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = FAILURE;
			break;
		}

		strcat(pszSAFRecord, "|");

		/*-------- Adding PromoCode ------------ */
		if(strlen(pstPymtDtls->szPromoCode) > 0)//Promo Code is available
		{
			strcat(pszSAFRecord, pstPymtDtls->szPromoCode);
		}
		strcat(pszSAFRecord, "|");

		/*-------- Adding Order Date Time ------------ */
		if(strlen(pstPymtDtls->szOrderDtTm) > 0)//Order Date Time is available
		{
			strcat(pszSAFRecord, pstPymtDtls->szOrderDtTm);
		}
		strcat(pszSAFRecord, "|");

		/*-------- Adding Credit Plan NBR ------------ */
		if(strlen(pstPymtDtls->szCreditPlanNBR) > 0)//Credit Plan NBR is available
		{
			strcat(pszSAFRecord, pstPymtDtls->szCreditPlanNBR);
		}
		strcat(pszSAFRecord, "|");

		/*-------- Adding Purchase APR ------------ */
		if(strlen(pstPymtDtls->szPurchaseApr) > 0)//Purchase APR is available
		{
			strcat(pszSAFRecord, pstPymtDtls->szPurchaseApr);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addRefernceDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the Reference Field details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addRefernceDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			 	= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	FTRANDTLS_STYPE	stFollowOnDtls;
	char			szAppLogData[300]	= "";
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}
	while(1)
	{
		memset(&stFollowOnDtls, 0x00, sizeof(FTRANDTLS_STYPE));

		/*-------- Getting Follow-On Details ------------ */
		iRetVal = getFollowOnDtlsForPymt(pszTranKey, &stFollowOnDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully fetched the Follow on Details of Transaction",__FUNCTION__);
			APP_TRACE(szDbgMsg);

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Follow-On Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Follow-On Details From the Data System");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, NULL);
			}
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		strcat(pszSAFRecord, "|");

		if(strlen(stFollowOnDtls.szReference) > 0)
		{
			strcat(pszSAFRecord, stFollowOnDtls.szReference);
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addCardHolderDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the Card Holder Name.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addCardHolderDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	CARDDTLS_STYPE  stCardDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}
	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

	while(1)
	{
		iRetVal = getCardDtlsForPymtTran(pszTranKey, &stCardDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully fetched Card Details of transaction",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the card details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		strcat(pszSAFRecord, "|");

		/*-------- Adding Card Holder Name to the SAF Record ------------ */
		if(strlen(stCardDtls.szName) > 0)//Card Holder Name is available
		{
			strcat(pszSAFRecord, stCardDtls.szName);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addBillPayDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with Bill Pay Details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addBillPayDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	PAAS_BOOL 		bBillPay		  = PAAS_FALSE;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(1)
	{
		iRetVal = getBillPayDtlsForPymtTran(pszTranKey, &bBillPay);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully fetched Bill Pay Details from transaction instance",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Bill Pay ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		strcat(pszSAFRecord, "|");

		/*----------Adding Force Flag -----------*/
		if(bBillPay)
		{
			strcat(pszSAFRecord, "1");
		}
		else
		{
			strcat(pszSAFRecord, "0");
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addTransDatenTimeDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with date and time Details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addTransDatenTimeDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	CTRANDTLS_STYPE	stCurTranDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(1)
	{
		/* Getting the following details */
		memset(&stCurTranDtls, 0x00, sizeof(CTRANDTLS_STYPE));
		iRetVal = getCurTranDtlsForPymt(pszTranKey, &stCurTranDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failure while getting the Card details from SSI tran instance!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(strlen(stCurTranDtls.szTransDate) == 0 || strlen(stCurTranDtls.szTransTime) == 0)
		{
			getDate("YYYY.MM.DD", stCurTranDtls.szTransDate);

			getTime("HH:MM:SS", stCurTranDtls.szTransTime);

			updateCurTranDtlsForPymt(pszTranKey, &stCurTranDtls);
		}

		strcat(pszSAFRecord, "|");
		/*-------- Adding Date Details ------------ */
		strcat(pszSAFRecord, stCurTranDtls.szTransDate);

		strcat(pszSAFRecord, "|");
		/*-------- Adding Time Details ------------ */
		strcat(pszSAFRecord, stCurTranDtls.szTransTime);

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addCashBackDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with Cashback Amount Details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addCashBackDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			= SUCCESS;
	AMTDTLS_STYPE	stAmountDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(1)
	{
		memset(&stAmountDtls, 0x00, sizeof(AMTDTLS_STYPE));
		iRetVal = getAmtDtlsForPymtTran(pszTranKey, &stAmountDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Amount details!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		strcat(pszSAFRecord, "|");

		/*-------- Adding Cashback Amount ------------ */
		if(strlen(stAmountDtls.cashBackAmt) > 0)//CashBack Amount is available
		{
			strcat(pszSAFRecord, stAmountDtls.cashBackAmt);
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addBarCodeDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with BarCode Details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addBarCodeDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int					iRetVal	= SUCCESS;
	CARDDTLS_STYPE		stCardDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(1)
	{
		/* Getting the following details */
		memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
		iRetVal = getCardDtlsForPymtTran(pszTranKey, &stCardDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failure while getting the Card details from SSI tran instance!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		strcat(pszSAFRecord, "|");

		/*-------- Adding BarCode ------------ */
		if(strlen(stCardDtls.szBarCode) > 0)//BarCode is available
		{
			strcat(pszSAFRecord, stCardDtls.szBarCode);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: BarCode is not present",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addPinCodeDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with PinCode Details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addPinCodeDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int					iRetVal	= SUCCESS;
	CARDDTLS_STYPE		stCardDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(1)
	{
		/* Getting the following details */
		memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
		iRetVal = getCardDtlsForPymtTran(pszTranKey, &stCardDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failure while getting the Card details from SSI tran instance!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		strcat(pszSAFRecord, "|");

		/*-------- Adding PINCode Details ------------ */
		if(strlen(stCardDtls.szPINCode) > 0)//PINCode is available
		{
			strcat(pszSAFRecord, stCardDtls.szPINCode);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: PINCode is not present",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addAprtypeDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with PinCode Details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addAprtypeDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int					iRetVal		= SUCCESS;
	PYMTTRAN_PTYPE		pstPymtDtls	= NULL;
	TRAN_PTYPE			pstTran		= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(1)
	{
		/* Get the SSI transcation from the stack */
		iRetVal = getTopSSITran(pszTranKey, &pstTran);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = FAILURE;
			break;
		}
		strcat(pszSAFRecord, "|");

		if(strlen(pstPymtDtls->szAPRType) > 0)//APR Type is available
		{
			strcat(pszSAFRecord, pstPymtDtls->szAPRType);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Apr Type is not present",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addVsdEncDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with Vsd Enc Details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addVsdEncDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int					iRetVal		= SUCCESS;
	PYMTTRAN_PTYPE		pstPymtDtls	= NULL;
	TRAN_PTYPE			pstTran		= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(1)
	{
		/* Get the SSI transcation from the stack */
		iRetVal = getTopSSITran(pszTranKey, &pstTran);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = FAILURE;
			break;
		}
		strcat(pszSAFRecord, "|");

		if(strlen(pstPymtDtls->pstCardDtls->szVSDInitVector) > 0)//Init Vector is available
		{
			strcat(pszSAFRecord, pstPymtDtls->pstCardDtls->szVSDInitVector);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Init Vector is not present",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}
/*
 * =================================================================================
 * Function Name: addPassThrghFieldsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the Pass through fields Details.
 *
 * Input Params	: SAFRecord buffer to be filled
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addPassThrghFieldsToSAFRec(char *pszTranKey, char *pszSAFRecord)
{
	int						iRetVal				= SUCCESS;
	int						iCnt				= 0;
	PASSTHRG_FIELDS_PTYPE	pstPassThrghDtls	= NULL;
	PYMTTRAN_PTYPE			pstPymtDtls			= NULL;
	TRAN_PTYPE				pstTran				= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(1)
	{
		/* Get the SSI transcation from the stack */
		iRetVal = getTopSSITran(pszTranKey, &pstTran);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = FAILURE;
			break;
		}
		pstPassThrghDtls = pstPymtDtls->pstPassThrghDtls;

		strcat(pszSAFRecord, "|");

		if(pstPassThrghDtls != NULL && pstPassThrghDtls->pstReqTagList != NULL && pstPassThrghDtls->iReqTagsCnt > 0)
		{
			for(iCnt = 0; iCnt < pstPassThrghDtls->iReqTagsCnt; iCnt++)
			{
				if(pstPassThrghDtls->pstReqTagList[iCnt].value != NULL)
				{
					strcat(pszSAFRecord, pstPassThrghDtls->pstReqTagList[iCnt].key);
					strcat(pszSAFRecord, "=");
					if(pstPassThrghDtls->pstReqTagList[iCnt].valType == BOOLEAN)
					{
						if(*(int*)pstPassThrghDtls->pstReqTagList[iCnt].value == PAAS_TRUE)
						{
							strcat(pszSAFRecord, "TRUE");
						}
						else
						{
							strcat(pszSAFRecord, "FALSE");
						}
					}
					else
					{
						strcat(pszSAFRecord, (char*)pstPassThrghDtls->pstReqTagList[iCnt].value);
					}

					strcat(pszSAFRecord, ",");
				}
			}
			// remove last , sysmbol from pszSAFRecord, if present.
			if(pszSAFRecord[strlen(pszSAFRecord) - 1] == ',')
			{
				pszSAFRecord[strlen(pszSAFRecord) - 1] = '\0';
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Pass through fields are not present",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addBatchTraceIdToSAFRec
 *
 * Description	: This API fills the Batch Trace ID in SAF Rec for TOR SAF records Only
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addBatchTraceIdToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int		iRetVal			= SUCCESS;
	int     iCmd    		= -1;
	int		iFxn			= -1;
	char	szBatchTraceId[50+1]	= "";
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	/* get the func and cmd of the current transaction instance */
	getFxnNCmdForSSITran(pszTranKey, &iFxn, &iCmd);

	while(1)
	{
		strcat(pszSAFRecord, "|");

		/*-------- Adding Batch Trace Id ------------ */
		if(iCmd == SSI_TOR)
		{
			memset(szBatchTraceId, 0x00, sizeof(szBatchTraceId));
			/*get the Batch Trace Id */
			if(getBatchTraceIdDtlsForPymtTran(pszTranKey, szBatchTraceId) == SUCCESS)
			{
				strcat(pszSAFRecord, szBatchTraceId);
				debug_sprintf(szDbgMsg, "%s: TOR Command: Batch Trace Added",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: TOR Command: Batch Trace ID missing",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Not a TOR Command",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addSessionDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the session details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addSessionDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	SESSDTLS_STYPE  stSessionDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	memset(&stSessionDtls, 0x00, sizeof(SESSDTLS_STYPE));

	while(1)
	{
		/*-------- Getting Session Details ------------ */

		iRetVal = getSessDtlsForPymtTran(pszTranKey, &stSessionDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Session Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Session Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_SESSDTLS_NOT_FOUND;
			break;
		}

		/*-------- Adding Invoice ------------ */
		if(strlen(stSessionDtls.szInvoice) > 0)//Invoice is available
		{
			strcat(pszSAFRecord, stSessionDtls.szInvoice);
		}
		strcat(pszSAFRecord, "|");

		/*-------- Adding Purchase ID ------------ */
		if(strlen(stSessionDtls.szPurchaseId) > 0)//PurchaseID is available
		{
			strcat(pszSAFRecord, stSessionDtls.szPurchaseId);
		}
		strcat(pszSAFRecord, "|");

		/*-------- Adding Cashier ID ------------ */
		if(strlen(stSessionDtls.szCashierId) > 0)//Cashier is available
		{
			strcat(pszSAFRecord, stSessionDtls.szCashierId);
		}
		strcat(pszSAFRecord, "|");
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addSessStoreIdDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the session details(store id).
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addSessStoreIdDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	SESSDTLS_STYPE  stSessionDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	memset(&stSessionDtls, 0x00, sizeof(SESSDTLS_STYPE));

	while(1)
	{
		/*-------- Getting Session Details ------------ */
		iRetVal = getSessDtlsForPymtTran(pszTranKey, &stSessionDtls);
		if( iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Session Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_SESSDTLS_NOT_FOUND;
			break;
		}

		strcat(pszSAFRecord, "|");
		/*-------- Adding store Id ------------ */
		if(strlen(stSessionDtls.szStoreNo) > 0)//Store Id is available
		{
			strcat(pszSAFRecord, stSessionDtls.szStoreNo);
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addSessLaneIdDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the session details(lane id).
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addSessLaneIdDtlsToSAFRec(char * pszTranKey, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
	SESSDTLS_STYPE  stSessionDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	memset(&stSessionDtls, 0x00, sizeof(SESSDTLS_STYPE));

	while(1)
	{
		/*-------- Getting Session Details ------------ */
		iRetVal = getSessDtlsForPymtTran(pszTranKey, &stSessionDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Session Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Session Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_SESSDTLS_NOT_FOUND;
			break;
		}

		strcat(pszSAFRecord, "|");
		/*-------- Adding lane Id ------------ */
		if(strlen(stSessionDtls.szLaneNo) > 0)//Lane Id is available
		{
			strcat(pszSAFRecord, stSessionDtls.szLaneNo);
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * =================================================================================
 * Function Name: addSignDtlsToSAFRec
 *
 * Description	: This API fills the SAF record buffer with the
 * 					Signature details.
 *
 * Input Params	: SAFRecord buffer to be filled, Transaction key to fetch the details
 *
 * Output Params: SUCCESS/FAILURE
 * ==================================================================================
 */
static int addSignDtlsToSAFRec(SIGDTLS_PTYPE pstSignDtls, char *pszSAFRecord)
{
	int				iRetVal			  = SUCCESS;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSignDtls == NULL || pszSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Passed Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(1)
	{
		if(pstSignDtls->szSign != NULL && (strlen(pstSignDtls->szSign) > 0)) //Signature Details are available
		{
			/*-------- Adding MIME type ------------ */
			strcat(pszSAFRecord, pstSignDtls->szMime);
			strcat(pszSAFRecord, "|");

			/*-------- Adding Signature ID ------------ */
			strcat(pszSAFRecord, pstSignDtls->szSign);
		}
		else
		{
			strcat(pszSAFRecord, "|"); //One for the Signature Type
		}
		strcat(pszSAFRecord, "|"); //One for the Signature Data
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: prepareSAFRecord
 *
 * Description	: This API fills the SAF record buffer with all the required
 * 					session, transaction details.
 *
 * Input Params	: SAFRecord buffer to be filled, SAF Status and SAF Record no
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static char * prepareSAFRecord(int iSize, char *pszSAFStatus, char *pszSAFRecordNum, char *pszTranKey)
{
	int				iRetVal			 	= SUCCESS;
	static int		iPreviousSize	 	= 0;
	char			szBusinessDate[9]	= "";
	char			*pchTemp 		 	= NULL;
	static char		*pszSAFRecord     	= NULL;
	char			*pszTempSAFRecord 	= NULL;
	SIGDTLS_STYPE    stSignatureDtls;
#ifdef DEVDEBUG
	char			szDbgMsg[4096]	= "";
#elif DEBUG
	char			szDbgMsg[1024]	= "";
#endif


	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTranKey == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter i.e. Tran Key is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Given Parameters: Size[%d], SAFStatus[%s], SAFRecordNum[%s]", __FUNCTION__, iSize, pszSAFStatus, pszSAFRecordNum);
	APP_TRACE(szDbgMsg);

	//Note: Please don't forget to update the macro(i.e "SAF_NUM_RECORD_SEPARATOR") if you increase the number of separator in the saf record.
	iSize = iSize + SAF_NUM_RECORD_SEPARATOR /*For the field separator i.e. |*/ + 10 /* For the length */ + 2 /* 1 for ; and 1 for LF*/;

	memset(&stSignatureDtls, 0x00, sizeof(SIGDTLS_STYPE));

	iRetVal = getSigDtlsForPymtTran(pszTranKey, &stSignatureDtls);
	if(iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failure while fetching Signature details of Transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return NULL;
	}

	if(stSignatureDtls.szSign != NULL && (strlen(stSignatureDtls.szSign) > 0))
	{
		debug_sprintf(szDbgMsg, "%s: Signature data is available..adding the length of the sig data to the size", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		iSize = iSize + strlen(stSignatureDtls.szSign) ;
	}

	debug_sprintf(szDbgMsg, "%s: Size is %d", __FUNCTION__, iSize);
	APP_TRACE(szDbgMsg);

	if(iPreviousSize < iSize)
	{
		debug_sprintf(szDbgMsg, "%s: (Re)allocating the memory for the SAF record", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchTemp = (char *)realloc(pszSAFRecord, iSize * sizeof(char));
		if(pchTemp == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while reallocating memory for the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return NULL;
		}
		memset(pchTemp, 0x00, iSize);
		pszSAFRecord = pchTemp;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: (Re)allocation of the memory for the SAF record is not required", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		memset(pszSAFRecord, 0x00, iSize);
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT|BAR_CODE|PIN_CODE|APR_TYPE|INIT_VECTOR|
	 * PASS_THROUGH_FIELD1=FIELD1_VALUE,PASS_THROUGH_FIELD2=FIELD2_VALUE,...PASS_THROUGH_FIELDn=FIELDn_VALUE||||;
	 *
	 */
	sprintf(pszSAFRecord, "%s%c%s%c", pszSAFStatus, PIPE, pszSAFRecordNum, PIPE);

	debug_sprintf(szDbgMsg, "%s: SAF Record after adding Status [%s]", __FUNCTION__, pszSAFRecord);
	APP_TRACE(szDbgMsg);
	while(1)
	{
		/*-------- Adding AUTH CODE ------------ */
		iRetVal = addAuthCodeToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Authcode(if present) to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Authcode(if present) to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND; //Praveen_P1: For now returning with this value
			break;
		}

		/*-------- Adding Card Details ------------ */
		iRetVal = addCardDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Card Details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Card Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND; //Praveen_P1: For now returning with this value
			break;
		}

#ifdef DEVDEBUG
		if(strlen(pszSAFRecord) < 4000)
		{
			debug_sprintf(szDbgMsg, "%s:%s: SAF Record after adding Card Details [%s]", DEVDEBUGMSG, __FUNCTION__, pszSAFRecord);
			APP_TRACE(szDbgMsg);
		}
#endif

		/*-------- Adding Force Flag ------------ */
		iRetVal = addForceFlagToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Force Flag to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Force Flag to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND; //Praveen_P1: For now returning with this value
			break;
		}

		/*-------- Adding Amount Details ------------ */
		iRetVal = addAmountDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Amount Details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Amount Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND; //Praveen_P1: For now returning with this value
			break;
		}

		/*-------- Adding Tax Amount Details ------------ */
		iRetVal = addTaxAmountToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Tax Amount Details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Tax Amount Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND; //Praveen_P1: For now returning with this value
			break;
		}

#ifdef DEVDEBUG
		if(strlen(pszSAFRecord) < 4000)
		{
			debug_sprintf(szDbgMsg, "%s:%s: SAF Record after adding Amount Details [%s]", DEVDEBUGMSG, __FUNCTION__, pszSAFRecord);
			APP_TRACE(szDbgMsg);
		}
#endif

		/*-------- Adding Payment Details ------------ */
		iRetVal = addPymtTypeToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Payment Type to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Payment Type to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND; //Praveen_P1: For now returning with this value
			break;
		}

		/*-------- Adding Session Details ------------ */
		iRetVal = addSessionDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Session Details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Session Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND; //Praveen_P1: For now returning with this value
			break;
		}

#ifdef DEVDEBUG
		if(strlen(pszSAFRecord) < 4000)
		{
			debug_sprintf(szDbgMsg, "%s:%s: SAF Record after adding Session Details [%s]", DEVDEBUGMSG, __FUNCTION__, pszSAFRecord);
			APP_TRACE(szDbgMsg);
		}
#endif

		/*-------- Adding Session Details ------------ */
		iRetVal = addSignDtlsToSAFRec(&stSignatureDtls, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Signature Details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Signature Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND; //Praveen_P1: For now returning with this value
			break;
		}

		/*-------- Adding Place holders for Resp Code, CTroutd and Troud Details ------------ */

		strcat(pszSAFRecord, "|"); //Place holder for Response Code

		iRetVal = addCTroutdToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added CTroutd to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Ctroutd to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND; //Praveen_P1: For now returning with this value
			break;
		}

		strcat(pszSAFRecord, "|"); //Place holder for Troutd

		strcat(pszSAFRecord, "|"); //Place holder for Auth code


		/*-------- Adding Encryption Details ------------ */
		iRetVal = addEncryptionDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Encryption Details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Encryption Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND; //Praveen_P1: For now returning with this value
			break;
		}

		/*-------- Adding Card Token If Its received from POS ------------ */
		iRetVal = addCardTokenDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully added card token to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding card token to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		/*-------- Adding CVV2 Details If Captured ------------ */
		iRetVal = addCVV2DtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully added CVV2 to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding CVV2 to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		/*-------- Adding Bank User Data ------------ */
		iRetVal = addBankUserDataToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Bank User Data to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Bank User Data to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND; //Praveen_P1: For now returning with this value
			break;
		}

		strcat(pszSAFRecord, "|"); //Place holder for Business Date

		/*-------- Adding Business Date ------------ */
		memset(szBusinessDate, 0x00, sizeof(szBusinessDate));
		getBusinessDate(szBusinessDate);
		if( strlen(szBusinessDate) > 0 )
		{
			debug_sprintf(szDbgMsg, "%s: Adding Business date[%s] to the SAF Record", __FUNCTION__, szBusinessDate);
			APP_TRACE(szDbgMsg);

			strcat(pszSAFRecord, szBusinessDate);
		}

		/*-------- Adding PINless Debit Details ------------ */
		iRetVal = addPINlessDebitCardDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added PINless Debit field to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding PINless Debit field to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND; //Praveen_P1: For now returning with this value
			break;
		}

		/*-------- Adding Place holders for Txn POS Entry Code, Merchant ID, Terminal ID------------ */
		strcat(pszSAFRecord, "|||"); //These values will be present only after posting to host and if host sends in resp.

		/*-------- Adding Session Details(LANE) ------------ */
		iRetVal = addSessLaneIdDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Lane Id Details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Lane Id Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND; //Praveen_P1: For now returning with this value
			break;
		}

		/*-------- Adding Place holders for APPROVED_AMOUNT------------ */
		strcat(pszSAFRecord, "|");

		/*-------- Adding Tax Indicator details ------------ */
		iRetVal = addTaxIndicatorToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully added tax indicator field to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding tax indicator field to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		/*-------- Adding command ------------ */
		iRetVal = addCmdTypeToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully added SSI command field to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding SSI command field to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		/*-------- Adding command ------------ */
		iRetVal = addTrackIndicatorToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully added track indicator field to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding track indicator field to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		/*-------- Adding Session Details(STORE_ID) ------------ */
		iRetVal = addSessStoreIdDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Tax Amount Details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Tax Amount Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		/*-------- Adding Customer Info Details ------------ */
		iRetVal = addCustInfoDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Cust Info Details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Cust Info Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		/*-------- Adding Level2 Details ------------ */
		iRetVal = addLevel2DtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Level2 Details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Level2 Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		/*-------- Adding FSA details ------------ */
		iRetVal = addFSADtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully added FSA Details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding FSA Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		/*-------- Adding EMV Details ------------ */
		iRetVal = addEMVFieldsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added EMV Details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Level2 Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		iRetVal = addPymtSubTypeDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Payment Sub Type Details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Payment Sub Type Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		iRetVal = addAdditionalPymtDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added additional payment details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding additional payment Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		iRetVal = addRefernceDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Reference Field details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Reference Field Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		iRetVal = addCardHolderDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added CardHolder Field details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding CardHolder Field Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		iRetVal = addBillPayDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Bill Pay Field details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Bill Pay Field Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		iRetVal = addTransDatenTimeDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Date and Time Field details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Date and Time Field Details to the SAF record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		iRetVal = addCashBackDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added CashBack details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Cash Back Field Details to the SAF Record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		iRetVal = addBarCodeDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Bar Code details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Bar Code Field Details to the SAF Record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		iRetVal = addPinCodeDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Pin Code details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Pin Code Field Details to the SAF Record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		iRetVal = addAprtypeDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Apr Type details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Apr Type Field Details to the SAF Record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		iRetVal = addVsdEncDtlsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added VSD Encryption details to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding VSD Encryption Details to the SAF Record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		iRetVal = addPassThrghFieldsToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Pass Through Fields to the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Pass Through Fields to the SAF Record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		iRetVal = addBatchTraceIdToSAFRec(pszTranKey, pszSAFRecord);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Added Batch Trace ID to the SAF record, if TOR SAF", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while adding Batch Trace ID to the SAF Record!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = ERR_SAF_PYMTDTLS_NOT_FOUND;
			break;
		}

		//ADD SAF RECORDS: Add new field here

		//Place holders for Future Use
		strcat(pszSAFRecord, "||||");

		/* MukeshS3: 13/10/16:
		 * If there are any <LF> characters present in the SAF record, these must be removed before writing the records in the file.
		 * These characters can be expected from POS with any XML field becuase application doesn't strip the POS requests for any whitespace or new line character.
		 * So, it is always safe to remove any <LF> characters present in between.
		 */
		strReplaceAll(pszSAFRecord, "\n", "");

		pszTempSAFRecord = (char *)malloc(sizeof(char) * (strlen(pszSAFRecord) + 10) /* For the length */);
		if(pszTempSAFRecord == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while allocating memory for the Temp SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return NULL;
		}

		sprintf(pszTempSAFRecord, "%d%c%s%c%c", strlen(pszSAFRecord) + 1 /* For the ;*/ + 1 /*For the <LF>*/, PIPE, pszSAFRecord, SEMI_COLON, LF /*New Line Field*/);
		memset(pszSAFRecord, 0x00, iSize);
		memcpy(pszSAFRecord, pszTempSAFRecord, strlen(pszTempSAFRecord));
		free(pszTempSAFRecord); //Freeing temp SAF record

		break;

	}

	iPreviousSize = iSize; //Assigning to preserve for the next call

	if(stSignatureDtls.szSign != NULL)//Need to free the sign details
	{
		free(stSignatureDtls.szSign);
	}

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pszSAFRecord;
}

/*
 * ============================================================================
 * Function Name: getSAFRecordSize
 *
 * Description	: This API calculates the size of the SAF record (excludes
 * 					signature data) system at the startup
 *
 * Input Params	: NONE
 *
 * Output Params: Size of the SAF record
 * ============================================================================
 */
int getSAFRecordSize()
{
	int 				iRecordSize 	= 0;
	SESSDTLS_STYPE		stSessionDtls;
	CARDDTLS_STYPE		stCardDtls;
	AMTDTLS_STYPE 		stAmtDtls;
	CTRANDTLS_STYPE		stCTranDtls;
	FTRANDTLS_STYPE		stFollowOnDtls;
	LVL2_STYPE			stLevel2Dtls;
	SIGDTLS_STYPE		stSignDtls;
	FSADTLS_STYPE		stFSADtls;
	PINDTLS_STYPE		stPINDtls;
	PYMTTRAN_STYPE		stPymtDtls;
	FTRANDTLS_STYPE 	stFollowTranDtls;
	CUSTINFODTLS_STYPE	stCustInfoDtls;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//Calculating for Current Tran dtls:
	iRecordSize = sizeof(stCTranDtls.szAuthCode) + sizeof(stCTranDtls.szBankUserData) +
				  sizeof(stCTranDtls.szTxnPosEntryMode) + sizeof(stCTranDtls.szMerchId)+
				  sizeof(stCTranDtls.szTermId) + sizeof(stCTranDtls.szAuthRespCode) + sizeof(stCTranDtls.szStoreId);

	//Calculate for follow on details.
	iRecordSize = iRecordSize + sizeof(stFollowOnDtls.szCardToken)  + sizeof(stFollowTranDtls.szReference) + sizeof(stFollowTranDtls.szCTroutd) + sizeof(stFollowTranDtls.szTroutd);

	//Calculating for Card Details
	iRecordSize = iRecordSize + sizeof(stCardDtls.szPAN) + sizeof(stCardDtls.szTrackData) + sizeof(stCardDtls.szExpMon) + sizeof(stCardDtls.szExpYear)
				  + sizeof(stCardDtls.szPymtMedia) + sizeof(stCardDtls.szEncPayLoad)+ sizeof(int) /* Encryption Type */
				  + sizeof(char) /*one char for card source*/ + sizeof(stCardDtls.szCVVCode) + sizeof(stCardDtls.szName) +  sizeof(stCardDtls.szEMVTags) + sizeof(stCardDtls.szEncBlob)
				  + sizeof(stCardDtls.szEmvReversalType) + sizeof(stCardDtls.szPINlessDebit) + sizeof(stCardDtls.szBarCode) + sizeof(stCardDtls.szPINCode);

	//Calculating for Force flag
	iRecordSize = iRecordSize + sizeof(char) /*one char for force flag 1 or 0 */;

	//Calculating for Encryption Type
	iRecordSize = iRecordSize + sizeof(char) /*one char for Encryption Type 0 or 3 or 5 */;

	//Calculating for Amount details
	iRecordSize = iRecordSize + sizeof(stAmtDtls.tipAmt) + sizeof(stAmtDtls.tranAmt) + sizeof(stAmtDtls.apprvdAmt) + sizeof(stAmtDtls.cashBackAmt) + sizeof(stAmtDtls.apprvdAmt);

	//Calculating for tax amount details
	iRecordSize = iRecordSize + sizeof(stLevel2Dtls);

	//Calculating for command type details
	iRecordSize = iRecordSize + sizeof(int);

	//Calculating for Bill Pay Field details
	iRecordSize = iRecordSize + sizeof(int);

	//Calculating for track indicator details
	iRecordSize = iRecordSize + sizeof(int);

	//Calculating for payment type
	iRecordSize = iRecordSize + (15 * sizeof(char)) /*Payment type CREDIT, PRIV_LBL */;

	//Calculating for session/transaction details
	iRecordSize = iRecordSize + sizeof(stSessionDtls.szCashierId) + sizeof(stSessionDtls.szInvoice) + sizeof(stSessionDtls.szPurchaseId)
							  + sizeof(stSessionDtls.szBusinessDate) +  sizeof(stSessionDtls.szLaneNo) + sizeof(stSessionDtls.szStoreNo);

	//Calculating for additional Payment Details
	iRecordSize = iRecordSize + sizeof(stPymtDtls.szPromoCode) + sizeof(stPymtDtls.szPurchaseApr)+ sizeof(stPymtDtls.szTranDt)
								+ sizeof(stPymtDtls.szTranTm) + sizeof(stPymtDtls.szOrigTranDt) + sizeof(stPymtDtls.szOrigTranTm)  + sizeof(stPymtDtls.iPymtSubType);

	//Calculating for sign details
	iRecordSize = iRecordSize + sizeof(stSignDtls.szMime);

	//Calculating for customer details
	iRecordSize = iRecordSize + sizeof(stCustInfoDtls.szCustZipNum) + sizeof(stCustInfoDtls.szCustStreetName);

	//Calculating for FSA details
	iRecordSize = iRecordSize + sizeof(stFSADtls);

	//Calculating for PIN details
	iRecordSize = iRecordSize + sizeof(stPINDtls);

	iRecordSize = iRecordSize + sizeof(stCTranDtls.szSAFNo) + (20 * sizeof(char)) /* for the SAF status */;

	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRecordSize);
	APP_TRACE(szDbgMsg);

	return iRecordSize;
}

/*
 * ============================================================================
 * Function Name: initSAFMutex
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int initSAFMutex()
{
	int	iRetVal	= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pthread_mutex_init(&gptSAFDetailsFileMutex, NULL))//Initialize the SAF Details File Mutex
	{
		debug_sprintf(szDbgMsg, "%s - Failed to create SAF Details File mutex!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(pthread_mutex_init(&gptSAFRecordFileMutex, NULL))//Initialize the SAF Records File Mutex
	{
		debug_sprintf(szDbgMsg, "%s - Failed to create SAF Records File mutex!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(pthread_mutex_init(&gptSAFTranKeyMutex, NULL))//Initialize the SAF Transaction Key Mutex
	{
		debug_sprintf(szDbgMsg, "%s - Failed to create SAF Records File mutex!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(pthread_mutex_init(&gptSAFRecordInProgressMutex, NULL))//Initialize the SAF Records In Progress Mutex
	{
		debug_sprintf(szDbgMsg, "%s - Failed to create SAF Records In Progress mutex!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: readQueuedSAFRecord
 *
 * Description	: This API stores the SAF records stored in the file to the data
 * 				  system at the startup
 *
 * Input Params	: NONE
 *
 * Output Params: SAF number stored
 * ============================================================================
 */
int readQueuedSAFRecord()
{
	FILE *Fh;
	int			iRetVal                = SUCCESS;
	int			iRecordLen   	       = 0;
	static int  iPreviousLen       	   = 0;
	int			iIndex       	       = 0;
	char		*pchTemp		       = NULL;
	static char	*pszSAFRecordData 	   = NULL;
	int			 iSAFRecordStatus      = 0;
	int			 iSAFRecordPresent     = 0;

	//char	szTempBuf[50]	 = "";
#ifdef DEBUG
	char	szDbgMsg[4096]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Total Number of SAF + SAF TOR records %d", __FUNCTION__, giTotalSAFRecords + giTotalSAFTORRecords);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
	if(Fh==NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
		return ERR_FILE_NOT_FOUND;
	}

	for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
	{
		debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iRecordLen = getRecordLength(Fh);

		debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d, ", __FUNCTION__, iIndex, iRecordLen);
		APP_TRACE(szDbgMsg);

		if(iRecordLen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No more records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		if(iPreviousLen < iRecordLen+1)
		{
			debug_sprintf(szDbgMsg, "%s: (Re)Allocating memory for the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//Allocate the memory to hold the SAF record
			pchTemp = (char *)realloc(pszSAFRecordData, iRecordLen + 10); //1 for <LF>, 1 for buffer
			if(pchTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			else
			{
				memset(pchTemp, 0x00, iRecordLen + 10);
				pszSAFRecordData = pchTemp;
			}

			iPreviousLen = iRecordLen + 10;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Not (Re)Allocating memory for the SAF record, its available already", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(pszSAFRecordData, 0x00, iPreviousLen);
		}

		if( fgets(pszSAFRecordData, iRecordLen+10, Fh) == NULL)
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record %d from the file", __FUNCTION__, iIndex);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
		}

		if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iIndex = iIndex -1; //Time Stmap lines are not part of the Total number of records, need to read more
			continue;
		}

		debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iSAFRecordStatus = getSAFRecordStatus(pszSAFRecordData);

		if(!(iSAFRecordStatus == SAF_STATUS_QUEUED || iSAFRecordStatus == SAF_STATUS_INPROCESS || SAF_TOR_STATUS_PENDING))
		{
			debug_sprintf(szDbgMsg, "%s: SAF record status is not ELIGIBLE/IN_PROCESS/PENDING", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = SUCCESS;
			continue; //Need to go to the next record if it exists
		}

		debug_sprintf(szDbgMsg, "%s:  %d SAF record is ELIGIBLE or IN_PROCESS or PENDING", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		// SAF record is present so need to create new SAF transaction instance
		iRetVal = pushNewSSITran(szSSITranKey);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s:Error while pushing the SAF SSI transaction to stack", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		iRetVal = parseSAFRecordLine(pszSAFRecordData);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s:  Successfully parsed the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iSAFRecordPresent = 1;        //SAF record is present
			giSAFRecordSerialNum = iIndex; //Updating the SAF record serial number
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while parsing the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//Since parsing of the SAF record is failure, we will delete the SAF Tran key
			iRetVal = popSSITran(szSSITranKey);
			if(iRetVal != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s:Error while popping the SAF SSI transaction from stack", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iRetVal = FAILURE;
				break;
			}
			deleteStack(szSSITranKey);
			memset(szSSITranKey, 0x00, sizeof(szSSITranKey));
			iRetVal = FAILURE;
			//Praveen_P1: FIXME: What to do here?? Should not come here...
		}

		break; //At present one record will be stored in the memory, after storing the first QUEUED SAF record breaking from the loop
	}

	if(Fh != NULL)
	{
		fclose(Fh); //Closing the file
	}
	//MukeshS3: 7-Nov-16: The file lock must be released after changing the IN_PROCESS status in SAF records file(If required)
	//releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(iSAFRecordPresent)
	{
		debug_sprintf(szDbgMsg, "%s: Found the SAF Record with Status ELIGIBLE or IN_PROCESS or PENDING", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iSAFRecordStatus == SAF_STATUS_QUEUED)
		{
			debug_sprintf(szDbgMsg, "%s: Need to change the SAF record status from ELIGIBLE to IN_PROCESS", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = updateSAFRecordStatus("IN_PROCESS", 0);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated the SAF record status to IN_PROCESS from ELIGIBLE", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating the SAF record status to IN_PROCESS from ELIGIBLE", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//Praveen_P1: What to do here??
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: The status is already IN_PROCESS or PENDING, no need to change", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: NO SAF Record with Status ELIGIBLE/IN_PROCESS/PENDING", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}


/*
 * ============================================================================
 * Function Name: updateSAFDetails
 *
 * Description	: This API update the SAF variables
 *
 * Input Params	: Current Transaction Amount, Buffer to be filled with Current
 * 					Record Number
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateSAFDetails(char *pszTranKey, double fCurrentTranAmount, char *pszSAFRecordNumber)
{
	int		iRetVal			= SUCCESS;
	int     iCmd    		= -1;
	int		iFxn			= -1;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRecordNumber == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Parameter is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	/* get the func and cmd of the current transaction instance */
	getFxnNCmdForSSITran(pszTranKey, &iFxn, &iCmd);

	giCurrentSAFTranSeqNum   = ( giCurrentSAFTranSeqNum % 9999   ) + 1;	//Updating the SAF Transaction Number
	giCurrentSAFIntrnSeqNum  = ( giCurrentSAFIntrnSeqNum % 9999  ) + 1;	//Updating the SAF Internal Sequence Number

	if(iCmd == SSI_TOR)
	{
		debug_sprintf(szDbgMsg, "%s: SAFing Of a TOR Transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		giTotalSAFTORRecords = giTotalSAFTORRecords + 1;
		giCurrentSAFTORRecordNumber = ( giCurrentSAFTORRecordNumber % 9999 ) + 1;	//Updating the SAF record number. Note: SAF record number that goes from 0001 to 9999.

		debug_sprintf(szDbgMsg, "%s - After Updation: Total SAF TOR Records[%d], Current SAF TOR Record Number[%d]", __FUNCTION__, giTotalSAFTORRecords, giCurrentSAFTORRecordNumber);
		APP_TRACE(szDbgMsg);

		sprintf(pszSAFRecordNumber, "%u", giCurrentSAFTORRecordNumber);
	}
	else
	{
		giTotalSAFRecords 		 = giTotalSAFRecords + 1; 	//Incrementing the SAF record count
		giCurrentSAFRecordNumber = ( giCurrentSAFRecordNumber % 9999 ) + 1;	//Updating the SAF record number. Note: SAF record number that goes from 0001 to 9999.

		if(iCmd == SSI_ACTIVATE || (iCmd == SSI_CREDIT && (isFloorLimitChkAllowedForSAFRefund() == PAAS_FALSE)) || iCmd == SSI_PAYACCOUNT)
		{
			debug_sprintf(szDbgMsg, "%s: For Gift Activate or Refund with Transa Floor Limit Check Disabled, No Need to Update the Total SAF Transaction Amount", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			gfTotalSAFAmount  		 = gfTotalSAFAmount  + fCurrentTranAmount;	//Updating the SAF Total Amount
		}
		debug_sprintf(szDbgMsg, "%s - After Updation: Total SAF Records[%d], Total SAF Amount[%lf], Current SAF Record Number[%d]", __FUNCTION__, giTotalSAFRecords, gfTotalSAFAmount, giCurrentSAFRecordNumber);
		APP_TRACE(szDbgMsg);

		sprintf(pszSAFRecordNumber, "%u", giCurrentSAFRecordNumber);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}


/*
 * ============================================================================
 * Function Name: updateSAFDataToFile
 *
 * Description	: This API adds the SAF Data details to the SAF Data file
 *
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int updateSAFDataToFile()
{
	FILE	*Fh     	  	= NULL;
	int 	iRetVal 	  	= SUCCESS;
	char	szBuffer[256] 	= "";
	int		iBufferLen	    = 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFDetailsFileMutex, "SAF Data File");

	Fh = fopen(SAF_DATA_FILE_NAME_PERMANENT, "w"); //Create an empty file for output operations. If a file with the same name already exists, its contents are discarded and the file is treated as a new empty file.
	if(Fh == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening %s file ", __FUNCTION__, SAF_DATA_FILE_NAME_PERMANENT);
		APP_TRACE(szDbgMsg);
		releaseMutexLock(&gptSAFDetailsFileMutex, "SAF Details File");
		return ERR_SAF_FILE_OPEN;
	}

	if(chmod(SAF_DATA_FILE_NAME_PERMANENT, 0666) == -1)
	{
		debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	/* Format of the header
	<TOTAL_RECORD_COUNT>|<TOTAL_SAF_TRANS_AMOUNT>|<CURRENT_SAF_RECORD>|<LAST_OFFLINE_EPOCH_TIME>|<SAF_INTERNAL_SEQ_NUM>|<LAST_TIMESTAMP>|<SAF_TRAN_SEQ_NUM>|<TOTAL_ELIGIBLE_SAF_RECORDS>;<LF>
	<TOTAL_TOR_RECORD_COUNT>|<CURRENT_TOR_SAF_RECORD>|<TOTAL_ELIGIBLE_SAF_TOR_RECORDS>;<LF>
	*/
	/* If SAF records are not present in Eligible Record List , i.e They are removed using SAF Remove from POS, then we can reset the gtLastOfflineEpochTime.
	 * This will ensure that we do not eventually stop SAF'ing the transactions because of offlinedayslimit check.
	 * This was an AMDOCs case.
	 */
	if(giTotalEligibleSAFRecords == 0 && giTotalPendingSAFTORRecords == 0)
	{
		gtLastOfflineEpochTime = 0;
	}
	sprintf(szBuffer, "%d%c%.2lf%c%u%c%ld%c%u%c%s%c%d%c%d%c%c%d%c%d%c%d%c%c",
			giTotalSAFRecords, PIPE, gfTotalSAFAmount, PIPE, giCurrentSAFRecordNumber, PIPE, gtLastOfflineEpochTime, PIPE, giCurrentSAFIntrnSeqNum, PIPE, gszLastTimeStamp, PIPE, giCurrentSAFTranSeqNum, PIPE, giTotalEligibleSAFRecords, SEMI_COLON, LF,
			giTotalSAFTORRecords, PIPE, giCurrentSAFTORRecordNumber, PIPE, giTotalPendingSAFTORRecords, SEMI_COLON, LF);

	iBufferLen = strlen(szBuffer);

	debug_sprintf(szDbgMsg, "%s -Updated SAF data [%s] and its length is %d", __FUNCTION__, szBuffer, iBufferLen);
	APP_TRACE(szDbgMsg);

	iRetVal = fwrite(szBuffer, sizeof(char), iBufferLen, Fh);

	if(iRetVal == iBufferLen)
	{
		debug_sprintf(szDbgMsg, "%s- Successfully updated the SAF Data file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SUCCESS;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s- Error while updating SAF data file, Number of bytes written(%d), Actual count (%d)", __FUNCTION__, iRetVal, iBufferLen);
		APP_TRACE(szDbgMsg);
		iRetVal = ERR_SAF_FILE_WRITE;
	}

	fclose(Fh); //Closing the file

	releaseMutexLock(&gptSAFDetailsFileMutex, "SAF Data File");

	debug_sprintf(szDbgMsg, "%s - Returning with %d ", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;

}


/*
 * ============================================================================
 * Function Name: addSAFRecordToFile
 *
 * Description	: This API adds the given record to the SAF file
 *
 * Input Params	: SAF Record Data buffer
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int addSAFRecordToFile(char *pszSAFRecordData, int iCmd)
{
	FILE	*Fh     	  = NULL;
	int 	iRetVal 	  = SUCCESS;
	int		iRecordLength = 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRecordData == NULL || (strlen(pszSAFRecordData) == 0))
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL or does not contain any value, Returning", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "a+"); //Open file for output at the end of a file. Output operations always write data at the end of the file, expanding it. Repositioning operations (fseek, fsetpos, rewind) are ignored. The file is created if it does not exist
	if(Fh == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening %s file ", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
		APP_TRACE(szDbgMsg);
		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
		return ERR_SAF_FILE_OPEN;
	}

	if(chmod(SAF_ELIGIBLE_RECORDS_FILE_NAME, 0666) == -1)
	{
		debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	iRecordLength = strlen(pszSAFRecordData);

	iRetVal = fwrite(pszSAFRecordData, sizeof(char), iRecordLength, Fh);

	if(iCmd == SSI_TOR)
	{
		giTotalPendingSAFTORRecords = giTotalPendingSAFTORRecords + 1;
	}
	else
	{
		giTotalEligibleSAFRecords = giTotalEligibleSAFRecords + 1;
	}

	if(iRetVal == iRecordLength)
	{
		debug_sprintf(szDbgMsg, "%s: Successfully written SAF record to the file ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SUCCESS;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failure while writing SAF record to the file, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLength, iRetVal);
		APP_TRACE(szDbgMsg);
		iRetVal = ERR_SAF_FILE_WRITE;
	}

	fclose(Fh); //Closing the file

	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	debug_sprintf(szDbgMsg, "%s - Returning with %d ", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}
/*
 * ============================================================================
 * Function Name: findNumEligibleRecords
 *
 * Description	: This function is used to count the number of Records in
				 SAFRecords.db
 *
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int findNumEligibleRecords()
{
	FILE	*fptr                       = NULL;
	char	szTempBuf[100]            = "";
	int		iRetVal					  = SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(SUCCESS != doesFileExist(SAF_ELIGIBLE_RECORDS_FILE_NAME))
	{
		debug_sprintf(szDbgMsg, "%s: %s doesn't exist, Reset Eligible Rec Count", __FUNCTION__, SAF_DATA_FILE_NAME_PERMANENT);
		APP_TRACE(szDbgMsg);

		giTotalEligibleSAFRecords = 0;

	}
	else
	{
		memset(szTempBuf, 0x00, sizeof(szTempBuf));
		sprintf(szTempBuf, "cat %s | wc -l > %s",SAF_ELIGIBLE_RECORDS_FILE_NAME,SAFRECORD_COUNT_FILE);
		debug_sprintf(szDbgMsg,"%s - The command to be executed is [%s]", __FUNCTION__,szTempBuf);
		APP_TRACE(szDbgMsg);
		iRetVal = local_svcSystem(szTempBuf);
		if(iRetVal == 0)
		{
			debug_sprintf(szDbgMsg,"%s - Successfully executed the command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s - Error in execution of the command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//iRetVal = FAILURE; CID 67473 (#1 of 1): Unused value (UNUSED_VALUE) T_Raghavendran, No Effect of setting iRetVal as it is overwritten below.
		}
		fptr = fopen(SAFRECORD_COUNT_FILE, "r");
		if (!fptr)
		{
			debug_sprintf(szDbgMsg, "%s: %s doesn't exist, Reset Eligible Rec Count", __FUNCTION__, SAFRECORD_COUNT_FILE);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			if(fscanf(fptr, "%d", &giTotalEligibleSAFRecords) != 1) // CID 67391 (#1 of 1): Unchecked return value from library (CHECKED_RETURN) T_RaghavendranR1
			{
				debug_sprintf(szDbgMsg, "%s: Could not read data from file!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			fclose(fptr); //CID 67345 (#1 of 1): Dereference after null check (FORWARD_NULL). T_RaghavendranR1. fclose only if it exists.
		}

		memset(szTempBuf, 0x00, sizeof(szTempBuf));
		sprintf(szTempBuf, "rm -f %s", SAFRECORD_COUNT_FILE); //removing the Temporary SAF record file
		debug_sprintf(szDbgMsg,"%s - The command to be executed is [%s]", __FUNCTION__,szTempBuf);
		APP_TRACE(szDbgMsg);

		iRetVal = local_svcSystem(szTempBuf);
		if(iRetVal == 0)
		{
			debug_sprintf(szDbgMsg,"%s - Successfully executed the command to remove the temp SAF records count file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

		}
		else
		{
			debug_sprintf(szDbgMsg,"%s - Error in execution of the command to remove the temp SAF records count file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	debug_sprintf(szDbgMsg, "%s - Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}
/*
 * ============================================================================
 * Function Name: readSAFData
 *
 * Description	: This API reads the first line of the SAF record file to update
 * 					the Total SAF amount, SAF Record Number, Total SAF records
 * 					and SAF offline days
 *
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int readSAFData()
{
	FILE	*Fh                       = NULL;
	char	szTempBuf[150]            = "";
	struct	stat st;
	int		iRetVal					  = SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(stat(SAF_DATA_FILE_NAME_PERMANENT, &st) != 0)
	{
		debug_sprintf(szDbgMsg, "%s: %s doesn't exist, Saving SAF Record Number to %d", __FUNCTION__, SAF_DATA_FILE_NAME_PERMANENT, 0);
		APP_TRACE(szDbgMsg);

		iRetVal = resetSAFDataFile();
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated the SAF details file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		Fh = fopen(SAF_DATA_FILE_NAME_PERMANENT, "r+");
		if(Fh!=NULL)
		{
			if(fgets(szTempBuf, sizeof(szTempBuf), Fh) != NULL)
			{
				debug_sprintf(szDbgMsg, "%s - Read line from the SAF file is [%s] ", __FUNCTION__, szTempBuf);
				APP_TRACE(szDbgMsg);

				iRetVal = parseSAFDataLine(szTempBuf);
				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s - Successfully parsed the SAF Data line", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					//After updating the global variables
					debug_sprintf(szDbgMsg, "%s - giTotalSAFRecords[%d], gfTotalSAFAmount[%lf], giCurrentSAFRecordNumber[%d], gtLastOfflineEpochTime[%ld] giCurrentSAFIntrnSeqNum[%d], gszLastTimeStamp[%s], giCurrentSAFTranSeqNum[%d] giTotalEligibleSAFRecords [%d]", __FUNCTION__, giTotalSAFRecords, gfTotalSAFAmount, giCurrentSAFRecordNumber, gtLastOfflineEpochTime, giCurrentSAFIntrnSeqNum, gszLastTimeStamp, giCurrentSAFTranSeqNum, giTotalEligibleSAFRecords);
					APP_TRACE(szDbgMsg);
					if(fgets(szTempBuf, sizeof(szTempBuf), Fh) != NULL)
					{
						iRetVal = parseSAFDataLineForTOR(szTempBuf);
						if(iRetVal == SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s - Successfully parsed the SAF TOR Data line", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							//After updating the global variables
							debug_sprintf(szDbgMsg, "%s - giTotalSAFTORRecords[%d], giCurrentSAFTORRecordNumber[%d] giTotalPendingSAFTORRecords [%d]", __FUNCTION__, giTotalSAFTORRecords, giCurrentSAFTORRecordNumber, giTotalPendingSAFTORRecords);
							APP_TRACE(szDbgMsg);

						}
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s - Failure while parsing the SAF TOR Data line, May be Old SAF File, Not throwing Error", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						iRetVal = SUCCESS;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s - Failure while parsing the SAF Data line", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iRetVal = FAILURE;
				}
				fclose(Fh);
			}
			else	// CID-67320: 27-Jan-16: MukeshS3:
			{		// Need to close the file for failure case.
				fclose(Fh);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while opening the file, ", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//FIXME: Is this OK??

			iRetVal = resetSAFDataFile();

			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated the SAF details file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s - Returning with %d ", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: readSAFData
 *
 * Description	: This API reads the first line of the SAF record file to update
 * 					the Total SAF amount, SAF Record Number, Total SAF records
 * 					and SAF offline days
 *
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int resetSAFDataFile()
{
	int		iRetVal					  = SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//Reseting the global variable to the default values
	giCurrentSAFRecordNumber = 0;
	giTotalSAFRecords        = 0;
	giTotalEligibleSAFRecords = 0;
	gfTotalSAFAmount         = 0.00;
	giCurrentSAFIntrnSeqNum  = 1000;
	giCurrentSAFTranSeqNum   = 0;
	gtLastOfflineEpochTime   = 0;
	memset(gszLastTimeStamp, 0, sizeof(gszLastTimeStamp));

	giCurrentSAFTORRecordNumber		= 0;
	giTotalSAFTORRecords        	= 0;
	giTotalPendingSAFTORRecords 	= 0;

	iRetVal = updateSAFDataToFile();
	if(iRetVal == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Successfully updated the SAF details file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s - Returning with %d ", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: updateTranWithSAFDetails
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int updateTranWithSAFDetails(char *pszTranKey)
{
	int				iRetVal				 = SUCCESS;
	int     		iPymtType    		 = -1;
	int     		iCmd    			 = -1;
	int				iFxn				 = -1;
	char 			szSAFRecNum[15]      = "";
	char 			szSAFTranSeqNum[15]  = "";
	char 			szSAFIntrnSeqNum[15] = "";
	char 			szBIN[6+1]			 = "";
	static char		szMID[20]			 = "";
	static char		szTID[20]			 = "";
	static int		iFirstTime			 = 0;
	ENC_TYPE		iEncType			 = getEncryptionType();
	RESPDTLS_STYPE  stRespDtls;
	AMTDTLS_STYPE   stAmountDtls;
	CTRANDTLS_STYPE stCurTranDtls;
	CARDDTLS_STYPE	stCardDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * Praveen_P1: Need to send MID/TID values to POS
	 * since it is McD branch, harcoding those valyes for now
	 * here
	 */
	if(iFirstTime == 0)
	{
		memset(szMID, 0x00, sizeof(szMID));

		strcpy(szMID, getDHIMID());

		memset(szTID, 0x00, sizeof(szTID));

		strcpy(szTID, getDHITID());

		debug_sprintf(szDbgMsg, "%s: MID [%s]", __FUNCTION__, szMID);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: TID [%s]", __FUNCTION__, szTID);
		APP_TRACE(szDbgMsg);

		iFirstTime = 1;
	}

	/* get the func and cmd of the current transaction instance */
	getFxnNCmdForSSITran(pszTranKey, &iFxn, &iCmd);

	memset(&stRespDtls, 0x00, sizeof(RESPDTLS_STYPE));
	memset(&stAmountDtls, 0x00, sizeof(AMTDTLS_STYPE));
	memset(&stCurTranDtls, 0x00, sizeof(CTRANDTLS_STYPE));
	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

	if(iCmd == SSI_TOR)
	{
		memset(szSAFRecNum, 0x00, sizeof(szSAFRecNum));
		sprintf(szSAFRecNum, "%u", giCurrentSAFTORRecordNumber);
	}
	else
	{
		memset(szSAFRecNum, 0x00, sizeof(szSAFRecNum));
		sprintf(szSAFRecNum, "%u", giCurrentSAFRecordNumber);
	}

	memset(szSAFTranSeqNum, 0x00, sizeof(szSAFTranSeqNum));
	sprintf(szSAFTranSeqNum, "%u", giCurrentSAFTranSeqNum);

	memset(szSAFIntrnSeqNum, 0x00, sizeof(szSAFIntrnSeqNum));
	sprintf(szSAFIntrnSeqNum, "%u", giCurrentSAFIntrnSeqNum);

	while(1)
	{
		if(iCmd == SSI_VOID)
		{
			strcpy(stRespDtls.szRslt, "VOIDED/STORED");      				//RESULT
			strcpy(stRespDtls.szRsltCode, "7");                				//RESULT_CODE
		}
		else
		{
			//TERMINATION_STATUS
			strcpy(stRespDtls.szRslt, "APPROVED/STORED");      				//RESULT
			strcpy(stRespDtls.szRsltCode, "4");                				//RESULT_CODE

		}
		strcpy(stRespDtls.szTermStat, "SUCCESS");
		strcpy(stRespDtls.szRespTxt, "Transaction Approved Offline"); 	//RESPONSE_TEXT

		iRetVal = updateGenRespDtlsForPymtTran(pszTranKey, &stRespDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Amount details!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		iRetVal = getAmtDtlsForPymtTran(pszTranKey, &stAmountDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Amount details!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		strcpy(stAmountDtls.apprvdAmt, stAmountDtls.tranAmt);// FIX_ME: Will it include Tip amount if it there

		iRetVal = updateAmtDtlsForPymtTran(pszTranKey, &stAmountDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Amount details!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		iRetVal = getCurTranDtlsForPymt(pszTranKey, &stCurTranDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in Getting the Current Transaction Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		/*-------------Update the current transaction details to send it to POS-----------*/
		/* Get the payment type of the current transaction */
		iRetVal = getPymtTypeForPymtTran(pszTranKey, &iPymtType);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get pymt type", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break; // breaking from here, no more states!
		}
		switch(iPymtType)
		{
		case PYMT_DEBIT:
			strcpy(stCurTranDtls.szPymntType, "DEBIT");
			break;
		case PYMT_EBT:
			strcpy(stCurTranDtls.szPymntType, "EBT");
			break;
		case PYMT_GIFT:
			strcpy(stCurTranDtls.szPymntType, "GIFT");
			break;
		case PYMT_MERCHCREDIT:
			strcpy(stCurTranDtls.szPymntType, "MERCH_CREDIT");
			break;
		case PYMT_CREDIT:
		case PYMT_GOOGLE:
		case PYMT_ISIS:
		case PYMT_FSA:
			strcpy(stCurTranDtls.szPymntType, "CREDIT");
			break;
		case PYMT_PRIVATE:
			strcpy(stCurTranDtls.szPymntType, "PRIV_LBL");
			break;

		case PYMT_CASH:
			strcpy(stCurTranDtls.szPymntType, "CASH");
			break;
		default:
			debug_sprintf(szDbgMsg, "%s: Invalid payment type, should never come here!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		strcpy(stCurTranDtls.szTranSeq, szSAFTranSeqNum);      //TRANS_SEQ_NUM
		strcpy(stCurTranDtls.szTranId, szSAFIntrnSeqNum);      //TRANS_INT_NUM
		if(iCmd == SSI_TOR)
		{
			strcpy(stCurTranDtls.szSAFTORNo, szSAFRecNum); 			//SAF_TOR_NUM
		}
		else
		{
			strcpy(stCurTranDtls.szSAFNo, szSAFRecNum); 			//SAF_NUM
		}

		if(strlen(stCurTranDtls.szMerchId) == 0)
		{
			if(strlen(szMID) > 0)
			{
				strcpy(stCurTranDtls.szMerchId, szMID);
			}
		}

		if(strlen(stCurTranDtls.szTermId) == 0)
		{
			if(strlen(szTID) > 0)
			{
				strcpy(stCurTranDtls.szTermId, szTID);
			}
		}

		iRetVal = updateCurTranDtlsForPymt(pszTranKey, &stCurTranDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Current Tran Details!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		/* Get the card details for the current transaction instance */
		iRetVal = getCardDtlsForPymtTran(pszTranKey, &stCardDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get card dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		if((iPymtType == PYMT_GIFT && returnEmbossedNumForGiftType() &&
			isEmbossedCardNumCalcRequired() && (strlen(stCardDtls.szPAN) > 0) &&
			(strcasecmp("SVDOT", (char *)getMerchantProcessor(PROCESSOR_GIFT)) == 0)))
		{
			/*Calculate the Embossed Card Number for Gift Transactions with ValueLink Processors as per the logic
			we already implemented in RCHI*/
			if(iEncType == VSP_ENC)
			{
				getEmbossedPANFromTrackData(stCardDtls.szPAN, stCardDtls.szEmbossedPAN, stCardDtls.bManEntry);
			}
		}
		else if( ((returnEmbossedNumForGiftType() && iPymtType == PYMT_GIFT) ||
				(returnEmbossedNumForPrivLblType() && iPymtType == PYMT_PRIVATE )) && (strlen(stCardDtls.szPAN) > 0))
		{
			memset(szBIN, 0x00, sizeof(szBIN));
			strncpy(szBIN, stCardDtls.szPAN, 6);

			if(checkBinInclusion(szBIN) == PAAS_FALSE || (strlen(stCardDtls.szPAN) <= 12))
			{
				debug_sprintf(szDbgMsg, "%s: Card is NOT Included in PCI Card Range, updating Embossed Card number", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iEncType == VSP_ENC)
				{
					strcpy(stCardDtls.szEmbossedPAN, stCardDtls.szPAN);
					getDataFromVCL(stCardDtls.szEmbossedPAN, NULL, NULL, NULL, stCardDtls.bManEntry);

					/*
					 * Praveen_P1: Putting extra check here so that we will not send pci clear range
					 */
					if(strlen(stCardDtls.szEmbossedPAN) > 0)
					{
						memset(szBIN, 0x00, sizeof(szBIN));
						strncpy(szBIN, stCardDtls.szEmbossedPAN, 6);
						if(checkBinInclusion(szBIN) == PAAS_TRUE && (strlen(stCardDtls.szPAN) > 12))
						{
							debug_sprintf(szDbgMsg, "%s: PAN is Included in PCI Card Range, not sending Embossed Card number", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(stCardDtls.szEmbossedPAN, 0x00, sizeof(stCardDtls.szEmbossedPAN));
						}
					}
				}
				else if(iEncType == VSD_ENC)
				{
					strcpy(stCardDtls.szEmbossedPAN, stCardDtls.szClrPAN);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Card is Included in PCI Card Range, not updating Embossed Card number", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		iRetVal = updateCardDtlsForPymtTran(pszTranKey, &stCardDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failure while updating card details to Payment Transaction",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}
/* ArjunU1: Added for EMV SAF. its dummy function for now just to send some response when
			emv card is approved offline. Update this function when fully EMV SCA SAF is implemented.
*/
/*
 * ============================================================================
 * Function Name: updateTranWithEmvSAFDetails
 *
 * Description	: Temporarly added to send response to POS for transaction appoved by card.
 *				  Remove this function when EMV SAF is implemented completely.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int updateTranWithEmvSAFDetails(char *pszTranKey)
{
	int				iRetVal				 	= SUCCESS;
	int     		iPymtType    			= -1;
	char 			szEmvSAFRecNum[15]      = "123";
	char 			szEmvSAFTranSeqNum[15]  = "456";
	char 			szEmvSAFIntrnSeqNum[15] = "789";
	RESPDTLS_STYPE  stRespDtls;
	AMTDTLS_STYPE   stAmountDtls;
	CTRANDTLS_STYPE stCurTranDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&stRespDtls, 0x00, sizeof(RESPDTLS_STYPE));
	memset(&stAmountDtls, 0x00, sizeof(AMTDTLS_STYPE));
	memset(&stCurTranDtls, 0x00, sizeof(CTRANDTLS_STYPE));

	while(1)
	{
		strcpy(stRespDtls.szTermStat, "SUCCESS");          				//TERMINATION_STATUS
		strcpy(stRespDtls.szRslt, "APPROVED/STORED");      				//RESULT
		strcpy(stRespDtls.szRsltCode, "4");                				//RESULT_CODE
		strcpy(stRespDtls.szRespTxt, "Transaction Approved by card(Offline)"); 	//RESPONSE_TEXT

		iRetVal = updateGenRespDtlsForPymtTran(pszTranKey, &stRespDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Amount details!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		iRetVal = getAmtDtlsForPymtTran(pszTranKey, &stAmountDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Amount details!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		strcpy(stAmountDtls.apprvdAmt, stAmountDtls.tranAmt);// FIX_ME: Will it include Tip amount if it there

		iRetVal = updateAmtDtlsForPymtTran(pszTranKey, &stAmountDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Amount details!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		/*-------------Update the current transaction details to send it to POS-----------*/
		/* Get the payment type of the current transaction */
		iRetVal = getPymtTypeForPymtTran(pszTranKey, &iPymtType);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get pymt type", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break; // breaking from here, no more states!
		}
		switch(iPymtType)
		{
		case PYMT_DEBIT:
			strcpy(stCurTranDtls.szPymntType, "DEBIT");
			break;
		case PYMT_GIFT:
			strcpy(stCurTranDtls.szPymntType, "GIFT");
			break;

		case PYMT_MERCHCREDIT:
			strcpy(stCurTranDtls.szPymntType, "MERCH_CREDIT");
			break;

		case PYMT_CREDIT:
		case PYMT_GOOGLE:
		case PYMT_ISIS:
			strcpy(stCurTranDtls.szPymntType, "CREDIT");
			break;
		case PYMT_PRIVATE:
			strcpy(stCurTranDtls.szPymntType, "PRIV_LBL");
			break;

		case PYMT_CASH:
			strcpy(stCurTranDtls.szPymntType, "CASH");
			break;
		default:
			debug_sprintf(szDbgMsg, "%s: Invalid payment type, should never come here!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		strcpy(stCurTranDtls.szTranSeq, szEmvSAFTranSeqNum);      //TRANS_SEQ_NUM
		strcpy(stCurTranDtls.szTranId, szEmvSAFIntrnSeqNum);      //TRANS_INT_NUM
		strcpy(stCurTranDtls.szSAFNo, szEmvSAFRecNum); 			//SAF_NUM

		iRetVal = updateCurTranDtlsForPymt(pszTranKey, &stCurTranDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Current Tran Details!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}


/*
 * ============================================================================
 * Function Name: postSAFRecordToHost
 *
 * Description	: This API posts the SAF record to PWC and then updates the SAF data system
 *
 *
 * Input Params	: NONE
 *
 * Output Params: TRUE/FALSE
 * ============================================================================
 */
int postSAFRecordToHost()
{
	int 			iRetVal 				= SUCCESS;
	int     		iCmd    				= -1;
	int				iFxn					= -1;
	int 			iResultCode				= 0;
	int 			iTmpResultCode			= 0;
	int				iAppLogEnabled			= isAppLogEnabled();
	char			szAppLogData[300]		= "";
	char			szSAFRecordStatus[15] 	= "";
	char			szStatMsg[100]        	= "";
	PAAS_BOOL 		bDuplicateTran     	 	= PAAS_FALSE;
	PAAS_BOOL		bSUCCESSTran 			= PAAS_FALSE;
	RESPDTLS_STYPE	stResponseDtls;
	SIGDTLS_STYPE	stSignatureDtls;
	SIGDTLS_STYPE	stTempSignatureDtls;

#ifdef DEBUG
	char	szDbgMsg[4608]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	if(strlen(szSSITranKey) <= 0)
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present, so no record to post", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	while(1)
	{
		/* Set function and command in the transaction instance */
		//setFxnNCmdForSSITran(szSSITranKey, SSI_PYMT, SSI_SALE);

		/* Get the signature placeholder from the transaction instance */
		memset(&stSignatureDtls, 0x00, sizeof(SIGDTLS_STYPE));
		iRetVal = getSigDtlsForPymtTran(szSSITranKey, &stSignatureDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get signature data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = FAILURE;
			break;
		}
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Posting the SAF Records to SSI Host");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		/* get the func and cmd of the current transaction instance */
		getFxnNCmdForSSITran(szSSITranKey, &iFxn, &iCmd);

		/*
		 * Dont have the signature details in the Transaction Instance
		 * so that we will not send the signature data in the SALE
		 * request
		 */
		memset(&stTempSignatureDtls, 0x00, sizeof(SIGDTLS_STYPE));
		iRetVal = updateSigDtlsForPymtTran(szSSITranKey, &stTempSignatureDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to reset signature data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = FAILURE;
			break;
		}

		/* Send the request to the host and get the response */
		//iRetVal = doSSICommunication(szSSITranKey, szStatMsg);
		iRetVal = procSSIRequest(szSSITranKey, szStatMsg);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: SSI communication FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//iRetVal = FAILURE;	//dont overwrite the value
			break;
		}
		debug_sprintf(szDbgMsg, "%s: SSI communication Successful", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* get the general host response details for the transaction instance */
		memset(&stResponseDtls, 0x00, sizeof(RESPDTLS_STYPE));
		iRetVal = getGenRespDtlsForSSITran(szSSITranKey, &stResponseDtls);
		//CID 67303 (#1 of 1): Unchecked return value (CHECKED_RETURN). T_RaghavendranR1
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get General Response Details",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = FAILURE;
			break;
		}

		iResultCode = atoi(stResponseDtls.szRsltCode);

		debug_sprintf(szDbgMsg, "%s: The Result Code from SSI is %d",__FUNCTION__, iResultCode);
		APP_TRACE(szDbgMsg);

		memset(szSAFRecordStatus, 0x00, sizeof(szSAFRecordStatus));

		switch(iResultCode)
		{
			case SUCCESS_PWC_RSLTCODE_CAPTURED:
			case SUCCESS_PWC_RSLTCODE_APPROVED:
			case SUCCESS_PWC_RSLTCODE_COMPLETED:
			case SUCCESS_PWC_RSLTCODE_VOIDED:
				debug_sprintf(szDbgMsg, "%s: Sale is CAPTURED/APPROVED/COMPLETED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				bSUCCESSTran = PAAS_TRUE;

				if(iCmd == SSI_TOR)
				{
					debug_sprintf(szDbgMsg, "%s: TORed SAF Transaction, Setting Status as REVERSED  ",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(szSAFRecordStatus, "REVERSED");
					break;
				}

				//Need to send the Signature data if it is available

#if 0			//Praveen_P1: This we moved up
				/* Get the signature placeholder from the transaction instance */
				memset(&stSignatureDtls, 0x00, sizeof(SIGDTLS_STYPE));
				iRetVal = getSigDtlsForPymtTran(szSSITranKey, &stSignatureDtls);
				if(iRetVal != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to get signature data",__FUNCTION__);
					APP_TRACE(szDbgMsg);

					iRetVal = FAILURE;
					break;
				}
#endif
				if(stSignatureDtls.szSign != NULL)
				{
					if(strlen(stSignatureDtls.szSign) > 0)
					{
						debug_sprintf(szDbgMsg, "%s: Signature Data is available..need to post it to PWC",__FUNCTION__);
						APP_TRACE(szDbgMsg);

						/*
						 * Update the signature details if present to the transaction instance
						 * to include them in the SIGNATURE command
						 */
						iRetVal = updateSigDtlsForPymtTran(szSSITranKey, &stSignatureDtls);
						if(iRetVal != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: FAILED to reset signature data",__FUNCTION__);
							APP_TRACE(szDbgMsg);

							iRetVal = FAILURE;
							break;
						}

						/*Add signature to the current transaction */
						iRetVal = addTrantoCurrentTran(szSSITranKey, SSI_SIGN, &iTmpResultCode);
						if(iRetVal == SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: Added the signature details to the current SAF transaction",__FUNCTION__);
							APP_TRACE(szDbgMsg);
							switch(iTmpResultCode)
							{
							case ERR_PWC_RSLTCODE_SUCCESS:
								strcpy(szSAFRecordStatus, "PROCESSED"); //SAF Record Status PROCESSED
								break;
							case ERR_PWC_RSLTCODE_INVLD_TROUTD_FOR_SIG:
							case ERR_PWC_RSLTCODE_INVLD_SIGNATURE:
							case ERR_PWC_RSLTCODE_DUP_SIG:
							case ERR_PWC_RSLTCODE_INVLD_SIG_REQUEST: //Praveen_P1: This should not be the case, some request framing error

								/* Error while adding the signature details */
								debug_sprintf(szDbgMsg, "%s: Error while adding signature details to the transaction",__FUNCTION__);
								APP_TRACE(szDbgMsg);
#if 0
								/* Voiding the transaction */
								addTrantoCurrentTran(szSSITranKey, SSI_VOID, &iResultCode);

								strcpy(szSAFRecordStatus, "NOT_PROCESSED"); //SAF Record Status NOT_PROCESSED
#endif

								/*
								 * Praveen_P1: Considering the transaction as succesful since we got succesful response
								 * for sale, thats why setting status as PROCESSED
								 */
								strcpy(szSAFRecordStatus, "PROCESSED"); //SAF Record Status PROCESSED

								break;
							default:
								debug_sprintf(szDbgMsg, "%s: Error while adding signature details to the transaction",__FUNCTION__);
								APP_TRACE(szDbgMsg);
#if 0
								strcpy(szSAFRecordStatus, "NOT_PROCESSED"); //SAF Record Status NOT_PROCESSED
#endif
								/*
								 * Praveen_P1: Considering the transaction as succesful since we got succesful response
								 * for sale, thats why setting status as PROCESSED
								 */
								strcpy(szSAFRecordStatus, "PROCESSED"); //SAF Record Status PROCESSED

								break;
							}

						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Error while adding signature details to the current SAF transaction",__FUNCTION__);
							APP_TRACE(szDbgMsg);
#if 0
							/* Error while adding the signature details */
							strcpy(szSAFRecordStatus, "NOT_PROCESSED"); //SAF Record Status NOT_PROCESSED
#endif
							/*
							 * Praveen_P1: Considering the transaction as succesful since we got succesful response
							 * for sale, thats why setting status as PROCESSED
							 */
							strcpy(szSAFRecordStatus, "PROCESSED"); //SAF Record Status PROCESSED

						}
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Signature Data is NOT available",__FUNCTION__);
						APP_TRACE(szDbgMsg);
						strcpy(szSAFRecordStatus, "PROCESSED"); //SAF Record Status PROCESSED
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Signature Data is NOT available",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(szSAFRecordStatus, "PROCESSED"); //SAF Record Status PROCESSED
				}
				break;
			case ERR_PWC_RSLTCODE_DECLINED:
				debug_sprintf(szDbgMsg, "%s: Sale is DECLINED",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iCmd == SSI_TOR)
				{
					debug_sprintf(szDbgMsg, "%s: TORed SAF Transaction, Setting Status as NOT_ALLOWED  ",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(szSAFRecordStatus, "NOT_ALLOWED");
					break;
				}

				bDuplicateTran = wasDuplicateTran(szSSITranKey);
				if(bDuplicateTran == PAAS_FALSE)
				{
					debug_sprintf(szDbgMsg, "%s: Declined not due to Duplicate transaction, ",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(szSAFRecordStatus, "DECLINED"); //SAF Record Status DECLINED

					break;
				}
				else
				{
					if( PAAS_TRUE == isForceFlgSetForSAFTran() )
					{
						debug_sprintf(szDbgMsg, "%s: Declined due to Duplicate transaction, ",__FUNCTION__);
						APP_TRACE(szDbgMsg);

						//Setting the force flag to true in the transaction

						debug_sprintf(szDbgMsg, "%s: Setting the FORCE_FLAG to true, ",__FUNCTION__);
						APP_TRACE(szDbgMsg);

						iRetVal = updateForceFlagForPymtTran(szSSITranKey, PAAS_TRUE);
						if(iRetVal != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: Error while updating the force flag!!! ",__FUNCTION__);
							APP_TRACE(szDbgMsg);
							//Not sure what to do in this case, currently keeping the transaction as not processed
							strcpy(szSAFRecordStatus, "NOT_PROCESSED"); //SAF Record Status NOT_PROCESSED
							break;
						}
						debug_sprintf(szDbgMsg, "%s: Successfully updated the Force Flag ",__FUNCTION__);
						APP_TRACE(szDbgMsg);

						continue; //Going to frame the PWC request once more...

					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Force Flag for SAF tran is set FALSE, No need post again",__FUNCTION__);
						APP_TRACE(szDbgMsg);

						debug_sprintf(szDbgMsg, "%s: Declined due to Duplicate transaction",__FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(szSAFRecordStatus, "DECLINED"); //SAF Record Status DECLINED

						break;
					}
					break;
				}
			case ERR_PWC_RSLTCODE_INVLD_CARD_NUM:
				debug_sprintf(szDbgMsg, "%s: Invalid Card Number",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(szSAFRecordStatus, "DECLINED"); //SAF Record Status DECLINED
				break;
			case ERR_PWC_RSLTCODE_INVLD_EXP_DATE:
				debug_sprintf(szDbgMsg, "%s: Invalid expiration Card ",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(szSAFRecordStatus, "NOT_PROCESSED"); //SAF Record Status DECLINED
				break;
			case 300:
				debug_sprintf(szDbgMsg, "%s: VSP Encryption Error ",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(szSAFRecordStatus, "NOT_PROCESSED"); //SAF Record Status NOT_PROCESSED
				break;
			case ERR_HOST_TRANS_NOT_FOUND:
				debug_sprintf(szDbgMsg, "%s: Trans Not Found ",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iCmd == SSI_TOR)
				{
					strcpy(szSAFRecordStatus, "NOT_FOUND"); //SAF Record Status NOT_FOUND
				}
				else
				{
					strcpy(szSAFRecordStatus, "NOT_PROCESSED"); //SAF Record Status NOT_PROCESSED
				}
				break;
			case ERR_HOST_TRANS_NOT_ALLOWED:
				debug_sprintf(szDbgMsg, "%s: Trans Not Found ",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iCmd == SSI_TOR)
				{
					strcpy(szSAFRecordStatus, "NOT_ALLOWED"); //SAF Record Status NOT_ALLOWED
				}
				else
				{
					strcpy(szSAFRecordStatus, "NOT_PROCESSED"); //SAF Record Status NOT_PROCESSED
				}
				break;

			case ERR_HOST_TRAN_REVERSED:
				if(iCmd == SSI_TOR)
				{
					debug_sprintf(szDbgMsg, "%s: TORed SAF Transaction Setting Status as REVERSED",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(szSAFRecordStatus, "REVERSED");
					break;
				}
			case ERR_HOST_NOT_AVAILABLE:
			case ERR_HOST_TRAN_TIMEOUT:
				debug_sprintf(szDbgMsg, "%s: Host is not available/Host tran timed out ",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = FAILURE;
				break;

			default:

				if(checkResultCodeinSAFErrCodeList(iResultCode))
				{
					debug_sprintf(szDbgMsg, "%s: Host is not available ",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					iRetVal = FAILURE;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Invalid ResultCode (Not Honoring) ...labelling them as NOT_PROCESSED/NOT_ALLOWED(for TOR SAF) ",__FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iCmd == SSI_TOR)
					{
						debug_sprintf(szDbgMsg, "%s: TORed SAF Transaction, Setting Status as NOT_ALLOWED  ",__FUNCTION__);
						APP_TRACE(szDbgMsg);
						strcpy(szSAFRecordStatus, "NOT_ALLOWED");
					}
					else
					{
						strcpy(szSAFRecordStatus, "NOT_PROCESSED"); //SAF Record Status NOT_PROCESSED
					}
				}

#if 0
				if(iResultCode > 0)
				{
					debug_sprintf(szDbgMsg, "%s: ResultCode is positive...labelling them as NOT_PROCESSED ",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(szSAFRecordStatus, "NOT_PROCESSED"); //SAF Record Status NOT_PROCESSED
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: ResultCode is NOT positive...leaving them in the SAF queue",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
#endif
				break;
		}
		break; //Break from the while loop
	}

	/* MukeshS3: 16-Sept-16:
	 * When SAF tracsactions are getting response timeout from HOST, we need to try TOR attempts for torretires time if configured in terminal
	 */
	if( (iRetVal == ERR_RESP_TO || iResultCode == ERR_HOST_TRAN_TIMEOUT) && ! isDHIEnabled() && iCmd != SSI_TOR)
	{
		debug_sprintf(szDbgMsg, "%s: SSI Host Response timeout, checking if TOR is enabled", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(isTOREnabled())
		{
			debug_sprintf(szDbgMsg, "%s: Sending TOR is required to SSI Listener", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(sendTORRequesttoSSI(szSSITranKey, szStatMsg) != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while sending TOR Request to SSI listener", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			debug_sprintf(szDbgMsg, "%s: TOR Processing done", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Sending TOR is NOT required to SSI Host", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	if(strlen(szSAFRecordStatus) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Status is changed and it is %s",__FUNCTION__, szSAFRecordStatus);
		APP_TRACE(szDbgMsg);

		/*
		 * Updating the SSI response details like
		 * AUTH_CODE, TROUTD etc
		 */
		iRetVal = updateSAFSSIRespDetails(szSSITranKey, iResultCode);


		/*---------- Update the SAF status of the record -----------------*/
		acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
		iRetVal = updateSAFRecordStatus(szSAFRecordStatus, 1);
		if(iAppLogEnabled == 1)
		{
			sprintf(szAppLogData, "Status of the SAF Record Posted is Changed to [%s]", szSAFRecordStatus);
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}
		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated the SAF record status",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while updating the SAF record status",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s -  SAF Record Status is NOT changed ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	// We need to free the Sign Details, only for declined/Error Transactions. For Success Transactions, the Signature Details will be freed while cleaning the SSI Details
	if(stSignatureDtls.szSign != NULL && bSUCCESSTran == PAAS_FALSE)//Need to free the sign details
	{
		free(stSignatureDtls.szSign);
	}

	debug_sprintf(szDbgMsg, "%s - Returning with %d ", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: purgeSAFRecords
 *
 * Description	: This API purges the SAF records after given number of days
 *
 *
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int purgeSAFRecords()
{
	FILE	*Fh                     = NULL;
	FILE	*FhTemp					= NULL;
	int		iRetVal					= SUCCESS;
	char	szFileBuffer[2048]		= "";
	int		iRecordLen   	   		= 0;
	int		iTotalRecLen	   		= 0;
	int 	iPreviousLen       		= 0;
	int		iPWCDetailsLength		= 0;
	int		iReadBytes				= 0;
	int		iWriteBytes				= 0;
	char	szRecordTimeStamp[20]   = "";
	char	szRecordLength[10]		= "";
	char	szTempBuf[128]			= "";
	char	*pszSAFRecordData  		= NULL;
	char	*pchTemp				= NULL;
	struct	stat st;
	int		ifilePosition			= 0;
	int		iDays					= 0;
	int		iPurgeFilePosition		= -1;
	int		iNeedToPurgeSAFRec		= 0;
	static int iSAFPurgeDays		= 0;
	int		iRecordsCount			= 0;
	double	fRecordsAmount			= 0.0;
	int		iPurgedRecordsCount		= 0;
	double	fPurgedRecordsAmount	= 0.0;

#ifdef DEBUG
	char	szDbgMsg[2560]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(iSAFPurgeDays == 0)
	{
		iSAFPurgeDays = getSAFPurgeDays();
	}

	debug_sprintf(szDbgMsg, "%s: Need to Purge SAF which are %d days old", __FUNCTION__, iSAFPurgeDays);
	APP_TRACE(szDbgMsg);

	FhTemp = fopen(SAF_RECORD_FILE_NAME_TEMP, "a+"); //Open file for output at the end of a file. Output operations always write data at the end of the file, expanding it. Repositioning operations (fseek, fsetpos, rewind) are ignored. The file is created if it does not exist
	if(FhTemp==NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening the SAF Temporary record file!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(stat(SAF_COMPLETED_RECORDS_FILE_NAME, &st) == 0)
	{
		if(st.st_size > 1)
		{
			Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
			if(Fh==NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				fclose(FhTemp);
				return FAILURE;
			}
			iPWCDetailsLength =  strlen(stHostRespDtls.szPWCSAFResponseCode) + strlen(stHostRespDtls.szPWCSAFAuthCode) +
								 strlen(stHostRespDtls.szPWCSAFTroutd) + strlen(stHostRespDtls.szPWCSAFCTroutd) +
								 strlen(stHostRespDtls.szPWCSAFCardToken) + strlen(stHostRespDtls.szPWCSAFCVVCode) +
								 strlen(stHostRespDtls.szPWCSAFBankUserData) + strlen(stHostRespDtls.szTxnPOSEntryMode) +
								 strlen(stHostRespDtls.szMerchId) + strlen(stHostRespDtls.szTermId) +
								 strlen(stHostRespDtls.szLaneId) + strlen(stHostRespDtls.szApprvdAmt) +  strlen(stHostRespDtls.szLPToken) +
								 strlen(stHostRespDtls.szPPCV) + strlen(stHostRespDtls.szPaymentMedia) + strlen(stHostRespDtls.szAVSCode) +
								 strlen(stHostRespDtls.szHostRespCode) + strlen(stHostRespDtls.szSignatureRef) + strlen(stHostRespDtls.szAcctNum) + strlen(stHostRespDtls.szActionCode)+
								 strlen(stHostRespDtls.szAprType)+ strlen(stHostRespDtls.szPurchaseApr)+ strlen(stHostRespDtls.szReceiptText) +
								 strlen(stHostRespDtls.szStatusFlag)+ strlen(stHostRespDtls.szSvcPhone) + strlen(stHostRespDtls.szCreditPlanNbr) + strlen(stHostRespDtls.szAthNtwID)+
								 strlen(stHostRespDtls.szTransDate) + strlen(stHostRespDtls.szTransTime);

			//for(iIndex = 1; iIndex <= giTotalSAFRecords; iIndex++)
			while(1)
			{
				ifilePosition = ftell(Fh); //Storing the current position of the stream

				debug_sprintf(szDbgMsg, "%s: Current File Location is %d", __FUNCTION__, ifilePosition);
				APP_TRACE(szDbgMsg);

				iRecordLen = getRecordLength(Fh);

				memset(szRecordLength, 0x00, sizeof(szRecordLength));
				sprintf(szRecordLength, "%d", iRecordLen);

				debug_sprintf(szDbgMsg, "%s: Length of the SAF record is %d", __FUNCTION__, iRecordLen);
				APP_TRACE(szDbgMsg);

				if(iRecordLen <= 0)
				{
					debug_sprintf(szDbgMsg, "%s: No more records", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}

				iTotalRecLen = iRecordLen + strlen(szRecordLength)/*For the length*/ + 1 /*For PIPE after length*/ + iPWCDetailsLength + 10; /*//1 for <LF>, 9 for buffer, for updating the status, new status length could be greater than the existing one..allocation extra bytes for that sake */

				if(iPreviousLen < iTotalRecLen)
				{
					//Allocate the memory to hold the SAF record
					pchTemp = (char *)realloc(pszSAFRecordData, iTotalRecLen);
					if(pchTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						fclose(Fh);
						fclose(FhTemp); //Closing the file before returning
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
					else
					{
						memset(pchTemp, 0x00, iTotalRecLen);
						pszSAFRecordData = pchTemp;
					}
				}
				else
				{
					if(pszSAFRecordData != NULL) // CID 67366 (#1 of 2): Dereference before null check (REVERSE_INULL) T_RaghavendranR1
					{
						memset(pszSAFRecordData, 0x00, iPreviousLen);
					}
				}

				iPreviousLen = iTotalRecLen;


				if( (pszSAFRecordData != NULL) && (fgets(pszSAFRecordData, iTotalRecLen, Fh) == NULL)) // CID 67366 (#1 of 2): Dereference before null check (REVERSE_INULL) T_RaghavendranR1
				{
					if(feof(Fh))
					{
						debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						fclose(Fh);
						fclose(FhTemp); //Closing the file before returning
						// CID-67429: 25-Jan-16: MukeshS3:
						// deallocate memory with SAF record data before abnormal return.
						if(pszSAFRecordData != NULL)
						{
							free(pszSAFRecordData);
						}
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}

				if(pszSAFRecordData != NULL) //CID 67366 (#1 of 2): Dereference before null check (REVERSE_INULL). T_RaghavendranR1. NULL Check Added
				{
					if(strstr(pszSAFRecordData, "TimeStamp") == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: It is not SAF Time Stamp line..Skipping it...", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						iRecordsCount = iRecordsCount + 1;
						fRecordsAmount = fRecordsAmount + getTransactionAmount(pszSAFRecordData);
						continue;
					}


					if((pchTemp = strchr(pszSAFRecordData, LF)) != NULL)
					{
						*pchTemp = 0x00;
					}

					pchTemp = strchr(pszSAFRecordData, '=');
					if(pchTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: It is invalid Time Stamp line", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						if(Fh != NULL)
						{
							fclose(Fh);
						}
						if(FhTemp != NULL)
						{
							fclose(FhTemp);
						}
						if(pszSAFRecordData != NULL)
						{
							free(pszSAFRecordData);
						}
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE; //FIXME: What to do here
					}
				}

				memset(szRecordTimeStamp, 0x00, sizeof(szRecordTimeStamp));
				strncpy(szRecordTimeStamp, pchTemp+1, 10); //dd-mm-yyyy

				debug_sprintf(szDbgMsg, "%s: Time Stamp of the Record is %s", __FUNCTION__, szRecordTimeStamp);
				APP_TRACE(szDbgMsg);

				iDays = getDiffbetweenDates(szRecordTimeStamp);

				if(iDays == FAILURE)
				{
					debug_sprintf(szDbgMsg, "%s: Error while getting the Difference between dates %d", __FUNCTION__, iDays);
					APP_TRACE(szDbgMsg);
					if(Fh != NULL)
					{
						fclose(Fh);
					}
					if(FhTemp != NULL)
					{
						fclose(FhTemp);
					}
					if(pszSAFRecordData != NULL)
					{
						free(pszSAFRecordData);
					}
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}

				if(iDays > iSAFPurgeDays)
				{
					debug_sprintf(szDbgMsg, "%s: Current SAF record age[%d] is > SAF Purge Days[%d], need to purge", __FUNCTION__, iDays, iSAFPurgeDays);
					APP_TRACE(szDbgMsg);

					iNeedToPurgeSAFRec = 1;

					debug_sprintf(szDbgMsg, "%s: Set the SAF Purge Required Flag", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Current SAF record age[%d] is <= SAF Purge Days[%d], no need to see further", __FUNCTION__, iDays, iSAFPurgeDays);
					APP_TRACE(szDbgMsg);

					iPurgeFilePosition = ifilePosition; //Need to purge above records

					debug_sprintf(szDbgMsg, "%s: Position of Purge File Location is %d", __FUNCTION__, iPurgeFilePosition);
					APP_TRACE(szDbgMsg);

					iPurgedRecordsCount = iPurgedRecordsCount + iRecordsCount;
					fPurgedRecordsAmount = fPurgedRecordsAmount + fRecordsAmount;

					debug_sprintf(szDbgMsg, "%s: Updated: PurgedRecordscount[%d], PurgedRecordsAmount[%lf]", __FUNCTION__, iPurgedRecordsCount, fPurgedRecordsAmount);
					APP_TRACE(szDbgMsg);

					iRecordsCount = 0;
					fRecordsAmount = 0.0;

					break;
				}
			}

			if(iNeedToPurgeSAFRec == 1 && iPurgeFilePosition != -1)
			{
				debug_sprintf(szDbgMsg, "%s: Need to Purge the SAF records", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = fseek(Fh , iPurgeFilePosition , SEEK_SET);

				if(iRetVal != 0)
				{
					debug_sprintf(szDbgMsg, "%s: Error while seeking the file position ", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(Fh != NULL)
					{
						fclose(Fh);
					}
					if(FhTemp != NULL)
					{
						fclose(FhTemp);
					}
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}

				while(1)
				{
					iReadBytes = fread(szFileBuffer, 1, sizeof(szFileBuffer), Fh);

					if(iReadBytes > 0)
					{
						iWriteBytes = fwrite(szFileBuffer, sizeof(char), iReadBytes, FhTemp);

						if(iWriteBytes != iReadBytes)
						{
							debug_sprintf(szDbgMsg, "%s: Error while writing the records to Temp SAF records!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							if(Fh != NULL)
							{
								fclose(Fh);
							}
							if(FhTemp != NULL)
							{
								fclose(FhTemp);
							}
							if(pszSAFRecordData != NULL)
							{
								free(pszSAFRecordData);
							}
							releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
							return FAILURE;
						}
						debug_sprintf(szDbgMsg, "%s: Successfully written to Temp file", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else if(feof(Fh))
					{
						debug_sprintf(szDbgMsg, "%s: End of file, nothing to read more", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: File Read Error!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						if(Fh != NULL)
						{
							fclose(Fh);
						}
						if(FhTemp != NULL)
						{
							fclose(FhTemp);
						}
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}

				if(Fh != NULL)
				{
					fclose(Fh);
					Fh = NULL;
				}
				if(FhTemp != NULL)
				{
					fclose(FhTemp);
					FhTemp = NULL;
				}

				//Moving the temporary SAF record to original file
				memset(szTempBuf, 0x00, sizeof(szTempBuf));
				sprintf(szTempBuf, "mv -f %s %s", SAF_RECORD_FILE_NAME_TEMP, SAF_COMPLETED_RECORDS_FILE_NAME); //copying the file
				debug_sprintf(szDbgMsg,"%s - The command to be executed is [%s]", __FUNCTION__,szTempBuf);
				APP_TRACE(szDbgMsg);
				iRetVal = local_svcSystem(szTempBuf);

				if(iRetVal == 0)
				{
					debug_sprintf(szDbgMsg,"%s - Successfully executed the command", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iRetVal = SUCCESS;
				}
				else
				{
					debug_sprintf(szDbgMsg,"%s - Error in execution of the command", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iRetVal = FAILURE;
				}
				if(chmod(SAF_COMPLETED_RECORDS_FILE_NAME, 0666) == -1)
				{
					debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

			}
			else if(iNeedToPurgeSAFRec == 1)
			{
				debug_sprintf(szDbgMsg, "%s: Need to purge all the records!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(Fh != NULL)
				{
					fclose(Fh);
					Fh = NULL;
				}
				if(FhTemp != NULL)
				{
					fclose(FhTemp);
					FhTemp = NULL;
				}

				Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "w"); //Create an empty file for output operations. If a file with the same name already exists, its contents are discarded and the file is treated as a new empty file.
				if(Fh!= NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Deleted the contents of the completed SAF Records file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(chmod(SAF_COMPLETED_RECORDS_FILE_NAME, 0666) == -1)
					{
						debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					fclose(Fh);
					Fh = NULL;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while opening the %s FILE", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				/* Daivik : 27/1/2016 - Coverity 67291 - Close File in this path , as it will not be closed */
				if(Fh != NULL)
				{
					fclose(Fh);
					Fh = NULL;
				}
				if(FhTemp != NULL)
				{
					fclose(FhTemp);
					FhTemp = NULL;
				}
				debug_sprintf(szDbgMsg, "%s: No Need to Purge the SAF records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, Nothing to purge", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(iNeedToPurgeSAFRec == 1 && iPurgedRecordsCount > 0)
		{
			debug_sprintf(szDbgMsg,"%s - Before Updation: Total SAF Records[%d], Purged Records Count[%d]", __FUNCTION__, giTotalSAFRecords, iPurgedRecordsCount);
			APP_TRACE(szDbgMsg);

			//Update the SAF Details File
			giTotalSAFRecords = giTotalSAFRecords - iPurgedRecordsCount;

			debug_sprintf(szDbgMsg,"%s - After Updation: Total SAF Records[%d]", __FUNCTION__, giTotalSAFRecords);
			APP_TRACE(szDbgMsg);

			iRetVal = updateSAFDataToFile();
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Updated the SAF Details file after updating Number of records and Total SAF amount", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iRetVal = SUCCESS;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//Praveen_P1: what to do here!!!!
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: %s does not exist, Nothing to Purge", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
		APP_TRACE(szDbgMsg);
	}

	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}
	// CID-67415: 25-Jan-16: MukeshS3: This temp file may not be closed because of breaking from while loop for some failure cases
	// So, closing this temporary file before leaving from this function.
	if(FhTemp != NULL)
	{
		fclose(FhTemp);
		FhTemp = NULL;
	}
	debug_sprintf(szDbgMsg, "%s - Returning with %d ", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: updateSAFRecordStatus
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int updateSAFRecordStatus(char *pszSAFRecordStatus, int iAddPWCDetails)
{
	FILE 	*FhPerm;
	FILE 	*FhTemp;
	FILE 	*FhPerm1;
	int		iRetVal            	= SUCCESS;
	int		iRecordLen   	   	= 0;
	int		iTotalRecLen	   	= 0;
	int 	iPreviousLen      	= 0;
	int		iIndex       	   	= 0;
	int		iAppLogEnabled	 	= isAppLogEnabled();
	char 	szAppLogData[300]	= "";
	char	*pszSAFRecordData  	= NULL;
	char	*pchTemp		   	= NULL;
	char	*pchEndPos		   	= NULL;
	char	*pszTempSAFRecord  	= NULL;
	double	fCurrentTranAmount 	= 0.0;
	char	szTempBuf[100]     	= "";
	//long	ifilePosition      	= 0;
	char	szRecordLength[15] 	= "";
	int		iPWCDetailsLength  	= 0;

#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRecordStatus == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Incoming parameter is NULL, nothing to update", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Number of SAF records %d TOR SAF Records %d", __FUNCTION__, giTotalSAFRecords, giTotalSAFTORRecords);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Serial number of the SAF record to change is  %d", __FUNCTION__, giSAFRecordSerialNum);
	APP_TRACE(szDbgMsg);

	/* MukeshS3: 7-nov-16: This function has been used at multiple places like while reading the new SAF records from SAFRecords.db & Updating
	 * the status (PROCESSED/NOT_PROCESSED) after posting to host. The first part where the SAF records is being loaded into the stack must include the
	 * operation "changing the SAF status to IN_PROCESS" at the same time without releasing the mutex lock over the SAF files.
	 * So, the mutex lock mechanism has been moved out side & must be used in the Caller function in future uses
	 *
	 */
	//acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	FhPerm = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
	if(FhPerm==NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening the SAF original record file!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		//releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
		return ERR_FILE_NOT_FOUND;
	}

	/* Open file for output at the end of a file. Output operations always write data at the end of the file, expanding it.
	 * Repositioning operations (fseek, fsetpos, rewind) are ignored. The file is created if it does not exist
	 */
	FhPerm1 = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "a+");
	if(FhPerm1==NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening the SAF Completed original record file!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		fclose(FhPerm);
		//releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
		return ERR_FILE_NOT_FOUND;
	}

	if(chmod(SAF_COMPLETED_RECORDS_FILE_NAME, 0666) == -1)
	{
		debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	/* Open file for output at the end of a file. Output operations always write data at the end of the file, expanding it.
	 * Repositioning operations (fseek, fsetpos, rewind) are ignored. The file is created if it does not exist
	 */
	FhTemp = fopen(SAF_RECORD_FILE_NAME_TEMP, "a+");
	if(FhTemp==NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening the SAF Temporary record file!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		fclose(FhPerm);
		fclose(FhPerm1);
		//releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
		return ERR_FILE_NOT_FOUND;
	}

	iPWCDetailsLength =  sizeof(stHostRespDtls) + getPassThrghRespLen();

	for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
	{
		debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iRecordLen = getRecordLength(FhPerm);

		memset(szRecordLength, 0x00, sizeof(szRecordLength));
		sprintf(szRecordLength, "%d", iRecordLen);

		debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
		APP_TRACE(szDbgMsg);

		if(iRecordLen == 0)
		{
			debug_sprintf(szDbgMsg, "%s: No more records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		iTotalRecLen = iRecordLen + strlen(szRecordLength)/*For the length*/ + 1 /*For PIPE after length*/ + iPWCDetailsLength + 10; /*//1 for <LF>, 9 for buffer, for updating the status, new status length could be greater than the existing one..allocation extra bytes for that sake */

		if(iPreviousLen < iTotalRecLen)
		{
			//Allocate the memory to hold the SAF record
			pchTemp = (char *)realloc(pszSAFRecordData, iTotalRecLen);
			if(pchTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm);
				fclose(FhPerm1);
				fclose(FhTemp); //Closing the file before returning
				//releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			else
			{
				memset(pchTemp, 0x00, iTotalRecLen);
				pszSAFRecordData = pchTemp;
			}
			iPreviousLen = iTotalRecLen;
		}
		else
		{
			memset(pszSAFRecordData, 0x00, iPreviousLen);
		}

		//fseek(FhPerm, ifilePosition, SEEK_SET); //Going to starting of the line

		if( fgets(pszSAFRecordData, iTotalRecLen, FhPerm) == NULL)
		{
			if(feof(FhPerm))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record %d from the file", __FUNCTION__, iIndex);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm);
				fclose(FhPerm1);
				fclose(FhTemp); //Closing the file before returning
				// CID-67306: 29-Jan-16: MukeshS3: Freeing unsed memory before abnormal return
				free(pszSAFRecordData);
				//releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
		}

		if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iIndex = iIndex -1; //Time Stamp lines are not part of the Total number of records, need to read more

			continue;
		}

		debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		pchEndPos = strchr(pszSAFRecordData, SEMI_COLON);

		if(pchEndPos == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain SEMI COLON in the SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			fclose(FhPerm);
			fclose(FhPerm1);
			fclose(FhTemp); //Closing the file before returning
			//releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return FAILURE;
		}

		//*pchEndPos = 0x00; //ArjunU1: Don't nullyfy this here since we need end indicator while updating the SAF Records.
		pchEndPos++; //Pointing to LF
		*pchEndPos = 0x00; //Nullyfying the LF also

#ifdef DEVDEBUG
		if(strlen(pszSAFRecordData) < 5000)
		{
			debug_sprintf(szDbgMsg, "%s: SAF record from the file is [%s]", __FUNCTION__, pszSAFRecordData);
			APP_TRACE(szDbgMsg);
		}
#endif

		if(giSAFRecordSerialNum == iIndex)
		{
			debug_sprintf(szDbgMsg, "%s: Status of this SAF record needs to updated/changed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = updateSAFRecord(pszSAFRecordStatus, pszSAFRecordData, iAddPWCDetails);

			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated the status of the SAF record", __FUNCTION__);
				APP_TRACE(szDbgMsg);
#ifdef DEVDEBUG
				if(strlen(pszSAFRecordData) < 5000)
				{
					debug_sprintf(szDbgMsg, "%s: SAF Record after updating status [%s]", __FUNCTION__, pszSAFRecordData);
					APP_TRACE(szDbgMsg);
				}
#endif
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating the status of the SAF record", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//FIXME: What to do here
			}

			fCurrentTranAmount = getTransactionAmount(pszSAFRecordData);
			debug_sprintf(szDbgMsg, "%s: Transaction Amount of this record is %lf", __FUNCTION__, fCurrentTranAmount);
			APP_TRACE(szDbgMsg);
		}

		/* MukeshS3: 13/10/16:
		 * If there are any <LF> characters present in the SAF record, these must be removed before writing the records in the file.
		 * These characters can be expected from POS with any XML field becuase application doesn't strip the POS requests for any whitespace or new line character.
		 * So, it is always safe to remove any <LF> characters present in between.
		 */
		strReplaceAll(pszSAFRecordData, "\n", "");

		pszTempSAFRecord = (char *)malloc(sizeof(char) * (iTotalRecLen + 10) /* Buffer */);
		if(pszTempSAFRecord == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while allocating memory for the Temp SAF record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			fclose(FhPerm);	// CID-67460: 22-Jan-16: MukeshS3:
			fclose(FhPerm1);
			fclose(FhTemp); //Closing the file before returning
			//releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return FAILURE;
		}
		memset(pszTempSAFRecord, 0x00,  iTotalRecLen + 10);
		//sprintf(pszTempSAFRecord, "%d%c%s%c", strlen(pszSAFRecordData) + 1 /* For the ;*/ + 1 /*For the <LF>*/, PIPE, pszSAFRecordData, LF /*New Line Field*/);
		sprintf(pszTempSAFRecord, "%d%c%s%c", strlen(pszSAFRecordData) + 1 /*For the <LF>*/, PIPE, pszSAFRecordData, LF /*New Line Field*/);
		memset(pszSAFRecordData, 0x00, iTotalRecLen);
		memcpy(pszSAFRecordData, pszTempSAFRecord, strlen(pszTempSAFRecord));
		free(pszTempSAFRecord); //Freeing temp SAF record
#if 0
		debug_sprintf(szDbgMsg, "%s: Entire SAF record [%s]", __FUNCTION__, pszSAFRecordData);
		APP_TRACE(szDbgMsg);
#endif

		//if(stSAFRecord.iSAFRecordSerialNum == iIndex) //Writing Completed/processed transactions to the different file
		//Praveen_P1: TO DO : Need to include correct one
		if(giSAFRecordSerialNum == iIndex) //Writing Completed/processed transactions to the different file
		{
			//Update the temporary file
			iRecordLen = strlen(pszSAFRecordData);

			/*AjayS2: 18 Aug 2016: No need of Adding TOR SAF check here as for TOR the status of SAF will be PENDING always until posted to Host.
			 */
			if(strcmp(pszSAFRecordStatus, "IN_PROCESS") == 0) //so it should go to Eligible SAF records file
			{
				iRetVal = fwrite(pszSAFRecordData, sizeof(char), iRecordLen, FhTemp);
				if(iRetVal == iRecordLen)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully written SAF record of index %d to the temporary file ", __FUNCTION__, iIndex);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Failure while writing SAF record to the file, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLen, iRetVal);
					APP_TRACE(szDbgMsg);
					//Praveen_P1: What to do here???
				}
			}
			else //so it should go to Completed SAF records file
			{
				iRetVal = fwrite(pszSAFRecordData, sizeof(char), iRecordLen, FhPerm1);
				if(iRetVal == iRecordLen)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully appended processed SAF record of index %d to the Processed SAF record file ", __FUNCTION__, iIndex);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Failure while writing SAF record to the file, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLen, iRetVal);
					APP_TRACE(szDbgMsg);
					//Praveen_P1: What to do here???
				}
				/*AjayS2: 23 Aug 2016
				 * pszSAFRecordData is pointing to length, Skipping one pipe to point pchEndPos to STATUS
				 */
				pchEndPos = strchr(pszSAFRecordData, PIPE);
				if(pchEndPos != NULL)
				{
					pchEndPos = pchEndPos + 1;
				}
				iRetVal = getSAFRecordStatus(pchEndPos);
				//Amount Updataion Not needed for TOR SAF Records
				if(!(iRetVal == SAF_TOR_STATUS_PENDING || iRetVal == SAF_TOR_STATUS_NOTFOUND || iRetVal == SAF_TOR_STATUS_NOTALLOWED || iRetVal == SAF_TOR_STATUS_REVERSED))
				{
					debug_sprintf(szDbgMsg, "%s: Before Updation: Total SAF amount[%lf], Processed SAF record Amount[%lf] ", __FUNCTION__, gfTotalSAFAmount, fCurrentTranAmount);
					APP_TRACE(szDbgMsg);

					//Need to update the Current SAF total amount, need to substrate this record amount from the
					gfTotalSAFAmount = gfTotalSAFAmount - fCurrentTranAmount;

					/*AjayS2: 24 Aug 2016, Due to some limitations in IEEE representations of floating numbers,
					 * If gfTotalSAFAmount and fCurrentTranAmount are equal, we are getting [-0.00], (instead of 0.00) on subtracting them,
					 * so we have added below check to remove that negative zero.
					 * http://stackoverflow.com/questions/9657993/negative-zero-in-c
					 */
					if(gfTotalSAFAmount == 0)// negative zero and positive zero, both will satisfy this
					{
						gfTotalSAFAmount = 0;//setting to positive zero
					}

					giTotalEligibleSAFRecords = giTotalEligibleSAFRecords - 1;

					debug_sprintf(szDbgMsg, "%s: Total SAF amount after updation [%lf] ", __FUNCTION__, gfTotalSAFAmount);
					APP_TRACE(szDbgMsg);

					if(gfTotalSAFAmount < 0)//gfTotalSAFAmount as Positive zero, will fail this condition
					{
						debug_sprintf(szDbgMsg, "%s: SAF Total amount can never go to negative... ERROR!!!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//Praveen_P1: FIXME: What to so here!!!
					}
				}
				else
				{
					giTotalPendingSAFTORRecords = giTotalPendingSAFTORRecords - 1;
				}
				iRetVal = updateSAFDataToFile();
				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully Updated the SAF Details file after updating  SAF total amount", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					//iRetVal = SAF_RECORD_FOUND;
					/* Daivik : 8/2/2016 - Coverity 67262 - The above statement is perhaps not required since this iRetval is no where
					 * checked with SAF_RECORD_FOUND value in this function or by the caller.
					 */
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					//Praveen_P1: what to do here!!!!
				}

			}
		}
		else
		{
			//Update the temporary file
			iRecordLen = strlen(pszSAFRecordData);

			iRetVal = fwrite(pszSAFRecordData, sizeof(char), iRecordLen, FhTemp);

			if(iRetVal == iRecordLen)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully written SAF record of index %d to the temporary file ", __FUNCTION__, iIndex);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while writing SAF record to the file, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLen, iRetVal);
				APP_TRACE(szDbgMsg);
				//Praveen_P1: What to do here???
			}
		}

	}

	if(FhPerm != NULL)
	{
		fclose(FhPerm);
	}
	if(FhTemp != NULL)
	{
		fclose(FhTemp);
	}
	if(FhPerm1 != NULL)
	{
		fclose(FhPerm1);
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	//Moving the temporary SAF record to original file
	memset(szTempBuf, 0x00, sizeof(szTempBuf));
	sprintf(szTempBuf, "mv -f %s %s", SAF_RECORD_FILE_NAME_TEMP, SAF_ELIGIBLE_RECORDS_FILE_NAME); //copying the file
	debug_sprintf(szDbgMsg,"%s - The command to be executed is [%s]", __FUNCTION__,szTempBuf);
	APP_TRACE(szDbgMsg);
	iRetVal = local_svcSystem(szTempBuf);

	if(iRetVal == 0)
	{
		debug_sprintf(szDbgMsg,"%s - Successfully executed the command", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SUCCESS;

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Successfully Updated the SAF Record Status");
			addAppEventLog(SCA, PAAS_INFO, PROCESSED, szAppLogData, NULL);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s - Error in execution of the command", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = FAILURE;

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Error While Updating the SAF Record Status. Execution of Command Failed.");
			addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
		}
	}

	if(chmod(SAF_ELIGIBLE_RECORDS_FILE_NAME, 0666) == -1)
	{
		debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	//releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: updateSAFRecord
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int updateSAFRecord(char *pszSAFRecordStatus, char *pszSAFRecord, int iAddPWCDetails)
{
	int				iRetVal      	 	 	= SUCCESS;
	int				iPWCDetailsLength	 	= 0;
	char			*pchFirst        	 	= NULL;
	char			*pchStart        	 	= NULL;
	char			*pchLast         		= NULL;
	char			*pchNext		 		= NULL;
	char			*curPtr				    = NULL;
	char			*nxtPtr				    = NULL;
	char			*pszTempSAFRecord		= NULL;
	int				iRecordLength    		= 0;
	char			szPWCDetails[200] 		= "";
	char			szSAFRecordNumber[20] 	= "";
	//char 			szAuthCode[20]		  	= "";
	char			szMaskedPAN[30]		  	= "";
	char			szClearPAN[30]		  	= "";
	char 			szBusinessDate[9]		= "";
	char 			szCmdType[21]			= "";
	PAAS_BOOL		bLastFieldFound			= PAAS_FALSE;
#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT|BAR_CODE|PIN_CODE|APR_TYPE|INIT_VECTOR|PASS_THROUGH_FIELDS
	 * ||||;
	 *
	 */
	iPWCDetailsLength =  sizeof(stHostRespDtls) + getPassThrghRespLen();

	pszTempSAFRecord = (char *)malloc(sizeof(char) * (strlen(pszSAFRecord) + strlen(pszSAFRecordStatus) + iPWCDetailsLength)); //allocating few bytes more for the safety side
	if(pszTempSAFRecord == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while allocating memory for the Temp SAF record", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(iAddPWCDetails == 1)
	{
		debug_sprintf(szDbgMsg, "%s: PROCESSED record", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/*-------------- Getting SAF Status Field -----------------*/
		pchFirst = strchr(pszSAFRecord, PIPE);
		if(pchFirst == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			free(pszTempSAFRecord);
			return FAILURE;
		}
		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Number field

		/*-------------- Getting SAF Record Number -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			free(pszTempSAFRecord);
			return FAILURE;
		}
		memset(szSAFRecordNumber, 0x00, sizeof(szSAFRecordNumber));
		strncpy(szSAFRecordNumber, pchFirst, (pchNext - pchFirst)); //Getting the SAF record number from the file

		pchFirst = pchNext + 1; //pchFirst points to start of the AUTH code field

		/*-------------- Crossing SAF Record AUTH field -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Account Number field

		/*-------------- Getting SAF Record Account Number -----------------*/
		if((*pchFirst) != PIPE) //Record contains Account Number field
		{
			debug_sprintf(szDbgMsg, "%s: Record contains Account Number field", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				free(pszTempSAFRecord);
				return FAILURE;
			}

			curPtr = pchFirst;
			nxtPtr = strchr(curPtr, '#');
			if(nxtPtr != NULL)
			{
				strncpy(szClearPAN, curPtr, (nxtPtr - curPtr));
			}
			else
			{
				strncpy(szClearPAN, pchFirst, (pchNext - pchFirst));
			}

			memset(szMaskedPAN, 0x00, sizeof(szMaskedPAN));

			getMaskedPAN(szClearPAN, szMaskedPAN);

		#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s:%s: Clear PAN[%s], Masked PAN[%s]", DEVDEBUGMSG, __FUNCTION__, szClearPAN, szMaskedPAN);
			APP_TRACE(szDbgMsg);
		#endif

			debug_sprintf(szDbgMsg, "%s: Masked PAN is %s", __FUNCTION__, szMaskedPAN);
			APP_TRACE(szDbgMsg);

			pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Track2 field
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Account Number field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(szMaskedPAN, 0x00, sizeof(szMaskedPAN));
			if(strlen(stHostRespDtls.szAcctNum) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Acct Num From Host Response currenly Exists[%s]", __FUNCTION__, stHostRespDtls.szAcctNum);
				APP_TRACE(szDbgMsg);
				strcpy(szClearPAN, stHostRespDtls.szAcctNum);
				getMaskedPAN(szClearPAN, szMaskedPAN);
			}
			pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Track2 field
		}

		/*-------------- Crossing SAF Record Track2 data -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		if(strlen(szMaskedPAN) > 0)
		{
			sprintf(pszTempSAFRecord, "%s%c%s%c%c%s%c%c", pszSAFRecordStatus, PIPE, szSAFRecordNumber, PIPE, PIPE, szMaskedPAN, PIPE, PIPE);
		}
		else
		{
			//sprintf(pszTempSAFRecord, "%s%c%s%c%c%c%c%c", pszSAFRecordStatus, PIPE, szSAFRecordNumber, PIPE, PIPE, PIPE, PIPE, PIPE);
			sprintf(pszTempSAFRecord, "%s%c%s%c%c%c%c", pszSAFRecordStatus, PIPE, szSAFRecordNumber, PIPE, PIPE, PIPE, PIPE);
		}

	#ifdef DEVDEBUG
		if(strlen(pszTempSAFRecord) < 5000)
		{
			debug_sprintf(szDbgMsg, "%s: SAF Temp record adding till PAN is [%s]", __FUNCTION__, pszTempSAFRecord);
			APP_TRACE(szDbgMsg);
		}
	#endif

		pchStart = pchFirst ; //pchStart will be pointing to Expiry Month

		/*-------------- Crossing SAF Record Expiry Month -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Crossing SAF Record Expiry Year -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		pchLast = pchNext;

		strncat(pszTempSAFRecord, pchStart, pchLast - pchStart);

		/*-------------- Crossing SAF Record Payment Media -----------------*/
		strcat(pszTempSAFRecord, "|");
		pchNext = strchr(pchFirst, PIPE);
		if(strlen(stHostRespDtls.szPaymentMedia) > 0)
		{
			debug_sprintf(szDbgMsg, "%s: PaymentMedia From Host Response/Pymt media currenly Existing[%s]", __FUNCTION__, stHostRespDtls.szPaymentMedia);
			APP_TRACE(szDbgMsg);
			strcat(pszTempSAFRecord, stHostRespDtls.szPaymentMedia);
		}
		else
		{
			char szPymtMedia[21] = "";
			strncpy(szPymtMedia, pchFirst, pchNext - pchFirst);
			debug_sprintf(szDbgMsg, "%s: szPymtMedia data [%s]", __FUNCTION__, szPymtMedia);
			APP_TRACE(szDbgMsg);
			strcat(pszTempSAFRecord, szPymtMedia);
		}
		strcat(pszTempSAFRecord, "|");
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Card Source

#ifdef DEVDEBUG
	if(strlen(pszTempSAFRecord) < 5000)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Temp record adding till Payment media is [%s]", __FUNCTION__, pszTempSAFRecord);
		APP_TRACE(szDbgMsg);
	}
#endif

		pchStart = pchFirst ; //pchStart will be pointing to Card Source

		/*-------------- Crossing SAF Record Card Source -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Crossing SAF Record Duplicate flag -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Crossing SAF Record Transaction amount -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Tip Amount field

		/*-------------- Crossing SAF Record TIP amount -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Crossing SAF Record TAX amount -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Crossing SAF Record Payment Type -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Invoice field

		/*-------------- Crossing SAF Record Invoice -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Purchase ID field

		/*-------------- Crossing SAF Record Purchase ID -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Crossing SAF Record Cashier ID -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*
		 * Praveen_P1: By this following we are missing out one field, FIX for the following issue
		 * SAF query is falling for the PROCESSED records
		 */
		//pchLast = pchNext - 1; //pchLast pointing to last char of the Cashier number

		pchLast = pchNext;

	#ifdef DEVDEBUG
		if( strlen(pchStart) < 5000)
		debug_sprintf(szDbgMsg, "%s:%s: SAF record from status is [%s]", DEVDEBUGMSG, __FUNCTION__, pchStart);
		APP_TRACE(szDbgMsg);
	#endif

		strncat(pszTempSAFRecord, pchStart, pchLast - pchStart);

	#ifdef DEVDEBUG
		if(strlen(pszTempSAFRecord) < 5000)
		{
			debug_sprintf(szDbgMsg, "%s:%s: SAF Temp record, adding till cashier num is [%s]", DEVDEBUGMSG, __FUNCTION__, pszTempSAFRecord);
			APP_TRACE(szDbgMsg);
		}
	#endif

		debug_sprintf(szDbgMsg, "%s: Need to update the SAF record with the PWC details", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		iRecordLength = strlen(pszTempSAFRecord);

		/* We need to Get the Business date if present */
		/*-------------- Crossing SAF MIME TYPE -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Crossing SAF Signature data -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Crossing SAF Response Code-----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Crossing SAF Ctroutd-----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Crossing SAF Troutd -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Crossing SAF AuthCode -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext != NULL)
		{
			pchFirst = pchNext + 1;

			/*-------------- Crossing SAF Encryption type -----------------*/
			pchNext = strchr(pchFirst, PIPE);
			pchFirst = pchNext + 1;

			/*-------------- Crossing SAF Encryption Payload -----------------*/
			pchNext = strchr(pchFirst, PIPE);
			pchFirst = pchNext + 1;

			/*-------------- Crossing SAF Card Token -----------------*/
			pchNext = strchr(pchFirst, PIPE);
			pchFirst = pchNext + 1;

			/*-------------- Crossing SAF CVV2_CODE -----------------*/
			pchNext = strchr(pchFirst, PIPE);
			pchFirst = pchNext + 1;

			/*-------------- Crossing SAF Bank User Data -----------------*/
			pchNext = strchr(pchFirst, PIPE);
			pchFirst = pchNext + 1;

			/*-------------- Getting SAF Record Business Date -----------------*/
			if((*pchFirst) != SEMI_COLON || (*pchFirst) != PIPE) //Record contains business date
			{
				/*Note: Adding some more fields to SAF record after Business Date. Hence looking for PIPE character.
				 * 		For backward compatibility we are also looking for semi colon.
				 */
				debug_sprintf(szDbgMsg, "%s: Record contains Business date field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = strchr(pchFirst, PIPE);
				if( pchNext == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Going to look for semi colon", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					//Set the flag to indicate its last field.
					bLastFieldFound = PAAS_TRUE;

					if( (pchNext = strchr(pchFirst, SEMI_COLON)) == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Does not contain SEMICOLON(;) in the data!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						free(pszTempSAFRecord);
						return FAILURE;
					}
				}
				memset(szBusinessDate , 0x00, sizeof(szBusinessDate));
				strncpy(szBusinessDate, pchFirst, (pchNext - pchFirst));

				pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Transaction POS entry mode field
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Record does not contain Business date field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				memset(szBusinessDate, 0x00, sizeof(szBusinessDate));
				pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Txn POS Entry Mode field
			}

			if( bLastFieldFound == PAAS_FALSE)
			{
				/*-------------- Crossing SAF Record PINLess Debit -----------------*/
				pchNext = strchr(pchFirst, PIPE);
				pchFirst = pchNext + 1;

				/*-------------- Crossing SAF Record Txn POS Entry Mode -----------------*/
				pchNext = strchr(pchFirst, PIPE);
				pchFirst = pchNext + 1;

				/*-------------- Crossing SAF Record Merchant Id -----------------*/
				pchNext = strchr(pchFirst, PIPE);
				pchFirst = pchNext + 1;

				/*-------------- Crossing SAF Record Terminal Id -----------------*/
				pchNext = strchr(pchFirst, PIPE);
				pchFirst = pchNext + 1;

				/*-------------- Crossing SAF Record Lane Id -----------------*/
				pchNext = strchr(pchFirst, PIPE);
				pchFirst = pchNext + 1;

				/*-------------- Crossing SAF Record Approved Amount -----------------*/
				pchNext = strchr(pchFirst, PIPE);
				pchFirst = pchNext + 1;

				/*-------------- Crossing SAF Record Tax Indicator -----------------*/
				pchNext = strchr(pchFirst, PIPE);
				pchFirst = pchNext + 1;

				/*-------------- Getting SAF Record Command Type -----------------*/
				if((*pchFirst) != PIPE) //Record contains command type field
				{
					debug_sprintf(szDbgMsg, "%s: Record contains command type field", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						free(pszTempSAFRecord);
						return FAILURE;
					}
					memset(szCmdType, 0x00, sizeof(szCmdType));
					strncpy(szCmdType, pchFirst, (pchNext - pchFirst));

					debug_sprintf(szDbgMsg, "%s: Command Type[%s]", __FUNCTION__, szCmdType);
					APP_TRACE(szDbgMsg);

					pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Track Indicator
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Record does not contain command type field, Updating command type as SALE", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					//If Command Type is not present, update default command type as SALE.
					memset(szCmdType, 0x00, sizeof(szCmdType));
					sprintf(szCmdType, "%d", SSI_SALE);
					pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Track Indicator
				}

				//ADD SAF RECORDS: Write code here to update SAF records fields which we have to send it POS in future.
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Last field is Business Date", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		//iRecordLength = iRecordLength - 3; /* 3 for Auth Code, Troutd Id, Ctroutd and Resp code*/

		memset(pszSAFRecord, 0x00, strlen(pszSAFRecord));
		memcpy(pszSAFRecord, pszTempSAFRecord, iRecordLength); //Contains from the status till the cashier number

	#ifdef DEVDEBUG
		if(strlen(pszTempSAFRecord) < 5000)
		{
			debug_sprintf(szDbgMsg, "%s:%s: SAF record before adding SSI details [%s]", DEVDEBUGMSG, __FUNCTION__, pszSAFRecord);
			APP_TRACE(szDbgMsg);
		}
	#endif

		//|||<RESP_CODE>|<CTROUTD>|<TROUTD>|<AUTH_CODE>  //First empty ones for the MIME type and the signature data
		memset(szPWCDetails, 0x00, sizeof(szPWCDetails));
		sprintf(szPWCDetails, "%c%c%c%s%c%s%c%s%c%s", PIPE, PIPE, PIPE, stHostRespDtls.szPWCSAFResponseCode, PIPE, stHostRespDtls.szPWCSAFCTroutd,
													  PIPE, stHostRespDtls.szPWCSAFTroutd, PIPE, stHostRespDtls.szPWCSAFAuthCode);

		//|| Empty ones for the encryption type and encryption payload.
		strcat(szPWCDetails, "||");

		//Add Card token dtls
		strcat(szPWCDetails, "|");

		/* Format of the SAF Record
		 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
		 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
		 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
		 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
		 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
		 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
		 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT|BAR_CODE|PIN_CODE|APR_TYPE|INIT_VECTOR|PASS_THROUGH_FIELDS
		 * ||||;
		 *
		 */

		/* ArjunU1: We need to add some patch up code here to store card token. Since card token may contain
		 * 			PIPE separator between card source and id. So we can't store PIPE separator value
		 * 			because we use PIPE character between SAF fields. For instance, <SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|...so on.
		 */

#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s:%s: Card Token [%s]", DEVDEBUGMSG, __FUNCTION__,  stHostRespDtls.szPWCSAFCardToken);
		APP_TRACE(szDbgMsg);
#endif

		curPtr = stHostRespDtls.szPWCSAFCardToken;
		if( (nxtPtr = strchr(curPtr, '|')) != NULL ) //Card token doesn't contain PIPE separator between card source and id
		{
			//Replace the PIPE character from card token data by "=" separator.
			*nxtPtr = '/';
		}
		else	//PIPE character not found in the card token data.
		{
			//Do nothing.
		}
		strcat(szPWCDetails, stHostRespDtls.szPWCSAFCardToken);

		strcat(pszSAFRecord, szPWCDetails);

		//Add CVV2 CODE to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szPWCSAFCVVCode) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szPWCSAFCVVCode);
		}

		//Add Bank User Data to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szPWCSAFBankUserData) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szPWCSAFBankUserData);
		}

		//Add Business Date to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(szBusinessDate) > 0)
		{
			strcat(pszSAFRecord, szBusinessDate);
		}

		//Place holder for PINLess Debit to the SAF Record
		strcat(pszSAFRecord, "|");

		//Add Txn POS Entry Mode to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szTxnPOSEntryMode) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szTxnPOSEntryMode);
		}

		//Add Merchant Id to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szMerchId) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szMerchId);
		}

		//Add Terminal Id to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szTermId) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szTermId);
		}
		//Add Lane Id to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szLaneId) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szLaneId);
		}

		//Add Approved Amount to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szApprvdAmt) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szApprvdAmt);
		}

		//Place holder for TAX_IND.
		strcat(pszSAFRecord, "|");

		//Add Command Type to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(szCmdType) > 0)
		{
			strcat(pszSAFRecord, szCmdType);
		}

		//Place holder for Track Indicator
		strcat(pszSAFRecord, "|");

		//Add Store Id to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szStoreId) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szStoreId);
		}

		//Place holder for CUSTOMER_ZIP.
		strcat(pszSAFRecord, "|");

		//Place holder for CUSTOMER_STREET.
		strcat(pszSAFRecord, "|");

		//Place holder for DISCOUNT_AMOUNT, DUTY_AMOUNT, FREIGHT_AMOUNT, DEST_COUNTRY_CODE, DEST_POSTAL_CODE, SHIP_FROM_ZIP_CODE, RETAIL_ITEM_DESC_1, ALT_TAX_ID,CUSTOMER_CODE
		strcat(pszSAFRecord, "|||||||||");


		/* Format of the SAF Record
		 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
		 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
		 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
		 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
		 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
		 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
		 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT|BAR_CODE|PIN_CODE|APR_TYPE|INIT_VECTOR|PASS_THROUGH_FIELDS
		 * ||||;
		 *
		 */

		//Place holder for AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC| AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK
		strcat(pszSAFRecord, "|||||||||");

		//Place holder for KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE
		strcat(pszSAFRecord, "||");

		if(strlen(szSSITranKey) > 0)
		{
			int			iPaymentSubType = -1;

			iRetVal = getPymtSubTypeForPymtTran(szSSITranKey, &iPaymentSubType);
			if(iRetVal != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while fetching the Payment Sub Type details ",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = FAILURE;
			}

			debug_sprintf(szDbgMsg, "%s: Payment Sub Type while updating the SAF records [%d] ",__FUNCTION__, iPaymentSubType);
			APP_TRACE(szDbgMsg);

			/*-------- Adding Payment Type ------------ */
			if(iPaymentSubType == PYMT_SUBTYPE_INTERNAL )//Payment Type is available
			{
				strcat(pszSAFRecord, "INTERNAL");
			}
			else if(iPaymentSubType == PYMT_SUBTYPE_EXTERNAL )//Payment Type is available
			{
				strcat(pszSAFRecord, "EXTERNAL");
			}
			else if(iPaymentSubType == PYMT_SUBTYPE_GIFTMALL)
			{
				strcat(pszSAFRecord, "GIFTMALL");
			}
			else if(iPaymentSubType == PYMT_SUBTYPE_INCOMM)
			{
				strcat(pszSAFRecord, "INCOMM");
			}
			else if(iPaymentSubType == PYMT_SUBTYPE_BLACKHAWK)
			{
				strcat(pszSAFRecord, "BLACKHAWK");
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Payment Sub Type is not present in the Transaction, So Not Storing Anything in SAF Record",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: SAF SSI transaction Key Not Present",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		//Place holder for |PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR| PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY
		strcat(pszSAFRecord, "|||||||");

		//ORIG_TRANS_DATE
		strcat(pszSAFRecord, "|");
		if(strlen(stHostRespDtls.szTransDate))
		{
			strcat(pszSAFRecord, stHostRespDtls.szTransDate);
		}

		//ORIG_TRANS_TIME
		strcat(pszSAFRecord, "|");
		if(strlen(stHostRespDtls.szTransTime))
		{
			strcat(pszSAFRecord, stHostRespDtls.szTransTime);
		}
		//Place holder for |CASHBACK_AMNT|BAR_CODE|PIN_CODE|APR_TYPE|INIT_VECTOR|PASS_THROUGH_FIELDS|BATCH_TRACE_ID
		strcat(pszSAFRecord, "|||||||");

		//4 Place holders from extra place holders in SAFrecords file
		strcat(pszSAFRecord, "||||");


		//Add LPToken to the SAF Record
		/*T_RaghavendranR1_21072016: As we have already added the SAFRecord details and the place Holder, we continue adding the Host response details and save it to CompletedRecodsFile
		 * The below strcat statement is commented as we are more extra '|' and the same is looked up while filling the SAFRecord Details.
		 * This will result in some fields mix up during SAF QUERY */
		//strcat(pszSAFRecord, "|");

		if( strlen(stHostRespDtls.szLPToken) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szLPToken);
		}

		//Add PPCV to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szPPCV) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szPPCV);
		}

		//Add AVS_CODE to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szAVSCode) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szAVSCode);
		}

		//Add HOST_RESPCODE to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szHostRespCode) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szHostRespCode);
		}

		//Add SIGNATUREREF to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szSignatureRef) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szSignatureRef);
		}

		//Add APR_TYPE to the SAF record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szAprType) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szAprType);
		}

		//Add ACTION_CODE to the SAF record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szActionCode) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szActionCode);
		}

		//Add SVC_PHONE to the SAF record
		strcat(pszSAFRecord, "|");
		if(strlen(stHostRespDtls.szSvcPhone) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szSvcPhone);
		}

		//Add PURCHASE_APR to the SAF record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szPurchaseApr) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szPurchaseApr);
		}

		//Add STATUS_FLAG to the SAF record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szStatusFlag) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szStatusFlag);
		}

		//Add CREDIT_PLAN_NBR to the SAF record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szCreditPlanNbr) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szCreditPlanNbr);
		}

		//Add RECEIPT_TEXT to the SAF record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szReceiptText) > 0)
		{
			/* T_RaghavendranR1: "PTMX-1562: Point 2.19.26 B1 : SCA is crashing with a segmentation fault when querying for SAF records for CITI private label transactions"
			 * It is noticed with Citi Private label PWC Transactions, the receipt text contains the <CR> or <LF>,
			 * In SAf Completed Records we cannot store with <LF> as it will consider each new line as different record.
			 * getCorrectedReceiptText looks for <CR> or <LF> in the receipt text and removes the same if present. */
			getCorrectedReceiptText(stHostRespDtls.szReceiptText);
			strcat(pszSAFRecord, stHostRespDtls.szReceiptText);
		}

		// MukeshS3: 19-April-16: Add Pass Through Fields to the SAF Record
		strcat(pszSAFRecord, "|");
		if((updatePassThrghRespDtlstoSAFRec(pszSAFRecord)) != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to update Pass through Response details to SAF Records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		//Add AuthNwId to the SAF Record
		strcat(pszSAFRecord, "|");
		if( strlen(stHostRespDtls.szAthNtwID) > 0)
		{
			strcat(pszSAFRecord, stHostRespDtls.szAthNtwID);
		}
		//More fields might be added here to return fields to POS in SAF query.
		//Place holder reserved for future use.
		strcat(pszSAFRecord, "||||");

		//Add Record end character(;) the SAF Record
		strcat(pszSAFRecord, ";");
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: IN_PROCESS record", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = strchr(pszSAFRecord, PIPE);
		if(pchFirst == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			free(pszTempSAFRecord);
			return FAILURE;
		}

		pchStart = pchFirst + 1; //pchStart will be pointing to SAF Status

		sprintf(pszTempSAFRecord, "%s%c%s", pszSAFRecordStatus, PIPE, pchStart);

		debug_sprintf(szDbgMsg, "%s: Need NOT update the SAF record with the PWC details", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		memset(pszSAFRecord, 0x00, strlen(pszSAFRecord));
		memcpy(pszSAFRecord, pszTempSAFRecord, strlen(pszTempSAFRecord));
	}

#ifdef DEVDEBUG
	if(strlen(pszTempSAFRecord) < 5000)
	{
		debug_sprintf(szDbgMsg, "%s:%s: SAF record after changing status and adding SSI details is [%s]", DEVDEBUGMSG, __FUNCTION__, pszSAFRecord);
		APP_TRACE(szDbgMsg);
	}
#endif

	free(pszTempSAFRecord);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: isSAFRecordPresent
 *
 * Description	: This API tells whether SAF record is present in the SAf data system
 *
 *
 * Input Params	: NONE
 *
 * Output Params: TRUE/FALSE
 * ============================================================================
 */
PAAS_BOOL isSAFRecordPresent()
{
	PAAS_BOOL 		iRetVal 		= PAAS_TRUE;
	int		  		rv	  			= SUCCESS;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE	pstPymtDtls		= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	/*
	 * First Check if the SAF SSI Transaction Key has some value
	 */

	if(strlen(szSSITranKey) > 0)
	{
		/*	SSI Tran Key has some value
		 *  Check is the Stack has the value with this key
		 */
#if 0
		if(gfTotalSAFAmount > 0)
		{
			debug_sprintf(szDbgMsg, "%s - SAF Record is present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = PAAS_TRUE;
		}
		else
		{
			//debug_sprintf(szDbgMsg, "%s - SAF Record is NOT present", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
			iRetVal = PAAS_FALSE;
		}
#endif

		/* Checking whether tran is presen in the stack */
		/*
		 * This is one of doing it... we can do based on the SAF amount
		 */
		rv = getTopSSITran(szSSITranKey, &pstTran);
		if(rv != SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s - SAF Record is NOT present", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
			iRetVal = PAAS_FALSE;
		}
		else
		{
			/* get the payment details placeholder */
			pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
			if(pstPymtDtls == NULL)
			{
				//debug_sprintf(szDbgMsg, "%s - SAF Record is NOT present", __FUNCTION__);
				//APP_TRACE(szDbgMsg);
				iRetVal = PAAS_FALSE;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s - SAF Record is present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iRetVal = PAAS_TRUE;
			}

		}
	}
	else
	{
		iRetVal = PAAS_FALSE;
	}

	//debug_sprintf(szDbgMsg, "%s - Returning with %d ", __FUNCTION__, iRetVal);
	//APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: clearRangeofSAFRecords
 *
 * Description	: This API clears given the range of SAF records stored in the database.
 *				  (Clears from memory and the file)
 * Input Params	: Start Record Number, End Record Number
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int clearRangeofSAFRecords(SAFDTLS_PTYPE pstSAFDtls)
{
	int	  iRetVal		   	 = SUCCESS;
	int	  iRetValTemp    	 = SUCCESS;
	double fCurrentAmount 	 = 0;
	double fAmountSpecialCase = 0.0;
	#ifdef DEBUG
		char szDbgMsg[256]	= "";
	#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		//Check if there are any records presently
		if(giTotalSAFRecords == 0)
		{
			debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strcpy(pstSAFDtls->szRecCnt, "0");
			strcpy(pstSAFDtls->szTotAmt, "0.00");
			pstSAFDtls->recList = NULL;
			iRetVal = SAF_RECORD_NOT_FOUND;
			break;
		}
		debug_sprintf(szDbgMsg, "%s:Number of SAF Records present is %d", __FUNCTION__, giTotalSAFRecords);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Need to look for SAF record within the [%s,%s] range", __FUNCTION__, pstSAFDtls->szBegNo, pstSAFDtls->szEndNo);
		APP_TRACE(szDbgMsg);

		//Parse the SAF Record file for any record with in the range to clear them
		iRetVal = clearSAFRecsByRecRange(pstSAFDtls, SAF_ELIGIBLE_RECORDS_FILE_NAME);
		if(iRetVal == SAF_RECORD_FOUND || iRetVal == SAF_REC_PARTIALLY_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: Found the SAF record(s) within the requested SAF record number range in Eligible SAF records", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/*
			 * Need to update the Total SAF amount, total SAF amount is only for the Eligible SAf records
			 * If any record is cleared from the eligible SAf records, need to update the Total
			 * SAF amount.
			 * Updating the global vairable here and writing to the file is done little latter after
			 * updating the number of records also
			 */
			fCurrentAmount = atof(pstSAFDtls->szTotAmt); //Need to update the Total SAF amount
			fAmountSpecialCase = atof(pstSAFDtls->szTotSpecialCaseAmt);

			debug_sprintf(szDbgMsg,"%s - Before Updation: Total SAF amount[%.2lf], Cleared SAF Total Amount[%.2lf]", __FUNCTION__, gfTotalSAFAmount, fCurrentAmount);
			APP_TRACE(szDbgMsg);

			gfTotalSAFAmount = gfTotalSAFAmount - fCurrentAmount + fAmountSpecialCase;
			giTotalEligibleSAFRecords = giTotalEligibleSAFRecords - atoi(pstSAFDtls->szRecCnt);

			debug_sprintf(szDbgMsg,"%s - After Updation: Total SAF amount[%.2lf]", __FUNCTION__, gfTotalSAFAmount);
			APP_TRACE(szDbgMsg);

			if(gfTotalSAFAmount < 0)
			{
				debug_sprintf(szDbgMsg,"%s - Total SAF amount can never be negative!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			if(iRetVal == SAF_REC_PARTIALLY_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Found Some SAF Record in Available range along with IN_PROCESS(IN_FLIGHT) Status, Couldn't delete one Record.", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(iRetVal == SAF_RECORD_NOT_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s) within the requested SAF record number range in file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else if(iRetVal == SAF_RECORD_IN_PROGRESS)
		{
			debug_sprintf(szDbgMsg, "%s: SAF Record in IN_PROCESS Status, Cannot delete the Record.", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while clearing SAF records within the given range", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		//Parse the SAF Record file for any record with in the range to clear them
		iRetValTemp = clearSAFRecsByRecRange(pstSAFDtls, SAF_COMPLETED_RECORDS_FILE_NAME);
		if(iRetValTemp == SAF_RECORD_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: Found the SAF record(s) within the requested SAF record number range in Completed SAF records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else if(iRetValTemp == SAF_RECORD_NOT_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s) within the requested SAF record number range in file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while clearing SAF records within the given range", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(iRetVal == SAF_RECORD_FOUND || iRetValTemp == SAF_RECORD_FOUND || iRetVal == SAF_REC_PARTIALLY_FOUND) //Praveen_P1: FIXME: Revisit this logic and try to make it better
		{
			if(iRetVal != SAF_REC_PARTIALLY_FOUND)
			{
				iRetVal = SAF_RECORD_FOUND;
			}

			//Update the SAF Details File after seeing both the files
			giTotalSAFRecords = giTotalSAFRecords - atoi(pstSAFDtls->szRecCnt);

			debug_sprintf(szDbgMsg,"%s - After Updation: Total SAF Records[%d], Total SAF amount[%.2lf]", __FUNCTION__, giTotalSAFRecords, gfTotalSAFAmount);
			APP_TRACE(szDbgMsg);

			if(updateSAFDataToFile() == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Updated the SAF Details file after resetting SAF Last Offline Epoch Time", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//iRetVal = SAF_RECORD_FOUND;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//Praveen_P1: what to do here!!!!
			}
		}
		else if(iRetVal == SAF_RECORD_IN_PROGRESS)
		{
			debug_sprintf(szDbgMsg, "%s: SAF Record in IN_PROCESS Status, Cannot delete the Record.", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			iRetVal = SAF_RECORD_NOT_FOUND;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: clearOneSAFRecord
 *
 * Description	: This API clears given the SAF record stored in the database.
 *				  (Clears from memory and the file)
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int clearOneSAFRecord(SAFDTLS_PTYPE pstSAFDtls)
{
	int	  iRetVal		  = SUCCESS;
	char  szRecNumber[20] = "";
	double fCurrentAmount  = 0.0;
	double fAmountSpecialCase = 0.0;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	while(1)
	{
		//Check if there are any records presently
		if(giTotalSAFRecords == 0)
		{
			debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strcpy(pstSAFDtls->szRecCnt, "0");
			strcpy(pstSAFDtls->szTotAmt, "0.00");
			pstSAFDtls->recList = NULL;
			iRetVal = SAF_RECORD_NOT_FOUND;
			break;
		}
		debug_sprintf(szDbgMsg, "%s:Number of SAF Records present is %d", __FUNCTION__, giTotalSAFRecords);
		APP_TRACE(szDbgMsg);

		if(strlen(pstSAFDtls->szBegNo) > 0)
		{
			strcpy(szRecNumber, pstSAFDtls->szBegNo);
		}
		else if(strlen(pstSAFDtls->szEndNo) > 0)
		{
			strcpy(szRecNumber, pstSAFDtls->szEndNo);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain the SAF_BEGIN_NUM and SAF_END_NUM", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		debug_sprintf(szDbgMsg, "%s: Need to clear SAF record with %s as the SAF Record Number", __FUNCTION__, szRecNumber);
		APP_TRACE(szDbgMsg);

		//Parse the SAF Record file for any record with the given SAF Record Number
		iRetVal = clearSAFRecsByRecNumber(szRecNumber, SAF_ELIGIBLE_RECORDS_FILE_NAME, pstSAFDtls);
		if(iRetVal == SAF_RECORD_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: Found the SAF record(s) with the requested SAF Record Number i.e. %s and cleared the same SAF record", __FUNCTION__, szRecNumber);
			APP_TRACE(szDbgMsg);
			//Update the SAF Details File

			debug_sprintf(szDbgMsg,"%s - Before Updation: Total SAF Records[%d]", __FUNCTION__, giTotalSAFRecords);
			APP_TRACE(szDbgMsg);

			giTotalSAFRecords = giTotalSAFRecords - 1;
			giTotalEligibleSAFRecords = giTotalEligibleSAFRecords - 1;

			fCurrentAmount = atof(pstSAFDtls->szTotAmt);
			fAmountSpecialCase = atof(pstSAFDtls->szTotSpecialCaseAmt);

			debug_sprintf(szDbgMsg,"%s - Before Updation: Total SAF amount[%.2lf], cleared Tran Amount[%.2lf]", __FUNCTION__, gfTotalSAFAmount, fCurrentAmount);
			APP_TRACE(szDbgMsg);

			gfTotalSAFAmount = gfTotalSAFAmount - fCurrentAmount + fAmountSpecialCase;

			debug_sprintf(szDbgMsg,"%s - After Updation: Total SAF Records[%d], Total SAF amount[%.2lf]", __FUNCTION__, giTotalSAFRecords, gfTotalSAFAmount);
			APP_TRACE(szDbgMsg);

			iRetVal = updateSAFDataToFile();
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Updated the SAF Details file after updating SAF Records and SAF amount", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iRetVal = SAF_RECORD_FOUND;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//Praveen_P1: what to do here!!!!
			}
		}
		else if(iRetVal == SAF_RECORD_NOT_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s) with the requested SAF Record Number i.e. %s in %s file ", __FUNCTION__, szRecNumber, SAF_ELIGIBLE_RECORDS_FILE_NAME);
			APP_TRACE(szDbgMsg);

			iRetVal = clearSAFRecsByRecNumber(szRecNumber, SAF_COMPLETED_RECORDS_FILE_NAME, pstSAFDtls);
			if(iRetVal == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Found the SAF record(s) with the requested SAF Record Number i.e. %s and cleared the same SAF record", __FUNCTION__, szRecNumber);
				APP_TRACE(szDbgMsg);

				//Update the SAF Details File

				debug_sprintf(szDbgMsg,"%s - Before Updation: Total SAF Records[%d]", __FUNCTION__, giTotalSAFRecords);
				APP_TRACE(szDbgMsg);

				giTotalSAFRecords = giTotalSAFRecords - 1;

				debug_sprintf(szDbgMsg,"%s - After Updation: Total SAF Records[%d]", __FUNCTION__, giTotalSAFRecords);
				APP_TRACE(szDbgMsg);

				iRetVal = updateSAFDataToFile();
				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully Updated the SAF Details file after updating SAF Records and SAF amount", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iRetVal = SAF_RECORD_FOUND;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					//Praveen_P1: what to do here!!!!
				}
			}
			else if(iRetVal == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s) with the requested SAF Record Number i.e. %s in %s file ", __FUNCTION__, szRecNumber, SAF_COMPLETED_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(iRetVal == SAF_RECORD_IN_PROGRESS)
		{
			debug_sprintf(szDbgMsg, "%s: SAF Record in IN_PROCESS Status, Cannot delete the Record.", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given record number", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;

}

/*
 * ============================================================================
 * Function Name: getSAFTranDtlsBySAFRecRange
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getSAFDtlsByRecRange(SAFDTLS_PTYPE pstSAFDtls, PAAS_BOOL bTOR)
{
	int		iRetVal				= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	while(1)
	{
		//Check if there are any records presently
		if(giTotalSAFRecords == 0 && giTotalSAFTORRecords == 0)
		{
			debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strcpy(pstSAFDtls->szRecCnt, "0");
			strcpy(pstSAFDtls->szTotAmt, "0.00");
			pstSAFDtls->recList = NULL;
			iRetVal = SAF_RECORD_NOT_FOUND;
			break;
		}
		debug_sprintf(szDbgMsg, "%s:Number of SAF Records present is %d", __FUNCTION__, giTotalSAFRecords);
		APP_TRACE(szDbgMsg);

		if(bTOR == PAAS_FALSE)
		{
			if(strlen(pstSAFDtls->szBegNo) > 0 && strlen(pstSAFDtls->szEndNo) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Need to look for SAF record within the [%s,%s] range", __FUNCTION__, pstSAFDtls->szBegNo, pstSAFDtls->szEndNo);
				APP_TRACE(szDbgMsg);

				//Parse the SAF Record file for any record with the SAF status
				iRetVal = filterSAFRecsByRecRange(pstSAFDtls);
				if(iRetVal == SAF_RECORD_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: Found the SAF record(s) within the  requested SAF record number range", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else if(iRetVal == SAF_RECORD_NOT_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s) within the  requested SAF record number range", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		else
		{
			if(strlen(pstSAFDtls->szTORBegNo) > 0 && strlen(pstSAFDtls->szTOREndNo) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Need to look for SAF record within the [%s,%s] range", __FUNCTION__, pstSAFDtls->szTORBegNo, pstSAFDtls->szTOREndNo);
				APP_TRACE(szDbgMsg);
				iRetVal = filterSAFTORRecsByRecRange(pstSAFDtls);
				if(iRetVal == SAF_RECORD_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: Found the SAF record(s) within the  requested SAF record number range", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else if(iRetVal == SAF_RECORD_NOT_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s) within the  requested SAF record number range", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getSAFDtlsAll
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getSAFDtlsAll(SAFDTLS_PTYPE pstSAFDtls, PAAS_BOOL bTOR)
{
	int		iRetVal			= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	while(1)
	{
		//Check if there are any records presently
		if(giTotalSAFRecords == 0 && giTotalSAFTORRecords == 0)
		{
			debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strcpy(pstSAFDtls->szRecCnt, "0");
			strcpy(pstSAFDtls->szTotAmt, "0.00");
			pstSAFDtls->recList = NULL;
			iRetVal = SAF_RECORD_NOT_FOUND;
			break;
		}
		debug_sprintf(szDbgMsg, "%s:Number of SAF Records present is %d", __FUNCTION__, giTotalSAFRecords);
		APP_TRACE(szDbgMsg);
		if(bTOR == PAAS_FALSE)
		{
			//Parse the SAF Record file for all records
			if(strcasecmp(pstSAFDtls->szStatus, "ALL") == SUCCESS || (strlen(pstSAFDtls->szStatus) == 0 && strlen(pstSAFDtls->szTORStatus) == 0))
			{
				iRetVal = getAllSAFRecs(pstSAFDtls);	//paymenmts
				if(iRetVal == SAF_RECORD_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: Found the SAF record(s)", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else if(iRetVal == SAF_RECORD_NOT_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s)", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		else
		{
			if(strcasecmp(pstSAFDtls->szTORStatus,"ALL") == SUCCESS)
			{
				iRetVal = getAllSAFTORRecords(pstSAFDtls);	//TOR
				if(iRetVal == SAF_RECORD_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: Found the SAF TOR record(s)", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else if(iRetVal == SAF_RECORD_NOT_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: NOT Found the SAF TOR record(s)", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getSAFTranDtlsBySAFRecNum
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getSAFDtlsByRecNum(SAFDTLS_PTYPE pstSAFDtls, PAAS_BOOL bTOR)
{
	int		iRetVal			= SUCCESS;
	char	szRecNumber[20] = "";

#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	while(1)
	{
		//Check if there are any records presently
		if(giTotalSAFRecords == 0 && giTotalSAFTORRecords == 0)
		{
			debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strcpy(pstSAFDtls->szRecCnt, "0");
			strcpy(pstSAFDtls->szTotAmt, "0.00");
			pstSAFDtls->recList = NULL;
			iRetVal = SAF_RECORD_NOT_FOUND;
			break;
		}
		debug_sprintf(szDbgMsg, "%s:Number of SAF Records present is %d", __FUNCTION__, giTotalSAFRecords);
		APP_TRACE(szDbgMsg);
		if(bTOR == PAAS_FALSE)
		{
			if(strlen(pstSAFDtls->szBegNo) > 0  || strlen(pstSAFDtls->szEndNo) > 0)
			{
				if(strlen(pstSAFDtls->szBegNo) > 0)
				{
					strcpy(szRecNumber, pstSAFDtls->szBegNo);
				}
				else if(strlen(pstSAFDtls->szEndNo) > 0)
				{
					strcpy(szRecNumber, pstSAFDtls->szEndNo);
				}

				debug_sprintf(szDbgMsg, "%s: Need to look for SAF record %s", __FUNCTION__, szRecNumber);
				APP_TRACE(szDbgMsg);

				//Parse the SAF Record file for any record with the SAF status
				iRetVal = filterSAFRecsByRecNumber(szRecNumber, pstSAFDtls);
				if(iRetVal == SAF_RECORD_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: Found the SAF record(s) with the requested SAF Record Number i.e. %s", __FUNCTION__, szRecNumber);
					APP_TRACE(szDbgMsg);
				}
				else if(iRetVal == SAF_RECORD_NOT_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s) with the requested SAF Record Number i.e. %s", __FUNCTION__, szRecNumber);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

			}
		}
		else
		{
			if(strlen(pstSAFDtls->szTORBegNo) > 0  || strlen(pstSAFDtls->szTOREndNo) > 0)
			{
				if(strlen(pstSAFDtls->szTORBegNo) > 0)
				{
					strcpy(szRecNumber, pstSAFDtls->szTORBegNo);
				}
				else if(strlen(pstSAFDtls->szTOREndNo) > 0)
				{
					strcpy(szRecNumber, pstSAFDtls->szTOREndNo);
				}

				debug_sprintf(szDbgMsg, "%s: Need to look for SAF record %s", __FUNCTION__, szRecNumber);
				APP_TRACE(szDbgMsg);

				iRetVal = filterSAFTORRecsByRecNumber(szRecNumber, pstSAFDtls);
				if(iRetVal == SAF_RECORD_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: Found the SAF record(s) with the requested SAF Record Number i.e. %s", __FUNCTION__, szRecNumber);
					APP_TRACE(szDbgMsg);
				}
				else if(iRetVal == SAF_RECORD_NOT_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s) with the requested SAF Record Number i.e. %s", __FUNCTION__, szRecNumber);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getSAFDtlsBySAFRecStatusCumRecRange
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getSAFDtlsByRecStatusCumRange(SAFDTLS_PTYPE pstSAFDtls, PAAS_BOOL bTOR)
{
	int		iRetVal			= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	while(1)
	{
		//Check if there are any records presently
		if(giTotalSAFRecords == 0 && giTotalSAFTORRecords == 0)
		{
			debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strcpy(pstSAFDtls->szRecCnt, "0");
			strcpy(pstSAFDtls->szTotAmt, "0.00");
			pstSAFDtls->recList = NULL;
			iRetVal = SAF_RECORD_NOT_FOUND;
			break;
		}
		debug_sprintf(szDbgMsg, "%s:Number of SAF Records present is %d", __FUNCTION__, giTotalSAFRecords);
		APP_TRACE(szDbgMsg);

		if(bTOR == PAAS_FALSE)
		{
			//Parse the file for any record with the SAF payment status and payment Record Number Range
			if(strlen(pstSAFDtls->szStatus) > 0 && strlen(pstSAFDtls->szBegNo) > 0  && strlen(pstSAFDtls->szEndNo) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Need to look for SAF record with %s status and with Record number range [%s, %s]", __FUNCTION__, pstSAFDtls->szStatus, pstSAFDtls->szBegNo, pstSAFDtls->szEndNo);
				APP_TRACE(szDbgMsg);
				iRetVal = filterSAFRecsByStatusAndRecRange(pstSAFDtls);
				if(iRetVal == SAF_RECORD_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: Found the SAF record with the requested SAF Status [%s] and with the SAF Record Number Range [%s, %s]", __FUNCTION__, pstSAFDtls->szStatus, pstSAFDtls->szBegNo, pstSAFDtls->szEndNo);
					APP_TRACE(szDbgMsg);
				}
				else if(iRetVal == SAF_RECORD_NOT_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record with the requested SAF Status [%s] and with the SAF record Number Range [%s, %s]", __FUNCTION__, pstSAFDtls->szStatus, pstSAFDtls->szBegNo, pstSAFDtls->szEndNo);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status+Record Number", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		else
		{
			/*parse the file for any record with the SAF TOR status and TOR record Number range*/
			if(strlen(pstSAFDtls->szTORStatus) > 0 && strlen(pstSAFDtls->szTORBegNo) > 0  && strlen(pstSAFDtls->szTOREndNo) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Need to look for SAF record with %s status and with Record number range [%s, %s]", __FUNCTION__, pstSAFDtls->szTORStatus, pstSAFDtls->szTORBegNo, pstSAFDtls->szTOREndNo);
				APP_TRACE(szDbgMsg);
				iRetVal = filterSAFTORRecsByStatusAndRecRange(pstSAFDtls);
				if(iRetVal == SAF_RECORD_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: Found the SAF record with the requested SAF Status [%s] and with the SAF Record Number Range [%s, %s]", __FUNCTION__, pstSAFDtls->szTORStatus, pstSAFDtls->szTORBegNo, pstSAFDtls->szTOREndNo);
					APP_TRACE(szDbgMsg);
				}
				else if(iRetVal == SAF_RECORD_NOT_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record with the requested SAF Status [%s] and with the SAF record Number Range [%s, %s]", __FUNCTION__, pstSAFDtls->szTORStatus, pstSAFDtls->szTORBegNo, pstSAFDtls->szTOREndNo);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status+Record Number", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getSAFDtlsByRecStatusCumRecNum
 *
 * Description	:This API filters the SAF Payment records based on the SAF status and
 * 				Record Number and fills the SAF transaction details list
 *
 * Input Params	:
 *
 * Output Params: SAF_RECORDS_FOUND/SAF_RECORDS_NOT_FOUND
 * ============================================================================
 */
int getSAFDtlsByRecStatusCumRecNum(SAFDTLS_PTYPE pstSAFDtls, PAAS_BOOL bTOR)
{
	int		iRetVal			= SUCCESS;
	char	szRecNumber[20] = "";
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL param passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	while(1)
	{
		//Check if there are any records presently
		if(giTotalSAFRecords == 0 && giTotalSAFTORRecords == 0)
		{
			debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strcpy(pstSAFDtls->szRecCnt, "0");
			strcpy(pstSAFDtls->szTotAmt, "0.00");
			pstSAFDtls->recList = NULL;
			iRetVal = SAF_RECORD_NOT_FOUND;
			break;
		}
		debug_sprintf(szDbgMsg, "%s:Number of SAF Records present is %d", __FUNCTION__, giTotalSAFRecords);
		APP_TRACE(szDbgMsg);

		if(bTOR == PAAS_FALSE)
		{
			/*POS requesting for SAF_STATUS and SAF_NUM_BEGIN or SAF_NUM_END*/
			if(strlen(pstSAFDtls->szStatus) && (strlen(pstSAFDtls->szBegNo) > 0 || strlen(pstSAFDtls->szEndNo) > 0))
			{
				//Parse the file for any record with the SAF status and Record Number
				if(strlen(pstSAFDtls->szBegNo) > 0)
				{
					strcpy(szRecNumber, pstSAFDtls->szBegNo);
				}
				else if(strlen(pstSAFDtls->szEndNo) > 0)
				{
					strcpy(szRecNumber, pstSAFDtls->szEndNo);
				}

				debug_sprintf(szDbgMsg, "%s: Need to look for SAF record with %s status and with Record number %s", __FUNCTION__, pstSAFDtls->szStatus, szRecNumber);
				APP_TRACE(szDbgMsg);

				iRetVal = filterSAFRecsByStatusAndRecNum(pstSAFDtls, szRecNumber);
				if(iRetVal == SAF_RECORD_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: Found the SAF record with the requested SAF Status [%s] and with the SAF Record Number[%s]", __FUNCTION__, pstSAFDtls->szStatus, szRecNumber);
					APP_TRACE(szDbgMsg);
				}
				else if(iRetVal == SAF_RECORD_NOT_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record with the requested SAF Status [%s] and with the SAF record Number[%s]", __FUNCTION__, pstSAFDtls->szStatus, szRecNumber);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status+Record Number", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		else
		{
			/*POS requesting for SAF_TOR_STATUS and SAF_TOR_NUM_BEGIN or SAF_TOR_NUM_END*/
			if(strlen(pstSAFDtls->szTORStatus) && (strlen(pstSAFDtls->szTORBegNo) > 0 || strlen(pstSAFDtls->szTOREndNo) > 0) )
			{
				if(strlen(pstSAFDtls->szTORBegNo) > 0)
				{
					strcpy(szRecNumber, pstSAFDtls->szTORBegNo);
				}
				else if(strlen(pstSAFDtls->szTOREndNo) > 0)
				{
					strcpy(szRecNumber, pstSAFDtls->szTOREndNo);
				}

				debug_sprintf(szDbgMsg, "%s: Need to look for SAF record with %s status and with Record number %s", __FUNCTION__, pstSAFDtls->szTORStatus, szRecNumber);
				APP_TRACE(szDbgMsg);

				iRetVal = filterSAFTORRecsByStatusAndRecNum(pstSAFDtls, szRecNumber);
				if(iRetVal == SAF_RECORD_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: Found the SAF record with the requested SAF TOR Status [%s] and with the SAF TOR Record Number[%s]", __FUNCTION__, pstSAFDtls->szTORStatus, szRecNumber);
					APP_TRACE(szDbgMsg);
				}
				else if(iRetVal == SAF_RECORD_NOT_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record with the requested SAF TOR Status [%s] and with the SAF TOR record Number[%s]", __FUNCTION__, pstSAFDtls->szTORStatus, szRecNumber);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status+Record Number", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;

}

/*
 * ============================================================================
 * Function Name: getSAFDtlsByRecStatus
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getSAFDtlsByRecStatus(SAFDTLS_PTYPE pstSAFDtls, PAAS_BOOL bTOR)
{
	int		iRetVal			= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[512]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Param passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	while(1)
	{
		//Check if there are any records presently
		if(giTotalSAFRecords == 0 && giTotalSAFTORRecords == 0)
		{
			debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strcpy(pstSAFDtls->szRecCnt, "0");
			strcpy(pstSAFDtls->szTotAmt, "0.00");
			pstSAFDtls->recList = NULL;
			iRetVal = SAF_RECORD_NOT_FOUND;
			break;
		}
		debug_sprintf(szDbgMsg, "%s:Number of SAF Records present is %d", __FUNCTION__, giTotalSAFRecords);
		APP_TRACE(szDbgMsg);

		/*Parse the file for any record with the SAF Payment status*/
		if(bTOR == PAAS_FALSE)
		{
			if(strlen(pstSAFDtls->szStatus) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Need to look for SAF record with %s status", __FUNCTION__, pstSAFDtls->szStatus);
				APP_TRACE(szDbgMsg);

				iRetVal = filterSAFRecsByStatus(pstSAFDtls);
				if(iRetVal == SAF_RECORD_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: Found the SAF record(s) with the requested SAF Status i.e. %s", __FUNCTION__, pstSAFDtls->szStatus);
					APP_TRACE(szDbgMsg);
				}
				else if(iRetVal == SAF_RECORD_NOT_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s) with the requested SAF Status i.e. %s", __FUNCTION__, pstSAFDtls->szStatus);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		else
		{
			/*Parse the file for any record with the SAF TOR status*/
			if(strlen(pstSAFDtls->szTORStatus) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Need to look for TOR SAF record with %s status", __FUNCTION__, pstSAFDtls->szTORStatus);
				APP_TRACE(szDbgMsg);

				iRetVal = filterSAFRecsByTORStatus(pstSAFDtls);
				if(iRetVal == SAF_RECORD_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: Found the SAF record(s) with the requested SAF TOR Status i.e. %s", __FUNCTION__, pstSAFDtls->szTORStatus);
					APP_TRACE(szDbgMsg);
				}
				else if(iRetVal == SAF_RECORD_NOT_FOUND)
				{
					debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s) with the requested SAF TOR Status i.e. %s", __FUNCTION__, pstSAFDtls->szTORStatus);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: clearAllSAFRecords
 *
 * Description	: This API clears all the SAF records stored in the database.
 *				  (Clears from memory and the file)
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int clearAllSAFRecords(SAFDTLS_PTYPE pstSAFDtls)
{
	int	 iRetVal		= SUCCESS;
	int	 iRetValTemp	= SUCCESS;
	int	 rv 			= SUCCESS;
	FILE *Fh;
#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	while(1)
	{
		//Check if there are any records presently
		if(giTotalSAFRecords == 0 && giTotalSAFTORRecords == 0)
		{
			debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strcpy(pstSAFDtls->szRecCnt, "0");
			strcpy(pstSAFDtls->szTotAmt, "0.00");
			pstSAFDtls->recList = NULL;
			iRetVal = SAF_RECORD_NOT_FOUND;
			break;
		}
		debug_sprintf(szDbgMsg, "%s:Number of SAF Records present is %d", __FUNCTION__, giTotalSAFRecords);
		APP_TRACE(szDbgMsg);

		acquireMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");
		if(iSAFInProgressStatus == SAF_IN_FLIGHT)
		{
			debug_sprintf(szDbgMsg, "%s: SAF record with status IN_PROCESS is currently IN_FLIGHT, Should Not Delete the Records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = SAF_RECORD_IN_PROGRESS;
			break;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s:SAF Records are currently not IN_FLIGHT, Can Safely remove all the records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		//Parse the SAF Record file for all records
//		if(giTotalSAFRecords > 0 && giTotalSAFTORRecords == 0)		// Only when Payment SAF records are present in file
//		{
			iRetVal = getAllSAFRecs(pstSAFDtls);	//payment
			iRetValTemp = getAllSAFTORRecords(pstSAFDtls);	//TOR
			if(iRetVal == SAF_RECORD_FOUND || iRetValTemp == SAF_RECORD_FOUND)
			{
				iRetVal = SAF_RECORD_FOUND;
			}
			else
			{
				iRetVal = SAF_RECORD_NOT_FOUND;
			}

			if(iRetVal == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Found the SAF record(s)", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

				/*------------- Clear the contents of the SAF Records file------------------- */
				Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "w"); //Create an empty file for output operations. If a file with the same name already exists, its contents are discarded and the file is treated as a new empty file.
				if(Fh!= NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Deleted the contents of the Eligible SAF Records file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(chmod(SAF_ELIGIBLE_RECORDS_FILE_NAME, 0666) == -1)
					{
						debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					fclose(Fh);

					Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "w"); //Create an empty file for output operations. If a file with the same name already exists, its contents are discarded and the file is treated as a new empty file.
					if(Fh!= NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Deleted the contents of the completed SAF Records file", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						if(chmod(SAF_COMPLETED_RECORDS_FILE_NAME, 0666) == -1)
						{
							debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
						fclose(Fh);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error while opening the %s FILE", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
						APP_TRACE(szDbgMsg);
					}
					//Reset the Total SAF Records and Total SAF Amount
					giTotalSAFRecords        = 0;
					giTotalEligibleSAFRecords = 0;
					gfTotalSAFAmount         = 0.00;
					giTotalSAFTORRecords       	= 0;
					giTotalPendingSAFTORRecords	= 0;

					iRetVal = updateSAFDataToFile();
					if(iRetVal == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Successfully Updated the SAF Details file after resetting Total SAF Records and SAF Total Amount", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//iRetVal = SAF_RECORD_FOUND; CID 67335 (#1 of 1): Unused value (UNUSED_VALUE) T_RaghavendranR1. Commenting the stmt as it is overwritten below.
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//Praveen_P1: what to do here!!!!
					}

					if(strlen(szSSITranKey) > 0)
					{
						// T_RaghavendranR1: Since we deleted all the SAF Records, we popout the SSI Transaction Key updated at last.
						rv = popSSITran(szSSITranKey);
						if(rv != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s:Error while popping the SAF SSI transaction from stack", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
						deleteStack(szSSITranKey);
						memset(szSSITranKey, 0x00, sizeof(szSSITranKey));
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while opening the %s FILE", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);
				}

				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

				//Would have deleted all the contents of the file, need to add today's timestamp
				iRetVal = writeTimeStamptoSAFFile(0);
				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully Written TimeStamp to the SAF completed record file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iRetVal = SAF_RECORD_FOUND;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while writing timestamp to the SAF completed record File", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else if(iRetVal == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s)", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
//		}
#if 0
		else if(giTotalSAFRecords > 0 && giTotalSAFTORRecords > 0)				// If Payment and TOR records are present.
		{
			debug_sprintf(szDbgMsg, "%s: Remove only Payment record and keep TOR records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = clearNonTORSAFRecords(pstSAFDtls);					// Clears the payment Records
			if(iRetVal == SAF_RECORD_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Payment SAF records Found", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				//Reset the Total SAF Records and Total SAF Amount
				giTotalSAFRecords        = 0;
				giTotalEligibleSAFRecords = 0;
				gfTotalSAFAmount         = 0.00;

				iRetVal = updateSAFDataToFile();
				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully Updated the SAF Details file after resetting Total SAF Records and SAF Total Amount", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iRetVal = SAF_RECORD_FOUND;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					//Praveen_P1: what to do here!!!!
				}
			}
			else if(iRetVal == SAF_RECORD_NOT_FOUND)
			{
				debug_sprintf(szDbgMsg, "%s: Payment SAF records not Found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while compiling SAF records with the given Status", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
#endif
		break;
	}
	//After successFully removing all the SAF records, we can release the lock from InProgress Records.
	releaseMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getSAFRecordCount
 *
 * Description	: This API gives the number of SAF records to responsd to POS
 *
 * Input Params	: NONE
 *
 * Output Params: Number of records
 * ============================================================================
 */
int getSAFRecordCount()
{
	//return stSAFTranDetails.iSAFRecordCount;
	return 0; //Praveen_P1: Change it!!!
}

/*
 * ============================================================================
 * Function Name: getSAFRecordTotalAmount
 *
 * Description	: This API gives the number of SAF records to responsd to POS
 *
 * Input Params	: NONE
 *
 * Output Params: Number of records
 * ============================================================================
 */
int getSAFRecordTotalAmount(char *pszSAFAmount)
{
	int iRetVal = SUCCESS;

	#ifdef DEBUG
		char szDbgMsg[256]	= "";
	#endif

	if(pszSAFAmount == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	//strcpy(pszSAFAmount, stSAFTranDetails.szSAFTotalAmount);

	debug_sprintf(szDbgMsg, "%s: SAF Record Total amount is %s", __FUNCTION__, pszSAFAmount);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: fillSAFRecordDetails
 *
 * Description	: This API fills the data system for the given SAF record
 *
 * Input Params	: Record Line
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int fillSAFRecordDetails(char *pszSAFData, SAFDTLS_PTYPE pstSAFDtls)
{
	int				iRetVal			  = 0;
	char			*pchFirst 		  = NULL;
	char			*pchNext          = NULL;
	char			*curPtr			  = NULL;
	char			*nxtPtr			  = NULL;
	char			szClearPAN[20]    = "";
	char			szMaskedPAN[20]   = "";
	char			szClearCVV[5]	  = "";
	char			szMaskedCVV[5]    = "";
	char			szCmdType[21]     = "";
	PAAS_BOOL		bLastFieldFound	  = PAAS_FALSE;
	SAFREC_PTYPE pstSAFRecordNode = NULL;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFData == NULL || pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	pstSAFRecordNode = (SAFREC_PTYPE)malloc(sizeof(SAFREC_STYPE));
	if(pstSAFRecordNode == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Could not allocate memory for SAF Record node!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(pstSAFRecordNode, 0x00, sizeof(SAFREC_STYPE));

	while(1)
	{
		/* Format of the SAF Record
		 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
		 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
		 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
		 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
		 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
		 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
		 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT|BAR_CODE|PIN_CODE|APR_TYPE||||;
		 *
		 */

		/*-------------- Getting SAF Status Field -----------------*/
		pchFirst = strchr(pszSAFData, PIPE);
		if(pchFirst == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			free(pstSAFRecordNode);
			return FAILURE;
		}
		memset(pstSAFRecordNode->szSAFStatus, 0x00, sizeof(pstSAFRecordNode->szSAFStatus));
		strncpy(pstSAFRecordNode->szSAFStatus, pszSAFData, (pchFirst - pszSAFData)); //Getting the SAF status from the line

		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Number field

		/*-------------- Getting SAF Record Number -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)//CID 67389 (#1 of 1): Dereference before null check (REVERSE_INULL) T_RaghavendranR1. Checking pchNext for NULL and not on pchFirst
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			free(pstSAFRecordNode);
			return FAILURE;
		}
		memset(pstSAFRecordNode->szSAFNo, 0x00, sizeof(pstSAFRecordNode->szSAFNo));
		strncpy(pstSAFRecordNode->szSAFNo, pchFirst, (pchNext - pchFirst)); //Getting the SAF record number from the file

		pchFirst = pchNext + 1; //pchFirst points to start of the AUTH code field

		/*-------------- Crossing SAF Record AUTH field -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Account Number field

		/*-------------- Getting SAF Record Account Number -----------------*/
		if((*pchFirst) != PIPE) //Record contains Account Number field
		{
			debug_sprintf(szDbgMsg, "%s: Record contains Account Number field", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				free(pstSAFRecordNode);
				return FAILURE;
			}

			curPtr = pchFirst;
			nxtPtr = strchr(curPtr, '#');
			if(nxtPtr != NULL)
			{
				strncpy(szClearPAN, curPtr, (nxtPtr - curPtr));
			}
			else
			{
				strncpy(szClearPAN, pchFirst, (pchNext - pchFirst));
			}

			memset(szMaskedPAN, 0x00, sizeof(szMaskedPAN));
			getMaskedPAN(szClearPAN, szMaskedPAN);
			debug_sprintf(szDbgMsg, "%s: Masked PAN is %s", __FUNCTION__, szMaskedPAN);
			APP_TRACE(szDbgMsg);

			memset(pstSAFRecordNode->szPAN, 0x00, sizeof(pstSAFRecordNode->szPAN));
			strncpy(pstSAFRecordNode->szPAN, szMaskedPAN, strlen(szMaskedPAN));

			pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Track2 field
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Account Number field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(pstSAFRecordNode->szPAN, 0x00, sizeof(pstSAFRecordNode->szPAN));
			pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Track2 field
		}

		/*-------------- Crossing SAF Record Track2 data -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Crossing SAF Record Expiry Month -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Crossing SAF Record Expiry Year -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Getting SAF Record Payment Media -----------------*/
		if((*pchFirst) != PIPE) //Record contains Payment Media field
		{
			debug_sprintf(szDbgMsg, "%s: Record contains Payment Media field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				free(pstSAFRecordNode);
				return FAILURE;
			}
			memset(pstSAFRecordNode->szPymtMedia, 0x00, sizeof(pstSAFRecordNode->szPymtMedia));
			strncpy(pstSAFRecordNode->szPymtMedia, pchFirst, (pchNext - pchFirst));

			debug_sprintf(szDbgMsg, "%s: Payment Media[%s]", __FUNCTION__, pstSAFRecordNode->szPymtMedia);
			APP_TRACE(szDbgMsg);

			pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Card Source
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Payment Media field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(pstSAFRecordNode->szPymtMedia, 0x00, sizeof(pstSAFRecordNode->szPymtMedia));
			pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Card Source
		}

		/*-------------- Getting SAF Record Card Source -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Getting SAF Record Transaction Amount -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Getting SAF Record Transaction amount -----------------*/
		if((*pchFirst) != PIPE) //Record contains Transaction amount field
		{
			debug_sprintf(szDbgMsg, "%s: Record contains Transaction amount field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//				free(pstSAFRecordNode);
				//				return FAILURE;
				break;
			}
			memset(pstSAFRecordNode->szTranAmt, 0x00, sizeof(pstSAFRecordNode->szTranAmt));
			strncpy(pstSAFRecordNode->szTranAmt, pchFirst, (pchNext - pchFirst));
			pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Tip Amount field
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Transaction amount field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(pstSAFRecordNode->szTranAmt, 0x00, sizeof(pstSAFRecordNode->szTranAmt));
			pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Tip Amount field
		}

		/*-------------- Getting SAF Record TIP amount -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Getting SAF Record TAX amount -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;


		/*-------------- Getting SAF Record Payment Type -----------------*/
		if((*pchFirst) != PIPE) //Record contains Payment Type field
		{
			debug_sprintf(szDbgMsg, "%s: Record contains Payment Type field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//				free(pstSAFRecordNode);
				//				return FAILURE;
				break;
			}
			memset(pstSAFRecordNode->szPymtType, 0x00, sizeof(pstSAFRecordNode->szPymtType));
			strncpy(pstSAFRecordNode->szPymtType, pchFirst, (pchNext - pchFirst));
			pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Invoice field
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Payment Type field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(pstSAFRecordNode->szPymtType, 0x00, sizeof(pstSAFRecordNode->szPymtType));
			pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Invoice field
		}

		/*-------------- Getting SAF Record Invoice -----------------*/
		if((*pchFirst) != PIPE) //Record contains Invoice field
		{
			debug_sprintf(szDbgMsg, "%s: Record contains Invoice field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//				free(pstSAFRecordNode);
				//				return FAILURE;
				break;
			}
			memset(pstSAFRecordNode->szInvoice, 0x00, sizeof(pstSAFRecordNode->szInvoice));
			strncpy(pstSAFRecordNode->szInvoice, pchFirst, (pchNext - pchFirst));
			pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Purchase ID field
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Invoice field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(pstSAFRecordNode->szInvoice, 0x00, sizeof(pstSAFRecordNode->szInvoice));
			pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record cashier id field
		}

		/*-------------- Getting SAF Record Purchase ID -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Getting SAF Record Cashier ID -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Getting SAF Record Sign Image Type -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Getting SAF Record Sign Data -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1;

		/*-------------- Getting SAF Record PWC Result Code -----------------*/
		if((*pchFirst) != PIPE) //Record contains Result Code
		{
			debug_sprintf(szDbgMsg, "%s: Record contains Result code field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//				free(pstSAFRecordNode);
				//				return FAILURE;
				break;
			}
			memset(pstSAFRecordNode->szResultCode, 0x00, sizeof(pstSAFRecordNode->szResultCode));
			strncpy(pstSAFRecordNode->szResultCode, pchFirst, (pchNext - pchFirst));
			pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record CTROUTD field
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Result code field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(pstSAFRecordNode->szResultCode, 0x00, sizeof(pstSAFRecordNode->szResultCode));
			pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record CTROUTD ID field
		}

		/*-------------- Getting SAF Record PWC CTROUTD -----------------*/
		if((*pchFirst) != PIPE) //Record contains CTROUTD
		{
			debug_sprintf(szDbgMsg, "%s: Record contains CTroutd field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//				free(pstSAFRecordNode);
				//				return FAILURE;
				break;
			}
			memset(pstSAFRecordNode->szCTroutd , 0x00, sizeof(pstSAFRecordNode->szCTroutd));
			strncpy(pstSAFRecordNode->szCTroutd, pchFirst, (pchNext - pchFirst));
			pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Purchase ID field
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain CTroutd field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(pstSAFRecordNode->szCTroutd, 0x00, sizeof(pstSAFRecordNode->szCTroutd));
			pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record TROUTD field
		}

		/*-------------- Getting SAF Record PWC TROUTD -----------------*/
		if((*pchFirst) != PIPE) //Record contains TROUTD
		{
			debug_sprintf(szDbgMsg, "%s: Record contains Troutd field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//				free(pstSAFRecordNode);
				//				return FAILURE;
				break;
			}
			memset(pstSAFRecordNode->szTroutd , 0x00, sizeof(pstSAFRecordNode->szTroutd));
			strncpy(pstSAFRecordNode->szTroutd, pchFirst, (pchNext - pchFirst));
			pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record AUTH_CODE field
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Troutd field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(pstSAFRecordNode->szTroutd, 0x00, sizeof(pstSAFRecordNode->szTroutd));
			pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record AUTH_CODE field
		}

		/*-------------- Getting SAF Record PWC AUTH CODE -----------------*/
		if((*pchFirst) != SEMI_COLON || (*pchFirst) != PIPE) //Record contains AUTH_CODE
		{
			/*Note: Adding some more fields to SAF record after auth code. Hence looking for PIPE character.
			 * 		For backward compatibility we are also looking for semi colon.
			 */
			debug_sprintf(szDbgMsg, "%s: Record contains Auth Code field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = strchr(pchFirst, PIPE);
			if( pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Going to look for semi colon", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				//Set the flag to indicate its last field.
				bLastFieldFound = PAAS_TRUE;

				if( (pchNext = strchr(pchFirst, SEMI_COLON)) == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Does not contain SEMICOLON(;) in the data!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					//				free(pstSAFRecordNode);
					//				return FAILURE;
					break;
				}
			}
			memset(pstSAFRecordNode->szAuthCode , 0x00, sizeof(pstSAFRecordNode->szAuthCode));
			strncpy(pstSAFRecordNode->szAuthCode, pchFirst, (pchNext - pchFirst));
			pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record encryption type field
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Auth Code field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(pstSAFRecordNode->szAuthCode, 0x00, sizeof(pstSAFRecordNode->szAuthCode));
			pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record encryption type field
		}

		if(bLastFieldFound == PAAS_FALSE)
		{
			/*-------------- Crossing SAF Record Encryption type field -----------------*/
			pchNext = strchr(pchFirst, PIPE);
			pchFirst = pchNext + 1;

			/*-------------- Crossing SAF Record Encryption payload field -----------------*/
			pchNext = strchr(pchFirst, PIPE);
			pchFirst = pchNext + 1;

			/*-------------- Getting SAF Record CARD TOKEN -----------------*/
			if((*pchFirst) != PIPE) //Record contains CARD TOKEN
			{
				debug_sprintf(szDbgMsg, "%s: Record contains card token field", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				pchNext = strchr(pchFirst, PIPE);
				if(pchNext == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					//				free(pstSAFRecordNode);
					//				return FAILURE;
					break;
				}
				memset(pstSAFRecordNode->szCardToken , 0x00, sizeof(pstSAFRecordNode->szCardToken));
				strncpy(pstSAFRecordNode->szCardToken, pchFirst, (pchNext - pchFirst));

				/* ArjunU1: We need to add some patch up code here to fill card token. If card token contains
				 * 			"/" separator between card source and id then we can need replace with PIPE character because
				 * 			POS expects card source and id needs to be separated by PIPE.
				 */
				curPtr = pstSAFRecordNode->szCardToken;
				if( (nxtPtr = strchr(curPtr, '/')) != NULL ) //Card token doesn't contain '/' separator between card source and id
				{
					//Replace the PIPE character from card token data by "/" separator.
					*nxtPtr = '|';
				}
				else	//'/' character not found in the card token data.
				{
					//Do nothing.
				}
				pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record CVV2 Code
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Record does not contain card token field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				memset(pstSAFRecordNode->szCardToken, 0x00, sizeof(pstSAFRecordNode->szCardToken));
				pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record CVV2 code field
			}

			/*-------------- Getting SAF Record CVV2 Code -----------------*/
			if((*pchFirst) != PIPE) //Record contains CVV2 Code
			{
				debug_sprintf(szDbgMsg, "%s: Record contains CVV2 Code field", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				pchNext = strchr(pchFirst, PIPE);
				if(pchNext == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					//				free(pstSAFRecordNode);
					//				return FAILURE;
					break;
				}

				/*Akshaya :PTMX - 822 - Masking the CVV code field While doing SAF Query*/
				memset(szClearCVV, 0x00, sizeof(szClearCVV));
				memset(szMaskedCVV, 0x00, sizeof(szMaskedCVV));
				strncpy(szClearCVV, pchFirst, (pchNext - pchFirst));
				getMaskedCVV(szClearCVV, szMaskedCVV);

				memset(pstSAFRecordNode->szCVVCode , 0x00, sizeof(pstSAFRecordNode->szCVVCode));
				strcpy(pstSAFRecordNode->szCVVCode, szMaskedCVV);

				pchFirst = pchNext + 1; //pchFirst points to start of the SAF Bank User data field
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Record does not contain CVV2 Code", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				memset(pstSAFRecordNode->szCVVCode, 0x00, sizeof(pstSAFRecordNode->szCVVCode));
				pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Bank User Data field
			}

			/*-------------- Getting SAF Record PWC BANK USER DATA -----------------*/
			if((*pchFirst) != SEMI_COLON || (*pchFirst) != PIPE) //Record contains Bank user data
			{
				/*Note: Adding some more fields to SAF record after CVV2 Code. Hence looking for PIPE character.
				 * 		For backward compatibility we are also looking for semi colon.
				 */
				debug_sprintf(szDbgMsg, "%s: Record may contain Bank User data field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = strchr(pchFirst, PIPE);
				if( pchNext == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Going to look for semi colon", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					//Set the flag to indicate its last field.
					bLastFieldFound = PAAS_TRUE;

					if( (pchNext = strchr(pchFirst, SEMI_COLON)) == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Does not contain SEMICOLON(;) in the data!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//				free(pstSAFRecordNode);
						//				return FAILURE;
						break;
					}
				}
				memset(pstSAFRecordNode->szBankUserData , 0x00, sizeof(pstSAFRecordNode->szBankUserData));
				strncpy(pstSAFRecordNode->szBankUserData, pchFirst, (pchNext - pchFirst));
				pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Business date field
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Record does not contain Bank User data field", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				memset(pstSAFRecordNode->szBankUserData, 0x00, sizeof(pstSAFRecordNode->szBankUserData));
				pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Business date field
			}

			if( bLastFieldFound == PAAS_FALSE)
			{
				/*-------------- Getting SAF Record Business date -----------------*/
				if((*pchFirst) != PIPE) //Record contains Business Date
				{
					debug_sprintf(szDbgMsg, "%s: Record contains Business Date field", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					pchNext = strchr(pchFirst, PIPE);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//				free(pstSAFRecordNode);
						//				return FAILURE;
						break;
					}
					memset(pstSAFRecordNode->szBusinessDate , 0x00, sizeof(pstSAFRecordNode->szBusinessDate));
					strncpy(pstSAFRecordNode->szBusinessDate, pchFirst, (pchNext - pchFirst));

					pchFirst = pchNext + 1; //pchFirst points to start of the SAF PINless Debit field
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Record does not contain Business Date field", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					memset(pstSAFRecordNode->szBusinessDate, 0x00, sizeof(pstSAFRecordNode->szBusinessDate));
					pchFirst = pchFirst + 1; //pchFirst points to start of the PINless Debit field
				}

				/*-------------- Crossing SAF Record PINless debit -----------------*/
				if((*pchFirst) != SEMI_COLON || (*pchFirst) != PIPE) //Record contains PINless Debit
				{
					/*Note: Adding some more fields to SAF record after Business Date. Hence looking for PIPE character.
					 * 		For backward compatibility we are also looking for semi colon.
					 */
					debug_sprintf(szDbgMsg, "%s: Record may contain PINless Debit field", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					pchNext = strchr(pchFirst, PIPE);
					if( pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Going to look for semi colon", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						//Set the flag to indicate its last field.
						bLastFieldFound = PAAS_TRUE;

						if( (pchNext = strchr(pchFirst, SEMI_COLON)) == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//				free(pstSAFRecordNode);
							//				return FAILURE;
							break;
						}
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Txn POS Entry Mode field
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Record does not contain PINless Debit field", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Txn POS Entry Mode field
				}

				if( bLastFieldFound == PAAS_FALSE)
				{
					/*-------------- Getting Transaction POS Entry Mode------------*/
					if((*pchFirst) != PIPE) //Record contains Txn POS Entry Mode
					{
						debug_sprintf(szDbgMsg, "%s: Record contains Txn POS Entry Mode field", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pchNext = strchr(pchFirst, PIPE);
						if(pchNext == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//				free(pstSAFRecordNode);
							//				return FAILURE;
							break;
						}
						memset(pstSAFRecordNode->szTxnPosEntryMode , 0x00, sizeof(pstSAFRecordNode->szTxnPosEntryMode));
						strncpy(pstSAFRecordNode->szTxnPosEntryMode, pchFirst, (pchNext - pchFirst));

						pchFirst = pchNext + 1; //pchFirst points to start of the Merchant ID field
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Record does not contain Txn POS Entry Mode", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						memset(pstSAFRecordNode->szTxnPosEntryMode, 0x00, sizeof(pstSAFRecordNode->szTxnPosEntryMode));
						pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Merchant Id field
					}

					/*-------------- Getting SAF Record Merchant ID -----------------*/
					if((*pchFirst) != PIPE) //Record contains Merchant Id
					{
						debug_sprintf(szDbgMsg, "%s: Record contains Merchant Id field", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pchNext = strchr(pchFirst, PIPE);
						if(pchNext == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//				free(pstSAFRecordNode);
							//				return FAILURE;
							break;
						}
						memset(pstSAFRecordNode->szMerchId , 0x00, sizeof(pstSAFRecordNode->szMerchId));
						strncpy(pstSAFRecordNode->szMerchId, pchFirst, (pchNext - pchFirst));

						pchFirst = pchNext + 1; //pchFirst points to start of the SAF Terminal Id field
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Record does not contain Merchant Id field", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						memset(pstSAFRecordNode->szMerchId, 0x00, sizeof(pstSAFRecordNode->szMerchId));
						pchFirst = pchFirst + 1; //pchFirst points to start of the Terminal Id field
					}

					/*-------------- Getting SAF Record Terminal ID -----------------*/
					if((*pchFirst) != PIPE) //Record contains Terminal Id
					{
						debug_sprintf(szDbgMsg, "%s: Record contains Terminal Id field", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pchNext = strchr(pchFirst, PIPE);
						if(pchNext == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//				free(pstSAFRecordNode);
							//				return FAILURE;
							break;
						}
						memset(pstSAFRecordNode->szTermId , 0x00, sizeof(pstSAFRecordNode->szTermId));
						strncpy(pstSAFRecordNode->szTermId, pchFirst, (pchNext - pchFirst));

						pchFirst = pchNext + 1; //pchFirst points to start of the SAF Lane ID field
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Record does not contain Terminal Id field", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						memset(pstSAFRecordNode->szTermId, 0x00, sizeof(pstSAFRecordNode->szTermId));
						pchFirst = pchFirst + 1; //pchFirst points to start of the Lane Id field
					}

					/*-------------- Getting SAF Record LANE ID -----------------*/
					if((*pchFirst) != PIPE) //Record contains Lane Id
					{
						debug_sprintf(szDbgMsg, "%s: Record contains Lane Id field", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pchNext = strchr(pchFirst, PIPE);
						if(pchNext == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//				free(pstSAFRecordNode);
							//				return FAILURE;
							break;
						}
						memset(pstSAFRecordNode->szLaneId , 0x00, sizeof(pstSAFRecordNode->szLaneId));
						strncpy(pstSAFRecordNode->szLaneId, pchFirst, (pchNext - pchFirst));

						pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Record does not contain Lane Id field", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						memset(pstSAFRecordNode->szLaneId, 0x00, sizeof(pstSAFRecordNode->szLaneId));
						pchFirst = pchFirst + 1; //pchFirst points to start of the  ApprvdAmt SAF record field
					}

					/*-------------- Getting SAF Record APPROVED_AMOUNT -----------------*/
					if((*pchFirst) != PIPE) //Record contains approved amount
					{
						debug_sprintf(szDbgMsg, "%s: Record contains Approved Amount field", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pchNext = strchr(pchFirst, PIPE);
						if(pchNext == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							free(pstSAFRecordNode);
							return FAILURE;
						}
						memset(pstSAFRecordNode->szApprvdAmt , 0x00, sizeof(pstSAFRecordNode->szApprvdAmt));
						strncpy(pstSAFRecordNode->szApprvdAmt, pchFirst, (pchNext - pchFirst));

						pchFirst = pchNext + 1; //pchFirst points to start of the next TAX_IND record field
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Record does not contain Approved Amount field", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						memset(pstSAFRecordNode->szApprvdAmt, 0x00, sizeof(pstSAFRecordNode->szApprvdAmt));
						pchFirst = pchFirst + 1; //pchFirst points to start of the  TAX_IND SAF record field
					}

					/*-------------- Crossing SAF Record Tax Indicator field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchNext + 1;

					/*-------------- Getting SAF Record Command Type -----------------*/
					if((*pchFirst) != PIPE) //Record contains command type
					{
						debug_sprintf(szDbgMsg, "%s: Record contains command type field", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pchNext = strchr(pchFirst, PIPE);
						if(pchNext == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//				free(pstSAFRecordNode);
							//				return FAILURE;
							break;
						}
						memset(szCmdType , 0x00, sizeof(szCmdType));
						strncpy(szCmdType, pchFirst, (pchNext - pchFirst));

						pstSAFRecordNode->iCmd = atoi(szCmdType);
						if(pstSAFRecordNode->iCmd == SSI_ACTIVATE || (pstSAFRecordNode->iCmd == SSI_CREDIT && (isFloorLimitChkAllowedForSAFRefund() == PAAS_FALSE)) || pstSAFRecordNode->iCmd == SSI_PAYACCOUNT)
						{
							debug_sprintf(szDbgMsg, "%s: For Gift Activate or refund with Trans Floor Limit Check Disabled, we update the Special Case Amount. This Amount will be added back the SAF Total Amt, when the special case SAF Records are Present", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//memset(pstSAFRecordNode->szTranAmt, 0x00, sizeof(pstSAFRecordNode->szTranAmt));
							if(strlen(pstSAFRecordNode->szTranAmt) > 0)
							{
								strcpy(pstSAFRecordNode->szAmtSpecialcase, pstSAFRecordNode->szTranAmt);
							}
						}

						pchFirst = pchNext + 1; //pchFirst points to start of the track indicator record field
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Record does not contain command type field, Filling with default command as SALE", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pstSAFRecordNode->iCmd = SSI_SALE; //Default to SALE.
						pchFirst = pchFirst + 1; //pchFirst points to start of the  track indicator SAF record field
					}

					/*-------------- Crossing SAF Record Track Indicator field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchNext + 1;

					/*-------------- Getting SAF Record STORE_ID -----------------*/
					if((*pchFirst) != PIPE) //Record contains store id
					{
						debug_sprintf(szDbgMsg, "%s: Record contains Store Id field", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pchNext = strchr(pchFirst, PIPE);
						if(pchNext == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//				free(pstSAFRecordNode);
							//				return FAILURE;
							break;
						}
						memset(pstSAFRecordNode->szStoreId , 0x00, sizeof(pstSAFRecordNode->szStoreId));
						strncpy(pstSAFRecordNode->szStoreId, pchFirst, (pchNext - pchFirst));

						pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Record does not contain Store Id field", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						memset(pstSAFRecordNode->szStoreId, 0x00, sizeof(pstSAFRecordNode->szStoreId));
						pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
					}

					/*-------------- Crossing SAF Record CUSTOMER_ZIP field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchNext + 1;

					/*-------------- Crossing SAF Record CUSTOMER_STREET field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchNext + 1;

					/*-------------- Crossing SAF Record DISCOUNT_AMOUNT field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchNext + 1;

					/*-------------- Crossing SAF Record DUTY_AMOUNT field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchNext + 1;

					/*-------------- Crossing SAF Record FREIGHT_AMOUNT field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchNext + 1;

					/*-------------- Crossing SAF Record DEST_COUNTRY_CODE field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchNext + 1;

					/*-------------- Crossing SAF Record DEST_POSTAL_CODE field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchNext + 1;

					/*-------------- Crossing SAF Record SHIP_FROM_ZIP_CODE field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchNext + 1;

					/*-------------- Crossing SAF Record RETAIL_ITEM_DESC_1 field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchNext + 1;

					/*-------------- Crossing SAF Record ALT_TAX_ID field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchNext + 1;

					/*-------------- Crossing SAF Record CUSTOMER_CODE field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchNext + 1;

					/*-------------- Crossing SAF Record AMOUNT_HEALTHCARE field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record AMOUNT_PRESCRIPTION field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record AMOUNT_VISION field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record AMOUNT_CLINIC field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record AMOUNT_DENTAL field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record EMV_TAGS field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record EMV_ENCRYPTED_BLOB field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record EMV_REVERSAL_TYPE field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record PIN_BLOCK field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record KEY_SERIAL_NUMBER field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Getting SAF Payment Sub Type -----------------*/
					if((*pchFirst) != PIPE) //Record contains AVS_CODE
					{
						debug_sprintf(szDbgMsg, "%s: Record contains Payment Sub Type field", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pchNext = strchr(pchFirst, PIPE);
						if(pchNext == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							free(pstSAFRecordNode);
							return FAILURE;
						}
						memset(pstSAFRecordNode->szPymtSubType , 0x00, sizeof(pstSAFRecordNode->szPymtSubType));
						strncpy(pstSAFRecordNode->szPymtSubType, pchFirst, (pchNext - pchFirst));

						pchFirst = pchNext + 1; //pchFirst points to start of Host Response Code field
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Record does not contain Payment Sub Type field", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						memset(pstSAFRecordNode->szPymtSubType, 0x00, sizeof(pstSAFRecordNode->szPymtSubType));
						pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
					}

					/* Format of the SAF Record
					 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
					 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
					 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
					 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
					 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
					 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
					 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT|BAR_CODE|PIN_CODE|APR_TYPE|INIT_VECTOR|PASS_THROUGH_FIELDS
					 * ||||;
					 *
					 */

					/*-------------- Crossing SAF Record PROMO_CODE field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record ORDER_DATETIME field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record CREDIT_PLAN_NBR field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record PURCHASE_APR field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record REFERENCE field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record CARDHOLDER field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record BILLPAY field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Getting SAF Record ORIG_TRANS_DATE field -----------------*/
					if((*pchFirst) != PIPE)
					{
						debug_sprintf(szDbgMsg, "%s: Record contains Orig Tran Date field", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pchNext = strchr(pchFirst, PIPE);
						if(pchNext == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//				free(pstSAFRecordNode);
							//				return FAILURE;
							break;

						}
						memset(pstSAFRecordNode->szTransDate , 0x00, sizeof(pstSAFRecordNode->szTransDate));
						strncpy(pstSAFRecordNode->szTransDate, pchFirst, (pchNext - pchFirst));

						pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Record does not contain Tran Date field", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						memset(pstSAFRecordNode->szTransDate, 0x00, sizeof(pstSAFRecordNode->szTransDate));
						pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
					}

					/*-------------- Crossing SAF Record ORIG_TRANS_TIME field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if((*pchFirst) != PIPE)
					{
						debug_sprintf(szDbgMsg, "%s: Record contains Orig Tran Time field", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						pchNext = strchr(pchFirst, PIPE);
						if(pchNext == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							//				free(pstSAFRecordNode);
							//				return FAILURE;
							break;

						}
						memset(pstSAFRecordNode->szTransTime , 0x00, sizeof(pstSAFRecordNode->szTransTime));
						strncpy(pstSAFRecordNode->szTransTime, pchFirst, (pchNext - pchFirst));

						pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Record does not contain Tran Time field", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						memset(pstSAFRecordNode->szTransTime, 0x00, sizeof(pstSAFRecordNode->szTransTime));
						pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
					}

					/*-------------- Crossing SAF Record CASHBACK_AMNT field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record BAR_CODE field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record PIN_CODE field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record APR_TYPE field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record INIT_VECTOR field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record PASS_THROUGH_FIELDS field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record BATCH_TRACE_ID field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record Place holder PIPE_FIELDS field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record Place holder PIPE_FIELDS field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					/*-------------- Crossing SAF Record Place holder PIPE_FIELDS field -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					if(pchNext != NULL)
					{
						pchFirst = pchNext + 1;
					}

					if((*pchFirst) == SEMI_COLON)
					{
						debug_sprintf(szDbgMsg, "%s: This Record is from SAFRecords.db File or May be a old SAF Completed Record, Not parsing further", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Data still present, This record may be a from completed records", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						/*-------------- Getting SAF Record LP_TOKEN -----------------*/
						if((*pchFirst) != PIPE) //Record contains LP Token
						{
							debug_sprintf(szDbgMsg, "%s: Record contains LP Token field", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//				free(pstSAFRecordNode);
								//				return FAILURE;
								break;
							}
							memset(pstSAFRecordNode->szLPToken , 0x00, sizeof(pstSAFRecordNode->szLPToken));
							strncpy(pstSAFRecordNode->szLPToken, pchFirst, (pchNext - pchFirst));

							pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain LP Token field", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(pstSAFRecordNode->szLPToken, 0x00, sizeof(pstSAFRecordNode->szLPToken));
							pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
						}

						/*-------------- Getting SAF Record PPCV -----------------*/
						if((*pchFirst) != PIPE) //Record contains PPCV
						{
							debug_sprintf(szDbgMsg, "%s: Record contains PPCV field", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//				free(pstSAFRecordNode);
								//				return FAILURE;
								break;
							}
							memset(pstSAFRecordNode->szPPCV , 0x00, sizeof(pstSAFRecordNode->szPPCV));
							strncpy(pstSAFRecordNode->szPPCV, pchFirst, (pchNext - pchFirst));

							pchFirst = pchNext + 1; //pchFirst points to start of AVS_CODE field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain PPCV field", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(pstSAFRecordNode->szPPCV, 0x00, sizeof(pstSAFRecordNode->szPPCV));
							pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
						}

						/*-------------- Getting SAF Record AVS Code -----------------*/
						if((*pchFirst) != PIPE) //Record contains AVS_CODE
						{
							debug_sprintf(szDbgMsg, "%s: Record contains AVS_CODE field", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//		free(pstSAFRecordNode);
								//		return FAILURE;
								break;
							}
							memset(pstSAFRecordNode->szAvsCode , 0x00, sizeof(pstSAFRecordNode->szAvsCode));
							strncpy(pstSAFRecordNode->szAvsCode, pchFirst, (pchNext - pchFirst));

							pchFirst = pchNext + 1; //pchFirst points to start of Host Response Code field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain AVS_CODE field", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(pstSAFRecordNode->szAvsCode, 0x00, sizeof(pstSAFRecordNode->szAvsCode));
							pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
						}

						/*-------------- Getting SAF Record Host Response Code -----------------*/
						if((*pchFirst) != PIPE) //Record contains Host Response Code
						{
							debug_sprintf(szDbgMsg, "%s: Record contains Host Response Code field", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//		free(pstSAFRecordNode);
								//		return FAILURE;
								break;
							}
							memset(pstSAFRecordNode->szHostRespCode , 0x00, sizeof(pstSAFRecordNode->szHostRespCode));
							strncpy(pstSAFRecordNode->szHostRespCode, pchFirst, (pchNext - pchFirst));

							pchFirst = pchNext + 1; //pchFirst points to start of Signature Ref field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain Host Response Code field", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(pstSAFRecordNode->szHostRespCode, 0x00, sizeof(pstSAFRecordNode->szHostRespCode));
							pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
						}

						/*-------------- Getting SAF Record Signature Ref -----------------*/
						if((*pchFirst) != PIPE) //Record contains Signature Ref
						{
							debug_sprintf(szDbgMsg, "%s: Record contains Signature Ref field", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//		free(pstSAFRecordNode);
								//		return FAILURE;
								break;
							}
							memset(pstSAFRecordNode->szSignatureRef , 0x00, sizeof(pstSAFRecordNode->szSignatureRef));
							strncpy(pstSAFRecordNode->szSignatureRef, pchFirst, (pchNext - pchFirst));

							pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain Signature Ref field", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(pstSAFRecordNode->szSignatureRef, 0x00, sizeof(pstSAFRecordNode->szSignatureRef));
							pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
						}

						/*-------------- Getting SAF Record APR_TYPE-----------------*/
						if((*pchFirst) != PIPE) //Record contains APR_TYPE
						{
							debug_sprintf(szDbgMsg, "%s: Record contains APR_TYPE field", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//		free(pstSAFRecordNode);
								//		return FAILURE;
								break;
							}
							memset(pstSAFRecordNode->szAprType , 0x00, sizeof(pstSAFRecordNode->szAprType));
							strncpy(pstSAFRecordNode->szAprType, pchFirst, (pchNext - pchFirst));

							pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain APR_TYPE field", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(pstSAFRecordNode->szAprType, 0x00, sizeof(pstSAFRecordNode->szAprType));
							pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
						}
						/*Akshaya: Fixing PTMX - 1376 - SAF Query not populating Citi Tags*/
						/*-------------- Getting SAF Record ACTION_CODE field -----------------*/
						if((*pchFirst) != PIPE) //Record contains ACTION_CODE
						{
							debug_sprintf(szDbgMsg, "%s: Record contains ACTION_CODE field", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//		free(pstSAFRecordNode);
								//		return FAILURE;
								break;
							}
							memset(pstSAFRecordNode->szActionCode , 0x00, sizeof(pstSAFRecordNode->szActionCode));
							strncpy(pstSAFRecordNode->szActionCode, pchFirst, (pchNext - pchFirst));

							pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain ACTION_CODE field", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(pstSAFRecordNode->szActionCode, 0x00, sizeof(pstSAFRecordNode->szActionCode));
							pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
						}

						/*-------------- Getting SAF Record SVC_PHONE field -----------------*/
						if((*pchFirst) != PIPE) //Record contains SVC_PHONE
						{
							debug_sprintf(szDbgMsg, "%s: Record contains SVC_PHONE field", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//		free(pstSAFRecordNode);
								//		return FAILURE;
								break;
							}
							memset(pstSAFRecordNode->szSvcPhone , 0x00, sizeof(pstSAFRecordNode->szSvcPhone));
							strncpy(pstSAFRecordNode->szSvcPhone, pchFirst, (pchNext - pchFirst));

							pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain SVC_PHONE field", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(pstSAFRecordNode->szSvcPhone, 0x00, sizeof(pstSAFRecordNode->szSvcPhone));
							pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
						}

						/*-------------- Getting SAF Record PURCHASE_APR field -----------------*/
						if((*pchFirst) != PIPE) //Record contains PURCHASE_APR
						{
							debug_sprintf(szDbgMsg, "%s: Record contains PURCHASE_APR field", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//		free(pstSAFRecordNode);
								//		return FAILURE;
								break;
							}
							memset(pstSAFRecordNode->szPurchaseApr , 0x00, sizeof(pstSAFRecordNode->szPurchaseApr));
							strncpy(pstSAFRecordNode->szPurchaseApr, pchFirst, (pchNext - pchFirst));
							pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain PURCHASE_APR field", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(pstSAFRecordNode->szPurchaseApr, 0x00, sizeof(pstSAFRecordNode->szPurchaseApr));
							pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
						}

						/*-------------- Getting SAF Record STATUS_FLAG field -----------------*/
						if((*pchFirst) != PIPE) //Record contains STATUS_FLAG
						{
							debug_sprintf(szDbgMsg, "%s: Record contains STATUS_FLAG field", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//		free(pstSAFRecordNode);
								//		return FAILURE;
								break;
							}
							memset(pstSAFRecordNode->szStatusFlag , 0x00, sizeof(pstSAFRecordNode->szStatusFlag));
							strncpy(pstSAFRecordNode->szStatusFlag, pchFirst, (pchNext - pchFirst));

							pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain STATUS_FLAG field", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(pstSAFRecordNode->szStatusFlag, 0x00, sizeof(pstSAFRecordNode->szStatusFlag));
							pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
						}

						/*-------------- Getting SAF Record CREDIT_PLAN_NBR field -----------------*/
						if((*pchFirst) != PIPE) //Record contains CREDIT_PLAN_NBR
						{
							debug_sprintf(szDbgMsg, "%s: Record contains CREDIT_PLAN_NBR field", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//		free(pstSAFRecordNode);
								//		return FAILURE;
								break;
							}
							memset(pstSAFRecordNode->szCreditPlanNbr , 0x00, sizeof(pstSAFRecordNode->szCreditPlanNbr));
							strncpy(pstSAFRecordNode->szCreditPlanNbr, pchFirst, (pchNext - pchFirst));

							pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain CREDIT_PLAN_NBR field", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(pstSAFRecordNode->szCreditPlanNbr, 0x00, sizeof(pstSAFRecordNode->szCreditPlanNbr));
							pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
						}

						/*-------------- Getting SAF Record RECEIPT_TEXT field -----------------*/
						if((*pchFirst) != PIPE) //Record contains RECEIPT_TEXT
						{
							debug_sprintf(szDbgMsg, "%s: Record contains RECEIPT_TEXT field", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//		free(pstSAFRecordNode);
								//		return FAILURE;
								break;
							}
							memset(pstSAFRecordNode->szReceiptText , 0x00, sizeof(pstSAFRecordNode->szReceiptText));
							strncpy(pstSAFRecordNode->szReceiptText, pchFirst, (pchNext - pchFirst));

							pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain RECEIPT_TEXT field", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(pstSAFRecordNode->szReceiptText, 0x00, sizeof(pstSAFRecordNode->szReceiptText));
							pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
						}

						/*-------------- Getting PASS THROUGH FIELDS-----------------*/
						if((*pchFirst) != PIPE) //Record contains pass through fields
						{
							debug_sprintf(szDbgMsg, "%s: Record contains Pass Through fields", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//		free(pstSAFRecordNode);
								//		return FAILURE;
								break;
							}

							fillPassThrghRespDtlsToSAFRec(pchFirst, &(pstSAFRecordNode->pstPassThrghDtls));
							/* T_RaghavendranR1_21072016 : Not required to get the return pointer for the above function, We are sending a location which is not a correct location pointing to next field.
							 * Hence a crash noticed while filling the next updated completed record field.
							 * So we move the pchFirst based on the normal method, assuming our response will not contain the | in the response fields.
							 */
							pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain Pass Through fields", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							pstSAFRecordNode->pstPassThrghDtls = NULL;
							pchFirst = pchFirst + 1; //pchFirst points to start of the  next SAF record field
						}

						/*-------------- Getting SAF Record AUTHNWID -----------------*/
						if((*pchFirst) != PIPE) //Record contains AuthNwID
						{
							debug_sprintf(szDbgMsg, "%s: Record contains Auth Network ID field", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							pchNext = strchr(pchFirst, PIPE);
							if(pchNext == NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								//		free(pstSAFRecordNode);
								//		return FAILURE;
								break;
							}
							memset(pstSAFRecordNode->szAthNtwID , 0x00, sizeof(pstSAFRecordNode->szAthNtwID));
							strncpy(pstSAFRecordNode->szAthNtwID, pchFirst, (pchNext - pchFirst));

							pchFirst = pchNext + 1; //pchFirst points to start of the next AUTHNWID record field
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Record does not contain Approved Amount field", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							memset(pstSAFRecordNode->szAthNtwID, 0x00, sizeof(pstSAFRecordNode->szAthNtwID));
							pchFirst = pchFirst + 1; //pchFirst points to start of the  TAX_IND SAF record field
						}
					}

				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Last Record is Business Date", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Last Record is CVV2 Code", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Last Record is Auth Code", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	}

	iRetVal = addNodeToSAFList(pstSAFRecordNode, pstSAFDtls);
	if(iRetVal == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Successfully added the details to the list", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Error while adding the details to the list", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: updateSAFRecAuthCode
 *
 * Description	: This API parses parses the SAF record line for the AUTH Code
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ============================================================================
 */
static char * updateSAFRecAuthCode(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	FTRANDTLS_STYPE stFollowTranDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stFollowTranDtls, 0x00, sizeof(FTRANDTLS_STYPE));

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	/*-------------- Pass the SAF Status Field -----------------*/
	pchFirst = strchr(pszSAFRec, PIPE);
	if(pchFirst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Number field

	/* Daivik : 4/Feb/2016 - Coverity 67326 - Validate pchFirst before accessing it below */
	if(pchFirst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: pchFirst is NULL ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	/*-------------- Pass the SAF Record Number -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	/* Daivik : 4/feb/2016 We need to check the pchNext value instead of the pchFirst Value.
	 * pchNext will denote whether we found PIPE or not.
	 */
	if(pchNext == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	pchFirst = pchNext + 1; //pchFirst points to start of the Auth Code
	/* Daivik : 4/Feb/2016 - Coverity 67326 - Validate pchFirst before accessing it below */
	if(pchFirst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: pchFirst is NULL ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchFirst;
	}
	/*-------------- Getting SAF Record AUTH Code -----------------*/
	if((*pchFirst) != PIPE) //Record contains Account Number field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains AUTH code field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return NULL;
		}
		strncpy(stFollowTranDtls.szAuthCode, pchFirst, (pchNext - pchFirst));
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain AUTH code field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = pchFirst; //pchFirst points to PIPE before the Account number
	}

	if(strlen(stFollowTranDtls.szAuthCode) > 0)//Need to update the Key with these details
	{
		debug_sprintf(szDbgMsg, "%s: Auth code is present, need to update the SSI Key with the value", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(strlen(szSSITranKey) > 0)
		{
			iRetVal = updateFollowOnDtlsForPymt(szSSITranKey, &stFollowTranDtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated the stack with the value", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while updating the stack!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Stack (SSI Key) is not created to update, Error !!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;

}

/*
 * ==============================================================================
 * Function Name: updateSAFRecCardDtls
 *
 * Description	: This API parses the SAF record line for the Card Details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ==============================================================================
 */
static char * updateSAFRecCardDtls(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	char			*curPtr			  = NULL;
	char			*nxtPtr			  = NULL;
 	CARDDTLS_STYPE  stCardDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record Account Number -----------------*/
	if((*pchFirst) != PIPE) //Record contains Account Number field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Account Number field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return NULL;
		}


		/*
		 * In case of RSA Man entry there will be enc blob
		 * store in the format PAN/ENCBlob in the SAF Record
		 * Both PAN and ENCBlob are read from the SAF Record and stored in the cardDtls
		 */

		curPtr = pchFirst;
		nxtPtr = strchr(curPtr, '#');
		if(nxtPtr != NULL)
		{
			memcpy(stCardDtls.szPAN, curPtr, nxtPtr - curPtr);

			nxtPtr = nxtPtr + 1;
			memcpy(stCardDtls.szEncBlob, nxtPtr, pchNext - nxtPtr);
		}
		else
		{
			strncpy(stCardDtls.szPAN, pchFirst, pchNext - pchFirst);
		}

		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Track2 field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Account Number field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Track2 field
	}

	/*-------------- Getting SAF Record Track2 data -----------------*/
	if((*pchFirst) != PIPE) //Record contains Track2 field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Track2 field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stCardDtls.szTrackData, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Transaction Amount field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Track2 field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Expiry Month field
	}

	/*-------------- Getting SAF Record Expiry Month -----------------*/
	if((*pchFirst) != PIPE) //Record contains Expiry Month field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Expiry Month field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stCardDtls.szExpMon, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Expiry Year field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Expiry field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Expiry Year field
	}

	/*-------------- Getting SAF Record Expiry Year -----------------*/
	if((*pchFirst) != PIPE) //Record contains Expiry Year field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Expiry Year field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stCardDtls.szExpYear, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Payment Media field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Expiry field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Payment Media field
	}

	/*-------------- Getting SAF Record Payment Media -----------------*/
	if((*pchFirst) != PIPE) //Record contains Payment Media field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Payment Media field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stCardDtls.szPymtMedia, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Present Flag
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Payment Media field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Present Flag
	}

	/*-------------- Getting SAF Record Card Source-----------------*/
	if(*pchFirst != PIPE)
	{
		//char *	szProcId	= NULL;
		char	szTmp[10]	= "";

		/* Card source data is present in the file for this record */
		debug_sprintf(szDbgMsg, "%s: Card Source present in the file", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Doesnt contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return pchNext;
		}

		/* Payment type is CREDIT (only one payment type currently supported
		 * for SAF) */
		memcpy(szTmp, pchFirst, pchNext - pchFirst);

		debug_sprintf(szDbgMsg, "%s: Card Source  = %s",__FUNCTION__, szTmp);
		APP_TRACE(szDbgMsg);

		if(szTmp[0] == CRD_MSR)
		{
			//stCardDtls.iCardSrc = 3;
			stCardDtls.iCardSrc = CRD_MSR;
		}
		else if(szTmp[0] == CRD_RFID)
		{
			//stCardDtls.iCardSrc = 4;
			stCardDtls.iCardSrc = CRD_RFID;
		}
		else if(szTmp[0] == CRD_NFC)
		{
			//stCardDtls.iCardSrc = 4;
			stCardDtls.iCardSrc = CRD_NFC;
		}
		else if(szTmp[0] == '5')
		{
			stCardDtls.iCardSrc = 5;
		}
		else if(szTmp[0] == '6')
		{
			stCardDtls.iCardSrc = 6;
		}
		else if(szTmp[0] == '7')
		{
			stCardDtls.iCardSrc = 7;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Card Source [%s]!!!",__FUNCTION__, szTmp);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		/* Card source data is missing from the file for this record */
		debug_sprintf(szDbgMsg, "%s: Card Source not present in the file",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Must be a manual entry case */
		stCardDtls.iCardSrc = 2;

		pchNext = pchFirst;
	}

	/*-------------- Updating the manual entry flag -----------------*/

	if((strlen(stCardDtls.szTrackData) == 0) && (strlen(stCardDtls.szPAN) > 0))
	{
		/*
		 * Track data is not present and Account number is present in the
		 * SAF record, considering it as the manual entry
		 */
		stCardDtls.bManEntry = PAAS_TRUE;
	}
	else
	{
		/*
		 * Track data is present in the
		 * SAF record, considering it as the swiped transaction
		 */
		stCardDtls.bManEntry = PAAS_FALSE;
	}

	/* Set the encryption type */
	stCardDtls.iEncType = getEncryptionType();

	if(strlen(szSSITranKey) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Key/stack is present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		iRetVal = updateCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Card Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the card details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ==============================================================================
 * Function Name: updateSAFRecEncryptionDtls
 *
 * Description	: This API parses parses the SAF record line for the Encryption Details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ==============================================================================
 */
static char * updateSAFRecEncryptionDtls(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	CARDDTLS_STYPE  stCardDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
	getCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	while(1)
	{
		/*-------------- Getting SAF Record Encryption Type-----------------*/
		if((*pchFirst) != PIPE) //Record contains Encryption Type field
		{
			//char *	szProcId	= NULL;
			char	szTmp[10]	= "";

			debug_sprintf(szDbgMsg, "%s: Record contains encryption type field", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Doesnt contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return pchNext;
			}
			memcpy(szTmp, pchFirst, pchNext - pchFirst);

			debug_sprintf(szDbgMsg, "%s: Encryption Type [%s]", __FUNCTION__, szTmp);
			APP_TRACE(szDbgMsg);

			stCardDtls.iEncType = atoi(szTmp);

			debug_sprintf(szDbgMsg, "%s: Encryption Type Value [%d]", __FUNCTION__, stCardDtls.iEncType);
			APP_TRACE(szDbgMsg);

			pchFirst = pchNext + 1; //pchFirst points to start of the Encryption payload field
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Encryption Type field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchFirst = pchFirst + 1; //pchFirst points to start of the Encryption payload field
		}

		/*-------------- Getting SAF Record Encryption payload field -----------------*/
		if((*pchFirst) != PIPE) //Record contains Encryption payload field
		{
			debug_sprintf(szDbgMsg, "%s: Record contains Encryption payload field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return pchNext;
			}
			strncpy(stCardDtls.szEncPayLoad, pchFirst, (pchNext - pchFirst));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Encryption payload field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		
		if(strlen(szSSITranKey) > 0)
		{
			debug_sprintf(szDbgMsg, "%s: Key/stack is present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = updateCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated with encryption details of SAF record",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating the encryption details ",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
			
		if( strlen(stCardDtls.szEncPayLoad) > 0) //Update encryption dtls only if present
		{
			//Praveen_P1: Moved the code from here to above tomake sure that card details are encrypted even payload is not present
		}
		else
		{	//ArjunU1: For backward compatibility don't return NULL if encryption dtls are not present in the SAF record.
			debug_sprintf(szDbgMsg, "%s: Encryption Details are not present, Not updating encryption dtls in card dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = pchFirst;
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ==============================================================================
 * Function Name: updateSAFRecCardTokenDtls
 *
 * Description	: This API parses the SAF record line for the card token details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ==============================================================================
 */
static char * updateSAFRecCardTokenDtls(char *pszSAFRec)
{
	char				*pchFirst 		  = NULL;
	char 				*pchNext 		  = NULL;
	FTRANDTLS_STYPE		stFollowOnDtlsLst;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stFollowOnDtlsLst, 0x00, sizeof(FTRANDTLS_STYPE));
	getFollowOnDtlsForPymt(szSSITranKey, &stFollowOnDtlsLst);

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	while(1)
	{
		/*-------------- Getting SAF Record Card token field-----------------*/
		if((*pchFirst) != PIPE) //Record contains card token field
		{
			debug_sprintf(szDbgMsg, "%s: Record contains card token field", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Doesn't contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return pchNext;
			}
			memcpy(stFollowOnDtlsLst.szCardToken, pchFirst, pchNext - pchFirst);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain card token field", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pchNext = pchFirst;
		}

		if( strlen(stFollowOnDtlsLst.szCardToken) > 0) //Update card token dtls only if present
		{
			if(strlen(szSSITranKey) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Key/stack is present to update",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				if( SUCCESS != updateFollowOnDtlsForPymt(szSSITranKey, &stFollowOnDtlsLst))
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to update follow-on dtls", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					pchNext = NULL;
					break;
				}
				debug_sprintf(szDbgMsg, "%s: Successfully updated with card token details of SAF record",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}
		else
		{	//ArjunU1: For backward compatibility don't return NULL if card token dtls are not present in the SAF record.
			debug_sprintf(szDbgMsg, "%s: Card token details are not present, Not updating card token dtls in cur tran dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = pchFirst;
		}
		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ==============================================================================
 * Function Name: updateSAFRecCVV2Dtls
 *
 * Description	: This API parses the SAF record line for the CVV2 details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ==============================================================================
 */
static char * updateSAFRecCVV2Dtls(char *pszSAFRec)
{
	int					iRetVal			  = 0;
	char				*pchFirst 		  = NULL;
	char 				*pchNext 		  = NULL;
	CARDDTLS_STYPE		stCardDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
	getCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	while(1)
	{
		/*-------------- Getting SAF Record CVV2 field-----------------*/
		if((*pchFirst) != PIPE) //Record contains CVV2 field
		{
			debug_sprintf(szDbgMsg, "%s: Record contains CVV2 field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return pchNext;
			}
			strncpy(stCardDtls.szCVV, pchFirst, (pchNext - pchFirst));
			pchFirst = pchNext + 1;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain CVV2 field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = pchFirst;
		}


		if(strlen(stCardDtls.szCVV) > 0)//Need to update the CVV2 Details only if Present.
		{
			if(strlen(szSSITranKey) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Key/stack is present to update",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = updateCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully updated with Card Details with CVV2 Details of SAF record",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating the CVV2 Details of Card details ",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					pchNext = NULL;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ============================================================================
 * Function Name: updateSAFRecForceFlag
 *
 * Description	: This API parses parses the SAF record line for the Force Flag
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ============================================================================
 */
static char * updateSAFRecForceFlag(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	PAAS_BOOL        bForceFlag       = PAAS_TRUE;
	char			 szTemp[10]       = "";

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record Force Flag -----------------*/
	if((*pchFirst) != PIPE) //Record contains Force Flag field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Force Flag field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		memcpy(szTemp, pchFirst, pchNext - pchFirst);
		if(szTemp[0] == '1')
		{
			bForceFlag = PAAS_TRUE;
		}
		else if(szTemp[0] == '0')
		{
			bForceFlag = PAAS_FALSE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid force flag value read!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			bForceFlag = PAAS_TRUE;
		}

		if(strlen(szSSITranKey) > 0)
		{
			iRetVal = updateForceFlagForPymtTran(szSSITranKey, bForceFlag);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated with Force flag of SAF record",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating the Force flag ",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Force Flag field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = pchFirst;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ============================================================================
 * Function Name: updateSAFRecBillPayDtls
 *
 * Description	: This API parses parses the SAF record line for the Bill Pay Flag
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ============================================================================
 */
static char * updateSAFRecBillPayDtls(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	PAAS_BOOL        bBillPay       = PAAS_TRUE;
	char			 szTemp[10]       = "";

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record Force Flag -----------------*/
	if((*pchFirst) != PIPE) //Record contains Force Flag field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Bill Pay field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		memcpy(szTemp, pchFirst, pchNext - pchFirst);
		if(szTemp[0] == '1')
		{
			bBillPay = PAAS_TRUE;
		}
		else if(szTemp[0] == '0')
		{
			bBillPay = PAAS_FALSE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Bill Pay value read!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			bBillPay = PAAS_TRUE;
		}

		if(strlen(szSSITranKey) > 0)
		{
			iRetVal = updateBillPayForPymtTran(szSSITranKey, bBillPay);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated with Bill Pay flag of SAF record",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating the Bill Pay flag ",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Bill Pay Flag field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = pchFirst;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ================================================================================
 * Function Name: updateSAFRecTransDatenTimeDtls
 *
 * Description	: This API parses the SAF record line for the Trans Date and Time details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * =================================================================================
 */
static char * updateSAFRecTransDatenTimeDtls(char *pszSAFRec)
{
	int						iRetVal			  = SUCCESS;
	char					*pchFirst 		  = NULL;
	char 					*pchNext 		  = NULL;
	PYMTTRAN_PTYPE			pstPymtDtls		  = NULL;
	TRAN_PTYPE				pstTran			  = NULL;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	if(strlen(szSSITranKey) > 0)
	{
		/* Get the SSI transcation from the stack */
		iRetVal = getTopSSITran(szSSITranKey, &pstTran);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			return pchNext;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return pchNext;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}
	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record Promo Code -----------------*/
	if((*pchFirst) != PIPE) //Record Promo Code
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Transaction Date", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		memset(pstPymtDtls->szOrigTranDt, 0x00, sizeof(pstPymtDtls->szPromoCode));
		strncpy(pstPymtDtls->szOrigTranDt, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the Trans Time
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not Transaction Date", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the Trans Time
	}

	/*-------------- Getting SAF Record Transaction Time -----------------*/
	if((*pchFirst) != PIPE) //Record contains Transaction Time
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Transaction Time", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		memset(pstPymtDtls->szOrigTranTm, 0x00, sizeof(pstPymtDtls->szOrigTranTm));
		strncpy(pstPymtDtls->szOrigTranTm, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Transaction Time", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = pchFirst;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ===============================================================================
 * Function Name: updateSAFRecCashBackDtlsToSAFRec
 *
 * Description	: This API parses parses the SAF record line for the Amount Details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ================================================================================
 */
static char * updateSAFRecCashBackDtlsToSAFRec(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	AMTDTLS_STYPE   stAmountDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stAmountDtls, 0x00, sizeof(AMTDTLS_STYPE));
	iRetVal = getAmtDtlsForPymtTran(szSSITranKey, &stAmountDtls);
	if(iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while fetching the Amount details!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return pchNext;
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record CashBack amount -----------------*/
	if((*pchFirst) != PIPE) //Record contains CashBack Amount field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains CashBack amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stAmountDtls.cashBackAmt, pchFirst, (pchNext - pchFirst));
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Cashback amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = pchFirst;
	}

	if(strlen(szSSITranKey) > 0)
	{
		iRetVal = updateAmtDtlsForPymtTran(szSSITranKey, &stAmountDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Amount Details of SAF record with CashBack Amount",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the CashBack Amount Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ===============================================================================
 * Function Name: updateSAFRecBarCodeDtlsToSAFRec
 *
 * Description	: This API parses parses the SAF record line for the BarCode Details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ================================================================================
 */
static char * updateSAFRecBarCodeDtlsToSAFRec(char *pszSAFRec)
{
	int					iRetVal			  = 0;
	char				*pchFirst 		  = NULL;
	char 				*pchNext 		  = NULL;
	CARDDTLS_STYPE		stCardDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
	getCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT|BAR_CODE|PIN_CODE||||;
	 *
	 */

	pchFirst = pszSAFRec;

	while(1)
	{
		/*-------------- Getting SAF Record Bar Code field-----------------*/
		if((*pchFirst) != PIPE) //Record contains Bar Code field
		{
			debug_sprintf(szDbgMsg, "%s: Record contains Bar Code field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return pchNext;
			}
			strncpy(stCardDtls.szBarCode, pchFirst, (pchNext - pchFirst));
			pchFirst = pchNext + 1;

			 /* Since BarCode is present in the SAF record, We consider it as a Manual Entry.*/
			stCardDtls.bManEntry = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Bar Code field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = pchFirst;
		}


		if(strlen(stCardDtls.szBarCode) > 0)//Need to update the Bar Code Details only if Present.
		{
			if(strlen(szSSITranKey) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Key/stack is present to update",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = updateCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully updated with Card Details with Bar Code Details of SAF record",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating the Bar Code Details of Card details ",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					pchNext = NULL;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ===============================================================================
 * Function Name: updateSAFRecPinCodeDtlsToSAFRec
 *
 * Description	: This API parses parses the SAF record line for the PinCode Details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ================================================================================
 */
static char * updateSAFRecPinCodeDtlsToSAFRec(char *pszSAFRec)
{
	int					iRetVal			  = 0;
	char				*pchFirst 		  = NULL;
	char 				*pchNext 		  = NULL;
	CARDDTLS_STYPE		stCardDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
	getCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	while(1)
	{
		/*-------------- Getting SAF Record Pin Code field-----------------*/
		if((*pchFirst) != PIPE) //Record contains Pin Code field
		{
			debug_sprintf(szDbgMsg, "%s: Record contains Pin Code field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return pchNext;
			}
			strncpy(stCardDtls.szPINCode, pchFirst, (pchNext - pchFirst));
			pchFirst = pchNext + 1;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Pin Code field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = pchFirst;
		}


		if(strlen(stCardDtls.szPINCode) > 0)//Need to update the Pin Code Details only if Present.
		{
			if(strlen(szSSITranKey) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Key/stack is present to update",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = updateCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully updated with Card Details with Pin Code Details of SAF record",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating the Pin Code Details of Card details ",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					pchNext = NULL;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ===============================================================================
 * Function Name: updateSAFAprTypeDtlsToSAFRec
 *
 * Description	: This API parses parses the SAF record line for the PinCode Details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ================================================================================
 */
static char * updateSAFAprTypeDtlsToSAFRec(char *pszSAFRec)
{
	int					iRetVal			  = 0;
	char				*pchFirst 		  = NULL;
	char 				*pchNext 		  = NULL;
	PYMTTRAN_PTYPE			pstPymtDtls		  = NULL;
	TRAN_PTYPE				pstTran			  = NULL;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	if(strlen(szSSITranKey) > 0)
	{
		/* Get the SSI transcation from the stack */
		iRetVal = getTopSSITran(szSSITranKey, &pstTran);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);

			return pchNext;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return pchNext;
		}
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	while(1)
	{
		/*-------------- Getting SAF Record APR_TYPE-----------------*/
		if((*pchFirst) != PIPE) //Record contains APR_TYPE
		{
			debug_sprintf(szDbgMsg, "%s: Record contains APR_TYPE field", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return pchNext;
			}
			memset(pstPymtDtls->szAPRType, 0x00, sizeof(pstPymtDtls->szAPRType));
			strncpy(pstPymtDtls->szAPRType, pchFirst, (pchNext - pchFirst));
			pchFirst = pchNext + 1; //pchFirst points to Start of Next one
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain Apr type field", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pchNext = pchFirst ; //pchFirst points to  start of next one
		}
		break;
	}


	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ==============================================================================
 * Function Name: updateVsdEncDtlsToSAFRec
 *
 * Description	: This API parses the SAF record line for the Card Holder Name
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ==============================================================================
 */
static char * updateVsdEncDtlsToSAFRec(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	char			szTmp[100]		  = "";
	CARDDTLS_STYPE  stCardDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
	getCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT|BAR_CODE|PIN_CODE|APR_TYPE|INIT_VECTOR||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record Encryption Type-----------------*/
	if((*pchFirst) != PIPE) //Record contains Init Vector field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Init Vector field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Doesnt contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return pchNext;
		}
		memcpy(szTmp, pchFirst, pchNext - pchFirst);
		strcpy(stCardDtls.szVSDInitVector,szTmp);

		debug_sprintf(szDbgMsg, "%s: Vsd Init Vector [%s]", __FUNCTION__, szTmp);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not Init Vector field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = pchFirst ;
	}


	if(strlen(szSSITranKey) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Key/stack is present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		iRetVal = updateCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Card Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the card details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ==============================================================================
 * Function Name: updatePassThrghReqsDtlsToSAFRec
 *
 * Description	: This API parses the SAF record line for the Pass through fields & Update them into payment transaction
 *
 * Input Params	: Record Line, PASSTHRG_FIELDS_PTYPE;
 *
 * Output Params: Pointer to the next field of the record
 * ==============================================================================
 */
static char * updatePassThrghReqsDtlsToSAFRec(char *pszSAFRec)
{
	int							iRetVal			  	= SUCCESS;
	int							iReqTagsCnt		  	= 0;
	int							iCnt		  		= 0;
	char						*pchFirst 		  	= NULL;
	char 						*pchNext 		  	= NULL;
	char						szTemp[100]		  	= "";
	char						*pTemp			  	= NULL;
	char						*cPtr			  	= NULL;
	PASSTHRG_FIELDS_STYPE		stPassThrghDtls;
	KEYVAL_PTYPE				pstLocKeyValPtr		= NULL;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}
	while(1)
	{
		memset(&stPassThrghDtls, 0x00, sizeof(PASSTHRG_FIELDS_STYPE));
		/* Format of the SAF Record
		 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
		 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
		 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
		 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
		 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
		 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
		 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT|BAR_CODE|PIN_CODE|APR_TYPE|INIT_VECTOR|PASS_THROUGH_FIELDS|BATCH_TRACE_ID
		 * ||||;
		 *
		 */

		pchFirst = pszSAFRec;

		/*-------------- Getting SAF Record Encryption Type-----------------*/
		if((*pchFirst) != PIPE) //Record contains Pass Through fields
		{
			debug_sprintf(szDbgMsg, "%s: Record contains Pass Through fields", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iReqTagsCnt++; //atleast one pass through field is there.
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Doesnt contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			pTemp = (char*)malloc(pchNext - pchFirst + 1);
			if(pTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return pchNext;
			}
			memset(pTemp, 0x00, pchNext - pchFirst + 1);
			memcpy(pTemp, pchFirst, pchNext - pchFirst);

			// find the total number of pass thorugh fields present in the SAF record
			cPtr = strchr(pTemp, ',');
			while(cPtr != NULL)
			{
				iReqTagsCnt++;
				cPtr = strchr(cPtr + 1, ',');
			}

			// allocate memory for storing each key value pair of pass through fields
			pstLocKeyValPtr = (KEYVAL_PTYPE) malloc(iReqTagsCnt * KEYVAL_SIZE);
			if(pstLocKeyValPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			memset(pstLocKeyValPtr, 0x00, iReqTagsCnt * KEYVAL_SIZE);

			pchFirst = pTemp;
			pchNext = strtok(pchFirst, ",");
			for(iCnt = 0; iCnt < iReqTagsCnt && pchNext != NULL; iCnt++) // T_RaghavendranR1 CID 83373 (#1-2 of 2): Dereference null return value (NULL_RETURNS)
			{
				pchFirst = pchNext;
				// key
				pchNext = strchr(pchFirst, '=');
				if(pchNext != NULL)
				{
					memset(szTemp, 0x00, sizeof(szTemp));
					memcpy(szTemp, pchFirst, pchNext - pchFirst);
					pstLocKeyValPtr[iCnt].key = strdup(szTemp);
					pchFirst = pchNext + 1;
					//value
					memset(szTemp, 0x00, sizeof(szTemp));
					strcpy(szTemp, pchFirst);	// copy til NULL character
					pstLocKeyValPtr[iCnt].value = strdup(szTemp);
					//pchFirst = pchNext + 1; // T_RaghavendranR1 CID 84336 (#1 of 1): Unused value (UNUSED_VALUE)
				}

				pstLocKeyValPtr[iCnt].valType = SINGLETON;
				pchNext = strtok(NULL, ",");
				debug_sprintf(szDbgMsg, "%s: Pass Through Field[%d] Key[%s] Value[%s]", __FUNCTION__, iCnt + 1, pstLocKeyValPtr[iCnt].key,
						(char*)pstLocKeyValPtr[iCnt].value);
				APP_TRACE(szDbgMsg);
			}

			if(pchNext != NULL)
			{
				pchNext++;
			}

			if(strlen(szSSITranKey) > 0)
			{
				debug_sprintf(szDbgMsg, "%s: Key/stack is present to update",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				stPassThrghDtls.iReqTagsCnt = iReqTagsCnt;
				stPassThrghDtls.pstReqTagList = pstLocKeyValPtr;

				iRetVal = updatePassThrghDtlsForPymtTran(szSSITranKey, &stPassThrghDtls);

				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully updated with Pass Through Details of SAF record",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating the pass through details ",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					pchNext = NULL;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain pass through fields", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pchNext = pchFirst ;
		}
		break;
	}

	// free the memory allocated for pTemp
	free(pTemp);
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ==============================================================================
 * Function Name: updateBatchTraceIDReqsDtlsToSAFRec
 *
 * Description	: This API parses the SAF record line for the Batch Trace ID & Update them into payment transaction
 *
 * Input Params	: Record Line, PASSTHRG_FIELDS_PTYPE;
 *
 * Output Params: Pointer to the next field of the record
 * ==============================================================================
 */
static char * updateBatchTraceIDReqsDtlsToSAFRec(char *pszSAFRec)
{
	char			*pchFirst 		  			= NULL;
	char 			*pchNext				    = NULL;
	char			szBatchTraceId[50+1]        = "";

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record Payment Type -----------------*/
	if((*pchFirst) != PIPE) //Record contains Payment Type field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Batch Trace ID field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		memset(szBatchTraceId, 0x00, sizeof(szBatchTraceId));
		strncpy(szBatchTraceId, pchFirst, (pchNext - pchFirst));

		if((strlen(szSSITranKey) > 0) && (strlen(szBatchTraceId) > 0))
		{
			/* Set the Batch Trace Id */
			updateBatchTraceIdForPymtTran(szSSITranKey, szBatchTraceId);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain  Batch Trace ID field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = pchFirst;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ==============================================================================
 * Function Name: updatePassThrghRespDtlstoSAFRec
 *
 * Description	: This API updates the pass through response details into SAF completion records
 *
 * Input Params	: Record Line
 *
 * Output Params: SUCCESS/FAILURE
 * ==============================================================================
 */
static int updatePassThrghRespDtlstoSAFRec(char *pszSAFRec)
{
	int							iRetVal			  	= SUCCESS;
	int							iCnt		  		= 0;
	int							iTotCnt		  		= 0;
	KEYVAL_PTYPE				pstLocKeyValPtr		= NULL;
	PASSTHRG_FIELDS_STYPE		stPassThrghDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	while(1)
	{
		if(strlen(szSSITranKey) > 0)
		{
			debug_sprintf(szDbgMsg, "%s: Key/stack is present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			memset(&stPassThrghDtls, 0x00, sizeof(PASSTHRG_FIELDS_STYPE));
			iRetVal = getPassThrghDtlsForPymtTran(szSSITranKey, &stPassThrghDtls);
			if(iRetVal != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the pass through details ",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		if(stPassThrghDtls.pstResTagList == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pass Through Fields are not present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		else
		{
			pstLocKeyValPtr = stPassThrghDtls.pstResTagList;
			iTotCnt = stPassThrghDtls.iResTagsCnt;

			for(iCnt = 0; iCnt < iTotCnt; iCnt++)
			{
				if(pstLocKeyValPtr[iCnt].value != NULL)
				{
					strcat(pszSAFRec, pstLocKeyValPtr[iCnt].key);
					strcat(pszSAFRec, "=");
					strcat(pszSAFRec, (char*)pstLocKeyValPtr[iCnt].value);
					strcat(pszSAFRec, ",");
				}
			}
			// remove the last (,) separator from pszSAFRec
			if(pszSAFRec[strlen(pszSAFRec) - 1] == ',')
			{
				pszSAFRec[strlen(pszSAFRec) - 1] = '\0';
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ===============================================================================
 * Function Name: updateSAFRecAmountDtls
 *
 * Description	: This API parses parses the SAF record line for the Amount Details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ================================================================================
 */
static char * updateSAFRecAmountDtls(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	AMTDTLS_STYPE   stAmountDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stAmountDtls, 0x00, sizeof(AMTDTLS_STYPE));

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record Transaction amount -----------------*/
	if((*pchFirst) != PIPE) //Record contains Transaction amount field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Transaction amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stAmountDtls.tranAmt, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Tip Amount field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Transaction amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Tip Amount field
	}

	/*-------------- Getting SAF Record TIP amount -----------------*/
	if((*pchFirst) != PIPE) //Record contains Tip Amount field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Tip amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stAmountDtls.tipAmt, pchFirst, (pchNext - pchFirst));
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Tip amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = pchFirst;
	}

	if(strlen(szSSITranKey) > 0)
	{
		iRetVal = updateAmtDtlsForPymtTran(szSSITranKey, &stAmountDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Amount Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Amount Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ============================================================================
 * Function Name: updateSAFRecBankUserData
 *
 * Description	: This API parses the SAF record line for the Bank User Data
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ============================================================================
 */
static char * updateSAFRecBankUserData(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	FTRANDTLS_STYPE stFollowTranDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stFollowTranDtls, 0x00, sizeof(FTRANDTLS_STYPE));

	getFollowOnDtlsForPymt(szSSITranKey, &stFollowTranDtls);

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */
	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record CTroutd -----------------*/
	if((*pchFirst) != PIPE) //Record contains CTROUTD field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Bank User Data field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stFollowTranDtls.szBankUserData, pchFirst, (pchNext - pchFirst));

		if(strlen(szSSITranKey) > 0)
		{
			iRetVal = updateFollowOnDtlsForPymt(szSSITranKey, &stFollowTranDtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated the stack with the value", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while updating the stack!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Bank User Data field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = pchFirst;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;

}

/*
 * ============================================================================
 * Function Name: updateSAFRecCTroutd
 *
 * Description	: This API parses parses the SAF record line for the CTroutd
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ============================================================================
 */
static char * updateSAFRecCTroutd(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	FTRANDTLS_STYPE stFollowTranDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stFollowTranDtls, 0x00, sizeof(FTRANDTLS_STYPE));

	getFollowOnDtlsForPymt(szSSITranKey, &stFollowTranDtls);

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record CTroutd -----------------*/
	if((*pchFirst) != PIPE) //Record contains CTROUTD field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains CTROUTD field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stFollowTranDtls.szCTroutd, pchFirst, (pchNext - pchFirst));

		if(strlen(szSSITranKey) > 0)
		{
			iRetVal = updateFollowOnDtlsForPymt(szSSITranKey, &stFollowTranDtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated the stack with the value", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while updating the stack!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain CTROUTD field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = pchFirst;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;

}

/*
 * ===============================================================================
 * Function Name: updateSAFRecTaxAmount
 *
 * Description	: This API parses parses the SAF record line for the Tax Amount
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ================================================================================
 */
static char * updateSAFRecTaxAmount(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	LVL2_STYPE   	stLevel2Dtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stLevel2Dtls, 0x00, sizeof(LVL2_STYPE));

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record TAX amount -----------------*/
	if((*pchFirst) != PIPE) //Record contains Tax Amount field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Tax amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stLevel2Dtls.taxAmt, pchFirst, (pchNext - pchFirst));

		if(strlen(szSSITranKey) > 0)
		{
			iRetVal = updateTaxDtlsForPymtTran(szSSITranKey, &stLevel2Dtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated with Tax Amount Details of SAF record",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating the Tax amount Details ",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Tax amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = pchFirst;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ===============================================================================
 * Function Name: updateSAFRecTaxIndicator
 *
 * Description	: This API parses parses the SAF record line for the Tax Indicator
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ================================================================================
 */
static char * updateSAFRecTaxIndicator(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			szTaxInd[5]		  = "";
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	LVL2_STYPE   	stLevel2Dtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stLevel2Dtls, 0x00, sizeof(LVL2_STYPE));

	/*-------- Getting Current Level2 Details ------------ */
	iRetVal = getLevel2DtlsForPymtTran(szSSITranKey, &stLevel2Dtls);
	if(iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Level2 Details are not present",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		//return NULL;
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record TAX Indicator -----------------*/
	if((*pchFirst) != PIPE) //Record contains Tax indicator field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Tax Indicator field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(szTaxInd, pchFirst, (pchNext - pchFirst));

		stLevel2Dtls.taxInd = atoi(szTaxInd);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Tax Indicator field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

	  /* If tax indicator is not present we will set tax indicator to -1 default value since 0 is the
	   * valid value for this field.
	   */
		stLevel2Dtls.taxInd = -1;

		pchNext = pchFirst;
	}

	//Updating the transaction instance with -1 i.e. default value or actual value from the SAF record
	if(strlen(szSSITranKey) > 0)
	{
		iRetVal = updateTaxDtlsForPymtTran(szSSITranKey, &stLevel2Dtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Tax Amount Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Tax amount Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ==============================================================================
 * Function Name: updateSAFRecCommand
 *
 * Description	: This API parses parses the SAF record line for the command type
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ==============================================================================
 */
static char * updateSAFRecCommand(char *pszSAFRec)
{
	int				iCmd			  = -1;
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	char			szTmp[10]		  = "";
	CARDDTLS_STYPE	stCardDtls;
#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	while(1)
	{
		/*-------------- Getting SAF Record Command Type-----------------*/
		if((*pchFirst) != PIPE) //Record contains command type field
		{
			debug_sprintf(szDbgMsg, "%s: Record contains command type field", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Doesnt contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return pchNext;
			}
			memcpy(szTmp, pchFirst, pchNext - pchFirst);
			iCmd = atoi(szTmp);

			debug_sprintf(szDbgMsg, "%s: command Type [%d]", __FUNCTION__, iCmd);
			APP_TRACE(szDbgMsg);

			/* Set function and command in the transaction instance */
			setFxnNCmdForSSITran(szSSITranKey, SSI_PYMT, iCmd);

			if(iCmd == SSI_PAYACCOUNT && (getEncryptionType() == VSP_ENC))
			{
				if(strlen(szSSITranKey) > 0)
				{
					memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
					getCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

					stCardDtls.bManEntry = PAAS_TRUE;

					debug_sprintf(szDbgMsg, "%s: Key/stack is present to update",__FUNCTION__);
					APP_TRACE(szDbgMsg);

					iRetVal = updateCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

					if(iRetVal == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Successfully updated with encryption details of SAF record",__FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error while updating the encryption details ",__FUNCTION__);
						APP_TRACE(szDbgMsg);
						pchNext = NULL;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					pchNext = NULL;
				}
			}


			//pchFirst = pchNext + 1; //pchFirst points to start of the track indicator field
		}
		else
		{
			/* Set default function and command in the transaction instance */
			setFxnNCmdForSSITran(szSSITranKey, SSI_PYMT, SSI_SALE);

			debug_sprintf(szDbgMsg, "%s: Record does not contain command type field, setting default command as SALE", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pchNext = pchFirst; //pchFirst points to start of the track indicator field
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ==============================================================================
 * Function Name: updateSAFRecCardTrkIndicator
 *
 * Description	: This API parses parses the SAF record line for the card track indicator
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ==============================================================================
 */
static char * updateSAFRecCardTrkIndicator(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	char			szTmp[10]		  = "";
	CARDDTLS_STYPE  stCardDtls;
#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
	getCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	while(1)
	{
		/*-------------- Getting SAF Record Track Indicator-----------------*/
		if((*pchFirst) != PIPE) //Record contains track indicator field
		{
			debug_sprintf(szDbgMsg, "%s: Record contains track indicator field", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Doesnt contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return pchNext;
			}
			memcpy(szTmp, pchFirst, pchNext - pchFirst);
			stCardDtls.iTrkNo = atoi(szTmp);

			debug_sprintf(szDbgMsg, "%s: Track Number [%d]", __FUNCTION__, stCardDtls.iTrkNo);
			APP_TRACE(szDbgMsg);

			if( strlen(szSSITranKey) > 0 )
			{
				debug_sprintf(szDbgMsg, "%s: Key/stack is present to update",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = updateCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully updated with Card Details (Track Indicator) of SAF record",__FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while updating the card details (Track Indicator)",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					pchNext = NULL;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}

			//pchFirst = pchNext + 1; //pchFirst points to start of the track indicator field
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain command type field", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pchNext = pchFirst; //pchFirst points to start of the track indicator field
		}

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ===============================================================================
 * Function Name: updateSAFRecPymtType
 *
 * Description	: This API parses the SAF record line for the Payment Type
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ================================================================================
 */
static char * updateSAFRecPymtType(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	int				iPaymentType      = 0;
	char			szTemp[25]        = "";

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record Payment Type -----------------*/
	if((*pchFirst) != PIPE) //Record contains Payment Type field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Payment Type field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		memset(szTemp, 0x00, sizeof(szTemp));
		strncpy(szTemp, pchFirst, (pchNext - pchFirst));

		if(!strcmp(szTemp, "CREDIT"))
		{
			iPaymentType = PYMT_CREDIT;
		}
		else if(!strcmp(szTemp, "PRIV_LBL"))
		{
			iPaymentType = PYMT_PRIVATE;
		}
		else if(!strcmp(szTemp, "DEBIT"))
		{
			iPaymentType = PYMT_DEBIT;
		}
		else if(!strcmp(szTemp, "MERCH_CREDIT"))
		{
			iPaymentType = PYMT_MERCHCREDIT;
		}
		else if(!strcmp(szTemp, "GIFT"))
		{
			iPaymentType = PYMT_GIFT;
		}
		else if(!strcmp(szTemp, "PAYPAL"))
		{
			iPaymentType = PYMT_PAYPAL;
		}
		else if(!strcmp(szTemp, "EBT"))
		{
			iPaymentType = PYMT_EBT;
		}
		else if(!strcmp(szTemp, "PAYACCOUNT"))
		{
			iPaymentType = PYMT_PAYACCOUNT;
		}
		else if(!strcmp(szTemp, "CHECK"))
		{
			iPaymentType = PYMT_CHECK;
		}
		else
		{
			iPaymentType = PYMT_CREDIT; //TODO: Is this fine to set default payment type as CREDIT if payment type is not available.
		}

		if(strlen(szSSITranKey) > 0)
		{
			iRetVal = updatePymtTypeForPymtTran(szSSITranKey, iPaymentType);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated with Payment Type Detail of SAF record",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating the Payment Type Details ",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Payment Type field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = pchFirst;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ================================================================================
 * Function Name: updateSAFRecSessionDtls
 *
 * Description	: This API parses the SAF record line for the Session Details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * =================================================================================
 */
static char * updateSAFRecSessionDtls(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	SESSDTLS_STYPE  stSessionDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stSessionDtls, 0x00, sizeof(SESSDTLS_STYPE));

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;
	/*-------------- Getting SAF Record Invoice -----------------*/
	if((*pchFirst) != PIPE) //Record contains Invoice field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Invoice field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stSessionDtls.szInvoice, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Purchase ID field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Invoice field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Purchase ID field
	}

	/*-------------- Getting SAF Record Purchase ID -----------------*/
	if((*pchFirst) != PIPE) //Record contains Purchase ID field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Purchase ID field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stSessionDtls.szPurchaseId, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Cashier Number field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Purchase ID field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Cashier Number field
	}

	/*-------------- Getting SAF Record Cashier Number -----------------*/
	if((*pchFirst) != PIPE) //Record contains Cashier Number field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Cashier Number field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return NULL;
		}
		strncpy(stSessionDtls.szCashierId, pchFirst, (pchNext - pchFirst));

	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Cashier Number field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = pchFirst;
	}

	if(strlen(szSSITranKey) > 0)
	{
		iRetVal = updateSessDtlsForPymtTran(szSSITranKey, &stSessionDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Session Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Session Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ================================================================================
 * Function Name: updateSAFRecSessStoreIdDtls
 *
 * Description	: This API parses the SAF record line for the Session Details(Store Id)
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * =================================================================================
 */
static char * updateSAFRecSessStoreIdDtls(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	SESSDTLS_STYPE  stSessionDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stSessionDtls, 0x00, sizeof(SESSDTLS_STYPE));

	iRetVal = getSessDtlsForPymtTran(szSSITranKey, &stSessionDtls);
	if( iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while getting the Session Details ",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		return NULL;
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;
	/*-------------- Getting SAF Record Store Id -----------------*/
	if((*pchFirst) != PIPE) //Record contains Invoice field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Store Id field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stSessionDtls.szStoreNo, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the next SAF Record field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Store Id field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = pchFirst;
	}

	if(strlen(szSSITranKey) > 0)
	{
		iRetVal = updateSessDtlsForPymtTran(szSSITranKey, &stSessionDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Session Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Session Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ================================================================================
 * Function Name: updateSAFRecSessLaneIdDtls
 *
 * Description	: This API parses the SAF record line for the Session Details(Lane Id)
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * =================================================================================
 */
static char * updateSAFRecSessLaneIdDtls(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	SESSDTLS_STYPE  stSessionDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stSessionDtls, 0x00, sizeof(SESSDTLS_STYPE));

	iRetVal = getSessDtlsForPymtTran(szSSITranKey, &stSessionDtls);
	if( iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while getting the Session Details ",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = ERR_SAF_SESSDTLS_NOT_FOUND;
		return NULL;
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;
	/*-------------- Getting SAF Record Store Id -----------------*/
	if((*pchFirst) != PIPE) //Record contains Invoice field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Lane Id field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}

		strncpy(stSessionDtls.szLaneNo, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the next SAF Record field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Lane Id field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = pchFirst ;
	}

	if(strlen(szSSITranKey) > 0)
	{
		iRetVal = updateSessDtlsForPymtTran(szSSITranKey, &stSessionDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Session Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Session Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ================================================================================
 * Function Name: updateSAFRecFSADtls
 *
 * Description	: This API parses the SAF record line for the FSA details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * =================================================================================
 */
static char * updateSAFRecFSADtls(char *pszSAFRec)
{
	int						iRetVal			  = SUCCESS;
	char					*pchFirst 		  = NULL;
	char 					*pchNext 		  = NULL;
	FSADTLS_STYPE   		stFSADtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stFSADtls, 0x00, sizeof(FSADTLS_STYPE));

	if(strlen(szSSITranKey) > 0)
	{
		/*-------- Getting FSA Details ------------ */
		iRetVal = getFSADtlsForPymtTran(szSSITranKey, &stFSADtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Missing FSA Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */
	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record Amount HealthCare -----------------*/
	if((*pchFirst) != PIPE) //Record Amount HealthCare
	{
		debug_sprintf(szDbgMsg, "%s: Record contains AMOUNT_HEALTHCARE", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stFSADtls.szAmtHealthCare, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the AMOUNT_PRESCRIPTION field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain AMOUNT_HEALTHCARE", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the AMOUNT_PRESCRIPTION field
	}

	/*-------------- Getting SAF Record AMOUNT_PRESCRIPTION -----------------*/
	if((*pchFirst) != PIPE) //Record contains Duty Amount field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains AMOUNT_PRESCRIPTION field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stFSADtls.szAmtPres, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the AMOUNT_VISION field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain AMOUNT_PRESCRIPTION field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the AMOUNT_VISION field
	}

	/*-------------- Getting SAF Record AMOUNT_VISION -----------------*/
	if((*pchFirst) != PIPE) //Record contains Freight Amount field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains AMOUNT_VISION field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stFSADtls.szAmtVision, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the AMOUNT_CLINIC field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain AMOUNT_VISION field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the AMOUNT_CLINIC field
	}

	/*-------------- Getting SAF Record AMOUNT_CLINIC -----------------*/
	if((*pchFirst) != PIPE) //Record contains Destination Country code field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains AMOUNT_CLINIC field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stFSADtls.szAmtClinic, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the AMOUNT_DENTAL field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain AMOUNT_CLINIC field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the AMOUNT_DENTAL field
	}

	/*-------------- Getting SAF Record AMOUNT_DENTAL -----------------*/
	if((*pchFirst) != PIPE) //Record contains Destination country code field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains AMOUNT_DENTAL field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stFSADtls.szAmtDental, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain AMOUNT_DENTAL field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = pchFirst;
	}

	if(strlen(szSSITranKey) > 0)
	{
		iRetVal = updateFSADtlsForPymtTran(szSSITranKey, &stFSADtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with FSA Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the FSA Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ================================================================================
 * Function Name: updateSAFRecEMVDtls
 *
 * Description	: This API parses the SAF record line for the EMV details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * =================================================================================
 */
static char * updateSAFRecEMVDtls(char *pszSAFRec)
{
	int						iRetVal			  = SUCCESS;
	char					*pchFirst 		  = NULL;
	char 					*pchNext 		  = NULL;
	CARDDTLS_STYPE   		stCardDtls;
	PINDTLS_STYPE			stPINDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
	memset(&stPINDtls, 0x00, sizeof(PINDTLS_STYPE));

	if(strlen(szSSITranKey) > 0)
	{
		/*-------- Getting EMV Details ------------ */
		iRetVal = getCardDtlsForPymtTran(szSSITranKey, &stCardDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Card Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		/*-------- Getting PIN Details ------------ */
		iRetVal = getPINDtlsForPymtTran(szSSITranKey, &stPINDtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Missing PIN Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record EMV Tags -----------------*/
	if((*pchFirst) != PIPE) //Record contains EMV Tags
	{
		debug_sprintf(szDbgMsg, "%s: Record contains EMV Tags", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		stCardDtls.bEmvData = PAAS_TRUE;
		strncpy(stCardDtls.szEMVTags, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the EMV Encrypted Blob field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain EMV Tags", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the EMV Encrypted Blob field
	}

	/*-------------- Getting SAF Record EMV Encrypted Blob -----------------*/
	if((*pchFirst) != PIPE) //Record contains EMV Encrypted Blob field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains EMV Encrypted Blob field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		stCardDtls.bEmvData = PAAS_TRUE;
		strncpy(stCardDtls.szEMVEncBlob, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the EMV Reversal Type field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain EMV Encrypted Blob field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the EMV Reversal Type field
	}

	/*-------------- Getting SAF Record EMV Reversal Type -----------------*/
	if((*pchFirst) != PIPE) //Record contains EMV Reversal Type
	{
		debug_sprintf(szDbgMsg, "%s: Record contains EMV Reversal Type field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		stCardDtls.bEmvData = PAAS_TRUE;
		strncpy(stCardDtls.szEmvReversalType, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to PIN Block
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain EMV Reversal Type field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to PIN Block
	}


	/*-------------- Getting SAF Record PIN Block -----------------*/
	if((*pchFirst) != PIPE) //Record contains PIN Block
	{
		debug_sprintf(szDbgMsg, "%s: Record contains PIN Block Type field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stPINDtls.szPIN, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to Key Serial Number
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain PIN Block field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to  Key Serial Number
	}


	/*-------------- Getting SAF Record  Key Serial Number -----------------*/
	if((*pchFirst) != PIPE) //Record contains  Key Serial Number
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Key Serial Number field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stPINDtls.szKSN, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the Next field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain  Key Serial Number field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = pchFirst; //pchFirst points to start of the Next field
	}

	if(strlen(szSSITranKey) > 0)
	{
		iRetVal = updateCardDtlsForPymtTran(szSSITranKey, &stCardDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with EMV Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the EMV Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}

		iRetVal = updatePINDtlsForPymtTran(szSSITranKey, &stPINDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with PIN Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the PIN Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ================================================================================
 * Function Name: updateSAFRecPymtSubTypeDtls
 *
 * Description	: This API parses the SAF record line for the Payment Sub Type details Required for Gift Activate Transaction
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * =================================================================================
 */
static char * updateSAFRecPymtSubTypeDtls(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	int				iPaymentSubType   = -1;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	char			szTemp[25]        = "";

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record Payment Type -----------------*/
	if((*pchFirst) != PIPE) //Record contains Payment Type field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Payment Sub Type field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		memset(szTemp, 0x00, sizeof(szTemp));
		strncpy(szTemp, pchFirst, (pchNext - pchFirst));

		if(!strcmp(szTemp, "INTERNAL"))
		{
			iPaymentSubType = PYMT_SUBTYPE_INTERNAL;
		}
		else if(!strcmp(szTemp, "EXTERNAL"))
		{
			iPaymentSubType = PYMT_SUBTYPE_EXTERNAL;
		}
		else if(!strcmp(szTemp, "GIFTMALL"))
		{
			iPaymentSubType = PYMT_SUBTYPE_GIFTMALL;
		}
		else if(!strcmp(szTemp, "INCOMM"))
		{
			iPaymentSubType = PYMT_SUBTYPE_INCOMM;
		}
		else if(!strcmp(szTemp, "BLACKHAWK"))
		{
			iPaymentSubType = PYMT_SUBTYPE_BLACKHAWK;
		}
		else
		{
			iPaymentSubType = -1; // Not Sending PAYMENT_SUBTYPE for SAF GIFT ACTIVATE Transactions
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Payment Sub Type field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		iPaymentSubType = -1;

		pchNext = pchFirst;
	}

	if(strlen(szSSITranKey) > 0)
	{

		iRetVal = updatePymtSubTypeForPymtTran(szSSITranKey, iPaymentSubType);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the payment sub type for SAF Trans", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ================================================================================
 * Function Name: updateSAFRecAddPymtDtls
 *
 * Description	: This API parses the SAF record line for the EMV details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * =================================================================================
 */
static char * updateSAFRecAddPymtDtls(char *pszSAFRec)
{
	int						iRetVal			  = SUCCESS;
	char					*pchFirst 		  = NULL;
	char 					*pchNext 		  = NULL;
	PYMTTRAN_PTYPE			pstPymtDtls		  = NULL;
	TRAN_PTYPE				pstTran			  = NULL;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	if(strlen(szSSITranKey) > 0)
	{
		/* Get the SSI transcation from the stack */
		iRetVal = getTopSSITran(szSSITranKey, &pstTran);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get SSI tran from stack",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			return pchNext;
		}

		/* get the payment details placeholder */
		pstPymtDtls = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pymnt dtls not present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return pchNext;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}
	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record Promo Code -----------------*/
	if((*pchFirst) != PIPE) //Record Promo Code
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Promo Code", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		memset(pstPymtDtls->szPromoCode, 0x00, sizeof(pstPymtDtls->szPromoCode));
		strncpy(pstPymtDtls->szPromoCode, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the EMV Encrypted Blob field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not Promo Code", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the Order Date Time
	}

	/*-------------- Getting SAF Record ORDER_DATETIME -----------------*/
	if((*pchFirst) != PIPE) //Record contains ORDER_DATETIME
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Order Date Time field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		memset(pstPymtDtls->szOrderDtTm, 0x00, sizeof(pstPymtDtls->szOrderDtTm));
		strncpy(pstPymtDtls->szOrderDtTm, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the Credit NBR Plan
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Order Date Time field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the Credit NBR Plan
	}

	/*-------------- Getting SAF Record CREDIT_PLAN_NBR -----------------*/
	if((*pchFirst) != PIPE) //Record contains CREDIT_PLAN_NBR
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Credit NBR Plan field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		memset(pstPymtDtls->szCreditPlanNBR, 0x00, sizeof(pstPymtDtls->szCreditPlanNBR));
		strncpy(pstPymtDtls->szCreditPlanNBR, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to Purchase APR
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Credit NBR Plan field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to Purchase APR
	}


	/*-------------- Getting SAF Record Purchase APR -----------------*/
	if((*pchFirst) != PIPE) //Record contains Purchase APR
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Purchase APR field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		memset(pstPymtDtls->szPurchaseApr, 0x00, sizeof(pstPymtDtls->szPurchaseApr));
		strncpy(pstPymtDtls->szPurchaseApr, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to Key Serial Number
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain PIN Block field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = pchFirst; //pchFirst points to  start of next one
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ==============================================================================
 * Function Name: updateSAFRecReferenceDtls
 *
 * Description	: This API parses the SAF record line for the Reference
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ==============================================================================
 */
static char * updateSAFRecReferenceDtls(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	FTRANDTLS_STYPE stFollowTranDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stFollowTranDtls, 0x00, sizeof(FTRANDTLS_STYPE));

	getFollowOnDtlsForPymt(szSSITranKey, &stFollowTranDtls);

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record CTroutd -----------------*/
	if((*pchFirst) != PIPE) //Record contains CTROUTD field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Reference field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stFollowTranDtls.szReference, pchFirst, (pchNext - pchFirst));

		if(strlen(szSSITranKey) > 0)
		{
			iRetVal = updateFollowOnDtlsForPymt(szSSITranKey, &stFollowTranDtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated the stack with the value", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while updating the stack!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain CTROUTD field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = pchFirst;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;

}

/*
 * ==============================================================================
 * Function Name: updateSAFRecCardHolderDtls
 *
 * Description	: This API parses the SAF record line for the Card Holder Name
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * ==============================================================================
 */
static char * updateSAFRecCardHolderDtls(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	char			szTmp[100]		  = "";
	CARDDTLS_STYPE  stCardDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
	getCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record Encryption Type-----------------*/
	if((*pchFirst) != PIPE) //Record contains Encryption Type field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Card Holder Name field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Doesnt contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return pchNext;
		}
		memcpy(szTmp, pchFirst, pchNext - pchFirst);
		strcpy(stCardDtls.szName,szTmp);

		debug_sprintf(szDbgMsg, "%s: Card Holder Name [%s]", __FUNCTION__, szTmp);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not Card Holder Name field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = pchFirst ;
	}


	if(strlen(szSSITranKey) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Key/stack is present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		iRetVal = updateCardDtlsForPymtTran(szSSITranKey, &stCardDtls);

		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Card Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the card details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ================================================================================
 * Function Name: updateSAFRecLevel2Dtls
 *
 * Description	: This API parses the SAF record line for the level2 details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * =================================================================================
 */
static char * updateSAFRecLevel2Dtls(char *pszSAFRec)
{
	int						iRetVal			  = SUCCESS;
	char					*pchFirst 		  = NULL;
	char 					*pchNext 		  = NULL;
	LVL2_STYPE   			stLevel2Dtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stLevel2Dtls, 0x00, sizeof(LVL2_STYPE));

	if(strlen(szSSITranKey) > 0)
	{
		/*-------- Getting Current Level2 Details ------------ */
		iRetVal = getLevel2DtlsForPymtTran(szSSITranKey, &stLevel2Dtls);
		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Level2 Details are not present",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			//return NULL;
		}
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record Discount Amount -----------------*/
	if((*pchFirst) != PIPE) //Record contains Discount Amount field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Discount Amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stLevel2Dtls.discAmt, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the DUTY_AMOUNT field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Discount Amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF DUTY_AMOUNT field
	}

	/*-------------- Getting SAF Record Duty Amount -----------------*/
	if((*pchFirst) != PIPE) //Record contains Duty Amount field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Duty Amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stLevel2Dtls.dutyAmt, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the FREIGHT_AMOUNT field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Duty Amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF FREIGHT_AMOUNT field
	}

	/*-------------- Getting SAF Record Freight Amount -----------------*/
	if((*pchFirst) != PIPE) //Record contains Freight Amount field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Freight Amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stLevel2Dtls.freightAmt, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the DEST_COUNTRY_CODE field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Freight Amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF DEST_COUNTRY_CODE field
	}

	/*-------------- Getting SAF Record Destination Country Code -----------------*/
	if((*pchFirst) != PIPE) //Record contains Destination Country code field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Destination Country Code field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stLevel2Dtls.destCountryCode, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the DEST_POSTAL_CODE field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Destination Country Code field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF DEST_POSTAL_CODE field
	}

	/*-------------- Getting SAF Record Destination Country Code -----------------*/
	if((*pchFirst) != PIPE) //Record contains Destination country code field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Destination Postal Code field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stLevel2Dtls.destPostalcode, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the SHIP_FROM_ZIP_CODE field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Destination Postal Code field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF SHIP_FROM_ZIP_CODE field
	}

	/*-------------- Getting SAF Record Destination Country Code -----------------*/
	if((*pchFirst) != PIPE) //Record contains Destination county code field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Ship Postal Code field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stLevel2Dtls.shipPostalcode, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the RETAIL_ITEM_DESC_1 field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Ship Postal Code field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF RETAIL_ITEM_DESC_1 field
	}

	/*-------------- Getting SAF Record Product Description ------------------*/
	if((*pchFirst) != PIPE) //Record contains Product Description field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Product Description field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stLevel2Dtls.prodDesc, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the ALT_TAX_ID field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Product Description field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF ALT_TAX_ID field
	}

	/*-------------- Getting SAF Record Alternate Tax ID ------------------*/
	if((*pchFirst) != PIPE) //Record contains Alternate Tax ID field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Alternate Tax ID field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stLevel2Dtls.alternateTaxId, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the CUSTOMER_CODE field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Alternate Tax ID field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1;//pchFirst points to start of the SAF CUSTOMER_CODE field
	}

	/*-------------- Getting SAF Record CUSTOMER_CODE ID ------------------*/
	if((*pchFirst) != PIPE) //Record contains Customer Code
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Customer Code field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stLevel2Dtls.customerCode, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the next field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Customer Code field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = pchFirst ;
	}

	if(strlen(szSSITranKey) > 0)
	{
		iRetVal = updateTaxDtlsForPymtTran(szSSITranKey, &stLevel2Dtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Level2 Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Level2 Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ================================================================================
 * Function Name: updateSAFRecCustInfoDtls
 *
 * Description	: This API parses the SAF record line for the customer info details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * =================================================================================
 */
static char * updateSAFRecCustInfoDtls(char *pszSAFRec)
{
	int						iRetVal			  = SUCCESS;
	char					*pchFirst 		  = NULL;
	char 					*pchNext 		  = NULL;
	CUSTINFODTLS_STYPE  	stCustInfoDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stCustInfoDtls, 0x00, sizeof(CUSTINFODTLS_STYPE));
	iRetVal = getCustInfoDtlsForPymtTran(szSSITranKey, &stCustInfoDtls);
	if(iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to get ZipCode details",
															__FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;
	/*-------------- Getting SAF Record cust zip code -----------------*/
	if((*pchFirst) != PIPE) //Record contains cust zip code field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Cust Zip Code field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stCustInfoDtls.szCustZipNum, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the next SAF Record field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Cust Zip Code field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF CUSTOMER_STREET field
	}

	/*-------------- Getting SAF Record cust street code -----------------*/
	if((*pchFirst) != PIPE) //Record contains cust street code field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Cust street field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stCustInfoDtls.szCustStreetName, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the next SAF Record field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Cust street field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = pchFirst;
	}

	if(strlen(szSSITranKey) > 0)
	{
		iRetVal = updateCustInfoDtlsForPymtTran(szSSITranKey, &stCustInfoDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully updated with Cust Info Details of SAF record",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the Cust Info Details ",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}

/*
 * ================================================================================
 * Function Name: updateSAFRecSignDtls
 *
 * Description	: This API parses parses the SAF record line for the Signature Details
 * 				  and updates the structure with the value
 *
 * Input Params	: Record Line
 *
 * Output Params: Pointer to the next field of the record
 * =================================================================================
 */
static char * updateSAFRecSignDtls(char *pszSAFRec)
{
	int				iRetVal			  = SUCCESS;
	char			*pchFirst 		  = NULL;
	char 			*pchNext 		  = NULL;
	SIGDTLS_STYPE   stSignatureDtls;
	int             iSignatureLen     = 0;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}

	memset(&stSignatureDtls, 0x00, sizeof(SIGDTLS_STYPE));

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	pchFirst = pszSAFRec;

	/*-------------- Getting SAF Record MIME type -----------------*/
	if((*pchFirst) != PIPE) //Record contains MIME Type field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains MIME Type field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		strncpy(stSignatureDtls.szMime, pchFirst, (pchNext - pchFirst));
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Signature Data field

		//Praveen_P1: FIXME:Assuming that if MIME type present then signature is present
		/*-------------- Getting SAF Record Signature Data -----------------*/
		debug_sprintf(szDbgMsg, "%s: Record contains Signature data field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return pchNext;
		}
		iSignatureLen = pchNext - pchFirst;
		debug_sprintf(szDbgMsg, "%s: Length of the signature is %d", __FUNCTION__, iSignatureLen);
		APP_TRACE(szDbgMsg);

		stSignatureDtls.szSign = (char *)malloc(iSignatureLen + 1); //Allocating memory for the Signature data
		if(stSignatureDtls.szSign == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error in allocating memory for the Signature", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return NULL;
		}
		memset(stSignatureDtls.szSign, 0x00, iSignatureLen + 1);
		strncpy(stSignatureDtls.szSign, pchFirst, iSignatureLen); //Copying the signature data

		if(strlen(szSSITranKey) > 0)
		{
			iRetVal = updateSigDtlsForPymtTran(szSSITranKey, &stSignatureDtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated with Signature Details of SAF record",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating the Signature Details ",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				pchNext = NULL;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Key is not present to update",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			pchNext = NULL;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not containSignature field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = pchFirst + 1; //pointer now points to signature data

	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//return pszSAFRec; //Praveen_P1: Since we should return non NULL value for successful return, doing this
	return pchNext;
}

/*
 * ============================================================================
 * Function Name: updateSAFLocalApprovalCodeDtls
 *
 * Description	: This API parses parses the SAF record line for the SAF Number
 * 				  and updates the structure with the SAF local approval code
 *
 * Input Params	: Record Line
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int updateSAFLocalApprovalCodeDtls(char *pszSAFRec)
{
	int				iRetVal			  	= SUCCESS;
	int				iSAFRecNum			= 0;
	char			*pchFirst 		  	= NULL;
	char 			*pchNext 		  	= NULL;
	char			szSAFRecordNum[10]	= "";
	FTRANDTLS_STYPE stFollowTranDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(&stFollowTranDtls, 0x00, sizeof(FTRANDTLS_STYPE));

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */
	/*-------------- Pass the SAF Status Field -----------------*/
	pchFirst = strchr(pszSAFRec, PIPE);
	if(pchFirst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Number field

	/*-------------- Getting SAF Record Number -----------------*/
	if((*pchFirst) != PIPE) //Record contains SAF Record Number field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains AUTH code field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		memset(szSAFRecordNum, 0x00, sizeof(szSAFRecordNum));
		strncpy(szSAFRecordNum, pchFirst, (pchNext - pchFirst));
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain SAF Record Number", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = FAILURE; //pchFirst points to PIPE before the Account number
	}

	if(strlen(szSAFRecordNum) > 0)//Need to update the transaction instance with these details
	{
		debug_sprintf(szDbgMsg, "%s: SAF Local Approval code is present, need to update the SSI Key with the value", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(strlen(szSSITranKey) > 0)
		{
			iRetVal = getFollowOnDtlsForPymt(szSSITranKey, &stFollowTranDtls);
			if(iRetVal != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to get Follow on Dtls", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				memset(stFollowTranDtls.szSAFApprovalCode, 0x00, sizeof(stFollowTranDtls.szSAFApprovalCode));

				iSAFRecNum = atoi(szSAFRecordNum);

				debug_sprintf(szDbgMsg, "%s: SAF Record Number is %d", __FUNCTION__, iSAFRecNum);
				APP_TRACE(szDbgMsg);

				sprintf(stFollowTranDtls.szSAFApprovalCode, "LA%04d", iSAFRecNum);

				debug_sprintf(szDbgMsg, "%s: SAF Local Approval code [%s]", __FUNCTION__, stFollowTranDtls.szSAFApprovalCode);
				APP_TRACE(szDbgMsg);

				iRetVal = updateFollowOnDtlsForPymt(szSSITranKey, &stFollowTranDtls);
				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully updated the stack with the value", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Failure while updating the stack!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iRetVal = FAILURE;
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Stack (SSI Key) is not created to update, Error !!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return iRetVal;

}

/*
 * ============================================================================
 * Function Name: parseSAFRecordLine
 *
 * Description	: This API parses the given SAF record line
 *
 * Input Params	: Record Line
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int parseSAFRecordLine(char *pszSAFData)
{
	char		*pchFirst 		  = NULL;
	char		*pchNext          = NULL;
	PAAS_BOOL	bLastFieldFound	  = PAAS_FALSE;
#ifdef DEBUG
	char szDbgMsg[4096]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFData == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	while(1)
	{
		pchFirst = pszSAFData;

		pchNext = updateSAFRecAuthCode(pchFirst);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the Auth code!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		pchFirst = pchNext + 1; //pchFirst points to start of the Account number field

		pchNext = updateSAFRecCardDtls(pchFirst);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the Card Details!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		pchFirst = pchNext + 1; //pchFirst points to start of the Force Flag Field

		pchNext = updateSAFRecForceFlag(pchFirst);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the Force Flag!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		pchFirst = pchNext + 1; //pchFirst points to start of the Trans Amount


		pchNext = updateSAFRecAmountDtls(pchFirst);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the Amount Details!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		pchFirst = pchNext + 1; //pchFirst points to start of the Tax Amount

		pchNext = updateSAFRecTaxAmount(pchFirst);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the Tax Amount!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		pchFirst = pchNext + 1; //pchFirst points to start of the Payment Type


		pchNext = updateSAFRecPymtType(pchFirst);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the PaymentType !!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		pchFirst = pchNext + 1; //pchFirst points to start of the Invoice

		pchNext = updateSAFRecSessionDtls(pchFirst);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the Session Details !!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		pchFirst = pchNext + 1; //pchFirst points to start of the MIME Type

		pchNext = updateSAFRecSignDtls(pchFirst);

		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the Signature Details !!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		pchFirst = pchNext + 1; //pchFirst points to start of the Response Code

		/*-------------- Crossing SAF Response Code -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1; //pchFirst points to start of the CTROUTD

		pchNext = updateSAFRecCTroutd(pchFirst);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the Tax Amount!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		pchFirst = pchNext + 1; //pchFirst points to start of the TROUTD

		/*-------------- Crossing SAF TROUTD field -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		pchFirst = pchNext + 1; //pchFirst points to start of the AUTH_CODE field

		/*-------------- Crossing SAF AUTH_CODE field -----------------*/
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Going to look for semicolon", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//ArjunU1: For backward compatibility looking for ';' also.
			//Note: Till PS version 2.19.9, Auth code field was last field. Hence if it's last record we should look for simicolon.
			if ( (pchNext = strchr(pchFirst, SEMI_COLON)) != NULL )
			{
				pchFirst = pchNext + 1; //pchFirst points to start of the ENCRYPTION_TYPE field

				bLastFieldFound = PAAS_TRUE;	//If semicolon found means its last record, So don't look for encryption dtls.
				debug_sprintf(szDbgMsg, "%s: Reached to last field, No more fields", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			pchFirst = pchNext + 1; //pchFirst points to start of the ENCRYPTION_TYPE field
		}

		if(bLastFieldFound != PAAS_TRUE)
		{
			//Update encryption dtls like encryption type and encryption payload
			pchNext = updateSAFRecEncryptionDtls(pchFirst);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the Encryption Details !!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return FAILURE;
			}
			pchFirst = pchNext + 1; //pchFirst points to start of the card token field

			//TODO: Does application will have card token to update? Need to Revisit and Comment the below function if not required.
			//Update Card Token dtls
			pchNext = updateSAFRecCardTokenDtls(pchFirst);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the Card Token Details !!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return FAILURE;
			}
			pchFirst = pchNext + 1; //pchFirst points to start of the CVV2 Code

			//Update Card CVV2 Details
			pchNext = updateSAFRecCVV2Dtls(pchFirst);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the CVV Details !!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return FAILURE;
			}
			pchFirst = pchNext + 1;//pchFirst points to start of the Bank User Data

			/*-------------- Crossing SAF Record PWC BANK USER DATA -----------------*/
			if((*pchFirst) != SEMI_COLON || (*pchFirst) != PIPE) //Record contains Bank user data
			{
				/*Note: Adding some more fields to SAF record after CVV2 Code. Hence looking for PIPE character.
				 * 		For backward compatibility we are also looking for semi colon.
				 */
				debug_sprintf(szDbgMsg, "%s: Record contains Bank User data field", __FUNCTION__);
				APP_TRACE(szDbgMsg);


				pchNext = strchr(pchFirst, PIPE);
				if( pchNext == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Going to look for semi colon", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					//Set the flag to indicate its last field.
					bLastFieldFound = PAAS_TRUE;

					if( (pchNext = strchr(pchFirst, SEMI_COLON)) == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Does not contain SEMICOLON(;) in the data!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						return FAILURE;
					}
				}
				else
				{
					pchNext = updateSAFRecBankUserData(pchFirst);
				}
				pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Business date field
			}
			else
			{
				pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Business date field
			}
			if( bLastFieldFound == PAAS_FALSE)
			{
				/*-------------- Crossing SAF Record Business date -----------------*/
				if((*pchFirst) != PIPE) //Record contains Business Date
				{
					debug_sprintf(szDbgMsg, "%s: Record contains Business Date field", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					pchNext = strchr(pchFirst, PIPE);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						return FAILURE;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the SAF PINless Debit field
				}
				else
				{
					pchFirst = pchFirst + 1; //pchFirst points to start of the PINless Debit field
				}

				/*-------------- Crossing SAF Record PINless debit -----------------*/
				if((*pchFirst) != SEMI_COLON || (*pchFirst) != PIPE) //Record contains PINless Debit
				{
					/*Note: Adding some more fields to SAF record after Business Date. Hence looking for PIPE character.
					 * 		For backward compatibility we are also looking for semi colon.
					 */
					debug_sprintf(szDbgMsg, "%s: Record contains PINless Debit field", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					pchNext = strchr(pchFirst, PIPE);
					if( pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Going to look for semi colon", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						//Set the flag to indicate its last field.
						bLastFieldFound = PAAS_TRUE;

						if( (pchNext = strchr(pchFirst, SEMI_COLON)) == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Does not contain SEMICOLON(;) in the data!!!", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							return FAILURE;
						}
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Txn POS Entry Mode field
				}
				else
				{
					pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Txn POS Entry Mode field
				}

				if( bLastFieldFound == PAAS_FALSE)
				{
					/*-------------- Crossing Transaction POS Entry Mode------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Merchant Id field

					/*-------------- Crossing SAF Record Merchant ID -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Terminal Id field


					/*-------------- Crossing SAF Record Terminal ID -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Lane Id field

					//Update card Lane Id for SAF transaction
					pchNext = updateSAFRecSessLaneIdDtls(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the Lane id!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						return FAILURE;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the approved amount SAF record.

					/*-------------- Crossing SAF Record APPROVED_AMOUNT -----------------*/
					pchNext = strchr(pchFirst, PIPE);
					pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Tax Indicator field

					//Update Tax Indicator
					pchNext = updateSAFRecTaxIndicator(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the Tax Indicator!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						return FAILURE;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the track indicator SAF record.

					//Update SSI command for SAF transaction
					pchNext = updateSAFRecCommand(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the command type!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						return FAILURE;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the track indicator SAF record.

					//Update card track indicator for SAF transaction
					pchNext = updateSAFRecCardTrkIndicator(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the card track indicator!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						return FAILURE;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the STORE_ID SAF record.

					//Update card Store Id for SAF transaction
					pchNext = updateSAFRecSessStoreIdDtls(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while parsing and updating the store id!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						return FAILURE;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the cust zip code record.


					/*
					 * Following fields are added from 2.19.14 version of SCA, to make sure
					 * backward compatibility works, if any of the following functions return NULL
					 * we will not consider it as FAILURE since these records will not be present when this
					 * Piece of code executing on old saf record from previous version
					 */
					//Update customer info details for SAF transaction
					pchNext = updateSAFRecCustInfoDtls(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Cust Info Records are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the DISCOUNT_AMOUNT field record.

					//Update level2 details for SAF transaction
					pchNext = updateSAFRecLevel2Dtls(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Level2 Records are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the next FSA AMOUNT_HEALTHCARE record.

					//Update FSA details for SAF transaction

					pchNext = updateSAFRecFSADtls(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: FSA Records are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the EMV Tags Field.

					debug_sprintf(szDbgMsg, "%s: Updating EMV Cadr Details in SAF", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					pchNext = updateSAFRecEMVDtls(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: EMV Details are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of Payment Sub Type.

					pchNext = updateSAFRecPymtSubTypeDtls(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Payment Sub Type Details are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of PROMO_CODE.

					pchNext = updateSAFRecAddPymtDtls(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Addtional Payment Details are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of REFERENCE field.

					pchNext = updateSAFRecReferenceDtls(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: REFERNCE Details are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of Card Holder Name.

					pchNext = updateSAFRecCardHolderDtls(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Card Holder Details are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of BILLPAY Field in SAF record.

					pchNext = updateSAFRecBillPayDtls(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Bill Pay Details are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of Trans date Field.

					pchNext = updateSAFRecTransDatenTimeDtls(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Transaction Date and Time are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of CashBack Field.

					pchNext = updateSAFRecCashBackDtlsToSAFRec(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Cash Back Details are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of Bar Code Details.

					pchNext = updateSAFRecBarCodeDtlsToSAFRec(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Bar Code Details are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of Pin Code Details.

					pchNext = updateSAFRecPinCodeDtlsToSAFRec(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Pin Code Details are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of APR_TYPE Details.

					pchNext = updateSAFAprTypeDtlsToSAFRec(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Apr Type Details are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record.

					pchNext = updateVsdEncDtlsToSAFRec(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Vsd Encryption Details are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record.

					pchNext = updatePassThrghReqsDtlsToSAFRec(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Pass through fields Details are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record.


					pchNext = updateBatchTraceIDReqsDtlsToSAFRec(pchFirst);
					if(pchNext == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Pass through fields Details are not found, may be posting saf record from old version of SCA", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						break;
					}
					pchFirst = pchNext + 1; //pchFirst points to start of the next SAF record.

					//Add code here to update future fields to the transaction instance.
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Last Record is Business Date", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Last Record is CVV2 Code", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Last Record is Auth Code", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	//Update the SAF Transaction indicator.
	if( updateSAFTranIndicatorForPymtTran(szSSITranKey, PAAS_TRUE) != SUCCESS )
	{
		debug_sprintf(szDbgMsg, "%s: ERROR! while updating the SAF tran indicator!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	//Update the SAF Local Approval code.
	if( updateSAFLocalApprovalCodeDtls(pszSAFData) != SUCCESS )
	{
		debug_sprintf(szDbgMsg, "%s: ERROR! while updating the SAF local approval code !!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	return SUCCESS;
}


/*
 * ============================================================================
 * Function Name: parseSAFDataLine
 *
 * Description	: This API parses the given the first line of the SAF record file
 *
 * Input Params	: Data and buffers to be filled
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int parseSAFDataLine(char *pszData)
{
	char	*pchFirst 	 = NULL;
	char	*pchNext  	 = NULL;
	char    szBuffer[25] = "";
	int     iSAFEligibleAbsent = 0;


#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Data to be parsed [%s]", __FUNCTION__, pszData);
	APP_TRACE(szDbgMsg);
	/*
	 <TOTAL_RECORD_COUNT>|<TOTAL_SAF_TRANS_AMOUNT>|<CURRENT_SAF_RECORD>|<LAST_OFFLINE_EPOCH_TIME>|<SAF_INTERNAL_SEQ_NUM>|<LAST_TIMESTAMP>|<SAF_TRAN_SEQ_NUM>|<TOTAL_ELIGIBLE_SAF_RECORDS>;<LF>
	 */
	pchFirst = strchr(pszData, PIPE);
	if(pchFirst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(szBuffer, 0x00, sizeof(szBuffer));
	strncpy(szBuffer, pszData, (pchFirst - pszData)); //Getting the Total SAF records from the file
	debug_sprintf(szDbgMsg, "%s: Total SAF Records [%s]", __FUNCTION__, szBuffer);
	APP_TRACE(szDbgMsg);

	//Updating the global variable
	giTotalSAFRecords  = atoi(szBuffer);

	pchFirst = pchFirst + 1; //pchFirst pointing to the first char of the Total SAF amount field

	pchNext = strchr(pchFirst, PIPE);
	if(pchNext == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(szBuffer, 0x00, sizeof(szBuffer));
	strncpy(szBuffer, pchFirst, pchNext - pchFirst); //Getting the Total SAF amount
	debug_sprintf(szDbgMsg, "%s: Total SAF Amount [%s]", __FUNCTION__, szBuffer);
	APP_TRACE(szDbgMsg);
	//Updating the global variable
	gfTotalSAFAmount  = atof(szBuffer);

	pchNext = pchNext + 1; //pchNext pointing to the fist char of the SAF Record Number

	pchFirst = strchr(pchNext, PIPE);
	if(pchFirst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(szBuffer, 0x00, sizeof(szBuffer));
	strncpy(szBuffer, pchNext, pchFirst - pchNext); //Getting the SAF Record Number
	debug_sprintf(szDbgMsg, "%s: Current SAF Record [%s]", __FUNCTION__, szBuffer);
	APP_TRACE(szDbgMsg);
	//Updating the global variable
	giCurrentSAFRecordNumber = atoi(szBuffer);

	pchFirst = pchFirst + 1; //pchFirst pointing to the first char of the Days offline

	pchNext = strchr(pchFirst, PIPE);
	if(pchNext == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPS(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(szBuffer, 0x00, sizeof(szBuffer));
	strncpy(szBuffer, pchFirst, pchNext - pchFirst); //Getting the Number of days offline

	debug_sprintf(szDbgMsg, "%s: Number of Days offline [%s]", __FUNCTION__, szBuffer);
	APP_TRACE(szDbgMsg);

	//Updating the global variable
	gtLastOfflineEpochTime   = atol(szBuffer);

	pchNext = pchNext + 1; //pchNext pointing to the fist char of the SAF Internal Sequence Number

	pchFirst = strchr(pchNext, PIPE);
	if(pchFirst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(szBuffer, 0x00, sizeof(szBuffer));
	strncpy(szBuffer, pchNext, pchFirst - pchNext); //Getting the SAF Internal Sequence Number

	debug_sprintf(szDbgMsg, "%s: SAF Internal Sequence  [%s]", __FUNCTION__, szBuffer);
	APP_TRACE(szDbgMsg);

	//Updating the global variable
	giCurrentSAFIntrnSeqNum  = atoi(szBuffer);

	pchNext = pchFirst + 1; //pchNext pointing to the fist char of the last offline time

	pchFirst = strchr(pchNext, PIPE);
	if(pchFirst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/*
		 * Praveen_P1: We are adding SAF Transaction Sequence Number to the file now, previous(till 2.19.15)
		 * will not have this so checking this one for the backward compartability
		 * In those cases this will be executed only once
		 */

		//pchFirst = pchFirst + 1; //pchFirst pointing to the first char of the time stamp

		pchFirst = strchr(pchNext, SEMI_COLON);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain SEMI_COLON(;) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		strncpy(gszLastTimeStamp, pchNext,  pchFirst - pchNext);
		debug_sprintf(szDbgMsg, "%s: SAF Last Time stamp  [%s]", __FUNCTION__, gszLastTimeStamp);
		APP_TRACE(szDbgMsg);

		if(findNumEligibleRecords() == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Found number of Eligible Records:%d ", __FUNCTION__, giTotalEligibleSAFRecords);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while finding number of Eligible records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		return SUCCESS;
	}
	memset(szBuffer, 0x00, sizeof(szBuffer));
	strncpy(szBuffer, pchNext, pchFirst - pchNext); //Getting the SAF Internal Sequence Number

	strncpy(gszLastTimeStamp, szBuffer, strlen(szBuffer));

	debug_sprintf(szDbgMsg, "%s: SAF Last Time stamp  [%s]", __FUNCTION__, gszLastTimeStamp);
	APP_TRACE(szDbgMsg);

	pchFirst = pchFirst + 1; //pchFirst pointing to the first char of the time stamp

	pchNext = strchr(pchFirst, PIPE);
	if(pchNext == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);


		pchNext = strchr(pchFirst, SEMI_COLON);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain SEMI_COLON(;) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		iSAFEligibleAbsent = 1;
	}
	memset(szBuffer, 0x00, sizeof(szBuffer));
	strncpy(szBuffer, pchFirst, pchNext - pchFirst);

	//Updating the global variable
	giCurrentSAFTranSeqNum  = atoi(szBuffer);

	debug_sprintf(szDbgMsg, "%s: SAF Transaction Sequence  [%d]", __FUNCTION__, giCurrentSAFTranSeqNum);
	APP_TRACE(szDbgMsg);

	pchNext = pchNext + 1; //pchNext pointing to the fist char of Eligible SAF records

	if(iSAFEligibleAbsent)
	{
		if(findNumEligibleRecords() == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Found number of Eligible Records:%d ", __FUNCTION__, giTotalEligibleSAFRecords);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while finding number of Eligible records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else
	{
		pchFirst = strchr(pchNext, SEMI_COLON);
		if(pchFirst == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain  SEMI_COLON(;) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		memset(szBuffer, 0x00, sizeof(szBuffer));
		strncpy(szBuffer, pchNext, pchFirst - pchNext); //Getting the Eligible SAF records

		debug_sprintf(szDbgMsg, "%s: SAF Eligible Number of Records  [%s]", __FUNCTION__, szBuffer);
		APP_TRACE(szDbgMsg);

		//Updating the global variable
		giTotalEligibleSAFRecords  = atoi(szBuffer);
	}


	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: parseSAFDataLineForTOR
 *
 * Description	: This API parses the given the TOR line of the SAF record file
 *
 * Input Params	: Data and buffers to be filled
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int parseSAFDataLineForTOR(char *pszData)
{
	char	*pchFirst 	 = NULL;
	char	*pchNext  	 = NULL;
	char    szBuffer[25] = "";


#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Data to be parsed [%s]", __FUNCTION__, pszData);
	APP_TRACE(szDbgMsg);
	/*
	 * <TOTAL_TOR_RECORD_COUNT>|<CURRENT_TOR_SAF_RECORD>|<TOTAL_ELIGIBLE_SAF_TOR_RECORDS>;<LF>
	 */
	pchFirst = strchr(pszData, PIPE);
	if(pchFirst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	memset(szBuffer, 0x00, sizeof(szBuffer));
	strncpy(szBuffer, pszData, (pchFirst - pszData)); //Getting the Total SAF records from the file
	debug_sprintf(szDbgMsg, "%s: Total TOR SAF Records [%s]", __FUNCTION__, szBuffer);
	APP_TRACE(szDbgMsg);

	//Updating the global variable
	giTotalSAFTORRecords  = atoi(szBuffer);
	pchFirst = pchFirst + 1; //pchFirst pointing to the first char of the Total SAF amount field
	pchNext = strchr(pchFirst, PIPE);
	if(pchNext == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(szBuffer, 0x00, sizeof(szBuffer));
	strncpy(szBuffer, pchFirst, pchNext - pchFirst); //Getting the SAF Record Number
	debug_sprintf(szDbgMsg, "%s: Current SAF TOR Record [%s]", __FUNCTION__, szBuffer);
	APP_TRACE(szDbgMsg);
	//Updating the global variable
	giCurrentSAFTORRecordNumber = atoi(szBuffer);

	pchNext = pchNext + 1;
	pchFirst = strchr(pchNext, SEMI_COLON);
	if(pchFirst == NULL) //Praveen_P1: Coverity fix for CID# 83356
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain SEMI_COLON(;) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(szBuffer, 0x00, sizeof(szBuffer));
	strncpy(szBuffer, pchNext, pchFirst - pchNext); //Getting the Eligible SAF records
	debug_sprintf(szDbgMsg, "%s: SAF Eligible Number of Records  [%s]", __FUNCTION__, szBuffer);
	APP_TRACE(szDbgMsg);

	//Updating the global variable
	giTotalPendingSAFTORRecords  = atoi(szBuffer);

	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: getRecordLength
 *
 * Description	: This API gives the length of the record after parsing the length field
 *                in the SAF record of the SAF record file
 *
 * Input Params	: File des
 *
 * Output Params: Length of the Record/FAILURE
 * ============================================================================
 */
static int getRecordLength(FILE *Fh)
{
//	char cLen		   = 0;	// CID-67376: 25-Jan-16: MukeshS3: Initialize cLen before using it.
	int  cLen		   = 0;	// CID-67278: 3-Feb-16: MukeshS3: fgetc() return the character read as an
							// unsigned char cast to an int or EOF on end of file or error.
	char szLength[10]  = "";
	int  iLength       = 0;
	int  iCurrentLen   = 0;
	//char szBuffer[256] = "";
#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(Fh == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Incoming parameter is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(szLength, 0x00, sizeof(szLength));

	if(feof(Fh))
	{
		debug_sprintf(szDbgMsg, "%s: End of file, no records", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return 0;
	}

	while(cLen != EOF)
	{
		cLen = fgetc(Fh);
		if(feof(Fh))
		{
			debug_sprintf(szDbgMsg, "%s: End of file, no records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return 0;
		}
		debug_sprintf(szDbgMsg, "%s: Read Character is %c", __FUNCTION__, cLen);
		APP_TRACE(szDbgMsg);
#if 0
		if(cLen == 'T')
		{
			debug_sprintf(szDbgMsg, "%s: It is the Timestamp line , need to skip the line", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(fgets(szBuffer, sizeof(szBuffer), Fh) == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the TimeStamp line!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return 0;
			}
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file, no records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return 0;
			}
			continue;
		}
#endif
		if(cLen == PIPE)
		{
			break;
		}
		iCurrentLen = strlen(szLength);
		szLength[iCurrentLen + 1] = szLength[iCurrentLen];
		szLength[iCurrentLen] = cLen;
	}

	debug_sprintf(szDbgMsg, "%s: Length of the record is %s", __FUNCTION__, szLength);
	APP_TRACE(szDbgMsg);
	iLength = atoi(szLength);

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iLength);
	APP_TRACE(szDbgMsg);

	return iLength;
}

/*
 * ============================================================================
 * Function Name: getSAFRecordNumber
 *
 * Description	: This API gives the record number of the SAF record after parsing SAF record
 *
 *
 * Input Params	: SAF Record buffer, SAF Record Number buffer to be filled
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getSAFRecordNumber(char *pszRecordData, char *pszSAFRecNum)
{
	int		iRetVal      = SUCCESS;
	char	*pchFirst    = NULL;
	char	*pchNext	 = NULL;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/*
	 * <SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<EXPIRY_DATE>|
	 * <PAYMENT_TYPE>|<PAYMENT_MEDIA>|<INVOICE>|<CARD_SOURCE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESULT_CODE>|
	 * <CTROUTD>|<TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID||||;
	 */
	pchFirst = strchr(pszRecordData, PIPE);
	if(pchFirst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	pchFirst = pchFirst + 1;

	if(*pchFirst != PIPE)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record contains the Record Number", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		strncpy(pszSAFRecNum, pchFirst, (pchNext - pchFirst)); //Getting the SAF Status from the file
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record does not contain the Record Number", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getSAFRecordStatusAndNumber
 *
 * Description	: This API gives the SAF Status and record number of the given SAF record
 *
 *
 * Input Params	: SAF Record buffer, SAF Record Number buffer to be filled
 *
 * Output Params: SAF Status/FAILURE
 * ============================================================================
 */
static int getSAFRecordStatusAndNumber(char *pszRecordData, char *pszSAFRecNum)
{
	int		iRetVal      = 0;
	char	*pchFirst    = NULL;
	char	*pchNext	 = NULL;
	char	szStatus[20] = "";

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/*
	 * <SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<EXPIRY_DATE>|
	 * <PAYMENT_TYPE>|<PAYMENT_MEDIA>|<INVOICE>|<CARD_SOURCE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESULT_CODE>|
	 * <CTROUTD>|<TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID||||;
	 */
	pchFirst = strchr(pszRecordData, PIPE);
	if(pchFirst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	strncpy(szStatus, pszRecordData, (pchFirst - pszRecordData)); //Getting the SAF Status from the file

	debug_sprintf(szDbgMsg, "%s: Status of the SAF record is %s", __FUNCTION__, szStatus);
	APP_TRACE(szDbgMsg);

	pchFirst = pchFirst + 1;

	if(*pchFirst != PIPE)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record contains the Record Number", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		strncpy(pszSAFRecNum, pchFirst, (pchNext - pchFirst)); //Getting the SAF Status from the file
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record does not contain the Record Number", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Record Number of the SAF record is %s", __FUNCTION__, pszSAFRecNum);
	APP_TRACE(szDbgMsg);

	if(strcasecmp(szStatus, "ELIGIBLE") == 0)
	{
		iRetVal = SAF_STATUS_QUEUED;
	}
	else if(strcasecmp(szStatus, "PROCESSED") == 0)
	{
		iRetVal = SAF_STATUS_PROCESSED;
	}
	else if(strcasecmp(szStatus, "IN_PROCESS") == 0)
	{
		iRetVal = SAF_STATUS_INPROCESS;
	}
	else if(strcasecmp(szStatus, "NOT_PROCESSED") == 0)
	{
		iRetVal = SAF_STATUS_NOTPROCESSED;
	}
	else if(strcasecmp(szStatus, "DECLINED") == 0)
	{
		iRetVal = SAF_STATUS_DECLINED;
	}
	else if(strcasecmp(szStatus, "PENDING") == 0)				/*The SAF status Pending, Reversed, Not allowed, Not found are added for SAF on TOR*/
	{
		iRetVal = SAF_TOR_STATUS_PENDING;
	}
	else if(strcasecmp(szStatus, "REVERSED") == 0)
	{
		iRetVal = SAF_TOR_STATUS_REVERSED;
	}
	else if(strcasecmp(szStatus, "NOT_ALLOWED") == 0)
	{
		iRetVal = SAF_TOR_STATUS_NOTALLOWED;
	}
	else if(strcasecmp(szStatus, "NOT_FOUND") == 0)
	{
		iRetVal = SAF_TOR_STATUS_NOTFOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid SAF Status!!!![%s]", __FUNCTION__, szStatus);
		APP_TRACE(szDbgMsg);
		iRetVal = FAILURE;
		//FIXME: Should not come here..if it comes what to do???
	}

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getSAFRecordStatus
 *
 * Description	: This API gives the status of the SAF record after parsing the status field
 *                in the SAF record of the SAF record file
 *
 * Input Params	: SAF Record buffer
 *
 * Output Params: SAF Status value
 * ============================================================================
 */
static int getSAFRecordStatus(char *pszRecordData)
{
	int		iRetVal      = 0;
	char	*pchFirst    = NULL;
	char	szStatus[20] = "";

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/*
	 * <SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE><ACCT_NUM>|<TRACK2>|<TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<EXPIRY_DATE>|
	 * <PAYMENT_TYPE>|<PAYMENT_MEDIA>|<INVOICE>|<CARD_SOURCE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESULT_CODE>|
	 * <CTROUTD>|<TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID||||;
	 */
	/* Daivik:26/8/2016 Fix for Coverity : 83361 */
	if(pszRecordData == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Null Record Data Return Failure!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	pchFirst = strchr(pszRecordData, PIPE);
	if(pchFirst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	strncpy(szStatus, pszRecordData, (pchFirst - pszRecordData)); //Getting the SAF Status from the file

	debug_sprintf(szDbgMsg, "%s: Status of the SAF record is %s", __FUNCTION__, szStatus);
	APP_TRACE(szDbgMsg);

	if(strcasecmp(szStatus, "ELIGIBLE") == 0)
	{
		iRetVal = SAF_STATUS_QUEUED;
	}
	else if(strcasecmp(szStatus, "PROCESSED") == 0)
	{
		iRetVal = SAF_STATUS_PROCESSED;
	}
	else if(strcasecmp(szStatus, "IN_PROCESS") == 0)
	{
		iRetVal = SAF_STATUS_INPROCESS;
	}
	else if(strcasecmp(szStatus, "NOT_PROCESSED") == 0)
	{
		iRetVal = SAF_STATUS_NOTPROCESSED;
	}
	else if(strcasecmp(szStatus, "DECLINED") == 0)
	{
		iRetVal = SAF_STATUS_DECLINED;
	}
	else if(strcasecmp(szStatus, "PENDING") == 0)				/*The SAF status Pending, Reversed, Not allowed, Not found are added for SAF on TOR*/
	{
		iRetVal = SAF_TOR_STATUS_PENDING;
	}
	else if(strcasecmp(szStatus, "REVERSED") == 0)
	{
		iRetVal = SAF_TOR_STATUS_REVERSED;
	}
	else if(strcasecmp(szStatus, "NOT_ALLOWED") == 0)
	{
		iRetVal = SAF_TOR_STATUS_NOTALLOWED;
	}
	else if(strcasecmp(szStatus, "NOT_FOUND") == 0)
	{
		iRetVal = SAF_TOR_STATUS_NOTFOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid SAF Status!!!![%s]", __FUNCTION__, szStatus);
		APP_TRACE(szDbgMsg);
		//FIXME: Should not come here..if it comes what to do???
	}

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: updateSAFSSIRespDetails
 *
 * Description	: This API updates the SSI response details
 *
 *
 * Input Params	: SAF SSI Tran key
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int updateSAFSSIRespDetails(char *pszTranKey, int iResultCode)
{
	int					rv		     	= SUCCESS;
	CTRANDTLS_STYPE		stCTranDtls;
	CARDDTLS_STYPE  	stCardDtls;
	AMTDTLS_STYPE		stAmountDtls;
	RESPDTLS_STYPE		stResponseDtls;
#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&stCTranDtls, 0x00, sizeof(CTRANDTLS_STYPE));
	memset(&stCardDtls, 0x00, sizeof(CARDDTLS_STYPE));
	memset(&stAmountDtls, 0x00, sizeof(AMTDTLS_STYPE));

	//Don't memset the this host response details structure any where other then this place.
	memset(&stHostRespDtls, 0x00, sizeof(stHostRespDtls));

	while(1)
	{
		rv = getCardDtlsForPymtTran(pszTranKey, &stCardDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR!, Failed to fetched Card Details from the transaction instance",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = getCurTranDtlsForPymt(pszTranKey, &stCTranDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR!, Failed to fetched current tran details from the transaction instance",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = getAmtDtlsForPymtTran(pszTranKey, &stAmountDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR!, Failed to get amount details from the transaction instance", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* get the general host response details for the transaction instance */
		memset(&stResponseDtls, 0x00, sizeof(RESPDTLS_STYPE));
		rv = getGenRespDtlsForSSITran(pszTranKey, &stResponseDtls);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR!, Failed to get General Response Details from the transaction instance", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//Copy the required data
		sprintf(stHostRespDtls.szPWCSAFResponseCode, "%d", iResultCode);
		strcpy(stHostRespDtls.szPWCSAFCVVCode, stCardDtls.szCVVCode);
		strcpy(stHostRespDtls.szPWCSAFAuthCode, stCTranDtls.szAuthCode);
		strcpy(stHostRespDtls.szPWCSAFTroutd, stCTranDtls.szTroutd);
		strcpy(stHostRespDtls.szPWCSAFCTroutd, stCTranDtls.szCTroutd);
		strcpy(stHostRespDtls.szPWCSAFCardToken, stCTranDtls.szCardToken);
		strcpy(stHostRespDtls.szPWCSAFBankUserData, stCTranDtls.szBankUserData);
		strcpy(stHostRespDtls.szTxnPOSEntryMode, stCTranDtls.szTxnPosEntryMode);
		strcpy(stHostRespDtls.szMerchId, stCTranDtls.szMerchId);
		strcpy(stHostRespDtls.szTermId, stCTranDtls.szTermId);
		strcpy(stHostRespDtls.szLaneId, stCTranDtls.szLaneId);
		strcpy(stHostRespDtls.szStoreId, stCTranDtls.szStoreId);
		strcpy(stHostRespDtls.szApprvdAmt, stAmountDtls.apprvdAmt);
		strcpy(stHostRespDtls.szLPToken, stCTranDtls.szLPToken);
		strcpy(stHostRespDtls.szPPCV, stCTranDtls.szPPCV);
		strcpy(stHostRespDtls.szPaymentMedia, stCardDtls.szPymtMedia);
		strcpy(stHostRespDtls.szAVSCode, stCTranDtls.szAvsCode);
		strcpy(stHostRespDtls.szHostRespCode, stResponseDtls.szHostRsltCode);
		strcpy(stHostRespDtls.szSignatureRef, stCTranDtls.szSignatureRef);
		strcpy(stHostRespDtls.szAprType, stCTranDtls.szAprType);
		strcpy(stHostRespDtls.szActionCode, stCTranDtls.szActionCode);
		strcpy(stHostRespDtls.szSvcPhone, stCTranDtls.szSvcPhone);
		strcpy(stHostRespDtls.szStatusFlag, stCTranDtls.szStatusFlag);
		strcpy(stHostRespDtls.szPurchaseApr, stCTranDtls.szPurchaseApr);
		strcpy(stHostRespDtls.szCreditPlanNbr, stCTranDtls.szCreditPlanNbr);
		strcpy(stHostRespDtls.szReceiptText, stCTranDtls.szReceiptText);
		strcpy(stHostRespDtls.szAthNtwID, stCTranDtls.szAthNtwID);
		strcpy(stHostRespDtls.szTransDate, stCTranDtls.szTransDate);
		strcpy(stHostRespDtls.szTransTime, stCTranDtls.szTransTime);

		break;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: filterSAFRecsByRecRange
 *
 * Description	: This API filters the SAF records based on the specified range of SAF record number
 *                and fills the SAF transaction details list
 *
 * Input Params	: SAF Record Details st
 *
 * Output Params: SAF_RECORD_FOUND/SAF_RECORD_NOT_FOUND
 * ============================================================================
 */
static int filterSAFRecsByRecRange(SAFDTLS_PTYPE pstSAFDtls)
{
	int		iRetVal      	     = SUCCESS;
	FILE	*Fh			 	     = NULL;
	int		iIndex		 	     = 0;
	int		iRecordLen	 	     = 0;
	int		iPreviousRecLen      = 0;
	char	*pszSAFRecordData    = NULL;
	char	*pchTemp		     = NULL;
	int		iSAFRecFound         = 0;
	char	szSAFRecNum[20]      = "";
	int     iSAFStartRecordNum   = 0;
	int     iSAFEndRecordNum     = 0;
	int     iSAFCurrentRecordNum = 0;
	int		iFileNumber          = 0;
	struct	stat st;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Incoming parameter is NULL ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	//Check if there are any records presently
	if(giTotalSAFRecords == 0)
	{
		debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	iSAFStartRecordNum = atoi(pstSAFDtls->szBegNo);
	iSAFEndRecordNum   = atoi(pstSAFDtls->szEndNo);

	debug_sprintf(szDbgMsg, "%s: Need to look for SAF Record within [%s,%s] range", __FUNCTION__, pstSAFDtls->szBegNo, pstSAFDtls->szEndNo);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	for(iFileNumber = 0; iFileNumber < 2; iFileNumber++)
	{
		if(iFileNumber == 0)
		{
			if(stat(SAF_ELIGIBLE_RECORDS_FILE_NAME, &st) == 0)
			{
				if(st.st_size > 1)
				{
					Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
					if(Fh==NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
						APP_TRACE(szDbgMsg);
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}
		else if(iFileNumber == 1)
		{
			if(stat(SAF_COMPLETED_RECORDS_FILE_NAME, &st) == 0)
			{
				if(st.st_size > 1)
				{
					Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
					if(Fh==NULL)
					{
						// CID 67398 (#1 of 1): Copy-paste error (COPY_PASTE_ERROR) changed the File name in debug print. T_RaghavendranR1
						debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
						APP_TRACE(szDbgMsg);
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}

		for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
		{
			debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			iRecordLen = getRecordLength(Fh);

			debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
			APP_TRACE(szDbgMsg);

			if(iRecordLen == 0)
			{
				debug_sprintf(szDbgMsg, "%s: No more records present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			if(iPreviousRecLen < iRecordLen + 1)
			{
				//Allocate the memory to hold the SAF record
				pchTemp = (char *)realloc(pszSAFRecordData, iRecordLen + 10);
				if(pchTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					fclose(Fh); //Closing the file before returning
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					if(pszSAFRecordData != NULL)
					{
						free(pszSAFRecordData);
					}
					return FAILURE;
				}
				else
				{
					memset(pchTemp, 0x00, iRecordLen + 10);
					pszSAFRecordData = pchTemp;
				}
				iPreviousRecLen = iRecordLen + 10;
			}
			else
			{
				if(pszSAFRecordData != NULL) // CID 67400 (#1 of 1): Explicit null dereferenced (FORWARD_NULL) T_RaghavendranR1
				{
					memset(pszSAFRecordData, 0x00, iPreviousRecLen);
				}
			}

			// CID 67275 (#1 of 1): Out-of-bounds access (OVERRUN). T_RaghavendranR1. Allocating enough memory, so that same can be accessed in fgets() and we do not overrun it also.
			if((pszSAFRecordData != NULL) && (fgets(pszSAFRecordData, iRecordLen+10, Fh) == NULL))// CID 67400 (#1 of 1): Explicit null dereferenced (FORWARD_NULL) T_RaghavendranR1
			{
				if(feof(Fh))
				{
					debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					fclose(Fh); //Closing the file before returning
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					if(pszSAFRecordData != NULL)
					{
						free(pszSAFRecordData);
					}
					return FAILURE;
				}
			}

			//CID 67400 (#1 of 1): Dereference after null check (FORWARD_NULL). T_RaghavendranR1. DeReference it after NULL check.
			if(pszSAFRecordData != NULL)
			{
				if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
				{
					debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iIndex = iIndex -1; //Time Stamp lines are not part of the Total number of records, need to read more
					continue;
				}

				iRetVal = getSAFRecordStatus(pszSAFRecordData);
				if(iRetVal == SAF_TOR_STATUS_NOTALLOWED || iRetVal == SAF_TOR_STATUS_NOTFOUND ||
						iRetVal == SAF_TOR_STATUS_PENDING || iRetVal == SAF_TOR_STATUS_REVERSED)
				{
					debug_sprintf(szDbgMsg, "%s: It is a TOR records..Skipping it...", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}
				debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
				APP_TRACE(szDbgMsg);

				memset(szSAFRecNum, 0x00, sizeof(szSAFRecNum));
				iRetVal = getSAFRecordNumber(pszSAFRecordData, szSAFRecNum);

				iSAFCurrentRecordNum = atoi(szSAFRecNum);


				if(iSAFCurrentRecordNum >= iSAFStartRecordNum && iSAFCurrentRecordNum <= iSAFEndRecordNum)//If the SAF record number of current record is not same, need to fetch the next record
				{
					debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is in the given range of SAF Records", __FUNCTION__, szSAFRecNum);
					APP_TRACE(szDbgMsg);
					iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
					if(iRetVal == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						iSAFRecFound = 1;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//FIXME: What to do here???
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is not in the given range of SAF Records", __FUNCTION__, szSAFRecNum);
					APP_TRACE(szDbgMsg);
					/*if(strcmp(szSAFRecNum, pstSAFDtls->szEndNo) > 0)//We reached the record which is greater than the END record // This is wrong comparision. Should not string compare here.*/
					if(iSAFCurrentRecordNum > iSAFEndRecordNum)//We reached the record which is greater than the END record
					{
						break;
					}
					else
					{
						continue; //Need to go to the next record if it exists
					}
				}
			}
		}
		if(Fh != NULL)
		{
			fclose(Fh); //Closing the file
		}
	}

	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_FOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_NOT_FOUND;
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: filterSAFTORRecsByRecRange
 *
 * Description	: This API filters the SAF TOR records based on the specified range of SAF record number
 *                and fills the SAF transaction details list
 *
 * Input Params	: SAF Record Details st
 *
 * Output Params: SAF_RECORD_FOUND/SAF_RECORD_NOT_FOUND
 * ============================================================================
 */
static int filterSAFTORRecsByRecRange(SAFDTLS_PTYPE pstSAFDtls)
{
	int		iRetVal      	     = SUCCESS;
	FILE	*Fh			 	     = NULL;
	int		iIndex		 	     = 0;
	int		iRecordLen	 	     = 0;
	int		iPreviousRecLen      = 0;
	char	*pszSAFRecordData    = NULL;
	char	*pchTemp		     = NULL;
	int		iSAFRecFound         = 0;
	char	szSAFRecNum[20]      = "";
	int     iSAFTORStartRecordNum   = 0;
	int     iSAFTOREndRecordNum     = 0;
	int     iSAFCurrentRecordNum = 0;
	int		iFileNumber          = 0;
	struct	stat st;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Incoming parameter is NULL ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	if(giTotalSAFTORRecords == 0)
	{
		debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	iSAFTORStartRecordNum = atoi(pstSAFDtls->szTORBegNo);
	iSAFTOREndRecordNum   = atoi(pstSAFDtls->szTOREndNo);

	debug_sprintf(szDbgMsg, "%s: Need to look for SAF Record within [%s,%s] range", __FUNCTION__, pstSAFDtls->szTORBegNo, pstSAFDtls->szTOREndNo);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	for(iFileNumber = 0; iFileNumber < 2; iFileNumber++)
	{
		if(iFileNumber == 0)
		{
			if(stat(SAF_ELIGIBLE_RECORDS_FILE_NAME, &st) == 0)
			{
				if(st.st_size > 1)
				{
					Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
					if(Fh==NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
						APP_TRACE(szDbgMsg);
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}
		else if(iFileNumber == 1)
		{
			if(stat(SAF_COMPLETED_RECORDS_FILE_NAME, &st) == 0)
			{
				if(st.st_size > 1)
				{
					Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
					if(Fh==NULL)
					{
						// CID 67398 (#1 of 1): Copy-paste error (COPY_PASTE_ERROR) changed the File name in debug print. T_RaghavendranR1
						debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
						APP_TRACE(szDbgMsg);
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}

		for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
		{
			debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			iRecordLen = getRecordLength(Fh);

			debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
			APP_TRACE(szDbgMsg);

			if(iRecordLen == 0)
			{
				debug_sprintf(szDbgMsg, "%s: No more records present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			if(iPreviousRecLen < iRecordLen + 1)
			{
				//Allocate the memory to hold the SAF record
				pchTemp = (char *)realloc(pszSAFRecordData, iRecordLen + 10);
				if(pchTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					fclose(Fh); //Closing the file before returning
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					if(pszSAFRecordData != NULL)
					{
						free(pszSAFRecordData);
					}
					return FAILURE;
				}
				else
				{
					memset(pchTemp, 0x00, iRecordLen + 10);
					pszSAFRecordData = pchTemp;
				}
				iPreviousRecLen = iRecordLen + 10;
			}
			else
			{
				if(pszSAFRecordData != NULL) // CID 67400 (#1 of 1): Explicit null dereferenced (FORWARD_NULL) T_RaghavendranR1
				{
					memset(pszSAFRecordData, 0x00, iPreviousRecLen);
				}
			}

			// CID 67275 (#1 of 1): Out-of-bounds access (OVERRUN). T_RaghavendranR1. Allocating enough memory, so that same can be accessed in fgets() and we do not overrun it also.
			if((pszSAFRecordData != NULL) && (fgets(pszSAFRecordData, iRecordLen+10, Fh) == NULL))// CID 67400 (#1 of 1): Explicit null dereferenced (FORWARD_NULL) T_RaghavendranR1
			{
				if(feof(Fh))
				{
					debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					fclose(Fh); //Closing the file before returning
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					if(pszSAFRecordData != NULL)
					{
						free(pszSAFRecordData);
					}
					return FAILURE;
				}
			}

			//CID 67400 (#1 of 1): Dereference after null check (FORWARD_NULL). T_RaghavendranR1. DeReference it after NULL check.
			if(pszSAFRecordData != NULL)
			{
				if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
				{
					debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iIndex = iIndex -1; //Time Stamp lines are not part of the Total number of records, need to read more
					continue;
				}

				iRetVal = getSAFRecordStatus(pszSAFRecordData);
				if(iRetVal == SAF_STATUS_DECLINED || iRetVal == SAF_STATUS_INPROCESS ||
						iRetVal == SAF_STATUS_NOTPROCESSED || iRetVal == SAF_STATUS_PROCESSED ||
						iRetVal == SAF_STATUS_QUEUED)
				{
					debug_sprintf(szDbgMsg, "%s: It is a Payment SAF records..Skipping it...", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}

				memset(szSAFRecNum, 0x00, sizeof(szSAFRecNum));
				iRetVal = getSAFRecordNumber(pszSAFRecordData, szSAFRecNum);

				iSAFCurrentRecordNum = atoi(szSAFRecNum);

				debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
				APP_TRACE(szDbgMsg);

				if((iSAFCurrentRecordNum >= iSAFTORStartRecordNum && iSAFCurrentRecordNum <= iSAFTOREndRecordNum))//If the SAF record number of current record is not same, need to fetch the next record
				{
					debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is in the given range of SAF Records", __FUNCTION__, szSAFRecNum);
					APP_TRACE(szDbgMsg);
					iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
					if(iRetVal == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						iSAFRecFound = 1;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//FIXME: What to do here???
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is not in the given range of SAF Records or Current SAF record is payment record", __FUNCTION__, szSAFRecNum);
					APP_TRACE(szDbgMsg);
					/*if(strcmp(szSAFRecNum, pstSAFDtls->szEndNo) > 0)//We reached the record which is greater than the END record // This is wrong comparision. Should not string compare here.*/
					if(iSAFCurrentRecordNum > iSAFTOREndRecordNum)//We reached the record which is greater than the END record
					{
						break;
					}
					else
					{
						continue; //Need to go to the next record if it exists
					}
				}
			}
		}
		if(Fh != NULL)
		{
			fclose(Fh); //Closing the file
		}
	}

	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_FOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_NOT_FOUND;
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;

}
/*
 * ============================================================================
 * Function Name: getAllSAFRecs
 *
 * Description	: This API gets all the SAF records from the file and
 *                fills the SAF transaction details list
 *
 * Input Params	: NONE
 *
 * Output Params: SAF_RECORD_FOUND/SAF_RECORD_NOT_FOUND
 * ============================================================================
 */
static int getAllSAFRecs(SAFDTLS_PTYPE pstSAFDtls)
{
	int		iRetVal      	 = SUCCESS;
	FILE	*Fh			 	 = NULL;
	int		iIndex		 	 = 0;
	int		iRecordLen	 	 = 0;
	int		iPreviousRecLen  = 0;
	char	*pszSAFRecordData = NULL;
	char	*pchTemp		 = NULL;
	//char    szSAFTORCmd[20]	 = "";
	int		iSAFRecFound     = 0;
	int		iFileNumber		 = 0;
	struct	stat st;
#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	//Check if there are any records presently
	if(giTotalSAFRecords == 0)
	{
		debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	for(iFileNumber = 0; iFileNumber < 2; iFileNumber++)
	{
		if(iFileNumber == 0)
		{
			if(stat(SAF_ELIGIBLE_RECORDS_FILE_NAME, &st) == 0)
			{
				if(st.st_size > 1)
				{
					Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
					if(Fh==NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
						APP_TRACE(szDbgMsg);
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}
		else if(iFileNumber == 1)
		{
			if(stat(SAF_COMPLETED_RECORDS_FILE_NAME, &st) == 0)
			{
				if(st.st_size > 1)
				{
					Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
					if(Fh==NULL)
					{
						//CID 67217 (#1 of 1): Copy-paste error (COPY_PASTE_ERROR). changed the File name in debug print. T_RaghavendranR1
						debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
						APP_TRACE(szDbgMsg);
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}

		for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords ; iIndex++)
		{
			debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			iRecordLen = getRecordLength(Fh);

			if(iRecordLen <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: No more records present in the file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			if(iPreviousRecLen < iRecordLen + 1)
			{
				//Allocate the memory to hold the SAF record
				pchTemp = (char *)realloc(pszSAFRecordData, iRecordLen + 10);
				if(pchTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					fclose(Fh); //Closing the file before returning
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
				else
				{
					memset(pchTemp, 0x00, iRecordLen + 10);
					pszSAFRecordData = pchTemp;
				}
				iPreviousRecLen = iRecordLen + 10;
			}
			else
			{
				if(pszSAFRecordData != NULL)
				{
					memset(pszSAFRecordData, 0x00, iRecordLen + 1); // CID 67234 (#1 of 1): Explicit null dereferenced (FORWARD_NULL). T_RaghavendranR1 NULl check added
				}
			}

			// CID 67490 (#1 of 1): Out-of-bounds access (OVERRUN), T_RaghavendranR1, Allocating enough memory, so that same can be accessed in fgets() and we do not overrun it also.
			if((pszSAFRecordData != NULL) && (fgets(pszSAFRecordData, iRecordLen+10, Fh) == NULL))// CID 67234 (#1 of 1): Explicit null dereferenced (FORWARD_NULL). T_RaghavendranR1 NULl check added
			{
				if(feof(Fh))
				{
					debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					fclose(Fh); //Closing the file before returning
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					if(pszSAFRecordData != NULL)
					{
						free(pszSAFRecordData);
					}
					return FAILURE;
				}
			}

			if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iIndex = iIndex -1; //Time Stmap lines are not part of the Total number of records, need to read more
				continue;
			}

			iRetVal = getSAFRecordStatus(pszSAFRecordData);
			if(iRetVal == SAF_TOR_STATUS_NOTALLOWED || iRetVal == SAF_TOR_STATUS_NOTFOUND ||
					iRetVal == SAF_TOR_STATUS_PENDING || iRetVal == SAF_TOR_STATUS_REVERSED)
			{
				debug_sprintf(szDbgMsg, "%s: It is a TOR records..Skipping it...", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				continue;
			}

			debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iSAFRecFound = 1;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//FIXME: What to do here???
			}
		}

		if(Fh != NULL)
		{
			fclose(Fh);
		}
	}

	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_FOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_NOT_FOUND;
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: clearSAFRecsByRecRange
 *
 * Description	: This API clears the SAF records based on the range of SAF record number and
 *                fills the SAF transaction details list
 *
 * Input Params	: SAF Record Number range (Begin and End SAF record number)
 *
 * Output Params: SAF_RECORD_FOUND/SAF_RECORD_NOT_FOUND
 * ============================================================================
 */
static int clearSAFRecsByRecRange(SAFDTLS_PTYPE pstSAFDtls, char *pszFileName)
{
	int			iRetVal      	     	= SUCCESS;
	int			iIndex		 	     	= 0;
	int			iRecordLen	 	     	= 0;
	int			iPreviousRecLen      	= 0;
	int			iSAFStartRecordNum   	= 0;
	int			iSAFEndRecordNum     	= 0;
	int			iSAFCurrentRecordNum 	= 0;
	int			iClearedInProcessRecord	= 0;
	int			iSAFRecFound         	= 0;
	int			iTotalRecLen         	= 0;
	char		*pszSAFRecordData    	= NULL;
	char		*pchTemp		     	= NULL;
	char		szSAFRecNum[20]      	= "";
	char		szRecordLength[10]   	= "";
	char		*pchEndPos           	= NULL;
	//float		fCurrentAmount       	= 0.0;
	char		szTempBuf[100]	     	= "";
	char    	*pszTempSAFRecord    	= NULL;
	char		szTimeStampLine[50]		= "";
	FILE		*FhPerm			     	= NULL;
	FILE    	*FhTemp              	= NULL;
	PAAS_BOOL	bIsTORTran				= PAAS_FALSE;
	PAAS_BOOL	bAllowInProcessRecToRemove	= PAAS_TRUE;
	PAAS_BOOL	bRemoveRec				= PAAS_FALSE;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Incoming parameter is NULL or does not contain any value", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	iSAFStartRecordNum = atoi(pstSAFDtls->szBegNo);
	iSAFEndRecordNum   = atoi(pstSAFDtls->szEndNo);

	debug_sprintf(szDbgMsg, "%s: Need to look for SAF Record within [%d,%d] range and clear them in %s file", __FUNCTION__, iSAFStartRecordNum, iSAFEndRecordNum, pszFileName);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	FhPerm = fopen(pszFileName, "r+"); //Open a file for update (both for input and output). The file must exist.
	if(FhPerm==NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
		APP_TRACE(szDbgMsg);
		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
		return ERR_FILE_NOT_FOUND;
	}

	FhTemp = fopen(SAF_RECORD_FILE_NAME_TEMP, "a+"); //Open file for output at the end of a file. Output operations always write data at the end of the file, expanding it. Repositioning operations (fseek, fsetpos, rewind) are ignored. The file is created if it does not exist
	if(FhTemp==NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening the SAF Temporary record file!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		fclose(FhPerm);
		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
		return ERR_FILE_NOT_FOUND;
	}

	for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
	{
		debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iRecordLen = getRecordLength(FhPerm);
		if(iRecordLen == FAILURE)
		{
			debug_sprintf(szDbgMsg, "%s: Error while getting the length of the SAF record !!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			fclose(FhPerm);
			fclose(FhTemp);//Closing the file before returning
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return FAILURE;
		}
		debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
		APP_TRACE(szDbgMsg);

		if(iRecordLen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No more records present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		memset(szRecordLength, 0x00, sizeof(szRecordLength));
		sprintf(szRecordLength, "%d", iRecordLen);

		iTotalRecLen = iRecordLen + strlen(szRecordLength)/*For the length*/ + 1 /*For PIPE after length*/ + 2 /*Buffer*/;

		if(iPreviousRecLen < iTotalRecLen)
		{
			//Allocate the memory to hold the SAF record
			pchTemp = (char *)realloc(pszSAFRecordData, iTotalRecLen);
			if(pchTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm);
				fclose(FhTemp); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			else
			{
				memset(pchTemp, 0x00, iTotalRecLen);
				pszSAFRecordData = pchTemp;
			}
			iPreviousRecLen = iTotalRecLen;
		}
		else
		{
			if(pszSAFRecordData != NULL)
			{
				memset(pszSAFRecordData, 0x00, iPreviousRecLen);
			}
		}

		if((pszSAFRecordData != NULL) && (fgets(pszSAFRecordData, iTotalRecLen, FhPerm) == NULL))
		{
			if(feof(FhPerm))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm);
				fclose(FhTemp);//Closing the file before returning
				free(pszSAFRecordData);	// CID-67328: 27-Jan-16: MukeshS3: Freeing memory before abnormal return
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
		}

		if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iIndex = iIndex -1; //Time Stamp lines are not part of the Total number of records, need to read more

			debug_sprintf(szDbgMsg, "%s: Need to write the time stamp line to Temp file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szTimeStampLine, 0x00, sizeof(szTimeStampLine));
			sprintf(szTimeStampLine, "%d%c%s", strlen(pszSAFRecordData), PIPE, pszSAFRecordData);

			iRecordLen = strlen(szTimeStampLine);

			iRetVal = fwrite(szTimeStampLine, sizeof(char), iRecordLen, FhTemp);

			if(iRetVal == iRecordLen)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully written time stamp line to the temporary file ", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while writing time stamp line, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLen, iRetVal);
				APP_TRACE(szDbgMsg);
				//Praveen_P1: What to do here???
			}

			continue;
		}

		debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		memset(szSAFRecNum, 0x00, sizeof(szSAFRecNum));
		iRetVal = getSAFRecordStatusAndNumber(pszSAFRecordData, szSAFRecNum);

		//iRetVal = getSAFRecordNumber(pszSAFRecordData, szSAFRecNum);

		if(iRetVal > 0 && ((iRetVal == SAF_TOR_STATUS_PENDING) || (iRetVal == SAF_TOR_STATUS_NOTALLOWED) || (iRetVal == SAF_TOR_STATUS_NOTFOUND)
				||	(iRetVal == SAF_TOR_STATUS_REVERSED)))
		{
			bIsTORTran = PAAS_TRUE;
		}
		else
		{
			bIsTORTran = PAAS_FALSE;
		}

		debug_sprintf(szDbgMsg, "%s: The TOR Tran status Value [%d]", __FUNCTION__, bIsTORTran);
		APP_TRACE(szDbgMsg);

		iSAFCurrentRecordNum = atoi(szSAFRecNum);

		if((iSAFCurrentRecordNum >= iSAFStartRecordNum && iSAFCurrentRecordNum <= iSAFEndRecordNum) && (! bIsTORTran))//If the fetched SAF record number of current record falls into the range, need to fill the details
		{
			debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is in the given range of SAF Records", __FUNCTION__, szSAFRecNum);
			APP_TRACE(szDbgMsg);

			if((iRetVal == SAF_STATUS_INPROCESS))
			{
				acquireMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");
				if(iSAFInProgressStatus == SAF_IN_FLIGHT)
				{
					debug_sprintf(szDbgMsg, "%s: SAF record with status IN_PROCESS is currently IN_FLIGHT, Should Not Delete the Record. Copying the record to temp file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					bAllowInProcessRecToRemove = PAAS_FALSE;
					bRemoveRec = PAAS_FALSE;
				}
				else
				{
					/*-----------Delete the SAF SSI Tran Key ---------- */
					if(popSSITran(szSSITranKey) != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s:Error while popping the SAF SSI transaction from stack", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					deleteStack(szSSITranKey);
					memset(szSSITranKey, 0x00, sizeof(szSSITranKey));
					bRemoveRec = PAAS_TRUE;
				}
				releaseMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");
			}
			else
			{
				bRemoveRec = PAAS_TRUE;
			}
		}
		else
		{
			bRemoveRec = PAAS_FALSE;
		}

		if(bRemoveRec)
		{
			iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iSAFRecFound = 1;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//FIXME: What to do here???
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is not in the given range of SAF Records", __FUNCTION__, szSAFRecNum);
			APP_TRACE(szDbgMsg);

			//Need to copy the record to the temp file
			debug_sprintf(szDbgMsg, "%s:  Writing %d SAF record Number to temp file", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			pchEndPos = strchr(pszSAFRecordData, SEMI_COLON);

			if(pchEndPos == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain SEMI COLON in the SAF record", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm);
				fclose(FhTemp); //Closing the file before returning
				free(pszSAFRecordData); // CID 67328 (#1 of 2): Resource leak (RESOURCE_LEAK). T_RaghavendranR1. Free the previously allocated Data if the operation fails.
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}

			*pchEndPos = 0x00; //Ending the SAF record at the SEMI COLON
			pchEndPos++; //Pointing to LF
			*pchEndPos = 0x00; //Nullyfying the LF also

			pszTempSAFRecord = (char *)malloc(sizeof(char) * (iTotalRecLen + 2) /* Buffer */);
			if(pszTempSAFRecord == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while allocating memory for the Temp SAF record", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm);
				fclose(FhTemp); //CID-67483,67413: 22-Jan-16: MuekeshS3: Closing the file before returning
				free(pszSAFRecordData); // CID 67328 (#2 of 2): Resource leak (RESOURCE_LEAK). T_RaghavendranR1. Free the previously allocated Data if the operation fails.
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			memset(pszTempSAFRecord, 0x00,  iTotalRecLen + 2);
			sprintf(pszTempSAFRecord, "%d%c%s%c%c", strlen(pszSAFRecordData) + 1 /* For the ;*/ + 1 /*For the <LF>*/, PIPE, pszSAFRecordData, SEMI_COLON, LF /*New Line Field*/);
			memset(pszSAFRecordData, 0x00, iTotalRecLen);
			memcpy(pszSAFRecordData, pszTempSAFRecord, strlen(pszTempSAFRecord));
			free(pszTempSAFRecord); //Freeing temp SAF record

			//Update the temporary file
			iRecordLen = strlen(pszSAFRecordData);

			iRetVal = fwrite(pszSAFRecordData, sizeof(char), iRecordLen, FhTemp);

			if(iRetVal == iRecordLen)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully written SAF record of index %d to the temporary file ", __FUNCTION__, iIndex);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while writing SAF record to the file, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLen, iRetVal);
				APP_TRACE(szDbgMsg);
				//Praveen_P1: What to do here???
			}
		}
	}

	fclose(FhPerm);
	fclose(FhTemp); //Closing the file before doing some operations like moving

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//Copying the temporary SAF record to original file
		memset(szTempBuf, 0x00, sizeof(szTempBuf));
		sprintf(szTempBuf, "mv -f %s %s", SAF_RECORD_FILE_NAME_TEMP, pszFileName); //copying the file
		debug_sprintf(szDbgMsg,"%s - The command to be executed is [%s]", __FUNCTION__,szTempBuf);
		APP_TRACE(szDbgMsg);
		iRetVal = local_svcSystem(szTempBuf);

		if(iRetVal == 0)
		{
			debug_sprintf(szDbgMsg,"%s - Successfully executed the command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = SAF_RECORD_FOUND;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s - Error in execution of the command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
		}

		if(chmod(pszFileName, 0666) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

		if(iClearedInProcessRecord == 1)
		{
			/* The part of the code will not be executed now, as we are not deleting the IN_PROCESS SFA Record */
			debug_sprintf(szDbgMsg,"%s - Cleared the IN_PROCESS SAF record, need to remove the same from the SAF record structure", __FUNCTION__);
			APP_TRACE(szDbgMsg);
#if 0
			/*------------- Clear the contents of the SAF Record Structure------------------- */
			if(stSAFRecord.stSignatureDetails.sigData != NULL)
			{
				free(stSAFRecord.stSignatureDetails.sigData);
			}
			memset(&stSAFRecord.stTransactionDetails, 0x00, sizeof(SAF_TRANDATA_S_TYPE));
			memset(&stSAFRecord.stCardDetails, 0x00, sizeof(SAF_CARDDATA_S_TYPE));
			memset(&stSAFRecord.stSignatureDetails, 0x00, sizeof(SIGN_DATA_STYPE));
			memset(&stSAFRecord, 0x00, sizeof(SAF_RECORD_ST_TYPE)); //Mem setting the structure
#endif
			//Since we have cleared the IN_PROCESS SAF record, need to update the system with the next ELIGIBLE record
			/*-----------Update the SAF data system with the next ELIGIBLE SAF record ---------- */
			iRetVal = readQueuedSAFRecord();
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated the SAF data system with the next SAF record with ELIGIBLE status",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = SAF_RECORD_FOUND;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while updating the SAF data system with the next SAF record with ELIGIBLE status",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		if(bAllowInProcessRecToRemove == PAAS_FALSE)
		{
			iRetVal = SAF_REC_PARTIALLY_FOUND;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//MukeshS3: 7-Nov-16: moving this one down after removing the temp file
		//releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

		memset(szTempBuf, 0x00, sizeof(szTempBuf));
		sprintf(szTempBuf, "rm -f %s", SAF_RECORD_FILE_NAME_TEMP); //removing the Temporary SAF record file
		debug_sprintf(szDbgMsg,"%s - The command to be executed is [%s]", __FUNCTION__,szTempBuf);
		APP_TRACE(szDbgMsg);

		iRetVal = local_svcSystem(szTempBuf);
		if(iRetVal == 0)
		{
			debug_sprintf(szDbgMsg,"%s - Successfully executed the command to remove the temp SAF records file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

		}
		else
		{
			debug_sprintf(szDbgMsg,"%s - Error in execution of the command to remove the temp SAF records file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

		iRetVal = SAF_RECORD_NOT_FOUND;
		if(bAllowInProcessRecToRemove == PAAS_FALSE)
		{
			iRetVal = SAF_RECORD_IN_PROGRESS;
		}
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;

}

/*
 * ============================================================================
 * Function Name: clearSAFRecsByRecNumber
 *
 * Description	: This API clears the SAF records based on the SAF record number and
 *                fills the SAF transaction details list
 *
 * Input Params	: SAF Record Number
 *
 * Output Params: SAF_RECORD_FOUND/SAF_RECORD_NOT_FOUND
 * ============================================================================
 */
static int clearSAFRecsByRecNumber(char *pszSAFRecordNum, char *pszFileName, SAFDTLS_PTYPE pstSAFDtls)
{
	int		iRetVal      	   		= SUCCESS;
	int		iClearedInProcessRecord = 0;
	int		iIndex		 	   		= 0;
	int		iRecordLen	 	   		= 0;
	int		iPreviousRecLen    		= 0;
	int		iSAFRecFound       		= 0;
	int		iTotalRecLen       		= 0;
	int		iRecInProgress			= 0;
	FILE	*FhPerm			   		= NULL;
	FILE    *FhTemp            		= NULL;
	char	*pszSAFRecordData  		= NULL;
	char	*pchTemp		   		= NULL;
	char	szSAFRecNum[20]    		= "";
	char	szRecordLength[10] 		= "";
	char	*pchEndPos         		= NULL;
	//float	fCurrentAmount     		= 0.0;
	char	szTempBuf[100]	   		= "";
	char    *pszTempSAFRecord  		= NULL;
	char	szTimeStampLine[50]	    = "";
	PAAS_BOOL bIsTORTran			= PAAS_FALSE;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRecordNum == NULL || strlen(pszSAFRecordNum) == 0)
	{
		debug_sprintf(szDbgMsg, "%s: Incoming parameter is NULL or does not contain any value", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Need to clear the SAF Record with %s number in %s file", __FUNCTION__, pszSAFRecordNum, pszFileName);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	FhPerm = fopen(pszFileName, "r+"); //Open a file for update (both for input and output). The file must exist.
	if(FhPerm==NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, pszFileName);
		APP_TRACE(szDbgMsg);
		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
		return FAILURE;
		//return SAF_RECORD_FILE_OPEN_ERROR;
	}

	FhTemp = fopen(SAF_RECORD_FILE_NAME_TEMP, "a+"); //Open file for output at the end of a file. Output operations always write data at the end of the file, expanding it. Repositioning operations (fseek, fsetpos, rewind) are ignored. The file is created if it does not exist
	if(FhTemp==NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening the SAF Temporary record file!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		fclose(FhPerm);
		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
		return ERR_FILE_NOT_FOUND;
	}

	for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
	{
		debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iRecordLen = getRecordLength(FhPerm);
		if(iRecordLen == FAILURE)
		{
			debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record length!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			fclose(FhPerm);
			fclose(FhTemp);
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return FAILURE;
		}

		debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
		APP_TRACE(szDbgMsg);

		if(iRecordLen == 0)
		{
			debug_sprintf(szDbgMsg, "%s: No more records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		memset(szRecordLength, 0x00, sizeof(szRecordLength));
		sprintf(szRecordLength, "%d", iRecordLen);

		iTotalRecLen = iRecordLen + strlen(szRecordLength)/*For the length*/ + 1 /*For PIPE after length*/ + 2 /*Buffer*/;

		if(iPreviousRecLen < iTotalRecLen)
		{
			//Allocate the memory to hold the SAF record
			pchTemp = (char *)realloc(pszSAFRecordData, iTotalRecLen);
			if(pchTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhTemp);
				fclose(FhPerm); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			else
			{
				memset(pchTemp, 0x00, iTotalRecLen);
				pszSAFRecordData = pchTemp;
			}
			iPreviousRecLen = iTotalRecLen;
		}
		else
		{
			memset(pszSAFRecordData, 0x00, iPreviousRecLen);
		}

		if(fgets(pszSAFRecordData, iTotalRecLen, FhPerm) == NULL)
		{
			if(feof(FhPerm))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhTemp);
				fclose(FhPerm); //Closing the file before returning
				/* Daivik-25/1/2016: Coverity 67199 - Memory Leak if we get NULL and it is not end of file. */
				free(pszSAFRecordData);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
		}

		if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line...", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iIndex = iIndex -1; //Time Stamp lines are not part of the Total number of records, need to read more

			debug_sprintf(szDbgMsg, "%s: Need to write the timestamp line to Temp file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szTimeStampLine, 0x00, sizeof(szTimeStampLine));
			sprintf(szTimeStampLine, "%d%c%s", strlen(pszSAFRecordData), PIPE, pszSAFRecordData);

			iRecordLen = strlen(szTimeStampLine);

			iRetVal = fwrite(szTimeStampLine, sizeof(char), iRecordLen, FhTemp);

			if(iRetVal == iRecordLen)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully written timestamp line to the temporary file ", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while writing timestamp line, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLen, iRetVal);
				APP_TRACE(szDbgMsg);
				//Praveen_P1: What to do here???
			}

			continue;
		}

		debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		memset(szSAFRecNum, 0x00, sizeof(szSAFRecNum));
		iRetVal = getSAFRecordStatusAndNumber(pszSAFRecordData, szSAFRecNum);
		//iRetVal = getSAFRecordNumber(pszSAFRecordData, szSAFRecNum);

		if(iRetVal > 0 && ((iRetVal == SAF_TOR_STATUS_PENDING) || (iRetVal == SAF_TOR_STATUS_NOTALLOWED) || (iRetVal == SAF_TOR_STATUS_NOTFOUND)
			||	(iRetVal == SAF_TOR_STATUS_REVERSED)))
		{
			bIsTORTran = PAAS_TRUE;
		}
		else
		{
			bIsTORTran = PAAS_FALSE;
		}

		debug_sprintf(szDbgMsg, "%s: The TOR Tran status [%d]", __FUNCTION__, bIsTORTran);
		APP_TRACE(szDbgMsg);



		if((strcmp(szSAFRecNum, pszSAFRecordNum) == 0) && !bIsTORTran) //If the SAF record number of fetched SAF record is same as the given SAF record, need to fetch the next record
		{
			debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is same as given SAF Record Number[%s]", __FUNCTION__, szSAFRecNum, pszSAFRecordNum);
			APP_TRACE(szDbgMsg);
			if((iRetVal == SAF_STATUS_INPROCESS))
			{
				acquireMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");
				if(iSAFInProgressStatus == SAF_IN_FLIGHT)
				{
					debug_sprintf(szDbgMsg, "%s: SAF record with status IN_PROCESS is currently IN_FLIGHT, Should Not Delete the Record. Copying the record to temp file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iRecInProgress = 1;
					releaseMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");
					break;
				}
				else
				{
					/*-----------Delete the SAF SSI Tran Key ---------- */
					if(popSSITran(szSSITranKey) != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s:Error while popping the SAF SSI transaction from stack", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					deleteStack(szSSITranKey);
					memset(szSSITranKey, 0x00, sizeof(szSSITranKey));
					releaseMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");
				}
			}

			debug_sprintf(szDbgMsg, "%s:  Clearing %d SAF record Number, not writing to temp file", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iSAFRecFound = 1;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while filling the SAF transaction details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//FIXME: What to do here???
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is not same as given SAF Record Number[%s], copying the record to temp file", __FUNCTION__, szSAFRecNum, pszSAFRecordNum);
			APP_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s:  Writing %d SAF record Number to temp file", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			pchEndPos = strchr(pszSAFRecordData, SEMI_COLON);

			if(pchEndPos == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain SEMI COLON in the SAF record", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm);
				fclose(FhTemp); //Closing the file before returning
				//CID 67199 (#1 of 2): Resource leak (RESOURCE_LEAK). T_RaghavendranR1. Free the allocated memory if failed.
				free(pszSAFRecordData);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}

			*pchEndPos = 0x00; //Ending the SAF record at the SEMI COLON
			pchEndPos++; //Pointing to LF
			*pchEndPos = 0x00; //Nullyfying the LF also

			pszTempSAFRecord = (char *)malloc(sizeof(char) * (iTotalRecLen + 2) /* Buffer */);
			if(pszTempSAFRecord == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while allocating memory for the Temp SAF record", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm);
				fclose(FhTemp); //Closing the file before returning
				//CID 67199 (#2 of 2): Resource leak (RESOURCE_LEAK). T_RaghavendranR1. Free the allocated memory if failed.
				free(pszSAFRecordData);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			memset(pszTempSAFRecord, 0x00,  iTotalRecLen + 2);
			sprintf(pszTempSAFRecord, "%d%c%s%c%c", strlen(pszSAFRecordData) + 1 /* For the ;*/ + 1 /*For the <LF>*/, PIPE, pszSAFRecordData, SEMI_COLON, LF /*New Line Field*/);
			memset(pszSAFRecordData, 0x00, iTotalRecLen);
			memcpy(pszSAFRecordData, pszTempSAFRecord, strlen(pszTempSAFRecord));
			free(pszTempSAFRecord); //Freeing temp SAF record

			//Update the temporary file
			iRecordLen = strlen(pszSAFRecordData);

			iRetVal = fwrite(pszSAFRecordData, sizeof(char), iRecordLen, FhTemp);

			if(iRetVal == iRecordLen)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully written SAF record of index %d to the temporary file ", __FUNCTION__, iIndex);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while writing SAF record to the file, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLen, iRetVal);
				APP_TRACE(szDbgMsg);
				//Praveen_P1: What to do here???
			}
		}
	}

	fclose(FhPerm);
	fclose(FhTemp); //Closing the file before doing some operations like moving

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//Copying the temporary SAF record to original file
		memset(szTempBuf, 0x00, sizeof(szTempBuf));
		sprintf(szTempBuf, "mv -f %s %s", SAF_RECORD_FILE_NAME_TEMP, pszFileName); //copying the file
		debug_sprintf(szDbgMsg,"%s - The command to be executed is [%s]", __FUNCTION__,szTempBuf);
		APP_TRACE(szDbgMsg);
		iRetVal = local_svcSystem(szTempBuf);

		if(iRetVal == 0)
		{
			debug_sprintf(szDbgMsg,"%s - Successfully executed the command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = SAF_RECORD_FOUND;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s - Error in execution of the command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
		}

		if(chmod(pszFileName, 0666) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

		if(iClearedInProcessRecord == 1)
		{
			/* The part of the code will not be executed now, as we are not deleting the IN_PROCESS SFA Record */
			debug_sprintf(szDbgMsg,"%s - Cleared the IN_PROCESS SAF record, need to remove the same from the SAF record structure", __FUNCTION__);
			APP_TRACE(szDbgMsg);
#if 0
			/*------------- Clear the contents of the SAF Record Structure------------------- */
			if(stSAFRecord.stSignatureDetails.sigData != NULL)
			{
				free(stSAFRecord.stSignatureDetails.sigData);
			}
			memset(&stSAFRecord.stTransactionDetails, 0x00, sizeof(SAF_TRANDATA_S_TYPE));
			memset(&stSAFRecord.stCardDetails, 0x00, sizeof(SAF_CARDDATA_S_TYPE));
			memset(&stSAFRecord.stSignatureDetails, 0x00, sizeof(SIGN_DATA_STYPE));
			memset(&stSAFRecord, 0x00, sizeof(SAF_RECORD_ST_TYPE)); //Mem setting the structure
#endif
			//Since we have cleared the IN_PROCESS SAF record, need to update the system with the next ELIGIBLE record
			/*-----------Update the SAF data system with the next ELIGIBLE SAF record ---------- */
			iRetVal = readQueuedSAFRecord();
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated the SAF data system with the next SAF record with ELIGIBLE status",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				iRetVal = SAF_RECORD_FOUND;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while updating the SAF data system with the next SAF record with ELIGIBLE status",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
	}
	else if(iRecInProgress == 1)
	{
		debug_sprintf(szDbgMsg, "%s: Should not clear SAF record with status as IN_PROCESS", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

		iRetVal = SAF_RECORD_IN_PROGRESS;
	}
	else
	{
		//MukeshS3: 7-Nov-16: moving this one down after removing the SAF temp record file
		//releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

		memset(szTempBuf, 0x00, sizeof(szTempBuf));
		sprintf(szTempBuf, "rm -f %s", SAF_RECORD_FILE_NAME_TEMP); //removing the Temporary SAF record file
		debug_sprintf(szDbgMsg,"%s - The command to be executed is [%s]", __FUNCTION__,szTempBuf);
		APP_TRACE(szDbgMsg);

		iRetVal = local_svcSystem(szTempBuf);
		if(iRetVal == 0)
		{
			debug_sprintf(szDbgMsg,"%s - Successfully executed the command to remove the temp SAF records file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

		}
		else
		{
			debug_sprintf(szDbgMsg,"%s - Error in execution of the command to remove the temp SAF records file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

		if(iRecInProgress == 1)
		{
			debug_sprintf(szDbgMsg, "%s: Should not clear SAF record with status as IN_PROCESS", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = SAF_RECORD_IN_PROGRESS;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = SAF_RECORD_NOT_FOUND;
		}
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: filterSAFRecsByRecNumber
 *
 * Description	: This API filters the SAF records based on the SAF record number and
 *                fills the SAF transaction details list
 *
 * Input Params	: SAF Record Number
 *
 * Output Params: SAF_RECORD_FOUND/SAF_RECORD_NOT_FOUND
 * ============================================================================
 */
static int filterSAFRecsByRecNumber(char *pszSAFRecordNum, SAFDTLS_PTYPE pstSAFDtls)
{
	int		iRetVal      	 = SUCCESS;
	FILE	*Fh			 	 = NULL;
	int		iIndex		 	 = 0;
	int		iRecordLen	 	 = 0;
	int		iPreviousRecLen  = 0;
	char	*pszSAFRecordData = NULL;
	char	*pchTemp		 = NULL;
	int		iSAFRecFound     = 0;
	char	szSAFRecNum[20]  = "";
	int		iFileNumber	     = 0;
	struct	stat st;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRecordNum == NULL || strlen(pszSAFRecordNum) == 0 || pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Incoming parameter is NULL or does not contain any value", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	//Check if there are any records presently
	if(giTotalSAFRecords == 0)
	{
		debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	debug_sprintf(szDbgMsg, "%s: Need to look for SAF Record with %s number", __FUNCTION__, pszSAFRecordNum);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	for(iFileNumber = 0; iFileNumber < 2; iFileNumber++)
	{
		if(iSAFRecFound == 1)
		{
			debug_sprintf(szDbgMsg, "%s: Got the required SAF record, breaking", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		if(iFileNumber == 0)
		{
			if(stat(SAF_ELIGIBLE_RECORDS_FILE_NAME, &st) == 0)
			{
				if(st.st_size > 1)
				{
					Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
					if(Fh==NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
						APP_TRACE(szDbgMsg);
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}
		else if(iFileNumber == 1)
		{
			if(stat(SAF_COMPLETED_RECORDS_FILE_NAME, &st) == 0)
			{
				if(st.st_size > 1)
				{
					Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
					if(Fh==NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
						APP_TRACE(szDbgMsg);
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}

		for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
		{
			debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			iRecordLen = getRecordLength(Fh);

			debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
			APP_TRACE(szDbgMsg);

			if(iRecordLen <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: No more records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			if(iPreviousRecLen < iRecordLen + 1)
			{
				//Allocate the memory to hold the SAF record
				pchTemp = (char *)realloc(pszSAFRecordData, iRecordLen + 10);
				if(pchTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					fclose(Fh); //Closing the file before returning
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
				else
				{
					memset(pchTemp, 0x00, iRecordLen + 10);
					pszSAFRecordData = pchTemp;
				}
				iPreviousRecLen = iRecordLen + 10;
			}
			else
			{
				if(pszSAFRecordData != NULL) //CID 67404, 67342 (#1 of 1): Dereference before null check (REVERSE_INULL) T_RaghavendranR1
				{
					memset(pszSAFRecordData, 0x00, iRecordLen + 1);
				}
			}

			//CID 67449 (#1 of 1): Out-of-bounds access (OVERRUN), T_RaghavendranR1 Allocating enough memory, so that same can be accessed in fgets() and we do not overrun it also.
			if((pszSAFRecordData != NULL)  && (fgets(pszSAFRecordData, iRecordLen+10, Fh) == NULL)) //CID 67404, 67342 (#1 of 1): Dereference before null check (REVERSE_INULL) T_RaghavendranR1
			{
				if(feof(Fh))
				{
					debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					fclose(Fh); //Closing the file before returning
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					if(pszSAFRecordData != NULL)
					{
						free(pszSAFRecordData);
					}
					return FAILURE;
				}
			}

			if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iIndex = iIndex -1; //Time Stmap lines are not part of the Total number of records, need to read more
				continue;
			}

			iRetVal = getSAFRecordStatus(pszSAFRecordData);
			if(iRetVal == SAF_TOR_STATUS_NOTALLOWED || iRetVal == SAF_TOR_STATUS_NOTFOUND ||
					iRetVal == SAF_TOR_STATUS_PENDING || iRetVal == SAF_TOR_STATUS_REVERSED)
			{
				debug_sprintf(szDbgMsg, "%s: It is a TOR records..Skipping it...", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				continue;
			}

			debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			memset(szSAFRecNum, 0x00, sizeof(szSAFRecNum));
			iRetVal = getSAFRecordNumber(pszSAFRecordData, szSAFRecNum);

			if(strcmp(szSAFRecNum, pszSAFRecordNum)) //If the SAF record number of current record is not same, need to fetch the next record
			{
				debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is not same as given SAF Record Number[%s]", __FUNCTION__, szSAFRecNum, pszSAFRecordNum);
				APP_TRACE(szDbgMsg);
				continue; //Need to go to the next record if it exists
			}

			debug_sprintf(szDbgMsg, "%s:  %d SAF record Number is %s", __FUNCTION__, iIndex, szSAFRecNum);
			APP_TRACE(szDbgMsg);

			iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iSAFRecFound = 1;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//FIXME: What to do here???
			}
			break;
		}
		if(Fh != NULL)
		{
			fclose(Fh); //Closing the file
		}
	}

	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_FOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_NOT_FOUND;
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;

}

/*
 * ============================================================================
 * Function Name: filterSAFTORRecsByRecNumber
 *
 * Description	: This API filters the TOR SAF records based on the SAF record number and
 *                fills the SAF transaction details list
 *
 * Input Params	: SAF Record Number
 *
 * Output Params: SAF_RECORD_FOUND/SAF_RECORD_NOT_FOUND
 * ============================================================================
 */
static int filterSAFTORRecsByRecNumber(char *pszSAFRecordNum, SAFDTLS_PTYPE pstSAFDtls)
{
	int		iRetVal      	 = SUCCESS;
	FILE	*Fh			 	 = NULL;
	int		iIndex		 	 = 0;
	int		iRecordLen	 	 = 0;
	int		iPreviousRecLen  = 0;
	char	*pszSAFRecordData = NULL;
	char	*pchTemp		 = NULL;
	int		iSAFRecFound     = 0;
	char	szSAFRecNum[20]  = "";
	int		iFileNumber	     = 0;
	struct	stat st;
#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRecordNum == NULL || strlen(pszSAFRecordNum) == 0 || pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Incoming parameter is NULL or does not contain any value", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	if(giTotalSAFTORRecords == 0)
	{
		debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}
	debug_sprintf(szDbgMsg, "%s: Need to look for SAF Record with %s number", __FUNCTION__, pszSAFRecordNum);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	for(iFileNumber = 0; iFileNumber < 2; iFileNumber++)
	{
		if(iSAFRecFound == 1)
		{
			debug_sprintf(szDbgMsg, "%s: Got the required SAF record, breaking", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		if(iFileNumber == 0)
		{
			if(stat(SAF_ELIGIBLE_RECORDS_FILE_NAME, &st) == 0)
			{
				if(st.st_size > 1)
				{
					Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
					if(Fh==NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
						APP_TRACE(szDbgMsg);
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}
		else if(iFileNumber == 1)
		{
			if(stat(SAF_COMPLETED_RECORDS_FILE_NAME, &st) == 0)
			{
				if(st.st_size > 1)
				{
					Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
					if(Fh==NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
						APP_TRACE(szDbgMsg);
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}

		for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
		{
			debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			iRecordLen = getRecordLength(Fh);

			debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
			APP_TRACE(szDbgMsg);

			if(iRecordLen <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: No more records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			if(iPreviousRecLen < iRecordLen + 1)
			{
				//Allocate the memory to hold the SAF record
				pchTemp = (char *)realloc(pszSAFRecordData, iRecordLen + 10);
				if(pchTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					fclose(Fh); //Closing the file before returning
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
				else
				{
					memset(pchTemp, 0x00, iRecordLen + 10);
					pszSAFRecordData = pchTemp;
				}
				iPreviousRecLen = iRecordLen + 10;
			}
			else
			{
				if(pszSAFRecordData != NULL) //CID 67404, 67342 (#1 of 1): Dereference before null check (REVERSE_INULL) T_RaghavendranR1
				{
					memset(pszSAFRecordData, 0x00, iRecordLen + 1);
				}
			}

			//CID 67449 (#1 of 1): Out-of-bounds access (OVERRUN), T_RaghavendranR1 Allocating enough memory, so that same can be accessed in fgets() and we do not overrun it also.
			if((pszSAFRecordData != NULL)  && (fgets(pszSAFRecordData, iRecordLen+10, Fh) == NULL)) //CID 67404, 67342 (#1 of 1): Dereference before null check (REVERSE_INULL) T_RaghavendranR1
			{
				if(feof(Fh))
				{
					debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					fclose(Fh); //Closing the file before returning
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					if(pszSAFRecordData != NULL)
					{
						free(pszSAFRecordData);
					}
					return FAILURE;
				}
			}

			if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iIndex = iIndex -1; //Time Stmap lines are not part of the Total number of records, need to read more
				continue;
			}

			iRetVal = getSAFRecordStatus(pszSAFRecordData);
			if(iRetVal == SAF_STATUS_DECLINED || iRetVal == SAF_STATUS_INPROCESS ||
					iRetVal == SAF_STATUS_NOTPROCESSED || iRetVal == SAF_STATUS_PROCESSED ||
					iRetVal == SAF_STATUS_QUEUED)
			{
				debug_sprintf(szDbgMsg, "%s: It is a Payment SAF records..Skipping it...", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				continue;
			}

			memset(szSAFRecNum, 0x00, sizeof(szSAFRecNum));
			iRetVal = getSAFRecordNumber(pszSAFRecordData, szSAFRecNum);

			if(strcmp(szSAFRecNum, pszSAFRecordNum)) //If the SAF record number of current record is not same, need to fetch the next record
			{
				debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is not same as given SAF Record Number[%s]", __FUNCTION__, szSAFRecNum, pszSAFRecordNum);
				APP_TRACE(szDbgMsg);
				continue; //Need to go to the next record if it exists
			}

			debug_sprintf(szDbgMsg, "%s:  %d SAF record Number is %s", __FUNCTION__, iIndex, szSAFRecNum);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iSAFRecFound = 1;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//FIXME: What to do here???
			}
			break;
		}
		if(Fh != NULL)
		{
			fclose(Fh); //Closing the file
		}
	}

	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_FOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_NOT_FOUND;
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: filterSAFRecsByStatusAndRecRange
 *
 * Description	: This API filters the SAF records based on the SAF record number range and
 *                SAF Status and fills the SAF transaction details list
 *
 * Input Params	: SAF Status & SAF Start Record Number, SAF End Record Number
 *
 * Output Params: SAF_RECORD_FOUND/SAF_RECORD_NOT_FOUND
 * ============================================================================
 */
static int filterSAFRecsByStatusAndRecRange(SAFDTLS_PTYPE pstSAFDtls)
{
	int		iRetVal      	  = SUCCESS;
	FILE	*Fh			 	  = NULL;
	int		iIndex		 	  = 0;
	int		iRecordLen	 	  = 0;
	int		iPreviousRecLen   = 0;
	char	*pszSAFRecordData = NULL;
	char	*pchTemp		  = NULL;
	int		iSAFStatus		  = 0;
	int		iSAFRecFound      = 0;
	char	szSAFRecNum[20]   = "";
	int		iSAFStartRecNum   = 0;
	int		iSAFEndRecNum     = 0;
	int		iSAFCurrentRecNum = 0;
	struct	stat st;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(giTotalSAFRecords == 0)
	{
		debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	iSAFStartRecNum = atoi(pstSAFDtls->szBegNo);
	iSAFEndRecNum   = atoi(pstSAFDtls->szEndNo);

	if(strcasecmp(pstSAFDtls->szStatus, "ELIGIBLE") == 0)
	{
		iSAFStatus = SAF_STATUS_QUEUED;
	}
	else if(strcasecmp(pstSAFDtls->szStatus, "PROCESSED") == 0)
	{
		iSAFStatus = SAF_STATUS_PROCESSED;
	}
	else if(strcasecmp(pstSAFDtls->szStatus, "IN_PROCESS") == 0)
	{
		iSAFStatus = SAF_STATUS_INPROCESS;
	}
	else if(strcasecmp(pstSAFDtls->szStatus, "NOT_PROCESSED") == 0)
	{
		iSAFStatus = SAF_STATUS_NOTPROCESSED;
	}
	else if(strcasecmp(pstSAFDtls->szStatus, "DECLINED") == 0)
	{
		iSAFStatus = SAF_STATUS_DECLINED;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid SAF Status!!!![%s]", __FUNCTION__, pstSAFDtls->szStatus);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	debug_sprintf(szDbgMsg, "%s: Given SAF Status [%s] [%d], SAF Record Number Range [%s, %s]", __FUNCTION__, pstSAFDtls->szStatus, iSAFStatus, pstSAFDtls->szBegNo, pstSAFDtls->szEndNo);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(iSAFStatus == SAF_STATUS_QUEUED || iSAFStatus == SAF_STATUS_INPROCESS)
	{
		if(stat(SAF_ELIGIBLE_RECORDS_FILE_NAME, &st) == 0)
		{
			if(st.st_size > 1)
			{
				Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
				if(Fh==NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return SAF_RECORD_NOT_FOUND;//File would not have created if there are no records found
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
			APP_TRACE(szDbgMsg);
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
		}
	}
	else if(iSAFStatus == SAF_STATUS_PROCESSED || iSAFStatus == SAF_STATUS_DECLINED || iSAFStatus == SAF_STATUS_NOTPROCESSED)
	{
		if(stat(SAF_COMPLETED_RECORDS_FILE_NAME, &st) == 0)
		{
			if(st.st_size > 1)
			{
				Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
				if(Fh==NULL)
				{
					//CID 67341 (#1 of 1): Copy-paste error (COPY_PASTE_ERROR) changed the File name in debug print. T_RaghavendranR1.
					debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
			APP_TRACE(szDbgMsg);
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
		}
	}


	for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
	{
		debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iRecordLen = getRecordLength(Fh);

		debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
		APP_TRACE(szDbgMsg);

		if(iRecordLen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No more records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		if(iPreviousRecLen < iRecordLen + 1)
		{
			//Allocate the memory to hold the SAF record
			pchTemp = (char *)realloc(pszSAFRecordData, iRecordLen + 10);
			if(pchTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			else
			{
				memset(pchTemp, 0x00, iRecordLen + 10);
				pszSAFRecordData = pchTemp;
			}
			iPreviousRecLen = iRecordLen + 10;
		}
		else
		{
			if(pszSAFRecordData != NULL)
			{
				memset(pszSAFRecordData, 0x00, iPreviousRecLen); // CID 67276 (#1 of 1): Explicit null dereferenced (FORWARD_NULL) T_raghavendranR1
			}
		}

		//CID 67463 (#1 of 1): Out-of-bounds access (OVERRUN). T_RaghavendranR1. Allocating enough memory, so that same can be accessed in fgets() and we do not overrun it also.
		if( (pszSAFRecordData != NULL) && (fgets(pszSAFRecordData, iRecordLen+10, Fh) == NULL)) // CID 67276 (#1 of 1): Explicit null dereferenced (FORWARD_NULL) T_RaghavendranR1
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				if(pszSAFRecordData != NULL)
				{
					free(pszSAFRecordData);
				}
				return FAILURE;
			}
		}

		if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iIndex = iIndex -1; //Time Stmap lines are not part of the Total number of records, need to read more
			continue;
		}

		debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		memset(szSAFRecNum, 0x00, sizeof(szSAFRecNum));
		iRetVal = getSAFRecordStatusAndNumber(pszSAFRecordData, szSAFRecNum);

		iSAFCurrentRecNum = atoi(szSAFRecNum);

		if(iRetVal == SAF_TOR_STATUS_NOTALLOWED || iRetVal == SAF_TOR_STATUS_NOTFOUND ||
				iRetVal == SAF_TOR_STATUS_PENDING || iRetVal == SAF_TOR_STATUS_REVERSED)
		{
			debug_sprintf(szDbgMsg, "%s: It is a TOR records..Skipping it...", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			continue;
		}

		if(iRetVal == iSAFStatus && iSAFCurrentRecNum >= iSAFStartRecNum && iSAFCurrentRecNum <= iSAFEndRecNum) //If the SAF record number of current record is not same, need to fetch the next record
		{
			debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is in the given range of SAF Records and its status is %s", __FUNCTION__, szSAFRecNum, pstSAFDtls->szStatus);
			APP_TRACE(szDbgMsg);
			iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iSAFRecFound = 1;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//FIXME: What to do here???
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is not in the given range of SAF Records or SAF Status is not %s", __FUNCTION__, szSAFRecNum, pstSAFDtls->szStatus);
			APP_TRACE(szDbgMsg);
			if(iSAFCurrentRecNum > iSAFEndRecNum)//We reached the record which is greater than the END record
			{
				debug_sprintf(szDbgMsg, "%s: We are reaching the SAF record number which is greater than the given end range, breaking...", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
			else
			{
				continue; //Need to go to the next record if it exists
			}
		}
	}

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_FOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_NOT_FOUND;
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	fclose(Fh); //Closing the SAF Record File
	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: filterSAFTORRecsByStatusAndRecRange
 *
 * Description	: This API filters the TOR SAF records based on the SAF record number range and
 *                SAF Status and fills the SAF transaction details list
 *
 * Input Params	: SAF Status & SAF Start Record Number, SAF End Record Number
 *
 * Output Params: SAF_RECORD_FOUND/SAF_RECORD_NOT_FOUND
 * ============================================================================
 */
static int filterSAFTORRecsByStatusAndRecRange(SAFDTLS_PTYPE pstSAFDtls)
{
	int		iRetVal      	  = SUCCESS;
	FILE	*Fh			 	  = NULL;
	int		iIndex		 	  = 0;
	int		iRecordLen	 	  = 0;
	int		iPreviousRecLen   = 0;
	char	*pszSAFRecordData = NULL;
	char	*pchTemp		  = NULL;
	int		iSAFStatus		  = 0;
	int		iSAFRecFound      = 0;
	char	szSAFRecNum[20]   = "";
	int		iSAFStartRecNum   = 0;
	int		iSAFEndRecNum     = 0;
	int		iSAFCurrentRecNum = 0;
	struct	stat st;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(giTotalSAFTORRecords== 0)
	{
		debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	iSAFStartRecNum = atoi(pstSAFDtls->szTORBegNo);
	iSAFEndRecNum   = atoi(pstSAFDtls->szTOREndNo);

	if(strcasecmp(pstSAFDtls->szTORStatus, "PENDING") == 0)				/*The SAF status Pending, Reversed, Not allowed, Not found are added for SAF on TOR*/
	{
		iSAFStatus = SAF_TOR_STATUS_PENDING;
	}
	else if(strcasecmp(pstSAFDtls->szTORStatus, "REVERSED") == 0)
	{
		iSAFStatus = SAF_TOR_STATUS_REVERSED;
	}
	else if(strcasecmp(pstSAFDtls->szTORStatus, "NOT_ALLOWED") == 0)
	{
		iSAFStatus = SAF_TOR_STATUS_NOTALLOWED;
	}
	else if(strcasecmp(pstSAFDtls->szTORStatus, "NOT_FOUND") == 0)
	{
		iSAFStatus = SAF_TOR_STATUS_NOTFOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid SAF Status!!!![%s]", __FUNCTION__, pstSAFDtls->szStatus);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	debug_sprintf(szDbgMsg, "%s: Given SAF Status [%s] [%d], SAF Record Number Range [%s, %s]", __FUNCTION__, pstSAFDtls->szTORStatus, iSAFStatus, pstSAFDtls->szTORBegNo, pstSAFDtls->szTOREndNo);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(iSAFStatus == SAF_TOR_STATUS_PENDING)
	{
		if(stat(SAF_ELIGIBLE_RECORDS_FILE_NAME, &st) == 0)
		{
			if(st.st_size > 1)
			{
				Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
				if(Fh==NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return SAF_RECORD_NOT_FOUND;//File would not have created if there are no records found
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
			APP_TRACE(szDbgMsg);
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
		}
	}
	else if(iSAFStatus == SAF_TOR_STATUS_REVERSED || iSAFStatus == SAF_TOR_STATUS_NOTALLOWED || iSAFStatus == SAF_TOR_STATUS_NOTFOUND)
	{
		if(stat(SAF_COMPLETED_RECORDS_FILE_NAME, &st) == 0)
		{
			if(st.st_size > 1)
			{
				Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
				if(Fh==NULL)
				{
					//CID 67341 (#1 of 1): Copy-paste error (COPY_PASTE_ERROR) changed the File name in debug print. T_RaghavendranR1.
					debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
			APP_TRACE(szDbgMsg);
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
		}
	}


	for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
	{
		debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iRecordLen = getRecordLength(Fh);

		debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
		APP_TRACE(szDbgMsg);

		if(iRecordLen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No more records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		if(iPreviousRecLen < iRecordLen + 1)
		{
			//Allocate the memory to hold the SAF record
			pchTemp = (char *)realloc(pszSAFRecordData, iRecordLen + 10);
			if(pchTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			else
			{
				memset(pchTemp, 0x00, iRecordLen + 10);
				pszSAFRecordData = pchTemp;
			}
			iPreviousRecLen = iRecordLen + 10;
		}
		else
		{
			if(pszSAFRecordData != NULL)
			{
				memset(pszSAFRecordData, 0x00, iPreviousRecLen); // CID 67276 (#1 of 1): Explicit null dereferenced (FORWARD_NULL) T_raghavendranR1
			}
		}

		//CID 67463 (#1 of 1): Out-of-bounds access (OVERRUN). T_RaghavendranR1. Allocating enough memory, so that same can be accessed in fgets() and we do not overrun it also.
		if( (pszSAFRecordData != NULL) && (fgets(pszSAFRecordData, iRecordLen+10, Fh) == NULL)) // CID 67276 (#1 of 1): Explicit null dereferenced (FORWARD_NULL) T_RaghavendranR1
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				if(pszSAFRecordData != NULL)
				{
					free(pszSAFRecordData);
				}
				return FAILURE;
			}
		}

		if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iIndex = iIndex -1; //Time Stmap lines are not part of the Total number of records, need to read more
			continue;
		}

		debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		memset(szSAFRecNum, 0x00, sizeof(szSAFRecNum));
		iRetVal = getSAFRecordStatusAndNumber(pszSAFRecordData, szSAFRecNum);

		iSAFCurrentRecNum = atoi(szSAFRecNum);

		if(iRetVal == SAF_STATUS_DECLINED || iRetVal == SAF_STATUS_INPROCESS || iRetVal == SAF_STATUS_NOTPROCESSED
				|| iRetVal == SAF_STATUS_PROCESSED || iRetVal == SAF_STATUS_QUEUED)
		{
			debug_sprintf(szDbgMsg, "%s: It is a Payment SAF records..Skipping it...", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			continue;
		}

		if(iRetVal == iSAFStatus && iSAFCurrentRecNum >= iSAFStartRecNum && iSAFCurrentRecNum <= iSAFEndRecNum) //If the SAF record number of current record is not same, need to fetch the next record
		{
			debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is in the given range of SAF Records and its status is %s", __FUNCTION__, szSAFRecNum, pstSAFDtls->szStatus);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: It is a TOR record", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iSAFRecFound = 1;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//FIXME: What to do here???
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current SAF Record Number[%s] is not in the given range of SAF Records or SAF Status is not %s", __FUNCTION__, szSAFRecNum, pstSAFDtls->szStatus);
			APP_TRACE(szDbgMsg);
			if(iSAFCurrentRecNum > iSAFEndRecNum)//We reached the record which is greater than the END record
			{
				debug_sprintf(szDbgMsg, "%s: We are reaching the SAF record number which is greater than the given end range, breaking...", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
			else
			{
				continue; //Need to go to the next record if it exists
			}
		}
	}

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_FOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_NOT_FOUND;
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	fclose(Fh); //Closing the SAF Record File
	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;

}
/*
 * ============================================================================
 * Function Name: filterSAFRecsByStatusAndRecNum
 *
 * Description	: This API filters the SAF records based on the SAF record number and SAF Status
 *                fills the SAF transaction details list
 *
 * Input Params	: SAF Status & SAF Record Number
 *
 * Output Params: SAF_RECORD_FOUND/SAF_RECORD_NOT_FOUND
 * ============================================================================
 */
static int filterSAFRecsByStatusAndRecNum(SAFDTLS_PTYPE pstSAFDtls, char *pszSAFRecordNum)
{
	int		iRetVal      	  = SUCCESS;
	FILE	*Fh			 	  = NULL;
	int		iIndex		 	  = 0;
	int		iRecordLen	 	  = 0;
	int		iPreviousRecLen   = 0;
	char	*pszSAFRecordData = NULL;
	char	*pchTemp		  = NULL;
	int		iSAFStatus		  = 0;
	int		iSAFRecFound      = 0;
	char	szSAFRecNum[20]   = "";
	struct	stat st;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL || pszSAFRecordNum == NULL || strlen(pszSAFRecordNum) == 0)
	{
		debug_sprintf(szDbgMsg, "%s: Incoming parameter(s) is(are) NULL or does not contain any value", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(giTotalSAFRecords == 0)
	{
		debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	if(strcasecmp(pstSAFDtls->szStatus, "ELIGIBLE") == 0)
	{
		iSAFStatus = SAF_STATUS_QUEUED;
	}
	else if(strcasecmp(pstSAFDtls->szStatus, "PROCESSED") == 0)
	{
		iSAFStatus = SAF_STATUS_PROCESSED;
	}
	else if(strcasecmp(pstSAFDtls->szStatus, "IN_PROCESS") == 0)
	{
		iSAFStatus = SAF_STATUS_INPROCESS;
	}
	else if(strcasecmp(pstSAFDtls->szStatus, "NOT_PROCESSED") == 0)
	{
		iSAFStatus = SAF_STATUS_NOTPROCESSED;
	}
	else if(strcasecmp(pstSAFDtls->szStatus, "DECLINED") == 0)
	{
		iSAFStatus = SAF_STATUS_DECLINED;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid SAF Status!!!![%s]", __FUNCTION__, pstSAFDtls->szStatus);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	debug_sprintf(szDbgMsg, "%s: Given SAF Status [%s] [%d], SAF Record Number [%s]", __FUNCTION__, pstSAFDtls->szStatus, iSAFStatus, pszSAFRecordNum);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(iSAFStatus == SAF_STATUS_QUEUED || iSAFStatus == SAF_STATUS_INPROCESS)
	{
		if(stat(SAF_ELIGIBLE_RECORDS_FILE_NAME, &st) == 0)
		{
			if(st.st_size > 1)
			{
				Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
				if(Fh==NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering it as no records found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
			APP_TRACE(szDbgMsg);
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return SAF_RECORD_NOT_FOUND;//File would not have created if there are no records found
		}
	}
	else if(iSAFStatus == SAF_STATUS_PROCESSED || iSAFStatus == SAF_STATUS_DECLINED || iSAFStatus == SAF_STATUS_NOTPROCESSED)
	{
		if(stat(SAF_COMPLETED_RECORDS_FILE_NAME, &st) == 0)
		{
			if(st.st_size > 1)
			{
				Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
				if(Fh==NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering it as no records found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
			APP_TRACE(szDbgMsg);
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return SAF_RECORD_NOT_FOUND;//File would not have created if there are no records found
		}
	}

	for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
	{
		debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iRecordLen = getRecordLength(Fh);

		debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
		APP_TRACE(szDbgMsg);

		if(iRecordLen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No more records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		if(iPreviousRecLen < iRecordLen + 1)
		{
			//Allocate the memory to hold the SAF record
			pchTemp = (char *)realloc(pszSAFRecordData, iRecordLen + 10);
			if(pchTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file beofore returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			else
			{
				memset(pchTemp, 0x00, iRecordLen + 10);
				pszSAFRecordData = pchTemp;
			}
			iPreviousRecLen = iRecordLen + 10;
		}
		else
		{
			if(pszSAFRecordData != NULL)
			{
				memset(pszSAFRecordData, 0x00, iPreviousRecLen); // CID 67363, 67359 (#1 of 1): Explicit null dereferenced (FORWARD_NULL). T_RaghavendranR1 NULl check added
			}
		}

		//CID 67331 (#1 of 1): Out-of-bounds access (OVERRUN). T_RaghavendranR1. Allocating enough memory, so that same can be accessed in fgets() and we do not overrun it also.
		if( (pszSAFRecordData != NULL) && fgets(pszSAFRecordData, iRecordLen+10, Fh) == NULL) // CID 67363, 67359(#1 of 1): Explicit null dereferenced (FORWARD_NULL). T_RaghavendranR1 NULl check added
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				if(pszSAFRecordData != NULL)
				{
					free(pszSAFRecordData);
				}
				return FAILURE;
			}
		}

		if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iIndex = iIndex -1; //Time Stmap lines are not part of the Total number of records, need to read more
			continue;
		}

		debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		memset(szSAFRecNum, 0x00, sizeof(szSAFRecNum));
		iRetVal = getSAFRecordStatusAndNumber(pszSAFRecordData, szSAFRecNum);

		if(iRetVal != iSAFStatus || strcmp(szSAFRecNum, pszSAFRecordNum))//If any one of them is not matching, proceed to the next one
		{
			debug_sprintf(szDbgMsg, "%s: SAF record status is not %s", __FUNCTION__, pstSAFDtls->szStatus);
			APP_TRACE(szDbgMsg);
			continue; //Need to go to the next record if it exists
		}

		debug_sprintf(szDbgMsg, "%s:  %d SAF record is %s", __FUNCTION__, iIndex, pstSAFDtls->szStatus);
		APP_TRACE(szDbgMsg);

		iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iSAFRecFound = 1;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

		}
		break; //Since only single record is requested, we can break from here
	}

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_FOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_NOT_FOUND;
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}
	fclose(Fh); //Closing the file
	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}
/*
 * ============================================================================
 * Function Name: filterSAFTORRecsByStatusAndRecNum
 *
 * Description	: This API filters the SAF TOR records based on the SAF record number and SAF Status
 *                fills the SAF transaction details list
 *
 * Input Params	: SAF Status & SAF Record Number
 *
 * Output Params: SAF_RECORD_FOUND/SAF_RECORD_NOT_FOUND
 * ============================================================================
 */
static int filterSAFTORRecsByStatusAndRecNum(SAFDTLS_PTYPE pstSAFDtls, char *pszSAFRecordNum)
{
	int		iRetVal      	  = SUCCESS;
	FILE	*Fh			 	  = NULL;
	int		iIndex		 	  = 0;
	int		iRecordLen	 	  = 0;
	int		iPreviousRecLen   = 0;
	char	*pszSAFRecordData = NULL;
	char	*pchTemp		  = NULL;
	int		iSAFStatus		  = 0;
	int		iSAFRecFound      = 0;
	char	szSAFRecNum[20]   = "";
	struct	stat st;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL || pszSAFRecordNum == NULL || strlen(pszSAFRecordNum) == 0)
	{
		debug_sprintf(szDbgMsg, "%s: Incoming parameter(s) is(are) NULL or does not contain any value", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	//Check if there are any records presently
	if(giTotalSAFTORRecords == 0)
	{
		debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	if(strcasecmp(pstSAFDtls->szTORStatus, "PENDING") == 0)				/*The SAF status Pending, Reversed, Not allowed, Not found are added for SAF on TOR*/
	{
		iSAFStatus = SAF_TOR_STATUS_PENDING;
	}
	else if(strcasecmp(pstSAFDtls->szTORStatus, "REVERSED") == 0)
	{
		iSAFStatus = SAF_TOR_STATUS_REVERSED;
	}
	else if(strcasecmp(pstSAFDtls->szTORStatus, "NOT_ALLOWED") == 0)
	{
		iSAFStatus = SAF_TOR_STATUS_NOTALLOWED;
	}
	else if(strcasecmp(pstSAFDtls->szTORStatus, "NOT_FOUND") == 0)
	{
		iSAFStatus = SAF_TOR_STATUS_NOTFOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid SAF Status!!!![%s]", __FUNCTION__, pstSAFDtls->szTORStatus);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	debug_sprintf(szDbgMsg, "%s: Given SAF Status [%s] [%d], SAF Record Number [%s]", __FUNCTION__, pstSAFDtls->szTORStatus, iSAFStatus, pszSAFRecordNum);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(iSAFStatus == SAF_TOR_STATUS_PENDING)
	{
		if(stat(SAF_ELIGIBLE_RECORDS_FILE_NAME, &st) == 0)
		{
			if(st.st_size > 1)
			{
				Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
				if(Fh==NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering it as no records found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
			APP_TRACE(szDbgMsg);
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return SAF_RECORD_NOT_FOUND;//File would not have created if there are no records found
		}
	}
	else if(iSAFStatus == SAF_TOR_STATUS_REVERSED || iSAFStatus == SAF_TOR_STATUS_NOTALLOWED || iSAFStatus == SAF_TOR_STATUS_NOTFOUND)
	{
		if(stat(SAF_COMPLETED_RECORDS_FILE_NAME, &st) == 0)
		{
			if(st.st_size > 1)
			{
				Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
				if(Fh==NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering it as no records found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
			APP_TRACE(szDbgMsg);
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return SAF_RECORD_NOT_FOUND;//File would not have created if there are no records found
		}
	}

	for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
	{
		debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iRecordLen = getRecordLength(Fh);

		debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
		APP_TRACE(szDbgMsg);

		if(iRecordLen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No more records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		if(iPreviousRecLen < iRecordLen + 1)
		{
			//Allocate the memory to hold the SAF record
			pchTemp = (char *)realloc(pszSAFRecordData, iRecordLen + 10);
			if(pchTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file beofore returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			else
			{
				memset(pchTemp, 0x00, iRecordLen + 10);
				pszSAFRecordData = pchTemp;
			}
			iPreviousRecLen = iRecordLen + 10;
		}
		else
		{
			if(pszSAFRecordData != NULL)
			{
				memset(pszSAFRecordData, 0x00, iPreviousRecLen); // CID 67363, 67359 (#1 of 1): Explicit null dereferenced (FORWARD_NULL). T_RaghavendranR1 NULl check added
			}
		}

		//CID 67331 (#1 of 1): Out-of-bounds access (OVERRUN). T_RaghavendranR1. Allocating enough memory, so that same can be accessed in fgets() and we do not overrun it also.
		if( (pszSAFRecordData != NULL) && fgets(pszSAFRecordData, iRecordLen+10, Fh) == NULL) // CID 67363, 67359(#1 of 1): Explicit null dereferenced (FORWARD_NULL). T_RaghavendranR1 NULl check added
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				if(pszSAFRecordData != NULL)
				{
					free(pszSAFRecordData);
				}
				return FAILURE;
			}
		}

		if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iIndex = iIndex -1; //Time Stmap lines are not part of the Total number of records, need to read more
			continue;
		}

		debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		memset(szSAFRecNum, 0x00, sizeof(szSAFRecNum));
		iRetVal = getSAFRecordStatusAndNumber(pszSAFRecordData, szSAFRecNum);

		if(iRetVal != iSAFStatus || strcmp(szSAFRecNum, pszSAFRecordNum))//If any one of them is not matching, proceed to the next one
		{
			debug_sprintf(szDbgMsg, "%s: SAF record status is not %s", __FUNCTION__, pstSAFDtls->szStatus);
			APP_TRACE(szDbgMsg);
			continue; //Need to go to the next record if it exists
		}

		debug_sprintf(szDbgMsg, "%s:  %d SAF record is %s", __FUNCTION__, iIndex, pstSAFDtls->szStatus);
		APP_TRACE(szDbgMsg);

		iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iSAFRecFound = 1;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

		}

		break; //Since only single record is requested, we can break from here
	}

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_FOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_NOT_FOUND;
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}
	fclose(Fh); //Closing the file
	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;

}
/*
 * ============================================================================
 * Function Name: filterSAFRecsByStatus
 *
 * Description	: This API filters the SAF payment records based on the SAF status and
 *                fills the SAF transaction details list
 *
 * Input Params	: SAF Details st
 *
 * Output Params: SAF_RECORD_FOUND/SAF_RECORD_NOT_FOUND
 * ============================================================================
 */
static int filterSAFRecsByStatus(SAFDTLS_PTYPE pstSAFDtls)
{
	int		iRetVal      	 = SUCCESS;
	FILE	*Fh			 	 = NULL;
	int		iIndex		 	 = 0;
	int		iRecordLen	 	 = 0;
	int		iPreviousRecLen  = 0;
	char	*pszSAFRecordData = NULL;
	char	*pchTemp		 = NULL;
	int		iSAFStatus		 = 0;
	int		iSAFRecFound     = 0;
	struct	stat st;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Incoming parameter is NULL ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	//Check if there are any records presently
	if(giTotalSAFRecords == 0)
	{
		debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	if(strcasecmp(pstSAFDtls->szStatus, "ELIGIBLE") == 0)
	{
		iSAFStatus = SAF_STATUS_QUEUED;
	}
	else if(strcasecmp(pstSAFDtls->szStatus, "PROCESSED") == 0)
	{
		iSAFStatus = SAF_STATUS_PROCESSED;
	}
	else if(strcasecmp(pstSAFDtls->szStatus, "IN_PROCESS") == 0)
	{
		iSAFStatus = SAF_STATUS_INPROCESS;
	}
	else if(strcasecmp(pstSAFDtls->szStatus, "NOT_PROCESSED") == 0)
	{
		iSAFStatus = SAF_STATUS_NOTPROCESSED;
	}
	else if(strcasecmp(pstSAFDtls->szStatus, "DECLINED") == 0)
	{
		iSAFStatus = SAF_STATUS_DECLINED;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid SAF Status!!!![%s]", __FUNCTION__, pstSAFDtls->szStatus);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	debug_sprintf(szDbgMsg, "%s: Given SAF Status [%s] [%d]", __FUNCTION__, pstSAFDtls->szStatus, iSAFStatus);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(iSAFStatus == SAF_STATUS_QUEUED || iSAFStatus == SAF_STATUS_INPROCESS)
	{
		if(stat(SAF_ELIGIBLE_RECORDS_FILE_NAME, &st) == 0)
		{
			if(st.st_size > 1)
			{
				Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
				if(Fh==NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering it as no records found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
			APP_TRACE(szDbgMsg);
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return SAF_RECORD_NOT_FOUND;//File would not have created if there are no records found
		}
	}
	else if(iSAFStatus == SAF_STATUS_PROCESSED || iSAFStatus == SAF_STATUS_DECLINED || iSAFStatus == SAF_STATUS_NOTPROCESSED)
	{
		if(stat(SAF_COMPLETED_RECORDS_FILE_NAME, &st) == 0)
		{
			if(st.st_size > 1)
			{
				Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
				if(Fh==NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);	// CID-67271: 2-Feb-16: MukeshS3: Changing it to SAF_COMPLETED_RECORDS_FILE_NAME from SAF_ELIGIBLE_RECORDS_FILE_NAME
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering it as no records found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
			APP_TRACE(szDbgMsg);
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return SAF_RECORD_NOT_FOUND;//File would not have created if there are no records found
		}
	}

	debug_sprintf(szDbgMsg, "%s: Total Number SAF Record(s) [%d]", __FUNCTION__, giTotalSAFRecords);
	APP_TRACE(szDbgMsg);

	for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
	{
		debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iRecordLen = getRecordLength(Fh);

		debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
		APP_TRACE(szDbgMsg);

		if(iRecordLen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No more records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		debug_sprintf(szDbgMsg, "%s: PreviousRecLen [%d], RecLen[%d]", __FUNCTION__, iPreviousRecLen, iRecordLen);
		APP_TRACE(szDbgMsg);

		if(iPreviousRecLen < iRecordLen + 1)
		{
			//Allocate the memory to hold the SAF record
			// CID 68331 (#1 of 1): Out-of-bounds access (OVERRUN). Allocating enough memory, so that same can be accessed in fgets() and we do not overrun it also.
			pchTemp = (char *)realloc(pszSAFRecordData, iRecordLen + 10);
			if(pchTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			else
			{
				memset(pchTemp, 0x00, iRecordLen + 10);
				pszSAFRecordData = pchTemp;
			}

			iPreviousRecLen = iRecordLen + 10;
		}
		else
		{
			if(pszSAFRecordData != NULL) // CID 67439 (#1 of 1): Explicit null dereferenced (FORWARD_NULL)T_raghavendranR1
			{
				memset(pszSAFRecordData, 0x00, iPreviousRecLen);
			}
		}

		// CID 67439 (#1 of 1): Explicit null dereferenced (FORWARD_NULL)T_raghavendranR1
		if( (pszSAFRecordData != NULL) && (fgets(pszSAFRecordData, iRecordLen + 10, Fh) == NULL))//Added 10, wanted it to terminate by the newline character
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				/* Daivik :Coverity 67388 - 4/2/2016
				 * Removing this unecessary check , because if this path has been hit , then pszSAFRecordData cannot be NULL.
				 * Moreover we also have a check within free to check the pointer value before freeing.
				 */
				free(pszSAFRecordData);

				return FAILURE;
			}
		}

		if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iIndex = iIndex -1; //Time Stmap lines are not part of the Total number of records, need to read more
			continue;
		}

		debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iRetVal = getSAFRecordStatus(pszSAFRecordData);

		if(iRetVal != iSAFStatus)
		{
			debug_sprintf(szDbgMsg, "%s: SAF record status is not %s", __FUNCTION__, pstSAFDtls->szStatus);
			APP_TRACE(szDbgMsg);
			continue; //Need to go to the next record if it exists
		}

		debug_sprintf(szDbgMsg, "%s:  %d SAF record is %s", __FUNCTION__, iIndex, pstSAFDtls->szStatus);
		APP_TRACE(szDbgMsg);

		iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iSAFRecFound = 1;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

		}
	}

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_FOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_NOT_FOUND;
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	if(Fh != NULL)
	{
		fclose(Fh); //Closing the file
	}

	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: addNodeToSAFList
 *
 * Description	: This API updates adds the node at the end of the SAF list
 *
 * Input Params	: SAF_RECORD_LIST_P_TYPE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int addNodeToSAFList(SAFREC_PTYPE pstCurrentRecordNode, SAFDTLS_PTYPE pstSAFDtls)
{
	int			 iRetVal 			= SUCCESS;
	double 		 fTotalSAFAmount 	= 0.0;
	double		 fTotalSAFAmountSpecialCase = 0.0;
	int			 iSAFRecCnt      	= 0;
	SAFREC_PTYPE pstTempRecordNode	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCurrentRecordNode == NULL || pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input param is NULL...returning!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Before Updation : SAF Record Count [%s], SAF Total amount [%s]", __FUNCTION__, pstSAFDtls->szRecCnt, pstSAFDtls->szTotAmt);
	APP_TRACE(szDbgMsg);

	iSAFRecCnt = atoi(pstSAFDtls->szRecCnt) + 1;

	sprintf(pstSAFDtls->szRecCnt, "%d", iSAFRecCnt);

	fTotalSAFAmount = atof(pstSAFDtls->szTotAmt) + atof(pstCurrentRecordNode->szTranAmt);
	fTotalSAFAmountSpecialCase = atof(pstSAFDtls->szTotSpecialCaseAmt) + atof(pstCurrentRecordNode->szAmtSpecialcase);

	sprintf(pstSAFDtls->szTotAmt, "%.2lf", fTotalSAFAmount); //Updating the SAF amount
	sprintf(pstSAFDtls->szTotSpecialCaseAmt, "%.2lf", fTotalSAFAmountSpecialCase);

	debug_sprintf(szDbgMsg, "%s: After Updation : SAF Record Count [%s], SAF Total amount [%s]", __FUNCTION__, pstSAFDtls->szRecCnt, pstSAFDtls->szTotAmt);
	APP_TRACE(szDbgMsg);

	//Check for first insertion
	if(pstSAFDtls->recList == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No first node.... setting this node as the first node", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pstSAFDtls->recList = pstCurrentRecordNode;
	}
	else
	{
		pstTempRecordNode = pstSAFDtls->recList;
		while(1)
		{
			if(pstTempRecordNode->next == NULL) //Parse till the last node
			{
				pstTempRecordNode->next = pstCurrentRecordNode; //Adding
				debug_sprintf(szDbgMsg, "%s: Added node at the end", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			pstTempRecordNode = pstTempRecordNode->next;
		}
	}

	debug_sprintf(szDbgMsg, "%s - Returning with %d ", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getSAFErrorCodes
 *
 * Description	: This API parse the SAF error code file and fill the datastructure
 * 				  with SAF error codes to consider them as Host Not Available case
 *
 *
 * Input Params	: NONE
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getSAFErrorCodes()
{
	int	iAppLogEnabled			= isAppLogEnabled();
	int  iRetVal    			= SUCCESS;
	int	 iFileSize				= 0;
	char szAppLogData[300]		= "";
	char szAppLogDiag[300]		= "";
	FILE *fpSAFErrCodes			= NULL;
	char *pszLocSAFErrCodes    	= NULL;
	struct stat	fileStatus;

	#ifdef DEBUG
		char	szDbgMsg[5024]	= "";
	#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	gpszSafErrCodes = NULL;

	while(1)
	{
		iRetVal = stat(SAF_ERR_CODES_FILE, &fileStatus);

		if(iRetVal != 0)
		{
			if(errno == EACCES)
			{
				debug_sprintf(szDbgMsg, "%s: Not enough search permissions for %s", __FUNCTION__, SAF_ERR_CODES_FILE);
				APP_TRACE(szDbgMsg);
			}
			else if (errno == ENAMETOOLONG)
			{
				debug_sprintf(szDbgMsg, "%s: Name %s is too long", __FUNCTION__,SAF_ERR_CODES_FILE);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				/* For ENOENT/ENOTDIR/ELOOP */
				debug_sprintf(szDbgMsg,"%s: File - [%s] (Name not correct)/(file doesnt exist)", __FUNCTION__, SAF_ERR_CODES_FILE);
				APP_TRACE(szDbgMsg);
			}

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "SAF Error Codes file(SAF_ERR_CODES.DAT) Not Found, Considering SAF Enhanced Code Detection is Not Configured");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

			iRetVal = SUCCESS;
			break;
		}
		else
		{
			/* Check if the file is a regular file */
			if(S_ISREG(fileStatus.st_mode))
			{
				/* Check if the file is not empty */
				if(fileStatus.st_size <= 0)
				{
					debug_sprintf(szDbgMsg, "%s: File [%s] is empty", __FUNCTION__, SAF_ERR_CODES_FILE);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "SAF Error Codes file(SAF_ERR_CODES.DAT) is Empty, Considering SAF Enhanced Code Detection is Not Configured");
						addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
					}

					iRetVal = SUCCESS;
					break;
				}

				iFileSize = fileStatus.st_size;

				debug_sprintf(szDbgMsg, "%s: File Size of SAF_ERR_CODES.DAT is %d", __FUNCTION__, iFileSize);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "SAF Error Codes file(SAF_ERR_CODES.DAT) is Found, Parsing File For Configured SAF Error Codes");
					addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error: File [%s] is not a regular file", __FUNCTION__, SAF_ERR_CODES_FILE);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "SAF Error Codes file(SAF_ERR_CODES.DAT) is Not a Regular File, Considering SAF Enhanced Code Detection is Not Configured");
					addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
				}

				iRetVal = SUCCESS;
				break;
			}
		}

		iRetVal = AuthenticateSAFErrCodesFile();

		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Either SAF_ERR_CODES.DAT file not present or authentication of this file fails, Considering as success and proceeding", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Authentication of SAF Error Codes file(SAF_ERR_CODES.DAT) Failed, Considering SAF Enhanced Code Detection is Not Configured");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

			iRetVal = SUCCESS;
			break;
		}

		fpSAFErrCodes = fopen(SAF_ERR_CODES_FILE,"r");
		if(!fpSAFErrCodes) //It should not enter this conidtion ideally since we are chekcing presence of this file above itself
		{
			debug_sprintf(szDbgMsg, "%s: SAF Error Codes file(SAF_ERR_CODES.DAT) not found", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = SUCCESS;
			break;
		}

		//Allocating buffer to hold the content of the file i.e. SAF Error Codes
		pszLocSAFErrCodes = (char *)malloc((iFileSize + 2) * sizeof(char));
		if(pszLocSAFErrCodes == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error While Allocating Memory to hold SAF Error Codes", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error While Allocating Memory, Considering SAF Enhanced Code Detection is Not Configured");
				addAppEventLog(SCA, PAAS_ERROR, START_UP, szAppLogData, NULL);
			}

			fclose(fpSAFErrCodes);

			iRetVal = SUCCESS;
			break;
		}
		memset(pszLocSAFErrCodes, 0x00, iFileSize + 2);

		while(!feof(fpSAFErrCodes))
		{
			memset(pszLocSAFErrCodes, 0x00, iFileSize + 2);

			if(fgets(pszLocSAFErrCodes,iFileSize+1,fpSAFErrCodes)==NULL)
			{
				break;
			}

			if(strlen(pszLocSAFErrCodes) < 5000)
			{
				debug_sprintf(szDbgMsg, "%s: Line Read [%s]",__FUNCTION__, pszLocSAFErrCodes);
				APP_TRACE(szDbgMsg);
			}

			/*if the first character is new line or # -> for comment, then ignore it and continue*/
			if ( pszLocSAFErrCodes[0] == '\n' || pszLocSAFErrCodes[0] == '#' || pszLocSAFErrCodes[0] == '\r')
			{
				while(pszLocSAFErrCodes[strlen(pszLocSAFErrCodes)-1] != '\n')
				{
					memset(pszLocSAFErrCodes, 0x00, iFileSize + 2);
					if(fgets(pszLocSAFErrCodes,iFileSize+1,fpSAFErrCodes)==NULL)
					{
						break;
					}
					if(strlen(pszLocSAFErrCodes) < 5000)
					{
						debug_sprintf(szDbgMsg, "%s: Line Read [%s]",__FUNCTION__, pszLocSAFErrCodes);
						APP_TRACE(szDbgMsg);
					}
				}
				continue;
			}

			if (pszLocSAFErrCodes[strlen(pszLocSAFErrCodes)-1] == '\n')
			{
				pszLocSAFErrCodes[strlen(pszLocSAFErrCodes)-1] = 0x00;/*eliminate the last new line character character*/
			}
			if (pszLocSAFErrCodes[strlen(pszLocSAFErrCodes)-1] == '\r')
			{
				pszLocSAFErrCodes[strlen(pszLocSAFErrCodes)-1] = 0x00;/*eliminate the last new line character character*/
			}

			/* Check for characters */
			if(strspn(pszLocSAFErrCodes, "1234567890|-") == strlen(pszLocSAFErrCodes))
			{
				if(strlen(pszLocSAFErrCodes) < 5000)
				{
					debug_sprintf(szDbgMsg, "%s: SAF Error Code Buffer [%s]",__FUNCTION__, pszLocSAFErrCodes);
					APP_TRACE(szDbgMsg);
				}

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "SAF_ERR_CODES.DAT file is Parsed Successfully, Considering SAF Enhanced Code Detection is Configured");
					addAppEventLog(SCA, PAAS_SUCCESS, START_UP, szAppLogData, NULL);
				}

				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Characters other than 0-9, | found, SAF Error Code",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				memset(pszLocSAFErrCodes, 0x00, iFileSize + 2);

				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Invalid Format(Characters Other Than 0-9, | Found), Considering SAF Enhanced Code Detection is Not Configured");
					addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
				}
			}

		}

		fclose(fpSAFErrCodes);

		break;

	}

	gpszSafErrCodes = NULL;

	if(pszLocSAFErrCodes != NULL) //Check if it really contains the data or empty one
	{
		if(strlen(pszLocSAFErrCodes) > 0) //It contains SAF Error Code line
		{
			gpszSafErrCodes = (char *)malloc((strlen(pszLocSAFErrCodes) + 1) * sizeof(char));
			if(gpszSafErrCodes == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while allocating memory for gpszSafErrCodes, pointing gpszSafErrCodes to pszLocSAFErrCodes", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				gpszSafErrCodes = pszLocSAFErrCodes;
			}
			else
			{
				memset(gpszSafErrCodes, 0x00, (strlen(pszLocSAFErrCodes) + 1));
				memcpy(gpszSafErrCodes, pszLocSAFErrCodes, strlen(pszLocSAFErrCodes));
				free(pszLocSAFErrCodes);
				pszLocSAFErrCodes = NULL;
			}

			if(strlen(gpszSafErrCodes) < 5000)
			{
				debug_sprintf(szDbgMsg, "%s: SAF Error Codes Final Stored Buffer [%s]",__FUNCTION__, gpszSafErrCodes);
				APP_TRACE(szDbgMsg);
			}

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Configured SAF Error Codes From SAF_ERR_CODES.DAT File Are");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);

				addAppEventLog(SCA, PAAS_INFO, START_UP, gpszSafErrCodes, NULL);
			}
		}
		else //Must be file without SAF Error code line
		{
			free(pszLocSAFErrCodes);
			pszLocSAFErrCodes = NULL;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: AuthenticateSAFErrCodesFile
 *
 * Description	: This API Validate the SAF Error Codes File and returns the status
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int AuthenticateSAFErrCodesFile()
{
	int 	rv 					= SUCCESS;
	int	iAppLogEnabled			= isAppLogEnabled();
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	if( (doesFileExist(SAF_ERR_CODES_FILE".P7S") == SUCCESS) || (doesFileExist(SAF_ERR_CODES_FILE".p7s") == SUCCESS) )
    {
        debug_sprintf(szDbgMsg, "%s - %s and its signature file found", __FUNCTION__, SAF_ERR_CODES_FILE);
        APP_TRACE(szDbgMsg); // SAF_ERR_CODES.DAT found in home directory

        if (authFile(SAF_ERR_CODES_FILE) == 1)
        {
            debug_sprintf(szDbgMsg, "%s - SAF Error Codes File(SAF_ERR_CODES.DAT) is authenticated", __FUNCTION__);
            APP_TRACE(szDbgMsg); // SAF_ERR_CODES.DAT found in home directory is authenticated

            if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Authentication of SAF Error Codes File(SAF_ERR_CODES.DAT) Succeeded");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

            rv = SUCCESS;
        }
        else
        {
        	 debug_sprintf(szDbgMsg, "%s - SAF Error Codes File(SAF_ERR_CODES.DAT) is not authenticated", __FUNCTION__);
        	 APP_TRACE(szDbgMsg); // BIT.DAT found in home directory is authenticated

        	if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Authentication of SAF Error Codes File(SAF_ERR_CODES.DAT) Failed. ");
				strcpy(szAppLogDiag, "Please Sign and Download SAF Error Codes File");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, szAppLogDiag);
			}


        	rv = FAILURE;
        }
    }
    else
    {
    	debug_sprintf(szDbgMsg, "%s - SAF Error Codes File(SAF_ERR_CODES.DAT.p7s) Signature file is not Found", __FUNCTION__);
    	APP_TRACE(szDbgMsg); // SAF_ERR_CODES.DAT found in home directory is not found/authenticated

    	if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "SAF Error Codes File (SAF_ERR_CODES.DAT.p7s) Signature File is Missing. ");
			strcpy(szAppLogDiag, "Please Download SAF Error Codes File and Its Signature File");
			addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, szAppLogDiag);
		}

    	rv = FAILURE;
    }

    debug_sprintf(szDbgMsg, "%s - returning %d", __FUNCTION__, rv);
    APP_TRACE(szDbgMsg);

    return rv;
}

/*
 * ============================================================================
 * Function Name: getCorrectedReceiptText
 *
 * Description	: This API looks for <CR><LF> or <LF> in the receipt text and removes the same if present. This is done to ensure the while storing the receipt text in completed records,
 * we do not store with <LF> as it will have some undefined behaviour
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
void getCorrectedReceiptText(char * szReceiptText)
{
	char * 		pszCurrPtr = NULL;
	char 		szTempBuff[4096] = "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pszCurrPtr = szReceiptText;
	while(pszCurrPtr != NULL && *pszCurrPtr != '\0')
	{
		if( (*pszCurrPtr == CR) || (*pszCurrPtr == LF) )
		{
			memset(szTempBuff, 0x00, sizeof(szTempBuff));
			strcpy(szTempBuff, pszCurrPtr + 1);
			strcpy(pszCurrPtr, szTempBuff);
			continue;
		}

		pszCurrPtr++;
	}

	debug_sprintf(szDbgMsg, "%s: --- returning ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return ;
}

/*
 * ============================================================================
 * Function Name: writeTimeStamptoSAFFile
 *
 * Description	: This API writes the current date to the SAF completed records 
 * 					file
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int writeTimeStamptoSAFFile(int iCheckFlag)
{
	int  iRetVal       		 	= SUCCESS;
	int	 iRecordLength			= 0;
	FILE *Fh	       		 	= NULL;
	char szCurrentTimeStamp[20] = "";
	char szTimeStampLine[50]    = "";

	#ifdef DEBUG
		char	szDbgMsg[256]	= "";
	#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Check Flag is %d", __FUNCTION__, iCheckFlag);
	APP_TRACE(szDbgMsg);

	iRetVal = getCurrentTimeStamp(szCurrentTimeStamp);

	if(iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while receiving the current timestamp", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	if(iCheckFlag)
	{
		if(strcmp(szCurrentTimeStamp, gszLastTimeStamp) == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Already current Timestamp[%s] present in the SAF completed records file", __FUNCTION__, gszLastTimeStamp);
			APP_TRACE(szDbgMsg);
			return SUCCESS;
		}
	}

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "a+"); //Open file for output at the end of a file. Output operations always write data at the end of the file, expanding it. Repositioning operations (fseek, fsetpos, rewind) are ignored. The file is created if it does not exist
	if(Fh==NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
		APP_TRACE(szDbgMsg);
		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
		return FAILURE;
	}

	if(chmod(SAF_COMPLETED_RECORDS_FILE_NAME, 0666) == -1)
	{
		debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	debug_sprintf(szDbgMsg, "%s: TimeStamp is %s", __FUNCTION__, szCurrentTimeStamp);
	APP_TRACE(szDbgMsg);

	sprintf(szTimeStampLine, "%d%cTimeStamp=%s%c", 21, PIPE, szCurrentTimeStamp, LF); //TimeStamp=dd-mm-yyyy<LF>

	debug_sprintf(szDbgMsg, "%s: TimeStamp line is %s", __FUNCTION__, szTimeStampLine);
	APP_TRACE(szDbgMsg);

	iRecordLength = strlen(szTimeStampLine);

	iRetVal = fwrite(szTimeStampLine, sizeof(char), iRecordLength, Fh);

	if(iRetVal == iRecordLength)
	{
		debug_sprintf(szDbgMsg, "%s: Successfully written Time Stamp to the file ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SUCCESS;

		//Updating the Time stamp
		memset(gszLastTimeStamp, 0x00, sizeof(gszLastTimeStamp));
		strcpy(gszLastTimeStamp, szCurrentTimeStamp);

		iRetVal = updateSAFDataToFile(); //Updating the last time stamp
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Updated the SAF Details file after updating Last Time stamp", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = SUCCESS;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			//Praveen_P1: what to do here!!!!
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Failure while writing Time Stamp to the file, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLength, iRetVal);
		APP_TRACE(szDbgMsg);
		iRetVal = FAILURE;
	}

	if(Fh != NULL)
	{
		fclose(Fh);
	}

	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	debug_sprintf(szDbgMsg, "%s: Returning with [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getTransactionAmount
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static double getTransactionAmount(char *pszRecordData)
{
	int 	iCmdType		  	= -1;
	double 	fTransactionAmount  = 0.0;
	char	*pchFirst 		 	= NULL;
	char	*pchNext          	= NULL;
	char  	szTranAmount[20]    = "";
	char 	szCmdType[21]	  	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	/*-------------- Crossing SAF Status Field -----------------*/
	pchNext = strchr(pszRecordData, PIPE);
	pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record

	/*-------------- Crossing SAF Record Number -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1; //pchFirst points to start of the Auth

	/*-------------- Crossing SAF Record AUTH field -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1; //pchFirst points to start of the SAF Account Number field

	/*-------------- Crossing SAF Record Account Number -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Crossing SAF Record Track2 data -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Crossing SAF Record Expiry Month -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Crossing SAF Record Expiry Year -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Crossing SAF Record Payment Media -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record Card Source -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record Transaction Amount -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record Transaction amount -----------------*/
	if((*pchFirst) != PIPE) //Record contains Transaction amount field
	{
		debug_sprintf(szDbgMsg, "%s: Record contains Transaction amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return 0;
		}
		memset(szTranAmount, 0x00, sizeof(szTranAmount));
		strncpy(szTranAmount, pchFirst, (pchNext - pchFirst));
		debug_sprintf(szDbgMsg, "%s: Transaction Amount is %s", __FUNCTION__, szTranAmount);
		APP_TRACE(szDbgMsg);

		fTransactionAmount = atof(szTranAmount);
		pchFirst = pchNext + 1; //pchFirst points to start of the SAF Record Tip Amount field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain Transaction amount field", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		fTransactionAmount = 0;
		pchFirst = pchFirst + 1; //pchFirst points to start of the SAF Record Tip Amount field
	}

	/*-------------- Getting SAF Record TIP amount -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record TAX amount -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record Payment Type -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record Invoice Number -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record Purchase ID -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record Cashier ID -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record Sign Image Type -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record Sign Data -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record PWC Result Code -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record PWC CTROUTD -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record PWC TROUTD -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/* Format of the SAF Record
	 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
	 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
	 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
	 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
	 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
	 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT||||;
	 *
	 */

	/*-------------- Getting SAF Record PWC AUTH CODE -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Crossing SAF Record Encryption type field -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Crossing SAF Record Encryption payload field -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record CARD TOKEN -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record CVV2 Code -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record PWC BANK USER DATA -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record Business date -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Crossing SAF Record PINless debit -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting Transaction POS Entry Mode------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;
	/*-------------- Getting SAF Record Merchant ID -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record Terminal ID -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record LANE ID -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record APPROVED_AMOUNT -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;
	/*-------------- Crossing SAF Record Tax Indicator field -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;

	/*-------------- Getting SAF Record Command Type -----------------*/
	if((*pchFirst) != PIPE) //Record contains command type
	{
		debug_sprintf(szDbgMsg, "%s: Record contains command type field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return FAILURE;
		}
		memset(szCmdType , 0x00, sizeof(szCmdType));
		strncpy(szCmdType, pchFirst, (pchNext - pchFirst));

		iCmdType = atoi(szCmdType);
		if(iCmdType == SSI_ACTIVATE || (iCmdType == SSI_CREDIT && (isFloorLimitChkAllowedForSAFRefund() == PAAS_FALSE)) || iCmdType == SSI_PAYACCOUNT)
		{
			debug_sprintf(szDbgMsg, "%s: For Gift Activate or Refund with Trans Floor Limit Check Disabled, we dont take the amount into consideration, So reset/memset it", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			fTransactionAmount = 0;
		}

		pchFirst = pchNext + 1; //pchFirst points to start of the track indicator record field
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Record does not contain command type field, Filling with default command as SALE", __FUNCTION__);
		APP_TRACE(szDbgMsg);


		pchFirst = pchFirst + 1; //pchFirst points to start of the  track indicator SAF record field
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%lf]", __FUNCTION__, fTransactionAmount);
	APP_TRACE(szDbgMsg);

	/*-------------- Crossing SAF Record Track Indicator field -----------------*/
	pchNext = strchr(pchFirst, PIPE);
	pchFirst = pchNext + 1;


	return fTransactionAmount;

}

/*
 * ============================================================================
 * Function Name: wasDuplicateTran
 *
 * Description	: This API is supposed to check for the duplicate transcation
 * 					fields in the parsed SSI response, to check if the
 * 					transaction was declined on the basis of being a duplicate
 * 					check. 
 *
 * Input Params	: Transaction Key
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
static PAAS_BOOL wasDuplicateTran(char * pszTranStkKey)
{
	int				rv				= SUCCESS;
	PAAS_BOOL		bRetVal			= PAAS_FALSE;
	DUPFIELDDTLS_STYPE stDupFieldDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&stDupFieldDtls, 0x00, sizeof(DUPFIELDDTLS_STYPE));

	rv = getDupFieldDtlsForPymtTran(pszTranStkKey, &stDupFieldDtls);
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while getting the Duplicate fields", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return bRetVal;
	}

	if(strlen(stDupFieldDtls.szDupAccNum)    > 0 ||
	   strlen(stDupFieldDtls.szDupAuthCode)  > 0 ||
	   strlen(stDupFieldDtls.szDupCTroutd)   > 0 ||
	   strlen(stDupFieldDtls.szDupPymtMedia) > 0 ||
	   strlen(stDupFieldDtls.szDupTroutd)    > 0
	   )
	{
		debug_sprintf(szDbgMsg, "%s: Duplicate transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		bRetVal = PAAS_TRUE;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: NON-Duplicate transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, bRetVal);
	APP_TRACE(szDbgMsg);

	return bRetVal;
}

/*
 * ============================================================================
 * Function Name: SAFPurgeRecords
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void * SAFPurgeRecords(void * arg)
{
	int		  iRetVal     		   	= SUCCESS;
	int		  iVal					= 0;
	int		  iRemainingTimeInSecs 	= 0;
	int		  iIndex 				= 0;
	int		  iNextDuration 		= 0;
	int		  iFirstTime 			= 1;
	int		  iWaitingTime 			= 0;

#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: SAF Purging thread begins execution", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Add signal handler for SIGSAFTIME SETTIME */
	if(SIG_ERR == signal(SIGSAFTIME, safTimeSigHandler))
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add signal handler for SIGSAFTIME",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	iVal = sigsetjmp(safJmpBuf, 1);
	if(iVal == 1)
	{
		debug_sprintf(szDbgMsg, "%s: Change in Time. Resetting SAF purging timer",
								__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	iRetVal = getRemainingTimeofDayinSecs(&iRemainingTimeInSecs);

	if(iRetVal == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Calculated the remaining time of the day, %d", __FUNCTION__, iRemainingTimeInSecs);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Error while calculating the remaining time of the day!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		//Praveen_P1: What to do here!!!
	}

	//iRemainingTimeInSecs = 1000; //Praveen_P1: Purely for the testing purpose...please remove if u see this line

	iNextDuration = (24 * 60 * 60) + 1000; //Added 1000 secs more just to make that date changes

	while(1)
	{
		//Add the TimeStamp to the file
		iRetVal = writeTimeStamptoSAFFile(1);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Written TimeStamp to the SAF completed record file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while writing timestamp to the SAF completed record File", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		//Its time to Purge the SAF records
		iRetVal = purgeSAFRecords();

		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Successfully Purged the SAF records if any", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while purging SAF records!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(iFirstTime)
		{
			iWaitingTime = iRemainingTimeInSecs/50;
		}
		else
		{
			iWaitingTime = iNextDuration/50;
		}

		for(iIndex = 0; iIndex <= 50; iIndex++)
		{
			debug_sprintf(szDbgMsg, "%s: Index = %d, Waiting", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);
			svcWait(iWaitingTime*1000);
		}

		debug_sprintf(szDbgMsg, "%s: Waiting time over, need to do SAF purging", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		iFirstTime = 0; //After the first time, need to check after 24 hours
	}

	debug_sprintf(szDbgMsg, "%s: SAF Purging thread existing!!!", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: SAFProcessor
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void * SAFProcessor(void * arg)
{
	PAAS_BOOL 		iRecPresent 		 	= PAAS_TRUE;
	PAAS_BOOL 		bIsHostWentOffline 		= PAAS_TRUE;
	int		 		iRetVal     		 	= SUCCESS;
//	int		 	 	iOldSAFRecords 	 	 	= 0;
//	int 	 	 	iCurrentSAFRecords 	 	= 0;
	int				iSAFPostInterval	 	= 0;
	int				iAppLogEnabled			= isAppLogEnabled();
	long 			lSAFPostIntrvlMilliSec	= 0;
	char			szAppLogData[300]		= "";

#ifdef DEBUG
	char	szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: SAF thread begins execution", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	iSAFPostInterval = getSAFPostInterval();
	lSAFPostIntrvlMilliSec = iSAFPostInterval * 1000;

	debug_sprintf(szDbgMsg, "%s: SAF Post interval in MilliSec is %ld", __FUNCTION__, lSAFPostIntrvlMilliSec);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iRetVal = SUCCESS;

		/*-------Step1: Check whether SAF record is available-------- */
		iRecPresent = isSAFRecordPresent();
		if(iRecPresent == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: SAF Record is present to post it to SSI", __FUNCTION__);
			APP_TRACE(szDbgMsg);


			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, SAF_START_INDICATOR);
				addAppEventLog(SCA, PAAS_INFO, START, szAppLogData, NULL);
				strcpy(szAppLogData, "SAF Records Present to Post it to SSI");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			//AjayS2: Not posting Any SAF records if Demo Mode is Enabled
			if(isDemoModeEnabled())
			{
				debug_sprintf(szDbgMsg, "%s: Demo Mode is Enabled, NOT Posting Any SAF Records", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				svcWait(lSAFPostIntrvlMilliSec);
				continue;
			}

			/* ArjunU1: If VSP is enabled and VSP registration is failed previously or while doing transaction.
			 * We need to wait for vsp registrartion issue to resolve completely before trying to post existing SAF records to host.
			 */
			if( PAAS_TRUE == isVSPEnabledInDevice())
			{
				if( PAAS_TRUE == isVSPRegReqdAgain())
				{
					debug_sprintf(szDbgMsg, "%s: Waiting for VSP re-registration to complete!, Got some SAF records to post", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Waiting for VSP Re-Registration to Complete to Post SAF Records to SSI");
						addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
						strcpy(szAppLogData, SAF_END_INDICATOR);
						addAppEventLog(SCA, PAAS_INFO, COMMAND_END, szAppLogData, NULL);
					}

					svcWait(lSAFPostIntrvlMilliSec);
					continue;
				}
			}

			/*
			 * Praveen_P1: We are introducing new config variable whether check connection to host
			 * is required before posting the SAf transaction.
			 * if this variable is set then we will check the connection status otherwise
			 * we will post the SAf tran directly
			 *
			 */
			if(isCheckConnReqdForSAFTran() == PAAS_TRUE)
			{
				debug_sprintf(szDbgMsg, "%s: Check Connection is required for SAF Transaction, so checking for the connection status before posting the SAF request", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/*-------Step2: Check whether connection is available with the PWC-------- */
				iRetVal = waitForHostConnection(&bIsHostWentOffline, 1); //29 Sep 2016: Praveen_P1: Passing additional parameter to print differnt app log statement
				if(iRetVal == FAILURE)
				{
					debug_sprintf(szDbgMsg, "%s: Error, while waiting for PWC!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					svcWait(lSAFPostIntrvlMilliSec);
					continue;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Check Connection is NOT required for SAF Transaction, not checking the connection status before posting the SAF request", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			/*
			 * Praveen_P1: We are keeping track of HOST status to determine to post
			 * any request. if the status is offline, we are not trying to post the
			 * request to the host. so setting to TRUE here since the host connectivity
			 * came up,
			 */
			if(bIsHostWentOffline == PAAS_TRUE) //Host Came from Offline to Online
			{
				debug_sprintf(szDbgMsg, "%s: Host Status changed from offline to online", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				updateHostConnectionStatus(PAAS_TRUE); //Updating the host status
			}

			if(PAAS_FALSE == isSAFRecordsAllowedToProcNow(&bIsHostWentOffline))
			{
//				debug_sprintf(szDbgMsg, "%s: Waiting for Online Transaction to complete!, Got some SAF records to post", __FUNCTION__);
//				APP_TRACE(szDbgMsg);
//
//				if(iAppLogEnabled == 1)
//				{
//					strcpy(szAppLogData, "Waiting for Online Transaction to Complete, to Post SAF Records to SSI");
//					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
//				}
				svcWait(lSAFPostIntrvlMilliSec);
				continue;
			}

			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: SSI Host is up, need to post the SAF record", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = resetSAFLastOfflineEpochTime();
				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully reset the last SAF offline Epcoh time", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while resetting the last SAF offline Epcoh time", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

				/* T_MukeshS3: while posting the SAF records to HOST, application must not allow VHQ Agent to Reboot/Restart the Device.
				 * So, setting this SAFRecordsInProgress Flag here to TRUE. same thread will reset this Flag, once
				 * it successfully POST the records and copy the completed records in SAFCompleteRecords.db file or it is unable to
				 * post records to Host.
				 */
				setCriticalStepInProgressFlag(PAAS_TRUE);

				/*-------Step3: Post the SAF record to the PWC-------- */
				iRetVal = postSAFRecordToHost();

				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully Posted the SAF record to SSI Host", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Successfully Posted the SAF Record to SSI Host");
						addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
						strcpy(szAppLogData, SAF_END_INDICATOR);
						addAppEventLog(SCA, PAAS_INFO, COMMAND_END, szAppLogData, NULL);
					}

					/*-----------Delete the SAF SSI Tran Key ---------- */
					iRetVal = popSSITran(szSSITranKey);
					if(iRetVal != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s:Error while popping the SAF SSI transaction from stack", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					deleteStack(szSSITranKey);

					memset(szSSITranKey, 0x00, sizeof(szSSITranKey));
					/* T_MukeshS3: Once it has successfully posted the current SAF record to Host and copied the completed record into SAFCompleteRecords.db file
					 * changing the 'Critical Step In Progress' status to FALSE for VHQ to allow Reboot/Restart of Device.
					 */
					setCriticalStepInProgressFlag(PAAS_FALSE);

					/*-----------Update the SAF data system with the next ELIGIBLE SAF record ---------- */
					iRetVal = readQueuedSAFRecord();
					if(iRetVal == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Successfully updated the SAF data system with the next SAF record with ELIGIBLE/PENDING status",__FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Failure while updating the SAF data system with the next SAF record with ELIGIBLE or PENDING status",__FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error, while posting the SAF record to PWC", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Error While Posting the SAF Record to SSI Host");
						addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
						strcpy(szAppLogData, SAF_END_INDICATOR);
						addAppEventLog(SCA, PAAS_INFO, COMMAND_END, szAppLogData, NULL);
					}
					// resetting this Flag here, as it is failed to post records to host.
					setCriticalStepInProgressFlag(PAAS_FALSE);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Invalid return value!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

			}
			acquireMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");
			iSAFInProgressStatus = SAF_IN_IDLE;
			releaseMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");
			svcWait(lSAFPostIntrvlMilliSec);
		}
		else
		{
#if 0
			iCurrentSAFRecords = getSAFTotalRecords();
			if(iCurrentSAFRecords != iOldSAFRecords)
			{
				debug_sprintf(szDbgMsg, "%s: SAF records got added, reading/checking the SAF record with ELIGIBLE status if it is present", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = readQueuedSAFRecord();
				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully Parsed the SAF records for SAF record with ELIGIBLE status", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iOldSAFRecords = iCurrentSAFRecords;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading SAF record with ELIGIBLE status to the SAF data system", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
#endif

			/*AjayS2: 22 Sep 2016
			 * Removing Above Code and getting now the size of SAFRecords.db file to get see whether any SAF records is there or Not, to Post to Host
			 */
			if(isSAFRecordsPendingInDevice() > 0)
			{
				debug_sprintf(szDbgMsg, "%s: SAF records are Present, reading/checking the SAF record with ELIGIBLE/PENDING status if it is present", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = readQueuedSAFRecord();
				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Successfully Parsed the SAF records for SAF record with ELIGIBLE status", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while reading SAF record with ELIGIBLE status to the SAF data system", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			svcWait(lSAFPostIntrvlMilliSec);
		}
	}

	debug_sprintf(szDbgMsg, "%s: SAF thread existing!!!", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: getRemainingTimeofDayinSecs
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int createSAFTranStack()
{
	int		iRetVal		= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFTranKeyMutex, "SAF Record File");
	while(1)
	{
		if(strlen(szSSITranKey) > 0)
		{
			debug_sprintf(szDbgMsg, "%s:Key is present already, need to delete it", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//Releasing the mutex here so that it can acquired while deleting the ley
			releaseMutexLock(&gptSAFTranKeyMutex, "SAF Record File");

			iRetVal = deleteStack(szSSITranKey);
			if(iRetVal != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s:Error while deleting the SAF stack Key", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return FAILURE;
			}
			//Acquiring once again
			acquireMutexLock(&gptSAFTranKeyMutex, "SAF Record File");
		}

		memset(szSSITranKey, 0x00, sizeof(szSSITranKey));

		iRetVal = createStack(szSSITranKey);

		if(iRetVal != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s:Error while creating the new SAF SSI Transaction Key", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
			break;
		}

		break;
	}

	releaseMutexLock(&gptSAFTranKeyMutex, "SAF Record File");

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getRemainingTimeofDayinSecs
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getRemainingTimeofDayinSecs(int *piRemainingTime)
{
	int		iRetVal		= SUCCESS;
	time_t 	tTime;
	struct 	tm stTime;
	int 	iCurrentTimeinSecs  = 0;
	static int 	iMidNighttimeinSecs = 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iMidNighttimeinSecs = 24 * 60 * 60;

	tTime = time(NULL);
	memcpy(&stTime, localtime(&tTime), sizeof(stTime));

	iCurrentTimeinSecs = (stTime.tm_hour * 60 * 60) + (stTime.tm_min * 60) + stTime.tm_sec;

	*piRemainingTime = iMidNighttimeinSecs - iCurrentTimeinSecs;

	debug_sprintf(szDbgMsg, "%s: MidNightTime[%d] - CurrentTime[%d] = RemainingTime[%d]", __FUNCTION__, iMidNighttimeinSecs, iCurrentTimeinSecs, *piRemainingTime);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getCurrentTimeStamp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getCurrentTimeStamp(char *pszTimeStamp)
{
	int    iRetVal			= SUCCESS;
	time_t tTime;
	struct tm stTime;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	tTime = time(NULL);
	memcpy(&stTime, localtime(&tTime), sizeof(stTime));

    sprintf(pszTimeStamp, "%02d-%02d-%04d", stTime.tm_mday, stTime.tm_mon+1, stTime.tm_year+1900);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getCurrentTimeStamp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getDiffbetweenDates(char *pszInTimeStamp)
{
	int    iRetVal				  = SUCCESS;
	char   szCurrentTimeStamp[20] = "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iRetVal = getCurrentTimeStamp(szCurrentTimeStamp);

	if(iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while receiving the current timestamp", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Current Timestamp is %s", __FUNCTION__, szCurrentTimeStamp);
	APP_TRACE(szDbgMsg);

	iRetVal = calcDiffbetweenDates(pszInTimeStamp, szCurrentTimeStamp);

	if(iRetVal == FAILURE)
	{
		debug_sprintf(szDbgMsg, "%s: Error while calculating difference between given two dates", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Number of Days(difference) is %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: waitForHostConnection
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int waitForHostConnection(PAAS_BOOL *bHostWentOffline, int iCalledReason)
{
	int 		iRetVal 	          	= SUCCESS;
	static int  iSAFInterval          	= 0;
	static long lSAFIntervalMillisecs	= 0;
	int			iAppLogEnabled			= isAppLogEnabled();
	char		szAppLogData[300]		= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	if(iSAFInterval == 0)
	{
		iSAFInterval = getSAFPingInterval();
		debug_sprintf(szDbgMsg, "%s: SAF Host connection check Interval is %d", __FUNCTION__, iSAFInterval);
		APP_TRACE(szDbgMsg);
		lSAFIntervalMillisecs = iSAFInterval * 60 * 1000;
	}

	debug_sprintf(szDbgMsg, "%s: SAF Host connection check waiting time is %ld in millisecs", __FUNCTION__, lSAFIntervalMillisecs);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(iAppLogEnabled == 1)
		{
			if(iCalledReason == 1) //SAF processor calling this function
			{
				strcpy(szAppLogData, "Checking for Host Connection to Post SAF Records");
			}
			else if(iCalledReason == 2) //Checkhostconnectionstatus thread calling this function
			{
				strcpy(szAppLogData, "Checking for Host Connection to Attempt to Post Online Transactions");
			}
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		if(isHostConnected() == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: Host connection is available",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				if(iCalledReason == 1) //SAF processor calling this function
				{
					strcpy(szAppLogData, "Host Connection is Available to Post the SAF Records");
				}
				else if(iCalledReason == 2) //Checkhostconnectionstatus thread calling this function
				{
					strcpy(szAppLogData, "Host Connection is Available to Attempt to Post Online Transactions");
				}
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			break;
		}
		debug_sprintf(szDbgMsg, "%s: Host connection is not available, checking once more...",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Host Connection is Not Available, Waiting for SAF Ping Interval Time");
			addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
		}

		//Set veriable to indicate host went offline.
		*bHostWentOffline = PAAS_TRUE;

		svcWait(lSAFIntervalMillisecs); //Will check the connection part after the given interval of time
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: isSAFRecordsAllowedToProcNow
 *
 * Description	: Function would check whether both offline and online transaction can be processed simultaneously, if throttling is
 * 				  enabled it will wait for throttling interval time.
 *
 * Input Params	:
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */
static PAAS_BOOL isSAFRecordsAllowedToProcNow(PAAS_BOOL *bHostWentOffline)
{
	int						iAppLogEnabled					= isAppLogEnabled();
	PAAS_BOOL				bRetVal 	  			        = PAAS_TRUE;
	static PAAS_BOOL		bFirstTime						= PAAS_TRUE;
	static int  			iSAFThrottlingInterval          = 0;
	static long  			lSAFThrottlingInterval          = 0;
	static PAAS_BOOL		bAllowSAFTranWithOnlineTran		= PAAS_FALSE;
	static PAAS_BOOL		bSAFThrottlingEnabled			= PAAS_FALSE;
	PAAS_BOOL				bSessPresent					= PAAS_FALSE;
	char 					szDevSrlNum[12]					= "";
	char					szAppLogData[300]				= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	if(bFirstTime == PAAS_TRUE)
	{
		bSAFThrottlingEnabled = isSAFThrottlingEnabled();
		if( bSAFThrottlingEnabled == PAAS_TRUE )
		{
			debug_sprintf(szDbgMsg, "%s: SAF Throttling Enabled, Need to calculate SAF actual Throttling Interval", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iSAFThrottlingInterval = getSAFThrottlingInterval();

			/*ArjunU1: Calculate the actual throttling interval by finding modulus of device serial number using throttling interval config
						parameter value as denominator.
			*/

			//TODO: Later use the efficient method to calculate throttling interval(rand function or any other method).
			/* ------------ Get the device serial number ----------- */
			memset(szDevSrlNum, 0x00, sizeof(szDevSrlNum));
			getDevSrlNum(szDevSrlNum);

			lSAFThrottlingInterval = atoll(szDevSrlNum) % iSAFThrottlingInterval;

			debug_sprintf(szDbgMsg, "%s: SAF Throttling Interval waiting time is %ld in secs", __FUNCTION__, lSAFThrottlingInterval);
			APP_TRACE(szDbgMsg);
		}
		bAllowSAFTranWithOnlineTran	= bProcSAFTranWithOnlineTran();

		bFirstTime = PAAS_FALSE;
	}

	while(1)
	{
		if( bAllowSAFTranWithOnlineTran == PAAS_FALSE) //we should not allow SAF transactions to go with online, thats why we need to check whether session is open
		{
			debug_sprintf(szDbgMsg, "%s: Offline transaction are NOT allowed to process when online tran are being processed",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Need to check whether session is opened or not",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			//Check whether application is performing any online transactions.
			bSessPresent = isSessionInProgress();
			if(bSessPresent == PAAS_TRUE)
			{
				debug_sprintf(szDbgMsg, "%s: Session is in progress, Can't process SAF transactions for now", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Offline Transaction are NOT Allowed to Process When Online Tran are Being Processed, Session is In Progress");
					addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
				}
				bRetVal = PAAS_FALSE;
				break;
			}

			bRetVal = PAAS_TRUE;
		}

		if( bSAFThrottlingEnabled == PAAS_TRUE && *bHostWentOffline == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg, "%s: SAF Throttling Enabled, Going to wait for throttling interval [%ld]sec", __FUNCTION__,
																										lSAFThrottlingInterval);
			APP_TRACE(szDbgMsg);

			svcWait(lSAFThrottlingInterval * 1000); //Need to wait for those many secs

			bRetVal = PAAS_TRUE;

			*bHostWentOffline = PAAS_FALSE;

			break;
		}
		break;
	}

	/* MukeshS3: 28-Sept-16:
	 * Process SAF records only when it is present in Stack, there might be possibility that it was removed from
	 * SAF removal thread
	 */
	if(bRetVal)
	{
		acquireMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");
		if(isSAFRecordPresent())
		{
			iSAFInProgressStatus = SAF_IN_FLIGHT;
			debug_sprintf(szDbgMsg, "%s: Posting SAF IN_PROCESS record to Host, Changing status to IN_FLGHT in memory", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: SAF Records with status IN_PROCESS has removed from POS, Going for next SAF transactions", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "SAF Records With Status IN_PROCESS Has been Removed By POS, Going For Next SAF Transaction");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}
			bRetVal = PAAS_FALSE;
		}
		releaseMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, ((bRetVal == PAAS_TRUE)?"TRUE":"FALSE"));
	APP_TRACE(szDbgMsg);

	return bRetVal;
}

/*
 * ============================================================================
 * Function Name: calcDiffbetweenDates
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int calcDiffbetweenDates(char *pszTimeStamp1, char *pszTimeStamp2)
{
	int		iRetVal		= SUCCESS;
	int 	iDay1		= 0;
	int		iMonth1		= 0;
	int		iYear1		= 0;
	int		iDay2		= 0;
	int		iMonth2		= 0;
	int		iYear2		= 0;
	int		iNoDays1	= 0;
	int		iNoDays2	= 0;
	int		iRef		= 0;
	int		iIndex		= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Need to find difference between [%s] and [%s]", __FUNCTION__, pszTimeStamp1, pszTimeStamp2);
	APP_TRACE(szDbgMsg);

	iRetVal = parseTimeStamp(pszTimeStamp1, &iDay1, &iMonth1, &iYear1);
	if(iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while parsing the first timestamp", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	iRetVal = parseTimeStamp(pszTimeStamp2, &iDay2, &iMonth2, &iYear2);
	if(iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while parsing the second timestamp", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	iRef = iYear1;
	if(iYear2 < iYear1)
	{
		iRef = iYear2;
	}

	iNoDays1 = getNumberofDays(iMonth1);

	for(iIndex = iRef; iIndex < iYear1; iIndex++)
	{
		if(iIndex%4==0)
		{
			iNoDays1+=1;
		}
	}

	iNoDays1 = iNoDays1 + iDay1 + (iYear1-iRef)*365;

	debug_sprintf(szDbgMsg, "%s: Number of days for the First timestamp from Jan1 is %d", __FUNCTION__, iNoDays1);
	APP_TRACE(szDbgMsg);

	iNoDays2 = getNumberofDays(iMonth2);

	for(iIndex = iRef; iIndex < iYear2; iIndex++)
	{
		if(iIndex%4==0)
		{
			iNoDays2+=1;
		}
	}

	iNoDays2 = iNoDays2 + iDay2 + (iYear2-iRef)*365;

	debug_sprintf(szDbgMsg, "%s: Number of days for the second timestamp from Jan1 is %d", __FUNCTION__, iNoDays2);
	APP_TRACE(szDbgMsg);

	iRetVal = iNoDays2 - iNoDays1;

	debug_sprintf(szDbgMsg, "%s: The Difference between the given two timestamps in days is %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: parseTimeStamp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseTimeStamp(char *pszTimeStamp, int *piDay, int *piMonth, int *piYear)
{
	int		iRetVal		= SUCCESS;
	char	szTemp[5]   = "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/*
	 * Format: dd-mm-yyyy
	 */
	strncpy(szTemp, pszTimeStamp, 2); //Copying the first two bytes which is date

	*piDay = atoi(szTemp);

	memset(szTemp, 0x00, sizeof(szTemp));
	strncpy(szTemp, &pszTimeStamp[3], 2); //Copying the two bytes which is month

	*piMonth = atoi(szTemp);

	memset(szTemp, 0x00, sizeof(szTemp));
	strcpy(szTemp, &pszTimeStamp[6]);

	*piYear = atoi(szTemp);

	debug_sprintf(szDbgMsg, "%s: Day[%d], Month[%d], Year[%d]", __FUNCTION__, *piDay, *piMonth, *piYear);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getNumberofDays
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getNumberofDays(int iMonth)
{
	int iDays = 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(iMonth)
	{
		case 1:
			iDays=0;
			break;
		case 2:
			iDays=31;
			break;
		case 3:
			iDays=59;
			break;
		case 4:
			iDays=90;
			break;
		case 5:
			iDays=120;
			break;
		case 6:
			iDays=151;
			break;
		case 7:
			iDays=181;
			break;
		case 8:
			iDays=212;
			break;
		case 9:
			iDays=243;
			break;
		case 10:
			iDays=273;
			break;
		case 11:
			iDays=304;
			break;
		case 12:
			iDays=334;
			break;
		default:
			debug_sprintf(szDbgMsg, "%s: Invalid Month!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iDays);
	APP_TRACE(szDbgMsg);

	return(iDays);
}

/*
 * ============================================================================
 * Function Name: safTimeSigHandler
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static void safTimeSigHandler(int iSigNo)
{
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(iSigNo == SIGSAFTIME)
	{
		debug_sprintf(szDbgMsg, "%s: Signal SIGSAFTIME delivered", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		siglongjmp(safJmpBuf, 1);
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Unknown signal", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	return;
}

/*
 * ============================================================================
 * Function Name: resetSAFPurgingTimer
 *
 * Description	: Sends the signal to UI thread to update image on idle screen
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
int resetSAFPurgingTimer()
{
	int		iRetVal			= SUCCESS;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif


	debug_sprintf(szDbgMsg, "%s: Entered", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: SAF purging thread id: %d", __FUNCTION__, (int)ptSAFPurgingId);
	APP_TRACE(szDbgMsg);

	if(ptSAFPurgingId != 0) //This thread exists
	{
		iRetVal = pthread_kill(ptSAFPurgingId, SIGSAFTIME);
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getPassThrghRespLen
 *
 * Description	: get the total length of pass through response fields.
 *
 * Input Params	:
 *
 * Output Params: length
 * ============================================================================
 */
static int getPassThrghRespLen()
{
	int						iCnt				= 0;
	int						iPassThrgRecLen		= 0;
	PASSTHRG_FIELDS_STYPE	stPassThrghDtls;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	memset(&stPassThrghDtls, 0x00, sizeof(PASSTHRG_FIELDS_STYPE));
	getPassThrghDtlsForPymtTran(szSSITranKey, &stPassThrghDtls);

	// Get the total length for storing all pass through fields in pszSAFRec
	if(stPassThrghDtls.pstResTagList != NULL)
	{
		for(iCnt = 0; iCnt < stPassThrghDtls.iResTagsCnt; iCnt++)
		{
			if(stPassThrghDtls.pstResTagList[iCnt].value != NULL)
			{
				iPassThrgRecLen += strlen(stPassThrghDtls.pstResTagList[iCnt].key);	// key length
				iPassThrgRecLen++;	// for '=' separator
				iPassThrgRecLen += strlen((char*)stPassThrghDtls.pstResTagList[iCnt].value);
				iPassThrgRecLen++;	// for ',' separator
			}
		}
	}
	debug_sprintf(szDbgMsg, "%s: Total Record Length for Pass through fields is[%d]", __FUNCTION__, iPassThrgRecLen);
	APP_TRACE(szDbgMsg);

	return iPassThrgRecLen;
}

/*
 * ==============================================================================
 * Function Name: fillPassThrghRespDtlsToSAFRec
 *
 * Description	: This API parses the SAF record line for the Pass through fields & Update them for each SAF records node
					This would be used when SAF query is done from POS.
 *
 * Input Params	: Record Line, PASSTHRG_FIELDS_PTYPE;
 *
 * Output Params: Pointer to the next field of the record
 * ==============================================================================
 */
static char * fillPassThrghRespDtlsToSAFRec(char *pszSAFRec, PASSTHRG_FIELDS_PTYPE *dpstPassThrghDtls)
{
	int							iResTagsCnt		  	= 0;
	int							iCnt		  		= 0;
	char						*pchFirst 		  	= NULL;
	char 						*pchNext 		  	= NULL;
//	char						szTemp[256]		  	= "";
	char						*pTemp			  	= NULL;
	char						*pLocal			  	= NULL;
	char						*cPtr			  	= NULL;
	KEYVAL_PTYPE				pstLocKeyValPtr		= NULL;
	PASSTHRG_FIELDS_STYPE		stPassThrghDtls;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszSAFRec == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter is NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return pchNext;
	}
	while(1)
	{
		memset(&stPassThrghDtls, 0x00, sizeof(PASSTHRG_FIELDS_STYPE));
		/* Format of the SAF Record
		 * <LEN>|<SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE>|<ACCT_NUM>|<TRACK2>|<EXPIRY_MONTH>|<EXPIRY_YEAR>|<PAYMENT_MEDIA>|<CARD_SOURCE>|<FORCE_FLAG>|
		 * <TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<PAYMENT_TYPE>|<INVOICE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESP_CODE>|<CTROUTD>|
		 * <TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
		 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|
		 * DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID|CUSTOMER_CODE|AMOUNT_HEALTHCARE|AMOUNT_PRESCRIPTION|AMOUNT_VISION|AMOUNT_CLINIC|
		 * AMOUNT_DENTAL|EMV_TAGS|EMV_ENCRYPTED_BLOB|EMV_REVERSAL_TYPE|PIN_BLOCK|KEY_SERIAL_NUMBER|PAYMENT_SUBTYPE|PROMO_CODE|ORDER_DATETIME|CREDIT_PLAN_NBR|
		 * PURCHASE_APR|REFERENCE|CARDHOLDER|BILLPAY|ORIG_TRANS_DATE|ORIG_TRANS_TIME|CASHBACK_AMNT|BAR_CODE|PIN_CODE|APR_TYPE|INIT_VECTOR|PASS_THROUGH_FIELDS
		 * ||||;
		 *
		 */

		pchFirst = pszSAFRec;

		/*-------------- Getting SAF Record PASS THROUGH FIELDS----------------*/
		if((*pchFirst) != PIPE) //Record contains Pass Through fields
		{
			debug_sprintf(szDbgMsg, "%s: Record contains Pass Through fields", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iResTagsCnt++; //atleast one pass through is there.
			pchNext = strchr(pchFirst, PIPE);
			if(pchNext == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Doesnt contain PIPE(|) in the data!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			pTemp = (char*)malloc(pchNext - pchFirst + 1);
			if(pTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				return pchNext;
			}
			memset(pTemp, 0x00, pchNext - pchFirst + 1);
			memcpy(pTemp, pchFirst, pchNext - pchFirst);

			// find the total number of pass thorugh fields present in the SAF record
			cPtr = strchr(pTemp, ',');
			while(cPtr != NULL)
			{
				iResTagsCnt++;
				cPtr = strchr(cPtr + 1, ',');
			}

			// allocate memory for storing each key value pair of pass through fields
			pstLocKeyValPtr = (KEYVAL_PTYPE) malloc(iResTagsCnt * KEYVAL_SIZE);
			if(pstLocKeyValPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			memset(pstLocKeyValPtr, 0x00, iResTagsCnt * KEYVAL_SIZE);

			pchFirst = pTemp;
			pchNext = strtok(pchFirst, ",");
			for(iCnt = 0; iCnt < iResTagsCnt && pchNext != NULL; iCnt++)
			{
				pchFirst = pchNext;
				// key
				pchNext = strchr(pchFirst, '=');
				if(pchNext != NULL)
				{
					pLocal = (char*) malloc(sizeof(char) * (pchNext - pchFirst + 1));
					if(pLocal != NULL)
					{
						memset(pLocal, 0x00, sizeof(char) * (pchNext - pchFirst + 1));
						memcpy(pLocal, pchFirst, pchNext - pchFirst);
						pstLocKeyValPtr[iCnt].key = pLocal;
					}

					pchFirst = pchNext + 1;
					//value
					if(strlen(pchFirst))
					{
						pLocal = (char*) malloc(sizeof(char) * (strlen(pchFirst) + 1));
						if(pLocal != NULL)
						{
							memset(pLocal, 0x00, sizeof(char) * (strlen(pchFirst) + 1));
							memcpy(pLocal, pchFirst, sizeof(char) * strlen(pchFirst));
							pstLocKeyValPtr[iCnt].value = pLocal;
						}
					}

					pstLocKeyValPtr[iCnt].valType = SINGLETON;
				}

				pchNext = strtok(NULL, ",");
				debug_sprintf(szDbgMsg, "%s: Pass Through Field[%d] Key[%s] Value[%s]", __FUNCTION__, iCnt + 1, pstLocKeyValPtr[iCnt].key,
						(char*)pstLocKeyValPtr[iCnt].value);
				APP_TRACE(szDbgMsg);
			}

			if(pchNext != NULL)	// T_RaghavendranR1 CID 83369 (#1 of 1): Dereference after null check (FORWARD_NULL)
			{
				pchNext++;
			}

			stPassThrghDtls.iResTagsCnt = iResTagsCnt;
			stPassThrghDtls.pstResTagList = pstLocKeyValPtr;
			if(*dpstPassThrghDtls == NULL)
			{
				*dpstPassThrghDtls = (PASSTHRG_FIELDS_PTYPE) malloc(sizeof(PASSTHRG_FIELDS_STYPE));
				if(*dpstPassThrghDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					pchNext = NULL;
					break;
				}
			}
			memset(*dpstPassThrghDtls, 0x00, sizeof(PASSTHRG_FIELDS_STYPE));
			memcpy(*dpstPassThrghDtls, &stPassThrghDtls, sizeof(PASSTHRG_FIELDS_STYPE));
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Record does not contain pass through fields", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pchNext = pchFirst ;
		}
		break;
	}

	// free the memory allocated for pTemp
	free(pTemp);
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return pchNext;
}
/*
 * ============================================================================
 * Function Name: getAllSAFTORRecords
 *
 * Description	:
 *
 * Input Params	: SAF Record buffer
 *
 * Output Params: SAF Status value
 * ============================================================================
 */
static int getAllSAFTORRecords(SAFDTLS_PTYPE pstSAFDtls)
{
	int		iRetVal      	 = SUCCESS;
	FILE	*Fh			 	 = NULL;
	int		iIndex		 	 = 0;
	int		iRecordLen	 	 = 0;
	int		iPreviousRecLen  = 0;
	char	*pszSAFRecordData = NULL;
	char	*pchTemp		 = NULL;
	//char    szSAFTORCmd[20]	 = "";
	int		iSAFRecFound     = 0;
	int		iFileNumber		 = 0;
	struct	stat st;
#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	//Check if there are any records presently
	if(giTotalSAFTORRecords == 0)
	{
		debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}
	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	for(iFileNumber = 0; iFileNumber < 2; iFileNumber++)
	{
		if(iFileNumber == 0)
		{
			if(stat(SAF_ELIGIBLE_RECORDS_FILE_NAME, &st) == 0)
			{
				if(st.st_size > 1)
				{
					Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
					if(Fh==NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
						APP_TRACE(szDbgMsg);
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}
		else if(iFileNumber == 1)
		{
			if(stat(SAF_COMPLETED_RECORDS_FILE_NAME, &st) == 0)
			{
				if(st.st_size > 1)
				{
					Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
					if(Fh==NULL)
					{
						//CID 67217 (#1 of 1): Copy-paste error (COPY_PASTE_ERROR). changed the File name in debug print. T_RaghavendranR1
						debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
						APP_TRACE(szDbgMsg);
						releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
						return FAILURE;
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering no records found", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					continue;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
				APP_TRACE(szDbgMsg);
				continue;
			}
		}

		for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
		{
			debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			iRecordLen = getRecordLength(Fh);

			if(iRecordLen <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: No more records present in the file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			if(iPreviousRecLen < iRecordLen + 1)
			{
				//Allocate the memory to hold the SAF record
				pchTemp = (char *)realloc(pszSAFRecordData, iRecordLen + 10);
				if(pchTemp == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					fclose(Fh); //Closing the file before returning
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
				else
				{
					memset(pchTemp, 0x00, iRecordLen + 10);
					pszSAFRecordData = pchTemp;
				}
				iPreviousRecLen = iRecordLen + 10;
			}
			else
			{
				if(pszSAFRecordData != NULL)
				{
					memset(pszSAFRecordData, 0x00, iRecordLen + 1); // CID 67234 (#1 of 1): Explicit null dereferenced (FORWARD_NULL). T_RaghavendranR1 NULl check added
				}
			}

			// CID 67490 (#1 of 1): Out-of-bounds access (OVERRUN), T_RaghavendranR1, Allocating enough memory, so that same can be accessed in fgets() and we do not overrun it also.
			if((pszSAFRecordData != NULL) && (fgets(pszSAFRecordData, iRecordLen+10, Fh) == NULL))// CID 67234 (#1 of 1): Explicit null dereferenced (FORWARD_NULL). T_RaghavendranR1 NULl check added
			{
				if(feof(Fh))
				{
					debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					fclose(Fh); //Closing the file before returning
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					if(pszSAFRecordData != NULL)
					{
						free(pszSAFRecordData);
					}
					return FAILURE;
				}
			}

			if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iIndex = iIndex -1; //Time Stmap lines are not part of the Total number of records, need to read more
				continue;
			}

			iRetVal = getSAFRecordStatus(pszSAFRecordData);
			if(iRetVal == SAF_STATUS_DECLINED || iRetVal == SAF_STATUS_INPROCESS ||
					iRetVal == SAF_STATUS_NOTPROCESSED || iRetVal == SAF_STATUS_PROCESSED ||
					iRetVal == SAF_STATUS_QUEUED)
			{
				debug_sprintf(szDbgMsg, "%s: It is a Payment SAF records..Skipping it...", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				continue;
			}

			debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iSAFRecFound = 1;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//FIXME: What to do here???
			}
		}

		if(Fh != NULL)
		{
			fclose(Fh);
		}
	}

	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_FOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_NOT_FOUND;
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

#if 0
/*
 * ============================================================================
 * Function Name: getSAFRecordCommand
 *
 * Description	: This API gives the command of the SAF record after parsing the command
 * 				  field in the SAF record of the SAF record file
 *
 * Input Params	: SAF Record buffer
 *
 * Output Params: SAF Status value
 * ============================================================================
 */
static int getSAFRecordCommand(char *pszRecordData)
{
	int		iRetVal			= 0;
	int		iCnt			= 0;
	char	*pchFirst		= NULL;
	char	*pchNext		= NULL;
	char	szCommand[5]	= "";

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/*
	 * <SAF_STATUS>|<SAF_RECORD_NUM>|<AUTH_CODE><ACCT_NUM>|<TRACK2>|<TRANS_AMOUNT>|<TIP_AMOUNT>|<TAX_AMOUNT>|<EXPIRY_DATE>|
	 * <PAYMENT_TYPE>|<PAYMENT_MEDIA>|<INVOICE>|<CARD_SOURCE>|<PURCHASE_ID>|<CASHIER_NUM>|<MIME_TYPE>|<SIGNATURE_DATA>|<RESULT_CODE>|
	 * <CTROUTD>|<TROUTD>|<AUTH_CODE>|ENCRYPTION_TYPE|ENCRYPTION_PAYLOAD|CARD_TOKEN|CVV2_CODE|BANKUSERDATA|BUSINESSDATE|PINLESSDEBIT|TXN_POSENTRYMODE|MERCHID|TERMID|LANE|
	 * APPROVED_AMOUNT|TAX_IND|COMMAND|TRACK_INDICATOR|STORE_NUM|CUSTOMER_ZIP|CUSTOMER_STREET|DISCOUNT_AMOUNT|DUTY_AMOUNT|FREIGHT_AMOUNT|DEST_COUNTRY_CODE|DEST_POSTAL_CODE|SHIP_FROM_ZIP_CODE|RETAIL_ITEM_DESC_1|ALT_TAX_ID||||;
	 */

	pchFirst = pszRecordData;
	for(iCnt = 0; iCnt < 36; iCnt++)
	{
		pchNext = strchr(pchFirst, PIPE);
		if(pchNext == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		pchFirst = pchNext + 1;
	}
	pchNext = strchr(pchFirst, PIPE);
	if(pchNext == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Does not contain PIPE(|) in the data!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	strncpy(szCommand, pchFirst, (pchNext - pchFirst)); //Getting the SAF Status from the file

	debug_sprintf(szDbgMsg, "%s: Command of the SAF record is %s", __FUNCTION__, szCommand);
	APP_TRACE(szDbgMsg);

	iRetVal = atoi(szCommand);

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}
#endif

/*
 * ============================================================================
 * Function Name: filterSAFRecsByTORStatus
 *
 * Description	:This API filters the SAF TOR records based on the SAF status and
 *                fills the SAF transaction details list
 *
 * Input Params	: SAF Record buffer
 *
 * Output Params: SAF Status value
 * ============================================================================
 */
static int filterSAFRecsByTORStatus(SAFDTLS_PTYPE pstSAFDtls)
{
	int		iRetVal      	 = SUCCESS;
	FILE	*Fh			 	 = NULL;
	int		iIndex		 	 = 0;
	int		iRecordLen	 	 = 0;
	int		iPreviousRecLen  = 0;
	char	*pszSAFRecordData = NULL;
	//PAAS_BOOL bSAFStatus	= PAAS_FALSE;
	char	*pchTemp		 = NULL;
	int		iSAFStatus		 = 0;
	int		iSAFRecFound     = 0;
	struct	stat st;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSAFDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Incoming parameter is NULL ", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	//Check if there are any records presently
	if(giTotalSAFTORRecords == 0)
	{
		debug_sprintf(szDbgMsg, "%s:There are no SAF Records stored", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	if(strcasecmp(pstSAFDtls->szTORStatus, "PENDING") == 0)				/*The SAF status Pending, Reversed, Not allowed, Not found are added for SAF on TOR*/
	{
		iSAFStatus = SAF_TOR_STATUS_PENDING;
	}
	else if(strcasecmp(pstSAFDtls->szTORStatus, "REVERSED") == 0)
	{
		iSAFStatus = SAF_TOR_STATUS_REVERSED;
	}
	else if(strcasecmp(pstSAFDtls->szTORStatus, "NOT_ALLOWED") == 0)
	{
		iSAFStatus = SAF_TOR_STATUS_NOTALLOWED;
	}
	else if(strcasecmp(pstSAFDtls->szTORStatus, "NOT_FOUND") == 0)
	{
		iSAFStatus = SAF_TOR_STATUS_NOTFOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid SAF TOR Status!!!![%s]", __FUNCTION__, pstSAFDtls->szTORStatus);
		APP_TRACE(szDbgMsg);
		return SAF_RECORD_NOT_FOUND;
	}

	debug_sprintf(szDbgMsg, "%s: Given SAF TOR Status [%s] [%d]", __FUNCTION__, pstSAFDtls->szTORStatus, iSAFStatus);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	if( iSAFStatus == SAF_TOR_STATUS_PENDING)
	{
		if(stat(SAF_ELIGIBLE_RECORDS_FILE_NAME, &st) == 0)
		{
			if(st.st_size > 1)
			{
				Fh = fopen(SAF_ELIGIBLE_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
				if(Fh==NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering it as no records found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
			APP_TRACE(szDbgMsg);
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return SAF_RECORD_NOT_FOUND;//File would not have created if there are no records found
		}
	}
	else if(iSAFStatus == SAF_TOR_STATUS_REVERSED || iSAFStatus == SAF_TOR_STATUS_NOTALLOWED || iSAFStatus == SAF_TOR_STATUS_NOTFOUND )
	{
		if(stat(SAF_COMPLETED_RECORDS_FILE_NAME, &st) == 0)
		{
			if(st.st_size > 1)
			{
				Fh = fopen(SAF_COMPLETED_RECORDS_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
				if(Fh==NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
					APP_TRACE(szDbgMsg);	// CID-67271: 2-Feb-16: MukeshS3: Changing it to SAF_COMPLETED_RECORDS_FILE_NAME from SAF_ELIGIBLE_RECORDS_FILE_NAME
					releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
					return FAILURE;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Size of the file is not greater than 1, considering it as no records found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return SAF_RECORD_NOT_FOUND; //File would not have created if there are no records found
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: %s does not exist, considering it as no records found", __FUNCTION__, SAF_COMPLETED_RECORDS_FILE_NAME);
			APP_TRACE(szDbgMsg);
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return SAF_RECORD_NOT_FOUND;//File would not have created if there are no records found
		}
	}

	debug_sprintf(szDbgMsg, "%s: Total Number SAF Record(s) [%d]", __FUNCTION__, giTotalSAFRecords);
	APP_TRACE(szDbgMsg);

	for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
	{
		debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iRecordLen = getRecordLength(Fh);

		debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
		APP_TRACE(szDbgMsg);

		if(iRecordLen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No more records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		debug_sprintf(szDbgMsg, "%s: PreviousRecLen [%d], RecLen[%d]", __FUNCTION__, iPreviousRecLen, iRecordLen);
		APP_TRACE(szDbgMsg);

		if(iPreviousRecLen < iRecordLen + 1)
		{
			//Allocate the memory to hold the SAF record
			// CID 68331 (#1 of 1): Out-of-bounds access (OVERRUN). Allocating enough memory, so that same can be accessed in fgets() and we do not overrun it also.
			pchTemp = (char *)realloc(pszSAFRecordData, iRecordLen + 10);
			if(pchTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			else
			{
				memset(pchTemp, 0x00, iRecordLen + 10);
				pszSAFRecordData = pchTemp;
			}

			iPreviousRecLen = iRecordLen + 10;
		}
		else
		{
			if(pszSAFRecordData != NULL) // CID 67439 (#1 of 1): Explicit null dereferenced (FORWARD_NULL)T_raghavendranR1
			{
				memset(pszSAFRecordData, 0x00, iPreviousRecLen);
			}
		}

		// CID 67439 (#1 of 1): Explicit null dereferenced (FORWARD_NULL)T_raghavendranR1
		if( (pszSAFRecordData != NULL) && (fgets(pszSAFRecordData, iRecordLen + 10, Fh) == NULL))//Added 10, wanted it to terminate by the newline character
		{
			if(feof(Fh))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(Fh); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				/* Daivik :Coverity 67388 - 4/2/2016
				 * Removing this unecessary check , because if this path has been hit , then pszSAFRecordData cannot be NULL.
				 * Moreover we also have a check within free to check the pointer value before freeing.
				 */
				free(pszSAFRecordData);

				return FAILURE;
			}
		}

		if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..Skipping it...", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iIndex = iIndex -1; //Time Stmap lines are not part of the Total number of records, need to read more
			continue;
		}

		debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iRetVal = getSAFRecordStatus(pszSAFRecordData);

		if(iRetVal != iSAFStatus)
		{
			debug_sprintf(szDbgMsg, "%s: SAF  record status is not %s", __FUNCTION__, pstSAFDtls->szTORStatus);
			APP_TRACE(szDbgMsg);
			continue; //Need to go to the next record if it exists
		}

		debug_sprintf(szDbgMsg, "%s:  %d SAF record is %s", __FUNCTION__, iIndex, pstSAFDtls->szTORStatus);
		APP_TRACE(szDbgMsg);

		iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
		if(iRetVal == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iSAFRecFound = 1;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_FOUND;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = SAF_RECORD_NOT_FOUND;
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	if(Fh != NULL)
	{
		fclose(Fh); //Closing the file
	}

	releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

#if 0
/*
 * ============================================================================
 * Function Name: clearNonTORSAFRecords
 *
 * Description	: This function will remove Payment SAF Records from both files
 * 					and retain all TOR records in both files.
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int clearNonTORSAFRecords(SAFDTLS_PTYPE pstSAFDtls)
{
	int	  iRetVal		   	 = SUCCESS;
	int	  iRetValTemp    	 = SUCCESS;
	double fCurrentAmount 	 = 0;
	double fAmountSpecialCase = 0.0;
	#ifdef DEBUG
		char szDbgMsg[256]	= "";
	#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		debug_sprintf(szDbgMsg, "%s:Number of Payment SAF Records present is %d", __FUNCTION__, giTotalSAFRecords);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Need to look based on SAF STATUS", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//Parse the SAF Record file for any record with in the range to clear them
		iRetVal = clearNonTORSAFRecsFromFile(pstSAFDtls, SAF_ELIGIBLE_RECORDS_FILE_NAME);
		if(iRetVal == SAF_RECORD_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: Found the SAF record(s) within the requested SAF record number range in Eligible SAF records", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/*
			 * Need to update the Total SAF amount, total SAF amount is only for the Eligible SAf records
			 * If any record is cleared from the eligible SAf records, need to update the Total
			 * SAF amount.
			 * Updating the global vairable here and writing to the file is done little latter after
			 * updating the number of records also
			 */
			fCurrentAmount = atof(pstSAFDtls->szTotAmt); //Need to update the Total SAF amount
			fAmountSpecialCase = atof(pstSAFDtls->szTotSpecialCaseAmt);

			debug_sprintf(szDbgMsg,"%s - Before Updation: Total SAF amount[%.2lf], Cleared SAF Total Amount[%.2lf]", __FUNCTION__, gfTotalSAFAmount, fCurrentAmount);
			APP_TRACE(szDbgMsg);

			gfTotalSAFAmount = gfTotalSAFAmount - fCurrentAmount + fAmountSpecialCase;
			giTotalEligibleSAFRecords = giTotalEligibleSAFRecords - atoi(pstSAFDtls->szRecCnt);

			debug_sprintf(szDbgMsg,"%s - After Updation: Total SAF amount[%.2lf]", __FUNCTION__, gfTotalSAFAmount);
			APP_TRACE(szDbgMsg);

			if(gfTotalSAFAmount < 0)
			{
				debug_sprintf(szDbgMsg,"%s - Total SAF amount can never be negative!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(iRetVal == SAF_RECORD_NOT_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s) within the requested SAF record number range in file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else if(iRetVal == SAF_RECORD_IN_PROGRESS)
		{
			debug_sprintf(szDbgMsg, "%s: SAF Record in IN_PROCESS Status, Cannot delete the Record.", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while clearing SAF records within the given range", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		//Parse the SAF Record file for any record with in the range to clear them
		iRetValTemp = clearNonTORSAFRecsFromFile(pstSAFDtls, SAF_COMPLETED_RECORDS_FILE_NAME);
		if(iRetValTemp == SAF_RECORD_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: Found the SAF record(s) within the requested SAF record number range in Completed SAF records", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else if(iRetValTemp == SAF_RECORD_NOT_FOUND)
		{
			debug_sprintf(szDbgMsg, "%s: NOT Found the SAF record(s) within the requested SAF record number range in file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while clearing SAF records within the given range", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(iRetVal == SAF_RECORD_FOUND || iRetValTemp == SAF_RECORD_FOUND) //Praveen_P1: FIXME: Revisit this logic and try to make it better
		{
			iRetVal = SAF_RECORD_FOUND;

			//Update the SAF Details File after seeing both the files
			giTotalSAFRecords = giTotalSAFRecords - atoi(pstSAFDtls->szRecCnt);

			debug_sprintf(szDbgMsg,"%s - After Updation: Total SAF Records[%d], Total SAF amount[%.2lf]", __FUNCTION__, giTotalSAFRecords, gfTotalSAFAmount);
			APP_TRACE(szDbgMsg);

			iRetVal = updateSAFDataToFile();
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully Updated the SAF Details file after resetting SAF Last Offline Epoch Time", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iRetVal = SAF_RECORD_FOUND;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while updating the SAF details file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				//Praveen_P1: what to do here!!!!
			}
		}
		else if(iRetVal == SAF_RECORD_IN_PROGRESS)
		{
			debug_sprintf(szDbgMsg, "%s: SAF Record in IN_PROCESS Status, Cannot delete the Record.", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			iRetVal = SAF_RECORD_NOT_FOUND;
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}
#endif
#if 0
/*
 * ============================================================================
 * Function Name: clearNonTORSAFRecsFromFile
 *
 * Description	: This function will clear Non TOR SAF records from the file passed
 *
 * Input Params	: File name(Eligible or Completed)
 *
 * Output Params:
 * ============================================================================
 */
static int clearNonTORSAFRecsFromFile(SAFDTLS_PTYPE pstSAFDtls, char* pszFileName)
{

	int			iRetVal      	     	= SUCCESS;
	int			iIndex		 	     	= 0;
	int			iRecordLen	 	     	= 0;
	int			iPreviousRecLen      	= 0;
	int			iClearedInProcessRecord	= 0;
	int			iSAFRecFound         	= 0;
	int			iTotalRecLen         	= 0;
	PAAS_BOOL	bAllowInProcessRecToRemove	= PAAS_TRUE;
	char		*pszSAFRecordData    	= NULL;
	char		*pchTemp		     	= NULL;
	char		szSAFRecNum[20]      	= "";
	char		szRecordLength[10]   	= "";
	char		*pchEndPos           	= NULL;
	//float		fCurrentAmount       	= 0.0;
	char		szTempBuf[100]	     	= "";
	char    	*pszTempSAFRecord    	= NULL;
	char		szTimeStampLine[50]		= "";
	FILE		*FhPerm			     	= NULL;
	FILE    	*FhTemp              	= NULL;
	PAAS_BOOL	bIsTORTran				= PAAS_TRUE;

#ifdef DEBUG
	char szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Need to clear  all Payment records from the File[%s]", __FUNCTION__, pszFileName);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

	FhPerm = fopen(pszFileName, "r+"); //Open a file for update (both for input and output). The file must exist.
	if(FhPerm==NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening the SAF record file[%s]!!!", __FUNCTION__, SAF_ELIGIBLE_RECORDS_FILE_NAME);
		APP_TRACE(szDbgMsg);
		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
		return ERR_FILE_NOT_FOUND;
	}

	FhTemp = fopen(SAF_RECORD_FILE_NAME_TEMP, "a+"); //Open file for output at the end of a file. Output operations always write data at the end of the file, expanding it. Repositioning operations (fseek, fsetpos, rewind) are ignored. The file is created if it does not exist
	if(FhTemp==NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening the SAF Temporary record file!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		fclose(FhPerm);
		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
		return ERR_FILE_NOT_FOUND;
	}

	for(iIndex = 1; iIndex <= giTotalSAFRecords + giTotalSAFTORRecords; iIndex++)
	{
		debug_sprintf(szDbgMsg, "%s: Reading the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		iRecordLen = getRecordLength(FhPerm);
		if(iRecordLen == FAILURE)
		{
			debug_sprintf(szDbgMsg, "%s: Error while getting the length of the SAF record !!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			fclose(FhPerm);
			fclose(FhTemp);//Closing the file before returning
			releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
			return FAILURE;
		}
		debug_sprintf(szDbgMsg, "%s: Length of the %d SAF record is %d", __FUNCTION__, iIndex, iRecordLen);
		APP_TRACE(szDbgMsg);

		if(iRecordLen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: No more records present", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		memset(szRecordLength, 0x00, sizeof(szRecordLength));
		sprintf(szRecordLength, "%d", iRecordLen);

		iTotalRecLen = iRecordLen + strlen(szRecordLength)/*For the length*/ + 1 /*For PIPE after length*/ + 2 /*Buffer*/;
		debug_sprintf(szDbgMsg, "%s:iTotalrecLen[%d]", __FUNCTION__, iTotalRecLen);
				APP_TRACE(szDbgMsg);

		if(iPreviousRecLen < iTotalRecLen)
		{
			debug_sprintf(szDbgMsg, "%s:entered into iPrevLength", __FUNCTION__);
							APP_TRACE(szDbgMsg);
			//Allocate the memory to hold the SAF record
			pchTemp = (char *)realloc(pszSAFRecordData, iTotalRecLen);
			if(pchTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while re-allocating memory for the SAF record buffer", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm);
				fclose(FhTemp); //Closing the file before returning
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			else
			{
				memset(pchTemp, 0x00, iTotalRecLen);
				pszSAFRecordData = pchTemp;
			}
			iPreviousRecLen = iTotalRecLen;
		}
		else
		{
			if(pszSAFRecordData != NULL)
			{
				memset(pszSAFRecordData, 0x00, iPreviousRecLen);
			}
		}

		if((pszSAFRecordData != NULL) && (fgets(pszSAFRecordData, iTotalRecLen, FhPerm) == NULL))
		{
			if(feof(FhPerm))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the SAF record from the file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm);
				fclose(FhTemp);//Closing the file before returning
				free(pszSAFRecordData);	// CID-67328: 27-Jan-16: MukeshS3: Freeing memory before abnormal return
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
		}

		if(strstr(pszSAFRecordData, "TimeStamp") != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: It is a time stamp SAF record line..", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iIndex = iIndex -1; //Time Stamp lines are not part of the Total number of records, need to read more

			debug_sprintf(szDbgMsg, "%s: Need to write the time stamp line to Temp file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			memset(szTimeStampLine, 0x00, sizeof(szTimeStampLine));
			sprintf(szTimeStampLine, "%d%c%s", strlen(pszSAFRecordData), PIPE, pszSAFRecordData);

			iRecordLen = strlen(szTimeStampLine);

			iRetVal = fwrite(szTimeStampLine, sizeof(char), iRecordLen, FhTemp);

			if(iRetVal == iRecordLen)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully written time stamp line to the temporary file ", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while writing time stamp line, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLen, iRetVal);
				APP_TRACE(szDbgMsg);
				//Praveen_P1: What to do here???
			}

			continue;
		}

		debug_sprintf(szDbgMsg, "%s: Got the %d SAF record", __FUNCTION__, iIndex);
		APP_TRACE(szDbgMsg);

		memset(szSAFRecNum, 0x00, sizeof(szSAFRecNum));
		iRetVal = getSAFRecordStatus(pszSAFRecordData);

		//iRetVal = getSAFRecordNumber(pszSAFRecordData, szSAFRecNum);

		if(iRetVal > 0 && ((iRetVal == SAF_TOR_STATUS_PENDING) || (iRetVal == SAF_TOR_STATUS_NOTALLOWED) || (iRetVal == SAF_TOR_STATUS_NOTFOUND)
				||	(iRetVal == SAF_TOR_STATUS_REVERSED)))
		{
			bIsTORTran = PAAS_TRUE;
		}
		else
		{
			bIsTORTran = PAAS_FALSE;
		}

		debug_sprintf(szDbgMsg, "%s: The status Value [%d]", __FUNCTION__, bIsTORTran);
		APP_TRACE(szDbgMsg);

		//iSAFCurrentRecordNum = atoi(szSAFRecNum);

		if(!bIsTORTran)	// If the Status is not TOR status and not In Process then remove that record.
		{
			debug_sprintf(szDbgMsg, "%s: Current SAF status [%d]", __FUNCTION__, iRetVal);
			APP_TRACE(szDbgMsg);

			if((iRetVal == SAF_STATUS_INPROCESS))
			{
				acquireMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");
				if(iSAFInProgressStatus == SAF_IN_FLIGHT)
				{
					debug_sprintf(szDbgMsg, "%s: SAF record with status IN_PROCESS is currently IN_FLIGHT, Should Not Delete the Record. Copying the record to temp file", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					releaseMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");
					bAllowInProcessRecToRemove = PAAS_FALSE;
				}
				else
				{
					/*-----------Delete the SAF SSI Tran Key ---------- */
					if(popSSITran(szSSITranKey) != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s:Error while popping the SAF SSI transaction from stack", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					deleteStack(szSSITranKey);
					memset(szSSITranKey, 0x00, sizeof(szSSITranKey));
					releaseMutexLock(&gptSAFRecordInProgressMutex, "SAF Records In Progress Mutex");
					iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
					if(iRetVal == SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						iSAFRecFound = 1;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						//FIXME: What to do here???
					}
				}
			}
			else
			{
				iRetVal = fillSAFRecordDetails(pszSAFRecordData, pstSAFDtls);
				if(iRetVal == SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s:  Successfully Filled the SAF Transaction Details", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iSAFRecFound = 1;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Failure while filled the SAF transaction details", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					//FIXME: What to do here???
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current SAF Status[%d] ", __FUNCTION__, iRetVal);
			APP_TRACE(szDbgMsg);

			//Need to copy the record to the temp file
			debug_sprintf(szDbgMsg, "%s:  Writing %d SAF record Number to temp file", __FUNCTION__, iIndex);
			APP_TRACE(szDbgMsg);

			pchEndPos = strchr(pszSAFRecordData, SEMI_COLON);

			if(pchEndPos == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Does not contain SEMI COLON in the SAF record", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm);
				fclose(FhTemp); //Closing the file before returning
				free(pszSAFRecordData); // CID 67328 (#1 of 2): Resource leak (RESOURCE_LEAK). T_RaghavendranR1. Free the previously allocated Data if the operation fails.
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}

			*pchEndPos = 0x00; //Ending the SAF record at the SEMI COLON
			pchEndPos++; //Pointing to LF
			*pchEndPos = 0x00; //Nullyfying the LF also

			pszTempSAFRecord = (char *)malloc(sizeof(char) * (iTotalRecLen + 2) /* Buffer */);
			if(pszTempSAFRecord == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while allocating memory for the Temp SAF record", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm);
				fclose(FhTemp); //CID-67483,67413: 22-Jan-16: MuekeshS3: Closing the file before returning
				free(pszSAFRecordData); // CID 67328 (#2 of 2): Resource leak (RESOURCE_LEAK). T_RaghavendranR1. Free the previously allocated Data if the operation fails.
				releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");
				return FAILURE;
			}
			memset(pszTempSAFRecord, 0x00,  iTotalRecLen + 2);
			sprintf(pszTempSAFRecord, "%d%c%s%c%c", strlen(pszSAFRecordData) + 1 /* For the ;*/ + 1 /*For the <LF>*/, PIPE, pszSAFRecordData, SEMI_COLON, LF /*New Line Field*/);
			memset(pszSAFRecordData, 0x00, iTotalRecLen);
			memcpy(pszSAFRecordData, pszTempSAFRecord, strlen(pszTempSAFRecord));
			free(pszTempSAFRecord); //Freeing temp SAF record

			//Update the temporary file
			iRecordLen = strlen(pszSAFRecordData);

			iRetVal = fwrite(pszSAFRecordData, sizeof(char), iRecordLen, FhTemp);

			if(iRetVal == iRecordLen)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully written SAF record of index %d to the temporary file ", __FUNCTION__, iIndex);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while writing SAF record to the file, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLen, iRetVal);
				APP_TRACE(szDbgMsg);
				//Praveen_P1: What to do here???
			}
		}
	}

	fclose(FhPerm);
	fclose(FhTemp); //Closing the file before doing some operations like moving

	if(iSAFRecFound == 1)
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//Copying the temporary SAF record to original file
		memset(szTempBuf, 0x00, sizeof(szTempBuf));
		sprintf(szTempBuf, "mv -f %s %s", SAF_RECORD_FILE_NAME_TEMP, pszFileName); //copying the file
		debug_sprintf(szDbgMsg,"%s - The command to be executed is [%s]", __FUNCTION__,szTempBuf);
		APP_TRACE(szDbgMsg);
		iRetVal = local_svcSystem(szTempBuf);

		if(iRetVal == 0)
		{
			debug_sprintf(szDbgMsg,"%s - Successfully executed the command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = SAF_RECORD_FOUND;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s - Error in execution of the command", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			iRetVal = FAILURE;
		}

		if(chmod(pszFileName, 0666) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Error while setting permission to the file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

		if(iClearedInProcessRecord == 1)
		{
			/* The part of the code will not be executed now, as we are not deleting the IN_PROCESS SFA Record */
			debug_sprintf(szDbgMsg,"%s - Cleared the IN_PROCESS SAF record, need to remove the same from the SAF record structure", __FUNCTION__);
			APP_TRACE(szDbgMsg);
#if 0
			/*------------- Clear the contents of the SAF Record Structure------------------- */
			if(stSAFRecord.stSignatureDetails.sigData != NULL)
			{
				free(stSAFRecord.stSignatureDetails.sigData);
			}
			memset(&stSAFRecord.stTransactionDetails, 0x00, sizeof(SAF_TRANDATA_S_TYPE));
			memset(&stSAFRecord.stCardDetails, 0x00, sizeof(SAF_CARDDATA_S_TYPE));
			memset(&stSAFRecord.stSignatureDetails, 0x00, sizeof(SIGN_DATA_STYPE));
			memset(&stSAFRecord, 0x00, sizeof(SAF_RECORD_ST_TYPE)); //Mem setting the structure
#endif
			//Since we have cleared the IN_PROCESS SAF record, need to update the system with the next ELIGIBLE record
			/*-----------Update the SAF data system with the next ELIGIBLE SAF record ---------- */
			iRetVal = readQueuedSAFRecord();
			if(iRetVal == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Successfully updated the SAF data system with the next SAF record with ELIGIBLE status",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = SAF_RECORD_FOUND;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Failure while updating the SAF data system with the next SAF record with ELIGIBLE status",__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: SAF Record NOT Found", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		releaseMutexLock(&gptSAFRecordFileMutex, "SAF Record File");

		memset(szTempBuf, 0x00, sizeof(szTempBuf));
		sprintf(szTempBuf, "rm -f %s", SAF_RECORD_FILE_NAME_TEMP); //removing the Temporary SAF record file
		debug_sprintf(szDbgMsg,"%s - The command to be executed is [%s]", __FUNCTION__,szTempBuf);
		APP_TRACE(szDbgMsg);

		iRetVal = local_svcSystem(szTempBuf);
		if(iRetVal == 0)
		{
			debug_sprintf(szDbgMsg,"%s - Successfully executed the command to remove the temp SAF records file", __FUNCTION__);
			APP_TRACE(szDbgMsg);

		}
		else
		{
			debug_sprintf(szDbgMsg,"%s - Error in execution of the command to remove the temp SAF records file", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		iRetVal = SAF_RECORD_NOT_FOUND;
		if(bAllowInProcessRecToRemove == PAAS_FALSE)
		{
			iRetVal = SAF_RECORD_IN_PROGRESS;
		}
	}

	if(pszSAFRecordData != NULL)
	{
		free(pszSAFRecordData);
	}

	debug_sprintf(szDbgMsg, "%s: Returning with %d", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}
#endif
