/******************************************************************
*                       appLogCfgParams.c                         *
*******************************************************************
* Application: Point Solution                                     *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the APIs which loads and reads the 		  *
* 			   config parameters required for the appLog Module   *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   	Developer     				Description       *
* -------- ----  ---------------------		--------------------- *
*                ArjunU1/R.Raghavendran                           *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <svc.h>

#include "appLog/appLogCfgDef.h"

/*Static variables declaration*/
static APP_LOG_STYPE 	stAppLogSettings;

/*
 * ============================================================================
 * Function Name: loadAppLogParams
 *
 * Description	: Loads all the config parameters from config file required by app log module
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
int loadAppLogParams()
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	long	lVal			= 0;
	char	szTemp[50]		= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg,"%s: ----- Enter ----- ",__FUNCTION__);
	APP_TRACE(szDbgMsg);

	iLen = sizeof(szTemp);

	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, APP_LOG_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Bool value accordingly */
		if( (*szTemp == 'y') || (*szTemp == 'Y') )
		{
			stAppLogSettings.bAppLogEnabled = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Application Logging ENABLED",
																__FUNCTION__);
		}
		else
		{
			stAppLogSettings.bAppLogEnabled = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: Application Logging DISABLED",
																__FUNCTION__);
		}
		rv = SUCCESS;
	}
	else
	{
		/* Value not found.. setting default */
		stAppLogSettings.bAppLogEnabled = PAAS_FALSE; //TODO: For McD customer, set default value TRUE.
		debug_sprintf(szDbgMsg, "%s: Application Logging Disabled by default",__FUNCTION__);
	}
	APP_TRACE(szDbgMsg);

	if(stAppLogSettings.bAppLogEnabled == PAAS_TRUE)
	{
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DEVICE, MAX_BCKUP_LOG_FILES, szTemp, iLen);
		if(rv > 0)
		{
			lVal = atol(szTemp);
			if( (lVal > 0) && (lVal <= DFLT_MAX_BCKUP_APP_LOG_FILES))
			{
				debug_sprintf(szDbgMsg, "%s:  Max Log Files for App Logging [%ld]",
																__FUNCTION__,lVal);
				APP_TRACE(szDbgMsg);

				stAppLogSettings.lAppMaxBckupLogFiles = lVal;
			}
			else
			{
				debug_sprintf(szDbgMsg,
						"%s: Incorrect value for Max Log Files for App Logging, Setting it to default [%d]",
												__FUNCTION__, DFLT_MAX_BCKUP_APP_LOG_FILES);
				APP_TRACE(szDbgMsg);

				stAppLogSettings.lAppMaxBckupLogFiles = DFLT_MAX_BCKUP_APP_LOG_FILES;
				sprintf(szTemp, "%d", DFLT_MAX_BCKUP_APP_LOG_FILES);
				putEnvFile(SECTION_DEVICE, MAX_BCKUP_LOG_FILES, szTemp);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg,
						"%s: No value found for Max Log Files for App Logging, Setting it to default [%d]",
												__FUNCTION__, DFLT_MAX_BCKUP_APP_LOG_FILES);
			APP_TRACE(szDbgMsg);

			stAppLogSettings.lAppMaxBckupLogFiles = DFLT_MAX_BCKUP_APP_LOG_FILES;
			sprintf(szTemp, "%d", DFLT_MAX_BCKUP_APP_LOG_FILES);
			putEnvFile(SECTION_DEVICE, MAX_BCKUP_LOG_FILES, szTemp);
		}

		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DEVICE, CUR_NUM_BCKUP_LOG_FILES, szTemp, iLen);
		if(rv > 0)
		{
			lVal = atol(szTemp);
			if( (lVal >= 0) && (lVal <= DFLT_MAX_BCKUP_APP_LOG_FILES))
			{
				debug_sprintf(szDbgMsg, "%s:  Total Log Files available for Application Logging [%ld]",
																__FUNCTION__,lVal);
				APP_TRACE(szDbgMsg);

				stAppLogSettings.iAppCurNumBckupLogFiles = lVal;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Total Log Files available exceeds the max limit for Application Logging, Setting it to default [%d]",
												__FUNCTION__, DFLT_MAX_BCKUP_APP_LOG_FILES);
				APP_TRACE(szDbgMsg);

				stAppLogSettings.iAppCurNumBckupLogFiles = DFLT_MAX_BCKUP_APP_LOG_FILES;
				sprintf(szTemp, "%d", DFLT_MAX_BCKUP_APP_LOG_FILES);
				putEnvFile(SECTION_DEVICE, CUR_NUM_BCKUP_LOG_FILES, szTemp);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg,
						"%s: No value found for Total Log Files available, Setting it to 0",
												__FUNCTION__);
			APP_TRACE(szDbgMsg);

			stAppLogSettings.iAppCurNumBckupLogFiles = 0;
			putEnvFile(SECTION_DEVICE, CUR_NUM_BCKUP_LOG_FILES, "0");
		}

		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DEVICE, MAX_LOG_FILE_SIZE, szTemp, iLen);
		if(rv > 0)
		{
			lVal = atol(szTemp);
			if( (lVal > 0) && (lVal <= DFLT_MAX_APP_LOG_FILE_SIZE))
			{
				debug_sprintf(szDbgMsg, "%s:  App Log File Size [%ld]", __FUNCTION__, lVal);
				APP_TRACE(szDbgMsg);

				stAppLogSettings.lAppLogFileSize = ( lVal * 1024 * 1024 ) ; // while storing converting into bytes.
			}
			else
			{
				debug_sprintf(szDbgMsg,
						"%s: Incorrect val for App Log File Size, Setting it to default [%d]",
												__FUNCTION__, DFLT_MAX_APP_LOG_FILE_SIZE);
				APP_TRACE(szDbgMsg);

				stAppLogSettings.lAppLogFileSize = ( DFLT_MAX_APP_LOG_FILE_SIZE * 1024 * 1024 );

				sprintf(szTemp, "%d", DFLT_MAX_APP_LOG_FILE_SIZE);
				putEnvFile(SECTION_DEVICE, MAX_LOG_FILE_SIZE, szTemp);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg,
						"%s: No val found for App Log File Size, Setting it to default [%d]",
												__FUNCTION__, DFLT_MAX_APP_LOG_FILE_SIZE);
			APP_TRACE(szDbgMsg);

			stAppLogSettings.lAppLogFileSize = ( DFLT_MAX_APP_LOG_FILE_SIZE * 1024 * 1024 );
			sprintf(szTemp, "%d", DFLT_MAX_APP_LOG_FILE_SIZE);
			putEnvFile(SECTION_DEVICE, MAX_LOG_FILE_SIZE, szTemp);
		}

		memset(szTemp, 0x00, iLen);
		memset(stAppLogSettings.szAppLogFileLocation, 0x00, sizeof(stAppLogSettings.szAppLogFileLocation));
		rv = getEnvFile(SECTION_DEVICE, LOG_FILE_LOCATION, szTemp, iLen);
		if(rv > 0)
		{
			debug_sprintf(szDbgMsg, "%s:  Application Log File Location path[%s]",
																__FUNCTION__, szTemp);
			APP_TRACE(szDbgMsg);

			strcpy(stAppLogSettings.szAppLogFileLocation, szTemp);
		}
		else
		{
			debug_sprintf(szDbgMsg,
						"%s: No path found for App Log File Location, Setting it to Default [%s]",
												__FUNCTION__, DFLT_APP_LOG_FILE_LOC);
			APP_TRACE(szDbgMsg);

			strcpy(stAppLogSettings.szAppLogFileLocation, DFLT_APP_LOG_FILE_LOC);
			putEnvFile(SECTION_DEVICE, LOG_FILE_LOCATION, DFLT_APP_LOG_FILE_LOC);
		}

		rv = SUCCESS;
	}
	return rv;
}

/*
 * ============================================================================
 * Function Name: getTranAppLogSettings
 *
 * Description	: Gives settings  set for transaction application logs.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
void getTranAppLogSettings( APP_LOG_PTYPE pstAppLogSettings)
{

	if( pstAppLogSettings != NULL )
	{
		memcpy(pstAppLogSettings, &stAppLogSettings, sizeof(APP_LOG_STYPE));
	}
}

/*
 * ============================================================================
 * Function Name: getAppCurNumBckupLogFiles
 *
 * Description	: Returns application current number of back up log files.
 *
 * Input Params	: none
 *
 * Output Params: Number of app back up log files
 * ============================================================================
 */
int getAppCurNumBckupLogFiles()
{
	return stAppLogSettings.iAppCurNumBckupLogFiles;
}

/*
 * ============================================================================
 * Function Name: updateNumOfLogFiles
 *
 * Description	: Updates number of log files present in the devices.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
void updateNumOfLogFiles( int iCurNumBckupLogFiles)
{
	char	szTemp[50]		= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//Current number of application log files shouldn't be greater than max log files.
	if( iCurNumBckupLogFiles > stAppLogSettings.lAppMaxBckupLogFiles )
	{
		iCurNumBckupLogFiles = stAppLogSettings.lAppMaxBckupLogFiles;
	}

	debug_sprintf(szDbgMsg, "%s: Updating Num Of Log Files to %d", __FUNCTION__, iCurNumBckupLogFiles);
	APP_TRACE(szDbgMsg);

	stAppLogSettings.iAppCurNumBckupLogFiles = iCurNumBckupLogFiles;
	sprintf(szTemp, "%d", iCurNumBckupLogFiles);

	//Also Add to the the config file
	putEnvFile(SECTION_DEVICE, CUR_NUM_BCKUP_LOG_FILES, szTemp);
}

/*
 * ============================================================================
 * Function Name: isAppLogEnabled
 *
 * Description	: Returns Whether the applog is Enabled or Not.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int isAppLogEnabled()
{
	return stAppLogSettings.bAppLogEnabled;
}
/*
 * ============================================================================
 * End of file appLogCfgParams.c
 * ============================================================================
 */
