/*
 * =============================================================================
 * Filename    : appLogAPIs.c
 *
 * Application : Mx Point SCA
 *
 * Description : This module is responsible for print application logs to the given file location.
 * 				 It uses message queue IPC mechanism to receive data from one or more application
 * 				 and writes logs to the given file.
 *
 * Modification History:
 * 
 *  Date      Version No     	Programmer		  			Change History
 *  -------   -----------  	 ---------------------		------------------------
 *							ArjunU1/R.Raghavendran
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include <signal.h>
#include <pthread.h>
#include <sys/timeb.h>	// For struct timeb
#include <syslog.h>
#include <svc.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include "db/dataSystem.h"
#include "appLog/appLogCfgDef.h"

//MemDebug
//#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
//#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
//#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

static char	gszMaskXMLNode[MAX_XML_MASK_NODE][MAX_XML_NODE_SIZE];
static char gszRequestType[MAX_MASK_ENTRYID][MAX_ENTRYID_SIZE];

static APP_LOG_STYPE	stAppLogSettings;
static PAAS_BOOL		bInitAppLogDone = PAAS_FALSE;
static int				fFD				= 0;
static pthread_t		appLogThId		= 0;
static key_t			msgQKey;
static int				msgQId;
static PAAS_BOOL		bRunSCAAppLog 	= PAAS_FALSE;
#if 0
static FILE *          	gfp             = NULL;
#endif

static void 		loadSensitiveDataList();
static int 			createAppLogMsgQueue();
static void 		flushMsgQueue();
static int 			displayMsgQueueInfo();
//static int 		setMsgQueueParameter();
static void * 		bSCAAppLogsThread(void *);
static void	  		freeAppLogData(APPLOGDATA_PTYPE);
static void 		saveSCAAppLogs(APPLOGDATA_PTYPE );
static PAAS_BOOL	isEntryId2BeMasked(char *);
static int 			local_svcSystem(const char *);
static int 			maskSignatureData(char *, char ** );
static int 			maskSensitiveNodeData(char *);
static int 			maskCPHostSensitiveData(char * );
static int			maskVHIHostSensitiveData(char * );
static int			maskElavonHostSensitiveData(char * );
#if 0
static int 			checkEachNodeInXML2Mask(xmlNode*);
static int 			isNodeInMaskList(xmlChar*);
static int			maskXMLNodeData(char*, xmlChar**);
static int			maskXMLSignatureData(char*, xmlChar**);
static int			maskXMLPayLoadCData(char*, xmlChar** );
static xmlNode * 	findNodeInXMLData(char *, xmlNode *);
#endif


/*
 * ============================================================================
 * Function Name: initAppLog
 *
 * Description	: This function initiates the necessary steps to be taken if transaction logging is enabled.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int initAppLog()
{
	int 			rv 					= SUCCESS;
	char 			szErrMsg[100]		= "";
#ifdef DEBUG
	char		szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg,"%s: ----- Enter ----- ",__FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if ( bInitAppLogDone == PAAS_TRUE )
		{
			debug_sprintf(szDbgMsg,"%s: App Log Initialized Already!!!",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
		rv = loadAppLogParams();
		if(rv != SUCCESS)
		{
			debug_sprintf(szErrMsg,"%s: FAILED to load App log config parameters",__FUNCTION__);
			APP_TRACE(szErrMsg);
			syslog(LOG_ERR|LOG_USER, szErrMsg);

			break;
		}

		//Get App log settings
		memset(&stAppLogSettings, 0x00, sizeof(stAppLogSettings));
		getTranAppLogSettings(&stAppLogSettings);

		if( stAppLogSettings.bAppLogEnabled == PAAS_TRUE)
		{
			debug_sprintf(szDbgMsg,"%s: Initializing App Log",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			/* Create a msg queue to send/receive data to print the application logs */
			debug_sprintf(szDbgMsg, "%s: Creating Queue to Receive Data for App Log", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = createAppLogMsgQueue();
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to create message queue for App log", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			//Its very important to flush the existing data in the message queue.
			flushMsgQueue();

			//Display Message Queue Information
			rv = displayMsgQueueInfo();
			if( rv != FAILURE)
			{
				debug_sprintf(szDbgMsg, "%s: Current Number of messages on specified queue: %d", __FUNCTION__, rv);
				APP_TRACE(szDbgMsg);
			}

			bRunSCAAppLog = PAAS_TRUE;
			rv = pthread_create(&appLogThId, NULL, bSCAAppLogsThread, (void *)msgQId);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to create App Log thread",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			debug_sprintf(szDbgMsg, "%s: Created thread for Application Logging", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//Set this flag to perform initialization only once.
			bInitAppLogDone = PAAS_TRUE;
		}
		else
		{
			debug_sprintf(szDbgMsg,"%s: Application Logging is disabled",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		loadSensitiveDataList();

		break;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: LoadSensitiveDataList
 *
 * Description	: This Function would Load the Entry Id to be checked for and the
 *
 * Sensitive Data List to be Masked before Logging in the 2D Global Array List.
 *
 * Data will be Masked only if the Entry Id is XML REQUEST or XML RESPONSE
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
static void loadSensitiveDataList()
{
	int 			iCnt 				= 0;
	int 			iLineCnt 			= 0;
	char *			cPtr				= NULL;
	char			szBuff[1024]			= "";
	char *			pszCurrStr			= NULL;
	char *			pszParseStr			= NULL;
	FILE			*fMaskFilePtr		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]			= "";
#endif

	iCnt = 0;
	fMaskFilePtr = fopen(XML_MASK_LIST_FILELOC, "r");
	if(fMaskFilePtr != NULL) //file is not present
	{
		while((fgets(szBuff, sizeof(szBuff) - 1, fMaskFilePtr) != NULL) && (iCnt < MAX_XML_MASK_NODE))
		{
			iCnt = 0;
			iLineCnt++;
			cPtr = strrchr(szBuff, '\n');
			if(cPtr != NULL)
			{
				*cPtr = '\0';
			}

			cPtr = strrchr(szBuff, '\r');
			if(cPtr != NULL)
			{
				*cPtr = '\0';
			}

			pszCurrStr = strchr(szBuff,'=');
			if(pszCurrStr != NULL)
			{
				pszCurrStr++;
				pszParseStr = strtok(pszCurrStr, ",");
				while( pszParseStr != NULL)
				{
					debug_sprintf(szDbgMsg,"%s: iLineCnt [%d], iCnt [%d], pszParseStr [%s]",__FUNCTION__, iLineCnt, iCnt, pszParseStr);
					APP_TRACE(szDbgMsg);

					if(iLineCnt == 1)
					{
						memset(gszRequestType[iCnt], 0x00, sizeof(gszRequestType[iCnt]));
						memcpy(gszRequestType[iCnt], pszParseStr, strlen(pszParseStr));
					}
					else
					{
						memset(gszMaskXMLNode[iCnt], 0x00, sizeof(gszMaskXMLNode[iCnt]));
						memcpy(gszMaskXMLNode[iCnt], pszParseStr, strlen(pszParseStr));
					}
					iCnt++;
					pszParseStr = strtok(NULL, ",");
				}
			}
		}
		fclose(fMaskFilePtr);
	}
	else
	{
		debug_sprintf(szDbgMsg,"%s: Cannot Open the XML Node Mask List File, Creating Few Default Sensitive Data List",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(gszRequestType[0], "SCI_REQUEST");
		strcpy(gszRequestType[1], "SCI_RESPONSE");
		strcpy(gszRequestType[2], "SSI_REQUEST");
		strcpy(gszRequestType[3], "SSI_RESPONSE");
		strcpy(gszRequestType[4], "DHI_REQUEST");
		strcpy(gszRequestType[5], "DHI_RESPONSE");
		strcpy(gszRequestType[6], "RC_REQUEST");
		strcpy(gszRequestType[7], "RC_RESPONSE");
		strcpy(gszRequestType[8], "CP_REQUEST");
		strcpy(gszRequestType[9], "CP_RESPONSE");
		strcpy(gszRequestType[10], "VHI_REQUEST");
		strcpy(gszRequestType[11], "VHI_RESPONSE");
		strcpy(gszRequestType[12], "VANTIV_REQUEST");
		strcpy(gszRequestType[13], "ELAVON_REQUEST");
		strcpy(gszRequestType[14], "ELAVON_RESPONSE");


		strcpy(gszMaskXMLNode[0], "AcctNum");
		strcpy(gszMaskXMLNode[1], "Track1Data");
		strcpy(gszMaskXMLNode[2], "Track2Data");
		strcpy(gszMaskXMLNode[3], "CCVData");
		strcpy(gszMaskXMLNode[4], "EncrptBlock");
		strcpy(gszMaskXMLNode[5], "CARDHOLDER");
		strcpy(gszMaskXMLNode[6], "TRACK_DATA");
		strcpy(gszMaskXMLNode[7], "PAN");
		strcpy(gszMaskXMLNode[8], "CVV");
		strcpy(gszMaskXMLNode[9], "PIN_BLOCK");
		strcpy(gszMaskXMLNode[10], "CHECK_NUM");
		strcpy(gszMaskXMLNode[11], "CVV2");
		strcpy(gszMaskXMLNode[12], "ACCT_NUM");
		strcpy(gszMaskXMLNode[13], "EMBOSSED_ACCT_NUM");
		strcpy(gszMaskXMLNode[14], "KEY_SERIAL_NUMBER");
		strcpy(gszMaskXMLNode[15], "PIN_CODE");
		strcpy(gszMaskXMLNode[16], "CARD_TRACK1");
		strcpy(gszMaskXMLNode[17], "CARD_TRACK2");
		strcpy(gszMaskXMLNode[18], "CARD_TRACK3");
		strcpy(gszMaskXMLNode[19], "POS_TENDER1_DATA");
		strcpy(gszMaskXMLNode[20], "POS_TENDER2_DATA");
		strcpy(gszMaskXMLNode[21], "POS_TENDER3_DATA");
	}

	return;

}
/*
 * ============================================================================
 * Function Name: createAppLogMsgQueue
 *
 * Description	: This Function would create the msg queue to send/receive app log data.
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
int createAppLogMsgQueue()
{
	int				rv					= SUCCESS;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (msgQKey = ftok(stAppLogSettings.szAppLogFileLocation, 'B')) == -1 )
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to generate MSG Queue key", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Something went wrong ftok(): %s, errno=%d", __FUNCTION__,
																			strerror(errno), errno);
			APP_TRACE(szDbgMsg);


			rv = FAILURE;
			break;
		}

		if( (msgQId = msgget(msgQKey, 0666 | IPC_CREAT)) == -1)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get MSG Queue Id", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Something went wrong msgget(): %s, errno=%d", __FUNCTION__,
																			strerror(errno), errno);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Successfully Created Application Log Message Queue",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning[%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: displayMsgQueueInfo
 *
 * Description	: This Function would display information about the msg queue.
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
// Returns number of messages on specified queue
static int displayMsgQueueInfo()
{
	int					rv 				= SUCCESS;
	struct msqid_ds buf;

#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	// Flush the queue in case there are remnant messages in it
	debug_sprintf(szDbgMsg, "%s: Message Queue ID=%d", __FUNCTION__, msgQId);
	APP_TRACE(szDbgMsg);

	// Get a copy of the current message queue control structure and show it
	rv = msgctl(msgQId, IPC_STAT, &buf);
	if (rv == -1)
	{
		debug_sprintf(szDbgMsg, "%s: msgctl() failed!, errno=%d [%s]", __FUNCTION__, errno, strerror(errno));
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		/* ---
        The msqid_ds data structure is defined in <sys/msg.h> as follows:
        struct msqid_ds
        {
            struct ipc_perm msg_perm;    // Ownership and permissions
            time_t         msg_stime;    // Time of last msgsnd()
            time_t         msg_rtime;    // Time of last msgrcv()
            time_t         msg_ctime;    // Time of last change
            unsigned long  __msg_cbytes; // Current number of bytes in queue (non-standard)
            msgqnum_t      msg_qnum;     // Current number of messages in queue
            msglen_t       msg_qbytes;   // Maximum number of bytes allowed in queue
            pid_t          msg_lspid;    // PID of last msgsnd()
            pid_t          msg_lrpid;    // PID of last msgrcv()
        };
        --- */
		debug_sprintf(szDbgMsg, "%s: msg_cbytes (current # bytes on q) = %lu", __FUNCTION__, buf.msg_cbytes);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_qbytes (max # of bytes on q)  = %lu", __FUNCTION__, buf.msg_qbytes);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_qnum (# of messages on q)     = %lu", __FUNCTION__, buf.msg_qnum);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_lspid (pid of last msgsnd)    = %d",  __FUNCTION__, buf.msg_lspid);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_lrpid (pid of last msgrcv)     %d", __FUNCTION__, buf.msg_lrpid);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_stime (last msgsnd time)      = %s", __FUNCTION__,
														buf.msg_stime ? ctime(&buf.msg_stime) : "Not Set");
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_rtime (last msgrcv time)      = %s", __FUNCTION__,
														buf.msg_rtime ? ctime(&buf.msg_rtime) : "Not Set");
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: msg_ctime (last change time)      = %s", __FUNCTION__, ctime(&buf.msg_ctime));
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returned", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return buf.msg_qnum;
}

/*
 * ============================================================================
 * Function Name: flushMsgQueue
 *
 * Description	: This Function would read the message queue and ignores the data in order to flush the queue first time.
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
static void flushMsgQueue()
{
	int					rv 				= SUCCESS;
	int 				i				= 0;
	APPLOGDATA_STYPE    stAppLogData;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	// Flush the queue in case there are remnant messages in it
	debug_sprintf(szDbgMsg, "%s: Message Queue ID=%d", __FUNCTION__, msgQId);
	APP_TRACE(szDbgMsg);

	memset(&stAppLogData, 0x00, sizeof(stAppLogData));
	// Read through all messages on queue and discard
	i = 0;
	while( (rv = msgrcv(msgQId, &stAppLogData, sizeof(stAppLogData), 0, IPC_NOWAIT)) > 0 )
	{
		i++;
		debug_sprintf(szDbgMsg, "%s: Flushing Message Queue %d - msg# %d", __FUNCTION__, msgQId, i);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returned", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: addAppEventLog
 *
 * Description	: This Function will add the data to the Log Queue which takes care printing it to the file.
 *
 * Input Params	: none
 *
 * Output Params: NULL
 * ============================================================================
 */
void addAppEventLog(char *pszAppName, char *pszEntryType, char *pszEntryId, char *pszData, char *pszErrDiagSteps)
{
	int					rv					= SUCCESS;
	int					iLen				= 0;
	char				szTSPrefix[41]		= ""; /* MM/DD/YYYY HH:MM:SS:sss */
	char *				pszSigMaskedXML		= NULL;
	struct tm *			tm_ptr				= NULL;
	struct timeb		the_time;
	APPLOGDATA_STYPE 	stAppLogData;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	if( stAppLogSettings.bAppLogEnabled == PAAS_FALSE)
	{
		debug_sprintf(szDbgMsg, "%s: Transaction logging is disabled, Can't log the data", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}

	if( pszData == NULL || strlen(pszData) == 0)
	{
		debug_sprintf(szDbgMsg, "%s: Empty packet recevied, Can't log the data", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}
	/* Signature Data will be masked After 100 bytes, so that we cut the Data before adding it into the Queue.
	 * This is Done only for Signature Data before adding the data to Queue. The Remaining Sensitive Data will be
	 * masked when we read the data and print to the log File by the applog thread.
	 */
	if((pszEntryId != NULL) && ((strcmp(pszEntryId, "SSI_REQUEST") == SUCCESS) || (strcmp(pszEntryId, "SCI_RESPONSE") == SUCCESS) || (strcmp(pszEntryId, "DHI_REQUEST") == SUCCESS)
			 || (strcmp(pszEntryId, "VHI_REQUEST") == SUCCESS)))
	{
		rv = maskSignatureData(pszData, &pszSigMaskedXML);
		if(rv == SUCCESS)
		{
			pszData = pszSigMaskedXML;
		}
	}

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
		/* Keep within 3 digits, since sprintf's width.precision specification
		 * does not truncate large values */
		the_time.millitm = 999;
	}

	memset(szTSPrefix, 0x00, sizeof(szTSPrefix));
	sprintf(szTSPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d", tm_ptr->tm_mon+1,
			tm_ptr->tm_mday, 1900+tm_ptr->tm_year, tm_ptr->tm_hour,
			tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	memset(&stAppLogData, 0x00, sizeof(APPLOGDATA_STYPE));

	stAppLogData.lMsgType = (long)appLogThId;

	if( strlen(szTSPrefix) > 0 ) //CID 67395 (#1 of 1): Array compared against 0 (NO_EFFECT). T_RaghavendranR1, Removed Array NULL Check
	{
		strncpy(stAppLogData.szTSPrefix, szTSPrefix, sizeof(stAppLogData.szTSPrefix) - 1);
	}
	if( (pszAppName != NULL) && ( strlen(pszAppName) > 0 ) )
	{
		strncpy(stAppLogData.szAppName, pszAppName, sizeof(stAppLogData.szAppName) - 1);
	}
	if( (pszEntryType != NULL) && ( strlen(pszEntryType) > 0 ) )
	{
		strncpy(stAppLogData.szEntryType, pszEntryType, sizeof(stAppLogData.szEntryType) - 1);
	}
	if( (pszEntryId != NULL) && ( strlen(pszEntryId) > 0 ) )
	{
#if 0
		debug_sprintf(szDbgMsg, "%s: pszEntryId [%s] ", __FUNCTION__, pszEntryId);
		APP_TRACE(szDbgMsg);
#endif
		strncpy(stAppLogData.szEntryId, pszEntryId, sizeof(stAppLogData.szEntryId) - 1);
	}
	if(pszData != NULL)
	{
		iLen = strlen(pszData); //Get the Length of the Data to be logged
	}
	if( (pszErrDiagSteps != NULL) && ( strlen(pszErrDiagSteps) > 0 ) )
	{
		strncpy(stAppLogData.szErrDiagSteps, pszErrDiagSteps, sizeof(stAppLogData.szErrDiagSteps) - 1);
	}

	if( iLen > (MAX_DATA_SIZE - 1) ) //Log Only MAX_DATA_SIZE Bytes, Show a comment saying that we are logging only MAX_DATA_SIZE
	{
		iLen = (MAX_DATA_SIZE - 1);
		//sprintf(stAppLogData.szErrDiagSteps, "\nCould Not Log the Entire Data, Logging only [%d] Bytes of Data", MAX_DATA_SIZE);
	}

	/* Allocate and Copy the Data to be Logged, This Pointer should be Freed After Logging the statement in the File */
	stAppLogData.pszData = (char*) malloc((sizeof(char) * iLen) + 1);
	if(stAppLogData.pszData == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Memory Not Allocated", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}
	memset(stAppLogData.pszData, 0x00, ((sizeof(char)*iLen) + 1));
	memcpy(stAppLogData.pszData, pszData, iLen);

	/*Add Application Log Data to the Message Queue*/
	rv = msgsnd(msgQId, (void *)&stAppLogData, sizeof(APPLOGDATA_STYPE), IPC_NOWAIT); //IPC system call
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to add Application Log Data to Message Queue, [%d] [%s] ",__FUNCTION__, errno, strerror(errno));
		APP_TRACE(szDbgMsg);

		free(stAppLogData.pszData);
	}

	if(pszSigMaskedXML != NULL)
	{
		free(pszSigMaskedXML);
		pszSigMaskedXML = NULL;
	}

}

/*
 * ============================================================================
 * Function Name: bSCAAppLogsThread
 *
 * Description	: This thread will receive the application log statements
 * 					from various threads regarding the entry type(info, warning
 * 					error, failure or success), entry id(request, response,
 * 					display screen, display message or capture details), data(request
 * 					or response xml data) and error diagnostics steps.
 * 					The log statements will be logged in the path provided in the config.usr1.
 *
 * Input Params	: none
 *
 * Output Params: NULL
 * ============================================================================
 */
static void * bSCAAppLogsThread(void * arg)
{
	APPLOGDATA_STYPE    stAppLogData;

#ifdef DEBUG
	char            szDbgMsg[1024]  = "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: MSG Queue Id is [%d]", __FUNCTION__, msgQId);
	APP_TRACE(szDbgMsg);

	while(bRunSCAAppLog == PAAS_TRUE)
	{
		memset(&stAppLogData, 0x00, sizeof(APPLOGDATA_STYPE));

		//Read data from the message queue added by SCA application, DHI application.
		if( msgrcv(msgQId, &stAppLogData, sizeof(APPLOGDATA_STYPE), (long)appLogThId, 0) == -1) // IPC system call
		{
			debug_sprintf(szDbgMsg, "%s: Failed to Receive App Log Data From MSG Queue [%d] [%s]", __FUNCTION__, errno, strerror(errno));
			APP_TRACE(szDbgMsg);

			continue; //TODO: Check whether is it valid to do continue.
		}

		//Write logs to the file
		saveSCAAppLogs(&stAppLogData);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: isEntryId2BeMasked
 *
 * Description  : This function will write passed data to the scaapplogs.log files
 *
 * Input Params : none
 *
 * Output Params: NULL
 * ============================================================================
 */
static PAAS_BOOL isEntryId2BeMasked(char *pszEntryId)
{
	PAAS_BOOL	bEntryId2BeMasked		= PAAS_FALSE;
	int			iCnt					= 0;
	char *		szReqType				= NULL;
#ifdef DEBUG
	char szDbgMsg[256]   = "";	//TODO: keep smaller buffer later.
#endif

	if(pszEntryId == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL Parameters Passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return bEntryId2BeMasked;
	}

	// CID 67450 (#1 of 1): Out-of-bounds read (OVERRUN), Max Entryid to be ACcessed in gszRequestType must be less than MAX_MASK_ENTRYID and not MAX_XML_MASK_NODE.
	while((iCnt < MAX_MASK_ENTRYID) && ((szReqType = gszRequestType[iCnt]) != NULL))
	{																					//  should be evaluated first
		if(strcmp(pszEntryId, szReqType) == SUCCESS)
		{
			bEntryId2BeMasked = PAAS_TRUE;
			break;
		}
		iCnt++;
	}

	//debug_sprintf(szDbgMsg, "%s: Returning %s", __FUNCTION__, ( bEntryId2BeMasked == PAAS_TRUE)?"TRUE":"FALSE");
	//APP_TRACE(szDbgMsg);

	return bEntryId2BeMasked;
}

/*
 * ============================================================================
 * Function Name: saveSCAAppLogs
 *
 * Description  : This function will write passed data to the scaapplogs.log files
 *
 * Input Params : none
 *
 * Output Params: NULL
 * ============================================================================
 */
static void saveSCAAppLogs(APPLOGDATA_PTYPE pstAppLogData)
{
	int						rv						= SUCCESS;
	int						iTmpNumBckupAppLogFiles	= 0;
	int						iCurNumBckupAppLogFiles	= 0;
	long					lCurFileSize			= 0;
	long					lDataSize		 		= -1;
	long					lRetVal					= 0;
	static char            	szAppLogFile[120]  	    = "";
	static PAAS_BOOL		bFirstTime				= PAAS_TRUE;
	char            		szAppLogData[8192] 	    = "";
	char            		szCommand[100]       	= "";
	char *					pszData					= NULL;
	char *					pszEntryId				= NULL;
	struct stat				stFileBuf;
#ifdef DEBUG
	char szDbgMsg[256]   = "";
#endif

	if( pstAppLogData == NULL )
	{
		debug_sprintf(szDbgMsg, "%s: NULL param passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	if(bFirstTime)
	{
		memset(szAppLogFile, 0x00, sizeof(szAppLogFile));
		sprintf(szAppLogFile, "%s/scaapp.log", stAppLogSettings.szAppLogFileLocation); //E.g-> /mnt/flash/logs/usr1/scapapp.log
		bFirstTime = PAAS_FALSE;
	}

	pszData = pstAppLogData->pszData;
	pszEntryId = pstAppLogData->szEntryId;

	/* Mask the XML Request and XML Response for Sensitive XML Fields before Logging
	Check the Entry Id, If the Entry Id is a XML request or Response, mask it before Print/write to the Log File*/
#if 0
	debug_sprintf(szDbgMsg, "%s: App Name [%s], pszEntryId [%s]", __FUNCTION__, pstAppLogData->szAppName, pszEntryId);
	APP_TRACE(szDbgMsg);
#endif

	if((isEntryId2BeMasked(pszEntryId)) == PAAS_TRUE)
	{
		if((strcmp(pszEntryId, "CP_REQUEST") == SUCCESS))
		{
			rv = maskCPHostSensitiveData(pszData);				//CID:67211:T_POLISETTYG1:added comment for failure case
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Mask CP_REQUEST Sensitive Data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else if((strcmp(pszEntryId, "VANTIV_REQUEST") == SUCCESS))
		{
			rv = maskVHIHostSensitiveData(pszData);				//CID:67202:T_POLISETTYG1:added comment for failure case
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Mask VANTIV_REQUEST Sensitive Data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else if((strcmp(pszEntryId, "ELAVON_REQUEST") == SUCCESS))
		{
			rv = maskElavonHostSensitiveData(pszData);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Mask ELAVON_REQUEST Sensitive Data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			rv = maskSensitiveNodeData(pszData); //CID 67348 (#1 of 1): Unused value (UNUSED_VALUE) T_RaghavendranR1
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Failed to Mask Sensitive Data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
	}

	while(1)
	{
		/* Open a log file and write the list content in the file */
		fFD = open(szAppLogFile, O_WRONLY|O_APPEND|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
		if(fFD < 0)
		{
			/* Unable to open file for writing */
			debug_sprintf(szDbgMsg, "%s: Unable to open file [%s] for writing", __FUNCTION__, szAppLogFile);
			APP_TRACE(szDbgMsg);
			break;
		}

		// make sure permissions are 0666
		fchmod(fFD, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);

		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		sprintf(szAppLogData, "%s %-4s %-8s %-16s %s %-30s\n", pstAppLogData->szTSPrefix, pstAppLogData->szAppName, pstAppLogData->szEntryType,
				pstAppLogData->szEntryId, pszData, pstAppLogData->szErrDiagSteps);

		lDataSize = strlen(szAppLogData);

		memset(&stFileBuf, 0x00, sizeof(stFileBuf));
		rv = stat(szAppLogFile, &stFileBuf);
		if( rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Couldn't Find the Size of File[ %s]", __FUNCTION__, szAppLogFile);
			APP_TRACE(szDbgMsg);

			break;
		}
		lCurFileSize = (long)stFileBuf.st_size;

		if( (lCurFileSize + lDataSize) > ( stAppLogSettings.lAppLogFileSize) )
		{
			debug_sprintf(szDbgMsg, "%s: Max Log size Reached, Need to Rotate log files", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iCurNumBckupAppLogFiles = getAppCurNumBckupLogFiles();
			iTmpNumBckupAppLogFiles = iCurNumBckupAppLogFiles;

			/* If current num of app log files reaches the max app log file, then we need to
			 * decrement the cur num of log files count in order to rotate logs.
			 */
			if( iTmpNumBckupAppLogFiles == stAppLogSettings.lAppMaxBckupLogFiles)
			{
				iTmpNumBckupAppLogFiles--;
			}

			while( iTmpNumBckupAppLogFiles >= 1 )
			{
				//Update current Log file
				memset(szCommand, 0x00, sizeof(szCommand));
				sprintf(szCommand, "mv %s/scaapp%d.log %s/scaapp%d.log", stAppLogSettings.szAppLogFileLocation, iTmpNumBckupAppLogFiles,
						stAppLogSettings.szAppLogFileLocation, iTmpNumBckupAppLogFiles+1);

				debug_sprintf(szDbgMsg, "%s: Executing Command = [%s]", __FUNCTION__, szCommand);
				APP_TRACE(szDbgMsg);

				//Execute command
				rv = local_svcSystem(szCommand);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to execute [%s] rv = [%d]", __FUNCTION__, szCommand, rv);
					APP_TRACE(szDbgMsg);

					break;
				}

				iTmpNumBckupAppLogFiles--;
			}
			//Update current Log file
			memset(szCommand, 0x00, sizeof(szCommand));
			sprintf(szCommand, "mv %s/scaapp.log %s/scaapp%d.log", stAppLogSettings.szAppLogFileLocation,
					stAppLogSettings.szAppLogFileLocation, iTmpNumBckupAppLogFiles+1);

			//Restore the current number of log files.
			iTmpNumBckupAppLogFiles = iCurNumBckupAppLogFiles;

			debug_sprintf(szDbgMsg, "%s: Executing Last Command = [%s]", __FUNCTION__, szCommand);
			APP_TRACE(szDbgMsg);

			if( fFD > 0 )
			{
				close(fFD);
				fFD = 0;
			}
			//Execute command
			rv = local_svcSystem(szCommand);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to execute [%s] rv = [%d]", __FUNCTION__, szCommand, rv);
				APP_TRACE(szDbgMsg);

				break;
			}
			if( fFD == 0)
			{
				/* Open a Log file After Rotating  */
				fFD = open(szAppLogFile, O_WRONLY|O_APPEND|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
				if(fFD < 0)
				{
					/* Unable to open file for writing */
					debug_sprintf(szDbgMsg, "%s: Unable to open file [%s] for writing", __FUNCTION__, szAppLogFile);
					APP_TRACE(szDbgMsg);
					break;
				}
				// make sure permissions are 0666
				fchmod(fFD, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
			}

			//Increment the number of log files created counter and update the same in config file also.
			++iTmpNumBckupAppLogFiles;
			updateNumOfLogFiles(iTmpNumBckupAppLogFiles);

		}

		lRetVal = write(fFD, szAppLogData, lDataSize);

		if (lRetVal < lDataSize)
		{
			debug_sprintf(szDbgMsg, "%s: Write to file [%s] failed! retval=%ld", __FUNCTION__, szAppLogFile, lRetVal);
			APP_TRACE(szDbgMsg);

			close(fFD);
			fFD = 0;
			break;
		}

		close(fFD);
		fFD = 0;

		break;
	}

	freeAppLogData(pstAppLogData);
}
#if 0
/*
 * ============================================================================
 * Function Name: setMsgQueueParameter
 *
 * Description	: This Function would set change the msg queue parameters.
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
// Returns number of messages on specified queue
static int setMsgQueueParameter()
{
	int					rv 				= SUCCESS;
	struct msginfo msgInfobuf;

#ifdef DEBUG
	char		szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	// Flush the queue in case there are remnant messages in it
	debug_sprintf(szDbgMsg, "%s: Message Queue ID=%d", __FUNCTION__, msgQId);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Current Status Of the Message Queue", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	// Get a copy of the current message queue control structure and show it
	rv = msgctl(msgQId, MSG_INFO, (struct msqid_ds *)&msgInfobuf);
	if (rv == -1)
	{
		debug_sprintf(szDbgMsg, "%s: msgctl() failed!, errno=%d [%s]", __FUNCTION__, errno, strerror(errno));
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		/* ---
		struct msginfo {
			int msgpool;   		// Size in kibibytes of buffer pool used to hold message data; unused within kernel
			int msgmap;    		// Maximum number of entries in message map; unused within kernel
			int msgmax;    		// Maximum number of bytes that can be written in a single message
			int msgmnb;    		// Maximum number of bytes that can be written to queue; used to initialize msg_qbytes during queue creation (msgget(2))
			int msgmni;    		// Maximum number of message queues
			int msgssz;    		// Message segment size; unused within kernel
			int msgtql;    		// Maximum number of messages on all queues in system; unused within kernel
			unsigned short int msgseg; // Maximum number of segments; unused within kernel
		};
        --- */
		debug_sprintf(szDbgMsg, "%s: Size in kibibytes of buffer pool used to hold message data         = %d", __FUNCTION__, msgInfobuf.msgpool);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Maximum number of entries in message map                           = %d", __FUNCTION__, msgInfobuf.msgmap);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Maximum number of bytes that can be written in a single message    = %d", __FUNCTION__, msgInfobuf.msgmax);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Maximum number of bytes that can be written to queue               = %d",  __FUNCTION__, msgInfobuf.msgmnb);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Maximum number of message queues                                   = %d", __FUNCTION__, msgInfobuf.msgmni);
		debug_sprintf(szDbgMsg, "%s: Message segment size                                               = %d", __FUNCTION__, msgInfobuf.msgssz);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Maximum number of messages on all queues in system                 = %d",  __FUNCTION__, msgInfobuf.msgtql);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Maximum number of segments; unused within kernel                   = %u", __FUNCTION__, msgInfobuf.msgseg);
		APP_TRACE(szDbgMsg);
	}



	/* Change the max number of bytes that can be written in a single message */
	msgInfobuf.msgmax = 10000;

	/* Update the internal data structure */
	if( msgctl( msgQId, IPC_SET, (struct msqid_ds *)&msgInfobuf) == -1)
	{
		debug_sprintf(szDbgMsg, "%s: msgctl() failed!, errno=%d [%s]", __FUNCTION__, errno, strerror(errno));
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: After Updating; Status Of the Message Queue", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	// Get a copy of the current message queue control structure and show it
	rv = msgctl(msgQId, MSG_INFO, (struct msqid_ds *)&msgInfobuf);
	if (rv == -1)
	{
		debug_sprintf(szDbgMsg, "%s: msgctl() failed!, errno=%d [%s]", __FUNCTION__, errno, strerror(errno));
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		/* ---
		struct msginfo {
			int msgpool;   		// Size in kibibytes of buffer pool used to hold message data; unused within kernel
			int msgmap;    		// Maximum number of entries in message map; unused within kernel
			int msgmax;    		// Maximum number of bytes that can be written in a single message
			int msgmnb;    		// Maximum number of bytes that can be written to queue; used to initialize msg_qbytes during queue creation (msgget(2))
			int msgmni;    		// Maximum number of message queues
			int msgssz;    		// Message segment size; unused within kernel
			int msgtql;    		// Maximum number of messages on all queues in system; unused within kernel
			unsigned short int msgseg; // Maximum number of segments; unused within kernel
		};
        --- */
		debug_sprintf(szDbgMsg, "%s: Size in kibibytes of buffer pool used to hold message data         = %d", __FUNCTION__, msgInfobuf.msgpool);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Maximum number of entries in message map                           = %d", __FUNCTION__, msgInfobuf.msgmap);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Maximum number of bytes that can be written in a single message    = %d", __FUNCTION__, msgInfobuf.msgmax);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Maximum number of bytes that can be written to queue               = %d",  __FUNCTION__, msgInfobuf.msgmnb);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Maximum number of message queues                                   = %d", __FUNCTION__, msgInfobuf.msgmni);
		debug_sprintf(szDbgMsg, "%s: Message segment size                                               = %d", __FUNCTION__, msgInfobuf.msgssz);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Maximum number of messages on all queues in system                 = %d",  __FUNCTION__, msgInfobuf.msgtql);
		APP_TRACE(szDbgMsg);
		debug_sprintf(szDbgMsg, "%s: Maximum number of segments; unused within kernel                   = %u", __FUNCTION__, msgInfobuf.msgseg);
	}

	debug_sprintf(szDbgMsg, "%s: Returned", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return 0;
}
#endif

/*
 * ============================================================================
 * Function Name: freeAppLogData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static void freeAppLogData(APPLOGDATA_PTYPE pstAppLogData)
{

	if( pstAppLogData == NULL )
	{
		return;
	}
	if( ( pstAppLogData->pszData != NULL ) && strlen(pstAppLogData->pszData) > 0 )
	{
		/* Free the allocated transaction data */
		free(pstAppLogData->pszData);
	}
	pstAppLogData = NULL;

	return;
}

/*
 * ============================================================================
 * Function Name: closeAppLog
 *
 * Description	: Stops App Log Thread.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int closeAppLog()
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	bInitAppLogDone = PAAS_TRUE;
	bRunSCAAppLog   = PAAS_FALSE;

	pthread_join(appLogThId, NULL);

#if 0 //Don't remove the queue since same message queue is used by one/more application.
	//Destroying a message queue
	if(msgctl(msgQId, IPC_RMID, NULL) == -1)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to destroy a message queue", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
#endif

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: maskSignatureData
 *
 * Description	: Travel Each Node of the XML Request or Response and mask the Signature Data if present in the
 * XML message, This is done before adding the data to the queue so as to not log all the signature data in the log file.
 *
 * Here the masked Data is given in the   pszSigMaskedXML argument which needs to be freed by the user after
 * adding the data to the message queue.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int maskSignatureData(char * szXMLReqMsg, char **pszSigMaskedXML)
{
	int			rv									= SUCCESS;
	int			iAllocSize							= 0;
	char *		pszNodeName							= NULL;
	char 		pszStartTag[MAX_XML_NODE_SIZE+5]	= "";
	char 		pszEndTag[MAX_XML_NODE_SIZE+5]		= "";
	char *		pszStartPtr							= NULL;
	char *		pszCurrPtr							= NULL;
	char *		pszEndPtr							= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]							= "";
#endif

	if(szXMLReqMsg == NULL)
	{
		rv = FAILURE;
		return rv;
	}
	memset(pszStartTag, 0x00, sizeof(pszStartTag));
	memset(pszEndTag, 0x00, sizeof(pszEndTag));

	iAllocSize = strlen(szXMLReqMsg) + 1;
	*pszSigMaskedXML = (char*)malloc(sizeof(char) * iAllocSize);
	if(*pszSigMaskedXML == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		return rv;
	}

	memset(*pszSigMaskedXML, 0x00, iAllocSize);
	strcpy(*pszSigMaskedXML, szXMLReqMsg);
	pszStartPtr = *pszSigMaskedXML;

	pszNodeName = "SIGNATUREDATA";
	sprintf(pszStartTag, "<%s>", pszNodeName);
	if((pszCurrPtr = strstr(pszStartPtr, pszStartTag)) != NULL) /* Search for the Start Tag */
	{
		pszCurrPtr += strlen(pszStartTag); /* Go Forward By Node name Length */
		sprintf(pszEndTag, "</%s>", pszNodeName);
		if((pszEndPtr = strstr(pszCurrPtr, pszEndTag)) != NULL) /* Search for the End Tag */
		{
			if((pszEndPtr - pszCurrPtr) > 100)
			{
				pszCurrPtr += 96; /* Keep 96 Bytes of Original Signature Data */
			}
			if(pszEndPtr > pszCurrPtr)
			{
				memset(pszCurrPtr, '*', 4); /* Mask the Other Data, Add 4 "*" in the Data */
				pszCurrPtr += 4;
				strcpy(pszCurrPtr, pszEndPtr); /* Copy the Remaining Text After Masking */
			}
		}
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: maskCPHostSensitiveData
 *
 * Description	: Parsing the CP Host Request Message and Masking the Sensitive Data in the request, so that
 * the masked request data can be logged in app logging.
 *
 * Input Params	: Request message
 *
 * Output Params:
 * ============================================================================
 */
static int maskCPHostSensitiveData(char * pszReqMsg)
{
	int			rv									= SUCCESS;
	int			iCount								= 0;
	int			iDatalen							= 0;
	int			iCVVLen								= 0;
	char		szCVVLen[3]							= "";
	char		szTransCode[5]						= "";
	char *		pszStartPtr							= NULL;
	char *		pszCurrPtr							= NULL;
	char *		pszEndPtr							= NULL;
#ifdef DEBUG
	char	szDbgMsg[2560]							= "";
#endif

	if(pszReqMsg == NULL)
	{
		rv = FAILURE;
		return rv;
	}

	pszStartPtr = pszReqMsg;


	pszCurrPtr = pszStartPtr;
	pszCurrPtr = pszCurrPtr + 1 + 2 + 6 + 4 + 12 + 3 + 1 + 6 + 1;

	strncpy(szTransCode, pszCurrPtr, 2);

	debug_sprintf(szDbgMsg, "%s: CP_REQUEST, Transaction Code [%s]", __FUNCTION__, szTransCode);
	APP_TRACE(szDbgMsg);


	if((strcmp(szTransCode, "50") == SUCCESS) || (strcmp(szTransCode, "51") == SUCCESS) || (strcmp(szTransCode, "57") == SUCCESS)) // Batch Settle Commands Not Required to Mask Anything
	{

	}
	else if((strcmp(szTransCode, "41") == SUCCESS) || (strcmp(szTransCode, "78") == SUCCESS)) // Void Transaction, Need to MASK PAN at different position
	{
		if((pszEndPtr = strchr(pszCurrPtr, 0x1C)) != NULL)
		{
			pszCurrPtr = pszEndPtr + 1;
			if((pszEndPtr = strchr(pszCurrPtr, 0x1C)) != NULL)
			{
				pszCurrPtr = pszEndPtr + 1;
				if((pszEndPtr = strchr(pszCurrPtr, 0x1C)) != NULL)
				{
					pszCurrPtr += 4;
					memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
				}
			}
		}
	}
	else // Rest of the commands
	{
		if(strlen(pszStartPtr) > 41)
		{
			pszCurrPtr = pszStartPtr + 41; /* (+ 41)Moving to the position where the track Data or Account Number starts */
			if((pszEndPtr = strchr(pszCurrPtr, 0x1C)) != NULL)
			{
				iDatalen = pszEndPtr - pszCurrPtr;
				if((iDatalen) > 4) /* Keep 4 bytes data and Mask the Rest of the Data */
				{
					pszCurrPtr += 4;
				}
				memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
			}
		}

		pszCurrPtr = pszEndPtr + 1;

		iCount = 2;
		if(iDatalen < 20) /* We have Masked the Manually Entered Account Number, so we need to go one more <FS> ahead*/
		{
			iCount++;
		}

		while(iCount > 0 && pszCurrPtr != NULL && ((pszEndPtr = strchr(pszCurrPtr, 0x1C)) != NULL))
		{
			pszCurrPtr = pszEndPtr + 1;
			iCount--;
		}

		if(pszEndPtr != NULL)
		{
			pszCurrPtr = pszEndPtr + 1;
			pszEndPtr = strchr(pszCurrPtr, 0x1C);

			iDatalen = pszEndPtr - pszCurrPtr;

			if(iDatalen == 32)
			{
				pszCurrPtr += 4;
				memset(pszCurrPtr, '*', 12); /* Mask KSN by Adding the "*"s */

				pszCurrPtr += 16; /* Move to PIN Block (16, )Keep (+4) bytes data and Mask of PIN Block Data */
				iDatalen = pszEndPtr - pszCurrPtr;

				memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
			}
		}

		pszEndPtr = strstr(pszCurrPtr, "CVPI");
		if(pszEndPtr != NULL)
		{
			pszEndPtr += strlen("CVPI");
			pszEndPtr += 1;
			if((*pszEndPtr++ == 'V') && (*pszEndPtr++ == 'F'))
			{
				memset(szCVVLen, 0x00, sizeof(szCVVLen));
				szCVVLen[0] = *pszEndPtr;

				iCVVLen = atoi(szCVVLen);

				debug_sprintf(szDbgMsg, "%s: iCVVLen [%d]", __FUNCTION__, iCVVLen);
				APP_TRACE(szDbgMsg);

				pszCurrPtr = pszEndPtr + 1;
				pszEndPtr = pszCurrPtr + iCVVLen;

				memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
			}
		}

		pszEndPtr = strstr(pszCurrPtr, "TP");
		if(pszEndPtr != NULL && (*(pszEndPtr - 1) == 0x1C))
		{
			pszEndPtr += strlen("TP");
			pszCurrPtr = pszEndPtr;
			pszEndPtr +=  16; /* 16 is the no of Bytes for PIN Block during EMV Online PIN Verification*/;
			pszCurrPtr += 4; /* Keep (+4) bytes data and Mask the Rest of PIN Block Data */
			memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
		}

		pszEndPtr = strstr(pszCurrPtr, "TQ");
		if(pszEndPtr != NULL && (*(pszEndPtr - 1) == 0x1C))
		{
			pszEndPtr += strlen("TQ");
			pszCurrPtr = pszEndPtr;
			pszEndPtr +=  16; /* 16 is the no of Bytes for KSN */;
			pszCurrPtr += 4; /* Keep (+4) bytes data and Mask the Rest of KSN */
			memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
		}
	}

	return rv;

}

/*
 * ============================================================================
 * Function Name: maskVHIHostSensitiveData
 *
 * Description	: Parsing the VHI Host Request Message and Masking the Sensitive Data in the request, so that
 * the masked request data can be logged in app logging.
 *
 * Input Params	: Request message
 *
 * Output Params:
 * ============================================================================
 */
static int maskVHIHostSensitiveData(char * pszReqMsg)
{
	int			rv									= SUCCESS;
	int			iMaxPANLen							= 19;
	char		szMsgType[5]						= "";
	char		szBitMap[5]							= "";
	char *		pszStartPtr							= NULL;
	char *		pszCurrPtr							= NULL;
	char *		pszEndPtr							= NULL;
#ifdef DEBUG
	char	szDbgMsg[2560]							= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entry", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszReqMsg == NULL)
	{
		rv = FAILURE;
		return rv;
	}

	pszStartPtr = pszReqMsg;

	pszCurrPtr = pszStartPtr;
	pszCurrPtr = pszCurrPtr + strlen("REQUEST=BT") + 4 + 15 + 3 + 6;

	strncpy(szMsgType, pszCurrPtr, 4);
	pszCurrPtr += 4;

	strncpy(szBitMap, pszCurrPtr, 2);
	pszCurrPtr += 2;

	debug_sprintf(szDbgMsg, "%s: VHI_REQUEST, Message Type [%s]	Bit Map [%s]", __FUNCTION__, szMsgType, szBitMap);
	APP_TRACE(szDbgMsg);

	//Masking the sensitive Data in the Request which possibly contains TrackData, PIN BLOCK and KSN
	if((strcmp(szMsgType, "0100") == SUCCESS && strcmp(szBitMap, "10") == SUCCESS)
		|| (strcmp(szMsgType, "0200") == SUCCESS && strcmp(szBitMap, "10") == SUCCESS)
		|| (strcmp(szMsgType, "0200") == SUCCESS && strcmp(szBitMap, "13") == SUCCESS)
		|| (strcmp(szMsgType, "0100") == SUCCESS && strcmp(szBitMap, "14") == SUCCESS)
		|| (strcmp(szMsgType, "0200") == SUCCESS && strcmp(szBitMap, "14") == SUCCESS))
	{

		if((strcmp(szMsgType, "0100") == SUCCESS && (strcmp(szBitMap, "10") == SUCCESS || strcmp(szBitMap, "14") == SUCCESS))) // Debit or EBT Balance, It does not contain Transaction Amt Field
		{
			pszCurrPtr = pszCurrPtr + 6 + 10 + 6 + 6 + 6 + 3 + 10 + 4 + 3 + 12 + 3; // Move to the Location of TrackData
		}
		else
		{
			pszCurrPtr = pszCurrPtr + 6 + 9 + 10 + 6 + 6 + 6 + 3 + 10 + 4 + 3 + 12 + 3; // Move to the Location of TrackData
		}
		pszEndPtr = pszCurrPtr + 76;

		// Points to the start of Track Data
		while(pszCurrPtr && *pszCurrPtr == ' ')// Skips Spaces if any, until it points to a valid character
		{
			pszCurrPtr++;
		}

		if(pszCurrPtr)
		{
			pszCurrPtr += 4; // Leave out the first four digits of TrackData
		}

		memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */

		pszCurrPtr = pszEndPtr + 8;
		// Points to the start of PIN Block
		pszCurrPtr += 4;// Leave out the first four digits of PINBLOCK
		pszEndPtr = pszCurrPtr + 12;
		memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */

		if((strcmp(szMsgType, "0100") == SUCCESS && strcmp(szBitMap, "14") == SUCCESS)) // EBT Balance, It does not contain Certain Field
		{
			pszCurrPtr = pszEndPtr + 3 + 2 + 3 + 16;
		}
		else if(strcmp(szMsgType, "0100") == SUCCESS && strcmp(szBitMap, "10") == SUCCESS) // Debit Balance
		{
			pszCurrPtr = pszEndPtr + 3 + 2 + 16;
		}
		else if(strcmp(szMsgType, "0200") == SUCCESS && strcmp(szBitMap, "14") == SUCCESS) // EBT Sale
		{
			pszCurrPtr = pszEndPtr + 8 + 9 + 3 + 2 + 3 + 16;
		}
		else
		{
			pszCurrPtr = pszEndPtr + 8 + 9 + 3 + 2 + 16;
		}
		// Points to the start of KSN
		pszCurrPtr += 4;// Leave out the first four digits of KSN
		pszEndPtr = pszCurrPtr + 16;
		memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */

	}
	else if((strcmp(szMsgType, "0400") == SUCCESS && strcmp(szBitMap, "14") == SUCCESS)
			|| (strcmp(szMsgType, "0400") == SUCCESS && strcmp(szBitMap, "04") == SUCCESS)) //Masking the sensitive Data VOID Request with PIN_BLOCK and KSN.
	{
		pszEndPtr = pszCurrPtr + iMaxPANLen;
		while((pszCurrPtr) && (iMaxPANLen--) && (*pszCurrPtr == ' '))
		{
			pszCurrPtr++;
		}

		// Mask PAN if available in the VOID request.
		if(iMaxPANLen > 4)
		{
			pszCurrPtr += 4;
			memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
		}

		pszCurrPtr = pszEndPtr + 10 + 6 + 6 + 6 + 4 + 3 + 12 + 3 + 8;
		// Points to the start of PIN Block
		pszCurrPtr += 4;// Leave out the first four digits of KSN
		pszEndPtr = pszCurrPtr + 12;
		memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */

		pszCurrPtr = pszEndPtr + 8 + 3 + 8 + 2 + 16;
		// Points to the start of KSN
		pszCurrPtr += 4;// Leave out the first four digits of KSN
		pszEndPtr = pszCurrPtr + 16;
		memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */

	}
	else if((strcmp(szMsgType, "0100") == SUCCESS && strcmp(szBitMap, "07") == SUCCESS))
	{
		pszCurrPtr = pszCurrPtr + 173; // Move to the Location of Check No
		pszEndPtr = pszCurrPtr + 6;
		if(pszCurrPtr)
		{
			pszCurrPtr += 2;// Leave out the first four digits of TrackData
		}

		memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
	}
	else if((strcmp(szMsgType, "0100") == SUCCESS && strcmp(szBitMap, "07") == SUCCESS)) //Check Authorization
	{
		pszCurrPtr = pszCurrPtr + 173; // Move to the Location of Check No
		pszEndPtr = pszCurrPtr + 6;
		if(pszCurrPtr)
		{
			pszCurrPtr += 2;// Leave out the first four digits of TrackData
		}

		memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
	}
	else if((strcmp(szMsgType, "0200") == SUCCESS && strcmp(szBitMap, "16") == SUCCESS)) //VISA POS Check Conversion
	{
		pszCurrPtr = pszCurrPtr + 214; // Move to the Location of Check No
		pszEndPtr = pszCurrPtr + 6;
		if(pszCurrPtr)
		{
			pszCurrPtr += 2;// Leave out the first four digits of TrackData
		}

		memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
	}
	else if((strcmp(szMsgType, "0100") == SUCCESS && strcmp(szBitMap, "32") == SUCCESS) //Check Verification
			|| (strcmp(szMsgType, "0200") == SUCCESS && strcmp(szBitMap, "32") == SUCCESS)) //Check Conversion
	{
		pszCurrPtr = pszCurrPtr + 137; // Move to the Location of Check No
		pszEndPtr = pszCurrPtr + 6;
		if(pszCurrPtr)
		{
			pszCurrPtr += 2;// Leave out the first four digits of TrackData
		}

		memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
	}
	else if((strcmp(szMsgType, "0400") == SUCCESS && strcmp(szBitMap, "16") == SUCCESS)) //VISA POS Check Conversion Reversal (Void)
	{
		pszCurrPtr = pszCurrPtr + 113; // Move to the Location of Check No
		pszEndPtr = pszCurrPtr + 6;
		if(pszCurrPtr)
		{
			pszCurrPtr += 2;// Leave out the first four digits of TrackData
		}

		memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
	}
	else if((strcmp(szMsgType, "0400") == SUCCESS && strcmp(szBitMap, "32") == SUCCESS)) //Check Conversion Reversal (Void)
	{
		pszCurrPtr = pszCurrPtr + 116; // Move to the Location of Check No
		pszEndPtr = pszCurrPtr + 6;
		if(pszCurrPtr)
		{
			pszCurrPtr += 2;// Leave out the first two digits of TrackData
		}

		memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
	}
	else if(strcmp(szMsgType, "0400") == SUCCESS) //Masking the sensitive Data Other type of VOID Request
	{
		pszEndPtr = pszCurrPtr + iMaxPANLen;
		while((pszCurrPtr) && (iMaxPANLen--) && (*pszCurrPtr == ' '))
		{
			pszCurrPtr++;
		}

		// Mask PAN if available in the VOID request.
		if(iMaxPANLen > 4)
		{
			pszCurrPtr += 4;
			memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
		}
	}
	else if((strcmp(szMsgType, "0500") == SUCCESS && strcmp(szBitMap, "01") == SUCCESS) //Batch Enquiry & Batch Release
			|| (strcmp(szMsgType, "0500") == SUCCESS && strcmp(szBitMap, "60") == SUCCESS)	// Gift Card Batch Totals
			|| (strcmp(szMsgType, "0800") == SUCCESS && strcmp(szBitMap, "01") == SUCCESS)	// Terminal Validation
			|| (strcmp(szMsgType, "0800") == SUCCESS && strcmp(szBitMap, "05") == SUCCESS)) //Lane Validation
	{
		// Nothing to Mask
	}
	else if((strcmp(szMsgType, "0100") == SUCCESS && strcmp(szBitMap, "50") == SUCCESS)) //Masking the Token Query Request
	{
		pszCurrPtr = pszCurrPtr + 6 + 10 + 6 + 6 + 6 + 3 + 10 + 4 + 3 + 12 + 3; // No Amount Details resent
		pszEndPtr = pszCurrPtr + 76;

		// Points to the start of Track Data
		while(pszCurrPtr && *pszCurrPtr == ' ')// Skips Spaces if any, until it points to a valid character
		{
			pszCurrPtr++;
		}

		if(pszCurrPtr)
		{
			pszCurrPtr += 4;// Leave out the first four digits of TrackData
		}

		memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
	}
	else  //Masking the sensitive Data Other type Request like CREDIT Sale, Refund, POST_AUTH
	{

		if(strlen(pszCurrPtr) > 153)
		{
			pszCurrPtr = pszCurrPtr + 6 + 9 + 10 + 6 + 6 + 6 + 3 + 10 + 4 + 3 + 12 + 3;
			pszEndPtr = pszCurrPtr + 76;

			// Points to the start of Track Data
			while(pszCurrPtr && *pszCurrPtr == ' ')// Skips Spaces if any, until it points to a valid character
			{
				pszCurrPtr++;
			}

			if(pszCurrPtr)
			{
				pszCurrPtr += 4;// Leave out the first four digits of TrackData
			}

			if(pszEndPtr > pszCurrPtr)// For Token Based Transaction the Track data is filled with all spaces, hence we take care not to mask something else.
			{
				memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
			}
		}

	}

	return rv;

}

/*
 * ============================================================================
 * Function Name: maskElavonHostSensitiveData
 *
 * Description	: Parsing the Elavon XML Host Request Message and Masking the Sensitive Data in the request, so that
 * the masked request data can be logged in app logging.
 *
 * Input Params	: Request message
 *
 * Output Params:
 * ============================================================================
 */
static int maskElavonHostSensitiveData(char * szXMLReqMsg)
{
	int			rv															= SUCCESS;
	int			iCnt														= 0;
	char *		pszAPIName													= NULL;
	char 		pszStartTag[MAX_XML_NODE_SIZE + MAX_XML_NODE_SIZE + 5 + 10]	= "";
	char 		pszEndTag[MAX_XML_NODE_SIZE + MAX_XML_NODE_SIZE + 5 + 10]	= "";
	char *		pszStartPtr													= NULL;
	char *		pszCurrPtr													= NULL;
	char *		pszEndPtr													= NULL;
	char *		pszAPIList[]												= {"0003", "0018", "0019", "0050", NULL};

	if(szXMLReqMsg == NULL)
	{
		rv = FAILURE;
		return rv;
	}

	memset(pszStartTag, 0x00, sizeof(pszStartTag));
	memset(pszEndTag, 0x00, sizeof(pszEndTag));

	pszStartPtr = szXMLReqMsg;
	while(((pszAPIName = pszAPIList[iCnt]) != NULL) && strlen(pszAPIName))
	{
		sprintf(pszStartTag, "<Field_Number>%s</Field_Number>", pszAPIName);


		if((pszCurrPtr = strstr(pszStartPtr, pszStartTag)) != NULL) /* Search for the Start Tag */
		{
			pszCurrPtr += strlen(pszStartTag);

			if(strncmp(pszCurrPtr, "<Field_Value>", strlen("<Field_Value>")) == SUCCESS)
			{
				strcpy(pszEndTag, "</Field_Value>");
				if((pszEndPtr = strstr(pszCurrPtr, pszEndTag)) != NULL) /* Search for the End Tag */
				{
					pszCurrPtr += strlen("<Field_Value>");

					if((pszEndPtr - pszCurrPtr) > 4) /* Keep 4 bytes data and Mask the Rest of the Data */
					{
						pszCurrPtr += 4;
					}

					memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
				}
			}
		}
		iCnt++;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: maskSensitiveNodeData
 *
 * Description	: Travel Each Node of the XML Request or Response and mask the XML Node, if present in the Mask List.
 *
 * Here the Data is masked in the incoming data itself, which is a buffer data from the queue unlike signature data. No need to allocate
 * and free the Data
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int maskSensitiveNodeData(char * szXMLReqMsg)
{
	int			rv									= SUCCESS;
	int			iCnt								= 0;
	char *		pszNodeName							= NULL;
	char 		pszStartTag[MAX_XML_NODE_SIZE+5]	= "";
	char 		pszEndTag[MAX_XML_NODE_SIZE+5]		= "";
	char *		pszStartPtr							= NULL;
	char *		pszCurrPtr							= NULL;
	char *		pszEndPtr							= NULL;

	if(szXMLReqMsg == NULL)
	{
		rv = FAILURE;
		return rv;
	}

	memset(pszStartTag, 0x00, sizeof(pszStartTag));
	memset(pszEndTag, 0x00, sizeof(pszEndTag));

	pszStartPtr = szXMLReqMsg;
//	while(((pszNodeName = gszMaskXMLNode[iCnt]) != NULL) && strlen(pszNodeName) && (iCnt < MAX_XML_MASK_NODE))	// CID-67486: 22-Jan-16: MukeshS3: Index
	while((iCnt < MAX_XML_MASK_NODE) && ((pszNodeName = gszMaskXMLNode[iCnt]) != NULL) && strlen(pszNodeName))	// overflow check should be evaluated first
	{
		sprintf(pszStartTag, "<%s>", pszNodeName);
		if((pszCurrPtr = strstr(pszStartPtr, pszStartTag)) != NULL) /* Search for the Start Tag */
		{
			pszCurrPtr += strlen(pszStartTag);
			sprintf(pszEndTag, "</%s>", pszNodeName);
			if((pszEndPtr = strstr(pszCurrPtr, pszEndTag)) != NULL) /* Search for the End Tag */
			{
				if((pszEndPtr - pszCurrPtr) > 4) /* Keep 4 bytes data and Mask the Rest of the Data */
				{
					pszCurrPtr += 4;
				}
				memset(pszCurrPtr, '*', (pszEndPtr - pszCurrPtr)); /* Mask it by Adding the "*"s */
			}
		}
		iCnt++;
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: local_svcSystem
 *
 * Description	: This API is wrapper to svcSystem
 *
 * Input Params	: char*
 *
 * Output Params:
 * ============================================================================
 */
static int local_svcSystem(const char *command)
{
	int retval;
	struct sigaction sigact, saveact;

	sigact.sa_handler = SIG_DFL;
	sigemptyset(&sigact.sa_mask);
	sigact.sa_flags = 0;
	//save the existing SIGCHLD handler in saveact and set the handler to SIG_DFL.
	sigaction(SIGCHLD, &sigact, &saveact);

	retval = system(command);

	//Restore the original saved handler.
	sigaction(SIGCHLD, &saveact, NULL);
	return retval;
}

#if 0
/*
 * ============================================================================
 * Function Name: isNodeInMaskList
 *
 * Description	: Check Whether the Received Node Name is in the List of XML Mask List present in the file.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int isNodeInMaskList(xmlChar* nodeName)
{
	int			rv				= ERR_NODE_NOT_IN_LIST;
	int			iCnt			= 0;
	char *		szNodeName		= NULL;

	while(((szNodeName = gszMaskXMLNode[iCnt]) != NULL) && (nodeName != NULL) && (iCnt < MAX_XML_MASK_NODE))
	{
		if(xmlStrcmp(nodeName, (const xmlChar *) szNodeName) == SUCCESS)
		{
			rv = SUCCESS;
			break;
		}
		iCnt++;
	}
	return rv;
}

/*
 * ============================================================================
 * Function Name: checkEachNodeInXML2Mask
 *
 * Description	: Check Each Node by Node in the XML Request Root Node and check whether the node needs to be masked, if so mask it. Done Recursively on XML Tags of the XML Data.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int checkEachNodeInXML2Mask(xmlNode* currNode)
{
	int			rv				= SUCCESS;
	xmlChar *	nodeContent 	= NULL;
	xmlChar		maskContent[25]	= "";
#if 0
#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif
#endif

	if((currNode == NULL) || (strcmp((char*)currNode->name, "RECEIPT_DATA") == SUCCESS)
			              || (currNode->type == XML_TEXT_NODE))
	{
		return rv;
	}

	if(currNode->type == XML_ELEMENT_NODE)
	{
		rv = isNodeInMaskList((xmlChar*)currNode->name);
		if(rv == SUCCESS)
		{
			nodeContent = xmlNodeGetContent(currNode);
			if(nodeContent != NULL && (strlen((char*)nodeContent) > 4))
			{
				strncpy((char*)maskContent, (char*)nodeContent, 4);
			}
			strcat((char*)maskContent, "****");

			xmlNodeSetContent(currNode, maskContent);
			xmlFree(nodeContent);
		}
		checkEachNodeInXML2Mask(currNode->xmlChildrenNode);
	}

	if(currNode->next)
	{
		checkEachNodeInXML2Mask(currNode->next);
	}


	return rv;
}

/*
 * ============================================================================
 * Function Name: maskXMLNodeData
 *
 * Description	: Travel Each Node of the XML Request or Response and mask the XML Node, if present in the Mask List
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int maskXMLNodeData(char * szXMLReqMsg, xmlChar **pszMaskedReq)
{
	int			rv					= SUCCESS;
	int			iLen				= 0;
	int			iPtrSize			= 0;
	xmlDoc *	docPtr				= NULL;
	xmlNode *	rootPtr				= NULL;
	xmlChar		maskContent[25]		= "";
#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entry ---------", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(maskContent, 0x00, sizeof(maskContent));

	while(1)
	{
		if(szXMLReqMsg == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML message using the libxml API */
		iLen = strlen(szXMLReqMsg);

		docPtr = xmlParseMemory(szXMLReqMsg, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse the XML message",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty XML data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		//  Check Each Node for Sensitive Data and Mask it
		checkEachNodeInXML2Mask(rootPtr);

		// Dump the final Ptr to pszMaskedReq and return it
		xmlDocDumpFormatMemory(docPtr, pszMaskedReq, &iPtrSize, 0);

		break;
	}

	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: maskXMLSignatureData
 *
 * Description	: This Function only the Signature Data Present in the SSI_REQUEST or SCI_RESPONSE or DHI_REQUEST
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int maskXMLSignatureData(char * szXMLReqMsg, xmlChar **pszMaskedReq)
{
	int			rv					= SUCCESS;
	int			iLen				= 0;
	int			iPtrSize			= 0;
	char 		szNodeName[25]			= "";
	xmlDoc *	docPtr				= NULL;
	xmlNode *	rootPtr				= NULL;
	xmlNode *	curNode				= NULL;
	xmlChar *	nodeContent 		= NULL;
	xmlChar		maskContent[150]	= "";
#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entry ---------", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(maskContent, 0x00, sizeof(maskContent));

	while(1)
	{
		if(szXMLReqMsg == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML message using the libxml API */
		iLen = strlen(szXMLReqMsg);

		debug_sprintf(szDbgMsg, "%s: The length of the message [%d]", __FUNCTION__, iLen);
		APP_TRACE(szDbgMsg);

		docPtr = xmlParseMemory(szXMLReqMsg, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse the XML message",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty XML data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		strcpy(szNodeName, "SIGNATUREDATA");
		if(szNodeName != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Node to be masked [%s]", __FUNCTION__, szNodeName);
			APP_TRACE(szDbgMsg);

			memset(maskContent, 0x00, sizeof(maskContent));

			/* Get the current node in the xml data from where the search of
			 * required fields should begin */
			curNode = findNodeInXMLData(szNodeName, rootPtr);
			if(curNode == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Cant find [%s] node in xml file",
													__FUNCTION__, szNodeName);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_XML_MSG;
				break;
			}

			if(curNode != NULL)
			{
				nodeContent = xmlNodeGetContent(curNode);
				if(nodeContent != NULL && (strlen((char*)nodeContent) > 100))
				{
					strncpy((char*)maskContent, (char*)nodeContent, 100);
				}
				strcat((char*)maskContent, "****");

				debug_sprintf(szDbgMsg, "%s: maskContent ----- [%s]", __FUNCTION__, (char*)maskContent);
				APP_TRACE(szDbgMsg);

				xmlNodeSetContent(curNode, maskContent);
				xmlFree(nodeContent);

				xmlDocDumpFormatMemory(docPtr, pszMaskedReq, &iPtrSize, 0);
			}
		}
		break;
	}

	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: maskXMLPayLoadCData
 *
 * Description	: Function to mask the CDATA of Payload XML Tag in RC Request and RC Response (Rapid Connect)
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int maskXMLPayLoadCData(char * szXMLReqMsg, xmlChar **pszMaskedReq)
{
	int			rv					= SUCCESS;
	int			iLen				= 0;
	int			iPayLoadLen			= 0;
	int			iPtrSize			= 0;
	char 		szNodeName[25]		= "";
	xmlChar * 	payLoadContent		= NULL;
	xmlChar		maskContent[25]		= "";
	xmlDoc *	docPtr				= NULL;
	xmlDoc *	payLoadDocPtr		= NULL;
	xmlNode *	rootPtr				= NULL;
	xmlNode *	curNode				= NULL;
	xmlNode *	payLoadRootPtr		= NULL;
#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: Entry --------- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(maskContent, 0x00, sizeof(maskContent));

	while(1)
	{
		if(szXMLReqMsg == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Parse the XML message using the libxml API */
		iLen = strlen(szXMLReqMsg);

		docPtr = xmlParseMemory(szXMLReqMsg, iLen);
		if(docPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse the XML message",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		/* Get the root node for the xml data */
		rootPtr = xmlDocGetRootElement(docPtr);
		if(rootPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Empty XML data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = ERR_INV_XML_MSG;
			break;
		}

		strcpy(szNodeName, "Payload");
		if(szNodeName != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Node to be masked [%s]", __FUNCTION__, szNodeName);
			APP_TRACE(szDbgMsg);

			memset(maskContent, 0x00, sizeof(maskContent));

			/* Get the current node in the xml data from where the search of
			 * required fields should begin */
			curNode = findNodeInXMLData(szNodeName, rootPtr);
			if(curNode == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Cant find [%s] node in xml file",
													__FUNCTION__, szNodeName);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_XML_MSG;
				break;
			}
			payLoadContent = xmlNodeGetContent(curNode);
			if(payLoadContent == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: payLoadContent is Not Available ",
													__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_XML_MSG;
				break;
			}

#ifdef DEVDEBUG
			if(strlen((char *)payLoadContent) < 4500)
			{
				debug_sprintf(szDbgMsg, "%s: payLoadContent [%s]", __FUNCTION__, payLoadContent);
				APP_TRACE(szDbgMsg);
			}
#endif
			iPayLoadLen = strlen((char*)payLoadContent);
			payLoadDocPtr = xmlParseMemory((char*)payLoadContent, iPayLoadLen);
			if(payLoadDocPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to parse the XML payLoadDocPtr message", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_XML_MSG;
				break;
			}

			/* Get the root node for the xml data */
			payLoadRootPtr = xmlDocGetRootElement(payLoadDocPtr);
			if(payLoadRootPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Empty XML payLoadRootPtr data",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_XML_MSG;
				break;
			}

			checkEachNodeInXML2Mask(payLoadRootPtr); //Check in the PayLoad CDATA to mask

			xmlDocDumpFormatMemory(payLoadDocPtr, pszMaskedReq, &iPtrSize, 0);
		}
		break;
	}

	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: findNodeInXMLData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static xmlNode * findNodeInXMLData(char * szNodeId, xmlNode * curNode)
{
	xmlNode *	nxtNode			= NULL;
	xmlNode *	reqdNode		= NULL;

	if((curNode->type == XML_ELEMENT_NODE) &&
		(xmlStrcmp(curNode->name, (const xmlChar *) szNodeId) == SUCCESS) ) {
		/* Element found */
		reqdNode = curNode;
	}
	else {
		/* Search in other places */
		/* Go for the child node */
		nxtNode = curNode->xmlChildrenNode;
		if(nxtNode != NULL) {
			reqdNode = findNodeInXMLData(szNodeId, nxtNode);
		}

		if(reqdNode == NULL) {
			/* If all the child nodes are exhausted, go for the sibling node */
			nxtNode = curNode->next;
			if(nxtNode != NULL) {
				reqdNode = findNodeInXMLData(szNodeId, nxtNode);
			}
		}
	}

	return reqdNode;
}
#endif
#if 0
/*
 * ============================================================================
 * Function Name: scaMalloc
 *
 * Description	: Wrapper for malloc, will print num of bytes and starting
 * 					address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void *scaMalloc(unsigned int iSize, int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalAllocMem		= 0;
	void *		tmpPtr				= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	tmpPtr = malloc(iSize);

	if(tmpPtr != NULL)

	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] StartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				__FUNCTION__, tmpPtr, iSize, pszCallerFuncName, iLineNum);
		APP_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", __FUNCTION__);
		APP_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: scaReAlloc
 *
 * Description	: Wrapper for realloc, will print num of bytes and starting
 * 					and original address also
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern void *scaReAlloc(void *ptr, unsigned int iSize, int iLineNum, char * pszCallerFuncName)
{
	//static long int	iTotalReallocMem= 0;
	void *		tmpPtr				= NULL;
	char		szOrigAddr[16]		= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	//Storing Original Address
	memset(szOrigAddr, 0x00, sizeof(szOrigAddr));
	sprintf(szOrigAddr, "%p", ptr);

	tmpPtr = realloc(ptr, iSize);

	if(tmpPtr != NULL)
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] OrigStartAddr[%s] NewStartAddr[%p] CurAllocBytes[%u] called by [%s] from Line[%d]",
				__FUNCTION__, szOrigAddr, tmpPtr, iSize, pszCallerFuncName, iLineNum);
		APP_MEM_TRACE(szDbgMsg);
	}
	else
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] Failed", __FUNCTION__);
		APP_MEM_TRACE(szDbgMsg);
	}

	return tmpPtr;
}

/*
 * ============================================================================
 * Function Name: scaFree
 *
 * Description	: Wrapper for free, will print going to be free
 * 					address also and make the freed pointer points to NULL
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void scaFree(void **pptr, int iLineNum, char * pszCallerFuncName)
{
	//size_t *sizePtr					= ((size_t *)ptr) - 1;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(*pptr != NULL)
	{
		debug_mem_sprintf(szDbgMsg, "%s: [MEMDEBUG] FreedAddr[%p] called by [%s] from Line[%d]",
				__FUNCTION__, *pptr, pszCallerFuncName, iLineNum);
		APP_MEM_TRACE(szDbgMsg);
		free(*pptr);
		*pptr = NULL;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: [MEMDEBUG] NULL params Passed ", __FUNCTION__);
		APP_MEM_TRACE(szDbgMsg);
	}
}
#endif
/*
 * ============================================================================
 * End of file appLogAPIs.c
 * ============================================================================
 */
