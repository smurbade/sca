/******************************************************************
*                     sciConfigParams.c                           *
*******************************************************************
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis which loads and reads the 		  *
* 			   config parameters required for the SCI Module       *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                kranthik1                                        *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <svc.h>
#include <syslog.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include "common/common.h"
#include "sci/sciConfigDef.h"
#include "appLog/appLogAPIs.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);


static SCI_CFG_STYPE sciSettings;

static void initializeSCISettings();
static int	loadReceiptSettings();
static int  getMaxReceiptLineLenFromFile(char*);


#define RECEIPT_FILE_NAME "/home/usr1/flash/xmlReceiptFile.xml"
/*
 * ============================================================================
 * Function Name: loadSCIConfigParams
 *
 * Description	: This function reads all the config parameters from the
 * 				  config.ur1 file and store them in the static structure.
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int loadSCIConfigParams()
{
	int		rv					= 0;
	int		iVal				= 0;
	int		iLen				= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szTemp[20]			= "";
	char	szProcessor[20]		= "";
	char 	szErrMsg[256]		= "";
	char	szAppLogData[300]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));


	initializeSCISettings();

	iLen = sizeof(szTemp);
	/* ------------ Check for listening port ----------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, LISTEN_PORT, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		/* KranthiK1: Changed the lower bound to 1024 as these were not supposed to be used*/
		if( (iVal > 1024) && (iVal <= 65535))
		{
			debug_sprintf(szDbgMsg, "%s: Primary Port to listen for POS connections=%d",
															__FUNCTION__, iVal);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Listening to Primary Port [%d] for POS Connections", iVal);
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

			sciSettings.listenPort = iVal;
		}
		else
		{
			/* Incorrect value for EXT interface listening port */
			debug_sprintf(szDbgMsg,
					"%s: Incorrect val for primary listening port, setting default %d",
											__FUNCTION__, DFLT_LISTEN_PORT);
			APP_TRACE(szDbgMsg);

			sciSettings.listenPort = DFLT_LISTEN_PORT;
			sprintf(szTemp, "%d", DFLT_LISTEN_PORT);
			putEnvFile(SECTION_DEVICE, LISTEN_PORT, szTemp);
		}
	}
	else
	{
		/* No value found for EXT interface listening port */
		debug_sprintf(szDbgMsg,
					"%s: No val found for primary listening port, setting default %d",
											__FUNCTION__, DFLT_LISTEN_PORT);
		APP_TRACE(szDbgMsg);

		sciSettings.listenPort = DFLT_LISTEN_PORT;
		sprintf(szTemp, "%d", DFLT_LISTEN_PORT);
		putEnvFile(SECTION_DEVICE, LISTEN_PORT, szTemp);
	}

	/* --------------- Check for secondary port is Enabled ---------------- */
		memset(szTemp, 0x00, iLen);
		rv = getEnvFile(SECTION_DEVICE, SCND_PORT_ENABLED, szTemp, iLen);
		if(rv > 0)
		{
			/* Value found ... Setting Bool value accordingly */
			if( (*szTemp == 'y') || (*szTemp == 'Y') )
			{
				sciSettings.scndPortEnabled = PAAS_TRUE;
				debug_sprintf(szDbgMsg, "%s: Secondary Port ENABLED",
																	__FUNCTION__);
			}
			else
			{
				sciSettings.scndPortEnabled = PAAS_FALSE;
				debug_sprintf(szDbgMsg, "%s: Secondary Port DISABLED",
																	__FUNCTION__);
			}
			rv = SUCCESS;
		}
		else
		{
			/* Value not found.. setting default */
			sciSettings.scndPortEnabled = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: Secondary Port disabled by default",
																	__FUNCTION__);
		}
		APP_TRACE(szDbgMsg);

	/* ------------ Check for listening secondary port ----------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, LISTEN_SCND_PORT, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		/* KranthiK1: Changed the lower bound to 1024 as these were not supposed to be used*/
		if( (iVal > 1024) && (iVal <= 65535))
		{
			debug_sprintf(szDbgMsg, "%s: Secondary Port to listen for POS connections=%d",
															__FUNCTION__, iVal);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Listening to Secondary Port [%d] for POS Connections", iVal);
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

			sciSettings.listenScndPort = iVal;
		}
		else
		{
			/* Incorrect value for EXT interface listening port */
			debug_sprintf(szDbgMsg,
					"%s: Incorrect val for secodnary listening port, setting default %d",
											__FUNCTION__, DFLT_LISTEN_SCND_PORT);
			APP_TRACE(szDbgMsg);

			sciSettings.listenScndPort = DFLT_LISTEN_SCND_PORT;
			sprintf(szTemp, "%d", DFLT_LISTEN_SCND_PORT);
			putEnvFile(SECTION_DEVICE, LISTEN_SCND_PORT, szTemp);
		}
	}
	else
	{
		/* No value found for EXT interface listening port */
		debug_sprintf(szDbgMsg,
					"%s: No val found for secondary listening port, setting default %d",
											__FUNCTION__, DFLT_LISTEN_SCND_PORT);
		APP_TRACE(szDbgMsg);

		sciSettings.listenScndPort = DFLT_LISTEN_SCND_PORT;
		sprintf(szTemp, "%d", DFLT_LISTEN_SCND_PORT);
		putEnvFile(SECTION_DEVICE, LISTEN_SCND_PORT, szTemp);
	}

	/* ------------ Check for Max POS Connection Settings ----------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, MAX_POS_CONNECTIONS, szTemp, iLen);
	if(rv > 0)
	{
		iVal = atoi(szTemp);
		/* T_RaghavendranR1: Check for the Max POS Connections Range */
		if( (iVal >= 1) && (iVal <= DFLT_MAX_POS_CONNECTIONS))
		{
			debug_sprintf(szDbgMsg, "%s: Maximum POS Connections Allowed [%d]", __FUNCTION__, iVal);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				sprintf(szAppLogData, "Maximum POS Connections Allowed [%d] ", iVal);
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

			/*In this case, POS Connections will be restricted beyond the given value*/
			sciSettings.maxPOSConnections = iVal;
		}
		else
		{
			/* Incorrect value for Max POS Connections */
			debug_sprintf(szDbgMsg,
					"%s: Incorrect Value for Max POS Connections, Setting it to Default [%d]",
											__FUNCTION__, DFLT_MAX_POS_CONNECTIONS);
			APP_TRACE(szDbgMsg);

			/*In this case, We will keep the value to DFLT_MAX_POS_CONNECTIONS*/
			sciSettings.maxPOSConnections = DFLT_MAX_POS_CONNECTIONS;
		}
	}
	else
	{
		/* No value found for Max POS Connections */
		debug_sprintf(szDbgMsg,
					"%s: No Value Found for Max POS Connections, Setting it to Default [%d]",
											__FUNCTION__, DFLT_MAX_POS_CONNECTIONS);
		APP_TRACE(szDbgMsg);

		/*In this case, We will keep the value to -1,and follow circular method of POS connections*/
		sciSettings.maxPOSConnections = -1;
	}

	/* ------------- Check if POS Authentication required is Enabled ------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_DEVICE, POS_AUTHENTICATION_REQD, szTemp, iLen);
	if(rv > 0)
	{
		if((*szTemp == 'N') || (*szTemp == 'n'))
		{
			memset(szProcessor, 0x00, sizeof(szProcessor));
			getEnvFile(SECTION_DHI, PROCESSOR, szProcessor, sizeof(szProcessor));

			debug_sprintf(szDbgMsg, "%s: Processor [%s], DHI Enabled [%d]", __FUNCTION__, szProcessor, isDHIEnabled());
			APP_TRACE(szDbgMsg);

			if(isDHIEnabled() && (strlen(szProcessor) > 0) && (strcasecmp(szProcessor, "elvn") == SUCCESS))
			{
				debug_sprintf(szDbgMsg, "%s: POS Authentication Not Required", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				sciSettings.bPOSAuthReqd = PAAS_FALSE;
			}
			else
			{
				sciSettings.bPOSAuthReqd = PAAS_TRUE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: POS Authentication Required", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			sciSettings.bPOSAuthReqd = PAAS_TRUE;
		}
	}
	else
	{
		/* No value found for POS Authentication  flag*/
		debug_sprintf(szDbgMsg,"%s: No Value Found for POS Authentication.", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		sciSettings.bPOSAuthReqd = PAAS_TRUE;
	}

	/* --------------- Check for Receipt Enabled ---------------- */
	memset(szTemp, 0x00, iLen);
	rv = getEnvFile(SECTION_PAYMENT, RECEIPT_ENABLED, szTemp, iLen);
	if(rv > 0)
	{
		/* Value found ... Setting Bool value accordingly */
		if( (*szTemp == 'y') || (*szTemp == 'Y') )
		{
			sciSettings.receiptEnabled = PAAS_TRUE;
			debug_sprintf(szDbgMsg, "%s: Receipt Generation ENABLED",
																__FUNCTION__);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Receipt Generation Enabled in the Configuration Settings");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}
		}
		else
		{
			sciSettings.receiptEnabled = PAAS_FALSE;
			debug_sprintf(szDbgMsg, "%s: Receipt Generation DISABLED",
																__FUNCTION__);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Receipt Generation Enabled in the Configuration Settings");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}
		}
		rv = SUCCESS;
	}
	else
	{
		/* Value not found.. setting default */
		sciSettings.receiptEnabled = PAAS_TRUE;
		debug_sprintf(szDbgMsg, "%s: Receipt Generation ENABLED by default",
																__FUNCTION__);
	}
	APP_TRACE(szDbgMsg);

	if(sciSettings.receiptEnabled == PAAS_TRUE)
	{
		if(isReceiptTemplatePresent() != PAAS_TRUE)
		{
			sprintf(szErrMsg, "%s: Receipt Template file NOT present",
																__FUNCTION__);
			APP_TRACE(szErrMsg);
			syslog(LOG_ERR|LOG_USER, szErrMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Receipt Template File Not Present to Load the Receipt Settings");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

			rv =  FAILURE;
		}
		else
		{
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Receipt Template File Present to Load the Settings");
				addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
			}

			sciSettings.maxReceiptLineLen = getMaxReceiptLineLenFromFile(RECEIPT_FILE_NAME);
			rv = SUCCESS;
		}
	}

	if(rv == SUCCESS)
	{
		/* Load the receipt settings */
		rv = loadReceiptSettings(&(sciSettings.stRcptCfg));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error in loading Receipt settings",
									__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Load the Receipt Settings From the File");
				addAppEventLog(SCA, PAAS_FAILURE, START_UP, szAppLogData, NULL);
			}

			rv = FAILURE;
		}
	}
	return rv;
}

/*
 * ============================================================================
 * Function Name: initializeSCISettings
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params: none
 * ============================================================================
 */
static void initializeSCISettings()
{
	memset(&sciSettings, 0x00, sizeof(SCI_CFG_STYPE));
}

/*
 * ============================================================================
 * Function Name: isReceiptEnabled
 *
 * Description	: This function informs the caller if the receipt is enabled
 * 					for PaaS
 *
 * Input Params	: none
 *
 * Output Params: PAAS_TRUE / PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isReceiptEnabled()
{
	return sciSettings.receiptEnabled;
}

/*
 * ============================================================================
 * Function Name: isPOSAuthReqdFromConfig
 *
 * Description	: Function returns whether POS AUthentication is required or not from Config
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
PAAS_BOOL isPOSAuthReqdFromConfig()
{
	return sciSettings.bPOSAuthReqd;
}

static int getMaxReceiptLineLenFromFile(char* szXMLReceipt)
{
	int			maxReceiptLineLen	= 0;
	xmlDoc *	docPtr			 	= NULL;
	xmlNode *	rootPtr				= NULL;
	xmlChar *	receiptLineLen		= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	 	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(isReceiptTemplatePresent())
	{
		while(1)
		{
			/* Parse the XML file using the libxml API */
			docPtr = xmlParseFile(szXMLReceipt);
			if(docPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);
				maxReceiptLineLen = 40;
				break;
			}

			/* Find the root node */
			rootPtr = xmlDocGetRootElement(docPtr);
			if(rootPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Empty xml data",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				maxReceiptLineLen = 40;
				break;
			}

			receiptLineLen = xmlGetProp(rootPtr, (const xmlChar *)"MaxReceiptLineLen");
			if (receiptLineLen != NULL)
			{
				maxReceiptLineLen = atoi((char*)receiptLineLen);
				xmlFree(receiptLineLen);
			}
			else
			{
				maxReceiptLineLen = 40;
			}
			break;
		}
	}

	if (docPtr != NULL)
	{
		xmlFree(docPtr);
	}

	if (rootPtr != NULL)
	{
		xmlFree(rootPtr);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, maxReceiptLineLen);
	APP_TRACE(szDbgMsg);

	return maxReceiptLineLen;
}

/*
 * ============================================================================
 * Function Name: loadReceiptSettings
 *
 * Description	: This function is responsible for loading the reciept related
 *					paramaters.
 *
 * Input Params	: none
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int loadReceiptSettings(RCPT_DATA_PTYPE rcptCfgPtr)
{
	int		rv					= SUCCESS;
	int		iCnt				= 0;
	int		maxReceiptLineLen	= 0;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szTemp[256]			= "";
	char	szParam[16]			= "";
	char	szAppLogData[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "Loading the Receipt Settings");
		addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
	}

	memset(rcptCfgPtr, 0x00, sizeof(RCPT_DATA_STYPE));

	maxReceiptLineLen = getMaxReceiptLineLen();

	/* Check for the headers */
	for(iCnt = 0; iCnt < 4; iCnt++)
	{
		sprintf(szParam, "%s%d", RCT_HEADER_PREFIX, iCnt + 1);

		rcptCfgPtr->headers[iCnt] = (char*)malloc(maxReceiptLineLen+1);
		if (rcptCfgPtr->headers[iCnt] == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No memory",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break; //CID 67354 (#1 of 3): Unused value (UNUSED_VALUE) T_RaghavendranR1. No Effect of setting rv to FAILURE as it is overwritten below.
		}
		memset(rcptCfgPtr->headers[iCnt], 0x00, maxReceiptLineLen+1);

		rv = getEnvFile(SECTION_RECEIPT, szParam, szTemp, maxReceiptLineLen+1);
		if(rv > 0)
		{

			debug_sprintf(szDbgMsg, "%s: Header = [%s], len = [%d]",
												__FUNCTION__, szTemp, maxReceiptLineLen);
			APP_TRACE(szDbgMsg);

			memcpy(rcptCfgPtr->headers[iCnt], szTemp, maxReceiptLineLen);

			debug_sprintf(szDbgMsg,"%s: RCPT Header - %d = [%s]", __FUNCTION__,
										iCnt + 1, rcptCfgPtr->headers[iCnt]);
		}
		else
		{
			/* Value not found.. Ignoring */
			debug_sprintf(szDbgMsg, "%s: No value present for [%s] in [%s]",
									__FUNCTION__, szParam, SECTION_RECEIPT);
		}

		APP_TRACE(szDbgMsg);
	}

	/* Check for the footers */
	for(iCnt = 0; iCnt < 4; iCnt++)
	{
		sprintf(szParam, "%s%d", RCT_FOOTER_PREFIX, iCnt + 1);

		rcptCfgPtr->footers[iCnt] = (char*)malloc(maxReceiptLineLen+1);
		if (rcptCfgPtr->footers[iCnt] == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No memory",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break; //CID 67354 (#1 of 3): Unused value (UNUSED_VALUE) T_RaghavendranR1. No Effect of setting rv to FAILURE as it is overwritten below.
		}
		memset(rcptCfgPtr->footers[iCnt], 0x00, maxReceiptLineLen+1);

		rv = getEnvFile(SECTION_RECEIPT, szParam, szTemp, maxReceiptLineLen+1);
		if(rv > 0)
		{
			debug_sprintf(szDbgMsg, "%s: Footer = [%s], len = [%d]",
												__FUNCTION__, szTemp, maxReceiptLineLen);
			APP_TRACE(szDbgMsg);

			memcpy(rcptCfgPtr->footers[iCnt], szTemp, maxReceiptLineLen);
		}
		else
		{
			/* Value not found.. Ignoring */
			debug_sprintf(szDbgMsg, "%s: No value present for [%s] in [%s]",
									__FUNCTION__, szParam, SECTION_RECEIPT);
		}

		APP_TRACE(szDbgMsg);
	}

	/* Check for the disclaimers */
	for(iCnt = 0; iCnt < 4; iCnt++)
	{
		sprintf(szParam, "%s%d", RCT_DCLAIM_PREFIX, iCnt + 1);

		rcptCfgPtr->disclaim[iCnt] = (char*)malloc(maxReceiptLineLen+1);
		if (rcptCfgPtr->disclaim[iCnt] == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No memory",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break; //CID 67354 (#1 of 3): Unused value (UNUSED_VALUE) T_RaghavendranR1. No Effect of setting rv to FAILURE as it is overwritten below.
		}
		memset(rcptCfgPtr->disclaim[iCnt], 0x00, maxReceiptLineLen+1);

		rv = getEnvFile(SECTION_RECEIPT, szParam, szTemp, maxReceiptLineLen+1);
		if(rv > 0)
		{
			debug_sprintf(szDbgMsg, "%s: Disclaimer = [%s], len = [%d]",
												__FUNCTION__, szTemp, maxReceiptLineLen);
			APP_TRACE(szDbgMsg);

			memcpy(rcptCfgPtr->disclaim[iCnt], szTemp, maxReceiptLineLen);
		}
		else
		{
			/* Value not found.. Ignoring */
			debug_sprintf(szDbgMsg, "%s: No value present for [%s] in [%s]",
									__FUNCTION__, szParam, SECTION_RECEIPT);
		}

		APP_TRACE(szDbgMsg);
	}

	/* Check for the dcc disclaimers */
	for(iCnt = 0; iCnt < 4; iCnt++)
	{
		sprintf(szParam, "%s%d", RCT_DCC_DCLAIM_PREFIX, iCnt + 1);

		rcptCfgPtr->dccDisclaim[iCnt] = (char*)malloc(maxReceiptLineLen+1);
		if (rcptCfgPtr->dccDisclaim[iCnt] == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No memory",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break; //CID 67354 (#1 of 3): Unused value (UNUSED_VALUE) T_RaghavendranR1. No Effect of setting rv to FAILURE as it is overwritten below.
		}
		memset(rcptCfgPtr->dccDisclaim[iCnt], 0x00, maxReceiptLineLen+1);

		rv = getEnvFile(SECTION_RECEIPT, szParam, szTemp, maxReceiptLineLen+1);
		if(rv > 0)
		{
			debug_sprintf(szDbgMsg, "%s: DCC Disclaimer = [%s], len = [%d]",
												__FUNCTION__, szTemp, maxReceiptLineLen);
			APP_TRACE(szDbgMsg);

			memcpy(rcptCfgPtr->dccDisclaim[iCnt], szTemp, maxReceiptLineLen);
		}
		else
		{
			/* Value not found.. Ignoring */
			debug_sprintf(szDbgMsg, "%s: No value present for [%s] in [%s]",
									__FUNCTION__, szParam, SECTION_RECEIPT);
		}

		APP_TRACE(szDbgMsg);
	}
	if(iAppLogEnabled == 1)
	{
		strcpy(szAppLogData, "Receipt Settings Loaded Successfully");
		addAppEventLog(SCA, PAAS_INFO, START_UP, szAppLogData, NULL);
	}

	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: setReceiptHeader
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int setReceiptHeader(int index, char * szValue)
{
	int		rv				= SUCCESS;
	int		iLen			= 0;
	char	szTmp[41]		= "";
	char	szParam[20]		= "";
	int 	maxRcptLineLen	= 0;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	if((index <= 0) || (index > MAX_RCPT_HEADERS))
	{
		debug_sprintf(szDbgMsg, "%s: Invalid index [%d] for receipt header",
														__FUNCTION__, index);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		iLen 		   = strlen(szValue);
		maxRcptLineLen = getMaxReceiptLineLen();

		if(iLen > maxRcptLineLen)
		{
			iLen = maxRcptLineLen;
		}

		memcpy(szTmp, szValue, iLen);

		/* Set the value in the system memory */
		strcpy(sciSettings.stRcptCfg.headers[index - 1], szTmp);

		/* Set the value in the config.usr1 */
		sprintf(szParam, "%s%d", RCT_HEADER_PREFIX, index);

		putEnvFile(SECTION_RECEIPT, szParam, szTmp);
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getReceiptHeaders
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
int getReceiptHeaders(char ** rcptData)
{
	int		iCnt			= 0;
	int		iLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	for(iCnt = 0; iCnt < MAX_RCPT_HEADERS; iCnt++)
	{
		iLen = strlen(sciSettings.stRcptCfg.headers[iCnt]);
		if(iLen > 0)
		{
/*			debug_sprintf(szDbgMsg, "%s: header  = [%s]", __FUNCTION__,
										sciSettings.stRcptCfg.headers[iCnt]);
			APP_TRACE(szDbgMsg);*/

			rcptData[iCnt] = (char *) malloc((iLen+ 1) * sizeof(char));
			if(rcptData[iCnt] != NULL)
			{
				memset(rcptData[iCnt], 0x00, iLen + 1);
				memcpy(rcptData[iCnt], sciSettings.stRcptCfg.headers[iCnt],
																		iLen);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No value", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: getReceiptHeader
 *
 * Description	:
 *
 * Input Params	: Hearder Number and buufer to store the header data
 *
 * Output Params:
 * ============================================================================
 */
int getReceiptHeader(int iHeaderNum, char * pszHeaderData)
{
	int		iLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	if(pszHeaderData == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid buffer is passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	// CID-67425: 25-Jan-16: MukeshS3: iHeaderNum must be less than MAX_RCPT_HEADERS(4) because char* headers[4]; is of size of 4
	//if(iHeaderNum > MAX_RCPT_HEADERS)
	if(iHeaderNum >= MAX_RCPT_HEADERS)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Header number is passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

/*	debug_sprintf(szDbgMsg, "%s: Need to get the %d header", __FUNCTION__, iHeaderNum);
	APP_TRACE(szDbgMsg);*/


	iLen = strlen(sciSettings.stRcptCfg.headers[iHeaderNum]);
	if(iLen > 0)
	{
/*		debug_sprintf(szDbgMsg, "%s: header%d  = [%s]", __FUNCTION__, iHeaderNum,
									sciSettings.stRcptCfg.headers[iHeaderNum]);
		APP_TRACE(szDbgMsg);*/

		memset(pszHeaderData, 0x00, iLen + 1);
		memcpy(pszHeaderData, sciSettings.stRcptCfg.headers[iHeaderNum], iLen);

	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No value for the given header Number i.e. %d", __FUNCTION__, iHeaderNum);
		APP_TRACE(szDbgMsg);
	}


	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: setRcptDisclaimer
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int setReceiptDisclaimer(int index, char * szValue)
{
	int		rv				= SUCCESS;
	int		maxRcptLineLen  = 0;
	int		iLen			= 0;
	char	szParam[20]		= "";
	char	szTmp[100]		= "";
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	if((index <= 0) || (index > MAX_RCPT_DCLAIMERS))
	{
		debug_sprintf(szDbgMsg, "%s: Invalid index [%d] for receipt disclaimer",
														__FUNCTION__, index);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		iLen 		   = strlen(szValue);
		maxRcptLineLen = getMaxReceiptLineLen();

		if(iLen > maxRcptLineLen)
		{
			iLen = maxRcptLineLen;
		}

		/* Set the value in the system memory */
		memcpy(szTmp, szValue, iLen);

		strcpy(sciSettings.stRcptCfg.disclaim[index - 1], szTmp);

		/* Set the value in the config.usr1 */
		sprintf(szParam, "%s%d", RCT_DCLAIM_PREFIX, index);

		putEnvFile(SECTION_RECEIPT, szParam, szTmp);
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getRcptDisclaimers
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
int getRcptDisclaimers(char ** rcptData)
{
	int		iCnt			= 0;
	int		iLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	for(iCnt = 0; iCnt < MAX_RCPT_DCLAIMERS; iCnt++)
	{
		iLen = strlen(sciSettings.stRcptCfg.disclaim[iCnt]);
		if(iLen > 0)
		{
/*			debug_sprintf(szDbgMsg, "%s: disclaimer  = [%s]", __FUNCTION__,
									sciSettings.stRcptCfg.disclaim[iCnt]);
			APP_TRACE(szDbgMsg);*/

			rcptData[iCnt] = (char *) malloc((iLen+ 1) * sizeof(char));
			if(rcptData[iCnt] != NULL)
			{
				memset(rcptData[iCnt], 0x00, iLen + 1);
				memcpy(rcptData[iCnt], sciSettings.stRcptCfg.disclaim[iCnt], iLen);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No value", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: getRcptDCCDisclaimer
 *
 * Description	: gets the receipt dcc disclaimer
 *
 * Input Params	: disclaimer number and pointer for data
 *
 * Output Params:
 * ============================================================================
 */
int getRcptDCCDisclaimer(int iDisclaimerNum, char * pszDisclaimerData)
{
	int		iLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	if(pszDisclaimerData == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid buffer is passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	// CID-67337: 25-Jan-16: MukeshS3:
	// Need to inlcude the boundary value of MAX_RCPT_DCLAIMERS,while checking for invalid size of iDisclaimerNum,
	// because char*	disclaim[4]; is of size of 4 which is equals to MAX_RCPT_DCLAIMERS
	//if(iDisclaimerNum > MAX_RCPT_DCLAIMERS)
	if(iDisclaimerNum >= MAX_RCPT_DCLAIMERS)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Disclaimer number is passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

/*	debug_sprintf(szDbgMsg, "%s: Need to get the %d Disclaimer", __FUNCTION__, iDisclaimerNum);
	APP_TRACE(szDbgMsg);*/


	iLen = strlen(sciSettings.stRcptCfg.dccDisclaim[iDisclaimerNum]);
	if(iLen > 0)
	{
/*		debug_sprintf(szDbgMsg, "%s: Disclaimer%d  = [%s]", __FUNCTION__, iDisclaimerNum,
									sciSettings.stRcptCfg.disclaim[iDisclaimerNum]);
		APP_TRACE(szDbgMsg);*/

		memset(pszDisclaimerData, 0x00, iLen + 1);
		memcpy(pszDisclaimerData, sciSettings.stRcptCfg.dccDisclaim[iDisclaimerNum], iLen);

	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No value for the given Disclaimer Number i.e. %d", __FUNCTION__, iDisclaimerNum);
		APP_TRACE(szDbgMsg);
	}

	return SUCCESS;
}
/*
 * ============================================================================
 * Function Name: getRcptDisclaimer
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
int getRcptDisclaimer(int iDisclaimerNum, char * pszDisclaimerData)
{
	int		iLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	if(pszDisclaimerData == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid buffer is passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	// CID-67337: 25-Jan-16: MukeshS3:
	// Need to inlcude the boundary value of MAX_RCPT_DCLAIMERS,while checking for invalid size of iDisclaimerNum,
	// because char*	disclaim[4]; is of size of 4 which is equals to MAX_RCPT_DCLAIMERS
	//if(iDisclaimerNum > MAX_RCPT_DCLAIMERS)
	if(iDisclaimerNum >= MAX_RCPT_DCLAIMERS)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Disclaimer number is passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

/*	debug_sprintf(szDbgMsg, "%s: Need to get the %d Disclaimer", __FUNCTION__, iDisclaimerNum);
	APP_TRACE(szDbgMsg);*/


	iLen = strlen(sciSettings.stRcptCfg.disclaim[iDisclaimerNum]);
	if(iLen > 0)
	{
/*		debug_sprintf(szDbgMsg, "%s: Disclaimer%d  = [%s]", __FUNCTION__, iDisclaimerNum,
									sciSettings.stRcptCfg.disclaim[iDisclaimerNum]);
		APP_TRACE(szDbgMsg);*/

		memset(pszDisclaimerData, 0x00, iLen + 1);
		memcpy(pszDisclaimerData, sciSettings.stRcptCfg.disclaim[iDisclaimerNum], iLen);

	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No value for the given Disclaimer Number i.e. %d", __FUNCTION__, iDisclaimerNum);
		APP_TRACE(szDbgMsg);
	}

	return SUCCESS;
}
/*
 * ============================================================================
 * Function Name: setReceiptFooter
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int setReceiptFooter(int index, char * szValue)
{
	int		rv				= SUCCESS;
	int		maxRcptLineLen	= 0;
	int		iLen			= 0;
	char	szParam[20]		= "";
	char	szTmp[41]		= "";
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	if((index <= 0) || (index > MAX_RCPT_FOOTERS))
	{
		debug_sprintf(szDbgMsg, "%s: Invalid index [%d] for receipt footer",
														__FUNCTION__, index);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}
	else
	{
		iLen 		   = strlen(szValue);
		maxRcptLineLen = getMaxReceiptLineLen();

		if(iLen > maxRcptLineLen)
		{
			iLen = maxRcptLineLen;
		}

		/* Set the value in the system memory */
		memcpy(szTmp, szValue, iLen);

		strcpy(sciSettings.stRcptCfg.footers[index - 1], szTmp);

		/* Set the value in the config.usr1 */
		sprintf(szParam, "%s%d", RCT_FOOTER_PREFIX, index);

		putEnvFile(SECTION_RECEIPT, szParam, szTmp);
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: getReceiptFooters
 *
 * Description	:
 *
 * Input Params	: none
 *
 * Output Params:
 * ============================================================================
 */
int getReceiptFooters(char ** rcptData)
{
	int		iCnt			= 0;
	int		iLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	for(iCnt = 0; iCnt < MAX_RCPT_HEADERS; iCnt++)
	{
		iLen = strlen(sciSettings.stRcptCfg.footers[iCnt]);
		if(iLen > 0)
		{
			debug_sprintf(szDbgMsg, "%s: footer  = [%s]", __FUNCTION__,
										sciSettings.stRcptCfg.footers[iCnt]);
			APP_TRACE(szDbgMsg);

			rcptData[iCnt] = (char *) malloc((iLen+ 1) * sizeof(char));
			if(rcptData[iCnt] != NULL)
			{
				memset(rcptData[iCnt], 0x00, iLen + 1);
				memcpy(rcptData[iCnt], sciSettings.stRcptCfg.footers[iCnt],
																		iLen);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No value", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}

	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: getReceiptFooter
 *
 * Description	:
 *
 * Input Params	: Footer number and buufer to be filled
 *
 * Output Params:
 * ============================================================================
 */
int getReceiptFooter(int iFooterNum, char *pszFootertData)
{
	int		iLen			= 0;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	if(pszFootertData == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid buffer is passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	/* Daivik : 25/1/2016 - Coverity Issue 67229 - footers array can be indexed upto the 3rd location only.
	 * 4 is an invalid index for footers array and will lead to out of bound access is iFooterNum is 4.
	 */
	if(iFooterNum >= MAX_RCPT_HEADERS)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Footer number is passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Need to get the %d Footer", __FUNCTION__, iFooterNum);
	APP_TRACE(szDbgMsg);


	iLen = strlen(sciSettings.stRcptCfg.footers[iFooterNum]);
	if(iLen > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Footer%d  = [%s]", __FUNCTION__, iFooterNum,
									sciSettings.stRcptCfg.footers[iFooterNum]);
		APP_TRACE(szDbgMsg);

		memset(pszFootertData, 0x00, iLen + 1);
		memcpy(pszFootertData, sciSettings.stRcptCfg.footers[iFooterNum], iLen);

	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: No value for the given Footer Number i.e. %d", __FUNCTION__, iFooterNum);
		APP_TRACE(szDbgMsg);
	}

	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: getListeningPortNum
 *
 * Description	: This function would get the port number on which the ext
 * 					interface module would listen to the connections from POS.
 *
 * Input Params	: nothing
 *
 * Output Params: port number
 * ============================================================================
 */
int getListeningPortNum()
{
	return (sciSettings.listenPort);
}

/*
 * ============================================================================
 * Function Name: getListeningScndPortNum
 *
 * Description	: This function would get the secondary port number on which the ext
 * 					interface module would listen to the connections from POS.
 *
 * Input Params	: nothing
 *
 * Output Params: port number
 * ============================================================================
 */
int getListeningScndPortNum()
{
	return sciSettings.listenScndPort;
}

/*
 * ============================================================================
 * Function Name: getMaxPOSConnections
 *
 * Description	: This function returns the maximum possible POS Connections that can be made
 * to the PIN PAD Device. This setting is given in the Configuration File. This setting cannot exceed
 * DFLT_MAX_POS_CONNECTIONS
 *
 * Input Params	: nothing
 *
 * Output Params: maximum POS Connections.
 * ============================================================================
 */
int getMaxPOSConnections()
{
	return sciSettings.maxPOSConnections;
}

/*
 * ============================================================================
 * Function Name: getMaxReceiptLineLen
 *
 * Description	: This function returns the max receipt line length
 *
 * Input Params	: nothing
 *
 * Output Params: port number
 * ============================================================================
 */
int  getMaxReceiptLineLen()
{
	return sciSettings.maxReceiptLineLen;
}

/*
 * ============================================================================
 * Function Name: isSecondaryPortEnabled
 *
 * Description	: This function returns the max receipt line length
 *
 * Input Params	: nothing
 *
 * Output Params: port number
 * ============================================================================
 */
PAAS_BOOL  isSecondaryPortEnabled()
{
	return sciSettings.scndPortEnabled;
}
