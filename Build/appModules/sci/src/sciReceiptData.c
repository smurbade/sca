/******************************************************************
*                       receiptData.c                            *
*******************************************************************
* Application: PaaS                                               *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:                                                        *
*                                                                 *
*                                                                 *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                                                                 *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include "common/common.h"
#include "common/tranDef.h"
#include "common/xmlUtils.h"
#include "common/utils.h"
#include "ssi/ssiDef.h"
#include "sciReceiptData.h"

#define RECEIPT_FILE_NAME "/home/usr1/flash/xmlReceiptFile.xml"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);


/* Static functions declarations */
static void calculateSubTotal(char *, PYMTTRAN_PTYPE );
static int parseAndAddSinglePropNode(xmlNode * , TRAN_PTYPE, VAL_LST_PTYPE, int );
static int parseAndAddMultiPropNode(xmlNode * , TRAN_PTYPE, VAL_LST_PTYPE, int);
static int addEmptyPropNode(xmlNode * , VAL_LST_PTYPE );
static int addLineItemsToReceipt(xmlNode * , SESSDTLS_PTYPE, VAL_LST_PTYPE );
static int parseXMLFileForReceipt(char * , TRAN_PTYPE, VAL_LST_PTYPE, int);
static int getTagIDConstant(char * , int * );
static int getTagIDValue(char *, char *, char * , TRAN_PTYPE, int );
static int getEmvTagValue(char *, int, char *, TRAN_PTYPE);
static int addLineItemTagVal(int, xmlNode * , LI_NODE_PTYPE , char *);
static int addLineItemheaderToReceipt(xmlNode * , SESSDTLS_PTYPE , VAL_LST_PTYPE );
static int addMultiLineToReceipt(xmlNode * , TRAN_PTYPE , VAL_LST_PTYPE, int);

/* Extern functions declarations */
extern int getReceiptHeader(int , char *);
extern int getReceiptFooter(int , char *);
extern int getRcptDisclaimer(int , char *);
extern int getRcptDCCDisclaimer(int , char *);
extern int getClientId(char * );
extern PAAS_BOOL isReceiptEnabled();
extern int getMaxReceiptLineLen();
extern PAAS_BOOL isEmvEnabledInDevice();
extern PAAS_BOOL isVerboseRecptEnabled();
extern PAAS_BOOL isDescriptiveEntryModeEnabled();
extern PAAS_BOOL isDemoModeEnabled();
extern PAAS_BOOL isServiceCodeCheckInFallbackEnabled();
extern PAAS_BOOL isPartialEmvAllowed();

/*Global variables declarations*/
int maxReceiptLineLen = 0;

//ArjunU1: EMV testing
static char *vrbseRecptTagNames[] = { "AID", "Application Label", "Transaction Currency Code", "PAN Sequence Number",
									   "Application Interchange Profile", "Application Response Code", "Terminal Verifications Results",
									   "Transaction Date", "Transaction Status Info", "Transaction Type", "Amount Authorized",
									  "Other Amount", "Application Usage Control", "Issuer Action Code - Default",
									  "Issuer Action Code - Denial", "Issuer Action Code - Online",
									  "Terminal Action Code - Default", "Terminal Action Code - Denial", "Terminal Action Code - Online",
									  "Issuer Application Data (IAD)",
									  "Application Preferred Name", "Terminal Country Code", "Application Cryptogram",
									  "Cryptogram Information Data", "CVM Results", "Application Transaction Counter",
									 "Unpredictable Number", "POS Entry Mode", "Transaction Seq Counter",
									 "IFD", "Terminal Capabilities", "Transaction Time",
									 "Terminal AID", "Issuer Script Results", "Dedicated file name", "Terminal Transaction Time",
									 "ICC App version num", "Terminal App version num", "Terminal capabilities", "Terminal Type", NULL
									};
//ArjunU1: EMV testing34
static char *vrbseRecptTagList[] = { "4F", "50", "5F2A", "5F34", "82", "8A", "95", "9A", "9B",
									 "9C", "9F02", "9F03", "9F07", "9F0D", "9F0E", "9F0F",
									 "TACDefault", "TACDenial", "TACOnline",
									 "9F10", "9F12", "9F1A", "9F26", "9F27", "9F34", "9F36",
									 "9F37", "9F39", "9F41", "9F1E", "9F33", "9F21", "9F06",
									 "9F5B", "84", "9F21", "9F08", "9F09", "9F33", "9F35", NULL
									};


/*
 * ============================================================================
 * Function Name: isReceiptTemplatePresent
 *
 * Description	: This API tells whether Receipt template present
 *
 *
 * Input Params	: NONE
 *
 * Output Params: PAAS_TRUE/PAAS_FALSE
 * ============================================================================
 */
PAAS_BOOL isReceiptTemplatePresent()
{
	PAAS_BOOL iRetVal = PAAS_TRUE;

#ifdef DEBUG
	char	szDbgMsg[2048]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(doesFileExist(RECEIPT_FILE_NAME) == SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Receipt Template present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = PAAS_TRUE;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Receipt Template NOT present", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = PAAS_FALSE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;

}

/*
 * ============================================================================
 * Function Name: buildReceiptlist
 *
 * Description	: This API builds the list of textlines to be printed on the
 * 					receipt
 *
 * Input Params	: Session details
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
int buildReceiptlist(TRAN_PTYPE pstTran, VAL_LST_PTYPE receiptListPtr, int iResltCode)
{
	int			  iRetVal		= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[2048]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstTran == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Transaction details is NULL!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = FAILURE;
		return iRetVal;
	}
	if(receiptListPtr == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Receipt List is NULL!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = FAILURE;
		return iRetVal;
	}

	/* check whether Receipt is enabled */
	if(isReceiptEnabled() == PAAS_FALSE)
	{
		debug_sprintf(szDbgMsg, "%s: Receipt is not enabled", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	debug_sprintf(szDbgMsg, "%s: Receipt is enabled, need to build the receipt...", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(receiptListPtr, 0x00, VAL_LST_SIZE);

	/* Parse the receipt template and build the list of textlines on the receipt*/

	iRetVal = parseXMLFileForReceipt(RECEIPT_FILE_NAME, pstTran, receiptListPtr, iResltCode);

	if(iRetVal != SUCCESS)
	{
		switch(iRetVal) //Need to handle all error conditions
		{
		case ERR_INV_XML_MSG:
			debug_sprintf(szDbgMsg, "%s: Invalid XML, Error in parsing it", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		default:
			break;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Successfully built the Receipt list", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;

}

/*
 * ============================================================================
 * Function Name: getTagIDConstant
 *
 * Description	: This API gets the corresponding Tag number after comparing
 * 					with Tag names
 *
 * Input Params	: Tag Name, Tag Number
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getTagIDConstant(char * pszTag, int * piTagNo)
{
	int		iRetVal				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[2048]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	if(strcasecmp(pszTag, "Header1") == SUCCESS)
	{
		*piTagNo = HEADER1;
	}
	else if(strcasecmp(pszTag, "Header2") == SUCCESS)
	{
		*piTagNo = HEADER2;
	}
	else if(strcasecmp(pszTag, "Header3") == SUCCESS)
	{
		*piTagNo = HEADER3;
	}
	else if(strcasecmp(pszTag, "Header4") == SUCCESS )
	{
		*piTagNo = HEADER4;
	}
	else if(strcasecmp(pszTag, "DemoMode") == SUCCESS )
	{
		*piTagNo = DEMO_MODE;
	}
	else if(strcasecmp(pszTag, "ClientID") == SUCCESS)
	{
		*piTagNo = CLIENT_ID;
	}
	else if(strcasecmp(pszTag, "CashierID") == SUCCESS)
	{
		*piTagNo = CASHIER_ID;
	}
	else if(strcasecmp(pszTag, "MID") == SUCCESS)
	{
		*piTagNo = MID_ID;
	}
	else if(strcasecmp(pszTag, "TID") == SUCCESS)
	{
		*piTagNo = TID_ID;
	}
	else if(strcasecmp(pszTag, "Register") == SUCCESS)
	{
		*piTagNo = REGISTER_ID;
	}
	else if(strcasecmp(pszTag, "ServerID") == SUCCESS)
	{
		*piTagNo = SERVER_ID;
	}
	else if(strcasecmp(pszTag, "Store") == SUCCESS)
	{
		*piTagNo = STORE_ID;
	}
	else if(strcasecmp( pszTag, "Date") == SUCCESS)
	{
		*piTagNo = RECEIPT_DATE;
	}
	else if(strcasecmp( pszTag, "Time") == SUCCESS)
	{
		*piTagNo = RECEIPT_TIME;
	}
	else if(strcasecmp(pszTag, "TransType") == SUCCESS)
	{
		*piTagNo = TRANS_TYPE;
	}
	else if(strcasecmp(pszTag, "Invoice") == SUCCESS)
	{
		*piTagNo = INVOICE_ID;
	}
	else if(strcasecmp(pszTag, "Account") == SUCCESS)
	{
		*piTagNo = ACCOUNT;
	}
	else if(strcasecmp(pszTag, "EmbAccount") == SUCCESS)
	{
		*piTagNo = EMB_ACCOUNT;
	}
	else if(strcasecmp(pszTag, "PymntType") == SUCCESS)
	{
		*piTagNo = PYMNT_TYPE;
	}
	else if(strcasecmp(pszTag, "Cardholder") == SUCCESS)
	{
		*piTagNo = CARD_HOLDER_NAME;
	}
	else if( strcasecmp(pszTag, "Result") == SUCCESS)
	{
		*piTagNo = RESULT;
	}
	else if(strcasecmp(pszTag, "AuthCode") == SUCCESS)
	{
		*piTagNo = AUTH_NUM;
	}
	else if(strcasecmp(pszTag, "CTroutd") == SUCCESS)
	{
		*piTagNo = CTROUTD_NUM;
	}
	else if(strcasecmp(pszTag, "TransRefNum") == SUCCESS)
	{
		*piTagNo = TRANS_REF_NUM;
	}
	else if(strcasecmp(pszTag, "ValidationCode") == SUCCESS)
	{
		*piTagNo = VALIDATION_CODE;
	}
	else if(strcasecmp(pszTag, "VisaIdentifier") == SUCCESS)
	{
		*piTagNo = VISA_IDENTIFIER;
	}
	else if(strcasecmp(pszTag, "ApprovedAmount") == SUCCESS)
	{
		*piTagNo = APPROVED_AMOUNT;
	}
	else if(strcasecmp(pszTag, "Balance") == SUCCESS)
	{
		*piTagNo = BALANCE_AMOUNT;
	}
	else if(strcasecmp(pszTag, "TraceNum") == SUCCESS)
	{
		*piTagNo = TRACE_NUM;
	}
	else if(strcasecmp(pszTag, "qty") == SUCCESS)
	{
		*piTagNo = QUANTITY;
	}
	else if(strcasecmp(pszTag, "desc") == SUCCESS)
	{
		*piTagNo = DESCRIPTION;
	}
	else if(strcasecmp(pszTag, "unitprice") == SUCCESS)
	{
		*piTagNo = UNIT_PRICE;
	}
	else if(strcasecmp(pszTag, "price") == SUCCESS)
	{
		*piTagNo = PRICE;
	}
	else if(strcasecmp(pszTag, "lineItems") == SUCCESS)
	{
		*piTagNo = LINE_ITEMS;
	}
	else if(strcasecmp(pszTag, "Subtotal") == SUCCESS)
	{
		*piTagNo = SUB_TOTAL;
	}
	else if(strcasecmp(pszTag, "Tax") == SUCCESS)
	{
		*piTagNo = TAX;
	}
	else if(strcasecmp(pszTag, "Tip") == SUCCESS)
	{
		*piTagNo = TIP;
	}
	else if(strcasecmp(pszTag, "CashBack") == SUCCESS)
	{
		*piTagNo = CASH_BACK;
	}
	else if(strcasecmp(pszTag, "Total") == SUCCESS)
	{
		*piTagNo = TOTAL;
	}
	else if(strcasecmp(pszTag, "Disclaimer1") == SUCCESS)
	{
		*piTagNo = DISCLAIMER1;
	}
	else if(strcasecmp(pszTag, "Disclaimer2") == SUCCESS)
	{
		*piTagNo = DISCLAIMER2;
	}
	else if(strcasecmp(pszTag, "Disclaimer3") == SUCCESS)
	{
		*piTagNo = DISCLAIMER3;
	}
	else if(strcasecmp(pszTag, "Disclaimer4") == SUCCESS)
	{
		*piTagNo = DISCLAIMER4;
	}
	else if(strcasecmp(pszTag, "Footer1") == SUCCESS)
	{
		*piTagNo = FOOTER1;
	}
	else if(strcasecmp(pszTag, "Footer2") == SUCCESS)
	{
		*piTagNo = FOOTER2;
	}
	else if(strcasecmp(pszTag, "Footer3") == SUCCESS)
	{
		*piTagNo = FOOTER3;
	}
	else if(strcasecmp(pszTag, "Footer4") == SUCCESS)
	{
		*piTagNo = FOOTER4;
	}
	else if(strcasecmp(pszTag, "ApplicationName") == SUCCESS)
	{
		*piTagNo = APPLICATION_PREF_NAME;
	}
	else if(strcasecmp(pszTag, "ApplicationPAN") == SUCCESS)
	{
		*piTagNo = APPLICATION_PAN;
	}
	else if(strcasecmp(pszTag, "EntryMode") == SUCCESS)
	{
		*piTagNo = ENTRY_MODE;
	}
	else if(strcasecmp(pszTag, "AID") == SUCCESS)
	{
		*piTagNo = APP_ID;
	}
	else if(strcasecmp(pszTag, "TransTotal") == SUCCESS)
	{
		*piTagNo = TRANSACTION_TOTAL;
	}
	else if(strcasecmp(pszTag, "AuthMode") == SUCCESS)
	{
		*piTagNo = AUTH_MODE;
	}
	else if(strcasecmp(pszTag, "TVR") == SUCCESS)
	{
		*piTagNo = TVR;
	}
	else if(strcasecmp(pszTag, "TSI") == SUCCESS)
	{
		*piTagNo = TSI;
	}
	else if(strcasecmp(pszTag, "IAD") == SUCCESS)
	{
		*piTagNo = IAD;
	}
	else if(strcasecmp(pszTag, "ARC") == SUCCESS)
	{
		*piTagNo = ARC;
	}
	else if(strcasecmp(pszTag, "SEQUENCE") == SUCCESS)
	{
		*piTagNo = SEQUENCE;
	}
	else if(strcasecmp(pszTag, "SigDisclaimer") == SUCCESS)
	{
		*piTagNo = SIG_DISCLAIMER;
	}
	else if(strcasecmp(pszTag, "CancelDisclaimer") == SUCCESS)
	{
		*piTagNo = CANCEL_DISCLAIMER;
	}
	else if(strcasecmp(pszTag, "FGNCurrency") == SUCCESS)
	{
		*piTagNo = FGN_CURR;
	}
	else if(strcasecmp(pszTag, "ExchangeRate") == SUCCESS)
	{
		*piTagNo = EXG_RATE;
	}
	else if(strcasecmp(pszTag, "Margin") == SUCCESS)
	{
		*piTagNo = MARGIN;
	}
	else if(strcasecmp(pszTag, "DCCTranAmnt") == SUCCESS)
	{
		*piTagNo = DCC_TRAN_AMOUNT;
	}
	else if(strcasecmp(pszTag, "DCCDisclaimer1") == SUCCESS)
	{
		*piTagNo = DCC_DISCLAIMER1;
	}
	else if(strcasecmp(pszTag, "DCCDisclaimer2") == SUCCESS)
	{
		*piTagNo = DCC_DISCLAIMER2;
	}
	else if(strcasecmp(pszTag, "DCCDisclaimer3") == SUCCESS)
	{
		*piTagNo = DCC_DISCLAIMER3;
	}
	else if(strcasecmp(pszTag, "DCCDisclaimer4") == SUCCESS)
	{
		*piTagNo = DCC_DISCLAIMER4;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Unknown Label [%s]", __FUNCTION__, pszTag);
		APP_TRACE(szDbgMsg);

		*piTagNo = INVALID_TAG;
	}

/*	debug_sprintf(szDbgMsg, "%s: Returning Label = [%s], label Number = [%d]",
												__FUNCTION__, pszTag, *piTagNo);
	APP_TRACE(szDbgMsg);*/

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: getEmvTagValue
 *
 * Description	: This API gets the corresponding Emv Tag value for the given Emv Tag.
 * 			
 *
 * Input Params	: Tag Number, Buffer to fill the tag value
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getEmvTagValue(char *pszEmvTag, int iTagNo, char * pszTagValue, TRAN_PTYPE pstTran)
{
	int					iRetVal			= SUCCESS;
	PAAS_BOOL			bPrintTag		= PAAS_TRUE;
	PYMTTRAN_PTYPE 		pstPymtTran		= NULL;
	CARDDTLS_PTYPE		pstCardDtls		= NULL;

#ifdef DEBUG
	char	szDbgMsg[2048]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	pstPymtTran = (PYMTTRAN_PTYPE) (pstTran->data);
	if(pstPymtTran == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Missing Payment details",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}
	//Get Emv dtls structure ptr
	pstCardDtls	= pstPymtTran->pstCardDtls;

	if(pszTagValue == NULL || pszEmvTag == NULL  || pstCardDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Buffers is passed!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = FAILURE;
		return iRetVal;
	}

	if(strlen(pstTran->stRespDtls.szRslt) > 0)
	{
		if(strcmp(pstTran->stRespDtls.szRslt, "APPROVED") == 0)
		{
			bPrintTag = PAAS_FALSE;
		}
		else
		{
			bPrintTag = PAAS_TRUE;
		}
	}

	if(!strcasecmp(pszEmvTag, "4F"))
	{
		if( strlen(pstCardDtls->stEmvAppDtls.szAID) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvAppDtls.szAID);
		}	
	}
	else if(!strcasecmp(pszEmvTag, "50"))
	{
		if( strlen(pstCardDtls->stEmvAppDtls.szAppLabel) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
												pstCardDtls->stEmvAppDtls.szAppLabel);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "5F2A"))
	{
		if( strlen(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmCurrCode) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
											 	pstCardDtls->stEmvCryptgrmDtls.szCryptgrmCurrCode);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "5F34"))
	{
		if( strlen(pstCardDtls->stEmvAppDtls.szAppSeqNum) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
											 	pstCardDtls->stEmvAppDtls.szAppSeqNum);
		}
	}
	else if(!strcasecmp(pszEmvTag, "5F2D"))
	{
		if( strlen(pstCardDtls->szEMVlangPref) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->szEMVlangPref);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "82"))
	{
		if( strlen(pstCardDtls->stEmvAppDtls.szAppIntrChngProfile) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
											  pstCardDtls->stEmvAppDtls.szAppIntrChngProfile);
		}
	}
	else if(!strcasecmp(pszEmvTag, "8A"))
	{
		if( strlen(pstCardDtls->stEmvtranDlts.szAuthRespCode) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
											  pstCardDtls->stEmvtranDlts.szAuthRespCode);
		}
	}
	else if(!strcasecmp(pszEmvTag, "84"))
	{
		if( strlen(pstCardDtls->stEmvtranDlts.szDedicatedFileName) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
											  pstCardDtls->stEmvtranDlts.szDedicatedFileName);
		}
	}
	else if(!strcasecmp(pszEmvTag, "95"))
	{
		if( strlen(pstCardDtls->stEmvTerDtls.szTermVerificationResults) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
											pstCardDtls->stEmvTerDtls.szTermVerificationResults);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9A"))
	{
		if( strlen(pstCardDtls->stEmvtranDlts.szTranStatusInfo) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
												pstCardDtls->stEmvtranDlts.szTermTranDate);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9B"))
	{
		if( strlen(pstCardDtls->stEmvtranDlts.szTranStatusInfo) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
											pstCardDtls->stEmvtranDlts.szTranStatusInfo);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9C"))
	{
		if( strlen(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmTranType) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvCryptgrmDtls.szCryptgrmTranType);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9F02"))
	{
		if( strlen(pstCardDtls->stEmvtranDlts.szAuthAmnt) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvtranDlts.szAuthAmnt);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9F03"))
	{
		if( strlen(pstCardDtls->stEmvtranDlts.szEMVCashBackAmnt) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvtranDlts.szEMVCashBackAmnt);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9F07"))
	{
		if( strlen(pstCardDtls->stEmvAppDtls.szAppUsageControl) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvAppDtls.szAppUsageControl);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9F0D"))
	{
		if( strlen(pstCardDtls->stEmvIssuerDtls.szIACDefault) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvIssuerDtls.szIACDefault);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9F0E"))
	{
		if( strlen(pstCardDtls->stEmvIssuerDtls.szIACDenial) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvIssuerDtls.szIACDenial);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9F0F"))
	{
		if( strlen(pstCardDtls->stEmvIssuerDtls.szIACOnline) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvIssuerDtls.szIACOnline);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "TACDefault"))
	{
		if( strlen(pstCardDtls->stEmvTerDtls.szTACDefault) > 0 )
		{
			sprintf(pszTagValue, "%s %s", vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvTerDtls.szTACDefault);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "TACDenial"))
	{
		if( strlen(pstCardDtls->stEmvTerDtls.szTACDenial) > 0 )
		{
			sprintf(pszTagValue, "%s %s", vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvTerDtls.szTACDenial);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "TACOnline"))
	{
		if( strlen(pstCardDtls->stEmvTerDtls.szTACOnline) > 0 )
		{
			sprintf(pszTagValue, "%s %s", vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvTerDtls.szTACOnline);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9F10"))
	{
		if( strlen(pstCardDtls->stEmvIssuerDtls.szIssuerAppData) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
											pstCardDtls->stEmvIssuerDtls.szIssuerAppData);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9F12"))
	{
		if( strlen(pstCardDtls->stEmvAppDtls.szAppPrefName) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
											pstCardDtls->stEmvAppDtls.szAppPrefName);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9F1A"))
	{
		if( strlen(pstCardDtls->stEmvtranDlts.szTermTranCode) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
												pstCardDtls->stEmvtranDlts.szTermTranCode);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9F26"))
	{
		if( strlen(pstCardDtls->stEmvCryptgrmDtls.szAppCryptgrm) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvCryptgrmDtls.szAppCryptgrm);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9F27"))
	{
		if( strlen(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9F34"))
	{
		if( strlen(pstCardDtls->stEmvtranDlts.szCVMResult) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvtranDlts.szCVMResult);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9F36"))
	{
		if( strlen(pstCardDtls->stEmvAppDtls.szAppTranCounter) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
											pstCardDtls->stEmvAppDtls.szAppTranCounter);
		}
	}
	else if(bPrintTag && !strcasecmp(pszEmvTag, "9F37"))
	{
		if( strlen(pstCardDtls->stEmvCryptgrmDtls.szUnprdctbleNum) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
											pstCardDtls->stEmvCryptgrmDtls.szUnprdctbleNum);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9F39"))
	{
		//Copying only first two characters
		if( strlen(pstCardDtls->stEmvtranDlts.szPOSEntryMode) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %c%c", pszEmvTag, vrbseRecptTagNames[iTagNo],
					pstCardDtls->stEmvtranDlts.szPOSEntryMode[0], pstCardDtls->stEmvtranDlts.szPOSEntryMode[1]);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9F41"))
	{
		if( strlen(pstCardDtls->stEmvtranDlts.szTermTranSeqCount) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
											pstCardDtls->stEmvtranDlts.szTermTranSeqCount);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9F1E"))
	{
		if( strlen(pstCardDtls->stEmvTerDtls.szDevSerialNum) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
											pstCardDtls->stEmvTerDtls.szDevSerialNum);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9F33"))
	{
		if( strlen(pstCardDtls->stEmvTerDtls.szTermCapabilityProf) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvTerDtls.szTermCapabilityProf);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9F21"))
	{
		if( strlen(pstCardDtls->stEmvtranDlts.szTranTime) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvtranDlts.szTranTime);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9F06"))
	{
		if( strlen(pstCardDtls->stEmvTerDtls.szTermAID) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvTerDtls.szTermAID);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9F5B"))
	{
		if( strlen(pstCardDtls->stEmvIssuerDtls.szIssueScrptResults) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvIssuerDtls.szIssueScrptResults);
		}
	}
	else if(!strcasecmp(pszEmvTag, "84"))
	{
		if( strlen(pstCardDtls->stEmvtranDlts.szDedicatedFileName) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvtranDlts.szDedicatedFileName);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9F21"))
	{
		if( strlen(pstCardDtls->stEmvtranDlts.szTranTime) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvtranDlts.szTranTime);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9F08"))
	{
		if( strlen(pstCardDtls->stEmvAppDtls.szICCAppVersNum) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvAppDtls.szICCAppVersNum);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9F09"))
	{
		if( strlen(pstCardDtls->stEmvAppDtls.szAppVersNum) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvAppDtls.szAppVersNum);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9F33"))
	{
		if( strlen(pstCardDtls->stEmvTerDtls.szTermCapabilityProf) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvTerDtls.szTermCapabilityProf);
		}
	}
	else if(!strcasecmp(pszEmvTag, "9F35"))
	{
		if( strlen(pstCardDtls->stEmvTerDtls.szTermType) > 0 )
		{
			sprintf(pszTagValue, "Tag %s: %s %s", pszEmvTag, vrbseRecptTagNames[iTagNo],
														 pstCardDtls->stEmvTerDtls.szTermType);
		}
	}
	else;
	//Add more here if required in verbose

	return iRetVal;
}	
/*
 * ============================================================================
 * Function Name: getTagIDValue
 *
 * Description	: This API gets the corresponding Tag value for the given
 * 					TagID
 *
 * Input Params	: Tag Number, Buffer to fill the tag value
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int getTagIDValue(char *pszTag, char *pszFormat, char * pszTagValue, TRAN_PTYPE pstTran, int iResltCode)
{
	int					iRetVal			= SUCCESS;
	int    				piTagNo        	= 0;
	int					iFxn			= 0;
	int					iCmd			= 0;
	int					iTemp			= 0;
	char				cTmp			= 0;
	char				szTempVal[256] 	= "";
	static PAAS_BOOL	bEmvEnabled		= PAAS_FALSE;
	static PAAS_BOOL 	bFirstTime		= PAAS_TRUE;
	PAAS_BOOL			bCVMSuccess		= PAAS_TRUE;
	SESSDTLS_PTYPE		pstSessDtls		= NULL;
	PYMTTRAN_PTYPE 		pstPymtTran		= NULL;
	CARDDTLS_PTYPE 		pstCardDtls		= NULL;
	CTRANDTLS_PTYPE		pstCTranDtls	= NULL;
	AMTDTLS_PTYPE		pstAmtDtls		= NULL;
	LVL2_PTYPE			pstTaxDtls		= NULL;
	DCCDTLS_PTYPE		pstDCCdtls		= NULL;

#ifdef DEBUG
	char	szDbgMsg[2048]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);
	// CID-67486: 3-Feb-16: MukeshS3: Adding NULL check at beginning
	if(pstTran == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL param passed",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	pstPymtTran = (PYMTTRAN_PTYPE) (pstTran->data);
	if(pstPymtTran == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Missing Payment details",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	pstSessDtls		= pstPymtTran->pstSess;
	pstCardDtls		= pstPymtTran->pstCardDtls;
	pstCTranDtls	= pstPymtTran->pstCTranDtls;/*Not sure whether to use CTRAN OR FTRAN*/
	pstAmtDtls		= pstPymtTran->pstAmtDtls;
	pstTaxDtls		= pstPymtTran->pstLevel2Dtls;

	if(pstPymtTran->pstDCCDtls && pstPymtTran->pstDCCDtls->iDCCInd == DCC_ELIGIBLE_USER_OPTED)
	{
		pstDCCdtls		= pstPymtTran->pstDCCDtls;
	}

	if(pszTagValue == NULL || pszTag == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Invalid Buffer is passed!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iRetVal = FAILURE;
		return iRetVal;
	}

	iRetVal = getTagIDConstant(pszTag, &piTagNo);

	if(iRetVal != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Error while retrieving the TagID Constant", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}
	
	if(bFirstTime == PAAS_TRUE)
	{
		//Get EMV status
		bEmvEnabled = isEmvEnabledInDevice();
		bFirstTime = PAAS_FALSE;
	}

//	debug_sprintf(szDbgMsg, "%s: Tag ID [%d]", __FUNCTION__, piTagNo);
//	APP_TRACE(szDbgMsg);

	switch(piTagNo)
	{
	case HEADER1: /* Getting the Header1 from the config file*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		iRetVal = getReceiptHeader(FIRST, pszTagValue);
/*		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully fetched the Header1", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Header1!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case HEADER2: /* Getting the Header2 from the config file*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		iRetVal = getReceiptHeader(SECOND, pszTagValue);
/*		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully fetched the Header2", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Header2!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case HEADER3: /* Getting the Header3 from the config file*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		iRetVal = getReceiptHeader(THIRD, pszTagValue);
/*		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully fetched the Header3", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Header3!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case HEADER4: /* Getting the Header4 from the configfile*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		iRetVal = getReceiptHeader(FOURTH, pszTagValue);
/*		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully fetched the Header4", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Header4!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case DEMO_MODE:						/* Getting Demo Mode Details from Session Details */
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if (pstSessDtls != NULL)
		{
			if(isDemoModeEnabled())
			{
				strcpy(pszTagValue, "****TRAINING MODE****");
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Demo Mode is Disabled", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Session Details in the transaction details is null", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case CLIENT_ID: /* Getting the Client ID from the config file*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		iRetVal = getClientId(pszTagValue);
/*		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully fetched the Client ID", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Client ID!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case CASHIER_ID: /* Getting the Cashier ID from the session details*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if (pstSessDtls != NULL)
		{
			if(strlen(pstSessDtls->szCashierId) > 0)
			{
				strcpy(pszTagValue, pstSessDtls->szCashierId);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Value not available for Cashier ID", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Session Details in the transaction details is null", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case MID_ID: /* Getting the MID from the session details*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if (pstCTranDtls != NULL)
		{
			if(strlen(pstCTranDtls->szMerchId) > 0)
			{
				strcpy(pszTagValue, pstCTranDtls->szMerchId);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Value not available for MID", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current Transaction details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case TID_ID: /* Getting the TID from the session details*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if (pstCTranDtls != NULL)
		{
			if(strlen(pstCTranDtls->szTermId) > 0)
			{
				strcpy(pszTagValue, pstCTranDtls->szTermId);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Value not available for TID", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current Transaction details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case REGISTER_ID: /* Getting the LaneID from the session details*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if (pstSessDtls != NULL)
		{
			if(strlen(pstSessDtls->szLaneNo) > 0)
			{
				strcpy(pszTagValue, pstSessDtls->szLaneNo);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Value not available for Lane Num", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Session Details in the transaction details is null", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SERVER_ID: /* Getting the ServerID from the session details*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if (pstSessDtls != NULL)
		{
			if(strlen(pstSessDtls->szServerId) > 0)
			{
				strcpy(pszTagValue, pstSessDtls->szServerId);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Value not available for Server ID", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Session Details in the transaction details is null", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case STORE_ID: /* Getting the StoreID from the session details*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if (pstSessDtls != NULL)
		{
			if(strlen(pstSessDtls->szStoreNo) > 0)
			{
				strcpy(pszTagValue, pstSessDtls->szStoreNo);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Value not available for Store No", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Session Details in the transaction details is null", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case RECEIPT_DATE:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pszFormat != NULL)
		{
			getDate(pszFormat, pszTagValue);
		}
		else
		{
			getDate(DEFAULT_DATE_FORMAT, pszTagValue); //Date format is not sent, defaulting
		}
		break;

	case RECEIPT_TIME:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pszFormat != NULL)
		{
			getTime(pszFormat, pszTagValue);
		}
		else
		{
			getTime(DEFAULT_TIME_FORMAT, pszTagValue); //Time format is not sent, defaulting
		}
		break;

	case TRANS_TYPE: /* Getting the current Command of the transaction */
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstTran != NULL)
		{
			iFxn 	= pstTran->iFxn;
			iCmd	= pstTran->iCmd;

			switch(iCmd)
			{
			case SSI_SALE:
				strcpy(pszTagValue, "SALE");
				break;

			case SSI_VOID:
				strcpy(pszTagValue, "VOID");
				break;

			case SSI_CREDIT:
			case SSI_REVERSAL:
				strcpy(pszTagValue, "REFUND");
				break;

			case SSI_PREAUTH:
				strcpy(pszTagValue, "PRE AUTHORIZATION");
				break;

			case SSI_VOICEAUTH:
				strcpy(pszTagValue, "VOICE AUTHORIZATION");
				break;

			case SSI_POSTAUTH:
				strcpy(pszTagValue, "POST AUTHORIZATION");
				break;

			case SSI_COMPLETION:
				strcpy(pszTagValue, "COMPLETION");
				break;

			case SSI_ACTIVATE:
			case SSI_REACTIVATE:
				strcpy(pszTagValue, "ACTIVATE");
				break;

			case SSI_ADDVAL:
				strcpy(pszTagValue, "ADD VALUE");
				break;

			case SSI_BAL:
				strcpy(pszTagValue, "BALANCE");
				break;

			case SSI_DEACTIVATE:
				strcpy(pszTagValue, "DEACTIVATE");
				break;

			case SSI_GIFTCLOSE:
				strcpy(pszTagValue, "GIFT CLOSE");
				break;

			case SSI_VERIFY:
				strcpy(pszTagValue, "CHECK_VERIFY");
				break;

			case SSI_PAYACCOUNT:
				strcpy(pszTagValue, "PAYACCOUNT");
				break;

			default:
				debug_sprintf(szDbgMsg,"%s: Unsupp Cmd [%d]",__FUNCTION__, iCmd);
				APP_TRACE(szDbgMsg);
				break;
			}
			debug_sprintf(szDbgMsg,"%s: Tran status [%d]",__FUNCTION__, pstTran->iStatus);
			APP_TRACE(szDbgMsg);			
			
			if( bEmvEnabled == PAAS_TRUE && iResltCode ==  59001) //ERR_USR_CANCELED
			{
				debug_sprintf(szDbgMsg,"%s: Tran Type [%s]",__FUNCTION__, pszTagValue);
				APP_TRACE(szDbgMsg);			
				if( strlen(pszTagValue) > 0)
				{
					memset(szTempVal, 0x00, sizeof(szTempVal));
					sprintf(szTempVal, "%s - CANCELLED", pszTagValue);
					memset(pszTagValue, 0x00, maxReceiptLineLen); // CID 67372 (#1 of 1): Wrong sizeof argument (SIZEOF_MISMATCH) T_RaghavendranR1
					strcpy(pszTagValue, szTempVal);
				}
				debug_sprintf(szDbgMsg,"%s: Cancelled Msg[%s]",__FUNCTION__, pszTagValue);
				APP_TRACE(szDbgMsg);				
			}			
		}
/*		else
		{
			debug_sprintf(szDbgMsg, "%s: Transaction details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case INVOICE_ID:  /* Getting the Invoice from the session details*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if (pstSessDtls != NULL)
		{
			if(strlen(pstSessDtls->szInvoice) > 0)
			{
				strcpy(pszTagValue, pstSessDtls->szInvoice);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Value not available for Store No", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Session Details in the transaction details is null", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case ACCOUNT: /* Getting the PAN from the transaction details of the session*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		memset(szTempVal, 0x00, sizeof(szTempVal));

		if(pstCardDtls != NULL)
		{
			if(strlen(pstCardDtls->szPAN) > 0)
			{
				/*
				 * FTC (federal trade commission rule) rule, we have to
				 * send only last four digits in clear on receipt
				 */

				getMaskedPANForBAPI(pstCardDtls->szPAN, szTempVal);

				if(strlen(pstCardDtls->szPymtMedia) > 0)
				{
					sprintf(pszTagValue, "%s %s", pstCardDtls->szPymtMedia, szTempVal);
				}
				else
				{
					sprintf(pszTagValue, "%s", szTempVal);
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Card details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	case EMB_ACCOUNT: /* Getting the PAN from the transaction details of the session*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);

		if(pstCardDtls != NULL)
		{
			if(strlen(pstCardDtls->szEmbossedPAN) > 0)
			{
				/*
				 * We will print Embossed Account Number in clear
				 */
				strcpy(pszTagValue, pstCardDtls->szEmbossedPAN);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Card details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	case PYMNT_TYPE:
		memset(pszTagValue, 0x00, maxReceiptLineLen);

		if(pstCTranDtls != NULL)
		{
			if(strlen(pstCTranDtls->szPymntType) > 0)
			{
				strcpy(pszTagValue, pstCTranDtls->szPymntType);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Payment Type value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current Transaction Dtls not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	case CARD_HOLDER_NAME: /* Getting the Card Holder Name from the transaction details of the session*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);

		if(pstCardDtls != NULL)
		{
			if(strlen(pstCardDtls->szName) > 0)
			{
				strcpy(pszTagValue, pstCardDtls->szName);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Card details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	case RESULT: /* Getting the Result from the response details of the session*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstTran != NULL)
		{
			if(strlen(pstTran->stRespDtls.szRslt) > 0)
			{
				strcpy(pszTagValue, pstTran->stRespDtls.szRslt);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Transaction details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case AUTH_NUM: /* Getting the Auth code from the payment details of the session*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);

		if(pstCTranDtls != NULL)
		{
			if(strlen(pstCTranDtls->szAuthCode) > 0)
			{
				strcpy(pszTagValue, pstCTranDtls->szAuthCode);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Auth Code value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current Transaction details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case CTROUTD_NUM:
		memset(pszTagValue, 0x00, maxReceiptLineLen);

		if(pstCTranDtls != NULL)
		{
			if(strlen(pstCTranDtls->szCTroutd) > 0)
			{
				strcpy(pszTagValue, pstCTranDtls->szCTroutd);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: CTroutd value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current Transaction details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case TRANS_REF_NUM:
		memset(pszTagValue, 0x00, maxReceiptLineLen);

		if(pstCTranDtls != NULL)
		{
			if(strlen(pstCTranDtls->szReferenceNum) > 0)
			{
				strcpy(pszTagValue, pstCTranDtls->szReferenceNum);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Trans Ref Number value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current Transaction details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case VALIDATION_CODE:
		memset(pszTagValue, 0x00, maxReceiptLineLen);

		if(pstCTranDtls != NULL)
		{
			if(strlen(pstCTranDtls->szValidtionCode) > 0)
			{
				strcpy(pszTagValue, pstCTranDtls->szValidtionCode);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Validation Code value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current Transaction details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case VISA_IDENTIFIER:
		memset(pszTagValue, 0x00, maxReceiptLineLen);

		if(pstCTranDtls != NULL)
		{
			if(strlen(pstCTranDtls->szVisaIdentifier) > 0)
			{
				strcpy(pszTagValue, pstCTranDtls->szVisaIdentifier);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: VISA Identifier value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current Transaction details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case APPROVED_AMOUNT: /* Getting the Approved amount from the payment details of the session*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);

		if(pstDCCdtls != NULL)
		{
			if(strlen(pstDCCdtls->szFgnAmount) > 0)
			{
				memset(szTempVal, 0x00, sizeof(szTempVal));
				if(pstDCCdtls->iMinorUnits > 0)
				{
					getFGNAmntWithDecimals(pstDCCdtls->szFgnAmount, pstDCCdtls->iMinorUnits, szTempVal);
					sprintf(pszTagValue, "%s %s", pstDCCdtls->szAlphaCurrCode, szTempVal);
				}
				else
				{
					sprintf(pszTagValue, "%s %s", pstDCCdtls->szAlphaCurrCode, pstDCCdtls->szFgnAmount);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Approved amount value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(pstAmtDtls != NULL)
		{
			if(strlen(pstAmtDtls->apprvdAmt) > 0)
			{
				strcpy(pszTagValue, "USD$ ");
				strcat(pszTagValue, pstAmtDtls->apprvdAmt);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Approved amount value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Amount details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case BALANCE_AMOUNT: /* Getting the Balance amount from the payment details of the session*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);

		if(pstAmtDtls != NULL)
		{
			if(strlen(pstAmtDtls->availBal) > 0)
			{
				strcpy(pszTagValue, "USD$ ");
				strcat(pszTagValue, pstAmtDtls->availBal);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Balance amount value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Amount details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
		/*Added Trace Number*/
	case TRACE_NUM:
		memset(pszTagValue, 0x00, maxReceiptLineLen);

		if(pstCTranDtls != NULL)
		{
			if(strlen(pstCTranDtls->szTraceNum) > 0)
			{
				strcpy(pszTagValue, pstCTranDtls->szTraceNum);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: TraceNum value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Current Transaction details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	case QUANTITY:
		break;
	case DESCRIPTION:
		break;
	case UNIT_PRICE:
		break;
	case PRICE:
		break;
	case LINE_ITEMS:
		break;
	case SUB_TOTAL: /* Calculating the Subtotal */
		memset(pszTagValue, 0x00, maxReceiptLineLen);

		if(pstAmtDtls != NULL)
		{
			calculateSubTotal(pszTagValue, pstPymtTran);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Payment details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}


		break;
	case TAX:
		memset(pszTagValue, 0x00, maxReceiptLineLen);


		if(pstTaxDtls != NULL)
		{
			if(strlen(pstTaxDtls->taxAmt) > 0)
			{
				strcpy(pszTagValue, "USD$ ");
				strcat(pszTagValue, pstTaxDtls->taxAmt);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Tax amount value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Tax details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case TIP:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstAmtDtls != NULL)
		{
			if(strlen(pstAmtDtls->tipAmt) > 0)
			{
				strcpy(pszTagValue, "USD$ ");
				strcat(pszTagValue, pstAmtDtls->tipAmt);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Tip amount value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Amount details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case CASH_BACK:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstAmtDtls != NULL)
		{
			if(strlen(pstAmtDtls->cashBackAmt) > 0)
			{
				strcpy(pszTagValue, "USD$ ");
				strcat(pszTagValue, pstAmtDtls->cashBackAmt);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Cash Back amount value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Amount details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case TOTAL:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstAmtDtls != NULL)
		{
			if(strlen(pstAmtDtls->tranAmt) > 0)
			{
				strcpy(pszTagValue, "USD$ ");
				strcat(pszTagValue, pstAmtDtls->tranAmt);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Transaction amount value not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Amount details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case DISCLAIMER1:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		iRetVal = getRcptDisclaimer(FIRST, pszTagValue);
/*		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully fetched the Disclaimer1", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Disclaimer1!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case DISCLAIMER2:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		iRetVal = getRcptDisclaimer(SECOND, pszTagValue);
/*		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully fetched the Disclaimer2", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Disclaimer2!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;
	case DISCLAIMER3:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		iRetVal = getRcptDisclaimer(THIRD, pszTagValue);
/*		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully fetched the Disclaimer3", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Disclaimer3!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case DISCLAIMER4:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		iRetVal = getRcptDisclaimer(FOURTH, pszTagValue);
/*		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully fetched the Disclaimer4", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Disclaimer4!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;
	case FOOTER1:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		iRetVal = getReceiptFooter(FIRST, pszTagValue);
/*		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully fetched the Footer1", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Footer1!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case FOOTER2:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		iRetVal = getReceiptFooter(SECOND, pszTagValue);
/*		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully fetched the Footer2", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Footer2!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case FOOTER3:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		iRetVal = getReceiptFooter(THIRD, pszTagValue);
/*		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully fetched the Footer3", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Footer3!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case FOOTER4:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		iRetVal = getReceiptFooter(FOURTH, pszTagValue);
/*		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully fetched the Footer4", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while fetching the Footer4!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case APPLICATION_PREF_NAME:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstCardDtls != NULL && pstCardDtls ->bEmvData != PAAS_FALSE)
		{
			if(strlen(pstCardDtls->stEmvAppDtls.szAppPrefName) > 0)
			{
				strcpy(pszTagValue, pstCardDtls->stEmvAppDtls.szAppPrefName);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Application Pref Name not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: EMV details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	case APPLICATION_PAN:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstCardDtls != NULL && pstCardDtls ->bEmvData != PAAS_FALSE)
		{
			if(strlen(pstCardDtls->szPAN) > 0)
			{
			//	strcpy(pszTagValue, pstCardDtls->szPAN);
			/* Sending the masked the pan on the receipt*/
				/*
				 * FTC (federal trade commission rule) rule, we have to
				 * send only last four digits in clear on receipt
				 */
				getMaskedPANForBAPI(pstCardDtls->szPAN, pszTagValue);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Application Pref Name not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: EMV details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	case TRANSACTION_TOTAL:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstCardDtls != NULL && pstCardDtls ->bEmvData != PAAS_FALSE)
		{
			if(pstDCCdtls != NULL)
			{
				if(strlen(pstCardDtls->stEmvtranDlts.szAuthAmnt) > 0)
				{
					memset(szTempVal, 0x00, sizeof(szTempVal));
					if(pstDCCdtls->iMinorUnits > 0)
					{
						getFGNAmntWithDecimals(pstCardDtls->stEmvtranDlts.szAuthAmnt, pstDCCdtls->iMinorUnits, szTempVal);
						sprintf(pszTagValue, "%s %s", pstDCCdtls->szAlphaCurrCode, szTempVal);
					}
					else
					{
						sprintf(pszTagValue, "%s %s", pstDCCdtls->szAlphaCurrCode, pstCardDtls->stEmvtranDlts.szAuthAmnt);
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: DCC Tran Amount Not Available!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else if(strlen(pstCardDtls->stEmvtranDlts.szAuthAmnt) > 0)
			{
				iTemp = 0;
				iTemp = atoi(pstCardDtls->stEmvtranDlts.szAuthAmnt);
				sprintf(pszTagValue, "%s%d.%.2d", "USD$ ",  (iTemp)/100, (iTemp)%100);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Trans Total not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: EMV details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	case AUTH_MODE:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstCardDtls != NULL && pstCardDtls ->bEmvData != PAAS_FALSE)
		{
			//TODO:AJAYS2 Change it currently sending ISSUER always if cryptogram is there
			if(pstCardDtls->bPartialEMV == PAAS_FALSE && strlen(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData) > 0)
			{
				//(EMV CTLS and Online Req) or Host Declined or (Host Approved and Card Approved) => ISSUER
				if(		( (pstCardDtls->iCardSrc == CRD_EMV_CTLS) && (strcmp(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData, "80") == 0) )
						||
						( pstCardDtls->stEmvtranDlts.iHostStatus == ERR_PWC_RSLTCODE_DECLINED)
						||
						(	(pstCardDtls->stEmvtranDlts.iHostStatus == SUCCESS_PWC_RSLTCODE_CAPTURED || pstCardDtls->stEmvtranDlts.iHostStatus == SUCCESS_PWC_RSLTCODE_APPROVED)
								&&
								(strcmp(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData, "40") == 0)
						)
				)
				{
					strcpy(pszTagValue, "Issuer");
				}
				else
				{
					strcpy(pszTagValue, "Card");
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Trans Total not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: EMV details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	case ENTRY_MODE:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstCardDtls != NULL)
		{
			if(isDescriptiveEntryModeEnabled())
			{
				if(pstCardDtls->bManEntry == PAAS_TRUE)
				{
					strcpy(pszTagValue, "Manual");
				}
				else
				{
					switch (pstCardDtls->iCardSrc)
					{
					case CRD_MSR:
						if(isEmvEnabledInDevice() && isServiceCodeCheckInFallbackEnabled())
						{
							if( (strcasecmp(pstCardDtls->szServiceCodeByteOne, "2") == 0) ||
									(strcasecmp(pstCardDtls->szServiceCodeByteOne, "6") == 0)
							)
							{
								strcpy(pszTagValue, "Mag Stripe - Fallback");
							}
							else
							{
								strcpy(pszTagValue, "Mag Stripe - Swipe");
							}
						}
						else
						{
							strcpy(pszTagValue, "Mag Stripe - Swipe");
						}
						break;

					case CRD_EMV_FALLBACK_MSR:
						strcpy(pszTagValue, "Mag Stripe - Fallback");
						break;

					case CRD_NFC:
					case CRD_RFID:
						strcpy(pszTagValue, "Mag Stripe - Contactless");
						break;

					case CRD_EMV_CTLS:
						strcpy(pszTagValue, "Chip Read - Contactless");
						break;

					case CRD_EMV_CT:
						strcpy(pszTagValue, "Chip Read - Contact");
						break;

					case CRD_UNKNWN:
						strcpy(pszTagValue, "Unknown");
						break;

					default:
						debug_sprintf(szDbgMsg, "%s: Card source not available!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
			}
			else
			{
				if(pstCardDtls->bManEntry == PAAS_TRUE)
				{
					strcpy(pszTagValue, "Keyed");
				}
				else
				{
					switch (pstCardDtls->iCardSrc)
					{
					case CRD_MSR:
						if(isEmvEnabledInDevice() && isServiceCodeCheckInFallbackEnabled())
						{
							if( (strcasecmp(pstCardDtls->szServiceCodeByteOne, "2") == 0) ||
									(strcasecmp(pstCardDtls->szServiceCodeByteOne, "6") == 0)
							)
							{
								strcpy(pszTagValue, "FSwipe");
							}
							else
							{
								strcpy(pszTagValue, "Swiped");
							}
						}
						else
						{
							strcpy(pszTagValue, "Swiped");
						}
						break;

					case CRD_EMV_FALLBACK_MSR:
						strcpy(pszTagValue, "FSwipe");
						break;

					case CRD_NFC:
					case CRD_RFID:
					case CRD_EMV_CTLS:
						strcpy(pszTagValue, "Contactless");
						break;

					case CRD_EMV_CT:
						strcpy(pszTagValue, "Chip Read");
						break;

					case CRD_UNKNWN:
						strcpy(pszTagValue, "Unknown");
						break;

					default:
						debug_sprintf(szDbgMsg, "%s: Card source not available!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Card Details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SIG_DISCLAIMER:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstCardDtls != NULL)
		{
			if( pstCardDtls->bEmvData != PAAS_FALSE && (strlen(pstCardDtls->stEmvtranDlts.szCVMResult) > 0)
					//&&
					//(pstCardDtls->stEmvtranDlts.iHostStatus == SUCCESS_PWC_RSLTCODE_CAPTURED || pstCardDtls->stEmvtranDlts.iHostStatus == SUCCESS_PWC_RSLTCODE_APPROVED)
					 )
			{
				if(strlen(pstCardDtls->stEmvTerDtls.szTermVerificationResults) > 0)
				{
					if( pstCardDtls->stEmvTerDtls.szTermVerificationResults[4] == '8' ||
							pstCardDtls->stEmvTerDtls.szTermVerificationResults[4] == '9' ||
							(pstCardDtls->stEmvTerDtls.szTermVerificationResults[4] >= 'A' && pstCardDtls->stEmvTerDtls.szTermVerificationResults[4] <='F') ||
							(pstCardDtls->stEmvTerDtls.szTermVerificationResults[4] >= 'a' && pstCardDtls->stEmvTerDtls.szTermVerificationResults[4] <='f')
					)
					{
						bCVMSuccess = PAAS_FALSE;
					}
					debug_sprintf(szDbgMsg, "%s: CVMR Status [%d]", __FUNCTION__, bCVMSuccess);
					APP_TRACE(szDbgMsg);
				}

				//if(strncmp(pstCardDtls->stEmvtranDlts.szCVMResult, "1E", 2) == 0)
				cTmp = pstCardDtls->stEmvtranDlts.szCVMResult[1];
				debug_sprintf(szDbgMsg, "%s: cTmp %c !!!", __FUNCTION__, cTmp);
				APP_TRACE(szDbgMsg);

				memset(szTempVal, 0x00, sizeof(szTempVal));

				//For Partial EMV Trans Sending No CVM Performed
				if(isPartialEmvAllowed() && pstCardDtls->bPartialEMV)
				{
					if(pstPymtTran->pstSigDtls != NULL && pstPymtTran->pstSigDtls->szSign != NULL && (strlen(pstPymtTran->pstSigDtls->szSign) > 0) )
					{
						strcpy(pszTagValue, "Signature Captured");
						//breaking the switch case here
						break;
					}
					else
					{
						cTmp = 'F';
					}
				}
				/* Daivik:22/1/2016- For Offline Declined CTLS Transactions we do not prompt for PIN/Signature. Hence
				 * Hence we print NO Cvm Required.
				 */
				if((strcmp(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData, "00") == SUCCESS) && (pstCardDtls->iCardSrc == CRD_EMV_CTLS))
				{
					cTmp = 'F';
				}
				if(cTmp == '0')
				{
					strcpy(szTempVal, "Fail CVM Processing");
				}
				else if(cTmp >= '1' && cTmp <= '5')
				{
					//strcpy(pszTagValue, "No Signature Required - PIN Validated");
					if(bCVMSuccess)
					{
						strcpy(szTempVal, "Verified by PIN");

						if( (cTmp == '3' || cTmp == '5')
								&&
								(pstPymtTran->pstSigDtls != NULL && pstPymtTran->pstSigDtls->szSign != NULL && (strlen(pstPymtTran->pstSigDtls->szSign) > 0) ))
						{
							//strcpy(pszTagValue, "Signature Captured - PIN Validated");
							strcpy(szTempVal, "Signature Captured - Verified by PIN");
						}
					}
					else
					{
						strcpy(szTempVal, "PIN CVM Failed");
						if( (cTmp == '3' || cTmp == '5'))
						{
							if( (pstPymtTran->pstSigDtls != NULL && pstPymtTran->pstSigDtls->szSign != NULL && (strlen(pstPymtTran->pstSigDtls->szSign) > 0) ))
							{
								//strcpy(pszTagValue, "Signature Captured - PIN Validated");
								strcpy(szTempVal, "PIN CVM Failed - Signature Captured");
							}
						}
					}
				}
				else if( (cTmp == 'E')		&&
						 ((pstPymtTran->pstSigDtls != NULL && pstPymtTran->pstSigDtls->szSign != NULL && (strlen(pstPymtTran->pstSigDtls->szSign) > 0) ))
				)
				{
					strcpy(szTempVal, "Signature Captured");
				}
				else if(cTmp == 'F')
				{
					strcpy(szTempVal, "No CVM performed");
				}
				strcpy(pszTagValue, szTempVal);
			}
			else
			{
				if(pstPymtTran->pstSigDtls != NULL && pstPymtTran->pstSigDtls->szSign != NULL && (strlen(pstPymtTran->pstSigDtls->szSign) > 0) )
				{
					strcpy(pszTagValue, "Signature Captured");
				}
			}
		}
/*		else
		{
			debug_sprintf(szDbgMsg, "%s: Current EMV Tran details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case CANCEL_DISCLAIMER:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if( bEmvEnabled == PAAS_TRUE && iResltCode ==  59001) //ERR_USR_CANCELED
		{
			if( strlen("Transaction Cancelled") <= maxReceiptLineLen )
			{
				strcpy(pszTagValue, "Transaction Cancelled");
			}
//			debug_sprintf(szDbgMsg,"%s: Cancelled Disclaimer[%s]",__FUNCTION__, pszTagValue);
//			APP_TRACE(szDbgMsg);
		}
		break;
		
	case SEQUENCE:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstCardDtls != NULL && pstCardDtls->bEmvData != PAAS_FALSE)
		{
			if(strlen(pstCardDtls->stEmvtranDlts.szTermTranSeqCount) > 0)
			{
				strcpy(pszTagValue, pstCardDtls->stEmvtranDlts.szTermTranSeqCount);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Sequence not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
/*		else
		{
			debug_sprintf(szDbgMsg, "%s: Current Tran details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case APP_ID:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstCardDtls != NULL && pstCardDtls->bEmvData != PAAS_FALSE)
		{
			if(strlen(pstCardDtls->stEmvAppDtls.szAID) > 0)
			{
				strcpy(pszTagValue, pstCardDtls->stEmvAppDtls.szAID);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: AID not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
/*		else
		{
			debug_sprintf(szDbgMsg, "%s: EMV details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case TVR:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstCardDtls != NULL && pstCardDtls->bEmvData != PAAS_FALSE)
		{
			if(strlen(pstCardDtls->stEmvTerDtls.szTermVerificationResults) > 0)
			{
				strcpy(pszTagValue, pstCardDtls->stEmvTerDtls.szTermVerificationResults);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s:Terminal Verification Result not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
/*		else
		{
			debug_sprintf(szDbgMsg, "%s: EMV details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;

	case TSI:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstCardDtls != NULL && pstCardDtls->bEmvData != PAAS_FALSE)
		{
			if(strlen(pstCardDtls->stEmvtranDlts.szTranStatusInfo) > 0)
			{
				strcpy(pszTagValue, pstCardDtls->stEmvtranDlts.szTranStatusInfo);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s:Terminal Status Info not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
/*		else
		{
			debug_sprintf(szDbgMsg, "%s: EMV details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;
	case IAD:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstCardDtls != NULL && pstCardDtls->bEmvData != PAAS_FALSE)
		{
			if(strlen(pstCardDtls->stEmvIssuerDtls.szIssuerAppData) > 0)
			{
				strcpy(pszTagValue, pstCardDtls->stEmvIssuerDtls.szIssuerAppData);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s:IAD not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
/*		else
		{
			debug_sprintf(szDbgMsg, "%s: EMV details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;
	case ARC:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstCardDtls != NULL && pstCardDtls->bEmvData != PAAS_FALSE)
		{
			if(strlen(pstCardDtls->stEmvtranDlts.szAuthRespCode) > 0)
			{
				strcpy(pszTagValue, pstCardDtls->stEmvtranDlts.szAuthRespCode);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s:ARC not available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
/*		else
		{
			debug_sprintf(szDbgMsg, "%s: EMV details not available!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}*/
		break;
	case FGN_CURR:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstDCCdtls != NULL)
		{
			if(strlen(pstDCCdtls->szAlphaCurrCode) > 0)
			{
				strcpy(pszTagValue, pstDCCdtls->szAlphaCurrCode);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s:Currency Code Not Available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		break;
	case EXG_RATE:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstDCCdtls != NULL)
		{
			if(strlen(pstDCCdtls->szExchngRate) > 0)
			{
				sprintf(pszTagValue, "%.4lf", atof(pstDCCdtls->szExchngRate));
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s:Exchange Rate Not Available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		break;
	case MARGIN:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstDCCdtls != NULL)
		{
			if(strlen(pstDCCdtls->szMarginRatePercentage) > 0)
			{
				sprintf(pszTagValue, " %.2lf %% Margin", atof(pstDCCdtls->szMarginRatePercentage));
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s:Margin  Not Available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		break;
	case DCC_TRAN_AMOUNT:
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstDCCdtls != NULL)
		{
			if(strlen(pstDCCdtls->szFgnAmount) > 0)
			{
				memset(szTempVal, 0x00, sizeof(szTempVal));
				if(pstDCCdtls->iMinorUnits > 0)
				{
					getFGNAmntWithDecimals(pstDCCdtls->szFgnAmount, pstDCCdtls->iMinorUnits, szTempVal);
					sprintf(pszTagValue, "%s %s", pstDCCdtls->szAlphaCurrCode, szTempVal);
				}
				else
				{
					sprintf(pszTagValue, "%s %s", pstDCCdtls->szAlphaCurrCode, pstDCCdtls->szFgnAmount);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: DCC Tran Amount Not Available!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		break;
	case DCC_DISCLAIMER1:/* Getting the DCCDisclaimer1 from the config file*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstDCCdtls != NULL)
		{
			iRetVal = getRcptDCCDisclaimer(FIRST, pszTagValue);
		}
		break;

	case DCC_DISCLAIMER2: /* Getting the DCCDisclaimer2 from the config file*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstDCCdtls != NULL)
		{
			iRetVal = getRcptDCCDisclaimer(SECOND, pszTagValue);
		}
		break;

	case DCC_DISCLAIMER3:/* Getting the DCCDisclaimer3 from the config file*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstDCCdtls != NULL)
		{
			iRetVal = getRcptDCCDisclaimer(THIRD, pszTagValue);
		}
		break;

	case DCC_DISCLAIMER4: /* Getting the Header1 from the config file*/
		memset(pszTagValue, 0x00, maxReceiptLineLen);
		if(pstDCCdtls != NULL)
		{
			iRetVal = getRcptDCCDisclaimer(FOURTH, pszTagValue);
		}
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Invalid Tag ID!!! Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		iRetVal = FAILURE;
		break;
	}

/*	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, pszTagValue);
	APP_TRACE(szDbgMsg);*/

	return iRetVal;
}



/*
 * ============================================================================
 * Function Name: addRecieptDataNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addRecieptDataNode(VAL_LST_PTYPE valLstPtr, char* szVal)
{
	int				iRetVal				= SUCCESS;
	VAL_NODE_PTYPE	tmpNodePtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	tmpNodePtr = (VAL_NODE_PTYPE) malloc(sizeof(VAL_NODE_STYPE));
	if(tmpNodePtr != NULL)
	{
		/* Initialize the allocated memory */
		memset(tmpNodePtr, 0x00, sizeof(VAL_NODE_STYPE));
		tmpNodePtr->szVal = szVal;

		/* Add the node to the list */
		if(valLstPtr->end == NULL)
		{
			/* Adding the first element */
			valLstPtr->start = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

			//debug_sprintf(szDbgMsg, "%s: Adding first node", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			/* Adding the nth element */
			valLstPtr->end->next = tmpNodePtr;
			valLstPtr->end = tmpNodePtr;

			//debug_sprintf(szDbgMsg, "%s: Adding nth node", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}

		/* Increment the node count */
		valLstPtr->valCnt += 1;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		iRetVal = FAILURE;
	}

	//debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	//APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: addEmptyPropNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addEmptyPropNode(xmlNode * curNode, VAL_LST_PTYPE valLstPtr)
{
	int 		iRetVal 		 = SUCCESS;
	char  *     pszTagVal        = NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	pszTagVal = (char*)malloc((maxReceiptLineLen + 10) ); //+10 for safety purpose
	if (pszTagVal == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation failed for szTagVal", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(pszTagVal, 0x00, maxReceiptLineLen + 10);

	sprintf (pszTagVal,"%*c", maxReceiptLineLen, ' ');
	addRecieptDataNode(valLstPtr, pszTagVal);

	//debug_sprintf(szDbgMsg, "%s: returned [%d]", __FUNCTION__, iRetVal);
	//APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: parseAndAddSinglePropNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseAndAddSinglePropNode(xmlNode * curNode, TRAN_PTYPE pstTran, VAL_LST_PTYPE valLstPtr, int iResltCode)
{
	int 				iRetVal 		  		= SUCCESS;
	int					iPosition         		= 0;
	int					iTagCount 				= 0;
	char	   			szTempTagVal1[256]		= "";
	char	   			szTempTagVal2[256] 		= "";
	char    *  			pszTagVal         		= NULL;
	xmlChar * 			szTagid			  		= NULL;
	xmlChar * 			szAlign			  		= NULL;
	xmlChar * 			szDispName 		  		= NULL;
	xmlChar * 			szFormat        		= NULL;
	xmlChar *   		szPosition      		= NULL;
	xmlChar * 			szValue			 		= NULL;
	static PAAS_BOOL	bEmvEnabled				= PAAS_FALSE;
	static PAAS_BOOL	bVerboseRecptEnabled	= PAAS_FALSE;	
	static PAAS_BOOL 	bFirstTime				= PAAS_TRUE;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	if(bFirstTime == PAAS_TRUE)
	{
		//Get EMV status
		bEmvEnabled = isEmvEnabledInDevice();
		//Get Verbose Receipt status
		bVerboseRecptEnabled = isVerboseRecptEnabled();
		
		bFirstTime = PAAS_FALSE;
	}

	szTagid    = xmlGetProp(curNode, (const xmlChar *)"tagid");
	szAlign    = xmlGetProp(curNode, (const xmlChar *)"align");
	szFormat   = xmlGetProp( curNode, (const xmlChar *)"format");
	szPosition = xmlGetProp( curNode, (const xmlChar *)"pos");
	szDispName = xmlGetProp(curNode, (const xmlChar *)"DispName");

	while(1)
	{
		/*Should not free this memory. It will be freed once the xml is sent to the pos*/
		pszTagVal = (char*)malloc((maxReceiptLineLen + 10)); //+10 for safety purpose
		if (pszTagVal == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation failed for szTagVal", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		memset(pszTagVal, 0x00, maxReceiptLineLen + 10);

		if(szTagid != NULL) //Checking whether Tagid is present
		{
			memset(szTempTagVal1, 0x00, sizeof(szTempTagVal1));
			memset(szTempTagVal2, 0x00, sizeof(szTempTagVal2));
			if( strcasecmp("EmvTag", (char*)szTagid) == SUCCESS) //Checking whether the node name is EmvTag
			{
				if( bEmvEnabled == PAAS_TRUE && bVerboseRecptEnabled == PAAS_TRUE )
				{
					if( vrbseRecptTagList[iTagCount] != NULL )
					{
						iRetVal = getEmvTagValue(vrbseRecptTagList[iTagCount], iTagCount, szTempTagVal2, pstTran);
						if(iRetVal != SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: EMV Tag Values not present; Skip",__FUNCTION__);
							APP_TRACE(szDbgMsg);

							free(pszTagVal);
							pszTagVal = NULL;
							
							iRetVal = SUCCESS;
							break;
						}							
						iTagCount++;
					}
					else
					{
						//Finished Adding All tags.
						free(pszTagVal);
						pszTagVal = NULL;
						break;
					}
				}
			}
			else
			{
				iRetVal = getTagIDValue((char*)szTagid, (char *)szFormat, szTempTagVal2, pstTran, iResltCode);

				if(iRetVal != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while fetching the corresponding value for %s Tag", __FUNCTION__, szTagid);
					APP_TRACE(szDbgMsg);
					// CID-67451: 28-Jan-16: MukeshS3: Free the memory, if failed to get the tag value for the corresponding tag id.
					free(pszTagVal);
					pszTagVal = NULL;
					break;
				}
			}
//			debug_sprintf(szDbgMsg, "%s: Tag Value is %s", __FUNCTION__, szTempTagVal1);
//			APP_TRACE(szDbgMsg);
			if (szDispName != NULL) //Display name is present
			{
				//debug_sprintf(szDbgMsg, "%s: Display name is present", __FUNCTION__);
				//APP_TRACE(szDbgMsg);

				if(strlen(szTempTagVal2) != 0) //Tag Value is present
				{
					sprintf(szTempTagVal1, "%s:%s", szDispName, szTempTagVal2);
					memset(szTempTagVal2, 0x00, sizeof(szTempTagVal2));
					if( strlen(szTempTagVal1) > maxReceiptLineLen )
					{
						memcpy(szTempTagVal2, &szTempTagVal1[maxReceiptLineLen], maxReceiptLineLen);
					}
				}
				else //Tag Value is not present, putting only display name
				{
					/* DAIVIK:14-11-2015 - Not performing the below free will cause the tagvalue variable which was allocated to leak.
					 * For every tag value not found, we could leak upto 60 bytes.
					 */
					if(pszTagVal != NULL)
					{
						free(pszTagVal);
						pszTagVal = NULL;
					}
					//Praveen_P1: FIX_ME: If the value is not present, not displaying the display name also
					//sprintf(pszTagVal, "%s", szDispName);
					break;
				}
			}
			else  //Display name is not present, showing only value
			{
				//debug_sprintf(szDbgMsg, "%s: Display name is not present", __FUNCTION__);
				//APP_TRACE(szDbgMsg);
				sprintf(szTempTagVal1, "%s", szTempTagVal2);
				memset(szTempTagVal2, 0x00, sizeof(szTempTagVal2));
				if( strlen(szTempTagVal1) > maxReceiptLineLen )
				{
					memcpy(szTempTagVal2, &szTempTagVal1[maxReceiptLineLen], maxReceiptLineLen);
				}
			}
			if( strlen(szTempTagVal1) > 0 )
			{
				memcpy(pszTagVal, szTempTagVal1, maxReceiptLineLen);

				//debug_sprintf(szDbgMsg, "%s: Tag Value after adding Display name(if present) [%s] ", __FUNCTION__, pszTagVal);
				//APP_TRACE(szDbgMsg);
				if(szAlign != NULL) //Checking whether Align is present
				{
					//debug_sprintf(szDbgMsg, "%s: align prop is present", __FUNCTION__);
					//APP_TRACE(szDbgMsg);

					if(szPosition != NULL)
					{
						iPosition = atoi((char *)szPosition);
					}
					if(strlen(pszTagVal) > 0) //Aligning only if the value present
					{
						alignString(pszTagVal, (char*)szAlign, iPosition, maxReceiptLineLen);
					}

				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Alignment is not present, defaulting to LEFT", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(strlen(pszTagVal) > 0) //Aligning only if the value present
					{
						alignString(pszTagVal, "left", iPosition, maxReceiptLineLen); //Considering the default alignment is LEFT
					}
				}

				debug_sprintf(szDbgMsg, "%s: Value to be added to the Receipt list [%s], Len [%d]",
																	__FUNCTION__, pszTagVal, strlen(pszTagVal));
				APP_TRACE(szDbgMsg);

				if( strlen(pszTagVal) > 0) //Adding to the list if the value presents only
				{
					addRecieptDataNode(valLstPtr, pszTagVal);
				}
				else
				{
					free(pszTagVal);
					pszTagVal = NULL;
					//debug_sprintf(szDbgMsg, "%s: Tag Value not present, not adding to the receipt list", __FUNCTION__);
					//APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				/* DAIVIK:14-11-2015 - Not performing the below free will cause the tagvalue variable which was allocated to leak.
				 * For every tag value not found, we could leak upto 60 bytes.
				 */
				if(pszTagVal != NULL)
				{
					free(pszTagVal);
					pszTagVal = NULL;
				}

				//AjayS2: Since from the list of Emv Tags, some may not be present for all transactions, like Issuer Scripts etc,
				//so in that cases we should not break, we will continue, we have to proceed for adding more tags.
				if(strcasecmp("EmvTag", (char*)szTagid) != SUCCESS)
				{
					//Praveen_P1: FIX_ME: If the value is not present, not displaying the display name also
					//sprintf(pszTagVal, "%s", szDispName);
					break;
				}
			}
			/*ArjunU1: For verbose receipt; If EMV tag value is greater than maxReceiptLineLen then
						we need to add remaining value in the next line of the receipt.*/
			if( strcasecmp("EmvTag", (char*)szTagid) == SUCCESS )
			{
				if( strlen(szTempTagVal2) > 0 )
				{
					/*Should not free this memory. It will be freed once the xml is sent to the pos*/
					pszTagVal = (char*)malloc((maxReceiptLineLen + 10)); //+10 for safety purpose
					if (pszTagVal == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Memory allocation failed for szTagVal", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						return FAILURE;
					}
					memset(pszTagVal, 0x00, maxReceiptLineLen + 10);

					memcpy(pszTagVal, szTempTagVal2, maxReceiptLineLen);

					if(szAlign != NULL) //Checking whether Align is present
					{
						//debug_sprintf(szDbgMsg, "%s: align prop is present", __FUNCTION__);
						//APP_TRACE(szDbgMsg);

						if(szPosition != NULL)
						{
							iPosition = atoi((char *)szPosition);
						}
						if(strlen(pszTagVal) > 0) //Aligning only if the value present
						{
							alignString(pszTagVal, (char*)szAlign, iPosition, maxReceiptLineLen);
						}

					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Alignment is not present, defaulting to LEFT", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						if(strlen(pszTagVal) > 0) //Aligning only if the value present
						{
							alignString(pszTagVal, "left", iPosition, maxReceiptLineLen); //Considering the default alignment is LEFT
						}
					}

					debug_sprintf(szDbgMsg, "%s: Value to be added to the Receipt list [%s], Len [%d]",
																		__FUNCTION__, pszTagVal, strlen(pszTagVal));
					APP_TRACE(szDbgMsg);

					if( strlen(pszTagVal) > 0) //Adding to the list if the value presents only
					{
						addRecieptDataNode(valLstPtr, pszTagVal);
					}
					else
					{
						free(pszTagVal);
						pszTagVal = NULL;
						//debug_sprintf(szDbgMsg, "%s: Tag Value not present, not adding to the receipt list", __FUNCTION__);
						//APP_TRACE(szDbgMsg);
					}
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Tag ID not present!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: Search for Value!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			szValue = xmlGetProp(curNode, (const xmlChar *)"value");
			if (szValue != NULL)
			{
				sprintf(pszTagVal, "%s", szValue);

				if(szAlign != NULL) //Checking whether Align is present
				{
					//debug_sprintf(szDbgMsg, "%s: align prop is present", __FUNCTION__);
					//APP_TRACE(szDbgMsg);

					if(szPosition != NULL)
					{
						iPosition = atoi((char *)szPosition);
					}
					if(strlen(pszTagVal) > 0) //Aligning only if the value present
					{
						alignString(pszTagVal, (char*)szAlign, iPosition, maxReceiptLineLen);
					}

				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Alignment is not present, defaulting to Center", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					if(strlen(pszTagVal) > 0) //Aligning only if the value present
					{
						alignString(pszTagVal, "Center", iPosition, maxReceiptLineLen); //Considering the default alignment is LEFT
					}
				}

				debug_sprintf(szDbgMsg, "%s: Value to be added to the Receipt list [%s]", __FUNCTION__, pszTagVal);
				APP_TRACE(szDbgMsg);

				if(strlen(pszTagVal) > 0) //Adding to the list if the value presents only
				{
					addRecieptDataNode(valLstPtr, pszTagVal);
				}
				else
				{
					free(pszTagVal);
					pszTagVal = NULL;

					debug_sprintf(szDbgMsg, "%s: Tag Value not present, not adding to the receipt list", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				free(pszTagVal);
				pszTagVal = NULL;

				debug_sprintf(szDbgMsg, "%s: Value not present!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		//CID 67409, 67446 (#1 of 1): Dereference after null check (FORWARD_NULL) T_RaghavendranR1. Check szTagid for NULL and DeRefernce it.
		if((szTagid != NULL) && (strcasecmp("EmvTag", (char*)szTagid) == SUCCESS)) //Checking whether the node name is EmvTag
		{
			if( bEmvEnabled == PAAS_TRUE && bVerboseRecptEnabled == PAAS_TRUE )
			{
				//Don't break from the loop until all Emv Tags added to the receipt.
/*				debug_sprintf(szDbgMsg, "%s: Adding next EMV tag", __FUNCTION__);
				APP_TRACE(szDbgMsg);*/
				
				continue;
			}
			else
			{
				break; //Finished Adding child
			}	
		}
		else 
		{
			break; //Finished adding child
		}		
	}

	if(szAlign)
	{
		xmlFree(szAlign);
	}
	if(szTagid)
	{
		xmlFree(szTagid);
	}
	if(szFormat)
	{
		xmlFree(szFormat);
	}
	if(szPosition)
	{
		xmlFree(szPosition);
	}
	if(szDispName)
	{
		xmlFree(szDispName);
	}
	if (szValue)
	{
		xmlFree(szValue);
	}

	//debug_sprintf(szDbgMsg, "%s: returned [%d]", __FUNCTION__, iRetVal);
	//APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: parseAndAddMultiPropNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseAndAddMultiPropNode(xmlNode * rootPtr, TRAN_PTYPE pstTran, VAL_LST_PTYPE valLstPtr, int iResltCode)
{
	int 		iRetVal 		 = SUCCESS;
	xmlChar *   szTagid          = NULL;
	PYMTTRAN_PTYPE pstPymtTran	 = NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	//debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	pstPymtTran = (PYMTTRAN_PTYPE) (pstTran->data);
	if(pstPymtTran == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Missing Payment details",__FUNCTION__);
		APP_TRACE(szDbgMsg);

		return FAILURE;
	}

	szTagid = xmlGetProp(rootPtr, (const xmlChar *)"tagid");
	if(szTagid != NULL)
	{
		if (pstPymtTran->pstSess == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Session Details in the Payment details is null", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}

		if(strcasecmp((char*)szTagid, "lineItems") == SUCCESS) //It is Line Items Node
		{
			debug_sprintf(szDbgMsg, "%s: LineItems Node", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = addLineItemsToReceipt(rootPtr, pstPymtTran->pstSess, valLstPtr);
			if(iRetVal == SUCCESS)
			{
				//debug_sprintf(szDbgMsg, "%s: Successfully added Lineitems (if present) to the receipt list", __FUNCTION__);
				//APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding Lineitems to the receipt list", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else if(strcasecmp((char*)szTagid, "lineItemHeader") == SUCCESS) //It is line items header
		{
			debug_sprintf(szDbgMsg, "%s: LineItems Header Node", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			iRetVal = addLineItemheaderToReceipt(rootPtr, pstPymtTran->pstSess, valLstPtr);
			if(iRetVal == SUCCESS)
			{
				//debug_sprintf(szDbgMsg, "%s: Successfully added Lineitem header to the receipt list", __FUNCTION__);
				//APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding Lineitem header to the receipt list", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Contains tagid but not lineitems and lineitemheader!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//Should not be case...currently have only multi prop line with LineItems and lineitemheader tagid
			//memset(szVal, 0x00, maxReceiptLineLen);
		}

		if(szTagid)
		{
			xmlFree(szTagid);
		}
	}
	else
	{

		debug_sprintf(szDbgMsg, "%s: Non-LineItems Node", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		iRetVal = addMultiLineToReceipt(rootPtr, pstTran, valLstPtr, iResltCode);
		if(iRetVal == SUCCESS)
		{
			//debug_sprintf(szDbgMsg, "%s: Successfully added Lineitem header to the receipt list", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding Lineitem header to the receipt list", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}


	//debug_sprintf(szDbgMsg, "%s: returned [%d]", __FUNCTION__, iRetVal);
	//APP_TRACE(szDbgMsg);

	return iRetVal;
}


/*
 * ============================================================================
 * Function Name: addMultiLineToReceipt
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addMultiLineToReceipt(xmlNode * rootPtr, TRAN_PTYPE pstTran, VAL_LST_PTYPE valLstPtr, int iResltCode)
{
	int 		iRetVal 		 = SUCCESS;
	xmlNode *	curNode			 = NULL;
	char    *   pszTagVal        = NULL;
	xmlChar * 	szTagid			 = NULL;
	xmlChar * 	szAlign			 = NULL;
	xmlChar * 	szDispName 		 = NULL;
	xmlChar * 	szFormat         = NULL;
	xmlChar *   szPosition       = NULL;
	xmlChar * 	szValue			 = NULL;
	char        szTempTagVal[256]= "";
	char		szTemp[256]      = "";
	int         iPosition        = 0;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

/*	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);*/


	pszTagVal = (char*)malloc((maxReceiptLineLen + 10) * sizeof(char)); //+10 for safety purpose
	if (pszTagVal == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation failed for szTagVal", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}
	memset(pszTagVal, 0x00, maxReceiptLineLen + 10);

	memset(pszTagVal, ' ', maxReceiptLineLen);

	curNode = rootPtr->xmlChildrenNode;

	while(curNode != NULL)
	{
		if(curNode->type == 3)
		{
			curNode = curNode->next;
			continue;
		}

		if(strcasecmp((char *)curNode->name, "Data") != SUCCESS) //Checking whether the node name is Data
		{
			debug_sprintf(szDbgMsg, "%s: Current Node is not DATA", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			curNode = curNode->next;
			continue;
		}

		szTagid    = xmlGetProp(curNode, (const xmlChar *)"tagid");
		szAlign    = xmlGetProp(curNode, (const xmlChar *)"align");
		szFormat   = xmlGetProp(curNode, (const xmlChar *)"format");
		szPosition = xmlGetProp(curNode, (const xmlChar *)"pos");
		szDispName = xmlGetProp(curNode, (const xmlChar *)"DispName");

		while(1)
		{
			memset(szTemp, 0x00, sizeof(szTemp));
			memset(szTempTagVal, 0x00, sizeof(szTempTagVal));
			if(szTagid != NULL)
			{
				iRetVal = getTagIDValue((char*)szTagid, (char *)szFormat, szTemp, pstTran, iResltCode);
				if(iRetVal != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while fetching the corresponding value for %s Tag", __FUNCTION__, szTagid);
					APP_TRACE(szDbgMsg);
					break;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Tag Id is not present!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				debug_sprintf(szDbgMsg, "%s: Search for Value!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				szValue = xmlGetProp(curNode, (const xmlChar *)"value");
				if (szValue != NULL)
				{
					sprintf(szTemp, "%s", szValue);
					xmlFree(szValue);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Value property not present!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
			}

/*			debug_sprintf(szDbgMsg, "%s: Tag Value is %s", __FUNCTION__, szTemp);
			APP_TRACE(szDbgMsg);*/

			if (szDispName != NULL) //Display name is present
			{
				debug_sprintf(szDbgMsg, "%s: Display name is present", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(strlen(szTemp) != 0) //Tag Value is present
				{
					sprintf(szTempTagVal, "%s:%s", szDispName, szTemp);
				}
				else //Tag Value is not present, not putting display name also
				{

				}
			}
			else  //Display name is not present, showing only value
			{
				debug_sprintf(szDbgMsg, "%s: Display name is not present", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				sprintf(szTempTagVal, "%s", szTemp);
			}

			debug_sprintf(szDbgMsg, "%s: Tag Value after adding Display name(if present) [%s] ", __FUNCTION__, szTempTagVal);
			APP_TRACE(szDbgMsg);

			if(szAlign != NULL) //Checking whether Align is present
			{
				//debug_sprintf(szDbgMsg, "%s: align prop is present", __FUNCTION__);
				//APP_TRACE(szDbgMsg);

				if((strcasecmp((char*)szAlign, "custom") == SUCCESS)  && (szPosition != NULL))
				{
					iPosition = atoi((char *)szPosition);
					strncpy(&pszTagVal[iPosition], szTempTagVal, strlen(szTempTagVal));
				}
				else if(strcasecmp((char*)szAlign, "right") == SUCCESS)
				{
					iPosition = maxReceiptLineLen - strlen(szTempTagVal);
					strncpy(&pszTagVal[iPosition], szTempTagVal, strlen(szTempTagVal));
				}
				else if(strcasecmp((char*)szAlign, "left") == SUCCESS)
				{
					iPosition = 0;
					strncpy(&pszTagVal[iPosition], szTempTagVal, strlen(szTempTagVal)); //Copying from the left most position
				}
				else if(strcasecmp((char*)szAlign, "center") == SUCCESS)
				{
					iPosition = (maxReceiptLineLen - strlen(szTempTagVal))/2;
					strncpy(&pszTagVal[iPosition], szTempTagVal, strlen(szTempTagVal));
				}
				else if(strcasecmp((char*)szAlign, "justify") == SUCCESS)
				{
					//Praveen_P1: TODO Need to consider this alignment
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Alignment is not present, defaulting to left", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iPosition = 0;
				strcpy(&pszTagVal[iPosition], szTempTagVal); //Copying from the left most position
			}

			break;

		}//end of while loop

		if(szAlign)
		{
			xmlFree(szAlign);
		}
		if(szTagid)
		{
			xmlFree(szTagid);
		}
		if(szFormat)
		{
			xmlFree(szFormat);
		}
		if(szPosition)
		{
			xmlFree(szPosition);
		}
		if(szDispName)
		{
			xmlFree(szDispName);
		}

		iPosition = 0;
		curNode = curNode->next; //Going to the next child node
	}//end of while checking for curNode

	debug_sprintf(szDbgMsg, "%s: Value to be added to the Receipt list [%s]", __FUNCTION__, pszTagVal);
	APP_TRACE(szDbgMsg);

	if(strlen(pszTagVal) > 0) //Adding to the list if the value presents only
	{
		addRecieptDataNode(valLstPtr, pszTagVal);
	}
	else
	{
		/* Daivik:25/1/2016: Coverity 67236. Free the pointer which is not added to receipt list */
		free(pszTagVal);
		debug_sprintf(szDbgMsg, "%s: Tag Value not present, not adding to the receipt list", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

/*	debug_sprintf(szDbgMsg, "%s: returned [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);*/

	return iRetVal;
}
/*
 * ============================================================================
 * Function Name: addLineItemheaderToReceipt
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addLineItemheaderToReceipt(xmlNode * rootPtr, SESSDTLS_PTYPE pstSessionDtls, VAL_LST_PTYPE valLstPtr)
{
	int 		iRetVal 		 = SUCCESS;
	xmlNode *	curNode			 = NULL;
	char    *   pszHeaderTagVal  = NULL;
	xmlChar * 	szTagid			 = NULL;
	xmlChar * 	szAlign			 = NULL;
	xmlChar * 	szDispName 		 = NULL;
	xmlChar * 	szFormat         = NULL;
	xmlChar *   szPosition       = NULL;
	xmlChar * 	szValue			 = NULL;
	char        szTempTagVal[256]= "";
	int         iPosition        = 0;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

/*	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);*/

	if(pstSessionDtls->pstLIInfo == NULL)
	{
/*		debug_sprintf(szDbgMsg, "%s: Data System does not contain any line items, no need to add header", __FUNCTION__);
		APP_TRACE(szDbgMsg);*/
		return iRetVal;
	}

	if(pstSessionDtls->pstLIInfo->iItemCnt == 0 && pstSessionDtls->pstLIInfo->iOfferCnt == 0)
	{
		debug_sprintf(szDbgMsg, "%s: Data System does not contain any line items, no need to add header", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	pszHeaderTagVal = (char*)malloc((maxReceiptLineLen + 10) * sizeof(char)); //+10 for safety purpose
	if (pszHeaderTagVal == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Memory allocation failed for szTagVal", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}


	memset(pszHeaderTagVal, 0x00, maxReceiptLineLen + 10);

	memset(pszHeaderTagVal, ' ', maxReceiptLineLen);

	curNode = rootPtr->xmlChildrenNode;

	while(curNode != NULL)
	{
		if(curNode->type == 3)
		{
			curNode = curNode->next;
			continue;
		}

		if(strcasecmp((char *)curNode->name, "Data") != SUCCESS) //Checking whether the node name is Data
		{
			debug_sprintf(szDbgMsg, "%s: Current Node is not DATA", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			curNode = curNode->next;
			continue;
		}

		szTagid    = xmlGetProp(curNode, (const xmlChar *)"tagid");
		szAlign    = xmlGetProp(curNode, (const xmlChar *)"align");
		szFormat   = xmlGetProp(curNode, (const xmlChar *)"format");
		szPosition = xmlGetProp(curNode, (const xmlChar *)"pos");
		szDispName = xmlGetProp(curNode, (const xmlChar *)"DispName");
		while(1)
		{
			if(szTagid == NULL)
			{
/*				debug_sprintf(szDbgMsg, "%s: Tag Id is not present!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);*/

/*				debug_sprintf(szDbgMsg, "%s: Searching for property value!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);*/

				szValue = xmlGetProp(curNode, (const xmlChar *)"value");
				if (szValue != NULL)
				{
					/*Copying szValue pointer so that it gets printed if display name is not given*/
					szTagid = szValue;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Value property not present!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
			}

//			debug_sprintf(szDbgMsg, "%s: Tag id is %s", __FUNCTION__, szTagid);
//			APP_TRACE(szDbgMsg);

			if (szDispName != NULL) //Display name is present
			{
/*				debug_sprintf(szDbgMsg, "%s: Display name is present", __FUNCTION__);
				APP_TRACE(szDbgMsg);*/

				sprintf(szTempTagVal, "%s", (char *)szDispName);

			}
			else  //Display name is not present, showing only value
			{
/*				debug_sprintf(szDbgMsg, "%s: Display name is not present for the line item header", __FUNCTION__);
				APP_TRACE(szDbgMsg);*/

				sprintf(szTempTagVal, "%s", (char *)szTagid); //Praveen_P1: if display name is not present putting tagid itself there
			}

/*			debug_sprintf(szDbgMsg, "%s: Tag Value after adding Display name [%s] ", __FUNCTION__, szTempTagVal);
			APP_TRACE(szDbgMsg);*/

			if(szAlign != NULL) //Checking whether Align is present
			{
				//debug_sprintf(szDbgMsg, "%s: align prop is present", __FUNCTION__);
				//APP_TRACE(szDbgMsg);

				if((strcasecmp((char*)szAlign, "custom") == SUCCESS)  && (szPosition != NULL))
				{
					iPosition = atoi((char *)szPosition);

					strncpy(&pszHeaderTagVal[iPosition], szTempTagVal, strlen(szTempTagVal));

				}
				else if(strcasecmp((char*)szAlign, "right") == SUCCESS)
				{
					iPosition = maxReceiptLineLen - strlen(szTempTagVal);
					strncpy(&pszHeaderTagVal[iPosition], szTempTagVal, strlen(szTempTagVal));
				}
				else if(strcasecmp((char*)szAlign, "left") == SUCCESS)
				{
					iPosition = 0;
					strncpy(&pszHeaderTagVal[iPosition], szTempTagVal, strlen(szTempTagVal)); //Copying from the left most position
				}
				else if(strcasecmp((char*)szAlign, "center") == SUCCESS)
				{
					iPosition = (maxReceiptLineLen - strlen(szTempTagVal))/2;
					strncpy(&pszHeaderTagVal[iPosition], szTempTagVal, strlen(szTempTagVal));
				}
				else if(strcasecmp((char*)szAlign, "justify") == SUCCESS)
				{
					//Praveen_P1: TO_DO Need to consider this alignment
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Alignment is not present, defaulting to left", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iPosition = 0;
				strcpy(&pszHeaderTagVal[iPosition], szTempTagVal); //Copying from the left most position
			}

			break;

		}//end of while loop

		if(szAlign)
		{
			xmlFree(szAlign);
		}
		if(szTagid)
		{
			xmlFree(szTagid);
		}
		if(szFormat)
		{
			xmlFree(szFormat);
		}
		if(szPosition)
		{
			xmlFree(szPosition);
		}
		if(szDispName)
		{
			xmlFree(szDispName);
		}

		iPosition = 0;
		curNode = curNode->next; //Going to the next child node
	}//end of while checking for curNode

/*	debug_sprintf(szDbgMsg, "%s: Line item header to be added to the Receipt list [%s]", __FUNCTION__, pszHeaderTagVal);
	APP_TRACE(szDbgMsg);*/

	if(strlen(pszHeaderTagVal) > 0) //Adding to the list if the value presents only
	{
		addRecieptDataNode(valLstPtr, pszHeaderTagVal); //Adding the header
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Tag Value not present, not adding to the receipt list", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		// CID-67481: 27-Jan-16: MukeshS3: Free memory, if it is not being added to the receipt list.
		free(pszHeaderTagVal);
	}


	debug_sprintf(szDbgMsg, "%s: returned [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: addLineItemsToReceipt
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addLineItemsToReceipt(xmlNode * rootPtr, SESSDTLS_PTYPE pstSessionDtls, VAL_LST_PTYPE valLstPtr)
{
	int 		  iRetVal 		 = SUCCESS;
	xmlNode *	  curNode		 = NULL;
	char    *     pszTagVal      = NULL;
	xmlChar * 	  szTagid		 = NULL;
	LI_NODE_PTYPE lineLstNode    = NULL;
	int			  iTagIDNum      = 0;
	char 		  szTemp[256]    = "";

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

/*	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);*/

	if(pstSessionDtls->pstLIInfo == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Data System does not contain any line items", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	lineLstNode = pstSessionDtls->pstLIInfo->mainLstHead;

	if(lineLstNode == NULL) //Checking whether Line items are present
	{
		debug_sprintf(szDbgMsg, "%s: Data System does not contain any line items", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return iRetVal;
	}

	while(lineLstNode != NULL) //Parse through the line item list and add each lineitem to the receipt list
	{
		//Allocate the memory for the receipt line
		pszTagVal = (char*)malloc((maxReceiptLineLen + 10) * sizeof(char)); //+10 for safety purpose
		if (pszTagVal == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Memory allocation failed for szTagVal", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return FAILURE;
		}
		memset(pszTagVal, 0x00, maxReceiptLineLen + 10);

		memset(pszTagVal, ' ', maxReceiptLineLen);

		curNode = rootPtr->xmlChildrenNode;

		while(curNode != NULL)
		{
			if(curNode->type == 3)
			{
				curNode = curNode->next;
				continue;
			}

			if(strcasecmp((char *)curNode->name, "Data") != SUCCESS) //Checking whether the node name is Data
			{
				debug_sprintf(szDbgMsg, "%s: Current Node is not DATA", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				curNode = curNode->next;
				continue;
			}

			szTagid    = xmlGetProp(curNode, (const xmlChar *)"tagid");

			if(szTagid != NULL)
			{
				iRetVal = getTagIDConstant((char *)szTagid, &iTagIDNum);
				if(iRetVal != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: Error while getting Tag Num for the given Tag ID", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}

				iRetVal = addLineItemTagVal(iTagIDNum, curNode, lineLstNode, pszTagVal);
				if(iRetVal == SUCCESS)
				{
/*					debug_sprintf(szDbgMsg, "%s: Successfully added Line item tag value to Receipt line", __FUNCTION__);
					APP_TRACE(szDbgMsg);*/
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Error while adding line item tag value to receipt line", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}

			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Tag ID is not present", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			iTagIDNum = 0;
			if(szTagid)
			{
				xmlFree(szTagid);
			}
			curNode = curNode->next;
		}

		debug_sprintf(szDbgMsg, "%s: Receipt line to be added is [%s]", __FUNCTION__, pszTagVal);
		APP_TRACE(szDbgMsg);

		memset(szTemp, 0x00, sizeof(szTemp));
		/*
		* Praveen_P1: Added the width specifier to the following scanf 
		*             statement to fix the cppcheck warning
		* Message: scanf without field width limits can crash with huge input data. 
		*          Add a field width specifier to fix this problem:
		*/
		sscanf(pszTagVal, "%50s", szTemp); //To check whether the buffer contains other than spaces

		if(strlen(szTemp) != 0) //Praveen_P1: TO_DO Modify this check to make it better
		{
			addRecieptDataNode(valLstPtr, pszTagVal);//Adding line item details to the receipt list
		}
		else
		{
			/* Daivik:27/1/2016- Coverity 67252 - Free the allocated resource in case we are not adding it to the list
			 * ptr != NULL handled in free wrapper function
			 */
			free(pszTagVal);
		}

		lineLstNode = lineLstNode->nextLI;
	}

/*	debug_sprintf(szDbgMsg, "%s: returned [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);*/

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: addLineItemTagVal
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params: none
 * ============================================================================
 */
static int addLineItemTagVal(int iTagIDNum, xmlNode * curNode, LI_NODE_PTYPE lineLstNode, char *pszTagVal)
{
	int 		  iRetVal 		 = SUCCESS;
	xmlChar * 	  szAlign		 = NULL;
	xmlChar * 	  szDispName 	 = NULL;
	xmlChar * 	  szFormat       = NULL;
	xmlChar *     szPosition     = NULL;
	xmlChar *     szMaxLen       = NULL;
	int			  iPosition      = 0;
	int           iMaxLen        = 0;
	char          szValue[40]    = "";
	MCNT_LI_PTYPE mcntDataPtr	 = NULL;
	OFF_LI_PTYPE  offerDtlsPtr	= NULL;
	char		  szTmpAmt[11]	= "";

/*
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
*/

/*	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);*/

	szAlign    = xmlGetProp(curNode, (const xmlChar *)"align");
	szFormat   = xmlGetProp(curNode, (const xmlChar *)"format");
	szPosition = xmlGetProp(curNode, (const xmlChar *)"pos");
	szDispName = xmlGetProp(curNode, (const xmlChar *)"DispName");
	szMaxLen   = xmlGetProp(curNode, (const xmlChar *)"maxLen");

	switch(lineLstNode->liType)
	{
	case MERCHANDISE:
		mcntDataPtr = (MCNT_LI_PTYPE) lineLstNode->liData;
		memset(szValue, 0x00, sizeof(szValue));
		switch(iTagIDNum)
		{
		case QUANTITY:
			strcpy(szValue, mcntDataPtr->szQty);
			break;
		case DESCRIPTION:
			strcpy(szValue, lineLstNode->szDesc);
			break;
		case UNIT_PRICE:
			formatAmount(mcntDataPtr->szUnitPrice, szTmpAmt, AMT_POSITIVE);
			strcpy(szValue, szTmpAmt);
			break;
		case PRICE:
			if(mcntDataPtr->szExtPrice[0] == '-')
			{
				formatAmount(mcntDataPtr->szExtPrice, szTmpAmt, AMT_NEGATIVE);
			}
			else
			{
				formatAmount(mcntDataPtr->szExtPrice, szTmpAmt, AMT_POSITIVE);
			}
			strcpy(szValue, szTmpAmt);
			break;
		default:
			break;
		}
		break;
	case OFFER:
		offerDtlsPtr = (OFF_LI_PTYPE) lineLstNode->liData;
		memset(szValue, 0x00, sizeof(szValue));
		switch(iTagIDNum)
		{
		case QUANTITY:
			strcpy(szValue, "1"); //default to 1 for coupon lines
			break;
		case DESCRIPTION:
			strcpy(szValue, lineLstNode->szDesc);
			break;
		case UNIT_PRICE:
			formatAmount(offerDtlsPtr->szOfferAmt, szTmpAmt, AMT_NEGATIVE);
			strcpy(szValue, szTmpAmt);
			break;
		case PRICE:
			formatAmount(offerDtlsPtr->szOfferAmt, szTmpAmt, AMT_NEGATIVE);
			strcpy(szValue, szTmpAmt);
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}

	if(szAlign != NULL)		// CID-67486: 2-Feb-16: MukeshS3: Adding NULL check before even accessing at the first place
	{
		if((strcasecmp((char*)szAlign, "custom") == SUCCESS)  && (szPosition != NULL))
		{
			if(szPosition != NULL)
			{
				iPosition = atoi((char *)szPosition);
			}
			if(szMaxLen != NULL)
			{
				iMaxLen = atoi((char *)szMaxLen);
			}
			if(iMaxLen == 0 || iMaxLen > strlen(szValue))
			{
				iMaxLen = strlen(szValue);
			}
			strncpy(&pszTagVal[iPosition], szValue, iMaxLen);
		}
		else if(strcasecmp((char*)szAlign, "right") == SUCCESS)
		{
			iPosition = maxReceiptLineLen - strlen(szValue);
			strncpy(&pszTagVal[iPosition], szValue, strlen(szValue));
		}
		else if(strcasecmp((char*)szAlign, "left") == SUCCESS)
		{
			iPosition = 0;
			strncpy(&pszTagVal[iPosition], szValue, strlen(szValue)); //Copying from the left most position
		}
		else if(strcasecmp((char*)szAlign, "center") == SUCCESS)
		{
			iPosition = (maxReceiptLineLen - strlen(szValue))/2;
			strncpy(&pszTagVal[iPosition], szValue, strlen(szValue));
		}
		else if(strcasecmp((char*)szAlign, "justify") == SUCCESS)
		{
			//Praveen_P1: TO_DO Need to consider this alignment
		}
	}

	if(szAlign)
	{
		xmlFree(szAlign);
	}
	if(szMaxLen)
	{
		xmlFree(szMaxLen);
	}
	if(szFormat)
	{
		xmlFree(szFormat);
	}
	if(szPosition)
	{
		xmlFree(szPosition);
	}
	if(szDispName)
	{
		xmlFree(szDispName);
	}

/*	debug_sprintf(szDbgMsg, "%s: returned [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);*/

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: parseXMLFileForReceipt
 *
 * Description	:
 *
 * Input Params	: File path
 *
 * Output Params: none
 * ============================================================================
 */
static int parseXMLFileForReceipt(char * szXMLReceipt, TRAN_PTYPE pstTran, VAL_LST_PTYPE valLstPtr, int iResltCode)
{
	int					iRetVal		     = SUCCESS;
	int					iLen			 = 0;
	static xmlDoc  *	docPtr			 = NULL;
	static xmlNode *	rootPtr			 = NULL;
	static xmlNode *	curNode			 = NULL;
	static xmlChar *	szProp			 = NULL;
	static PAAS_BOOL	bFirstTime		 = PAAS_TRUE;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ----", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(bFirstTime)
		{
			if( (szXMLReceipt == NULL) || ((iLen = strlen(szXMLReceipt)) <= 0) )
			{
				debug_sprintf(szDbgMsg, "%s: Invalid data passed as parameter",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = FAILURE;
				break;
			}

			/* Parse the XML file using the libxml API */
			docPtr = xmlParseFile(szXMLReceipt);
			if(docPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Parsing of the xml file failed",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = ERR_INV_XML_MSG;
				break;
			}

			/* Find the root node */
			rootPtr = xmlDocGetRootElement(docPtr);
			if(rootPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Empty xml data",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = ERR_INV_XML_MSG;
				break;
			}
			else if(strcmp((char *)rootPtr->name, "SCARcptTemplate")!= SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Incorrect xml document", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				iRetVal = ERR_INV_XML_MSG;
				break;
			}

			maxReceiptLineLen = getMaxReceiptLineLen();

			debug_sprintf(szDbgMsg, "%s: Maximum receipt line length set to %d",
					__FUNCTION__, maxReceiptLineLen);
			APP_TRACE(szDbgMsg);

			bFirstTime = PAAS_FALSE;
		}

		curNode = rootPtr->xmlChildrenNode;
		memset(valLstPtr, 0x00, sizeof(VAL_LST_STYPE));

		while(curNode != NULL)
		{
			if(curNode->type == 3)
			{
				curNode = curNode->next;
				continue;
			}

			if(strcasecmp((char *)curNode->name, "Line") != SUCCESS) //Checking whether the node name is Line
			{
				debug_sprintf(szDbgMsg, "%s: Current node is not LINE", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				curNode = curNode->next; //Proceed to the next child node
				continue;
			}

			szProp     = xmlGetProp(curNode, (const xmlChar *)"prop");

			if (szProp != NULL)
			{
				if(strcasecmp((char*)szProp, "empty") == SUCCESS)
				{
					iRetVal = addEmptyPropNode(curNode, valLstPtr);
					if(iRetVal == SUCCESS)
					{
						//debug_sprintf(szDbgMsg, "%s: Successfully added empty node to the receipt list", __FUNCTION__);
						//APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error while adding the empty prop node to the receipt list", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
				else if(strcasecmp((char*)szProp, "multi") == SUCCESS)
				{
					iRetVal = parseAndAddMultiPropNode(curNode, pstTran, valLstPtr, iResltCode);

					if(iRetVal == SUCCESS)
					{
						//debug_sprintf(szDbgMsg, "%s: Successfully parsed and added to the receipt list", __FUNCTION__);
						//APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error while parsing and adding the Multi prop node to the receipt list", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
				else if(strcasecmp((char*)szProp, "single") == SUCCESS)
				{
					iRetVal = parseAndAddSinglePropNode(curNode, pstTran, valLstPtr, iResltCode);

					if(iRetVal == SUCCESS)
					{
						//debug_sprintf(szDbgMsg, "%s: Successfully parsed and added to the receipt list", __FUNCTION__);
						//APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error while parsing and adding the Single prop node to the receipt list", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: %s Incorrect Property name in XML Document",
							__FUNCTION__, szProp);
					APP_TRACE(szDbgMsg);
				}
				if(szProp)
				{
					xmlFree(szProp);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: prop attribute not present!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			curNode = curNode->next; //Proceed to the next child node
		} //End of while loop checking for curNode

		break;
	} //End of while loop

/*
	if (rootPtr != NULL)
	{
		xmlFree(rootPtr);
	}
*/

/*	if(docPtr != NULL)
	{
		xmlFreeDoc(docPtr);
	}
*/
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iRetVal);
	APP_TRACE(szDbgMsg);

	return iRetVal;
}

/*
 * ============================================================================
 * Function Name: calculateSubTotal
 *
 * Description	: This API calculates the Subtotal of the transaction
 *
 *
 * Input Params	: Tag Number, Buffer to fill the tag value
 *
 * Output Params: NONE
 * ============================================================================
 */
static void calculateSubTotal(char *pszSubTotal, PYMTTRAN_PTYPE pstPymtTran)
{
	double			fTotAmt			= 0.0;
	char *			curAmtPtr		= NULL;
	double			fCurAmt			= 0.0;
	int				iLength         = 0;
	AMTDTLS_PTYPE	pstAmtDtls		= NULL;
	LVL2_PTYPE		pstTaxDtls		= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstAmtDtls		= pstPymtTran->pstAmtDtls;
	if (pstAmtDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Amount details not available!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}

	pstTaxDtls		= pstPymtTran->pstLevel2Dtls;

	/* Adding the Tip amount */
	curAmtPtr = pstAmtDtls->tipAmt;
	iLength = strlen(curAmtPtr);
	if(iLength > 0)
	{
		fCurAmt = atof(curAmtPtr);
		fTotAmt += fCurAmt;
	}

	/* Adding the cashback amount */
	curAmtPtr = pstAmtDtls->cashBackAmt;
	iLength = strlen(curAmtPtr);
	if(iLength > 0)
	{
		fCurAmt = atof(curAmtPtr);
		fTotAmt += fCurAmt;
	}

	/* Adding the Tax amount */
	if(pstTaxDtls != NULL)
	{
		curAmtPtr = pstTaxDtls->taxAmt;
		iLength = strlen(curAmtPtr);
		if(iLength > 0)
		{
			fCurAmt = atof(curAmtPtr);
			fTotAmt += fCurAmt;
		}
	}

	/* Subtracting from the total amount */
	curAmtPtr = pstAmtDtls->tranAmt;
	iLength = strlen(curAmtPtr);
	if(iLength > 0)
	{
		fCurAmt = atof(curAmtPtr);

		/*Written or not printing the subtotal if the subtotal and total amount is same
		 * Commented it again to print sub total for every transaction
		 * */
		//	if (fTotAmt > 0.00)
		//	{
		fTotAmt = fCurAmt - fTotAmt;
		//	}
	}

	if(fTotAmt > 0.00) //Adding this value if is greater than 0
	{
		sprintf(pszSubTotal, "USD$ %.2lf", fTotAmt);
	}

	debug_sprintf(szDbgMsg, "%s: Subtotal Amount %s", __FUNCTION__, pszSubTotal);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Return", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}
