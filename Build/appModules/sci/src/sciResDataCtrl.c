/*
 * =============================================================================
 * Filename    : sciResDataCtrl.c
 *
 * Application : Mx Point SCA
 *
 * Description : 
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */

#include "stdio.h"
#include "string.h"
#include "stdlib.h"

#include "common/metaData.h"
#include "common/utils.h"
#include "common/tranDef.h"
#include "db/dataSystem.h"
#include "sciDef.h"
#include "bLogic/bLogicCfgDef.h"
#include "bLogic/blAPIs.h"
#include "sci/sciConfigDef.h"
#include "sci/sciRespLists.inc"
#include "sci/sciReceiptData.h"
#include "ssi/ssiDef.h"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

typedef METADATA_STYPE	META_STYPE;
typedef METADATA_PTYPE	META_PTYPE;


static char * szSCICmd[SSI_MAX_CMD] =
{
	"SETUP_V3",
	"PRE_AUTH",
	"VOICE_AUTH",
	"POST_AUTH",
	"SALE",
	"CREDIT",
	"VOID",
	"COMPLETION",
	"COMMERCIAL",
	"ACTIVATE",
	"ADD_VALUE",
	"BALANCE",
	"REGISTER",
	"GIFT_CLOSE",
	"DEACTIVATE",
	"REACTIVATE",
	"REMOVE_VALUE",
	"SIGNATURE",
	"SETTLE",
	"DAYSUMMARY",
	"PRESETTLEMENT",
	"SETTLEERROR",
	"TRANSEARCH",
	"SETTLESUMMARY",
	"LAST_TRAN",
	"DUPCHECK",
	"EMVADMIN",
	"TOKEN_QUERY",    	/* Token Query */
	"VERIFY",    		/*CHECK Verify */
	"REVERSAL",  		/*CHECK REFUND */
	"ADD_TIP",
	"RESET_TIP",
	"MESSAGE_ACK",      /* Message ACK to SSI */
	"VERSION",			/*Version For DHI*/
	"CUTOVER",
	"SITETOTALS",
	"CREDIT_APP",
	"TIMEOUT_REVERSAL",
	"CHECKIN",
	"CHECKOUT",
	"PAYACCOUNT",			/*KranthiK1: Changed it to ACCT_PAY from PAY_ACCOUNT Need to revert it back when UGP is enabled*/
	"REFUND",
	"QUERY",
	"REGISTER"
};

/* --------- Static functions declarations -------- */
/* APIs to populate the meta data */
static int getMetaForSecResp(TRAN_PTYPE, META_PTYPE);
static int getMetaForSessResp(TRAN_PTYPE, META_PTYPE);
static int getMetaForLIResp(TRAN_PTYPE, META_PTYPE);
static int getMetaForPymtResp(TRAN_PTYPE, META_PTYPE);
static int getMetaForSAFResp(TRAN_PTYPE, META_PTYPE);
static int getMetaForDevResp(TRAN_PTYPE, META_PTYPE);
static int getMetaForBatchResp(TRAN_PTYPE, META_PTYPE);
static int getMetaForAdminResp(TRAN_PTYPE , META_PTYPE );
static int getMetaForScndResp(TRAN_PTYPE , META_PTYPE );
//static int getMetaForRptResp(TRAN_PTYPE, META_PTYPE);

static int buildMetaForGenResp(TRAN_PTYPE, META_PTYPE);
static int buildMetaForSecReg(POSSEC_PTYPE, RESPDTLS_PTYPE, META_PTYPE, int);
static int buildMetaForPymt(char *, RESPDTLS_PTYPE, META_PTYPE, int );
static int buildMetaForSAF(SAFDTLS_PTYPE, RESPDTLS_PTYPE, META_PTYPE);
static int buildMetaForSign(DEVTRAN_PTYPE, RESPDTLS_PTYPE, META_PTYPE);
static int buildMetaForEmail(DEVTRAN_PTYPE, RESPDTLS_PTYPE, META_PTYPE);
static int buildMetaForLty(DEVTRAN_PTYPE, RESPDTLS_PTYPE, META_PTYPE);
static int buildMetaForProvisionPass(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForLPToken(char * , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForGenEResp(META_PTYPE , char *, char *);
static int buildMetaForCustQues(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForSurvey5(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForSurvey10(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForTime(DEVTRAN_PTYPE, RESPDTLS_PTYPE, META_PTYPE);
static int buildMetaForVersion(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForCharity(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForCustomerButton(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForGetCounter(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForLaneClosed(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForDevName(DEVTRAN_PTYPE, RESPDTLS_PTYPE, META_PTYPE);
static int buildMetaForEmpID(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForGetPaymentTypes(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForSetParm(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForScndryPortCmd(RESPDTLS_PTYPE, META_PTYPE);
static int buildMetaForStatus(RESPDTLS_PTYPE, META_PTYPE);
static int buildMetaForCreditApp(DEVTRAN_PTYPE, RESPDTLS_PTYPE, META_PTYPE);
static int buildMetaForDispMsg(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForGetParm(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForApplyUpdates(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForQueryNfcIni(DEVTRAN_PTYPE , RESPDTLS_PTYPE ,META_PTYPE);
static int buildMetaForDispQRCOde(RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForCancelQRCOde(RESPDTLS_PTYPE , META_PTYPE );
static int buildMetaForCustCheckbox(DEVTRAN_PTYPE, RESPDTLS_PTYPE, META_PTYPE );

/* APIs to populate the key value lists */
static int fillGenRespDtls(KEYVAL_PTYPE, RESPDTLS_PTYPE);
static int fillOtherGenRespDtls(KEYVAL_PTYPE, OTHER_RESPDTLS_PTYPE);
static int fillCustInfoDtls(KEYVAL_PTYPE, CUSTINFODTLS_PTYPE);
static int fillSAFDtls(KEYVAL_PTYPE, SAFDTLS_PTYPE);
static int fillSAFRecs(SAFREC_PTYPE, VAL_LST_PTYPE);
static int fillSAFRecDtls(SAFREC_PTYPE, KEYVAL_PTYPE);
static int fillRegRespDtls(KEYVAL_PTYPE, POSSEC_PTYPE, int);
static int fillSignDtls(KEYVAL_PTYPE, SIGDTLS_PTYPE);
static int fillEmailDtls(KEYVAL_PTYPE, char *);
static int fillLtyDtls(KEYVAL_PTYPE, LTYDTLS_PTYPE);
static int fillStandAloneLtyDtls(KEYVAL_PTYPE , LTYDTLS_PTYPE );
static int fillCardDtls(KEYVAL_PTYPE, CARDDTLS_PTYPE);
static int fillEMVDtls(KEYVAL_PTYPE, CARDDTLS_PTYPE);
static int fillCTranDtls(KEYVAL_PTYPE, CTRANDTLS_PTYPE);
static int fillAmtDtls(KEYVAL_PTYPE, AMTDTLS_PTYPE, char *);
static int fillDupTranDtls(KEYVAL_PTYPE, PAAS_BOOL);
static int fillReceiptData(KEYVAL_PTYPE, TRAN_PTYPE, int);
static int fillCustQuesDtls(KEYVAL_PTYPE , SURVEYDTLS_PTYPE );
static int fillSurvey5Dtls(KEYVAL_PTYPE , SURVEYDTLS_PTYPE );
static int fillSurvey10Dtls(KEYVAL_PTYPE , SURVEYDTLS_PTYPE );
static int fillTimeDtls(KEYVAL_PTYPE, TIMEDTLS_PTYPE);
static int fillConsOptDtls(KEYVAL_PTYPE , UNSOLMSGINFO_PTYPE , int , int );
static int fillVersionDtls(KEYVAL_PTYPE , DEVTRAN_PTYPE );
static int fillCharityDtls(KEYVAL_PTYPE , CHARITYDTLS_PTYPE );
static int fillCustomerButtonDtls(KEYVAL_PTYPE , CUSTBUTTONDTLS_PTYPE );
//static int fillCounterDtls(KEYVAL_PTYPE , COUNTERDTLS_PTYPE );
static int fillDevNameDtls(KEYVAL_PTYPE , DEVICENAME_PTYPE);
static int fillPaymentTypesDtls(KEYVAL_PTYPE , PAYMENTTYPES_PTYPE );
static int fillEmpIDDtls(KEYVAL_PTYPE ,   EMPID_PTYPE );
static int fillDupDtls(KEYVAL_PTYPE , DUPFIELDDTLS_PTYPE );
static int fillScndData(KEYVAL_PTYPE);
static int fillStatusDetails(KEYVAL_PTYPE);
static int fillCreditAppDtls(KEYVAL_PTYPE, CREDITAPPDTLS_PTYPE);
static int fillFSADtls(KEYVAL_PTYPE, AMTDTLS_PTYPE);
static int fillEBTDtls(KEYVAL_PTYPE , EBTDTLS_PTYPE );
static int fillPOSTndrDtls(KEYVAL_PTYPE , POSTENDERDTLS_PTYPE);
static int fillDispMsgDtls(KEYVAL_PTYPE , DISPMSG_PTYPE);
static int fillGetParmDtls(KEYVAL_PTYPE , GETPARM_PTYPE );
static int fillVASLoyaltyDtls(VAL_LST_PTYPE , VASDATA_DTLS_PTYPE );
static int fillVASData(KEYVAL_PTYPE , VASDATA_DTLS_PTYPE );
extern int getCardEntryMode(CARDDTLS_PTYPE, char *);
static int fillRawCardDtls(KEYVAL_PTYPE, RAW_CARDDTLS_PTYPE);
static int fillGetCardDtls(KEYVAL_PTYPE , GET_CARDDATA_PTYPE);
static int fillPassThrgResDtls(KEYVAL_PTYPE, PASSTHRG_FIELDS_PTYPE);
static int fillDCCResDtls(KEYVAL_PTYPE, DCCDTLS_PTYPE);
static int fillQueryNfcIniRespDtls(KEYVAL_PTYPE, NFCINI_CONTENT_INFO_PTYPE);
static int fillNfcSectionDtls(VAL_LST_PTYPE , NFC_APPLE_SECT_DTLS_PTYPE);
static int fillQueryNfcApplePayRespDtls(VAL_LST_PTYPE, NFCINI_CONTENT_INFO_PTYPE);
static int fillCustCheckBoxRespDtls(KEYVAL_PTYPE, CUSTCHECKBOX_DTLS_PTYPE);

static int addXMLRcptNodes(VAL_LST_PTYPE, TRAN_PTYPE, int);
static int populateRcptDataInRcptNode(VAL_NODE_PTYPE, TRAN_PTYPE, int);

extern int getStatusDtlsFromSession(STATUSDTLS_PTYPE);
extern PAAS_BOOL isEmvEnabledInDevice();
extern int getHostProtocolFormat();
extern PAAS_BOOL isServiceCodeCheckInFallbackEnabled();
extern PAAS_BOOL isPartialEmvAllowed();
extern PAAS_BOOL isDescriptiveEntryModeEnabled();
extern PAAS_BOOL isWalletEnabled();
extern PAAS_BOOL checkBinInclusion(char *);
extern int buildMetaForGetCardData(DEVTRAN_PTYPE , RESPDTLS_PTYPE , META_PTYPE );
extern int getDataFromVCL(char*, char *, char*, char*, PAAS_BOOL);
/*
 * ============================================================================
 * Function Name: getMetaDataForSCIResp 
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getMetaDataForSCIResp(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int			rv			 			= SUCCESS;
	PAAS_BOOL	bEmvEnabled				= PAAS_FALSE;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//Get EMV status.
	bEmvEnabled = isEmvEnabledInDevice();	
	
	while(1)
	{
		if( (pstMeta == NULL) || (pstTran == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Tran Counter [%s]", __FUNCTION__, pstTran->szTranCntr);
		APP_TRACE(szDbgMsg);

		//Copy transaction counter value to response dtls structure to send it to POS.
		strcpy(pstTran->stRespDtls.szTranCntr, pstTran->szTranCntr);

		/* In case some error happened while processing the transaction, only
		 * the 4 main response fields need to be sent so metadata should be
		 * formed for only those four fields */
		/*ArjunU1: When EMV is enabled, SCA has to return receipt data and other data present
				   for CANCELLED transaction to POS*/
		/*Daivik:16/9/2016 - For Transactions that were cancelled because PIN was not entered for EMV Cashback Transactions,
		 * it will help to have the EMV Tags in the response which will give additional information due to which transaction 
		 * was cancelled. Hence adding the rv of ERR_PIN_REQD_EMV_CASHBACK here.
		 */
		if( bEmvEnabled == PAAS_TRUE && (pstTran->iStatus == ERR_USR_CANCELED || pstTran->iStatus == ERR_PIN_REQD_EMV_CASHBACK) && pstTran->iFxn == SCI_PYMT)
		{
			debug_sprintf(szDbgMsg, "%s: Need to send  response dtls to POS for:%d",__FUNCTION__,pstTran->iStatus);
			APP_TRACE(szDbgMsg);
		}	
		else if (pstTran->iStatus != SUCCESS)
		{
			/* The response details would contain the error details */
			rv = buildMetaForGenResp(pstTran, pstMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to build error meta data",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
			break;
		}
		else;
		
		memset(pstMeta, 0x00, METADATA_SIZE);

		switch(pstTran->iFxn)
		{
		case SCI_SEC:
			rv = getMetaForSecResp(pstTran, pstMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta for SECURITY",
										__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;

		case SCI_SESS:
			rv = getMetaForSessResp(pstTran, pstMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta for SESSION",
									__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;

		case SCI_LI:
			rv = getMetaForLIResp(pstTran, pstMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta for LINE ITEM",
									__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;

		case SCI_PYMT:
			rv = getMetaForPymtResp(pstTran, pstMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta for PAYMENT",
									__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

		break;

		case SCI_SAF:
			rv = getMetaForSAFResp(pstTran, pstMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta for SAF",
								__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;

		case SCI_DEV:
			rv = getMetaForDevResp(pstTran, pstMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta for DEVICE",
									__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;

		case SCI_BATCH:
			rv = getMetaForBatchResp(pstTran, pstMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta for BATCH",
									__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;

		case SCI_RPT:
			/*
			 * Controls comes here only if it is REPORT function type and
			 * LPTOKEN Query command
			 * For this token we have created the PAYMENT instance, thats
			 * why calling the payment resp
			 */
			rv = getMetaForPymtResp(pstTran, pstMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta for PAYMENT",
									__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;

		case SCI_ADM:
			rv = getMetaForAdminResp(pstTran, pstMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta for BATCH",
									__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;

		case SCI_SCND:
			rv = getMetaForScndResp(pstTran, pstMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta for BATCH",
									__FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;

		default:
			/* If the control is coming here the issue is with the coding.
			 * Please do not change the return value for this case, fix the
			 * code instead.. */
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaDataForSCIConsOptResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getMetaDataForSCIConsOptResp(META_PTYPE pstMeta, UNSOLMSGINFO_PTYPE pstUnsolMsgInfo)
{
	int					rv			 	= SUCCESS;
	int					iCnt			= 0;
	int					iTotCnt			= 0;
	KEYVAL_PTYPE		pstList			= NULL;
	KEYVAL_PTYPE		curPtr			= NULL;
	RESPDTLS_STYPE  	stRespDtls;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstMeta == NULL) || (pstUnsolMsgInfo == NULL) )
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the size of the memory to be allocated for the key value pairs */
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;

		iTotCnt += sizeof(stConsOptDtlsLst) / KEYVAL_SIZE;

		iTotCnt += sizeof(stEmailLst) / KEYVAL_SIZE;

		if(pstUnsolMsgInfo->pstRawCrdDtls != NULL)
		{
			iTotCnt += sizeof(stRawCrdDtlsLst) / KEYVAL_SIZE;
		}

		/* Allocate the memory for the key value pairs */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize the variables */
		memset(pstList, 0x00, iTotCnt * KEYVAL_SIZE);

		curPtr = pstList;

		memset(&stRespDtls, 0x00, sizeof(RESPDTLS_STYPE));

		strcpy(stRespDtls.szRslt, "OK");
		strcpy(stRespDtls.szRsltCode, "-1");
		strcpy(stRespDtls.szTermStat, "SUCCESS");
		//strcpy(stRespDtls.szRespTxt, "ASYNCSELECT RESP");
		if(pstUnsolMsgInfo->iCancelPressed)
		{
			strcpy(stRespDtls.szRespTxt, "CONSUMER PRESSED CANCEL");
		}
		else if(pstUnsolMsgInfo->iPreSwiped && pstUnsolMsgInfo->iVasLoyalty)
		{
			strcpy(stRespDtls.szRespTxt, "CONSUMER PRE-SWIPED CARD AND PRE TAPPED VAS DATA");
		}
		else if(pstUnsolMsgInfo->iVasLoyalty)
		{
			strcpy(stRespDtls.szRespTxt, "CONSUMER PRE TAPPED VAS DATA");
		}
		else if(pstUnsolMsgInfo->iPreSwiped)
		{
			strcpy(stRespDtls.szRespTxt, "CONSUMER PRE-SWIPED CARD");
		}
		else if(pstUnsolMsgInfo->iPayWithPaypal)
		{
			strcpy(stRespDtls.szRespTxt, "PAY WITH PAYPAL SELECTED");
		}
		else if(pstUnsolMsgInfo->iCardDtlCaptured)
		{
			strcpy(stRespDtls.szRespTxt, "CONSUMER CARD ENTRY");
			if(pstUnsolMsgInfo->pstRawCrdDtls != NULL && (strlen(pstUnsolMsgInfo->pstRawCrdDtls->szRsltCode) > 0))
			{
				strcpy(stRespDtls.szRsltCode, pstUnsolMsgInfo->pstRawCrdDtls->szRsltCode);
			}
		}
		else if(pstUnsolMsgInfo->iTransInFlight)
		{
			strcpy(stRespDtls.szRespTxt, "TRANSACTION IN FLIGHT");
			if(pstUnsolMsgInfo->pstRawCrdDtls != NULL && (strlen(pstUnsolMsgInfo->pstRawCrdDtls->szRsltCode) > 0))
			{
				strcpy(stRespDtls.szRsltCode, pstUnsolMsgInfo->pstRawCrdDtls->szRsltCode);
			}
		}
		else
		{
			strcpy(stRespDtls.szRespTxt, "CONSUMER OPTION SELECTION");
		}

		/*Build Meta data for General Response details */

		iCnt = fillGenRespDtls(curPtr, &stRespDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillConsOptDtls(curPtr, pstUnsolMsgInfo, 1/*Include Card Token */, 1 /*Include Token Source*/);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillEmailDtls(curPtr, pstUnsolMsgInfo->szEmail);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstUnsolMsgInfo->pstRawCrdDtls != NULL)
		{
			curPtr += iCnt;

			iCnt = fillRawCardDtls(curPtr, pstUnsolMsgInfo->pstRawCrdDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		memset(pstMeta, 0x00, METADATA_SIZE);
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: getMetaDataForSCIVasDataResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getMetaDataForSCIVasDataResp(META_PTYPE pstMeta, VASDATA_DTLS_PTYPE pstVasDataDtls, RESPDTLS_PTYPE pstResp)
{
	int				rv			 	= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iTotVASCnt		= 0;
	KEYVAL_PTYPE	pstList			= NULL;
	KEYVAL_PTYPE	curPtr			= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstMeta == NULL) || (pstVasDataDtls == NULL) || (pstResp == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the size of the memory to be allocated for the key value pairs */
		iTotVASCnt = sizeof(stLoyaltyWalletOffers) / KEYVAL_SIZE;
		iTotCnt += iTotVASCnt;

		/* Get the size of the memory to be allocated for the key value pairs */
		iTotCnt  += sizeof(stMainRespLst) / KEYVAL_SIZE;

		/* Allocate the memory for the key value pairs */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;	//25-Jan-16: MukeshS3
		}

		/* Initialize the variables */
		memset(pstList, 0x00, iTotCnt * KEYVAL_SIZE);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillVASData(curPtr, pstVasDataDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill VAS data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		memset(pstMeta, 0x00, METADATA_SIZE);
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	// CID-67437: 22-Jan-16: MukeshS3: We must free any allocated memory above, if it is not going to be used anywhere for FAILURE case.
	// This function will free memory allocated for each value in the list from 0 till current location & than KEYVAL_PTYPE pointer itself.
	if(rv != SUCCESS)
	{
		freeKeyValDataNode(pstList, iTotCnt);	// iCnt value may be any FAILURE here, so passing iTotCnt, anyway once it reaches to NULL pointer it will come out
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: getMetaDataForSCIEResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getMetaDataForSCIEResp(META_PTYPE pstMeta, char *pszPayloadData, char *pszMacLabel)
{
	int		rv			 	= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if( (pstMeta == NULL) || (pszPayloadData == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		rv = buildMetaForGenEResp(pstMeta, pszPayloadData, pszMacLabel);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build resp meta for ERESPONSE" , __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaForSecResp
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getMetaForSecResp(TRAN_PTYPE pstTran, META_PTYPE pstMeta)
{
	int		rv			 	= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(pstTran->iCmd)
	{
	case SCI_REG:
	case SCI_EREG:
		/* Build the meta data for the REGISTER response */
		rv = buildMetaForSecReg(pstTran->data, &(pstTran->stRespDtls), pstMeta, pstTran->iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build resp meta for REGISTER"
																, __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	case SCI_UNREG:
	case SCI_UNREGALL:
	case SCI_TESTMAC:
		rv = buildMetaForGenResp(pstTran, pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to build gen meta",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	default:
		/* If the control is coming here the issue is with the coding.
		 * Please do not change the return value for this case, fix the
		 * code instead.. */
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaForSessResp 
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getMetaForSessResp(TRAN_PTYPE pstTran, META_PTYPE pstMeta)
{
	int		rv			 	= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(pstTran->iCmd)
	{
	case SCI_START:
	case SCI_FINISH:
		rv = buildMetaForGenResp(pstTran, pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to build gen meta",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	default:
		/* If the control is coming here the issue is with the coding.
		 * Please do not change the return value for this case, fix the
		 * code instead.. */
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaForLIResp
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getMetaForLIResp(TRAN_PTYPE pstTran, META_PTYPE pstMeta)
{
	int		rv			 	= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(pstTran->iCmd)
	{
	case SCI_ADD:
	case SCI_OVERRIDE:
	case SCI_REMOVE:
	case SCI_REMOVEALL:
	case SCI_SHOW:
		rv = buildMetaForGenResp(pstTran, pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to build gen meta",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	default:
		/* If the control is coming here the issue is with the coding.
		 * Please do not change the return value for this case, fix the
		 * code instead.. */
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaForPymtResp
 *
 * Description	: TODO: Add logic for the receipts inside 
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getMetaForPymtResp(TRAN_PTYPE pstTran, META_PTYPE pstMeta)
{
	int		rv			 	= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(pstTran->iCmd)
	{
	case SCI_AUTH:
	case SCI_CAPTURE:
	case SCI_CREDIT:
	case SCI_VOID:
	case SCI_COMPLETION:
	case SCI_ACTIVATE:
	case SCI_ADDVAL:
	case SCI_BAL:
	case SCI_GIFTCLOSE:
	case SCI_DEACTIVATE:
	case SCI_REACTIVATE:
	case SCI_REMVAL:
	case SCI_LPTOKEN:
	case SCI_TOKEN:
	case SCI_ADDTIP:
	case SCI_RESETTIP:
	case SCI_PAYACCOUNT:
	case SCI_TOKEN_UPDATE:
		rv = buildMetaForPymt(pstTran->data, &(pstTran->stRespDtls), pstMeta, pstTran->iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for PYMT resp",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	default:
		/* If the control is coming here the issue is with the coding.
		 * Please do not change the return value for this case, fix the
		 * code instead.. */
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaForSAFResp
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getMetaForSAFResp(TRAN_PTYPE pstTran, META_PTYPE pstMeta)
{
	int		rv			 	= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(pstTran->iCmd)
	{
	case SCI_QUERY:
	case SCI_REMOVE:
		rv = buildMetaForSAF(pstTran->data, &(pstTran->stRespDtls),pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for SAF resp",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	default:
		/* If the control is coming here the issue is with the coding.
		 * Please do not change the return value for this case, fix the
		 * code instead.. */
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaForDevResp
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getMetaForDevResp(TRAN_PTYPE pstTran, META_PTYPE pstMeta)
{
	int		rv			 	= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(pstTran->iCmd)
	{
	case SCI_SIGN:
	case SCI_SIGN_EX:
		rv = buildMetaForSign(pstTran->data,&(pstTran->stRespDtls),pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for sign resp",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	case SCI_EMAIL:
		rv = buildMetaForEmail(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for EMAIL resp",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	case SCI_LTY:
		rv = buildMetaForLty(pstTran->data, &(pstTran->stRespDtls),pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for LTY resp",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	case SCI_PROVISION_PASS:
		rv = buildMetaForProvisionPass(pstTran->data, &(pstTran->stRespDtls),pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Provision Pass Resp",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;
	case SCI_CUST_QUES:
		rv = buildMetaForCustQues(pstTran->data,&(pstTran->stRespDtls),pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Customer Question resp",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_SURVEY5:
		rv = buildMetaForSurvey5(pstTran->data,&(pstTran->stRespDtls),pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for survey5 resp",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	case SCI_SURVEY10:
		rv = buildMetaForSurvey10(pstTran->data,&(pstTran->stRespDtls),pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Survey10 resp",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_LPTOKEN:
		rv = buildMetaForLPToken(pstTran->data, &(pstTran->stRespDtls),pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for LP Token resp",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_VERSION:
		rv = buildMetaForVersion(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for VERSION resp",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	case SCI_CHARITY:
		rv = buildMetaForCharity(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for VERSION resp",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_GET_DEVICENAME:
		rv = buildMetaForDevName(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Device Name",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_SET_DEVICENAME:
		rv = buildMetaForGenResp(pstTran, pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to build gen meta for set device name cmd",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	case SCI_EMP_ID_CAPT:
		rv = buildMetaForEmpID(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Employee ID",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_CUST_BUTTON:
		rv = buildMetaForCustomerButton(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for customer button resp",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_CREDIT_APP:
		rv = buildMetaForCreditApp(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Credit Application Response",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_GET_PAYMENT_TYPES:
		rv = buildMetaForGetPaymentTypes(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Get Payment Types Response",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_SET_PARM:
		rv = buildMetaForSetParm(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Set Parm Response",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_DISPLAY_MESSAGE:
		rv = buildMetaForDispMsg(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Display Message Response",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_GET_PARM:
		rv = buildMetaForGetParm(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Set Parm Response",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_GET_CARD_DATA:
		rv = buildMetaForGetCardData(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Set Parm Response",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_QUERY_NFCINI:
		rv = buildMetaForQueryNfcIni(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Query NFC INI Response",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_DISP_QRCODE:
		rv = buildMetaForDispQRCOde(&(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Disp QR Code Response",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_CANCEL_QRCODE:
		rv = buildMetaForCancelQRCOde(&(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Disp QR Code Response",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_CUST_CHECKBOX:
		rv = buildMetaForCustCheckbox(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Disp QR Code Response",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
		
	default:
		/* If the control is coming here the issue is with the coding.
		 * Please do not change the return value for this case, fix the
		 * code instead.. */
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaForBatchResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getMetaForBatchResp(TRAN_PTYPE pstTran, META_PTYPE pstMeta)
{
	int		rv			 	= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(pstTran->iCmd)
	{
	case SCI_STL:

		/* The response details would contain the error details */
		rv = buildMetaForGenResp(pstTran, pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to build gen meta",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	default:
		/* If the control is coming here the issue is with the coding.
		 * Please do not change the return value for this case, fix the
		 * code instead.. */
		debug_sprintf(szDbgMsg, "%s: Invalid command", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaForAdminResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getMetaForScndResp(TRAN_PTYPE pstTran, META_PTYPE pstMeta)
{
	int		rv			 	= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(pstTran->iCmd)
	{
	case SCI_CANCEL:
	case SCI_REBOOT:
		/* The response details would contain the error details */
		rv = buildMetaForScndryPortCmd(&(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to build gen meta",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	case SCI_STATUS:
		rv = buildMetaForStatus(&(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to build gen meta",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_ANY_UPDATES:
		rv = buildMetaForScndryPortCmd(&(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to build gen meta",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	case SCI_UPDATE_STATUS:
		rv = buildMetaForStatus(&(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to build gen meta",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	default:
		/* If the control is coming here the issue is with the coding.
		 * Please do not change the return value for this case, fix the
		 * code instead.. */
		debug_sprintf(szDbgMsg, "%s: Invalid command", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaForAdminResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getMetaForAdminResp(TRAN_PTYPE pstTran, META_PTYPE pstMeta)
{
	int		rv			 	= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(pstTran->iCmd)
	{
	case SCI_SETTIME:
		rv = buildMetaForTime(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for EMAIL resp",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	case SCI_GETCOUNTER:
		rv = buildMetaForGetCounter(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for GetCounter resp",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_LANE_CLOSED:
		rv = buildMetaForLaneClosed(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Lane Closed resp",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_APPLYUPDATES:
		rv = buildMetaForApplyUpdates(pstTran->data, &(pstTran->stRespDtls), pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build meta for Apply Updates resp",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	default:
		/* If the control is coming here the issue is with the coding.
		 * Please do not change the return value for this case, fix the
		 * code instead.. */
		debug_sprintf(szDbgMsg, "%s: Invalid command", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#if 0
/*
 * ============================================================================
 * Function Name: getMetaForRptResp
 *
 * Description	: TODO: Complete this logic
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getMetaForRptResp(TRAN_PTYPE pstTran, META_PTYPE pstMeta)
{
	int		rv			 	= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch(pstTran->iCmd)
	{
	case SCI_DAYSUMM:
	case SCI_STLSUMM:
	case SCI_LASTTRAN:
	case SCI_DUPCHK:

		/* Nothing needs to be done here */
		/* FIXME: Make the code do something here, something wonderfull,
		 * something magical :D */
		/* The response details would contain the error details */
		rv = buildMetaForGenResp(pstTran, pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to build gen meta",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		break;

	default:
		/* If the control is coming here the issue is with the coding.
		 * Please do not change the return value for this case, fix the
		 * code instead.. */
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
#endif

/*
 * ============================================================================
 * Function Name: buildMetaForGenResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForGenResp(TRAN_PTYPE pstTran, META_PTYPE pstMeta)
{
	int				rv			 	= SUCCESS;
	int				iTotCnt			= 0;
	int				iCnt			= 0;
	KEYVAL_PTYPE	pstList			= NULL;
	KEYVAL_PTYPE	curPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt		 = sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt		+= sizeof(stOtherRespLst) / KEYVAL_SIZE;

		pstList = (KEYVAL_PTYPE) malloc(KEYVAL_SIZE * iTotCnt);
		if( pstList == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		iCnt = fillGenRespDtls(curPtr,  &(pstTran->stRespDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill General Resp dtls",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillOtherGenRespDtls(curPtr, &(pstTran->stOtherRespDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill General Resp dtls",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		memset(pstMeta, 0x00, METADATA_SIZE);
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}
	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* FIXME: Not a correct way of de-allocation, possible memory leakage
		 * as the values of filled keys would be lost. Find a way to solve this
		 * issue. */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForGenEResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForGenEResp(META_PTYPE pstMeta, char *pszData, char *pszMacLabel)
{
	int				rv			 	= SUCCESS;
	int				iTotCnt			= 0;
	int				iCnt			= 0;
	int				iLen			= 0;
	KEYVAL_PTYPE	pstList			= NULL;
	char			*tmpPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(genETranRespLst) / KEYVAL_SIZE;

	pstList = (KEYVAL_PTYPE) malloc(KEYVAL_SIZE * iTotCnt);
	if(pstList != NULL)
	{
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);

		iTotCnt = sizeof(genETranRespLst) / KEYVAL_SIZE;

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			memcpy(&pstList[iCnt], &genETranRespLst[iCnt], KEYVAL_SIZE);

			if(strcmp(pstList[iCnt].key, "PAYLOAD") == SUCCESS)
			{
				iLen = strlen(pszData);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, pszData);
					}
				}
			}
			else if(strcmp(pstList[iCnt].key, "MAC_LABEL") == SUCCESS)
			{
				iLen = strlen(pszMacLabel);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, pszMacLabel);
					}
				}
			}

			pstList[iCnt].value = tmpPtr;
			tmpPtr = NULL;
		}

		memset(pstMeta, 0x00, METADATA_SIZE);
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForSecReg
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForSecReg(POSSEC_PTYPE pstPosSec, RESPDTLS_PTYPE pstResp,
															META_PTYPE pstMeta, int iCmd)
{
	int				rv			 	= SUCCESS;
	int				iTotCnt			= 0;
	int				iCnt			= 0;
	KEYVAL_PTYPE	pstList			= NULL;
	KEYVAL_PTYPE	curPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Calculate the amount of memory to be allocated */
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(posRegRespLst) / KEYVAL_SIZE;

		/* Allocate the memory */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize memory */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* ---- Populate the keyvalue pairs --- */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillRegRespDtls(curPtr, pstPosSec, iCmd);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill REGISTER resp dtls",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* Set the metadata */
		memset(pstMeta, 0x00, METADATA_SIZE);
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* FIXME: Not a correct way of de-allocation, possible memory leakage
		 * as the values of filled keys would be lost. Find a way to solve this
		 * issue. */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForPymt
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForPymt(char * szTranKey, RESPDTLS_PTYPE pstResp,
														META_PTYPE pstMeta, int iCmd)
{
	int						rv			 		= SUCCESS;
	int						iResltCode			= -1;
	int						iCnt				= 0;
	int						iTotCnt				= 0;
	int						iIncludeCT			= 0;
	int						iIncludeTS			= 0;
	PAAS_BOOL				bVasDataResp		= PAAS_FALSE;
	TRAN_PTYPE				pstTran				= NULL;
	PYMTTRAN_PTYPE			pstPymtTran			= NULL;
	KEYVAL_PTYPE			pstList				= NULL;
	KEYVAL_PTYPE			curPtr				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		rv = getTopSSITran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get tran data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the payment details from the transaction in the stack */
		pstPymtTran = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtTran == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Payment details",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		//For VAS Data Response and VAS Data to POS
		if( isWalletEnabled() 										&&
			pstPymtTran->pstVasDataDtls != NULL 					&&
			(pstPymtTran->pstVasDataDtls->bProvisionPassSent == PAAS_TRUE || (pstPymtTran->pstVasDataDtls->bVASPresent == PAAS_TRUE && pstPymtTran->pstVasDataDtls->bVASsentToPOS == PAAS_FALSE))
			)
		{
			bVasDataResp = PAAS_TRUE;
		}

		/* Get the size of the memory to be allocated for the key value pairs */
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;

		if(pstPymtTran->pstCustInfoDtls != NULL)
		{
			iTotCnt += sizeof(stCustInfoLst) / KEYVAL_SIZE;
		}

		if(pstPymtTran->pstCTranDtls != NULL)
		{
			iTotCnt += sizeof(cTranDtlsLst) / KEYVAL_SIZE;
		}

		if(pstPymtTran->pstAmtDtls != NULL)
		{
			iTotCnt += sizeof(amtDtlsLst) / KEYVAL_SIZE;
		}

		if(pstPymtTran->pstCardDtls != NULL)
		{
			iTotCnt += sizeof(crdDtlsLst) / KEYVAL_SIZE;
		}

		if(pstPymtTran->pstSigDtls != NULL)
		{
			iTotCnt += sizeof(stSignLst) / KEYVAL_SIZE;
		}

		if(pstPymtTran->pstLtyDtls != NULL)
		{
			iTotCnt += sizeof(stLtyLst) / KEYVAL_SIZE;
		}

		if(pstPymtTran->pstDupDtls != NULL)
		{
			iTotCnt += sizeof(dupDtlsLst) / KEYVAL_SIZE;
		}

		if((pstPymtTran->pstFSADtls != NULL) && strlen(pstPymtTran->pstFSADtls->szAmtHealthCare) && (pstPymtTran->pstAmtDtls != NULL))
		{
			iTotCnt += sizeof(FSADtlsLst) / KEYVAL_SIZE;
		}

		if((pstPymtTran->pstEBTDtls != NULL) && strlen(pstPymtTran->pstEBTDtls->szEBTType) && (pstPymtTran->iPymtType == PYMT_EBT))
		{
			iTotCnt += sizeof(EBTDtlsLst) / KEYVAL_SIZE;
		}

		if(pstPymtTran->pstCardDtls != NULL && pstPymtTran->pstCardDtls->bEmvData == PAAS_TRUE)
		{
			iTotCnt += sizeof(EMVTagsForSCI) / KEYVAL_SIZE;
		}

		/*
		 * We need to send consumer option selected details
		 * only for the CAPTURE command response thats why
		 * following check
		 */
		if(iCmd == SCI_CAPTURE || iCmd == SCI_CREDIT || iCmd == SCI_ACTIVATE ||
		   iCmd == SCI_ADDVAL || iCmd == SCI_GIFTCLOSE || iCmd == SCI_BAL ||
		   iCmd == SCI_GIFTCLOSE || iCmd == SCI_DEACTIVATE)
		{
			iTotCnt += sizeof(stConsOptDtlsLst) / KEYVAL_SIZE;

			iTotCnt += sizeof(stEmailLst) / KEYVAL_SIZE;
		}

		//We need to send duplicate transaction indicator to POS in case of dup tran detected
		if(pstPymtTran->bDupTranDetected == PAAS_TRUE)
		{
			iTotCnt += sizeof(stDupTran) / KEYVAL_SIZE;
		}

		//No receipt for VAS Loyalty
		if(bVasDataResp)
		{
			iTotCnt += sizeof(stLoyaltyWalletOffers)/KEYVAL_SIZE;
		}
		else if(pstTran->iCmd != SSI_TOKENQUERY && pstPymtTran->bDupTranDetected == PAAS_FALSE)
		{
			if(isReceiptEnabled())
			{
				iTotCnt += sizeof(stPymntRcptList)/ KEYVAL_SIZE;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Not adding receipt as it is a token query command/Duplicate Transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if((pstPymtTran->pstPOSTenderDtls != NULL) && (iCmd == SCI_CAPTURE))
		{
			iTotCnt += sizeof(stPOSTndrDataLst) / KEYVAL_SIZE;
		}

		if(pstPymtTran->pstRawCardDtls != NULL)
		{
			iTotCnt += sizeof(stRawCrdDtlsLst) / KEYVAL_SIZE;
		}

		if(pstPymtTran->pstPassThrghDtls != NULL && pstPymtTran->pstPassThrghDtls->iResTagsCnt > 0)
		{
			iTotCnt += pstPymtTran->pstPassThrghDtls->iResTagsCnt;
		}

		if(pstPymtTran->pstDCCDtls != NULL && pstPymtTran->pstDCCDtls->iDCCInd == DCC_ELIGIBLE_USER_OPTED)
		{
			iTotCnt += sizeof(stDccRespDtlsLst) / KEYVAL_SIZE;
		}

		/* Allocate the memory for the key value pairs */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Initialize the variables */
		memset(pstList, 0x00, iTotCnt * KEYVAL_SIZE);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* Customer Information response data to POS */
		iCnt = fillCustInfoDtls(curPtr, pstPymtTran->pstCustInfoDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill Customer Info dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		if(pstPymtTran->pstCTranDtls != NULL)
		{
			/* Fill the current payment transaction general details */
			iCnt = fillCTranDtls(curPtr, pstPymtTran->pstCTranDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to fill pymt dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		if(pstPymtTran->pstAmtDtls != NULL)
		{
			/* Fill the amount details */
			iCnt = fillAmtDtls(curPtr, pstPymtTran->pstAmtDtls, pstResp->szRsltCode);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill amt dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		if(pstPymtTran->pstCardDtls != NULL)
		{
			/* Fill the card details */
			iCnt = fillCardDtls(curPtr, pstPymtTran->pstCardDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to fill card dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		if(pstPymtTran->pstCardDtls != NULL && pstPymtTran->pstCardDtls->bEmvData == PAAS_TRUE)
		{
			/* Fill the EMV details */
			iCnt = fillEMVDtls(curPtr, pstPymtTran->pstCardDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to fill EMV dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}
		if(pstPymtTran->pstSigDtls != NULL)
		{
			/* Fill the signature details */
			iCnt = fillSignDtls(curPtr, pstPymtTran->pstSigDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill sig dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		if(pstPymtTran->pstLtyDtls != NULL)
		{
			/* Fill the loyalty details */
			iCnt = fillLtyDtls(curPtr, pstPymtTran->pstLtyDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill lty dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		if(pstPymtTran->pstDupDtls != NULL)
		{
			/* Fill the Duplicate details */
			iCnt = fillDupDtls(curPtr, pstPymtTran->pstDupDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill duplicate dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill the consumer option selected details */
		if(iCmd == SCI_CAPTURE || iCmd == SCI_CREDIT || iCmd == SCI_ACTIVATE ||
		   iCmd == SCI_ADDVAL || iCmd == SCI_GIFTCLOSE || iCmd == SCI_BAL ||
		   iCmd == SCI_GIFTCLOSE || iCmd == SCI_DEACTIVATE)
		{
			/*
			 * CardToken and Token Source can be present in the response
			 * for the current transaction
			 * in that case not including these values which captured
			 * from consumer option selection
			 */
			if(pstPymtTran->pstCTranDtls != NULL)
			{
				if(strlen(pstPymtTran->pstCTranDtls->szCardToken) > 0)//Values are present, dont include
				{
					iIncludeCT = 0;
					iIncludeTS = 0;
				}
				else
				{
					iIncludeCT = 1;
					iIncludeTS = 1;
				}
			}
			else
			{
				iIncludeCT = 1;
				iIncludeTS = 1;
			}

			if(pstPymtTran->pstSess != NULL)
			{
				iCnt = fillConsOptDtls(curPtr, &pstPymtTran->pstSess->stUnsolMsgInfo, iIncludeCT, iIncludeTS);
				if(iCnt < 0)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				curPtr += iCnt;
			}

			if(pstPymtTran->pstSess != NULL)
			{
				iCnt = fillEmailDtls(curPtr, pstPymtTran->pstSess->stUnsolMsgInfo.szEmail);
				if(iCnt < 0)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				curPtr += iCnt;
			}
		}

		if(pstPymtTran->bDupTranDetected == PAAS_TRUE)
		{
			/* Fill the duplicate transaction details */
			iCnt = fillDupTranDtls(curPtr, PAAS_TRUE);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill duplicate Tran dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		if((pstPymtTran->pstFSADtls != NULL) && strlen(pstPymtTran->pstFSADtls->szAmtHealthCare) && (pstPymtTran->pstAmtDtls != NULL))
		{
			/* Fill the FSA Amount details */
			iCnt = fillFSADtls(curPtr, pstPymtTran->pstAmtDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to fill FSA dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		if((pstPymtTran->pstEBTDtls != NULL) && strlen(pstPymtTran->pstEBTDtls->szEBTType) && (pstPymtTran->iPymtType == PYMT_EBT))
		{
			/* Fill the EBT details */
			iCnt = fillEBTDtls(curPtr, pstPymtTran->pstEBTDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to fill EBT dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill The POS Tender Data, in case if user select POS defined tender type */
		if((pstPymtTran->pstPOSTenderDtls != NULL) && (iCmd == SCI_CAPTURE))
		{
			/* Fill the current payment transaction general details */
			iCnt = fillPOSTndrDtls(curPtr, pstPymtTran->pstPOSTenderDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to fill POS Tender Data dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill The VAS Loyalty Data */
		if(bVasDataResp)
		{
			/* Fill the VAS details */
			iCnt = fillVASData(curPtr, pstPymtTran->pstVasDataDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to fill POS Tender Data dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/* Fill The Raw card details for Early return card capture */
		if(pstPymtTran->pstRawCardDtls != NULL)
		{
			/* Fill the raw card details */
			iCnt = fillRawCardDtls(curPtr, pstPymtTran->pstRawCardDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to fill Raw card Data dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		// MukeshS3: Fill the curPtr according to the tags configured for the response list.
		/* Fill Pass through fields for SCI which came in SSI response*/
		if(pstPymtTran->pstPassThrghDtls != NULL && pstPymtTran->pstPassThrghDtls->pstResTagList != NULL)
		{
			iCnt = fillPassThrgResDtls(curPtr, pstPymtTran->pstPassThrghDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to fill Pass through dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		if(pstPymtTran->pstDCCDtls != NULL && pstPymtTran->pstDCCDtls->iDCCInd == DCC_ELIGIBLE_USER_OPTED)
		{
			iCnt = fillDCCResDtls(curPtr, pstPymtTran->pstDCCDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to fill DCC Response through dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}
			curPtr += iCnt;
		}

		/*Fill the receipt Details*/
		if(pstTran->iCmd != SSI_TOKENQUERY && pstPymtTran->bDupTranDetected == PAAS_FALSE && bVasDataResp == PAAS_FALSE && pstPymtTran->pstRawCardDtls == NULL)
		{
			if (isReceiptEnabled())
			{
				iResltCode = atoi(pstResp->szRsltCode);
				if(iResltCode == 59001) //user cancelled
				{
					/*ArjunU1: For cancelled transactions, especially when user press cancel on signature screen.
					 * Need to indicate receipt field result as CANCELLED.
					 */
					memset(pstTran->stRespDtls.szRslt, 0x00, sizeof(pstTran->stRespDtls.szRslt));
					strcpy(pstTran->stRespDtls.szRslt, pstResp->szRslt);
				}
				iCnt = fillReceiptData(curPtr, pstTran, iResltCode);
				if(iCnt < 0)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to fill receipt dtls",__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				curPtr += iCnt;
			}
		}

		/* Set the metadata */
		memset(pstMeta, 0x00, METADATA_SIZE);
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		//rv = iTotCnt;
		break;
	}

	if(rv != SUCCESS && (pstList != NULL))
	{
		/* FIXME: Not a correct way of de-allocation, possible memory leakage
		 * as the values of filled keys would be lost. Find a way to solve this
		 * issue. */
		//free(pstList);
		// MukeshS3: 28-Jan-15: Fixing this
		freeKeyValDataNode(pstList, iTotCnt);
		pstMeta->keyValList = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForSAF
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForSAF(SAFDTLS_PTYPE pstSafDtls, RESPDTLS_PTYPE pstResp,
														META_PTYPE pstMeta)
{
	int				rv			 	= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	curPtr			= NULL;
	KEYVAL_PTYPE	pstList			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(safRespLst) / KEYVAL_SIZE;

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillSAFDtls(curPtr, pstSafDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill SAF dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForSurvey5
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForSurvey5(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp,
														META_PTYPE pstMeta)
{
	int					rv			 	= SUCCESS;
	int					iCnt			= 0;
	int					iTotCnt			= 0;
	KEYVAL_PTYPE		curPtr			= NULL;
	KEYVAL_PTYPE		pstList			= NULL;
	SURVEYDTLS_PTYPE	pstSurveyDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stSurvey5Lst)  / KEYVAL_SIZE;

		pstSurveyDtls = &(pstDevTran->stSurveyDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillSurvey5Dtls(curPtr, pstSurveyDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill Survey5 dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForSurvey10
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForSurvey10(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp,
														META_PTYPE pstMeta)
{
	int					rv			 	= SUCCESS;
	int					iCnt			= 0;
	int					iTotCnt			= 0;
	KEYVAL_PTYPE		curPtr			= NULL;
	KEYVAL_PTYPE		pstList			= NULL;
	SURVEYDTLS_PTYPE	pstSurveyDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stSurvey10Lst)  / KEYVAL_SIZE;

		pstSurveyDtls = &(pstDevTran->stSurveyDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillSurvey10Dtls(curPtr, pstSurveyDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill Survey10 dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForDevName
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForDevName(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp,
														META_PTYPE pstMeta)
{
	int					rv			 	= SUCCESS;
	int					iCnt			= 0;
	int					iTotCnt			= 0;
	KEYVAL_PTYPE		curPtr			= NULL;
	KEYVAL_PTYPE		pstList			= NULL;
	DEVICENAME_PTYPE	pstDevNameDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stGetDevNameLst)  / KEYVAL_SIZE;

		pstDevNameDtls = &(pstDevTran->stDevNameDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillDevNameDtls(curPtr, pstDevNameDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill Charity dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForGetPaymentTypes
 *
 * Description	: Builds the Meta Data for the Get Payment Types command response
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForGetPaymentTypes(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int					rv			 		= SUCCESS;
	int					iCnt				= 0;
	int					iTotCnt				= 0;
	KEYVAL_PTYPE		curPtr				= NULL;
	KEYVAL_PTYPE		pstList				= NULL;
	PAYMENTTYPES_PTYPE  pstPymtTypesDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stPaymentTypesLst)  / KEYVAL_SIZE;

		pstPymtTypesDtls = &(pstDevTran->stPymtTypeDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillPaymentTypesDtls(curPtr, pstPymtTypesDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill Payment Types dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForSetParm
 *
 * Description	:  CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForSetParm(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				  rv			 	 = SUCCESS;
	int				  iCnt			     = 0;
	int				  iTotCnt			 = 0;
	KEYVAL_PTYPE	  curPtr			 = NULL;
	KEYVAL_PTYPE	  pstList			 = NULL;
	SETPARM_PTYPE	  pstSetParmDtls	 = NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;

		pstSetParmDtls = &(pstDevTran->stSetParmDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForDispMsg
 *
 * Description	:  CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForDispMsg(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				  rv			 	 = SUCCESS;
	int				  iCnt			     = 0;
	int				  iTotCnt			 = 0;
	KEYVAL_PTYPE	  curPtr			 = NULL;
	KEYVAL_PTYPE	  pstList			 = NULL;
	DISPMSG_PTYPE	  pstDispMsgDtls	 = NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stDispMsgLst)  / KEYVAL_SIZE;

		pstDispMsgDtls = &(pstDevTran->stDispMsgDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillDispMsgDtls(curPtr, pstDispMsgDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill Display Message Dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForGetParm
 *
 * Description	:  CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForGetParm(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				  rv			 	 = SUCCESS;
	int				  iCnt			     = 0;
	int				  iTotCnt			 = 0;
	KEYVAL_PTYPE	  curPtr			 = NULL;
	KEYVAL_PTYPE	  pstList			 = NULL;
	GETPARM_PTYPE	  pstGetParmDtls	 = NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stGetParmRespLst)  / KEYVAL_SIZE;

		pstGetParmDtls = &(pstDevTran->stGetParmDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		curPtr += iCnt;

		iCnt = fillGetParmDtls(curPtr, pstGetParmDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill Get Parm Dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForGetCardData
 *
 * Description	:Build Meta Data for GET_CARD_DATA Device command Resposne.
 *
 * Input Params	:DevTran, Resp Structure, Meta type
 *
 * Output Params:
 * ============================================================================
 */
int buildMetaForGetCardData(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				 		 rv			 	 	= SUCCESS;
	int				  		iCnt			    = 0;
	int				  		iTotCnt			 	= 0;
	KEYVAL_PTYPE	  		curPtr			 	= NULL;
	KEYVAL_PTYPE	  		pstList			 	= NULL;
	GET_CARDDATA_PTYPE	  	pstGetCardDataDtls = NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stRawCrdDtlsLst)  / KEYVAL_SIZE;

		pstGetCardDataDtls = &(pstDevTran->stGetCardDataDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		curPtr += iCnt;

		if(pstGetCardDataDtls != NULL)
		{
			iCnt = fillGetCardDtls(curPtr, pstGetCardDataDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForDispQRCOde
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForDispQRCOde(RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				  rv			 	 = SUCCESS;
	int				  iCnt			     = 0;
	int				  iTotCnt			 = 0;
	KEYVAL_PTYPE	  curPtr			 = NULL;
	KEYVAL_PTYPE	  pstList			 = NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForCancelQRCOde
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForCancelQRCOde(RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				  rv			 	 = SUCCESS;
	int				  iCnt			     = 0;
	int				  iTotCnt			 = 0;
	KEYVAL_PTYPE	  curPtr			 = NULL;
	KEYVAL_PTYPE	  pstList			 = NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForEmpID
 *
 * Description	: Builds the Meta Data for the Employee ID capture command response
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForEmpID(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp,
														META_PTYPE pstMeta)
{
	int					rv			 	= SUCCESS;
	int					iCnt			= 0;
	int					iTotCnt			= 0;
	KEYVAL_PTYPE		curPtr			= NULL;
	KEYVAL_PTYPE		pstList			= NULL;
	EMPID_PTYPE     	pstEmpIDDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stEmpIDLst)  / KEYVAL_SIZE;

		pstEmpIDDtls = &(pstDevTran->stEmpIDDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillEmpIDDtls(curPtr, pstEmpIDDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill Employee ID dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForCharity
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForCharity(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp,
														META_PTYPE pstMeta)
{
	int					rv			 	= SUCCESS;
	int					iCnt			= 0;
	int					iTotCnt			= 0;
	KEYVAL_PTYPE		curPtr			= NULL;
	KEYVAL_PTYPE		pstList			= NULL;
	CHARITYDTLS_PTYPE	pstCharityDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stCharityLst)  / KEYVAL_SIZE;

		pstCharityDtls = &(pstDevTran->stCharityDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillCharityDtls(curPtr, pstCharityDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill Charity dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForCustomerButton
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForCustomerButton(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp,
														META_PTYPE pstMeta)
{
	int					    rv			 	= SUCCESS;
	int					    iCnt			= 0;
	int					    iTotCnt			= 0;
	KEYVAL_PTYPE		    curPtr			= NULL;
	KEYVAL_PTYPE		    pstList			= NULL;
	CUSTBUTTONDTLS_PTYPE	pstCustButtonDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stCustButtonLst)  / KEYVAL_SIZE;

		pstCustButtonDtls = &(pstDevTran->stCustButtonDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillCustomerButtonDtls(curPtr, pstCustButtonDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill Customer Button dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForCustQues
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForCustQues(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp,
														META_PTYPE pstMeta)
{
	int					rv			 	= SUCCESS;
	int					iCnt			= 0;
	int					iTotCnt			= 0;
	KEYVAL_PTYPE		curPtr			= NULL;
	KEYVAL_PTYPE		pstList			= NULL;
	SURVEYDTLS_PTYPE	pstSurveyDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stCustQuesLst) / KEYVAL_SIZE;

		pstSurveyDtls = &(pstDevTran->stSurveyDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillCustQuesDtls(curPtr, pstSurveyDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill customer question dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: buildMetaForSign
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForSign(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp,
														META_PTYPE pstMeta)
{
	int				rv			 	= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	curPtr			= NULL;
	KEYVAL_PTYPE	pstList			= NULL;
	SIGDTLS_PTYPE	pstSigDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stSignLst) / KEYVAL_SIZE;

		pstSigDtls = &(pstDevTran->stSigDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillSignDtls(curPtr, pstSigDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill sign dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForEmail
 *
 * Description	: 
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForEmail(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp, 
														META_PTYPE pstMeta)
{
	int				rv			 	= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	curPtr			= NULL;
	KEYVAL_PTYPE	pstList			= NULL;
	LTYDTLS_PTYPE	pstLtyDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stEmailLst) / KEYVAL_SIZE;

		pstLtyDtls = &(pstDevTran->stLtyDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillEmailDtls(curPtr, pstLtyDtls->szEmail);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill email", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForLaneClosed
 *
 * Description	:  CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForLaneClosed(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				  rv			 	 = SUCCESS;
	int				  iCnt			     = 0;
	int				  iTotCnt			 = 0;
	KEYVAL_PTYPE	  curPtr			 = NULL;
	KEYVAL_PTYPE	  pstList			 = NULL;
	LANECLOSED_PTYPE  pstLaneClosedDtls  = NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;

		pstLaneClosedDtls = &(pstDevTran->stLaneClosed);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForApplyUpdates
 *
 * Description	:  CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForApplyUpdates(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				  rv			 	 = SUCCESS;
	int				  iCnt			     = 0;
	int				  iTotCnt			 = 0;
	KEYVAL_PTYPE	  curPtr			 = NULL;
	KEYVAL_PTYPE	  pstList			 = NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForGetCounter
 *
 * Description	:  CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForGetCounter(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				rv			 	 = SUCCESS;
	int				iCnt			 = 0;
	int				iTotCnt			 = 0;
	KEYVAL_PTYPE	curPtr			 = NULL;
	KEYVAL_PTYPE	pstList			 = NULL;
//	COUNTERDTLS_PTYPE pstCounterDtls = NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
//		iTotCnt += sizeof(stGetCounterLst) / KEYVAL_SIZE;

//		pstCounterDtls = &(pstDevTran->stCounterDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

/*		iCnt = fillCounterDtls(curPtr, pstCounterDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill counter details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}*/

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForScndryPortCmd
 *
 * Description	:  This API would build Meta data for only Main response List and Secondary Data List
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForScndryPortCmd(RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				rv			 	= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	curPtr			= NULL;
	KEYVAL_PTYPE	pstList			= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stScndLst) / KEYVAL_SIZE;

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillScndData(curPtr);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill time details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForTime
 *
 * Description	:  CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForStatus(RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				rv			 	= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	curPtr			= NULL;
	KEYVAL_PTYPE	pstList			= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stScndLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stStatusLst) / KEYVAL_SIZE;

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillScndData(curPtr);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill time details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillStatusDetails(curPtr);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill time details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForTime
 *
 * Description	:  CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForTime(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				rv			 	= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	curPtr			= NULL;
	KEYVAL_PTYPE	pstList			= NULL;
	TIMEDTLS_PTYPE	pstTimeDtls		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stTimeLst) / KEYVAL_SIZE;

		pstTimeDtls = &(pstDevTran->stTimeDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillTimeDtls(curPtr, pstTimeDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill time details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForVersion
 *
 * Description	:  CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForVersion(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				rv			 	= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	curPtr			= NULL;
	KEYVAL_PTYPE	pstList			= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stVersionLst) / KEYVAL_SIZE;

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillVersionDtls(curPtr, pstDevTran);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill version", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: buildMetaForLPToken
 *
 * Description	:  CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForLPToken(char * pszTranKey, RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				rv			 	= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	TRAN_PTYPE		pstTran			= NULL;
	PYMTTRAN_PTYPE  pstPymtTran     = NULL;
	KEYVAL_PTYPE	curPtr			= NULL;
	KEYVAL_PTYPE	pstList			= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	while(1)
	{
		rv = getTopSSITran(pszTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get tran data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		/* get the payment details from the transaction in the stack */
		pstPymtTran = (PYMTTRAN_PTYPE) (pstTran->data);
		if(pstPymtTran == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Missing Payment details",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Get the size of the memory to be allocated for the key value pairs */
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;

		if(pstPymtTran->pstCTranDtls != NULL)
		{
			iTotCnt += sizeof(cTranDtlsLst) / KEYVAL_SIZE;
		}

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		if(pstPymtTran->pstCTranDtls != NULL)
		{
			/* Fill the current payment transaction general details */
			iCnt = fillCTranDtls(curPtr, pstPymtTran->pstCTranDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to fill pymt dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}
	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;

}
/*
 * ============================================================================
 * Function Name: buildMetaForLty
 *
 * Description	:  CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForLty(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp,
														META_PTYPE pstMeta)
{
	int				rv			 	= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	curPtr			= NULL;
	KEYVAL_PTYPE	pstList			= NULL;
	LTYDTLS_PTYPE	pstLtyDtls		= NULL;
	VASDATA_DTLS_PTYPE	pstVasDataDtls	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		//For VAS Data ONLY Sending Gen Response and VAS Data to POS
		if(isWalletEnabled())
		{
			pstVasDataDtls = &(pstDevTran->stVasDataDtls);

			if(	pstVasDataDtls != NULL 						&&
				pstVasDataDtls->bVASPresent == PAAS_TRUE 	&&
				pstVasDataDtls->bVASsentToPOS == PAAS_FALSE	)
			{
				debug_sprintf(szDbgMsg, "%s: Sending Device VAS Data response to POS", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = getMetaDataForSCIVasDataResp(pstMeta, pstVasDataDtls, pstResp);
				//rv = iTotCnt;
				break;
			}
		}

		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		/*
		 * We need to send the LOYALTY_DATA tag for the stand alone
		 * loyalty command, thats why having the new list separately
		 * for this
		 * TO_DO: Change it to make it better
		 */
		iTotCnt += sizeof(stSALtyLst) / KEYVAL_SIZE;

		pstLtyDtls = &(pstDevTran->stLtyDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillStandAloneLtyDtls(curPtr, pstLtyDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill email", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: buildMetaForProvisionPass
 *
 * Description	:  CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForProvisionPass(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp,
														META_PTYPE pstMeta)
{
	int						rv			 	= SUCCESS;
	int						iCnt			= 0;
	int						iTotCnt			= 0;
	PAAS_BOOL				bVasDataResp	=PAAS_FALSE;
	KEYVAL_PTYPE			curPtr			= NULL;
	KEYVAL_PTYPE			pstList			= NULL;
	VASDATA_DTLS_PTYPE  	pstVasDataDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;

		pstVasDataDtls = &(pstDevTran->stVasDataDtls);

		if(pstVasDataDtls->bProvisionPassSent == PAAS_TRUE)
		{
			bVasDataResp = PAAS_TRUE;
		}
		if(bVasDataResp)
		{
			iTotCnt += sizeof(stLoyaltyWalletOffers)/KEYVAL_SIZE;
		}

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		if(bVasDataResp)
		{
			/* Fill the VAS details */
			iCnt = fillVASData(curPtr, pstVasDataDtls);
			if(iCnt < 0)
			{
				debug_sprintf(szDbgMsg,"%s: FAILED to fill POS Tender Data dtls",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			//curPtr += iCnt;
		}
		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		//free(pstList);
		//MukeshS3: 28-jan-16: Fixing this
		freeKeyValDataNode(pstList, iTotCnt);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForCustomerButton
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForCreditApp(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int					    rv			 		= SUCCESS;
	int					    iCnt				= 0;
	int					    iTotCnt				= 0;
	KEYVAL_PTYPE		    curPtr				= NULL;
	KEYVAL_PTYPE		    pstList				= NULL;
	CREDITAPPDTLS_PTYPE		pstCreditAppDtls	= NULL;
#ifdef DEBUG
	char					szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stCreditAppLst)  / KEYVAL_SIZE;

		pstCreditAppDtls = &(pstDevTran->stCreditAppDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to Fill General Details",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillCreditAppDtls(curPtr, pstCreditAppDtls);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to fill Credit Application Response Details",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		free(pstList);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForQueryNfcIni
 *
 * Description	:  CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForQueryNfcIni(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp,
														META_PTYPE pstMeta)
{
	int							rv			 	= SUCCESS;
	int							iCnt			= 0;
	int							iTotCnt			= 0;
	KEYVAL_PTYPE				curPtr			= NULL;
	KEYVAL_PTYPE				pstList			= NULL;
	NFCINI_CONTENT_INFO_PTYPE  	pstNfcIniContent= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stApplePayDtlsLst)/KEYVAL_SIZE;

		pstNfcIniContent = &(pstDevTran->stNfcIniContentInfoDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		iCnt = fillQueryNfcIniRespDtls(curPtr, pstNfcIniContent);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to QueryNfcIni file resp dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}


		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildMetaForCustCheckbox
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildMetaForCustCheckbox(DEVTRAN_PTYPE pstDevTran, RESPDTLS_PTYPE pstResp, META_PTYPE pstMeta)
{
	int				  rv			 	 = SUCCESS;
	int				  iCnt			     = 0;
	int				  iTotCnt			 = 0;
	KEYVAL_PTYPE	  curPtr			 = NULL;
	KEYVAL_PTYPE	  pstList			 = NULL;
	CUSTCHECKBOX_DTLS_PTYPE pstCustCheckBox;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt += sizeof(stMainRespLst) / KEYVAL_SIZE;
		iTotCnt += sizeof(stCustCheckboxDtlsLst)/ KEYVAL_SIZE;

		pstCustCheckBox = &(pstDevTran->stCustCheckBoxDtls);

		/* ----------- Assign the memory for building the list ---------- */
		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------------------- Build the list ---------------------- */
		memset(pstList, 0x00, KEYVAL_SIZE * iTotCnt);
		curPtr = pstList;

		/* General transaction response data to POS */
		iCnt = fillGenRespDtls(curPtr, pstResp);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill gen dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		curPtr += iCnt;

		iCnt = fillCustCheckBoxRespDtls(curPtr, pstCustCheckBox);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to fill Customer Checkbox Resp dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ------ Update the meta data with the list information ----- */
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = pstList;

		break;
	}

	if((rv != SUCCESS) && (pstList != NULL))
	{
		/* De-allocate memory for the key list FIXME */
		freeKeyValDataNode(pstList, iTotCnt);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillGenRespDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillGenRespDtls(KEYVAL_PTYPE curPtr, RESPDTLS_PTYPE pstResp)
{
	int		iLen			= 0;
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstResp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Resp struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stMainRespLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stMainRespLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "RESPONSE_TEXT") == SUCCESS)
		{
			iLen = strlen(pstResp->szRespTxt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstResp->szRespTxt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "RESULT") == SUCCESS)
		{
			iLen = strlen(pstResp->szRslt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstResp->szRslt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "RESULT_CODE") == SUCCESS)
		{
			iLen = strlen(pstResp->szRsltCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstResp->szRsltCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "HOST_RESPCODE") == SUCCESS)
		{
			/*
			 * Praveen_P1: We are keeping default value of host result code
			 * as -2, if it is some other value then send this
			 * field to the POS
			 */

			if(strcmp(pstResp->szHostRsltCode, "-2") != 0)
			{
				iLen = strlen(pstResp->szHostRsltCode);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, pstResp->szHostRsltCode);
					}
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "RESPONSE_CODE") == SUCCESS)
		{
			/*
			 * Praveen_P1: We are keeping default value of host result code
			 * as -2, if it is some other value then send this
			 * field to the POS
			 */

			if(strcmp(pstResp->szRespCode, "-2") != 0)
			{
				iLen = strlen(pstResp->szRespCode);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, pstResp->szRespCode);
					}
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TERMINATION_STATUS") == SUCCESS)
		{
			iLen = strlen(pstResp->szTermStat);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstResp->szTermStat);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TRAINING_MODE") == SUCCESS)
		{
			if(isDemoModeEnabled())
			{
				iLen = 2;
				if( iLen > 0 )
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, "ON");
					}
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "COUNTER") == SUCCESS)
		{
			iLen = strlen(pstResp->szTranCntr);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstResp->szTranCntr);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillOtherGenRespDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillOtherGenRespDtls(KEYVAL_PTYPE curPtr, OTHER_RESPDTLS_PTYPE pstOtherResp)
{
	int		iLen			= 0;
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstOtherResp == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Other Gen Resp struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stOtherRespLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stOtherRespLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "TRANS_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstOtherResp->szTranAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstOtherResp->szTranAmt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PAYMENT_TYPE") == SUCCESS)
		{
			iLen = strlen(pstOtherResp->szPymntType);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstOtherResp->szPymntType);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EBT_TYPE") == SUCCESS && (strlen(pstOtherResp->szPymntType) > 0) && (strcmp(pstOtherResp->szPymntType, "EBT") == SUCCESS))
		{
			iLen = strlen(pstOtherResp->szEBTType);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstOtherResp->szEBTType);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMBOSSED_ACCT_NUM") == SUCCESS)
		{
			iLen = strlen(pstOtherResp->szEmbossedPAN);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstOtherResp->szEmbossedPAN);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PAYMENT_MEDIA") == SUCCESS)
		{
			iLen = strlen(pstOtherResp->szPaymentMedia);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstOtherResp->szPaymentMedia);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARDHOLDER") == SUCCESS)
		{
			iLen = strlen(pstOtherResp->szCardHolder);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstOtherResp->szCardHolder);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "ACCT_NUM") == SUCCESS)
		{
			iLen = strlen(pstOtherResp->szPAN);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstOtherResp->szPAN);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_EXP_MONTH") == SUCCESS)
		{
			iLen = strlen(pstOtherResp->szClrMon);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstOtherResp->szClrMon);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_EXP_YEAR") == SUCCESS)
		{
			iLen = strlen(pstOtherResp->szClrYear);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstOtherResp->szClrYear);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_ENTRY_MODE") == SUCCESS)
		{
			iLen = strlen(pstOtherResp->szCardEntryMode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstOtherResp->szCardEntryMode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_ABBRV") == SUCCESS)
		{
			iLen = strlen(pstOtherResp->szCardAbbrv);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstOtherResp->szCardAbbrv);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillCustInfoDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillCustInfoDtls(KEYVAL_PTYPE curPtr, CUSTINFODTLS_PTYPE pstCustInfoDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCustInfoDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Cust Info dtls to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stCustInfoLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stCustInfoLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "CUSTOMER_STREET") == SUCCESS)
		{
			iLen = strlen(pstCustInfoDtls->szCustStreetName);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCustInfoDtls->szCustStreetName);
				}
				curPtr[iCnt].value = tmpPtr;
			}
		}
		if(strcmp(curPtr[iCnt].key, "CUSTOMER_ZIP") == SUCCESS)
		{
			iLen = strlen(pstCustInfoDtls->szCustZipNum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCustInfoDtls->szCustZipNum);
				}
				curPtr[iCnt].value = tmpPtr;
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillSAFDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillSAFDtls(KEYVAL_PTYPE curPtr, SAFDTLS_PTYPE pstSAFDtls)
{
	int		iLen			= 0;
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	void *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(safRespLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &safRespLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "RECORD_COUNT") == SUCCESS)
		{
			iLen = strlen(pstSAFDtls->szRecCnt);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc((iLen + 1) * sizeof(char));
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					memcpy(tmpPtr, pstSAFDtls->szRecCnt, iLen);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TOTAL_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstSAFDtls->szTotAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *)malloc((iLen + 1) * sizeof(char));
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, (iLen + 1) * sizeof(char));
					memcpy(tmpPtr, pstSAFDtls->szTotAmt, iLen);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "RECORDS") == SUCCESS)
		{
			if(pstSAFDtls->recList != NULL)
			{
				tmpPtr = (VAL_LST_PTYPE) malloc(sizeof(VAL_LST_STYPE));
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, sizeof(VAL_LST_STYPE));
					fillSAFRecs(pstSAFDtls->recList, (VAL_LST_PTYPE) tmpPtr);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillSAFRecs
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillSAFRecs(SAFREC_PTYPE pstSAFRec, VAL_LST_PTYPE pstValLst)
{
	int				rv			= SUCCESS;
	int				iTotCnt		= 0;
	int				iPassThrCnt = 0;
	KEYVAL_PTYPE	pstList		= NULL;
	VAL_NODE_PTYPE	pstTmp		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(safRecDtls) / KEYVAL_SIZE;

	while(pstSAFRec != NULL)
	{
		//MukeshS3: 19-April-16: Adding support for Pass through fields
		if(pstSAFRec->pstPassThrghDtls != NULL && pstSAFRec->pstPassThrghDtls->pstResTagList != NULL)
		{
			iPassThrCnt = pstSAFRec->pstPassThrghDtls->iResTagsCnt;
		}
		else
		{
			iPassThrCnt = 0;
		}

		pstTmp = (VAL_NODE_PTYPE) malloc(sizeof(VAL_NODE_STYPE));
		if(pstTmp == NULL)
		{
			rv = FAILURE;
			break;
		}

		memset(pstTmp, 0x00, sizeof(VAL_NODE_STYPE));

		pstList = (KEYVAL_PTYPE) malloc((iTotCnt + iPassThrCnt) * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			free(pstTmp);
			pstTmp = NULL;

			rv = FAILURE;
			break;
		}

		memset(pstList, 0x00, (iTotCnt + iPassThrCnt) * KEYVAL_SIZE);

		pstTmp->elemCnt = (iTotCnt + iPassThrCnt);
		pstTmp->elemList = pstList;
		pstTmp->szVal = (char *) malloc(strlen("RECORD") + 1);
		strcpy(pstTmp->szVal, "RECORD");

		fillSAFRecDtls(pstSAFRec, pstTmp->elemList);

		/* Add to the linked list */
		if(pstValLst->start == NULL)
		{
			pstValLst->start = pstValLst->end = pstTmp;
		}
		else
		{
			pstValLst->end->next = pstTmp;
			pstValLst->end = pstTmp;
		}

		pstSAFRec = (SAFREC_PTYPE) pstSAFRec->next;
	}

	/* TODO: Check for FAILURE conditions and de-allocate memory properly */

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillVASLoyaltyDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillVASLoyaltyDtls(VAL_LST_PTYPE pstValLst, VASDATA_DTLS_PTYPE pstVasDataDtls)
{
	int				iLen		= 0;
	char *			tmpPtr		= NULL;
	int				rv			= SUCCESS;
	int				iTotCnt		= 0;
	int				iCnt		= 0;
	KEYVAL_PTYPE	pstList		= NULL;
	KEYVAL_PTYPE 	curPtr		= NULL;
	VAL_NODE_PTYPE	pstTmp		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stLoyaltyWalletData) / KEYVAL_SIZE;

	if(pstVasDataDtls != NULL)
	{
		pstTmp = (VAL_NODE_PTYPE) malloc(sizeof(VAL_NODE_STYPE));
		if(pstTmp == NULL)
		{
			return FAILURE;
		}
		memset(pstTmp, 0x00, sizeof(VAL_NODE_STYPE));

		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			free(pstTmp);
			pstTmp = NULL;

			return FAILURE;
		}

		memset(pstList, 0x00, iTotCnt * KEYVAL_SIZE);

		pstTmp->elemCnt = iTotCnt;
		pstTmp->elemList = pstList;

		curPtr = pstTmp->elemList;
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			memcpy(&curPtr[iCnt], &stLoyaltyWalletData[iCnt], KEYVAL_SIZE);
			if(strcmp(curPtr[iCnt].key, "PUBLISHER") == SUCCESS)
			{
				iLen = strlen(pstVasDataDtls->szPubliserName);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstVasDataDtls->szPubliserName);
					}
					else
					{
						rv = FAILURE;
						break;
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "LOYALTY_PAYLOAD") == SUCCESS)
			{
				iLen = strlen(pstVasDataDtls->szLoyaltyData);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstVasDataDtls->szLoyaltyData);
					}
					else
					{
						rv = FAILURE;
						break;
					}
				}
			}
			curPtr[iCnt].value = tmpPtr;
			tmpPtr = NULL;
		}
		/* Add to the linked list */
		if(pstValLst->start == NULL)
		{
			pstValLst->start = pstValLst->end = pstTmp;
		}
		else
		{
			pstValLst->end->next = pstTmp;
			pstValLst->end = pstTmp;
		}
	}

	// CID-67434: 28-Jan-16: MukeshS3:
	/* Check for FAILURE conditions and de-allocate memory properly */
	if(rv != SUCCESS)
	{
		// memory for pstValLst was assiged in the caller function, but as we wont have iCnt value there to pass in this function
		// So, freeing this memory here only
		freeKeyValDataNode(pstList, iCnt);
		freeValNode(pstTmp);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillVASData
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillVASData(KEYVAL_PTYPE pstKeyValLst, VASDATA_DTLS_PTYPE pstVasDataDtls)
{
	void *			tmpPtr		= NULL;
	int				rv			= SUCCESS;
	int				iTotCnt		= 0;
	int				iCnt		= 0;
	int				iLen		= 0;
	char			szTmp[3]	= "";
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stLoyaltyWalletOffers) / KEYVAL_SIZE;

	if(pstVasDataDtls != NULL)
	{
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			memcpy(&pstKeyValLst[iCnt], &stLoyaltyWalletOffers[iCnt], KEYVAL_SIZE);

			if(strcmp(pstKeyValLst[iCnt].key, "LOYALTY_OFFERS") == SUCCESS && pstVasDataDtls->bVASPresent == PAAS_TRUE)
			{
				tmpPtr 	= (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, sizeof(VAL_LST_STYPE));
					rv 	= fillVASLoyaltyDtls(tmpPtr, pstVasDataDtls);
					if (rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to fill the data", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						// CID-67434: 22-Jan-16: MukeshS3: We must free allocated memory for tmpPtr.
						freeValListNode(tmpPtr);
						break;
					}
				}
			}
			else if(strcmp(pstKeyValLst[iCnt].key, "LOYALTY_VAS") == SUCCESS && pstVasDataDtls->bVASPresent == PAAS_TRUE)
			{
				memset(szTmp, 0x00, sizeof(szTmp));
				strcpy(szTmp, "1");
				iLen = strlen(szTmp);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						memcpy(tmpPtr, szTmp, iLen);
					}
					else
					{
						rv = FAILURE;
						break;
					}
				}
			}
			else if(strcmp(pstKeyValLst[iCnt].key, "PROVISION_PASS") == SUCCESS && pstVasDataDtls->bProvisionPassSent == PAAS_TRUE)
			{
				memset(szTmp, 0x00, sizeof(szTmp));
				strcpy(szTmp, "1");
				iLen = strlen(szTmp);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						memcpy(tmpPtr, szTmp, iLen);
					}
					else
					{
						rv = FAILURE;
						break;
					}
				}
			}

			pstKeyValLst[iCnt].value = tmpPtr;
			tmpPtr = NULL;
		}
	}

	/* TODO: Check for FAILURE conditions and de-allocate memory properly */
	// CID-67434: 29-Jan-16: MukeshS3:
	// Freeing the memory for Key-Value pair, till iCnt node. freeing memory for rest nodes will be taken care by caller function.
	if(rv != SUCCESS)
	{
		freeKeyValDataNode(pstKeyValLst, iCnt);
		pstKeyValLst = NULL;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillSAFRecDtls
 *
 * Description	: CHECKED 
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillSAFRecDtls(SAFREC_PTYPE pstSAFRec, KEYVAL_PTYPE curPtr)
{
	int		iCnt			= 0;
	int		jCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	iTotCnt = sizeof(safRecDtls) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &safRecDtls[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "ACCT_NUM") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szPAN);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szPAN);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TRANS_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szTranAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szTranAmt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "APPROVED_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szApprvdAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szApprvdAmt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "INVOICE") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szInvoice);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szInvoice);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PAYMENT_TYPE") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szPymtType);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szPymtType);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PAYMENT_MEDIA") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szPymtMedia);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szPymtMedia);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SAF_NUM") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szSAFNo);
			if(iLen > 0 && pstSAFRec->iCmd != SSI_TOR)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szSAFNo);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SAF_TOR_NUM") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szSAFNo);
			if(iLen > 0 && pstSAFRec->iCmd == SSI_TOR)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szSAFNo);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SAF_STATUS") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szSAFStatus);
			if(iLen > 0 && pstSAFRec->iCmd != SSI_TOR)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szSAFStatus);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SAF_TOR_STATUS") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szSAFStatus);
			if(iLen > 0 && pstSAFRec->iCmd == SSI_TOR)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szSAFStatus);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TROUTD") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szTroutd);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szTroutd);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CTROUTD") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szCTroutd);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szCTroutd);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "AUTH_CODE") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szAuthCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szAuthCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_TOKEN") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szCardToken);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szCardToken);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "BUSINESSDATE") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szBusinessDate);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szBusinessDate);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "BANK_USERDATA") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szBankUserData);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szBankUserData);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CVV2_CODE") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szCVVCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szCVVCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TXN_POSENTRYMODE") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szTxnPosEntryMode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szTxnPosEntryMode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "MERCHID") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szMerchId);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szMerchId);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TERMID") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szTermId);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szTermId);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "LANE") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szLaneId);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szLaneId);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "STORE_NUM") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szStoreId);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szStoreId);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "COMMAND") == SUCCESS)
		{
			iLen = strlen(szSCICmd[pstSAFRec->iCmd]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szSCICmd[pstSAFRec->iCmd]);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "LPTOKEN") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szLPToken);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szLPToken);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PPCV") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szPPCV);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szPPCV);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "AVS_CODE") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szAvsCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szAvsCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "HOST_RESPCODE") == SUCCESS)
		{
			/*
			 * Praveen_P1: We are keeping default value of host result code
			 * as -2, if it is some other value then send this
			 * field to the POS
			 * AjayS2: 18-May-2016: As Praveen mentioned above, we should not send this field to POS for SAF Query also.
			 * PTMX - 1375: 2.19.24 b3:When SAF query is done for not processed records ,hostrespcode tag has a value of -2
			 */
			if(strcmp(pstSAFRec->szHostRespCode, "-2") != 0)
			{
				iLen = strlen(pstSAFRec->szHostRespCode);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstSAFRec->szHostRespCode);
					}
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SIGNATUREREF") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szSignatureRef);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szSignatureRef);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "APR_TYPE") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szAprType);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szAprType);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PURCHASE_APR") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szPurchaseApr);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szPurchaseApr);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SVC_PHONE") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szSvcPhone);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szSvcPhone);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "STATUS_FLAG") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szStatusFlag);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szStatusFlag);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "RECEIPT_TEXT") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szReceiptText);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szReceiptText);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "ACTION_CODE") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szActionCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szActionCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CREDIT_PLAN_NBR") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szCreditPlanNbr);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szCreditPlanNbr);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PAYMENT_SUBTYPE") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szPymtSubType);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szPymtSubType);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "AUTHNWID") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szAthNtwID);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szAthNtwID);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TRANS_DATE") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szTransDate);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szTransDate);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TRANS_TIME") == SUCCESS)
		{
			iLen = strlen(pstSAFRec->szTransTime);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSAFRec->szTransTime);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	// MukeshS3: After filling the above fields, we need to look for pass through fields present in SAF records.
	if(pstSAFRec->pstPassThrghDtls != NULL && pstSAFRec->pstPassThrghDtls->pstResTagList != NULL)
	{
		iTotCnt += pstSAFRec->pstPassThrghDtls->iResTagsCnt;
		debug_sprintf(szDbgMsg, "%s: Total [%d]", __FUNCTION__, iTotCnt);
		APP_TRACE(szDbgMsg);
		for(/* continue iCnt value */jCnt = 0; iCnt < iTotCnt; iCnt++, jCnt++)
		{
			curPtr[iCnt].key = pstSAFRec->pstPassThrghDtls->pstResTagList[jCnt].key;
			curPtr[iCnt].valType = SINGLETON;
			if(pstSAFRec->pstPassThrghDtls->pstResTagList[jCnt].value != NULL)
			{
				iLen = strlen((char*)pstSAFRec->pstPassThrghDtls->pstResTagList[jCnt].value);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, (char*)pstSAFRec->pstPassThrghDtls->pstResTagList[jCnt].value);
					}
				}
			}
			curPtr[iCnt].value = tmpPtr;
			tmpPtr = NULL;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillRegRespDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillRegRespDtls(KEYVAL_PTYPE curPtr, POSSEC_PTYPE pstPosSec, int iCmd)
{
	int		iLen			= 0;
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(posRegRespLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &posRegRespLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "MAC_KEY") == SUCCESS)
		{
			if(iCmd == SCI_REG) //For the REGISTER command it is MAC_KEY
			{
				iLen  = strlen(pstPosSec->szMACKey);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstPosSec->szMACKey);
					}
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "MAC_LABEL") == SUCCESS)
		{
			iLen = strlen(pstPosSec->szMACLbl);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstPosSec->szMACLbl);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TERMINAL_KEY") == SUCCESS)
		{
			if(iCmd == SCI_EREG) //For the REGISTER_ENCRYPTION command it is TERMINAL_KEY
			{
				iLen  = strlen(pstPosSec->szMACKey);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstPosSec->szMACKey);
					}
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "ENTRY_CODE") == SUCCESS)
		{
			iLen  = strlen(pstPosSec->szEncEntryCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstPosSec->szEncEntryCode);
				}
			}
#if 0 //TODO: Waiting for clarification whether to send plain entry code or not.
			else
			{
				iLen  = strlen(pstPosSec->szPIN);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstPosSec->szPIN);
					}
				}
			}
#endif
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillCardDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillCardDtls(KEYVAL_PTYPE curPtr, CARDDTLS_PTYPE pstCardDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
	char	szMaskedPAN[32] = "";
	char	szCardMode[50]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCardDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Card details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(crdDtlsLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &crdDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "PAYMENT_MEDIA") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->szPymtMedia);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->szPymtMedia);
				}
				curPtr[iCnt].value = tmpPtr;
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMBOSSED_ACCT_NUM") == SUCCESS)
		{
			/*
			 * If we have Embossed account number from the
			 * gateway, we will send in clear
			 */
			iLen = strlen(pstCardDtls->szEmbossedPAN);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->szEmbossedPAN);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "ACCT_NUM") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->szPAN);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(szMaskedPAN, 0x00, sizeof(szMaskedPAN));
					getMaskedPAN(pstCardDtls->szPAN, szMaskedPAN);
					strcpy(tmpPtr, szMaskedPAN);
				}
			}

		}
		else if(strcmp(curPtr[iCnt].key, "CARDHOLDER") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->szName);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->szName);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_EXP_MONTH") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->szClrMon);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->szClrMon);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_EXP_YEAR") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->szClrYear);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->szClrYear);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CVV2_CODE") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->szCVVCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->szCVVCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_ENTRY_MODE") == SUCCESS)
		{
			memset(szCardMode, 0x00, sizeof(szCardMode));

			getCardEntryMode(pstCardDtls, szCardMode);
			iLen = strlen(szCardMode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, szCardMode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PINLESSDEBIT") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->szPINlessDebit);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->szPINlessDebit);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_ABBRV") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->szCardAbbrv);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->szCardAbbrv);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: getCardEntryMode
 *
 * Description	: This Function Fills the Card Details Entry Mode for the Transaction(Swiped/Contactless/Fall Back Swipe/Manual)
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getCardEntryMode(CARDDTLS_PTYPE pstCardDtls, char * pszCardEntryMode)
{
	int rv = SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	if(pstCardDtls != NULL)
	{
		if(isDescriptiveEntryModeEnabled())
		{
			if(pstCardDtls->bManEntry == PAAS_TRUE)
			{
				strcpy(pszCardEntryMode, "Manual");
			}
			else
			{
				switch (pstCardDtls->iCardSrc)
				{
				case CRD_MSR:
					if(isEmvEnabledInDevice() && isServiceCodeCheckInFallbackEnabled())
					{
						if( (strcasecmp(pstCardDtls->szServiceCodeByteOne, "2") == 0) ||
								(strcasecmp(pstCardDtls->szServiceCodeByteOne, "6") == 0)
						)
						{
							strcpy(pszCardEntryMode, "Mag Stripe - Fallback");
						}
						else
						{
							strcpy(pszCardEntryMode, "Mag Stripe - Swipe");
						}
					}
					else
					{
						strcpy(pszCardEntryMode, "Mag Stripe - Swipe");
					}
					break;

				case CRD_EMV_FALLBACK_MSR:
					strcpy(pszCardEntryMode, "Mag Stripe - Fallback");
					break;

				case CRD_NFC:
				case CRD_RFID:
					strcpy(pszCardEntryMode, "Mag Stripe - Contactless");
					break;

				case CRD_EMV_CTLS:
					strcpy(pszCardEntryMode, "Chip Read - Contactless");
					break;

				case CRD_EMV_CT:
					strcpy(pszCardEntryMode, "Chip Read - Contact");
					break;

				case CRD_UNKNWN:
					strcpy(pszCardEntryMode, "Unknown");
					break;

				default:
					debug_sprintf(szDbgMsg, "%s: Card source not available!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		else
		{
			if(pstCardDtls->bManEntry == PAAS_TRUE)
			{
				strcpy(pszCardEntryMode, "Keyed");
			}
			else
			{
				switch (pstCardDtls->iCardSrc)
				{
				case CRD_MSR:
					if(isEmvEnabledInDevice() && isServiceCodeCheckInFallbackEnabled())
					{
						if( (strcasecmp(pstCardDtls->szServiceCodeByteOne, "2") == 0) ||
								(strcasecmp(pstCardDtls->szServiceCodeByteOne, "6") == 0)
						)
						{
							strcpy(pszCardEntryMode, "FSwipe");
						}
						else
						{
							strcpy(pszCardEntryMode, "Swiped");
						}
					}
					else
					{
						strcpy(pszCardEntryMode, "Swiped");
					}
					break;

				case CRD_EMV_FALLBACK_MSR:
					strcpy(pszCardEntryMode, "FSwipe");
					break;

				case CRD_NFC:
				case CRD_RFID:
				case CRD_EMV_CTLS:
					strcpy(pszCardEntryMode, "Contactless");
					break;

				case CRD_EMV_CT:
					strcpy(pszCardEntryMode, "Chip Read");
					break;

				case CRD_UNKNWN:
					strcpy(pszCardEntryMode, "Unknown");
					break;

				default:
					debug_sprintf(szDbgMsg, "%s: Card source not available!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Card Details not available!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillEMVDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillEMVDtls(KEYVAL_PTYPE curPtr, CARDDTLS_PTYPE pstCardDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	int		iTemp			= 0;
	char	cTmp 			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCardDtls == NULL || pstCardDtls->bEmvData == PAAS_FALSE)
	{
		debug_sprintf(szDbgMsg, "%s: No EMV details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(EMVTagsForSCI) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &EMVTagsForSCI[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "EMV_TAG_4F") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvAppDtls.szAID);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvAppDtls.szAID);
				}
				curPtr[iCnt].value = tmpPtr;
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_50") == SUCCESS)
		{
			/*
			 * If we have Embossed account number from the
			 * gateway, we will send in clear
			 */
			iLen = strlen(pstCardDtls->stEmvAppDtls.szAppLabel);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvAppDtls.szAppLabel);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_5F2A") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmCurrCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvCryptgrmDtls.szCryptgrmCurrCode);
				}
			}

		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_5F34") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvAppDtls.szAppSeqNum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvAppDtls.szAppSeqNum);
				}
			}
			else
			{
				//Blank if not present as in FirstData document sec 12.2
				tmpPtr = (char *) malloc(1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, "");
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_82") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvAppDtls.szAppIntrChngProfile);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvAppDtls.szAppIntrChngProfile);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_8A") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvtranDlts.szAuthRespCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvtranDlts.szAuthRespCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_95") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvTerDtls.szTermVerificationResults);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvTerDtls.szTermVerificationResults);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9A") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvtranDlts.szTermTranDate);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvtranDlts.szTermTranDate);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9B") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvtranDlts.szTranStatusInfo);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvtranDlts.szTranStatusInfo);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9C") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmTranType);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvCryptgrmDtls.szCryptgrmTranType);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F02") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvtranDlts.szAuthAmnt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1 + 1);//one extra for decimal
				if(tmpPtr != NULL)
				{
					iTemp = 0;
					iTemp = atoi(pstCardDtls->stEmvtranDlts.szAuthAmnt);
					sprintf(tmpPtr, "%d.%.2d", (iTemp)/100, (iTemp)%100);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F03") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvtranDlts.szEMVCashBackAmnt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1 + 1);//one extra for decimal
				if(tmpPtr != NULL)
				{
					iTemp = 0;
					iTemp = atoi(pstCardDtls->stEmvtranDlts.szEMVCashBackAmnt);
					sprintf(tmpPtr, "%d.%.2d", (iTemp)/100, (iTemp)%100);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F07") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvAppDtls.szAppUsageControl);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvAppDtls.szAppUsageControl);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F0D") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvIssuerDtls.szIACDefault);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvIssuerDtls.szIACDefault);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F0E") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvIssuerDtls.szIACDenial);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvIssuerDtls.szIACDenial);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F0F") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvIssuerDtls.szIACOnline);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvIssuerDtls.szIACOnline);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F10") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvIssuerDtls.szIssuerAppData);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvIssuerDtls.szIssuerAppData);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F12") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvAppDtls.szAppPrefName);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvAppDtls.szAppPrefName);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F1A") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvtranDlts.szTermTranCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvtranDlts.szTermTranCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F26") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvCryptgrmDtls.szAppCryptgrm);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvCryptgrmDtls.szAppCryptgrm);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F27") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F34") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvtranDlts.szCVMResult);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvtranDlts.szCVMResult);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F36") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvAppDtls.szAppTranCounter);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvAppDtls.szAppTranCounter);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F37") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvCryptgrmDtls.szUnprdctbleNum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvCryptgrmDtls.szUnprdctbleNum);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_REVERSAL_TYPE") == SUCCESS) //Praveen_P1: 19 July, Adding this field to send in SCI response
		{
			iLen = strlen(pstCardDtls->szEmvReversalType);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->szEmvReversalType);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_MODE") == SUCCESS)
		{
			if(pstCardDtls->bPartialEMV == PAAS_FALSE && strlen(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData) > 0)
			{
				//(EMV CTLS and Online Req) or Host Declined or (Host Approved and Card Approved) => ISSUER
				if( 	( (pstCardDtls->iCardSrc == CRD_EMV_CTLS) && (strcmp(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData, "80") == 0) )
						||
						( pstCardDtls->stEmvtranDlts.iHostStatus == ERR_PWC_RSLTCODE_DECLINED)
						||
						(	(pstCardDtls->stEmvtranDlts.iHostStatus == SUCCESS_PWC_RSLTCODE_CAPTURED || pstCardDtls->stEmvtranDlts.iHostStatus == SUCCESS_PWC_RSLTCODE_APPROVED)
								&&
								(strcmp(pstCardDtls->stEmvCryptgrmDtls.szCryptgrmInfoData, "40") == 0)
						)
				)
				{
					tmpPtr = (char *) malloc(6 + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "ISSUER");
					}
				}
				else
				{
					tmpPtr = (char *) malloc(4 + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "CARD");
					}
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_CVM") == SUCCESS)
		{
			if(strlen(pstCardDtls->stEmvtranDlts.szCVMResult) > 0 )
			{
				cTmp = pstCardDtls->stEmvtranDlts.szCVMResult[1];

				//Setting NONE for Partial EMV
				if(isPartialEmvAllowed() && pstCardDtls->bPartialEMV)
				{
					cTmp = 0;
				}

				if(cTmp == 'E')
				{
					tmpPtr = (char *) malloc(9 + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "SIGNATURE");
					}
				}
				else if(cTmp == '1' || cTmp == '2' || cTmp == '4')
				{
					tmpPtr = (char *) malloc(3 + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "PIN");
					}
				}
				else if(cTmp == '3' || cTmp == '5')
				{
					tmpPtr = (char *) malloc(17 + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "PIN AND SIGNATURE");
					}
				}
				else
				{
					tmpPtr = (char *) malloc(4 + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "NONE");
					}
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_CHIP_INDICATOR") == SUCCESS)
		{
			if(pstCardDtls->bManEntry == PAAS_TRUE)
			{
				tmpPtr = (char *) malloc(5 + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, "KEYED");
				}
			}
			else
			{
				switch (pstCardDtls->iCardSrc)
				{
				case CRD_MSR:
					//Change related to serivice code Byte One 
					if(isEmvEnabledInDevice() && isServiceCodeCheckInFallbackEnabled())
					{
						if( (strcasecmp(pstCardDtls->szServiceCodeByteOne, "2") == 0) ||
								(strcasecmp(pstCardDtls->szServiceCodeByteOne, "6") == 0)
						)
						{
							tmpPtr = (char *) malloc(14 + 1);
							if(tmpPtr != NULL)
							{
								strcpy(tmpPtr, "FALLBACK SWIPED");
							}
						}
						else
						{
							tmpPtr = (char *) malloc(6 + 1);
							if(tmpPtr != NULL)
							{
								strcpy(tmpPtr, "SWIPED");
							}
						}
					}
					else
					{
						tmpPtr = (char *) malloc(6 + 1);
						if(tmpPtr != NULL)
						{
							strcpy(tmpPtr, "SWIPED");
						}
					}
					break;

				case CRD_EMV_FALLBACK_MSR:
					tmpPtr = (char *) malloc(14 + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "FALLBACK SWIPE");
					}
					break;

				case CRD_NFC:
				case CRD_RFID:
				case CRD_EMV_CTLS:
					tmpPtr = (char *) malloc(11 + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "CONTACTLESS");
					}
					break;

				case CRD_EMV_CT:
					tmpPtr = (char *) malloc(7 + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "CONTACT");
					}
					break;

				case CRD_UNKNWN:
					tmpPtr = (char *) malloc(7 + 1);
					if(tmpPtr != NULL)
					{
						strcpy(tmpPtr, "UNKNOWN");
					}
					break;

				default:
					debug_sprintf(szDbgMsg, "%s: Card source not available!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TAC_DEFAULT") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvTerDtls.szTACDefault);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvTerDtls.szTACDefault);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TAC_DENIAL") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvTerDtls.szTACDenial);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvTerDtls.szTACDenial);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TAC_ONLINE") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvTerDtls.szTACOnline);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvTerDtls.szTACOnline);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "ISSUER_SCRIPT_RESULTS") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvIssuerDtls.szIssueScrptResults);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvIssuerDtls.szIssueScrptResults);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_84") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvtranDlts.szDedicatedFileName);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvtranDlts.szDedicatedFileName);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F21") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvtranDlts.szTranTime);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvtranDlts.szTranTime);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F08") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvAppDtls.szICCAppVersNum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvAppDtls.szICCAppVersNum);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F09") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvAppDtls.szAppVersNum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvAppDtls.szAppVersNum);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F33") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvTerDtls.szTermCapabilityProf);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvTerDtls.szTermCapabilityProf);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F35") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvTerDtls.szTermType);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvTerDtls.szTermType);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMV_TAG_9F11") == SUCCESS)
		{
			iLen = strlen(pstCardDtls->stEmvIssuerDtls.szIssuerCodeTableIndex);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstCardDtls->stEmvIssuerDtls.szIssuerCodeTableIndex);
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}
/*
 * ============================================================================
 * Function Name: fillSurvey5Dtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillSurvey5Dtls(KEYVAL_PTYPE curPtr, SURVEYDTLS_PTYPE pstSurveyDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSurveyDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Survey details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stSurvey5Lst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stSurvey5Lst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "SURVEY5_DATA") == SUCCESS)
		{
			if((iLen = strlen(pstSurveyDtls->szResultData)) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSurveyDtls->szResultData);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillSurvey10Dtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillSurvey10Dtls(KEYVAL_PTYPE curPtr, SURVEYDTLS_PTYPE pstSurveyDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSurveyDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Survey details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stSurvey10Lst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stSurvey10Lst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "SURVEY10_DATA") == SUCCESS)
		{
			if((iLen = strlen(pstSurveyDtls->szResultData)) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSurveyDtls->szResultData);
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillPaymentTypesDtls
 *
 * Description	: Fills the Payment Types Enabled
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillPaymentTypesDtls(KEYVAL_PTYPE curPtr, PAYMENTTYPES_PTYPE pstPymtTypesDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstPymtTypesDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Payment Types details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stPaymentTypesLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stPaymentTypesLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "PAYMENT_TYPES") == SUCCESS)
		{
			if((iLen = strlen(pstPymtTypesDtls->szPaymentTypes)) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstPymtTypesDtls->szPaymentTypes);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillDevNameDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillDevNameDtls(KEYVAL_PTYPE curPtr, DEVICENAME_PTYPE pstDevNameDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstDevNameDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Device name details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stGetDevNameLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stGetDevNameLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "DEVICENAME") == SUCCESS)
		{
			if((iLen = strlen(pstDevNameDtls->szDeviceName)) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDevNameDtls->szDeviceName);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "MAC_LABELS") == SUCCESS)
		{
			iLen = strlen(pstDevNameDtls->szMacLabels);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstDevNameDtls->szMacLabels);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SERIAL_NUMBER") == SUCCESS)
		{
			iLen = strlen(pstDevNameDtls->szSerialNum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					strcpy(tmpPtr, pstDevNameDtls->szSerialNum);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillEmpIDDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillEmpIDDtls(KEYVAL_PTYPE curPtr, EMPID_PTYPE pstEmpIDDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstEmpIDDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Employee ID details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stEmpIDLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stEmpIDLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "EMPLOYEEID_DATA") == SUCCESS)
		{
			if((iLen = strlen(pstEmpIDDtls->szEmpID)) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstEmpIDDtls->szEmpID);
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillCharityDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillCharityDtls(KEYVAL_PTYPE curPtr, CHARITYDTLS_PTYPE pstCharityDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCharityDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Charity details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stSurvey10Lst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stCharityLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "AMOUNT_DATA") == SUCCESS)
		{
			if((iLen = strlen(pstCharityDtls->szResultData)) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCharityDtls->szResultData);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
* ============================================================================
* Function Name: fillGetCardDtls
*
* Description  :Fill Card Details into the Structure.
*
* Input Params :KEYVAL_PTYPE curPtr , GET_CARDDATA_PTYPE structure.
*
* Output Params:
* ============================================================================
*/
static int fillGetCardDtls(KEYVAL_PTYPE curPtr, GET_CARDDATA_PTYPE pstGetCardDataDtls)
{
	int           	iCnt                	= 0;
	int           	iTotCnt              	= 0;
	int           	iLen                 	= 0;
	char		 	szCardMode[50]			="";
	char* 			tmpPtr             		= NULL;
#ifdef DEBUG
       char   szDbgMsg[256] = "";
#endif

       debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
       APP_TRACE(szDbgMsg);

       iTotCnt = sizeof(stRawCrdDtlsLst) / KEYVAL_SIZE;

       for(iCnt = 0; iCnt < iTotCnt; iCnt++)
       {
              memcpy(&curPtr[iCnt], &stRawCrdDtlsLst[iCnt], KEYVAL_SIZE);

              if(strcmp(curPtr[iCnt].key, "CARD_TRACK1") == SUCCESS)
              {
                     iLen = strlen(pstGetCardDataDtls->szTrack1Data);
                     if(iLen > 0)
                     {
                           tmpPtr = (char *) malloc(iLen + 1);
                           if(tmpPtr != NULL)
                           {
                                  memset(tmpPtr, 0x00, iLen + 1);
                                  strcpy(tmpPtr, pstGetCardDataDtls->szTrack1Data);
                           }
                     }
              }
              else if(strcmp(curPtr[iCnt].key, "CARD_TRACK2") == SUCCESS)
              {
                     iLen = strlen(pstGetCardDataDtls->szTrack2Data);
                     if(iLen > 0)
                     {
                           tmpPtr = (char *) malloc(iLen + 1);
                           if(tmpPtr != NULL)
                           {
                                  memset(tmpPtr, 0x00, iLen + 1);
                                  strcpy(tmpPtr, pstGetCardDataDtls->szTrack2Data);
                           }
                     }
              }
              else if(strcmp(curPtr[iCnt].key, "CARD_TRACK3") == SUCCESS)
              {
                     iLen = strlen(pstGetCardDataDtls->szTrack3Data);
                     if(iLen > 0)
                     {
                           tmpPtr = (char *) malloc(iLen + 1);
                           if(tmpPtr != NULL)
                           {
                                  memset(tmpPtr, 0x00, iLen + 1);
                                  strcpy(tmpPtr, pstGetCardDataDtls->szTrack3Data);
                           }
                     }
              }
              else if(strcmp(curPtr[iCnt].key, "ACCT_NUM") == SUCCESS)
              {
                     iLen = strlen(pstGetCardDataDtls->szPAN);
                     if(iLen > 0)
                     {
                           tmpPtr = (char *) malloc(iLen + 1);
                           if(tmpPtr != NULL)
                           {
                                  memset(tmpPtr, 0x00, iLen + 1);
                                  strcpy(tmpPtr, pstGetCardDataDtls->szPAN);
                           }
                     }
              }
              else if(strcmp(curPtr[iCnt].key, "CARD_EXP_MONTH") == SUCCESS)
              {
            	  iLen = strlen(pstGetCardDataDtls->szClrMon);
            	  if(iLen > 0)
            	  {
            		  tmpPtr = (char *) malloc(iLen + 1);
            		  if(tmpPtr != NULL)
            		  {
            			  memset(tmpPtr, 0x00, iLen + 1);
            			  strcpy(tmpPtr, pstGetCardDataDtls->szClrMon);
            		  }
            	  }
              }
              else if(strcmp(curPtr[iCnt].key, "CARD_EXP_YEAR") == SUCCESS)
              {
            	  iLen = strlen(pstGetCardDataDtls->szClrYear);
            	  if(iLen > 0)
            	  {
            		  tmpPtr = (char *) malloc(iLen + 1);
            		  if(tmpPtr != NULL)
            		  {
            			  memset(tmpPtr, 0x00, iLen + 1);
            			  strcpy(tmpPtr, pstGetCardDataDtls->szClrYear);
            		  }
            	  }
              }
              else if(strcmp(curPtr[iCnt].key, "CARDHOLDER") == SUCCESS)
              {
            	  iLen = strlen(pstGetCardDataDtls->szName);
            	  if(iLen > 0)
            	  {
            		  tmpPtr = (char *) malloc(iLen + 1);
            		  if(tmpPtr != NULL)
            		  {
            			  memset(tmpPtr, 0x00, iLen + 1);
            			  strcpy(tmpPtr, pstGetCardDataDtls->szName);
            		  }
            	  }
              }
              else if(strcmp(curPtr[iCnt].key, "CARD_ENTRY_MODE") == SUCCESS)
              {
            	  if(pstGetCardDataDtls->iCardEntryMode)
            	  {
            					  switch (pstGetCardDataDtls->iCardEntryMode)
            					  {
            					  case CRD_MSR:
//            						  if(isEmvEnabledInDevice() && isServiceCodeCheckInFallbackEnabled())
//            						  {
//            							  if( (strcasecmp(pstCardDtls->szServiceCodeByteOne, "2") == 0) ||
//            									  (strcasecmp(pstCardDtls->szServiceCodeByteOne, "6") == 0)
//            							  )
//            							  {
//            								  strcpy(tmpPtr, "Mag Stripe - Fallback");
//            							  }
//            							  else
//            							  {
//            								  strcpy(tmpPtr, "Mag Stripe - Swipe");
//            							  }
//            						  }
            						  //else
            						 // {
            							  strcpy(szCardMode, "Mag Stripe - Swipe");
            						  break;

            					  case CRD_EMV_FALLBACK_MSR:
            							  strcpy(szCardMode, "Mag Stripe - Fallback");
            						  break;

            					  case CRD_NFC:
            					  case CRD_RFID:
            							  strcpy(szCardMode, "Mag Stripe - Contactless");

            						  break;

            					  case CRD_EMV_CTLS:

            							  strcpy(szCardMode, "Chip Read - Contactless");
            						  break;

            					  case CRD_EMV_CT:

            							  strcpy(szCardMode, "Chip Read - Contact");
            						  break;

            					  case CRD_UNKNWN:
            							  strcpy(szCardMode, "Unknown");
            						  break;

            					  default:
            						  debug_sprintf(szDbgMsg, "%s: Card source not available!!!", __FUNCTION__);
            						  APP_TRACE(szDbgMsg);
            					  }
            					  iLen = strlen(szCardMode);
            					  if(iLen > 0)
            					  {
            						  tmpPtr = (char *) malloc(iLen + 1);
            						  if(tmpPtr != NULL)
            						  {
            							  strcpy(tmpPtr, szCardMode);
            						  }
            					  }
            	  }
              }
              curPtr[iCnt].value = tmpPtr;
              tmpPtr = NULL;
       }

       debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
       APP_TRACE(szDbgMsg);

       return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillPassThrgResDtls
 *
 * Description	: Fills the Pass Through fields which has to be sent to the SCI
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillPassThrgResDtls(KEYVAL_PTYPE curPtr, PASSTHRG_FIELDS_PTYPE pstPassThrghDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	void *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstPassThrghDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	iTotCnt = pstPassThrghDtls->iResTagsCnt;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		curPtr[iCnt].key = pstPassThrghDtls->pstResTagList[iCnt].key;	// just assigning the key location, because we never free the key memory from curPtr
		curPtr[iCnt].isMand = pstPassThrghDtls->pstResTagList[iCnt].isMand;
		curPtr[iCnt].valType = pstPassThrghDtls->pstResTagList[iCnt].valType;

		if(pstPassThrghDtls->pstResTagList[iCnt].value == NULL)
		{
			continue;
		}
		iLen = strlen((char*)pstPassThrghDtls->pstResTagList[iCnt].value);
		if(iLen > 0)
		{
			tmpPtr = (char*) malloc(iLen + 1);
			if(tmpPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				iCnt = FAILURE;
				break;
			}
			memset(tmpPtr, 0x00, iLen + 1);
			memcpy(tmpPtr, (char*)pstPassThrghDtls->pstResTagList[iCnt].value, iLen);
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

#if 0
	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		debug_sprintf(szDbgMsg, "%s: key[%s] value[%s]", __FUNCTION__, curPtr[iCnt].key, (char*)(curPtr[iCnt].value));
		APP_TRACE(szDbgMsg);
	}
#endif
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);
	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillCustomerButtonDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillCustomerButtonDtls(KEYVAL_PTYPE curPtr, CUSTBUTTONDTLS_PTYPE pstCustButtonDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCustButtonDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Customer Button details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stCustButtonLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stCustButtonLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "CUST_BUTTON_DATA") == SUCCESS)
		{
			if((iLen = strlen(pstCustButtonDtls->szResultData)) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCustButtonDtls->szResultData);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillCreditAppDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillCreditAppDtls(KEYVAL_PTYPE curPtr, CREDITAPPDTLS_PTYPE pstCreditAppDtls)
{
	int		iCnt					= 0;
	int		iPromptCnt				= 0;
	int		iTotCnt					= 0;
	int		iLen					= 0;
	char *	tmpPtr					= NULL;
	char	szPromptTxt[50]			= "";
	char	szPromptDataTxt[50]		= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCreditAppDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Credit Application Details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stCreditAppLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stCreditAppLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "PROXY_RESULT") == SUCCESS)
		{
			if((iLen = strlen(pstCreditAppDtls->szProxyResult)) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCreditAppDtls->szProxyResult);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PROXY_REF") == SUCCESS)
		{
			if((iLen = strlen(pstCreditAppDtls->szProxyRef)) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCreditAppDtls->szProxyRef);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CREDITAPP_PAYLOAD") == SUCCESS)
		{
			if((iLen = strlen(pstCreditAppDtls->szCreditAppPayLoad)) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCreditAppDtls->szCreditAppPayLoad);
				}
			}
		}
		else if(strlen(pstCreditAppDtls->szForwardProxy) == 0)
		{
			/* T_RaghavendranR1_13102016: AMDOCS CASE 161011-13045 If the PROXY_URL is present in the SCI request,
			 * We will not send the captured Prompt data back to POS in SCI response, no matter whatever is the SSI response from proxy (SUCCESS/FAILURE/CONN TIMEOUT/RESPONSE TIMEOUT)*/

			/* Send Back the Prompt Data obtained from the user back to POS in case the response from the
			 * forward proxy is not successful (i.e incase of Failure or timeout responses from proxy)
			 * */
			for(iPromptCnt = 1; iPromptCnt <= CREDIT_APP_MAXPROMPTS; iPromptCnt++ )
			{
				memset(szPromptTxt, 0x00, sizeof(szPromptTxt));
				memset(szPromptDataTxt, 0x00, sizeof(szPromptDataTxt));

				sprintf(szPromptTxt, "PROMPT%d", iPromptCnt);
				sprintf(szPromptDataTxt, "PROMPT%d_DATA", iPromptCnt);

				if(strcmp(curPtr[iCnt].key, szPromptTxt) == SUCCESS)
				{
					if((iLen = strlen(pstCreditAppDtls->stPromptDtls[iPromptCnt - 1].szPromptText)) > 0 )
					{
						tmpPtr = (char *) malloc(iLen + 1);
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, pstCreditAppDtls->stPromptDtls[iPromptCnt - 1].szPromptText);
						}
					}
				}
				else if(strcmp(curPtr[iCnt].key, szPromptDataTxt) == SUCCESS)
				{
					if((iLen = strlen(pstCreditAppDtls->stPromptDtls[iPromptCnt - 1].szPromptDataWithLiterals)) > 0 )
					{
						tmpPtr = (char *) malloc(iLen + 1);
						if(tmpPtr != NULL)
						{
							memset(tmpPtr, 0x00, iLen + 1);
							strcpy(tmpPtr, pstCreditAppDtls->stPromptDtls[iPromptCnt - 1].szPromptDataWithLiterals);
						}
					}
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillCustQuesDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillCustQuesDtls(KEYVAL_PTYPE curPtr, SURVEYDTLS_PTYPE pstSurveyDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSurveyDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Survey details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stCustQuesLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stCustQuesLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "CUST_QUESTION_DATA") == SUCCESS)
		{
			if((iLen = strlen(pstSurveyDtls->szResultData)) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSurveyDtls->szResultData);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillSignDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillSignDtls(KEYVAL_PTYPE curPtr, SIGDTLS_PTYPE pstSigDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstSigDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Sign details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stSignLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stSignLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "SIGNATUREDATA") == SUCCESS)
		{
			if(	(pstSigDtls->szSign != NULL) &&
				((iLen = strlen(pstSigDtls->szSign)) > 0) )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSigDtls->szSign);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "MIME_TYPE") == SUCCESS)
		{
			iLen = strlen(pstSigDtls->szMime);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstSigDtls->szMime);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillVersionDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillVersionDtls(KEYVAL_PTYPE curPtr, DEVTRAN_PTYPE pstDevTran)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stVersionLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stVersionLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "VERSION_INFO") == SUCCESS)
		{
			iLen = strlen(pstDevTran->szData);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDevTran->szData);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*KranthiK1: TODO Remove it Later*/
#if 0
/*
 * ============================================================================
 * Function Name: fillCounterDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillCounterDtls(KEYVAL_PTYPE curPtr, COUNTERDTLS_PTYPE pstCounterDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stGetCounterLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stGetCounterLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "COUNTER") == SUCCESS)
		{
			iLen = strlen(pstCounterDtls->szCounterVal);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCounterDtls->szCounterVal);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}
#endif
/*
 * ============================================================================
 * Function Name: fillScndData
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillScndData(KEYVAL_PTYPE curPtr)
{
	int		iCnt					= 0;
	int		iTotCnt					= 0;
	int		iLen					= 0;
	int		iStatus					= -1;
	int		iDetailedStatus			= -1;
	char	szStatus[10]			= "";
	char	szDetailedStatus[10]	= "";
	char *	tmpPtr					= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iStatus = getScndData();
	iDetailedStatus = getScndDetailedStatus();

	sprintf(szStatus, "%d", iStatus);
	sprintf(szDetailedStatus, "%d", iDetailedStatus);

	iTotCnt = sizeof(stScndLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stScndLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "SECONDARY_DATA") == SUCCESS)
		{
			iLen = strlen(szStatus);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szStatus);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DETAILED_STATUS") == SUCCESS)
		{
			iLen = strlen(szDetailedStatus);
			if(iLen > 0 && (iDetailedStatus != -1))
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, szDetailedStatus);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillStatusDetails
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillStatusDetails(KEYVAL_PTYPE curPtr)
{
	int				 iCnt			= 0;
	int				 iTotCnt			= 0;
	int				 iLen			= 0;
	char *			 tmpPtr			= NULL;
	STATUSDTLS_STYPE stStatusDtls;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(&stStatusDtls, 0x00, sizeof(STATUSDTLS_STYPE));
	getStatusDtlsFromSession(&stStatusDtls);

	iTotCnt = sizeof(stStatusLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stStatusLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "MACLABEL_IN_SESSION") == SUCCESS)
		{
			iLen = strlen(stStatusDtls.szSessMacLbl);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, stStatusDtls.szSessMacLbl);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SESSION_DURATION") == SUCCESS)
		{
			iLen = strlen(stStatusDtls.szSessDuration);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, stStatusDtls.szSessDuration);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "INVOICE_SESSION") == SUCCESS)
		{
			iLen = strlen(stStatusDtls.szInvoice);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, stStatusDtls.szInvoice);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DEVICENAME") == SUCCESS)
		{
			iLen = strlen(stStatusDtls.szDeviceName);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, stStatusDtls.szDeviceName);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SERIAL_NUMBER") == SUCCESS)
		{
			iLen = strlen(stStatusDtls.szSerialNum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, stStatusDtls.szSerialNum);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillTimeDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillTimeDtls(KEYVAL_PTYPE curPtr, TIMEDTLS_PTYPE pstTimeDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stTimeLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stTimeLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "TIME") == SUCCESS)
		{
			iLen = strlen(pstTimeDtls->szTimeTxt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstTimeDtls->szTimeTxt);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillEmailDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillEmailDtls(KEYVAL_PTYPE curPtr, char *pszEmail)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stEmailLst) / KEYVAL_SIZE;

	if(pszEmail == NULL)	// CID-67292: 3-Feb-16: MukeshS3: Adding NULL checking for input parameters
	{
		debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return FAILURE;
	}

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stEmailLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "EMAILDATA") == SUCCESS)
		{
			iLen = strlen(pszEmail);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pszEmail);
				}

			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillStandAloneLtyDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillStandAloneLtyDtls(KEYVAL_PTYPE curPtr, LTYDTLS_PTYPE pstLtyDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstLtyDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Loyalty details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stSALtyLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stSALtyLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "LOYALTY_DATA") == SUCCESS) //Phone or Swiped consumer input
		{
			iLen = strlen(pstLtyDtls->szPAN);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLtyDtls->szPAN);
				}
			}
			else
			{
				iLen = strlen(pstLtyDtls->szPh);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstLtyDtls->szPh);
					}
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillConsOptDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillConsOptDtls(KEYVAL_PTYPE curPtr, UNSOLMSGINFO_PTYPE pstUnsolMsgInfo, int iIncCardToken, int iIncTokenSource)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstUnsolMsgInfo == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Consumer option details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stConsOptDtlsLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stConsOptDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "EMAIL_RECEIPT") == SUCCESS)
		{
			if(pstUnsolMsgInfo->iEmailReceipt == 1)
			{
				tmpPtr = (char *) malloc(1 + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, 1 + 1);
					strcpy(tmpPtr, "1");
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "GIFT_RECEIPT") == SUCCESS)
		{
			if(pstUnsolMsgInfo->iGiftReceipt == 1)
			{
				tmpPtr = (char *) malloc(1 + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, 1 + 1);
					strcpy(tmpPtr, "1");
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "EMAIL_OFFER") == SUCCESS)
		{
			if(pstUnsolMsgInfo->iEmailOffer == 1)
			{
				tmpPtr = (char *) malloc(1 + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, 1 + 1);
					strcpy(tmpPtr, "1");
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_TOKEN") == SUCCESS)
		{
			iLen = strlen(pstUnsolMsgInfo->szCardToken);
			if(iLen > 0 && iIncCardToken == 1)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstUnsolMsgInfo->szCardToken);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PRESWIPED") == SUCCESS)
		{
			if(pstUnsolMsgInfo->iPreSwiped == 1)
			{
				tmpPtr = (char *) malloc(1 + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, 1 + 1);
					strcpy(tmpPtr, "1");
				}
			}
			else if(pstUnsolMsgInfo->iPreSwiped == 2)
			{
				tmpPtr = (char *) malloc(1 + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, 1 + 1);
					strcpy(tmpPtr, "0");
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "LOYALTY_VAS") == SUCCESS)
		{
			if(pstUnsolMsgInfo->iVasLoyalty == 1)
			{
				tmpPtr = (char *) malloc(1 + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, 1 + 1);
					strcpy(tmpPtr, "1");
				}
			}
			else if(pstUnsolMsgInfo->iVasLoyalty == 2)
			{
				tmpPtr = (char *) malloc(1 + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, 1 + 1);
					strcpy(tmpPtr, "0");
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PAY_WITH_PAYPAL") == SUCCESS)
		{
			if(pstUnsolMsgInfo->iPayWithPaypal == 1)
			{
				tmpPtr = (char *) malloc(1 + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, 1 + 1);
					strcpy(tmpPtr, "1");
				}
			}
		}
#if 0
		else if(strcmp(curPtr[iCnt].key, "TOKEN_SOURCE") == SUCCESS)
		{
			iLen = strlen(pstUnsolMsgInfo->szTokenSource);
			if(iLen > 0 && iIncTokenSource == 1)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstUnsolMsgInfo->szTokenSource);
				}
			}
		}
#endif
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillLtyDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillLtyDtls(KEYVAL_PTYPE curPtr, LTYDTLS_PTYPE pstLtyDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstLtyDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Loyalty details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stLtyLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stLtyLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "LTY_ACCT_NUM") == SUCCESS)
		{
			iLen = strlen(pstLtyDtls->szPAN);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLtyDtls->szPAN);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "LTY_PHONE") == SUCCESS)
		{
			iLen = strlen(pstLtyDtls->szPh);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLtyDtls->szPh);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "LTY_EMAIL") == SUCCESS)
		{
			iLen = strlen(pstLtyDtls->szEmail);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLtyDtls->szEmail);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "LTY_TOKEN_ID") == SUCCESS)
		{
			iLen = strlen(pstLtyDtls->szToken);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLtyDtls->szToken);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "REWARD_RECEIPT_TEXT") == SUCCESS)
		{
			iLen = strlen(pstLtyDtls->szRewardTxt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLtyDtls->szRewardTxt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "REWARD_ID") == SUCCESS)
		{
			iLen = strlen(pstLtyDtls->szRewardId);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLtyDtls->szRewardId);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "REWARD_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstLtyDtls->szRewardAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstLtyDtls->szRewardAmt);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillDupDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillDupDtls(KEYVAL_PTYPE curPtr, DUPFIELDDTLS_PTYPE pstDupDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstDupDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Duplicate details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(dupDtlsLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &dupDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "DUP_CTROUTD") == SUCCESS)
		{
			iLen = strlen(pstDupDtls->szDupCTroutd);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDupDtls->szDupCTroutd);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DUP_AUTH_CODE") == SUCCESS)
		{
			iLen = strlen(pstDupDtls->szDupAuthCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDupDtls->szDupAuthCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DUP_ACCT_NUM") == SUCCESS)
		{
			iLen = strlen(pstDupDtls->szDupAccNum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDupDtls->szDupAccNum);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DUP_PAYMENT_MEDIA") == SUCCESS)
		{
			iLen = strlen(pstDupDtls->szDupPymtMedia);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDupDtls->szDupPymtMedia);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DUP_TROUTD") == SUCCESS)
		{
			iLen = strlen(pstDupDtls->szDupTroutd);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDupDtls->szDupTroutd);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DUP_INVOICE") == SUCCESS)
		{
			iLen = strlen(pstDupDtls->szDupInvoice);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDupDtls->szDupInvoice);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DUP_TRANS_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstDupDtls->szDupTranAmount);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDupDtls->szDupTranAmount);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DUP_TRANS_DATE") == SUCCESS)
		{
			iLen = strlen(pstDupDtls->szDupTransDate);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDupDtls->szDupTransDate);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DUP_TRANS_TIME") == SUCCESS)
		{
			iLen = strlen(pstDupDtls->szDupTransTime);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDupDtls->szDupTransTime);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DUP_AVS_CODE") == SUCCESS)
		{
			iLen = strlen(pstDupDtls->szDupAVSCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDupDtls->szDupAVSCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DUP_CVV2_CODE") == SUCCESS)
		{
			iLen = strlen(pstDupDtls->szDupCVV2);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDupDtls->szDupCVV2);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillCTranDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillCTranDtls(KEYVAL_PTYPE curPtr, CTRANDTLS_PTYPE pstCTranDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCTranDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Current Tran details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(cTranDtlsLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &cTranDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "TRANS_SEQ_NUM") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szTranSeq);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szTranSeq);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "INTRN_SEQ_NUM") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szTranId);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szTranId);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "AUTH_CODE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szAuthCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szAuthCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TROUTD") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szTroutd);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szTroutd);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CTROUTD") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szCTroutd);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szCTroutd);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "LPTOKEN") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szLPToken);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szLPToken);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PAYMENT_TYPE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szPymntType);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szPymntType);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_TOKEN") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szCardToken);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szCardToken);
				}
			}
		}
#if 0
		else if(strcmp(curPtr[iCnt].key, "TOKEN_SOURCE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szTokenSource);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szTokenSource);
				}
			}
		}
#endif
		else if(strcmp(curPtr[iCnt].key, "TRACE_NUM") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szTraceNum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szTraceNum);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "RETURN_CHECK_FEE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szRtnCheckFee);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szRtnCheckFee);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "RETURN_CHECK_NOTE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szRtnCheckNote);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szRtnCheckNote);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CHECK_NUM") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szCheckNum);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szCheckNum);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "BANK_USERDATA") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szBankUserData);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szBankUserData);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "AUTH_RESP_CODE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szAuthRespCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szAuthRespCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TXN_POSENTRYMODE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szTxnPosEntryMode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szTxnPosEntryMode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "MERCHID") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szMerchId);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szMerchId);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TERMID") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szTermId);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szTermId);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "LANE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szLaneId);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szLaneId);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "STORE_NUM") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szStoreId);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szStoreId);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SAF_NUM") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szSAFNo);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szSAFNo);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SAF_TOR_NUM") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szSAFTORNo);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szSAFTORNo);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "BATCH_TRACE_ID") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szBatchTraceId);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szBatchTraceId);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PAYPAL_CHECKID_KEY") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szPaypalCheckIDKey);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szPaypalCheckIDKey);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TRANS_DATE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szTransDate);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szTransDate);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TRANS_TIME") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szTransTime);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szTransTime);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "AVS_CODE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szAvsCode);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szAvsCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CMRCL_FLAG") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szCmrclFlag);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szCmrclFlag);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PROCESSOR") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szProcessor);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szProcessor);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PPCV") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szPPCV);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szPPCV);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CREDIT_PLAN_NBR") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szCreditPlanNbr);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szCreditPlanNbr);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SVC_PHONE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szSvcPhone);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szSvcPhone);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "STATUS_FLAG") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szStatusFlag);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szStatusFlag);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "ACTION_CODE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szActionCode);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szActionCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "APR_TYPE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szAprType);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szAprType);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PURCHASE_APR") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szPurchaseApr);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szPurchaseApr);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "RECEIPT_TEXT") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szReceiptText);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szReceiptText);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "REF_NUMBER") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szReferenceNum);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szReferenceNum);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "VALIDATION_CODE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szValidtionCode);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szValidtionCode);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "VISA_IDENTIFIER") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szVisaIdentifier);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szVisaIdentifier);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DENIAL_REC_NUM") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szDenialRecNum);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szDenialRecNum);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "SIGNATUREREF") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szSignatureRef);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szSignatureRef);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "AUTHNWID") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szAthNtwID);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szAthNtwID);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "REFERENCE") == SUCCESS)
		{
			iLen = strlen(pstCTranDtls->szReference);
			if( iLen > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstCTranDtls->szReference);
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillAmtDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillAmtDtls(KEYVAL_PTYPE curPtr, AMTDTLS_PTYPE pstAmtDtls, char * pszResultCode)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstAmtDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Amount details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	if(pszResultCode == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Result response is present!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(amtDtlsLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &amtDtlsLst[iCnt], KEYVAL_SIZE);

		/*Akshaya_M1 - Fix for PTMX-1384 , The APPROVED_AMOUNT field is for Approved Transaction and TRANS_AMOUNT field for Declined Transaction*/
		if((strcmp(curPtr[iCnt].key, "APPROVED_AMOUNT") == SUCCESS) &&
				((strcmp(pszResultCode, "4") == SUCCESS) || (strcmp(pszResultCode, "5") == SUCCESS) ||
						(strcmp(pszResultCode, "7") == SUCCESS) || (strcmp(pszResultCode, "10") == SUCCESS) || (strcmp(pszResultCode, "22") == SUCCESS)))
		{
			iLen = strlen(pstAmtDtls->apprvdAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->apprvdAmt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TRANS_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstAmtDtls->apprvdAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->apprvdAmt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "AVAILABLE_BALANCE") == SUCCESS)
		{
			iLen = strlen(pstAmtDtls->availBal);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->availBal);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "ORIG_TRANS_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstAmtDtls->origTranAmtFrmHost);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->origTranAmtFrmHost);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DIFF_AMOUNT_DUE") == SUCCESS)
		{
			iLen = strlen(pstAmtDtls->dueamount);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->dueamount);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CASHBACK_AMNT") == SUCCESS)
		{
			iLen = strlen(pstAmtDtls->cashBackAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->cashBackAmt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "TIP_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstAmtDtls->tipAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->tipAmt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DIFF_AUTH_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstAmtDtls->diffAuthAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->diffAuthAmt);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "FS_AVAIL_BALANCE") == SUCCESS)
		{
			iLen = strlen(pstAmtDtls->EBTFoodSNAPBal);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->EBTFoodSNAPBal);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CB_AVAIL_BALANCE") == SUCCESS)
		{
			iLen = strlen(pstAmtDtls->EBTCashBenefitsBal);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->EBTCashBenefitsBal);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PREVIOUS_BALANCE") == SUCCESS)
		{
			iLen = strlen(pstAmtDtls->previousBal);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->previousBal);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillDCCResDtls
 *
 * Description	: Fills all the details of SCA into SCI response
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillDCCResDtls(KEYVAL_PTYPE curPtr, DCCDTLS_PTYPE pstDCCDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstDCCDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No DCC details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stDccRespDtlsLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stDccRespDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "DCC_TRAN_AMOUNT") == SUCCESS)
		{
			iLen = strlen(pstDCCDtls->szFgnAmount);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1 + 1); /* Extra 1 for '.'*/
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1 + 1);
					getFGNAmntWithDecimals(pstDCCDtls->szFgnAmount, pstDCCDtls->iMinorUnits, tmpPtr);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "DCC_EXCHANGE_RATE") == SUCCESS)
		{
			iLen = strlen(pstDCCDtls->szExchngRate);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDCCDtls->szExchngRate);
				}
			}
		}

		else if(strcmp(curPtr[iCnt].key, "DCC_ALPHA_CURR_CODE") == SUCCESS)
		{
			iLen = strlen(pstDCCDtls->szAlphaCurrCode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDCCDtls->szAlphaCurrCode);
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}


/*
 * ============================================================================
 * Function Name: fillDupTranDtls
 *
 * Description	: Fill the duplicate transaction details
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillDupTranDtls(KEYVAL_PTYPE curPtr, PAAS_BOOL bDupTranDetected)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stDupTran) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stDupTran[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "DUPLICATE_TRANSACTION") == SUCCESS)
		{
			if( bDupTranDetected == PAAS_TRUE)
			{
				iLen = 1;
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, "1");
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillReceiptData
 *
 * Description	: Fill receipt data in the key val pairs structure
 *
 * Input Params	:KEYVAL_PTR_TYPE curPtr, transaction key
 *
 * Output Params:
 * ============================================================================
 */
static int fillReceiptData(KEYVAL_PTYPE curPtr, TRAN_PTYPE pstTran, int iResltCode)
{
	int			  iCnt				= 0;
	int			  iTotCnt			= 0;
	int 		  rv 				= SUCCESS;
	VAL_LST_PTYPE xmlReceiptListPtr = NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stPymntRcptList) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stPymntRcptList[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "RECEIPT_DATA") == SUCCESS)
		{
			xmlReceiptListPtr = (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
			if(xmlReceiptListPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(xmlReceiptListPtr, 0x00,sizeof(VAL_LST_STYPE) );

			rv = addXMLRcptNodes(xmlReceiptListPtr, pstTran, iResltCode);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to add receipt nodes",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}

		curPtr[iCnt].value = xmlReceiptListPtr;
		xmlReceiptListPtr  = NULL;
	}
	// CID-67408: 22-Jan-16: MukeshS3: We must free any allocated memory above, if it is not going to be used anywhere for FAILURE case.
	// This function will free memory allocated for each Node in the list & than VAL_LST_PTYPE pointer itself.
	if(rv != SUCCESS)
	{
		freeValListNode(xmlReceiptListPtr);
		curPtr[iCnt].value = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iTotCnt);
	APP_TRACE(szDbgMsg);

	return iTotCnt;
}

/*
 * ============================================================================
 * Function Name: fillFSADtls
 *
 * Description	: Fill FSA data in the key val pairs structure
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillFSADtls(KEYVAL_PTYPE curPtr, AMTDTLS_PTYPE pstAmtDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(FSADtlsLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &FSADtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "FSA_AMOUNT") == SUCCESS)
		{
			/* We fill FSA_AMOUNT field with the Approved amount field from SSI Host and send it back to POS */
			iLen = strlen(pstAmtDtls->apprvdAmt);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstAmtDtls->apprvdAmt);
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillEBTDtls
 *
 * Description	: Fill EBT data in the key val pairs structure
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillEBTDtls(KEYVAL_PTYPE curPtr, EBTDTLS_PTYPE pstEBTDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(EBTDtlsLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &EBTDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "EBT_TYPE") == SUCCESS)
		{
			/* We fill EBT_TYPE field send it back to POS */
			iLen = strlen(pstEBTDtls->szEBTType);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstEBTDtls->szEBTType);
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillPOSTndrDtls
 *
 * Description	: Fill POS Tender data in the key val pairs structure
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillPOSTndrDtls(KEYVAL_PTYPE curPtr, POSTENDERDTLS_PTYPE pstPOSTenderDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
	char 	szTemp[30]		= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stPOSTndrDataLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stPOSTndrDataLst[iCnt], KEYVAL_SIZE);

		memset(szTemp, 0x00, sizeof(szTemp));
		sprintf(szTemp, "POS_TENDER%d_DATA", iCnt + 1);

		if(strcmp(curPtr[iCnt].key, szTemp) == SUCCESS)
		{
			iLen = strlen(pstPOSTenderDtls->szPOSTndrData[iCnt]);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstPOSTenderDtls->szPOSTndrData[iCnt]);
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillDispMsgDtls
 *
 * Description	: Fill Display Message data in the key val pairs structure
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillDispMsgDtls(KEYVAL_PTYPE curPtr, DISPMSG_PTYPE pstDispMsgDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stDispMsgLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stDispMsgLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "TIMEOUT_RESULT") == SUCCESS)
		{
			iLen = strlen(pstDispMsgDtls->szTimeOutResp);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstDispMsgDtls->szTimeOutResp);
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillGetParmDtls
 *
 * Description	: Fill Get Param Details, after getting the values for the requested Get parm fields
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillGetParmDtls(KEYVAL_PTYPE curPtr, GETPARM_PTYPE pstGetParmDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stGetParmRespLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stGetParmRespLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "PARAM") == SUCCESS)
		{
			iLen = strlen(pstGetParmDtls->szGetParmResp);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstGetParmDtls->szGetParmResp);
				}
			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * ============================================================================
 * Function Name: fillRawCardDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillRawCardDtls(KEYVAL_PTYPE curPtr, RAW_CARDDTLS_PTYPE pstRawCrdDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stRawCrdDtlsLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stRawCrdDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "CARD_TRACK1") == SUCCESS)
		{
			iLen = strlen(pstRawCrdDtls->szTrack1Data);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstRawCrdDtls->szTrack1Data);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_TRACK2") == SUCCESS)
		{
			iLen = strlen(pstRawCrdDtls->szTrack2Data);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstRawCrdDtls->szTrack2Data);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_TRACK3") == SUCCESS)
		{
			iLen = strlen(pstRawCrdDtls->szTrack3Data);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstRawCrdDtls->szTrack3Data);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "ACCT_NUM") == SUCCESS)
		{
			iLen = strlen(pstRawCrdDtls->szPAN);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstRawCrdDtls->szPAN);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_EXP_MONTH") == SUCCESS)
		{
			iLen = strlen(pstRawCrdDtls->szClrMon);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstRawCrdDtls->szClrMon);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_EXP_YEAR") == SUCCESS)
		{
			iLen = strlen(pstRawCrdDtls->szClrYear);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstRawCrdDtls->szClrYear);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARDHOLDER") == SUCCESS)
		{
			iLen = strlen(pstRawCrdDtls->szName);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstRawCrdDtls->szName);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PAYMENT_TYPE") == SUCCESS)
		{
			iLen = strlen(pstRawCrdDtls->szPymtType);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstRawCrdDtls->szPymtType);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "PAYMENT_MEDIA") == SUCCESS)
		{
			iLen = strlen(pstRawCrdDtls->szPymtMedia);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstRawCrdDtls->szPymtMedia);
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CARD_ENTRY_METHOD") == SUCCESS)
		{
			iLen = strlen(pstRawCrdDtls->szCardEntryMode);
			if(iLen > 0)
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, pstRawCrdDtls->szCardEntryMode);
				}

			}
		}
		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}

/*
 * =================================================================================
 * Function Name: fillQueryNfcIniRespDtls
 *
 * Description	:This Function fills the Resp Fields for QUERY_NFCINI device Command
 *
 * Input Params	:
 *
 * Output Params:
 * =================================================================================
 */
static int fillQueryNfcIniRespDtls(KEYVAL_PTYPE pstKeyValLst, NFCINI_CONTENT_INFO_PTYPE pstNfcIniContent)
{
	void *			tmpPtr		= NULL;
//	int				rv			= SUCCESS;
	int				iTotCnt		= 0;
	int				iCnt		= 0;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stApplePayDtlsLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&pstKeyValLst[iCnt], &stApplePayDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(pstKeyValLst[iCnt].key, "APPLEPAY") == SUCCESS)
		{
			tmpPtr = (VAL_LST_PTYPE) malloc(sizeof(VAL_LST_STYPE));
			if(tmpPtr != NULL)
			{
				memset(tmpPtr, 0x00, sizeof(VAL_LST_STYPE));
				fillQueryNfcApplePayRespDtls(tmpPtr, pstNfcIniContent);
			}
		}
		pstKeyValLst[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	/*
	 * Praveen_P1: 26 August 2016
	 * Coverity CID# 83371
	 * rv is not updated above, so this condition will not be satisfied
	 */
#if 0
	if(rv != SUCCESS)
	{
		freeKeyValDataNode(pstKeyValLst, iCnt);
		pstKeyValLst = NULL;
	}
#endif
	debug_sprintf(szDbgMsg, "%s: --- Returning [%d]---", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;

}


/*
 * =================================================================================
 * Function Name: fillQueryNfcApplePayRespDtls
 *
 * Description	:This Function fills the Resp Fields for QUERY_NFCINI device Command
 *
 * Input Params	:
 *
 * Output Params:
 * =================================================================================
 */
static int fillQueryNfcApplePayRespDtls(VAL_LST_PTYPE pstValLst, NFCINI_CONTENT_INFO_PTYPE pstNfcIniContent)
{
	int				iLen		= 0;
	void *			tmpPtr		= NULL;
	int				rv			= SUCCESS;
	int				iTotCnt		= 0;
	int				iCnt		= 0;
	KEYVAL_PTYPE	pstList		= NULL;
	KEYVAL_PTYPE 	curPtr		= NULL;
	VAL_NODE_PTYPE	pstTmp		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stApplePayDtls) / KEYVAL_SIZE;

	if(pstNfcIniContent != NULL)
	{
		pstTmp = (VAL_NODE_PTYPE) malloc(sizeof(VAL_NODE_STYPE));
		if(pstTmp == NULL)
		{
			return FAILURE;
		}
		memset(pstTmp, 0x00, sizeof(VAL_NODE_STYPE));

		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			free(pstTmp);
			pstTmp = NULL;

			return FAILURE;
		}

		memset(pstList, 0x00, iTotCnt * KEYVAL_SIZE);

		pstTmp->elemCnt = iTotCnt;
		pstTmp->elemList = pstList;

		curPtr = pstTmp->elemList;
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			memcpy(&curPtr[iCnt], &stApplePayDtls[iCnt], KEYVAL_SIZE);
			if(strcmp(curPtr[iCnt].key, "NUM_MERCHANT_IDS") == SUCCESS)
			{
				 iLen = strlen(pstNfcIniContent->szApplePayNumSections);
				 if(iLen > 0)
				 {
					 tmpPtr = (char *) malloc(iLen + 1);
					 if(tmpPtr != NULL)
					 {
						 memset(tmpPtr, 0x00, iLen + 1);
						 strcpy(tmpPtr ,pstNfcIniContent->szApplePayNumSections);
					 }
					 else
					 {
						 rv = FAILURE;
						 break;
					 }
				 }
			 }
			else if(strcmp(curPtr[iCnt].key, "SECTIONS") == SUCCESS)
			{
				tmpPtr 	= (VAL_LST_PTYPE)malloc(sizeof(VAL_LST_STYPE));
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, sizeof(VAL_LST_STYPE));

					rv 	= fillNfcSectionDtls(tmpPtr, pstNfcIniContent->pstApplePaySectionList);
					if (rv != SUCCESS)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to fill NFC Section data", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						freeValListNode(tmpPtr);
						break;
					}
				}
			}
			curPtr[iCnt].value = tmpPtr;
			tmpPtr = NULL;
		}
	}

	/* Add to the linked list */
	if(pstValLst->start == NULL)
	{
		pstValLst->start = pstValLst->end = pstTmp;
		pstValLst->valCnt++;
	}
	else
	{
		pstValLst->end->next = pstTmp;
		pstValLst->end = pstTmp;
		pstValLst->valCnt++;
	}

	if(rv != SUCCESS)
	{
		// memory for pstValLst was assiged in the caller function, but as we wont have iCnt value there to pass in this function
		// So, freeing this memory here only
		freeKeyValDataNode(pstList, iCnt);
		freeValNode(pstTmp);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * =======================================================================================
 * Function Name: fillNfcSectionDtls
 *
 * Description	:This Function fills the NFC section part of QUERY_NFCINI device command.
 *
 * Input Params	:
 *
 * Output Params:
 * ========================================================================================
 */
static int fillNfcSectionDtls(VAL_LST_PTYPE pstValLst, NFC_APPLE_SECT_DTLS_PTYPE pstNfcIniApplePayContent)
{
	int				iLen		= 0;
	char *			tmpPtr		= NULL;
	int				rv			= SUCCESS;
	int				iTotCnt		= 0;
	int				iCnt		= 0;
	KEYVAL_PTYPE	pstList		= NULL;
	KEYVAL_PTYPE 	curPtr		= NULL;
	VAL_NODE_PTYPE	pstTmp		= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(stQueryNfcDtls) / KEYVAL_SIZE;

	while(pstNfcIniApplePayContent != NULL)
	{
		pstTmp = (VAL_NODE_PTYPE) malloc(sizeof(VAL_NODE_STYPE));
		if(pstTmp == NULL)
		{
			return FAILURE;
		}
		memset(pstTmp, 0x00, sizeof(VAL_NODE_STYPE));

		pstList = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(pstList == NULL)
		{
			free(pstTmp);
			pstTmp = NULL;

			return FAILURE;
		}

		memset(pstList, 0x00, iTotCnt * KEYVAL_SIZE);

		pstTmp->elemCnt = iTotCnt;
		pstTmp->elemList = pstList;

		curPtr = pstTmp->elemList;
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			memcpy(&curPtr[iCnt], &stQueryNfcDtls[iCnt], KEYVAL_SIZE);

			if(strcmp(curPtr[iCnt].key, "Value") == SUCCESS)
			{
				iLen = strlen(pstNfcIniApplePayContent->szSectionName);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr ,pstNfcIniApplePayContent->szSectionName);
					}
					else
					{
						rv = FAILURE;
						break;
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "MERCHANT_INDEX") == SUCCESS)
			{
				iLen = strlen(pstNfcIniApplePayContent->szMerchIndex);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr ,pstNfcIniApplePayContent->szMerchIndex);
					}
					else
					{
						rv = FAILURE;
						break;
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "MERCHANT_ID") == SUCCESS)
			{
				iLen = strlen(pstNfcIniApplePayContent->szMerchID);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstNfcIniApplePayContent->szMerchID);
					}
					else
					{
						rv = FAILURE;
						break;
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "MERCHANT_URL") == SUCCESS)
			{
				iLen = strlen(pstNfcIniApplePayContent->szMerchURL);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstNfcIniApplePayContent->szMerchURL);
					}
					else
					{
						rv = FAILURE;
						break;
					}
				}
			}
			else if(strcmp(curPtr[iCnt].key, "MERCHANT_VAS_FILTER") == SUCCESS)
			{
				iLen = strlen(pstNfcIniApplePayContent->szMerchVASFilter);
				if(iLen > 0)
				{
					tmpPtr = (char *) malloc(iLen + 1);
					if(tmpPtr != NULL)
					{
						memset(tmpPtr, 0x00, iLen + 1);
						strcpy(tmpPtr, pstNfcIniApplePayContent->szMerchVASFilter);
					}
					else
					{
						rv = FAILURE;
						break;
					}
				}
			}
			curPtr[iCnt].value = tmpPtr;
			tmpPtr = NULL;
		}

		/* Add to the linked list */
		if(pstValLst->start == NULL)
		{
			pstValLst->start = pstValLst->end = pstTmp;
			pstValLst->valCnt++;
		}
		else
		{
			pstValLst->end->next = pstTmp;
			pstValLst->end = pstTmp;
			pstValLst->valCnt++;
		}
		pstNfcIniApplePayContent = pstNfcIniApplePayContent->next;
	}
	if(rv != SUCCESS)
	{
		// memory for pstValLst was assiged in the caller function, but as we wont have iCnt value there to pass in this function
		// So, freeing this memory here only
		freeKeyValDataNode(pstList, iCnt);
		freeValNode(pstTmp);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addXMLRcptNodes
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addXMLRcptNodes(VAL_LST_PTYPE rcptListPtr, TRAN_PTYPE pstTran, int iResltCode)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	VAL_NODE_PTYPE	tmpNodePtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* VDR: Creating only one receipt node instead of two nodes separately for
	 * merchant and customer.
	 * Mx needs to generate one receipt according to the requirement  */
	//for(iCnt = 0; iCnt < 2; iCnt++)
	for(iCnt = 0; iCnt < 1; iCnt++)
	{
		tmpNodePtr = (VAL_NODE_PTYPE) malloc(sizeof(VAL_NODE_STYPE));
		if(tmpNodePtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED %d", __FUNCTION__, iCnt);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(tmpNodePtr, 0x00, sizeof(VAL_NODE_STYPE));

		/* Populate the receipt related data in the receipt nodes for the
		 * customer and the merchant */
		rv = populateRcptDataInRcptNode(tmpNodePtr, pstTran, iResltCode);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Failed to populate receipt data in receipt node[%d]", __FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
			freeValNode(tmpNodePtr);
			break;
		}
		/* Add the node to the linked list */
		if(rcptListPtr->start == NULL)
		{
			/* Adding the first element */
			rcptListPtr->start = rcptListPtr->end = tmpNodePtr;
		}
		else
		{
			/* Adding the nth element */
			rcptListPtr->end->next = tmpNodePtr;
			rcptListPtr->end = tmpNodePtr;
		}

		/* Increment the node count */
		rcptListPtr->valCnt += 1;
		tmpNodePtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: populateRcptDataInRcptNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int populateRcptDataInRcptNode(VAL_NODE_PTYPE rcptNodePtr, TRAN_PTYPE pstTran, int iResltCode)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	VAL_LST_PTYPE	rcptListPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		iTotCnt = sizeof(stRcptLines) / KEYVAL_SIZE;
		listPtr = (KEYVAL_PTYPE)malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			memcpy(&(listPtr[iCnt]), &stRcptLines[iCnt], KEYVAL_SIZE);

			rcptListPtr = (VAL_LST_PTYPE) malloc(sizeof(VAL_LST_STYPE));
			if(rcptListPtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			memset(rcptListPtr, 0x00, sizeof(VAL_LST_STYPE));
			rv = buildReceiptlist(pstTran, rcptListPtr, iResltCode);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to build receipt list",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);
				// CID-67421: 29-Jan-16: MukeshS3:
				// Freeing the allocated memory for rcptListPtr in case failure to build the receipt list
				freeValListNode(rcptListPtr);
				rcptListPtr = NULL;
				break;
			}

			listPtr[iCnt].value = rcptListPtr;

		}

		rcptNodePtr->type 		= 0;
		rcptNodePtr->elemList 	= listPtr;
		rcptNodePtr->elemCnt 	= iTotCnt;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: fillCustCheckBoxRespDtls
 *
 * Description	: CHECKED
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int fillCustCheckBoxRespDtls(KEYVAL_PTYPE curPtr, CUSTCHECKBOX_DTLS_PTYPE pstCustCheckBoxDtls)
{
	int		iCnt			= 0;
	int		iTotCnt			= 0;
	int		iLen			= 0;
	char *	tmpPtr			= NULL;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstCustCheckBoxDtls == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: No Customer Checkbox details struct to fill!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return iCnt; //Returning 0
	}

	iTotCnt = sizeof(stCustCheckboxDtlsLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		memcpy(&curPtr[iCnt], &stCustCheckboxDtlsLst[iCnt], KEYVAL_SIZE);

		if(strcmp(curPtr[iCnt].key, "CHECKBOX_1") == SUCCESS)
		{
			if((iLen = strlen(pstCustCheckBoxDtls->szRespCheckBox[iCnt])) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, "Y");
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CHECKBOX_2") == SUCCESS)
		{
			if((iLen = strlen(pstCustCheckBoxDtls->szRespCheckBox[iCnt])) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, "Y");
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CHECKBOX_3") == SUCCESS)
		{
			if((iLen = strlen(pstCustCheckBoxDtls->szRespCheckBox[iCnt])) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, "Y");
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CHECKBOX_4") == SUCCESS)
		{
			if((iLen = strlen(pstCustCheckBoxDtls->szRespCheckBox[iCnt])) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, "Y");
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CHECKBOX_5") == SUCCESS)
		{
			if((iLen = strlen(pstCustCheckBoxDtls->szRespCheckBox[iCnt])) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, "Y");
				}
			}
		}
		else if(strcmp(curPtr[iCnt].key, "CHECKBOX_6") == SUCCESS)
		{
			if((iLen = strlen(pstCustCheckBoxDtls->szRespCheckBox[iCnt])) > 0 )
			{
				tmpPtr = (char *) malloc(iLen + 1);
				if(tmpPtr != NULL)
				{
					memset(tmpPtr, 0x00, iLen + 1);
					strcpy(tmpPtr, "Y");
				}
			}
		}

		curPtr[iCnt].value = tmpPtr;
		tmpPtr = NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);

	return iCnt;
}
/*
 * ============================================================================
 * End of file sciResDataCtrl.c
 * ============================================================================
 */
