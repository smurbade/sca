/*
 * =============================================================================
 * Filename    : sciReqDataCtrl.c
 *
 * Application : Mx Point SCA
 *
 * Description : 
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */

#include "stdio.h"
#include "string.h"
#include "stdlib.h"

#include "common/metaData.h"
#include "common/tranDef.h"
#include "common/utils.h"
#include "sciDef.h"
#include "db/dataSystem.h"
#include "appLog/appLogAPIs.h"
#include "bLogic/bLogicCfgDef.h"
#include "uiAgent/uiCfgDef.h"

#include "sci/sciReqLists.inc"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

typedef METADATA_PTYPE	META_PTYPE;
typedef METADATA_STYPE	META_STYPE;

/* Static functions declarations */
static int getKeyListForSecReq(META_PTYPE, int);
static int getKeyListForSessReq(META_PTYPE, int);
static int getKeyListForLIReq(META_PTYPE, int);
static int getKeyListForSAFReq(META_PTYPE, int);
static int getKeyListForDevReq(META_PTYPE, int);
static int getKeyListForPymtReq(META_PTYPE, int);
static int getKeyListForBatchReq(META_PTYPE, int);
static int getKeyListForRptReq(META_PTYPE, int);
static int getKeyListForAdminReq(META_PTYPE, int);

static int storeSecReq(META_PTYPE, TRAN_PTYPE);
static int storeSessReq(META_PTYPE, TRAN_PTYPE , char *, int );
static int storeLIReq(META_PTYPE, TRAN_PTYPE);
static int storeSAFReq(META_PTYPE, TRAN_PTYPE);
static int storeDeviceReq(META_PTYPE, TRAN_PTYPE);
static int storePymtReq(META_PTYPE, TRAN_PTYPE);
static int storeBatchReq(META_PTYPE, TRAN_PTYPE);
static int storeRptReq(META_PTYPE, TRAN_PTYPE);
static int storeAdminReq(META_PTYPE, TRAN_PTYPE);

static int saveSecDtlsForReg(KEYVAL_PTYPE, POSSEC_PTYPE *);
static int saveSecDtlsForUnreg(KEYVAL_PTYPE, POSSEC_PTYPE *);
static int saveSessDtls(KEYVAL_PTYPE, SESSDTLS_PTYPE *, char *, int );
static int saveAddLIReq(KEYVAL_PTYPE, LIINFO_PTYPE *);
static int addReqLineItems(LIINFO_PTYPE, VAL_LST_PTYPE);
static int saveMerchandiseLI(LI_NODE_PTYPE, VAL_NODE_PTYPE);
static int saveRequestedLiChangeItems(LIINFO_PTYPE , VAL_LST_PTYPE , LI_NODE_PTYPE );
static int saveOfferLI(LI_NODE_PTYPE, VAL_NODE_PTYPE);
static int saveLIChgDtls(KEYVAL_PTYPE, LIINFO_PTYPE *);
static int saveSAFReqDtls(KEYVAL_PTYPE, SAFDTLS_PTYPE *);
static int saveSignDtls(KEYVAL_PTYPE, DEVTRAN_PTYPE *);
static int saveLPTokenDtls(KEYVAL_PTYPE , char **);
static int saveGenPymtRptDtls(KEYVAL_PTYPE , PASSTHRU_PTYPE );
static int savePymtReqDtls(KEYVAL_PTYPE, char **);
static int saveGenPymtDtls(KEYVAL_PTYPE, PYMTTRAN_PTYPE);
static int saveAdditionalPymtDtls(KEYVAL_PTYPE , PYMTTRAN_PTYPE );
static int savePymtAmtDtls(KEYVAL_PTYPE, AMTDTLS_PTYPE *);
static int saveFollowOnDtls(KEYVAL_PTYPE, FTRANDTLS_PTYPE *);
static int saveTaxDtls(KEYVAL_PTYPE, LVL2_PTYPE *);
static int saveBatchReqDtls(KEYVAL_PTYPE, char **);
static int saveRptReqDtls(META_PTYPE, char **);
static int saveChkDtls(KEYVAL_PTYPE , CHKDTLS_PTYPE * );
static int saveCustInfoDtls(KEYVAL_PTYPE , CUSTINFODTLS_PTYPE * );
static int saveSurveyReqDtls(KEYVAL_PTYPE , DEVTRAN_PTYPE * );
static int saveTimeDtls(KEYVAL_PTYPE, DEVTRAN_PTYPE *);
static int saveLaneClosedDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran);
static int saveEmailReqDtls(KEYVAL_PTYPE , DEVTRAN_PTYPE * );
static int saveOrigTranDtls(KEYVAL_PTYPE , PYMTTRAN_PTYPE );
static int saveCardExpDtls(KEYVAL_PTYPE , CARDDTLS_PTYPE * );
static int saveDupSwipeDetectDtls(KEYVAL_PTYPE , PYMTTRAN_PTYPE );
static int saveVersionReqDtls(KEYVAL_PTYPE , DEVTRAN_PTYPE * );
static int saveCharityReqDtls(KEYVAL_PTYPE , DEVTRAN_PTYPE * );
static int saveCustButtonReqDtls(KEYVAL_PTYPE , DEVTRAN_PTYPE * );
static int saveCreditAppReqDtls(KEYVAL_PTYPE , DEVTRAN_PTYPE * );
static int saveSetDevNameReqDtls(KEYVAL_PTYPE , DEVTRAN_PTYPE * );
static int saveCounterDtls(KEYVAL_PTYPE , DEVTRAN_PTYPE * );
static int saveLoyaltyReqDtls(KEYVAL_PTYPE , DEVTRAN_PTYPE * );
static int saveAttributeDtls(KEYVAL_PTYPE, PYMTTRAN_PTYPE);
static int saveEBTTransDtls(KEYVAL_PTYPE, EBTDTLS_PTYPE *);
static int saveFSATransDtls(KEYVAL_PTYPE, FSADTLS_PTYPE *);
static int saveSetParmReqDtls(KEYVAL_PTYPE ,  DEVTRAN_PTYPE * );
static int saveDispMsgReqDtls(KEYVAL_PTYPE ,  DEVTRAN_PTYPE * );
static int saveApplyUpdatesDtls(KEYVAL_PTYPE , DEVTRAN_PTYPE *);
extern int getHostProtocolFormat();
static int saveGetParmReqDtls(KEYVAL_PTYPE ,  DEVTRAN_PTYPE * );
static int saveProvisionPassReqDtls(KEYVAL_PTYPE ,  DEVTRAN_PTYPE * );
static int saveGetCardDataReqDtls(KEYVAL_PTYPE ,  DEVTRAN_PTYPE * );
static int savePassThrgReqFields(KEYVAL_PTYPE, PYMTTRAN_PTYPE);
//static int saveShowLIReqDtls(KEYVAL_PTYPE, SHWLIINFO_PTYPE *);
static int saveDispQRCodeReqDtls(KEYVAL_PTYPE,  DEVTRAN_PTYPE *);
static int saveCheckBoxReqDtls(KEYVAL_PTYPE, DEVTRAN_PTYPE *);
static VAL_LST_PTYPE storeVarListPassThroughFields(VAL_LST_PTYPE );
static int addVarListNode(VAL_LST_PTYPE , VAL_NODE_PTYPE );
/*
 * ============================================================================
 * Function Name: getGenMetaDataForSSIReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getGenMetaDataForSSIReq(META_PTYPE pstMeta)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstMeta == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pstMeta, 0x00, METADATA_SIZE);
		iTotCnt = sizeof(genReqLst) / KEYVAL_SIZE;

		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		memcpy(listPtr, genReqLst, iTotCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		pstMeta->iTotCnt = iTotCnt;
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getGenMetaDataForSSIEReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getGenMetaDataForSSIEReq(META_PTYPE pstMeta)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstMeta == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pstMeta, 0x00, METADATA_SIZE);
		iTotCnt = sizeof(genETranLst) / KEYVAL_SIZE;

		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		memcpy(listPtr, genETranLst, iTotCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		pstMeta->iTotCnt = iTotCnt;
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaDataForPOSAuth
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getMetaDataForPOSAuth(META_PTYPE pstMeta)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstMeta == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pstMeta, 0x00, METADATA_SIZE);
		iTotCnt = sizeof(authLst) / KEYVAL_SIZE;

		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		memcpy(listPtr, authLst, iTotCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		pstMeta->iTotCnt = iTotCnt;
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMetaDataForSCIReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getMetaDataForSCIReq(META_PTYPE pstMeta, int iFxn, int iCmd)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstMeta == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pstMeta, 0x00, METADATA_SIZE);

		switch(iFxn)
		{
		case SCI_SEC:
			rv = getKeyListForSecReq(pstMeta, iCmd);
			break;

		case SCI_SESS:
			rv = getKeyListForSessReq(pstMeta, iCmd);
			break;

		case SCI_LI:
			rv = getKeyListForLIReq(pstMeta, iCmd);
			break;

		case SCI_SAF:
			rv = getKeyListForSAFReq(pstMeta, iCmd);
			break;

		case SCI_DEV:
			rv = getKeyListForDevReq(pstMeta, iCmd);
			break;

		case SCI_PYMT:
			rv = getKeyListForPymtReq(pstMeta, iCmd);
			break;

		case SCI_BATCH:
			rv = getKeyListForBatchReq(pstMeta, iCmd);
			break;

		case SCI_RPT:
			rv = getKeyListForRptReq(pstMeta, iCmd);
			break;

		case SCI_ADM:
			rv = getKeyListForAdminReq(pstMeta, iCmd);
			break;

		case SCI_SCND:
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getFxnNCmdNCntrFromMetaData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getFxnNCmdNCntrFromMetaData(META_PTYPE pstMeta, int * iFxn, int * iCmd, char *pszTranCntr)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(genReqLst) / KEYVAL_SIZE;
	listPtr = pstMeta->keyValList;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		if(listPtr[iCnt].value == NULL)
		{
			continue;
		}
		else if(strcmp(listPtr[iCnt].key, "FUNCTION_TYPE") == SUCCESS)
		{
			*iFxn = *( (int *) (listPtr[iCnt].value) );

		}
		else if(strcmp(listPtr[iCnt].key, "COMMAND") == SUCCESS)
		{
			*iCmd = *( (int *) (listPtr[iCnt].value) );
		}
		else if(strcmp(listPtr[iCnt].key, "COUNTER") == SUCCESS)
		{
			strcpy(pszTranCntr, listPtr[iCnt].value);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getMacLblNPayLoadFromMetaData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getMacLblNPayLoadFromMetaData(META_PTYPE pstMeta, char ** lbl, char ** payload)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(genETranLst) / KEYVAL_SIZE;
	listPtr = pstMeta->keyValList;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		if(listPtr[iCnt].value == NULL)
		{
			continue;
		}
		else if(strcmp(listPtr[iCnt].key, "PAYLOAD") == SUCCESS)
		{
			*payload = (char *) (listPtr[iCnt].value);
		}
		else if(strcmp(listPtr[iCnt].key, "MAC_LABEL") == SUCCESS)
		{
			*lbl = (char *) (listPtr[iCnt].value);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getAuthFlds
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int getAuthFlds(META_PTYPE pstMeta, char ** lbl, char ** mac, char ** tranCtr)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(authLst) / KEYVAL_SIZE;
	listPtr = pstMeta->keyValList;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		if(listPtr[iCnt].value == NULL)
		{
			continue;
		}
		else if(strcmp(listPtr[iCnt].key, "COUNTER") == SUCCESS)
		{
			*tranCtr = (char *) (listPtr[iCnt].value);
		}
		else if(strcmp(listPtr[iCnt].key, "MAC") == SUCCESS)
		{
			*mac = (char *) (listPtr[iCnt].value);
		}
		else if(strcmp(listPtr[iCnt].key, "MAC_LABEL") == SUCCESS)
		{
			*lbl = (char *) (listPtr[iCnt].value);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: procParsedSCIReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int procParsedSCIReq(TRAN_PTYPE pstTran, META_PTYPE pstMeta, char * pszMacLbl, int iEncFlag)
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if((pstTran == NULL) || (pstMeta == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		switch(pstTran->iFxn)
		{
		case SCI_SEC:
			rv = storeSecReq(pstMeta, pstTran);
			break;

		case SCI_SESS:
			rv = storeSessReq(pstMeta, pstTran, pszMacLbl, iEncFlag);
			break;

		case SCI_LI:
			rv = storeLIReq(pstMeta, pstTran);
			break;

		case SCI_SAF:
			rv = storeSAFReq(pstMeta, pstTran);
			break;

		case SCI_DEV:
			rv = storeDeviceReq(pstMeta, pstTran);
			break;

		case SCI_PYMT:
			rv = storePymtReq(pstMeta, pstTran);
			break;

		case SCI_BATCH:
			rv = storeBatchReq(pstMeta, pstTran);
			break;

		case SCI_RPT:
			rv = storeRptReq(pstMeta, pstTran);
			break;

		case SCI_ADM:
			rv = storeAdminReq(pstMeta, pstTran);
			break;

		case SCI_SCND:
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getKeyListForSecReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getKeyListForSecReq(META_PTYPE pstMeta, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	KEYVAL_PTYPE	srcListPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{

		switch(iCmd)
		{
		case SCI_REG:
		case SCI_EREG:
			srcListPtr = regReqLst;
			iTotCnt = sizeof(regReqLst) / KEYVAL_SIZE;
			break;

		case SCI_UNREG:
			srcListPtr = unregReqLst;
			iTotCnt = sizeof(unregReqLst) / KEYVAL_SIZE;
			break;

		case SCI_UNREGALL: //We dont get any fields for this request
		case SCI_TESTMAC:
			/* Passing Dummy Request List as there are no fields left as such to
			 * be parsed for the POS request */
			srcListPtr = dummyReqLst;
			iTotCnt = sizeof(dummyReqLst) / KEYVAL_SIZE;
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate memory for the key list */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Populate the values */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		memcpy(listPtr, srcListPtr, iTotCnt * KEYVAL_SIZE);
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getKeyListForSessReq 
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getKeyListForSessReq(META_PTYPE pstMeta, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	KEYVAL_PTYPE	srcListPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		switch(iCmd)
		{
		case SCI_START:
			srcListPtr = startSessReqLst;
			iTotCnt = sizeof(startSessReqLst) / KEYVAL_SIZE;
			break;

		case SCI_FINISH:
			/* Passing Dummy Request List as there are no fields left as such to
			 * be parsed for the POS request */
			srcListPtr = dummyReqLst;
			iTotCnt = sizeof(dummyReqLst) / KEYVAL_SIZE;
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate memory for the key list */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Populate the key list */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		memcpy(listPtr, srcListPtr, iTotCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		/* Set the metadata */
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getKeyListForLIReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getKeyListForLIReq(META_PTYPE pstMeta, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	KEYVAL_PTYPE	srcPtr			= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		switch(iCmd)
		{
		case SCI_ADD:
			iTotCnt = sizeof(liAddReqLst)/KEYVAL_SIZE;
			srcPtr = liAddReqLst;

			break;

		case SCI_OVERRIDE:
		case SCI_REMOVE:
			iTotCnt = sizeof(liChngReqLst)/KEYVAL_SIZE;
			srcPtr = liChngReqLst;

			break;
		case SCI_SHOW:
		case SCI_REMOVEALL:
			/* Passing Dummy Request List as there are no fields left as such to
			 * be parsed for the POS request */
			iTotCnt = sizeof(dummyReqLst) / KEYVAL_SIZE;
			srcPtr = dummyReqLst;
			break;
	/*Akshaya :21-07-16 . Apple Pay :Ignoring the Implementation if having NFCVAS_MODE and MERCHANT_INDEX are sent in SHOW Line Item Command.*/
#if 0
		case SCI_SHOW:
				iTotCnt = sizeof(ShowLineItemReqLst)/KEYVAL_SIZE;
				srcPtr = ShowLineItemReqLst;
			break;
#endif
		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate the memory for the key list */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Populate the key list */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		memcpy(listPtr, srcPtr, iTotCnt * KEYVAL_SIZE);

		if(iCmd == SCI_REMOVE)
		{
			listPtr[1].isMand = PAAS_FALSE;	/* Quantity */
			listPtr[2].isMand = PAAS_FALSE;	/* Unit price */
			listPtr[3].isMand = PAAS_FALSE;	/* Extended price */
		}

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getKeyListForSAFReq 
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getKeyListForSAFReq(META_PTYPE pstMeta, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	KEYVAL_PTYPE	locListPtr		= NULL;
	KEYVAL_PTYPE	srcListPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		switch(iCmd)
		{
		case SCI_QUERY:
		case SCI_REMOVE:
			iTotCnt = sizeof(safReqLst) / KEYVAL_SIZE;
			srcListPtr = safReqLst;

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate the memory for the key list */
		locListPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(locListPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Populate the key list */
		memset(locListPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		memcpy(locListPtr, srcListPtr, iTotCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(locListPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		/* Set the meta data */
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = locListPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getKeyListForDevReq 
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getKeyListForAdminReq(META_PTYPE pstMeta, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	KEYVAL_PTYPE	locListPtr		= NULL;
	KEYVAL_PTYPE	srcListPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		switch(iCmd)
		{
		case SCI_SETTIME:
			iTotCnt		= sizeof(setTimeReqLst) / KEYVAL_SIZE;
			srcListPtr	= setTimeReqLst;
			break;

		case SCI_GETCOUNTER:
			iTotCnt		= sizeof(getCounterReqLst) / KEYVAL_SIZE;
			srcListPtr	= getCounterReqLst;
			break;

		case SCI_LANE_CLOSED:
			iTotCnt		= sizeof(laneClosedReqLst) / KEYVAL_SIZE;
			srcListPtr	= laneClosedReqLst;
			break;

		case SCI_APPLYUPDATES:
			iTotCnt		= sizeof(applyUpdatesReqLst) / KEYVAL_SIZE;
			srcListPtr	= applyUpdatesReqLst;
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate the memory for the key list */
		locListPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(locListPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Populate the key list */
		memset(locListPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		memcpy(locListPtr, srcListPtr, iTotCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(locListPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		/* Set the metadata */
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = locListPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getKeyListForDevReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getKeyListForDevReq(META_PTYPE pstMeta, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	KEYVAL_PTYPE	locListPtr		= NULL;
	KEYVAL_PTYPE	srcListPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		switch(iCmd)
		{
		case SCI_SIGN:
		case SCI_SIGN_EX:
			iTotCnt = sizeof(sigReqLst) / KEYVAL_SIZE;
			srcListPtr = sigReqLst;
			break;

		case SCI_CUST_QUES:
		case SCI_SURVEY5:
		case SCI_SURVEY10:
			iTotCnt = sizeof(surveyReqLst) / KEYVAL_SIZE;
			srcListPtr = surveyReqLst;
			break;

		case SCI_EMAIL:
			iTotCnt = sizeof(emailReqLst) / KEYVAL_SIZE;
			srcListPtr = emailReqLst;
			break;

		case SCI_VERSION:
			iTotCnt = sizeof(stVersionLst) / KEYVAL_SIZE;
			srcListPtr = stVersionLst;
			break;

		case SCI_CHARITY:
			iTotCnt = sizeof(charityReqLst) / KEYVAL_SIZE;
			srcListPtr = charityReqLst;
			break;

		case SCI_LTY:
			iTotCnt = sizeof(stLoyaltyCapLst) / KEYVAL_SIZE;
			srcListPtr = stLoyaltyCapLst;
			break;

		case SCI_LPTOKEN:
		case SCI_TOKEN:
			iTotCnt = sizeof(genPymtReqLst) / KEYVAL_SIZE;
			srcListPtr = genPymtReqLst;
			break;

		case SCI_GET_DEVICENAME:
		case SCI_EMP_ID_CAPT:
		case SCI_GET_PAYMENT_TYPES:
		case SCI_QUERY_NFCINI:
			/* Passing Dummy Request List as there are no fields left as such to
			 * be parsed for the POS request */
			iTotCnt = sizeof(dummyReqLst) / KEYVAL_SIZE;
			srcListPtr = dummyReqLst;
			break;

		case SCI_SET_DEVICENAME:
			iTotCnt = sizeof(stSetDevNameLst) / KEYVAL_SIZE;
			srcListPtr = stSetDevNameLst;
			break;

		case SCI_CUST_BUTTON:
			iTotCnt = sizeof(custButtReqLst) / KEYVAL_SIZE;
			srcListPtr = custButtReqLst;
			break;

		case SCI_CREDIT_APP:
			iTotCnt = sizeof(creditAppReqLst) / KEYVAL_SIZE;
			srcListPtr = creditAppReqLst;
			break;

		case SCI_SET_PARM:
			iTotCnt = sizeof(setParmReqLst) / KEYVAL_SIZE;
			srcListPtr = setParmReqLst;
			break;

		case SCI_DISPLAY_MESSAGE:
			iTotCnt = sizeof(dispMsgReqLst) / KEYVAL_SIZE;
			srcListPtr = dispMsgReqLst;
			break;

		case SCI_GET_PARM:
			iTotCnt = sizeof(getParmReqLst) / KEYVAL_SIZE;
			srcListPtr = getParmReqLst;
			break;

		case SCI_PROVISION_PASS:
			iTotCnt = sizeof(getProvisionPassReqLst) / KEYVAL_SIZE;
			srcListPtr = getProvisionPassReqLst;
			break;

		case SCI_GET_CARD_DATA:
			iTotCnt = sizeof(getCardDataReqLst) / KEYVAL_SIZE;
			srcListPtr = getCardDataReqLst;
			break;
			
		case SCI_DISP_QRCODE:
			iTotCnt = sizeof(getDispQRCodeReqLst) / KEYVAL_SIZE;
			srcListPtr = getDispQRCodeReqLst;
			break;

		case SCI_CANCEL_QRCODE:
			break;

		case SCI_CUST_CHECKBOX:
			iTotCnt = sizeof(custCheckBoxReqLst) / KEYVAL_SIZE;
			srcListPtr = custCheckBoxReqLst;
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate the memory for the key list */
		locListPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(locListPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Populate the key list */
		memset(locListPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		memcpy(locListPtr, srcListPtr, iTotCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(locListPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		/* Set the metadata */
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = locListPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getKeyListForPymtReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getKeyListForPymtReq(META_PTYPE pstMeta, int iCmd)
{
	int						rv					= SUCCESS;
	int						iCnt				= 0;
	int						iTotCnt				= 0;
	int						iMandCnt			= 0;
	KEYVAL_PTYPE			listPtr				= NULL;
	PASSTHRG_FIELDS_PTYPE	pstPassThrghDtls 	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/* MukeshS3: 14-April-16:
	 * Adding support for parsing of pass through fields dynamically.
	 * The list of pass through fields is being configured in a .INI file present at flash location.
	 * Need to add all those pass through request fields to the listPtr before parsing.
	 * First we will get the details of pass through fields
	 */
	pstPassThrghDtls = getPassThrgFieldsDtls();

	while(1)
	{
		switch(iCmd)
		{
		case SCI_AUTH:
		case SCI_CAPTURE:
		case SCI_CREDIT:
		case SCI_VOID:
		case SCI_COMPLETION:
		case SCI_ACTIVATE:
		case SCI_ADDVAL:
		case SCI_BAL:
		case SCI_GIFTCLOSE:
		case SCI_DEACTIVATE:
		case SCI_REACTIVATE:
		case SCI_REMVAL:
		case SCI_ADDTIP:
		case SCI_RESETTIP:
		case SCI_LPTOKEN:
		case SCI_TOKEN:
		case SCI_PAYACCOUNT:
		case SCI_TOKEN_UPDATE:
			iTotCnt += sizeof(genPymtReqLst) / KEYVAL_SIZE;
			iTotCnt += sizeof(pymtAmtReqLst) / KEYVAL_SIZE;
			iTotCnt += sizeof(pymtFollowOnReqLst) / KEYVAL_SIZE;
			iTotCnt += sizeof(lvl2ReqLst) / KEYVAL_SIZE;
			iTotCnt += sizeof(pymtChkReqLst) / KEYVAL_SIZE; //Added for check processing
			iTotCnt += sizeof(custInfoReqLst) / KEYVAL_SIZE; //Added for customer information
			iTotCnt += sizeof(pymtOrigTranDateLst) / KEYVAL_SIZE; //Added for Original Tran Date/Time
			iTotCnt += sizeof(pymtCardExpLst) / KEYVAL_SIZE; //Added for Card Expiry Month/Year
			iTotCnt += sizeof(dupSwipeDetect) / KEYVAL_SIZE; //Added for Duplicate Swipe detect
			iTotCnt += sizeof(pymtAttributeReqLst)/ KEYVAL_SIZE;//Added for the support of accnt num and barcode
			iTotCnt += sizeof(ebtReqLst)/ KEYVAL_SIZE;//Added for the support EBT Transactions
			iTotCnt += sizeof(FSAReqLst)/ KEYVAL_SIZE;//Added for the support EBT Transactions
			iTotCnt += sizeof(addPymtReqLst) / KEYVAL_SIZE;
			/* MukeshS3: 14-April-16:
			 * Parsing pass through fields present in SCI request for payment commands.
			 */
			if(pstPassThrghDtls->iReqTagsCnt > 0)
			{
				iTotCnt += pstPassThrghDtls->iReqTagsCnt;
			}
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate memory for the keylist */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Populate the keylist */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		iTotCnt = 0;

		iCnt = sizeof(genPymtReqLst) / KEYVAL_SIZE;
		memcpy(listPtr + iTotCnt, genPymtReqLst, iCnt * KEYVAL_SIZE);
		iTotCnt += iCnt;

		iCnt = sizeof(pymtAmtReqLst) / KEYVAL_SIZE;
		memcpy(listPtr + iTotCnt, pymtAmtReqLst, iCnt * KEYVAL_SIZE);
		iTotCnt += iCnt;

		iCnt = sizeof(pymtFollowOnReqLst) / KEYVAL_SIZE;
		memcpy(listPtr + iTotCnt, pymtFollowOnReqLst, iCnt * KEYVAL_SIZE);
		iTotCnt += iCnt;

		iCnt = sizeof(lvl2ReqLst) / KEYVAL_SIZE;
		memcpy(listPtr + iTotCnt, lvl2ReqLst, iCnt * KEYVAL_SIZE);
		iTotCnt += iCnt;

		iCnt = sizeof(pymtChkReqLst) / KEYVAL_SIZE;
		memcpy(listPtr + iTotCnt, pymtChkReqLst, iCnt * KEYVAL_SIZE);
		iTotCnt += iCnt;

		iCnt = sizeof(custInfoReqLst) / KEYVAL_SIZE;
		memcpy(listPtr + iTotCnt, custInfoReqLst, iCnt * KEYVAL_SIZE);
		iTotCnt += iCnt;

		iCnt = sizeof(pymtOrigTranDateLst) / KEYVAL_SIZE;
		memcpy(listPtr + iTotCnt, pymtOrigTranDateLst, iCnt * KEYVAL_SIZE);
		iTotCnt += iCnt;

		iCnt = sizeof(pymtCardExpLst) / KEYVAL_SIZE;
		memcpy(listPtr + iTotCnt, pymtCardExpLst, iCnt * KEYVAL_SIZE);
		iTotCnt += iCnt;

		iCnt = sizeof(dupSwipeDetect) / KEYVAL_SIZE;
		memcpy(listPtr + iTotCnt, dupSwipeDetect, iCnt * KEYVAL_SIZE);
		iTotCnt += iCnt;

		iCnt = sizeof(pymtAttributeReqLst) / KEYVAL_SIZE;
		memcpy(listPtr + iTotCnt, pymtAttributeReqLst, iCnt * KEYVAL_SIZE);
		iTotCnt += iCnt;

		iCnt = sizeof(ebtReqLst) / KEYVAL_SIZE;
		memcpy(listPtr + iTotCnt, ebtReqLst, iCnt * KEYVAL_SIZE);
		iTotCnt += iCnt;

		iCnt = sizeof(FSAReqLst) / KEYVAL_SIZE;
		memcpy(listPtr + iTotCnt, FSAReqLst, iCnt * KEYVAL_SIZE);
		iTotCnt += iCnt;

		iCnt = sizeof(addPymtReqLst) / KEYVAL_SIZE;
		memcpy(listPtr + iTotCnt, addPymtReqLst, iCnt * KEYVAL_SIZE);
		iTotCnt += iCnt;

		/* MukeshS3: 14-April-16:
		 * We have used the same key value pair structure KEYVAL_STYPE for pass through fields, which we uses for other XML tags.
		 */
		if(pstPassThrghDtls->iReqTagsCnt > 0)
		{
			iCnt = pstPassThrghDtls->iReqTagsCnt;
			memcpy(listPtr + iTotCnt, pstPassThrghDtls->pstReqTagList, iCnt * KEYVAL_SIZE);
			iTotCnt += iCnt;
		}

		switch(iCmd)
		{
		case SCI_AUTH:
		case SCI_CAPTURE:
		//case SCI_ACTIVATE:
		case SCI_ADDVAL:
		case SCI_REMVAL:
		case SCI_PAYACCOUNT:
			listPtr[7].isMand = PAAS_TRUE;		/* TRANS_AMOUNT */
			break;
		case SCI_LPTOKEN:
		case SCI_TOKEN:
			listPtr[7].isMand = PAAS_FALSE;		/* TRANS_AMOUNT */
			break;
		case SCI_CREDIT:
#if 0
			if(isSTBLogicEnabled())
			{
				/*
				 * IF STB Logic is enabled then we can determine payment type
				 * if the card details are present(swiped/tapped)
				 * Thats why making this field optional
				 */
				listPtr[0].isMand = PAAS_FALSE;		/* PAYMENT_TYPE */
			}
			else
			{
				listPtr[0].isMand = PAAS_TRUE;		/* PAYMENT_TYPE */
			}
#endif
			listPtr[0].isMand = PAAS_FALSE;		/* PAYMENT_TYPE */
			listPtr[7].isMand = PAAS_TRUE;		/* TRANS_AMOUNT */
			break;

		case SCI_BAL:
		case SCI_GIFTCLOSE:
		case SCI_DEACTIVATE:
		case SCI_REACTIVATE:
		case SCI_ACTIVATE:
			listPtr[7].isMand = PAAS_FALSE;		/* TRANS_AMOUNT */
			break;

		case SCI_ADDTIP:
			listPtr[8].isMand = PAAS_TRUE;		/* TIP_AMOUNT */
		case SCI_RESETTIP:
			listPtr[7].isMand = PAAS_FALSE;		/* TRANS_AMOUNT */
			listPtr[11].isMand = PAAS_TRUE;		/* CTROUTD */
			break;

		case SCI_VOID:
			listPtr[0].isMand = PAAS_TRUE;		/* PAYMENT_TYPE */
			listPtr[7].isMand = PAAS_FALSE;		/* TRANS_AMOUNT */
			listPtr[11].isMand = PAAS_TRUE;		/* CTROUTD */
			break;
		}

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		/* Set the meta data */
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getKeyListForBatchReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getKeyListForBatchReq(META_PTYPE pstMeta, int iCmd)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				iMandCnt		= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	KEYVAL_PTYPE	srcListPtr		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		switch(iCmd)
		{
		case SCI_STL:
			/* Passing Dummy Request List as there are no fields left as such to
			 * be parsed for the POS request */
			srcListPtr = dummyReqLst;
			iTotCnt = sizeof(dummyReqLst) / KEYVAL_SIZE;

			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Invalid Command, Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate memory for the key list */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Populate the key list */
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);
		memcpy(listPtr, srcListPtr, iTotCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		/* Set the metadata */
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getKeyListForRptReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getKeyListForRptReq(META_PTYPE pstMeta, int iCmd)
{
	int						rv					= SUCCESS;
	int						iCnt				= 0;
	int						iTotCnt				= 0;
	int						iListCnt			= 0;
	int						iMandCnt			= 0;
	KEYVAL_PTYPE			listPtr				= NULL;
	KEYVAL_PTYPE			srcListPtr			= NULL;
	PASSTHRG_FIELDS_PTYPE	pstPassThrghDtls 	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* MukeshS3: 8-June-16:
	 * Adding support for parsing of pass through fields dynamically.
	 * The list of pass through fields is being configured in a .INI file present at flash location.
	 * Need to add all those pass through request fields to the listPtr before parsing.
	 * First we will get the details of pass through fields here
	 */
	pstPassThrghDtls = getPassThrgFieldsDtls();

	while(1)
	{
		switch(iCmd)
		{
		case SCI_DAYSUMM:
		case SCI_PRESTL:
		case SCI_STLERR:
		case SCI_TRANSEARCH:
			iTotCnt = sizeof(rptReqLst) / KEYVAL_SIZE;
			srcListPtr = rptReqLst;
			break;

		case SCI_STLSUMM:
			iTotCnt = sizeof(stlSummReqLst) / KEYVAL_SIZE;
			srcListPtr = stlSummReqLst;
			break;

		case SCI_DUPCHK:
			iTotCnt = sizeof(dupChkReqLst) / KEYVAL_SIZE;
			srcListPtr = dupChkReqLst;
			break;

		case SCI_LASTTRAN:
			/* Passing Dummy Request List as there are no fields left as such to
			 * be parsed for the POS request */
			iTotCnt = sizeof(lastTranReqLst) / KEYVAL_SIZE;
			srcListPtr = lastTranReqLst;
			break;

		case SCI_CUTOVER:
			/* Passing Dummy Request List as there are no fields left as such to
			 * be parsed for the POS request */
			iTotCnt = sizeof(dummyReqLst) / KEYVAL_SIZE;
			srcListPtr = dummyReqLst;
			break;

		case SCI_SITETOTALS:
			/* Passing Dummy Request List as there are no fields left as such to
			 * be parsed for the POS request */
			iTotCnt = sizeof(dummyReqLst) / KEYVAL_SIZE;
			srcListPtr = dummyReqLst;
			break;
#if 0
		case SCI_LPTOKEN:
			iTotCnt = sizeof(genPymtReqLst) / KEYVAL_SIZE;
			srcListPtr = genPymtReqLst;
			break;
#endif
		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here, un-supported command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		// pass through fields
		iTotCnt += pstPassThrghDtls->iReqTagsCnt;

		if(rv != SUCCESS)
		{
			break;
		}

		/* Allocate memory for the key list */
		listPtr = (KEYVAL_PTYPE) malloc(iTotCnt * KEYVAL_SIZE);
		if(listPtr == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(listPtr, 0x00, iTotCnt * KEYVAL_SIZE);

		/* Populate the static key list */
		iListCnt = iTotCnt - pstPassThrghDtls->iReqTagsCnt;
		memcpy(listPtr, srcListPtr, iListCnt * KEYVAL_SIZE);

		// Populate pass through fields, if present
		if(pstPassThrghDtls->pstReqTagList != NULL)
		{
			memcpy(listPtr + iListCnt, pstPassThrghDtls->pstReqTagList, (pstPassThrghDtls->iReqTagsCnt) * KEYVAL_SIZE);
		}

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].isMand == PAAS_TRUE)
			{
				iMandCnt++;
			}
		}

		/* Set the metadata */
		pstMeta->iMandCnt = iMandCnt;
		pstMeta->iTotCnt = iTotCnt;
		pstMeta->keyValList = listPtr;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeSecReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeSecReq(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	KEYVAL_PTYPE	listPtr				= NULL;
	char			szAppLogData[300]	= "";
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	listPtr = pstMeta->keyValList;

	switch(pstTran->iCmd)
	{
	case SCI_REG:
	case SCI_EREG:
		rv = saveSecDtlsForReg(listPtr, (POSSEC_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save sec dtls for REG",
									__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		strcpy(szAppLogData, "Received POS Register Command");
		break;

	case SCI_UNREG:
		rv = saveSecDtlsForUnreg(listPtr, (POSSEC_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save sec dtls for UNREG",
									__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		strcpy(szAppLogData, "Received POS UnRegister Command");
		break;

	case SCI_TESTMAC:
		debug_sprintf(szDbgMsg, "%s: Nothing to be saved", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		strcpy(szAppLogData, "Received POS TestMAC Command");
		break;

	case SCI_UNREGALL:
		debug_sprintf(szDbgMsg, "%s: Nothing to be saved", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		strcpy(szAppLogData, "Received POS UNREGISTERALL Command");
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}
	if(iAppLogEnabled == 1)
	{
		addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
		debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	}
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeSessReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeSessReq(META_PTYPE pstMeta, TRAN_PTYPE pstTran, char * pszMacLbl, int iEncFlag)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	KEYVAL_PTYPE	listPtr				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	listPtr = pstMeta->keyValList;

	switch(pstTran->iCmd)
	{
	case SCI_START:
		rv = saveSessDtls(listPtr, (SESSDTLS_PTYPE *) &(pstTran->data), pszMacLbl, iEncFlag);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to save sess dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		strcpy(szAppLogData, "Received Start Session Command");
		break;

	case SCI_FINISH:
		debug_sprintf(szDbgMsg, "%s: Nothing to be saved", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szAppLogData, "Received Session Finish Command");
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}
	if(iAppLogEnabled == 1)
	{
		addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeLIReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeLIReq(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int				rv					= SUCCESS;
	KEYVAL_PTYPE	listPtr				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	listPtr = pstMeta->keyValList;

	switch(pstTran->iCmd)
	{
	case SCI_ADD:
		rv = saveAddLIReq(listPtr, (LIINFO_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save LI", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_OVERRIDE:
	case SCI_REMOVE:
		rv = saveLIChgDtls(listPtr, (LIINFO_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save LI change details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	/*Akshaya :21-07-16 . Apple Pay :Ignoring the Implementation if having NFCVAS_MODE and MERCHANT_INDEX are sent in SHOW Line Item Command.*/
#if 0
	case SCI_SHOW:
		rv = saveShowLIReqDtls(listPtr, (SHWLIINFO_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to store Show Line Item Req details",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
#endif
	case SCI_SHOW:
	case SCI_REMOVEALL:
		debug_sprintf(szDbgMsg, "%s: Nothing to be saved", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeSAFReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeSAFReq(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	KEYVAL_PTYPE	listPtr				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	listPtr = pstMeta->keyValList;

	switch(pstTran->iCmd)
	{
	case SCI_QUERY:
		strcpy(szAppLogData, "Received SAF Query Command");
	case SCI_REMOVE:
		if(strlen(szAppLogData) == 0)
		{
			strcpy(szAppLogData, "Received SAF Remove Command");
		}
		rv = saveSAFReqDtls(listPtr, (SAFDTLS_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save SAF request details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}
	if(iAppLogEnabled == 1)
	{
		addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeDeviceReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeAdminReq(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	KEYVAL_PTYPE	listPtr				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	listPtr = pstMeta->keyValList;

	switch(pstTran->iCmd)
	{
	case SCI_SETTIME:
		strcpy(szAppLogData, "Received Set Time Command");
		rv = saveTimeDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Time details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_GETCOUNTER:
		strcpy(szAppLogData, "Received Get Counter Command");
		rv = saveCounterDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Counter details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_LANE_CLOSED:
		strcpy(szAppLogData, "Received Lane Closed Command");
		rv = saveLaneClosedDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Lane Closed details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	case SCI_APPLYUPDATES:
		strcpy(szAppLogData, "Received Apply Updates Command");
		rv = saveApplyUpdatesDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Apply Updates details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}
	if(iAppLogEnabled == 1)
	{
		addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeDeviceReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeDeviceReq(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	KEYVAL_PTYPE	listPtr				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	listPtr = pstMeta->keyValList;

	switch(pstTran->iCmd)
	{
	case SCI_SIGN:
	case SCI_SIGN_EX:
		strcpy(szAppLogData, "Received Get Signature Command");
		rv = saveSignDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save sig details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_CUST_QUES:
		strcpy(szAppLogData, "Received Customer Question Command");
	case SCI_SURVEY5:
		if(strlen(szAppLogData) == 0)
		{
			strcpy(szAppLogData, "Received Get Survey (5) Details Command");
		}
	case SCI_SURVEY10:
		if(strlen(szAppLogData) == 0)
		{
			strcpy(szAppLogData, "Received Get Survey (10) Details Command");
		}
		rv = saveSurveyReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save survey details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_EMAIL:
		strcpy(szAppLogData, "Received Get Email Command");
		rv = saveEmailReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save email details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_VERSION:
		strcpy(szAppLogData, "Received Get SCI Version Command");
		rv = saveVersionReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Version details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_CHARITY:
		strcpy(szAppLogData, "Received Charity Command");
		rv = saveCharityReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Charity details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_LTY:
		strcpy(szAppLogData, "Received Loyalty Command");
		rv = saveLoyaltyReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Device name details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_GET_DEVICENAME:
		strcpy(szAppLogData, "Received Get Device Name Command");
		debug_sprintf(szDbgMsg, "%s: Nothing to be saved", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;

	case SCI_SET_DEVICENAME:
		strcpy(szAppLogData, "Received Set Device Name Command");
		rv = saveSetDevNameReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Device name details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_EMP_ID_CAPT:
		strcpy(szAppLogData, "Received Employee Id Capture Command");
		debug_sprintf(szDbgMsg, "%s: Nothing to be saved", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;

	case SCI_CUST_BUTTON:
		strcpy(szAppLogData, "Received Customer Button Command");
		rv = saveCustButtonReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save cust Button details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_CREDIT_APP:
		strcpy(szAppLogData, "Received Credit Application Command");
		rv = saveCreditAppReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Credit Application Request Details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_GET_PAYMENT_TYPES:
		strcpy(szAppLogData, "Received Get Payment Types Command");
		debug_sprintf(szDbgMsg, "%s: Nothing to be saved", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;

	case SCI_SET_PARM:
		strcpy(szAppLogData, "Received Set Parameter Command");
		rv = saveSetParmReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Set Parameter Request Details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_DISPLAY_MESSAGE:
		strcpy(szAppLogData, "Received Display Message Command");
		rv = saveDispMsgReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Display Message Request Details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_GET_PARM:
		strcpy(szAppLogData, "Received Get Parameter Command");
		rv = saveGetParmReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Get Parameter Request Details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_PROVISION_PASS:
		strcpy(szAppLogData, "Received Provision Pass Command");
		rv = saveProvisionPassReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Provision Pass Request Details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_GET_CARD_DATA:
		strcpy(szAppLogData,"Received Get Card Data Command");
		rv = saveGetCardDataReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Card Data Request Details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_QUERY_NFCINI:
		strcpy(szAppLogData, "Received Query NFC INI Command");
		debug_sprintf(szDbgMsg, "%s: Nothing to be saved", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;


	case SCI_DISP_QRCODE:
		strcpy(szAppLogData, "Received Display QR Code Command");
		rv = saveDispQRCodeReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Display QR Code command Request Details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	case SCI_CANCEL_QRCODE:
		strcpy(szAppLogData, "Received Cancel QR Code Command");
		debug_sprintf(szDbgMsg, "%s: Received Cancel QR Code Command",__FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;
		
	case SCI_CUST_CHECKBOX:
		strcpy(szAppLogData, "Received Customer Checkbox Command");
		rv = saveCheckBoxReqDtls(listPtr, (DEVTRAN_PTYPE *) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Customer Checkbox command Request Details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}
	if(iAppLogEnabled == 1)
	{
		addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storePymtReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storePymtReq(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	KEYVAL_PTYPE	listPtr				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	if(iAppLogEnabled == 1)
	{
		switch(pstTran->iCmd)
		{
		case SCI_AUTH:
			strcpy(szAppLogData, "Received Authorization AUTH Command");
			break;
		case SCI_CAPTURE:
			strcpy(szAppLogData, "Received SALE Command");
			break;
		case SCI_CREDIT:
			strcpy(szAppLogData, "Received REFUND Command");
			break;
		case SCI_VOID:
			strcpy(szAppLogData, "Received VOID Transaction Command");
			break;
		case SCI_COMPLETION:
			strcpy(szAppLogData, "Received COMPLETION Command");
			break;
		case SCI_ADDTIP:
			strcpy(szAppLogData, "Received ADD_TIP Command");
			break;
		case SCI_RESETTIP:
			strcpy(szAppLogData, "Received RESET_TIP Command");
			break;
		case SCI_TOKEN:
			strcpy(szAppLogData, "Received TOKEN_QUERY Command");
			break;
		case SCI_LPTOKEN:
			strcpy(szAppLogData, "Received QUERY_LPTOKEN Command");
			break;
		case SCI_ACTIVATE:
			strcpy(szAppLogData, "Received ACTIVATE Command");
			break;
		case SCI_ADDVAL:
			strcpy(szAppLogData, "Received ADD_VALUE Command");
			break;
		case SCI_BAL:
			strcpy(szAppLogData, "Received BALANCE Command");
			break;
		case SCI_GIFTCLOSE:
			strcpy(szAppLogData, "Received GIFT_CLOSE Command");
			break;
		case SCI_DEACTIVATE:
			strcpy(szAppLogData, "Received DEACTIVATE Command");
			break;
		case SCI_REACTIVATE:
			strcpy(szAppLogData, "Received REACTIVATE Command");
			break;
		case SCI_REMVAL:
			strcpy(szAppLogData, "Received REMOVE_VALUE Command");
			break;
		case SCI_PAYACCOUNT:
			strcpy(szAppLogData, "Received PAYACCOUNT Command");
			break;
		case SCI_TOKEN_UPDATE:
			strcpy(szAppLogData, "Received Token Update Command");
			break;
		default:
			break;
		}
		
		addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
	}

	listPtr = pstMeta->keyValList;

	switch(pstTran->iCmd)
	{
	case SCI_AUTH:
	case SCI_CAPTURE:
	case SCI_CREDIT:
	case SCI_VOID:
	case SCI_COMPLETION:
	case SCI_ACTIVATE:
	case SCI_ADDVAL:
	case SCI_BAL:
	case SCI_GIFTCLOSE:
	case SCI_DEACTIVATE:
	case SCI_REACTIVATE:
	case SCI_REMVAL:
	case SCI_ADDTIP:
	case SCI_RESETTIP:
	case SCI_LPTOKEN:
	case SCI_TOKEN:
	case SCI_PAYACCOUNT:
	case SCI_TOKEN_UPDATE:
		rv = savePymtReqDtls(listPtr, (char **) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to save pymt dtls",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeBatchReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeBatchReq(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	KEYVAL_PTYPE	listPtr				= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	listPtr = pstMeta->keyValList;

	switch(pstTran->iCmd)
	{
	case SCI_STL:
		strcpy(szAppLogData, "Received Batch Settle Command From POS");
		rv = saveBatchReqDtls(listPtr, (char **) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save batch request details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}
	if(iAppLogEnabled == 1)
	{
		addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: storeRptReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int storeRptReq(META_PTYPE pstMeta, TRAN_PTYPE pstTran)
{
	int				rv					= SUCCESS;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szAppLogData[300]	= "";
	KEYVAL_PTYPE listPtr;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	debug_sprintf(szDbgMsg, "%s: TotalCount[%d], MandCount[%d], Key[%s], bMand[%d], ValType[%d]", __FUNCTION__, pstMeta->iTotCnt, pstMeta->iMandCnt, pstMeta->keyValList->key, pstMeta->keyValList->isMand, pstMeta->keyValList->valType);
	APP_TRACE(szDbgMsg);

	if(iAppLogEnabled == 1)
	{
		//Below Switch is For Application Logging Purpose
		switch(pstTran->iCmd)
		{
		case SCI_DAYSUMM:
			strcpy(szAppLogData, "Received DAY SUMMARY Request Command");
			break;
		case SCI_PRESTL:
			strcpy(szAppLogData, "Received PRE-SETTLEMENT Request Command");
			break;
		case SCI_STLERR:
			strcpy(szAppLogData, "Received SETTLE ERROR Request Command");
			break;
		case SCI_TRANSEARCH:
			strcpy(szAppLogData, "Received Transaction Search Request Command");
			break;
		case SCI_STLSUMM:
			strcpy(szAppLogData, "Received SETTLE Summary Request Command");
			break;
		case SCI_LASTTRAN:
			strcpy(szAppLogData, "Received LAST TRANSACTION Request Command");
			break;
		case SCI_DUPCHK:
			strcpy(szAppLogData, "Received DUPLICATE CHECK Request Command");
			break;
		case SCI_CUTOVER:
			strcpy(szAppLogData, "Received CUTOVER Request Command");
			break;
		case SCI_SITETOTALS:
			strcpy(szAppLogData, "Received SITETOTALS Request Command");
			break;
		default:
			break;
		}
		addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
	}

	listPtr = pstMeta->keyValList;

	switch(pstTran->iCmd)
	{
	case SCI_DAYSUMM:
	case SCI_PRESTL:
	case SCI_STLERR:
	case SCI_TRANSEARCH:
	case SCI_STLSUMM:
	case SCI_LASTTRAN:
	case SCI_DUPCHK:
	case SCI_CUTOVER:
	case SCI_SITETOTALS:
		rv = saveRptReqDtls(pstMeta, (char **) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save report request details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;
	case SCI_LPTOKEN:
		/*
		 * LP token needs to capture card details and communication with the
		 * SSI host, for this we have added these fields to the PAASTHRU Structure
		 * We need to store these values in the data system that why calling
		 * different function for this case
		 */
		rv = saveLPTokenDtls(pstMeta->keyValList, (char **) &(pstTran->data));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save LP Token details",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		break;

	default:
		debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveSecDtlsForReg
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveSecDtlsForReg(KEYVAL_PTYPE listPtr, POSSEC_PTYPE * dpstSec)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	POSSEC_STYPE	stSecDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(POSSEC_STYPE);

	while(1)
	{
		if(*dpstSec != NULL)
		{
			memcpy(&stSecDtls, *dpstSec, iSize);
		}
		else
		{
			memset(&stSecDtls, 0x00, iSize);
		}

		iTotCnt = sizeof(regReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "ENTRY_CODE") == SUCCESS)
			{
				strcpy(stSecDtls.szPIN, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "KEY") == SUCCESS)
			{
				strcpy(stSecDtls.szRsaKey, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "REG_VER") == SUCCESS)
			{
				strcpy(stSecDtls.szRegCmdVer, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstSec == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstSec = (POSSEC_PTYPE) malloc(iSize);
				if(*dpstSec == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstSec, 0x00, iSize);
			memcpy(*dpstSec, &stSecDtls, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveSecDtlsForUnreg
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveSecDtlsForUnreg(KEYVAL_PTYPE listPtr, POSSEC_PTYPE * dpstSec)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	POSSEC_STYPE	stSecDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(POSSEC_STYPE);

	while(1)
	{
		if(*dpstSec != NULL)
		{
			memcpy(&stSecDtls, *dpstSec, iSize);
		}
		else
		{
			memset(&stSecDtls, 0x00, iSize);
		}

		iTotCnt = sizeof(unregReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "ENTRY_CODE") == SUCCESS)
			{
				strcpy(stSecDtls.szPIN, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "MAC_LABEL") == SUCCESS)
			{
				strcpy(stSecDtls.szMACLbl, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstSec == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstSec = (POSSEC_PTYPE) malloc(iSize);
				if(*dpstSec == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstSec, 0x00, iSize);
			memcpy(*dpstSec, &stSecDtls, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveSessDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveSessDtls(KEYVAL_PTYPE listPtr, SESSDTLS_PTYPE * dpstSessDtls, char * pszMacLbl, int iEncFlag)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	SESSDTLS_STYPE	stSessDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(SESSDTLS_STYPE);

	while(1)
	{
		if(*dpstSessDtls != NULL)
		{
			memset(&stSessDtls, 0x00, iSize);
			stSessDtls.iSwipeAheadFlag = -1; //Set the default value as -1
			memcpy(&stSessDtls, *dpstSessDtls, iSize);
		}
		else
		{
			memset(&stSessDtls, 0x00, iSize);
			stSessDtls.iSwipeAheadFlag = -1; //Set the default value as -1
		}

		stSessDtls.iDemoModeFlag  = -1; //Set the default value as -1, if field is not present in the Request

		stSessDtls.bScreenChangereqd  = PAAS_TRUE; //Set the default value as TRUE, if field is not present in the Request

		iTotCnt = sizeof(startSessReqLst) / KEYVAL_SIZE;

		/* Copying the maclabel */
		strcpy(stSessDtls.szPOSId, pszMacLbl);

		/* Copying Encrypted mode */
		stSessDtls.iEncFlag = iEncFlag;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "INVOICE") == SUCCESS)
			{
				strcpy(stSessDtls.szInvoice, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "STORE_NUM") == SUCCESS)
			{
				strcpy(stSessDtls.szStoreNo, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "LANE") == SUCCESS)
			{
				strcpy(stSessDtls.szLaneNo, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CASHIER_ID") == SUCCESS)
			{
				strcpy(stSessDtls.szCashierId, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "SERVER_ID") == SUCCESS)
			{
				strcpy(stSessDtls.szServerId, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "SHIFT_ID") == SUCCESS)
			{
				strcpy(stSessDtls.szShiftId, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TABLE_NUM") == SUCCESS)
			{
				strcpy(stSessDtls.szTableNo, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PURCHASE_ID") == SUCCESS)
			{
				strcpy(stSessDtls.szPurchaseId, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "POS_IP") == SUCCESS)
			{
				strcpy(stSessDtls.stUnsolMsgInfo.szPOSIP, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "POS_PORT") == SUCCESS)
			{
				strcpy(stSessDtls.stUnsolMsgInfo.szPOSPort, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "BUSINESSDATE") == SUCCESS)
			{
				strcpy(stSessDtls.szBusinessDate, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "SWIPE_AHEAD") == SUCCESS)
			{
				stSessDtls.iSwipeAheadFlag = atoi((char *) listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TRAINING_MODE") == SUCCESS)
			{
				stSessDtls.iDemoModeFlag = (*((int *)listPtr[iCnt].value));
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "MERCHANT_INDEX") == SUCCESS)
			{
				strcpy(stSessDtls.szMerchantIndex, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "SCREEN_CHANGE") == SUCCESS)
			{
				stSessDtls.bScreenChangereqd = (*((int *)listPtr[iCnt].value));
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "NFCVAS_MODE") == SUCCESS)
			{
				strcpy(stSessDtls.szNFCVASMode, listPtr[iCnt].value);
				jCnt++;
		}
		}

		if(jCnt > 0)
		{
			if(*dpstSessDtls == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstSessDtls = (SESSDTLS_PTYPE) malloc(iSize);
				if(*dpstSessDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstSessDtls, 0x00, iSize);
			memcpy(*dpstSessDtls, &stSessDtls, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveAddLIReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveAddLIReq(KEYVAL_PTYPE listPtr, LIINFO_PTYPE * dpstLIInfo)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	LIINFO_STYPE	stLIInfo;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* TODO: Complete this */
	while(1)
	{
		if(*dpstLIInfo != NULL)
		{
			memcpy(&stLIInfo, *dpstLIInfo, sizeof(LIINFO_STYPE));
		}
		else
		{
			memset(&stLIInfo, 0x00, sizeof(LIINFO_STYPE));
		}

		iTotCnt = sizeof(liAddReqLst) / KEYVAL_SIZE;

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "RUNNING_TAX_AMOUNT") == SUCCESS)
			{
				strcpy(stLIInfo.runTaxAmt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "RUNNING_TRANS_AMOUNT") == SUCCESS)
			{
				strcpy(stLIInfo.runTranAmt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "RUNNING_SUB_TOTAL") == SUCCESS)
			{
				strcpy(stLIInfo.runSubTtlAmt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "LINE_ITEMS") == SUCCESS)
			{
				jCnt += addReqLineItems(&stLIInfo, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstLIInfo == NULL)
			{
				*dpstLIInfo = (LIINFO_PTYPE) malloc(sizeof(LIINFO_STYPE));
				if(*dpstLIInfo == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memcpy(*dpstLIInfo, &stLIInfo, sizeof(LIINFO_STYPE));
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addReqLineItems
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addReqLineItems(LIINFO_PTYPE pstLIInfo, VAL_LST_PTYPE pstValLst)
{
	int				rv				= SUCCESS;
	LI_NODE_PTYPE	pstLINode		= NULL;
	VAL_NODE_PTYPE	nodePtr			= NULL;
	LI_NODE_STYPE	stLINode;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	nodePtr = pstValLst->start;

	while(nodePtr != NULL)
	{
		memset(&stLINode, 0x00, sizeof(LI_NODE_STYPE));

		if(nodePtr->type == MERCHANDISE)
		{
			rv = saveMerchandiseLI(&stLINode, nodePtr);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to store merchandise",
								__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else if(nodePtr->type == OFFER)
		{
			rv = saveOfferLI(&stLINode, nodePtr);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to store offer",
								__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid line item type %d",
										__FUNCTION__, nodePtr->type);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		pstLINode = (LI_NODE_PTYPE) malloc(sizeof(LI_NODE_STYPE));
		if(pstLINode == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(pstLINode, 0x00, sizeof(LI_NODE_STYPE));

		memcpy(pstLINode, &stLINode, sizeof(LI_NODE_STYPE));

		if(pstLIInfo->mainLstHead == NULL)
		{
			pstLIInfo->mainLstHead = pstLINode;
			pstLIInfo->mainLstTail = pstLINode;
		}
		else
		{
			pstLIInfo->mainLstTail->nextLI = pstLINode;
			pstLIInfo->mainLstTail = pstLINode;
		}

		pstLINode = NULL;
		nodePtr = nodePtr->next;
	}

	if(rv != SUCCESS)
	{
		LI_NODE_PTYPE	pstTmpNode	= NULL;

		pstLINode = pstLIInfo->mainLstHead;
		while(pstLINode != NULL)
		{
			if(pstLINode->liData != NULL)
			{
				free(pstLINode->liData);
			}

			pstTmpNode = pstLINode;
			pstLINode  = pstLINode->nextLI;
			free(pstTmpNode);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveRequestedLiChangeItems
 *
 * Description	:  This function will parse the requested Remove/Override Line
 * 					items and store in the sciReq link list in session.
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveRequestedLiChangeItems(LIINFO_PTYPE pstLIInfo, VAL_LST_PTYPE pstValLst, LI_NODE_PTYPE pstOutLINode)
{
	int				rv				= SUCCESS;
	LI_NODE_PTYPE	pstLINode		= NULL;
	VAL_NODE_PTYPE	nodePtr			= NULL;
	LI_NODE_STYPE	stLINode;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pstOutLINode != NULL && pstOutLINode->liType == MERCHANDISE)
	{
		debug_sprintf(szDbgMsg, "%s: Got a LI Outside LINE_ITEMS Adding it As Head in Node List", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pstLINode = (LI_NODE_PTYPE) malloc(sizeof(LI_NODE_STYPE));
		if(pstLINode == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			return rv;
		}
		memset(pstLINode, 0x00, sizeof(LI_NODE_STYPE));

		memcpy(pstLINode, pstOutLINode, sizeof(LI_NODE_STYPE));

		pstLINode->liData = (MCNT_LI_PTYPE) malloc(sizeof(MCNT_LI_STYPE));
		if(pstLINode->liData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(pstLINode != NULL)
			{
				free(pstLINode);
			}

			rv = FAILURE;
			return rv;
		}

		memcpy(pstLINode->liData, pstOutLINode->liData, sizeof(MCNT_LI_STYPE));

		pstLIInfo->mainLstHead = pstLINode;
		pstLIInfo->mainLstTail = pstLINode;
	}

	if(pstValLst != NULL)
	{
		nodePtr = pstValLst->start;

		while(nodePtr != NULL)
		{
			memset(&stLINode, 0x00, sizeof(LI_NODE_STYPE));

			if(nodePtr->type == MERCHANDISE)
			{
				rv = saveMerchandiseLI(&stLINode, nodePtr);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to store merchandise",
							__FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}
			}
			else if(nodePtr->type == OFFER)
			{
				rv = saveOfferLI(&stLINode, nodePtr);
				if(rv != SUCCESS)
				{
					debug_sprintf(szDbgMsg, "%s: FAILED to store offer",
							__FUNCTION__);
					APP_TRACE(szDbgMsg);

					break;
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Invalid line item type %d",
						__FUNCTION__, nodePtr->type);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}

			pstLINode = (LI_NODE_PTYPE) malloc(sizeof(LI_NODE_STYPE));
			if(pstLINode == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
			memset(pstLINode, 0x00, sizeof(LI_NODE_STYPE));

			memcpy(pstLINode, &stLINode, sizeof(LI_NODE_STYPE));

			if(pstLIInfo->mainLstHead == NULL)
			{
				pstLIInfo->mainLstHead = pstLINode;
				pstLIInfo->mainLstTail = pstLINode;
			}
			else
			{
				pstLIInfo->mainLstTail->nextLI = pstLINode;
				pstLIInfo->mainLstTail = pstLINode;
			}

			pstLINode = NULL;
			nodePtr = nodePtr->next;
		}
	}

	if(rv != SUCCESS)
	{
		LI_NODE_PTYPE	pstTmpNode	= NULL;

		pstLINode = pstLIInfo->mainLstHead;
		while(pstLINode != NULL)
		{
			if(pstLINode->liData != NULL)
			{
				free(pstLINode->liData);
			}

			pstTmpNode = pstLINode;
			pstLINode  = pstLINode->nextLI;
			free(pstTmpNode);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveMerchandiseLI
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveMerchandiseLI(LI_NODE_PTYPE pstLINode,VAL_NODE_PTYPE pstValNode)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	MCNT_LI_STYPE	stMcntDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif
//
//	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
//	APP_TRACE(szDbgMsg);

	while(1)
	{
		listPtr = pstValNode->elemList;
		iTotCnt = pstValNode->elemCnt;
		memset(&stMcntDtls, 0x00, sizeof(MCNT_LI_STYPE));

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "LINE_ITEM_ID") == SUCCESS)
			{
				strcpy(pstLINode->szLiNo, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "SKU") == SUCCESS)
			{
				strcpy(pstLINode->szSKU, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "UPC") == SUCCESS)
			{
				strcpy(stMcntDtls.szUPC, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "DESCRIPTION") == SUCCESS)
			{
				strcpy(pstLINode->szDesc, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "QUANTITY") == SUCCESS)
			{
				strcpy(stMcntDtls.szQty, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "UNIT_PRICE") == SUCCESS)
			{
				strcpy(stMcntDtls.szUnitPrice, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "EXTENDED_PRICE") == SUCCESS)
			{
				strcpy(stMcntDtls.szExtPrice, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "FONT_COL_VALUE") == SUCCESS)
			{
				strcpy(pstLINode->szLiFontCol, listPtr[iCnt].value);
			}
		}

		pstLINode->liData = (MCNT_LI_PTYPE) malloc(sizeof(MCNT_LI_STYPE));
		if(pstLINode->liData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memcpy(pstLINode->liData, &stMcntDtls, sizeof(MCNT_LI_STYPE));
		pstLINode->liType = MERCHANDISE;

		break;
	}

//	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
//	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveOfferLI
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveOfferLI(LI_NODE_PTYPE pstLINode, VAL_NODE_PTYPE pstValNode)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	KEYVAL_PTYPE	listPtr			= NULL;
	OFF_LI_STYPE	stOfferDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		listPtr = pstValNode->elemList;
		iTotCnt = pstValNode->elemCnt;
		memset(&stOfferDtls, 0x00, sizeof(OFF_LI_STYPE));

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "TYPE") == SUCCESS)
			{
				stOfferDtls.eOfferType = *((int *) (listPtr[iCnt].value));
			}
			else if(strcmp(listPtr[iCnt].key, "LINE_ITEM_ID") == SUCCESS)
			{
				strcpy(pstLINode->szLiNo, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "SKU") == SUCCESS)
			{
				strcpy(pstLINode->szSKU, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "DESCRIPTION") == SUCCESS)
			{
				strcpy(pstLINode->szDesc, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "OFFER_AMOUNT") == SUCCESS)
			{
				strcpy(stOfferDtls.szOfferAmt, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "OFFER_LINE_ITEM") == SUCCESS)
			{
				strcpy(stOfferDtls.szOfferLI, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "FONT_COL_VALUE") == SUCCESS)
			{
				strcpy(pstLINode->szLiFontCol, listPtr[iCnt].value);
			}
		}

		pstLINode->liData = (OFF_LI_PTYPE) malloc(sizeof(OFF_LI_STYPE));
		if(pstLINode->liData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memcpy(pstLINode->liData, &stOfferDtls, sizeof(OFF_LI_STYPE));
		pstLINode->liType = OFFER;

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveLIChgDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveLIChgDtls(KEYVAL_PTYPE listPtr, LIINFO_PTYPE * dpstLIInfo)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	VAL_LST_PTYPE	pTmpValues		= NULL;
	//MCNT_LI_PTYPE	pstMcntDtls		= NULL;
	//LI_NODE_PTYPE	pstLINode		= NULL;
	LIINFO_STYPE	stLIInfo;
	LI_NODE_STYPE	stLINode;
	MCNT_LI_STYPE	stMcntDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* TODO: Complete this */
	while(1)
	{
		if(*dpstLIInfo != NULL)
		{
			memcpy(&stLIInfo, *dpstLIInfo, sizeof(LIINFO_STYPE));
		}
		else
		{
			memset(&stLIInfo, 0x00, sizeof(LIINFO_STYPE));
		}

		memset(&stMcntDtls, 0x00, sizeof(MCNT_LI_STYPE));
		memset(&stLINode, 0x00, sizeof(LI_NODE_STYPE));

#if 0
		pstMcntDtls = (MCNT_LI_PTYPE) malloc(sizeof(MCNT_LI_STYPE));
		if(pstMcntDtls == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED for mcnt dtls",
														__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		pstLINode = (LI_NODE_PTYPE) malloc(sizeof(LI_NODE_STYPE));
		if(pstLINode == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc FAILED for LI node",
														__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pstMcntDtls, 0x00, sizeof(MCNT_LI_STYPE));
		memset(pstLINode, 0x00, sizeof(LI_NODE_STYPE));
		pstLINode->liData = pstMcntDtls;
		pstLINode->liType = MERCHANDISE;

		stLIInfo.mainLstHead  = pstLINode;
		stLIInfo.mainLstTail  = pstLINode;
#endif

		iTotCnt = sizeof(liChngReqLst) / KEYVAL_SIZE;

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "LINE_ITEM_ID") == SUCCESS)
			{
				strcpy(stLINode.szLiNo, listPtr[iCnt].value);
				stLINode.liType = MERCHANDISE;
				stLINode.liData = (MCNT_LI_PTYPE) (&stMcntDtls);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "QUANTITY") == SUCCESS)
			{
				strcpy(stMcntDtls.szQty, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DESCRIPTION") == SUCCESS)
			{
				strcpy(stLINode.szDesc, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "UNIT_PRICE") == SUCCESS)
			{
				strcpy(stMcntDtls.szUnitPrice, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "EXTENDED_PRICE") == SUCCESS)
			{
				strcpy(stMcntDtls.szExtPrice, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "RUNNING_TAX_AMOUNT") == SUCCESS)
			{
				strcpy(stLIInfo.runTaxAmt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "RUNNING_TRANS_AMOUNT") == SUCCESS)
			{
				strcpy(stLIInfo.runTranAmt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "RUNNING_SUB_TOTAL") == SUCCESS)
			{
				strcpy(stLIInfo.runSubTtlAmt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "FONT_COL_VALUE") == SUCCESS)
			{
				strcpy(stLINode.szLiFontCol, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "LINE_ITEMS") == SUCCESS)
			{
				//jCnt += saveRequestedLiChangeItems(&stLIInfo, listPtr[iCnt].value);
				pTmpValues = (VAL_LST_PTYPE) listPtr[iCnt].value;
				jCnt++;
			}
		}

		jCnt += saveRequestedLiChangeItems(&stLIInfo, pTmpValues, &stLINode);

		if(jCnt > 0)
		{
			if(*dpstLIInfo == NULL)
			{
				*dpstLIInfo = (LIINFO_PTYPE) malloc(sizeof(LIINFO_STYPE));
				if(*dpstLIInfo == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED for li info",
							__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memcpy(*dpstLIInfo, &stLIInfo, sizeof(LIINFO_STYPE));
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveSAFReqDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveSAFReqDtls(KEYVAL_PTYPE listPtr, SAFDTLS_PTYPE * dpstSAF)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	SAFDTLS_STYPE	stSAFDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(SAFDTLS_STYPE);

	while(1)
	{
		if(*dpstSAF != NULL)
		{
			memcpy(&stSAFDtls, *dpstSAF, iSize);
		}
		else
		{
			memset(&stSAFDtls, 0x00, iSize);
		}

		iTotCnt = sizeof(safReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "SAF_NUM_BEGIN") == SUCCESS)
			{
				strcpy(stSAFDtls.szBegNo, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "SAF_NUM_END") == SUCCESS)
			{
				strcpy(stSAFDtls.szEndNo, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "SAF_STATUS") == SUCCESS)
			{
				strcpy(stSAFDtls.szStatus, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "SAF_TOR_NUM_BEGIN") == SUCCESS)
			{
				strcpy(stSAFDtls.szTORBegNo, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "SAF_TOR_NUM_END") == SUCCESS)
			{
				strcpy(stSAFDtls.szTOREndNo, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "SAF_TOR_STATUS") == SUCCESS)
			{
				strcpy(stSAFDtls.szTORStatus, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstSAF == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstSAF = (SAFDTLS_PTYPE) malloc(iSize);
				if(*dpstSAF == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstSAF, 0x00, iSize);
			memcpy(*dpstSAF, &stSAFDtls, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveLPTokenDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveLPTokenDtls(KEYVAL_PTYPE listPtr, char **pszTranKey)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	char *			szTranKey		= NULL;
	KEYVAL_PTYPE	curPtr			= NULL;
	TRAN_PTYPE		pstSSITran		= NULL;
	PASSTHRU_STYPE	stPassThru;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		curPtr = listPtr;
		memset(&stPassThru, 0x00, sizeof(PASSTHRU_STYPE));

		/* save the general pymt details */
		iCnt = saveGenPymtRptDtls(curPtr, &stPassThru);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save gen pymt Report details", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		/*
		 * Required field for all authentication types
		 * Payment type should be filled for the LP Token query
		 */

		stPassThru.iPymtType = PYMT_CREDIT;

		/* Create a transaction stack for this payment request */
		szTranKey = (char *) malloc(sizeof(char) * 10);
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: key Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(szTranKey, 0x00, sizeof(char) * 10);

		/* Create a new SSI transaction */
		rv = pushNewSSITran(szTranKey);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create SSI transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Get a reference to this new SSI transaction (stored in stack) */
		rv = getTopSSITran(szTranKey, &pstSSITran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to access SSI transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		pstSSITran->data = (PASSTHRU_PTYPE) malloc(sizeof(PASSTHRU_STYPE));
		if(pstSSITran->data == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: MAlloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset((PASSTHRU_PTYPE)pstSSITran->data, 0x00, sizeof(PASSTHRU_STYPE));

		memcpy(pstSSITran->data, &stPassThru, sizeof(PASSTHRU_STYPE));
		*pszTranKey = szTranKey;

		break;
	}

	if(rv != SUCCESS)
	{
		/* Free any memory allocated to the data for the payment transaction */
		if(szTranKey != NULL)
		{
			free(szTranKey);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveCounterDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveCounterDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(getCounterReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "MAC_LABEL") == SUCCESS)
			{
				strcpy(stDevTran.stCounterDtls.szMacLabel, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: saveTimeDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveTimeDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(setTimeReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "TIME") == SUCCESS)
			{
				strcpy(stDevTran.stTimeDtls.szTimeTxt, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveLaneClosedDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveLaneClosedDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(laneClosedReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT") == SUCCESS)
			{
				strcpy(stDevTran.stLaneClosed.szDisplayText, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "FONT_SIZE") == SUCCESS)
			{
				stDevTran.stLaneClosed.iFontSize = atoi((char *) (listPtr[iCnt].value));
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "FONT_COL_VALUE") == SUCCESS)
			{
				strcpy(stDevTran.stLaneClosed.szFontColor, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveVersionReqDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveVersionReqDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(stVersionLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_VERSION") == SUCCESS)
			{
				stDevTran.stVerDtls.iDisplayFlag = atoi((char *)listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveEmailReqDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveEmailReqDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(emailReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "EMAILDATA") == SUCCESS)
			{
				strcpy(stDevTran.stLtyDtls.szEmail, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveCharityReqDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveCharityReqDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(charityReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT1") == SUCCESS)
			{
				strcpy(stDevTran.stCharityDtls.szDispTxt1, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT2") == SUCCESS)
			{
				strcpy(stDevTran.stCharityDtls.szDispTxt2, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT3") == SUCCESS)
			{
				strcpy(stDevTran.stCharityDtls.szDispTxt3, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT4") == SUCCESS)
			{
				strcpy(stDevTran.stCharityDtls.szDispTxt4, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT5") == SUCCESS)
			{
				strcpy(stDevTran.stCharityDtls.szDispTxt5, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AMOUNT1") == SUCCESS)
			{
				strcpy(stDevTran.stCharityDtls.szAmount1, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AMOUNT2") == SUCCESS)
			{
				strcpy(stDevTran.stCharityDtls.szAmount2, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AMOUNT3") == SUCCESS)
			{
				strcpy(stDevTran.stCharityDtls.szAmount3, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AMOUNT4") == SUCCESS)
			{
				strcpy(stDevTran.stCharityDtls.szAmount4, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AMOUNT5") == SUCCESS)
			{
				strcpy(stDevTran.stCharityDtls.szAmount5, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveCreditAppReqDtls
 *
 * Description	: Saves the Credit Application Request Related Details to data System
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveCreditAppReqDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{

	int				rv						= SUCCESS;
	int				iCnt					= 0;
	int				jCnt					= 0;
	int				iPromptCnt				= 0;
	int				iTotCnt					= 0;
	int				iSize					= 0;
	char			szPromptTxt[50]			= "";
	char			szPromptDataTxt[50]		= "";
	char			szPromptTypeTxt[50]		= "";
	char			szPromptMinChars[50]	= "";
	char			szPromptMaxChars[50]	= "";
	char			szPromptFormatTxt[50]	= "";
	char			szPromptMaskTxt[50]		= "";
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(creditAppReqLst) / KEYVAL_SIZE;
		iPromptCnt += 1;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "FORWARD_PROXY") == SUCCESS)
			{
				strcpy(stDevTran.stCreditAppDtls.szForwardProxy, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PROXY_TIMEOUT") == SUCCESS)
			{
				stDevTran.stCreditAppDtls.iProxyTimeout = atoi((char *) (listPtr[iCnt].value));
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PROXY_REF") == SUCCESS)
			{
				strcpy(stDevTran.stCreditAppDtls.szProxyRef, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "RETURN_SCREEN") == SUCCESS)
			{
				stDevTran.stCreditAppDtls.iRetScreen = *( (int *) (listPtr[iCnt].value) );
				jCnt++;
			}
			else if(strncmp(listPtr[iCnt].key, "PROMPT", 6) == SUCCESS)
			{
				for(iPromptCnt = 1; iPromptCnt <= CREDIT_APP_MAXPROMPTS; iPromptCnt++ )
				{
					memset(szPromptTxt, 0x00, sizeof(szPromptTxt));
					memset(szPromptDataTxt, 0x00, sizeof(szPromptDataTxt));
					memset(szPromptTypeTxt, 0x00, sizeof(szPromptTypeTxt));
					memset(szPromptMinChars, 0x00, sizeof(szPromptMinChars));
					memset(szPromptMaxChars, 0x00, sizeof(szPromptMaxChars));
					memset(szPromptFormatTxt, 0x00, sizeof(szPromptFormatTxt));
					memset(szPromptMaskTxt, 0x00, sizeof(szPromptMaskTxt));

					sprintf(szPromptTxt, "PROMPT%d", iPromptCnt);
					sprintf(szPromptDataTxt, "PROMPT%d_DATA", iPromptCnt);
					sprintf(szPromptTypeTxt, "PROMPT%d_TYPE", iPromptCnt);
					sprintf(szPromptMinChars, "PROMPT%d_MIN", iPromptCnt);
					sprintf(szPromptMaxChars, "PROMPT%d_MAX", iPromptCnt);
					sprintf(szPromptFormatTxt, "PROMPT%d_FORMAT", iPromptCnt);
					sprintf(szPromptMaskTxt, "PROMPT%d_MASK", iPromptCnt);

					if(strcmp(listPtr[iCnt].key, szPromptTxt) == SUCCESS)
					{
						strcpy(stDevTran.stCreditAppDtls.stPromptDtls[iPromptCnt - 1].szPromptText, listPtr[iCnt].value);

						/* If we come across any prompt texts, and suppose the other fields are not present
						 * in the credit application request, set it to default values
						 * */
						jCnt++;
						break;
					}
					else if(strcmp(listPtr[iCnt].key, szPromptDataTxt) == SUCCESS)
					{
						strcpy(stDevTran.stCreditAppDtls.stPromptDtls[iPromptCnt - 1].szPromptData, listPtr[iCnt].value);
						jCnt++;
						break;
					}
					else if(strcmp(listPtr[iCnt].key, szPromptTypeTxt) == SUCCESS)
					{
						stDevTran.stCreditAppDtls.stPromptDtls[iPromptCnt - 1].iPromptType = *( (int *) (listPtr[iCnt].value) );
						jCnt++;
						break;
					}
					else if(strcmp(listPtr[iCnt].key, szPromptMinChars) == SUCCESS)
					{
						stDevTran.stCreditAppDtls.stPromptDtls[iPromptCnt - 1].iPromptMinChars = atoi((char *) listPtr[iCnt].value);
						jCnt++;
						break;
					}
					else if(strcmp(listPtr[iCnt].key, szPromptMaxChars) == SUCCESS)
					{
						stDevTran.stCreditAppDtls.stPromptDtls[iPromptCnt - 1].iPromptMaxChars = atoi((char *) listPtr[iCnt].value);
						jCnt++;
						break;
					}
					else if(strcmp(listPtr[iCnt].key, szPromptFormatTxt) == SUCCESS)
					{
						strcpy(stDevTran.stCreditAppDtls.stPromptDtls[iPromptCnt - 1].szPromptFormat, listPtr[iCnt].value);
						jCnt++;
						break;
					}
					else if(strcmp(listPtr[iCnt].key, szPromptMaskTxt) == SUCCESS)
					{
						stDevTran.stCreditAppDtls.stPromptDtls[iPromptCnt - 1].iPromptMaskType = *( (int *) (listPtr[iCnt].value) );
						jCnt++;
						break;
					}

				}
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the credit application details request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveCustButtonReqDtls
 *
 * Description	: Saves the Customer Button Related Details to data System
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveCustButtonReqDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(custButtReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT1") == SUCCESS)
			{
				strcpy(stDevTran.stCustButtonDtls.szDispTxt1, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT2") == SUCCESS)
			{
				strcpy(stDevTran.stCustButtonDtls.szDispTxt2, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT3") == SUCCESS)
			{
				strcpy(stDevTran.stCustButtonDtls.szDispTxt3, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT4") == SUCCESS)
			{
				strcpy(stDevTran.stCustButtonDtls.szDispTxt4, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT5") == SUCCESS)
			{
				strcpy(stDevTran.stCustButtonDtls.szDispTxt5, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "BUTTON_LABEL1") == SUCCESS)
			{
				strcpy(stDevTran.stCustButtonDtls.szButtonLabel1, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "BUTTON_LABEL2") == SUCCESS)
			{
				strcpy(stDevTran.stCustButtonDtls.szButtonLabel2, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "BUTTON_LABEL3") == SUCCESS)
			{
				strcpy(stDevTran.stCustButtonDtls.szButtonLabel3, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "BUTTON_LABEL4") == SUCCESS)
			{
				strcpy(stDevTran.stCustButtonDtls.szButtonLabel4, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "BUTTON_LABEL5") == SUCCESS)
			{
				strcpy(stDevTran.stCustButtonDtls.szButtonLabel5, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "BUTTON_LABEL6") == SUCCESS)
			{
				strcpy(stDevTran.stCustButtonDtls.szButtonLabel6, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveSetDevNameReqDtls
 *
 * Description	: Saves the device name sent by POS into the data system.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int saveSetDevNameReqDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(stSetDevNameLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "DEVICENAME") == SUCCESS)
			{
				strcpy(stDevTran.stDevNameDtls.szDeviceName, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the device name details for the
				 * set device name request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveLoyaltyReqDtls
 *
 * Description	: Saves the Loyalty request details by POS into the data system.
 *
 * Input Params	:
 *
 * Output Params: SUCCESS/FAILURE
 * ============================================================================
 */
static int saveLoyaltyReqDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(stLoyaltyCapLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "TYPE") == SUCCESS)
			{
				strcpy(stDevTran.stLtyDtls.szInputType, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the device name details for the
				 * set device name request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveSurveyReqDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveSurveyReqDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(surveyReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT1") == SUCCESS)
			{
				strcpy(stDevTran.stSurveyDtls.szDispTxt1, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT2") == SUCCESS)
			{
				strcpy(stDevTran.stSurveyDtls.szDispTxt2, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT3") == SUCCESS)
			{
				strcpy(stDevTran.stSurveyDtls.szDispTxt3, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT4") == SUCCESS)
			{
				strcpy(stDevTran.stSurveyDtls.szDispTxt4, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT5") == SUCCESS)
			{
				strcpy(stDevTran.stSurveyDtls.szDispTxt5, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT6") == SUCCESS)
			{
				strcpy(stDevTran.stSurveyDtls.szDispTxt6, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT7") == SUCCESS)
			{
				strcpy(stDevTran.stSurveyDtls.szDispTxt7, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT8") == SUCCESS)
			{
				strcpy(stDevTran.stSurveyDtls.szDispTxt8, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT9") == SUCCESS)
			{
				strcpy(stDevTran.stSurveyDtls.szDispTxt9, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT10") == SUCCESS)
			{
				strcpy(stDevTran.stSurveyDtls.szDispTxt10, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_BULKDATA") == SUCCESS)
			{
				strcpy(stDevTran.stSurveyDtls.szDispBulkTxt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "RETURN_SCREEN") == SUCCESS)
			{
				stDevTran.stSurveyDtls.iRetScreen = *( (int *) (listPtr[iCnt].value) );
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveSignDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveSignDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(sigReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT") == SUCCESS)
			{
				strcpy(stDevTran.stSigDtls.szDispTxt, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: savePymtReqDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int savePymtReqDtls(KEYVAL_PTYPE listPtr, char ** pszTranKey)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	char *			szTranKey		= NULL;
	KEYVAL_PTYPE	curPtr			= NULL;
	TRAN_PTYPE		pstSSITran		= NULL;
	PYMTTRAN_STYPE	stPymtTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		curPtr = listPtr;
		memset(&stPymtTran, 0x00, sizeof(PYMTTRAN_STYPE));

		/*
		 * Setting Default value as -1 for Payment type
		 * This is done to support if valid PAYMENT_TYPE is present
		 * in the SCI request, we will not show the tender selection screen
		 * If you dont set to -1 here, after memset the value is 0
		 * The enum value for PYMT_CREDIT is 0... if this tag is not present
		 * in the SCI request also, it will consider it as CREDIT payment type
		 */
		stPymtTran.iPymtType = -1;

		/* save the general pymt details */
		iCnt = saveGenPymtDtls(curPtr, &stPymtTran);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save gen pymt details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* save the amount details */
		iCnt = savePymtAmtDtls(curPtr, &(stPymtTran.pstAmtDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save pymt amount details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* save the follow on details */
		iCnt = saveFollowOnDtls(curPtr, &(stPymtTran.pstFTranDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save follow on details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* save the level 2 (tax) details */
		iCnt = saveTaxDtls(curPtr, &(stPymtTran.pstLevel2Dtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save pymt tax details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* save the check details */
		iCnt = saveChkDtls(curPtr, &(stPymtTran.pstChkDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save pymt check details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* save the check details */
		iCnt = saveCustInfoDtls(curPtr, &(stPymtTran.pstCustInfoDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save pymt cust info details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* save the Orig Tran Date/Time */
		iCnt = saveOrigTranDtls(curPtr, &stPymtTran);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save orig tran date/time details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* save the card expiry check details */
		iCnt = saveCardExpDtls(curPtr, &(stPymtTran.pstCardDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save card expiry info details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* save the Duplicate swipe detect details */
		iCnt = saveDupSwipeDetectDtls(curPtr, &stPymtTran);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Duplicate Swipe Detect info details",
																				__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* save the Duplicate swipe detect details */
		iCnt = saveAttributeDtls(curPtr, &stPymtTran);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save Payment Attributes info details",
																				__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* Save the EBT Transaction details */
		iCnt = saveEBTTransDtls(curPtr, &(stPymtTran.pstEBTDtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save EBT Transaction info details",
																				__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* Save the FSA Transaction details */
		iCnt = saveFSATransDtls(curPtr, &(stPymtTran.pstFSADtls));
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save FSA Transaction info details",
																				__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* save the additional pymt details */
		iCnt = saveAdditionalPymtDtls(curPtr, &stPymtTran);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save additional pymt details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* save pass through request fields details into data system */
		iCnt = savePassThrgReqFields(curPtr, &stPymtTran);
		if(iCnt < 0)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to save pass through fields details",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		curPtr += iCnt;

		/* Create a transaction stack for this payment request */
		szTranKey = (char *) malloc(sizeof(char) * 10);
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: key Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(szTranKey, 0x00, sizeof(char) * 10);

		/* Create a new SSI transaction */
		rv = pushNewSSITran(szTranKey);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create SSI transaction",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Get a reference to this new SSI transaction (stored in stack) */
		rv = getTopSSITran(szTranKey, &pstSSITran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to access SSI transaction",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		pstSSITran->data = (PYMTTRAN_PTYPE) malloc(sizeof(PYMTTRAN_STYPE));
		if(pstSSITran->data == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: MAlloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset((PYMTTRAN_PTYPE)pstSSITran->data, 0x00, sizeof(PYMTTRAN_STYPE));

		memcpy(pstSSITran->data, &stPymtTran, sizeof(PYMTTRAN_STYPE));
		*pszTranKey = szTranKey;

		break;
	}

	if(rv != SUCCESS)
	{
		/* Free any memory allocated to the data for the payment transaction */

		if(stPymtTran.pstAmtDtls != NULL)
		{
			free(stPymtTran.pstAmtDtls);
		}

		if(stPymtTran.pstFTranDtls != NULL)
		{
			free(stPymtTran.pstFTranDtls);
		}

		if(stPymtTran.pstLevel2Dtls != NULL)
		{
			free(stPymtTran.pstLevel2Dtls);
		}

		if(szTranKey != NULL)
		{
			free(szTranKey);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveDupSwipeDetectDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveDupSwipeDetectDtls(KEYVAL_PTYPE listPtr, PYMTTRAN_PTYPE pstPymtTran)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		iTotCnt			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(dupSwipeDetect) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		if(listPtr[iCnt].value == NULL)
		{
			continue;
		}
		else if(strcmp(listPtr[iCnt].key, "ALLOW_DUP_TRAN") == SUCCESS)
		{
			memset(pstPymtTran->szAllowDupTran, 0x00, sizeof(pstPymtTran->szAllowDupTran));
			strcpy(pstPymtTran->szAllowDupTran, listPtr[iCnt].value);
		}
	}

	rv = iTotCnt;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveCardExpDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveCardExpDtls(KEYVAL_PTYPE listPtr, CARDDTLS_PTYPE * pstCardDtls)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				jCnt			= 0;
	int				iSize			= 0;
	CARDDTLS_STYPE	stCardDtls;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(CARDDTLS_STYPE);

	while(1)
	{
		memset(&stCardDtls, 0x00, iSize);
		if(*pstCardDtls != NULL)
		{
			memcpy(&stCardDtls, *pstCardDtls, iSize);
		}

		iTotCnt = sizeof(pymtCardExpLst) / KEYVAL_SIZE;

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "CARD_EXP_MONTH") == SUCCESS)
			{
				memset(stCardDtls.szExpMon, 0x00, sizeof(stCardDtls.szExpMon));
				strcpy(stCardDtls.szExpMon, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CARD_EXP_YEAR") == SUCCESS)
			{
				memset(stCardDtls.szExpYear, 0x00, sizeof(stCardDtls.szExpYear));
				strcpy(stCardDtls.szExpYear, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*pstCardDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Allocating memory for Card details struct", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* Allocate memory to card expiry details */
				*pstCardDtls = (CARDDTLS_PTYPE) malloc(iSize);
				if(*pstCardDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(*pstCardDtls, 0x00, iSize);
			}
			memcpy(*pstCardDtls, &stCardDtls, iSize);
		}
		//T_POLISETTYG1:assigned rv inside while loop becuase if malloc fails it will return FAILURE
		rv = iTotCnt;
		break;
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveOrigTranDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveOrigTranDtls(KEYVAL_PTYPE listPtr, PYMTTRAN_PTYPE pstPymtTran)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		iTotCnt			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(pymtOrigTranDateLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		if(listPtr[iCnt].value == NULL)
		{
			continue;
		}
		else if(strcmp(listPtr[iCnt].key, "ORIG_TRANS_DATE") == SUCCESS)
		{
			memset(pstPymtTran->szOrigTranDt, 0x00, sizeof(pstPymtTran->szOrigTranDt));
			strcpy(pstPymtTran->szOrigTranDt, listPtr[iCnt].value);
		}
		else if(strcmp(listPtr[iCnt].key, "ORIG_TRANS_TIME") == SUCCESS)
		{
			memset(pstPymtTran->szOrigTranTm, 0x00, sizeof(pstPymtTran->szOrigTranTm));
			strcpy(pstPymtTran->szOrigTranTm, listPtr[iCnt].value);
		}
	}

	rv = iTotCnt;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveAdditionalPymtDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveAdditionalPymtDtls(KEYVAL_PTYPE listPtr, PYMTTRAN_PTYPE pstPymtTran)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		jCnt			= 0;
	int		iTotCnt			= 0;
	int 	iSize			= 0;
	int 	iSizeCrdDtls	= 0;
	POSTENDERDTLS_STYPE		stPOSTenderDtls;
	CARDDTLS_STYPE			stCardDtls;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(POSTENDERDTLS_STYPE);
	iSizeCrdDtls = sizeof(CARDDTLS_STYPE);

	while(1)
	{
		memset(&stPOSTenderDtls, 0x00, iSize);
		if(pstPymtTran->pstPOSTenderDtls != NULL )
		{
			memcpy(&stPOSTenderDtls, pstPymtTran->pstPOSTenderDtls, iSize);
		}

		memset(&stCardDtls, 0x00, iSizeCrdDtls);
		if(pstPymtTran->pstCardDtls != NULL )
		{
			memcpy(&stCardDtls, pstPymtTran->pstCardDtls, iSizeCrdDtls);
		}

		iTotCnt = sizeof(addPymtReqLst) / KEYVAL_SIZE;

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "PROMO_CODE") == SUCCESS)
			{
				memset(pstPymtTran->szPromoCode, 0x00, sizeof(pstPymtTran->szPromoCode));
				strcpy(pstPymtTran->szPromoCode, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "CREDIT_PLAN_NBR") == SUCCESS)
			{
				memset(pstPymtTran->szCreditPlanNBR, 0x00, sizeof(pstPymtTran->szCreditPlanNBR));
				strcpy(pstPymtTran->szCreditPlanNBR, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "ORDER_DATETIME") == SUCCESS)
			{
				memset(pstPymtTran->szOrderDtTm, 0x00, sizeof(pstPymtTran->szOrderDtTm));
				strcpy(pstPymtTran->szOrderDtTm, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "PURCHASE_APR") == SUCCESS)
			{
				memset(pstPymtTran->szPurchaseApr, 0x00, sizeof(pstPymtTran->szPurchaseApr));
				strcpy(pstPymtTran->szPurchaseApr, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "POS_TENDER1") == SUCCESS)
			{
				strcpy(stPOSTenderDtls.szPOSTender[0], (char *)(listPtr[iCnt].value));
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "POS_TENDER2") == SUCCESS)
			{
				strcpy(stPOSTenderDtls.szPOSTender[1], (char *)(listPtr[iCnt].value));
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "POS_TENDER3") == SUCCESS)
			{
				strcpy(stPOSTenderDtls.szPOSTender[2], (char *)(listPtr[iCnt].value));
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TENDER1_ACTION") == SUCCESS)
			{
				stPOSTenderDtls.iPOSTenderAction[0] = *( (int *) (listPtr[iCnt].value) );
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TENDER2_ACTION") == SUCCESS)
			{
				stPOSTenderDtls.iPOSTenderAction[1] = *( (int *) (listPtr[iCnt].value) );
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TENDER3_ACTION") == SUCCESS)
			{
				stPOSTenderDtls.iPOSTenderAction[2] = *( (int *) (listPtr[iCnt].value) );
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "ORIG_ACCT_NUM") == SUCCESS)
			{
				strcpy(stCardDtls.szOrigPAN, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "CAPTURECARD_EARLYRETURN") == SUCCESS)
			{
				pstPymtTran->bEarlyRtrnCardFlag = *( (int *) (listPtr[iCnt].value) );
			}
			else if(strcmp(listPtr[iCnt].key, "APR_TYPE") == SUCCESS)
			{
				memset(pstPymtTran->szAPRType, 0x00, sizeof(pstPymtTran->szAPRType));
				strcpy(pstPymtTran->szAPRType, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "DCC_NOT_ALLOWED") == SUCCESS)
			{
				pstPymtTran->bDCCNotAllowed = (*( (int *) (listPtr[iCnt].value) ));
			}
			else if(strcmp(listPtr[iCnt].key, "DATE") == SUCCESS)
			{
				memset(pstPymtTran->szTranDt, 0x00, sizeof(pstPymtTran->szTranDt));
				strcpy(pstPymtTran->szTranDt, listPtr[iCnt].value);
			}
			else if(strcmp(listPtr[iCnt].key, "TIME") == SUCCESS)
			{
				memset(pstPymtTran->szTranTm, 0x00, sizeof(pstPymtTran->szTranTm));
				strcpy(pstPymtTran->szTranTm, listPtr[iCnt].value);
			}
		}

		if(jCnt > 0)
		{
			if(pstPymtTran->pstPOSTenderDtls == NULL )
			{
				/* Allocating memory for storing POS defined Tender details */
				pstPymtTran->pstPOSTenderDtls = (POSTENDERDTLS_PTYPE)malloc(iSize);

				if(pstPymtTran->pstPOSTenderDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(pstPymtTran->pstPOSTenderDtls, 0x00, iSize);
			}
			memcpy(pstPymtTran->pstPOSTenderDtls, &stPOSTenderDtls, iSize);
		}

		if(strlen(stCardDtls.szOrigPAN) > 0)
		{
			if(pstPymtTran->pstCardDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Allocating memory for Card details struct", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* Allocate memory to card orig PAN */
				pstPymtTran->pstCardDtls = (CARDDTLS_PTYPE) malloc(iSizeCrdDtls);
				if(pstPymtTran->pstCardDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(pstPymtTran->pstCardDtls, 0x00, iSizeCrdDtls);
			}
			memcpy(pstPymtTran->pstCardDtls, &stCardDtls, iSizeCrdDtls);
		}

		//T_POLISETTYG1:assigned rv inside while loop becuase if malloc fails it will return FAILURE
		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: savePassThrgReqFields
 *
 * Description	: This function is responsible for updating pass through request fields in payment transaction
 *
 * Input Params	: KEYVAL_PTYPE
 *
 * Output Params: Number of total request fields
 * ============================================================================
 */
static int savePassThrgReqFields(KEYVAL_PTYPE listPtr, PYMTTRAN_PTYPE pstPymtTran)
{
	int						iCnt 					= 0;
	int						rvTemp					= SUCCESS;
	int						iLen 					= 0;
	void					*pTemp					= NULL;
	PASSTHRG_FIELDS_PTYPE	pstLocPassThrgDtls		= NULL;
	PASSTHRG_FIELDS_PTYPE	pstTmpPassThrgDtls		= NULL;
	KEYVAL_PTYPE			pstLocKeyValPtr		 	= NULL;
	VAL_LST_PTYPE 			pstPassThrgVarLst = NULL;
#ifdef DEBUG
	char	szDbgMsg[256]		= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		pstLocPassThrgDtls = getPassThrgFieldsDtls();

		if(pstLocPassThrgDtls->pstReqTagList == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Pass Through fields has not been configured", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return SUCCESS;
		}

		pstLocKeyValPtr = (KEYVAL_PTYPE) malloc(pstLocPassThrgDtls->iReqTagsCnt * KEYVAL_SIZE);
		if(pstLocKeyValPtr == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rvTemp = FAILURE;
			break;
		}
		memset(pstLocKeyValPtr, 0x00, pstLocPassThrgDtls->iReqTagsCnt * KEYVAL_SIZE);

		for(iCnt = 0; iCnt < pstLocPassThrgDtls->iReqTagsCnt; iCnt++)
		{
			pstLocKeyValPtr[iCnt].key = strdup(pstLocPassThrgDtls->pstReqTagList[iCnt].key);	// create memory for key & copy that
			pstLocKeyValPtr[iCnt].isMand = pstLocPassThrgDtls->pstReqTagList[iCnt].isMand;
			pstLocKeyValPtr[iCnt].valType = pstLocPassThrgDtls->pstReqTagList[iCnt].valType;
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, (pstLocKeyValPtr + iCnt)->key) == SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: listPtr[iCnt].valType [%d]", __FUNCTION__, listPtr[iCnt].valType);
				APP_TRACE(szDbgMsg);

				if(listPtr[iCnt].valType == BOOLEAN)		//save BOOl value, if present
				{
					iLen = sizeof(int);
					pTemp = (int *) malloc(iLen);
					if(pTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						/* Coverity 83374:Using ICnt to depect failure is not correct since Icnt is used below for freeing memory */
						rvTemp = FAILURE;
						break;
					}
					memset(pTemp, 0x00, iLen);
					memcpy(pTemp, (int*)listPtr[iCnt].value, iLen);
					pstLocKeyValPtr[iCnt].value = pTemp;
					pTemp = NULL;
				}
				else if(listPtr[iCnt].valType == VARLIST)
				{
					pstLocKeyValPtr[iCnt].valMetaInfo = pstLocPassThrgDtls->pstReqTagList[iCnt].valMetaInfo;

					debug_sprintf(szDbgMsg, "%s: ValCnt [%d]", __FUNCTION__, ((VAL_LST_PTYPE) listPtr[iCnt].value)->valCnt);
					APP_TRACE(szDbgMsg);

					pstPassThrgVarLst = storeVarListPassThroughFields((VAL_LST_PTYPE) listPtr[iCnt].value);
					if(pstPassThrgVarLst == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to Store the VarList Passthrough Fields", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						rvTemp = FAILURE;
						break;
					}
					pstLocKeyValPtr[iCnt].value = pstPassThrgVarLst;
				}
				else
				{
					iLen = strlen((char*)listPtr[iCnt].value);
					if(iLen > 0)
					{
						pTemp = (char*)malloc(iLen + 1);
						if(pTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							/* Coverity 83374:Using ICnt to depect failure is not correct since Icnt is used below for freeing memory */
							rvTemp = FAILURE;
							break;
						}
						memset(pTemp, 0x00, iLen + 1);
						memcpy(pTemp, (char*)listPtr[iCnt].value, iLen);
					}
					pstLocKeyValPtr[iCnt].value = pTemp;
					pTemp = NULL;
				}

			}
		}
#if 0
		for(iCnt = 0; iCnt < pstLocPassThrgDtls->iReqTagsCnt; iCnt++)
		{
			pstLocStrDef = (STR_DEF_PTYPE)(pstLocPassThrgDtls->pstReqTagList + iCnt)->valMetaInfo;
			if(pstLocPassThrgDtls->pstReqTagList[iCnt].valType == BOOLEAN && pstLocPassThrgDtls->pstReqTagList[iCnt].value != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: key[%s] val[%d] valType[%d] mandFlag[%d]", __FUNCTION__,
												(pstLocPassThrgDtls->pstReqTagList+iCnt)->key,
												*((int*)(pstLocPassThrgDtls->pstReqTagList+iCnt)->value),
												(pstLocPassThrgDtls->pstReqTagList+iCnt)->valType,
												(pstLocPassThrgDtls->pstReqTagList+iCnt)->isMand
												);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: key[%s] val[%s] valType[%d] mandFlag[%d]", __FUNCTION__,
												(pstLocPassThrgDtls->pstReqTagList+iCnt)->key,
												(char*)(pstLocPassThrgDtls->pstReqTagList+iCnt)->value,
												(pstLocPassThrgDtls->pstReqTagList+iCnt)->valType,
												(pstLocPassThrgDtls->pstReqTagList+iCnt)->isMand
												);
				APP_TRACE(szDbgMsg);
			}

			if(pstLocStrDef != NULL)
			{
				debug_sprintf(szDbgMsg, "%s: minLen[%d] maxLen[%d]", __FUNCTION__,
															pstLocStrDef->minLen,
															pstLocStrDef->maxLen);
				APP_TRACE(szDbgMsg);
			}

		}
#endif
		break;
	}

	if(iCnt > 0 && (rvTemp == SUCCESS))	// SUCCESS
	{
		if(pstPymtTran->pstPassThrghDtls == NULL)
		{
			// Create a place holder for Pass through fields in payment transaction
			pstTmpPassThrgDtls = (PASSTHRG_FIELDS_PTYPE) malloc(sizeof(PASSTHRG_FIELDS_STYPE));
			if(pstTmpPassThrgDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				/* Coverity 83374:Using ICnt to depect failure is not correct since Icnt is used below for freeing memory */
				rvTemp = FAILURE;
			}
			else
			{
				memset(pstTmpPassThrgDtls, 0x00, sizeof(PASSTHRG_FIELDS_STYPE));
			}
		}
		/*AjayS2 25 Aug 2016 - Coverity - 83375 -  If malloc fails we should not perform any operations on pstTmpPassThrgDtls.
		 * Adding check for NULL
		 */
		if(pstTmpPassThrgDtls != NULL)
		{
			pstTmpPassThrgDtls->iReqTagsCnt = pstLocPassThrgDtls->iReqTagsCnt;
			pstTmpPassThrgDtls->pstReqTagList = pstLocKeyValPtr;
			pstPymtTran->pstPassThrghDtls = pstTmpPassThrgDtls;	// Assign it into payment transaction instance
		}
	}
	else	// FAILURE;
	{

		/* Coverity 83374: We were not cleaning up array index 0 , It is possible for Icnt value to be 0 when filling first field caused error*/
		// T_RaghavendranR1 CID 84333 (#1 of 1): Out-of-bounds read (OVERRUN)(iCnt - 1 added to free the required elements and not to access the out of bound memory)
		if(pstLocKeyValPtr != NULL)
		{
			while((iCnt > 0))
			{
				free(pstLocKeyValPtr[iCnt - 1].key);
				free(pstLocKeyValPtr[iCnt - 1].value);
				iCnt--;
			}
			free(pstLocKeyValPtr);
		}

		if(pstTmpPassThrgDtls != NULL)
		{
			free(pstTmpPassThrgDtls);
		}
	}
	if(rvTemp == FAILURE)
	{
		iCnt = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, iCnt);
	APP_TRACE(szDbgMsg);
	return iCnt;
}

/*
 * ============================================================================
 * Function Name: storeVarListPassThroughFields
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static VAL_LST_PTYPE storeVarListPassThroughFields(VAL_LST_PTYPE pstVarLstPtr)
{
	int					rv						= SUCCESS;
	int					iLen					= 0;
	int					iTotCnt					= 0;
	int					iCnt					= 0;
	void *				pTemp					= NULL;
	VAL_NODE_PTYPE		nodePtr					= NULL;
	VAL_NODE_PTYPE		currNodePtr 			= NULL;
	VAL_LST_PTYPE 		pstPassThrgVarLst 		= NULL;
	VAL_LST_PTYPE 		pstPassThrgLstInsideLst = NULL;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstPassThrgVarLst = (VAL_LST_PTYPE) malloc(sizeof(VAL_LST_STYPE));
	if(pstPassThrgVarLst == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}
	memset(pstPassThrgVarLst, 0x00, sizeof(VAL_LST_STYPE));

	pstPassThrgVarLst->valCnt = pstVarLstPtr->valCnt;
	nodePtr = pstVarLstPtr->start;

	while(nodePtr != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Node type [%d], Node Count [%d], Node String [%s]", __FUNCTION__, nodePtr->type, pstVarLstPtr->valCnt, nodePtr->szVal);
		APP_TRACE(szDbgMsg);

		if(nodePtr->type != -1)
		{
			currNodePtr = (VAL_NODE_PTYPE) malloc (sizeof(VAL_NODE_STYPE));
			if(currNodePtr == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			memset(currNodePtr, 0x00, sizeof(VAL_NODE_STYPE));

			currNodePtr->elemCnt = nodePtr->elemCnt;
			if((nodePtr->szVal != NULL) && (strlen(nodePtr->szVal) > 0))
			{
				currNodePtr->szVal = (char *) malloc(strlen(nodePtr->szVal) + 1);
				if(currNodePtr->szVal == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					free(pstPassThrgVarLst)
					pstPassThrgVarLst = NULL;
					break;
				}

				strcpy(currNodePtr->szVal, nodePtr->szVal);

			}

			currNodePtr->type = nodePtr->type;
			currNodePtr->elemList = (KEYVAL_PTYPE) malloc(nodePtr->elemCnt * sizeof(KEYVAL_STYPE));
			if(currNodePtr->elemList == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				free(pstPassThrgVarLst)
				pstPassThrgVarLst = NULL;
				break;
			}
			memset(currNodePtr->elemList, 0x00, nodePtr->elemCnt * sizeof(KEYVAL_STYPE));

			iTotCnt = nodePtr->elemCnt;

			debug_sprintf(szDbgMsg, "%s: Toltal Count in Node [%d]", __FUNCTION__, iTotCnt);
			APP_TRACE(szDbgMsg);

			for(iCnt = 0; iCnt < iTotCnt; iCnt++)
			{
				if(nodePtr->elemList[iCnt].value == NULL)
				{
					continue;
				}

				debug_sprintf(szDbgMsg, "%s: Key  [%s], Value  [%s]", __FUNCTION__, (char *) (nodePtr->elemList[iCnt]).key, (char *)(nodePtr->elemList[iCnt]).value);
				APP_TRACE(szDbgMsg);

				debug_sprintf(szDbgMsg, "%s: ValType [%d]", __FUNCTION__, (nodePtr->elemList[iCnt]).valType);
				APP_TRACE(szDbgMsg);

				currNodePtr->elemList[iCnt].key = strdup(nodePtr->elemList[iCnt].key);
				currNodePtr->elemList[iCnt].isMand = nodePtr->elemList[iCnt].isMand;
				currNodePtr->elemList[iCnt].valType = nodePtr->elemList[iCnt].valType;

				if(nodePtr->elemList[iCnt].valType == BOOLEAN)		//save BOOl value, if present
				{
					iLen = sizeof(int);
					pTemp = (int *) malloc(iLen);
					if(pTemp == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Malloc Failed", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						free(pstPassThrgVarLst)
						pstPassThrgVarLst = NULL;
						break;
					}
					memset(pTemp, 0x00, iLen);
					memcpy(pTemp, (int*)nodePtr->elemList[iCnt].value, iLen);
					currNodePtr->elemList[iCnt].value = pTemp;

					pTemp = NULL;
				}
				else if(nodePtr->elemList[iCnt].valType == VARLIST)
				{
					debug_sprintf(szDbgMsg, "%s: ValCnt of List Inside List [%d]", __FUNCTION__, ((VAL_LST_PTYPE) nodePtr->elemList[iCnt].value)->valCnt);
					APP_TRACE(szDbgMsg);

					pstPassThrgLstInsideLst = storeVarListPassThroughFields((VAL_LST_PTYPE) nodePtr->elemList[iCnt].value);
					if(pstPassThrgLstInsideLst == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Failed to Store the VarList Passthrough Fields", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						free(pstPassThrgVarLst)
						pstPassThrgVarLst = NULL;
						break;
					}
					currNodePtr->elemList[iCnt].value = pstPassThrgLstInsideLst;
					currNodePtr->elemList[iCnt].valMetaInfo = ((VAL_LST_PTYPE) (nodePtr->elemList[iCnt].valMetaInfo));
				}
				else
				{
					iLen = strlen((char*)nodePtr->elemList[iCnt].value);
					if(iLen > 0)
					{
						pTemp = (char*)malloc(iLen + 1);
						if(pTemp == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: malloc failed", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							free(pstPassThrgVarLst)
							pstPassThrgVarLst = NULL;
							break;
						}
						memset(pTemp, 0x00, iLen + 1);
						memcpy(pTemp, (char*)nodePtr->elemList[iCnt].value, iLen);
					}
					currNodePtr->elemList[iCnt].value = pTemp;

					pTemp = NULL;
				}

			}

			addVarListNode(pstPassThrgVarLst, currNodePtr);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Varist type %d", __FUNCTION__, nodePtr->type);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		nodePtr = nodePtr->next;
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return pstPassThrgVarLst;
}

/*
 * ============================================================================
 * Function Name: addDataNode
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addVarListNode(VAL_LST_PTYPE valLstPtr, VAL_NODE_PTYPE currNodePtr)
{
	int				rv				= SUCCESS;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(currNodePtr != NULL)
	{
		/* Add the node to the list */
		if(valLstPtr->end == NULL)
		{
			/* Adding the first element */
			valLstPtr->start = currNodePtr;
			valLstPtr->end = currNodePtr;
		}
		else
		{
			/* Adding the nth element */
			valLstPtr->end->next = currNodePtr;
			valLstPtr->end = currNodePtr;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveGenPymtDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveGenPymtDtls(KEYVAL_PTYPE listPtr, PYMTTRAN_PTYPE pstPymtTran)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		iTotCnt			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(genPymtReqLst) / KEYVAL_SIZE;

	pstPymtTran->iPymtSubType = -1; //default value

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		if(listPtr[iCnt].value == NULL)
		{
			continue;
		}
		else if(strcmp(listPtr[iCnt].key, "PAYMENT_TYPE") == SUCCESS)
		{
			pstPymtTran->iPymtType = *( (int *) (listPtr[iCnt].value) );
			memset(pstPymtTran->szPaymentTypes, 0x00, sizeof(pstPymtTran->szPaymentTypes));
		}
		else if(strcmp(listPtr[iCnt].key, "PAYMENT_TYPES") == SUCCESS && pstPymtTran->iPymtType == -1)
		{
			memset(pstPymtTran->szPaymentTypes, 0x00, sizeof(pstPymtTran->szPaymentTypes));
			strcpy(pstPymtTran->szPaymentTypes, listPtr[iCnt].value);
		}
		else if(strcmp(listPtr[iCnt].key, "PAYMENT_SUBTYPE") == SUCCESS)
		{
			pstPymtTran->iPymtSubType = *( (int *) (listPtr[iCnt].value) );
		}
		else if(strcmp(listPtr[iCnt].key, "MANUAL_ENTRY") == SUCCESS)
		{
			pstPymtTran->bManEntry = *( (int *) (listPtr[iCnt].value) );
		}
		else if(strcmp(listPtr[iCnt].key, "MANUAL_PROMPT_OPTIONS") == SUCCESS)
		{
			memset(pstPymtTran->szManPrmptOptns, 0x00, sizeof(pstPymtTran->szManPrmptOptns));
			strcpy(pstPymtTran->szManPrmptOptns, listPtr[iCnt].value);
		}
		else if(strcmp(listPtr[iCnt].key, "FORCE_FLAG") == SUCCESS)
		{
			pstPymtTran->bForce = *( (int *) (listPtr[iCnt].value) );
		}
		else if(strcmp(listPtr[iCnt].key, "BILLPAY") == SUCCESS)
		{
			pstPymtTran->bBillPay = *( (int *) (listPtr[iCnt].value) );
		}
	}

	rv = iTotCnt;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveGenPymtRptDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveGenPymtRptDtls(KEYVAL_PTYPE listPtr, PASSTHRU_PTYPE pstPassThru)
{
	int		rv				= SUCCESS;
	int		iCnt			= 0;
	int		iTotCnt			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iTotCnt = sizeof(genPymtReqLst) / KEYVAL_SIZE;

	for(iCnt = 0; iCnt < iTotCnt; iCnt++)
	{
		if(listPtr[iCnt].value == NULL)
		{
			continue;
		}
		else if(strcmp(listPtr[iCnt].key, "PAYMENT_TYPE") == SUCCESS)
		{
			pstPassThru->iPymtType = *( (int *) (listPtr[iCnt].value) );
		}
		else if(strcmp(listPtr[iCnt].key, "MANUAL_ENTRY") == SUCCESS)
		{
			pstPassThru->bManEntry = *( (int *) (listPtr[iCnt].value) );
		}
		else if(strcmp(listPtr[iCnt].key, "FORCE_FLAG") == SUCCESS)
		{

		}
	}

	rv = iTotCnt;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: savePymtAmtDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int savePymtAmtDtls(KEYVAL_PTYPE listPtr, AMTDTLS_PTYPE * dpstAmt)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	AMTDTLS_STYPE	stAmtDtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(AMTDTLS_STYPE);

	while(1)
	{
		memset(&stAmtDtls, 0x00, iSize);
		if(*dpstAmt != NULL)
		{
			memcpy(&stAmtDtls, *dpstAmt, iSize);
		}

		iTotCnt = sizeof(pymtAmtReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "TRANS_AMOUNT") == SUCCESS)
			{
				strcpy(stAmtDtls.tranAmt, listPtr[iCnt].value);
				strcpy(stAmtDtls.origTranAmt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TIP_AMOUNT") == SUCCESS)
			{
				strcpy(stAmtDtls.tipAmt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CASHBACK_AMNT") == SUCCESS)
			{
				strcpy(stAmtDtls.cashBackAmtFromPOS, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstAmt == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstAmt = (AMTDTLS_PTYPE) malloc(iSize);
				if(*dpstAmt == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(*dpstAmt, 0x00, iSize);
			}
			memcpy(*dpstAmt, &stAmtDtls, iSize);
		}
		//CID:67244:T_POLISETTYG1:assigned rv inside while loop becuase if malloc fails it will return FAILURE
		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveFollowOnDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveFollowOnDtls(KEYVAL_PTYPE listPtr, FTRANDTLS_PTYPE * dpstFTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	FTRANDTLS_STYPE	stFTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(FTRANDTLS_STYPE);

	while(1)
	{
		memset(&stFTran, 0x00, iSize);
		if(*dpstFTran != NULL)
		{
			memcpy(&stFTran, *dpstFTran, iSize);
		}

		iTotCnt = sizeof(pymtFollowOnReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "AUTH_CODE") == SUCCESS)
			{
				strcpy(stFTran.szAuthCode, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CTROUTD") == SUCCESS)
			{
				strcpy(stFTran.szCTroutd, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "REF_TROUTD") == SUCCESS)
			{
				strcpy(stFTran.szTroutd, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CARD_TOKEN") == SUCCESS)
			{
				strcpy(stFTran.szCardToken, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "BANK_USERDATA") == SUCCESS)
			{
				strcpy(stFTran.szBankUserData, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "REFERENCE") == SUCCESS)
			{
				strcpy(stFTran.szReference, listPtr[iCnt].value);
				jCnt++;
			}
#if 0
			else if(strcmp(listPtr[iCnt].key, "TOKEN_SOURCE") == SUCCESS)
			{
				strcpy(stFTran.szTokenSrc, listPtr[iCnt].value);
				jCnt++;
			}
#endif
		}

		debug_sprintf(szDbgMsg, "%s: jCnt [%d]", __FUNCTION__, jCnt);
		APP_TRACE(szDbgMsg);

		if(jCnt > 0)
		{
			if(*dpstFTran == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstFTran = (FTRANDTLS_PTYPE) malloc(iSize);
				if(*dpstFTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(*dpstFTran, 0x00, iSize);
			}
			memcpy(*dpstFTran, &stFTran, iSize);
		}
		//CID:67240:T_POLISETTYG1:assigned rv inside while loop becuase if malloc fails it will return FAILURE
		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveTaxDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveTaxDtls(KEYVAL_PTYPE listPtr, LVL2_PTYPE * dpstLevel2Dtls)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	LVL2_STYPE	stLevel2Dtls;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(LVL2_STYPE);

	while(1)
	{
		memset(&stLevel2Dtls, 0x00, iSize);

		//Praveen_P1: Defaulting to -1 since 0 is valid value for this field, if this field did not come from POS still we are sending 0 since it is memset
		stLevel2Dtls.taxInd = -1;

		stLevel2Dtls.taxInd = -1; //Setting default value as -1 since 0 is valid tax indicator
		if(*dpstLevel2Dtls != NULL)
		{
			memcpy(&stLevel2Dtls, *dpstLevel2Dtls, iSize);
		}

		iTotCnt = sizeof(lvl2ReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "TAX_AMOUNT") == SUCCESS)
			{
				strcpy(stLevel2Dtls.taxAmt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TAX_IND") == SUCCESS)
			{
				stLevel2Dtls.taxInd = *((int *) (listPtr[iCnt].value));
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CMRCL_FLAG") == SUCCESS)
			{
				stLevel2Dtls.cmrclFlag = *((int *) (listPtr[iCnt].value));
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISCOUNT_AMOUNT") == SUCCESS)
			{
				strcpy(stLevel2Dtls.discAmt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "FREIGHT_AMOUNT") == SUCCESS)
			{
				strcpy(stLevel2Dtls.freightAmt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DUTY_AMOUNT") == SUCCESS)
			{
				strcpy(stLevel2Dtls.dutyAmt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "SHIP_FROM_ZIP_CODE") == SUCCESS)
			{
				strcpy(stLevel2Dtls.shipPostalcode, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "RETAIL_ITEM_DESC_1") == SUCCESS)
			{
				strcpy(stLevel2Dtls.prodDesc, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DEST_POSTAL_CODE") == SUCCESS)
			{
				strcpy(stLevel2Dtls.destPostalcode, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "ALT_TAX_ID") == SUCCESS)
			{
				strcpy(stLevel2Dtls.alternateTaxId, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DEST_COUNTRY_CODE") == SUCCESS)
			{
				strcpy(stLevel2Dtls.destCountryCode, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CUSTOMER_CODE") == SUCCESS)
			{
				strcpy(stLevel2Dtls.customerCode, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstLevel2Dtls == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstLevel2Dtls = (LVL2_PTYPE) malloc(iSize);
				if(*dpstLevel2Dtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(*dpstLevel2Dtls, 0x00, iSize);
			}
			memcpy(*dpstLevel2Dtls, &stLevel2Dtls, iSize);
		}

		//CID:67191:T_POLISETTYG1:assigned rv inside while loop becuase if malloc fails it will return FAILURE
		rv = iTotCnt;
		break;
	}



	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveChkDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveChkDtls(KEYVAL_PTYPE listPtr, CHKDTLS_PTYPE * dpstChkDtls)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	CHKDTLS_STYPE stChkDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(CHKDTLS_STYPE);

	while(1)
	{
		memset(&stChkDtls, 0x00, iSize);
		if(*dpstChkDtls != NULL)
		{
			memcpy(&stChkDtls, *dpstChkDtls, iSize);
		}

		iTotCnt = sizeof(pymtChkReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "MICR") == SUCCESS)
			{
				strcpy(stChkDtls.szMICR, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "ACCT_NUM") == SUCCESS)
			{
				strcpy(stChkDtls.szAcctNum, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "ABA_NUM") == SUCCESS)
			{
				strcpy(stChkDtls.szABANum, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CHECK_NUM") == SUCCESS)
			{
				strcpy(stChkDtls.szChkNum, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DL_STATE") == SUCCESS)
			{
				strcpy(stChkDtls.szDLState, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DL_NUMBER") == SUCCESS)
			{
				strcpy(stChkDtls.szDLNumber, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CHECK_TYPE") == SUCCESS)
			{
				stChkDtls.iChkType = *((int *) (listPtr[iCnt].value));
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstChkDtls == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstChkDtls = (CHKDTLS_PTYPE) malloc(iSize);
				if(*dpstChkDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(*dpstChkDtls, 0x00, iSize);
			}
			memcpy(*dpstChkDtls, &stChkDtls, iSize);
		}
		//CID:67231:T_POLISETTYG1:assigned rv inside while loop becuase if malloc fails it will return FAILURE
		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveCustInfoDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveCustInfoDtls(KEYVAL_PTYPE listPtr, CUSTINFODTLS_PTYPE * pstCustInfoDtls)
{
	int					rv				= SUCCESS;
	int					iCnt			= 0;
	int					jCnt			= 0;
	int					iTotCnt			= 0;
	int					iSize			= 0;
	CUSTINFODTLS_STYPE 	stCustInfoDtls;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(CUSTINFODTLS_STYPE);

	while(1)
	{
		memset(&stCustInfoDtls, 0x00, iSize);

		if(*pstCustInfoDtls != NULL)
		{
			memcpy(&stCustInfoDtls, *pstCustInfoDtls, iSize);
		}

		iTotCnt = sizeof(custInfoReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "CUSTOMER_STREET") == SUCCESS)
			{
				strcpy(stCustInfoDtls.szCustStreetName, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CUSTOMER_ZIP") == SUCCESS)
			{
				strcpy(stCustInfoDtls.szCustZipNum, listPtr[iCnt].value);
				strcpy(stCustInfoDtls.szPosCustZipNum, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CUST_DATA") == SUCCESS)
			{
				strcpy(stCustInfoDtls.szCustData, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "MERCHANT_INDEX") == SUCCESS)
			{
				strcpy(stCustInfoDtls.szMerchantIndex, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "NFCVAS_MODE") == SUCCESS)
			{
				strcpy(stCustInfoDtls.szNFCVASMode, listPtr[iCnt].value);
				jCnt++;
		}
		}

		if(jCnt > 0)
		{
			if(*pstCustInfoDtls == NULL)
			{
				/* Allocate memory to store the customer details for the
				 * payment request of the POS */
				*pstCustInfoDtls = (CUSTINFODTLS_PTYPE) malloc(iSize);
				if(*pstCustInfoDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(*pstCustInfoDtls, 0x00, iSize);
			}
			memcpy(*pstCustInfoDtls, &stCustInfoDtls, iSize);
		}
		//T_POLISETTYG1:assigned rv inside while loop becuase if malloc fails it will return FAILURE
		rv = iTotCnt;
		break;
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}



/*
 * ============================================================================
 * Function Name: saveBatchReqDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveBatchReqDtls(KEYVAL_PTYPE listPtr, char ** pszTranKey)
{
	int			rv				= SUCCESS;
	char *		szTranKey		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Create a transaction stack for this batch request */
		szTranKey = (char *) malloc(sizeof(char) * 10);
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: key Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(szTranKey, 0x00, sizeof(char) * 10);

		/* Create a new SSI transaction */
		rv = pushNewSSITran(szTranKey);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create SSI transaction",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		*pszTranKey = szTranKey;

		break;
	}

	if(rv != SUCCESS)
	{
		if(szTranKey != NULL)
		{
			free(szTranKey);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveRptReqDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveRptReqDtls(META_PTYPE pstMeta, char ** pszTranKey)
{
	int				rv				= SUCCESS;
	char *			szTranKey		= NULL;
	TRAN_PTYPE		pstSSITran		= NULL;
	PASSTHRU_STYPE	stPassThru;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Initialize the data structures */
		memset(&stPassThru, 0x00, sizeof(PASSTHRU_STYPE));

		/* Copy the meta data for the passthru transaction. */
		stPassThru.pstMetaData = (META_PTYPE) malloc(METADATA_SIZE);
		if(stPassThru.pstMetaData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: meta Maloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		debug_sprintf(szDbgMsg, "%s: Allocated memory for metadata of Passthru", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		memset((META_PTYPE)stPassThru.pstMetaData, 0x00, sizeof(META_STYPE));
		memcpy((META_PTYPE)stPassThru.pstMetaData, pstMeta, sizeof(META_STYPE));

		/* Create a transaction stack for this batch request */
		szTranKey = (char *) malloc(sizeof(char) * 10);
		if(szTranKey == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: key Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(szTranKey, 0x00, sizeof(char) * 10);

		/* Create a new SSI transaction */
		rv = pushNewSSITran(szTranKey);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create SSI transaction",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		debug_sprintf(szDbgMsg, "%s: Created new SSI Tran instance on the stack", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: SSI Tran Key [%s]", __FUNCTION__, szTranKey);
		APP_TRACE(szDbgMsg);


		/* Get a reference to this new SSI transaction (stored in stack) */
		rv = getTopSSITran(szTranKey, &pstSSITran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to access SSI transaction",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Got the SSI Tran instance", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Put a transaction for the batch request data in the stack */
		pstSSITran->data = (PASSTHRU_PTYPE) malloc(sizeof(PASSTHRU_STYPE));
		if(pstSSITran->data == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: passthru Malloc FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		/* Memsetting the passthru struct */
		memset((PASSTHRU_PTYPE)pstSSITran->data, 0x00, sizeof(PASSTHRU_STYPE));

		debug_sprintf(szDbgMsg, "%s: Assigned data pointer in SSI tran instance", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		*pszTranKey = szTranKey;
		memcpy((PASSTHRU_PTYPE)pstSSITran->data, &stPassThru, sizeof(PASSTHRU_STYPE));

		//memset(pstMeta, 0x00, sizeof(META_STYPE));

		break;
	}

	if(rv != SUCCESS)
	{
		if(szTranKey != NULL)
		{
			free(szTranKey);
		}

		if(stPassThru.pstMetaData != NULL)
		{
			free(stPassThru.pstMetaData);
		}

		if(pstSSITran!= NULL && pstSSITran->data != NULL)
		{
			free(pstSSITran->data);
			pstSSITran->data = NULL;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveAttributeDtls
 *
 * Description	: This function saves the attribute list in the card details structure
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveAttributeDtls(KEYVAL_PTYPE listPtr, PYMTTRAN_PTYPE pstPymntDtls)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				jCnt			= 0;
	int				iSize			= 0;
	int				iPymntType		= -1;
	CARDDTLS_STYPE	stCardDtls;
	CARDDTLS_PTYPE  pstCardDtls;
	CHKDTLS_PTYPE	pstChkDtls;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(CARDDTLS_STYPE);

	iPymntType	= pstPymntDtls->iPymtType;
	pstChkDtls = pstPymntDtls->pstChkDtls;
	pstCardDtls = pstPymntDtls->pstCardDtls;

	while(1)
	{
		memset(&stCardDtls, 0x00, iSize);

		if(pstCardDtls != NULL)
		{
			memcpy(&stCardDtls, pstCardDtls, iSize);
		}

		iTotCnt = sizeof(pymtAttributeReqLst) / KEYVAL_SIZE;

		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "CVV2") == SUCCESS)
			{
				strcpy(stCardDtls.szCVV, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "BARCODE") == SUCCESS)
			{
				strcpy(stCardDtls.szBarCode, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PIN_CODE") == SUCCESS)
			{
				strcpy(stCardDtls.szPINCode, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "ENCRYPT") == SUCCESS)
			{
				stCardDtls.bEncReqd = *( (int *) (listPtr[iCnt].value) );
			}
		}

		/* KranthiK1: This is part of the code is to copy the acct num to be carddtls structure
		 * and then free check structure if the payment type is not sent as check
		 * This is done as the same name ACCT_NUM has to be used at both the places
		 */
		if(pstChkDtls && strlen(pstChkDtls->szAcctNum) > 0)
		{
			if(iPymntType != PYMT_CHECK && iPymntType != PYMT_CHKSALE && iPymntType != PYMT_CHKVERIFY)
			{
				strcpy(stCardDtls.szPAN, pstChkDtls->szAcctNum);
				free(pstChkDtls);

				pstPymntDtls->pstChkDtls = NULL;
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(pstCardDtls == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Allocating memory for Card details struct", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* Allocate memory to card expiry details */
				pstCardDtls = (CARDDTLS_PTYPE) malloc(iSize);
				if(pstCardDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(pstCardDtls, 0x00, iSize);
			}
			memcpy(pstCardDtls, &stCardDtls, iSize);
			pstPymntDtls->pstCardDtls = pstCardDtls;
		}
		//CID:67210:T_POLISETTYG1:assigned rv inside while loop becuase if malloc fails it will return FAILURE
		rv = iTotCnt;
		break;
	}


	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveEBTTransDtls
 *
 * Description	: This function saves the EBT Transactions Related details into the structure
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveEBTTransDtls(KEYVAL_PTYPE listPtr, EBTDTLS_PTYPE *dpstPymntDtls)
{

	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				jCnt			= 0;
	int				iSize			= 0;
	int 			ebtType			= -1;
	EBTDTLS_STYPE	stEBTDtls;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(EBTDTLS_STYPE);

	while(1)
	{
		memset(&stEBTDtls, 0x00, iSize);
		if(*dpstPymntDtls != NULL)
		{
			memcpy(&stEBTDtls, *dpstPymntDtls, iSize);
		}

		iTotCnt = sizeof(ebtReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "EBT_TYPE") == SUCCESS)
			{
				ebtType = *( (int *) (listPtr[iCnt].value) );

				if(ebtType == EBT_FOODSNAP)
				{
					strcpy(stEBTDtls.szEBTType, "FOOD_STAMP");
				}
				else if (ebtType == EBT_CASHBENEFITS)
				{
					strcpy(stEBTDtls.szEBTType, "CASH_BENEFITS");
				}
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "EBTCASH_ELIGIBLE") == SUCCESS)
			{
				strcpy(stEBTDtls.szEBTCashElig, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "EBTSNAP_ELIGIBLE") == SUCCESS)
			{
				strcpy(stEBTDtls.szEBTSNAPElig, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstPymntDtls == NULL)
			{
				/* Allocate memory to store the EBT details for the Trans*/
				*dpstPymntDtls = (EBTDTLS_PTYPE) malloc(iSize);
				if(*dpstPymntDtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(*dpstPymntDtls, 0x00, iSize);
			}
			memcpy(*dpstPymntDtls, &stEBTDtls, iSize);
		}

		//T_POLISETTYG1:assigned rv inside while loop becuase if malloc fails it will return FAILURE
		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveFSATransDtls
 *
 * Description	: This function saves the FSA Transactions Related details into the structure
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveFSATransDtls(KEYVAL_PTYPE listPtr, FSADTLS_PTYPE *dpstFSADtls)
{

	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				iTotCnt			= 0;
	int				jCnt			= 0;
	int				iSize			= 0;
	FSADTLS_STYPE	stFSADtls;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(FSADTLS_STYPE);

	while(1)
	{
		memset(&stFSADtls, 0x00, iSize);
		if(*dpstFSADtls != NULL)
		{
			memcpy(&stFSADtls, *dpstFSADtls, iSize);
		}

		iTotCnt = sizeof(FSAReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "AMOUNT_HEALTHCARE") == SUCCESS)
			{
				strcpy(stFSADtls.szAmtHealthCare, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AMOUNT_PRESCRIPTION") == SUCCESS)
			{
				strcpy(stFSADtls.szAmtPres, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AMOUNT_VISION") == SUCCESS)
			{
				strcpy(stFSADtls.szAmtVision, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AMOUNT_CLINIC") == SUCCESS)
			{
				strcpy(stFSADtls.szAmtClinic, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "AMOUNT_DENTAL") == SUCCESS)
			{
				strcpy(stFSADtls.szAmtDental, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstFSADtls == NULL)
			{
				/* Allocate memory to store the FSA details for the Trans*/
				*dpstFSADtls = (FSADTLS_PTYPE) malloc(iSize);
				if(*dpstFSADtls == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
				memset(*dpstFSADtls, 0x00, iSize);
			}
			memcpy(*dpstFSADtls, &stFSADtls, iSize);
		}
		//CID:67216:T_POLISETTYG1:assigned rv inside while loop becuase if malloc fails it will return FAILURE
		rv = iTotCnt;
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveSetParmReqDtls
 *
 * Description	: This function saves the Set PARM Related Details into the structure
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveSetParmReqDtls(KEYVAL_PTYPE listPtr,  DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv						= SUCCESS;
	int				iCnt					= 0;
	int				jCnt					= 0;
	int				iTotCnt					= 0;
	int				iSize					= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(setParmReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "PARM_MID") == SUCCESS)
			{
				strcpy(stDevTran.stSetParmDtls.szMID, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PARM_TID") == SUCCESS)
			{
				strcpy(stDevTran.stSetParmDtls.szTID, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PARM_LANE") == SUCCESS)
			{
				strcpy(stDevTran.stSetParmDtls.szLane, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PARM_HOST_IND") == SUCCESS)
			{
				stDevTran.stSetParmDtls.iHostType = *( (int *) (listPtr[iCnt].value) );

				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PARM_ADMINURL") == SUCCESS)
			{
				strcpy(stDevTran.stSetParmDtls.szAdminURL, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PARM_PRIMURL") == SUCCESS)
			{
				strcpy(stDevTran.stSetParmDtls.szPrimURL, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PARM_SCNDURL") == SUCCESS)
			{
				strcpy(stDevTran.stSetParmDtls.szScndURL, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PARM_ALTMERCHID") == SUCCESS)
			{
				strcpy(stDevTran.stSetParmDtls.szAltMerchId, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "PARM_TIMEZONE") == SUCCESS)
			{
				strcpy(stDevTran.stSetParmDtls.szTimeZone, listPtr[iCnt].value);
				jCnt++;
			}

		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the set parm details request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveDispMsgReqDtls
 *
 * Description	: This function saves the Display Message Related Details into the structure
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveDispMsgReqDtls(KEYVAL_PTYPE listPtr,  DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv						= SUCCESS;
	int				iCnt					= 0;
	int				jCnt					= 0;
	int				iTotCnt					= 0;
	int				iSize					= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(dispMsgReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT") == SUCCESS)
			{
				strcpy(stDevTran.stDispMsgDtls.szDispTxt, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "TIMEOUT_DATA") == SUCCESS)
			{
				stDevTran.stDispMsgDtls.iTimeOut = atoi((char *)listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the set parm details request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: saveApplyUpdatesDtls
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveApplyUpdatesDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(applyUpdatesReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "FLAG") == SUCCESS)
			{
				stDevTran.stApplyUpdatesDtls.bUpdateFlag = *( (int *) (listPtr[iCnt].value) );
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveGetParmReqDtls
 *
 * Description	: This function saves the Get PARM Related Details into the structure
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveGetParmReqDtls(KEYVAL_PTYPE listPtr,  DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv						= SUCCESS;
	int				iCnt					= 0;
	int				jCnt					= 0;
	int				iTotCnt					= 0;
	int				iSize					= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(getParmReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "PARAM") == SUCCESS)
			{
				strcpy(stDevTran.stGetParmDtls.szGetParmReq, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the get parm details request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveProvisionPassReqDtls
 *
 * Description	: This function saves the Provision Pass Related Details into the structure
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveProvisionPassReqDtls(KEYVAL_PTYPE listPtr,  DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv						= SUCCESS;
	int				iCnt					= 0;
	int				jCnt					= 0;
	int				iTotCnt					= 0;
	int				iSize					= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(getProvisionPassReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "MERCHANT_INDEX") == SUCCESS)
			{
				strcpy(stDevTran.stProvPassDtls.szMerchIndex, listPtr[iCnt].value);
				jCnt++;
			}
//			else if(strcmp(listPtr[iCnt].key, "MERCHANT_ID") == SUCCESS)
//			{
//				strcpy(stDevTran.stProvPassDtls.szMerchID, listPtr[iCnt].value);
//				jCnt++;
//			}
//			else if(strcmp(listPtr[iCnt].key, "MERCHANT_URL") == SUCCESS)
//			{
//				strcpy(stDevTran.stProvPassDtls.szMerchURL, listPtr[iCnt].value);
//				jCnt++;
//			}
			else if(strcmp(listPtr[iCnt].key, "CUST_DATA") == SUCCESS)
			{
				strcpy(stDevTran.stProvPassDtls.szCustData, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the get parm details request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveGetCardDataReqDtls
 *
 * Description	:This function will be used to store the Card Details in to
 * 				 the Structure.
 *
 * Input Params	:Key Value, DEVTRAN_PTYPE structure
 *
 * Output Params:
 * ============================================================================
 */
static int saveGetCardDataReqDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- Enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(getCardDataReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT1") == SUCCESS)
			{
				strcpy(stDevTran.stGetCardDataDtls.szDispTxt1, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT2") == SUCCESS)
			{
				strcpy(stDevTran.stGetCardDataDtls.szDispTxt2, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT3") == SUCCESS)
			{
				strcpy(stDevTran.stGetCardDataDtls.szDispTxt3, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CARD_INPUT_METHOD") == SUCCESS)
			{
				strcpy(stDevTran.stGetCardDataDtls.szInputMethod, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*Akshaya :21-07-16 . Apple Pay :Ignoring the Implementation if having NFCVAS_MODE and MERCHANT_INDEX are sent in SHOW Line Item Command.*/
#if 0
/*
 * ================================================================================
 * Function Name: saveShowLIReqDtls
 *
 * Description	:This function will be used to store the VAS Terminal Mode sent in
 * 				 show Line Item
 *
 * Input Params	:Key Value, SHWLIINFO_PTYPE structure
 *
 * Output Params:
 * ================================================================================
 */
static int saveShowLIReqDtls(KEYVAL_PTYPE listPtr, SHWLIINFO_PTYPE * dpstShwLiInfo)
{

	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	SHWLIINFO_STYPE	stShwLIInfo;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* TODO: Complete this */
	while(1)
	{
		if(*dpstShwLiInfo != NULL)
		{
			memcpy(&stShwLIInfo, *dpstShwLiInfo, sizeof(SHWLIINFO_STYPE));
		}
		else
		{
			memset(&stShwLIInfo, 0x00, sizeof(SHWLIINFO_STYPE));
		}

		iTotCnt = sizeof(ShowLineItemReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "NFCVAS_MODE") == SUCCESS)
			{
				strcpy(stShwLIInfo.szNFCVASMode, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "MERCHANT_INDEX") == SUCCESS)
			{
				strcpy(stShwLIInfo.szMerchantIndex, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstShwLiInfo == NULL)
			{
				*dpstShwLiInfo = (SHWLIINFO_PTYPE) malloc(sizeof(SHWLIINFO_STYPE));
				if(*dpstShwLiInfo == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED for li info",
							__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memcpy(*dpstShwLiInfo, &stShwLIInfo, sizeof(SHWLIINFO_STYPE));
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return rv;

}
#endif
/*
 * ============================================================================
 * Function Name: saveDispQRCodeReqDtls
 *
 * Description	: This function saves the Display QR Code Related Details into the structure
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveDispQRCodeReqDtls(KEYVAL_PTYPE listPtr,  DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv						= SUCCESS;
	int				iCnt					= 0;
	int				jCnt					= 0;
	int				iTotCnt					= 0;
	int				iSize					= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]			= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(getDispQRCodeReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "QRCODE_DATA") == SUCCESS)
			{
				strcpy(stDevTran.stDispQRCodeDtls.szQRCodeData, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT") == SUCCESS)
			{
				strcpy(stDevTran.stDispQRCodeDtls.szDispText, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "BUTTON_LABEL") == SUCCESS)
			{
				strcpy(stDevTran.stDispQRCodeDtls.szBtnLabel, listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the get parm details request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: saveCheckBoxReqDtls
 *
 * Description	: Saves the Checkbox device command Related Details to data System
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int saveCheckBoxReqDtls(KEYVAL_PTYPE listPtr, DEVTRAN_PTYPE * dpstDevTran)
{
	int				rv				= SUCCESS;
	int				iCnt			= 0;
	int				jCnt			= 0;
	int				iTotCnt			= 0;
	int				iSize			= 0;
	DEVTRAN_STYPE	stDevTran;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iSize = sizeof(DEVTRAN_STYPE);

	while(1)
	{
		if(*dpstDevTran != NULL)
		{
			memset(&stDevTran, 0x00, iSize);
			memcpy(&stDevTran, *dpstDevTran, iSize);
		}
		else
		{
			memset(&stDevTran, 0x00, iSize);
		}

		iTotCnt = sizeof(custCheckBoxReqLst) / KEYVAL_SIZE;

		/* Populate the values */
		for(iCnt = 0; iCnt < iTotCnt; iCnt++)
		{
			if(listPtr[iCnt].value == NULL)
			{
				continue;
			}
			else if(strcmp(listPtr[iCnt].key, "DISPLAY_TEXT") == SUCCESS)
			{
				strcpy(stDevTran.stCustCheckBoxDtls.szTitletext, listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CHECKBOX_LABEL1") == SUCCESS)
			{
				strcpy(stDevTran.stCustCheckBoxDtls.szCheckboxLabels[0], listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CHECKBOX_LABEL2") == SUCCESS)
			{
				strcpy(stDevTran.stCustCheckBoxDtls.szCheckboxLabels[1], listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CHECKBOX_LABEL3") == SUCCESS)
			{
				strcpy(stDevTran.stCustCheckBoxDtls.szCheckboxLabels[2], listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CHECKBOX_LABEL4") == SUCCESS)
			{
				strcpy(stDevTran.stCustCheckBoxDtls.szCheckboxLabels[3], listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CHECKBOX_LABEL5") == SUCCESS)
			{
				strcpy(stDevTran.stCustCheckBoxDtls.szCheckboxLabels[4], listPtr[iCnt].value);
				jCnt++;
			}
			else if(strcmp(listPtr[iCnt].key, "CHECKBOX_LABEL6") == SUCCESS)
			{
				strcpy(stDevTran.stCustCheckBoxDtls.szCheckboxLabels[5], listPtr[iCnt].value);
				jCnt++;
			}
		}

		if(jCnt > 0)
		{
			if(*dpstDevTran == NULL)
			{
				/* Allocate memory to store the security details for the
				 * register request of the POS */
				*dpstDevTran = (DEVTRAN_PTYPE) malloc(iSize);
				if(*dpstDevTran == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Malloc FAILED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}
			}

			memset(*dpstDevTran, 0x00, iSize);
			memcpy(*dpstDevTran, &stDevTran, iSize);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * End of file sciReqDataCtrl.c
 * ============================================================================
 */
