/*
 * =============================================================================
 * Filename    : sciMain.c
 *
 * Application : Mx Point SCA
 *
 * Description : 
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <svcsec.h>
#if 0
#include <sys/time.h>

#include <svcsec.h>
#include <sys/timeb.h>
#endif

#include "common/common.h"
#include "common/xmlUtils.h"
#include "common/metaData.h"
#include "common/utils.h"
#include "sciDef.h"
#include "sciMain.h"
#include "db/dataSystem.h"
#include "db/posDataStore.h"
#include "sci/sciIface.h"
#include "sci/sciConfigDef.h"
#include "appLog/appLogAPIs.h"

typedef METADATA_STYPE	META_STYPE;
typedef METADATA_PTYPE	META_PTYPE;

#define SCIREQ_STR	"TRANSACTION"
#define SCIRESP_STR	"RESPONSE"

#define SCIEREQ_STR	    "ETRANSACTION"
#define SCIERESP_STR	"ERESPONSE"

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

/* Extern functions */
extern int getGenMetaDataForSSIReq(META_PTYPE);
extern int getGenMetaDataForSSIEReq(META_PTYPE );
extern int getFxnNCmdNCntrFromMetaData(META_PTYPE, int *, int *, char *);
extern int getMetaDataForPOSAuth(META_PTYPE);
extern int getMacLblNPayLoadFromMetaData(META_PTYPE , char ** , char ** );
extern int getAuthFlds(META_PTYPE, char **, char **, char **);
extern int authenticatePOS(char *, char *, char *, int , char *);
extern int getMetaDataForSCIReq(META_PTYPE, int, int);
extern int procParsedSCIReq(TRAN_PTYPE, META_PTYPE , char *, int);
extern int getMetaDataForSCIResp(META_PTYPE, TRAN_PTYPE);
extern int getMetaDataForSCIConsOptResp(META_PTYPE , UNSOLMSGINFO_PTYPE);
extern int getMetaDataForSCIVasDataResp(META_PTYPE , VASDATA_DTLS_PTYPE , RESPDTLS_PTYPE );
extern int getMetaDataForSCIEResp(META_PTYPE , char *, char *);
extern int loadSCIConfigParams();

extern int createQueue(char *);
extern int deleteQueue(char *);
extern int addDataToQ(void *, int, char *);
extern int getListeningPortNum();

/* Static global variables */
char	szSndQ[10]	= "";
char	szRcvQ[10]	= "";

/* Static functions */
static int	buildSCIResp(char **, int *, char *);
static int	getFxnNCmdNCntr(char *, int, int *, int *, int, char *);
static int	doPOSAuth(char *, int, char *, int, char *);
static int	parseSCIReq(char * , META_PTYPE, int, int);
static int	validateFxnCmdCombi(int, int, int);
static void connectionAvailable(char *);
static void dataAvailable(uchar *, int, char *, int);
static void disconnected(char *, void *);
static PAAS_BOOL posAuthReqd(int, int);
static int decryptXMLPacket(char * , char **, char *, char *);
static int decryptPayloadData(char *, char *, char **, char *);
static int encryptXMLPacket(uchar ** , uchar **, char *, int *);
static int encryptPayloadData(char **, char *, char **, int *);

#if 0
/* Added for the calculating the timings of the request and response*/
FILE*	fptr = NULL;
#endif

/*
 * ============================================================================
 * Function Name: initSCIModule
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int initSCIModule(char * szRcvQId)
{
	int				rv			  		= SUCCESS;
	int				listenPrimPortNum	= 0;
	int				listenScndPortNum	= 0;
	char			szErrMsg[256]		= "";
	SCIINIT_STYPE	stSCIParam;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{

		/* Load the configuration parameters into the application memory */
		rv	= loadSCIConfigParams();
		if(rv != SUCCESS)
		{
			/* Replacing debug_sprintf with sprintf so that syslogs get printed
			 * even with the execution of release version of the application. */
			sprintf(szErrMsg,"%s: FAILED to load SCI config params",__FUNCTION__);
			APP_TRACE(szErrMsg);
			syslog(LOG_ERR|LOG_USER, szErrMsg);
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Creating the Send queue between SCI module and POS", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Create a queue for the data sending */
		rv = createQueue(szSndQ);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to create snd Q", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		debug_sprintf(szDbgMsg, "%s: Send Queue id between SCI Module and POS [%s]", __FUNCTION__, szSndQ);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Creating the Receive queue between SCI module and POS", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Create a queue for the data recieving */
		rv = createQueue(szRcvQ);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to create rcv Q", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		debug_sprintf(szDbgMsg, "%s: Receive Queue id between SCI Module and POS [%s]", __FUNCTION__, szRcvQ);
		APP_TRACE(szDbgMsg);

		listenPrimPortNum	= getListeningPortNum();
		listenScndPortNum	= getListeningScndPortNum();

		memset(&stSCIParam, 0x00, sizeof(SCIINIT_STYPE));
		stSCIParam.iListenPrimPort	= listenPrimPortNum;
		stSCIParam.iListenScndPort	= listenScndPortNum;
		stSCIParam.iMaxConn 		= 10;
		stSCIParam.connFunc 		= connectionAvailable;
		stSCIParam.dataFunc 		= dataAvailable;
		stSCIParam.disconFunc 		= disconnected;

		strcpy(stSCIParam.szSndQ, szSndQ);

		rv = initSCIIface(&stSCIParam);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to init SCI iface",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		strcpy(szRcvQId, szRcvQ);

		break;
	}

	if(rv != SUCCESS)
	{
		/* In case of FAILURE, flush and delete the queues */
		if(strlen(szSndQ) > 0)
		{
			deleteQueue(szSndQ);
			memset(szSndQ, 0x00, sizeof(szSndQ));
		}

		if(strlen(szRcvQ) > 0)
		{
			deleteQueue(szRcvQ);
			memset(szRcvQ, 0x00, sizeof(szRcvQ));
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: closeSCIModule
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int closeSCIModule()
{
	int		rv				= SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Close the SCI interface */
		closeSCIIface();

		/* Flush and delete the queues meant for data sending and receiving */
		deleteQueue(szSndQ);
		deleteQueue(szRcvQ);

		memset(szSndQ, 0x00, sizeof(szSndQ));
		memset(szRcvQ, 0x00, sizeof(szRcvQ));

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: processSCIReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int processSCIReq(char ** szMsg, int iSize, char * szTranKey, char * szMACLbl,
											char * szErrMsg, int iPortType)
{
	int			rv			 		= SUCCESS;
	int			iFxn				= -1;
	int			iCmd				= -1;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szRootName[30]  	= "";
	char 		szTranCntr[11]		= "";
	char 		*szTmpMsg			= NULL;
	char		szAppLogData[300]	= "";
	char		szAppLogDiag[300]	= "";
	TRAN_PTYPE	pstTran				= NULL;
	META_STYPE	stMeta;
#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		memset(&stMeta, 0x00, METADATA_SIZE);
		if(*szMsg == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		memset(szAppLogData, 0x00, sizeof(szAppLogData));
		memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));


		/* Get the reference to the transaction placeholder */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get access to new tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		memset(szRootName, 0x00, sizeof(szRootName));
		rv = getXMLRootName(*szMsg, szRootName);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get root name of the SCI XML",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		debug_sprintf(szDbgMsg, "%s: The root name of the XML is %s",__FUNCTION__, szRootName);
		APP_TRACE(szDbgMsg);

		if(strcmp(szRootName, SCIEREQ_STR) == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Received XML is Encrypted one!!!",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				addAppEventLog(SCA, PAAS_INFO, SCI_REQUEST, *szMsg, NULL);
				strcpy(szAppLogData, "Received SCI Request is Encrypted, Decrypting the Request");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
			}

			pstTran->iEncrypted = 1;

			rv = decryptXMLPacket(*szMsg, &szTmpMsg, szMACLbl, szErrMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while decrypting XML!!!",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "Error while Decrypting the SCI Request");
					addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, NULL);
				}

				break;
			}
			if(*szMsg != NULL)
			{
				free(*szMsg);
				*szMsg = NULL;
			}
			*szMsg = szTmpMsg; //Will be processed as <TRANSACTION> here onwards
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "SCI Request After Decrypting");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);

				addAppEventLog(SCA, PAAS_INFO, SCI_REQUEST, szTmpMsg, NULL);
			}
		}
		else if(strcmp(szRootName, SCIREQ_STR) == 0)
		{
			debug_sprintf(szDbgMsg, "%s: Received XML is NON encrypted one!!!",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				addAppEventLog(SCA, PAAS_INFO, SCI_REQUEST, *szMsg, NULL);
			}

			pstTran->iEncrypted = 0;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Root name",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Invalid XML Root Name in the SSI Request");
				addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, NULL);
			}

			rv = ERR_INV_XML_MSG;
			break;
		}

		memset(szTranCntr, 0x00, sizeof(szTranCntr));
		/* Get the function and command for the POS request */
		rv = getFxnNCmdNCntr(*szMsg, iSize, &iFxn, &iCmd, iPortType, szTranCntr);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to get fxn & cmd", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Parse Function Type and Command From the SCI Request.");
				strcpy(szAppLogDiag, "Please Check for Valid Function Type and Command in the SCI Request");
				addAppEventLog(SCA, PAAS_FAILURE, RECEIVE, szAppLogData, szAppLogDiag);
			}

			break;
		}

		debug_sprintf(szDbgMsg,"%s: Transaction Counter [%s]", __FUNCTION__, szTranCntr);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg,"%s: Function [%d] Command [%d]", __FUNCTION__, iFxn, iCmd);
		APP_TRACE(szDbgMsg);


		/* Update the function, command and counter for the transaction */
		pstTran->iFxn = iFxn;
		pstTran->iCmd = iCmd;
		strcpy(pstTran->szTranCntr, szTranCntr);

		/* Get the authentication done if required */
		if(PAAS_TRUE == posAuthReqd(iFxn, iCmd))
		{
#if 0
			if(iFxn == SCI_ADM && iCmd == SCI_CANCEL)
			{
				pstTran->iEncrypted = 1;
			}//Praveen_P1: Testing purpose only to bypass the MAC authentication
#endif
			/*
			 * Passing the Encrypted flag to the following function
			 * If Flag is on, we have to do only counter test
			 * otherwise, we have to authenticate POS how we are doing currently
			 */
			rv = doPOSAuth(*szMsg, iSize, szMACLbl, pstTran->iEncrypted, szErrMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: POS authentication FAILED",
																__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
#if 0
			if(iFxn == SCI_ADM && iCmd == SCI_CANCEL)
			{
				pstTran->iEncrypted = 0;
			}
#endif
		}

		if(iFxn == SCI_SCND)
		{
			debug_sprintf(szDbgMsg, "%s: It is Secondary Port function type ", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			switch(iCmd)
			{
			case SCI_CANCEL:
				debug_sprintf(szDbgMsg, "%s: It is CANCEL command under Secondary Port function type", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			case SCI_REBOOT:
				debug_sprintf(szDbgMsg, "%s: It is REBOOT command under Secondary Port function type", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			case SCI_STATUS:
				debug_sprintf(szDbgMsg, "%s: It is STATUS command under Secondary Port function type", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			case SCI_ANY_UPDATES:
				debug_sprintf(szDbgMsg, "%s: It is ANY_UPDATES command under Secondary Port function type", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			case SCI_UPDATE_STATUS:
				debug_sprintf(szDbgMsg, "%s: It is REBOOT command under Secondary Port function type", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			default:
				debug_sprintf(szDbgMsg, "%s: It is INVALID command under Secondary Port function type", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = FAILURE;
				break;
			}

			if(rv != FAILURE)
			{
				break;
			}
		}

		/* Parse the rest of POS request */
		rv = parseSCIReq(*szMsg, &stMeta, iFxn, iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse rest of SCI req msg",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Process the parsed POS request */
		rv = procParsedSCIReq(pstTran, &stMeta, szMACLbl, pstTran->iEncrypted);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to process parsed SCI req",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	/* Free the meta data */
	/* DAIVIK:14-11-2015 - The Below free was not being done and was causing a memory leak per SCI Request, depending on the metadata , key value pairs.
	 * For Payment Transactions it causes a leak of about 1200 bytes per SCI Request.
	 */
	if(iFxn != SCI_RPT || rv != SUCCESS)
	{
		freeMetaDataEx(&stMeta); //Praveen_P1: We are copying this metadata to SSI tran which will be freed later
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sendSCIResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int sendSCIResp(char * szConnInfo, char * szTranKey, char *szMacLabel)
{
	int			rv					= SUCCESS;
	int			iLen				= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
	uchar *		szMsg				= NULL;
	uchar *		szTmpMsg			= NULL;
	TRAN_PTYPE	pstTran				= NULL;
	MSG_STYPE	stResp;

#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		if(szConnInfo == NULL || (szTranKey == NULL))
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Build the SCI response from the transaction data, whose access key
		 * has been provided */
		rv = buildSCIResp((char **)&szMsg, &iLen, szTranKey);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to build SCI resp",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* get the transaction data */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get access to new tran",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(pstTran->iEncrypted == 1)
		{
			debug_sprintf(szDbgMsg, "%s: Encryption flag is set",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Need to encrypt the data before sending!!!",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				addAppEventLog(SCA, PAAS_INFO, SCI_RESPONSE,(char*) szMsg, NULL);

				strcpy(szAppLogData, "Encrypting the Response Before Sending it to POS");
				addAppEventLog(SCA, PAAS_INFO, PROCESSING, szAppLogData, NULL);
			}

			rv = encryptXMLPacket(&szMsg, &szTmpMsg, szMacLabel, &iLen);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while encrypting the packet!!!",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				/*AjayS2: 16-May-2016
				 * Enhancement PTMX 1319: ETRANSACTION with non-existent MAC label causes error. 2.19.21 B5 (Amdocs 160329-7735)
				 * Sending Clear SCI response in MAC Label Missing case now
				 */
				if(rv == ERR_MAC_MISSING)
				{
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "MAC Label Does Not Exists, Sending Clear SCI Response");
						addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
					}
				}
				else
				{
					if(iAppLogEnabled == 1)
					{
						strcpy(szAppLogData, "Error While Encrypting the SCI Response");
						addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
					}
					break;
				}
			}
			if(rv != ERR_MAC_MISSING)
			{
				if(szMsg != NULL)
				{
					free(szMsg);
					szMsg = NULL;
				}
				szMsg = szTmpMsg;
			}

			if(iAppLogEnabled == 1)
			{
				addAppEventLog(SCA, PAAS_INFO, SCI_RESPONSE, (char*)szMsg, NULL);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Encryption flag is NOT set",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				addAppEventLog(SCA, PAAS_INFO, SCI_RESPONSE, (char*)szMsg, NULL);
			}

			debug_sprintf(szDbgMsg, "%s: Data can be sent as it is!!!",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* Prepare the message node for the SCI interface */
		memset(&stResp, 0x00, sizeof(MSG_STYPE));
		stResp.szMsg = szMsg;
		stResp.iLen = iLen;
		strcpy(stResp.szConnInfo, szConnInfo);

		/* Add the data to the queue for further processing by the SCI
		 * interface */
		rv = addDataToQ(&stResp, sizeof(MSG_STYPE), szSndQ);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to add data to Q",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/*
		 * Praveen_P1: TO_DO Need to know why szMsg set to NULL
		 */
		szMsg = NULL;

		break;
	}

	if(rv != SUCCESS)
	{
		if(szMsg != NULL)
		{
			free(szMsg);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: sendSCIUnsolMsg
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int sendSCIUnsolMsg(UNSOLMSGINFO_PTYPE pstUnsolMsgInfo, char * pszMacLbl, int iEncFlag, char* szAppLogDiag)
{
	int					rv					= SUCCESS;
	int					iLen				= 0;
	int					iAppLogEnabled		= isAppLogEnabled();
	uchar *				szMsg				= NULL;
	uchar *				szTmpMsg			= NULL;
	VASDATA_DTLS_STYPE	stVasDataDtls;
	RESPDTLS_STYPE		stRespDtls;

	META_STYPE	stMeta;
#ifdef DEVDEBUG
	char			szDbgMsg[5120]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstUnsolMsgInfo == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input Param is NULL!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv= FAILURE;
			break;
		}


		if((strlen(pstUnsolMsgInfo->szPOSIP) == 0) || (strlen(pstUnsolMsgInfo->szPOSPort) == 0))
		{
			debug_sprintf(szDbgMsg, "%s: Server details are not available to send data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogDiag, "POS Server Details Not Available to Send the Unsolicited Message");
			}
			rv= FAILURE;
			break;
		}
#if 0
		rv = connectToServer(pstUnsolMsgInfo->szPOSIP, atoi(pstUnsolMsgInfo->szPOSPort), &iFd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while creating client!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
#endif
		memset(&stMeta, 0x00, METADATA_SIZE);
		if(strlen(pstUnsolMsgInfo->szVASLoyaltyData) > 0)
		{
			memset(&stVasDataDtls, 0x00, sizeof(VASDATA_DTLS_STYPE));
			strcpy(stVasDataDtls.szPubliserName, pstUnsolMsgInfo->szPubliserName);
			strcpy(stVasDataDtls.szLoyaltyData, pstUnsolMsgInfo->szVASLoyaltyData);
			stVasDataDtls.bVASPresent = PAAS_TRUE;

			memset(&stRespDtls, 0x00, sizeof(RESPDTLS_STYPE));
			strcpy(stRespDtls.szRespTxt, "CONSUMER PRE TAPPED VAS DATA");
			strcpy(stRespDtls.szRslt, "OK");
			strcpy(stRespDtls.szRsltCode, "-1");
			strcpy(stRespDtls.szTermStat, "SUCCESS");

			rv = getMetaDataForSCIVasDataResp(&stMeta, &stVasDataDtls, &stRespDtls);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get VAS Meta data",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else
		{
			rv = getMetaDataForSCIConsOptResp(&stMeta, pstUnsolMsgInfo);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}

		/* Call the xml utility api to build the xml message from the metadata
		 * just build from the transaction data */
		rv = buildXMLMsgFromMetaData((char **)&szMsg, &iLen, SCIRESP_STR, &stMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build SCI response message",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		/* Free the meta data */
		freeMetaDataEx(&stMeta);

		if(iEncFlag == 1)
		{
			debug_sprintf(szDbgMsg, "%s: Encryption flag is set",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Need to encrypt the data before sending!!!",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = encryptXMLPacket(&szMsg, &szTmpMsg, pszMacLbl, &iLen);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while encrypting the packet!!!",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			if(szMsg != NULL)
			{
				free(szMsg);
				szMsg = NULL;
			}
			szMsg = szTmpMsg;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Encryption flag is NOT set",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			debug_sprintf(szDbgMsg, "%s: Data can be sent as it is!!!",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		rv = sendDatatoSCIServer(szMsg, iLen, pstUnsolMsgInfo->szPOSIP, atoi(pstUnsolMsgInfo->szPOSPort));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to send data to SCI server",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogDiag, "Failed to Connect/Send to POS Server");
			}
		}
#if 0
		rv = writeBytes(iFd, iLen, szMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to write Consumer Option data to POS",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
#endif
		//Freeing the message
		if(szMsg != NULL)
		{
			free(szMsg);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: buildSCIResp
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int buildSCIResp(char ** szMsg, int * iLen, char * szTranKey)
{
	int				rv			 		= SUCCESS;
	TRAN_PTYPE		pstTran				= NULL;
	TRAN_PTYPE		pstSSITran			= NULL;
	META_STYPE		stMeta;
	PASSTHRU_PTYPE	pstPassThru			= NULL;
	char			szSSITranKey[10] 	= "";
	int				iRspMsgLen			= 0;
#ifdef DEVDEBUG
	char			szDbgMsg[4096]		= "";
#elif DEBUG
	char			szDbgMsg[256]		= "";
#endif


	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* get the transaction data */
		rv = getTran(szTranKey, &pstTran);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get access to new tran",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/*
		 * Praveen_P1: For the Report response, we need not build the XML response
		 * we can send the XML response received from the SSI host as it is
		 * TO_DO: Please see if this can be moved from here and make it better
		 */
		if(pstTran->iFxn == SCI_RPT && pstTran->iStatus == SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: REPORT function, have to build SCI resp separately", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			strcpy(szSSITranKey, (char *)pstTran->data);

			/* Get a reference to this new SSI transaction (stored in stack) */
			rv = getTopSSITran(szSSITranKey, &pstSSITran);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to access SSI transaction",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			debug_sprintf(szDbgMsg, "%s: Got the SSI Tran instance", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pstPassThru = (PASSTHRU_PTYPE)pstSSITran->data;
			if(pstPassThru == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: No Pass through data!!!",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				/*
				 * Should not be the case..some problem in the code
				 */
				rv = FAILURE;
				break;
			}
			/*
			 * Assigning the xml response received from the HOST to build the
			 * response to SCI
			 */

			if(pstPassThru->szRespMsg != NULL)
			{
				/*
				 * This msg bening deallocated after sending it to SCI
				 * in dataSender func. passthru also bening deallocated
				 * seperately by cleantran func. Thats why reallocating
				 * memory for szMsg here even though memory is there for
				 * passthru msg
				 */
				iRspMsgLen = strlen(pstPassThru->szRespMsg);

				debug_sprintf(szDbgMsg, "%s: Response length is %d",__FUNCTION__, iRspMsgLen);
				APP_TRACE(szDbgMsg);

				*szMsg = (char *)malloc(sizeof(char) * (iRspMsgLen + 1));

				if(*szMsg == NULL)
				{
					debug_sprintf(szDbgMsg, "%s: Error in allocation memory for resp msg",__FUNCTION__);
					APP_TRACE(szDbgMsg);

					rv = FAILURE;
					break;
				}

				memset(*szMsg, 0x00, iRspMsgLen + 1);
				memcpy(*szMsg, pstPassThru->szRespMsg, iRspMsgLen);
				*iLen = iRspMsgLen;

			#ifdef DEVDEBUG
				if(iRspMsgLen < 4000)
				{
					debug_sprintf(szDbgMsg, "%s:%s: Resp Msg = [%s]",DEVDEBUGMSG,__FUNCTION__, *szMsg);
					APP_TRACE(szDbgMsg);
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s:%s: Resp Msg length= [%d]",DEVDEBUGMSG,__FUNCTION__, iRspMsgLen);
					APP_TRACE(szDbgMsg);
				}
			#endif
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: No response data to assign!!!",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				/*
				 * Should not be the case..some problem in the code
				 */
				rv = FAILURE;
				break;
			}
		}
		else
		{
			/* Got the transaction data. Use this data to create the metadata for
			 * message building */
			memset(&stMeta, 0x00, METADATA_SIZE);
			rv = getMetaDataForSCIResp(&stMeta, pstTran);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}

			/* Call the xml utility api to build the xml message from the metadata
			 * just build from the transaction data */
			rv = buildXMLMsgFromMetaData(szMsg, iLen, SCIRESP_STR, &stMeta);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to build SCI response message",
																	__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
			/* Free the meta data */
			freeMetaDataEx(&stMeta);
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: encryptXMLPacket
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int encryptXMLPacket(uchar ** pszClearMsg, uchar **pszEncryptedMsg, char *pszMacLabel, int *piLen)
{
	int			rv					= SUCCESS;
	int			iEncodedLen			= 0;
	int			iEncLength			= 0;
	int			iAppLogEnabled		= isAppLogEnabled();
	char 		*szEncPayloadData   = NULL;
	char		*szEncodedData		= NULL;
	char		szAppLogData[300]	= "";
	META_STYPE	stMeta;

#ifdef DEVDEBUG
	char			szDbgMsg[4096]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	// CID-67387: 27-Jan-16: MukeshS3: Intialize the meta data at beginning
	memset(&stMeta, 0x00, METADATA_SIZE);
	while(1)
	{
		if(*pszClearMsg == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(szAppLogData, 0x00, sizeof(szAppLogData));

		/* Encrypt the Payload Data */
		rv = encryptPayloadData((char **)pszClearMsg, pszMacLabel, &szEncPayloadData, &iEncLength);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to encrypt PayLoad Data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

#ifdef DEVDEBUG
#if 0
		sprintf(szDbgMsg, "%s: Encrypted Data = ", __FUNCTION__);
		for(iCnt = 0; iCnt < iEncLength; iCnt++)
		{
			sprintf(szTmp, "%02X ", szEncPayloadData[iCnt]);
			strcat(szDbgMsg, szTmp);
		}
		APP_TRACE(szDbgMsg);
#endif
#endif

		debug_sprintf(szDbgMsg, "%s: Encrypted Buffer length %d",__FUNCTION__, iEncLength);
		APP_TRACE(szDbgMsg);

		/*
		 * Encoded length will be dependent on the length of the Encrypted buffer
		 * so passing iEncLength as the parameter in the above function
		 * which is used to calculate the encoded length
		 */
		iEncodedLen = (( iEncLength / 3) * 4) + 2 + 10 /*Added 10 for safety purpose */;

		debug_sprintf(szDbgMsg, "%s: Allocating Encoded length[%d] buffer", __FUNCTION__, iEncodedLen);
		APP_TRACE(szDbgMsg);


		szEncodedData = (char *)malloc(sizeof(char) * (iEncodedLen + 1));
		if(szEncodedData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc failed for encoded buffer!!!",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(szEncodedData, 0x00, iEncodedLen + 1);

		rv = encdBase64((uchar *)szEncPayloadData, iEncLength, (uchar *)szEncodedData, &iEncodedLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Base 64 encoding of payload data failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Base64 Encoding of Payload Data Failed");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, NULL);
			}

			rv = ERR_ENCODE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Encoded Buffer Length %d", __FUNCTION__, iEncodedLen);
		APP_TRACE(szDbgMsg);

		//memset(&stMeta, 0x00, METADATA_SIZE);
		rv = getMetaDataForSCIEResp(&stMeta, szEncodedData, pszMacLabel);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get meta data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Call the xml utility api to build the xml message from the metadata
		 * just build from the transaction data */
		rv = buildXMLMsgFromMetaData((char **)pszEncryptedMsg, piLen, SCIERESP_STR, &stMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to build SCI ERESPONSE message",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		// CID-67387: 27-Jan-16: MukeshS3:
		// Free allocated memory after breaking from while loop, so, need not write it for each failure case
		// So, shifting it down after while loop block
#if 0
		/* Free the meta data */
		freeMetaDataEx(&stMeta);

		if(szEncPayloadData != NULL)
		{
			free(szEncPayloadData);
		}
		if(szEncodedData != NULL)
		{
			free(szEncodedData);
		}
#endif
		break;
	}
	// CID-67387: 27-Jan-16: MukeshS3:
	// Free allocated memory after breaking from while loop, so, need not write it for each failure case
	/* Free the meta data */
	freeMetaDataEx(&stMeta);

	if(szEncPayloadData != NULL)
	{
		free(szEncPayloadData);
	}
	if(szEncodedData != NULL)
	{
		free(szEncodedData);
	}
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;

}

/*
 * ============================================================================
 * Function Name: decryptXMLPacket
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int decryptXMLPacket(char * pszEncryptedMsg, char **pszDecryptedMsg, char *pszMacLabel, char *pszErrMsg)
{
	int			rv					= SUCCESS;
	META_STYPE	stMeta;
	char *		szMACLbl			= NULL;
	char *		szPayloadData		= NULL;
	char *		szClearPayloadData 	= NULL;

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pszEncryptedMsg == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		rv = getGenMetaDataForSSIEReq(&stMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get gen meta data for ETran",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		/* Parse the xml message */
		rv = parseXMLMsg_1(pszEncryptedMsg, SCIEREQ_STR, NULL, &stMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse XML msg",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		rv = getMacLblNPayLoadFromMetaData(&stMeta, &szMACLbl, &szPayloadData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get Mac Label & Payload",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		strcpy(pszMacLabel, szMACLbl); //Copying the MAclabel

		rv = decryptPayloadData(szPayloadData, szMACLbl, &szClearPayloadData, pszErrMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to decrypt PayLoad Data",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		*pszDecryptedMsg = szClearPayloadData; //Assigning the clear data buffer

		/* Free the meta data */
		freeMetaDataEx(&stMeta); //Praveen_P1: Please check whether it is required here

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: encryptPayloadData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int encryptPayloadData(char **pszClearData, char *pszMacLabel, char **pszEncData, int *piEncLen)
{
	int		rv					= SUCCESS;
    int		iAppLogEnabled		= isAppLogEnabled();
    char	szAppLogData[300]	= "";
	char	szAESKey[16]    	= "";
	char	szErrMsg[256]  	 	= "";

#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(*pszClearData == NULL || pszMacLabel == NULL)
		{

			debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Get the AES key for the corresponding label (from the data system) */
		rv = getAESKey(pszMacLabel, (uchar *)szAESKey, szErrMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Couldnt get AES key", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(rv == ERR_NO_FLD)
			{
				rv = ERR_MAC_MISSING;
			}

			break;
		}

		rv = encryptDataWithAES(pszClearData, pszEncData, szAESKey, piEncLen);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: AES Encryption failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//CID 67190 (#1 of 1): Logically dead code (DEADCODE) T_raghavendranR1
			memset(szAppLogData, 0x00, sizeof(szAppLogData));
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "AES Encryption Error");
				addAppEventLog(SCA, PAAS_ERROR, PROCESSING, szAppLogData, NULL);
			}

			rv = ERR_AES_ENCRYPTION;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: decryptPayloadData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int decryptPayloadData(char *pszEncData, char *pszMacLabel, char **pszDecData, char *pszErrMsg)
{
	int			rv				= SUCCESS;
	int			iLength			= 0;
	char		szAESKey[16+1]    = "";
	char 		*pszDecodedData = NULL;
	int			iDecodedDataLen = 0;

#ifdef DEVDEBUG
	char			szDbgMsg[4096]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif


	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pszEncData == NULL || pszMacLabel == NULL)
		{

			debug_sprintf(szDbgMsg, "%s: Input param is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

		/* Get the AES key for the corresponding label (from the data system) */
		rv = getAESKey(pszMacLabel, (uchar *)szAESKey, pszErrMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Couldnt get AES key", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		iDecodedDataLen = ((strlen((char *)pszEncData) / 4) * 3 ) + 1;

		pszDecodedData = (char *)malloc(sizeof(char) * iDecodedDataLen);
		if(pszDecodedData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Malloc failed for decoded data!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(pszDecodedData, 0x00, iDecodedDataLen);

		/* Decode the payload data */
		rv = dcdBase64((unsigned char *)pszEncData, strlen((char *)pszEncData), (unsigned char *)pszDecodedData, &iLength);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: dcdBase64 failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}

#ifdef DEVDEBUG
#if 0
		sprintf(szDbgMsg, "%s: Payload Data = ", __FUNCTION__);
		for(iCnt = 0; iCnt < iLength; iCnt++)
		{
			sprintf(szTmp, "%02X ", pszDecodedData[iCnt]);
			strcat(szDbgMsg, szTmp);
		}
		APP_TRACE(szDbgMsg);
#endif
#endif

		*pszDecData = (char *)malloc(sizeof(char) * (iLength + 1)); // Length of encrypted and decrypted data will be same
		if(*pszDecData == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while allocating memory!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = FAILURE;
			break;
		}
		memset(*pszDecData, 0x00, iLength + 1);

		rv = decryptDataWithAES(pszDecodedData, *pszDecData, szAESKey, iLength);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: AES Decryption failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		break;
	}

	if(pszDecodedData != NULL)
	{
		free(pszDecodedData);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getFxnNCmdNCntr
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getFxnNCmdNCntr(char * szMsg, int iLen, int * iFxn, int * iCmd, int iPortType, char * pszTranCntr)
{
	int			rv					= SUCCESS;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
	char		szAppLogDiag[300]	= "";
	META_STYPE	stMeta;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		memset(&stMeta, 0x00, METADATA_SIZE);

		/* Get the generic meta data for parsing the function and command from
		 * the xml request message sent by POS */
		rv = getGenMetaDataForSSIReq(&stMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get gen meta data",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Parse the xml message */
		rv = parseXMLMsg_1(szMsg, SCIREQ_STR, NULL, &stMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse XML msg",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Get the function, command and counter from the parsed xml data */
		rv = getFxnNCmdNCntrFromMetaData(&stMeta, iFxn, iCmd, pszTranCntr);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get fxn & cmd",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Command and Function Type from the Command.");
				strcpy(szAppLogDiag, "Please Check the Command Sent");
				addAppEventLog(SCA, PAAS_ERROR, RECEIVE, szAppLogData, szAppLogDiag);
			}

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Fxn  %d, cmd  %d", __FUNCTION__, *iFxn,
						*iCmd);
		APP_TRACE(szDbgMsg);

		/* Check for valid combinations of the function and commands */
		rv = validateFxnCmdCombi(*iFxn, *iCmd, iPortType);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid fxn/cmd", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Received Invalid Combination of Command and Function Type.");
				strcpy(szAppLogDiag, "Please Check the Command Sent");
				addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, szAppLogDiag);
			}

			break;
		}

		break;
	}

	/* Free the meta data */
	freeMetaDataEx(&stMeta);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: doPOSAuth
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int doPOSAuth(char * szMsg, int iLen, char * pszMacLbl, int iEncFlag, char * szErrMsg)
{
	int			rv					= SUCCESS;
	int			iAppLogEnabled		= isAppLogEnabled();
	char *		szMACLbl			= NULL;
	char *		szMACCtr			= NULL;
	char *		szTranCtr			= NULL;
	char		szAppLogData[300]	= "";
	char		szAppLogDiag[300]	= "";
	META_STYPE	stMeta;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	while(1)
	{
		/* Get the meta data for parsing the POS auth fields */ 
		rv = getMetaDataForPOSAuth(&stMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get POS auth meta data",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}
		memset(szAppLogData, 0x00, sizeof(szAppLogData));

		/* Parse the xml message */
		rv = parseXMLMsg_1(szMsg, SCIREQ_STR, NULL, &stMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse XML msg",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		/* Get the authentication related fields from the parsed xml data */
		rv = getAuthFlds(&stMeta, &szMACLbl, &szMACCtr, &szTranCtr);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg,"%s: FAILED to get auth flds",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Error, Authentication of POS Failed.");
				strcpy(szAppLogDiag, "Failed to Get the POS Authentication Fields in the POS Request");
				addAppEventLog(SCA, PAAS_FAILURE, PROCESSED, szAppLogData, szAppLogDiag);
			}

			break;
		}
		/*
		 * We have made MAC field in the authLst as the non-mandatory field to make it
		 * generic for two cases (encrypted and non encrypted packet)
		 * It is mandatory for Non-Encrypted packets from POS but not mandatory
		 * for encrypted packets
		 * We have the following condition to take care of it for non-encrypted packets
		 */
		if(iEncFlag != 1)
		{
			if(szMACCtr == NULL)
			{
				debug_sprintf(szDbgMsg,"%s: MAC field is missing which is mandatory!!!",__FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(iAppLogEnabled == 1)
				{
					strcpy(szAppLogData, "MAC Data Field Not Available to do POS Authentication.");
					strcpy(szAppLogDiag, "Please Fill MAC Data Field in the POS Request to Process.");
					addAppEventLog(SCA, PAAS_ERROR, PROCESSED, szAppLogData, szAppLogDiag);
				}

				rv = ERR_FLD_REQD;
				break;
			}
		}
		/* Authenticate the POS request */
		rv = authenticatePOS(szTranCtr, szMACCtr, szMACLbl, iEncFlag, szErrMsg);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to do POS auth", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "POS Authentication Done Successfully");
			addAppEventLog(SCA, PAAS_SUCCESS, PROCESSED, szAppLogData, NULL);
		}
		strcpy(pszMacLbl, szMACLbl);

		break;
	}

	/* Free the meta data */
	freeMetaDataEx(&stMeta);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: parseSCIReq
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int parseSCIReq(char * szMsg, META_PTYPE pstMeta, int iFxn, int iCmd)
{
	int			rv					= SUCCESS;
	int			iAppLogEnabled		= isAppLogEnabled();
	char		szAppLogData[300]	= "";
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	while(1)
	{
		rv = getMetaDataForSCIReq(pstMeta, iFxn, iCmd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to get key list", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Get the Data List From SCI Request");
				addAppEventLog(SCA, PAAS_FAILURE, RECEIVE, szAppLogData, NULL);
			}

			break;
		}

		rv = parseXMLMsg_1(szMsg, SCIREQ_STR, NULL, pstMeta);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to parse XML msg",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szAppLogData, "Failed to Parse the SCI Request");
				addAppEventLog(SCA, PAAS_FAILURE, RECEIVE, szAppLogData, NULL);
			}
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: posAuthReqd
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static PAAS_BOOL posAuthReqd(int iFxn, int iCmd)
{
	PAAS_BOOL	bRv				= PAAS_FALSE;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(isPOSAuthReqdFromConfig() == PAAS_FALSE)
	{
		debug_sprintf(szDbgMsg, "%s: --- POS Authentication Required is Disabled From Config. So for all Transactions, POS Authentication will be DIsabled", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		bRv = PAAS_FALSE;
	}
	else
	{
		switch(iFxn)
		{
		case SCI_SEC:
			if(iCmd == SCI_TESTMAC)
			{
				bRv = PAAS_TRUE;
			}
			else
			{
				bRv = PAAS_FALSE;
			}
			break;

		case SCI_SESS:
		case SCI_LI:
		case SCI_SAF:
		case SCI_DEV:
			if(iCmd == SCI_GET_DEVICENAME || iCmd == SCI_SET_DEVICENAME)
			{
				bRv = PAAS_FALSE;
			}
			else
			{
				bRv = PAAS_TRUE;
			}
			break;

		case SCI_PYMT:
		case SCI_BATCH:
		case SCI_RPT:
			bRv = PAAS_TRUE;
			break;

		case SCI_ADM:
			if(iCmd == SCI_GETCOUNTER) //Praveen_P1: Currently setting POS Auth not required for Getcounter command
			{
				bRv = PAAS_FALSE;
			}
			else
			{
				bRv = PAAS_TRUE;
			}

			break;

		case SCI_SCND:
			bRv = PAAS_FALSE;
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Should not come here", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			bRv = PAAS_TRUE;
			break;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__,
										(bRv == PAAS_TRUE) ? "TRUE" : "FALSE");
	APP_TRACE(szDbgMsg);

	return bRv;
}

/*
 * ============================================================================
 * Function Name: validateFxnCmdCombi
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int validateFxnCmdCombi(int iFxn, int iCmd, int iPortType)
{
	int		rv					= SUCCESS;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);


	if(iFxn == SCI_SCND && iPortType == PRIMARY)
	{
		debug_sprintf(szDbgMsg, "%s: Secondary Port Command given on Primary port", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = ERR_INV_COMBI;
	}
	else if(iFxn != SCI_SCND && iPortType == SECONDARY)
	{
		debug_sprintf(szDbgMsg, "%s:  Primary Port Command given on Secondary port", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(iAppLogEnabled == 1)
		{
			strcpy(szAppLogData, "Validating the Command Received Through Secondary Port");
			addAppEventLog(SCA, PAAS_INFO, RECEIVE, szAppLogData, NULL);
		}

		rv = ERR_INV_COMBI;
	}

	if(rv == SUCCESS)
	{
		switch(iFxn)
		{
		case SCI_SEC:
			switch(iCmd)
			{
			case SCI_REG:
			case SCI_EREG:
			case SCI_UNREG:
			case SCI_UNREGALL:
			case SCI_TESTMAC:
				/* Valid commands list for SECURITY function */
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Invalid cmd [%d]", __FUNCTION__, iCmd);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_COMBI;
				break;
			}
			break;

		case SCI_SESS:
			switch(iCmd)
			{
			case SCI_START:
			case SCI_FINISH:
				/* Valid commands list for SESSION function */
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Invalid cmd [%d]", __FUNCTION__, iCmd);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_COMBI;
				break;
			}

			break;

		case SCI_LI:
			switch(iCmd)
			{
			case SCI_ADD:
			case SCI_REMOVE:
			case SCI_REMOVEALL:
			case SCI_OVERRIDE:
			case SCI_SHOW:
				/* Valid commands list for LINE_ITEM function */
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Invalid cmd [%d]", __FUNCTION__, iCmd);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_COMBI;
				break;
			}

			break;

		case SCI_SAF:
			switch(iCmd)
			{
			case SCI_QUERY:
			case SCI_REMOVE:
				/* Valid commands list for SAF function */
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Invalid cmd [%d]", __FUNCTION__, iCmd);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_COMBI;
				break;
			}

			break;

		case SCI_DEV:
			switch(iCmd)
			{
			case SCI_SIGN:
			case SCI_SIGN_EX:
			case SCI_EMAIL:
			case SCI_LTY:
			case SCI_CUST_QUES:
			case SCI_SURVEY5:
			case SCI_SURVEY10:
			case SCI_VERSION:
			case SCI_CHARITY:
			case SCI_GET_DEVICENAME:
			case SCI_SET_DEVICENAME:
			case SCI_EMP_ID_CAPT:
			case SCI_CUST_BUTTON:
			case SCI_CREDIT_APP:
			case SCI_GET_PAYMENT_TYPES:
			case SCI_SET_PARM:
			case SCI_DISPLAY_MESSAGE:
			case SCI_GET_PARM:
			case SCI_PROVISION_PASS:
			case SCI_GET_CARD_DATA:
			case SCI_QUERY_NFCINI:
			case SCI_DISP_QRCODE:
			case SCI_CANCEL_QRCODE:
			case SCI_CUST_CHECKBOX:
			/* Valid commands list for DEVICE function */
			break;

			default:
				debug_sprintf(szDbgMsg, "%s: Invalid cmd [%d]", __FUNCTION__, iCmd);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_COMBI;
				break;
			}

			break;

		case SCI_PYMT:
			switch(iCmd)
			{
			case SCI_AUTH:
			case SCI_CAPTURE:
			case SCI_CREDIT:
			case SCI_VOID:
			case SCI_ACTIVATE:
			case SCI_ADDVAL:
			case SCI_BAL:
			case SCI_GIFTCLOSE:
			case SCI_DEACTIVATE:
			case SCI_REACTIVATE:
			case SCI_REMVAL:
			case SCI_COMPLETION:
			case SCI_ADDTIP:
			case SCI_RESETTIP:
			case SCI_LPTOKEN:
			case SCI_TOKEN:
			case SCI_PAYACCOUNT:
			case SCI_TOKEN_UPDATE:
				/* Valid commands list for Payment function */
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Invalid cmd [%d]", __FUNCTION__, iCmd);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_COMBI;
				break;
			}

			break;

		case SCI_BATCH:
			switch(iCmd)
			{
			case SCI_STL:
				/* Valid commands list for BATCH function */
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Invalid cmd [%d]", __FUNCTION__, iCmd);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_COMBI;
				break;
			}

			break;

		case SCI_RPT:
			switch(iCmd)
			{
			case SCI_DAYSUMM:
			case SCI_PRESTL:
			case SCI_STLERR:
			case SCI_TRANSEARCH:
			case SCI_STLSUMM:
			case SCI_LASTTRAN:
			case SCI_DUPCHK:
			case SCI_CUTOVER:
			case SCI_SITETOTALS:
				/* Valid commands list for REPORT function */
				break;

			default:
				debug_sprintf(szDbgMsg, "%s: Invalid cmd [%d]", __FUNCTION__, iCmd);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_COMBI;
				break;
			}

			break;

		case SCI_ADM:
			switch(iCmd)
			{
			case SCI_SETTIME:
			case SCI_GETCOUNTER:
        	case SCI_LANE_CLOSED:
        	case SCI_APPLYUPDATES:
				/* Valid commands list for ADMIN function */
				break;
			default:
				debug_sprintf(szDbgMsg, "%s: Invalid cmd [%d]", __FUNCTION__, iCmd);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_COMBI;
				break;
			}
			break;

		case SCI_SCND:
			switch(iCmd)
			{
			case SCI_CANCEL:
			case SCI_REBOOT:
			case SCI_STATUS:
			case SCI_ANY_UPDATES:
			case SCI_UPDATE_STATUS:
				break;
			default:
				debug_sprintf(szDbgMsg, "%s: Invalid cmd [%d]", __FUNCTION__, iCmd);
				APP_TRACE(szDbgMsg);

				rv = ERR_INV_COMBI;
				break;
			}
			break;

		default:
			debug_sprintf(szDbgMsg, "%s: Invalid fxn [%d]", __FUNCTION__, iFxn);
			APP_TRACE(szDbgMsg);

			rv = ERR_UNSUPP_OPR;
			break;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: connectionAvailable
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void connectionAvailable(char * szConnInfo)
{
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: New connection available from [%s]",
													__FUNCTION__, szConnInfo);
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: dataAvailable
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void dataAvailable(uchar * szData, int iSize, char * szConnInfo, int iPortType)
{
	int				rv				= SUCCESS;
	uchar *			pszMsg			= NULL;
	SCIDATA_STYPE	stSCIData;
	
	#if 0
	char			szTSPrefix[20]  = "";
	struct tm *		tm_ptr			= NULL;
	struct timeb	the_time;
	#endif

#ifdef DEVDEBUG
	char			szDbgMsg[4096]	= "";
#elif DEBUG
	char			szDbgMsg[256]	= "";
#endif

	while(1)
	{
#ifdef DEVDEBUG
		if(iSize < 4000)
		{
			debug_sprintf(szDbgMsg, "%s:%s: POS REQ = %s",DEVDEBUGMSG,__FUNCTION__, szData);
			APP_TRACE(szDbgMsg);
		}
#endif
		#if 0
		fptr = fopen("/home/usr1/flash/timingsForLI.txt", "a");


		ftime(&the_time);
		tm_ptr = gmtime(&the_time.time);
		if(the_time.millitm >= 1000)
		{
			/* Keep within 3 digits, since sprintf's width.precision specification
			 * does not truncate large values */
			the_time.millitm = 999;
		}

		sprintf(szTSPrefix, "%02d:%02d:%02d:%03d", tm_ptr->tm_hour,
					tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

		fprintf (fptr, "Request arrived at %s\n", szTSPrefix);
		#endif
		
		/* Allocate memory for the SCI data */
		pszMsg = (uchar *) malloc(iSize + 1);
		if(pszMsg == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pszMsg, 0x00, iSize + 1);
		memcpy(pszMsg, szData, iSize);

		/* Populate the SCI data */
		memset(&stSCIData, 0x00, sizeof(SCIDATA_STYPE));
		stSCIData.iEvtType	= DATA;
		stSCIData.szMsg 	= pszMsg;
		stSCIData.iPortType = iPortType;

		strcpy(stSCIData.szConnInfo, szConnInfo);

		/* Add the data to the queue for further processing */
		rv = addDataToQ(&stSCIData, sizeof(SCIDATA_STYPE), szRcvQ);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to add data to Q",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		break;
	}

	if(rv != SUCCESS)
	{
		if(pszMsg != NULL)
		{
			free(pszMsg);
		}
	}

	return;
}

/*
 * ============================================================================
 * Function Name: disconnected
 *
 * Description	: It is a callback function. It will be called by the SCI
 * 					interface upon getting a disconnection from a POS.
 *
 * Input Params	: szConnInfo -> connection Information of the POS
 *
 * Output Params: none
 * ============================================================================
 */
static void disconnected(char * szConnInfo, void * pstSCIDataForConn)
{
	int				rv				= SUCCESS;
	int				iFd				= 0;
	SCIDATA_STYPE	stSCIData;
	CONN_PTYPE		pstConnInfoHandle	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: %s got disconnected",__FUNCTION__,szConnInfo);
	APP_TRACE(szDbgMsg);

	getFDFrmConnList(szConnInfo, &iFd, (SCI_PTYPE) pstSCIDataForConn, &pstConnInfoHandle);

	if(pstConnInfoHandle != NULL && pstConnInfoHandle->bDataSent == PAAS_TRUE)
	{
		debug_sprintf(szDbgMsg, "%s: Data Already Sent, Ignoring the Disconnection",__FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else
	{
		/* Populate the SCI data */
		memset(&stSCIData, 0x00, sizeof(SCIDATA_STYPE));
		stSCIData.iEvtType = DISCONN;
		strcpy(stSCIData.szConnInfo, szConnInfo);

		/* Add the data to the queue for further processing */
		rv = addDataToQ(&stSCIData, sizeof(SCIDATA_STYPE), szRcvQ);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to add data to Q", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	return;
}

/* Temporary debugging thing */
void printQIds()
{
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: sndQ=%s, rcvQ=%s", __FUNCTION__, szSndQ,
					szRcvQ);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * End of file sciMain.c
 * ============================================================================
 */
