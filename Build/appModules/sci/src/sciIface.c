/*
 * =============================================================================
 * Filename    : sciIface.c
 *
 * Application : Mx Point SCA
 *
 * Description : 
 *
 * Modification History:
 * 
 *  Date      Version No     Programmer       Change History
 *  -------   -----------  	 ----------- 	  ------------------------
 *                           Vikram Datt Rana
 *
 * DISCLAIMER : Copyright (C) 1998-1999 by VeriFone Inc. All rights reserved. 
 *              No part of this software may be used, stored, compiled,
 *              reproduced, modified, transcribed, translated, transmitted, or
 *              transferred, in any form or by any means  whether electronic,
 *              mechanical,  magnetic, optical, or otherwise, without the
 *              express prior written permission of VeriFone, Inc.
 * =============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/time.h>

#include "svc.h"

#include <svcsec.h>
#include <sys/timeb.h>

#include "common/utils.h"
#include "sci/sciConfigDef.h"
#include "sci/sciMain.h"
#include "sci/sciIface.h"
#include "db/dataSystem.h"
#include "appLog/appLogAPIs.h"

#define MAX_RETRIES			6
#define DFLT_MAX_CONN		5
#define DFLT_LISTEN_PRT		5015

//MemDebug
#define malloc(size)		scaMalloc(size, __LINE__, (char*)__FUNCTION__);
#define realloc(ptr, size)	scaReAlloc((void*)ptr, size, __LINE__, (char*)__FUNCTION__);
#define free(ptr)			scaFree((void**)&(ptr), __LINE__, (char*)__FUNCTION__);

typedef struct sockaddr_in	ADDR_IN;
typedef struct sockaddr		ADDR;
typedef struct timeval		TIME_VAL;

static PAAS_BOOL	bRunListener;
static PAAS_BOOL	bRunSender;
static pthread_t	threads[3];

/* Static functions declarations */
static int startServer(char *, int, int *);
static int getNewPrimConns(SCI_PTYPE);
static int getNewScndConns(SCI_PTYPE);
static int checkForPrimConnData(int, SCI_PTYPE);
static int checkForScndConnData(int, SCI_PTYPE);
static int writeBytes(int, int, uchar *);
static int addConnToPrimList(int, char *, SCI_PTYPE);
static int addConnToScndList(int, char *, SCI_PTYPE);
static int remConnFrmList(int, SCI_PTYPE);
static int populatePrimFDSet(fd_set *, int *, SCI_PTYPE);
static int populateScndFDSet(fd_set *, int *, SCI_PTYPE);
static int getPrimConnInfoFrmList(int, char *, SCI_PTYPE, CONN_PTYPE *);
static int getScndConnInfoFrmList(int, char *, SCI_PTYPE);
static int connectToServer(char *, int , int *);

static void * listenerOnPrimPort(void *);
static void * listenerOnScndPort(void *);
static void * dataSender(void *);

#if 0
/* Added for the calculating the timings of the request and response*/
extern FILE*     fptr;
#endif 

/*
 * ============================================================================
 * Function Name: initSCIIface
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int initSCIIface(SCIINIT_PTYPE pstSCIParam)
{
	int			rv				= SUCCESS;
	int			iFD				= 0;
	SCI_PTYPE	pstSCIData		= NULL;
#ifdef DEBUG
	char		szDbgMsg[128]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Check for SCI params */
		if(pstSCIParam == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: NULL params passed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		else if(pstSCIParam->dataFunc == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Data call back function can't be NULL"
																, __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}
		else if(strlen(pstSCIParam->szSndQ) <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: Queue ID missing", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(pstSCIParam->iMaxConn <= 0)
		{
			pstSCIParam->iMaxConn = DFLT_MAX_CONN;
		}

		pstSCIData = (SCI_PTYPE) malloc(sizeof(SCI_STYPE));
		if(pstSCIData == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		memset(pstSCIData, 0x00, sizeof(SCI_STYPE));
		pthread_mutex_init(&(pstSCIData->listLock), NULL);
		memcpy(&(pstSCIData->stSCIParam), pstSCIParam, sizeof(SCIINIT_STYPE));

		/* Start the SCI primary server */
		rv = startServer(pstSCIParam->szDevIP, pstSCIParam->iListenPrimPort, &iFD);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to start SCI server",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		pstSCIData->iListenPrimFd = iFD;

		memset(threads, 0x00, sizeof(pthread_t) * 3);

		/* Run the listener thread */
		bRunListener = PAAS_TRUE;
		rv = pthread_create(&threads[0], NULL, listenerOnPrimPort, pstSCIData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to start listener thread",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Run the sender thread */
		bRunSender = PAAS_TRUE;
		rv = pthread_create(&threads[1], NULL, dataSender, pstSCIData);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to start sender thread",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		if(isSecondaryPortEnabled())
		{
			/* Start the SCI secondary server */
			rv = startServer(pstSCIParam->szDevIP, pstSCIParam->iListenScndPort, &iFD);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to start SCI server",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
			pstSCIData->iListenScndFd = iFD;

			rv = pthread_create(&threads[2], NULL, listenerOnScndPort, pstSCIData);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to start sender thread",
						__FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}

		break;
	}

	if(rv != SUCCESS)
	{
		if(pstSCIData != NULL)
		{
			free(pstSCIData);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: closeSCIIface
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int closeSCIIface()
{
	int		iCnt			= 0;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	bRunListener = PAAS_FALSE;
	bRunSender = PAAS_FALSE;

	for(iCnt = 0; iCnt < 2; iCnt++)
	{
		if(threads[iCnt] != 0)
		{
			pthread_join(threads[iCnt], NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return SUCCESS;
}

/*
 * ============================================================================
 * Function Name: sendDatatoSCIServer
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
int sendDatatoSCIServer(uchar *buf, int size, char *pszIP, int port)
{
	int		iFd				= 0;
	int		rv				= SUCCESS;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		rv = connectToServer(pszIP, port, &iFd);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while creating client!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		rv = writeBytes(iFd, size, buf);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to write Consumer Option data to POS",__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		//closing the client fd
		close(iFd);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: connectToServer
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int connectToServer(char *pszIP, int port, int *fd)
{
	int		rv				= SUCCESS;
	int		serverFd		= 0;
	int 	numDesc;
	ADDR_IN	stSelfAddr;
	fd_set	 wset;
	TIME_VAL timeOut;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Create the socket for listening to POS connections */
		/* Daivik : 16/5/2016 - making socket non blocking since we were hanging here if the IP address being provided was incorrect.
		 *
		 */
		serverFd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
		if(serverFd == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Socket creation FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the address and port details for the server */
		memset(&stSelfAddr, 0x00, sizeof(ADDR_IN));
		stSelfAddr.sin_family = AF_INET;
		stSelfAddr.sin_port = htons(port);
		stSelfAddr.sin_addr.s_addr = inet_addr(pszIP);

		rv = connect(serverFd, (ADDR *) &stSelfAddr, sizeof(stSelfAddr));
		debug_sprintf(szDbgMsg, "%s: Connect to server FAILED with an error No: [%d] s:[%s]", __FUNCTION__, errno, strerror(errno));
		APP_TRACE(szDbgMsg);
		/* It is possible that Connect fails because we have now made it non blocking. So we need to wait for sometime and check if the
		 * socket is available to write to.
		 */
		if((rv != SUCCESS) && (errno == 115)) // Only if Error indicates EINPROGRESS
		{
			FD_ZERO(&wset);
			FD_SET(serverFd, &wset);
			memset(&timeOut, 0x00, sizeof(timeOut));
			timeOut.tv_sec =  0;
			timeOut.tv_usec = 300 * 1000; /* Timeout after 300ms */

			if ( (numDesc = select(serverFd+1, NULL, &wset, NULL, &timeOut)) == 0)
			{
				errno = ETIMEDOUT; 	/* timeout */
				rv = FAILURE;
				debug_sprintf(szDbgMsg, "%s: Select Timedout", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			if (FD_ISSET(serverFd, &wset))
			{
				debug_sprintf(szDbgMsg, "%s: Able to connect to socket", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				rv = SUCCESS;

			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Unable to connect to socket", __FUNCTION__);
				APP_TRACE(szDbgMsg);;
			}

			/* Daivik:27/1/2016 - Coverity : 67294 Close the File Descriptor associated with the socket that was created. */
		}

		debug_sprintf(szDbgMsg, "%s: Connecting to [%d]", __FUNCTION__,serverFd);
		APP_TRACE(szDbgMsg);

		*fd = serverFd;

		break;
	}
	if(rv != SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Connect to server FAILED with an error No: [%d] s:[%s]", __FUNCTION__, errno, strerror(errno));
		APP_TRACE(szDbgMsg);
		if(serverFd != -1)
		{
			close(serverFd);
		}
		rv = FAILURE;
	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: startServer
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int startServer(char * pszIP, int port, int * fd)
{
	int		rv				= SUCCESS;
	int		iOn				= 1;
	int		listenFd		= 0;
	ADDR_IN	stSelfAddr;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Create the socket for listening to POS connections */
		listenFd = socket(AF_INET, SOCK_STREAM, 0);
		if(listenFd == -1)
		{
			debug_sprintf(szDbgMsg, "%s: Socket creation FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* ---------- Set the socket options ----------- */

		/* 1. Socket Option: Set socket as reusable */
		rv = setsockopt(listenFd, SOL_SOCKET, SO_REUSEADDR, (char *)&iOn,
																sizeof(iOn));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to make socket reusable",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* 2. Socket Option: Set socket as non blocking */
		rv = ioctl(listenFd, FIONBIO, &iOn);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to make socket non-blocking",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Set the address and port details for the server */
		memset(&stSelfAddr, 0x00, sizeof(ADDR_IN));
		stSelfAddr.sin_family = AF_INET;
		stSelfAddr.sin_port = htons(port);
		if(strlen(pszIP) > 0)
		{
			stSelfAddr.sin_addr.s_addr = inet_addr(pszIP);
		}
		else
		{
			stSelfAddr.sin_addr.s_addr = htonl(INADDR_ANY);
		}

		/* Bind the socket to the details just set in the step above */
		rv = bind(listenFd, (ADDR *) &stSelfAddr, sizeof(stSelfAddr));
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Server bind FAILED", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Start listening on the socket */
		rv = listen(listenFd, 10);
		if(rv != SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: FAILED to listen", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Listening on [%d]", __FUNCTION__,listenFd);
		APP_TRACE(szDbgMsg);

		*fd = listenFd;

		break;
	}

	//if((rv != SUCCESS) && (listenFd > 0))
	if((rv != SUCCESS) && (listenFd >= 0))	// CID-67458: 22-Jan-16: MukeshS3: when listenFd is returned as 0, the resource allocated may be leaked in this case.
	{										// ZERO(0) is a valid file descriptor.
		/* Close the socket in case the socket was created but some other step
		 * failed. */
		close(listenFd);
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: listenerOnPrimPort
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void* listenerOnPrimPort(void * arg)
{
	int			iFd				= 0;
	int			iMaxFd			= 0;
	int			iFdReady		= 0;
	SCI_PTYPE	pstSCIData		= NULL;
	fd_set		inputSet;
	TIME_VAL	timeOut;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstSCIData = (SCI_PTYPE) arg;

	while(bRunListener == PAAS_TRUE)
	{
		/* Set the timeout for the select */
		memset(&timeOut, 0x00, sizeof(timeOut));
		timeOut.tv_sec = 5 * 60; /* 5 minutes timeout for select */
		timeOut.tv_usec = 0;

		populatePrimFDSet(&inputSet, &iMaxFd, pstSCIData);

		iFdReady = select(iMaxFd + 1, &inputSet, NULL, NULL, &timeOut);
		if(iFdReady < 0)
		{
			if(errno == EINTR)
			{
				debug_sprintf(szDbgMsg, "%s: SELECT interrupted", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* Wait for some time before retrying */
				svcWait(10); //Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
				continue;
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Some Error in SELECT",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else if(iFdReady == 0)
		{
			debug_sprintf(szDbgMsg, "%s: SELECT timeout", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			continue;
		}

		/* Check for all the sockets in the list */
		for(iFd = 0; (iFd <= iMaxFd) && (iFdReady > 0); iFd++)
		{
			if(FD_ISSET(iFd, &inputSet))
			{
				iFdReady = -1;

				if(iFd == pstSCIData->iListenPrimFd)
				{
					/* New connection available */
					getNewPrimConns(pstSCIData);
				}
				else
				{
					/* Data available on a connected socket for reading */
					checkForPrimConnData(iFd, pstSCIData);
				}
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: listenerOnScndPort
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void* listenerOnScndPort(void * arg)
{
	int			iFd				= 0;
	int			iMaxFd			= 0;
	int			iFdReady		= 0;
	SCI_PTYPE	pstSCIData		= NULL;
	fd_set		inputSet;
	TIME_VAL	timeOut;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstSCIData = (SCI_PTYPE) arg;

	while(bRunListener == PAAS_TRUE)
	{
		/* Set the timeout for the select */
		memset(&timeOut, 0x00, sizeof(timeOut));
		timeOut.tv_sec = 5 * 60; /* 5 minutes timeout for select */
		timeOut.tv_usec = 0;

		populateScndFDSet(&inputSet, &iMaxFd, pstSCIData);

		iFdReady = select(iMaxFd + 1, &inputSet, NULL, NULL, &timeOut);
		if(iFdReady < 0)
		{
			if(errno == EINTR)
			{
				debug_sprintf(szDbgMsg, "%s: SELECT interrupted", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* Wait for some time before retrying */
				svcWait(10);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
				continue;
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s: Some Error in SELECT",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				break;
			}
		}
		else if(iFdReady == 0)
		{
			debug_sprintf(szDbgMsg, "%s: SELECT timeout", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			continue;
		}

		/* Check for all the sockets in the list */
		for(iFd = 0; (iFd <= iMaxFd) && (iFdReady > 0); iFd++)
		{
			if(FD_ISSET(iFd, &inputSet))
			{
				iFdReady = -1;

				if(iFd == pstSCIData->iListenScndFd)
				{
					/* New connection available */
					getNewScndConns(pstSCIData);
				}
				else
				{
					/* Data available on a connected socket for reading */
					checkForScndConnData(iFd, pstSCIData);
				}
			}
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: checkForPrimConnData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int checkForPrimConnData(int iFd, SCI_PTYPE pstSCIData)
{
	int				rv					= SUCCESS;
	int				iRetryCnt			= 0;
	int				jRetryCnt			= 0;
	int				bytesRead			= 0;
	int				iTotBytes			= 0;
	int				maxLen				= 0;
	int				iAllocBuff			= 0; // For realloc this is used to initialize the newly allocated memory only.
	char			szConnInfo[25]		= "";
	uchar			szBuf[6000]	= ""; //SZ buf is used to Recv Data from socket, Once this is done it is copied into the dynamic bufPtr which is infinite.
	uchar *			bufPtr				= NULL; // This Pointer holds the starting address of the SCI Data
	uchar *			tmpPtr				= NULL; // This Pointer is used to Traverse the the original pointer and copy data at required point
	SCIINIT_PTYPE	pstSCIParam			= NULL;
	CONN_PTYPE		pstConnInfoHandle	= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstSCIParam = &(pstSCIData->stSCIParam);

	memset(szBuf,0x00,sizeof(szBuf));
	maxLen = sizeof(szBuf)-1;

	getPrimConnInfoFrmList(iFd, szConnInfo, pstSCIData, &pstConnInfoHandle);

	while(iRetryCnt <= MAX_RETRIES)
	{
		bytesRead = recv(iFd, szBuf, maxLen, 0);
		if(bytesRead < 0)
		{
			/* Recv got interrupted by a signal perhaps, since it is not a
			 * fatal error, reading on the socket can be tried again a couple
			 * of times */
			if((errno == EINTR) || (errno == EAGAIN) || (errno == EWOULDBLOCK))
			{
				debug_sprintf(szDbgMsg, "%s: read interrupted", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				svcWait(20 + (iRetryCnt * 20));
				iRetryCnt++;
			}
			else
			{
				/* Recv failed because of some fatal error */
				debug_sprintf(szDbgMsg, "%s: recv FAILED - error = [%d]",
														__FUNCTION__, errno);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		else if(bytesRead > 0)
		{
			//Got some data from new Handle Setting Data received to False
			if(pstConnInfoHandle != NULL)
			{
				pstConnInfoHandle->bDataSent = PAAS_FALSE;
				debug_sprintf(szDbgMsg, "%s: Data sent, Setting bdataSent to FALSE in ConnectionInfo Node", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			/* DAIVIK 22/11/2016 - Since we now allow infinite length of Command to be recieved from the POS , we will copy the buffer recieved
			 * into a dynamic buffer and expect more data to be read.Once we read all the data available , we will try to get the data ( 6 times ) and if
			 * we do not recieve any data then we assume it is the end of POS Transmission and we then start processing the recieved Command.
			 */

			bufPtr = (uchar *)malloc(sizeof(uchar)*(bytesRead+1));
			iAllocBuff = bytesRead+1;
			memset(bufPtr,0x00,sizeof(uchar)*(bytesRead+1));
			memcpy(bufPtr,szBuf,bytesRead);

			iTotBytes += bytesRead;
			tmpPtr = bufPtr + bytesRead;

			jRetryCnt = 0;
			while(jRetryCnt < MAX_RETRIES)
			{
				memset(szBuf,0x00,sizeof(szBuf));
				bytesRead = recv(iFd, szBuf, maxLen, 0);
				if(bytesRead > 0)
				{
					jRetryCnt = 0;
					if( iAllocBuff <= (iTotBytes + bytesRead))
					{
						/* Daivik 22/11/2016 - Reallocte enough buffer to fit the original number of bytes plus the newly read buffer
						 * Initialize only the newly allocated buffer.
						 */
						bufPtr = (uchar *)realloc(bufPtr,sizeof(uchar) * (iTotBytes+bytesRead+1));
						memset(bufPtr + iAllocBuff,0x00,sizeof(uchar));
						tmpPtr = bufPtr+iTotBytes; // Move the tmpPtr to the point where we need to start copying the new buffer.
						iAllocBuff = iTotBytes+ bytesRead + 1; //Incrementing the currently allocated buffer to check at the point of next read.

					}
					memcpy(tmpPtr,szBuf,bytesRead);
					iTotBytes += bytesRead;
					tmpPtr += bytesRead;
				}
				else
				{
					jRetryCnt++;
				}
				svcWait(10);
			}

			if(iTotBytes > 0)
			{
				/* Call the data callback function */
				if(pstSCIParam->dataFunc != NULL)
				{
					pstSCIParam->dataFunc(bufPtr, iTotBytes, szConnInfo, PRIMARY);
				}
			}

			break;
		}
		else
		{
			/* ---- Connection was closed by POS ---- */

			/* Call the disconnection callback function */
			if(pstSCIParam->disconFunc != NULL)
			{
				pstSCIParam->disconFunc(szConnInfo, (void *)pstSCIData);
			}

			/* Remove the connection info */
			remConnFrmList(iFd, pstSCIData);

			break;
		}
	}

	if(iRetryCnt > MAX_RETRIES)
	{
		debug_sprintf(szDbgMsg, "%s: Max retries done", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Call the disconnection callback function */
		if(pstSCIParam->disconFunc != NULL)
		{
			pstSCIParam->disconFunc(szConnInfo, (void *)pstSCIData);
		}

		/* Close the connection and remove the connection info */
		remConnFrmList(iFd, pstSCIData);
	}
	free(bufPtr);
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: checkForScndConnData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int checkForScndConnData(int iFd, SCI_PTYPE pstSCIData)
{
	int				rv				= SUCCESS;
	int				iRetryCnt		= 0;
	int				jRetryCnt		= 0;
	int				bytesRead		= 0;
	int				iTotBytes		= 0;
	int				maxLen			= 0;
	char			szConnInfo[25]	= "";
	uchar			szBuf[6000]		= ""; //Praveen_P1: Increased to 6000 to support standalone sig in encryption mode
	uchar *			bufPtr			= NULL;
	SCIINIT_PTYPE	pstSCIParam		= NULL;
#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstSCIParam = &(pstSCIData->stSCIParam);
	maxLen = sizeof(szBuf) - 1;
	bufPtr = szBuf;

	getScndConnInfoFrmList(iFd, szConnInfo, pstSCIData);

	while(iRetryCnt <= MAX_RETRIES)
	{
		bytesRead = recv(iFd, bufPtr, maxLen, 0);
		if(bytesRead < 0)
		{
			/* Recv got interrupted by a signal perhaps, since it is not a
			 * fatal error, reading on the socket can be tried again a couple
			 * of times */
			if((errno == EINTR) || (errno == EAGAIN) || (errno == EWOULDBLOCK))
			{
				debug_sprintf(szDbgMsg, "%s: read interrupted", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				svcWait(20 + (iRetryCnt * 20));
				iRetryCnt++;
			}
			else
			{
				/* Recv failed because of some fatal error */
				debug_sprintf(szDbgMsg, "%s: recv FAILED - error = [%d]",
														__FUNCTION__, errno);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		else if(bytesRead > 0)
		{
			jRetryCnt = 0;
			while(jRetryCnt < MAX_RETRIES)
			{
				if(bytesRead > 0)
				{
					if(iTotBytes + bytesRead <= maxLen)
					{
						bufPtr += bytesRead;
						iTotBytes += bytesRead;
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Exceeding Maximum bytes read!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						break;
					}
				}

				bytesRead = recv(iFd, bufPtr, maxLen - iTotBytes, 0);
				jRetryCnt++;
				svcWait(10);
			}

			if(iTotBytes > 0)
			{
				/* Call the data callback function */
				if(pstSCIParam->dataFunc != NULL)
				{
					pstSCIParam->dataFunc(szBuf, iTotBytes, szConnInfo, SECONDARY);
				}
			}

			break;
		}
		else
		{
			/* ---- Connection was closed by POS ---- */

			/* Call the disconnection callback function */
			if(pstSCIParam->disconFunc != NULL)
			{
				pstSCIParam->disconFunc(szConnInfo, (void *)pstSCIData);
			}

			/* Remove the connection info */
			remConnFrmList(iFd, pstSCIData);

			break;
		}
	}

	if(iRetryCnt > MAX_RETRIES)
	{
		debug_sprintf(szDbgMsg, "%s: Max retries done", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/* Call the disconnection callback function */
		if(pstSCIParam->disconFunc != NULL)
		{
			pstSCIParam->disconFunc(szConnInfo, (void *)pstSCIData);
		}

		/* Close the connection and remove the connection info */
		remConnFrmList(iFd, pstSCIData);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
/*
 * ============================================================================
 * Function Name: getNewPrimConns
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getNewPrimConns(SCI_PTYPE pstSCIData)
{
	int		rv				= SUCCESS;
	int		iOn				= 1;
	int		newFd			= 0;
	int		iSize			= 0;
	int		listenFd		= 0;
	char	szIPPrt[25]		= "";
	ADDR_IN	stAddr;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	do
	{
		listenFd = pstSCIData->iListenPrimFd;
		iSize = sizeof(stAddr);
		memset(&stAddr, 0x00, iSize);

		/* Accept the new connection */
		newFd = accept(listenFd, (ADDR *)&stAddr, (socklen_t *) &iSize);
		if(newFd < 0)
		{
			if((errno != EAGAIN) && (errno !=  EWOULDBLOCK))
			{
				debug_sprintf(szDbgMsg, "%s: accept FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		//else if(newFd > 0)	// CID-67403: 1-Feb-16: MukeshS3: As accept call returns a non-negative ineteger for success case.
		else if(newFd >= 0)		// So, considering 0 also as a valid fd value.
		{
			/* ------- Got the new connection -------- */
			/* Get the connection information string for the new connection. The
			 * connection information string consists of IP and the port number 
			 * of the POS client in the format specified below
			 * xxx.xxx.xxx.xxx:yyyy */
			sprintf(szIPPrt, "%s:%d", inet_ntoa(stAddr.sin_addr),
														htons(stAddr.sin_port));

			debug_sprintf(szDbgMsg, "%s: new conn from [%s] on %d",__FUNCTION__,
															szIPPrt, newFd);
			APP_TRACE(szDbgMsg);

			/* Set the new socket as non-blocking */
			ioctl(newFd, FIONBIO, &iOn);

			/* Add to the existing list of alive connections */
			rv = addConnToPrimList(newFd, szIPPrt, pstSCIData);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to add new conn to list",
										__FUNCTION__);
				APP_TRACE(szDbgMsg);

				continue;
			}

			/* Call the connect callback function is provided */
			if(pstSCIData->stSCIParam.connFunc)
			{
				pstSCIData->stSCIParam.connFunc(szIPPrt);
			}
		}
	}
	//while(newFd > 0);	// CID-67403: 1-Feb-16: MukeshS3: As accept call returns a non-negative ineteger for success case.
	while(newFd >= 0);	// So, considering 0 also as a valid fd value. http://man7.org/linux/man-pages/man2/accept.2.html

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getNewScndConns
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getNewScndConns(SCI_PTYPE pstSCIData)
{
	int		rv				= SUCCESS;
	int		iOn				= 1;
	int		newFd			= 0;
	int		iSize			= 0;
	int		listenFd		= 0;
	char	szIPPrt[25]		= "";
	ADDR_IN	stAddr;
#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	do
	{
		listenFd = pstSCIData->iListenScndFd;
		iSize = sizeof(stAddr);
		memset(&stAddr, 0x00, iSize);

		/* Accept the new connection */
		newFd = accept(listenFd, (ADDR *)&stAddr, (socklen_t *) &iSize);
		if(newFd < 0)
		{
			if((errno != EAGAIN) && (errno !=  EWOULDBLOCK))
			{
				debug_sprintf(szDbgMsg, "%s: accept FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = FAILURE;
				break;
			}
		}
		//else if(newFd > 0)	// CID-67403: 1-Feb-16: MukeshS3: As accept call returns a non-negative ineteger for success case.
		else if(newFd >= 0)		// So, considering 0 also as a valid fd value.
		{
			/* ------- Got the new connection -------- */
			/* Get the connection information string for the new connection. The
			 * connection information string consists of IP and the port number
			 * of the POS client in the format specified below
			 * xxx.xxx.xxx.xxx:yyyy */
			sprintf(szIPPrt, "%s:%d", inet_ntoa(stAddr.sin_addr),
														htons(stAddr.sin_port));

			debug_sprintf(szDbgMsg, "%s: new conn from [%s] on %d",__FUNCTION__,
															szIPPrt, newFd);
			APP_TRACE(szDbgMsg);

			/* Set the new socket as non-blocking */
			ioctl(newFd, FIONBIO, &iOn);

			/* Add to the existing list of alive connections */
			rv = addConnToScndList(newFd, szIPPrt, pstSCIData);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to add new conn to list",
										__FUNCTION__);
				APP_TRACE(szDbgMsg);

				continue;
			}

			/* Call the connect callback function is provided */
			if(pstSCIData->stSCIParam.connFunc)
			{
				pstSCIData->stSCIParam.connFunc(szIPPrt);
			}
		}
	}
	//while(newFd > 0);	// CID-67403: 1-Feb-16: MukeshS3: As accept call returns a non-negative ineteger for success case.
	while(newFd >= 0);	// So, considering 0 also as a valid fd value.

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: dataSender
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void * dataSender(void * arg)
{
	int				rv					= SUCCESS;
	int				fd					= 0;
	int				iSize				= 0;
	int				iAppLogEnabled		= isAppLogEnabled();
	char			szEntryType[25]		= "";
	char			szAppLogData[300]	= "";
	char			szAppLogDiag[300]	= "";
	SCI_PTYPE		pstSCIData			= NULL;
	MSG_PTYPE		pstMsg				= NULL;
	SCIINIT_PTYPE	pstParam			= NULL;
	CONN_PTYPE		pstConnInfoHandle	= NULL;

#ifdef DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pstSCIData = (SCI_PTYPE) arg;
	pstParam = &(pstSCIData->stSCIParam);
	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));
	memset(szEntryType, 0x00, sizeof(szEntryType));

	while(bRunSender == PAAS_TRUE)
	{
		/* Try to get the POS response data from the queue */
		pstMsg = (MSG_PTYPE) getDataFrmQ(pstParam->szSndQ, &iSize);
		if(pstMsg == NULL)
		{
			svcWait(15);//Praveen_P1: Reducing svcWait to minimum value to increase the turn around time
			continue;
		}

		/* --- Got the POS response data. --- */

		/* Search for the file descriptor using the conn information, in
		 * the connection list */
		rv = getFDFrmConnList(pstMsg->szConnInfo, &fd, pstSCIData, &pstConnInfoHandle);
		if(rv == SUCCESS)
		{
			if(pstConnInfoHandle != NULL)
			{
				/*Data being sent to POS, so setting this flag to true, so that we can ignore the disconnection
				 * if we got disconnection after sending data. But we should not ignore the disconnection if POS got disconnected
				 * during writing data to POS write bytes will fail and this flag won't  be set.
				 */
				pstConnInfoHandle->bDataSent = PAAS_TRUE;
				debug_sprintf(szDbgMsg, "%s: Sending Data, Setting bdataSent to TRUE, We will Set it to FALSE again in case writeBytes Fail", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			rv = writeBytes(fd, pstMsg->iLen, pstMsg->szMsg);
			if(rv != SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: FAILED to write data to POS", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(pstConnInfoHandle != NULL)
				{
					pstConnInfoHandle->bDataSent = PAAS_FALSE;
					debug_sprintf(szDbgMsg, "%s: Error While Sending Data sent, Setting bdataSent to False Now", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

				/* Call the callback function for disconnection */
				if(pstParam->disconFunc)
				{
					pstParam->disconFunc(pstMsg->szConnInfo, (void *)pstSCIData);
				}

				/* Remove the connection from the list */
				remConnFrmList(fd, pstSCIData);

				if(iAppLogEnabled == 1)
				{
					strcpy(szEntryType, PAAS_FAILURE);
					strcpy(szAppLogData, "Fail to Send the Response back to POS");
					addAppEventLog(SCA, szEntryType, SENT, szAppLogData, NULL);
				}
			}
			else
			{
				if(iAppLogEnabled == 1)
				{
					strcpy(szEntryType, PAAS_SUCCESS);
					strcpy(szAppLogData, "Response Sent Back to POS");
					addAppEventLog(SCA, szEntryType, SENT, szAppLogData, NULL);
				}
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No connection present for [%s]", __FUNCTION__, pstMsg->szConnInfo);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled == 1)
			{
				strcpy(szEntryType, PAAS_FAILURE);
				strcpy(szAppLogData, "Connection Lost with POS, Cannot Send the Response Back to POS.");
				strcpy(szAppLogDiag, "Please Check the Connection Between POS and Device");
				addAppEventLog(SCA, szEntryType, SENT, szAppLogData, szAppLogDiag);
			}
		}
		/* DAIVIK: 26/11/2015 :Allocate the pointer which we had recieved back to NULL. This is to ensure we dont point to a memory location which could be freed and allocated for something else.
		 * This was required to fix a crash that was observed when sending multiple Secondary Port Cancel requests within short interval.
		 */
		pstConnInfoHandle = NULL;
		/* De-allocate memories */
		if(pstMsg->szMsg != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Freeing Message", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			free(pstMsg->szMsg);
		}
		if(pstMsg != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Freeing Message struct", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			free(pstMsg);
		}
		pstMsg = NULL;

		if(iAppLogEnabled == 1)
		{
			addAppEventLog(SCA, PAAS_INFO, COMMAND_END, END_INDICATOR, NULL);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

/*
 * ============================================================================
 * Function Name: addConnToPrimList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addConnToPrimList(int fd, char * szConnInfo, SCI_PTYPE pstSCIData)
{
	int			rv				= SUCCESS;
	int			iCurCnt			= 0;
	int			iMaxCnt			= 0;
	CONN_PTYPE	pstConnInfo		= NULL;
	CONN_PTYPE	pstTmpConn		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Acquire the lock for the connection list */
	acquireMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	while(1)
	{
		iCurCnt = pstSCIData->iCurPrimConnCnt;
		iMaxCnt = pstSCIData->stSCIParam.iMaxConn;

		if((iCurCnt + 1) > iMaxCnt)
		{
			debug_sprintf(szDbgMsg, "%s: Exceeding max lim; not adding conn",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Allocate memory for a new connection node */
		pstTmpConn = (CONN_PTYPE) malloc(sizeof(CONN_STYPE));
		if(pstTmpConn == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Populate the connection node */
		memset(pstTmpConn, 0x00, sizeof(CONN_STYPE));
		pstTmpConn->fd = fd;
		strcpy(pstTmpConn->szConnInfo, szConnInfo);
		pstTmpConn->bDataSent = PAAS_FALSE;

		/* Add the connection node to the linked list of existing connection's
		 * nodes */
		pstConnInfo = pstSCIData->pstPrimPortConnList;
		if(pstConnInfo == NULL)
		{
			pstSCIData->pstPrimPortConnList = pstTmpConn;
		}
		else
		{
			while(pstConnInfo->next != NULL)
			{
				pstConnInfo = pstConnInfo->next;
			}
			pstConnInfo->next = pstTmpConn;
		}

		pstSCIData->iCurPrimConnCnt = iCurCnt + 1;

		break;
	}

	/* Release the lock for the connection list */
	releaseMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	if(rv != SUCCESS)
	{
		/* Close the socket */
		close(fd);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addConnToScndList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int addConnToScndList(int fd, char * szConnInfo, SCI_PTYPE pstSCIData)
{
	int			rv				= SUCCESS;
	int			iCurCnt			= 0;
	int			iMaxCnt			= 0;
	CONN_PTYPE	pstConnInfo		= NULL;
	CONN_PTYPE	pstTmpConn		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Acquire the lock for the connection list */
	acquireMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	while(1)
	{
		iCurCnt = pstSCIData->iCurScndConnCnt;
		iMaxCnt = pstSCIData->stSCIParam.iMaxConn;

		if((iCurCnt + 1) > iMaxCnt)
		{
			debug_sprintf(szDbgMsg, "%s: Exceeding max lim; not adding conn",
																__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Allocate memory for a new connection node */
		pstTmpConn = (CONN_PTYPE) malloc(sizeof(CONN_STYPE));
		if(pstTmpConn == NULL)
		{
			debug_sprintf(szDbgMsg,"%s: Memory allocation FAILED",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = FAILURE;
			break;
		}

		/* Populate the connection node */
		memset(pstTmpConn, 0x00, sizeof(CONN_STYPE));
		pstTmpConn->fd = fd;
		strcpy(pstTmpConn->szConnInfo, szConnInfo);

		/* Add the connection node to the linked list of existing connection's
		 * nodes */
		pstConnInfo = pstSCIData->pstScndPortConnList;
		if(pstConnInfo == NULL)
		{
			pstSCIData->pstScndPortConnList = pstTmpConn;
		}
		else
		{
			while(pstConnInfo->next != NULL)
			{
				pstConnInfo = pstConnInfo->next;
			}
			pstConnInfo->next = pstTmpConn;
		}

		pstSCIData->iCurScndConnCnt = iCurCnt + 1;

		break;
	}

	/* Release the lock for the connection list */
	releaseMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	if(rv != SUCCESS)
	{
		/* Close the socket */
		close(fd);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: remConnFrmList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int remConnFrmList(int fd, SCI_PTYPE pstSCIData)
{
	int			rv				= SUCCESS;
	CONN_PTYPE	pstConnInfo		= NULL;
	CONN_PTYPE	pstTmpNode		= NULL;
	PAAS_BOOL	biSConnPresent	= PAAS_FALSE;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Acquire the lock for the connection list */
	acquireMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	while(1)
	{
		pstConnInfo = pstSCIData->pstPrimPortConnList;
		while(pstConnInfo != NULL)
		{
			if(pstConnInfo->fd == fd)
			{
				if(pstTmpNode == NULL)
				{
					/* Removing the first node */
					pstSCIData->pstPrimPortConnList = pstConnInfo->next;
				}
				else
				{
					/* Removing the nth node */
					pstTmpNode->next = pstConnInfo->next;
				}

				close(pstConnInfo->fd);
				free(pstConnInfo);

				pstSCIData->iCurPrimConnCnt -= 1;
				biSConnPresent = PAAS_TRUE;
				break;
			}

			pstTmpNode = pstConnInfo;
			pstConnInfo = pstConnInfo->next;
		}

		if(pstConnInfo == NULL && biSConnPresent == PAAS_FALSE)
		{
			pstTmpNode		= NULL;
			pstConnInfo 	= pstSCIData->pstScndPortConnList;
			while(pstConnInfo != NULL)
			{
				if(pstConnInfo->fd == fd)
				{
					if(pstTmpNode == NULL)
					{
						/* Removing the first node */
						pstSCIData->pstScndPortConnList = pstConnInfo->next;
					}
					else
					{
						/* Removing the nth node */
						pstTmpNode->next = pstConnInfo->next;
					}

					close(pstConnInfo->fd);
					free(pstConnInfo);

					pstSCIData->iCurScndConnCnt -= 1;
					biSConnPresent = PAAS_TRUE;
					break;
				}

				pstTmpNode = pstConnInfo;
				pstConnInfo = pstConnInfo->next;
			}
		}
		break;
	}

	if(biSConnPresent == PAAS_FALSE)
	{
		debug_sprintf(szDbgMsg, "%s: Couldnt find the conn", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	/* Release the lock for the connection list */
	releaseMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: populatePrimFDSet
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int populatePrimFDSet(fd_set * pstFDSet, int * piMaxFd, SCI_PTYPE pstSCIData)
{
	int			rv				= SUCCESS;
	int			iMaxFd			= 0;
	CONN_PTYPE	pstConnInfo		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Clear the fd set initially */
	FD_ZERO(pstFDSet);

	/* Assign the listening fd first */
	iMaxFd = pstSCIData->iListenPrimFd;
	FD_SET(iMaxFd, pstFDSet);

	/* Acquire the lock for the connection list */
	acquireMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	/* Traverse through the existing connection list and get all the file
	 * descriptors present in the list, for the FD_SET */
	pstConnInfo = pstSCIData->pstPrimPortConnList;
	while(pstConnInfo != NULL)
	{
		if(iMaxFd < pstConnInfo->fd)
		{
			iMaxFd = pstConnInfo->fd;
		}

		FD_SET(pstConnInfo->fd, pstFDSet);

		pstConnInfo = pstConnInfo->next;
	}

	/* Release the lock for the connection list */
	releaseMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	*piMaxFd = iMaxFd;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: populateFDSet
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int populateScndFDSet(fd_set * pstFDSet, int * piMaxFd, SCI_PTYPE pstSCIData)
{
	int			rv				= SUCCESS;
	int			iMaxFd			= 0;
	CONN_PTYPE	pstConnInfo		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Clear the fd set initially */
	FD_ZERO(pstFDSet);

	/* Assign the listening fd first */
	iMaxFd = pstSCIData->iListenScndFd;
	FD_SET(iMaxFd, pstFDSet);

	/* Acquire the lock for the connection list */
	acquireMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	/* Traverse through the existing connection list and get all the file
	 * descriptors present in the list, for the FD_SET */
	pstConnInfo = pstSCIData->pstScndPortConnList;
	while(pstConnInfo != NULL)
	{
		if(iMaxFd < pstConnInfo->fd)
		{
			iMaxFd = pstConnInfo->fd;
		}

		FD_SET(pstConnInfo->fd, pstFDSet);

		pstConnInfo = pstConnInfo->next;
	}

	/* Release the lock for the connection list */
	releaseMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	*piMaxFd = iMaxFd;

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getFDFrmConnList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
extern int getFDFrmConnList(char * szConnInfo, int * pFd, SCI_PTYPE pstSCIData, CONN_PTYPE *pstConnInfoHandle)
{
	int			rv				= SUCCESS;
	CONN_PTYPE	pstConnInfo		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Acquire the lock for the connection list */
	acquireMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	while(1)
	{
		pstConnInfo = pstSCIData->pstPrimPortConnList;
		while(pstConnInfo != NULL)
		{
			if(strcmp(szConnInfo, pstConnInfo->szConnInfo) == SUCCESS)
			{
				*pFd = pstConnInfo->fd;
				//Added pstConnInfoHandle so that we can set the data sent as TRUE after writing
				*pstConnInfoHandle = pstConnInfo;
				break;
			}

			pstConnInfo = pstConnInfo->next;
		}

		if(pstConnInfo == NULL)
		{
			pstConnInfo = pstSCIData->pstScndPortConnList;
			while(pstConnInfo != NULL)
			{
				if(strcmp(szConnInfo, pstConnInfo->szConnInfo) == SUCCESS)
				{
					*pFd = pstConnInfo->fd;
					break;
				}

				pstConnInfo = pstConnInfo->next;
			}
		}
		break;
	}

	if(pstConnInfo == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to find connection", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	/* Release the lock for the connection list */
	releaseMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getPrimConnInfoFrmList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getPrimConnInfoFrmList(int fd, char * szConnInfo, SCI_PTYPE pstSCIData, CONN_PTYPE *pstConnInfoHandle)
{
	int			rv				= SUCCESS;
	CONN_PTYPE	pstConnInfo		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Acquire the lock for the connection list */
	acquireMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	pstConnInfo = pstSCIData->pstPrimPortConnList;
	while(pstConnInfo != NULL)
	{
		if(pstConnInfo->fd == fd)
		{
			strcpy(szConnInfo, pstConnInfo->szConnInfo);
			*pstConnInfoHandle = pstConnInfo;
			break;
		}
		pstConnInfo = pstConnInfo->next;
	}

	if(pstConnInfo == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to find connection", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	/* Release the lock for the connection list */
	releaseMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: getScndConnInfoFrmList
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int getScndConnInfoFrmList(int fd, char * szConnInfo, SCI_PTYPE pstSCIData)
{
	int			rv				= SUCCESS;
	CONN_PTYPE	pstConnInfo		= NULL;
#ifdef DEBUG
	char		szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* Acquire the lock for the connection list */
	acquireMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	pstConnInfo = pstSCIData->pstScndPortConnList;
	while(pstConnInfo != NULL)
	{
		if(pstConnInfo->fd == fd)
		{
			strcpy(szConnInfo, pstConnInfo->szConnInfo);
			break;
		}

		pstConnInfo = pstConnInfo->next;
	}

	if(pstConnInfo == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to find connection", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		rv = FAILURE;
	}

	/* Release the lock for the connection list */
	releaseMutexLock(&(pstSCIData->listLock), __FUNCTION__);

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: writeBytes
 *
 * Description	: This function is responsible for writing data to the socket.
 *
 * Input Params	: sockFd-> socket to which the data needs to be written
 * 				  size	-> length of the data to be written
 * 				  buf	-> the data to be written
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int writeBytes(int sockFd, int size, uchar *buf)
{
	int		rv				= SUCCESS;
	int		iRetryCnt		= 0;
	int		bytesSent		= 0;
	int		bytesLeft		= 0;
	uchar *	sendPtr			= NULL;
#if 0
	struct tm *		tm_ptr			= NULL;
	struct timeb	the_time;
	char szTSPrefix[30] = "";
#endif

#ifdef DEVDEBUG
	char			szDbgMsg[4096]		= "";
#elif DEBUG
	char			szDbgMsg[256]		= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	sendPtr = buf;
	bytesLeft = size;

#ifdef DEVDEBUG
	if(size < 4000)
	{
		#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s:%s: Buffer to be written = [%s]",DEVDEBUGMSG,__FUNCTION__, buf);
			APP_TRACE(szDbgMsg);
		#endif
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Size of the buffer to be written [%d]", __FUNCTION__, size);
		APP_TRACE(szDbgMsg);
	}
#endif
	while((iRetryCnt <= MAX_RETRIES)  && (bytesLeft > 0))
	{
		bytesSent = send(sockFd, sendPtr, bytesLeft, MSG_NOSIGNAL);
		if(bytesSent < 0)
		{
			/* Some error occured while writing */
			if( (errno == EINTR)||(errno == EAGAIN)||(errno == EWOULDBLOCK) )
			{
				/* try writing again after sleep */
				iRetryCnt++;
				svcWait(750 + iRetryCnt * 250);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: FAILED with error = %d",
														__FUNCTION__, errno);
				APP_TRACE(szDbgMsg);

				/* writing has failed */
				rv = FAILURE;
				break;
			}
		}
		else
		{
			/* Normal Write */
			iRetryCnt = 0;
			bytesLeft -= bytesSent;

			if(bytesLeft == 0)
			{
				rv = SUCCESS;
				break;
			}

			sendPtr += bytesSent;
		}
	}

#if 0
	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
		/* Keep within 3 digits, since sprintf's width.precision specification
		 * does not truncate large values */
		the_time.millitm = 999;
	}

	sprintf(szTSPrefix, "%02d:%02d:%02d:%03d", tm_ptr->tm_hour,
				tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	fprintf (fptr, "Response sent at %s\n", szTSPrefix);

	fclose (fptr);
#endif 

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * End of file sciIface.c
 * ============================================================================
 */
