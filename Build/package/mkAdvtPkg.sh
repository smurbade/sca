# ----------------------------------------------------------------------------
# Author : Vikram Datt Rana
# Build  : 07 Dec 2012
# ----------------------------------------------------------------------------

appPrefix="PointMx_SCA"
STG_DIR=c:\staging
HOME=`pwd`
ADVT_DIR=advert
IMG_DIR=$ADVT_DIR/images
VID_DIR=$ADVT_DIR/videos
MEDIA_DIR=../../ADVT_MEDIA

cleanup()
{
	echo "Removing directories if present"

	if [ -d $IMG_DIR ]; then
		echo "Removing $IMG_DIR"
		rm -f -r $IMG_DIR
	fi

	if [ -d $VID_DIR ]; then
		echo "Removing $VID_DIR"
		rm -f -r $VID_DIR
	fi

	if [ -d $ADVT_DIR ]; then
		echo "Removing $ADVT_DIR"
		rm -f -r $ADVT_DIR
	fi
}

# Do the cleanup (removal of unnecessary folders)
cleanup

# Create the necessary folders before starting with package creation 
mkdir -p $ADVT_DIR $IMG_DIR $VID_DIR

echo "************************************************************************"
echo "Creating Images package"
echo "************************************************************************"
mkdir $IMG_DIR/CONTROL

# Get the images from the media folder
cp $MEDIA_DIR/images/* $IMG_DIR 

# go to the images folder
cd $IMG_DIR
echo "                      "
echo "in directory `pwd` now"

# Modify and add the needed files in the folder for bundling
rm -r -f ./svn
touch imageList1.dat
touch imageList2.dat
echo "7" > imageList1.dat
echo "15" > imageList2.dat
ls *Image* >> imageList1.dat
ls *Image* >> imageList2.dat

cat imageList1.dat
cat imageList2.dat

cd $HOME

# Store the control data for the images package
touch control 
echo "Package: $appPrefix-AdvtImg" > control
echo "Version: 1.0.0" >> control
echo "User: usr1" >> control
echo "Type: userflash" >> control

mv control $IMG_DIR/CONTROL

# create tar of the advertisement images
cd $IMG_DIR
echo "                      "
echo "Bundling the advertisment images"
tar -cvf ../$appPrefix-AdvtImgs.tar ./*

cd $HOME
rm -f -r $IMG_DIR

echo "************************************************************************"
echo "Creating Videos package"
echo "************************************************************************"
mkdir $VID_DIR/CONTROL

# Get the videos from the media folder
cp $MEDIA_DIR/videos/* $VID_DIR

# go to the videos folder
cd $VID_DIR
echo "                      "
echo "in directory `pwd` now"

# Modify and add the needed files in the folder for bundling
rm -r -f ./svn
ls *Video* > videoList.dat

cat videoList.dat

cd $HOME
echo "                      "
echo "in directory `pwd` now"

# Store the control data for the videos package
touch control 
echo "Package: $appPrefix-AdvtVideos" > control
echo "Version: 1.0.0" >> control
echo "User: usr1" >> control
echo "Type: userflash" >> control

mv control $VID_DIR/CONTROL

# create tar of the advertisement videos
cd $VID_DIR
echo "                      "
echo "Bundling the advertisement videos"
tar -cvf ../$appPrefix-AdvtVideos.tar ./*

cd $HOME
rm -f -r $VID_DIR

echo "************************************************************************"
echo "Creating Advertisements bundle from the videos and images"
echo "************************************************************************"
mkdir $ADVT_DIR/CONTROL $ADVT_DIR/crt

touch control
echo "Package: $appPrefix-Advt" > control
echo "Version: 1.0.0" >> control
echo "User: usr1" >> control

mv control $ADVT_DIR/CONTROL
cp appsignerA1.crt $ADVT_DIR/crt

# sign the images tar ball
echo "                      "
echo "Signing the advertisement images tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert appsignerA1.crt --key appsignerA1.key $ADVT_DIR/$appPrefix-AdvtImgs.tar

# sign the videos tar ball
echo "                      "
echo "Signing the advertisement videos tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert appsignerA1.crt --key appsignerA1.key $ADVT_DIR/$appPrefix-AdvtVideos.tar

# Create the directory for the signing certificates

# tar into USR1.pkg ..
cd $ADVT_DIR
echo "                      "
echo "creating USR1 package for advertisement media"
tar -cvzf ../USR1.$appPrefix-AdvtMedia.tgz ./*

cd $HOME

# sign the USR1 package tar ball
echo "                      "
echo "signing USR1 package"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ./appsignerA1.crt --key ./appsignerA1.key USR1.$appPrefix-AdvtMedia.tgz

#create the downloadable file
echo "                      "
echo "creating download package"
tar -cvzf dl-$appPrefix-AdvtMedia.tgz USR1.$appPrefix-AdvtMedia.tgz*

rm -f -r USR1.$appPrefix-AdvtMedia.tgz*

# Do the cleanup (removal of unnecessary folders)
cleanup
