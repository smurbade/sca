appPrefix="PointMx_SCA"
STG_DIR=c:\staging
home=`pwd`

scrUsageDisp()
{
	echo "usage mkpFormsPkg.sh [form folder] [SCA_PACKAGE[1/0]"
	echo "example: mkpFormsPkg.sh 2.19.12 formName folderName 1"	
}

cleanup()
{
	echo "Removing directories if present"

	if [ -d FRM_PKG ]; then
		echo "Removing FRM_PKG"
		rm -f -r FRM_PKG
	fi

	if [ -d FRM_PKG ]; then
		echo "Removing FRM_PKG"
		rm -f -r FRM_PKG
	fi
}

if [ "$1" = "" ]; then
	echo "ERROR: Need to suppy form deck path"
	scrUsageDisp
	return 1
fi

if [ "$3" = "" ]; then
	echo "ERROR: Need to supply if SCA FLASH FILES PACKAGE is required or not"
	scrUsageDisp
	return 1
fi

APP_EXEC_DIR=$2
FRM_DIR=$1

if ! [ -d "$FRM_DIR" ]; then
	echo "ERROR: Invalid Path given: $FRM_DIR.. Exiting..."
	return 1
fi

ver=1.0.0

cleanup

echo "***********************************************************************"
echo "Creating the form bundle"
echo "***********************************************************************"
mkdir -p FRM_PKG/CONTROL FRM_PKG/crt

touch control
echo "Package: $appPrefix-Forms" > control
echo "Version: $ver" >> control
echo "User: usr1" >> control
echo "Type: userflash" >> control

mv control FRM_PKG/CONTROL
cp ./appsignerA1.crt FRM_PKG/crt
cp -R $FRM_DIR/themes FRM_PKG

cd FRM_PKG
cp $FRM_DIR/*.tgz .
tar -xf *.tgz
rm -f *.tgz

if [ "$3" = "1" ]; then
	echo "***********************************************************************"
	echo "Adding the SCA FILES to the form bundle"
	echo "***********************************************************************"
	cp -R $APP_EXEC_DIR/SCAFiles/* .
fi

#create the tar of the form deck
tar -cvf ../$appPrefix-Forms.tar ./*

cd $home
rm -f -r FRM_PKG

echo "***********************************************************************"
echo "Creating the download Form Bundle"
echo "***********************************************************************"
mkdir -p FRM_PKG/CONTROL FRM_PKG/crt
touch control
echo "Package: $appPrefix-Forms" > control
echo "Version: $ver" >> control
echo "User: usr1" >> control

mv control FRM_PKG/CONTROL
cp ./appsignerA1.crt FRM_PKG/crt
mv $appPrefix-Forms.tar FRM_PKG

cd FRM_PKG

echo
echo "Signing the form deck tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../appsignerA1.crt --key ../appsignerA1.key $appPrefix-Forms.tar

tar -cvzf ../USR1.$appPrefix-Forms.tgz ./*

cd $home

echo
echo "Signing the usr1 package tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ./appsignerA1.crt --key ./appsignerA1.key USR1.$appPrefix-Forms.tgz

rm -f -r FRM_PKG

echo
echo "Creating the download package dl-$appPrefix-Forms.tgz"
tar -cvzf dl-$appPrefix-Forms.tgz USR1.$appPrefix-Forms.tgz*

rm -f -r USR1.$appPrefix-Forms.tgz*
