appPrefix="pointMx_SCA"
STG_DIR=C:/staging
home=`pwd`

scrUsageDisp()
{
	echo "usage mkpkg.sh [version] [app folder] [form folder] [SCA_PACKAGE[1/0]"
	echo "example: mkpkg.sh 2.3"
}

cleanup()
{
	echo "Removing directories if present"

	if [ -d APP_PKG ]; then
		echo "Removing APP_PKG"
		rm -f -r APP_PKG
	fi

	if [ -d FRM_PKG ]; then
		echo "Removing FRM_PKG"
		rm -f -r FRM_PKG
	fi

	if [ -d PKG ]; then
		echo "Removing PKG"
		rm -f -r PKG
	fi
}

if [ "$1" = "" ]; then
	echo "ERROR: Need to suppy version"
	scrUsageDisp
	return 1
fi

if [ "$2" = "" ]; then
	echo "ERROR: Need to suppy application path"
	scrUsageDisp
	return 1
fi

if [ "$3" = "" ]; then
	echo "ERROR: Need to suppy form deck path"
	scrUsageDisp
	return 1
fi

if [ "$4" = "" ]; then
	echo "ERROR: Need to supply if SCA FLASH FILES PACKAGE is required or not"
	scrUsageDisp
	return 1
fi

APP_EXEC_DIR=$2
FRM_DIR=$3

if ! [ -d "$APP_EXEC_DIR" ]; then
	echo "ERROR: Invalid Path given: $APP_EXEC_DIR.. Exiting..."
	return 1
fi

if ! [ -d "$FRM_DIR" ]; then
	echo "ERROR: Invalid Path given: $FRM_DIR.. Exiting..."
	return 1
fi

today=$(date +%d%m%y)
ver=$1
appName="$appPrefix-$ver-$today"

cleanup

echo "***********************************************************************"
echo "Creating the application bundle"
echo "***********************************************************************"
mkdir -p APP_PKG/lib APP_PKG/CONTROL

#Stripping the libraries in the lib folder
echo "***********************************************************************"
echo "Stripping the libraries in lib folder"
echo "***********************************************************************"
arm-verifone-linux-gnueabi-strip.exe --strip-unneeded -F elf32-littlearm $APP_EXEC_DIR/lib/*

#Copy the data into the folder for the application
cp $APP_EXEC_DIR/output/*.exe APP_PKG
cp $APP_EXEC_DIR/lib/*.so APP_PKG/lib
cp $APP_EXEC_DIR/CONFIG/config.usr1 APP_PKG/CONTROL
cp $APP_EXEC_DIR/CONFIG/.profile APP_PKG/CONTROL
cp $APP_EXEC_DIR/CONFIG/rest/* APP_PKG
cp $APP_EXEC_DIR/certs/ca-bundle.crt APP_PKG

touch control
echo "Package: $appPrefix-App" > control
echo "Version: $ver" >> control
echo "User: usr1" >> control

mv control APP_PKG/CONTROL

#create the tar of the application
cd APP_PKG
tar -cvf ../$appPrefix-App.tar ./*

cd $home
rm -f -r APP_PKG

echo "***********************************************************************"
echo "Creating the form bundle"
echo "***********************************************************************"
mkdir -p FRM_PKG/CONTROL FRM_PKG/crt FRM_PKG/xpi

touch control
echo "Package: $appPrefix-Forms" > control
echo "Version: $ver" >> control
echo "User: usr1" >> control
echo "Type: userflash" >> control

mv control FRM_PKG/CONTROL
cp ./appsignerA1.crt FRM_PKG/crt
cp -R $FRM_DIR/themes FRM_PKG
cp -R $APP_EXEC_DIR/XPI_FORMS/* FRM_PKG/xpi/


cd FRM_PKG

cd xpi
echo "*************Signing XPI Forms**************" 
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_2LINE16_860.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_2LINE16_925.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_2LINE20_860.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_2LINE20_925.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_4LINE20_860.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_4LINE20_925.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_5APP_SELECTION_860.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_5APP_SELECTION_925.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_APP_SELECTION_860.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_APP_SELECTION_925.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_APP_SELECTION_BUT_860.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_EMVPIN_ENTRY_860.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_EMVPIN_ENTRY_925.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_PIN_ENTRY_860.FRM
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key FORM_PIN_ENTRY_925.FRM

cd ..

cp $FRM_DIR/*.tgz .
tar -xf *.tgz
rm -f *.tgz

if [ "$4" = "1" ]; then
	echo "***********************************************************************"
	echo "Adding the SCA FILES to the form bundle"
	echo "***********************************************************************"
	cp -R $APP_EXEC_DIR/SCAFiles/* .
	cp -R $APP_EXEC_DIR/SCAFiles/.profile .
	$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../crt/appsignerA1.crt --key ../../appsignerA1.key PCI_BIT.DAT
fi

#create the tar of the form deck
tar -cvf ../$appPrefix-Forms.tar ./* ./.profile

cd $home
rm -f -r FRM_PKG

echo "***********************************************************************"
echo "Creating the download bundle"
echo "***********************************************************************"
mkdir -p PKG/CONTROL PKG/crt
touch control
echo "Package: $appPrefix" > control
echo "Version: $ver" >> control
echo "User: usr1" >> control

mv control PKG/CONTROL
cp ./appsignerA1.crt PKG/crt
mv $appPrefix-App.tar PKG
mv $appPrefix-Forms.tar PKG

cd PKG

echo
echo "Signing the application tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../appsignerA1.crt --key ../appsignerA1.key $appPrefix-App.tar

echo
echo "Signing the form deck tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../appsignerA1.crt --key ../appsignerA1.key $appPrefix-Forms.tar

tar -cvzf ../USR1.$appName.tgz ./*

cd $home

echo
echo "Signing the usr1 package tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ./appsignerA1.crt --key ./appsignerA1.key USR1.$appName.tgz

rm -f -r PKG

echo
echo "Creating the download package dl-$appName.tgz"
tar -cvzf dl-$appName.tgz USR1.$appName.tgz*

rm -f -r USR1.$appName.tgz*
