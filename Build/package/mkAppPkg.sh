appPrefix="PointMx_SCA"
STG_DIR=c:\staging
home=`pwd`

scrUsageDisp()
{
	echo "usage mkAppPkg.sh [version] [app folder]"
	echo "example: mkAppPkg.sh 2.19.12 app folder name"
}

cleanup()
{
	echo "Removing directories if present"

	if [ -d APP_PKG ]; then
		echo "Removing APP_PKG"
		rm -f -r APP_PKG
	fi

	if [ -d APP_PKG ]; then
		echo "Removing APP_PKG"
		rm -f -r APP_PKG
	fi
}

if [ "$1" = "" ]; then
	echo "ERROR: Need to suppy version"
	scrUsageDisp
	return 1
fi

if [ "$2" = "" ]; then
	echo "ERROR: Need to suppy application path"
	scrUsageDisp
	return 1
fi

APP_EXEC_DIR=$2

if ! [ -d "$APP_EXEC_DIR" ]; then
	echo "ERROR: Invalid Path given: $APP_EXEC_DIR.. Exiting..."
	return 1
fi

today=$(date +%d%m%y)
ver=$1
appName="$appPrefix-$ver-$today"

cleanup

echo "***********************************************************************"
echo "Creating the application bundle"
echo "***********************************************************************"
mkdir -p APP_PKG/lib APP_PKG/CONTROL

#Copy the data into the folder for the application
cp $APP_EXEC_DIR/output/*.exe APP_PKG
cp $APP_EXEC_DIR/lib/*.so APP_PKG/lib
cp $APP_EXEC_DIR/CONFIG/config.usr1 APP_PKG/CONTROL
cp $APP_EXEC_DIR/CONFIG/.profile APP_PKG/CONTROL
cp $APP_EXEC_DIR/CONFIG/rest/*.ini APP_PKG
cp $APP_EXEC_DIR/certs/ca-bundle.crt APP_PKG

touch control
echo "Package: $appPrefix-App" > control
echo "Version: $ver" >> control
echo "User: usr1" >> control

mv control APP_PKG/CONTROL

#create the tar of the application
cd APP_PKG
tar -cvf ../$appPrefix-App.tar ./*

cd $home
rm -f -r APP_PKG

echo "***********************************************************************"
echo "Creating the download Application Bundle"
echo "***********************************************************************"
mkdir -p APP_PKG/CONTROL APP_PKG/crt
touch control
echo "Package: $appPrefix" > control
echo "Version: $ver" >> control
echo "User: usr1" >> control

mv control APP_PKG/CONTROL
cp ./appsignerA1.crt APP_PKG/crt
mv $appPrefix-App.tar APP_PKG

cd APP_PKG

echo
echo "Signing the application tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../appsignerA1.crt --key ../appsignerA1.key $appPrefix-App.tar

tar -cvzf ../USR1.$appName.tgz ./*

cd $home

echo
echo "Signing the usr1 package tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ./appsignerA1.crt --key ./appsignerA1.key USR1.$appName.tgz

rm -f -r APP_PKG

echo
echo "Creating the download package dl-$appName.tgz"
tar -cvzf dl-$appName.tgz USR1.$appName.tgz*

rm -f -r USR1.$appName.tgz*
