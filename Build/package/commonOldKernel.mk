# -----------------------------------------------------------------------------
# Common make data
# -----------------------------------------------------------------------------

STAGING_DIR := c:/staging
STRIP_FLAGS	:= --strip-unneeded -F elf32-littlearm
SYSROOT_CFLAGS := --sysroot=$(STAGING_DIR)
ARCH := arm-verifone-linux-gnueabi-
CPP := $(ARCH)gcc.exe $(SYSROOT_CFLAGS)
SDK := $(STAGING_DIR)
STRIP := $(ARCH)strip.exe $(STRIP_FLAGS)

SRC_PATH := ./src/
INC_PATH := ../../inc
D_OBJ_PATH := ./debugObj/
R_OBJ_PATH := ./releaseObj/
OUTDIR := ./output
RM = rm -rf

R_OBJ := $(patsubst $(SRC_PATH)%.c,$(R_OBJ_PATH)%.o,$(wildcard $(SRC_PATH)*.c))
D_OBJ := $(patsubst $(SRC_PATH)%.c,$(D_OBJ_PATH)%.o,$(wildcard $(SRC_PATH)*.c))

R_LIB := $(OUTDIR)/$(OUTPUT).so
D_LIB := $(OUTDIR)/$(OUTPUT)D.so

INC_PATHS = -I"$(INC_PATH)" \
			-I"$(SDK)/usr/include" \
			-I"$(SDK)/usr/local/include/svcmgr" \
			-I"$(SDK)/usr/local/include/" \
			-I"$(SDK)/usr/local/include/fancypants" \
			-I"$(SDK)/usr/include/libxml2" 

LIB_PATHS = -L"$(SDK)/lib" \
			-L"$(SDK)/usr/local/lib"\
			-L"$(SDK)/usr/lib" \
			-L"../../lib" \
			-L"$(SDK)/usr/local/lib/svcmgr"

#CFLAGS = $(INC_PATHS) $(CUST_INCPATHS) -fPIC -fexpensive-optimizations -O2 -Wall
CFLAGS = $(INC_PATHS) $(CUST_INCPATHS) -fPIC -O2 -Wall
DCFLAGS = -DDEBUG -DDEVDEBUG
RLFLAGS = -shared -Wl,-soname,$(notdir $(R_LIB))
DLFLAGS = -shared -Wl,-soname,$(notdir $(D_LIB))

.PHONY: REL DEB all clean clean-custom post-build

all: $(R_LIB) $(D_LIB) post-build
REL: $(R_LIB) 
DEB: $(D_LIB) 

clean: clean-custom
	@echo ""
	@echo "Cleaning Release objects/binaries........"
	@echo ""
	${RM} $(R_OBJ) $(R_LIB)
	@echo ""
	@echo "Cleaning Debug objects/binaries........"
	@echo ""
	${RM} $(D_OBJ) $(D_LIB)


$(R_LIB): $(R_OBJ)
	@echo ""
	@echo "Linking $(notdir $(R_LIB)) .........."
	@echo ""
	$(CPP) $(R_OBJ) -o $(R_LIB) $(RLFLAGS) $(LIB_PATHS) $(CLIBS) $(RLIBS)
	@echo ""
	@echo "Stripping the lib $(R_LIB) ................"
	$(STRIP) -o $(R_LIB)  $(R_LIB)
	@echo ""
	
$(D_LIB): $(D_OBJ)
	@echo ""
	@echo "Linking $(notdir $(D_LIB)) .........."
	@echo ""
	$(CPP) $(D_OBJ) -o $(D_LIB) $(DLFLAGS) $(LIB_PATHS) $(CLIBS) $(DLIBS)
	@echo ""
	@echo "Stripping the lib $(D_LIB) ................"
	$(STRIP) -o $(D_LIB)  $(D_LIB)
	@echo ""

$(R_OBJ_PATH)%.o: $(SRC_PATH)%.c
	$(CPP) $(CFLAGS) -c $< -o $@

$(D_OBJ_PATH)%.o: $(SRC_PATH)%.c
	$(CPP) $(CFLAGS) $(DCFLAGS) -c $< -o $@

post-build:
	@echo "installing $(OUTPUT) library"
	@echo ""
	install -m 0755 ./$(OUTDIR)/*.so* ../../appExecData/lib
