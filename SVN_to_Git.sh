#!/bin/bash
git svn clone https://blrwvisdsvn1.verifone.com/svn/Paas/ -T trunk -b branch -t tag
cd Paas
cp -Rf .git/refs/remotes/origin/tags/* .git/refs/tags/
rm -Rf .git/refs/remotes/origin/tags
cp -Rf .git/refs/remotes/origin/* .git/refs/heads/
rm -Rf .git/refs/remotes/origin
git push SCA --all
git push SCA --tags
